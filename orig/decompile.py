import os
import re
import sys
import time
import glob
import shutil
import javalang
from pathlib import Path
import threading
from concurrent.futures import ThreadPoolExecutor, wait
from ruamel.std.zipfile import ZipFile
from ruamel.std.zipfile import delete_from_zip_file
import multiprocessing

cpus = multiprocessing.cpu_count()

APK = list(Path(__file__).parent.glob('com.fossil.wearables.fossil*.apk'))[0].name
print("Processing", APK)

mapping = {}
# orig_coroutine_import = None
mapping_file = Path('../mapping_smali.txt')
if mapping_file.exists():
    print("Loading", mapping_file)
    mapping_text = mapping_file.read_text()

    # orig_coroutine = re.findall(r'com.fossil.(\S+) *-> *com.mapped.Coroutine', mapping_text)[0]
    # orig_coroutine_import = f'com.fossil.{orig_coroutine}'
    for old, new in re.findall(r'^com.fossil.([a-zA-Z0-9\.]+) *-> *(com.\S+)$', mapping_text, flags=re.MULTILINE):
        mapping[old] = new
        # print(old, '->', new)

import shutil

import os
import sys
from zipfile import ZipFile
from concurrent.futures import ThreadPoolExecutor


target = "java_decomp"

"""
Re-run particular files by passing on command line
"""
# jadx_command_verbose = r'jadx-1.0.0-b1165-8ba3e935\bin\jadx -d src -e --no-imports --show-bad-code --no-inline-anonymous --escape-unicode --rename-flags none --single-class %s ' + APK
# verbose = False
# if len(sys.argv) > 1:
#     # print(sys.argv)
#     jadx = Path(sys.argv[1]).resolve()
#     try:
#         tail = jadx.relative_to(Path(r".\src\src\main\java").resolve())
#     except ValueError:
#         dest = jadx
#         tail = jadx.relative_to(Path("../src/main", target).resolve())
#         jadx = Path(r".\src\src\main\java") / tail
#     else:
#         dest = Path(r"..\src\main", target) / tail

#     print("Verbose processing", jadx)

#     class_name = str(tail)[:-5].replace('\\', '.').replace('/', '.')
#     os.system(jadx_command_verbose % class_name)    

#     try:
#         rewrite(jadx, dest, True)
#     except:
#         print("Failed, copying instead for inspection:", dest)
#         dest.parent.mkdir(parents=True, exist_ok=True)
#         dest.write_text(jadx.read_text())
#     sys.exit()

"""
Normal Usage
"""

conversions = (
  (Path(r".\src\src\main\java\com\fossil"), Path(r"..\src\main\%s\com\fossil" % target)), 
  (Path(r".\src\src\main\java\com\mapped"), Path(r"..\src\main\%s\com\mapped" % target)), 
  (Path(r".\src\src\main\java\com\misfit"), Path(r"..\src\main\%s\com\misfit" % target)), 
  (Path(r".\src\src\main\java\com\portfolio"), Path(r"..\src\main\%s\com\portfolio" % target)), 
)

dirname = APK[:-4]

orig_apk_name = "orig_" + APK
orig_apk = Path(orig_apk_name)
if not orig_apk.exists():
    # shutil.copyfile(APK, orig_apk)
    odir = f'orig_{dirname}'
    os.system(f'java -jar apktool_2.5.0.jar d {APK} -f -o {odir}')
    drawabledir = Path(odir) / 'res' / 'drawable'
    (drawabledir / '$avd_hide_password__0.xml').rename(drawabledir / 'avd_hide_password__0.xml')
    (drawabledir / '$avd_hide_password__1.xml').rename(drawabledir / 'avd_hide_password__1.xml')
    (drawabledir / '$avd_hide_password__2.xml').rename(drawabledir / 'avd_hide_password__2.xml')
    (drawabledir / '$avd_show_password__0.xml').rename(drawabledir / 'avd_show_password__0.xml')
    (drawabledir / '$avd_show_password__1.xml').rename(drawabledir / 'avd_show_password__1.xml')
    (drawabledir / '$avd_show_password__2.xml').rename(drawabledir / 'avd_show_password__2.xml')
    # public = Path(odir) / 'res' / 'values' / 'public.xml'
    # public.write_bytes(
    #     public.read_bytes().replace(b'$avd_', b'avd_')
    # )
   
    for filepath in (list(glob.iglob(f'{odir}/res/**/*', recursive=True)) + list(glob.iglob(f'{odir}/META-INF/*', recursive=True))):
        if Path(filepath).is_file():
            c = Path(filepath).read_bytes()
            n = c.replace(b'$avd_', b'avd_')
            if len(n) != len(c):
                print("Fixing $avd in", filepath)
                Path(filepath).write_bytes(n)

    os.system(f'java -jar apktool_2.5.0.jar b {odir}')
    Path(APK).rename(orig_apk)
    (Path(odir) / 'dist' / APK).rename(Path('.') / APK)
    sys.exit()


if not Path(dirname).exists():
    print("Extracting APK")
    os.makedirs(dirname, exist_ok=True)

    # os.system('java -jar apktool-cli-all_b05f19b.jar d com.fossil.wearables.fossil-4.2.0.apk')
    with ZipFile(APK, 'r') as zipObj:
       # Extract all the contents of zip file in current directory
       zipObj.extractall(dirname)

dex_files = [f for f in os.listdir(dirname) if f.endswith('.dex')]

# dexpatcher_jar = Path.home() / r'.gradle/caches/modules-2/files-2.1/dexpatcher-repo.dexpatcher.dexpatcher-tool/dexpatcher/1.8.0-alpha3/6c242e8efc14f4f662e166c76ed9b8d6b7a3e9af/dexpatcher-1.8.0-alpha3.jar'

try:
    Path('src').rename('src_old')
    threading.Thread(target = lambda : shutil.rmtree('src_old')).start()
except:
    pass

if mapping:
    start = time.time()
    for d in Path(dirname).glob('smali*'):
        shutil.rmtree(d, ignore_errors=True)

    smali_packages = []
    for dex in dex_files:
        dest = dirname + '\\' + dex.replace('.dex', '').replace('classes', 'smali')
        smali_packages.append(Path(dest))
        command = f'java -jar baksmali-2.4.0.jar dis -o {dest} {dirname}\\{dex}'
        ret = os.system(command)

    smali_mapping = {
        b"Lcom/portfolio/platform/PortfolioApp$a;": b"Lcom/portfolio/platform/PortfolioApp$inner;",
        b"Lcom/portfolio/platform/PortfolioApp$a$": b"Lcom/portfolio/platform/PortfolioApp$inner$",
    }
    for o, n in mapping.items():
        smali_mapping[b"Lcom/fossil/%s;" % o.encode()] = b"L%s;" % n.replace('.', '/').encode()
        smali_mapping[b"Lcom/fossil/%s$" % o.encode()] = b"L%s$" % n.replace('.', '/').encode()
        smali_mapping[b"Lcom/fossil/%s<" % o.encode()] = b"L%s<" % n.replace('.', '/').encode()

    PortfolioApp_inner = None
    with ThreadPoolExecutor(max_workers=cpus*2) as executor:

        for pkg in smali_packages:
            print("Mapping in smali:", pkg)
            for root, dirs, files in os.walk(pkg):
                def srename(fname):
                    f = Path(root) / fname
                    contents = f.read_bytes()
                    # Ensure inner classes/enums are uniquly named
                    # contents = re.sub(b"(Lcom/fossil/[a-z0-9]+\\$)([a-z])\\$([a-z])([;<])",
                    #                   b'\\1\\U2i$\\U3ii\\4', contents)
                    # contents = re.sub(b"(Lcom/fossil/[a-z0-9]+\\$)([a-z])([;<])",
                    #                   b'\\1\\U2i$\\3', contents)
                    def upper_inners(g):
                        m = g.groups()
                        s = [m[0]]
                        for i, a in enumerate(m[1:-1]):
                            if a:
                                s.append(a.upper())
                                s.append(b'i'*(i+1))
                        s.append(m[-1])
                        r = b''.join(s)
                        return r
                    contents = re.sub(b"(Lcom/fossil/[a-z0-9]+)(\\$[a-z])(\\$[a-z])?(\\$[a-z])?(\\$[a-z])?([;<])",
                                      upper_inners, contents)
                    cname = re.match(b'^\\.class .*? Lcom/.*\\$([A-Z]i+);', contents)
                    if cname:
                        contents = re.sub(b'name = "[a-z]"', b'name = "%s"' % cname.group(1), contents)
                        

                    if fname == 'PortfolioApp$a.smali':
                        PortfolioApp_inner = f
                        contents = contents.replace(b'name = "a"', b'name = "inner"')
                    if b'Lcom/fossil' in contents or b'Lcom/portfolio' in contents:
                        for old, new in smali_mapping.items():
                            contents = contents.replace(old, new)
                        # Make all remaining obf classes start with uppercase for better decompiling
                        contents = re.sub(b"Lcom/fossil/([a-z0-9\\$]+[;\\$<])",
                                          lambda g: b'Lcom/fossil/' + g.groups()[0][0:1].upper()+g.groups()[0][1:],
                                          contents)
                        f.write_bytes(contents)

                for _ in executor.map(srename, files):
                    pass

    print("smali mapping took", (time.time() - start))

    if PortfolioApp_inner:
        PortfolioApp_inner = PortfolioApp_inner.parent / 'PortfolioApp$inner.smali'
        PortfolioApp_inner.rename(PortfolioApp_inner)
        # c = PortfolioApp_inner.read_bytes()
        # c = re.sub(
        #     b'(\.annotation system Ldalvik/annotation/InnerClass;\r?\n(    .*\r?\n)*    )name = ".*"$',
        #     b'\\1name = "inner"',
        #     c, flags=re.MULTILINE
        # )
        # PortfolioApp_inner.write_bytes(c.replace(b'name = "a"', b'name = "inner"'))

    for smali_path in smali_packages:
        mapped_folder = smali_path / 'com' / 'mapped'
        mapped_folder.mkdir(exist_ok=True)
        for old, new in mapping.items():
            for o in [smali_path / 'com' / 'fossil' / (old+'.smali')] + \
                     list((smali_path / 'com' / 'fossil').glob(f'{old}$*.smali')):
                if o.exists():
                    newpath = Path(str(o).replace('\\','/').replace(f'com/fossil/{old}', new.replace('.', '/')))
                    newpath.parent.mkdir(parents=True, exist_ok=True)
                    print(o, '->', newpath)

                    o.rename(newpath)
                    # if '$' in str(newpath):
                    #     n = Path(newpath)
                    #     contents = newpath.read_bytes()
                    #     newname = str(newpath.stem).split('$')[-1]
                    #     # inner classes sometimes have a name specified here in annotation which conflicts with our renaming
                    #     contents = re.sub(
                    #         b'(\.annotation system Ldalvik/annotation/InnerClass;\r?\n(    .*\r?\n)*    )name = ".*"$',
                    #         b'\\1name = "%s"' % newname.encode(),
                    #         contents, flags=re.MULTILINE
                    #     )
                    #     newpath.write_bytes(contents)
                    #     sys.exit()

        for obf in (smali_path / 'com' / 'fossil').glob('*.smali'):
            name = obf.name
            newname = name[0].upper() + name[1:]
            obf.rename((smali_path / 'com' / 'fossil') / newname)

    smapped = Path('.') / 'smapped'
    smapped.mkdir(parents=True, exist_ok=True)
    os.system(f'java -jar smali-2.4.0.jar as -o {smapped}/classes.dex {dirname}\smali')
    os.system(f'java -jar smali-2.4.0.jar as -o {smapped}/classes2.dex {dirname}\smali2')
    os.system(f'java -jar smali-2.4.0.jar as -o {smapped}/classes3.dex {dirname}\smali3')
    # os.system(f'java -jar smali-2.4.0.jar as -o {smapped}/classes4.dex {dirname}\smali4')

    smapped_apk_name = "sp_" + APK
    smapped_apk = Path(smapped_apk_name)
    if not smapped_apk.exists():
        shutil.copyfile(APK, smapped_apk)

    delete_from_zip_file(str(smapped_apk), pattern="classes.*")
    with ZipFile(smapped_apk, 'a') as smapped_apk_z:
        smapped_apk_z.write(f"{smapped}/classes.dex", arcname="classes.dex")
        smapped_apk_z.write(f"{smapped}/classes2.dex", arcname="classes2.dex")
        smapped_apk_z.write(f"{smapped}/classes3.dex", arcname="classes3.dex")

    dirname = smapped

dexpatcher_jar = None
# dexpatcher = 'dexpatcher-1.8.0-alpha4.jar'
# for root, dirs, files in os.walk(Path.home() / '.gradle' / 'caches' / 'modules-2' / 'files-2.1'):
#     if dexpatcher in files:
#         dexpatcher_jar = Path(root) / dexpatcher

# dexpatcher_jar = Path.home() / '.gradle' / 'caches' / 'modules-2' / 'files-2.1' / 'dexpatcher-repo.dexpatcher.dexpatcher-tool/dexpatcher/1.8.0-alpha4/6b5e1a945d4319dd41554eca80dbb8791b2e7e14/dexpatcher-1.8.0-alpha4.jar'
# dexpatcher_jar = Path(__file__).parent / '..' / '..' / 'dexpatcher-tool' / 'tool' / 'build' / 'libs' / 'dexpatcher-1.8.0-alpha4.jar'
dexpatcher_jar = Path(__file__).parent / "dexpatcher-1.8.0-beta1.jar"

# dex_files
dex_mapping_file = Path('../mapping.txt')
os.makedirs('Anon', exist_ok=True)
command = f'java -jar "{dexpatcher_jar.resolve()}" --multi-dex-threaded --max-dex-pool-size 196608  --deanon-source --main-plan "Anon[_Level]" --pre-transform all --output Anon {dirname}'
if dex_mapping_file.exists():
    print("Using", dex_mapping_file)
    command += f' --map-source --map {dex_mapping_file}'

print(command)
ret = os.system(command)
if ret != 0:
    raise SystemExit()

dirname = 'Anon'

dedex_files = [f'{dirname}\\{f}' for f in os.listdir('Anon') if f.endswith('.dex')]


# jarfiles = []
# for dex in dedex_files:
#     # d2j
#     outfile = dex.replace(".dex", ".jar")
#     jarfiles.append(outfile)
#     # if not Path(outfile).exists():
#     command = f'dex-tools-2.1-SNAPSHOT\\d2j-dex2jar.bat --force {dex} -o {outfile}'
#     os.system(command)

apkfile = f'{dirname}/classes.apk'
jarfile = f'{dirname}/classes.jar'
jarfiles = [jarfile]
with ZipFile(apkfile,'w') as zip:
    for dex in dedex_files:
        zip.write(dex, arcname=Path(dex).name)
command = f'dex-tools-2.1-SNAPSHOT\\d2j-dex2jar.bat --force {apkfile} -o {jarfile}'
os.system(command)

USE_CFR = False
if USE_CFR:
    # cfr_command = f'java -jar cfr-0.151-SNAPSHOT.jar --outputdir src\\src\\main\\java --removeinnerclasssynthetics false --forbidanonymousclasses --usesignatures false --jarfilter "com.(fossil|mapped|misfit|portfolio)" {outfile}'
    cfr_command = f'java -jar cfr-0.151-SNAPSHOT.jar --outputdir src\\src\\main\\java --removeinnerclasssynthetics false --forbidanonymousclasses true --usesignatures false --jarfilter "com.(fossil|mapped|misfit|portfolio)" {jarfile}'
    print(cfr_command)
    ret = os.system(cfr_command)
    if ret != 0:
        raise SystemExit()
else:
    # Path("src").rename("src_prev")
    jadx_command = f'jadx-1.2.0-b1429-5c75f249\\bin\\jadx -d src -e -j 16 --no-inline-anonymous --escape-unicode --respect-bytecode-access-modifiers --rename-flags printable {jarfile}'
    # --show-bad-code results in truncation - need to raise issue
    # jadx_command = f'jadx-1.2.0-b1429-5c75f249\\bin\\jadx -d src -e -j 16 --show-bad-code --no-inline-anonymous --escape-unicode --respect-bytecode-access-modifiers --rename-flags printable {jarfile}'
    print(jadx_command)
    ret = os.system(jadx_command)
    if ret != 0:
        raise SystemExit()
    pass



def rewrite(sample, target, verbose=False):
    java = ''
    try:
        # print(sample, '->', target)
        java = sample.read_text()
        # print(len(java))

        java = re.sub(r'^( +)\? ', r'\1Object ', java, flags=re.MULTILINE)
        java = java.replace('??', 'Object')

        java = re.sub(r'(public final \S+> create\(.*\{\n)(( +.*;\n)+?)( *)(return \S+;)', r'\1\2\4throw null;\n\4//\5', java, flags=re.MULTILINE)
        java = re.sub(r'(public final \S+ invoke\(.*\{\n)( *)(return .+;)', r'\1\2throw null;\n\2//\3', java, flags=re.MULTILINE)

        # "implements" doesn't seem to get mapped correctly
        # if orig_coroutine_import and orig_coroutine_import in java:
        #     java = java.replace(orig_coroutine_import, 'com.mapped.Coroutine')
        #     java = java.replace(orig_coroutine, 'Coroutine')
        #
        #     # java = re.sub(r'implements Coroutine(<[a-z0-9]+, [a-z0-9]+<\? super [a-z0-9]+>, Object>)', r'implements Coroutine/*\1*/', java)
        #     # java = re.sub(r'implements Coroutine(<[a-z0-9]+, [a-z0-9]+<\? super [a-z0-9]+>, Object>)', r'implements Coroutine/*\1*/', java)
        #     # java = re.sub(
        #     #     r'(public final [a-z0-9]+)(<[a-z0-9]+>)(create\(Object obj, [a-z0-9]+)(<.*?>)( [a-z0-9]+)(\) {)',
        #     #     r'\1/*\2*/\3/*\4*/\5\6', java
        #     # ) # public final s67<v47> create(Object obj, s67<?> s67) {
        #
        #     for unmapped in re.findall(r'implements Coroutine<([a-z0-9]+), ([a-z0-9]+)<\? super ([a-z0-9]+)>, Object>', java):
        #         for pkg in unmapped:
        #             print('unmapped:', pkg)
        #             mapped = mapping.get(pkg)
        #             if mapped:
        #                 print(f"Fixing {pkg} -> {mapped}")
        #                 java = java.replace(f'com.fossil.{pkg}', f'com.mapped.{mapped}')
        #                 java = java.replace(pkg, mapped)

        lines = java.split('\n')

        remove = []
        for i, line in enumerate(lines):
            if '/* access modifiers changed from' in line:
                correct = line.split(': ')[1].split(' ')[0]
                correct = " " if correct == "0000" else (" "+correct+" ")
                n = i+1
                lines[n] = lines[n].replace(" public ", correct)
                remove.append(i)
            if '    kotlin.jvm.internal.Intrinsics.' in line:
                line = line.replace('kotlin.jvm.internal.Intrinsics.', '// kotlin.jvm.internal.Intrinsics.')
                lines[i] = line

            if '@kotlin.Metadata' in line:
                line = line.replace('@kotlin.Metadata', '// @kotlin.Metadata')
                lines[i] = line
    
            if '** GOTO' in line:
                line = re.sub('( +)(\\*\\* GOTO )', '\\1throw null;\n\\1//\\2', line)
                lines[i] = line
    
            if '** if' in line:
                line = re.sub('( +)\\*\\* if ', '\\1/* ** */ if ', line)
                line = re.sub('( goto \\S+$)', ' //\\1', line)
                lines[i] = line

            if ' void ' in line:
                line = re.sub('(^ +)void (\\S+;)', '\\1Object \\2  // void declaration', line)
                lines[i] = line
        
            if '(void)' in line:
                line = re.sub(r'\(void\)(.*)$', '(Object)\\1  // void declaration', line)
                lines[i] = line
                print(line)

            if ' public static class com.' in line or ' public class com.' in line:
                line = re.sub('(^ +public .*class? )(com\\..*?\\$)(\\S+ {)', '\\1\\3', line)
                lines[i] = line
            
            if line.startswith('lbl-'):
                line = '// ' + line
                lines[i] = line
            
            if '} else lbl-' in line:
                line = line.replace('} else ', '} else //')
                lines[i] = line


        for i in reversed(remove):
            lines.pop(i)

        java = "\n".join(lines)

        java = re.sub(r"( +)(static \{\n)((.*\n)+?)(\1})", r"\1/*\n\1\2\3\5\n\1*/", java)

        tree = javalang.parse.parse(java)
        #     print(tree)
        classes = [node for path, node in tree.filter(javalang.tree.ClassDeclaration)]
        fields = [node for path, node in tree.filter(javalang.tree.FieldDeclaration)]
        methods = [node for path, node in tree.filter(javalang.tree.MethodDeclaration)]
        inits = [node for path, node in tree.filter(javalang.tree.ConstructorDeclaration)]
        enums = [node for path, node in tree.filter(javalang.tree.EnumDeclaration)]
        iface = [node for path, node in tree.filter(javalang.tree.InterfaceDeclaration)]

        details = sorted(classes + fields + methods + inits + enums + iface, key=lambda x:x.position)

        lines = java.split('\n')

        interface = []

        if not details:
            if 'public @interface' in java:
                print("Ignoring @interface, copy to %s" % target)
                Path(target).write_text(java)
                return

        if isinstance(details[0], javalang.tree.ClassDeclaration):
            header = "\n" \
                     "import lanchon.dexpatcher.annotation.DexEdit;\n" \
                     "import lanchon.dexpatcher.annotation.DexIgnore;\n" \
                     "import lanchon.dexpatcher.annotation.DexAction;\n" \
                     "\n" \
                     "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"
            # "@DexEdit(defaultAction = DexAction.IGNORE)"

        elif isinstance(details[0], (javalang.tree.InterfaceDeclaration, javalang.tree.EnumDeclaration)):
            header = "\n" \
                     "import lanchon.dexpatcher.annotation.DexEdit;\n" \
                     "import lanchon.dexpatcher.annotation.DexIgnore;\n" \
                     "import lanchon.dexpatcher.annotation.DexAction;\n" \
                     "\n" \
                     "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"

        else:
            raise ValueError(sample)

        for i in iface:
            #         print(dir(i))
            #         print(i.children)
            #         print(i.fields)
            interface.extend([n for n in i.fields if isinstance(n, javalang.tree.FieldDeclaration)])

        #     print(interface)

        pos = details[0].position.line-1
        while lines[pos-1].strip().startswith('@') or \
                lines[pos-1].strip().startswith('/* ') or \
                lines[pos-1].strip().startswith('// '):
            pos -= 1

        lines.insert(pos, header)

        for node in reversed(details[1:]):

            line = lines[node.position.line]

            if isinstance(node, javalang.tree.FieldDeclaration) and node not in interface:
                line = line.replace(' final ', ' /* final */ ')
                line = line.replace(' = ', '; // = ')
                lines[node.position.line] = line

            #             print(dir(node))
            #             print(node)

            indent = len(line) - len(line.lstrip())

            if isinstance(node, javalang.tree.ClassDeclaration):
                prev = lines[node.position.line-1]
                if "/* renamed from" in prev:
                    clash_target = prev.strip().split(' ')[3]
                    annotation = '@DexEdit(defaultAction = DexAction.IGNORE, target = "%s")' % clash_target
                else:
                    annotation = "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"
            else:
                annotation = "@DexIgnore"

            pos = node.position.line
            while lines[pos-1].strip().startswith('@') or \
                    lines[pos-1].strip().startswith('// ') or \
                    lines[pos-1].strip().startswith('/* '):
                pos -= 1

            lines.insert(pos, (" "*indent + annotation))


        # print("Writing:", target)
        target.parent.mkdir(parents=True, exist_ok=True)
        with open(target, 'wb') as f:
            f.write(("\n".join(lines)).encode())
        # print(target.write_bytes()
        # os.system('git add %s' % target)

    except Exception as ex:
        import traceback
        print(sample, '->', target)
        traceback.print_exc()
        print("EXCEPTION IN", sample, ex)
        target.parent.mkdir(parents=True, exist_ok=True)
        if java:
            Path(target).write_text(java)
        # raise


targets = []
with ThreadPoolExecutor(max_workers=cpus) as executor:

    for jadx, dest in conversions:

        args = []
        for root, dirs, files in os.walk(jadx):
            for f in files:
                # if 'com/fossil/blesdk/obfuscated' in root.replace('\\', '/'):
                #     continue

                sample = Path(os.path.join(root, f))

                rel = os.path.relpath(root, jadx)
                target = dest / rel / f

                targets.append(target)

                if target.exists() and False:

                    # print("Not replacing", target)
                    pass
                else:
                    args.append((sample, target))

                    # try:
                    #     futures.append(executor.submit(rewrite, ))
                    # except:
                    #     print(sample)
                    #     # raise
                    # # executor.submit(os.system, "git add %s" % target)
        # print(len(args))
        for _ in executor.map(lambda x: rewrite(*x), args):
            pass

for root, dirs, files in os.walk(dest):
    for f in files:
        fp = Path(root) / f
        if fp not in targets:
            fp.unlink()

os.system('git add %s' % dest)
