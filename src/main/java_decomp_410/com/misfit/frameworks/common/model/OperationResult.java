package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum OperationResult implements Parcelable {
    STATE_SUCCESSFUL(0),
    STATE_FAILED(1),
    STATE_REMOTE_UNREACHABLE(2),
    STATE_REMOTE_CONNECTION_LOST(3),
    STATE_NOT_REGISTERED(4),
    STATE_NOT_LOGGED_IN(5),
    STATE_UNKNOWN(-1);
    
    @DexIgnore
    public static /* final */ Parcelable.Creator<OperationResult> CREATOR; // = null;
    @DexIgnore
    public int mId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<OperationResult> {
        @DexIgnore
        public OperationResult createFromParcel(Parcel parcel) {
            return OperationResult.getValue(parcel.readInt());
        }

        @DexIgnore
        public OperationResult[] newArray(int i) {
            return new OperationResult[i];
        }
    }

    /*
    static {
        CREATOR = new Anon1();
    }
    */

    @DexIgnore
    OperationResult(int i) {
        this.mId = i;
    }

    @DexIgnore
    public static OperationResult getValue(int i) {
        for (OperationResult operationResult : values()) {
            if (operationResult.mId == i) {
                return operationResult;
            }
        }
        return STATE_UNKNOWN;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
    }
}
