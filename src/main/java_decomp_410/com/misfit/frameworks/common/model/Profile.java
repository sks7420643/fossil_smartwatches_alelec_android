package com.misfit.frameworks.common.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Profile {
    @DexIgnore
    public String accessToken;
    @DexIgnore
    public String createdAt;
    @DexIgnore
    public String expiredAt;
    @DexIgnore
    public String objectId;
    @DexIgnore
    public String refreshToken;
    @DexIgnore
    public String uid;

    @DexIgnore
    public String getAccessToken() {
        return this.accessToken;
    }

    @DexIgnore
    public String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getExpiredAt() {
        return this.expiredAt;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public String getRefreshToken() {
        return this.refreshToken;
    }

    @DexIgnore
    public String getUid() {
        return this.uid;
    }

    @DexIgnore
    public void setAccessToken(String str) {
        this.accessToken = str;
    }

    @DexIgnore
    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public void setExpiredAt(String str) {
        this.expiredAt = str;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setRefreshToken(String str) {
        this.refreshToken = str;
    }

    @DexIgnore
    public void setUid(String str) {
        this.uid = str;
    }
}
