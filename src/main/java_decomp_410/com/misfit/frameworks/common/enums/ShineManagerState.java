package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ShineManagerState {
    SCANNING,
    TRYING,
    CONNECTING,
    CONNECTED,
    DISCONNECTED,
    CLOSED,
    SYNCING
}
