package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ResultCode {
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultActivationFailed; // = 3;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultNetworkError; // = 5;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultOK; // = 0;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultOTAToButtonFWFailed; // = 2;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultPairButtonFailed; // = 6;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultSwitchModeFailed; // = 4;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultUnknown; // = 999;
    @DexIgnore
    public static /* final */ int MFLButtonSetupResultUserCancelled; // = 20;
    @DexIgnore
    public static /* final */ int nMFLButtonSetupResultNoFlashFound; // = 1;
}
