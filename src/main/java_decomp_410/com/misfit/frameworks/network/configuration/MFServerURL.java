package com.misfit.frameworks.network.configuration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFServerURL {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class MFProductionUrl {
        @DexIgnore
        public static /* final */ String CUCUMBER_BASE_URL; // = "https://cucumber.misfit.com/api/v1";
        @DexIgnore
        public static /* final */ String DEVELOPER_API_BASE_URL; // = "https://build.misfit.com";
        @DexIgnore
        public static /* final */ String DEVELOP_API_BASE_URL; // = "https://build.misfit.com";
        @DexIgnore
        public static /* final */ String HOME_BASE_URL; // = "https://home.misfit.com/shine/v8";
        @DexIgnore
        public static /* final */ String LINK_BASE_URL; // = "https://link.misfit.com/flash-button/v1";
        @DexIgnore
        public static /* final */ String SHINE_BASE_URL; // = "https://api.misfit.com/shine/v8/pedometers";
        @DexIgnore
        public static /* final */ String URL_CALLBACK_RESET_PASSWORD; // = "https://sso.misfit.com/auth/resetpassword";

        @DexIgnore
        public MFProductionUrl() {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class MFStagingUrl {
        @DexIgnore
        public static /* final */ String CUCUMBER_BASE_URL; // = "https://cucumber-int.misfit.com/api/v1";
        @DexIgnore
        public static /* final */ String DEVELOPER_API_BASE_URL; // = "https://build.int.misfit.com";
        @DexIgnore
        public static /* final */ String DEVELOP_API_BASE_URL; // = "https://build.int.misfit.com";
        @DexIgnore
        public static /* final */ String HOME_BASE_URL; // = "https://home.int.misfit.com/shine/v";
        @DexIgnore
        public static /* final */ String LINK_BASE_URL; // = "https://link.int.misfit.com/flash-button/v1";
        @DexIgnore
        public static /* final */ String SHINE_BASE_URL; // = "https://api.int.misfit.com/shine/v8/pedometers";
        @DexIgnore
        public static /* final */ String URL_CALLBACK_RESET_PASSWORD; // = "https://sso.int.misfit.com/auth/resetpassword";

        @DexIgnore
        public MFStagingUrl() {
        }
    }
}
