package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.GraphRequest;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.HTTPMethod;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFConfiguration;
import com.misfit.frameworks.network.configuration.MFHeader;
import com.misfit.frameworks.network.manager.MFNetwork;
import com.misfit.frameworks.network.responses.MFResponse;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFBaseRequest";
    @DexIgnore
    public String apiMethod;
    @DexIgnore
    public MFResponse buttonApiResponse; // = new MFResponse();
    @DexIgnore
    public MFConfiguration configuration;
    @DexIgnore
    public Context context;
    @DexIgnore
    public Exception exception;
    @DexIgnore
    public HttpURLConnection httpURLConnection;
    @DexIgnore
    public Object jsonData; // = new JSONObject();
    @DexIgnore
    public HTTPMethod method;
    @DexIgnore
    public int requestId;
    @DexIgnore
    public URL url;

    @DexIgnore
    public MFBaseRequest(Context context2) {
        this.context = context2;
    }

    @DexIgnore
    public void buildHeader(MFHeader mFHeader) {
        this.httpURLConnection.setRequestProperty(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
        this.httpURLConnection.setRequestProperty(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        this.httpURLConnection.setRequestProperty("User-Agent", MFNetwork.getInstance(this.context).getUserAgent());
        this.httpURLConnection.setRequestProperty("Installation-ID", MFNetwork.getInstance(this.context).getInstallationId());
        this.httpURLConnection.setRequestProperty("Locale", MFNetwork.getInstance(this.context).getLocale());
        if (this.method == HTTPMethod.PATCH) {
            this.httpURLConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        }
        if (mFHeader != null) {
            HashMap<String, String> headerMap = this.configuration.getHeader().getHeaderMap();
            if (headerMap != null) {
                for (String next : headerMap.keySet()) {
                    String str = headerMap.get(next);
                    if (!TextUtils.isEmpty(str)) {
                        this.httpURLConnection.setRequestProperty(next, str);
                    }
                }
            }
        }
        this.httpURLConnection.setConnectTimeout(10000);
        this.httpURLConnection.setReadTimeout(10000);
    }

    @DexIgnore
    public void buildRequest() {
        this.configuration = initConfiguration();
        this.jsonData = initJsonData();
        this.method = initHttpMethod();
        this.apiMethod = initApiMethod();
        this.buttonApiResponse = initResponse();
        Uri parse = Uri.parse(this.configuration.getBaseServerUrl() + this.apiMethod);
        try {
            if (this.method == HTTPMethod.GET) {
                if (this.jsonData != null) {
                    if (this.jsonData instanceof JSONObject) {
                        JSONObject jSONObject = (JSONObject) this.jsonData;
                        Iterator<String> keys = jSONObject.keys();
                        Uri.Builder buildUpon = parse.buildUpon();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            buildUpon.appendQueryParameter(next, String.valueOf(jSONObject.get(next)));
                        }
                        parse = buildUpon.build();
                    } else {
                        throw new Exception("We do not support JSONArray now for GET request.");
                    }
                }
                this.url = new URL(parse.toString());
                this.httpURLConnection = (HttpURLConnection) this.url.openConnection();
                this.httpURLConnection.setDoInput(true);
                this.httpURLConnection.setRequestMethod("GET");
            } else {
                if (this.method != HTTPMethod.POST) {
                    if (this.method != HTTPMethod.PATCH) {
                        if (this.method == HTTPMethod.PUT) {
                            this.url = new URL(parse.toString());
                            this.httpURLConnection = (HttpURLConnection) this.url.openConnection();
                            this.httpURLConnection.setDoOutput(true);
                            this.httpURLConnection.setRequestMethod("PUT");
                        } else if (this.method == HTTPMethod.DELETE) {
                            if (this.jsonData != null) {
                                if (this.jsonData instanceof JSONObject) {
                                    JSONObject jSONObject2 = (JSONObject) this.jsonData;
                                    Iterator<String> keys2 = jSONObject2.keys();
                                    Uri.Builder buildUpon2 = parse.buildUpon();
                                    while (keys2.hasNext()) {
                                        String next2 = keys2.next();
                                        buildUpon2.appendQueryParameter(next2, String.valueOf(jSONObject2.get(next2)));
                                    }
                                    parse = buildUpon2.build();
                                } else {
                                    throw new Exception("We do not support JSONArray now for DELETE request.");
                                }
                            }
                            this.url = new URL(parse.toString());
                            this.httpURLConnection = (HttpURLConnection) this.url.openConnection();
                            this.httpURLConnection.setDoInput(true);
                            this.httpURLConnection.setRequestMethod("DELETE");
                        }
                    }
                }
                this.url = new URL(parse.toString());
                this.httpURLConnection = (HttpURLConnection) this.url.openConnection();
                this.httpURLConnection.setDoOutput(true);
                this.httpURLConnection.setRequestMethod("POST");
            }
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "Exception when build request " + e);
        }
        buildHeader(this.configuration.getHeader());
    }

    @DexIgnore
    public MFResponse execute() {
        BufferedInputStream bufferedInputStream;
        InputStream inputStream = null;
        try {
            buildRequest();
            this.buttonApiResponse.setRequestId(this.requestId);
            if (this.httpURLConnection == null) {
                MFLogger.d(TAG, "execute - httpUriRequest == null");
                this.buttonApiResponse.setHttpReturnCode(MFNetworkReturnCode.REQUEST_NOT_FOUND);
                return this.buttonApiResponse;
            }
            if (MFNetwork.isDebug()) {
                Map requestProperties = this.httpURLConnection.getRequestProperties();
                if (requestProperties != null) {
                    for (Map.Entry entry : requestProperties.entrySet()) {
                        List list = (List) entry.getValue();
                        String str = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Header value--");
                        sb.append((String) entry.getKey());
                        sb.append(":");
                        sb.append((list == null || list.isEmpty()) ? "" : (String) list.get(0));
                        MFLogger.d(str, sb.toString());
                    }
                    String str2 = TAG;
                    MFLogger.d(str2, "Inside MFBaseRequest.doInBackground - sending REQUEST: command=, requestId=" + this.requestId + "\nurl=" + this.httpURLConnection.getURL().toString() + "\njsonData =");
                    if (this.jsonData != null) {
                        String str3 = TAG;
                        MFLogger.d(str3, "Inside MFBaseRequest.doInBackground - jsondata : " + this.jsonData.toString());
                    }
                }
            }
            this.httpURLConnection.setUseCaches(false);
            if (this.method != HTTPMethod.GET) {
                byte[] initBinaryData = initBinaryData();
                if (initBinaryData != null) {
                    DataOutputStream dataOutputStream = new DataOutputStream(this.httpURLConnection.getOutputStream());
                    dataOutputStream.write(initBinaryData);
                    dataOutputStream.flush();
                    dataOutputStream.close();
                } else if (this.jsonData != null) {
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.httpURLConnection.getOutputStream(), "UTF-8"));
                    bufferedWriter.write(this.jsonData.toString());
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } else {
                this.httpURLConnection.connect();
            }
            if (this.httpURLConnection.getResponseCode() < 200 || this.httpURLConnection.getResponseCode() >= 400) {
                bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getErrorStream());
            } else {
                bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getInputStream());
            }
            BufferedInputStream bufferedInputStream2 = bufferedInputStream;
            String readStream = readStream(bufferedInputStream2);
            String str4 = TAG;
            MFLogger.d(str4, "Inside MFBaseRequest.Worker.doInBackground requestId " + this.buttonApiResponse.getRequestId() + "- RESPONSE {httpStatus=" + this.httpURLConnection.getResponseCode() + ", jsonData=" + readStream + "}");
            this.buttonApiResponse.setHttpReturnCode(this.httpURLConnection.getResponseCode());
            if (!TextUtils.isEmpty(readStream)) {
                Object nextValue = new JSONTokener(readStream).nextValue();
                if (nextValue instanceof JSONObject) {
                    this.buttonApiResponse.parse(new JSONObject(readStream));
                } else if (nextValue instanceof JSONArray) {
                    this.buttonApiResponse.parse(new JSONArray(readStream));
                }
            }
            try {
                bufferedInputStream2.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.buttonApiResponse;
        } catch (SocketTimeoutException e2) {
            MFLogger.e(TAG, "Error inside MFBaseRequest.Worker.doInBackground - e=SocketTimeoutException");
            this.buttonApiResponse.setHttpReturnCode(601);
            this.exception = e2;
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (ConnectException e3) {
            MFLogger.e(TAG, "Error inside MFBaseRequest.Worker.doInBackground - e=ConnectException");
            this.buttonApiResponse.setHttpReturnCode(500);
            this.exception = e3;
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Exception e4) {
            String str5 = TAG;
            MFLogger.e(str5, "Error inside MFBaseRequest.Worker.doInBackground - e=" + e4);
            this.exception = e4;
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public Exception getException() {
        return this.exception;
    }

    @DexIgnore
    public abstract String initApiMethod();

    @DexIgnore
    public byte[] initBinaryData() {
        return null;
    }

    @DexIgnore
    public abstract MFConfiguration initConfiguration();

    @DexIgnore
    public abstract HTTPMethod initHttpMethod();

    @DexIgnore
    public abstract Object initJsonData();

    @DexIgnore
    public abstract MFResponse initResponse();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002c A[SYNTHETIC, Splitter:B:18:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003b A[SYNTHETIC, Splitter:B:25:0x003b] */
    public String readStream(InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream));
            while (true) {
                try {
                    String readLine = bufferedReader2.readLine();
                    if (readLine != null) {
                        sb.append(readLine);
                    } else {
                        try {
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e2) {
                    e = e2;
                    bufferedReader = bufferedReader2;
                    try {
                        e.printStackTrace();
                        if (bufferedReader != null) {
                        }
                        return sb.toString();
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedReader = bufferedReader2;
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
            }
            bufferedReader2.close();
        } catch (IOException e4) {
            e = e4;
            e.printStackTrace();
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            return sb.toString();
        }
        return sb.toString();
    }

    @DexIgnore
    public void setRequestId(int i) {
        this.requestId = i;
    }
}
