package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$timeZoneChangeReceiver$1 extends android.content.BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.ButtonService this$0;

    @DexIgnore
    public ButtonService$timeZoneChangeReceiver$1(com.misfit.frameworks.buttonservice.ButtonService buttonService) {
        this.this$0 = buttonService;
    }

    @DexIgnore
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        java.lang.String action = intent.getAction();
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "android.intent.action.TIME_SET", (java.lang.Object) action) || com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "android.intent.action.TIMEZONE_CHANGED", (java.lang.Object) action)) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Inside .timeZoneChangeReceiver");
            for (java.lang.String next : com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                com.misfit.frameworks.buttonservice.ButtonService buttonService = this.this$0;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next, "serial");
                long unused = buttonService.doUpdateTime(next);
            }
        } else if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) "android.intent.action.TIME_TICK", (java.lang.Object) action)) {
            java.util.TimeZone timeZone = java.util.TimeZone.getDefault();
            try {
                java.util.Calendar instance = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance, "calendar");
                java.util.Calendar instance2 = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                java.util.Calendar instance3 = java.util.Calendar.getInstance();
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.ButtonService.TAG, "Inside .timeZoneChangeReceiver - DST change");
                    for (java.lang.String next2 : com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils.getAllActiveButtonSerial(this.this$0)) {
                        com.misfit.frameworks.buttonservice.ButtonService buttonService2 = this.this$0;
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) next2, "serial");
                        long unused2 = buttonService2.doUpdateTime(next2);
                    }
                }
            } catch (java.lang.Exception e) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.ButtonService.TAG;
                local.mo33256e(access$getTAG$cp, ".timeZoneChangeReceiver - ex=" + e.toString());
            }
        }
    }
}
