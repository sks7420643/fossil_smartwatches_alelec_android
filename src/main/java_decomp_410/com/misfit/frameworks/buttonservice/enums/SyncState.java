package com.misfit.frameworks.buttonservice.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum SyncState {
    NORMAL,
    SYNCING,
    SYNCING_SUCCESS,
    SYNCING_WAITING_FOR_GOAL,
    SYNCING_FAILED,
    SYNCING_WAIT_FOR_RESPONSE,
    UNALLOWED
}
