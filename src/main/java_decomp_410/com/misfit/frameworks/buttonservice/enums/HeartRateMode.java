package com.misfit.frameworks.buttonservice.enums;

import com.fossil.blesdk.device.data.config.HeartRateModeConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum HeartRateMode {
    NONE(0),
    CONTINUOUS(1),
    LOW_POWER(2),
    DISABLE(3);
    
    @DexIgnore
    public static /* final */ Companion Companion; // = null;
    @DexIgnore
    public /* final */ int value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[HeartRateModeConfig.HeartRateMode.values().length];
                $EnumSwitchMapping$Anon0[HeartRateModeConfig.HeartRateMode.CONTINUOUS.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[HeartRateModeConfig.HeartRateMode.LOW_POWER.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[HeartRateModeConfig.HeartRateMode.DISABLE.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final HeartRateMode consume(HeartRateModeConfig.HeartRateMode heartRateMode) {
            kd4.b(heartRateMode, "sdkHeartRateMode");
            int i = WhenMappings.$EnumSwitchMapping$Anon0[heartRateMode.ordinal()];
            if (i == 1) {
                return HeartRateMode.CONTINUOUS;
            }
            if (i == 2) {
                return HeartRateMode.LOW_POWER;
            }
            if (i == 3) {
                return HeartRateMode.DISABLE;
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public final HeartRateMode fromValue(int i) {
            HeartRateMode heartRateMode;
            HeartRateMode[] values = HeartRateMode.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    heartRateMode = null;
                    break;
                }
                heartRateMode = values[i2];
                if (heartRateMode.getValue() == i) {
                    break;
                }
                i2++;
            }
            return heartRateMode != null ? heartRateMode : HeartRateMode.NONE;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

        /*
        static {
            $EnumSwitchMapping$Anon0 = new int[HeartRateMode.values().length];
            $EnumSwitchMapping$Anon0[HeartRateMode.CONTINUOUS.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[HeartRateMode.LOW_POWER.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[HeartRateMode.DISABLE.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[HeartRateMode.NONE.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        Companion = new Companion((fd4) null);
    }
    */

    @DexIgnore
    HeartRateMode(int i) {
        this.value = i;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public final HeartRateModeConfig.HeartRateMode toSDKHeartRateMode() {
        int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
        if (i == 1) {
            return HeartRateModeConfig.HeartRateMode.CONTINUOUS;
        }
        if (i == 2) {
            return HeartRateModeConfig.HeartRateMode.LOW_POWER;
        }
        if (i == 3) {
            return HeartRateModeConfig.HeartRateMode.DISABLE;
        }
        if (i == 4) {
            return HeartRateModeConfig.HeartRateMode.DISABLE;
        }
        throw new NoWhenBranchMatchedException();
    }
}
