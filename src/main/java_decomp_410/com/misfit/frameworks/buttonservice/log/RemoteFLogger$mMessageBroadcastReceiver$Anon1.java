package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$Anon0;

    @DexIgnore
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1(RemoteFLogger remoteFLogger) {
        this.this$Anon0 = remoteFLogger;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0089, code lost:
        if ((!com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r6.this$Anon0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00b2, code lost:
        if ((!com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) r6.this$Anon0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public void onReceive(Context context, Intent intent) {
        String str;
        kd4.b(context, "context");
        kd4.b(intent, "intent");
        String stringExtra = intent.getStringExtra("action");
        RemoteFLogger.MessageTarget fromValue = RemoteFLogger.MessageTarget.Companion.fromValue(intent.getIntExtra("target", RemoteFLogger.MessageTarget.TO_ALL_FLOGGER.getValue()));
        if (intent.hasExtra(RemoteFLogger.MESSAGE_SENDER_KEY)) {
            str = intent.getStringExtra(RemoteFLogger.MESSAGE_SENDER_KEY);
            kd4.a((Object) str, "intent.getStringExtra(MESSAGE_SENDER_KEY)");
        } else {
            str = "";
        }
        FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, "mMessageBroadcastReceiver.onReceive(), logAction=" + stringExtra + ", target=" + fromValue + ", sender=" + str);
        int i = RemoteFLogger.WhenMappings.$EnumSwitchMapping$Anon0[fromValue.ordinal()];
        boolean z = false;
        if (i != 1) {
            if (i == 2) {
                z = !kd4.a((Object) this.this$Anon0.floggerName, (Object) str);
            } else if (i != 3) {
                throw new NoWhenBranchMatchedException();
            } else if (!this.this$Anon0.isMainFLogger) {
            }
            if (!z && stringExtra != null) {
                switch (stringExtra.hashCode()) {
                    case -1960182802:
                        if (stringExtra.equals(RemoteFLogger.MESSAGE_ACTION_END_SESSION)) {
                            String stringExtra2 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            if (stringExtra2 != null) {
                                RemoteFLogger.SessionSummary sessionSummary = (RemoteFLogger.SessionSummary) this.this$Anon0.summarySessionMap.remove(stringExtra2);
                                return;
                            }
                            return;
                        }
                        return;
                    case -1885432660:
                        if (stringExtra.equals(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER)) {
                            BufferLogWriter access$getBufferLogWriter$p = this.this$Anon0.bufferLogWriter;
                            if (access$getBufferLogWriter$p != null) {
                                access$getBufferLogWriter$p.forceFlushBuffer();
                                return;
                            }
                            return;
                        }
                        return;
                    case 308052341:
                        if (stringExtra.equals(RemoteFLogger.MESSAGE_ACTION_START_SESSION)) {
                            String stringExtra3 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            String stringExtra4 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SERIAL);
                            if (stringExtra3 != null) {
                                RemoteFLogger remoteFLogger = this.this$Anon0;
                                kd4.a((Object) stringExtra4, "serial");
                                remoteFLogger.startSession(stringExtra3, stringExtra4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 1513386699:
                        if (stringExtra.equals(RemoteFLogger.MESSAGE_ACTION_ERROR_RECORDED)) {
                            String stringExtra5 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            String stringExtra6 = intent.getStringExtra(RemoteFLogger.MESSAGE_PARAM_ERROR_CODE);
                            if (stringExtra5 != null && stringExtra6 != null) {
                                RemoteFLogger.SessionSummary sessionSummary2 = (RemoteFLogger.SessionSummary) this.this$Anon0.summarySessionMap.get(stringExtra5);
                                if (sessionSummary2 != null) {
                                    sessionSummary2.getErrors().add(stringExtra6);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    case 1920521161:
                        if (stringExtra.equals(RemoteFLogger.MESSAGE_ACTION_FLUSH)) {
                            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4(this, (yb4) null), 3, (Object) null);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } else {
                return;
            }
        } else {
            if (this.this$Anon0.isMainFLogger) {
            }
            if (!z) {
                return;
            }
            return;
        }
        z = true;
        if (!z) {
        }
    }
}
