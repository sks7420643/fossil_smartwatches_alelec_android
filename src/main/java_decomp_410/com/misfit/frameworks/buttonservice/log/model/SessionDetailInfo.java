package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SessionDetailInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    @f02("battery_levels")
    public int batteryLevel;
    @DexIgnore
    @f02("daily_steps")
    public int dailySteps;
    @DexIgnore
    @f02("realtime_steps")
    public int realTimeSteps;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SessionDetailInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public SessionDetailInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new SessionDetailInfo(parcel);
        }

        @DexIgnore
        public SessionDetailInfo[] newArray(int i) {
            return new SessionDetailInfo[i];
        }
    }

    @DexIgnore
    public SessionDetailInfo(int i, int i2, int i3) {
        this.batteryLevel = i;
        this.realTimeSteps = i2;
        this.dailySteps = i3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final int getDailySteps() {
        return this.dailySteps;
    }

    @DexIgnore
    public final int getRealTimeSteps() {
        return this.realTimeSteps;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public final void setDailySteps(int i) {
        this.dailySteps = i;
    }

    @DexIgnore
    public final void setRealTimeSteps(int i) {
        this.realTimeSteps = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeInt(this.batteryLevel);
        parcel.writeInt(this.realTimeSteps);
        parcel.writeInt(this.dailySteps);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SessionDetailInfo(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
        kd4.b(parcel, "parcel");
    }
}
