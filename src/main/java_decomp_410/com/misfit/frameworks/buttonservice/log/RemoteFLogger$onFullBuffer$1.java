package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1", mo27670f = "RemoteFLogger.kt", mo27671l = {153}, mo27672m = "invokeSuspend")
public final class RemoteFLogger$onFullBuffer$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $logLines;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20858p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onFullBuffer$1(com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger, java.util.List list, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = remoteFLogger;
        this.$logLines = list;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1 remoteFLogger$onFullBuffer$1 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1(this.this$0, this.$logLines, this.$forceFlush, yb4);
        remoteFLogger$onFullBuffer$1.f20858p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return remoteFLogger$onFullBuffer$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.fossil.blesdk.obfuscated.zg4 zg4 = this.f20858p$;
            if (this.this$0.isMainFLogger) {
                com.misfit.frameworks.buttonservice.log.DBLogWriter access$getDbLogWriter$p = this.this$0.dbLogWriter;
                if (access$getDbLogWriter$p != null) {
                    access$getDbLogWriter$p.writeLog(this.$logLines);
                }
                if (this.$forceFlush) {
                    com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger = this.this$0;
                    this.L$0 = zg4;
                    this.label = 1;
                    if (remoteFLogger.flushDB(this) == a) {
                        return a;
                    }
                }
            } else if (!this.$forceFlush) {
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.log.RemoteFLogger.TAG, ".onFullBuffer(), broadcast");
                com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger2 = this.this$0;
                java.lang.String access$getFloggerName$p = remoteFLogger2.floggerName;
                com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget messageTarget = com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                android.os.Bundle bundle = android.os.Bundle.EMPTY;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bundle, "Bundle.EMPTY");
                remoteFLogger2.sendInternalMessage(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER, access$getFloggerName$p, messageTarget, bundle);
            } else {
                com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger3 = this.this$0;
                java.lang.String access$getFloggerName$p2 = remoteFLogger3.floggerName;
                com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget messageTarget2 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                android.os.Bundle bundle2 = android.os.Bundle.EMPTY;
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bundle2, "Bundle.EMPTY");
                remoteFLogger3.sendInternalMessage(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_FLUSH, access$getFloggerName$p2, messageTarget2, bundle2);
            }
        } else if (i == 1) {
            com.fossil.blesdk.obfuscated.zg4 zg42 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }
}
