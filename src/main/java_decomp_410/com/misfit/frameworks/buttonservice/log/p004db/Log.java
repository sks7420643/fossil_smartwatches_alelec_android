package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.Log */
public final class Log {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.log.p004db.Log.Flag cloudFlag;
    @DexIgnore
    public java.lang.String content;

    @DexIgnore
    /* renamed from: id */
    public int f20861id;
    @DexIgnore
    public long timeStamp;

    @DexIgnore
    /* renamed from: com.misfit.frameworks.buttonservice.log.db.Log$Flag */
    public enum Flag {
        ADD("ADD"),
        SYNCING("SYNCING");
        
        @DexIgnore
        public static /* final */ com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.Companion Companion; // = null;
        @DexIgnore
        public /* final */ java.lang.String value;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.log.db.Log$Flag$Companion")
        /* renamed from: com.misfit.frameworks.buttonservice.log.db.Log$Flag$Companion */
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final com.misfit.frameworks.buttonservice.log.p004db.Log.Flag fromValue(java.lang.String str) {
                com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag;
                com.fossil.blesdk.obfuscated.kd4.m24411b(str, "value");
                com.misfit.frameworks.buttonservice.log.p004db.Log.Flag[] values = com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        flag = null;
                        break;
                    }
                    flag = values[i];
                    if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) flag.getValue(), (java.lang.Object) str)) {
                        break;
                    }
                    i++;
                }
                return flag != null ? flag : com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.ADD;
            }

            @DexIgnore
            public /* synthetic */ Companion(com.fossil.blesdk.obfuscated.fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new com.misfit.frameworks.buttonservice.log.p004db.Log.Flag.Companion((com.fossil.blesdk.obfuscated.fd4) null);
        }
        */

        @DexIgnore
        public Flag(java.lang.String str) {
            this.value = str;
        }

        @DexIgnore
        public final java.lang.String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public Log(long j, java.lang.String str, com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "content");
        com.fossil.blesdk.obfuscated.kd4.m24411b(flag, "cloudFlag");
        this.timeStamp = j;
        this.content = str;
        this.cloudFlag = flag;
    }

    @DexIgnore
    public static /* synthetic */ com.misfit.frameworks.buttonservice.log.p004db.Log copy$default(com.misfit.frameworks.buttonservice.log.p004db.Log log, long j, java.lang.String str, com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag, int i, java.lang.Object obj) {
        if ((i & 1) != 0) {
            j = log.timeStamp;
        }
        if ((i & 2) != 0) {
            str = log.content;
        }
        if ((i & 4) != 0) {
            flag = log.cloudFlag;
        }
        return log.copy(j, str, flag);
    }

    @DexIgnore
    public final long component1() {
        return this.timeStamp;
    }

    @DexIgnore
    public final java.lang.String component2() {
        return this.content;
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.p004db.Log.Flag component3() {
        return this.cloudFlag;
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.p004db.Log copy(long j, java.lang.String str, com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "content");
        com.fossil.blesdk.obfuscated.kd4.m24411b(flag, "cloudFlag");
        return new com.misfit.frameworks.buttonservice.log.p004db.Log(j, str, flag);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this != obj) {
            if (obj instanceof com.misfit.frameworks.buttonservice.log.p004db.Log) {
                com.misfit.frameworks.buttonservice.log.p004db.Log log = (com.misfit.frameworks.buttonservice.log.p004db.Log) obj;
                if (!(this.timeStamp == log.timeStamp) || !com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.content, (java.lang.Object) log.content) || !com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.cloudFlag, (java.lang.Object) log.cloudFlag)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final com.misfit.frameworks.buttonservice.log.p004db.Log.Flag getCloudFlag() {
        return this.cloudFlag;
    }

    @DexIgnore
    public final java.lang.String getContent() {
        return this.content;
    }

    @DexIgnore
    public final int getId() {
        return this.f20861id;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.timeStamp;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        java.lang.String str = this.content;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag = this.cloudFlag;
        if (flag != null) {
            i2 = flag.hashCode();
        }
        return hashCode + i2;
    }

    @DexIgnore
    public final void setCloudFlag(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(flag, "<set-?>");
        this.cloudFlag = flag;
    }

    @DexIgnore
    public final void setContent(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "<set-?>");
        this.content = str;
    }

    @DexIgnore
    public final void setId(int i) {
        this.f20861id = i;
    }

    @DexIgnore
    public final void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Log(timeStamp=" + this.timeStamp + ", content=" + this.content + ", cloudFlag=" + this.cloudFlag + ")";
    }
}
