package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$2 */
public final class C5567x76071dd1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.zg4 $this_launch$inlined;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20845p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C5567x76071dd1(com.fossil.blesdk.obfuscated.yb4 yb4, com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1 bufferLogWriter$renameToFullBufferFile$1, com.fossil.blesdk.obfuscated.zg4 zg4) {
        super(2, yb4);
        this.this$0 = bufferLogWriter$renameToFullBufferFile$1;
        this.$this_launch$inlined = zg4;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.C5567x76071dd1 bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$2 = new com.misfit.frameworks.buttonservice.log.C5567x76071dd1(yb4, this.this$0, this.$this_launch$inlined);
        bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$2.f20845p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bufferLogWriter$renameToFullBufferFile$1$invokeSuspend$$inlined$withLock$lambda$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.C5567x76071dd1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback callback = this.this$0.this$0.getCallback();
            if (callback == null) {
                return null;
            }
            callback.onFullBuffer(new java.util.ArrayList(), this.this$0.$forceFlush);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
