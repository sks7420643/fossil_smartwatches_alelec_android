package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.DBConstants */
public final class DBConstants {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.p004db.DBConstants INSTANCE; // = new com.misfit.frameworks.buttonservice.log.p004db.DBConstants();
    @DexIgnore
    public static /* final */ java.lang.String LOG_DB_NAME; // = "log_db.db";
    @DexIgnore
    public static /* final */ int LOG_DB_VERSION; // = 1;
}
