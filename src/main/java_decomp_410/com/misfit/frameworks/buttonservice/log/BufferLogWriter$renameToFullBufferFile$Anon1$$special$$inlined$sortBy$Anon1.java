package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.hf4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wb4;
import java.io.File;
import java.util.Comparator;
import kotlin.text.Regex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$Anon1$$special$$inlined$sortBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        File file = (File) t;
        Regex regex = new Regex("\\d+");
        kd4.a((Object) file, "it");
        String name = file.getName();
        kd4.a((Object) name, "it.name");
        String str = null;
        hf4 find$default = Regex.find$default(regex, name, 0, 2, (Object) null);
        String value = find$default != null ? find$default.getValue() : null;
        File file2 = (File) t2;
        Regex regex2 = new Regex("\\d+");
        kd4.a((Object) file2, "it");
        String name2 = file2.getName();
        kd4.a((Object) name2, "it.name");
        hf4 find$default2 = Regex.find$default(regex2, name2, 0, 2, (Object) null);
        if (find$default2 != null) {
            str = find$default2.getValue();
        }
        return wb4.a(value, str);
    }
}
