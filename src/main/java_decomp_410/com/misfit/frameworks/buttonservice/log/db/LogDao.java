package com.misfit.frameworks.buttonservice.log.db;

import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface LogDao {
    @DexIgnore
    int countExcept(Log.Flag flag);

    @DexIgnore
    int delete(List<Integer> list);

    @DexIgnore
    List<Log> getAllLogEventsExcept(Log.Flag flag);

    @DexIgnore
    void insertLogEvent(List<Log> list);

    @DexIgnore
    void updateCloudFlagByIds(List<Integer> list, Log.Flag flag);
}
