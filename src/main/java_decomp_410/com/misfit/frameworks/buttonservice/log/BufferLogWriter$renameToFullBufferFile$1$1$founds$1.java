package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$1$1$founds$1 implements java.io.FileFilter {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$1$founds$1 INSTANCE; // = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$1$founds$1();

    @DexIgnore
    public final boolean accept(java.io.File file) {
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) file, "subFile");
        if (file.isFile()) {
            java.lang.String name = file.getName();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) name, "subFile.name");
            if (new kotlin.text.Regex(com.misfit.frameworks.buttonservice.log.BufferLogWriter.FULL_BUFFER_FILE_NAME_REGEX_PATTERN).matches(name)) {
                return true;
            }
        }
        return false;
    }
}
