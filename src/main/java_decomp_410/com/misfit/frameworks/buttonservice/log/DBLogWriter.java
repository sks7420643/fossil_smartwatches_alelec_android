package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.fl4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rf;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yf4;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.misfit.frameworks.buttonservice.log.db.Log;
import com.misfit.frameworks.buttonservice.log.db.LogDao;
import com.misfit.frameworks.buttonservice.log.db.LogDatabase;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DBLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ int LIMIT_INPUT_IDS_PARAM; // = 500;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ IDBLogWriterCallback callback;
    @DexIgnore
    public /* final */ LogDao logDao;
    @DexIgnore
    public /* final */ dl4 mMutex; // = fl4.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ int thresholdValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface IDBLogWriterCallback {
        @DexIgnore
        void onReachDBThreshold();
    }

    /*
    static {
        String simpleName = DBLogWriter.class.getSimpleName();
        kd4.a((Object) simpleName, "DBLogWriter::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DBLogWriter(Context context, int i, IDBLogWriterCallback iDBLogWriterCallback) {
        kd4.b(context, "context");
        this.thresholdValue = i;
        this.callback = iDBLogWriterCallback;
        RoomDatabase.a<LogDatabase> a = rf.a(context, LogDatabase.class, DBConstants.LOG_DB_NAME);
        a.d();
        this.logDao = a.b().getLogDao();
    }

    @DexIgnore
    public final /* synthetic */ Object deleteLogs(List<Integer> list, yb4<? super Integer> yb4) {
        if (!list.isEmpty()) {
            return yf4.a(nh4.a(), new DBLogWriter$deleteLogs$Anon2(this, list, (yb4) null), yb4);
        }
        return dc4.a(0);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r0.L$Anon0 = r5;
        r0.L$Anon1 = r14;
        r0.L$Anon2 = r15;
        r0.label = 2;
        r2 = r5.getAllLogsExceptSyncing(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fa, code lost:
        if (r2 != r1) goto L_0x00fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fc, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fd, code lost:
        r6 = r5;
        r5 = r14;
        r14 = r15;
        r15 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r2 = (java.util.List) r15;
        r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r7 = TAG;
        r15.d(r7, ".flush(), logEventSize=" + r2.size());
        r15 = kotlin.sequences.SequencesKt___SequencesKt.g(kotlin.sequences.SequencesKt___SequencesKt.d(com.fossil.blesdk.obfuscated.kb4.b(r2), com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1.INSTANCE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0137, code lost:
        if ((true ^ r15.isEmpty()) == false) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0139, code lost:
        r3 = com.misfit.frameworks.buttonservice.log.db.Log.Flag.SYNCING;
        r0.L$Anon0 = r6;
        r0.L$Anon1 = r5;
        r0.L$Anon2 = r14;
        r0.L$Anon3 = r2;
        r0.L$Anon4 = r15;
        r0.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x014c, code lost:
        if (r6.updateCloudFlag(r15, r3, r0) != r1) goto L_0x014f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x014e, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x014f, code lost:
        r0.L$Anon0 = r6;
        r0.L$Anon1 = r5;
        r0.L$Anon2 = r14;
        r0.L$Anon3 = r2;
        r0.L$Anon4 = r15;
        r0.label = 4;
        r3 = r5.sendLog(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0160, code lost:
        if (r3 != r1) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0162, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0163, code lost:
        r7 = r5;
        r8 = r6;
        r5 = r2;
        r12 = r3;
        r3 = r15;
        r15 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0169, code lost:
        r2 = (java.util.List) r15;
        r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r6 = TAG;
        r15.d(r6, ".flush(), sentLog=" + r2.size());
        r15 = kotlin.sequences.SequencesKt___SequencesKt.g(kotlin.sequences.SequencesKt___SequencesKt.d(com.fossil.blesdk.obfuscated.kb4.b(r2), com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$sentLogIds$Anon1.INSTANCE));
        r0.L$Anon0 = r8;
        r0.L$Anon1 = r7;
        r0.L$Anon2 = r14;
        r0.L$Anon3 = r5;
        r0.L$Anon4 = r3;
        r0.L$Anon5 = r2;
        r0.L$Anon6 = r15;
        r0.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01af, code lost:
        if (r8.deleteLogs(r15, r0) != r1) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01b1, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01b2, code lost:
        r6 = r14;
        r14 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        r3.removeAll(r14);
        r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r9 = TAG;
        r15.d(r9, ".flush(), syncingLogs=" + r3.size());
        r15 = com.misfit.frameworks.buttonservice.log.db.Log.Flag.ADD;
        r0.L$Anon0 = r8;
        r0.L$Anon1 = r7;
        r0.L$Anon2 = r6;
        r0.L$Anon3 = r5;
        r0.L$Anon4 = r3;
        r0.L$Anon5 = r2;
        r0.L$Anon6 = r14;
        r0.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01ee, code lost:
        if (r8.updateCloudFlag(r3, r15, r0) != r1) goto L_0x01f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01f0, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01f1, code lost:
        r14 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r15 = com.fossil.blesdk.obfuscated.qa4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01f4, code lost:
        r14.a((java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01f9, code lost:
        return com.fossil.blesdk.obfuscated.qa4.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01fa, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01fb, code lost:
        r12 = r15;
        r15 = r14;
        r14 = r12;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object flushTo(IRemoteLogWriter iRemoteLogWriter, yb4<? super qa4> yb4) {
        DBLogWriter$flushTo$Anon1 dBLogWriter$flushTo$Anon1;
        Throwable th;
        dl4 dl4;
        DBLogWriter dBLogWriter;
        IRemoteLogWriter iRemoteLogWriter2;
        List list;
        List list2;
        DBLogWriter dBLogWriter2;
        IRemoteLogWriter iRemoteLogWriter3;
        dl4 dl42;
        DBLogWriter dBLogWriter3;
        dl4 dl43;
        if (yb4 instanceof DBLogWriter$flushTo$Anon1) {
            dBLogWriter$flushTo$Anon1 = (DBLogWriter$flushTo$Anon1) yb4;
            int i = dBLogWriter$flushTo$Anon1.label;
            if ((i & Integer.MIN_VALUE) != 0) {
                dBLogWriter$flushTo$Anon1.label = i - Integer.MIN_VALUE;
                Object obj = dBLogWriter$flushTo$Anon1.result;
                Object a = cc4.a();
                switch (dBLogWriter$flushTo$Anon1.label) {
                    case 0:
                        na4.a(obj);
                        dl42 = this.mMutex;
                        dBLogWriter$flushTo$Anon1.L$Anon0 = this;
                        dBLogWriter$flushTo$Anon1.L$Anon1 = iRemoteLogWriter;
                        dBLogWriter$flushTo$Anon1.L$Anon2 = dl42;
                        dBLogWriter$flushTo$Anon1.label = 1;
                        if (dl42.a((Object) null, dBLogWriter$flushTo$Anon1) != a) {
                            dBLogWriter3 = this;
                            break;
                        } else {
                            return a;
                        }
                    case 1:
                        dBLogWriter3 = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        na4.a(obj);
                        dl42 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        iRemoteLogWriter = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        break;
                    case 2:
                        dl4 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        IRemoteLogWriter iRemoteLogWriter4 = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        DBLogWriter dBLogWriter4 = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        na4.a(obj);
                        dBLogWriter2 = dBLogWriter4;
                        iRemoteLogWriter3 = iRemoteLogWriter4;
                    case 3:
                        List list3 = (List) dBLogWriter$flushTo$Anon1.L$Anon4;
                        List list4 = (List) dBLogWriter$flushTo$Anon1.L$Anon3;
                        dl43 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        iRemoteLogWriter3 = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        dBLogWriter2 = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        na4.a(obj);
                        List list5 = list3;
                        dl4 = dl43;
                        break;
                    case 4:
                        List list6 = (List) dBLogWriter$flushTo$Anon1.L$Anon4;
                        List list7 = (List) dBLogWriter$flushTo$Anon1.L$Anon3;
                        dl43 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        IRemoteLogWriter iRemoteLogWriter5 = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        DBLogWriter dBLogWriter5 = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        try {
                            na4.a(obj);
                            iRemoteLogWriter2 = iRemoteLogWriter5;
                            dBLogWriter = dBLogWriter5;
                            list = list7;
                            dl4 dl44 = dl43;
                            list2 = list6;
                            dl4 = dl44;
                            break;
                        } catch (Throwable th2) {
                            th = th2;
                            dl4 = dl43;
                            dl4.a((Object) null);
                            throw th;
                        }
                    case 5:
                        List list8 = (List) dBLogWriter$flushTo$Anon1.L$Anon6;
                        List list9 = (List) dBLogWriter$flushTo$Anon1.L$Anon5;
                        list2 = (List) dBLogWriter$flushTo$Anon1.L$Anon4;
                        list = (List) dBLogWriter$flushTo$Anon1.L$Anon3;
                        dl4 dl45 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        iRemoteLogWriter2 = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        dBLogWriter = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        try {
                            na4.a(obj);
                        } catch (Throwable th3) {
                            th = th3;
                            dl4 = dl45;
                            break;
                        }
                    case 6:
                        List list10 = (List) dBLogWriter$flushTo$Anon1.L$Anon6;
                        List list11 = (List) dBLogWriter$flushTo$Anon1.L$Anon5;
                        List list12 = (List) dBLogWriter$flushTo$Anon1.L$Anon4;
                        List list13 = (List) dBLogWriter$flushTo$Anon1.L$Anon3;
                        dl4 = (dl4) dBLogWriter$flushTo$Anon1.L$Anon2;
                        IRemoteLogWriter iRemoteLogWriter6 = (IRemoteLogWriter) dBLogWriter$flushTo$Anon1.L$Anon1;
                        DBLogWriter dBLogWriter6 = (DBLogWriter) dBLogWriter$flushTo$Anon1.L$Anon0;
                        try {
                            na4.a(obj);
                        } catch (Throwable th4) {
                            th = th4;
                            break;
                        }
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }
        dBLogWriter$flushTo$Anon1 = new DBLogWriter$flushTo$Anon1(this, yb4);
        Object obj2 = dBLogWriter$flushTo$Anon1.result;
        Object a2 = cc4.a();
        switch (dBLogWriter$flushTo$Anon1.label) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object getAllLogsExceptSyncing(yb4<? super List<LogEvent>> yb4) {
        return yf4.a(nh4.a(), new DBLogWriter$getAllLogsExceptSyncing$Anon2(this, (yb4) null), yb4);
    }

    @DexIgnore
    public final /* synthetic */ Object updateCloudFlag(List<Integer> list, Log.Flag flag, yb4<? super qa4> yb4) {
        return yf4.a(nh4.a(), new DBLogWriter$updateCloudFlag$Anon2(this, list, flag, (yb4) null), yb4);
    }

    @DexIgnore
    public final synchronized void writeLog(List<LogEvent> list) {
        kd4.b(list, "logEvents");
        this.logDao.insertLogEvent(SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.c(kb4.b(list), DBLogWriter$writeLog$Anon1.INSTANCE)));
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new DBLogWriter$writeLog$Anon2(this, (yb4) null), 3, (Object) null);
    }
}
