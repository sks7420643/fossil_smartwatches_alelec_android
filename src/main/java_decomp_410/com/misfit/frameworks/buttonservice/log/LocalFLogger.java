package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import android.util.Log;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalFLogger implements ILocalFLogger {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String LOG_PATTERN; // = "%s %s: %s\n";
    @DexIgnore
    public FileLogWriter fileLogWriter;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public String prefix; // = "";
    @DexIgnore
    public /* final */ SimpleDateFormat sdf; // = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS", Locale.US);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    private final boolean getCanWriteLog() {
        return this.isInitialized && this.isDebuggable;
    }

    @DexIgnore
    public void d(String str, String str2) {
        kd4.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.d(this.prefix + " - " + str, str2);
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /D", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    public void e(String str, String str2) {
        kd4.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.e(this.prefix + " - " + str, str2);
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /E", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    public List<File> exportAppLogs() {
        FileLogWriter fileLogWriter2 = this.fileLogWriter;
        if (fileLogWriter2 != null) {
            List<File> exportLogs = fileLogWriter2.exportLogs();
            if (exportLogs != null) {
                return exportLogs;
            }
        }
        return new ArrayList();
    }

    @DexIgnore
    public void i(String str, String str2) {
        kd4.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.i(this.prefix + " - " + str, str2);
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /I", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }

    @DexIgnore
    public void init(Context context, String str, boolean z) {
        kd4.b(context, "context");
        kd4.b(str, "prefix");
        this.prefix = str;
        this.fileLogWriter = new FileLogWriter();
        FileLogWriter fileLogWriter2 = this.fileLogWriter;
        if (fileLogWriter2 != null) {
            String file = context.getFilesDir().toString();
            kd4.a((Object) file, "context.filesDir.toString()");
            fileLogWriter2.startWriter(file);
        }
        this.isDebuggable = z;
        this.isInitialized = true;
    }

    @DexIgnore
    public void v(String str, String str2) {
        kd4.b(str, "tag");
        if (getCanWriteLog() && str2 != null) {
            Log.v(this.prefix + " - " + str, str2);
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            Object[] objArr = {this.sdf.format(new Date()), this.prefix + " - " + str + " /V", str2};
            String format = String.format(locale, "%s %s: %s\n", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            FileLogWriter fileLogWriter2 = this.fileLogWriter;
            if (fileLogWriter2 != null) {
                fileLogWriter2.writeLog(format);
            }
        }
    }
}
