package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.f02;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.yb4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.OtaDetailLog;
import com.misfit.frameworks.buttonservice.log.model.RemoveDeviceLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.PinObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemoteFLogger implements IRemoteFLogger, BufferLogWriter.IBufferLogCallback, DBLogWriter.IDBLogWriterCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String LOG_MESSAGE_INTENT_ACTION; // = "fossil.log.action.messages";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_END_SESSION; // = "action:end_session";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_ERROR_RECORDED; // = "action:error_recorded";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FLUSH; // = "action:flush_data";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FULL_BUFFER; // = "action:full_buffer";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_KEY; // = "action";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_START_SESSION; // = "action:start_session";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_ERROR_CODE; // = "param:error";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SERIAL; // = "param:serial";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SUMMARY_KEY; // = "param:summary_key";
    @DexIgnore
    public static /* final */ String MESSAGE_SENDER_KEY; // = "sender";
    @DexIgnore
    public static /* final */ String MESSAGE_TARGET_KEY; // = "target";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveDeviceInfo activeDeviceInfo; // = new ActiveDeviceInfo("", "", "");
    @DexIgnore
    public AppLogInfo appLogInfo; // = new AppLogInfo("", "", "", "", "", "", "");
    @DexIgnore
    public BufferLogWriter bufferLogWriter;
    @DexIgnore
    public Context context;
    @DexIgnore
    public DBLogWriter dbLogWriter;
    @DexIgnore
    public String floggerName; // = "";
    @DexIgnore
    public int flushLogTimeThreshold; // = LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public volatile boolean isFlushing;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public boolean isPrintToConsole; // = true;
    @DexIgnore
    public long lastSyncTime;
    @DexIgnore
    public /* final */ BroadcastReceiver mMessageBroadcastReceiver; // = new RemoteFLogger$mMessageBroadcastReceiver$Anon1(this);
    @DexIgnore
    public IRemoteLogWriter remoteLogWriter;
    @DexIgnore
    public SessionDetailInfo sessionDetailInfo; // = new SessionDetailInfo(-1, 0, 0);
    @DexIgnore
    public /* final */ HashMap<String, SessionSummary> summarySessionMap; // = new HashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ErrorEvent {
        @DexIgnore
        public /* final */ FLogger.Component component;
        @DexIgnore
        public /* final */ String error;
        @DexIgnore
        public /* final */ String errorCode;
        @DexIgnore
        public /* final */ String step;

        @DexIgnore
        public ErrorEvent(String str, String str2, FLogger.Component component2, String str3) {
            kd4.b(str, "errorCode");
            kd4.b(str2, "step");
            kd4.b(component2, "component");
            kd4.b(str3, "error");
            this.errorCode = str;
            this.step = str2;
            this.component = component2;
            this.error = str3;
        }

        @DexIgnore
        public final FLogger.Component getComponent() {
            return this.component;
        }

        @DexIgnore
        public final String getError() {
            return this.error;
        }

        @DexIgnore
        public final String getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getStep() {
            return this.step;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = new Gson().a((Object) this);
            kd4.a((Object) a, "Gson().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }
    }

    @DexIgnore
    public enum MessageTarget {
        TO_ALL_FLOGGER(1),
        TO_MAIN_FLOGGER(2),
        TO_ALL_EXCEPT_MAIN_FLOGGER(3);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ int value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MessageTarget fromValue(int i) {
                MessageTarget messageTarget;
                MessageTarget[] values = MessageTarget.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        messageTarget = null;
                        break;
                    }
                    messageTarget = values[i2];
                    if (messageTarget.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return messageTarget != null ? messageTarget : MessageTarget.TO_ALL_FLOGGER;
            }

            @DexIgnore
            public /* synthetic */ Companion(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((fd4) null);
        }
        */

        @DexIgnore
        MessageTarget(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SessionSummary {
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion((fd4) null);
        @DexIgnore
        @f02("details")
        public Object details;
        @DexIgnore
        @f02("end_timestamp")
        public long endTimeStamp;
        @DexIgnore
        @f02("errors")
        public /* final */ List<String> errors; // = new ArrayList();
        @DexIgnore
        @f02("failure_reason")
        public String failureReason; // = "";
        @DexIgnore
        @f02("is_success")
        public boolean isSuccess; // = true;
        @DexIgnore
        @f02("start_timestamp")
        public long startTimeStamp;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final String getSummaryKey(String str, FLogger.Session session) {
                kd4.b(str, "serial");
                kd4.b(session, Constants.SESSION);
                return str + ':' + session;
            }

            @DexIgnore
            public /* synthetic */ Companion(fd4 fd4) {
                this();
            }
        }

        @DexIgnore
        public SessionSummary(long j, long j2) {
            this.startTimeStamp = j;
            this.endTimeStamp = j2;
        }

        @DexIgnore
        public final Object getDetails() {
            return this.details;
        }

        @DexIgnore
        public final long getEndTimeStamp() {
            return this.endTimeStamp;
        }

        @DexIgnore
        public final List<String> getErrors() {
            return this.errors;
        }

        @DexIgnore
        public final String getFailureReason() {
            return this.failureReason;
        }

        @DexIgnore
        public final long getStartTimeStamp() {
            return this.startTimeStamp;
        }

        @DexIgnore
        public final boolean isSuccess() {
            return this.isSuccess;
        }

        @DexIgnore
        public final void setDetails(Object obj) {
            this.details = obj;
        }

        @DexIgnore
        public final void setEndTimeStamp(long j) {
            this.endTimeStamp = j;
        }

        @DexIgnore
        public final void setStartTimeStamp(long j) {
            this.startTimeStamp = j;
        }

        @DexIgnore
        public final String toJsonString() {
            String a = FLogUtils.INSTANCE.getGsonForLogEvent().a((Object) this);
            kd4.a((Object) a, "FLogUtils.getGsonForLogEvent().toJson(this)");
            return a;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }

        @DexIgnore
        public final void update(int i) {
            this.isSuccess = i == 0;
            this.failureReason = true ^ this.errors.isEmpty() ? (String) kb4.f(this.errors) : "";
            if (!this.isSuccess && this.errors.isEmpty()) {
                this.errors.add(String.valueOf(i));
                this.failureReason = (String) kb4.f(this.errors);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = RemoteFLogger.TAG;
            local.d(access$getTAG$cp, "failureReason " + this.failureReason + " errors " + this.errors + " finalCode " + i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[MessageTarget.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon1; // = new int[FLogger.LogLevel.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[MessageTarget.TO_MAIN_FLOGGER.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[MessageTarget.TO_ALL_FLOGGER.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER.ordinal()] = 3;
            $EnumSwitchMapping$Anon1[FLogger.LogLevel.DEBUG.ordinal()] = 1;
            $EnumSwitchMapping$Anon1[FLogger.LogLevel.INFO.ordinal()] = 2;
            $EnumSwitchMapping$Anon1[FLogger.LogLevel.ERROR.ordinal()] = 3;
            $EnumSwitchMapping$Anon1[FLogger.LogLevel.SUMMARY.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        String name = RemoteFLogger.class.getName();
        kd4.a((Object) name, "RemoteFLogger::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getInternalIntentAction() {
        Context context2 = this.context;
        return kd4.a(context2 != null ? context2.getPackageName() : null, (Object) LOG_MESSAGE_INTENT_ACTION);
    }

    @DexIgnore
    private final Object getSessionDetails(String str, String str2) {
        if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) FLogger.Session.SYNC.name(), false, 2, (Object) null)) {
            return this.sessionDetailInfo;
        }
        if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) FLogger.Session.OTA.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        if (StringsKt__StringsKt.a((CharSequence) str, (CharSequence) FLogger.Session.PAIR.name(), false, 2, (Object) null)) {
            return new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel());
        }
        return StringsKt__StringsKt.a((CharSequence) str, (CharSequence) FLogger.Session.REMOVE_DEVICE.name(), false, 2, (Object) null) ? new RemoveDeviceLog(str2) : "";
    }

    @DexIgnore
    private final void registerBroadcastMessages(Context context2) {
        context2.registerReceiver(this.mMessageBroadcastReceiver, new IntentFilter(getInternalIntentAction()));
    }

    @DexIgnore
    private final void sendInternalMessage(String str, String str2, MessageTarget messageTarget, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(getInternalIntentAction());
        intent.putExtra(MESSAGE_SENDER_KEY, str2);
        intent.putExtra("action", str);
        intent.putExtra("target", messageTarget.getValue());
        intent.putExtras(bundle);
        Context context2 = this.context;
        if (context2 != null) {
            context2.sendBroadcast(intent);
        }
    }

    @DexIgnore
    public void d(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        kd4.b(str3, "message");
        if (this.isInitialized && this.isDebuggable) {
            log(FLogger.LogLevel.DEBUG, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3, ErrorCodeBuilder.Step step, String str4) {
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        kd4.b(str3, "errorCode");
        kd4.b(step, "step");
        kd4.b(str4, "errorMessage");
        if (this.isInitialized) {
            if (log(FLogger.LogLevel.ERROR, component, session, str, str2, new ErrorEvent(str3, step.getNameValue(), component, str4).toJsonString())) {
                String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
                SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
                if (sessionSummary != null) {
                    List<String> errors = sessionSummary.getErrors();
                    if (errors != null) {
                        errors.add(str3);
                    }
                }
                String str5 = this.floggerName;
                MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
                Bundle bundle = new Bundle();
                bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
                bundle.putString(MESSAGE_PARAM_ERROR_CODE, str3);
                sendInternalMessage(MESSAGE_ACTION_ERROR_RECORDED, str5, messageTarget, bundle);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
        if (r1 != null) goto L_0x0015;
     */
    @DexIgnore
    public List<File> exportAppLogs() {
        Collection collection;
        ArrayList arrayList = new ArrayList();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            collection = bufferLogWriter2.exportLogs();
        }
        collection = new ArrayList();
        arrayList.addAll(collection);
        return arrayList;
    }

    @DexIgnore
    public void flush() {
        fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$flush$Anon1(this, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ Object flushBuffer(yb4<? super qa4> yb4) {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        this.lastSyncTime = instance.getTimeInMillis();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.forceFlushBuffer();
        }
        return qa4.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object flushDB(yb4<? super qa4> yb4) {
        RemoteFLogger$flushDB$Anon1 remoteFLogger$flushDB$Anon1;
        int i;
        if (yb4 instanceof RemoteFLogger$flushDB$Anon1) {
            remoteFLogger$flushDB$Anon1 = (RemoteFLogger$flushDB$Anon1) yb4;
            int i2 = remoteFLogger$flushDB$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                remoteFLogger$flushDB$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = remoteFLogger$flushDB$Anon1.result;
                Object a = cc4.a();
                i = remoteFLogger$flushDB$Anon1.label;
                if (i != 0) {
                    na4.a(obj);
                    if (this.isMainFLogger) {
                        IRemoteLogWriter iRemoteLogWriter = this.remoteLogWriter;
                        if (iRemoteLogWriter != null) {
                            DBLogWriter dBLogWriter = this.dbLogWriter;
                            if (dBLogWriter != null) {
                                remoteFLogger$flushDB$Anon1.L$Anon0 = this;
                                remoteFLogger$flushDB$Anon1.L$Anon1 = iRemoteLogWriter;
                                remoteFLogger$flushDB$Anon1.label = 1;
                                obj = dBLogWriter.flushTo(iRemoteLogWriter, remoteFLogger$flushDB$Anon1);
                                if (obj == a) {
                                    return a;
                                }
                            }
                        }
                    }
                    return qa4.a;
                } else if (i == 1) {
                    IRemoteLogWriter iRemoteLogWriter2 = (IRemoteLogWriter) remoteFLogger$flushDB$Anon1.L$Anon1;
                    RemoteFLogger remoteFLogger = (RemoteFLogger) remoteFLogger$flushDB$Anon1.L$Anon0;
                    na4.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                qa4 qa4 = (qa4) obj;
                return qa4.a;
            }
        }
        remoteFLogger$flushDB$Anon1 = new RemoteFLogger$flushDB$Anon1(this, yb4);
        Object obj2 = remoteFLogger$flushDB$Anon1.result;
        Object a2 = cc4.a();
        i = remoteFLogger$flushDB$Anon1.label;
        if (i != 0) {
        }
        qa4 qa42 = (qa4) obj2;
        return qa4.a;
    }

    @DexIgnore
    public void i(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        kd4.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.INFO, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    public void init(String str, AppLogInfo appLogInfo2, ActiveDeviceInfo activeDeviceInfo2, CloudLogConfig cloudLogConfig, Context context2, boolean z, boolean z2) {
        kd4.b(str, "name");
        kd4.b(appLogInfo2, "appLogInfo");
        kd4.b(activeDeviceInfo2, "activeDeviceInfo");
        kd4.b(cloudLogConfig, "cloudLogConfig");
        kd4.b(context2, "context");
        int i = z2 ? 10 : 100;
        int i2 = z2 ? 50 : 500;
        this.flushLogTimeThreshold = z2 ? 1800000 : LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
        this.floggerName = str;
        this.appLogInfo = appLogInfo2;
        this.activeDeviceInfo = activeDeviceInfo2;
        this.context = context2;
        this.bufferLogWriter = new BufferLogWriter(str, i);
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.setCallback(this);
        }
        BufferLogWriter bufferLogWriter3 = this.bufferLogWriter;
        if (bufferLogWriter3 != null) {
            String file = context2.getFilesDir().toString();
            kd4.a((Object) file, "context.filesDir.toString()");
            bufferLogWriter3.startWriter(file, z, this);
        }
        this.dbLogWriter = new DBLogWriter(context2, i2, this);
        updateCloudLogConfig(cloudLogConfig);
        registerBroadcastMessages(context2);
        this.isMainFLogger = z;
        this.isDebuggable = z2;
        this.isInitialized = true;
    }

    @DexIgnore
    public final boolean log(FLogger.LogLevel logLevel, FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        String str4;
        String str5;
        String str6;
        String str7 = str;
        String str8 = str2;
        String str9 = str3;
        kd4.b(logLevel, "logLevel");
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str7, "serial");
        kd4.b(str8, PinObject.COLUMN_CLASS_NAME);
        kd4.b(str9, "message");
        boolean z = false;
        if (!qf4.a(this.appLogInfo.getUserId())) {
            if (this.isPrintToConsole) {
                int i = WhenMappings.$EnumSwitchMapping$Anon1[logLevel.ordinal()];
                if (i == 1) {
                    Log.d("REMOTE - " + str8, str9);
                } else if (i == 2) {
                    Log.i("REMOTE - " + str8, str9);
                } else if (i == 3) {
                    Log.e("REMOTE - " + str8, str9);
                } else if (i == 4) {
                    Log.i("SUMMARY - " + str8, str9);
                }
            }
            if (qf4.b(this.activeDeviceInfo.getDeviceSerial(), str7, true)) {
                str6 = this.activeDeviceInfo.getFwVersion();
                str5 = this.activeDeviceInfo.getDeviceModel();
            } else {
                str6 = "";
                str5 = str6;
            }
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            str4 = "Calendar.getInstance()";
            LogEvent logEvent = r1;
            LogEvent logEvent2 = new LogEvent(logLevel, instance.getTimeInMillis(), this.appLogInfo.getUserId(), this.appLogInfo.getPhoneID(), this.appLogInfo.getAppVersion(), this.appLogInfo.getPlatform(), this.appLogInfo.getPlatformVersion(), this.appLogInfo.getPhoneModel(), str6, this.appLogInfo.getSdkVersion(), str5, component, session, str, str3);
            BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
            if (bufferLogWriter2 != null) {
                bufferLogWriter2.writeLog(logEvent);
                z = true;
            }
        } else {
            str4 = "Calendar.getInstance()";
        }
        Calendar instance2 = Calendar.getInstance();
        kd4.a((Object) instance2, str4);
        if (instance2.getTimeInMillis() - this.lastSyncTime >= ((long) this.flushLogTimeThreshold)) {
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$log$Anon1(this, (yb4) null), 3, (Object) null);
        }
        return z;
    }

    @DexIgnore
    public void onFullBuffer(List<LogEvent> list, boolean z) {
        kd4.b(list, "logLines");
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$onFullBuffer$Anon1(this, list, z, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public void onReachDBThreshold() {
        if (this.isMainFLogger) {
            fi4 unused = ag4.b(ah4.a(nh4.b()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$onReachDBThreshold$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void onWrittenSummaryLog(LogEvent logEvent) {
        kd4.b(logEvent, "logEvent");
        if (this.isDebuggable) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".onWrittenSummaryLog(), logEvent=" + logEvent);
            fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new RemoteFLogger$onWrittenSummaryLog$Anon1(this, (yb4) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void setPrintToConsole(boolean z) {
        this.isPrintToConsole = z;
    }

    @DexIgnore
    public void startSession(FLogger.Session session, String str, String str2) {
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            startSession(summaryKey, str);
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_SERIAL, str);
            sendInternalMessage(MESSAGE_ACTION_START_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public void summary(int i, FLogger.Component component, FLogger.Session session, String str, String str2) {
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            long currentTimeMillis = System.currentTimeMillis();
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (sessionSummary != null) {
                sessionSummary.update(i);
                sessionSummary.setEndTimeStamp(currentTimeMillis);
                if (this.isInitialized) {
                    log(FLogger.LogLevel.SUMMARY, component, session, str, str2, sessionSummary.toJsonString());
                }
                SessionSummary remove = this.summarySessionMap.remove(summaryKey);
            }
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            sendInternalMessage(MESSAGE_ACTION_END_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public final void unregisterBroadcastMessage(Context context2) {
        kd4.b(context2, "context");
        context2.unregisterReceiver(this.mMessageBroadcastReceiver);
    }

    @DexIgnore
    public void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo2) {
        kd4.b(activeDeviceInfo2, "activeDeviceInfo");
        this.activeDeviceInfo = activeDeviceInfo2;
    }

    @DexIgnore
    public void updateAppLogInfo(AppLogInfo appLogInfo2) {
        kd4.b(appLogInfo2, "appLogInfo");
        this.appLogInfo = appLogInfo2;
    }

    @DexIgnore
    public void updateCloudLogConfig(CloudLogConfig cloudLogConfig) {
        kd4.b(cloudLogConfig, "cloudLogConfig");
        LogEndPoint.INSTANCE.init(cloudLogConfig.getLogBrandName(), cloudLogConfig.getEndPointBaseUrl(), cloudLogConfig.getAccessKey(), cloudLogConfig.getSecretKey());
        LogApiService logApiService = LogEndPoint.INSTANCE.getLogApiService();
        if (logApiService != null) {
            this.remoteLogWriter = new CloudLogWriter(logApiService);
        }
    }

    @DexIgnore
    public void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo2) {
        kd4.b(sessionDetailInfo2, "sessionDetailInfo");
        this.sessionDetailInfo = sessionDetailInfo2;
    }

    @DexIgnore
    private final void startSession(String str, String str2) {
        SessionSummary sessionSummary = this.summarySessionMap.get(str);
        if (sessionSummary != null) {
            sessionSummary.setStartTimeStamp(System.currentTimeMillis());
            sessionSummary.setEndTimeStamp(-1);
            sessionSummary.getErrors().clear();
            SessionSummary sessionSummary2 = this.summarySessionMap.get(str);
            if (sessionSummary2 != null) {
                sessionSummary2.setDetails(getSessionDetails(str, str2));
                return;
            }
            return;
        }
        this.summarySessionMap.put(str, new SessionSummary(System.currentTimeMillis(), -1));
        SessionSummary sessionSummary3 = this.summarySessionMap.get(str);
        if (sessionSummary3 != null) {
            sessionSummary3.setDetails(getSessionDetails(str, str2));
        }
    }

    @DexIgnore
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        kd4.b(component, "component");
        kd4.b(session, Constants.SESSION);
        kd4.b(str, "serial");
        kd4.b(str2, PinObject.COLUMN_CLASS_NAME);
        kd4.b(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.ERROR, component, session, str, str2, str3);
        }
    }
}
