package com.misfit.frameworks.buttonservice.log;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFSyncLog extends MFLog {
    @DexIgnore
    public long activityPoint; // = -1;
    @DexIgnore
    public int activityTaggingState; // = -1;
    @DexIgnore
    public String alarm; // = "";
    @DexIgnore
    public int battery; // = -1;
    @DexIgnore
    public long goal; // = -1;
    @DexIgnore
    public int phoneTimestamp; // = 0;
    @DexIgnore
    public long postSyncActivityPoint; // = -1;
    @DexIgnore
    public long postSyncGoal; // = -1;
    @DexIgnore
    public int postSyncTimezone; // = -1;
    @DexIgnore
    public long realTimeStep; // = 0;
    @DexIgnore
    public int retries; // = -1;
    @DexIgnore
    public int rssi; // = -1;
    @DexIgnore
    public int syncMode; // = -1;
    @DexIgnore
    public int timezone; // = -1;
    @DexIgnore
    public long todaySumMinuteDataStep; // = 0;
    @DexIgnore
    public int watchTimestamp; // = 0;

    @DexIgnore
    public MFSyncLog(Context context) {
        super(context);
    }

    @DexIgnore
    public long getActivityPoint() {
        return this.activityPoint;
    }

    @DexIgnore
    public int getActivityTaggingState() {
        return this.activityTaggingState;
    }

    @DexIgnore
    public String getAlarm() {
        return this.alarm;
    }

    @DexIgnore
    public int getBattery() {
        return this.battery;
    }

    @DexIgnore
    public long getGoal() {
        return this.goal;
    }

    @DexIgnore
    public int getLogType() {
        return 1;
    }

    @DexIgnore
    public int getPhoneTimestamp() {
        return this.phoneTimestamp;
    }

    @DexIgnore
    public long getPostSyncActivityPoint() {
        return this.postSyncActivityPoint;
    }

    @DexIgnore
    public long getPostSyncGoal() {
        return this.postSyncGoal;
    }

    @DexIgnore
    public int getPostSyncTimezone() {
        return this.postSyncTimezone;
    }

    @DexIgnore
    public long getRealTimeStep() {
        return this.realTimeStep;
    }

    @DexIgnore
    public int getRetries() {
        return this.retries;
    }

    @DexIgnore
    public int getRssi() {
        return this.rssi;
    }

    @DexIgnore
    public int getSyncMode() {
        return this.syncMode;
    }

    @DexIgnore
    public int getTimezone() {
        return this.timezone;
    }

    @DexIgnore
    public long getTodaySumMinuteDataStep() {
        return this.todaySumMinuteDataStep;
    }

    @DexIgnore
    public int getWatchTimestamp() {
        return this.watchTimestamp;
    }

    @DexIgnore
    public void setActivityPoint(long j) {
        this.activityPoint = j;
    }

    @DexIgnore
    public void setActivityTaggingState(int i) {
        this.activityTaggingState = i;
    }

    @DexIgnore
    public void setAlarm(String str) {
        this.alarm = str;
    }

    @DexIgnore
    public void setBattery(int i) {
        this.battery = i;
    }

    @DexIgnore
    public void setGoal(long j) {
        this.goal = j;
    }

    @DexIgnore
    public void setPhoneTimestamp(int i) {
        this.phoneTimestamp = i;
    }

    @DexIgnore
    public void setPostSyncActivityPoint(long j) {
        this.postSyncActivityPoint = j;
    }

    @DexIgnore
    public void setPostSyncGoal(long j) {
        this.postSyncGoal = j;
    }

    @DexIgnore
    public void setPostSyncTimezone(int i) {
        this.postSyncTimezone = i;
    }

    @DexIgnore
    public void setRealTimeStep(long j) {
        this.realTimeStep = j;
    }

    @DexIgnore
    public void setRetries(int i) {
        this.retries = i;
    }

    @DexIgnore
    public void setRssi(int i) {
        this.rssi = i;
    }

    @DexIgnore
    public void setSyncMode(int i) {
        this.syncMode = i;
    }

    @DexIgnore
    public void setTimezone(int i) {
        this.timezone = i;
    }

    @DexIgnore
    public void setTodaySumMinuteDataStep(long j) {
        this.todaySumMinuteDataStep = j;
    }

    @DexIgnore
    public void setWatchTimestamp(int i) {
        this.watchTimestamp = i;
    }
}
