package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogFlagConverter {
    @DexIgnore
    public final String logFlagEnumToString(Log.Flag flag) {
        kd4.b(flag, "value");
        return flag.getValue();
    }

    @DexIgnore
    public final Log.Flag stringToLogFlag(String str) {
        if (str != null) {
            Log.Flag fromValue = Log.Flag.Companion.fromValue(str);
            if (fromValue != null) {
                return fromValue;
            }
        }
        return Log.Flag.ADD;
    }
}
