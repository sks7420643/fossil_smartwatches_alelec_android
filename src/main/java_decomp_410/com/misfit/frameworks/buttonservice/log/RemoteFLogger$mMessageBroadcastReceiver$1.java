package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RemoteFLogger$mMessageBroadcastReceiver$1 extends android.content.BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.RemoteFLogger this$0;

    @DexIgnore
    public RemoteFLogger$mMessageBroadcastReceiver$1(com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger) {
        this.this$0 = remoteFLogger;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0089, code lost:
        if ((!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00b2, code lost:
        if ((!com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        java.lang.String str;
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        java.lang.String stringExtra = intent.getStringExtra("action");
        com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget fromValue = com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.Companion.fromValue(intent.getIntExtra("target", com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.TO_ALL_FLOGGER.getValue()));
        if (intent.hasExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_SENDER_KEY)) {
            str = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_SENDER_KEY);
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) str, "intent.getStringExtra(MESSAGE_SENDER_KEY)");
        } else {
            str = "";
        }
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(com.misfit.frameworks.buttonservice.log.RemoteFLogger.TAG, "mMessageBroadcastReceiver.onReceive(), logAction=" + stringExtra + ", target=" + fromValue + ", sender=" + str);
        int i = com.misfit.frameworks.buttonservice.log.RemoteFLogger.WhenMappings.$EnumSwitchMapping$0[fromValue.ordinal()];
        boolean z = false;
        if (i != 1) {
            if (i == 2) {
                z = !com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) this.this$0.floggerName, (java.lang.Object) str);
            } else if (i != 3) {
                throw new kotlin.NoWhenBranchMatchedException();
            } else if (!this.this$0.isMainFLogger) {
            }
            if (!z && stringExtra != null) {
                switch (stringExtra.hashCode()) {
                    case -1960182802:
                        if (stringExtra.equals(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_END_SESSION)) {
                            java.lang.String stringExtra2 = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            if (stringExtra2 != null) {
                                com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary sessionSummary = (com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary) this.this$0.summarySessionMap.remove(stringExtra2);
                                return;
                            }
                            return;
                        }
                        return;
                    case -1885432660:
                        if (stringExtra.equals(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER)) {
                            com.misfit.frameworks.buttonservice.log.BufferLogWriter access$getBufferLogWriter$p = this.this$0.bufferLogWriter;
                            if (access$getBufferLogWriter$p != null) {
                                access$getBufferLogWriter$p.forceFlushBuffer();
                                return;
                            }
                            return;
                        }
                        return;
                    case 308052341:
                        if (stringExtra.equals(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_START_SESSION)) {
                            java.lang.String stringExtra3 = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            java.lang.String stringExtra4 = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_PARAM_SERIAL);
                            if (stringExtra3 != null) {
                                com.misfit.frameworks.buttonservice.log.RemoteFLogger remoteFLogger = this.this$0;
                                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) stringExtra4, "serial");
                                remoteFLogger.startSession(stringExtra3, stringExtra4);
                                return;
                            }
                            return;
                        }
                        return;
                    case 1513386699:
                        if (stringExtra.equals(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_ERROR_RECORDED)) {
                            java.lang.String stringExtra5 = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_PARAM_SUMMARY_KEY);
                            java.lang.String stringExtra6 = intent.getStringExtra(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_PARAM_ERROR_CODE);
                            if (stringExtra5 != null && stringExtra6 != null) {
                                com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary sessionSummary2 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary) this.this$0.summarySessionMap.get(stringExtra5);
                                if (sessionSummary2 != null) {
                                    sessionSummary2.getErrors().add(stringExtra6);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    case 1920521161:
                        if (stringExtra.equals(com.misfit.frameworks.buttonservice.log.RemoteFLogger.MESSAGE_ACTION_FLUSH)) {
                            com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$1$onReceive$4(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            } else {
                return;
            }
        } else {
            if (this.this$0.isMainFLogger) {
            }
            if (!z) {
                return;
            }
            return;
        }
        z = true;
        if (!z) {
        }
    }
}
