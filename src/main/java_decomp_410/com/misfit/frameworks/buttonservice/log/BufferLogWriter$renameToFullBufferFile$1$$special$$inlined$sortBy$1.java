package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BufferLogWriter$renameToFullBufferFile$1$$special$$inlined$sortBy$1<T> implements java.util.Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        java.io.File file = (java.io.File) t;
        kotlin.text.Regex regex = new kotlin.text.Regex("\\d+");
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) file, "it");
        java.lang.String name = file.getName();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) name, "it.name");
        java.lang.String str = null;
        com.fossil.blesdk.obfuscated.hf4 find$default = kotlin.text.Regex.find$default(regex, name, 0, 2, (java.lang.Object) null);
        java.lang.String value = find$default != null ? find$default.getValue() : null;
        java.io.File file2 = (java.io.File) t2;
        kotlin.text.Regex regex2 = new kotlin.text.Regex("\\d+");
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) file2, "it");
        java.lang.String name2 = file2.getName();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) name2, "it.name");
        com.fossil.blesdk.obfuscated.hf4 find$default2 = kotlin.text.Regex.find$default(regex2, name2, 0, 2, (java.lang.Object) null);
        if (find$default2 != null) {
            str = find$default2.getValue();
        }
        return com.fossil.blesdk.obfuscated.wb4.a(value, str);
    }
}
