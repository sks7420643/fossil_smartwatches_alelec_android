package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Lambda;
import kotlin.sequences.SequencesKt___SequencesKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super List<LogEvent>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements xc4<Log, LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final LogEvent invoke(Log log) {
            kd4.b(log, "it");
            try {
                LogEvent logEvent = (LogEvent) this.$gson.a(log.getContent(), LogEvent.class);
                logEvent.setTag(Integer.valueOf(log.getId()));
                return logEvent;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = DBLogWriter.TAG;
                local.e(access$getTAG$cp, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$Anon2(DBLogWriter dBLogWriter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dBLogWriter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DBLogWriter$getAllLogsExceptSyncing$Anon2 dBLogWriter$getAllLogsExceptSyncing$Anon2 = new DBLogWriter$getAllLogsExceptSyncing$Anon2(this.this$Anon0, yb4);
        dBLogWriter$getAllLogsExceptSyncing$Anon2.p$ = (zg4) obj;
        return dBLogWriter$getAllLogsExceptSyncing$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$getAllLogsExceptSyncing$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            return SequencesKt___SequencesKt.g(SequencesKt___SequencesKt.d(kb4.b(this.this$Anon0.logDao.getAllLogEventsExcept(Log.Flag.SYNCING)), new Anon1(new Gson())));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
