package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4", f = "RemoteFLogger.kt", l = {95}, m = "invokeSuspend")
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger$mMessageBroadcastReceiver$Anon1 this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4(RemoteFLogger$mMessageBroadcastReceiver$Anon1 remoteFLogger$mMessageBroadcastReceiver$Anon1, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = remoteFLogger$mMessageBroadcastReceiver$Anon1;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4 remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4 = new RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4(this.this$Anon0, yb4);
        remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4.p$ = (zg4) obj;
        return remoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 zg4 = this.p$;
            RemoteFLogger remoteFLogger = this.this$Anon0.this$Anon0;
            this.L$Anon0 = zg4;
            this.label = 1;
            if (remoteFLogger.flushBuffer(this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 zg42 = (zg4) this.L$Anon0;
            na4.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return qa4.a;
    }
}
