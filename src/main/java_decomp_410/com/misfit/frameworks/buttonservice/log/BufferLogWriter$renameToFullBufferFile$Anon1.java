package com.misfit.frameworks.buttonservice.log;

import android.util.Log;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.dl4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.gh4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.kv1;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$Anon1", f = "BufferLogWriter.kt", l = {220}, m = "invokeSuspend")
public final class BufferLogWriter$renameToFullBufferFile$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public Object L$Anon0;
    @DexIgnore
    public Object L$Anon1;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$renameToFullBufferFile$Anon1(BufferLogWriter bufferLogWriter, boolean z, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = bufferLogWriter;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BufferLogWriter$renameToFullBufferFile$Anon1 bufferLogWriter$renameToFullBufferFile$Anon1 = new BufferLogWriter$renameToFullBufferFile$Anon1(this.this$Anon0, this.$forceFlush, yb4);
        bufferLogWriter$renameToFullBufferFile$Anon1.p$ = (zg4) obj;
        return bufferLogWriter$renameToFullBufferFile$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$renameToFullBufferFile$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        dl4 dl4;
        zg4 zg4;
        File file;
        Iterable iterable;
        Object a = cc4.a();
        int i = this.label;
        if (i == 0) {
            na4.a(obj);
            zg4 = this.p$;
            dl4 = this.this$Anon0.mMutex;
            this.L$Anon0 = zg4;
            this.L$Anon1 = dl4;
            this.label = 1;
            if (dl4.a((Object) null, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (zg4) this.L$Anon0;
            na4.a(obj);
            dl4 = (dl4) this.L$Anon1;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        zg4 zg42 = zg4;
        try {
            File[] listFiles = new File(this.this$Anon0.directoryPath).listFiles(BufferLogWriter$renameToFullBufferFile$Anon1$Anon1$founds$Anon1.INSTANCE);
            int i2 = 0;
            if (this.this$Anon0.isMainFLogger) {
                kd4.a((Object) listFiles, "founds");
                if (listFiles.length > 1) {
                    ya4.a((T[]) listFiles, new BufferLogWriter$renameToFullBufferFile$Anon1$$special$$inlined$sortBy$Anon1());
                }
                ArrayList arrayList = new ArrayList();
                int length = listFiles.length;
                for (int i3 = 0; i3 < length; i3++) {
                    file = listFiles[i3];
                    iterable = kv1.b(file, Charset.defaultCharset());
                    hb4.a(arrayList, iterable);
                }
                List d = kb4.d(arrayList);
                int length2 = listFiles.length;
                while (i2 < length2) {
                    File file2 = listFiles[i2];
                    try {
                        file2.delete();
                    } catch (Exception e) {
                        String access$getTAG$cp = BufferLogWriter.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append(".renameToFullBufferFile(), delete buffer file, name=");
                        kd4.a((Object) file2, "it");
                        sb.append(file2.getName());
                        sb.append(", ex=");
                        sb.append(e);
                        Log.e(access$getTAG$cp, sb.toString());
                    }
                    i2++;
                }
                gh4 unused = ag4.a(zg42, nh4.a(), (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1(d, (yb4) null, this, zg42), 2, (Object) null);
            } else {
                kd4.a((Object) listFiles, "founds");
                if (listFiles.length == 0) {
                    i2 = 1;
                }
                if ((i2 ^ 1) != 0) {
                    gh4 unused2 = ag4.a(zg42, nh4.a(), (CoroutineStart) null, new BufferLogWriter$renameToFullBufferFile$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2((yb4) null, this, zg42), 2, (Object) null);
                } else {
                    qa4 qa4 = qa4.a;
                }
            }
            dl4.a((Object) null);
            return qa4.a;
        } catch (Exception e2) {
            String access$getTAG$cp2 = BufferLogWriter.TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(".renameToFullBufferFile(), read buffer file, name=");
            kd4.a((Object) file, "it");
            sb2.append(file.getName());
            sb2.append(", ex=");
            sb2.append(e2);
            Log.e(access$getTAG$cp2, sb2.toString());
            iterable = new ArrayList();
        } catch (Throwable th) {
            dl4.a((Object) null);
            throw th;
        }
    }
}
