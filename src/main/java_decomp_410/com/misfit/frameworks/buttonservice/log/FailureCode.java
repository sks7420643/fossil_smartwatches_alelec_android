package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FailureCode {
    @DexIgnore
    public static /* final */ int APP_CRASH_FROM_APP_LAYER; // = 1906;
    @DexIgnore
    public static /* final */ int APP_CRASH_FROM_BUTTON_SERVICE; // = 1905;
    @DexIgnore
    public static /* final */ int BLUETOOTH_IS_DISABLED; // = 1101;
    @DexIgnore
    public static /* final */ int CONNECT_BY_MAC_SUCCESS; // = 1217;
    @DexIgnore
    public static /* final */ int CONNECT_BY_SCANNED_DEVICE_SUCCESS; // = 1218;
    @DexIgnore
    public static /* final */ int DEVICE_NOT_FOUND; // = 1122;
    @DexIgnore
    public static /* final */ int DEVICE_NOT_FOUND_AFTER_OTA; // = 1123;
    @DexIgnore
    public static /* final */ int FAILED_BY_DEVICE_DISCONNECTED; // = 8888;
    @DexIgnore
    public static /* final */ int FAILED_TO_ACTIVATE; // = 1204;
    @DexIgnore
    public static /* final */ int FAILED_TO_APPLY_HAND_POSITION; // = 1353;
    @DexIgnore
    public static /* final */ int FAILED_TO_CLEAR_ALARM; // = 1313;
    @DexIgnore
    public static /* final */ int FAILED_TO_CLEAR_DATA; // = 1927;
    @DexIgnore
    public static /* final */ int FAILED_TO_CLEAR_LAST_NOTIFICATION; // = 1345;
    @DexIgnore
    public static /* final */ int FAILED_TO_CLEAR_LINK_MAPPING; // = 1334;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT; // = 1201;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_AFTER_OTA; // = 1202;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_AFTER_OTA_BY_BOND; // = 1207;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_AFTER_OTA_BY_BOND_TIMEOUT; // = 1214;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_AFTER_OTA_TIMEOUT; // = 1212;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_BY_BOND; // = 1206;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_BY_BOND_TIMEOUT; // = 1213;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_BY_SCANNED_DEVICE; // = 1215;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_BY_SCANNED_DEVICE_TIMEOUT; // = 1216;
    @DexIgnore
    public static /* final */ int FAILED_TO_CONNECT_TIMEOUT; // = 1211;
    @DexIgnore
    public static /* final */ int FAILED_TO_DISABLE_GOAL_TRACKING; // = 1362;
    @DexIgnore
    public static /* final */ int FAILED_TO_DISABLE_HEART_RATE_NOTIFICATION; // = 1908;
    @DexIgnore
    public static /* final */ int FAILED_TO_ENABLE_GOAL_TRACKING; // = 1361;
    @DexIgnore
    public static /* final */ int FAILED_TO_ENABLE_HEART_RATE_NOTIFICATION; // = 1907;
    @DexIgnore
    public static /* final */ int FAILED_TO_ENABLE_MAINTAINING_CONNECTION; // = 2000;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_ALARM; // = 1312;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_BATTERY; // = 1305;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_CONFIG; // = 1302;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_COUNTDOWN; // = 1372;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_REAL_TIME_STEP; // = 1504;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_SECOND_TIMEZONE; // = 1322;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_STEP_GOAL; // = 1506;
    @DexIgnore
    public static /* final */ int FAILED_TO_GET_VIBRATION_STRENGTH; // = 1307;
    @DexIgnore
    public static /* final */ int FAILED_TO_LINK_SERVER; // = 1928;
    @DexIgnore
    public static /* final */ int FAILED_TO_MICRO_APP_SETTING; // = 1385;
    @DexIgnore
    public static /* final */ int FAILED_TO_MOVE_HAND; // = 1352;
    @DexIgnore
    public static /* final */ int FAILED_TO_OTA; // = 1401;
    @DexIgnore
    public static /* final */ int FAILED_TO_OTA_FILE_NOT_READY; // = 1403;
    @DexIgnore
    public static /* final */ int FAILED_TO_OTA_FW_NOT_MATCH; // = 1404;
    @DexIgnore
    public static /* final */ int FAILED_TO_PLAY_ANIMATION; // = 201;
    @DexIgnore
    public static /* final */ int FAILED_TO_PLAY_LIGHT_VIBRATION; // = 1344;
    @DexIgnore
    public static /* final */ int FAILED_TO_PLAY_VIBRATION; // = 1343;
    @DexIgnore
    public static /* final */ int FAILED_TO_PREPARE; // = 1203;
    @DexIgnore
    public static /* final */ int FAILED_TO_PREPARE_AFTER_OTA; // = 1205;
    @DexIgnore
    public static /* final */ int FAILED_TO_READ_CURRENT_WORKOUT_SESSION; // = 1507;
    @DexIgnore
    public static /* final */ int FAILED_TO_READ_RSSI; // = 1308;
    @DexIgnore
    public static /* final */ int FAILED_TO_RELEASE_HAND_CONTROL; // = 1354;
    @DexIgnore
    public static /* final */ int FAILED_TO_REQUEST_HAND_CONTROL; // = 1351;
    @DexIgnore
    public static /* final */ int FAILED_TO_RESET_HAND_CONTROL; // = 1355;
    @DexIgnore
    public static /* final */ int FAILED_TO_RETRIEVE_HEART_RATE_FILE; // = 1909;
    @DexIgnore
    public static /* final */ int FAILED_TO_SEND_NOTIFICATION; // = 1342;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_ALARM; // = 1311;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG; // = 1923;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_BIOMETRIC_DATA; // = 1922;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_COMPLICATION_APPS; // = 1920;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_CONFIG; // = 1301;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_CONFIG_BACK; // = 1304;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_CONNECTION_PARAM; // = 1303;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_CONNECTION_PARAM_INCOMPATIBLE; // = 1310;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_COUNTDOWN; // = 1371;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_INACTIVE_NUDGE_CONFIG; // = 1925;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_LINK_MAPPING; // = 1331;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_LINK_MAPPING_EMPTY; // = 1332;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_LOCALIZATION_DATA; // = 1926;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_MAPPING_ANIMATION; // = 1337;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_MICRO_APP_MAPPING; // = 1338;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_NOTIFICATION; // = 1341;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG; // = 1924;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_REAL_TIME_STEP; // = 1503;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_SECOND_TIMEZONE; // = 1321;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_STEP_GOAL; // = 1505;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_STOP_WATCH_SETTING; // = 1384;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_TIME; // = 1309;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_VIBRATION_STRENGTH; // = 1306;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_WATCH_APPS; // = 1921;
    @DexIgnore
    public static /* final */ int FAILED_TO_SET_WATCH_PARAMS; // = 1930;
    @DexIgnore
    public static /* final */ int FAILED_TO_START_STREAMING; // = 1335;
    @DexIgnore
    public static /* final */ int FAILED_TO_STOP_CURRENT_WORKOUT_SESSION; // = 1509;
    @DexIgnore
    public static /* final */ int FAILED_TO_STOP_STREAMING; // = 1336;
    @DexIgnore
    public static /* final */ int FAILED_TO_SYNC; // = 1501;
    @DexIgnore
    public static /* final */ int FAILED_TO_WORKOUT_IS_RUNNING; // = 1508;
    @DexIgnore
    public static /* final */ int FAIL_TO_AUTHENTICATE_DEVICE; // = 1217;
    @DexIgnore
    public static /* final */ int FAIL_TO_EXCHANGE_SECRET_KEY; // = 1218;
    @DexIgnore
    public static /* final */ int FAIL_TO_GET_RANDOM_KEY; // = 1219;
    @DexIgnore
    public static /* final */ int FEATURE_IS_NOT_SUPPORTED; // = 10000;
    @DexIgnore
    public static /* final */ int FIRMWARE_INCOMPATIBLE; // = 1502;
    @DexIgnore
    public static /* final */ int LOCATION_ACCESS_DENIED; // = 1113;
    @DexIgnore
    public static /* final */ int LOCATION_SERVICE_DISABLED; // = 1112;
    @DexIgnore
    public static /* final */ int NO_DEVICE_FOUND; // = 1121;
    @DexIgnore
    public static /* final */ int OTA_INTERRUPTED; // = 1402;
    @DexIgnore
    public static /* final */ int PERMISSION_GRANTED; // = 1114;
    @DexIgnore
    public static /* final */ int SCANNING_FAILED_WITH_SDK_EXCEPTION; // = 1111;
    @DexIgnore
    public static /* final */ int SESSION_INTERRUPTED; // = 1611;
    @DexIgnore
    public static /* final */ int SUCCESS; // = 0;
    @DexIgnore
    public static /* final */ int UNEXPECTED_DISCONNECT; // = 1801;
    @DexIgnore
    public static /* final */ int UNKNOWN_ERROR; // = 9999;
    @DexIgnore
    public static /* final */ int USER_CANCELLED; // = 1603;
    @DexIgnore
    public static /* final */ int USER_CANCELLED_BUT_USER_DID_NOT_SELECT_ANY_DEVICE; // = 1602;
    @DexIgnore
    public static /* final */ int USER_CANCELLED_NO_DEVICE_FOUND; // = 1601;
    @DexIgnore
    public static /* final */ int USER_KILL_APP_FROM_TASK_MANAGER; // = 1904;
    @DexIgnore
    public static /* final */ int VERIFY_MULTIPLE_ALARMS_FAILED; // = 101;

    @DexIgnore
    public static int getFailureCodeNoDeviceFound(Context context, int i) {
        if (!LocationUtils.isLocationEnable(context)) {
            return LOCATION_SERVICE_DISABLED;
        }
        return !LocationUtils.isLocationPermissionGranted(context) ? LOCATION_ACCESS_DENIED : i;
    }
}
