package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$run$Anon1", f = "BufferLogWriter.kt", l = {}, m = "invokeSuspend")
public final class BufferLogWriter$run$Anon1 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ LogEvent $logEvent;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$run$Anon1(BufferLogWriter bufferLogWriter, LogEvent logEvent, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = bufferLogWriter;
        this.$logEvent = logEvent;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BufferLogWriter$run$Anon1 bufferLogWriter$run$Anon1 = new BufferLogWriter$run$Anon1(this.this$Anon0, this.$logEvent, yb4);
        bufferLogWriter$run$Anon1.p$ = (zg4) obj;
        return bufferLogWriter$run$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$run$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            BufferLogWriter.IBufferLogCallback callback = this.this$Anon0.getCallback();
            if (callback != null) {
                callback.onWrittenSummaryLog(this.$logEvent);
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
