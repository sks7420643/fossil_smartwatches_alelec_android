package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogEndPoint$init$httpLogInterceptor$1 implements okhttp3.logging.HttpLoggingInterceptor.C7380a {
    @DexIgnore
    public static /* final */ com.misfit.frameworks.buttonservice.log.LogEndPoint$init$httpLogInterceptor$1 INSTANCE; // = new com.misfit.frameworks.buttonservice.log.LogEndPoint$init$httpLogInterceptor$1();

    @DexIgnore
    public final void log(java.lang.String str) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d("OkHttp", str);
    }
}
