package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppLogInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public /* final */ String appVersion;
    @DexIgnore
    public /* final */ String phoneID;
    @DexIgnore
    public /* final */ String phoneModel;
    @DexIgnore
    public /* final */ String platform;
    @DexIgnore
    public /* final */ String platformVersion;
    @DexIgnore
    public /* final */ String sdkVersion;
    @DexIgnore
    public /* final */ String userId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppLogInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AppLogInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AppLogInfo(parcel);
        }

        @DexIgnore
        public AppLogInfo[] newArray(int i) {
            return new AppLogInfo[i];
        }
    }

    @DexIgnore
    public AppLogInfo(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        kd4.b(str, ButtonService.USER_ID);
        kd4.b(str2, "appVersion");
        kd4.b(str3, "platform");
        kd4.b(str4, "platformVersion");
        kd4.b(str5, "phoneID");
        kd4.b(str6, "sdkVersion");
        kd4.b(str7, "phoneModel");
        this.userId = str;
        this.appVersion = str2;
        this.platform = str3;
        this.platformVersion = str4;
        this.phoneID = str5;
        this.sdkVersion = str6;
        this.phoneModel = str7;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final String getPhoneID() {
        return this.phoneID;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.userId);
        parcel.writeString(this.appVersion);
        parcel.writeString(this.platform);
        parcel.writeString(this.platformVersion);
        parcel.writeString(this.phoneID);
        parcel.writeString(this.sdkVersion);
        parcel.writeString(this.phoneModel);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public AppLogInfo(Parcel parcel) {
        throw null;
/*        this(r3, r4, r5, r6, r7, r8, r9);
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        String str7 = readString != null ? readString : "";
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        String readString4 = parcel.readString();
        if (readString4 != null) {
            str3 = readString4;
        } else {
            str3 = "";
        }
        String readString5 = parcel.readString();
        if (readString5 != null) {
            str4 = readString5;
        } else {
            str4 = "";
        }
        String readString6 = parcel.readString();
        if (readString6 != null) {
            str5 = readString6;
        } else {
            str5 = "";
        }
        String readString7 = parcel.readString();
        if (readString7 != null) {
            str6 = readString7;
        } else {
            str6 = "";
        }
*/    }
}
