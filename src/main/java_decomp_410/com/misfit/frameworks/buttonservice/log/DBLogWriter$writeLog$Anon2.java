package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.db.Log;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$Anon2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$writeLog$Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$Anon2(DBLogWriter dBLogWriter, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = dBLogWriter;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        DBLogWriter$writeLog$Anon2 dBLogWriter$writeLog$Anon2 = new DBLogWriter$writeLog$Anon2(this.this$Anon0, yb4);
        dBLogWriter$writeLog$Anon2.p$ = (zg4) obj;
        return dBLogWriter$writeLog$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$writeLog$Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            int countExcept = this.this$Anon0.logDao.countExcept(Log.Flag.SYNCING);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String access$getTAG$cp = DBLogWriter.TAG;
            local.d(access$getTAG$cp, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$Anon0.thresholdValue) {
                DBLogWriter.IDBLogWriterCallback access$getCallback$p = this.this$Anon0.callback;
                if (access$getCallback$p != null) {
                    access$getCallback$p.onReachDBThreshold();
                }
            }
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
