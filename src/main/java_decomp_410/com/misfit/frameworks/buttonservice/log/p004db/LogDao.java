package com.misfit.frameworks.buttonservice.log.p004db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.log.db.LogDao */
public interface LogDao {
    @DexIgnore
    int countExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag);

    @DexIgnore
    int delete(java.util.List<java.lang.Integer> list);

    @DexIgnore
    java.util.List<com.misfit.frameworks.buttonservice.log.p004db.Log> getAllLogEventsExcept(com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag);

    @DexIgnore
    void insertLogEvent(java.util.List<com.misfit.frameworks.buttonservice.log.p004db.Log> list);

    @DexIgnore
    void updateCloudFlagByIds(java.util.List<java.lang.Integer> list, com.misfit.frameworks.buttonservice.log.p004db.Log.Flag flag);
}
