package com.misfit.frameworks.buttonservice.log;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rz1;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FLogUtils {
    @DexIgnore
    public static /* final */ FLogUtils INSTANCE; // = new FLogUtils();

    @DexIgnore
    public final Gson getGsonForLogEvent() {
        rz1 rz1 = new rz1();
        rz1.a(Long.class, FLogUtils$getGsonForLogEvent$Anon1.INSTANCE);
        Gson a = rz1.a();
        kd4.a((Object) a, "GsonBuilder()\n          \u2026                .create()");
        return a;
    }
}
