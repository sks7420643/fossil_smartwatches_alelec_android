package com.misfit.frameworks.buttonservice.log.db;

import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.bg;
import com.fossil.blesdk.obfuscated.eg;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.hg;
import com.fossil.blesdk.obfuscated.jf;
import com.fossil.blesdk.obfuscated.pf;
import com.fossil.blesdk.obfuscated.tf;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogDatabase_Impl extends LogDatabase {
    @DexIgnore
    public volatile LogDao _logDao;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends tf.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(gg ggVar) {
            ggVar.b("CREATE TABLE IF NOT EXISTS `log` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `timeStamp` INTEGER NOT NULL, `content` TEXT NOT NULL, `cloudFlag` TEXT NOT NULL)");
            ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            ggVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '1e76ae5948872f78452bd16b18a13dc4')");
        }

        @DexIgnore
        public void dropAllTables(gg ggVar) {
            ggVar.b("DROP TABLE IF EXISTS `log`");
        }

        @DexIgnore
        public void onCreate(gg ggVar) {
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) LogDatabase_Impl.this.mCallbacks.get(i)).a(ggVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(gg ggVar) {
            gg unused = LogDatabase_Impl.this.mDatabase = ggVar;
            LogDatabase_Impl.this.internalInitInvalidationTracker(ggVar);
            if (LogDatabase_Impl.this.mCallbacks != null) {
                int size = LogDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((RoomDatabase.b) LogDatabase_Impl.this.mCallbacks.get(i)).b(ggVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(gg ggVar) {
        }

        @DexIgnore
        public void onPreMigrate(gg ggVar) {
            bg.a(ggVar);
        }

        @DexIgnore
        public void validateMigration(gg ggVar) {
            HashMap hashMap = new HashMap(4);
            hashMap.put("id", new eg.a("id", "INTEGER", true, 1));
            hashMap.put("timeStamp", new eg.a("timeStamp", "INTEGER", true, 0));
            hashMap.put("content", new eg.a("content", "TEXT", true, 0));
            hashMap.put("cloudFlag", new eg.a("cloudFlag", "TEXT", true, 0));
            eg egVar = new eg("log", hashMap, new HashSet(0), new HashSet(0));
            eg a = eg.a(ggVar, "log");
            if (!egVar.equals(a)) {
                throw new IllegalStateException("Migration didn't properly handle log(com.misfit.frameworks.buttonservice.log.db.Log).\n Expected:\n" + egVar + "\n Found:\n" + a);
            }
        }
    }

    @DexIgnore
    public void clearAllTables() {
        super.assertNotMainThread();
        gg a = super.getOpenHelper().a();
        try {
            super.beginTransaction();
            a.b("DELETE FROM `log`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.x()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public pf createInvalidationTracker() {
        return new pf(this, new HashMap(0), new HashMap(0), "log");
    }

    @DexIgnore
    public hg createOpenHelper(jf jfVar) {
        tf tfVar = new tf(jfVar, new Anon1(1), "1e76ae5948872f78452bd16b18a13dc4", "3ef7199be598e6814da70cf3eed15c90");
        hg.b.a a = hg.b.a(jfVar.b);
        a.a(jfVar.c);
        a.a((hg.a) tfVar);
        return jfVar.a.a(a.a());
    }

    @DexIgnore
    public LogDao getLogDao() {
        LogDao logDao;
        if (this._logDao != null) {
            return this._logDao;
        }
        synchronized (this) {
            if (this._logDao == null) {
                this._logDao = new LogDao_Impl(this);
            }
            logDao = this._logDao;
        }
        return logDao;
    }
}
