package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2", mo27670f = "DBLogWriter.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class DBLogWriter$deleteLogs$2 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super java.lang.Integer>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ java.util.List $logIds;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20849p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$deleteLogs$2(com.misfit.frameworks.buttonservice.log.DBLogWriter dBLogWriter, java.util.List list, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = dBLogWriter;
        this.$logIds = list;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2 dBLogWriter$deleteLogs$2 = new com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2(this.this$0, this.$logIds, yb4);
        dBLogWriter$deleteLogs$2.f20849p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return dBLogWriter$deleteLogs$2;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.DBLogWriter$deleteLogs$2) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            int i = 0;
            for (java.util.List d : com.fossil.blesdk.obfuscated.kb4.m24373b(this.$logIds, 500)) {
                i += this.this$0.logDao.delete(com.fossil.blesdk.obfuscated.kb4.m24381d(d));
            }
            return com.fossil.blesdk.obfuscated.dc4.m20843a(i);
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
