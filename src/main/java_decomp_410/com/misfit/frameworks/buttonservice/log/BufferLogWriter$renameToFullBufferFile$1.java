package com.misfit.frameworks.buttonservice.log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1", mo27670f = "BufferLogWriter.kt", mo27671l = {220}, mo27672m = "invokeSuspend")
public final class BufferLogWriter$renameToFullBufferFile$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public java.lang.Object L$0;
    @DexIgnore
    public java.lang.Object L$1;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f20846p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.BufferLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$renameToFullBufferFile$1(com.misfit.frameworks.buttonservice.log.BufferLogWriter bufferLogWriter, boolean z, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = bufferLogWriter;
        this.$forceFlush = z;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1 bufferLogWriter$renameToFullBufferFile$1 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1(this.this$0, this.$forceFlush, yb4);
        bufferLogWriter$renameToFullBufferFile$1.f20846p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bufferLogWriter$renameToFullBufferFile$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.dl4 dl4;
        com.fossil.blesdk.obfuscated.zg4 zg4;
        java.io.File file;
        java.lang.Iterable iterable;
        java.lang.Object a = com.fossil.blesdk.obfuscated.cc4.m20546a();
        int i = this.label;
        if (i == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            zg4 = this.f20846p$;
            dl4 = this.this$0.mMutex;
            this.L$0 = zg4;
            this.L$1 = dl4;
            this.label = 1;
            if (dl4.mo26535a((java.lang.Object) null, this) == a) {
                return a;
            }
        } else if (i == 1) {
            zg4 = (com.fossil.blesdk.obfuscated.zg4) this.L$0;
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            dl4 = (com.fossil.blesdk.obfuscated.dl4) this.L$1;
        } else {
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        com.fossil.blesdk.obfuscated.zg4 zg42 = zg4;
        try {
            java.io.File[] listFiles = new java.io.File(this.this$0.directoryPath).listFiles(com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$1$1$founds$1.INSTANCE);
            int i2 = 0;
            if (this.this$0.isMainFLogger) {
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) listFiles, "founds");
                if (listFiles.length > 1) {
                    com.fossil.blesdk.obfuscated.ya4.m30968a((T[]) listFiles, new com.misfit.frameworks.buttonservice.log.C5565xe809eb76());
                }
                java.util.ArrayList arrayList = new java.util.ArrayList();
                int length = listFiles.length;
                for (int i3 = 0; i3 < length; i3++) {
                    file = listFiles[i3];
                    iterable = com.fossil.blesdk.obfuscated.kv1.m9865b(file, java.nio.charset.Charset.defaultCharset());
                    com.fossil.blesdk.obfuscated.hb4.m23079a(arrayList, iterable);
                }
                java.util.List d = com.fossil.blesdk.obfuscated.kb4.m24381d(arrayList);
                int length2 = listFiles.length;
                while (i2 < length2) {
                    java.io.File file2 = listFiles[i2];
                    try {
                        file2.delete();
                    } catch (java.lang.Exception e) {
                        java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG;
                        java.lang.StringBuilder sb = new java.lang.StringBuilder();
                        sb.append(".renameToFullBufferFile(), delete buffer file, name=");
                        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) file2, "it");
                        sb.append(file2.getName());
                        sb.append(", ex=");
                        sb.append(e);
                        android.util.Log.e(access$getTAG$cp, sb.toString());
                    }
                    i2++;
                }
                com.fossil.blesdk.obfuscated.gh4 unused = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, com.fossil.blesdk.obfuscated.nh4.m25691a(), (kotlinx.coroutines.CoroutineStart) null, new com.misfit.frameworks.buttonservice.log.C5566x76071dd0(d, (com.fossil.blesdk.obfuscated.yb4) null, this, zg42), 2, (java.lang.Object) null);
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) listFiles, "founds");
                if (listFiles.length == 0) {
                    i2 = 1;
                }
                if ((i2 ^ 1) != 0) {
                    com.fossil.blesdk.obfuscated.gh4 unused2 = com.fossil.blesdk.obfuscated.ag4.m19841a(zg42, com.fossil.blesdk.obfuscated.nh4.m25691a(), (kotlinx.coroutines.CoroutineStart) null, new com.misfit.frameworks.buttonservice.log.C5567x76071dd1((com.fossil.blesdk.obfuscated.yb4) null, this, zg42), 2, (java.lang.Object) null);
                } else {
                    com.fossil.blesdk.obfuscated.qa4 qa4 = com.fossil.blesdk.obfuscated.qa4.f17909a;
                }
            }
            dl4.mo26536a((java.lang.Object) null);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        } catch (java.lang.Exception e2) {
            java.lang.String access$getTAG$cp2 = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG;
            java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
            sb2.append(".renameToFullBufferFile(), read buffer file, name=");
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) file, "it");
            sb2.append(file.getName());
            sb2.append(", ex=");
            sb2.append(e2);
            android.util.Log.e(access$getTAG$cp2, sb2.toString());
            iterable = new java.util.ArrayList();
        } catch (Throwable th) {
            dl4.mo26536a((java.lang.Object) null);
            throw th;
        }
    }
}
