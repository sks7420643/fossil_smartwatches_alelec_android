package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$onHidConnectionStateChanged$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$Anon0;

    @DexIgnore
    public ButtonService$onHidConnectionStateChanged$Anon1(ButtonService buttonService, String str) {
        this.this$Anon0 = buttonService;
        this.$serialNumber = str;
    }

    @DexIgnore
    public final void run() {
        this.this$Anon0.enqueue(this.$serialNumber);
    }
}
