package com.misfit.frameworks.buttonservice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ButtonService$bondChangedReceiver$1 extends android.content.BroadcastReceiver {
    @DexIgnore
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(context, "context");
        com.fossil.blesdk.obfuscated.kd4.m24411b(intent, "intent");
        android.bluetooth.BluetoothDevice bluetoothDevice = (android.bluetooth.BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (bluetoothDevice != null) {
            switch (intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1)) {
                case 10:
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String access$getTAG$cp = com.misfit.frameworks.buttonservice.ButtonService.TAG;
                    local.mo33255d(access$getTAG$cp, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: NONE");
                    return;
                case 11:
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String access$getTAG$cp2 = com.misfit.frameworks.buttonservice.ButtonService.TAG;
                    local2.mo33255d(access$getTAG$cp2, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDING");
                    return;
                case 12:
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                    java.lang.String access$getTAG$cp3 = com.misfit.frameworks.buttonservice.ButtonService.TAG;
                    local3.mo33255d(access$getTAG$cp3, "bondChangedReceiver - " + bluetoothDevice.getAddress() + ", bond state changed: BONDED");
                    return;
                default:
                    return;
            }
        }
    }
}
