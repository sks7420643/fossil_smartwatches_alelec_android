package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DataCollectorHelper */
public class DataCollectorHelper {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.DataCollectorHelper$1")
    /* renamed from: com.misfit.frameworks.buttonservice.db.DataCollectorHelper$1 */
    public static class C39581 implements java.lang.Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ android.content.Context val$context;
        @DexIgnore
        public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.p003db.HardwareLog val$hwLog;

        @DexIgnore
        public C39581(com.misfit.frameworks.buttonservice.p003db.HardwareLog hardwareLog, android.content.Context context) {
            this.val$hwLog = hardwareLog;
            this.val$context = context;
        }

        @DexIgnore
        public void run() {
            if (this.val$hwLog != null) {
                com.misfit.frameworks.buttonservice.p003db.HwLogProvider.getInstance(this.val$context).saveHwLog(this.val$hwLog);
            }
        }
    }

    @DexIgnore
    public static void saveDataFile(android.content.Context context, com.misfit.frameworks.buttonservice.p003db.DataFile dataFile) {
        if (dataFile != null) {
            com.misfit.frameworks.buttonservice.p003db.DataFileProvider.getInstance(context).saveDataFile(dataFile);
        }
    }

    @DexIgnore
    public static void saveHeartRateFile(android.content.Context context, com.misfit.frameworks.buttonservice.p003db.DataFile dataFile) {
        if (dataFile != null) {
            com.misfit.frameworks.buttonservice.p003db.HeartRateProvider.getInstance(context).saveDataFile(dataFile);
        }
    }

    @DexIgnore
    public static void saveHwLog(android.content.Context context, com.misfit.frameworks.buttonservice.p003db.HardwareLog hardwareLog) {
        java.util.concurrent.Executors.newSingleThreadExecutor().execute(new com.misfit.frameworks.buttonservice.p003db.DataCollectorHelper.C39581(hardwareLog, context));
    }
}
