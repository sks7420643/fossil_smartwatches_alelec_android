package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DatabaseHelper */
public class DatabaseHelper extends com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper {
    @DexIgnore
    public /* final */ com.j256.ormlite.logger.Logger LOG; // = com.j256.ormlite.logger.LoggerFactory.getLogger((java.lang.Class<?>) com.misfit.frameworks.buttonservice.p003db.DatabaseHelper.class);
    @DexIgnore
    public /* final */ java.lang.String TAG; // = com.misfit.frameworks.buttonservice.p003db.DatabaseHelper.class.getCanonicalName();
    @DexIgnore
    public java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> availableUpgrades;
    @DexIgnore
    public java.lang.String dbName;
    @DexIgnore
    public java.lang.Class<?>[] entities;

    @DexIgnore
    public DatabaseHelper(android.content.Context context, java.lang.String str, int i, java.lang.Class<?>[] clsArr, java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> map) {
        super(context, str, (android.database.sqlite.SQLiteDatabase.CursorFactory) null, i);
        this.dbName = str;
        this.entities = clsArr;
        this.availableUpgrades = map;
    }

    @DexIgnore
    private void copyFile(java.io.File file, java.io.File file2) throws java.io.IOException {
        java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
        java.nio.channels.FileChannel channel = fileInputStream.getChannel();
        java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(file2);
        java.nio.channels.FileChannel channel2 = fileOutputStream.getChannel();
        try {
            channel.transferTo(0, channel.size(), channel2);
        } finally {
            if (channel != null) {
                channel.close();
            }
            fileInputStream.close();
            if (channel2 != null) {
                channel2.close();
            }
            fileOutputStream.close();
        }
    }

    @DexIgnore
    private void createAllTables() throws java.sql.SQLException {
        createTables(this.entities);
    }

    @DexIgnore
    private void dropAllTables() throws java.sql.SQLException {
        dropTables(this.entities);
    }

    @DexIgnore
    public void close() {
        super.close();
    }

    @DexIgnore
    public void createTable(java.lang.Class<?> cls) throws java.sql.SQLException {
        com.j256.ormlite.table.TableUtils.createTable(getConnectionSource(), cls);
    }

    @DexIgnore
    public void createTables(java.lang.Class<?>[] clsArr) throws java.sql.SQLException {
        for (java.lang.Class<?> createTable : clsArr) {
            com.j256.ormlite.table.TableUtils.createTable(getConnectionSource(), createTable);
        }
    }

    @DexIgnore
    public void dropTable(java.lang.Class<?> cls) throws java.sql.SQLException {
        dropTable(getConnectionSource(), cls);
    }

    @DexIgnore
    public void dropTables(java.lang.Class<?>[] clsArr) throws java.sql.SQLException {
        for (java.lang.Class<?> dropTable : clsArr) {
            dropTable(getConnectionSource(), dropTable);
        }
    }

    @DexIgnore
    public void exportDB(android.content.Context context) {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.TAG, "exporting DB to file");
        java.io.File databasePath = context.getDatabasePath(getDatabaseName());
        java.io.File file = new java.io.File(android.os.Environment.getExternalStorageDirectory(), "nrml");
        if (!file.exists()) {
            file.mkdirs();
        }
        java.io.File file2 = new java.io.File(file, databasePath.getName());
        try {
            file2.createNewFile();
            copyFile(databasePath, file2);
        } catch (java.io.IOException e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33256e(str, "Unable to Export DB: " + e.getMessage());
        }
    }

    @DexIgnore
    public void onCreate(android.database.sqlite.SQLiteDatabase sQLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource) {
        try {
            createAllTables();
        } catch (java.sql.SQLException e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33256e(str, "Can't create database - e=" + e);
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    public void onUpgrade(android.database.sqlite.SQLiteDatabase sQLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource, int i, int i2) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = this.TAG;
        local.mo33258i(str, "onUpgrade, oldVersion=" + i + " newVersion=" + i2);
        try {
            upgrade(sQLiteDatabase, i, i2);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33258i(this.TAG, "successful upgrade!");
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local2.mo33256e(str2, "Can't migrate databases, bootstrap database, data will be lost - e=" + e);
            try {
                dropAllTables();
                onCreate(sQLiteDatabase, connectionSource);
            } catch (java.sql.SQLException e2) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str3 = this.TAG;
                local3.mo33256e(str3, "Can't drop databases - e=" + e2);
                throw new java.lang.RuntimeException(e);
            }
        }
    }

    @DexIgnore
    public void upgrade(android.database.sqlite.SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (this.availableUpgrades != null) {
            while (true) {
                i++;
                if (i <= i2) {
                    com.misfit.frameworks.buttonservice.p003db.UpgradeCommand upgradeCommand = this.availableUpgrades.get(java.lang.Integer.valueOf(i));
                    if (upgradeCommand != null) {
                        sQLiteDatabase.beginTransaction();
                        try {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String str = this.TAG;
                            local.mo33255d(str, "Upgrading database to: " + i);
                            upgradeCommand.execute(sQLiteDatabase);
                            sQLiteDatabase.setTransactionSuccessful();
                        } finally {
                            sQLiteDatabase.endTransaction();
                        }
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new java.lang.RuntimeException("No upgrade commands provided");
        }
    }

    @DexIgnore
    private void dropTable(com.j256.ormlite.support.ConnectionSource connectionSource, java.lang.Class<?> cls) {
        try {
            com.j256.ormlite.table.TableUtils.dropTable(connectionSource, cls, true);
        } catch (java.sql.SQLException unused) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33256e(str, "Could not drop table: " + cls.getSimpleName());
        }
    }
}
