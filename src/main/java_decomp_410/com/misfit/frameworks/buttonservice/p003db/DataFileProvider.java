package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DataFileProvider */
public class DataFileProvider extends com.misfit.frameworks.buttonservice.p003db.BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ java.lang.String DB_NAME; // = "data_file.db";
    @DexIgnore
    public static com.misfit.frameworks.buttonservice.p003db.DataFileProvider sInstance;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.DataFileProvider$1")
    /* renamed from: com.misfit.frameworks.buttonservice.db.DataFileProvider$1 */
    public class C39591 extends java.util.HashMap<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.DataFileProvider$1$1")
        /* renamed from: com.misfit.frameworks.buttonservice.db.DataFileProvider$1$1 */
        public class C39601 implements com.misfit.frameworks.buttonservice.p003db.UpgradeCommand {
            @DexIgnore
            public C39601() {
            }

            @DexIgnore
            public void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE datafile ADD COLUMN syncTime BIGINT");
            }
        }

        @DexIgnore
        public C39591() {
            put(2, new com.misfit.frameworks.buttonservice.p003db.DataFileProvider.C39591.C39601());
        }
    }

    @DexIgnore
    public DataFileProvider(android.content.Context context, java.lang.String str) {
        super(context, str);
    }

    @DexIgnore
    private com.j256.ormlite.dao.Dao<com.misfit.frameworks.buttonservice.p003db.DataFile, java.lang.String> getDataFileDao() throws java.sql.SQLException {
        return this.databaseHelper.getDao(com.misfit.frameworks.buttonservice.p003db.DataFile.class);
    }

    @DexIgnore
    public static synchronized com.misfit.frameworks.buttonservice.p003db.DataFileProvider getInstance(android.content.Context context) {
        com.misfit.frameworks.buttonservice.p003db.DataFileProvider dataFileProvider;
        synchronized (com.misfit.frameworks.buttonservice.p003db.DataFileProvider.class) {
            if (sInstance == null) {
                sInstance = new com.misfit.frameworks.buttonservice.p003db.DataFileProvider(context, DB_NAME);
            }
            dataFileProvider = sInstance;
        }
        return dataFileProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33258i(str, "Inside " + this.TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local2.mo33256e(str2, "Error inside " + this.TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public int deleteDataFiles(java.lang.String str) {
        try {
            com.j256.ormlite.stmt.DeleteBuilder<com.misfit.frameworks.buttonservice.p003db.DataFile, java.lang.String> deleteBuilder = getDataFileDao().deleteBuilder();
            deleteBuilder.where().mo24468eq("serial", str);
            return deleteBuilder.delete();
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local.mo33256e(str2, "Error inside " + this.TAG + ".deleteDataFile - e=" + e);
            return 0;
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.p003db.DataFile> getAllDataFiles(long j, java.lang.String str) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            java.lang.String str2 = "SELECT key FROM dataFile WHERE serial = '" + str + "' AND " + "syncTime" + " <= " + j;
            com.j256.ormlite.dao.GenericRawResults<java.lang.String[]> queryRaw = getDataFileDao().queryRaw(str2, new java.lang.String[0]);
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.TAG, str2);
            java.util.ArrayList arrayList2 = new java.util.ArrayList();
            for (java.lang.String[] next : queryRaw) {
                if (next.length > 0) {
                    arrayList2.add(next[0]);
                }
            }
            queryRaw.close();
            if (arrayList2.size() > 0) {
                java.util.Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    arrayList.add(getDataFile((java.lang.String) it.next()));
                }
            }
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(this.TAG, "Error inside " + this.TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012a, code lost:
        if (r3 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x012c, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x015a, code lost:
        if (r3 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x015d, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0160  */
    public com.misfit.frameworks.buttonservice.p003db.DataFile getDataFile(java.lang.String str, java.lang.String str2) {
        com.misfit.frameworks.buttonservice.p003db.DataFile dataFile;
        long j;
        java.lang.String str3;
        int i;
        java.lang.String str4 = str;
        android.database.sqlite.SQLiteDatabase readableDatabase = this.databaseHelper.getReadableDatabase();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("SELECT length(dataFile), serial, syncTime FROM dataFile WHERE `key` = ?");
        sb.append(str2 == null ? "" : " AND serial = ?");
        android.database.Cursor rawQuery = readableDatabase.rawQuery(sb.toString(), str2 == null ? new java.lang.String[]{str4} : new java.lang.String[]{str4, str2});
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.DataFile, java.lang.String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().mo24468eq("key", str4);
            if (rawQuery == null || rawQuery.getCount() <= 0) {
                str3 = "";
                i = 0;
                j = 0;
            } else {
                rawQuery.moveToFirst();
                i = rawQuery.getInt(0);
                str3 = rawQuery.getString(1);
                j = rawQuery.getLong(2);
                rawQuery.close();
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str5 = this.TAG;
            local.mo33255d(str5, "Inside .getDataFile - fileData len=" + i);
            if (i > 1000000) {
                java.lang.StringBuffer stringBuffer = new java.lang.StringBuffer();
                android.database.Cursor cursor = rawQuery;
                long j2 = 0;
                while (j2 < ((long) i)) {
                    long j3 = j2 + 1;
                    try {
                        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
                        sb2.append("SELECT substr(dataFile, ?, ? ) FROM dataFile WHERE `key` = ?");
                        sb2.append(str2 == null ? "" : " AND serial = ?");
                        cursor = readableDatabase.rawQuery(sb2.toString(), str2 == null ? new java.lang.String[]{java.lang.Long.toString(j3), java.lang.Long.toString(1000000), str4} : new java.lang.String[]{java.lang.Long.toString(j3), java.lang.Long.toString(1000000), str4, str2});
                        if (cursor != null && cursor.getCount() > 0) {
                            cursor.moveToFirst();
                            java.lang.String string = cursor.getString(0);
                            stringBuffer.append(string);
                            j2 += (long) string.length();
                        }
                    } catch (java.lang.Exception e) {
                        e = e;
                        rawQuery = cursor;
                        dataFile = null;
                        try {
                            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                            java.lang.String str6 = this.TAG;
                            local2.mo33256e(str6, "Error inside " + this.TAG + ".getDataFile - e=" + e);
                        } catch (Throwable th) {
                            th = th;
                            if (rawQuery != null) {
                                rawQuery.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        rawQuery = cursor;
                        if (rawQuery != null) {
                        }
                        throw th;
                    }
                }
                com.misfit.frameworks.buttonservice.p003db.DataFile dataFile2 = new com.misfit.frameworks.buttonservice.p003db.DataFile(str, stringBuffer.toString(), str3, j);
                rawQuery = cursor;
                dataFile = dataFile2;
            } else {
                dataFile = getDataFileDao().queryForFirst(queryBuilder.prepare());
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (java.lang.Exception e2) {
                    e = e2;
                }
            }
        } catch (java.lang.Exception e3) {
            e = e3;
            dataFile = null;
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local22 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str62 = this.TAG;
            local22.mo33256e(str62, "Error inside " + this.TAG + ".getDataFile - e=" + e);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public java.lang.Class<?>[] getDbEntities() {
        return new java.lang.Class[]{com.misfit.frameworks.buttonservice.p003db.DataFile.class};
    }

    @DexIgnore
    public java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> getDbUpgrades() {
        return new com.misfit.frameworks.buttonservice.p003db.DataFileProvider.C39591();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public void saveDataFile(com.misfit.frameworks.buttonservice.p003db.DataFile dataFile) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = this.TAG;
        local.mo33255d(str, "Inside " + this.TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str2 = this.TAG;
                local2.mo33255d(str2, "Inside " + this.TAG + ".saveDataFile - Save dataFile=" + dataFile);
                getDataFileDao().create(dataFile);
                return;
            }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str3 = this.TAG;
            local3.mo33256e(str3, "Error inside " + this.TAG + ".saveDataFile -e=Data file existed");
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str4 = this.TAG;
            local4.mo33256e(str4, "Error inside " + this.TAG + ".saveDataFile - e=" + e);
        }
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.p003db.DataFile getDataFile(java.lang.String str) {
        return getDataFile(str, (java.lang.String) null);
    }
}
