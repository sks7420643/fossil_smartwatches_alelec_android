package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.DataLogServiceProvider */
public class DataLogServiceProvider extends com.misfit.frameworks.buttonservice.p003db.BaseDbProvider {
    @DexIgnore
    public static /* final */ java.lang.String DB_NAME; // = "log_service.db";
    @DexIgnore
    public static /* final */ int KEY_NOTIFICATION_DEBUGLOG_ID; // = 999;
    @DexIgnore
    public static /* final */ java.lang.String TAG; // = "DataLogServiceProvider";
    @DexIgnore
    public static com.misfit.frameworks.buttonservice.p003db.DataLogServiceProvider sInstance;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.DataLogServiceProvider$1")
    /* renamed from: com.misfit.frameworks.buttonservice.db.DataLogServiceProvider$1 */
    public class C39611 extends java.util.HashMap<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> {
        @DexIgnore
        public C39611() {
        }
    }

    @DexIgnore
    public DataLogServiceProvider(android.content.Context context, java.lang.String str) {
        super(context, str);
    }

    @DexIgnore
    private com.j256.ormlite.dao.Dao<com.misfit.frameworks.buttonservice.p003db.DataLogService, java.lang.Integer> getDataLogServicesDAO() throws java.sql.SQLException {
        return this.databaseHelper.getDao(com.misfit.frameworks.buttonservice.p003db.DataLogService.class);
    }

    @DexIgnore
    public static com.misfit.frameworks.buttonservice.p003db.DataLogServiceProvider getInstance(android.content.Context context) {
        if (sInstance == null) {
            sInstance = new com.misfit.frameworks.buttonservice.p003db.DataLogServiceProvider(context, DB_NAME);
        }
        return sInstance;
    }

    @DexIgnore
    public synchronized void addNotificationDebugLog(java.lang.String str) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str2 = TAG;
        local.mo33255d(str2, "addNotificationDebugLog - content=" + str);
        com.misfit.frameworks.buttonservice.p003db.DataLogService dataLogServiceById = getDataLogServiceById(999);
        if (dataLogServiceById != null) {
            try {
                dataLogServiceById.setContent(dataLogServiceById.getContent() + str);
                getDataLogServicesDAO().update(dataLogServiceById);
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str3 = TAG;
                local2.mo33256e(str3, "addNotificationDebugLog - e=" + e);
            }
        } else {
            com.misfit.frameworks.buttonservice.p003db.DataLogService dataLogService = new com.misfit.frameworks.buttonservice.p003db.DataLogService();
            dataLogService.setContent(str);
            dataLogService.setId(999);
            getDataLogServicesDAO().create(dataLogService);
        }
        return;
    }

    @DexIgnore
    public synchronized void createOrUpdate(com.misfit.frameworks.buttonservice.p003db.DataLogService dataLogService) {
        if (dataLogService == null) {
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(TAG, "createOrUpdate - dataLogService=null");
            return;
        }
        int id = dataLogService.getId();
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "createOrUpdate - logId=" + id);
        com.misfit.frameworks.buttonservice.p003db.DataLogService dataLogServiceById = getDataLogServiceById(id);
        try {
            if (dataLogService.getContent() == null || dataLogService.getContent().getBytes().length <= 819200) {
                if (dataLogServiceById != null) {
                    getDataLogServicesDAO().update(dataLogService);
                } else {
                    getDataLogServicesDAO().create(dataLogService);
                }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str2 = TAG;
                local2.mo33255d(str2, "createOrUpdate - logId=" + id + ", currentTimeMillis=" + java.lang.System.currentTimeMillis());
                return;
            }
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33256e(TAG, "createOrUpdate - content data log > 800Kb");
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str3 = TAG;
            local3.mo33256e(str3, "createOrUpdate - logId=" + id + ", e=" + e);
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.p003db.DataLogService> getAllDataLogServiceByStatus(int i) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "getAllDataLogServiceByStatus - status=" + i);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.DataLogService, java.lang.Integer> queryBuilder = getDataLogServicesDAO().queryBuilder();
            queryBuilder.where().mo24468eq("status", java.lang.Integer.valueOf(i));
            return queryBuilder.query();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = TAG;
            local2.mo33256e(str2, "getAllDataLogServiceByStatus - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.p003db.DataLogService> getAllDataLogServiceByStatusAndLogStyle(int i, int i2) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "getAllDataLogServiceByStatusAndLogStyle - status=" + i + ", logStyle=" + i2);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.DataLogService, java.lang.Integer> queryBuilder = getDataLogServicesDAO().queryBuilder();
            queryBuilder.where().mo24468eq("status", java.lang.Integer.valueOf(i)).and().mo24468eq(com.misfit.frameworks.buttonservice.p003db.DataLogService.COLUMN_LOG_STYLE, java.lang.Integer.valueOf(i2));
            return queryBuilder.query();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = TAG;
            local2.mo33256e(str2, "getAllDataLogServiceByStatusAndLogStyle - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.p003db.DataLogService getDataLogServiceById(int i) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "getDataLogServiceById - id=" + i);
        try {
            return getDataLogServicesDAO().queryBuilder().where().mo24468eq("id", java.lang.Integer.valueOf(i)).queryForFirst();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = TAG;
            local2.mo33256e(str2, "getDataLogServiceById - e=" + e);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public java.lang.Class<?>[] getDbEntities() {
        return new java.lang.Class[]{com.misfit.frameworks.buttonservice.p003db.DataLogService.class};
    }

    @DexIgnore
    public java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> getDbUpgrades() {
        return new com.misfit.frameworks.buttonservice.p003db.DataLogServiceProvider.C39611();
    }

    @DexIgnore
    public int getDbVersion() {
        return 3;
    }

    @DexIgnore
    public void remove(com.misfit.frameworks.buttonservice.p003db.DataLogService dataLogService) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "remove - dataLogService=" + dataLogService);
        if (dataLogService != null) {
            try {
                com.j256.ormlite.stmt.DeleteBuilder<com.misfit.frameworks.buttonservice.p003db.DataLogService, java.lang.Integer> deleteBuilder = getDataLogServicesDAO().deleteBuilder();
                deleteBuilder.where().mo24468eq("id", java.lang.Integer.valueOf(dataLogService.getId()));
                deleteBuilder.delete();
            } catch (java.lang.Exception e) {
                e.printStackTrace();
                com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                java.lang.String str2 = TAG;
                local2.mo33256e(str2, "remove - e=" + e);
            }
        }
    }

    @DexIgnore
    public void removeById(int i) {
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String str = TAG;
        local.mo33255d(str, "removeById - id=" + i);
        try {
            com.j256.ormlite.stmt.DeleteBuilder<com.misfit.frameworks.buttonservice.p003db.DataLogService, java.lang.Integer> deleteBuilder = getDataLogServicesDAO().deleteBuilder();
            deleteBuilder.where().mo24468eq("id", java.lang.Integer.valueOf(i));
            deleteBuilder.delete();
        } catch (java.lang.Exception e) {
            e.printStackTrace();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = TAG;
            local2.mo33256e(str2, "removeById - e=" + e);
        }
    }
}
