package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.HwLogProvider */
public class HwLogProvider extends com.misfit.frameworks.buttonservice.p003db.BaseDbProvider {
    @DexIgnore
    public static /* final */ java.lang.String DB_NAME; // = "hw_log.db";
    @DexIgnore
    public static com.misfit.frameworks.buttonservice.p003db.HwLogProvider sInstance;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.HwLogProvider$1")
    /* renamed from: com.misfit.frameworks.buttonservice.db.HwLogProvider$1 */
    public class C39621 extends java.util.HashMap<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.db.HwLogProvider$1$1")
        /* renamed from: com.misfit.frameworks.buttonservice.db.HwLogProvider$1$1 */
        public class C39631 implements com.misfit.frameworks.buttonservice.p003db.UpgradeCommand {
            @DexIgnore
            public C39631() {
            }

            @DexIgnore
            public void execute(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE hardwarelog ADD COLUMN read INTEGER");
            }
        }

        @DexIgnore
        public C39621() {
            put(2, new com.misfit.frameworks.buttonservice.p003db.HwLogProvider.C39621.C39631());
        }
    }

    @DexIgnore
    public HwLogProvider(android.content.Context context, java.lang.String str) {
        super(context, str);
    }

    @DexIgnore
    private com.j256.ormlite.dao.Dao<com.misfit.frameworks.buttonservice.p003db.HardwareLog, java.lang.String> getHardwareLogDao() throws java.sql.SQLException {
        return this.databaseHelper.getDao(com.misfit.frameworks.buttonservice.p003db.HardwareLog.class);
    }

    @DexIgnore
    public static synchronized com.misfit.frameworks.buttonservice.p003db.HwLogProvider getInstance(android.content.Context context) {
        com.misfit.frameworks.buttonservice.p003db.HwLogProvider hwLogProvider;
        synchronized (com.misfit.frameworks.buttonservice.p003db.HwLogProvider.class) {
            if (sInstance == null) {
                sInstance = new com.misfit.frameworks.buttonservice.p003db.HwLogProvider(context, DB_NAME);
            }
            hwLogProvider = sInstance;
        }
        return hwLogProvider;
    }

    @DexIgnore
    public void clearHwLog() {
        try {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33258i(str, "Inside " + this.TAG + ".clearHwLog");
            getHardwareLogDao().deleteBuilder().delete();
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local2.mo33256e(str2, "Error inside " + this.TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.p003db.HardwareLog> getAllHardwareLogs() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.HardwareLog, java.lang.String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().mo24468eq(com.misfit.frameworks.buttonservice.p003db.HardwareLog.COLUMN_READ, false);
            java.util.List<com.misfit.frameworks.buttonservice.p003db.HardwareLog> query = getHardwareLogDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33256e(str, "Error inside " + this.TAG + ".getAllHardwareLogs - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public java.lang.Class<?>[] getDbEntities() {
        return new java.lang.Class[]{com.misfit.frameworks.buttonservice.p003db.HardwareLog.class};
    }

    @DexIgnore
    public java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> getDbUpgrades() {
        return new com.misfit.frameworks.buttonservice.p003db.HwLogProvider.C39621();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.p003db.HardwareLog getHardwareLog(java.lang.String str) {
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.HardwareLog, java.lang.String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().mo24468eq("serial", str);
            return getHardwareLogDao().queryForFirst(queryBuilder.prepare());
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local.mo33256e(str2, "Error inside " + this.TAG + ".getHardwareLog - =e" + e);
            return null;
        }
    }

    @DexIgnore
    public void saveHwLog(com.misfit.frameworks.buttonservice.p003db.HardwareLog hardwareLog) {
        try {
            getHardwareLogDao().create(hardwareLog);
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33256e(str, "Error inside " + this.TAG + ".saveHwLog - e=" + e);
        }
    }

    @DexIgnore
    public void setHardwareLogRead() {
        try {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str = this.TAG;
            local.mo33258i(str, "Inside " + this.TAG + ".setHardwareLogRead");
            com.j256.ormlite.stmt.UpdateBuilder<com.misfit.frameworks.buttonservice.p003db.HardwareLog, java.lang.String> updateBuilder = getHardwareLogDao().updateBuilder();
            updateBuilder.updateColumnValue(com.misfit.frameworks.buttonservice.p003db.HardwareLog.COLUMN_READ, true);
            updateBuilder.update();
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local2.mo33256e(str2, "Error inside " + this.TAG + ".setHardwareLogRead - e=" + e);
        }
    }

    @DexIgnore
    public java.util.List<com.misfit.frameworks.buttonservice.p003db.HardwareLog> getAllHardwareLogs(java.lang.String str) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            com.j256.ormlite.stmt.QueryBuilder<com.misfit.frameworks.buttonservice.p003db.HardwareLog, java.lang.String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().mo24483lt("serial", str);
            java.util.List<com.misfit.frameworks.buttonservice.p003db.HardwareLog> query = getHardwareLogDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (java.lang.Exception e) {
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String str2 = this.TAG;
            local.mo33256e(str2, "Error inside " + this.TAG + ".getAllHardwareLogs - e=" + e);
        }
        return arrayList;
    }
}
