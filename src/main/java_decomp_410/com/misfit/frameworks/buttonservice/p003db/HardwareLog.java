package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.HardwareLog */
public class HardwareLog implements java.io.Serializable {
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_LOG; // = "log";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_READ; // = "read";
    @DexIgnore
    public static /* final */ java.lang.String COLUMN_SERIAL; // = "serial";
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "date")
    public java.util.Date date;
    @com.j256.ormlite.field.DatabaseField(columnName = "id", generatedId = true)

    @DexIgnore
    /* renamed from: id */
    public long f13228id;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "log")
    public java.lang.String log;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "read")
    public boolean read;
    @DexIgnore
    @com.j256.ormlite.field.DatabaseField(columnName = "serial")
    public java.lang.String serial;

    @DexIgnore
    public java.util.Date getDate() {
        return this.date;
    }

    @DexIgnore
    public long getId() {
        return this.f13228id;
    }

    @DexIgnore
    public java.lang.String getLog() {
        return this.log;
    }

    @DexIgnore
    public java.lang.String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public boolean isRead() {
        return this.read;
    }

    @DexIgnore
    public void setDate(java.util.Date date2) {
        this.date = date2;
    }

    @DexIgnore
    public void setId(long j) {
        this.f13228id = j;
    }

    @DexIgnore
    public void setLog(java.lang.String str) {
        this.log = str;
    }

    @DexIgnore
    public void setRead(boolean z) {
        this.read = z;
    }

    @DexIgnore
    public void setSerial(java.lang.String str) {
        this.serial = str;
    }
}
