package com.misfit.frameworks.buttonservice.p003db;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.db.BaseDbProvider */
public abstract class BaseDbProvider {
    @DexIgnore
    public /* final */ java.lang.String TAG; // = getClass().getCanonicalName();
    @DexIgnore
    public android.content.Context context;
    @DexIgnore
    public com.misfit.frameworks.buttonservice.p003db.DatabaseHelper databaseHelper;
    @DexIgnore
    public boolean isCacheDirty; // = true;

    @DexIgnore
    public BaseDbProvider(android.content.Context context2, java.lang.String str) {
        this.context = context2;
        com.misfit.frameworks.buttonservice.p003db.DatabaseHelper databaseHelper2 = new com.misfit.frameworks.buttonservice.p003db.DatabaseHelper(context2, str, getDbVersion(), getDbEntities(), getDbUpgrades());
        this.databaseHelper = databaseHelper2;
    }

    @DexIgnore
    public abstract java.lang.Class<?>[] getDbEntities();

    @DexIgnore
    public abstract java.util.Map<java.lang.Integer, com.misfit.frameworks.buttonservice.p003db.UpgradeCommand> getDbUpgrades();

    @DexIgnore
    public abstract int getDbVersion();

    @DexIgnore
    public void setCacheToDirty() {
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().mo33255d(this.TAG, "Setting cache to dirty");
        this.isCacheDirty = true;
    }

    @DexIgnore
    public BaseDbProvider(android.content.Context context2, java.lang.String str, java.lang.String str2) {
        com.misfit.frameworks.buttonservice.p003db.DatabaseHelper databaseHelper2 = new com.misfit.frameworks.buttonservice.p003db.DatabaseHelper(context2, str + "_" + str2, getDbVersion(), getDbEntities(), getDbUpgrades());
        this.databaseHelper = databaseHelper2;
    }
}
