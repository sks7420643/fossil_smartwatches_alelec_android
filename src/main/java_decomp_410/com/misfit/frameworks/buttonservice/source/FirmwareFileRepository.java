package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.bp4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FirmwareFileRepository implements FirmwareFileSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static FirmwareFileRepository INSTANCE;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public String applicationFileDir;
    @DexIgnore
    public /* final */ FirmwareFileLocalSource mLocalFirmwareFileSource;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final FirmwareFileRepository getInstance(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
            kd4.b(context, "context");
            kd4.b(firmwareFileLocalSource, "firmwareFileLocalSource");
            FirmwareFileRepository access$getINSTANCE$cp = FirmwareFileRepository.INSTANCE;
            return access$getINSTANCE$cp != null ? access$getINSTANCE$cp : new FirmwareFileRepository(context, firmwareFileLocalSource);
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public FirmwareFileRepository(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
        kd4.b(context, "applicationContext");
        kd4.b(firmwareFileLocalSource, "mLocalFirmwareFileSource");
        this.mLocalFirmwareFileSource = firmwareFileLocalSource;
        String name = FirmwareFileRepository.class.getName();
        kd4.a((Object) name, "FirmwareFileRepository::class.java.name");
        this.TAG = name;
        String file = context.getFilesDir().toString();
        kd4.a((Object) file, "applicationContext.filesDir.toString()");
        this.applicationFileDir = file;
    }

    @DexIgnore
    public static /* synthetic */ Object downloadFirmware$suspendImpl(FirmwareFileRepository firmwareFileRepository, String str, String str2, String str3, yb4 yb4) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        File file = null;
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str4 = firmwareFileRepository.TAG;
            local.d(str4, "downloadFirmware() failed - fileUrl=" + str2 + ", checkSum=" + str3);
        } else {
            String firmwareFilePath = firmwareFileRepository.getFirmwareFilePath(str);
            try {
                if (!firmwareFileRepository.mLocalFirmwareFileSource.verify(firmwareFilePath, str3)) {
                    BufferedInputStream openConnectURL$buttonservice_release = firmwareFileRepository.openConnectURL$buttonservice_release(str2);
                    if (openConnectURL$buttonservice_release != null) {
                        z2 = firmwareFileRepository.mLocalFirmwareFileSource.saveFile(openConnectURL$buttonservice_release, firmwareFilePath);
                        openConnectURL$buttonservice_release.close();
                    } else {
                        z2 = false;
                    }
                    if (z2) {
                        z = firmwareFileRepository.mLocalFirmwareFileSource.verify(firmwareFilePath, str3);
                    }
                    z = false;
                } else {
                    z = true;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str5 = firmwareFileRepository.TAG;
                local2.e(str5, "downloadFirmware() - e=" + e);
            }
            if (!z) {
                firmwareFileRepository.mLocalFirmwareFileSource.deleteFile(firmwareFilePath);
            } else {
                file = firmwareFileRepository.mLocalFirmwareFileSource.getFile(firmwareFilePath);
            }
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str6 = firmwareFileRepository.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("downloadFirmware() - firmwareVersion=");
        sb.append(str);
        sb.append(", fileUrl=");
        sb.append(str2);
        sb.append(", checkSum=");
        sb.append(str3);
        sb.append(", download success =");
        if (file == null) {
            z3 = false;
        }
        sb.append(z3);
        local3.d(str6, sb.toString());
        return file;
    }

    @DexIgnore
    public Object downloadFirmware(String str, String str2, String str3, yb4<? super File> yb4) {
        return downloadFirmware$suspendImpl(this, str, str2, str3, yb4);
    }

    @DexIgnore
    public String getFirmwareFilePath(String str) {
        kd4.b(str, "firmwareVersion");
        return this.applicationFileDir + ZendeskConfig.SLASH + bp4.a(str);
    }

    @DexIgnore
    public boolean isDownloaded(String str, String str2) {
        kd4.b(str, "firmwareVersion");
        kd4.b(str2, "checkSum");
        String firmwareFilePath = getFirmwareFilePath(str);
        if (!(str2.length() == 0)) {
            return this.mLocalFirmwareFileSource.verify(firmwareFilePath, str2);
        }
        return this.mLocalFirmwareFileSource.getFile(firmwareFilePath) != null;
    }

    @DexIgnore
    public BufferedInputStream openConnectURL$buttonservice_release(String str) {
        kd4.b(str, "fileUrl");
        try {
            URL url = new URL(str);
            URLConnection openConnection = url.openConnection();
            openConnection.connect();
            kd4.a((Object) openConnection, "connection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, "openConnectURL(), filePath=" + url + ", size=" + ((long) openConnection.getContentLength()));
            return new BufferedInputStream(openConnection.getInputStream());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, "openConnectURL(), ex=" + e);
            return null;
        }
    }

    @DexIgnore
    public byte[] readFirmware(String str) {
        kd4.b(str, "firmwareVersion");
        FileInputStream readFile = this.mLocalFirmwareFileSource.readFile(getFirmwareFilePath(str));
        byte[] bArr = null;
        if (readFile != null) {
            try {
                int size = (int) readFile.getChannel().size();
                byte[] bArr2 = new byte[size];
                int read = readFile.read(bArr2);
                if (read != size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.TAG;
                    local.e(str2, "getOtaData() - expectedSize=" + size + ", readSize=" + read);
                } else {
                    bArr = bArr2;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = this.TAG;
                local2.e(str3, "getOtaData() - e=" + e);
            } catch (Throwable th) {
                readFile.close();
                throw th;
            }
            readFile.close();
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("readFirmware() - firmwareVersion=");
        sb.append(str);
        sb.append(", dataExist=");
        sb.append(bArr != null);
        local3.d(str4, sb.toString());
        return bArr;
    }
}
