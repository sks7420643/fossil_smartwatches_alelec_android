package com.misfit.frameworks.buttonservice.source;

import com.fossil.blesdk.obfuscated.yb4;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface FirmwareFileSource {
    @DexIgnore
    Object downloadFirmware(String str, String str2, String str3, yb4<? super File> yb4);

    @DexIgnore
    String getFirmwareFilePath(String str);

    @DexIgnore
    boolean isDownloaded(String str, String str2);

    @DexIgnore
    byte[] readFirmware(String str);
}
