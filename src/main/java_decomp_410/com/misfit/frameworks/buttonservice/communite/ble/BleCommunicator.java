package com.misfit.frameworks.buttonservice.communite.ble;

import android.content.Context;
import android.os.Bundle;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.f40;
import com.fossil.blesdk.obfuscated.i40;
import com.fossil.blesdk.obfuscated.j40;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l40;
import com.fossil.blesdk.obfuscated.p40;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.pairing.PairingLinkServerResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingUpdateFWResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BleCommunicator {
    @DexIgnore
    public /* final */ int NOTIFICATION_THRESHOLD; // = 20;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public BleSession.BleSessionCallback bleSessionCallback; // = new BleCommunicator$bleSessionCallback$Anon1(this);
    @DexIgnore
    public /* final */ CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ HandlerThread handlerThread; // = new HandlerThread(this.TAG);
    @DexIgnore
    public PriorityBlockingQueue<BleSession> highSessionQueue;
    @DexIgnore
    public PriorityBlockingQueue<BleSession> lowSessionQueue;
    @DexIgnore
    public /* final */ ArrayList<DianaNotificationObj> mNotificationQueue; // = new ArrayList<>();
    @DexIgnore
    public /* final */ String serial;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo(bleSession.getSessionType());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon2 INSTANCE; // = new Anon2();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo(bleSession.getSessionType());
        }
    }

    @DexIgnore
    public interface CommunicationResultCallback {
        @DexIgnore
        void onAskForCurrentSecretKey(String str);

        @DexIgnore
        void onAskForLinkServer(String str, CommunicateMode communicateMode, Bundle bundle);

        @DexIgnore
        void onAskForRandomKey(String str);

        @DexIgnore
        void onAskForServerSecretKey(String str, Bundle bundle);

        @DexIgnore
        void onAskForStopWorkout(String str);

        @DexIgnore
        void onCommunicatorResult(CommunicateMode communicateMode, String str, int i, List<Integer> list, Bundle bundle);

        @DexIgnore
        void onDeviceAppsRequest(int i, Bundle bundle, String str);

        @DexIgnore
        void onExchangeSecretKeySuccess(String str, String str2);

        @DexIgnore
        void onFirmwareLatest(String str);

        @DexIgnore
        void onGattConnectionStateChanged(String str, int i);

        @DexIgnore
        void onHeartBeatDataReceived(int i, int i2, String str);

        @DexIgnore
        void onHeartRateNotification(short s, String str);

        @DexIgnore
        void onHidConnectionStateChanged(String str, int i);

        @DexIgnore
        void onOtaProgressUpdated(String str, float f);

        @DexIgnore
        void onPreparationCompleted(boolean z, String str);

        @DexIgnore
        void onReceivedSyncData(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestFirmware(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestWatchParams(String str, Bundle bundle);

        @DexIgnore
        void onUpdateFirmwareFailed(String str);

        @DexIgnore
        void onUpdateFirmwareSuccess(String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[SessionType.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[SessionType.SPECIAL.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[SessionType.SYNC.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[SessionType.CONNECT.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[SessionType.UI.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[SessionType.DEVICE_SETTING.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[SessionType.URGENT.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public BleCommunicator(String str, CommunicationResultCallback communicationResultCallback2) {
        kd4.b(str, "serial");
        kd4.b(communicationResultCallback2, "communicationResultCallback");
        this.serial = str;
        this.communicationResultCallback = communicationResultCallback2;
        String simpleName = BleCommunicator.class.getSimpleName();
        kd4.a((Object) simpleName, "BleCommunicator::class.java.simpleName");
        this.TAG = simpleName;
        this.handlerThread.start();
        this.highSessionQueue = new PriorityBlockingQueue<>(11, Anon1.INSTANCE);
        this.lowSessionQueue = new PriorityBlockingQueue<>(11, Anon2.INSTANCE);
    }

    @DexIgnore
    public static /* synthetic */ void startSendDeviceAppResponse$default(BleCommunicator bleCommunicator, DeviceAppResponse deviceAppResponse, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z = false;
            }
            bleCommunicator.startSendDeviceAppResponse(deviceAppResponse, z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startSendDeviceAppResponse");
    }

    @DexIgnore
    public final synchronized void cancelCalibrationSession() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".cancelCalibrationSession() - current session=" + getCurrentSession());
        if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof ICalibrationSession)) {
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                ((ICalibrationSession) currentSession).handleReleaseHandControl();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession");
            }
        }
        ArrayList arrayList = new ArrayList();
        printQueue();
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            BleSession next = it.next();
            if (next instanceof ICalibrationSession) {
                arrayList.add(next);
                next.stop(0);
            }
        }
        this.highSessionQueue.removeAll(arrayList);
    }

    @DexIgnore
    public final void cancelPairDevice() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".cancelPairDevice() - current session=" + getCurrentSession());
        if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof IPairDeviceSession)) {
            FLogger.INSTANCE.getRemote().d(FLogger.Component.BLE, FLogger.Session.PAIR, this.serial, this.TAG, "App called cancel Pair Device.");
            getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
        }
        ArrayList arrayList = new ArrayList();
        printQueue();
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            BleSession next = it.next();
            if (next instanceof IPairDeviceSession) {
                arrayList.add(next);
                next.stop(0);
            }
        }
        this.highSessionQueue.removeAll(arrayList);
    }

    @DexIgnore
    public void cleanUp() {
        DevicePreferenceUtils.clearAutoListAlarm(getBleAdapter().getContext());
        DevicePreferenceUtils.clearAutoSetMapping(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoComplicationAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoWatchAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoBackgroundImageConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoNotificationFiltersConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(getBleAdapter().getContext());
    }

    @DexIgnore
    public abstract void clearQuickCommandQueue();

    @DexIgnore
    public final synchronized void clearSessionQueue() {
        synchronized (this.lowSessionQueue) {
            synchronized (this.highSessionQueue) {
                Iterator<BleSession> it = this.highSessionQueue.iterator();
                while (it.hasNext()) {
                    it.next().stop(FailureCode.SESSION_INTERRUPTED);
                }
                this.highSessionQueue.clear();
                Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
                while (it2.hasNext()) {
                    it2.next().stop(FailureCode.SESSION_INTERRUPTED);
                }
                this.lowSessionQueue.clear();
                if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
                    interruptCurrentSession();
                }
                qa4 qa4 = qa4.a;
            }
            qa4 qa42 = qa4.a;
        }
    }

    @DexIgnore
    public final void closeConnection() {
        clearSessionQueue();
        if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
            getBleAdapter().closeConnection(FLogger.Session.OTHER, true);
        }
    }

    @DexIgnore
    public abstract void confirmStopWorkout(String str, boolean z);

    @DexIgnore
    public final boolean containSyncMode() {
        CommunicateMode communicateMode = getCommunicateMode();
        if (communicateMode != CommunicateMode.SYNC && !this.highSessionQueue.isEmpty()) {
            Iterator<BleSession> it = this.highSessionQueue.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                CommunicateMode communicateMode2 = it.next().getCommunicateMode();
                CommunicateMode communicateMode3 = CommunicateMode.SYNC;
                if (communicateMode2 == communicateMode3) {
                    communicateMode = communicateMode3;
                    break;
                }
            }
        }
        return communicateMode == CommunicateMode.SYNC;
    }

    @DexIgnore
    public abstract boolean disableHeartRateNotification();

    @DexIgnore
    public abstract boolean enableHeartRateNotification();

    @DexIgnore
    public abstract BleAdapter getBleAdapter();

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return CommunicateMode.IDLE;
        }
        return getCurrentSession().getCommunicateMode();
    }

    @DexIgnore
    public final CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public abstract BleSession getCurrentSession();

    @DexIgnore
    public final String getCurrentSessionName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL";
        }
        String simpleName = getCurrentSession().getClass().getSimpleName();
        kd4.a((Object) simpleName, "currentSession.javaClass.simpleName");
        return simpleName;
    }

    @DexIgnore
    public final String getCurrentStateName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL Session";
        }
        if (BleState.Companion.isNull(getCurrentSession().getCurrentState())) {
            return "NULL State";
        }
        String name = getCurrentSession().getCurrentState().getClass().getName();
        kd4.a((Object) name, "currentSession.currentState.javaClass.name");
        return name;
    }

    @DexIgnore
    public final int getGattState() {
        return getBleAdapter().getGattState();
    }

    @DexIgnore
    public final HandlerThread getHandlerThread() {
        return this.handlerThread;
    }

    @DexIgnore
    public final int getHidState() {
        return getBleAdapter().getHidState();
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getHighSessionQueue() {
        return this.highSessionQueue;
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getLowSessionQueue() {
        return this.lowSessionQueue;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public abstract List<FitnessData> getSyncData();

    @DexIgnore
    public final String getTAG() {
        return this.TAG;
    }

    @DexIgnore
    public final synchronized void interruptCurrentSession() {
        if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() != SessionType.CONNECT_WITHOUT_TIMEOUT) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, "Inside " + this.TAG + ".interruptCurrentSession - currentSession=" + getCurrentSession());
            getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
            setNullCurrentSession();
        }
    }

    @DexIgnore
    public abstract boolean isDeviceReady();

    @DexIgnore
    public final synchronized boolean isQueueEmpty() {
        return this.highSessionQueue.isEmpty() && this.lowSessionQueue.isEmpty();
    }

    @DexIgnore
    public final boolean isRunning() {
        return !BleSession.Companion.isNull(getCurrentSession()) || !isQueueEmpty();
    }

    @DexIgnore
    public final void log(String str) {
        kd4.b(str, "message");
        if (!BleSession.Companion.isNull(getCurrentSession())) {
            MFLog mfLog = getCurrentSession().getMfLog();
            if (mfLog != null) {
                mfLog.log('[' + this.serial + "] " + str);
            }
        }
    }

    @DexIgnore
    public final void logNoCurrentSession(String str) {
        kd4.b(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No running session to handle " + str);
        log("No running session to handle " + str);
    }

    @DexIgnore
    public final void logNoCurrentState(String str) {
        kd4.b(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No current state to handle " + str);
        log("No current state to handle " + str);
    }

    @DexIgnore
    public abstract boolean onPing();

    @DexIgnore
    public abstract boolean onReceiveCurrentSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean onReceiveServerRandomKey(byte[] bArr, int i);

    @DexIgnore
    public abstract boolean onReceiveServerSecretKey(byte[] bArr, int i);

    @DexIgnore
    public abstract void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping);

    @DexIgnore
    public final boolean pairDeviceResponse(PairingResponse pairingResponse) {
        kd4.b(pairingResponse, "response");
        BleSession currentSession = getCurrentSession();
        if (!(currentSession instanceof IPairDeviceSession)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, ".pairDeviceResponse() FAILED, current session=" + currentSession);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String str2 = this.serial;
            String str3 = this.TAG;
            remote.i(component, session, str2, str3, "pairDeviceResponse FAILED, currentSession=" + currentSession);
        } else if (pairingResponse instanceof PairingUpdateFWResponse) {
            ((IPairDeviceSession) currentSession).updateFirmware(((PairingUpdateFWResponse) pairingResponse).getFirmwareData());
            return true;
        } else if (pairingResponse instanceof PairingLinkServerResponse) {
            PairingLinkServerResponse pairingLinkServerResponse = (PairingLinkServerResponse) pairingResponse;
            ((IPairDeviceSession) currentSession).onLinkServerSuccess(pairingLinkServerResponse.isSuccess(), pairingLinkServerResponse.getFailureCode());
            return true;
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = this.TAG;
            local2.d(str4, ".pairDeviceResponse() FAILED, response=" + pairingResponse);
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.BLE;
            FLogger.Session session2 = FLogger.Session.PAIR;
            String str5 = this.serial;
            String str6 = this.TAG;
            remote2.i(component2, session2, str5, str6, "pairDeviceResponse FAILED, response=" + pairingResponse);
        }
        return false;
    }

    @DexIgnore
    public final void printQueue() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".printQueue - current session=" + getCurrentSession().getClass().getSimpleName());
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.d(str2, "Inside " + this.TAG + ".printQueue - high session=" + it.next().getClass().getSimpleName());
        }
        Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
        while (it2.hasNext()) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.d(str3, "Inside " + this.TAG + ".printQueue - low session=" + it2.next().getClass().getSimpleName());
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0685  */
    public final synchronized boolean queueSessionAndStart(BleSession bleSession) {
        boolean z;
        kd4.b(bleSession, Constants.SESSION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside queueSessionAndStart - session=" + bleSession);
        SessionType sessionType = bleSession.getSessionType();
        z = false;
        if (sessionType != SessionType.CONNECT_WITHOUT_TIMEOUT) {
            if (!BleSession.Companion.isNull(getCurrentSession())) {
                if (!getCurrentSession().accept(bleSession)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = this.TAG;
                    local2.d(str2, "Inside " + this.TAG + ".queueSessionAndStart -Input session is same as current session. Do nothing.");
                }
            }
            switch (WhenMappings.$EnumSwitchMapping$Anon0[sessionType.ordinal()]) {
                case 1:
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = this.TAG;
                    local3.d(str3, "Inside " + this.TAG + ".queueSessionAndStart - Clean up all sessions and start input session.");
                    log("Input session is a SPECIAL session, interrupt all other ones.");
                    if (getCurrentSession().getSessionType() == bleSession.getSessionType()) {
                        setNullCurrentSession();
                    } else {
                        clearSessionQueue();
                    }
                    this.highSessionQueue.offer(bleSession);
                case 2:
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str4 = this.TAG;
                    local4.d(str4, "Inside " + this.TAG + ".queueSessionAndStart - Sync Mode");
                    if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() == SessionType.SPECIAL) {
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String str5 = this.TAG;
                        local5.d(str5, "Inside " + this.TAG + ".queueSessionAndStart - Sync Mode, current session is " + getCurrentSession().getSessionType() + ", reject this session.");
                        break;
                    } else {
                        ArrayList arrayList = new ArrayList();
                        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                        String str6 = this.TAG;
                        local6.d(str6, "Inside " + this.TAG + ".queueSessionAndStart - Sync Mode, highSessionQueueSize=" + this.highSessionQueue.size());
                        Iterator<BleSession> it = this.highSessionQueue.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                BleSession next = it.next();
                                SessionType sessionType2 = next.getSessionType();
                                ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                                String str7 = this.TAG;
                                local7.d(str7, "Inside " + this.TAG + ".queueSessionAndStart, SessionType=" + sessionType2 + ", communicateMode=" + bleSession.getCommunicateMode());
                                if (sessionType2 == SessionType.CONNECT) {
                                    arrayList.add(next);
                                } else if (sessionType2 != SessionType.SYNC) {
                                    continue;
                                } else if (bleSession.getCommunicateMode() == CommunicateMode.SYNC) {
                                    if (((ISyncSession) bleSession).getSyncMode() == 13) {
                                        arrayList.add(next);
                                    } else {
                                        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                                        String str8 = this.TAG;
                                        local8.d(str8, "Inside " + this.TAG + ".queueSessionAndStart, communicateMode=" + bleSession.getCommunicateMode() + ", syncMode=" + ((ISyncSession) bleSession).getSyncMode());
                                    }
                                }
                            } else {
                                z = true;
                            }
                        }
                        if (z) {
                            this.highSessionQueue.removeAll(arrayList);
                            this.highSessionQueue.offer(bleSession);
                            break;
                        }
                    }
                    break;
                case 3:
                    ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                    String str9 = this.TAG;
                    local9.d(str9, "Inside " + this.TAG + ".queueSessionAndStart - Connect Mode");
                    if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() == SessionType.SPECIAL) {
                        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                        String str10 = this.TAG;
                        local10.d(str10, "Inside " + this.TAG + ".queueSessionAndStart - Connect Mode, current session is " + getCurrentSession().getSessionType() + ", reject this session.");
                        break;
                    } else if (BleSession.Companion.isNull(getCurrentSession()) || getCurrentSession().getSessionType() != SessionType.CONNECT) {
                        Iterator<BleSession> it2 = this.highSessionQueue.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (it2.next().getSessionType() == SessionType.CONNECT) {
                                    z = true;
                                }
                            }
                        }
                        if (!z) {
                            this.highSessionQueue.offer(bleSession);
                        }
                    }
                    break;
                case 4:
                    ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                    String str11 = this.TAG;
                    local11.d(str11, "Inside " + this.TAG + ".queueSessionAndStart - type:" + sessionType);
                    if (!BleSession.Companion.isNull(getCurrentSession())) {
                        SessionType sessionType3 = getCurrentSession().getSessionType();
                        if (sessionType3 == SessionType.SPECIAL) {
                            ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                            String str12 = this.TAG;
                            local12.d(str12, "Inside " + this.TAG + ".queueSessionAndStart - Sync Mode, current session is " + sessionType3 + ", reject this session.");
                            break;
                        } else {
                            if (!(sessionType3 == SessionType.SYNC || sessionType3 == SessionType.CONNECT)) {
                                if (sessionType3 != SessionType.DEVICE_SETTING) {
                                    ArrayList arrayList2 = new ArrayList();
                                    Iterator<BleSession> it3 = this.highSessionQueue.iterator();
                                    while (it3.hasNext()) {
                                        BleSession next2 = it3.next();
                                        if (next2.getSessionType() == SessionType.UI) {
                                            arrayList2.add(next2);
                                        }
                                    }
                                    this.highSessionQueue.removeAll(arrayList2);
                                    if (sessionType3 != SessionType.CONNECT_WITHOUT_TIMEOUT) {
                                        if (sessionType3 == SessionType.UI) {
                                            interruptCurrentSession();
                                        } else {
                                            BleSession copyObject = getCurrentSession().copyObject();
                                            interruptCurrentSession();
                                            if (copyObject.getSessionType() == SessionType.BACK_GROUND) {
                                                this.lowSessionQueue.offer(copyObject);
                                            } else {
                                                this.highSessionQueue.offer(copyObject);
                                            }
                                        }
                                    }
                                    this.highSessionQueue.offer(bleSession);
                                }
                            }
                            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                            String str13 = this.TAG;
                            local13.d(str13, "Inside " + this.TAG + ".queueSessionAndStart - this UI session will be put in queue. CurrentSessionType: " + sessionType3);
                            ArrayList arrayList3 = new ArrayList();
                            Iterator<BleSession> it4 = this.highSessionQueue.iterator();
                            while (it4.hasNext()) {
                                BleSession next3 = it4.next();
                                if (next3.getSessionType() == SessionType.UI) {
                                    arrayList3.add(next3);
                                }
                            }
                            this.highSessionQueue.removeAll(arrayList3);
                            this.highSessionQueue.offer(bleSession);
                        }
                    } else {
                        ArrayList arrayList4 = new ArrayList();
                        Iterator<BleSession> it5 = this.highSessionQueue.iterator();
                        while (it5.hasNext()) {
                            BleSession next4 = it5.next();
                            if (next4.getSessionType() == SessionType.UI) {
                                arrayList4.add(next4);
                            }
                        }
                        this.highSessionQueue.removeAll(arrayList4);
                        this.highSessionQueue.offer(bleSession);
                    }
                case 5:
                    ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
                    String str14 = this.TAG;
                    local14.d(str14, "Inside " + this.TAG + ".queueSessionAndStart - Micro App Set Up Mode");
                    if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() == SessionType.SPECIAL) {
                        ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
                        String str15 = this.TAG;
                        local15.d(str15, "Inside " + this.TAG + ".queueSessionAndStart - Micro App Set Up Mode, current session is " + getCurrentSession().getSessionType() + ", reject this session.");
                        break;
                    } else {
                        ArrayList arrayList5 = new ArrayList();
                        Iterator<BleSession> it6 = this.highSessionQueue.iterator();
                        while (true) {
                            if (it6.hasNext()) {
                                BleSession next5 = it6.next();
                                if (next5.getSessionType() == SessionType.CONNECT) {
                                    arrayList5.add(next5);
                                } else if (next5.accept(bleSession)) {
                                    continue;
                                } else if (next5.getCommunicateMode() == bleSession.getCommunicateMode()) {
                                    arrayList5.add(next5);
                                } else {
                                    ILocalFLogger local16 = FLogger.INSTANCE.getLocal();
                                    String str16 = this.TAG;
                                    local16.d(str16, "Inside " + this.TAG + ".queueSessionAndStart - Micro App Set Up Mode, " + "session in queue is " + next5.getCommunicateMode().name() + ", reject this session, communicateMode=" + bleSession.getCommunicateMode().name());
                                }
                            } else {
                                z = true;
                            }
                        }
                        if (z) {
                            this.highSessionQueue.removeAll(arrayList5);
                            this.highSessionQueue.offer(bleSession);
                            break;
                        }
                    }
                    break;
                case 6:
                    if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() == SessionType.SPECIAL) {
                        ILocalFLogger local17 = FLogger.INSTANCE.getLocal();
                        String str17 = this.TAG;
                        local17.d(str17, "Inside " + this.TAG + ".queueSessionAndStart - Urgent Mode, current session is " + getCurrentSession().getSessionType() + ", reject this session.");
                        break;
                    } else {
                        ArrayList arrayList6 = new ArrayList();
                        Iterator<BleSession> it7 = this.highSessionQueue.iterator();
                        while (it7.hasNext()) {
                            BleSession next6 = it7.next();
                            if (qf4.b(bleSession.getClass().getName(), next6.getClass().getName(), true)) {
                                arrayList6.add(next6);
                            }
                        }
                        this.highSessionQueue.removeAll(arrayList6);
                        this.highSessionQueue.offer(bleSession);
                    }
                    break;
                default:
                    ILocalFLogger local18 = FLogger.INSTANCE.getLocal();
                    String str18 = this.TAG;
                    local18.d(str18, "Inside " + this.TAG + ".queueSessionAndStart - Other Mode");
                    if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() == SessionType.SPECIAL) {
                        ILocalFLogger local19 = FLogger.INSTANCE.getLocal();
                        String str19 = this.TAG;
                        local19.d(str19, "Inside " + this.TAG + ".queueSessionAndStart - Other Mode, current session is " + getCurrentSession().getSessionType() + ", reject this session.");
                        break;
                    } else {
                        CommunicateMode communicateMode = bleSession.getCommunicateMode();
                        ArrayList arrayList7 = new ArrayList();
                        Iterator<BleSession> it8 = this.lowSessionQueue.iterator();
                        while (it8.hasNext()) {
                            BleSession next7 = it8.next();
                            if (communicateMode == next7.getCommunicateMode()) {
                                arrayList7.add(next7);
                            }
                        }
                        this.lowSessionQueue.removeAll(arrayList7);
                        this.lowSessionQueue.offer(bleSession);
                    }
                    break;
            }
        } else if (!BleSession.Companion.isNull(getCurrentSession()) || !this.highSessionQueue.isEmpty() || !this.lowSessionQueue.isEmpty()) {
            ILocalFLogger local20 = FLogger.INSTANCE.getLocal();
            String str20 = this.TAG;
            local20.d(str20, "Inside " + this.TAG + ".queueSessionAndStart - Still have session in queue. Not queue ConnectWithoutTimeout Session.");
            printQueue();
            if (z) {
                startSessionInQueue();
            }
        } else {
            this.highSessionQueue.offer(bleSession);
        }
        z = true;
        printQueue();
        if (z) {
        }
        return z;
    }

    @DexIgnore
    public final void resetSettingFlagsToDefault() {
        Context context = getBleAdapter().getContext();
        List<AlarmSetting> autoListAlarm = DevicePreferenceUtils.getAutoListAlarm(context);
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(context);
        List<BLEMapping> autoMapping = DevicePreferenceUtils.getAutoMapping(context, this.serial);
        kd4.a((Object) autoMapping, "DevicePreferenceUtils.ge\u2026oMapping(context, serial)");
        ComplicationAppMappingSettings autoComplicationAppSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(context, this.serial);
        WatchAppMappingSettings autoWatchAppSettings = DevicePreferenceUtils.getAutoWatchAppSettings(context, this.serial);
        LocalizationData autoLocalizationDataSettings = DevicePreferenceUtils.getAutoLocalizationDataSettings(context, this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(context);
        if (!(autoListAlarm == null || !(!autoListAlarm.isEmpty()) || getBleAdapter().isSupportedFeature(f40.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MULTI_ALARM);
        }
        if (!TextUtils.isEmpty(autoSecondTimezoneId) && getBleAdapter().isSupportedFeature(j40.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.SECOND_TIMEZONE);
        }
        if (!autoMapping.isEmpty()) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MAPPINGS);
        }
        if (!(autoComplicationAppSettings == null || getBleAdapter().isSupportedFeature(i40.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.COMPLICATION_APPS);
        }
        if (!(autoWatchAppSettings == null || getBleAdapter().isSupportedFeature(p40.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.WATCH_APPS);
        }
        if (autoLocalizationDataSettings != null && getBleAdapter().isSupportedFeature(l40.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.LOCALIZATION_DATA);
        }
    }

    @DexIgnore
    public abstract void sendCustomCommand(CustomRequest customRequest);

    @DexIgnore
    public final void setBleSessionCallback(BleSession.BleSessionCallback bleSessionCallback2) {
        kd4.b(bleSessionCallback2, "<set-?>");
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public abstract void setCurrentSession(BleSession bleSession);

    @DexIgnore
    public final void setHighSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        kd4.b(priorityBlockingQueue, "<set-?>");
        this.highSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public final void setLowSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        kd4.b(priorityBlockingQueue, "<set-?>");
        this.lowSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public abstract void setNullCurrentSession();

    @DexIgnore
    public abstract void setSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean startCalibrationSession();

    @DexIgnore
    public abstract boolean startCleanLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startConnectionDeviceSession(boolean z);

    @DexIgnore
    public abstract boolean startGetBatteryLevelSession();

    @DexIgnore
    public abstract boolean startGetRssiSession();

    @DexIgnore
    public abstract boolean startGetVibrationStrengthSession();

    @DexIgnore
    public abstract boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPairingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPlayAnimationSession();

    @DexIgnore
    public abstract boolean startReadCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startReadRealTimeStepSession();

    @DexIgnore
    public abstract void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z);

    @DexIgnore
    public abstract void startSendMusicAppResponse(MusicResponse musicResponse);

    @DexIgnore
    public abstract boolean startSendNotification(NotificationBaseObj notificationBaseObj);

    @DexIgnore
    public final synchronized void startSessionInQueue() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".startSessionInQueue - Starting currentSession=" + getCurrentSession());
        startSessionInQueueProcess();
    }

    @DexIgnore
    public abstract void startSessionInQueueProcess();

    @DexIgnore
    public abstract boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetAutoBiometricData(UserBiometricData userBiometricData);

    @DexIgnore
    public abstract boolean startSetAutoComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetAutoMapping(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetAutoMultiAlarms(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetAutoSecondTimezone(String str);

    @DexIgnore
    public abstract boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetFrontLightEnable(boolean z);

    @DexIgnore
    public abstract boolean startSetHeartRateMode(HeartRateMode heartRateMode);

    @DexIgnore
    public abstract boolean startSetImplicitDeviceConfig(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit);

    @DexIgnore
    public abstract boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData);

    @DexIgnore
    public abstract boolean startSetLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetLocalizationData(LocalizationData localizationData);

    @DexIgnore
    public abstract boolean startSetMultipleAlarmsSession(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetSecondTimezoneSession(String str);

    @DexIgnore
    public abstract boolean startSetStepGoal(int i);

    @DexIgnore
    public abstract boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj);

    @DexIgnore
    public abstract boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startStopCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startSwitchDeviceSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSyncingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startUnlinkSession();

    @DexIgnore
    public abstract boolean startUpdateCurrentTime();

    @DexIgnore
    public final boolean switchDeviceResponse(boolean z, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISwitchDeviceSession) {
            ((ISwitchDeviceSession) currentSession).onLinkServerSuccess(z, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".switchDeviceResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = this.serial;
        String str3 = this.TAG;
        remote.i(component, session, str2, str3, "switchDeviceResponse FAILED, currentSession=" + currentSession);
        return false;
    }
}
