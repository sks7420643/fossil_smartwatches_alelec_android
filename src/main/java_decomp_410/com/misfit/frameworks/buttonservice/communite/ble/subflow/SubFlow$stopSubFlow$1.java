package com.misfit.frameworks.buttonservice.communite.ble.subflow;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1", mo27670f = "SubFlow.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SubFlow$stopSubFlow$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f13226p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$1(com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow subFlow, int i, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1 subFlow$stopSubFlow$1 = new com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1(this.this$0, this.$failureCode, yb4);
        subFlow$stopSubFlow$1.f13226p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return subFlow$stopSubFlow$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            if (!com.misfit.frameworks.buttonservice.communite.ble.BleState.Companion.isNull(this.this$0.getMCurrentState())) {
                this.this$0.getMCurrentState().stopTimeout();
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow subFlow = this.this$0;
                boolean unused = subFlow.enterSubState(new com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState(subFlow.getTAG()));
            }
            this.this$0.onStop(this.$failureCode);
            this.this$0.setExist(false);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
