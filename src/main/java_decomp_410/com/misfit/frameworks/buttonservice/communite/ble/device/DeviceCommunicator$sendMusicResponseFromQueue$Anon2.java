package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Lambda;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendMusicResponseFromQueue$Anon2 extends Lambda implements xc4<Error, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @gc4(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendMusicResponseFromQueue$Anon2$Anon2", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2 extends SuspendLambda implements yc4<zg4, yb4<? super qa4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public zg4 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendMusicResponseFromQueue$Anon2 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2, yb4 yb4) {
            super(2, yb4);
            this.this$Anon0 = deviceCommunicator$sendMusicResponseFromQueue$Anon2;
        }

        @DexIgnore
        public final yb4<qa4> create(Object obj, yb4<?> yb4) {
            kd4.b(yb4, "completion");
            Anon2 anon2 = new Anon2(this.this$Anon0, yb4);
            anon2.p$ = (zg4) obj;
            return anon2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            cc4.a();
            if (this.label == 0) {
                na4.a(obj);
                DeviceCommunicator$sendMusicResponseFromQueue$Anon2 deviceCommunicator$sendMusicResponseFromQueue$Anon2 = this.this$Anon0;
                deviceCommunicator$sendMusicResponseFromQueue$Anon2.this$Anon0.startSendMusicAppResponse(deviceCommunicator$sendMusicResponseFromQueue$Anon2.$response);
                return qa4.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendMusicResponseFromQueue$Anon2(DeviceCommunicator deviceCommunicator, MusicResponse musicResponse) {
        super(1);
        this.this$Anon0 = deviceCommunicator;
        this.$response = musicResponse;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Error) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Error error) {
        ErrorCodeBuilder.Step step;
        kd4.b(error, "error");
        MusicResponse musicResponse = this.$response;
        if (musicResponse instanceof NotifyMusicEventResponse) {
            step = ErrorCodeBuilder.Step.NOTIFY_MUSIC_EVENT;
        } else {
            step = musicResponse instanceof MusicTrackInfoResponse ? ErrorCodeBuilder.Step.SEND_TRACK_INFO : null;
        }
        if (step != null) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = this.this$Anon0.getSerial();
            String access$getTAG$p = this.this$Anon0.getTAG();
            remote.e(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getType() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, error));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$Anon0.getTAG();
        local.d(access$getTAG$p2, "device with serial = " + this.this$Anon0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", push back by result error=" + error.getErrorCode());
        fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon2(this, (yb4) null), 3, (Object) null);
    }
}
