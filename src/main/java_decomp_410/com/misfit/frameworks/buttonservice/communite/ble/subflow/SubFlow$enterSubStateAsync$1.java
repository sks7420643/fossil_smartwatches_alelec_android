package com.misfit.frameworks.buttonservice.communite.ble.subflow;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$enterSubStateAsync$1", mo27670f = "SubFlow.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class SubFlow$enterSubStateAsync$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs $newState;
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f13225p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$enterSubStateAsync$1(com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow subFlow, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs bleStateAbs, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = subFlow;
        this.$newState = bleStateAbs;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$enterSubStateAsync$1 subFlow$enterSubStateAsync$1 = new com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$enterSubStateAsync$1(this.this$0, this.$newState, yb4);
        subFlow$enterSubStateAsync$1.f13225p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return subFlow$enterSubStateAsync$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$enterSubStateAsync$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            boolean unused = this.this$0.enterSubState(this.$newState);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
