package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.fitness.FitnessData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(h90 h90);

    @DexIgnore
    void onApplyHandPositionSuccess();

    @DexIgnore
    void onAuthenticateDeviceFail(h90 h90);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    void onConfigureMicroAppFail(h90 h90);

    @DexIgnore
    void onConfigureMicroAppSuccess();

    @DexIgnore
    void onDataTransferCompleted();

    @DexIgnore
    void onDataTransferFailed(h90 h90);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(Device device, int i);

    @DexIgnore
    void onEraseDataFilesFailed(h90 h90);

    @DexIgnore
    void onEraseDataFilesSuccess();

    @DexIgnore
    void onEraseHWLogFailed(h90 h90);

    @DexIgnore
    void onEraseHWLogSuccess();

    @DexIgnore
    void onExchangeSecretKeyFail(h90 h90);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(h90 h90);

    @DexIgnore
    void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation);

    @DexIgnore
    void onGetDeviceConfigFailed(h90 h90);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap);

    @DexIgnore
    void onGetWatchParamsFail();

    @DexIgnore
    void onMoveHandFailed(h90 h90);

    @DexIgnore
    void onMoveHandSuccess();

    @DexIgnore
    void onNextSession();

    @DexIgnore
    void onPlayAnimationFail(h90 h90);

    @DexIgnore
    void onPlayAnimationSuccess();

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, h90 h90);

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(h90 h90);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession);

    @DexIgnore
    void onReadDataFilesFailed(h90 h90);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(h90 h90);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    void onReadHWLogSuccess();

    @DexIgnore
    void onReadRssiFailed(h90 h90);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(h90 h90);

    @DexIgnore
    void onReleaseHandControlSuccess();

    @DexIgnore
    void onRequestHandControlFailed(h90 h90);

    @DexIgnore
    void onRequestHandControlSuccess();

    @DexIgnore
    void onResetHandsFailed(h90 h90);

    @DexIgnore
    void onResetHandsSuccess();

    @DexIgnore
    void onScanFail(ScanError scanError);

    @DexIgnore
    void onSendMicroAppDataFail(h90 h90);

    @DexIgnore
    void onSendMicroAppDataSuccess();

    @DexIgnore
    void onSetAlarmFailed(h90 h90);

    @DexIgnore
    void onSetAlarmSuccess();

    @DexIgnore
    void onSetBackgroundImageFailed(h90 h90);

    @DexIgnore
    void onSetBackgroundImageSuccess();

    @DexIgnore
    void onSetComplicationFailed(h90 h90);

    @DexIgnore
    void onSetComplicationSuccess();

    @DexIgnore
    void onSetDeviceConfigFailed(h90 h90);

    @DexIgnore
    void onSetDeviceConfigSuccess();

    @DexIgnore
    void onSetFrontLightFailed(h90 h90);

    @DexIgnore
    void onSetFrontLightSuccess();

    @DexIgnore
    void onSetLocalizationDataFail(h90 h90);

    @DexIgnore
    void onSetLocalizationDataSuccess();

    @DexIgnore
    void onSetNotificationFilterFailed(h90 h90);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    void onSetNotificationFilterSuccess();

    @DexIgnore
    void onSetWatchAppFailed(h90 h90);

    @DexIgnore
    void onSetWatchAppSuccess();

    @DexIgnore
    void onSetWatchParamsFail(h90 h90);

    @DexIgnore
    void onSetWatchParamsSuccess();

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(h90 h90);

    @DexIgnore
    void onStopCurrentWorkoutSessionSuccess();

    @DexIgnore
    void onVerifySecretKeyFail(h90 h90);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
