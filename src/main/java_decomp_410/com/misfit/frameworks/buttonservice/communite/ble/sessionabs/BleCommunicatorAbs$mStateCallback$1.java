package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleCommunicatorAbs$mStateCallback$1 implements com.fossil.blesdk.device.Device.C0757a {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs this$0;

    @DexIgnore
    public BleCommunicatorAbs$mStateCallback$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    public void onDeviceStateChanged(com.fossil.blesdk.device.Device device, com.fossil.blesdk.device.Device.State state, com.fossil.blesdk.device.Device.State state2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(device, "device");
        com.fossil.blesdk.obfuscated.kd4.m24411b(state, "previousState");
        com.fossil.blesdk.obfuscated.kd4.m24411b(state2, "newState");
        this.this$0.handleDeviceStateChanged(device, state, state2);
    }

    @DexIgnore
    public void onEventReceived(com.fossil.blesdk.device.Device device, com.fossil.blesdk.device.event.DeviceEvent deviceEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(device, "device");
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceEvent, com.misfit.frameworks.common.constants.Constants.EVENT);
        this.this$0.handleEventReceived(device, deviceEvent);
    }
}
