package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.InactiveNudgeConfig;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.device.data.enumerate.CaloriesUnit;
import com.fossil.blesdk.device.data.enumerate.DateFormat;
import com.fossil.blesdk.device.data.enumerate.TimeFormat;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetImplicitDeviceConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE);
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDeviceConfigSession.this.stop(0);
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetDeviceConfigState() {
            super(SetImplicitDeviceConfigSession.this.getTAG());
        }

        @DexIgnore
        private final DeviceConfigItem[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            deviceConfigBuilder.d(SetImplicitDeviceConfigSession.this.userProfile.getCurrentSteps());
            deviceConfigBuilder.e(SetImplicitDeviceConfigSession.this.userProfile.getGoalSteps());
            deviceConfigBuilder.b(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinute());
            deviceConfigBuilder.a(SetImplicitDeviceConfigSession.this.userProfile.getActiveMinuteGoal());
            deviceConfigBuilder.a(SetImplicitDeviceConfigSession.this.userProfile.getCalories());
            deviceConfigBuilder.b(SetImplicitDeviceConfigSession.this.userProfile.getCaloriesGoal());
            deviceConfigBuilder.c(SetImplicitDeviceConfigSession.this.userProfile.getDistanceInCentimeter());
            deviceConfigBuilder.a(SetImplicitDeviceConfigSession.this.userProfile.getTotalSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getAwakeInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getLightSleepInMinute(), SetImplicitDeviceConfigSession.this.userProfile.getDeepSleepInMinute());
            UserDisplayUnit displayUnit = SetImplicitDeviceConfigSession.this.userProfile.getDisplayUnit();
            if (displayUnit != null) {
                deviceConfigBuilder.a(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), CaloriesUnit.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), TimeFormat.TWELVE, DateFormat.MONTH_DAY_YEAR);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No user display unit.");
                qa4 qa4 = qa4.a;
            }
            InactiveNudgeData inactiveNudgeData = SetImplicitDeviceConfigSession.this.userProfile.getInactiveNudgeData();
            if (inactiveNudgeData != null) {
                deviceConfigBuilder.a(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? InactiveNudgeConfig.State.ENABLE : InactiveNudgeConfig.State.DISABLE);
            } else {
                SetImplicitDeviceConfigSession.this.log("Set Device Config: No inactive nudge config.");
                qa4 qa42 = qa4.a;
            }
            try {
                BiometricProfile sDKBiometricProfile = SetImplicitDeviceConfigSession.this.userProfile.getUserBiometricData().toSDKBiometricProfile();
                deviceConfigBuilder.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
                setImplicitDeviceConfigSession.log("Set Device Config: exception=" + e.getMessage());
            }
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetImplicitDeviceConfigSession.this.getBleAdapter().setDeviceConfig(SetImplicitDeviceConfigSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetImplicitDeviceConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            SetImplicitDeviceConfigSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = SetImplicitDeviceConfigSession.this;
            setImplicitDeviceConfigSession.enterStateAsync(setImplicitDeviceConfigSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDeviceConfigSession.this.log("Set Device Config timeout. Cancel.");
            g90<DeviceConfigKey[]> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDeviceConfigSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DEVICE_CONFIG, bleAdapterImpl, bleSessionCallback);
        kd4.b(userProfile2, "userProfile");
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetImplicitDeviceConfigSession setImplicitDeviceConfigSession = new SetImplicitDeviceConfigSession(this.userProfile, getBleAdapter(), getBleSessionCallback());
        setImplicitDeviceConfigSession.setDevice(getDevice());
        return setImplicitDeviceConfigSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DEVICE_CONFIG_STATE;
        String name = SetDeviceConfigState.class.getName();
        kd4.a((Object) name, "SetDeviceConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        kd4.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        kd4.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
