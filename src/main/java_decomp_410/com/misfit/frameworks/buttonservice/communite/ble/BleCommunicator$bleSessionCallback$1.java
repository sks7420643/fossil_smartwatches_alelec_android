package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleCommunicator$bleSessionCallback$1 implements com.misfit.frameworks.buttonservice.communite.ble.BleSession.BleSessionCallback {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator this$0;

    @DexIgnore
    public BleCommunicator$bleSessionCallback$1(com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator bleCommunicator) {
        this.this$0 = bleCommunicator;
    }

    @DexIgnore
    public void broadcastExchangeSecretKeySuccess(java.lang.String str, java.lang.String str2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(str2, "secretKey");
        this.this$0.getCommunicationResultCallback().onExchangeSecretKeySuccess(str, str2);
    }

    @DexIgnore
    public void onAskForCurrentSecretKey(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForCurrentSecretKey(str);
    }

    @DexIgnore
    public void onAskForLinkServer(com.misfit.frameworks.buttonservice.communite.CommunicateMode communicateMode, android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(communicateMode, "communicateMode");
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK || this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SWITCH_DEVICE) {
            this.this$0.getCommunicationResultCallback().onAskForLinkServer(this.this$0.getSerial(), communicateMode, bundle);
        }
    }

    @DexIgnore
    public void onAskForRandomKey(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForRandomKey(str);
    }

    @DexIgnore
    public void onAskForSecretKey(android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onAskForServerSecretKey(this.this$0.getSerial(), bundle);
    }

    @DexIgnore
    public void onAskForStopWorkout(java.lang.String str) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        this.this$0.getCommunicationResultCallback().onAskForStopWorkout(str);
    }

    @DexIgnore
    public void onBleStateResult(int i, android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        if (!com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion.isNull(this.this$0.getCurrentSession())) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(this.this$0.getCurrentSession().getCommunicateMode(), this.this$0.getSerial(), i, new java.util.ArrayList(), bundle);
        }
    }

    @DexIgnore
    public void onFirmwareLatest() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onFirmwareLatest(this.this$0.getSerial());
        }
    }

    @DexIgnore
    public void onReceivedSyncData(android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC) {
            this.this$0.getCommunicationResultCallback().onReceivedSyncData(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestFirmware(android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onRequestLatestFirmware(this.this$0.getSerial(), bundle);
        }
    }

    @DexIgnore
    public void onRequestLatestWatchParams(java.lang.String str, android.os.Bundle bundle) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(str, "serial");
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        this.this$0.getCommunicationResultCallback().onRequestLatestWatchParams(str, bundle);
    }

    @DexIgnore
    public void onStop(int i, java.util.List<java.lang.Integer> list, android.os.Bundle bundle, com.misfit.frameworks.buttonservice.communite.ble.BleSession bleSession) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(list, "requiredPermissionCodes");
        com.fossil.blesdk.obfuscated.kd4.m24411b(bundle, com.misfit.frameworks.buttonservice.model.Mapping.COLUMN_EXTRA_INFO);
        com.fossil.blesdk.obfuscated.kd4.m24411b(bleSession, com.misfit.frameworks.common.constants.Constants.SESSION);
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) bleSession, (java.lang.Object) this.this$0.getCurrentSession()) || bleSession.requireBroadCastInAnyCase()) {
            this.this$0.getCommunicationResultCallback().onCommunicatorResult(bleSession.getCommunicateMode(), this.this$0.getSerial(), i, list, bundle);
        }
        if (com.fossil.blesdk.obfuscated.kd4.m24409a((java.lang.Object) bleSession, (java.lang.Object) this.this$0.getCurrentSession())) {
            this.this$0.setNullCurrentSession();
            com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            java.lang.String tag = this.this$0.getTAG();
            local.mo33255d(tag, "Inside " + this.this$0.getTAG() + ".bleSessionCallback.onStop");
            this.this$0.startSessionInQueue();
        }
    }

    @DexIgnore
    public void onUpdateFirmwareFailed() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareFailed(this.this$0.getSerial());
        }
    }

    @DexIgnore
    public void onUpdateFirmwareSuccess() {
        if (this.this$0.getCurrentSession().getCommunicateMode() == com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK) {
            this.this$0.getCommunicationResultCallback().onUpdateFirmwareSuccess(this.this$0.getSerial());
        }
    }
}
