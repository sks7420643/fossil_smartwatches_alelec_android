package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1 extends Lambda implements xc4<DeviceInformation, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((DeviceInformation) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "it");
        this.this$Anon0.log(this.$logSession$inlined, "Fetch Device Information Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$cp = BleAdapterImpl.TAG;
        local.d(access$getTAG$cp, ".fetchDeviceInfo(), data=" + new Gson().a((Object) deviceInformation));
        this.$callback$inlined.onFetchDeviceInfoSuccess(deviceInformation);
    }
}
