package com.misfit.frameworks.buttonservice.communite.ble.session;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadRssiSession$onStart$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession this$0;

    @DexIgnore
    public ReadRssiSession$onStart$1(com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession readRssiSession) {
        this.this$0 = readRssiSession;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stop(com.misfit.frameworks.buttonservice.log.FailureCode.BLUETOOTH_IS_DISABLED);
    }
}
