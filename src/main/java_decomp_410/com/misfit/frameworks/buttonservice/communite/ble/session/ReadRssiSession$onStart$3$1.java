package com.misfit.frameworks.buttonservice.communite.ble.session;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadRssiSession$onStart$3$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession $this_run;

    @DexIgnore
    public ReadRssiSession$onStart$3$1(com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession readRssiSession) {
        this.$this_run = readRssiSession;
    }

    @DexIgnore
    public final void run() {
        this.$this_run.stop(com.misfit.frameworks.buttonservice.log.FailureCode.FAILED_TO_READ_RSSI);
    }
}
