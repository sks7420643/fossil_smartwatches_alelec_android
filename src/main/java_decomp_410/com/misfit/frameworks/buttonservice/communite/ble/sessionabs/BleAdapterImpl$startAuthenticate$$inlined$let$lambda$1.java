package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$startAuthenticate$$inlined$let$lambda$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<byte[], com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $phoneRandomNumber$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$startAuthenticate$$inlined$let$lambda$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl bleAdapterImpl, byte[] bArr, com.misfit.frameworks.buttonservice.log.FLogger.Session session, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$phoneRandomNumber$inlined = bArr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((byte[]) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(byte[] bArr) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bArr, "it");
        com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl bleAdapterImpl = this.this$0;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Authenticate Success " + com.misfit.frameworks.buttonservice.utils.ConversionUtils.byteArrayToString(bArr));
        this.$callback$inlined.onAuthenticateDeviceSuccess(bArr);
    }
}
