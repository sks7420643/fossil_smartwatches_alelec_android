package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.yb4;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetBatteryLevelSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.GetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PlayAnimationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRealTimeStepsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadRssiSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetStepGoalSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetVibrationStrengthSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UnlinkSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.UpdateCurrentTimeSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.QuickCommandQueue;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BleCommunicatorAbs extends BleCommunicator {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ HandlerThread handlerThread; // = new HandlerThread(TAG);
    @DexIgnore
    public BleSession currentSession;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapter;
    @DexIgnore
    public /* final */ Handler mHandler; // = new Handler(getHandlerThread().getLooper());
    @DexIgnore
    public QuickCommandQueue mQuickCommandQueue; // = new QuickCommandQueue();
    @DexIgnore
    public /* final */ Device.a mStateCallback;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final HandlerThread getHandlerThread() {
            return BleCommunicatorAbs.handlerThread;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[Device.State.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[Device.State.CONNECTED.ordinal()] = 1;
        }
        */
    }

    /*
    static {
        String simpleName = BleCommunicatorAbs.class.getSimpleName();
        kd4.a((Object) simpleName, "BleCommunicatorAbs::class.java.simpleName");
        TAG = simpleName;
        handlerThread.start();
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs(BleAdapterImpl bleAdapterImpl, Context context, String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(str, communicationResultCallback);
        kd4.b(bleAdapterImpl, "mBleAdapter");
        kd4.b(context, "context");
        kd4.b(str, "serial");
        kd4.b(communicationResultCallback, "communicationResultCallback");
        this.mBleAdapter = bleAdapterImpl;
        this.currentSession = BleSessionAbs.Companion.createNullSession(context);
        this.mStateCallback = new BleCommunicatorAbs$mStateCallback$Anon1(this);
        this.mBleAdapter.registerBluetoothStateCallback(this.mStateCallback);
    }

    @DexIgnore
    private final synchronized void processQuickCommandQueue() {
        throw null;
        // Object poll = this.mQuickCommandQueue.poll();
        // if (poll != null) {
        //     if (getBleAdapter().isDeviceReady()) {
        //         onQuickCommandAction(poll);
        //     }
        //     fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new BleCommunicatorAbs$processQuickCommandQueue$Anon1(this, (yb4) null), 3, (Object) null);
        // }
    }

    @DexIgnore
    public final synchronized void addToQuickCommandQueue(Object obj) {
        kd4.b(obj, "obj");
        this.mQuickCommandQueue.add(obj);
        processQuickCommandQueue();
    }

    @DexIgnore
    public void cleanUp() {
        super.cleanUp();
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    public void clearQuickCommandQueue() {
        this.mQuickCommandQueue.clear();
    }

    @DexIgnore
    public BleSession getCurrentSession() {
        return this.currentSession;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapter() {
        return this.mBleAdapter;
    }

    @DexIgnore
    public final Handler getMHandler() {
        return this.mHandler;
    }

    @DexIgnore
    public void handleDeviceStateChanged(Device device, Device.State state, Device.State state2) {
        kd4.b(device, "device");
        kd4.b(state, "previousState");
        kd4.b(state2, "newState");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onDeviceStateChanged, device=" + device.getDeviceInformation().getSerialNumber() + ", previousState=" + state + ", newState=" + state2);
        if (WhenMappings.$EnumSwitchMapping$Anon0[state2.ordinal()] != 1) {
            getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 0);
            return;
        }
        getCommunicationResultCallback().onGattConnectionStateChanged(getBleAdapter().getSerial(), 2);
        processQuickCommandQueue();
    }

    @DexIgnore
    public void handleEventReceived(Device device, DeviceEvent deviceEvent) {
        kd4.b(device, "device");
        kd4.b(deviceEvent, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onEventReceived(), device=" + device.getDeviceInformation().getSerialNumber() + ", event=" + deviceEvent);
    }

    @DexIgnore
    public boolean isDeviceReady() {
        return this.mBleAdapter.isDeviceReady();
    }

    @DexIgnore
    public abstract void onQuickCommandAction(Object obj);

    @DexIgnore
    public void sendCustomCommand(CustomRequest customRequest) {
        kd4.b(customRequest, Constants.COMMAND);
        this.mBleAdapter.sendCustomCommand(customRequest);
    }

    @DexIgnore
    public void setCurrentSession(BleSession bleSession) {
        kd4.b(bleSession, "<set-?>");
        this.currentSession = bleSession;
    }

    @DexIgnore
    public void setNullCurrentSession() {
        setCurrentSession(BleSessionAbs.Companion.createNullSession(getBleAdapter().getContext()));
    }

    @DexIgnore
    public boolean startCalibrationSession() {
        return false;
    }

    @DexIgnore
    public boolean startGetBatteryLevelSession() {
        queueSessionAndStart(new GetBatteryLevelSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startGetRssiSession() {
        queueSessionAndStart(new ReadRssiSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startGetVibrationStrengthSession() {
        queueSessionAndStart(new GetVibrationStrengthSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startPlayAnimationSession() {
        queueSessionAndStart(new PlayAnimationSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startReadRealTimeStepSession() {
        queueSessionAndStart(new ReadRealTimeStepsSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        kd4.b(notificationBaseObj, "newNotification");
        return false;
    }

    @DexIgnore
    public void startSessionInQueueProcess() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            BleSession poll = getHighSessionQueue().poll();
            if (poll == null) {
                poll = getLowSessionQueue().poll();
            }
            if (poll == null || !(poll instanceof BleSessionAbs)) {
                FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - queue is empty. Be idle now.");
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".startSessionInQueueProcess() - next session is " + poll);
            setCurrentSession(poll);
            getCurrentSession().start(new Object[0]);
            return;
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), ".startSessionInQueueProcess() - a session is exist, session will be start later.");
    }

    @DexIgnore
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        kd4.b(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        kd4.b(localizationData, "localizationData");
        return false;
    }

    @DexIgnore
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        kd4.b(list, "multipleAlarmList");
        return false;
    }

    @DexIgnore
    public boolean startSetStepGoal(int i) {
        queueSessionAndStart(new SetStepGoalSession(i, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj) {
        kd4.b(vibrationStrengthObj, "vibrationStrengthLevelObj");
        queueSessionAndStart(new SetVibrationStrengthSession(vibrationStrengthObj, this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startUnlinkSession() {
        queueSessionAndStart(new UnlinkSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startUpdateCurrentTime() {
        queueSessionAndStart(new UpdateCurrentTimeSession(this.mBleAdapter, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public BleAdapterImpl getBleAdapter() {
        return this.mBleAdapter;
    }
}
