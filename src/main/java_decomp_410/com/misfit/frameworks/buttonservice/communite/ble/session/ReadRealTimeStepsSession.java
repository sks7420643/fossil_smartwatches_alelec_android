package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DailyStepConfig;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadRealTimeStepsSession extends EnableMaintainingSession {
    @DexIgnore
    public long mRealTimeSteps; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRealTimeStepsState extends BleStateAbs {
        @DexIgnore
        public g90<HashMap<DeviceConfigKey, DeviceConfigItem>> task;

        @DexIgnore
        public ReadRealTimeStepsState() {
            super(ReadRealTimeStepsSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
            pd4 pd4 = pd4.a;
            Object[] objArr = {hashMap.get(DeviceConfigKey.TIME), hashMap.get(DeviceConfigKey.BATTERY), hashMap.get(DeviceConfigKey.BIOMETRIC_PROFILE), hashMap.get(DeviceConfigKey.DAILY_STEP), hashMap.get(DeviceConfigKey.DAILY_STEP_GOAL), hashMap.get(DeviceConfigKey.DAILY_CALORIE), hashMap.get(DeviceConfigKey.DAILY_CALORIE_GOAL), hashMap.get(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(DeviceConfigKey.DAILY_DISTANCE), hashMap.get(DeviceConfigKey.INACTIVE_NUDGE), hashMap.get(DeviceConfigKey.VIBE_STRENGTH), hashMap.get(DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + ReadRealTimeStepsSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            readRealTimeStepsSession.log(format);
        }

        @DexIgnore
        public boolean onEnter() {
            ReadRealTimeStepsSession.this.log("Read Real Time Steps");
            this.task = ReadRealTimeStepsSession.this.getBleAdapter().getDeviceConfig(ReadRealTimeStepsSession.this.getLogSession(), this);
            if (this.task == null) {
                ReadRealTimeStepsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            ReadRealTimeStepsSession.this.stop(FailureCode.FAILED_TO_GET_REAL_TIME_STEP);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
            kd4.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(DeviceConfigKey.DAILY_STEP)) {
                ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
                DeviceConfigItem deviceConfigItem = hashMap.get(DeviceConfigKey.DAILY_STEP);
                if (deviceConfigItem != null) {
                    readRealTimeStepsSession.mRealTimeSteps = ((DailyStepConfig) deviceConfigItem).getStep();
                    ReadRealTimeStepsSession readRealTimeStepsSession2 = ReadRealTimeStepsSession.this;
                    readRealTimeStepsSession2.log("Read Real Time Steps Success, value=" + ReadRealTimeStepsSession.this.mRealTimeSteps);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                }
            } else {
                ReadRealTimeStepsSession.this.log("Read Real Time Steps Success, but no value.");
            }
            ReadRealTimeStepsSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<HashMap<DeviceConfigKey, DeviceConfigItem>> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRealTimeStepsSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        kd4.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putLong(Constants.DAILY_STEPS, this.mRealTimeSteps);
    }

    @DexIgnore
    public BleSession copyObject() {
        ReadRealTimeStepsSession readRealTimeStepsSession = new ReadRealTimeStepsSession(getBleAdapter(), getBleSessionCallback());
        readRealTimeStepsSession.setDevice(getDevice());
        return readRealTimeStepsSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE;
        String name = ReadRealTimeStepsState.class.getName();
        kd4.a((Object) name, "ReadRealTimeStepsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
