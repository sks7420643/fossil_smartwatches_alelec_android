package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.error.Error, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.log.FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$2(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl bleAdapterImpl, com.misfit.frameworks.buttonservice.log.FLogger.Session session, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.error.Error) obj);
        return com.fossil.blesdk.obfuscated.qa4.a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.error.Error error) {
        com.fossil.blesdk.obfuscated.kd4.b(error, "it");
        this.this$0.logSdkError(this.$logSession$inlined, "Apply Hands Positions", com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Step.SET_CALIBRATION_POSITION, error);
        this.$callback$inlined.onApplyHandPositionFailed(error.getErrorCode());
    }
}
