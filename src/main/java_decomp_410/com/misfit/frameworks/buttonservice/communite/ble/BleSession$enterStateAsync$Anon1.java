package com.misfit.frameworks.buttonservice.communite.ble;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.communite.ble.BleSession$enterStateAsync$Anon1", f = "BleSession.kt", l = {}, m = "invokeSuspend")
public final class BleSession$enterStateAsync$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleState $newState;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BleSession this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleSession$enterStateAsync$Anon1(BleSession bleSession, BleState bleState, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = bleSession;
        this.$newState = bleState;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        kd4.b(yb4, "completion");
        BleSession$enterStateAsync$Anon1 bleSession$enterStateAsync$Anon1 = new BleSession$enterStateAsync$Anon1(this.this$Anon0, this.$newState, yb4);
        bleSession$enterStateAsync$Anon1.p$ = (zg4) obj;
        return bleSession$enterStateAsync$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleSession$enterStateAsync$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        cc4.a();
        if (this.label == 0) {
            na4.a(obj);
            this.this$Anon0.enterState(this.$newState);
            return qa4.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
