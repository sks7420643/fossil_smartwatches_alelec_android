package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetComplicationSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings mComplicationAppMappingSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplicationsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetComplicationsState() {
            super(SetComplicationSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetComplicationSession.this.getBleAdapter().setComplications(SetComplicationSession.this.getLogSession(), SetComplicationSession.this.mComplicationAppMappingSettings, this);
            if (this.task == null) {
                SetComplicationSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetComplicationSession.this.getContext(), SetComplicationSession.this.getSerial())) {
                SetComplicationSession.this.log("Reach the limit retry. Stop.");
                SetComplicationSession.this.stop(1920);
            }
        }

        @DexIgnore
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoComplicationAppSettings(SetComplicationSession.this.getBleAdapter().getContext(), SetComplicationSession.this.getBleAdapter().getSerial(), new Gson().a((Object) SetComplicationSession.this.mComplicationAppMappingSettings));
            SetComplicationSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetComplicationSession(ComplicationAppMappingSettings complicationAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_COMPLICATION_APPS, bleAdapterImpl, bleSessionCallback);
        kd4.b(complicationAppMappingSettings, "mComplicationAppMappingSettings");
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.mComplicationAppMappingSettings = complicationAppMappingSettings;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        kd4.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_COMPLICATION_APPS) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetComplicationSession setComplicationSession = new SetComplicationSession(this.mComplicationAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setComplicationSession.setDevice(getDevice());
        return setComplicationSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE;
        String name = SetComplicationsState.class.getName();
        kd4.a((Object) name, "SetComplicationsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
