package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.blesdk.obfuscated.cc4;
import com.fossil.blesdk.obfuscated.gc4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.zg4;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import kotlin.coroutines.jvm.internal.SuspendLambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@gc4(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$Anon1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$stopSubFlow$Anon1 extends SuspendLambda implements yc4<Object, Object, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;
    @DexIgnore
    public zg4 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$Anon1(SubFlow subFlow, int i, yb4 yb4) {
        super(2, yb4);
        this.this$Anon0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    public final yb4<qa4> create(Object obj, yb4<?> yb4) {
        throw null;
        // kd4.b(yb4, "completion");
        // SubFlow$stopSubFlow$Anon1 subFlow$stopSubFlow$Anon1 = new SubFlow$stopSubFlow$Anon1(this.this$Anon0, this.$failureCode, yb4);
        // subFlow$stopSubFlow$Anon1.p$ = (zg4) obj;
        // return subFlow$stopSubFlow$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        throw null;
        // return ((SubFlow$stopSubFlow$Anon1) create(obj, (yb4) obj2)).invokeSuspend(qa4.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        throw null;
        // cc4.a();
        // if (this.label == 0) {
        //     na4.a(obj);
        //     if (!BleState.Companion.isNull(this.this$Anon0.getMCurrentState())) {
        //         this.this$Anon0.getMCurrentState().stopTimeout();
        //         SubFlow subFlow = this.this$Anon0;
        //         boolean unused = subFlow.enterSubState(new NullBleState(subFlow.getTAG()));
        //     }
        //     this.this$Anon0.onStop(this.$failureCode);
        //     this.this$Anon0.setExist(false);
        //     return qa4.a;
        // }
        // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
