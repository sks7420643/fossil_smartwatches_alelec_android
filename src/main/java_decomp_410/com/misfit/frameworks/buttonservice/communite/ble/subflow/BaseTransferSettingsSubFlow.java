package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.InactiveNudgeConfig;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.device.data.enumerate.CaloriesUnit;
import com.fossil.blesdk.device.data.enumerate.DateFormat;
import com.fossil.blesdk.device.data.enumerate.TimeFormat;
import com.fossil.blesdk.obfuscated.f90;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseTransferSettingsSubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BackgroundConfig backgroundConfig;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isFullSync;
    @DexIgnore
    public /* final */ LocalizationData localizationData;
    @DexIgnore
    public /* final */ List<MicroAppMapping> microAppMappings;
    @DexIgnore
    public /* final */ List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public /* final */ AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public /* final */ int secondTimezoneOffset;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public /* final */ WatchAppMappingSettings watchAppMappingSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfig extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetBackgroundImageConfig() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                BackgroundConfig backgroundConfig = BaseTransferSettingsSubFlow.this.getBackgroundConfig();
                if (backgroundConfig != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setBackgroundImage(BaseTransferSettingsSubFlow.this.getLogSession(), backgroundConfig, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                    } else {
                        startTimeout();
                        obj = qa4.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No background image config");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Background Image Config step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetBackgroundConfig=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            return true;
        }

        @DexIgnore
        public void onSetBackgroundImageFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Background Image timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplications extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetComplications() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                ComplicationAppMappingSettings complicationAppMappingSettings = BaseTransferSettingsSubFlow.this.getComplicationAppMappingSettings();
                if (complicationAppMappingSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setComplications(BaseTransferSettingsSubFlow.this.getLogSession(), complicationAppMappingSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                    } else {
                        startTimeout();
                        obj = qa4.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Complication step. No complication settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Complication step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(1920);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Complication timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetDeviceConfigState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:37:0x01d1, code lost:
            if (r0 == null) goto L_0x01d4;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0154  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0190  */
        private final DeviceConfigItem[] prepareConfigData() {
            UserProfile userProfile;
            UserProfile userProfile2;
            Object obj;
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            if (!BaseTransferSettingsSubFlow.this.getBleAdapter().getVibrationStrength().isDefaultValue()) {
                deviceConfigBuilder.a(BaseTransferSettingsSubFlow.this.getBleAdapter().getVibrationStrength().toSDKVibrationStrengthLevel());
            } else {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: VibeStrength is default value, do not set it to device.");
            }
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange((short) BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset())) {
                deviceConfigBuilder.a((short) BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset());
            } else {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.log("Set Device Config: Timezone is out of range: " + BaseTransferSettingsSubFlow.this.getSecondTimezoneOffset());
            }
            if (BaseTransferSettingsSubFlow.this.getUserProfile() != null) {
                deviceConfigBuilder.d(BaseTransferSettingsSubFlow.this.getUserProfile().getCurrentSteps());
                deviceConfigBuilder.e(BaseTransferSettingsSubFlow.this.getUserProfile().getGoalSteps());
                deviceConfigBuilder.b(BaseTransferSettingsSubFlow.this.getUserProfile().getActiveMinute());
                deviceConfigBuilder.a(BaseTransferSettingsSubFlow.this.getUserProfile().getActiveMinuteGoal());
                deviceConfigBuilder.a(BaseTransferSettingsSubFlow.this.getUserProfile().getCalories());
                deviceConfigBuilder.b(BaseTransferSettingsSubFlow.this.getUserProfile().getCaloriesGoal());
                deviceConfigBuilder.c(BaseTransferSettingsSubFlow.this.getUserProfile().getDistanceInCentimeter());
                deviceConfigBuilder.a(BaseTransferSettingsSubFlow.this.getUserProfile().getTotalSleepInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getAwakeInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getLightSleepInMinute(), BaseTransferSettingsSubFlow.this.getUserProfile().getDeepSleepInMinute());
            } else {
                BaseTransferSettingsSubFlow.this.log("Set Device Config: No user profile data.");
                throw null;
                // qa4 qa4 = qa4.a;
            }
            UserProfile userProfile3 = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (userProfile3 != null) {
                UserDisplayUnit displayUnit = userProfile3.getDisplayUnit();
                if (displayUnit != null) {
                    deviceConfigBuilder.a(displayUnit.getTemperatureUnit().toSDKTemperatureUnit(), CaloriesUnit.KCAL, displayUnit.getDistanceUnit().toSDKDistanceUnit(), TimeFormat.TWELVE, DateFormat.MONTH_DAY_YEAR);
                    userProfile = BaseTransferSettingsSubFlow.this.getUserProfile();
                    if (userProfile != null) {
                        InactiveNudgeData inactiveNudgeData = userProfile.getInactiveNudgeData();
                        if (inactiveNudgeData != null) {
                            deviceConfigBuilder.a(inactiveNudgeData.getStartHour(), inactiveNudgeData.getStartMinute(), inactiveNudgeData.getStopHour(), inactiveNudgeData.getStopMinute(), inactiveNudgeData.getRepeatInterval(), inactiveNudgeData.isEnable() ? InactiveNudgeConfig.State.ENABLE : InactiveNudgeConfig.State.DISABLE);
                            userProfile2 = BaseTransferSettingsSubFlow.this.getUserProfile();
                            if (userProfile2 != null) {
                                UserBiometricData userBiometricData = userProfile2.getUserBiometricData();
                                if (userBiometricData != null) {
                                    try {
                                        BiometricProfile sDKBiometricProfile = userBiometricData.toSDKBiometricProfile();
                                        deviceConfigBuilder.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWeightInKilogram(), sDKBiometricProfile.getWearingPosition());
                                        obj = deviceConfigBuilder;
                                    } catch (Exception e) {
                                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                                        baseTransferSettingsSubFlow2.log("Set Device Config: exception=" + e.getMessage());
                                        obj = qa4.a;
                                    }
                                }
                            }
                            BaseTransferSettingsSubFlow.this.log("Set Device Config: No biometric data.");
                            qa4 qa42 = qa4.a;
                            return deviceConfigBuilder.a();
                        }
                    }
                    BaseTransferSettingsSubFlow.this.log("Set Device Config: No inactive nudge config.");
                    qa4 qa43 = qa4.a;
                    userProfile2 = BaseTransferSettingsSubFlow.this.getUserProfile();
                    if (userProfile2 != null) {
                    }
                    BaseTransferSettingsSubFlow.this.log("Set Device Config: No biometric data.");
                    qa4 qa422 = qa4.a;
                    return deviceConfigBuilder.a();
                }
            }
            BaseTransferSettingsSubFlow.this.log("Set Device Config: No user display unit.");
            qa4 qa44 = qa4.a;
            userProfile = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (userProfile != null) {
            }
            BaseTransferSettingsSubFlow.this.log("Set Device Config: No inactive nudge config.");
            qa4 qa432 = qa4.a;
            userProfile2 = BaseTransferSettingsSubFlow.this.getUserProfile();
            if (userProfile2 != null) {
            }
            BaseTransferSettingsSubFlow.this.log("Set Device Config: No biometric data.");
            qa4 qa4222 = qa4.a;
            return deviceConfigBuilder.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setDeviceConfig(BaseTransferSettingsSubFlow.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Device Config timeout. Cancel.");
            g90<DeviceConfigKey[]> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetListAlarmsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                List<AlarmSetting> multiAlarmSettings = BaseTransferSettingsSubFlow.this.getMultiAlarmSettings();
                if (multiAlarmSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setAlarms(BaseTransferSettingsSubFlow.this.getLogSession(), multiAlarmSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
                    } else {
                        startTimeout();
                        obj = qa4.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Alarm step. Alarm settings null");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Alarm step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetMultiAlarmSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetAlarmFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set List Alarms timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalization extends BleStateAbs {
        @DexIgnore
        public f90<String> task;

        @DexIgnore
        public SetLocalization() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                LocalizationData localizationData = BaseTransferSettingsSubFlow.this.getLocalizationData();
                if (localizationData != null) {
                    if (localizationData.isDataValid()) {
                        this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setLocalizationData(localizationData, BaseTransferSettingsSubFlow.this.getLogSession(), this);
                        if (this.task == null) {
                            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                            obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                        } else {
                            startTimeout();
                            obj = qa4.a;
                        }
                    } else {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                        baseTransferSettingsSubFlow2.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow3.enterSubStateAsync(baseTransferSettingsSubFlow3.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Localization step. No localization settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow5 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow5.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow6 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow6.enterSubStateAsync(baseTransferSettingsSubFlow6.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            return true;
        }

        @DexIgnore
        public void onSetLocalizationDataFail(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Localization timeout. Cancel.");
            f90<String> f90 = this.task;
            if (f90 != null) {
                f90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public f90<qa4> task;

        @DexIgnore
        public SetMicroAppMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public void onConfigureMicroAppFail(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public void onConfigureMicroAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            throw null;
            // if (BaseTransferSettingsSubFlow.this.getMicroAppMappings() == null || !(!BaseTransferSettingsSubFlow.this.getMicroAppMappings().isEmpty())) {
            //     BaseTransferSettingsSubFlow.this.stopSubFlow(0);
            // } else {
            //     BleAdapterImpl bleAdapter = BaseTransferSettingsSubFlow.this.getBleAdapter();
            //     FLogger.Session logSession = BaseTransferSettingsSubFlow.this.getLogSession();
            //     List<com.fossil.blesdk.model.microapp.MicroAppMapping> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(BaseTransferSettingsSubFlow.this.getMicroAppMappings());
            //     kd4.a((Object) convertToSDKMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
            //     this.task = bleAdapter.configureMicroApp(logSession, convertToSDKMapping, this);
            //     if (this.task == null) {
            //         BaseTransferSettingsSubFlow.this.stopSubFlow(0);
            //     } else {
            //         startTimeout();
            //     }
            // }
            // return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Micro Apps timeout. Cancel.");
            f90<qa4> f90 = this.task;
            if (f90 != null) {
                f90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilterSettings extends BleStateAbs {
        @DexIgnore
        public f90<qa4> task;

        @DexIgnore
        public SetNotificationFilterSettings() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                AppNotificationFilterSettings notificationFilterSettings = BaseTransferSettingsSubFlow.this.getNotificationFilterSettings();
                if (notificationFilterSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setNotificationFilters(BaseTransferSettingsSubFlow.this.getLogSession(), notificationFilterSettings.getNotificationFilters(), this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                    } else {
                        startTimeout();
                        obj = qa4.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No notification filter settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Notification Filter Settings step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetNotificationFilterSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            return true;
        }

        @DexIgnore
        public void onSetNotificationFilterFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Notification Filter Settings timeout. Cancel.");
            f90<qa4> f90 = this.task;
            if (f90 != null) {
                f90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchApps extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetWatchApps() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                WatchAppMappingSettings watchAppMappingSettings = BaseTransferSettingsSubFlow.this.getWatchAppMappingSettings();
                if (watchAppMappingSettings != null) {
                    this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchApps(BaseTransferSettingsSubFlow.this.getLogSession(), watchAppMappingSettings, this);
                    if (this.task == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                    } else {
                        startTimeout();
                        obj = qa4.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Watch App step. No watch apps settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Watch App step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
            return true;
        }

        @DexIgnore
        public void onSetWatchAppFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Apps timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchParamsState extends BleStateAbs {
        @DexIgnore
        public g90<qa4> task;

        @DexIgnore
        public SetWatchParamsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle exportFirmwareVersion() {
            Bundle bundle = new Bundle();
            bundle.putInt(ButtonService.WATCH_PARAMS_MAJOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMajor());
            bundle.putInt(ButtonService.WATCH_PARAMS_MINOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMinor());
            bundle.putFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION, Float.parseFloat(BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion()));
            return bundle;
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            startTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "supportedWatchParamVersion = " + BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamVersion() + ", currentWatchParamVersion=" + BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion());
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferSettingsSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            bleSessionCallback.onRequestLatestWatchParams(BaseTransferSettingsSubFlow.this.getSerial(), exportFirmwareVersion());
            return true;
        }

        @DexIgnore
        public void onGetWatchParamsFail() {
            stopTimeout();
            if (!retry(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), BaseTransferSettingsSubFlow.this.getSerial())) {
                BaseTransferSettingsSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            }
        }

        @DexIgnore
        public void onSetWatchParamsFail(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Param failed");
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onSetWatchParamsSuccess() {
            BaseTransferSettingsSubFlow.this.log("Set Watch Param success");
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Params timeout. Cancel.");
            g90<qa4> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }

        @DexIgnore
        public final void setWatchParamToDevice(String str, WatchParamsFileMapping watchParamsFileMapping) {
            kd4.b(str, "serial");
            kd4.b(watchParamsFileMapping, "watchParamsFileMapping");
            BaseTransferSettingsSubFlow.this.log("Set WatchParam to device");
            this.task = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchParams(BaseTransferSettingsSubFlow.this.getLogSession(), watchParamsFileMapping, this);
            if (this.task == null) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferSettingsSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, boolean z, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, List<AlarmSetting> list, ComplicationAppMappingSettings complicationAppMappingSettings2, WatchAppMappingSettings watchAppMappingSettings2, BackgroundConfig backgroundConfig2, AppNotificationFilterSettings appNotificationFilterSettings, LocalizationData localizationData2, List<MicroAppMapping> list2, int i, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        kd4.b(str, "tagName");
        kd4.b(bleSession, "bleSession");
        kd4.b(session, "logSession");
        kd4.b(str2, "serial");
        kd4.b(bleAdapterImpl, "mBleAdapterV2");
        this.isFullSync = z;
        this.userProfile = userProfile2;
        this.multiAlarmSettings = list;
        this.complicationAppMappingSettings = complicationAppMappingSettings2;
        this.watchAppMappingSettings = watchAppMappingSettings2;
        this.backgroundConfig = backgroundConfig2;
        this.notificationFilterSettings = appNotificationFilterSettings;
        this.localizationData = localizationData2;
        this.microAppMappings = list2;
        this.secondTimezoneOffset = i;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final void doNextState() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, failed because the current sub state is not an instance of SetWatchParamsState");
        }
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final ComplicationAppMappingSettings getComplicationAppMappingSettings() {
        return this.complicationAppMappingSettings;
    }

    @DexIgnore
    public final LocalizationData getLocalizationData() {
        return this.localizationData;
    }

    @DexIgnore
    public final List<MicroAppMapping> getMicroAppMappings() {
        return this.microAppMappings;
    }

    @DexIgnore
    public final List<AlarmSetting> getMultiAlarmSettings() {
        return this.multiAlarmSettings;
    }

    @DexIgnore
    public final AppNotificationFilterSettings getNotificationFilterSettings() {
        return this.notificationFilterSettings;
    }

    @DexIgnore
    public final int getSecondTimezoneOffset() {
        return this.secondTimezoneOffset;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public final WatchAppMappingSettings getWatchAppMappingSettings() {
        return this.watchAppMappingSettings;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.SET_WATCH_PARAMS;
        String name = SetWatchParamsState.class.getName();
        kd4.a((Object) name, "SetWatchParamsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.SET_DEVICE_CONFIG_STATE;
        String name2 = SetDeviceConfigState.class.getName();
        kd4.a((Object) name2, "SetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.SET_LIST_ALARMS_STATE;
        String name3 = SetListAlarmsState.class.getName();
        kd4.a((Object) name3, "SetListAlarmsState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.SET_COMPLICATIONS_STATE;
        String name4 = SetComplications.class.getName();
        kd4.a((Object) name4, "SetComplications::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.SET_WATCH_APPS_STATE;
        String name5 = SetWatchApps.class.getName();
        kd4.a((Object) name5, "SetWatchApps::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<SubFlow.SessionState, String> sessionStateMap6 = getSessionStateMap();
        SubFlow.SessionState sessionState6 = SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name6 = SetBackgroundImageConfig.class.getName();
        kd4.a((Object) name6, "SetBackgroundImageConfig::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<SubFlow.SessionState, String> sessionStateMap7 = getSessionStateMap();
        SubFlow.SessionState sessionState7 = SubFlow.SessionState.SET_LOCALIZATION_STATE;
        String name7 = SetLocalization.class.getName();
        kd4.a((Object) name7, "SetLocalization::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<SubFlow.SessionState, String> sessionStateMap8 = getSessionStateMap();
        SubFlow.SessionState sessionState8 = SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name8 = SetNotificationFilterSettings.class.getName();
        kd4.a((Object) name8, "SetNotificationFilterSettings::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<SubFlow.SessionState, String> sessionStateMap9 = getSessionStateMap();
        SubFlow.SessionState sessionState9 = SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE;
        String name9 = SetMicroAppMappingState.class.getName();
        kd4.a((Object) name9, "SetMicroAppMappingState::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return this.isFullSync;
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_WATCH_PARAMS));
        return true;
    }

    @DexIgnore
    public final void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "getWatchParamFailed");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).onGetWatchParamsFail();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        kd4.b(str, "serial");
        kd4.b(watchParamsFileMapping, "watchParamsData");
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam...");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).setWatchParamToDevice(str, watchParamsFileMapping);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }
}
