package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleAdapterImpl$sendCustomCommand$Anon2 extends Lambda implements xc4<Error, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$sendCustomCommand$Anon2(BleAdapterImpl bleAdapterImpl) {
        super(1);
        this.this$Anon0 = bleAdapterImpl;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Error) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Error error) {
        kd4.b(error, "it");
        this.this$Anon0.logSdkError(FLogger.Session.OTHER, "Send Custom Command", ErrorCodeBuilder.Step.UNKNOWN, error);
    }
}
