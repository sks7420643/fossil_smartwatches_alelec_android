package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.InactiveNudgeConfig;
import com.fossil.blesdk.device.data.config.builder.DeviceConfigBuilder;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetInactiveNudgeConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ InactiveNudgeData mInactiveNudgeData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetInactiveNudgeConfigState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceConfigKey[]> task;

        @DexIgnore
        public SetInactiveNudgeConfigState() {
            super(SetInactiveNudgeConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            DeviceConfigBuilder deviceConfigBuilder = new DeviceConfigBuilder();
            deviceConfigBuilder.a(SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStartMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopHour(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getStopMinute(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.getRepeatInterval(), SetInactiveNudgeConfigSession.this.mInactiveNudgeData.isEnable() ? InactiveNudgeConfig.State.ENABLE : InactiveNudgeConfig.State.DISABLE);
            this.task = SetInactiveNudgeConfigSession.this.getBleAdapter().setDeviceConfig(SetInactiveNudgeConfigSession.this.getLogSession(), deviceConfigBuilder.a(), this);
            if (this.task == null) {
                SetInactiveNudgeConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            if (!retry(SetInactiveNudgeConfigSession.this.getContext(), SetInactiveNudgeConfigSession.this.getSerial())) {
                SetInactiveNudgeConfigSession.this.log("Reach the limit retry. Stop.");
                SetInactiveNudgeConfigSession.this.stop(FailureCode.FAILED_TO_SET_INACTIVE_NUDGE_CONFIG);
            }
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetInactiveNudgeConfigSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            SetInactiveNudgeConfigSession.this.log("Set Inactive nudge timeout. Cancel.");
            g90<DeviceConfigKey[]> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_INACTIVE_NUDGE_CONFIG, bleAdapterImpl, bleSessionCallback);
        kd4.b(inactiveNudgeData, "mInactiveNudgeData");
        kd4.b(bleAdapterImpl, "bleAdapterV2");
        this.mInactiveNudgeData = inactiveNudgeData;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetInactiveNudgeConfigSession setInactiveNudgeConfigSession = new SetInactiveNudgeConfigSession(this.mInactiveNudgeData, getBleAdapter(), getBleSessionCallback());
        setInactiveNudgeConfigSession.setDevice(getDevice());
        return setInactiveNudgeConfigSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_INACTIVE_NUDGE_STATE;
        String name = SetInactiveNudgeConfigState.class.getName();
        kd4.a((Object) name, "SetInactiveNudgeConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
