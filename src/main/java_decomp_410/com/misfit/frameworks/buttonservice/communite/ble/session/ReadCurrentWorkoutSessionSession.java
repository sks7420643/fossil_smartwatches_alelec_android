package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadCurrentWorkoutSessionSession extends EnableMaintainingSession {
    @DexIgnore
    public WorkoutSession mCurrentWorkoutSession;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadCurrentWorkoutSession extends BleStateAbs {
        @DexIgnore
        public g90<WorkoutSession> task;

        @DexIgnore
        public ReadCurrentWorkoutSession() {
            super(ReadCurrentWorkoutSessionSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = ReadCurrentWorkoutSessionSession.this.getBleAdapter().readCurrentWorkoutSession(ReadCurrentWorkoutSessionSession.this.getLogSession(), this);
            if (this.task == null) {
                ReadCurrentWorkoutSessionSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession) {
            kd4.b(workoutSession, "workoutSession");
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.mCurrentWorkoutSession = workoutSession;
            ReadCurrentWorkoutSessionSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<WorkoutSession> g90 = this.task;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadCurrentWorkoutSessionSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, bleAdapterImpl, bleSessionCallback);
        kd4.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(Constants.CURRENT_WORKOUT_SESSION, this.mCurrentWorkoutSession);
    }

    @DexIgnore
    public BleSession copyObject() {
        ReadCurrentWorkoutSessionSession readCurrentWorkoutSessionSession = new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback());
        readCurrentWorkoutSessionSession.setDevice(getDevice());
        return readCurrentWorkoutSessionSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE;
        String name = ReadCurrentWorkoutSession.class.getName();
        kd4.a((Object) name, "ReadCurrentWorkoutSession::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
