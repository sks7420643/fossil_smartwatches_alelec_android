package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleState$timeoutTask$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.BleState this$0;

    @DexIgnore
    public BleState$timeoutTask$1(com.misfit.frameworks.buttonservice.communite.ble.BleState bleState) {
        this.this$0 = bleState;
    }

    @DexIgnore
    public final void run() {
        this.this$0.onTimeout();
    }
}
