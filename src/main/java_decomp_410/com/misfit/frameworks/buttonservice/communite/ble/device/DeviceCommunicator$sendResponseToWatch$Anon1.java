package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendResponseToWatch$Anon1 extends Lambda implements xc4<qa4, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceAppResponse $response;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendResponseToWatch$Anon1(DeviceCommunicator deviceCommunicator, DeviceAppResponse deviceAppResponse) {
        super(1);
        this.this$Anon0 = deviceCommunicator;
        this.$response = deviceAppResponse;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((qa4) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(qa4 qa4) {
        kd4.b(qa4, "it");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = this.this$Anon0.getSerial();
        String access$getTAG$p = this.this$Anon0.getTAG();
        remote.i(component, session, serial, access$getTAG$p, "Send respond: " + this.$response.getDeviceEventId().name() + " Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$Anon0.getTAG();
        local.d(access$getTAG$p2, "device with serial = " + this.this$Anon0.getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + this.$response.toString() + ", result success");
    }
}
