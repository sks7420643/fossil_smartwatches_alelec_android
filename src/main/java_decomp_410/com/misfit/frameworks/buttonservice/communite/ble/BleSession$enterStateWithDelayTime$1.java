package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleSession$enterStateWithDelayTime$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.BleState $state;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.BleSession this$0;

    @DexIgnore
    public BleSession$enterStateWithDelayTime$1(com.misfit.frameworks.buttonservice.communite.ble.BleSession bleSession, com.misfit.frameworks.buttonservice.communite.ble.BleState bleState) {
        this.this$0 = bleSession;
        this.$state = bleState;
    }

    @DexIgnore
    public final void run() {
        this.this$0.enterState(this.$state);
    }
}
