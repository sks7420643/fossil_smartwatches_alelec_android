package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.f90;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseOTASubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ byte[] firmwareBytes;
    @DexIgnore
    public /* final */ FirmwareData firmwareData;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public g90<DeviceInformation> mFetchDeviceInfoTask;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(BaseOTASubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            throw null;
            // if (qf4.a(BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion())) {
            //     this.mFetchDeviceInfoTask = BaseOTASubFlow.this.getBleAdapter().fetchDeviceInfo(BaseOTASubFlow.this.getLogSession(), this);
            //     if (this.mFetchDeviceInfoTask == null) {
            //         BaseOTASubFlow.this.stopSubFlow(10000);
            //         return true;
            //     }
            //     startTimeout();
            //     return true;
            // }
            // BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            // baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
            // return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(h90 h90) {
            kd4.b(h90, "error");
            stopTimeout();
            Device deviceObj = BaseOTASubFlow.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != Device.State.CONNECTED) {
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                BaseOTASubFlow.this.getBleSession().log("Reach the limit retry. Stop.");
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
            kd4.b(deviceInformation, "deviceInformation");
            stopTimeout();
            BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            g90<DeviceInformation> g90 = this.mFetchDeviceInfoTask;
            if (g90 != null) {
                g90.e();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTAState extends BleStateAbs {
        @DexIgnore
        public f90<String> mOTATask;
        @DexIgnore
        public float previousProgress;

        @DexIgnore
        public OTAState() {
            super(BaseOTASubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        public void onDataTransferCompleted() {
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                BaseOTASubFlow.this.stopSubFlow(0);
            }
        }

        @DexIgnore
        public void onDataTransferFailed(h90 h90) {
            kd4.b(h90, "error");
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                    BaseOTASubFlow.this.log("Reach the limit retry. Stop.");
                    BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_OTA);
                }
            }
        }

        @DexIgnore
        public void onDataTransferProgressChange(float f) {
            if (isExist()) {
                float f2 = f * ((float) 100);
                if (f2 - this.previousProgress >= ((float) 1) || f2 >= 100.0f) {
                    this.previousProgress = f2;
                    setTimeout(f2 >= 100.0f ? 60000 : 30000);
                    startTimeout();
                }
                BleCommunicator.CommunicationResultCallback communicationResultCallback = BaseOTASubFlow.this.getCommunicationResultCallback();
                if (communicationResultCallback != null) {
                    communicationResultCallback.onOtaProgressUpdated(BaseOTASubFlow.this.getSerial(), f2);
                }
            }
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (qf4.b(BaseOTASubFlow.this.getFirmwareData().getFirmwareVersion(), BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion(), true)) {
                BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
                baseOTASubFlow.log("Do OTA: Old fw and New fw are same. " + BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion());
                BaseOTASubFlow.this.stopSubFlow(0);
            } else {
                this.mOTATask = BaseOTASubFlow.this.getBleAdapter().doOTA(BaseOTASubFlow.this.getLogSession(), BaseOTASubFlow.this.getFirmwareBytes(), this);
                if (this.mOTATask == null) {
                    BaseOTASubFlow.this.stopSubFlow(10000);
                } else {
                    startTimeout();
                    this.previousProgress = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
            }
            return true;
        }

        @DexIgnore
        public void onExit() {
            super.onExit();
            f90<String> f90 = this.mOTATask;
            if (f90 != null) {
                f90.e();
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            f90<String> f90 = this.mOTATask;
            if (f90 != null) {
                f90.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseOTASubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, FirmwareData firmwareData2, byte[] bArr, BleSession.BleSessionCallback bleSessionCallback2, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        kd4.b(str, "tagName");
        kd4.b(bleSession, "bleSession");
        kd4.b(session, "logSession");
        kd4.b(str2, "serial");
        kd4.b(bleAdapterImpl, "bleAdapterV2");
        kd4.b(firmwareData2, "firmwareData");
        kd4.b(bArr, "firmwareBytes");
        this.mflog = mFLog;
        this.firmwareData = firmwareData2;
        this.firmwareBytes = bArr;
        this.bleSessionCallback = bleSessionCallback2;
        this.communicationResultCallback = communicationResultCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final byte[] getFirmwareBytes() {
        return this.firmwareBytes;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        kd4.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.OTA_STATE;
        String name2 = OTAState.class.getName();
        kd4.a((Object) name2, "OTAState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.FETCH_DEVICE_INFO_STATE));
        return true;
    }
}
