package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1", mo27670f = "BleCommunicatorAbs.kt", mo27671l = {}, mo27672m = "invokeSuspend")
public final class BleCommunicatorAbs$processQuickCommandQueue$1 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
    @DexIgnore
    public int label;

    @DexIgnore
    /* renamed from: p$ */
    public com.fossil.blesdk.obfuscated.zg4 f13224p$;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs$processQuickCommandQueue$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs bleCommunicatorAbs, com.fossil.blesdk.obfuscated.yb4 yb4) {
        super(2, yb4);
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
        com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1 bleCommunicatorAbs$processQuickCommandQueue$1 = new com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1(this.this$0, yb4);
        bleCommunicatorAbs$processQuickCommandQueue$1.f13224p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
        return bleCommunicatorAbs$processQuickCommandQueue$1;
    }

    @DexIgnore
    public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        return ((com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
    }

    @DexIgnore
    public final java.lang.Object invokeSuspend(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.cc4.m20546a();
        if (this.label == 0) {
            com.fossil.blesdk.obfuscated.na4.m25642a(obj);
            this.this$0.processQuickCommandQueue();
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }
        throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
