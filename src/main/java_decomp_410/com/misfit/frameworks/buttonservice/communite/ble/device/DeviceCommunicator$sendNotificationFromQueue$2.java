package com.misfit.frameworks.buttonservice.communite.ble.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendNotificationFromQueue$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.error.Error, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2$1")
    @com.fossil.blesdk.obfuscated.gc4(mo27669c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2$1", mo27670f = "DeviceCommunicator.kt", mo27671l = {}, mo27672m = "invokeSuspend")
    /* renamed from: com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2$1 */
    public static final class C39471 extends kotlin.coroutines.jvm.internal.SuspendLambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.obfuscated.zg4, com.fossil.blesdk.obfuscated.yb4<? super com.fossil.blesdk.obfuscated.qa4>, java.lang.Object> {
        @DexIgnore
        public int label;

        @DexIgnore
        /* renamed from: p$ */
        public com.fossil.blesdk.obfuscated.zg4 f13222p$;
        @DexIgnore
        public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C39471(com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2 deviceCommunicator$sendNotificationFromQueue$2, com.fossil.blesdk.obfuscated.yb4 yb4) {
            super(2, yb4);
            this.this$0 = deviceCommunicator$sendNotificationFromQueue$2;
        }

        @DexIgnore
        public final com.fossil.blesdk.obfuscated.yb4<com.fossil.blesdk.obfuscated.qa4> create(java.lang.Object obj, com.fossil.blesdk.obfuscated.yb4<?> yb4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(yb4, "completion");
            com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2.C39471 r0 = new com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2.C39471(this.this$0, yb4);
            r0.f13222p$ = (com.fossil.blesdk.obfuscated.zg4) obj;
            return r0;
        }

        @DexIgnore
        public final java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
            return ((com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2.C39471) create(obj, (com.fossil.blesdk.obfuscated.yb4) obj2)).invokeSuspend(com.fossil.blesdk.obfuscated.qa4.f17909a);
        }

        @DexIgnore
        public final java.lang.Object invokeSuspend(java.lang.Object obj) {
            com.fossil.blesdk.obfuscated.cc4.m20546a();
            if (this.label == 0) {
                com.fossil.blesdk.obfuscated.na4.m25642a(obj);
                com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2 deviceCommunicator$sendNotificationFromQueue$2 = this.this$0;
                deviceCommunicator$sendNotificationFromQueue$2.this$0.sendDianaNotification(deviceCommunicator$sendNotificationFromQueue$2.$notification);
                return com.fossil.blesdk.obfuscated.qa4.f17909a;
            }
            throw new java.lang.IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$2(com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator deviceCommunicator, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.error.Error) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.error.Error error) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(error, "error");
        com.misfit.frameworks.buttonservice.log.IRemoteFLogger remote = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote();
        com.misfit.frameworks.buttonservice.log.FLogger.Component component = com.misfit.frameworks.buttonservice.log.FLogger.Component.BLE;
        com.misfit.frameworks.buttonservice.log.FLogger.Session session = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER;
        java.lang.String serial = this.this$0.getSerial();
        java.lang.String access$getTAG$p = this.this$0.getTAG();
        remote.mo33262e(component, session, serial, access$getTAG$p, "Send notification: " + this.$notification.toRemoteLogString() + " Failed, error=" + com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.INSTANCE.build(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Step.SEND_APP_NOTIFICATION, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Component.SDK, error));
        com.misfit.frameworks.buttonservice.log.ILocalFLogger local = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        java.lang.String access$getTAG$p2 = this.this$0.getTAG();
        local.mo33255d(access$getTAG$p2, " .sendNotificationFromQueue() = " + this.$notification + ", error=" + error.getErrorCode());
        com.fossil.blesdk.obfuscated.fi4 unused = com.fossil.blesdk.obfuscated.ag4.m19844b(com.fossil.blesdk.obfuscated.ah4.m19846a(com.fossil.blesdk.obfuscated.nh4.m25691a()), (kotlin.coroutines.CoroutineContext) null, (kotlinx.coroutines.CoroutineStart) null, new com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2.C39471(this, (com.fossil.blesdk.obfuscated.yb4) null), 3, (java.lang.Object) null);
    }
}
