package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.blesdk.adapter.ScanError;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.ag4;
import com.fossil.blesdk.obfuscated.ah4;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.nh4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.yb4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class SubFlow extends BleStateAbs implements ISessionSdkCallback {
    @DexIgnore
    public /* final */ BleAdapterImpl bleAdapter;
    @DexIgnore
    public /* final */ BleSession bleSession;
    @DexIgnore
    public /* final */ FLogger.Session logSession;
    @DexIgnore
    public BleStateAbs mCurrentState; // = new NullBleState(getTAG());
    @DexIgnore
    public /* final */ MFLog mfLog;
    @DexIgnore
    public /* final */ String serial;
    @DexIgnore
    public HashMap<SessionState, String> sessionStateMap;

    @DexIgnore
    public enum SessionState {
        SCANNING_STATE,
        ENABLE_MAINTAINING_CONNECTION_STATE,
        FETCH_DEVICE_INFO_STATE,
        GET_DEVICE_CONFIG_STATE,
        PLAY_DEVICE_ANIMATION_STATE,
        ERASE_DATA_FILE_STATE,
        DONE_STATE,
        OTA_STATE,
        SET_DEVICE_CONFIG_STATE,
        SET_WATCH_PARAMS,
        READ_OR_ERASE_STATE,
        READ_DATA_FILE_STATE,
        PROCESS_AND_STORE_DATA_STATE,
        SET_COMPLICATIONS_STATE,
        SET_WATCH_APPS_STATE,
        SET_BACKGROUND_IMAGE_CONFIG_STATE,
        SET_LOCALIZATION_STATE,
        SET_MICRO_APP_MAPPING,
        CLOSE_CONNECTION_STATE,
        REQUEST_HAND_CONTROL_STATE,
        RESET_HANDS_STATE,
        MOVE_HAND_STATE,
        APPLY_HAND_STATE,
        RELEASE_HAND_CONTROL_STATE,
        SET_STEP_GOAL_STATE,
        READ_RSSI_STATE,
        READ_REAL_TIME_STEPS_STATE,
        UPDATE_CURRENT_TIME_STATE,
        GET_BATTERY_LEVEL_STATE,
        SET_VIBRATION_STRENGTH_STATE,
        GET_VIBRATION_STRENGTH_STATE,
        SET_LIST_ALARMS_STATE,
        SET_BIOMETRIC_DATA_STATE,
        SET_SETTING_DONE_STATE,
        SET_HEART_RATE_MODE_STATE,
        SET_FRONT_LIGHT_ENABLE_STATE,
        READ_CURRENT_WORKOUT_STATE,
        STOP_CURRENT_WORKOUT_STATE,
        SET_NOTIFICATION_FILTERS_STATE,
        VERIFY_SECRET_KEY,
        GENERATE_RANDOM_KEY,
        AUTHENTICATE_DEVICE,
        EXCHANGE_SECRET_KEY,
        PROCESS_MAPPING,
        SET_MICRO_APP_MAPPING_AFTER_OTA_STATE,
        PROCESS_HID
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow(String str, BleSession bleSession2, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl) {
        super(str);
        kd4.b(str, "tagName");
        kd4.b(bleSession2, "bleSession");
        kd4.b(session, "logSession");
        kd4.b(str2, "serial");
        kd4.b(bleAdapterImpl, "bleAdapter");
        this.bleSession = bleSession2;
        this.mfLog = mFLog;
        this.logSession = session;
        this.serial = str2;
        this.bleAdapter = bleAdapterImpl;
        initMap();
    }

    @DexIgnore
    private final boolean enterSubState(BleStateAbs bleStateAbs) {
        if (!isExist()) {
            return false;
        }
        if (!BleState.Companion.isNull(this.mCurrentState) && !BleState.Companion.isNull(bleStateAbs) && qf4.b(this.mCurrentState.getClass().getName(), bleStateAbs.getClass().getName(), true)) {
            return true;
        }
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.onExit();
        }
        this.mCurrentState = bleStateAbs;
        if (BleState.Companion.isNull(this.mCurrentState)) {
            return false;
        }
        boolean onEnter = bleStateAbs.onEnter();
        if (!onEnter) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.e(tag, "Failed to enter state: " + bleStateAbs);
            this.mCurrentState = new NullBleState(getTAG());
        }
        return onEnter;
    }

    @DexIgnore
    private final void initMap() {
        this.sessionStateMap = new HashMap<>();
        initStateMap();
    }

    @DexIgnore
    public final void addFailureCode(int i) {
        this.bleSession.addFailureCode(i);
    }

    @DexIgnore
    public final BleStateAbs createConcreteState(SessionState sessionState) {
        BleStateAbs bleStateAbs;
        kd4.b(sessionState, "state");
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            String str = hashMap.get(sessionState);
            if (str != null) {
                try {
                    Class<?> cls = Class.forName(str);
                    kd4.a((Object) cls, "Class.forName(stateClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{cls.getDeclaringClass()});
                    kd4.a((Object) declaredConstructor, "innerClass.getDeclaredConstructor(parentClass)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(new Object[]{this});
                    if (newInstance != null) {
                        bleStateAbs = (BleStateAbs) newInstance;
                        return bleStateAbs != null ? bleStateAbs : new NullBleState(getTAG());
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs");
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = getTAG();
                    local.e(tag, "Inside getState method, cannot instance state " + str + ", e = " + e);
                    bleStateAbs = null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.d("sessionStateMap");
            throw null;
        }
        throw null;
    }

    @DexIgnore
    public final fi4 enterSubStateAsync(BleStateAbs bleStateAbs) {
        throw null;
        // kd4.b(bleStateAbs, "newState");
        // return ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new SubFlow$enterSubStateAsync$Anon1(this, bleStateAbs, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void errorLog(String str, FLogger.Component component, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        kd4.b(str, "message");
        kd4.b(component, "component");
        kd4.b(step, "step");
        kd4.b(appError, "error");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.APP, appError);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(component, this.logSession, this.serial, getTAG(), build, step, str);
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapter() {
        return this.bleAdapter;
    }

    @DexIgnore
    public final BleSession getBleSession() {
        return this.bleSession;
    }

    @DexIgnore
    public final FLogger.Session getLogSession() {
        return this.logSession;
    }

    @DexIgnore
    public final BleStateAbs getMCurrentState() {
        return this.mCurrentState;
    }

    @DexIgnore
    public final MFLog getMfLog() {
        return this.mfLog;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final HashMap<SessionState, String> getSessionStateMap() {
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            return hashMap;
        }
        kd4.d("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public abstract void initStateMap();

    @DexIgnore
    public void log(String str) {
        kd4.b(str, "message");
        FLogger.INSTANCE.getLocal().d(getTAG(), str);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str);
        }
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), str);
    }

    @DexIgnore
    public void onApplyHandPositionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onDeviceFound(Device device, int i) {
        kd4.b(device, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onExit() {
        if (!BleState.Companion.isNull(this.mCurrentState)) {
            this.mCurrentState.stopTimeout();
            enterSubState(new NullBleState(getTAG()));
        }
        super.onExit();
    }

    @DexIgnore
    public void onFetchDeviceInfoFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onFetchDeviceInfoSuccess(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onGetDeviceConfigSuccess(HashMap<DeviceConfigKey, DeviceConfigItem> hashMap) {
        kd4.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onPlayDeviceAnimation(boolean z, h90 h90) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadCurrentWorkoutSessionSuccess(WorkoutSession workoutSession) {
        kd4.b(workoutSession, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        kd4.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onScanFail(ScanError scanError) {
        kd4.b(scanError, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public abstract void onStop(int i);

    @DexIgnore
    public void onStopCurrentWorkoutSessionFailed(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeyFail(h90 h90) {
        kd4.b(h90, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final void setMCurrentState(BleStateAbs bleStateAbs) {
        kd4.b(bleStateAbs, "<set-?>");
        this.mCurrentState = bleStateAbs;
    }

    @DexIgnore
    public final void setSessionStateMap(HashMap<SessionState, String> hashMap) {
        kd4.b(hashMap, "<set-?>");
        this.sessionStateMap = hashMap;
    }

    @DexIgnore
    public final void stopSubFlow(int i) {
        throw null;
        // fi4 unused = ag4.b(ah4.a(nh4.a()), (CoroutineContext) null, (CoroutineStart) null, new SubFlow$stopSubFlow$Anon1(this, i, (yb4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, Error error) {
        kd4.b(str, "message");
        kd4.b(step, "step");
        kd4.b(error, "sdkError");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, error);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.log('[' + this.serial + "] " + str + " , error=" + build);
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, this.logSession, this.serial, getTAG(), build, step, str);
    }
}
