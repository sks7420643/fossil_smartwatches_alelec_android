package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BleSession$startWithDelay$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ java.lang.Object[] $params;
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.BleSession this$0;

    @DexIgnore
    public BleSession$startWithDelay$1(com.misfit.frameworks.buttonservice.communite.ble.BleSession bleSession, java.lang.Object[] objArr) {
        this.this$0 = bleSession;
        this.$params = objArr;
    }

    @DexIgnore
    public final void run() {
        com.misfit.frameworks.buttonservice.communite.ble.BleSession bleSession = this.this$0;
        java.lang.Object[] objArr = this.$params;
        bleSession.start(java.util.Arrays.copyOf(objArr, objArr.length));
    }
}
