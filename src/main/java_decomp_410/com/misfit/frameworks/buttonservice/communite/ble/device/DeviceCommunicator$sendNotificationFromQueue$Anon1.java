package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator$sendNotificationFromQueue$Anon1 extends Lambda implements xc4<qa4, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$Anon1(DeviceCommunicator deviceCommunicator, NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$Anon0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((qa4) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(qa4 qa4) {
        kd4.b(qa4, "it");
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = this.this$Anon0.getSerial();
        String access$getTAG$p = this.this$Anon0.getTAG();
        remote.i(component, session, serial, access$getTAG$p, "Send notification: " + this.$notification.toRemoteLogString() + " Success");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$Anon0.getTAG();
        local.d(access$getTAG$p2, " .sendNotificationFromQueue() = " + this.$notification + ", result=success");
    }
}
