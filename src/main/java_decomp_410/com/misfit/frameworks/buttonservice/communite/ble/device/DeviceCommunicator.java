package com.misfit.frameworks.buttonservice.communite.ble.device;

import android.content.Context;
import android.os.Bundle;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.device.event.notification.AlarmSyncNotification;
import com.fossil.blesdk.device.event.notification.AppNotificationControlNotification;
import com.fossil.blesdk.device.event.notification.CommuteTimeWatchAppNotification;
import com.fossil.blesdk.device.event.notification.DeviceConfigSyncNotification;
import com.fossil.blesdk.device.event.notification.DeviceNotification;
import com.fossil.blesdk.device.event.notification.MusicControlNotification;
import com.fossil.blesdk.device.event.notification.NotificationFilterSyncNotification;
import com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.MicroAppRequest;
import com.fossil.blesdk.device.event.request.RingPhoneRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.enumerate.CommuteTimeWatchAppAction;
import com.fossil.blesdk.obfuscated.g90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.fitness.FitnessData;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ClearLinkMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBiometricDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoComplicationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoLocalizationDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetComplicationSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetFrontLightEnableSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetHeartRateModeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDeviceConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDisplayUnitSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetInactiveNudgeConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetLinkMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetPresetAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.StopCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceCommunicator extends BleCommunicatorAbs {
    @DexIgnore
    public /* final */ HashMap<String, DeviceRequest> mDeviceAppRequests; // = new HashMap<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator(Context context, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(new BleAdapterImpl(context, str, str2), context, str, communicationResultCallback);
        kd4.b(context, "context");
        kd4.b(str, "serial");
        kd4.b(str2, "macAddress");
        kd4.b(communicationResultCallback, "communicationResultCallback");
    }

    @DexIgnore
    private final synchronized void sendDeviceAppResponseFromQueue(DeviceAppResponse deviceAppResponse) {
        if (!deviceAppResponse.getLifeTimeObject().isExpire()) {
            if (deviceAppResponse.isForceUpdate()) {
                sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceData());
            } else {
                String name = deviceAppResponse.getDeviceEventId().name();
                DeviceRequest deviceRequest = this.mDeviceAppRequests.get(name);
                if (deviceRequest != null) {
                    sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceResponse(deviceRequest, new Version((byte) getBleAdapter().getMicroAppMajorVersion(), (byte) getBleAdapter().getMicroAppMinorVersion())));
                } else {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.BLE;
                    FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
                    String serial = getSerial();
                    String tag = getTAG();
                    String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
                    ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
                    remote.e(component, session, serial, tag, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. deviceAppRequest with key[" + name + "] does not exist.");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag2 = getTAG();
                    local.e(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue(), deviceAppRequest with key[" + name + "] does not exist.");
                }
            }
        }
    }

    @DexIgnore
    private final synchronized void sendDianaNotification(NotificationBaseObj notificationBaseObj) {
        addToQuickCommandQueue(notificationBaseObj);
    }

    @DexIgnore
    private final synchronized void sendMusicResponseFromQueue(MusicResponse musicResponse) {
        if (!musicResponse.getLifeCountDownObject().isExpire()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() = " + musicResponse.toString());
            g90<qa4> g90 = null;
            if (musicResponse instanceof NotifyMusicEventResponse) {
                MusicEvent sDKMusicEvent = ((NotifyMusicEventResponse) musicResponse).toSDKMusicEvent();
                if (sDKMusicEvent != null) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.BLE;
                    FLogger.Session session = FLogger.Session.OTHER;
                    String serial = getSerial();
                    String tag2 = getTAG();
                    remote.d(component, session, serial, tag2, "NotifyMusicEvent: " + musicResponse.toRemoteLogString());
                    g90 = getBleAdapter().notifyMusicEvent(FLogger.Session.OTHER, sDKMusicEvent);
                } else {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag3 = getTAG();
                    local2.e(tag3, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() fail cause by sdkMusicEvent=" + sDKMusicEvent);
                }
            } else if (musicResponse instanceof MusicTrackInfoResponse) {
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.BLE;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String serial2 = getSerial();
                String tag4 = getTAG();
                remote2.d(component2, session2, serial2, tag4, "SendTrackInfo: " + musicResponse.toRemoteLogString());
                g90 = getBleAdapter().sendTrackInfo(FLogger.Session.OTHER, ((MusicTrackInfoResponse) musicResponse).toSDKTrackInfo());
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tag5 = getTAG();
                local3.d(tag5, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() no support value = " + musicResponse.toRemoteLogString());
            }
            musicResponse.getLifeCountDownObject().goDown();
            if (g90 != null) {
                g90<qa4> h = g90.e(new DeviceCommunicator$sendMusicResponseFromQueue$Anon1(this, musicResponse));
                if (h != null) {
                    h.c(new DeviceCommunicator$sendMusicResponseFromQueue$Anon2(this, musicResponse));
                }
            }
        }
    }

    @DexIgnore
    private final synchronized void sendNotificationFromQueue(NotificationBaseObj notificationBaseObj) {
        if (!notificationBaseObj.getLifeCountDownObject().isExpire()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, " .sendNotificationFromQueue() = " + notificationBaseObj + ", serial=" + getSerial());
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.d(component, session, serial, tag2, "Send notification: " + notificationBaseObj.toRemoteLogString());
            g90<qa4> sendNotification = getBleAdapter().sendNotification(FLogger.Session.OTHER, notificationBaseObj);
            notificationBaseObj.getLifeCountDownObject().goDown();
            if (sendNotification != null) {
                g90<qa4> h = sendNotification.e(new DeviceCommunicator$sendNotificationFromQueue$Anon1(this, notificationBaseObj));
                if (h != null) {
                    h.c(new DeviceCommunicator$sendNotificationFromQueue$Anon2(this, notificationBaseObj));
                }
            }
        }
    }

    @DexIgnore
    private final void sendResponseToWatch(DeviceAppResponse deviceAppResponse, DeviceData deviceData) {
        DeviceAppResponse deviceAppResponse2 = deviceAppResponse;
        DeviceData deviceData2 = deviceData;
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = getSerial();
        String tag = getTAG();
        remote.i(component, session, serial, tag, "Send respond: " + deviceAppResponse2);
        if (deviceData2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local.d(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + deviceAppResponse.toString());
            g90<qa4> sendRespond = getBleAdapter().sendRespond(FLogger.Session.HANDLE_WATCH_REQUEST, deviceData2);
            if (sendRespond != null) {
                g90<qa4> h = sendRespond.e(new DeviceCommunicator$sendResponseToWatch$Anon1(this, deviceAppResponse2));
                if (h != null) {
                    h.c(new DeviceCommunicator$sendResponseToWatch$Anon2(this, deviceAppResponse2));
                    return;
                }
                return;
            }
            return;
        }
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial2 = getSerial();
        String tag3 = getTAG();
        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
        remote2.e(component2, session2, serial2, tag3, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. device request type is not match with device response");
        FLogger.INSTANCE.getLocal().e(getTAG(), " .sendDeviceAppResponseFromQueue(), device request type is not match with device response or could not get DeviceData from DeviceAppResponse when forceUpdate");
    }

    @DexIgnore
    public void confirmStopWorkout(String str, boolean z) {
        kd4.b(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IReadWorkoutStateSession) {
            ((IReadWorkoutStateSession) currentSession).confirmStopWorkout(str, z);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".confirmStopWorkout() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.i(component, session, str, tag2, "confirmStopWorkout FAILED, currentSession=" + currentSession);
    }

    @DexIgnore
    public boolean disableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    public boolean enableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    public List<FitnessData> getSyncData() {
        if (getCurrentSession() instanceof SyncSession) {
            log("Current session is Sync Session, start get sync data.");
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                return ((SyncSession) currentSession).getSyncData();
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession");
        }
        log("Current session is not Sync Session, empty sync data.");
        return new ArrayList();
    }

    @DexIgnore
    public void handleEventReceived(Device device, DeviceEvent deviceEvent) {
        kd4.b(device, "device");
        kd4.b(deviceEvent, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleEventReceived, device = " + device + ".deviceInformation.serialNumber, event = " + deviceEvent);
        if (deviceEvent instanceof DeviceRequest) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.i(component, session, serial, tag2, "Receive device request, device=" + getSerial() + ", device event=" + deviceEvent);
            this.mDeviceAppRequests.put(deviceEvent.getDeviceEventId().name(), deviceEvent);
            Bundle bundle = new Bundle();
            if (deviceEvent instanceof RingPhoneRequest) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((RingPhoneRequest) deviceEvent).getAction().ordinal());
            } else if (deviceEvent instanceof CommuteTimeWatchAppRequest) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, CommuteTimeWatchAppAction.START.ordinal());
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((CommuteTimeWatchAppRequest) deviceEvent).getDestination());
            }
            getCommunicationResultCallback().onDeviceAppsRequest(deviceEvent.getDeviceEventId().ordinal(), bundle, getSerial());
        } else if (deviceEvent instanceof DeviceNotification) {
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.BLE;
            FLogger.Session session2 = FLogger.Session.OTHER;
            String serial2 = getSerial();
            String tag3 = getTAG();
            remote2.i(component2, session2, serial2, tag3, "Receive device notification, device=" + getSerial() + ", event=" + deviceEvent);
            Bundle bundle2 = new Bundle();
            if (deviceEvent instanceof MusicControlNotification) {
                bundle2.putInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.Companion.fromSDKMusicAction(((MusicControlNotification) deviceEvent).getAction()).ordinal());
            } else if (deviceEvent instanceof NotificationFilterSyncNotification) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((NotificationFilterSyncNotification) deviceEvent).getAction().ordinal());
            } else if (deviceEvent instanceof AlarmSyncNotification) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((AlarmSyncNotification) deviceEvent).getAction().ordinal());
            } else if (deviceEvent instanceof DeviceConfigSyncNotification) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((DeviceConfigSyncNotification) deviceEvent).getAction().ordinal());
            } else if (deviceEvent instanceof AppNotificationControlNotification) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((AppNotificationControlNotification) deviceEvent).getAction().ordinal());
            } else if (deviceEvent instanceof CommuteTimeWatchAppNotification) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((CommuteTimeWatchAppNotification) deviceEvent).getAction().ordinal());
            }
            getCommunicationResultCallback().onDeviceAppsRequest(deviceEvent.getDeviceEventId().ordinal(), bundle2, getSerial());
        } else if (deviceEvent instanceof MicroAppRequest) {
            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
            FLogger.Component component3 = FLogger.Component.BLE;
            FLogger.Session session3 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial3 = getSerial();
            String tag4 = getTAG();
            remote3.i(component3, session3, serial3, tag4, "Receive micro app request, device=" + getSerial() + ", event=" + deviceEvent);
            this.mDeviceAppRequests.put(deviceEvent.getDeviceEventId().name(), deviceEvent);
            getCommunicationResultCallback().onDeviceAppsRequest(deviceEvent.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else {
            super.handleEventReceived(device, deviceEvent);
        }
    }

    @DexIgnore
    public boolean onPing() {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onPing();
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onPing() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onPing FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public void onQuickCommandAction(Object obj) {
        kd4.b(obj, Constants.COMMAND);
        if (obj instanceof DeviceAppResponse) {
            sendDeviceAppResponseFromQueue((DeviceAppResponse) obj);
        } else if (obj instanceof MusicResponse) {
            sendMusicResponseFromQueue((MusicResponse) obj);
        } else if (obj instanceof NotificationBaseObj) {
            sendNotificationFromQueue((NotificationBaseObj) obj);
        }
    }

    @DexIgnore
    public boolean onReceiveCurrentSecretKey(byte[] bArr) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveCurrentSecretKey(bArr);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveCurrentSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveCurrentSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public boolean onReceiveServerRandomKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveRandomKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerRandomKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerRandomKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public boolean onReceiveServerSecretKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveServerSecretKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) {
        kd4.b(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (!(currentSession instanceof ISetWatchParamStateSession)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onSetWatchParamResponse() FAILED, current session=" + currentSession);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String tag2 = getTAG();
            remote.i(component, session, str, tag2, "onSetWatchParamResponse FAILED, currentSession=" + currentSession);
        } else if (!z) {
            ((ISetWatchParamStateSession) currentSession).onGetWatchParamFailed();
        } else if (watchParamsFileMapping == null) {
            ((ISetWatchParamStateSession) currentSession).doNextState();
        } else {
            ((ISetWatchParamStateSession) currentSession).setLatestWatchParam(str, watchParamsFileMapping);
        }
    }

    @DexIgnore
    public void setSecretKey(byte[] bArr) {
        BleAdapterImpl mBleAdapter = getMBleAdapter();
        FLogger.Session session = FLogger.Session.OTHER;
        if (bArr != null) {
            mBleAdapter.setSecretKey(session, bArr);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public boolean startCalibrationSession() {
        queueSessionAndStart(new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startCleanLinkMappingSession(List<? extends BLEMapping> list) {
        kd4.b(list, "mappings");
        return queueSessionAndStart(new ClearLinkMappingSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    public boolean startConnectionDeviceSession(boolean z) {
        queueSessionAndStart(new ConnectDeviceSession(z, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile) {
        kd4.b(firmwareData, "firmwareData");
        kd4.b(userProfile, "userProfile");
        queueSessionAndStart(new OtaSession(firmwareData, userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    public boolean startPairingSession(UserProfile userProfile) {
        kd4.b(userProfile, "userProfile");
        queueSessionAndStart(new PairingNewDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    public boolean startReadCurrentWorkoutSession() {
        queueSessionAndStart(new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z) {
        kd4.b(deviceAppResponse, "deviceAppResponse");
        deviceAppResponse.getLifeTimeObject().startExpireTimeCountDown();
        deviceAppResponse.setForceUpdate(z);
        addToQuickCommandQueue(deviceAppResponse);
    }

    @DexIgnore
    public synchronized void startSendMusicAppResponse(MusicResponse musicResponse) {
        kd4.b(musicResponse, "musicResponse");
        addToQuickCommandQueue(musicResponse);
    }

    @DexIgnore
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        kd4.b(notificationBaseObj, "newNotification");
        sendDianaNotification(notificationBaseObj);
        return true;
    }

    @DexIgnore
    public boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        kd4.b(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetAutoBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetAutoBiometricData(UserBiometricData userBiometricData) {
        kd4.b(userBiometricData, "userBiometricData");
        queueSessionAndStart(new SetAutoBiometricDataSession(userBiometricData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetAutoComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings) {
        kd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        queueSessionAndStart(new SetAutoComplicationSession(complicationAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetAutoMapping(List<? extends BLEMapping> list) {
        kd4.b(list, "mappings");
        return queueSessionAndStart(new SetAutoMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        kd4.b(list, "multipleAlarmList");
        queueSessionAndStart(new SetAutoMultiAlarmsSession(list, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        kd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetAutoNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetAutoSecondTimezone(String str) {
        kd4.b(str, "secondTimezoneId");
        return queueSessionAndStart(new SetAutoSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    public boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        kd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetAutoWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        kd4.b(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings) {
        kd4.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        queueSessionAndStart(new SetComplicationSession(complicationAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetFrontLightEnable(boolean z) {
        queueSessionAndStart(new SetFrontLightEnableSession(z, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetHeartRateMode(HeartRateMode heartRateMode) {
        kd4.b(heartRateMode, "heartRateMode");
        queueSessionAndStart(new SetHeartRateModeSession(heartRateMode, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetImplicitDeviceConfig(UserProfile userProfile) {
        kd4.b(userProfile, "userProfile");
        queueSessionAndStart(new SetImplicitDeviceConfigSession(userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit) {
        kd4.b(userDisplayUnit, "userDisplayUnit");
        queueSessionAndStart(new SetImplicitDisplayUnitSession(userDisplayUnit, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData) {
        kd4.b(inactiveNudgeData, "inactiveNudgeData");
        queueSessionAndStart(new SetInactiveNudgeConfigSession(inactiveNudgeData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetLinkMappingSession(List<? extends BLEMapping> list) {
        kd4.b(list, "mappings");
        return queueSessionAndStart(new SetLinkMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        kd4.b(localizationData, "localizationData");
        queueSessionAndStart(new SetAutoLocalizationDataSession(localizationData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        kd4.b(list, "multipleAlarmList");
        queueSessionAndStart(new SetMultiAlarmsSession(getBleAdapter(), list, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        kd4.b(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig) {
        queueSessionAndStart(new SetPresetAppsSession(complicationAppMappingSettings, watchAppMappingSettings, backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSetSecondTimezoneSession(String str) {
        kd4.b(str, "secondTimezoneId");
        return queueSessionAndStart(new SetSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    public boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        kd4.b(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startStopCurrentWorkoutSession() {
        queueSessionAndStart(new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSwitchDeviceSession(UserProfile userProfile) {
        kd4.b(userProfile, "userProfile");
        queueSessionAndStart(new SwitchActiveDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    public boolean startSyncingSession(UserProfile userProfile) {
        kd4.b(userProfile, "userProfile");
        queueSessionAndStart(new SyncSession(getBleAdapter(), getBleSessionCallback(), userProfile));
        return true;
    }
}
