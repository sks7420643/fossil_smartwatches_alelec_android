package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EnableMaintainingSession$onStart$$inlined$let$lambda$1 implements java.lang.Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession this$0;

    @DexIgnore
    public EnableMaintainingSession$onStart$$inlined$let$lambda$1(com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession enableMaintainingSession) {
        this.this$0 = enableMaintainingSession;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stop(com.misfit.frameworks.buttonservice.log.FailureCode.BLUETOOTH_IS_DISABLED);
    }
}
