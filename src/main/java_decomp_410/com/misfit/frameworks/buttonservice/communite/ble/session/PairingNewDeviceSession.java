package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseExchangeSecretKeySubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseOTASubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FirmwareUtils;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingNewDeviceSession extends BasePairingNewDeviceSession implements IPairDeviceSession, IExchangeKeySession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public int failureCode;

        @DexIgnore
        public CloseConnectionState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final int getFailureCode() {
            return this.failureCode;
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            PairingNewDeviceSession.this.getBleAdapter().closeConnection(PairingNewDeviceSession.this.getLogSession(), true);
            PairingNewDeviceSession.this.stop(this.failureCode);
            return true;
        }

        @DexIgnore
        public final void setFailureCode(int i) {
            this.failureCode = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        public ExchangeSecretKeySubFlow() {
            super(CommunicateMode.LINK, PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        public void onStop(int i) {
            if (i == FeatureErrorCode.REQUEST_UNSUPPORTED.getCode() || i == 0) {
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return;
            }
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (access$createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) access$createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class LinkServerState extends BleStateAbs {
        @DexIgnore
        public LinkServerState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p == null) {
                return true;
            }
            CommunicateMode communicateMode = CommunicateMode.LINK;
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(PairingNewDeviceSession.this.getBleAdapter()));
            bundle.putBoolean(Constants.IS_JUST_OTA, PairingNewDeviceSession.this.isJustUpdateFW());
            access$getBleSessionCallback$p.onAskForLinkServer(communicateMode, bundle);
            return true;
        }

        @DexIgnore
        public final void onLinkServerCompleted(boolean z, int i) {
            if (z) {
                PairingNewDeviceSession.this.stop(0);
                return;
            }
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (access$createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) access$createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTASubFlow extends BaseOTASubFlow {
        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public OTASubFlow() {
            super(null, null, null, null, null, null, null, null, null, null);
            throw null;
            // super(r1, PairingNewDeviceSession.this, r3, r4, r5, r6, r7, r8, PairingNewDeviceSession.this.getBleSessionCallback(), PairingNewDeviceSession.this.getCommunicationResultCallback());
            // String access$getTAG$p = PairingNewDeviceSession.this.getTAG();
            // MFLog mfLog = PairingNewDeviceSession.this.getMfLog();
            // FLogger.Session access$getLogSession$p = PairingNewDeviceSession.this.getLogSession();
            // String access$getSerial$p = PairingNewDeviceSession.this.getSerial();
            // BleAdapterImpl access$getBleAdapter$p = PairingNewDeviceSession.this.getBleAdapter();
            // FirmwareData access$getFirmwareData$p = PairingNewDeviceSession.this.getFirmwareData();
            // if (access$getFirmwareData$p != null) {
            //     byte[] access$getFirmwareBytes$p = PairingNewDeviceSession.this.getFirmwareBytes();
            //     if (access$getFirmwareBytes$p != null) {
            //         return;
            //     }
            //     kd4.a();
            //     throw null;
            // }
            // kd4.a();
            // throw null;
        }

        @DexIgnore
        public void onStop(int i) {
            if (i == 0) {
                PairingNewDeviceSession.this.setJustUpdateFW(true);
                BleSession.BleSessionCallback bleSessionCallback = getBleSessionCallback();
                if (bleSessionCallback != null) {
                    bleSessionCallback.onUpdateFirmwareSuccess();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return;
            }
            BleSession.BleSessionCallback bleSessionCallback2 = getBleSessionCallback();
            if (bleSessionCallback2 != null) {
                bleSessionCallback2.onUpdateFirmwareFailed();
            }
            addFailureCode(FailureCode.FAILED_TO_OTA);
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE);
            if (access$createConcreteState instanceof PairingCheckFirmware) {
                ((PairingCheckFirmware) access$createConcreteState).setFromOTAFailed(true);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PairingCheckFirmware extends BleStateAbs {
        @DexIgnore
        public boolean isFromOTAFailed;

        @DexIgnore
        public PairingCheckFirmware() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final boolean isFromOTAFailed() {
            return this.isFromOTAFailed;
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (this.isFromOTAFailed) {
                return true;
            }
            BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p == null) {
                return true;
            }
            Bundle bundle = new Bundle();
            bundle.putString("device_model", PairingNewDeviceSession.this.getBleAdapter().getDeviceModel());
            access$getBleSessionCallback$p.onRequestLatestFirmware(bundle);
            return true;
        }

        @DexIgnore
        public final void onReceiveLatestFirmwareData(FirmwareData firmwareData) {
            kd4.b(firmwareData, "firmwareData");
            if ((firmwareData instanceof SkipFirmwareData) || qf4.b(PairingNewDeviceSession.this.getBleAdapter().getFirmwareVersion(), firmwareData.getFirmwareVersion(), true)) {
                BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
                if (access$getBleSessionCallback$p != null) {
                    access$getBleSessionCallback$p.onFirmwareLatest();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return;
            }
            String firmwareVersion = firmwareData.getFirmwareVersion();
            String checkSum = firmwareData.getCheckSum();
            boolean isEmbedded = firmwareData.isEmbedded();
            byte[] readFirmware = FirmwareUtils.INSTANCE.readFirmware(firmwareData, PairingNewDeviceSession.this.getContext());
            PairingNewDeviceSession.this.log("Verifying firmware...");
            PairingNewDeviceSession pairingNewDeviceSession2 = PairingNewDeviceSession.this;
            pairingNewDeviceSession2.log("- Version: " + firmwareVersion);
            PairingNewDeviceSession pairingNewDeviceSession3 = PairingNewDeviceSession.this;
            pairingNewDeviceSession3.log("- Checksum: " + checkSum);
            PairingNewDeviceSession pairingNewDeviceSession4 = PairingNewDeviceSession.this;
            pairingNewDeviceSession4.log("- Length: " + readFirmware.length);
            PairingNewDeviceSession pairingNewDeviceSession5 = PairingNewDeviceSession.this;
            pairingNewDeviceSession5.log("- In-app Bundled: " + isEmbedded);
            if (isEmbedded) {
                PairingNewDeviceSession.this.log("In-app bundled firmware, skip verifying!");
            } else if (FirmwareUtils.INSTANCE.verifyFirmware(readFirmware, checkSum)) {
                PairingNewDeviceSession.this.log("Verified: OK");
            } else {
                PairingNewDeviceSession.this.log("Verified: FAILED. OTA: FAILED");
                PairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_OTA_FILE_NOT_READY);
            }
            PairingNewDeviceSession.this.setFirmwareData(firmwareData);
            PairingNewDeviceSession.this.setFirmwareBytes(readFirmware);
            PairingNewDeviceSession pairingNewDeviceSession6 = PairingNewDeviceSession.this;
            pairingNewDeviceSession6.enterStateAsync(pairingNewDeviceSession6.createConcreteState(BleSessionAbs.SessionState.OTA_STATE));
        }

        @DexIgnore
        public final void setFromOTAFailed(boolean z) {
            this.isFromOTAFailed = z;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        public TransferDataSubFlow() {
            super(PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getUserProfile(), PairingNewDeviceSession.this.getBleSessionCallback(), true);
        }

        @DexIgnore
        public void onStop(int i) {
            if (i != 0) {
                PairingNewDeviceSession.this.stop(i);
                return;
            }
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        public /* final */ /* synthetic */ PairingNewDeviceSession this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public TransferSettingsSubFlow(PairingNewDeviceSession pairingNewDeviceSession) {
            super(pairingNewDeviceSession.getTAG(), pairingNewDeviceSession, pairingNewDeviceSession.getMfLog(), pairingNewDeviceSession.getLogSession(), true, pairingNewDeviceSession.getSerial(), pairingNewDeviceSession.getBleAdapter(), pairingNewDeviceSession.getUserProfile(), pairingNewDeviceSession.multiAlarmSettings, pairingNewDeviceSession.complicationAppMappingSettings, pairingNewDeviceSession.watchAppMappingSettings, pairingNewDeviceSession.backgroundConfig, pairingNewDeviceSession.notificationFilterSettings, pairingNewDeviceSession.localizationData, pairingNewDeviceSession.microAppMappings, pairingNewDeviceSession.secondTimezoneOffset, pairingNewDeviceSession.getBleSessionCallback());
            this.this$Anon0 = pairingNewDeviceSession;
        }

        @DexIgnore
        public void onStop(int i) {
            PairingNewDeviceSession pairingNewDeviceSession = this.this$Anon0;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.LINK_SERVER));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingNewDeviceSession(UserProfile userProfile, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(userProfile, bleAdapterImpl, bleSessionCallback, communicationResultCallback);
        kd4.b(userProfile, "userProfile");
        kd4.b(bleAdapterImpl, "bleAdapterV2");
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.PAIR);
        userProfile.setNewDevice(true);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        kd4.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public BleSession copyObject() {
        PairingNewDeviceSession pairingNewDeviceSession = new PairingNewDeviceSession(getUserProfile(), getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback());
        pairingNewDeviceSession.setDevice(getDevice());
        return pairingNewDeviceSession;
    }

    @DexIgnore
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW;
        String name = ExchangeSecretKeySubFlow.class.getName();
        kd4.a((Object) name, "ExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name2 = TransferDataSubFlow.class.getName();
        kd4.a((Object) name2, "TransferDataSubFlow::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE;
        String name3 = PairingCheckFirmware.class.getName();
        kd4.a((Object) name3, "PairingCheckFirmware::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.OTA_STATE;
        String name4 = OTASubFlow.class.getName();
        kd4.a((Object) name4, "OTASubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name5 = TransferSettingsSubFlow.class.getName();
        kd4.a((Object) name5, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap6 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState6 = BleSessionAbs.SessionState.LINK_SERVER;
        String name6 = LinkServerState.class.getName();
        kd4.a((Object) name6, "LinkServerState::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap7 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState7 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name7 = CloseConnectionState.class.getName();
        kd4.a((Object) name7, "CloseConnectionState::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
    }

    @DexIgnore
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).onGetWatchParamFailed();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void onLinkServerSuccess(boolean z, int i) {
        BleState currentState = getCurrentState();
        if (currentState instanceof LinkServerState) {
            ((LinkServerState) currentState).onLinkServerCompleted(z, i);
        }
    }

    @DexIgnore
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        kd4.b(str, "serial");
        kd4.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void updateFirmware(FirmwareData firmwareData) {
        kd4.b(firmwareData, "firmwareData");
        BleState currentState = getCurrentState();
        if (currentState instanceof PairingCheckFirmware) {
            ((PairingCheckFirmware) currentState).onReceiveLatestFirmwareData(firmwareData);
        }
    }
}
