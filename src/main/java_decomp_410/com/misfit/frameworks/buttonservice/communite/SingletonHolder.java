package com.misfit.frameworks.buttonservice.communite;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SingletonHolder<T, A> {
    @DexIgnore
    public xc4<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(xc4<? super A, ? extends T> xc4) {
        kd4.b(xc4, "creator");
        this.creator = xc4;
    }

    @DexIgnore
    public final T getInstance(A a) {
        throw null;
        // T t;
        // T t2 = this.instance;
        // if (t2 != null) {
        //     return t2;
        // }
        // synchronized (this) {
        //     t = this.instance;
        //     if (t == null) {
        //         xc4 xc4 = this.creator;
        //         if (xc4 != null) {
        //             t = xc4.invoke(a);
        //             this.instance = t;
        //             this.creator = null;
        //         } else {
        //             kd4.a();
        //             throw null;
        //         }
        //     }
        // }
        // return t;
    }
}
