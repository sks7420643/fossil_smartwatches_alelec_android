package com.misfit.frameworks.buttonservice.extensions;

import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SynchronizeSetQueue<T> {
    @DexIgnore
    public /* final */ HashSet<T> hashSet; // = new HashSet<>();
    @DexIgnore
    public /* final */ Object locker; // = new Object();

    @DexIgnore
    public final void add(T t) {
        synchronized (this.locker) {
            this.hashSet.add(t);
        }
    }

    @DexIgnore
    public final void clear() {
        throw null;
        // synchronized (this.locker) {
        //     this.hashSet.clear();
        //     qa4 qa4 = qa4.a;
        // }
    }

    @DexIgnore
    public final int getSize() {
        return this.hashSet.size();
    }

    @DexIgnore
    public final boolean isEmpty() {
        boolean isEmpty;
        synchronized (this.locker) {
            isEmpty = this.hashSet.isEmpty();
        }
        return isEmpty;
    }

    @DexIgnore
    public final T poll() {
        T t;
        synchronized (this.locker) {
            Iterator<T> it = this.hashSet.iterator();
            kd4.a((Object) it, "hashSet.iterator()");
            if (it.hasNext()) {
                t = it.next();
                this.hashSet.remove(t);
            } else {
                t = null;
            }
        }
        return t;
    }

    @DexIgnore
    public final void remove(T t) {
        synchronized (this.locker) {
            this.hashSet.remove(t);
        }
    }

    @DexIgnore
    public final SynchronizeSetQueue<T> sortWith(Comparator<? super T> comparator) {
        throw null;
        // kd4.b(comparator, "comparator");
        // kb4.a(this.hashSet, comparator);
        // return this;
    }
}
