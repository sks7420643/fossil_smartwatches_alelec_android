package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class BLEMappingDeserializer implements vz1<BLEMapping> {
    @DexIgnore
    public BLEMapping deserialize(JsonElement jsonElement, Type type, uz1 uz1) throws JsonParseException {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().a("mType").b();
        rz1 rz1 = new rz1();
        rz1.a(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) rz1.a().a(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) rz1.a().a(jsonElement, MicroAppMapping.class);
    }
}
