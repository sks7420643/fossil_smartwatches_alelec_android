package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rz1;
import com.fossil.blesdk.obfuscated.uz1;
import com.fossil.blesdk.obfuscated.vz1;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppMappingDeserializer implements vz1<WatchAppMapping> {
    @DexIgnore
    public WatchAppMapping deserialize(JsonElement jsonElement, Type type, uz1 uz1) throws JsonParseException {
        kd4.b(jsonElement, "json");
        kd4.b(type, "typeOfT");
        kd4.b(uz1, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String name = WatchAppMappingDeserializer.class.getName();
        kd4.a((Object) name, "WatchAppMappingDeserializer::class.java.name");
        local.d(name, jsonElement.toString());
        JsonElement a = jsonElement.d().a(WatchAppMapping.Companion.getFIELD_TYPE());
        kd4.a((Object) a, "jsonType");
        int b = a.b();
        rz1 rz1 = new rz1();
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getDIAGNOTICS()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, DiagnosticsWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, WellnessWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWORK_OUT()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, WorkoutWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getEMPTY()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, NoneWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getMUSIC()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, MusicWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getNOTIFICATION_PANEL()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, NotificationPanelWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getSTOP_WATCH()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, StopWatchWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, TimerWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getWEATHER()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, WeatherWatchAppMapping.class);
        }
        if (b == WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME()) {
            return (WatchAppMapping) rz1.a().a(jsonElement, CommuteTimeWatchAppMapping.class);
        }
        return null;
    }
}
