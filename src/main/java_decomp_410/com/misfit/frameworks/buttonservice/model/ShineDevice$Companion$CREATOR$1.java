package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ShineDevice$Companion$CREATOR$1 implements android.os.Parcelable.Creator<com.misfit.frameworks.buttonservice.model.ShineDevice> {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.ShineDevice createFromParcel(android.os.Parcel parcel) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(parcel, "parcel");
        return new com.misfit.frameworks.buttonservice.model.ShineDevice(parcel);
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.ShineDevice[] newArray(int i) {
        return new com.misfit.frameworks.buttonservice.model.ShineDevice[i];
    }
}
