package com.misfit.frameworks.buttonservice.model.notification;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import com.fossil.blesdk.device.data.notification.NotificationType;
import com.fossil.blesdk.device.data.notification.NotificationVibePattern;
import com.fossil.blesdk.model.file.NotificationIcon;
import com.fossil.blesdk.model.notification.filter.NotificationIconConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.pc4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qc4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotificationFilter implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public static /* final */ int IS_FIELD_EXIST; // = 1;
    @DexIgnore
    public static /* final */ int IS_FIELD_NOT_EXIST; // = 0;
    @DexIgnore
    public FNotification fNotification;
    @DexIgnore
    public NotificationHandMovingConfig handMovingConfig;
    @DexIgnore
    public Short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public NotificationVibePattern vibePattern;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilter> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AppNotificationFilter createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AppNotificationFilter(parcel, (fd4) null);
        }

        @DexIgnore
        public AppNotificationFilter[] newArray(int i) {
            return new AppNotificationFilter[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationFilter(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AppNotificationFilter)) {
            return false;
        }
        AppNotificationFilter appNotificationFilter = (AppNotificationFilter) obj;
        if (!kd4.a((Object) this.fNotification, (Object) appNotificationFilter.fNotification) || !kd4.a((Object) this.sender, (Object) appNotificationFilter.sender) || !kd4.a((Object) this.priority, (Object) appNotificationFilter.priority)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getBundleCrc() {
        return this.fNotification.getBundleCrc();
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final Short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.handMovingConfig = notificationHandMovingConfig;
    }

    @DexIgnore
    public final void setPriority(Short sh) {
        this.priority = sh;
    }

    @DexIgnore
    public final void setSender(String str) {
        this.sender = str;
    }

    @DexIgnore
    public final void setVibePattern(NotificationVibePattern notificationVibePattern) {
        this.vibePattern = notificationVibePattern;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bb, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r9, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bf, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0132, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r9, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0136, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0139, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        com.fossil.blesdk.obfuscated.qc4.a(r2, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x013d, code lost:
        throw r1;
     */
    @DexIgnore
    public final NotificationFilter toSDKNotificationFilter(Context context, boolean z) {
        NotificationFilter notificationFilter;
        kd4.b(context, "context");
        if (this.handMovingConfig == null || this.vibePattern == null) {
            notificationFilter = new NotificationFilter(this.fNotification.getBundleCrc(), this.fNotification.getGroupId());
        } else {
            long bundleCrc = this.fNotification.getBundleCrc();
            byte groupId = this.fNotification.getGroupId();
            NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
            if (notificationHandMovingConfig != null) {
                NotificationVibePattern notificationVibePattern = this.vibePattern;
                if (notificationVibePattern != null) {
                    notificationFilter = new NotificationFilter(bundleCrc, groupId, notificationHandMovingConfig, notificationVibePattern);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        String str = this.sender;
        if (str != null) {
            notificationFilter.setSender(str);
        }
        Short sh = this.priority;
        if (sh != null) {
            notificationFilter.setPriority(sh.shortValue());
        }
        if (z) {
            if (this.fNotification.getBundleCrc() == DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getBundleCrc() || this.fNotification.getBundleCrc() == DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL.getBundleCrc()) {
                try {
                    InputStream open = context.getAssets().open(DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getIconFwPath());
                    String iconFwPath = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getIconFwPath();
                    kd4.a((Object) open, "it");
                    NotificationIcon notificationIcon = new NotificationIcon(iconFwPath, pc4.a(open));
                    qc4.a(open, (Throwable) null);
                    InputStream open2 = context.getAssets().open(DianaNotificationObj.AApplicationName.PHONE_MISSED_CALL.getIconFwPath());
                    String iconFwPath2 = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL.getIconFwPath();
                    kd4.a((Object) open2, "it");
                    NotificationIcon notificationIcon2 = new NotificationIcon(iconFwPath2, pc4.a(open2));
                    qc4.a(open2, (Throwable) null);
                    notificationFilter.setIconConfig(new NotificationIconConfig(notificationIcon).setIconForType(NotificationType.INCOMING_CALL, notificationIcon).setIconForType(NotificationType.MISSED_CALL, notificationIcon2));
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    e.printStackTrace();
                    local.e("toSDKNotificationFilter", String.valueOf(qa4.a));
                }
            } else {
                if (this.fNotification.getIconFwPath().length() > 0) {
                    try {
                        InputStream open3 = context.getAssets().open(this.fNotification.getIconFwPath());
                        String iconFwPath3 = this.fNotification.getIconFwPath();
                        kd4.a((Object) open3, "it");
                        notificationFilter.setIconConfig(new NotificationIconConfig(new NotificationIcon(iconFwPath3, pc4.a(open3))));
                        qc4.a(open3, (Throwable) null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        e2.printStackTrace();
                        local2.e("toSDKNotificationFilter", String.valueOf(qa4.a));
                    }
                }
            }
        }
        return notificationFilter;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeParcelable(this.fNotification, 0);
        String str = this.sender;
        if (str != null) {
            parcel.writeInt(1);
            parcel.writeString(str);
        } else {
            parcel.writeInt(0);
        }
        Short sh = this.priority;
        if (sh != null) {
            short shortValue = sh.shortValue();
            parcel.writeInt(1);
            parcel.writeInt(shortValue);
        } else {
            parcel.writeInt(0);
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            parcel.writeInt(1);
            parcel.writeParcelable(notificationHandMovingConfig, 0);
        } else {
            parcel.writeInt(0);
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            parcel.writeInt(1);
            parcel.writeInt(notificationVibePattern.ordinal());
            return;
        }
        parcel.writeInt(0);
    }

    @DexIgnore
    public AppNotificationFilter(FNotification fNotification2) {
        kd4.b(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public AppNotificationFilter(Parcel parcel) {
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification(parcel) : fNotification2;
        if (parcel.readInt() == 1) {
            this.sender = parcel.readString();
        }
        if (parcel.readInt() == 1) {
            this.priority = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readInt() == 1) {
            this.handMovingConfig = (NotificationHandMovingConfig) parcel.readParcelable(HandMovingConfig.class.getClassLoader());
        }
        if (parcel.readInt() == 1) {
            this.vibePattern = NotificationVibePattern.values()[parcel.readInt()];
        }
    }
}
