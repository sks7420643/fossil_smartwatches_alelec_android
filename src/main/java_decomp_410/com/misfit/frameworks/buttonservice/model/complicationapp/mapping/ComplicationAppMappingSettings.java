package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.complication.ComplicationConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<ComplicationAppMappingSettings> CREATOR; // = new ComplicationAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public ComplicationAppMapping bottomAppMapping;
    @DexIgnore
    public ComplicationAppMapping leftAppMapping;
    @DexIgnore
    public ComplicationAppMapping rightAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public ComplicationAppMapping topAppMapping;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final long compareTimeStamp(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            long j = 0;
            long timeStamp = complicationAppMappingSettings != null ? complicationAppMappingSettings.getTimeStamp() : 0;
            if (complicationAppMappingSettings2 != null) {
                j = complicationAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            if ((complicationAppMappingSettings != null || complicationAppMappingSettings2 == null) && (complicationAppMappingSettings == null || complicationAppMappingSettings2 != null)) {
                return kd4.a((Object) complicationAppMappingSettings, (Object) complicationAppMappingSettings2);
            }
            return false;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationAppMappingSettings(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ComplicationAppMappingSettings)) {
            return false;
        }
        return kd4.a((Object) getHash(), (Object) ((ComplicationAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":" + this.leftAppMapping + ":" + this.rightAppMapping;
        kd4.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final ComplicationConfig toSDKSetting() {
        return new ComplicationConfig(this.topAppMapping.toSDKSetting(), this.rightAppMapping.toSDKSetting(), this.bottomAppMapping.toSDKSetting(), this.leftAppMapping.toSDKSetting());
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
        parcel.writeParcelable(this.leftAppMapping, i);
        parcel.writeParcelable(this.rightAppMapping, i);
    }

    @DexIgnore
    public ComplicationAppMappingSettings(ComplicationAppMapping complicationAppMapping, ComplicationAppMapping complicationAppMapping2, ComplicationAppMapping complicationAppMapping3, ComplicationAppMapping complicationAppMapping4, long j) {
        kd4.b(complicationAppMapping, "topAppMapping");
        kd4.b(complicationAppMapping2, "bottomAppMapping");
        kd4.b(complicationAppMapping3, "leftAppMapping");
        kd4.b(complicationAppMapping4, "rightAppMapping");
        this.topAppMapping = complicationAppMapping;
        this.bottomAppMapping = complicationAppMapping2;
        this.leftAppMapping = complicationAppMapping3;
        this.rightAppMapping = complicationAppMapping4;
        this.timeStamp = j;
    }

    @DexIgnore
    public ComplicationAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        ComplicationAppMapping complicationAppMapping = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.topAppMapping = complicationAppMapping == null ? new CaloriesComplicationAppMapping() : complicationAppMapping;
        ComplicationAppMapping complicationAppMapping2 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.bottomAppMapping = complicationAppMapping2 == null ? new ActiveMinutesComplicationAppMapping() : complicationAppMapping2;
        ComplicationAppMapping complicationAppMapping3 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.leftAppMapping = complicationAppMapping3 == null ? new DateComplicationAppMapping() : complicationAppMapping3;
        ComplicationAppMapping complicationAppMapping4 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.rightAppMapping = complicationAppMapping4 == null ? new ChanceOfRainComplicationAppMapping() : complicationAppMapping4;
    }
}
