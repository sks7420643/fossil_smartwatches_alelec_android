package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.RingPhoneRequest;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.model.devicedata.RingPhoneData;
import com.fossil.blesdk.model.enumerate.RingPhoneState;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingPhoneComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public RingPhoneState mRingPhoneState; // = RingPhoneState.OFF;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(RingPhoneState ringPhoneState) {
        super(DeviceEventId.RING_PHONE);
        kd4.b(ringPhoneState, "ringPhoneState");
        this.mRingPhoneState = ringPhoneState;
    }

    @DexIgnore
    public final RingPhoneState getMRingPhoneState() {
        return this.mRingPhoneState;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return new RingPhoneData(this.mRingPhoneState);
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (deviceRequest instanceof RingPhoneRequest) {
            return new RingPhoneData((RingPhoneRequest) deviceRequest, this.mRingPhoneState);
        }
        return null;
    }

    @DexIgnore
    public final void setMRingPhoneState(RingPhoneState ringPhoneState) {
        kd4.b(ringPhoneState, "<set-?>");
        this.mRingPhoneState = ringPhoneState;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mRingPhoneState.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.mRingPhoneState = RingPhoneState.values()[parcel.readInt()];
    }
}
