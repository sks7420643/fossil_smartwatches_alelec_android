package com.misfit.frameworks.buttonservice.model.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationBaseObj$Companion$CREATOR$1 implements android.os.Parcelable.Creator<com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj> {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj createFromParcel(android.os.Parcel parcel) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(parcel, "parcel");
        java.lang.String readString = parcel.readString();
        if (readString != null) {
            try {
                java.lang.Class<?> cls = java.lang.Class.forName(readString);
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) cls, "Class.forName(dynamicClassName!!)");
                java.lang.reflect.Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new java.lang.Class[]{android.os.Parcel.class});
                com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                declaredConstructor.setAccessible(true);
                java.lang.Object newInstance = declaredConstructor.newInstance(new java.lang.Object[]{parcel});
                if (newInstance != null) {
                    return (com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj) newInstance;
                }
                throw new kotlin.TypeCastException("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj");
            } catch (java.lang.ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (java.lang.NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (java.lang.IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (java.lang.InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (java.lang.reflect.InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj[] newArray(int i) {
        return new com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj[i];
    }
}
