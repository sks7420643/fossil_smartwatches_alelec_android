package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.customization.MicroAppCustomization;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BLECustomization implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<BLECustomization> CREATOR; // = new BLECustomization$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String FIELD_TYPE; // = "type";
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public int type;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class BLEMappingCustomizationType {
        @DexIgnore
        public static /* final */ int GOAL_TRACKING_TYPE; // = 1;
        @DexIgnore
        public static /* final */ BLEMappingCustomizationType INSTANCE; // = new BLEMappingCustomizationType();
        @DexIgnore
        public static /* final */ int NON_TYPE; // = 0;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Parcelable.Creator<BLECustomization> getCREATOR() {
            return BLECustomization.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<BLECustomization> creator) {
            BLECustomization.CREATOR = creator;
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public BLECustomization(int i) {
        String name = getClass().getName();
        kd4.a((Object) name, "javaClass.name");
        this.className = name;
        this.type = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BLECustomization)) {
            return false;
        }
        return qf4.b(getHash(), ((BLECustomization) obj).getHash(), true);
    }

    @DexIgnore
    public abstract MicroAppCustomization getCustomizationFrame();

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "dest");
        parcel.writeString(this.className);
        parcel.writeInt(this.type);
    }

    @DexIgnore
    public BLECustomization(Parcel parcel) {
        kd4.b(parcel, "in");
        String name = getClass().getName();
        kd4.a((Object) name, "javaClass.name");
        this.className = name;
        this.type = parcel.readInt();
    }
}
