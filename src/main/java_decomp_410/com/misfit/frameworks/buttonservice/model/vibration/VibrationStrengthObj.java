package com.misfit.frameworks.buttonservice.model.vibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VibrationStrengthObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<VibrationStrengthObj> CREATOR; // = new VibrationStrengthObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public /* final */ boolean isDefaultValue;
    @DexIgnore
    public /* final */ int vibrationStrengthLevel;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[VibeStrengthConfig.VibeStrengthLevel.values().length];

            /*
            static {
                $EnumSwitchMapping$Anon0[VibeStrengthConfig.VibeStrengthLevel.LOW.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[VibeStrengthConfig.VibeStrengthLevel.MEDIUM.ordinal()] = 2;
                $EnumSwitchMapping$Anon0[VibeStrengthConfig.VibeStrengthLevel.HIGH.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ VibrationStrengthObj consumeSDKVibrationStrengthLevel$default(Companion companion, VibeStrengthConfig.VibeStrengthLevel vibeStrengthLevel, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return companion.consumeSDKVibrationStrengthLevel(vibeStrengthLevel, z);
        }

        @DexIgnore
        public final VibrationStrengthObj consumeSDKVibrationStrengthLevel(VibeStrengthConfig.VibeStrengthLevel vibeStrengthLevel, boolean z) {
            kd4.b(vibeStrengthLevel, "vibrationStrengthLevel");
            int i = WhenMappings.$EnumSwitchMapping$Anon0[vibeStrengthLevel.ordinal()];
            int i2 = 2;
            if (i == 1) {
                i2 = 1;
            } else if (i != 2 && i == 3) {
                i2 = 3;
            }
            return new VibrationStrengthObj(i2, z);
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public VibrationStrengthObj(int i, boolean z) {
        this.vibrationStrengthLevel = i;
        this.isDefaultValue = z;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getVibrationStrengthLevel() {
        return this.vibrationStrengthLevel;
    }

    @DexIgnore
    public final boolean isDefaultValue() {
        return this.isDefaultValue;
    }

    @DexIgnore
    public final VibeStrengthConfig.VibeStrengthLevel toSDKVibrationStrengthLevel() {
        int i = this.vibrationStrengthLevel;
        if (i == 1) {
            return VibeStrengthConfig.VibeStrengthLevel.LOW;
        }
        if (i == 2) {
            return VibeStrengthConfig.VibeStrengthLevel.MEDIUM;
        }
        if (i != 3) {
            return VibeStrengthConfig.VibeStrengthLevel.MEDIUM;
        }
        return VibeStrengthConfig.VibeStrengthLevel.HIGH;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(VibrationStrengthObj.class.getName());
        parcel.writeInt(this.vibrationStrengthLevel);
        parcel.writeInt(this.isDefaultValue ? 1 : 0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ VibrationStrengthObj(int i, boolean z, int i2, fd4 fd4) {
        this(i, (i2 & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public VibrationStrengthObj(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt() != 1 ? false : true);
        kd4.b(parcel, "parcel");
    }
}
