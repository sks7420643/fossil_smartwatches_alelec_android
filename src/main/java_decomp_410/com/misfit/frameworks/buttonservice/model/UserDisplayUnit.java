package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.data.model.MFUser;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserDisplayUnit implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public DistanceUnit distanceUnit;
    @DexIgnore
    public TemperatureUnit temperatureUnit;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<UserDisplayUnit> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public UserDisplayUnit createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new UserDisplayUnit(parcel);
        }

        @DexIgnore
        public UserDisplayUnit[] newArray(int i) {
            return new UserDisplayUnit[i];
        }
    }

    @DexIgnore
    public enum DistanceUnit {
        KM("KM"),
        MILE("MILE");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final DistanceUnit fromValue(String str) {
                DistanceUnit distanceUnit;
                kd4.b(str, "value");
                DistanceUnit[] values = DistanceUnit.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        distanceUnit = null;
                        break;
                    }
                    distanceUnit = values[i];
                    if (kd4.a((Object) distanceUnit.getValue(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                return distanceUnit != null ? distanceUnit : DistanceUnit.KM;
            }

            @DexIgnore
            public /* synthetic */ Companion(fd4 fd4) {
                this();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[DistanceUnit.values().length];
                $EnumSwitchMapping$Anon0[DistanceUnit.KM.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[DistanceUnit.MILE.ordinal()] = 2;
            }
            */
        }

        /*
        static {
            Companion = new Companion((fd4) null);
        }
        */

        @DexIgnore
        DistanceUnit(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final com.fossil.blesdk.device.data.enumerate.DistanceUnit toSDKDistanceUnit() {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
            if (i == 1) {
                return com.fossil.blesdk.device.data.enumerate.DistanceUnit.KM;
            }
            if (i == 2) {
                return com.fossil.blesdk.device.data.enumerate.DistanceUnit.MILE;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public enum TemperatureUnit {
        C("C"),
        F(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final TemperatureUnit fromValue(String str) {
                TemperatureUnit temperatureUnit;
                kd4.b(str, "value");
                TemperatureUnit[] values = TemperatureUnit.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        temperatureUnit = null;
                        break;
                    }
                    temperatureUnit = values[i];
                    if (kd4.a((Object) temperatureUnit.getValue(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                return temperatureUnit != null ? temperatureUnit : TemperatureUnit.C;
            }

            @DexIgnore
            public /* synthetic */ Companion(fd4 fd4) {
                this();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = null;

            /*
            static {
                $EnumSwitchMapping$Anon0 = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$Anon0[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        /*
        static {
            Companion = new Companion((fd4) null);
        }
        */

        @DexIgnore
        TemperatureUnit(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final com.fossil.blesdk.device.data.enumerate.TemperatureUnit toSDKTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$Anon0[ordinal()];
            if (i == 1) {
                return com.fossil.blesdk.device.data.enumerate.TemperatureUnit.C;
            }
            if (i == 2) {
                return com.fossil.blesdk.device.data.enumerate.TemperatureUnit.F;
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public UserDisplayUnit(TemperatureUnit temperatureUnit2, DistanceUnit distanceUnit2) {
        kd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        kd4.b(distanceUnit2, MFUser.DISTANCE_UNIT);
        this.temperatureUnit = temperatureUnit2;
        this.distanceUnit = distanceUnit2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final DistanceUnit getDistanceUnit() {
        return this.distanceUnit;
    }

    @DexIgnore
    public final TemperatureUnit getTemperatureUnit() {
        return this.temperatureUnit;
    }

    @DexIgnore
    public final void setDistanceUnit(DistanceUnit distanceUnit2) {
        kd4.b(distanceUnit2, "<set-?>");
        this.distanceUnit = distanceUnit2;
    }

    @DexIgnore
    public final void setTemperatureUnit(TemperatureUnit temperatureUnit2) {
        kd4.b(temperatureUnit2, "<set-?>");
        this.temperatureUnit = temperatureUnit2;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a((Object) this);
        kd4.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "dest");
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeString(this.distanceUnit.getValue());
    }

    @DexIgnore
    public UserDisplayUnit(Parcel parcel) {
        kd4.b(parcel, "parcel");
        TemperatureUnit.Companion companion = TemperatureUnit.Companion;
        String readString = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString == null ? TemperatureUnit.C.getValue() : readString);
        DistanceUnit.Companion companion2 = DistanceUnit.Companion;
        String readString2 = parcel.readString();
        this.distanceUnit = companion2.fromValue(readString2 == null ? TemperatureUnit.C.getValue() : readString2);
    }
}
