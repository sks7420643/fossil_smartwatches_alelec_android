package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationAppMappingSettings$Companion$CREATOR$1 implements android.os.Parcelable.Creator<com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings> {
    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings createFromParcel(android.os.Parcel parcel) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(parcel, "parcel");
        return new com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings(parcel, (com.fossil.blesdk.obfuscated.fd4) null);
    }

    @DexIgnore
    public com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings[] newArray(int i) {
        return new com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings[i];
    }
}
