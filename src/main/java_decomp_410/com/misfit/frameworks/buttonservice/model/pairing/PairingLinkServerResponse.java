package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PairingLinkServerResponse extends PairingResponse {
    @DexIgnore
    public int failureCode;
    @DexIgnore
    public /* final */ boolean isSuccess;

    @DexIgnore
    public PairingLinkServerResponse(boolean z, int i) {
        this.isSuccess = z;
        this.failureCode = i;
    }

    @DexIgnore
    public final int getFailureCode() {
        return this.failureCode;
    }

    @DexIgnore
    public final boolean isSuccess() {
        return this.isSuccess;
    }

    @DexIgnore
    public final void setFailureCode(int i) {
        this.failureCode = i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.isSuccess ? 1 : 0);
        parcel.writeInt(this.failureCode);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingLinkServerResponse(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.isSuccess = parcel.readInt() != 0;
        this.failureCode = parcel.readInt();
    }
}
