package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.device.data.enumerate.TemperatureUnit;
import com.fossil.blesdk.device.data.weather.CurrentWeatherInfo;
import com.fossil.blesdk.device.data.weather.WeatherDayForecast;
import com.fossil.blesdk.device.data.weather.WeatherHourForecast;
import com.fossil.blesdk.device.data.weather.WeatherInfo;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.portfolio.platform.data.model.MFUser;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public /* final */ CurrentWeatherInfo currentWeatherInfo;
    @DexIgnore
    public /* final */ List<WeatherDayForecast> dayForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public /* final */ List<WeatherHourForecast> hourForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ UserDisplayUnit.TemperatureUnit temperatureUnit;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherWatchAppInfo(parcel);
        }

        @DexIgnore
        public WeatherWatchAppInfo[] newArray(int i) {
            return new WeatherWatchAppInfo[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppInfo(String str, UserDisplayUnit.TemperatureUnit temperatureUnit2, CurrentWeatherInfo currentWeatherInfo2, List<WeatherDayForecast> list, List<WeatherHourForecast> list2, long j) {
        kd4.b(str, PlaceFields.LOCATION);
        kd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        kd4.b(currentWeatherInfo2, "currentWeatherInfo");
        kd4.b(list, "dayForecast");
        kd4.b(list2, "hourForecast");
        this.location = str;
        this.temperatureUnit = temperatureUnit2;
        this.currentWeatherInfo = currentWeatherInfo2;
        this.dayForecast.addAll(list);
        this.hourForecast.addAll(list2);
        this.expiredAt = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final WeatherInfo toSDKWeatherAppInfo() {
        List<WeatherDayForecast> list = this.dayForecast;
        ArrayList arrayList = new ArrayList(db4.a(list, 10));
        for (WeatherDayForecast sDKWeatherDayForecast : list) {
            arrayList.add(sDKWeatherDayForecast.toSDKWeatherDayForecast());
        }
        List<WeatherHourForecast> list2 = this.hourForecast;
        ArrayList arrayList2 = new ArrayList(db4.a(list2, 10));
        for (WeatherHourForecast sDKWeatherHourForecast : list2) {
            arrayList2.add(sDKWeatherHourForecast.toSDKWeatherHourForecast());
        }
        long j = this.expiredAt;
        String str = this.location;
        TemperatureUnit sDKTemperatureUnit = this.temperatureUnit.toSDKTemperatureUnit();
        CurrentWeatherInfo sDKCurrentWeatherInfo = this.currentWeatherInfo.toSDKCurrentWeatherInfo();
        Object[] array = arrayList2.toArray(new WeatherHourForecast[0]);
        if (array != null) {
            WeatherHourForecast[] weatherHourForecastArr = (WeatherHourForecast[]) array;
            Object[] array2 = arrayList.toArray(new WeatherDayForecast[0]);
            if (array2 != null) {
                return new WeatherInfo(j, str, sDKTemperatureUnit, sDKCurrentWeatherInfo, weatherHourForecastArr, (WeatherDayForecast[]) array2);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeParcelable(this.currentWeatherInfo, i);
        parcel.writeTypedList(this.dayForecast);
        parcel.writeTypedList(this.hourForecast);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    public WeatherWatchAppInfo(Parcel parcel) {
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        UserDisplayUnit.TemperatureUnit.Companion companion = UserDisplayUnit.TemperatureUnit.Companion;
        String readString2 = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString2 == null ? "C" : readString2);
        CurrentWeatherInfo currentWeatherInfo2 = (CurrentWeatherInfo) parcel.readParcelable(CurrentWeatherInfo.class.getClassLoader());
        this.currentWeatherInfo = currentWeatherInfo2 == null ? new CurrentWeatherInfo() : currentWeatherInfo2;
        parcel.readTypedList(this.dayForecast, WeatherDayForecast.CREATOR);
        parcel.readTypedList(this.hourForecast, WeatherHourForecast.CREATOR);
        this.expiredAt = parcel.readLong();
    }
}
