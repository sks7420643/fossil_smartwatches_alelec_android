package com.misfit.frameworks.buttonservice.model.alarm;

import com.fossil.blesdk.device.data.alarm.Alarm;
import com.fossil.blesdk.device.data.alarm.OneShotAlarm;
import com.fossil.blesdk.device.data.alarm.RepeatedAlarm;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.db4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.HashSet;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AlarmSetting {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HashSet<AlarmDay> alarmDays;
    @DexIgnore
    public /* final */ int hour;
    @DexIgnore
    public /* final */ boolean isRepeat;
    @DexIgnore
    public /* final */ int minute;

    @DexIgnore
    public enum AlarmDay {
        SUNDAY(1),
        MONDAY(2),
        TUESDAY(3),
        WEDNESDAY(4),
        THURSDAY(5),
        FRIDAY(6),
        SATURDAY(7);
        
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        AlarmDay(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static  final  /* synthetic */ int[] $EnumSwitchMapping$Anon0 = new int[AlarmDay.values().length];

        /*
        static {
            $EnumSwitchMapping$Anon0[AlarmDay.SUNDAY.ordinal()] = 1;
            $EnumSwitchMapping$Anon0[AlarmDay.MONDAY.ordinal()] = 2;
            $EnumSwitchMapping$Anon0[AlarmDay.TUESDAY.ordinal()] = 3;
            $EnumSwitchMapping$Anon0[AlarmDay.WEDNESDAY.ordinal()] = 4;
            $EnumSwitchMapping$Anon0[AlarmDay.THURSDAY.ordinal()] = 5;
            $EnumSwitchMapping$Anon0[AlarmDay.FRIDAY.ordinal()] = 6;
            $EnumSwitchMapping$Anon0[AlarmDay.SATURDAY.ordinal()] = 7;
        }
        */
    }

    /*
    static {
        String name = AlarmSetting.class.getName();
        kd4.a((Object) name, "AlarmSetting::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public AlarmSetting(int i, int i2, boolean z, HashSet<AlarmDay> hashSet) {
        this.hour = i;
        this.minute = i2;
        this.isRepeat = z;
        this.alarmDays = hashSet;
    }

    @DexIgnore
    private final Weekday convertCalendarDayToSDKV2Day(AlarmDay alarmDay) {
        switch (WhenMappings.$EnumSwitchMapping$Anon0[alarmDay.ordinal()]) {
            case 1:
                return Weekday.SUNDAY;
            case 2:
                return Weekday.MONDAY;
            case 3:
                return Weekday.TUESDAY;
            case 4:
                return Weekday.WEDNESDAY;
            case 5:
                return Weekday.THURSDAY;
            case 6:
                return Weekday.FRIDAY;
            case 7:
                return Weekday.SATURDAY;
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public final HashSet<AlarmDay> getAlarmDays() {
        return this.alarmDays;
    }

    @DexIgnore
    public final int getAlarmDaysAsInt() {
        HashSet<AlarmDay> hashSet = this.alarmDays;
        int i = 0;
        if (hashSet != null) {
            for (AlarmDay value : hashSet) {
                i |= value.getValue();
            }
        }
        return i;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final boolean isRepeat() {
        return this.isRepeat;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.blesdk.device.data.alarm.RepeatedAlarm] */
    /* JADX WARNING: type inference failed for: r0v7, types: [com.fossil.blesdk.device.data.alarm.Alarm] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Alarm toSDKV2Setting() {
        HashSet<AlarmDay> hashSet = this.alarmDays;
        Weekday weekday = null;
        Alarm ret = null;
        if (hashSet == null) {
            return new OneShotAlarm((byte) this.hour, (byte) this.minute, (Weekday) null);
        }
        if (hashSet.isEmpty()) {
            return new OneShotAlarm((byte) this.hour, (byte) this.minute, (Weekday) null);
        }
        if (this.isRepeat) {
            HashSet<AlarmDay> hashSet2 = this.alarmDays;
            ArrayList arrayList = new ArrayList(db4.a(hashSet2, 10));
            for (AlarmDay convertCalendarDayToSDKV2Day : hashSet2) {
                arrayList.add(convertCalendarDayToSDKV2Day(convertCalendarDayToSDKV2Day));
            }
            ret = new RepeatedAlarm((byte) this.hour, (byte) this.minute, kb4.n(arrayList));
        } else if (this.alarmDays.size() > 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".toSDKV2Setting(), the alarm setting is wrong format, alarmSetting=" + this);
        } else {
            if (!this.alarmDays.isEmpty()) {
                weekday = convertCalendarDayToSDKV2Day(kb4.k(this.alarmDays).get(0));
            }
            return new OneShotAlarm((byte) this.hour, (byte) this.minute, weekday);
        }
        return ret;
    }

    @DexIgnore
    public String toString() {
        return "AlarmSetting{alarmHour=" + this.hour + ", alarmMinute=" + this.minute + ", alarmDays=" + this.alarmDays + ", alarmRepeat=" + this.isRepeat + "}";
    }

    @DexIgnore
    public AlarmSetting(int i, int i2) {
        this(i, i2, false, new HashSet());
    }
}
