package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.blesdk.device.data.watchapp.CommuteTimeWatchApp;
import com.fossil.blesdk.device.data.watchapp.WatchApp;
import com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public List<String> destinations;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(List<String> list) {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME());
        kd4.b(list, "destinations");
        this.destinations = list;
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public WatchApp toSDKSetting() {
        List<String> list = this.destinations;
        if (list != null) {
            Object[] array = list.toArray(new String[0]);
            if (array != null) {
                return new CommuteTimeWatchApp(new CommuteTimeWatchAppDataConfig((String[]) array));
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        kd4.d("destinations");
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        List<String> list = this.destinations;
        if (list != null) {
            parcel.writeStringList(list);
        } else {
            kd4.d("destinations");
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.destinations = arrayList;
    }
}
