package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotificationFilterSettings implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((fd4) null);
    @DexIgnore
    public List<AppNotificationFilter> mNotificationFilters;
    @DexIgnore
    public /* final */ long timeStamp;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilterSettings> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public final long compareTimeStamp(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            long j = 0;
            long timeStamp = appNotificationFilterSettings != null ? appNotificationFilterSettings.getTimeStamp() : 0;
            if (appNotificationFilterSettings2 != null) {
                j = appNotificationFilterSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
            if (r3 != null) goto L_0x000e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0014, code lost:
            if (r4 != null) goto L_0x001c;
         */
        @DexIgnore
        public final boolean isSettingsSame(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            List list;
            List list2;
            if (appNotificationFilterSettings != null) {
                list = appNotificationFilterSettings.getNotificationFilters();
            }
            list = new ArrayList();
            if (appNotificationFilterSettings2 != null) {
                list2 = appNotificationFilterSettings2.getNotificationFilters();
            }
            list2 = new ArrayList();
            return list.size() == list2.size() && list.containsAll(list2);
        }

        @DexIgnore
        public /* synthetic */ CREATOR(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AppNotificationFilterSettings createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AppNotificationFilterSettings(parcel);
        }

        @DexIgnore
        public AppNotificationFilterSettings[] newArray(int i) {
            return new AppNotificationFilterSettings[i];
        }
    }

    @DexIgnore
    public AppNotificationFilterSettings(List<AppNotificationFilter> list, long j) {
        kd4.b(list, "notificationFilters");
        this.mNotificationFilters = list;
        this.timeStamp = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<AppNotificationFilter> getNotificationFilters() {
        return this.mNotificationFilters;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeTypedList(this.mNotificationFilters);
    }

    @DexIgnore
    public AppNotificationFilterSettings(Parcel parcel) {
        kd4.b(parcel, "parcel");
        this.timeStamp = parcel.readLong();
        this.mNotificationFilters = new ArrayList();
        parcel.readTypedList(this.mNotificationFilters, AppNotificationFilter.CREATOR);
    }
}
