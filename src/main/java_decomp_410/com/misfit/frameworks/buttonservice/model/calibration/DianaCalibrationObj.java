package com.misfit.frameworks.buttonservice.model.calibration;

import com.fossil.blesdk.device.data.calibration.HandId;
import com.fossil.blesdk.device.data.calibration.HandMovingDirection;
import com.fossil.blesdk.device.data.calibration.HandMovingSpeed;
import com.fossil.blesdk.device.data.calibration.HandMovingType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DianaCalibrationObj {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((fd4) null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ HandId handId;
    @DexIgnore
    public /* final */ HandMovingDirection handMovingDirection;
    @DexIgnore
    public /* final */ HandMovingSpeed handMovingSpeed;
    @DexIgnore
    public /* final */ HandMovingType handMovingType;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon0; // = new int[CalibrationEnums.MovingType.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon1; // = new int[CalibrationEnums.HandId.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon2; // = new int[CalibrationEnums.Direction.values().length];
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$Anon3; // = new int[CalibrationEnums.Speed.values().length];

            /*
            static {
                $EnumSwitchMapping$Anon0[CalibrationEnums.MovingType.DISTANCE.ordinal()] = 1;
                $EnumSwitchMapping$Anon0[CalibrationEnums.MovingType.POSITION.ordinal()] = 2;
                $EnumSwitchMapping$Anon1[CalibrationEnums.HandId.HOUR.ordinal()] = 1;
                $EnumSwitchMapping$Anon1[CalibrationEnums.HandId.MINUTE.ordinal()] = 2;
                $EnumSwitchMapping$Anon1[CalibrationEnums.HandId.SUB_EYE.ordinal()] = 3;
                $EnumSwitchMapping$Anon2[CalibrationEnums.Direction.CLOCKWISE.ordinal()] = 1;
                $EnumSwitchMapping$Anon2[CalibrationEnums.Direction.COUNTER_CLOCKWISE.ordinal()] = 2;
                $EnumSwitchMapping$Anon2[CalibrationEnums.Direction.SHORTEST_PATH.ordinal()] = 3;
                $EnumSwitchMapping$Anon3[CalibrationEnums.Speed.SIXTEENTH.ordinal()] = 1;
                $EnumSwitchMapping$Anon3[CalibrationEnums.Speed.EIGHTH.ordinal()] = 2;
                $EnumSwitchMapping$Anon3[CalibrationEnums.Speed.QUARTER.ordinal()] = 3;
                $EnumSwitchMapping$Anon3[CalibrationEnums.Speed.HALF.ordinal()] = 4;
                $EnumSwitchMapping$Anon3[CalibrationEnums.Speed.FULL.ordinal()] = 5;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final DianaCalibrationObj consume(HandCalibrationObj handCalibrationObj) {
            HandMovingType handMovingType;
            HandId handId;
            HandMovingDirection handMovingDirection;
            HandMovingSpeed handMovingSpeed;
            kd4.b(handCalibrationObj, "handCalibrationObj");
            int i = WhenMappings.$EnumSwitchMapping$Anon0[handCalibrationObj.getMovingType().ordinal()];
            if (i == 1) {
                handMovingType = HandMovingType.DISTANCE;
            } else if (i == 2) {
                handMovingType = HandMovingType.POSITION;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            HandMovingType handMovingType2 = handMovingType;
            int i2 = WhenMappings.$EnumSwitchMapping$Anon1[handCalibrationObj.getHandId().ordinal()];
            if (i2 == 1) {
                handId = HandId.HOUR;
            } else if (i2 == 2) {
                handId = HandId.MINUTE;
            } else if (i2 == 3) {
                handId = HandId.SUB_EYE;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            HandId handId2 = handId;
            int i3 = WhenMappings.$EnumSwitchMapping$Anon2[handCalibrationObj.getDirection().ordinal()];
            if (i3 == 1) {
                handMovingDirection = HandMovingDirection.CLOCKWISE;
            } else if (i3 == 2) {
                handMovingDirection = HandMovingDirection.COUNTER_CLOCKWISE;
            } else if (i3 == 3) {
                handMovingDirection = HandMovingDirection.SHORTEST_PATH;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            HandMovingDirection handMovingDirection2 = handMovingDirection;
            int i4 = WhenMappings.$EnumSwitchMapping$Anon3[handCalibrationObj.getSpeed().ordinal()];
            if (i4 == 1) {
                handMovingSpeed = HandMovingSpeed.SIXTEENTH;
            } else if (i4 == 2) {
                handMovingSpeed = HandMovingSpeed.EIGHTH;
            } else if (i4 == 3) {
                handMovingSpeed = HandMovingSpeed.QUARTER;
            } else if (i4 == 4) {
                handMovingSpeed = HandMovingSpeed.HALF;
            } else if (i4 == 5) {
                handMovingSpeed = HandMovingSpeed.FULL;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            return new DianaCalibrationObj(handMovingType2, handId2, handMovingDirection2, handMovingSpeed, handCalibrationObj.getDegree());
        }

        @DexIgnore
        public /* synthetic */ Companion(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public DianaCalibrationObj(HandMovingType handMovingType2, HandId handId2, HandMovingDirection handMovingDirection2, HandMovingSpeed handMovingSpeed2, int i) {
        kd4.b(handMovingType2, "handMovingType");
        kd4.b(handId2, "handId");
        kd4.b(handMovingDirection2, "handMovingDirection");
        kd4.b(handMovingSpeed2, "handMovingSpeed");
        this.handMovingType = handMovingType2;
        this.handId = handId2;
        this.handMovingDirection = handMovingDirection2;
        this.handMovingSpeed = handMovingSpeed2;
        this.degree = i;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final HandId getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final HandMovingDirection getHandMovingDirection() {
        return this.handMovingDirection;
    }

    @DexIgnore
    public final HandMovingSpeed getHandMovingSpeed() {
        return this.handMovingSpeed;
    }

    @DexIgnore
    public final HandMovingType getHandMovingType() {
        return this.handMovingType;
    }
}
