package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.ChanceOfRainInfo;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.ChanceOfRainComplicationRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.ChanceOfRainComplicationData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public int probability;

    @DexIgnore
    public ChanceOfRainComplicationAppInfo(int i, long j) {
        super(DeviceEventId.CHANCE_OF_RAIN_COMPLICATION);
        this.probability = i;
        this.expiredAt = j;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return new ChanceOfRainComplicationData(new ChanceOfRainInfo(this.expiredAt, this.probability));
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof ChanceOfRainComplicationRequest)) {
            return null;
        }
        return new ChanceOfRainComplicationData((ChanceOfRainComplicationRequest) deviceRequest, new ChanceOfRainInfo(this.expiredAt, this.probability));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.probability);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppInfo(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.probability = parcel.readInt();
        this.expiredAt = parcel.readLong();
    }
}
