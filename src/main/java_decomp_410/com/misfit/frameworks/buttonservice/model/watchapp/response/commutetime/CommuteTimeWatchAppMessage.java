package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppMessage extends DeviceAppResponse {
    @DexIgnore
    public String message; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(String str) {
        super(DeviceEventId.COMMUTE_TIME_WATCH_APP);
        kd4.b(str, "message");
        this.message = str;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return new CommuteTimeWatchAppData(this.message);
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (deviceRequest instanceof CommuteTimeWatchAppRequest) {
            return new CommuteTimeWatchAppData(this.message);
        }
        return null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
    }
}
