package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import com.fossil.blesdk.device.data.complication.Complication;
import com.fossil.blesdk.device.data.complication.EmptyComplication;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NoneComplicationAppMapping extends ComplicationAppMapping {
    @DexIgnore
    public NoneComplicationAppMapping() {
        super(ComplicationAppMapping.ComplicationAppMappingType.INSTANCE.getEMPTY_TYPE());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public Complication toSDKSetting() {
        return new EmptyComplication();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NoneComplicationAppMapping(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
    }
}
