package com.misfit.frameworks.buttonservice.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.misfit.frameworks.buttonservice.model.QuickCommandQueue$addForNotificationResponse$$inlined$compareBy$1 */
public final class C5580x7c641ceb<T> implements java.util.Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        return com.fossil.blesdk.obfuscated.wb4.m29575a(java.lang.Integer.valueOf(((com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj) t).getLifeCountDownObject().getCount()), java.lang.Integer.valueOf(((com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj) t2).getLifeCountDownObject().getCount()));
    }
}
