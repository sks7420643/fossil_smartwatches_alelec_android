package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData;
import com.fossil.blesdk.model.devicedata.DeviceData;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeTravelMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mTravelTimeInMinute;

    @DexIgnore
    public CommuteTimeTravelMicroAppResponse(int i) {
        super(DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP);
        this.mTravelTimeInMinute = i;
    }

    @DexIgnore
    public DeviceData getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public DeviceData getSDKDeviceResponse(DeviceRequest deviceRequest, Version version) {
        kd4.b(deviceRequest, "deviceRequest");
        if (!(deviceRequest instanceof CommuteTimeTravelMicroAppRequest) || version == null) {
            return null;
        }
        return new CommuteTimeTravelMicroAppData((CommuteTimeTravelMicroAppRequest) deviceRequest, version, this.mTravelTimeInMinute);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mTravelTimeInMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppResponse(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.mTravelTimeInMinute = parcel.readInt();
    }
}
