package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SettingsUtils {
    @DexIgnore
    public static /* final */ SettingsUtils INSTANCE; // = new SettingsUtils();

    @DexIgnore
    public final boolean isSecondTimezoneInRange(short s) {
        return -720 <= s && s <= 720;
    }

    @DexIgnore
    public final boolean isSupportCountDown(String str) {
        kd4.b(str, "serial");
        return !TextUtils.isEmpty(str) && FossilDeviceSerialPatternUtil.getBrandBySerial(str) == FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE && FossilDeviceSerialPatternUtil.isHybridSmartWatchDevice(str);
    }
}
