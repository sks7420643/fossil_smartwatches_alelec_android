package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SharePreferencesUtils {
    @DexIgnore
    public static SharePreferencesUtils instance;
    @DexIgnore
    public SharedPreferences sharedPreferences; // = null;

    @DexIgnore
    public SharePreferencesUtils(Context context) {
        this.sharedPreferences = context.getSharedPreferences(KeyUtils.getButtonPreferenceKey(context), 4);
    }

    @DexIgnore
    public static synchronized SharePreferencesUtils getInstance(Context context) {
        SharePreferencesUtils sharePreferencesUtils;
        synchronized (SharePreferencesUtils.class) {
            if (instance == null) {
                instance = new SharePreferencesUtils(context);
            }
            sharePreferencesUtils = instance;
        }
        return sharePreferencesUtils;
    }

    @DexIgnore
    public long getLong(String str) {
        return this.sharedPreferences.getLong(str, -1);
    }

    @DexIgnore
    public String getString(String str) {
        return this.sharedPreferences.getString(str, (String) null);
    }

    @DexIgnore
    public void setLong(String str, long j) {
        this.sharedPreferences.edit().putLong(str, j).apply();
    }

    @DexIgnore
    public void setString(String str, String str2) {
        this.sharedPreferences.edit().putString(str, str2).apply();
    }
}
