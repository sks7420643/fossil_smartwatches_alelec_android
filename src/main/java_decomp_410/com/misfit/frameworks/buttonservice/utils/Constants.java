package com.misfit.frameworks.buttonservice.utils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Constants {
    @DexIgnore
    public static /* final */ String CURRENT_WORKOUT_SESSION; // = "CURRENT_WORKOUT_SESSION";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_ACTION; // = "DEVICE_REQUEST_ACTION";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_EVENT; // = "DEVICE_REQUEST_EVENT";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_EXTRA; // = "DEVICE_REQUEST_EXTRA";
    @DexIgnore
    public static /* final */ Constants INSTANCE; // = new Constants();
    @DexIgnore
    public static /* final */ String IS_JUST_OTA; // = "IS_JUST_OTA";
    @DexIgnore
    public static /* final */ int MAXIMUM_SECOND_TIMEZONE_OFFSET; // = 720;
    @DexIgnore
    public static /* final */ float MAXIMUM_USER_BIOMETRIC_HEIGHT; // = 2.5f;
    @DexIgnore
    public static /* final */ float MAXIMUM_USER_BIOMETRIC_WEIGHT; // = 250.0f;
    @DexIgnore
    public static /* final */ int MINIMUM_SECOND_TIMEZONE_OFFSET; // = -720;
    @DexIgnore
    public static /* final */ float MINIMUM_USER_BIOMETRIC_HEIGHT; // = 1.0f;
    @DexIgnore
    public static /* final */ float MINIMUM_USER_BIOMETRIC_WEIGHT; // = 35.0f;
    @DexIgnore
    public static /* final */ String START_FOREGROUND_ACTION; // = "START_FOREGROUND_ACTION";
    @DexIgnore
    public static /* final */ String STOP_FOREGROUND_ACTION; // = "STOP_FOREGROUND_ACTION";
}
