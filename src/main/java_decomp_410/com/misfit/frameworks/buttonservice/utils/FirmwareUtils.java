package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.internal.Utility;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.cp4;
import com.fossil.blesdk.obfuscated.gp4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.up4;
import com.fossil.blesdk.obfuscated.vp4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.source.Injection;
import java.security.MessageDigest;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FirmwareUtils {
    @DexIgnore
    public static /* final */ FirmwareUtils INSTANCE; // = new FirmwareUtils();
    @DexIgnore
    public static /* final */ String TAG; // = TAG;

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        int length = bArr.length;
        String str = "";
        int i = 0;
        while (i < length) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            String num = Integer.toString((bArr[i] & FileType.MASKED_INDEX) + 256, 16);
            kd4.a((Object) num, "Integer.toString((input[\u2026() and 0xff) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                kd4.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                str = sb.toString();
                i++;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        if (str != null) {
            String lowerCase = str.toLowerCase();
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean isLatestFirmware(String str, String str2) {
        kd4.b(str, "oldFw");
        kd4.b(str2, "latestFw");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return true;
        }
        if (new vp4(str2).compareTo((up4) new vp4(str)) >= 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[RETURN, SYNTHETIC] */
    public final byte[] readFirmware(FirmwareData firmwareData, Context context) {
        byte[] bArr;
        kd4.b(firmwareData, "firmwareData");
        kd4.b(context, "context");
        String firmwareVersion = firmwareData.getFirmwareVersion();
        if (!TextUtils.isEmpty(firmwareVersion)) {
            if (firmwareData.isEmbedded()) {
                try {
                    bArr = cp4.a(context.getResources().openRawResource(firmwareData.getRawBundleResource()));
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "readFirmware() - e=" + e);
                }
            } else {
                bArr = Injection.INSTANCE.provideFilesRepository(context).readFirmware(firmwareVersion);
            }
            return bArr == null ? bArr : new byte[0];
        }
        bArr = null;
        if (bArr == null) {
        }
    }

    @DexIgnore
    public final boolean verifyFirmware(byte[] bArr, String str) {
        kd4.b(str, "checksum");
        if (!gp4.b(str) && bArr != null) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                instance.update(bArr);
                byte[] digest = instance.digest();
                kd4.a((Object) digest, "md5");
                String bytesToString = bytesToString(digest);
                String lowerCase = str.toLowerCase();
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return kd4.a((Object) lowerCase, (Object) bytesToString);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.e(str2, "Error inside " + TAG + ".verifyFirmware - e=" + e);
            }
        }
        return false;
    }
}
