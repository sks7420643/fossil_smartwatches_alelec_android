package com.misfit.frameworks.buttonservice.interfaces;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface FirmwareUpdaterListener {
    @DexIgnore
    void onUpdateFirmware(byte[] bArr);
}
