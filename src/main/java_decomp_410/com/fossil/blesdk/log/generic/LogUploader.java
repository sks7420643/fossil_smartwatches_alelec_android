package com.fossil.blesdk.log.generic;

import android.os.Handler;
import com.fossil.blesdk.obfuscated.bb0;
import com.fossil.blesdk.obfuscated.em4;
import com.fossil.blesdk.obfuscated.er4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.ib0;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.na0;
import com.fossil.blesdk.obfuscated.oa0;
import com.fossil.blesdk.obfuscated.pa0;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.qr4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.v90;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.w90;
import com.fossil.blesdk.obfuscated.za4;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONObject;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LogUploader {
    @DexIgnore
    public /* final */ String a; // = LogUploader.class.getSimpleName();
    @DexIgnore
    public oa0 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ ArrayList<File> d; // = new ArrayList<>();
    @DexIgnore
    public b e;
    @DexIgnore
    public bb0 f;
    @DexIgnore
    public long g; // = 60000;
    @DexIgnore
    public /* final */ w90 h;
    @DexIgnore
    public na0 i;
    @DexIgnore
    public /* final */ v90 j;
    @DexIgnore
    public /* final */ Handler k;
    @DexIgnore
    public boolean l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final void a() {
            this.e = true;
        }

        @DexIgnore
        public void run() {
            if (!this.e) {
                LogUploader.this.b();
                b a = LogUploader.this.e;
                if (a != null) {
                    LogUploader.this.k.postDelayed(a, LogUploader.this.a());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements er4<String> {
        @DexIgnore
        public /* final */ /* synthetic */ File e;
        @DexIgnore
        public /* final */ /* synthetic */ LogUploader f;

        @DexIgnore
        public c(File file, LogUploader logUploader, oa0 oa0) {
            this.e = file;
            this.f = logUploader;
        }

        @DexIgnore
        public void onFailure(Call<String> call, Throwable th) {
            kd4.b(call, "call");
            kd4.b(th, "t");
            t90 t90 = t90.c;
            t90.b("LogUploader", "Upload Log Fail: throw: " + th.getMessage());
            this.f.e();
        }

        @DexIgnore
        public void onResponse(Call<String> call, qr4<String> qr4) {
            kd4.b(call, "call");
            kd4.b(qr4, "response");
            t90 t90 = t90.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Upload Log Response: message: ");
            sb.append(qr4.e());
            sb.append(", ");
            sb.append("code: ");
            sb.append(qr4.b());
            sb.append(", ");
            sb.append("body: ");
            sb.append(qr4.a());
            sb.append(", ");
            sb.append("error body: ");
            em4 c = qr4.c();
            sb.append(c != null ? c.F() : null);
            t90.a("LogUploader", sb.toString());
            int b = qr4.b();
            if ((200 <= b && 299 >= b) || b == 400) {
                this.f.d.remove(this.e);
                this.e.delete();
                this.f.f();
                return;
            }
            this.f.e();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public LogUploader(w90 w90, na0 na0, v90 v90, Handler handler, boolean z) {
        kd4.b(w90, "logIO");
        kd4.b(na0, "endpoint");
        kd4.b(v90, "logFileParser");
        kd4.b(handler, "handler");
        this.h = w90;
        this.i = na0;
        this.j = v90;
        this.k = handler;
        this.l = z;
        a(this.i);
    }

    @DexIgnore
    public final long a() {
        return this.g;
    }

    @DexIgnore
    public final void b(long j2) {
        if (j2 > 0) {
            this.g = j2;
        }
    }

    @DexIgnore
    public final void c(long j2) {
        c();
        b(j2);
        this.e = new b();
        b bVar = this.e;
        if (bVar != null) {
            this.k.post(bVar);
        }
    }

    @DexIgnore
    public final void d() {
        bb0 bb0 = this.f;
        if (bb0 != null) {
            bb0.a();
            this.k.removeCallbacks(bb0);
        }
    }

    @DexIgnore
    public final void e() {
        synchronized (Boolean.valueOf(this.c)) {
            this.d.clear();
            this.c = false;
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public final void f() {
        String str;
        oa0 oa0 = this.b;
        boolean z = false;
        if (oa0 == null) {
            str = "Invalid end point";
        } else if (!ib0.a(ib0.a, va0.f.a(), false, 2, (Object) null)) {
            str = "Network is not available";
        } else if (!this.l || ib0.a.a(va0.f.a())) {
            str = new String();
        } else {
            str = "Wifi is not available";
        }
        if (str.length() > 0) {
            z = true;
        }
        if (z || oa0 == null) {
            t90 t90 = t90.c;
            String str2 = this.a;
            kd4.a((Object) str2, "tagName");
            t90.a(str2, "Stop uploading: " + str + '.');
            e();
            return;
        }
        File file = (File) kb4.e(this.d);
        if (file != null) {
            JSONObject a2 = this.j.a(file);
            if (a2.length() > 0) {
                String c2 = this.i.c();
                String jSONObject = a2.toString();
                kd4.a((Object) jSONObject, "logFileInJSON.toString()");
                oa0.a(c2, jSONObject).a(new c(file, this, oa0));
                return;
            }
            this.d.remove(file);
            file.delete();
            f();
            return;
        }
        e();
    }

    @DexIgnore
    public final boolean a(na0 na0) {
        kd4.b(na0, "endpoint");
        if (na0.c().length() == 0) {
            return false;
        }
        oa0 a2 = pa0.a.a(na0.b(), na0.a());
        if (a2 == null) {
            return false;
        }
        this.i = na0;
        this.b = a2;
        return true;
    }

    @DexIgnore
    public final void b() {
        synchronized (Boolean.valueOf(this.c)) {
            if (!this.c) {
                this.c = true;
                this.d.clear();
                hb4.a(this.d, (T[]) za4.a((T[]) this.h.a()));
                f();
            }
        }
    }

    @DexIgnore
    public final void a(long j2) {
        d();
        this.f = new bb0(new LogUploader$scheduleSingleLogUploading$Anon1(this));
        bb0 bb0 = this.f;
        if (bb0 != null) {
            this.k.postDelayed(bb0, j2);
        }
    }

    @DexIgnore
    public final void c() {
        b bVar = this.e;
        if (bVar != null) {
            bVar.a();
            this.k.removeCallbacks(bVar);
        }
    }
}
