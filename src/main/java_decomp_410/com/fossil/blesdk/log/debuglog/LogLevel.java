package com.fossil.blesdk.log.debuglog;

import com.fossil.blesdk.obfuscated.fd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum LogLevel {
    VERBOSE(2, 'V'),
    DEBUG(3, 'D'),
    INFO(4, 'I'),
    WARN(5, 'W'),
    ERROR(6, 'E'),
    ASSERT(7, 'A');
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ int priority;
    @DexIgnore
    public /* final */ char symbol;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final LogLevel a(int i) {
            for (LogLevel logLevel : LogLevel.values()) {
                if (logLevel.getPriority$blesdk_productionRelease() == i) {
                    return logLevel;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    LogLevel(int i, char c) {
        this.priority = i;
        this.symbol = c;
    }

    @DexIgnore
    public final int getPriority$blesdk_productionRelease() {
        return this.priority;
    }

    @DexIgnore
    public final char getSymbol$blesdk_productionRelease() {
        return this.symbol;
    }
}
