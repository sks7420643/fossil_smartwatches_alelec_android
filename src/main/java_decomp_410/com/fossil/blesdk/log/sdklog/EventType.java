package com.fossil.blesdk.log.sdklog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum EventType {
    PHASE_INIT,
    PHASE_START,
    PHASE_END,
    REQUEST,
    RESPONSE,
    SYSTEM_EVENT,
    CENTRAL_EVENT,
    DEVICE_EVENT,
    GATT_SERVER_EVENT,
    EXCEPTION,
    DATABASE;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
