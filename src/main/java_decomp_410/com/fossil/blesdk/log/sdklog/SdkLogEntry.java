package com.fossil.blesdk.log.sdklog;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.log.generic.LogEntry;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting;
import java.util.LinkedHashMap;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SdkLogEntry extends LogEntry {
    @DexIgnore
    public DeviceInformation deviceInformation;
    @DexIgnore
    public /* final */ String entryUuid;
    @DexIgnore
    public /* final */ String eventName;
    @DexIgnore
    public /* final */ EventType eventType;
    @DexIgnore
    public JSONObject extraData;
    @DexIgnore
    public boolean isSuccess;
    @DexIgnore
    public /* final */ String macAddress;
    @DexIgnore
    public String phaseName;
    @DexIgnore
    public String phaseUuid;
    @DexIgnore
    public String sessionUuid;
    @DexIgnore
    public ea0 systemInformation;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SdkLogEntry(String str, EventType eventType2, String str2, String str3, String str4, boolean z, String str5, DeviceInformation deviceInformation2, ea0 ea0, JSONObject jSONObject, int i, fd4 fd4) {
        super(null);
        throw null;
        // this(str, eventType2, str2, str3, str4, z, (r0 & 64) != 0 ? "" : str5, (r0 & 128) != 0 ? new DeviceInformation("", str2, "", (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131064, (fd4) null) : deviceInformation2, (r0 & 256) != 0 ? new ea0("", "", "", 0, "", (String) null, 32, (fd4) null) : ea0, (r0 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? new JSONObject() : jSONObject);
        // int i2 = i;
    }

    @DexIgnore
    public final DeviceInformation getDeviceInformation() {
        return this.deviceInformation;
    }

    @DexIgnore
    public final String getEventName() {
        return this.eventName;
    }

    @DexIgnore
    public final EventType getEventType() {
        return this.eventType;
    }

    @DexIgnore
    public final JSONObject getExtraData() {
        return this.extraData;
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public final String getPhaseName() {
        return this.phaseName;
    }

    @DexIgnore
    public final String getPhaseUuid() {
        return this.phaseUuid;
    }

    @DexIgnore
    public final String getSessionUuid() {
        return this.sessionUuid;
    }

    @DexIgnore
    public final ea0 getSystemInformation() {
        return this.systemInformation;
    }

    @DexIgnore
    public final boolean isSuccess() {
        return this.isSuccess;
    }

    @DexIgnore
    public final void setDeviceInformation(DeviceInformation deviceInformation2) {
        kd4.b(deviceInformation2, "<set-?>");
        this.deviceInformation = deviceInformation2;
    }

    @DexIgnore
    public final void setExtraData(JSONObject jSONObject) {
        kd4.b(jSONObject, "<set-?>");
        this.extraData = jSONObject;
    }

    @DexIgnore
    public final void setPhaseName(String str) {
        kd4.b(str, "<set-?>");
        this.phaseName = str;
    }

    @DexIgnore
    public final void setPhaseUuid(String str) {
        kd4.b(str, "<set-?>");
        this.phaseUuid = str;
    }

    @DexIgnore
    public final void setSessionUuid(String str) {
        kd4.b(str, "<set-?>");
        this.sessionUuid = str;
    }

    @DexIgnore
    public final void setSuccess(boolean z) {
        this.isSuccess = z;
    }

    @DexIgnore
    public final void setSystemInformation(ea0 ea0) {
        kd4.b(ea0, "<set-?>");
        this.systemInformation = ea0;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jSONObject = super.toJSONObject();
        jSONObject.put("phase_uuid", this.phaseUuid).put("phase_name", this.phaseName).put("entry_uuid", this.entryUuid).put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, this.eventName).put("type", this.eventType.getLogName$blesdk_productionRelease()).put("is_success", this.isSuccess).put("value", this.extraData).put("user_id", this.systemInformation.f()).put(Constants.SERIAL_NUMBER, this.deviceInformation.getSerialNumber()).put("model_number", this.deviceInformation.getModelNumber()).put(Constants.FIRMWARE_VERSION, this.deviceInformation.getFirmwareVersion()).put("phone_model", this.systemInformation.c()).put("os", this.systemInformation.a()).put(Constants.OS_VERSION, this.systemInformation.b()).put(FetchedAppGateKeepersManager.APPLICATION_SDK_VERSION, this.systemInformation.d()).put("session_uuid", this.sessionUuid).put(LegacySecondTimezoneSetting.COLUMN_TIMEZONE_OFFSET, this.systemInformation.e());
        return jSONObject;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SdkLogEntry(String str, EventType eventType2, String str2, String str3, String str4, boolean z, String str5, DeviceInformation deviceInformation2, ea0 ea0, JSONObject jSONObject) {
        super(str2 + str4 + str3 + str + eventType2.getLogName$blesdk_productionRelease());
        kd4.b(str, "eventName");
        kd4.b(eventType2, "eventType");
        kd4.b(str2, "macAddress");
        kd4.b(str3, "phaseName");
        kd4.b(str4, "phaseUuid");
        kd4.b(str5, "sessionUuid");
        kd4.b(deviceInformation2, "deviceInformation");
        kd4.b(ea0, "systemInformation");
        kd4.b(jSONObject, "extraData");
        this.eventName = str;
        this.eventType = eventType2;
        this.macAddress = str2;
        this.phaseName = str3;
        this.phaseUuid = str4;
        this.isSuccess = z;
        this.sessionUuid = str5;
        this.deviceInformation = deviceInformation2;
        this.systemInformation = ea0;
        this.extraData = jSONObject;
        String uuid = UUID.randomUUID().toString();
        kd4.a((Object) uuid, "UUID.randomUUID().toString()");
        this.entryUuid = uuid;
    }
}
