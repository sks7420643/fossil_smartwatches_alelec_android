package com.fossil.blesdk.adapter;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.adapter.filter.DeviceScanFilter;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.gattserver.service.CurrentTimeService;
import com.fossil.blesdk.gattserver.service.GattService;
import com.fossil.blesdk.hid.HIDProfile;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.bf4;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.i00;
import com.fossil.blesdk.obfuscated.jb0;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mb0;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rb4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.v00;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Pair;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter {
    @DexIgnore
    public static /* final */ Handler a; // = hb0.a.a();
    @DexIgnore
    public static /* final */ BluetoothAdapter b; // = BluetoothAdapter.getDefaultAdapter();
    @DexIgnore
    public static GetConnectedDevicesRunnable c;
    @DexIgnore
    public static /* final */ Hashtable<String, v00<DeviceInformation, Phase.Result>> d; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ScanCallback e; // = new e();
    @DexIgnore
    public static /* final */ LinkedHashMap<b, DeviceScanFilter> f; // = new LinkedHashMap<>();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new c();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new d();
    @DexIgnore
    public static boolean i;
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static GattServer j;
    @DexIgnore
    public static a k;
    @DexIgnore
    public static /* final */ BluetoothLeAdapter l; // = new BluetoothLeAdapter();

    @DexIgnore
    public enum AdvertisingDataType {
        FLAG((byte) 1),
        INCOMPLETE_16_BIT_SERVICE_CLASS_UUID((byte) 2),
        COMPLETE_16_BIT_SERVICE_CLASS_UUID((byte) 3),
        INCOMPLETE_32_BIT_SERVICE_CLASS_UUID((byte) 4),
        COMPLETE_32_BIT_SERVICE_CLASS_UUID((byte) 5),
        INCOMPLETE_128_BIT_SERVICE_CLASS_UUID((byte) 6),
        COMPLETE_128_BIT_SERVICE_CLASS_UUID((byte) 7);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ byte id;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final AdvertisingDataType a(byte b) {
                for (AdvertisingDataType advertisingDataType : AdvertisingDataType.values()) {
                    if (advertisingDataType.getId$blesdk_productionRelease() == b) {
                        return advertisingDataType;
                    }
                }
                return null;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        AdvertisingDataType(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }
    }

    @DexIgnore
    public enum CentralEventName {
        START_SCAN,
        START_SCAN_FAILED,
        STOP_SCAN,
        DEVICE_RETRIEVED,
        DEVICE_FOUND,
        GET_GATT_CONNECTED_DEVICES,
        GET_HID_CONNECTED_DEVICES,
        BOND_STATE_CHANGED,
        BLUETOOTH_STATE_CHANGED;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class GetConnectedDevicesRunnable implements Runnable {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public final void a() {
            this.e = true;
        }

        @DexIgnore
        public void run() {
            if (!this.e) {
                t90.c.a("BluetoothLeAdapter", "getGattConnectedDevice.");
                List b = BluetoothLeAdapter.l.a();
                t90 t90 = t90.c;
                t90.a("BluetoothLeAdapter", "gattConnectedBluetoothDevices: " + kb4.a(b, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon1.INSTANCE, 31, (Object) null));
                da0 da0 = da0.l;
                SdkLogEntry sdkLogEntry = r4;
                SdkLogEntry sdkLogEntry2 = new SdkLogEntry(CentralEventName.GET_GATT_CONNECTED_DEVICES.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(new JSONObject(), JSONKey.GATT_CONNECTED_DEVICES, a(b)), 448, (fd4) null);
                da0.b(sdkLogEntry);
                List c = BluetoothLeAdapter.l.b();
                t90 t902 = t90.c;
                t902.a("BluetoothLeAdapter", "hidConnectedBluetoothDevices: " + kb4.a(c, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon2.INSTANCE, 31, (Object) null));
                da0 da02 = da0.l;
                SdkLogEntry sdkLogEntry3 = r5;
                SdkLogEntry sdkLogEntry4 = new SdkLogEntry(CentralEventName.GET_HID_CONNECTED_DEVICES.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(new JSONObject(), JSONKey.HID_CONNECTED_DEVICES, a(c)), 448, (fd4) null);
                da02.b(sdkLogEntry3);
                ArrayList arrayList = new ArrayList(b);
                arrayList.addAll(c);
                for (BluetoothDevice bluetoothDevice : kb4.c(arrayList)) {
                    if (bluetoothDevice.getType() != 1) {
                        DeviceImplementation.b bVar = DeviceImplementation.b.d;
                        String address = bluetoothDevice.getAddress();
                        kd4.a((Object) address, "bluetoothDevice.address");
                        String b2 = bVar.b(address);
                        if (b2 != null) {
                            BluetoothLeAdapter.l.c(DeviceImplementation.b.d.a(bluetoothDevice, b2));
                        } else if (BluetoothLeAdapter.d.get(bluetoothDevice.getAddress()) == null) {
                            DeviceImplementation a = DeviceImplementation.b.a(DeviceImplementation.b.d, bluetoothDevice, (String) null, 2, (Object) null);
                            HashMap a2 = rb4.a((Pair<? extends K, ? extends V>[]) new Pair[]{oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(y00.f.a(true))), oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 30000L)});
                            t90.c.a("BluetoothLeAdapter", "getGattConnectedDevice: makeDeviceReady to get Serial Number.");
                            v00<DeviceInformation, Phase.Result> b3 = a.b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) a2);
                            BluetoothLeAdapter.d.put(bluetoothDevice.getAddress(), b3);
                            b3.e(new BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon3(bluetoothDevice, a)).c(new BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon4(bluetoothDevice));
                        }
                    }
                }
                BluetoothLeAdapter.a.postDelayed(this, 5000);
            }
        }

        @DexIgnore
        public final JSONArray a(List<BluetoothDevice> list) {
            JSONArray jSONArray = new JSONArray();
            for (BluetoothDevice bluetoothDevice : list) {
                JSONObject jSONObject = new JSONObject();
                JSONKey jSONKey = JSONKey.NAME;
                Object name = bluetoothDevice.getName();
                if (name == null) {
                    name = JSONObject.NULL;
                }
                jSONArray.put(wa0.a(wa0.a(wa0.a(jSONObject, jSONKey, name), JSONKey.MAC_ADDRESS, bluetoothDevice.getAddress()), JSONKey.CURRENT_BOND_STATE, Peripheral.BondState.Companion.a(bluetoothDevice.getBondState())));
            }
            return jSONArray;
        }
    }

    @DexIgnore
    public enum State {
        DISABLED(10),
        ENABLING(11),
        ENABLED(12),
        DISABLING(13);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ int id;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final State a(int i) {
                State state;
                State[] values = State.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        state = null;
                        break;
                    }
                    state = values[i2];
                    if (state.getId$blesdk_productionRelease() == i) {
                        break;
                    }
                    i2++;
                }
                return state != null ? state : State.DISABLED;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        State(int i) {
            this.id = i;
            String name = name();
            if (name != null) {
                String lowerCase = name.toLowerCase();
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final int getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(BluetoothLeAdapter bluetoothLeAdapter, State state, State state2);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceFound(Device device, int i);

        @DexIgnore
        void onStartScanFailed(ScanError scanError);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                String action = intent.getAction();
                if (action != null && action.hashCode() == -1530327060 && action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                    BluetoothLeAdapter.l.b(State.Companion.a(intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", Integer.MIN_VALUE)), State.Companion.a(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1);
                if (bluetoothDevice != null) {
                    BluetoothLeAdapter.l.b(bluetoothDevice, intExtra, intExtra2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ScanCallback {
        @DexIgnore
        public void onScanFailed(int i) {
            BluetoothLeAdapter.l.a(new ScanError(ScanErrorCode.Companion.a(i)));
        }

        @DexIgnore
        public void onScanResult(int i, ScanResult scanResult) {
            byte[] bArr;
            if (scanResult != null) {
                ScanRecord scanRecord = scanResult.getScanRecord();
                if (scanRecord != null) {
                    bArr = scanRecord.getBytes();
                    if (bArr != null && BluetoothLeAdapter.l.c(bArr)) {
                        BluetoothDevice device = scanResult.getDevice();
                        String b = BluetoothLeAdapter.l.d(bArr);
                        if (device != null && DeviceInformation.Companion.a(b)) {
                            DeviceImplementation a = DeviceImplementation.b.d.a(device, b);
                            if (BluetoothLeAdapter.l.b(a)) {
                                BluetoothLeAdapter.l.a(a, scanResult.getRssi());
                                return;
                            }
                            return;
                        }
                        return;
                    }
                }
            }
            bArr = null;
            if (bArr != null) {
            }
        }
    }

    @DexIgnore
    public final String a(int i2) {
        switch (i2) {
            case 10:
                return "BOND_NONE";
            case 11:
                return "BOND_BONDING";
            case 12:
                return "BOND_BONDED";
            default:
                return "UNKNOWN";
        }
    }

    @DexIgnore
    public final void e() {
        int i2;
        b[] bVarArr;
        synchronized (f) {
            Set<b> keySet = f.keySet();
            kd4.a((Object) keySet, "activeLeScanCallbacks.keys");
            Object[] array = keySet.toArray(new b[0]);
            if (array != null) {
                bVarArr = (b[]) array;
                qa4 qa4 = qa4.a;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        for (b a2 : bVarArr) {
            l.a(a2);
        }
    }

    @DexIgnore
    public final void f(Context context) {
        context.registerReceiver(h, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    @DexIgnore
    public final void g() {
        GetConnectedDevicesRunnable getConnectedDevicesRunnable = c;
        if (getConnectedDevicesRunnable != null) {
            getConnectedDevicesRunnable.a();
        }
        GetConnectedDevicesRunnable getConnectedDevicesRunnable2 = c;
        if (getConnectedDevicesRunnable2 != null) {
            a.removeCallbacks(getConnectedDevicesRunnable2);
        }
        c = null;
        Collection<v00<DeviceInformation, Phase.Result>> values = d.values();
        kd4.a((Object) values, "deviceInFetchingInformation.values");
        Object[] array = values.toArray(new v00[0]);
        if (array != null) {
            for (Object obj : array) {
                ((v00) obj).a(new Phase.Result((PhaseId) null, Phase.Result.ResultCode.INTERRUPTED, (Request.Result) null, 5, (fd4) null));
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void h() {
        BluetoothAdapter bluetoothAdapter = b;
        if (bluetoothAdapter != null) {
            BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
            if (bluetoothLeScanner != null) {
                bluetoothLeScanner.stopScan(e);
            }
        }
    }

    @DexIgnore
    public final State c() {
        if (b == null) {
            return State.DISABLED;
        }
        return State.Companion.a(b.getState());
    }

    @DexIgnore
    public final void d() {
        c = new GetConnectedDevicesRunnable();
        GetConnectedDevicesRunnable getConnectedDevicesRunnable = c;
        if (getConnectedDevicesRunnable != null) {
            getConnectedDevicesRunnable.run();
        }
    }

    @DexIgnore
    public final void f() {
        GattServer gattServer = j;
        if (gattServer != null) {
            gattServer.c();
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> b() {
        return HIDProfile.e.a();
    }

    @DexIgnore
    public final String d(byte[] bArr) {
        if (bArr == null) {
            return new String();
        }
        return new String(a(bArr), bf4.a);
    }

    @DexIgnore
    public final boolean b(DeviceImplementation deviceImplementation) {
        if (i || deviceImplementation.getDeviceInformation().getDeviceType() != DeviceType.UNKNOWN) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void c(Context context) {
        kd4.b(context, "context");
        e(context);
        f(context);
        d(context);
    }

    @DexIgnore
    public final void d(Context context) {
        if (j == null) {
            j = new GattServer(context);
        }
        GattServer gattServer = j;
        if (gattServer != null) {
            gattServer.a((GattService) new CurrentTimeService(gattServer.a()));
            gattServer.b();
        }
    }

    @DexIgnore
    public final HashMap<AdvertisingDataType, String[]> b(byte[] bArr) {
        int i2;
        HashMap<AdvertisingDataType, String[]> hashMap = new HashMap<>();
        int i3 = 0;
        while (i3 < bArr.length) {
            int i4 = i3 + 1;
            short b2 = n90.b((byte) (bArr[i3] & ((byte) 255)));
            if (b2 != 0) {
                int i5 = i4 + 1;
                if (i5 <= bArr.length) {
                    AdvertisingDataType a2 = AdvertisingDataType.Companion.a(bArr[i4]);
                    if (a2 != null) {
                        switch (i00.a[a2.ordinal()]) {
                            case 1:
                            case 2:
                                i2 = 2;
                                break;
                            case 3:
                            case 4:
                                i2 = 4;
                                break;
                            case 5:
                            case 6:
                                i2 = 16;
                                break;
                        }
                    }
                    i2 = 0;
                    if (a2 != null && i2 > 0) {
                        byte[] copyOfRange = Arrays.copyOfRange(bArr, i5, i4 + b2);
                        ArrayList arrayList = new ArrayList();
                        String[] strArr = hashMap.get(a2);
                        if (strArr != null) {
                            kd4.a((Object) strArr, "currentServiceUUIDs");
                            hb4.a(arrayList, (T[]) strArr);
                        }
                        kd4.a((Object) copyOfRange, "serviceUUIDRaw");
                        byte[][] a3 = k90.a(copyOfRange, i2);
                        ArrayList arrayList2 = new ArrayList(a3.length);
                        for (byte[] c2 : a3) {
                            arrayList2.add(k90.a(za4.c(c2), (String) null, 1, (Object) null));
                        }
                        arrayList.addAll(arrayList2);
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            hashMap.put(a2, array);
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    }
                    i3 = b2 + i4;
                }
            }
            return hashMap;
        }
        return hashMap;
    }

    @DexIgnore
    public static /* synthetic */ void a(BluetoothLeAdapter bluetoothLeAdapter, List list, ScanSettings scanSettings, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            list = new ArrayList();
        }
        if ((i2 & 2) != 0) {
            scanSettings = new ScanSettings.Builder().setScanMode(2).build();
            kd4.a((Object) scanSettings, "ScanSettings.Builder()\n \u2026MODE_LOW_LATENCY).build()");
        }
        bluetoothLeAdapter.a((List<ScanFilter>) list, scanSettings);
    }

    @DexIgnore
    public final void c(DeviceImplementation deviceImplementation) {
        if (b(deviceImplementation)) {
            a(deviceImplementation, 0);
        }
    }

    @DexIgnore
    public final boolean c(byte[] bArr) {
        HashMap<AdvertisingDataType, String[]> b2 = b(bArr);
        String[] strArr = b2.get(AdvertisingDataType.INCOMPLETE_128_BIT_SERVICE_CLASS_UUID);
        if (strArr != null && za4.b((T[]) strArr, ua0.y.v())) {
            return true;
        }
        String[] strArr2 = b2.get(AdvertisingDataType.COMPLETE_128_BIT_SERVICE_CLASS_UUID);
        if (strArr2 == null || !za4.b((T[]) strArr2, ua0.y.v())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void e(Context context) {
        context.registerReceiver(g, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
    }

    @DexIgnore
    public final void a(List<ScanFilter> list, ScanSettings scanSettings) {
        BluetoothAdapter bluetoothAdapter = b;
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter != null ? bluetoothAdapter.getBluetoothLeScanner() : null;
        if (bluetoothLeScanner == null) {
            a(new ScanError(ScanErrorCode.UNKNOWN_ERROR));
        } else {
            bluetoothLeScanner.startScan(list, scanSettings, e);
        }
    }

    @DexIgnore
    public final void a(b bVar, DeviceScanFilter deviceScanFilter) {
        t90 t90 = t90.c;
        StringBuilder sb = new StringBuilder();
        sb.append("internalStartScan invoked: ");
        sb.append("deviceScanFilter: ");
        boolean z = false;
        sb.append(JSONAbleObject.toJSONString$default(deviceScanFilter, 0, 1, (Object) null));
        sb.append(", ");
        sb.append("leScanCallback: ");
        sb.append(n90.a(bVar.hashCode()));
        t90.a("BluetoothLeAdapter", sb.toString());
        if (c() != State.ENABLED) {
            a(bVar, new ScanError(ScanErrorCode.BLUETOOTH_OFF));
            return;
        }
        synchronized (f) {
            f.put(bVar, deviceScanFilter);
            if (f.size() == 1) {
                z = true;
            }
            qa4 qa4 = qa4.a;
        }
        if (z) {
            a(this, (List) null, (ScanSettings) null, 3, (Object) null);
            d();
        }
    }

    @DexIgnore
    public final void b(State state, State state2) {
        State state3 = state;
        State state4 = state2;
        t90 t90 = t90.c;
        t90.a("BluetoothLeAdapter", "Bluetooth state changed " + state3 + " - " + state4);
        da0.l.b(new SdkLogEntry(CentralEventName.BLUETOOTH_STATE_CHANGED.getLogName$blesdk_productionRelease(), EventType.SYSTEM_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(new JSONObject(), JSONKey.PREV_STATE, state.getLogName$blesdk_productionRelease()), JSONKey.NEW_STATE, state2.getLogName$blesdk_productionRelease()), 448, (fd4) null));
        a(state, state2);
        int i2 = i00.b[state2.ordinal()];
        if (i2 == 1) {
            Context a2 = va0.f.a();
            if (a2 != null) {
                l.d(a2);
            }
        } else if (i2 == 2 || i2 == 3 || i2 == 4) {
            e();
            f();
            t90 t902 = t90.c;
            t902.a("BluetoothLeAdapter", "Stop all done: " + f.size());
        }
        a aVar = k;
        if (aVar != null) {
            aVar.a(this, state3, state4);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        boolean z;
        t90 t90 = t90.c;
        t90.a("BluetoothLeAdapter", "internalStopScan invoked: " + "leScanCallback: " + n90.a(bVar.hashCode()));
        synchronized (f) {
            f.remove(bVar);
            z = f.size() == 0;
            qa4 qa4 = qa4.a;
        }
        if (z) {
            g();
            h();
            DeviceImplementation.b.d.b();
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> a() {
        ArrayList arrayList = new ArrayList();
        Context a2 = va0.f.a();
        List<BluetoothDevice> list = null;
        BluetoothManager bluetoothManager = (BluetoothManager) (a2 != null ? a2.getSystemService(PlaceManager.PARAM_BLUETOOTH) : null);
        if (bluetoothManager != null) {
            list = bluetoothManager.getConnectedDevices(7);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(BluetoothDevice bluetoothDevice, int i2, int i3) {
        int i4 = i2;
        int i5 = i3;
        t90 t90 = t90.c;
        t90.a("BluetoothLeAdapter", "Bond state changed: " + bluetoothDevice.getAddress() + ' ' + a(i4) + '(' + i4 + ") to " + a(i5) + '(' + i5 + ')');
        da0 da0 = da0.l;
        String logName$blesdk_productionRelease = CentralEventName.BOND_STATE_CHANGED.getLogName$blesdk_productionRelease();
        EventType eventType = EventType.CENTRAL_EVENT;
        String address = bluetoothDevice.getAddress();
        String str = address != null ? address : "";
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.MAC_ADDRESS;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        SdkLogEntry sdkLogEntry = r4;
        SdkLogEntry sdkLogEntry2 = new SdkLogEntry(logName$blesdk_productionRelease, eventType, str, "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(wa0.a(jSONObject, jSONKey, address2), JSONKey.PREV_STATE, Device.BondState.Companion.a(i4).getLogName$blesdk_productionRelease()), JSONKey.NEW_STATE, Device.BondState.Companion.a(i5).getLogName$blesdk_productionRelease()), 448, (fd4) null);
        da0.b(sdkLogEntry);
        a(bluetoothDevice, i2, i3);
    }

    @DexIgnore
    public final byte[] a(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = n90.b(bArr[i2]);
            if (b2 == 0) {
                break;
            }
            byte b3 = bArr[i3];
            if (b3 == ((byte) 0)) {
                break;
            } else if (b3 != -1 || b2 < 13) {
                i2 = b2 + i3;
            } else {
                int i4 = i3 + 1 + 2;
                byte[] copyOfRange = Arrays.copyOfRange(bArr, i4, i4 + 10);
                kd4.a((Object) copyOfRange, "Arrays.copyOfRange(adver\u2026RER_SERIAL_NUMBER_LENGTH)");
                return copyOfRange;
            }
        }
        return new byte[0];
    }

    @DexIgnore
    public final void a(State state, State state2) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE", state);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE", state2);
        mb0.a.a(intent);
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i2, int i3) {
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", i2);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", i3);
        mb0.a.a(intent);
    }

    @DexIgnore
    public final boolean b(Context context) {
        boolean z;
        boolean z2;
        LocationManager locationManager = (LocationManager) context.getSystemService(PlaceFields.LOCATION);
        if (locationManager == null) {
            return false;
        }
        if (Build.VERSION.SDK_INT >= 28) {
            return locationManager.isLocationEnabled();
        }
        try {
            z = locationManager.isProviderEnabled("gps");
        } catch (SecurityException e2) {
            da0.l.a(e2);
            z = false;
        }
        try {
            z2 = locationManager.isProviderEnabled("network");
        } catch (SecurityException e3) {
            da0.l.a(e3);
            z2 = false;
        }
        if (z || z2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation, int i2) {
        Set<Map.Entry<b, DeviceScanFilter>> entrySet;
        synchronized (f) {
            entrySet = f.entrySet();
            kd4.a((Object) entrySet, "activeLeScanCallbacks.entries");
            qa4 qa4 = qa4.a;
        }
        for (Map.Entry entry : entrySet) {
            if (((DeviceScanFilter) entry.getValue()).matches$blesdk_productionRelease(deviceImplementation)) {
                l.a((b) entry.getKey(), deviceImplementation, i2);
            }
        }
    }

    @DexIgnore
    public final void b(b bVar) {
        kd4.b(bVar, "leScanCallback");
        da0.l.b(new SdkLogEntry(CentralEventName.STOP_SCAN.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(new JSONObject(), JSONKey.SCAN_CALLBACK, n90.a(bVar.hashCode())), 448, (fd4) null));
        a(bVar);
    }

    @DexIgnore
    public final void a(b bVar, DeviceImplementation deviceImplementation, int i2) {
        t90 t90 = t90.c;
        t90.a("BluetoothLeAdapter", "return found device: " + "MAC: " + deviceImplementation.getDeviceInformation().getMacAddress() + ", " + "serial number: " + deviceImplementation.getDeviceInformation().getSerialNumber() + ", " + "rssi: " + i2);
        da0.l.b(new SdkLogEntry(CentralEventName.DEVICE_FOUND.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.SCAN_CALLBACK, n90.a(bVar.hashCode())), JSONKey.DEVICE, a(deviceImplementation)), JSONKey.RSSI, Integer.valueOf(i2)), 448, (fd4) null));
        bVar.onDeviceFound(deviceImplementation, i2);
    }

    @DexIgnore
    public final void a(ScanError scanError) {
        Set<b> keySet;
        synchronized (f) {
            keySet = f.keySet();
            kd4.a((Object) keySet, "activeLeScanCallbacks.keys");
            qa4 qa4 = qa4.a;
        }
        for (b a2 : keySet) {
            l.a(a2, scanError);
        }
    }

    @DexIgnore
    public final void a(b bVar, ScanError scanError) {
        da0.l.b(new SdkLogEntry(CentralEventName.START_SCAN_FAILED.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", false, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(new JSONObject(), JSONKey.SCAN_CALLBACK, String.valueOf(bVar.hashCode())), JSONKey.SCAN_ERROR, scanError.toJSONObject()), 448, (fd4) null));
        t90 t90 = t90.c;
        t90.a("BluetoothLeAdapter", "startScan FAILED: " + "leScanCallback: " + n90.a(bVar.hashCode()) + ", \n" + scanError + '.');
        bVar.onStartScanFailed(scanError);
    }

    @DexIgnore
    public final JSONObject a(DeviceImplementation deviceImplementation) {
        return wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.SERIAL_NUMBER, deviceImplementation.getDeviceInformation().getSerialNumber()), JSONKey.STATE, deviceImplementation.getState().getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, deviceImplementation.getDeviceInformation().getMacAddress()), JSONKey.CURRENT_BOND_STATE, deviceImplementation.getBondState().getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public final boolean a(Context context) {
        return jb0.a.a(context, new String[]{"android.permission.ACCESS_COARSE_LOCATION"}) || jb0.a.a(context, new String[]{"android.permission.ACCESS_FINE_LOCATION"});
    }

    @DexIgnore
    public final void a(DeviceScanFilter deviceScanFilter, b bVar) throws IllegalStateException {
        DeviceScanFilter deviceScanFilter2 = deviceScanFilter;
        b bVar2 = bVar;
        kd4.b(deviceScanFilter2, "deviceScanFilter");
        kd4.b(bVar2, "leScanCallback");
        da0 da0 = da0.l;
        SdkLogEntry sdkLogEntry = r4;
        SdkLogEntry sdkLogEntry2 = new SdkLogEntry(CentralEventName.START_SCAN.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(new JSONObject(), JSONKey.SCAN_FILTER, deviceScanFilter.toJSONObject()), JSONKey.SCAN_CALLBACK, n90.a(bVar.hashCode())), 448, (fd4) null);
        da0.b(sdkLogEntry);
        Context a2 = va0.f.a();
        if (a2 == null) {
            da0 da02 = da0.l;
            SdkLogEntry sdkLogEntry3 = r4;
            SdkLogEntry sdkLogEntry4 = new SdkLogEntry(CentralEventName.START_SCAN_FAILED.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", false, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.SCAN_FILTER, deviceScanFilter.toJSONObject()), JSONKey.SCAN_CALLBACK, n90.a(bVar.hashCode())), JSONKey.ERROR_DETAIL, "SdkLogManager.init must be call with application context first."), 448, (fd4) null);
            da02.b(sdkLogEntry3);
            t90 t90 = t90.c;
            t90.a("BluetoothLeAdapter", "startScan invoked: " + "deviceScanFilter: " + JSONAbleObject.toJSONString$default(deviceScanFilter2, 0, 1, (Object) null) + ", " + "leScanCallback: " + n90.a(bVar.hashCode()) + 10 + "FAILED - SdkLogManager.init must be call with application context first.");
            throw new IllegalArgumentException("SdkLogManager.init must be call with application context first.");
        } else if (Build.VERSION.SDK_INT >= 23 && !a(a2)) {
            a(bVar2, new ScanError(ScanErrorCode.LOCATION_PERMISSION_NOT_GRANTED));
        } else if (Build.VERSION.SDK_INT < 23 || b(a2)) {
            da0 da03 = da0.l;
            SdkLogEntry sdkLogEntry5 = r4;
            SdkLogEntry sdkLogEntry6 = new SdkLogEntry(CentralEventName.START_SCAN.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(new JSONObject(), JSONKey.SCAN_FILTER, deviceScanFilter.toJSONObject()), JSONKey.SCAN_CALLBACK, n90.a(bVar.hashCode())), 448, (fd4) null);
            da03.b(sdkLogEntry5);
            a(bVar2, deviceScanFilter2);
        } else {
            a(bVar2, new ScanError(ScanErrorCode.LOCATION_SERVICE_NOT_ENABLED));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a1  */
    public final Device a(String str, String str2) throws IllegalArgumentException {
        boolean z;
        DeviceImplementation deviceImplementation;
        Object obj;
        DeviceImplementation deviceImplementation2;
        String str3 = str;
        String str4 = str2;
        kd4.b(str3, "serialNumber");
        kd4.b(str4, "macAddress");
        t90.c.a("BluetoothLeAdapter", "retrieveDevice invoked: serial number=" + str3 + ", " + "macAddress=" + str4);
        JSONObject a2 = wa0.a(wa0.a(new JSONObject(), JSONKey.SERIAL_NUMBER, str3), JSONKey.MAC_ADDRESS, str4);
        if (!xa0.c.d()) {
            da0.l.b(new SdkLogEntry(CentralEventName.DEVICE_RETRIEVED.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", false, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(a2, JSONKey.ERROR_DETAIL, "SdkLogManager.init must be call with application context first."), 448, (fd4) null));
            t90.c.a("BluetoothLeAdapter", "retrieveDevice: FAILED - SdkLogManager.init must be call with application context first.");
            throw new IllegalArgumentException("SdkLogManager.init must be call with application context first.");
        } else if (DeviceInformation.Companion.a(str3)) {
            if (DeviceType.Companion.b(str3) != DeviceType.UNKNOWN) {
                String a3 = DeviceImplementation.b.d.a(str3);
                t90.c.a("BluetoothLeAdapter", "retrieveDevice: cached MAC address=" + a3);
                if (BluetoothAdapter.checkBluetoothAddress(a3)) {
                    BluetoothAdapter bluetoothAdapter = b;
                    BluetoothDevice remoteDevice = bluetoothAdapter != null ? bluetoothAdapter.getRemoteDevice(a3) : null;
                    if (remoteDevice != null) {
                        deviceImplementation2 = DeviceImplementation.b.d.a(remoteDevice, str3);
                        z = true;
                        if (deviceImplementation2 == null) {
                            t90.c.a("BluetoothLeAdapter", "retrieveDevice: Fail to retrieve device from cached MAC Address, switch to use passed MAC Address.");
                            if (BluetoothAdapter.checkBluetoothAddress(str2)) {
                                BluetoothAdapter bluetoothAdapter2 = b;
                                BluetoothDevice remoteDevice2 = bluetoothAdapter2 != null ? bluetoothAdapter2.getRemoteDevice(str4) : null;
                                if (remoteDevice2 != null) {
                                    deviceImplementation = DeviceImplementation.b.d.a(remoteDevice2, str3);
                                }
                            } else {
                                t90.c.a("BluetoothLeAdapter", "retrieveDevice: " + "invalid passed macAddress(" + str4 + ").");
                            }
                        }
                        deviceImplementation = deviceImplementation2;
                    }
                }
                deviceImplementation2 = null;
                z = false;
                if (deviceImplementation2 == null) {
                }
                deviceImplementation = deviceImplementation2;
            } else {
                deviceImplementation = null;
                z = false;
            }
            if (deviceImplementation != null && !b(deviceImplementation)) {
                t90.c.a("BluetoothLeAdapter", "retrieveDevice: retrieve an unsupported device: " + a(deviceImplementation) + ", will return null.");
                deviceImplementation = null;
            }
            wa0.a(a2, JSONKey.IS_CACHED, Boolean.valueOf(z));
            JSONKey jSONKey = JSONKey.DEVICE;
            if (deviceImplementation != null) {
                obj = a(deviceImplementation);
            } else {
                obj = JSONObject.NULL;
            }
            wa0.a(a2, jSONKey, obj);
            da0.l.b(new SdkLogEntry(CentralEventName.DEVICE_RETRIEVED.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", deviceImplementation != null, (String) null, (DeviceInformation) null, (ea0) null, a2, 448, (fd4) null));
            t90 t90 = t90.c;
            StringBuilder sb = new StringBuilder();
            sb.append("retrieveDevice return: ");
            sb.append(deviceImplementation != null ? a(deviceImplementation) : "null");
            t90.a("BluetoothLeAdapter", sb.toString());
            return deviceImplementation;
        } else {
            da0.l.b(new SdkLogEntry(CentralEventName.DEVICE_RETRIEVED.getLogName$blesdk_productionRelease(), EventType.CENTRAL_EVENT, "", "", "", false, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(a2, JSONKey.ERROR_DETAIL, "Invalid serial number."), 448, (fd4) null));
            t90.c.a("BluetoothLeAdapter", "retrieveDevice: " + "FAILED - Invalid Serial Number(" + str3 + ").");
            throw new IllegalArgumentException("serialNumber(" + str3 + ") is invalid.");
        }
    }
}
