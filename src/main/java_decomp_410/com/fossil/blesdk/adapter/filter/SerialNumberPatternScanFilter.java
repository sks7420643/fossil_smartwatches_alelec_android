package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SerialNumberPatternScanFilter extends AbstractScanFilter {
    @DexIgnore
    public Pattern serialNumberPattern;

    @DexIgnore
    public SerialNumberPatternScanFilter(String str) throws IllegalArgumentException {
        kd4.b(str, "serialNumberRegex");
        Pattern compile = Pattern.compile(str);
        kd4.a((Object) compile, "Pattern.compile(serialNumberRegex)");
        this.serialNumberPattern = compile;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        return this.serialNumberPattern.matcher(deviceImplementation.getDeviceInformation().getSerialNumber()).matches();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.FILTER_TYPE, "serial_number_pattern"), JSONKey.SERIAL_NUMBER_REGEX, this.serialNumberPattern.toString());
    }
}
