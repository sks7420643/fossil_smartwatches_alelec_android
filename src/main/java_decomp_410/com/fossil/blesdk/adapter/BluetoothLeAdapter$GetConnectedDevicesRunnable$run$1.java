package com.fossil.blesdk.adapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.bluetooth.BluetoothDevice, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.adapter.BluetoothLeAdapter$GetConnectedDevicesRunnable$run$1 INSTANCE; // = new com.fossil.blesdk.adapter.BluetoothLeAdapter$GetConnectedDevicesRunnable$run$1();

    @DexIgnore
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$1() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(android.bluetooth.BluetoothDevice bluetoothDevice) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothDevice, "it");
        java.lang.String address = bluetoothDevice.getAddress();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) address, "it.address");
        return address;
    }
}
