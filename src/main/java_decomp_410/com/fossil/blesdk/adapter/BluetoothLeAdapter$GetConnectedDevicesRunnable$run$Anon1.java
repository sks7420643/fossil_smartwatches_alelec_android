package com.fossil.blesdk.adapter;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon1 extends Lambda implements xc4<BluetoothDevice, String> {
    @DexIgnore
    public static /* final */ BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon1 INSTANCE; // = new BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon1();

    @DexIgnore
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(BluetoothDevice bluetoothDevice) {
        kd4.b(bluetoothDevice, "it");
        String address = bluetoothDevice.getAddress();
        kd4.a((Object) address, "it.address");
        return address;
    }
}
