package com.fossil.blesdk.adapter;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon3 extends Lambda implements xc4<DeviceInformation, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice $bluetoothDevice;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation $connectedDevice;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$Anon3(BluetoothDevice bluetoothDevice, DeviceImplementation deviceImplementation) {
        super(1);
        this.$bluetoothDevice = bluetoothDevice;
        this.$connectedDevice = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((DeviceInformation) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "it");
        DeviceImplementation.b bVar = DeviceImplementation.b.d;
        String serialNumber = deviceInformation.getSerialNumber();
        String address = this.$bluetoothDevice.getAddress();
        kd4.a((Object) address, "bluetoothDevice.address");
        bVar.a(serialNumber, address);
        BluetoothLeAdapter.d.remove(this.$bluetoothDevice.getAddress());
        BluetoothLeAdapter.l.c(this.$connectedDevice);
    }
}
