package com.fossil.blesdk.gattserver.request;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ClientRequest extends JSONAbleObject {
    @DexIgnore
    public /* final */ BluetoothDevice device;
    @DexIgnore
    public /* final */ int requestId;

    @DexIgnore
    public ClientRequest(BluetoothDevice bluetoothDevice, int i) {
        kd4.b(bluetoothDevice, "device");
        this.device = bluetoothDevice;
        this.requestId = i;
    }

    @DexIgnore
    public final BluetoothDevice getDevice$blesdk_productionRelease() {
        return this.device;
    }

    @DexIgnore
    public final int getRequestId$blesdk_productionRelease() {
        return this.requestId;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.DEVICE, this.device), JSONKey.REQUEST_ID, Integer.valueOf(this.requestId));
    }
}
