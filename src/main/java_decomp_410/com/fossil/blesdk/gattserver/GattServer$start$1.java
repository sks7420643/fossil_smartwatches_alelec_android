package com.fossil.blesdk.gattserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattServer$start$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.gattserver.GattServer this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.gattserver.GattServer$start$1$2")
    /* renamed from: com.fossil.blesdk.gattserver.GattServer$start$1$2 */
    public static final class C12802 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.bluetooth.BluetoothGattService, java.lang.String> {
        @DexIgnore
        public static /* final */ com.fossil.blesdk.gattserver.GattServer$start$1.C12802 INSTANCE; // = new com.fossil.blesdk.gattserver.GattServer$start$1.C12802();

        @DexIgnore
        public C12802() {
            super(1);
        }

        @DexIgnore
        public final java.lang.String invoke(android.bluetooth.BluetoothGattService bluetoothGattService) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bluetoothGattService, com.misfit.frameworks.common.constants.Constants.SERVICE);
            java.lang.String uuid = bluetoothGattService.getUuid().toString();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "service.uuid.toString()");
            return uuid;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.gattserver.GattServer$start$1$3")
    /* renamed from: com.fossil.blesdk.gattserver.GattServer$start$1$3 */
    public static final class C12813 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.bluetooth.BluetoothGattService, java.lang.String> {
        @DexIgnore
        public static /* final */ com.fossil.blesdk.gattserver.GattServer$start$1.C12813 INSTANCE; // = new com.fossil.blesdk.gattserver.GattServer$start$1.C12813();

        @DexIgnore
        public C12813() {
            super(1);
        }

        @DexIgnore
        public final java.lang.String invoke(android.bluetooth.BluetoothGattService bluetoothGattService) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bluetoothGattService, com.misfit.frameworks.common.constants.Constants.SERVICE);
            java.lang.String uuid = bluetoothGattService.getUuid().toString();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "service.uuid.toString()");
            return uuid;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattServer$start$1(com.fossil.blesdk.gattserver.GattServer gattServer) {
        super(0);
        this.this$0 = gattServer;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f0, code lost:
        if (r6 != null) goto L_0x00f5;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00d6  */
    public final void invoke() {
        java.lang.String str;
        android.bluetooth.BluetoothGattServer a;
        java.lang.Object obj;
        java.lang.Object systemService = this.this$0.f3177h.getSystemService(com.facebook.places.PlaceManager.PARAM_BLUETOOTH);
        if (systemService != null) {
            com.fossil.blesdk.gattserver.GattServer gattServer = this.this$0;
            gattServer.f3170a = ((android.bluetooth.BluetoothManager) systemService).openGattServer(gattServer.f3177h, this.this$0.f3174e);
            for (com.fossil.blesdk.gattserver.service.GattService gattService : this.this$0.f3171b) {
                android.bluetooth.BluetoothGattServer a2 = this.this$0.f3170a;
                if (a2 != null) {
                    a2.addService(gattService);
                }
            }
            com.fossil.blesdk.gattserver.GattServer gattServer2 = this.this$0;
            gattServer2.f3172c = gattServer2.f3170a != null;
            com.fossil.blesdk.obfuscated.t90 t90 = com.fossil.blesdk.obfuscated.t90.f9612c;
            java.lang.String d = com.fossil.blesdk.gattserver.GattServer.f3169i;
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("GattServer started: success=");
            sb.append(this.this$0.f3172c);
            sb.append(", ");
            sb.append("services=");
            android.bluetooth.BluetoothGattServer a3 = this.this$0.f3170a;
            if (a3 != null) {
                java.util.List<android.bluetooth.BluetoothGattService> services = a3.getServices();
                if (services != null) {
                    str = com.fossil.blesdk.obfuscated.kb4.m24365a(services, (java.lang.CharSequence) null, (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.gattserver.GattServer$start$1.C12802.INSTANCE, 31, (java.lang.Object) null);
                    sb.append(str);
                    sb.append('.');
                    t90.mo16336a(d, sb.toString());
                    com.fossil.blesdk.obfuscated.da0 da0 = com.fossil.blesdk.obfuscated.da0.f4334l;
                    java.lang.String logName$blesdk_productionRelease = com.fossil.blesdk.gattserver.log.GattServerEventName.GATT_SERVER_STARTED.getLogName$blesdk_productionRelease();
                    com.fossil.blesdk.log.sdklog.EventType eventType = com.fossil.blesdk.log.sdklog.EventType.GATT_SERVER_EVENT;
                    java.lang.String uuid = java.util.UUID.randomUUID().toString();
                    com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "UUID.randomUUID().toString()");
                    boolean f = this.this$0.f3172c;
                    org.json.JSONObject jSONObject = new org.json.JSONObject();
                    com.fossil.blesdk.setting.JSONKey jSONKey = com.fossil.blesdk.setting.JSONKey.SERVICES;
                    a = this.this$0.f3170a;
                    if (a != null) {
                        java.util.List<android.bluetooth.BluetoothGattService> services2 = a.getServices();
                        if (services2 != null) {
                            obj = com.fossil.blesdk.obfuscated.kb4.m24365a(services2, (java.lang.CharSequence) null, (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.gattserver.GattServer$start$1.C12813.INSTANCE, 31, (java.lang.Object) null);
                        }
                    }
                    obj = org.json.JSONObject.NULL;
                    com.fossil.blesdk.log.sdklog.SdkLogEntry sdkLogEntry = new com.fossil.blesdk.log.sdklog.SdkLogEntry(logName$blesdk_productionRelease, eventType, "", "", uuid, f, (java.lang.String) null, (com.fossil.blesdk.device.DeviceInformation) null, (com.fossil.blesdk.obfuscated.ea0) null, com.fossil.blesdk.obfuscated.wa0.m15640a(jSONObject, jSONKey, obj), 448, (com.fossil.blesdk.obfuscated.fd4) null);
                    da0.mo8649b(sdkLogEntry);
                    return;
                }
            }
            str = null;
            sb.append(str);
            sb.append('.');
            t90.mo16336a(d, sb.toString());
            com.fossil.blesdk.obfuscated.da0 da02 = com.fossil.blesdk.obfuscated.da0.f4334l;
            java.lang.String logName$blesdk_productionRelease2 = com.fossil.blesdk.gattserver.log.GattServerEventName.GATT_SERVER_STARTED.getLogName$blesdk_productionRelease();
            com.fossil.blesdk.log.sdklog.EventType eventType2 = com.fossil.blesdk.log.sdklog.EventType.GATT_SERVER_EVENT;
            java.lang.String uuid2 = java.util.UUID.randomUUID().toString();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid2, "UUID.randomUUID().toString()");
            boolean f2 = this.this$0.f3172c;
            org.json.JSONObject jSONObject2 = new org.json.JSONObject();
            com.fossil.blesdk.setting.JSONKey jSONKey2 = com.fossil.blesdk.setting.JSONKey.SERVICES;
            a = this.this$0.f3170a;
            if (a != null) {
            }
            obj = org.json.JSONObject.NULL;
            com.fossil.blesdk.log.sdklog.SdkLogEntry sdkLogEntry2 = new com.fossil.blesdk.log.sdklog.SdkLogEntry(logName$blesdk_productionRelease2, eventType2, "", "", uuid2, f2, (java.lang.String) null, (com.fossil.blesdk.device.DeviceInformation) null, (com.fossil.blesdk.obfuscated.ea0) null, com.fossil.blesdk.obfuscated.wa0.m15640a(jSONObject2, jSONKey2, obj), 448, (com.fossil.blesdk.obfuscated.fd4) null);
            da02.mo8649b(sdkLogEntry2);
            return;
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type android.bluetooth.BluetoothManager");
    }
}
