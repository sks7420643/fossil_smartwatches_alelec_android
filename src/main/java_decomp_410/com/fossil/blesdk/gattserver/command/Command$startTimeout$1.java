package com.fossil.blesdk.gattserver.command;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Command$startTimeout$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.gattserver.command.Command this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Command$startTimeout$1(com.fossil.blesdk.gattserver.command.Command command) {
        super(0);
        this.this$0 = command;
    }

    @DexIgnore
    public final void invoke() {
        this.this$0.mo7871a(com.fossil.blesdk.gattserver.command.Command.Result.ResultCode.TIMEOUT);
    }
}
