package com.fossil.blesdk.gattserver.command;

import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.bb0;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.p90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ra0;
import com.fossil.blesdk.obfuscated.sa0;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Locale;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Command implements sa0<GattOperationResult> {
    @DexIgnore
    public /* final */ int a; // = 255;
    @DexIgnore
    public /* final */ UUID b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Result g;
    @DexIgnore
    public xc4<? super Command, qa4> h;
    @DexIgnore
    public xc4<? super Command, qa4> i;
    @DexIgnore
    public xc4<? super Command, qa4> j;
    @DexIgnore
    public /* final */ CommandId k;
    @DexIgnore
    public /* final */ BluetoothDevice l;
    @DexIgnore
    public /* final */ GattServer.a m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Command e;

        @DexIgnore
        public b(Command command) {
            this.e = command;
        }

        @DexIgnore
        public final void run() {
            this.e.a();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public Command(CommandId commandId, BluetoothDevice bluetoothDevice, GattServer.a aVar) {
        kd4.b(commandId, "id");
        kd4.b(bluetoothDevice, "device");
        kd4.b(aVar, "gattServerOperationCallbackProvider");
        this.k = commandId;
        this.l = bluetoothDevice;
        this.m = aVar;
        UUID randomUUID = UUID.randomUUID();
        kd4.a((Object) randomUUID, "UUID.randomUUID()");
        this.b = randomUUID;
        this.c = 1000;
        this.d = this.k.getLogName$blesdk_productionRelease();
        this.e = hb0.a.a();
        this.g = new Result(this.k, Result.ResultCode.NOT_START, (GattOperationResult.GattResult) null, 4, (fd4) null);
    }

    @DexIgnore
    public abstract JSONObject a(boolean z);

    @DexIgnore
    public abstract void a(GattServer gattServer);

    @DexIgnore
    public final BluetoothDevice b() {
        return this.l;
    }

    @DexIgnore
    public abstract boolean b(GattOperationResult gattOperationResult);

    @DexIgnore
    public final GattServer.a c() {
        return this.m;
    }

    @DexIgnore
    public final CommandId d() {
        return this.k;
    }

    @DexIgnore
    public int e() {
        return this.a;
    }

    @DexIgnore
    public final Result f() {
        return this.g;
    }

    @DexIgnore
    public abstract ra0<GattOperationResult> g();

    @DexIgnore
    public boolean h() {
        return false;
    }

    @DexIgnore
    public final void i() {
        this.e.postDelayed(new bb0(new Command$startTimeout$Anon1(this)), this.c);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result extends JSONAbleObject {
        @DexIgnore
        public static /* final */ a Companion; // = new a((fd4) null);
        @DexIgnore
        public /* final */ CommandId commandId;
        @DexIgnore
        public /* final */ GattOperationResult.GattResult gattResult;
        @DexIgnore
        public /* final */ ResultCode resultCode;

        @DexIgnore
        public enum ResultCode {
            SUCCESS(0),
            NOT_START(1),
            TIMEOUT(4),
            WRONG_STATE(6),
            GATT_ERROR(7),
            UNEXPECTED_RESULT(8),
            BLUETOOTH_OFF(10),
            UNSUPPORTED(11),
            INTERRUPTED(254),
            CONNECTION_DROPPED(255);
            
            @DexIgnore
            public static /* final */ a Companion; // = null;
            @DexIgnore
            public /* final */ int id;
            @DexIgnore
            public /* final */ String logName;

            @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a {
                @DexIgnore
                public a() {
                }

                @DexIgnore
                public final ResultCode a(GattOperationResult.GattResult gattResult) {
                    throw null;
                    // kd4.b(gattResult, "gattResult");
                    // int i = p90.a[gattResult.getResultCode().ordinal()];
                    // if (i == 1) {
                    //     return ResultCode.SUCCESS;
                    // }
                    // if (i != 2) {
                    //     return ResultCode.GATT_ERROR;
                    // }
                    // return ResultCode.BLUETOOTH_OFF;
                }

                @DexIgnore
                public /* synthetic */ a(fd4 fd4) {
                    this();
                }
            }

            /*
            static {
                Companion = new a((fd4) null);
            }
            */

            @DexIgnore
            ResultCode(int i) {
                this.id = i;
                String name = name();
                Locale locale = Locale.US;
                kd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    this.logName = lowerCase;
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            @DexIgnore
            public final int getId() {
                return this.id;
            }

            @DexIgnore
            public final String getLogName$blesdk_productionRelease() {
                return this.logName;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final Result a(GattOperationResult.GattResult gattResult) {
                kd4.b(gattResult, "gattResult");
                return new Result((CommandId) null, ResultCode.Companion.a(gattResult), gattResult, 1, (fd4) null);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(CommandId commandId2, ResultCode resultCode2, GattOperationResult.GattResult gattResult2, int i, fd4 fd4) {
            this((i & 1) != 0 ? CommandId.UNKNOWN : commandId2, resultCode2, (i & 4) != 0 ? new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null) : gattResult2);
        }

        @DexIgnore
        public static /* synthetic */ Result copy$default(Result result, CommandId commandId2, ResultCode resultCode2, GattOperationResult.GattResult gattResult2, int i, Object obj) {
            if ((i & 1) != 0) {
                commandId2 = result.commandId;
            }
            if ((i & 2) != 0) {
                resultCode2 = result.resultCode;
            }
            if ((i & 4) != 0) {
                gattResult2 = result.gattResult;
            }
            return result.copy(commandId2, resultCode2, gattResult2);
        }

        @DexIgnore
        public final CommandId component1() {
            return this.commandId;
        }

        @DexIgnore
        public final ResultCode component2() {
            return this.resultCode;
        }

        @DexIgnore
        public final GattOperationResult.GattResult component3() {
            return this.gattResult;
        }

        @DexIgnore
        public final Result copy(CommandId commandId2, ResultCode resultCode2, GattOperationResult.GattResult gattResult2) {
            kd4.b(commandId2, "commandId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(gattResult2, "gattResult");
            return new Result(commandId2, resultCode2, gattResult2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return kd4.a((Object) this.commandId, (Object) result.commandId) && kd4.a((Object) this.resultCode, (Object) result.resultCode) && kd4.a((Object) this.gattResult, (Object) result.gattResult);
        }

        @DexIgnore
        public final CommandId getCommandId() {
            return this.commandId;
        }

        @DexIgnore
        public final GattOperationResult.GattResult getGattResult() {
            return this.gattResult;
        }

        @DexIgnore
        public final ResultCode getResultCode() {
            return this.resultCode;
        }

        @DexIgnore
        public int hashCode() {
            CommandId commandId2 = this.commandId;
            int i = 0;
            int hashCode = (commandId2 != null ? commandId2.hashCode() : 0) * 31;
            ResultCode resultCode2 = this.resultCode;
            int hashCode2 = (hashCode + (resultCode2 != null ? resultCode2.hashCode() : 0)) * 31;
            GattOperationResult.GattResult gattResult2 = this.gattResult;
            if (gattResult2 != null) {
                i = gattResult2.hashCode();
            }
            return hashCode2 + i;
        }

        @DexIgnore
        public JSONObject toJSONObject() throws JSONException {
            throw null;
            // JSONObject jSONObject = new JSONObject();
            // try {
            //     wa0.a(wa0.a(jSONObject, JSONKey.COMMAND_ID, this.commandId.getLogName$blesdk_productionRelease()), JSONKey.RESULT_CODE, this.resultCode.getLogName$blesdk_productionRelease());
            //     if (this.gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            //         wa0.a(jSONObject, JSONKey.GATT_RESULT, this.gattResult.toJSONObject());
            //     }
            // } catch (JSONException e) {
            //     da0.l.a(e);
            // }
            // return jSONObject;
        }

        @DexIgnore
        public String toString() {
            return "Result(commandId=" + this.commandId + ", resultCode=" + this.resultCode + ", gattResult=" + this.gattResult + ")";
        }

        @DexIgnore
        public Result(CommandId commandId2, ResultCode resultCode2, GattOperationResult.GattResult gattResult2) {
            kd4.b(commandId2, "commandId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(gattResult2, "gattResult");
            this.commandId = commandId2;
            this.resultCode = resultCode2;
            this.gattResult = gattResult2;
        }
    }

    @DexIgnore
    public final void a(Result result) {
        kd4.b(result, "<set-?>");
        this.g = result;
    }

    @DexIgnore
    public final Command b(GattServer gattServer) {
        throw null;
        // kd4.b(gattServer, "gattServer");
        // if (!this.f) {
        //     t90 t90 = t90.c;
        //     String str = this.d;
        //     t90.a(str, "Started, option=" + a(true));
        //     da0 da0 = da0.l;
        //     String logName$blesdk_productionRelease = this.k.getLogName$blesdk_productionRelease();
        //     EventType eventType = EventType.REQUEST;
        //     String address = this.l.getAddress();
        //     kd4.a((Object) address, "device.address");
        //     String uuid = this.b.toString();
        //     kd4.a((Object) uuid, "requestUuid.toString()");
        //     da0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (ea0) null, a(true), 448, (fd4) null));
        //     ra0<GattOperationResult> g2 = g();
        //     if (g2 != null) {
        //         g2.a((sa0<GattOperationResult>) this);
        //     }
        //     a(gattServer);
        //     i();
        // }
        // return this;
    }

    @DexIgnore
    public final Command c(xc4<? super Command, qa4> xc4) {
        kd4.b(xc4, "actionOnCommandSuccess");
        this.h = xc4;
        return this;
    }

    @DexIgnore
    public final void d(GattOperationResult gattOperationResult) {
        if (b(gattOperationResult)) {
            if (this.g.getResultCode() != Result.ResultCode.INTERRUPTED) {
                a(gattOperationResult);
            } else {
                this.g = Result.copy$default(this.g, (CommandId) null, (Result.ResultCode) null, gattOperationResult.a(), 3, (Object) null);
            }
            a();
        }
    }

    @DexIgnore
    public final Command a(xc4<? super Command, qa4> xc4) {
        kd4.b(xc4, "actionOnFinal");
        this.j = xc4;
        return this;
    }

    @DexIgnore
    /* renamed from: c */
    public void a(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "value");
        d(gattOperationResult);
    }

    @DexIgnore
    public final void a(Result.ResultCode resultCode) {
        throw null;
        // kd4.b(resultCode, "resultCode");
        // if (!this.f) {
        //     t90 t90 = t90.c;
        //     String str = this.d;
        //     t90.a(str, "stop: " + resultCode);
        //     this.g = Result.copy$default(this.g, (CommandId) null, resultCode, (GattOperationResult.GattResult) null, 5, (Object) null);
        //     if (h()) {
        //         this.e.postDelayed(new b(this), 1000);
        //     } else {
        //         a();
        //     }
        // }
    }

    // @DexIgnore
    // public void a(GattOperationResult gattOperationResult) {
    //     kd4.b(gattOperationResult, "gattOperationResult");
    //     Result a2 = Result.Companion.a(gattOperationResult.a());
    //     this.g = Result.copy$default(this.g, (CommandId) null, a2.getResultCode(), a2.getGattResult(), 1, (Object) null);
    // }

    @DexIgnore
    public final void a() {
        throw null;
        // if (!this.f) {
        //     this.f = true;
        //     ra0<GattOperationResult> g2 = g();
        //     if (g2 != null) {
        //         g2.b(this);
        //     }
        //     if (Result.ResultCode.SUCCESS == this.g.getResultCode()) {
        //         t90.c.a(this.d, JSONAbleObject.toJSONString$default(this.g, 0, 1, (Object) null));
        //         xc4<? super Command, qa4> xc4 = this.h;
        //         if (xc4 != null) {
        //             qa4 invoke = xc4.invoke(this);
        //         }
        //     } else {
        //         t90.c.b(this.d, JSONAbleObject.toJSONString$default(this.g, 0, 1, (Object) null));
        //         xc4<? super Command, qa4> xc42 = this.i;
        //         if (xc42 != null) {
        //             qa4 invoke2 = xc42.invoke(this);
        //         }
        //     }
        //     da0 da0 = da0.l;
        //     String logName$blesdk_productionRelease = this.k.getLogName$blesdk_productionRelease();
        //     EventType eventType = EventType.RESPONSE;
        //     String address = this.l.getAddress();
        //     kd4.a((Object) address, "device.address");
        //     String uuid = this.b.toString();
        //     kd4.a((Object) uuid, "requestUuid.toString()");
        //     da0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, address, "", uuid, true, (String) null, (DeviceInformation) null, (ea0) null, this.g.toJSONObject(), 448, (fd4) null));
        //     xc4<? super Command, qa4> xc43 = this.j;
        //     if (xc43 != null) {
        //         qa4 invoke3 = xc43.invoke(this);
        //     }
        // }
    }

    @DexIgnore
    public final Command b(xc4<? super Command, qa4> xc4) {
        kd4.b(xc4, "actionOnCommandError");
        this.i = xc4;
        return this;
    }
}
