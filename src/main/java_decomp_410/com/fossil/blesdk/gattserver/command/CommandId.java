package com.fossil.blesdk.gattserver.command;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum CommandId {
    SEND_RESPONSE,
    NOTIFY_CHARACTERISTIC_CHANGED,
    UNKNOWN;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
