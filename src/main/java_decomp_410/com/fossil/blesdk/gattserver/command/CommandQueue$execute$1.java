package com.fossil.blesdk.gattserver.command;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommandQueue$execute$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.gattserver.command.Command, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.gattserver.command.CommandQueue this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommandQueue$execute$1(com.fossil.blesdk.gattserver.command.CommandQueue commandQueue) {
        super(1);
        this.this$0 = commandQueue;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.gattserver.command.Command) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.gattserver.command.Command command) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(command, "it");
        this.this$0.mo7902a();
    }
}
