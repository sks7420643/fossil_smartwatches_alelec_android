package com.fossil.blesdk.gattserver.service;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.gattserver.request.ReadCharacteristicRequest;
import com.fossil.blesdk.gattserver.service.GattService;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.log.sdklog.SystemEventName;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CurrentTimeService extends GattService {
    @DexIgnore
    public static /* final */ a k; // = new a((fd4) null);
    @DexIgnore
    public /* final */ b j; // = new b(this);

    @DexIgnore
    public enum AdjustmentFlag {
        ADJUST_NONE((byte) 0),
        ADJUST_MANUAL((byte) 1),
        ADJUST_EXTERNAL((byte) 2),
        ADJUST_TIMEZONE((byte) 4),
        ADJUST_DST((byte) 8);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        AdjustmentFlag(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }
    }

    @DexIgnore
    public enum BluetoothWeekday {
        DAY_UNKNOWN((byte) 0),
        DAY_MONDAY((byte) 1),
        DAY_TUESDAY((byte) 2),
        DAY_WEDNESDAY((byte) 3),
        DAY_THURSDAY((byte) 4),
        DAY_FRIDAY((byte) 5),
        DAY_SATURDAY((byte) 6),
        DAY_SUNDAY((byte) 7);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        BluetoothWeekday(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }
    }

    @DexIgnore
    public enum DaySavingTimeOffset {
        DST_STANDARD((byte) 0),
        DST_HALF((byte) 2),
        DST_SINGLE((byte) 4),
        DST_DOUBLE((byte) 8),
        DST_UNKNOWN((byte) 255);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        DaySavingTimeOffset(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a(long j, AdjustmentFlag adjustmentFlag) {
            kd4.b(adjustmentFlag, "adjustReason");
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, LogBuilder.KEY_TIME);
            instance.setTimeInMillis(j);
            int i = instance.get(1);
            return new byte[]{(byte) (i & 255), (byte) ((i >> 8) & 255), (byte) (instance.get(2) + 1), (byte) instance.get(5), (byte) instance.get(11), (byte) instance.get(12), (byte) instance.get(13), a(instance.get(7)).getId$blesdk_productionRelease(), (byte) (instance.get(14) / 256), adjustmentFlag.getId$blesdk_productionRelease()};
        }

        @DexIgnore
        public final DaySavingTimeOffset b(int i) {
            if (i == 0) {
                return DaySavingTimeOffset.DST_STANDARD;
            }
            if (i == 1) {
                return DaySavingTimeOffset.DST_HALF;
            }
            if (i == 2) {
                return DaySavingTimeOffset.DST_SINGLE;
            }
            if (i != 4) {
                return DaySavingTimeOffset.DST_UNKNOWN;
            }
            return DaySavingTimeOffset.DST_DOUBLE;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final byte[] a(long j) {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, LogBuilder.KEY_TIME);
            instance.setTimeInMillis(j);
            return new byte[]{(byte) (instance.get(15) / 900000), b(instance.get(16) / 1800000).getId$blesdk_productionRelease()};
        }

        @DexIgnore
        public final BluetoothWeekday a(int i) {
            switch (i) {
                case 1:
                    return BluetoothWeekday.DAY_SUNDAY;
                case 2:
                    return BluetoothWeekday.DAY_MONDAY;
                case 3:
                    return BluetoothWeekday.DAY_TUESDAY;
                case 4:
                    return BluetoothWeekday.DAY_WEDNESDAY;
                case 5:
                    return BluetoothWeekday.DAY_THURSDAY;
                case 6:
                    return BluetoothWeekday.DAY_FRIDAY;
                case 7:
                    return BluetoothWeekday.DAY_SATURDAY;
                default:
                    return BluetoothWeekday.DAY_UNKNOWN;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ CurrentTimeService a;

        @DexIgnore
        public b(CurrentTimeService currentTimeService) {
            this.a = currentTimeService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            AdjustmentFlag adjustmentFlag;
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            long j3 = currentTimeMillis - (j * j2);
            int offset = (TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60;
            t90 t90 = t90.c;
            String b = this.a.b();
            t90.a(b, "onTimeChanged: second=" + j2 + ", " + "millisecond=" + j3 + ", " + "timeZoneOffsetInMinute=" + offset + '.');
            da0 da0 = da0.l;
            String logName$blesdk_productionRelease = SystemEventName.TIME_CHANGED.getLogName$blesdk_productionRelease();
            EventType eventType = EventType.SYSTEM_EVENT;
            String uuid = UUID.randomUUID().toString();
            kd4.a((Object) uuid, "UUID.randomUUID().toString()");
            da0.b(new SdkLogEntry(logName$blesdk_productionRelease, eventType, "", "", uuid, true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.SECOND, Long.valueOf(j2)), JSONKey.MILLISECOND, Long.valueOf(j3)), JSONKey.TIMEZONE_OFFSET_IN_MINUTE, Integer.valueOf(offset)), 448, (fd4) null));
            String action = intent.getAction();
            if (action != null) {
                int hashCode = action.hashCode();
                if (hashCode != 502473491) {
                    if (hashCode == 505380757 && action.equals("android.intent.action.TIME_SET")) {
                        adjustmentFlag = AdjustmentFlag.ADJUST_MANUAL;
                        this.a.a(System.currentTimeMillis(), adjustmentFlag);
                    }
                } else if (action.equals("android.intent.action.TIMEZONE_CHANGED")) {
                    adjustmentFlag = AdjustmentFlag.ADJUST_TIMEZONE;
                    this.a.a(System.currentTimeMillis(), adjustmentFlag);
                }
            }
            adjustmentFlag = AdjustmentFlag.ADJUST_NONE;
            this.a.a(System.currentTimeMillis(), adjustmentFlag);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CurrentTimeService(GattService.a aVar) {
        super("CurrentTimeService", r0, 0, aVar);
        kd4.b(aVar, "delegate");
        UUID d = ua0.y.d();
        kd4.a((Object) d, "Constant.CURRENT_TIME_SERVICE_UUID");
        BluetoothGattCharacteristic bluetoothGattCharacteristic = new BluetoothGattCharacteristic(ua0.y.b(), 18, 1);
        bluetoothGattCharacteristic.addDescriptor(new BluetoothGattDescriptor(ua0.y.a(), 17));
        BluetoothGattCharacteristic bluetoothGattCharacteristic2 = new BluetoothGattCharacteristic(ua0.y.c(), 2, 1);
        addCharacteristic(bluetoothGattCharacteristic);
        addCharacteristic(bluetoothGattCharacteristic2);
    }

    @DexIgnore
    public void c() {
        super.c();
        h();
    }

    @DexIgnore
    public void d() {
        super.d();
        i();
    }

    @DexIgnore
    public final IntentFilter g() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        return intentFilter;
    }

    @DexIgnore
    public final void h() {
        a().getContext().registerReceiver(this.j, g());
    }

    @DexIgnore
    public final void i() {
        try {
            a().getContext().unregisterReceiver(this.j);
        } catch (Exception e) {
            da0.l.a(e);
        }
    }

    @DexIgnore
    public boolean a(ReadCharacteristicRequest readCharacteristicRequest) {
        kd4.b(readCharacteristicRequest, "request");
        UUID uuid = readCharacteristicRequest.getCharacteristic$blesdk_productionRelease().getUuid();
        byte[] bArr = null;
        int i = 0;
        if (kd4.a((Object) uuid, (Object) ua0.y.b())) {
            bArr = k.a(System.currentTimeMillis(), AdjustmentFlag.ADJUST_NONE);
        } else if (kd4.a((Object) uuid, (Object) ua0.y.c())) {
            bArr = k.a(System.currentTimeMillis());
        } else {
            i = null;
        }
        if (i == null) {
            return false;
        }
        a(readCharacteristicRequest, 0, 0, bArr);
        return true;
    }

    @DexIgnore
    public final void a(long j2, AdjustmentFlag adjustmentFlag) {
        BluetoothGattCharacteristic characteristic = getCharacteristic(ua0.y.b());
        if (characteristic != null) {
            characteristic.setValue(k.a(j2, adjustmentFlag));
            a(characteristic);
        }
    }
}
