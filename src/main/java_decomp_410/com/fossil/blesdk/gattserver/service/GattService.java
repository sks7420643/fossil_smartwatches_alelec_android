package com.fossil.blesdk.gattserver.service;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.gattserver.request.ClientRequest;
import com.fossil.blesdk.gattserver.request.ReadCharacteristicRequest;
import com.fossil.blesdk.gattserver.request.ReadDescriptorRequest;
import com.fossil.blesdk.gattserver.request.WriteCharacteristicRequest;
import com.fossil.blesdk.gattserver.request.WriteDescriptorRequest;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.q90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.r90;
import com.fossil.blesdk.obfuscated.ta0;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GattService extends BluetoothGattService {
    @DexIgnore
    public /* final */ HashMap<BluetoothDevice, ta0> e; // = new HashMap<>();
    @DexIgnore
    public Hashtable<BluetoothDevice, Integer> f; // = new Hashtable<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ a i;

    @DexIgnore
    public interface a {
        @DexIgnore
        GattServer.a a();

        @DexIgnore
        void a(Command command);

        @DexIgnore
        Context getContext();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattService(String str, UUID uuid, int i2, a aVar) {
        super(uuid, i2);
        kd4.b(str, "name");
        kd4.b(uuid, "uuid");
        kd4.b(aVar, "delegate");
        this.h = str;
        this.i = aVar;
    }

    @DexIgnore
    public final a a() {
        return this.i;
    }

    @DexIgnore
    public boolean a(ReadCharacteristicRequest readCharacteristicRequest) {
        kd4.b(readCharacteristicRequest, "request");
        return false;
    }

    @DexIgnore
    public boolean a(WriteCharacteristicRequest writeCharacteristicRequest) {
        kd4.b(writeCharacteristicRequest, "request");
        return false;
    }

    @DexIgnore
    public final String b() {
        return this.h;
    }

    @DexIgnore
    public void b(Command command) {
        kd4.b(command, Constants.COMMAND);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void c(Command command) {
        kd4.b(command, Constants.COMMAND);
    }

    @DexIgnore
    public void d() {
    }

    @DexIgnore
    public void d(Command command) {
        kd4.b(command, Constants.COMMAND);
    }

    @DexIgnore
    public final void e() {
        synchronized (Boolean.valueOf(this.g)) {
            if (!this.g) {
                this.g = true;
                c();
            }
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            GattService gattService = (GattService) obj;
            return !(kd4.a((Object) getUuid(), (Object) gattService.getUuid()) ^ true) && getType() == gattService.getType();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.gattserver.service.GattService");
    }

    @DexIgnore
    public final void f() {
        synchronized (Boolean.valueOf(this.g)) {
            if (this.g) {
                d();
                this.g = false;
            }
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (getUuid().hashCode() * 31) + Integer.valueOf(getType()).hashCode();
    }

    @DexIgnore
    public void a(BluetoothDevice bluetoothDevice, int i2, int i3) {
        kd4.b(bluetoothDevice, "device");
        if (i3 == 0) {
            this.e.remove(bluetoothDevice);
        }
    }

    @DexIgnore
    public void a(int i2, BluetoothGattService bluetoothGattService) {
        kd4.b(bluetoothGattService, Constants.SERVICE);
        if (i2 == 0 && kd4.a((Object) this, (Object) bluetoothGattService)) {
            e();
        }
    }

    @DexIgnore
    public void a(BluetoothDevice bluetoothDevice, int i2) {
        kd4.b(bluetoothDevice, "device");
        this.f.put(bluetoothDevice, Integer.valueOf(i2));
    }

    @DexIgnore
    public boolean a(ClientRequest clientRequest) {
        kd4.b(clientRequest, "request");
        if (clientRequest instanceof WriteDescriptorRequest) {
            return a((WriteDescriptorRequest) clientRequest);
        }
        if (clientRequest instanceof ReadDescriptorRequest) {
            return a((ReadDescriptorRequest) clientRequest);
        }
        if (clientRequest instanceof WriteCharacteristicRequest) {
            return a((WriteCharacteristicRequest) clientRequest);
        }
        if (clientRequest instanceof ReadCharacteristicRequest) {
            return a((ReadCharacteristicRequest) clientRequest);
        }
        return false;
    }

    @DexIgnore
    public boolean a(WriteDescriptorRequest writeDescriptorRequest) {
        kd4.b(writeDescriptorRequest, "request");
        if (!kd4.a((Object) writeDescriptorRequest.getDescriptor$blesdk_productionRelease().getUuid(), (Object) ua0.y.a())) {
            return false;
        }
        synchronized (this.e) {
            ta0 ta0 = this.e.get(writeDescriptorRequest.getDevice$blesdk_productionRelease());
            if (ta0 == null) {
                ta0 = new ta0();
            }
            ta0.a().put(writeDescriptorRequest.getDescriptor$blesdk_productionRelease(), writeDescriptorRequest.getValue$blesdk_productionRelease());
            this.e.put(writeDescriptorRequest.getDevice$blesdk_productionRelease(), ta0);
            a((Command) new r90(writeDescriptorRequest, 0, 0, (byte[]) null, this.i.a()));
        }
        return true;
    }

    @DexIgnore
    public boolean a(ReadDescriptorRequest readDescriptorRequest) {
        kd4.b(readDescriptorRequest, "request");
        if (!kd4.a((Object) readDescriptorRequest.getDescriptor$blesdk_productionRelease().getUuid(), (Object) ua0.y.a())) {
            return false;
        }
        synchronized (this.e) {
            ta0 ta0 = this.e.get(readDescriptorRequest.getDevice$blesdk_productionRelease());
            if (ta0 == null) {
                ta0 = new ta0();
            }
            byte[] bArr = ta0.a().get(readDescriptorRequest.getDescriptor$blesdk_productionRelease());
            if (bArr == null) {
                bArr = GattDescriptor.d.a();
            }
            a((Command) new r90(readDescriptorRequest, 0, 0, bArr, this.i.a()));
        }
        return true;
    }

    @DexIgnore
    public final void a(ClientRequest clientRequest, int i2, int i3, byte[] bArr) {
        kd4.b(clientRequest, "request");
        a((Command) new r90(clientRequest, i2, i3, bArr, this.i.a()));
    }

    @DexIgnore
    public final void a(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        boolean z;
        int i2;
        kd4.b(bluetoothGattCharacteristic, "characteristic");
        HashMap<BluetoothDevice, ta0> hashMap = this.e;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator<Map.Entry<BluetoothDevice, ta0>> it = hashMap.entrySet().iterator();
        while (true) {
            z = true;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            Iterator<Map.Entry<BluetoothGattDescriptor, byte[]>> it2 = ((ta0) next.getValue()).a().entrySet().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                Map.Entry next2 = it2.next();
                Object key = next2.getKey();
                kd4.a(key, "descriptorSubscription.key");
                BluetoothGattCharacteristic characteristic = ((BluetoothGattDescriptor) key).getCharacteristic();
                kd4.a((Object) characteristic, "descriptor.characteristic");
                if (kd4.a((Object) characteristic.getUuid(), (Object) bluetoothGattCharacteristic.getUuid())) {
                    byte[] c = GattDescriptor.d.c();
                    Object value = next2.getValue();
                    kd4.a(value, "descriptorSubscription.value");
                    if (Arrays.equals(c, (byte[]) value)) {
                        break;
                    }
                    byte[] b = GattDescriptor.d.b();
                    Object value2 = next2.getValue();
                    kd4.a(value2, "descriptorSubscription.value");
                    if (Arrays.equals(b, (byte[]) value2)) {
                        break;
                    }
                }
            }
            if (z) {
                linkedHashMap.put(next.getKey(), next.getValue());
            }
        }
        Object[] array = linkedHashMap.keySet().toArray(new BluetoothDevice[0]);
        if (array != null) {
            BluetoothDevice[] bluetoothDeviceArr = (BluetoothDevice[]) array;
            if (!(bluetoothDeviceArr.length == 0)) {
                if ((bluetoothGattCharacteristic.getProperties() & 32) == 0) {
                    z = false;
                }
                for (BluetoothDevice q90 : bluetoothDeviceArr) {
                    a aVar = this.i;
                    aVar.a(new q90(q90, bluetoothGattCharacteristic, z, aVar.a()));
                }
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void a(Command command) {
        command.c((xc4<? super Command, qa4>) new GattService$executeCommand$Anon1(this));
        command.b((xc4<? super Command, qa4>) new GattService$executeCommand$Anon2(this));
        command.a((xc4<? super Command, qa4>) new GattService$executeCommand$Anon3(this));
        this.i.a(command);
    }
}
