package com.fossil.blesdk.model.enumerate;

import com.facebook.LegacyTokenHelper;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum RingPhoneAction {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final RingPhoneAction a(String str) {
            kd4.b(str, LegacyTokenHelper.TYPE_STRING);
            for (RingPhoneAction ringPhoneAction : RingPhoneAction.values()) {
                String logName$blesdk_productionRelease = ringPhoneAction.getLogName$blesdk_productionRelease();
                Locale locale = Locale.US;
                kd4.a((Object) locale, "Locale.US");
                String lowerCase = str.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (kd4.a((Object) logName$blesdk_productionRelease, (Object) lowerCase)) {
                    return ringPhoneAction;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
