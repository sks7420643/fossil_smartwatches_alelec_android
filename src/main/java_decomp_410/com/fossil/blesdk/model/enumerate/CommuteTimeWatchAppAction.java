package com.fossil.blesdk.model.enumerate;

import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum CommuteTimeWatchAppAction {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String jsonName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CommuteTimeWatchAppAction a(String str) {
            kd4.b(str, "jsonName");
            for (CommuteTimeWatchAppAction commuteTimeWatchAppAction : CommuteTimeWatchAppAction.values()) {
                if (kd4.a((Object) commuteTimeWatchAppAction.getJsonName$blesdk_productionRelease(), (Object) str)) {
                    return commuteTimeWatchAppAction;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    CommuteTimeWatchAppAction(String str) {
        this.jsonName = str;
    }

    @DexIgnore
    public final String getJsonName$blesdk_productionRelease() {
        return this.jsonName;
    }
}
