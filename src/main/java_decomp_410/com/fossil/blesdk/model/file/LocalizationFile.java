package com.fossil.blesdk.model.file;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LocalizationFile extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int MINIMUM_DATA_SIZE; // = 22;
    @DexIgnore
    public /* final */ byte[] data;
    @DexIgnore
    public /* final */ short fileHandle;
    @DexIgnore
    public /* final */ Version fileVersion;
    @DexIgnore
    public /* final */ String localeString;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<LocalizationFile> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public LocalizationFile createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new LocalizationFile(parcel, (fd4) null);
        }

        @DexIgnore
        public LocalizationFile[] newArray(int i) {
            return new LocalizationFile[i];
        }
    }

    @DexIgnore
    public /* synthetic */ LocalizationFile(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void localeString$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final short getFileHandle$blesdk_productionRelease() {
        return this.fileHandle;
    }

    @DexIgnore
    public final Version getFileVersion$blesdk_productionRelease() {
        return this.fileVersion;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.localeString;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (!(this.data.length >= 22)) {
            throw new IllegalArgumentException("data.size(" + this.data.length + ") is not equal or larger " + "than 22");
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.FILE_HANDLE, n90.a(this.fileHandle)), JSONKey.FILE_VERSION, this.fileVersion.toString()), JSONKey.LANGUAGE_CODE, this.localeString), JSONKey.FILE_CRC_C, Long.valueOf(Crc32Calculator.a.a(this.data, Crc32Calculator.CrcType.CRC32C))), JSONKey.FILE_SIZE, Integer.valueOf(this.data.length));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.data);
        }
    }

    @DexIgnore
    public LocalizationFile(byte[] bArr) throws IllegalArgumentException {
        kd4.b(bArr, "data");
        this.data = bArr;
        i();
        this.fileHandle = ByteBuffer.wrap(ya4.a(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
        this.fileVersion = new Version(bArr[2], bArr[3]);
        this.localeString = new String(ya4.a(bArr, 12, 17), ua0.y.f());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public LocalizationFile(Parcel parcel) {
        throw null;
/*        this(r1);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
