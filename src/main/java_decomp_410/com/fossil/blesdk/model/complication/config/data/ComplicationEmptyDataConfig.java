package com.fossil.blesdk.model.complication.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationEmptyDataConfig extends ComplicationDataConfig {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationEmptyDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationEmptyDataConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ComplicationEmptyDataConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public ComplicationEmptyDataConfig[] newArray(int i) {
            return new ComplicationEmptyDataConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationEmptyDataConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return new JSONObject();
    }

    @DexIgnore
    public ComplicationEmptyDataConfig() {
        super(ComplicationDataConfigId.EMPTY);
    }

    @DexIgnore
    public ComplicationEmptyDataConfig(Parcel parcel) {
        super(parcel);
    }
}
