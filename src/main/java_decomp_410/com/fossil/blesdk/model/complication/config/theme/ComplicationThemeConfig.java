package com.fossil.blesdk.model.complication.config.theme;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.FontColor;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationThemeConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ FontColor DEFAULT_FONT_COLOR; // = FontColor.DEFAULT;
    @DexIgnore
    public /* final */ FontColor fontColor;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationThemeConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final FontColor a() {
            return ComplicationThemeConfig.DEFAULT_FONT_COLOR;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationThemeConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ComplicationThemeConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public ComplicationThemeConfig[] newArray(int i) {
            return new ComplicationThemeConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationThemeConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void fontColor$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) ComplicationThemeConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.fontColor == ((ComplicationThemeConfig) obj).fontColor;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig");
    }

    @DexIgnore
    public final FontColor getFontColor() {
        return this.fontColor;
    }

    @DexIgnore
    public int hashCode() {
        return this.fontColor.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        throw null;
        // JSONObject put = new JSONObject().put("font_color", this.fontColor.getLogName$blesdk_productionRelease());
        // kd4.a((Object) put, "JSONObject()\n           \u2026COLOR, fontColor.logName)");
        // return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.fontColor.name());
        }
    }

    @DexIgnore
    public ComplicationThemeConfig(FontColor fontColor2) {
        kd4.b(fontColor2, "fontColor");
        this.fontColor = fontColor2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ComplicationThemeConfig(Parcel parcel) {
        throw null;
        // this(FontColor.valueOf(r1));
        // String readString = parcel.readString();
        // if (readString != null) {
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
