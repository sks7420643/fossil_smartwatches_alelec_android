package com.fossil.blesdk.model.complication.config.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ComplicationDataConfigId {
    EMPTY,
    SECOND_TIMEZONE
}
