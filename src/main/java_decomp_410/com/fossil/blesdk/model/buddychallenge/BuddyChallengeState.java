package com.fossil.blesdk.model.buddychallenge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum BuddyChallengeState {
    WAITING,
    START,
    IN_PROGRESS,
    END;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName() {
        return this.logName;
    }
}
