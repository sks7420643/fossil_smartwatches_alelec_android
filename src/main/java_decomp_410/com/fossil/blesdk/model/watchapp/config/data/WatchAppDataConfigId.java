package com.fossil.blesdk.model.watchapp.config.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WatchAppDataConfigId {
    EMPTY,
    COMMUTE
}
