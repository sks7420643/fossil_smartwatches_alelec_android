package com.fossil.blesdk.model.watchapp.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppEmptyDataConfig extends WatchAppDataConfig {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchAppEmptyDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WatchAppEmptyDataConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return createFromParcel(parcel);
        }

        @DexIgnore
        public WatchAppEmptyDataConfig[] newArray(int i) {
            return new WatchAppEmptyDataConfig[i];
        }
    }

    @DexIgnore
    public WatchAppEmptyDataConfig() {
        super(WatchAppDataConfigId.EMPTY);
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return new JSONObject();
    }
}
