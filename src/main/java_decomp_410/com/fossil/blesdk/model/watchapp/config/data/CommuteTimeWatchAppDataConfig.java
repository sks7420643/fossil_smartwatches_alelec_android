package com.fossil.blesdk.model.watchapp.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.i90;
import com.fossil.blesdk.obfuscated.kd4;
import com.j256.ormlite.field.types.BooleanCharType;
import java.util.Arrays;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppDataConfig extends WatchAppDataConfig {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_DESTINATIONS; // = 10;
    @DexIgnore
    public /* final */ String[] destinations;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeWatchAppDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchAppDataConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeWatchAppDataConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeWatchAppDataConfig[] newArray(int i) {
            return new CommuteTimeWatchAppDataConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeWatchAppDataConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!kd4.a((Object) CommuteTimeWatchAppDataConfig.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.destinations, ((CommuteTimeWatchAppDataConfig) obj).destinations);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public final String[] getDestinations() {
        return this.destinations;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.destinations.hashCode();
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (this.destinations.length > 10) {
            throw new IllegalArgumentException("Destinations(" + this.destinations + ") must be less than or equal to " + BooleanCharType.DEFAULT_TRUE_FALSE_FORMAT);
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject put = new JSONObject().put("commuteApp._.config.destinations", i90.a(this.destinations));
        kd4.a((Object) put, "JSONObject()\n           \u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeStringArray(this.destinations);
        }
    }

    @DexIgnore
    public CommuteTimeWatchAppDataConfig(Parcel parcel) {
        super(parcel);
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            this.destinations = createStringArray;
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppDataConfig(String[] strArr) throws IllegalArgumentException {
        super(WatchAppDataConfigId.COMMUTE);
        kd4.b(strArr, "destinations");
        this.destinations = strArr;
        i();
    }
}
