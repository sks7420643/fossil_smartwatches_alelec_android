package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceData extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a Companion; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_MESSAGE_LENGTH; // = 32;
    @DexIgnore
    public /* final */ DeviceRequest deviceRequest;
    @DexIgnore
    public /* final */ String message;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public DeviceData(DeviceRequest deviceRequest2, String str) {
        this.deviceRequest = deviceRequest2;
        this.message = str;
    }

    @DexIgnore
    public static /* synthetic */ byte[] getResponseData$blesdk_productionRelease$default(DeviceData deviceData, short s, Version version, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                s = 0;
            }
            if ((i & 2) != 0) {
                version = ua0.y.h();
            }
            return deviceData.getResponseData$blesdk_productionRelease(s, version);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getResponseData");
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DeviceData deviceData = (DeviceData) obj;
            return !(kd4.a((Object) this.deviceRequest, (Object) deviceData.deviceRequest) ^ true) && !(kd4.a((Object) this.message, (Object) deviceData.message) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.DeviceData");
    }

    @DexIgnore
    public final DeviceRequest getDeviceRequest() {
        return this.deviceRequest;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public abstract byte[] getResponseData$blesdk_productionRelease(short s, Version version);

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return new JSONObject();
    }

    @DexIgnore
    public int hashCode() {
        DeviceRequest deviceRequest2 = this.deviceRequest;
        int i = 0;
        int hashCode = deviceRequest2 != null ? deviceRequest2.hashCode() : 0;
        String str = this.message;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d A[Catch:{ JSONException -> 0x002d }] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[Catch:{ JSONException -> 0x002d }] */
    public final JSONObject toJSONObject() throws JSONException {
        Object obj;
        Object obj2;
        JSONObject jSONObject = new JSONObject();
        try {
            JSONKey jSONKey = JSONKey.REQUEST;
            DeviceRequest deviceRequest2 = this.deviceRequest;
            if (deviceRequest2 != null) {
                obj = deviceRequest2.toJSONObject();
                if (obj != null) {
                    wa0.a(jSONObject, jSONKey, obj);
                    JSONKey jSONKey2 = JSONKey.MESSAGE;
                    obj2 = this.message;
                    if (obj2 != null) {
                        obj2 = JSONObject.NULL;
                    }
                    wa0.a(jSONObject, jSONKey2, obj2);
                    wa0.a(jSONObject, JSONKey.DATA, getResponseJSONLog());
                    return jSONObject;
                }
            }
            obj = JSONObject.NULL;
            wa0.a(jSONObject, jSONKey, obj);
            JSONKey jSONKey22 = JSONKey.MESSAGE;
            obj2 = this.message;
            if (obj2 != null) {
            }
            wa0.a(jSONObject, jSONKey22, obj2);
            wa0.a(jSONObject, JSONKey.DATA, getResponseJSONLog());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.deviceRequest, i);
        }
        if (parcel != null) {
            parcel.writeString(this.message);
        }
    }
}
