package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.MicroAppRequest;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppErrorType;
import com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.v20;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppErrorData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ MicroAppErrorType errorType;
    @DexIgnore
    public /* final */ Version microAppVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppErrorData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppErrorData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MicroAppErrorData(parcel, (fd4) null);
        }

        @DexIgnore
        public MicroAppErrorData[] newArray(int i) {
            return new MicroAppErrorData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppErrorData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void errorType$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void microAppVersion$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) MicroAppErrorData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppErrorData microAppErrorData = (MicroAppErrorData) obj;
            return !(kd4.a((Object) this.microAppVersion, (Object) microAppErrorData.microAppVersion) ^ true) && this.errorType == microAppErrorData.errorType;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.MicroAppErrorData");
    }

    @DexIgnore
    public final MicroAppErrorType getErrorType() {
        return this.errorType;
    }

    @DexIgnore
    public final Version getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // try {
        //     v20 v20 = v20.c;
        //     DeviceRequest deviceRequest = getDeviceRequest();
        //     if (deviceRequest != null) {
        //         return v20.a(s, version, new MicroAppErrorResponse(((MicroAppRequest) deviceRequest).getMicroAppEvent$blesdk_productionRelease(), new Version(this.microAppVersion.getMajor(), this.microAppVersion.getMinor()), this.errorType.getShipHandsToTwelve$blesdk_productionRelease()).getData());
        //     }
        //     throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        // } catch (FileFormatException e) {
        //     da0.l.a(e);
        //     return new byte[0];
        // }
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return wa0.a(wa0.a(super.getResponseJSONLog(), JSONKey.MICRO_APP_VERSION, this.microAppVersion.toString()), JSONKey.MICRO_APP_ERROR_TYPE, this.errorType.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public int hashCode() {
        return (((super.hashCode() * 31) + this.microAppVersion.hashCode()) * 31) + this.errorType.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppVersion, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.errorType.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppErrorData(MicroAppRequest microAppRequest, Version version, MicroAppErrorType microAppErrorType) {
        super(microAppRequest, (String) null);
        kd4.b(microAppRequest, "microAppRequest");
        kd4.b(version, "microAppVersion");
        kd4.b(microAppErrorType, "errorType");
        this.errorType = microAppErrorType;
        this.microAppVersion = version;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppErrorData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (Version) r2, MicroAppErrorType.values()[parcel.readInt()]);
        Parcelable readParcelable = parcel.readParcelable(MicroAppRequest.class.getClassLoader());
        if (readParcelable != null) {
            MicroAppRequest microAppRequest = (MicroAppRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
