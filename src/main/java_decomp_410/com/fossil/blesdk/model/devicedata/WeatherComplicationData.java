package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.CompactWeatherInfo;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.WeatherComplicationRequest;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ga0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherComplicationData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ CompactWeatherInfo weatherInfo;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherComplicationData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherComplicationData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherComplicationData(parcel, (fd4) null);
        }

        @DexIgnore
        public WeatherComplicationData[] newArray(int i) {
            return new WeatherComplicationData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherComplicationData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void weatherInfo$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) WeatherComplicationData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.weatherInfo, (Object) ((WeatherComplicationData) obj).weatherInfo) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherComplicationData");
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // ga0 ga0 = ga0.a;
        // DeviceRequest deviceRequest = getDeviceRequest();
        // return ga0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return m90.a(super.getResponseJSONLog(), this.weatherInfo.toJSONObject());
    }

    @DexIgnore
    public final CompactWeatherInfo getWeatherInfo() {
        return this.weatherInfo;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.weatherInfo.hashCode();
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("weatherInfo", this.weatherInfo.getSettingJSONData$blesdk_productionRelease());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.weatherInfo, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationData(WeatherComplicationRequest weatherComplicationRequest, CompactWeatherInfo compactWeatherInfo) {
        super(weatherComplicationRequest, (String) null);
        kd4.b(weatherComplicationRequest, "weatherComplicationRequest");
        kd4.b(compactWeatherInfo, "weatherInfo");
        this.weatherInfo = compactWeatherInfo;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationData(CompactWeatherInfo compactWeatherInfo) {
        super((DeviceRequest) null, (String) null);
        kd4.b(compactWeatherInfo, "weatherInfo");
        this.weatherInfo = compactWeatherInfo;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WeatherComplicationData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (CompactWeatherInfo) r4);
        Parcelable readParcelable = parcel.readParcelable(WeatherComplicationRequest.class.getClassLoader());
        if (readParcelable != null) {
            WeatherComplicationRequest weatherComplicationRequest = (WeatherComplicationRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(CompactWeatherInfo.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
