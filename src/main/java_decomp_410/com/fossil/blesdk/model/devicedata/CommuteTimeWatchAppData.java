package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.commutetime.CommuteTimeInfo;
import com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ga0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeWatchAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ CommuteTimeInfo commuteTimeInfo;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeWatchAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeWatchAppData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeWatchAppData(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeWatchAppData[] newArray(int i) {
            return new CommuteTimeWatchAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeWatchAppData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!kd4.a((Object) CommuteTimeWatchAppData.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.commuteTimeInfo, (Object) ((CommuteTimeWatchAppData) obj).commuteTimeInfo) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData");
    }

    @DexIgnore
    public final CommuteTimeInfo getCommuteTimeInfo() {
        return this.commuteTimeInfo;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // ga0 ga0 = ga0.a;
        // DeviceRequest deviceRequest = getDeviceRequest();
        // return ga0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000e, code lost:
        if (r2 != null) goto L_0x0013;
     */
    @DexIgnore
    public JSONObject getResponseJSONLog() {
        Object obj;
        JSONObject responseJSONLog = super.getResponseJSONLog();
        JSONKey jSONKey = JSONKey.COMMUTE_INFO;
        CommuteTimeInfo commuteTimeInfo2 = this.commuteTimeInfo;
        if (commuteTimeInfo2 != null) {
            obj = commuteTimeInfo2.toJSONObject();
        }
        obj = JSONObject.NULL;
        return wa0.a(responseJSONLog, jSONKey, obj);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        CommuteTimeInfo commuteTimeInfo2 = this.commuteTimeInfo;
        return hashCode + (commuteTimeInfo2 != null ? commuteTimeInfo2.hashCode() : 0);
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (this.commuteTimeInfo != null) {
                jSONObject2.put("dest", this.commuteTimeInfo.getDestination()).put("commute", this.commuteTimeInfo.getCommuteTimeInMinute()).put("traffic", this.commuteTimeInfo.getTraffic());
            } else {
                String message = getMessage();
                if (message == null) {
                    message = "";
                }
                int min = Math.min(32, message.length());
                if (message != null) {
                    String substring = message.substring(0, min);
                    kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    jSONObject2.put("message", substring);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            jSONObject.put("commuteApp._.config.commute_info", jSONObject2);
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            CommuteTimeInfo commuteTimeInfo2 = this.commuteTimeInfo;
            if (commuteTimeInfo2 != null) {
                parcel.writeParcelable(commuteTimeInfo2, i);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.os.Parcelable");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppData(CommuteTimeWatchAppRequest commuteTimeWatchAppRequest, CommuteTimeInfo commuteTimeInfo2) throws IllegalArgumentException {
        super(commuteTimeWatchAppRequest, (String) null);
        kd4.b(commuteTimeWatchAppRequest, "commuteTimeWatchAppRequest");
        kd4.b(commuteTimeInfo2, "commuteTimeInfo");
        this.commuteTimeInfo = commuteTimeInfo2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppData(CommuteTimeInfo commuteTimeInfo2) throws IllegalArgumentException {
        super((DeviceRequest) null, (String) null);
        kd4.b(commuteTimeInfo2, "commuteTimeInfo");
        this.commuteTimeInfo = commuteTimeInfo2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppData(CommuteTimeWatchAppRequest commuteTimeWatchAppRequest, String str) throws IllegalArgumentException {
        super(commuteTimeWatchAppRequest, str);
        kd4.b(commuteTimeWatchAppRequest, "commuteTimeWatchAppRequest");
        kd4.b(str, "message");
        this.commuteTimeInfo = null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppData(String str) throws IllegalArgumentException {
        super((DeviceRequest) null, str);
        kd4.b(str, "message");
        this.commuteTimeInfo = null;
    }

    @DexIgnore
    public CommuteTimeWatchAppData(Parcel parcel) {
        super((DeviceRequest) parcel.readParcelable(CommuteTimeWatchAppRequest.class.getClassLoader()), parcel.readString());
        this.commuteTimeInfo = (CommuteTimeInfo) parcel.readParcelable(CommuteTimeInfo.class.getClassLoader());
    }
}
