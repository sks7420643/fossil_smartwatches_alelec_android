package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.weather.ChanceOfRainInfo;
import com.fossil.blesdk.device.event.request.ChanceOfRainComplicationRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ga0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainComplicationData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ChanceOfRainInfo chanceOfRainInfo;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ChanceOfRainComplicationData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ChanceOfRainComplicationData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ChanceOfRainComplicationData(parcel, (fd4) null);
        }

        @DexIgnore
        public ChanceOfRainComplicationData[] newArray(int i) {
            return new ChanceOfRainComplicationData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ChanceOfRainComplicationData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void chanceOfRainInfo$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) ChanceOfRainComplicationData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.chanceOfRainInfo, (Object) ((ChanceOfRainComplicationData) obj).chanceOfRainInfo) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.ChanceOfRainComplicationData");
    }

    @DexIgnore
    public final ChanceOfRainInfo getChanceOfRainInfo() {
        return this.chanceOfRainInfo;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // ga0 ga0 = ga0.a;
        // DeviceRequest deviceRequest = getDeviceRequest();
        // return ga0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return m90.a(super.getResponseJSONLog(), this.chanceOfRainInfo.toJSONObject());
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.chanceOfRainInfo.hashCode();
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("chanceOfRainSSE._.config.info", this.chanceOfRainInfo.getSettingJSONData$blesdk_productionRelease());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.chanceOfRainInfo, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationData(ChanceOfRainComplicationRequest chanceOfRainComplicationRequest, ChanceOfRainInfo chanceOfRainInfo2) {
        super(chanceOfRainComplicationRequest, (String) null);
        kd4.b(chanceOfRainComplicationRequest, "chanceOfRainComplicationRequest");
        kd4.b(chanceOfRainInfo2, "chanceOfRainInfo");
        this.chanceOfRainInfo = chanceOfRainInfo2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationData(ChanceOfRainInfo chanceOfRainInfo2) {
        super((DeviceRequest) null, (String) null);
        kd4.b(chanceOfRainInfo2, "chanceOfRainInfo");
        this.chanceOfRainInfo = chanceOfRainInfo2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ChanceOfRainComplicationData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (ChanceOfRainInfo) r4);
        Parcelable readParcelable = parcel.readParcelable(ChanceOfRainComplicationRequest.class.getClassLoader());
        if (readParcelable != null) {
            ChanceOfRainComplicationRequest chanceOfRainComplicationRequest = (ChanceOfRainComplicationRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(ChanceOfRainInfo.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
