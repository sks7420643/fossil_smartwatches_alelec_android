package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.event.request.BuddyChallengeStepUpdatedRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.model.buddychallenge.BuddyChallenge;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ga0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BuddyChallengeWatchAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ BuddyChallenge buddyChallenge;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BuddyChallengeWatchAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BuddyChallengeWatchAppData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BuddyChallengeWatchAppData(parcel, (fd4) null);
        }

        @DexIgnore
        public BuddyChallengeWatchAppData[] newArray(int i) {
            return new BuddyChallengeWatchAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BuddyChallengeWatchAppData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BuddyChallengeWatchAppData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.buddyChallenge, (Object) ((BuddyChallengeWatchAppData) obj).buddyChallenge) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppData");
    }

    @DexIgnore
    public final BuddyChallenge getBuddyChallenge() {
        return this.buddyChallenge;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // ga0 ga0 = ga0.a;
        // DeviceRequest deviceRequest = getDeviceRequest();
        // return ga0.a(deviceRequest != null ? Integer.valueOf(deviceRequest.getRequestId$blesdk_productionRelease()) : null, i());
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return m90.a(super.getResponseJSONLog(), this.buddyChallenge.toJSONObject());
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.buddyChallenge.hashCode();
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("buddyChallengeApp._.config.sync", this.buddyChallenge.toJSONObject());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.buddyChallenge, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BuddyChallengeWatchAppData(BuddyChallengeStepUpdatedRequest buddyChallengeStepUpdatedRequest, BuddyChallenge buddyChallenge2) {
        super(buddyChallengeStepUpdatedRequest, (String) null);
        kd4.b(buddyChallengeStepUpdatedRequest, "buddyChallengeStepUpdatedRequest");
        kd4.b(buddyChallenge2, "buddyChallenge");
        this.buddyChallenge = buddyChallenge2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BuddyChallengeWatchAppData(BuddyChallenge buddyChallenge2) {
        super((DeviceRequest) null, (String) null);
        kd4.b(buddyChallenge2, "buddyChallenge");
        this.buddyChallenge = buddyChallenge2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public BuddyChallengeWatchAppData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (BuddyChallenge) r4);
        Parcelable readParcelable = parcel.readParcelable(BuddyChallengeStepUpdatedRequest.class.getClassLoader());
        if (readParcelable != null) {
            BuddyChallengeStepUpdatedRequest buddyChallengeStepUpdatedRequest = (BuddyChallengeStepUpdatedRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(BuddyChallenge.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
