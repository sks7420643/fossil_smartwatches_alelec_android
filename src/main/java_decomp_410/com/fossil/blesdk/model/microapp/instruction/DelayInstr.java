package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DelayInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ double delayInSecond;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DelayInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DelayInstr createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DelayInstr(parcel, (fd4) null);
        }

        @DexIgnore
        public DelayInstr[] newArray(int i) {
            return new DelayInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DelayInstr(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DelayInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.delayInSecond == ((DelayInstr) obj).delayInSecond;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.DelayInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort((short) ((int) ((this.delayInSecond * ((double) 1000)) / 100.0d)));
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return (int) this.delayInSecond;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(super.toJSONObject(), JSONKey.DELAY_IN_SECOND, Double.valueOf(this.delayInSecond));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.delayInSecond);
        }
    }

    @DexIgnore
    public DelayInstr(double d) {
        super(InstructionId.DELAY);
        this.delayInSecond = d;
    }

    @DexIgnore
    public DelayInstr(Parcel parcel) {
        super(parcel);
        this.delayInSecond = parcel.readDouble();
    }
}
