package com.fossil.blesdk.model.microapp.configuration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.model.microapp.MicroAppMapping;
import com.fossil.blesdk.model.microapp.customization.MicroAppCustomization;
import com.fossil.blesdk.model.microapp.declaration.MicroAppDeclaration;
import com.fossil.blesdk.model.microapp.enumerate.FileType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ha0;
import com.fossil.blesdk.obfuscated.ja0;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.ka0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppConfiguration extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ List<byte[]> customizationFrames;
    @DexIgnore
    public /* final */ List<byte[]> declarationFrames;
    @DexIgnore
    public /* final */ List<byte[]> launchFrames;
    @DexIgnore
    public /* final */ MicroAppMapping[] mappings;
    @DexIgnore
    public /* final */ Version systemVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppConfiguration> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppConfiguration createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MicroAppConfiguration(parcel, (fd4) null);
        }

        @DexIgnore
        public MicroAppConfiguration[] newArray(int i) {
            return new MicroAppConfiguration[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppConfiguration(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final byte[] a(List<byte[]> list) {
        byte[] bArr = new byte[0];
        for (byte[] a2 : list) {
            bArr = k90.a(bArr, a2);
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 1).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) list.size());
        order.put(bArr);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) MicroAppConfiguration.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppConfiguration microAppConfiguration = (MicroAppConfiguration) obj;
            return !(kd4.a((Object) this.systemVersion, (Object) microAppConfiguration.systemVersion) ^ true) && Arrays.equals(this.mappings, microAppConfiguration.mappings);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration");
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] a2 = k90.a(k90.a(k90.a(new byte[]{this.systemVersion.getMajor(), this.systemVersion.getMinor(), FileType.CONFIGURATION_FILE.getValue()}, a(this.launchFrames)), a(this.declarationFrames)), a(this.customizationFrames));
        int a3 = (int) Crc32Calculator.a.a(a2, Crc32Calculator.CrcType.CRC32);
        ByteBuffer order = ByteBuffer.allocate(a2.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        order.put(a2);
        order.putInt(a3);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return (this.systemVersion.hashCode() * 31) + this.mappings.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (MicroAppMapping jSONObject : this.mappings) {
            jSONArray.put(jSONObject.toJSONObject());
        }
        return wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.SYSTEM_VERSION, this.systemVersion.toJSONObject()), JSONKey.MICRO_APP_MAPPINGS, jSONArray), JSONKey.TOTAL_MAPPINGS, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedArray(this.mappings, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.systemVersion, i);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppConfiguration(MicroAppMapping[] microAppMappingArr, Version version, int i, fd4 fd4) {
        this(microAppMappingArr, (i & 2) != 0 ? new Version((byte) 1, (byte) 0) : version);
    }

    @DexIgnore
    public MicroAppConfiguration(MicroAppMapping[] microAppMappingArr, Version version) {
        kd4.b(microAppMappingArr, "mappings");
        kd4.b(version, "systemVersion");
        this.declarationFrames = new ArrayList();
        this.customizationFrames = new ArrayList();
        this.launchFrames = new ArrayList();
        this.mappings = microAppMappingArr;
        this.systemVersion = version;
        for (MicroAppMapping microAppMapping : microAppMappingArr) {
            ArrayList arrayList = new ArrayList();
            for (MicroAppDeclaration microAppDeclaration : microAppMapping.getMicroAppDeclarations()) {
                ha0 ha0 = new ha0(microAppDeclaration.getDeclarationFileId$blesdk_productionRelease(), microAppDeclaration.getMicroAppVersion(), microAppDeclaration.getVariationNumber());
                arrayList.add(new ka0(ha0));
                this.declarationFrames.add(microAppDeclaration.getData());
                MicroAppCustomization customization = microAppDeclaration.getCustomization();
                if (customization != null) {
                    customization.setMicroAppHandle$blesdk_productionRelease(ha0);
                    this.customizationFrames.add(customization.getData$blesdk_productionRelease());
                }
            }
            this.launchFrames.add(new ja0(microAppMapping.getMicroAppButton(), arrayList).a());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppConfiguration(Parcel parcel) {
        throw null;
/*        this(r0, (Version) r4);
        Object[] createTypedArray = parcel.createTypedArray(MicroAppMapping.CREATOR);
        if (createTypedArray != null) {
            MicroAppMapping[] microAppMappingArr = (MicroAppMapping[]) createTypedArray;
            Parcelable readParcelable = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
