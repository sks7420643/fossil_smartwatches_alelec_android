package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.la0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Instruction extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ InstructionId id;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<Instruction> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public Instruction createFromParcel(Parcel parcel) {
            throw null;
            // kd4.b(parcel, "parcel");
            // String readString = parcel.readString();
            // if (readString != null) {
            //     InstructionId valueOf = InstructionId.valueOf(readString);
            //     parcel.setDataPosition(0);
            //     switch (la0.a[valueOf.ordinal()]) {
            //         case 1:
            //             return AnimationInstr.CREATOR.createFromParcel(parcel);
            //         case 2:
            //             return CloseInstr.CREATOR.createFromParcel(parcel);
            //         case 3:
            //             return SwitchActivityInstr.CREATOR.createFromParcel(parcel);
            //         case 4:
            //             return StartCriticalInstr.CREATOR.createFromParcel(parcel);
            //         case 5:
            //             return EndCriticalInstr.CREATOR.createFromParcel(parcel);
            //         case 6:
            //             return RemapInstr.CREATOR.createFromParcel(parcel);
            //         case 7:
            //             return StartRepeatInstr.CREATOR.createFromParcel(parcel);
            //         case 8:
            //             return EndRepeatInstr.CREATOR.createFromParcel(parcel);
            //         case 9:
            //             return DelayInstr.CREATOR.createFromParcel(parcel);
            //         case 10:
            //             return VibeInstr.CREATOR.createFromParcel(parcel);
            //         case 11:
            //             return StreamingInstr.CREATOR.createFromParcel(parcel);
            //         case 12:
            //             return HIDInstr.CREATOR.createFromParcel(parcel);
            //         default:
            //             throw new NoWhenBranchMatchedException();
            //     }
            // } else {
            //     kd4.a();
            //     throw null;
            // }
        }

        @DexIgnore
        public Instruction[] newArray(int i) {
            return new Instruction[i];
        }
    }

    @DexIgnore
    public Instruction(InstructionId instructionId) {
        kd4.b(instructionId, "id");
        this.id = instructionId;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.id == ((Instruction) obj).id;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.Instruction");
    }

    @DexIgnore
    public final byte[] getData() {
        ByteBuffer order = ByteBuffer.allocate(getParameters$blesdk_productionRelease().length + 2).order(ByteOrder.LITTLE_ENDIAN);
        order.putShort((short) ((getParameters$blesdk_productionRelease().length << 7) | this.id.getValue()));
        order.put(getParameters$blesdk_productionRelease());
        byte[] array = order.array();
        kd4.a((Object) array, "dataBuffer.array()");
        return array;
    }

    @DexIgnore
    public abstract byte[] getParameters$blesdk_productionRelease();

    @DexIgnore
    public int hashCode() {
        return this.id.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(new JSONObject(), JSONKey.INSTRUCTION_ID, this.id.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.id.getValue());
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Instruction(Parcel parcel) {
        throw null;
        // this(InstructionId.valueOf(r2));
        // kd4.b(parcel, "parcel");
        // String readString = parcel.readString();
        // if (readString != null) {
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
