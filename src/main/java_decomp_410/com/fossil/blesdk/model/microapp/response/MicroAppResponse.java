package com.fossil.blesdk.model.microapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.model.microapp.enumerate.FileType;
import com.fossil.blesdk.model.microapp.instruction.Instruction;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppResponse extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ MicroAppEvent event;
    @DexIgnore
    public /* final */ Version systemVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppResponse> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppResponse createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            String readString = parcel.readString();
            if (kd4.a((Object) readString, (Object) MicroAppCommuteTimeResponse.class.getName())) {
                return MicroAppCommuteTimeResponse.CREATOR.createFromParcel(parcel);
            }
            if (kd4.a((Object) readString, (Object) MicroAppRingPhoneResponse.class.getName())) {
                return MicroAppRingPhoneResponse.CREATOR.createFromParcel(parcel);
            }
            if (kd4.a((Object) readString, (Object) MicroAppErrorResponse.class.getName())) {
                return MicroAppErrorResponse.CREATOR.createFromParcel(parcel);
            }
            return null;
        }

        @DexIgnore
        public MicroAppResponse[] newArray(int i) {
            return new MicroAppResponse[i];
        }
    }

    @DexIgnore
    public MicroAppResponse(MicroAppEvent microAppEvent, Version version) {
        kd4.b(microAppEvent, Constants.EVENT);
        kd4.b(version, "systemVersion");
        this.event = microAppEvent;
        this.systemVersion = version;
    }

    @DexIgnore
    public static /* synthetic */ void systemVersion$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppResponse microAppResponse = (MicroAppResponse) obj;
            return !(kd4.a((Object) this.event, (Object) microAppResponse.event) ^ true) && !(kd4.a((Object) this.systemVersion, (Object) microAppResponse.systemVersion) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppResponse");
    }

    @DexIgnore
    public final byte[] getData() {
        byte[] a2 = ya4.a(this.event.getData$blesdk_productionRelease(), 0, 8);
        byte[] a3 = k90.a(k90.a(k90.a(new byte[0], new byte[]{this.systemVersion.getMajor(), this.systemVersion.getMinor(), FileType.REMOTE_ACTIVITY.getValue()}), a2), i());
        int a4 = (int) Crc32Calculator.a.a(a3, Crc32Calculator.CrcType.CRC32);
        ByteBuffer order = ByteBuffer.allocate(a3.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(resp\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(a3);
        order.putInt(a4);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public final MicroAppEvent getEvent() {
        return this.event;
    }

    @DexIgnore
    public abstract List<Instruction> getInstructions();

    @DexIgnore
    public final Version getSystemVersion() {
        return this.systemVersion;
    }

    @DexIgnore
    public int hashCode() {
        return (this.event.hashCode() * 31) + this.systemVersion.hashCode();
    }

    @DexIgnore
    public final byte[] i() {
        byte[] bArr = new byte[0];
        for (Instruction data : getInstructions()) {
            bArr = k90.a(bArr, data.getData());
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(3 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put((byte) 255);
        order.putShort((short) (((short) bArr.length) + 3));
        order.put(bArr);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.MICRO_APP_EVENT, this.event.toJSONObject()), JSONKey.SYSTEM_VERSION, this.systemVersion.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getName());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.event, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.systemVersion, i);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppResponse(Parcel parcel) {
        throw null;
/*        this(r0, (Version) r4);
        kd4.b(parcel, "parcel");
        Parcelable readParcelable = parcel.readParcelable(MicroAppEvent.class.getClassLoader());
        if (readParcelable != null) {
            MicroAppEvent microAppEvent = (MicroAppEvent) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
