package com.fossil.blesdk.model.microapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.ia0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppEvent extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public byte activityId;
    @DexIgnore
    public byte contextNumber;
    @DexIgnore
    public /* final */ byte[] data;
    @DexIgnore
    public /* final */ short declarationId;
    @DexIgnore
    public byte eventId;
    @DexIgnore
    public byte microAppEvent;
    @DexIgnore
    public /* final */ MicroAppId microAppId;
    @DexIgnore
    public /* final */ byte requestId;
    @DexIgnore
    public /* final */ MicroAppVariantId variant;
    @DexIgnore
    public byte variationNumber;
    @DexIgnore
    public /* final */ byte version;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MicroAppEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public MicroAppEvent[] newArray(int i) {
            return new MicroAppEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) MicroAppEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.data, ((MicroAppEvent) obj).data);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppEvent");
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        return this.data;
    }

    @DexIgnore
    public final MicroAppId getMicroAppId() {
        return this.microAppId;
    }

    @DexIgnore
    public final byte getRequestId() {
        return this.requestId;
    }

    @DexIgnore
    public final MicroAppVariantId getVariant() {
        return this.variant;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.data);
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.MICRO_APP_ID, this.microAppId.name()), JSONKey.VARIANT, this.variant), JSONKey.MICRO_APP_VERSION, Byte.valueOf(this.version)), JSONKey.VARIATION_NUMBER, Byte.valueOf(this.variationNumber)), JSONKey.CONTEXT_NUMBER, Byte.valueOf(this.contextNumber)), JSONKey.ACTIVITY_ID, Byte.valueOf(this.activityId)), JSONKey.EVENT_ID, Byte.valueOf(this.eventId)), JSONKey.REQUEST_ID, Byte.valueOf(this.requestId)), JSONKey.MICRO_APP_EVENT, Byte.valueOf(this.microAppEvent));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.data);
        }
    }

    @DexIgnore
    public MicroAppEvent(byte[] bArr) {
        kd4.b(bArr, "data");
        this.data = bArr;
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.wrap(data).or\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.version = order.get(0);
        this.declarationId = order.getShort(1);
        this.variationNumber = order.get(3);
        this.contextNumber = order.get(4);
        this.activityId = order.get(5);
        this.eventId = order.get(6);
        this.requestId = order.get(7);
        this.microAppEvent = order.get(8);
        this.microAppId = ia0.b.a(this.declarationId);
        this.variant = ia0.b.b(this.declarationId);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MicroAppEvent(Parcel parcel) {
        throw null;
/*        this(r1);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
