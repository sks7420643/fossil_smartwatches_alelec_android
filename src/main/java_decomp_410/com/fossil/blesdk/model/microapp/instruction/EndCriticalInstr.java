package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EndCriticalInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<EndCriticalInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public EndCriticalInstr createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new EndCriticalInstr(parcel, (fd4) null);
        }

        @DexIgnore
        public EndCriticalInstr[] newArray(int i) {
            return new EndCriticalInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ EndCriticalInstr(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        return new byte[0];
    }

    @DexIgnore
    public EndCriticalInstr() {
        super(InstructionId.END_CRITICAL);
    }

    @DexIgnore
    public EndCriticalInstr(Parcel parcel) {
        super(parcel);
    }
}
