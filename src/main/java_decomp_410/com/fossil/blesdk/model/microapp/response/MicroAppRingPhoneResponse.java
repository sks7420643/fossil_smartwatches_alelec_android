package com.fossil.blesdk.model.microapp.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.model.microapp.instruction.CloseInstr;
import com.fossil.blesdk.model.microapp.instruction.Instruction;
import com.fossil.blesdk.obfuscated.bb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppRingPhoneResponse extends MicroAppResponse {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppRingPhoneResponse> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppRingPhoneResponse createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MicroAppRingPhoneResponse(parcel, (fd4) null);
        }

        @DexIgnore
        public MicroAppRingPhoneResponse[] newArray(int i) {
            return new MicroAppRingPhoneResponse[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppRingPhoneResponse(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public List<Instruction> getInstructions() {
        return bb4.a(new CloseInstr());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRingPhoneResponse(MicroAppEvent microAppEvent, Version version) {
        super(microAppEvent, version);
        kd4.b(microAppEvent, Constants.EVENT);
        kd4.b(version, "systemVersion");
    }

    @DexIgnore
    public MicroAppRingPhoneResponse(Parcel parcel) {
        super(parcel);
    }
}
