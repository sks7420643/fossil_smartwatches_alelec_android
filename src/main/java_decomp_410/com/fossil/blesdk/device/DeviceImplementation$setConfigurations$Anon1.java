package com.fossil.blesdk.device;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setConfigurations$Anon1 extends Lambda implements xc4<DeviceConfigItem, String> {
    @DexIgnore
    public static /* final */ DeviceImplementation$setConfigurations$Anon1 INSTANCE; // = new DeviceImplementation$setConfigurations$Anon1();

    @DexIgnore
    public DeviceImplementation$setConfigurations$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(DeviceConfigItem deviceConfigItem) {
        kd4.b(deviceConfigItem, "it");
        return deviceConfigItem.toJSONString(2);
    }
}
