package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1 */
public final class C0828xedddfd2f extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a */
    public static final class C0829a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0828xedddfd2f f2452e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f2453f;

        @DexIgnore
        public C0829a(com.fossil.blesdk.device.C0828xedddfd2f deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1, java.lang.Object obj) {
            this.f2452e = deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2453f = obj;
        }

        @DexIgnore
        public final void run() {
            this.f2452e.$progressTask.mo16948c(this.f2453f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b */
    public static final class C0830b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0828xedddfd2f f2454e;

        @DexIgnore
        public C0830b(com.fossil.blesdk.device.C0828xedddfd2f deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1) {
            this.f2454e = deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
        }

        @DexIgnore
        public final void run() {
            this.f2454e.$progressTask.mo16946b(new com.fossil.blesdk.device.FeatureError(com.fossil.blesdk.device.FeatureErrorCode.UNKNOWN_ERROR, (com.fossil.blesdk.device.logic.phase.Phase.Result) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c */
    public static final class C0831c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0828xedddfd2f f2455e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2456f;

        @DexIgnore
        public C0831c(com.fossil.blesdk.device.C0828xedddfd2f deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1, com.fossil.blesdk.device.logic.phase.Phase phase) {
            this.f2455e = deviceImplementation$sendAppNotification$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2456f = phase;
        }

        @DexIgnore
        public final void run() {
            this.f2455e.$progressTask.mo16946b(com.fossil.blesdk.device.FeatureError.Companion.mo6343a(this.f2456f.mo7565k(), com.fossil.blesdk.obfuscated.rb4.m27313a((kotlin.Pair<? extends K, ? extends V>[]) new kotlin.Pair[]{com.fossil.blesdk.obfuscated.oa4.m26076a(com.fossil.blesdk.device.FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, java.lang.Boolean.valueOf(this.f2455e.this$0.mo5950q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0828xedddfd2f(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        if (phase.mo7565k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            java.lang.Object i = phase.mo7345i();
            if (i instanceof com.fossil.blesdk.obfuscated.qa4) {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0828xedddfd2f.C0829a(this, i));
            } else {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0828xedddfd2f.C0830b(this));
            }
        } else {
            this.this$0.f2346f.post(new com.fossil.blesdk.device.C0828xedddfd2f.C0831c(this, phase));
            this.this$0.mo5919a(phase.mo7565k());
        }
    }
}
