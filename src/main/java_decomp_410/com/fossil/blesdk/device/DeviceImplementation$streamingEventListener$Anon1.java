package com.fossil.blesdk.device;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$streamingEventListener$Anon1 extends Lambda implements xc4<AsyncEvent, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$streamingEventListener$Anon1(DeviceImplementation deviceImplementation) {
        super(1);
        this.this$Anon0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((AsyncEvent) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(AsyncEvent asyncEvent) {
        kd4.b(asyncEvent, "asyncEvent");
        this.this$Anon0.k.a(this.this$Anon0, asyncEvent);
    }
}
