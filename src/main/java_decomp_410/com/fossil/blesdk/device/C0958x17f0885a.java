package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$159 */
public final class C0958x17f0885a extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$159$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$159$a */
    public static final class C0959a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0958x17f0885a f2586e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2587f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ float f2588g;

        @DexIgnore
        public C0959a(com.fossil.blesdk.device.C0958x17f0885a deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$159, com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
            this.f2586e = deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$159;
            this.f2587f = phase;
            this.f2588g = f;
        }

        @DexIgnore
        public final void run() {
            com.fossil.blesdk.device.DeviceImplementation deviceImplementation = this.f2586e.this$0;
            com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
            java.lang.String logName$blesdk_productionRelease = this.f2587f.mo7562g().getLogName$blesdk_productionRelease();
            com.fossil.blesdk.device.DeviceImplementation.m2997a(deviceImplementation, logLevel, logName$blesdk_productionRelease, "Progress: " + this.f2588g, false, 8, (java.lang.Object) null);
            this.f2586e.$progressTask.mo16943a(this.f2588g);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0958x17f0885a(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(2);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executingPhase");
        this.this$0.f2346f.post(new com.fossil.blesdk.device.C0958x17f0885a.C0959a(this, phase, f));
    }
}
