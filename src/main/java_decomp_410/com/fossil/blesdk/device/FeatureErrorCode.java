package com.fossil.blesdk.device;

import com.fossil.blesdk.device.FeatureError;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.FileControlStatusCode;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.u00;
import java.util.HashMap;
import java.util.Locale;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FeatureErrorCode implements h90 {
    BLUETOOTH_OFF(0),
    INVALID_PARAMETERS(1),
    DEVICE_BUSY(2),
    EXECUTION_TIMEOUT(3),
    REQUEST_FAILED(4),
    REQUEST_UNSUPPORTED(5),
    RESPONSE_TIMEOUT(6),
    RESPONSE_FAILED(7),
    CONNECTION_DROPPED(8),
    INTERRUPTED(9),
    SERVICE_CHANGED(10),
    AUTHENTICATION_FAILED(11),
    SECRET_KEY_IS_REQUIRED(12),
    DATA_SIZE_OVER_LIMIT(13),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ FeatureErrorCode a(a aVar, Phase.Result result, HashMap hashMap, int i, Object obj) {
            if ((i & 2) != 0) {
                hashMap = new HashMap();
            }
            return aVar.a(result, hashMap);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final FeatureErrorCode a(Phase.Result result, HashMap<FeatureError.PhaseResultToFeatureErrorConversionOption, Object> hashMap) {
            throw null;
            // kd4.b(result, "phaseResult");
            // kd4.b(hashMap, "options");
            // Boolean bool = (Boolean) hashMap.get(FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED);
            // boolean booleanValue = bool != null ? bool.booleanValue() : false;
            // switch (u00.a[result.getResultCode().ordinal()]) {
            //     case 1:
            //         return FeatureErrorCode.INTERRUPTED;
            //     case 2:
            //         if (booleanValue) {
            //             return FeatureErrorCode.SERVICE_CHANGED;
            //         }
            //         return FeatureErrorCode.CONNECTION_DROPPED;
            //     case 3:
            //         return FeatureErrorCode.REQUEST_UNSUPPORTED;
            //     case 4:
            //         if (result.getRequestResult().getResultCode() == Request.Result.ResultCode.TIMEOUT || result.getRequestResult().getResultCode() == Request.Result.ResultCode.EOF_TIME_OUT) {
            //             return FeatureErrorCode.RESPONSE_TIMEOUT;
            //         }
            //         if (result.getRequestResult().getResponseStatus() == FileControlStatusCode.SIZE_OVER_LIMIT) {
            //             return FeatureErrorCode.DATA_SIZE_OVER_LIMIT;
            //         }
            //         return FeatureErrorCode.REQUEST_FAILED;
            //     case 5:
            //     case 6:
            //     case 7:
            //     case 8:
            //     case 9:
            //     case 10:
            //     case 11:
            //     case 12:
            //     case 13:
            //     case 14:
            //     case 15:
            //     case 16:
            //         return FeatureErrorCode.RESPONSE_FAILED;
            //     case 17:
            //         return FeatureErrorCode.EXECUTION_TIMEOUT;
            //     case 18:
            //     case 19:
            //         return FeatureErrorCode.REQUEST_UNSUPPORTED;
            //     case 20:
            //         return FeatureErrorCode.BLUETOOTH_OFF;
            //     case 21:
            //     case 22:
            //         return FeatureErrorCode.AUTHENTICATION_FAILED;
            //     case 23:
            //         return FeatureErrorCode.SECRET_KEY_IS_REQUIRED;
            //     case 24:
            //         return FeatureErrorCode.INVALID_PARAMETERS;
            //     case 25:
            //         return FeatureErrorCode.REQUEST_FAILED;
            //     case 26:
            //         return FeatureErrorCode.DEVICE_BUSY;
            //     case 27:
            //     case 28:
            //     case 29:
            //     case 30:
            //         return FeatureErrorCode.UNKNOWN_ERROR;
            //     default:
            //         throw new NoWhenBranchMatchedException();
            // }
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    FeatureErrorCode(int i) {
        this.code = i;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int getCode() {
        return this.code;
    }

    @DexIgnore
    public String getLogName() {
        return this.logName;
    }
}
