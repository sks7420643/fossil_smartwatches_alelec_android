package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionManager$checkAndMaintainDeviceHIDConnection$1 implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: e */
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation f2341e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1$1")
    /* renamed from: com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1$1 */
    public static final class C07531 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.qa4, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C07531(com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1 connectionManager$checkAndMaintainDeviceHIDConnection$1) {
            super(1);
            this.this$0 = connectionManager$checkAndMaintainDeviceHIDConnection$1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.obfuscated.qa4) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.obfuscated.qa4 qa4) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(qa4, "it");
            com.fossil.blesdk.device.ConnectionManager.f2334e.remove(this.this$0.f2341e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1$2")
    /* renamed from: com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1$2 */
    public static final class C07542 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.error.Error, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C07542(com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1 connectionManager$checkAndMaintainDeviceHIDConnection$1) {
            super(1);
            this.this$0 = connectionManager$checkAndMaintainDeviceHIDConnection$1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.error.Error) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.error.Error error) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(error, "error");
            com.fossil.blesdk.device.ConnectionManager.f2334e.remove(this.this$0.f2341e);
            com.fossil.blesdk.device.FeatureError featureError = (com.fossil.blesdk.device.FeatureError) error;
            if (featureError.getErrorCode() == com.fossil.blesdk.device.FeatureErrorCode.REQUEST_UNSUPPORTED) {
                com.fossil.blesdk.device.ConnectionManager.f2339j.mo5867g(this.this$0.f2341e);
                this.this$0.f2341e.mo5920a(error);
            } else if (featureError.getErrorCode() == com.fossil.blesdk.device.FeatureErrorCode.REQUEST_FAILED && featureError.getPhaseResult$blesdk_productionRelease().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.HID_INPUT_DEVICE_DISABLED) {
                com.fossil.blesdk.device.ConnectionManager.f2333d.add(this.this$0.f2341e);
            } else {
                com.fossil.blesdk.device.ConnectionManager.f2339j.mo5855a(this.this$0.f2341e, com.fossil.blesdk.obfuscated.lb0.f7176b.mo13221a("HID_EXPONENT_BACK_OFF_TAG"));
            }
        }
    }

    @DexIgnore
    public ConnectionManager$checkAndMaintainDeviceHIDConnection$1(com.fossil.blesdk.device.DeviceImplementation deviceImplementation) {
        this.f2341e = deviceImplementation;
    }

    @DexIgnore
    public final void run() {
        com.fossil.blesdk.device.ConnectionManager.f2333d.remove(this.f2341e);
        if (com.fossil.blesdk.device.ConnectionManager.f2339j.mo5865e(this.f2341e) && com.fossil.blesdk.adapter.BluetoothLeAdapter.f2311l.mo5762c() == com.fossil.blesdk.adapter.BluetoothLeAdapter.State.ENABLED) {
            if (com.fossil.blesdk.obfuscated.n00.f7623c[this.f2341e.mo5948o().ordinal()] == 1 && com.fossil.blesdk.device.ConnectionManager.f2334e.add(this.f2341e)) {
                com.fossil.blesdk.device.DeviceImplementation.m2992a(this.f2341e, (java.util.HashMap) null, 1, (java.lang.Object) null).mo10801e(new com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1.C07531(this)).mo10800c(new com.fossil.blesdk.device.ConnectionManager$checkAndMaintainDeviceHIDConnection$1.C07542(this));
            }
        }
    }
}
