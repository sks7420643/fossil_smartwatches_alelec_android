package com.fossil.blesdk.device.core;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$executeBluetoothCommand$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.core.command.BluetoothCommand, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.core.command.BluetoothCommand $bluetoothCommand;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.core.Peripheral this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Peripheral$executeBluetoothCommand$2(com.fossil.blesdk.device.core.Peripheral peripheral, com.fossil.blesdk.device.core.command.BluetoothCommand bluetoothCommand) {
        super(1);
        this.this$0 = peripheral;
        this.$bluetoothCommand = bluetoothCommand;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.core.command.BluetoothCommand) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.core.command.BluetoothCommand bluetoothCommand) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothCommand, "it");
        this.this$0.mo6435b(this.$bluetoothCommand);
    }
}
