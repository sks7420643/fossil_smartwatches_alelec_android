package com.fossil.blesdk.device.core.gatt;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.r10;
import com.fossil.blesdk.obfuscated.ua0;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattDescriptor {
    @DexIgnore
    public static /* final */ byte[] a; // = {1, 0};
    @DexIgnore
    public static /* final */ byte[] b; // = {2, 0};
    @DexIgnore
    public static /* final */ byte[] c; // = {0, 0};
    @DexIgnore
    public static /* final */ a d; // = new a((fd4) null);

    @DexIgnore
    public enum DescriptorId {
        CLIENT_CHARACTERISTIC_CONFIGURATION,
        UNKNOWN;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final UUID getUuid() {
            throw null;
            // if (r10.a[ordinal()] != 1) {
            //     return null;
            // }
            // return ua0.y.a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DescriptorId a(UUID uuid) {
            kd4.b(uuid, "uuid");
            if (kd4.a((Object) uuid, (Object) ua0.y.a())) {
                return DescriptorId.CLIENT_CHARACTERISTIC_CONFIGURATION;
            }
            return DescriptorId.UNKNOWN;
        }

        @DexIgnore
        public final byte[] b() {
            return GattDescriptor.b;
        }

        @DexIgnore
        public final byte[] c() {
            return GattDescriptor.a;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final byte[] a() {
            return GattDescriptor.c;
        }
    }
}
