package com.fossil.blesdk.device.core.command;

import android.os.Handler;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.b10;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ra0;
import com.fossil.blesdk.obfuscated.sa0;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.Locale;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BluetoothCommand implements sa0<GattOperationResult> {
    @DexIgnore
    public /* final */ String a; // = this.i.getLogName$blesdk_productionRelease();
    @DexIgnore
    public /* final */ Handler b; // = hb0.a.a();
    @DexIgnore
    public /* final */ int c; // = 255;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Result e; // = new Result(this.i, Result.ResultCode.NOT_START, (GattOperationResult.GattResult) null, 4, (fd4) null);
    @DexIgnore
    public xc4<? super BluetoothCommand, qa4> f;
    @DexIgnore
    public xc4<? super BluetoothCommand, qa4> g;
    @DexIgnore
    public wc4<qa4> h;
    @DexIgnore
    public /* final */ BluetoothCommandId i;
    @DexIgnore
    public /* final */ Peripheral.c j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BluetoothCommand e;

        @DexIgnore
        public b(BluetoothCommand bluetoothCommand) {
            this.e = bluetoothCommand;
        }

        @DexIgnore
        public final void run() {
            this.e.a();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public BluetoothCommand(BluetoothCommandId bluetoothCommandId, Peripheral.c cVar) {
        kd4.b(bluetoothCommandId, "id");
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.i = bluetoothCommandId;
        this.j = cVar;
    }

    @DexIgnore
    public abstract void a(Peripheral peripheral);

    @DexIgnore
    public final Peripheral.c b() {
        return this.j;
    }

    @DexIgnore
    public abstract boolean b(GattOperationResult gattOperationResult);

    @DexIgnore
    public final BluetoothCommandId c() {
        return this.i;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
    }

    @DexIgnore
    public final Result e() {
        return this.e;
    }

    @DexIgnore
    public abstract ra0<GattOperationResult> f();

    @DexIgnore
    public final boolean g() {
        return this.d;
    }

    @DexIgnore
    public boolean h() {
        return false;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result extends JSONAbleObject {
        @DexIgnore
        public static /* final */ a Companion; // = new a((fd4) null);
        @DexIgnore
        public /* final */ BluetoothCommandId commandId;
        @DexIgnore
        public /* final */ GattOperationResult.GattResult gattResult;
        @DexIgnore
        public /* final */ ResultCode resultCode;

        @DexIgnore
        public enum ResultCode {
            SUCCESS(0),
            NOT_START(1),
            WRONG_STATE(6),
            GATT_ERROR(7),
            UNEXPECTED_RESULT(8),
            INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT(9),
            BLUETOOTH_OFF(10),
            UNSUPPORTED(11),
            HID_PROXY_NOT_CONNECTED(256),
            HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
            HID_INPUT_DEVICE_DISABLED(258),
            HID_UNKNOWN_ERROR(511),
            INTERRUPTED(254),
            CONNECTION_DROPPED(255);
            
            @DexIgnore
            public static /* final */ a Companion; // = null;
            @DexIgnore
            public /* final */ int id;
            @DexIgnore
            public /* final */ String logName;

            @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a {
                @DexIgnore
                public a() {
                }

                @DexIgnore
                public final ResultCode a(GattOperationResult.GattResult gattResult) {
                    throw null;
                    // kd4.b(gattResult, "gattResult");
                    // switch (b10.a[gattResult.getResultCode().ordinal()]) {
                    //     case 1:
                    //         return ResultCode.SUCCESS;
                    //     case 2:
                    //         return ResultCode.HID_PROXY_NOT_CONNECTED;
                    //     case 3:
                    //         return ResultCode.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
                    //     case 4:
                    //         return ResultCode.HID_INPUT_DEVICE_DISABLED;
                    //     case 5:
                    //         return ResultCode.HID_UNKNOWN_ERROR;
                    //     case 6:
                    //         return ResultCode.BLUETOOTH_OFF;
                    //     default:
                    //         return ResultCode.GATT_ERROR;
                    // }
                }

                @DexIgnore
                public /* synthetic */ a(fd4 fd4) {
                    this();
                }
            }

            /*
            static {
                Companion = new a((fd4) null);
            }
            */

            @DexIgnore
            ResultCode(int i) {
                this.id = i;
                String name = name();
                Locale locale = Locale.US;
                kd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    this.logName = lowerCase;
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            @DexIgnore
            public final int getId() {
                return this.id;
            }

            @DexIgnore
            public final String getLogName$blesdk_productionRelease() {
                return this.logName;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final Result a(GattOperationResult.GattResult gattResult) {
                kd4.b(gattResult, "gattResult");
                return new Result((BluetoothCommandId) null, ResultCode.Companion.a(gattResult), gattResult, 1, (fd4) null);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(BluetoothCommandId bluetoothCommandId, ResultCode resultCode2, GattOperationResult.GattResult gattResult2, int i, fd4 fd4) {
            this((i & 1) != 0 ? BluetoothCommandId.UNKNOWN : bluetoothCommandId, resultCode2, (i & 4) != 0 ? new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null) : gattResult2);
        }

        @DexIgnore
        public static /* synthetic */ Result copy$default(Result result, BluetoothCommandId bluetoothCommandId, ResultCode resultCode2, GattOperationResult.GattResult gattResult2, int i, Object obj) {
            if ((i & 1) != 0) {
                bluetoothCommandId = result.commandId;
            }
            if ((i & 2) != 0) {
                resultCode2 = result.resultCode;
            }
            if ((i & 4) != 0) {
                gattResult2 = result.gattResult;
            }
            return result.copy(bluetoothCommandId, resultCode2, gattResult2);
        }

        @DexIgnore
        public final BluetoothCommandId component1() {
            return this.commandId;
        }

        @DexIgnore
        public final ResultCode component2() {
            return this.resultCode;
        }

        @DexIgnore
        public final GattOperationResult.GattResult component3() {
            return this.gattResult;
        }

        @DexIgnore
        public final Result copy(BluetoothCommandId bluetoothCommandId, ResultCode resultCode2, GattOperationResult.GattResult gattResult2) {
            kd4.b(bluetoothCommandId, "commandId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(gattResult2, "gattResult");
            return new Result(bluetoothCommandId, resultCode2, gattResult2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return kd4.a((Object) this.commandId, (Object) result.commandId) && kd4.a((Object) this.resultCode, (Object) result.resultCode) && kd4.a((Object) this.gattResult, (Object) result.gattResult);
        }

        @DexIgnore
        public final BluetoothCommandId getCommandId() {
            return this.commandId;
        }

        @DexIgnore
        public final GattOperationResult.GattResult getGattResult() {
            return this.gattResult;
        }

        @DexIgnore
        public final ResultCode getResultCode() {
            return this.resultCode;
        }

        @DexIgnore
        public int hashCode() {
            BluetoothCommandId bluetoothCommandId = this.commandId;
            int i = 0;
            int hashCode = (bluetoothCommandId != null ? bluetoothCommandId.hashCode() : 0) * 31;
            ResultCode resultCode2 = this.resultCode;
            int hashCode2 = (hashCode + (resultCode2 != null ? resultCode2.hashCode() : 0)) * 31;
            GattOperationResult.GattResult gattResult2 = this.gattResult;
            if (gattResult2 != null) {
                i = gattResult2.hashCode();
            }
            return hashCode2 + i;
        }

        @DexIgnore
        public JSONObject toJSONObject() throws JSONException {
            throw null;
            // JSONObject jSONObject = new JSONObject();
            // try {
            //     wa0.a(wa0.a(jSONObject, JSONKey.COMMAND_ID, this.commandId.getLogName$blesdk_productionRelease()), JSONKey.RESULT_CODE, this.resultCode.getLogName$blesdk_productionRelease());
            //     if (this.gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            //         wa0.a(jSONObject, JSONKey.GATT_RESULT, this.gattResult.toJSONObject());
            //     }
            // } catch (JSONException e) {
            //     da0.l.a(e);
            // }
            // return jSONObject;
        }

        @DexIgnore
        public String toString() {
            return "Result(commandId=" + this.commandId + ", resultCode=" + this.resultCode + ", gattResult=" + this.gattResult + ")";
        }

        @DexIgnore
        public Result(BluetoothCommandId bluetoothCommandId, ResultCode resultCode2, GattOperationResult.GattResult gattResult2) {
            kd4.b(bluetoothCommandId, "commandId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(gattResult2, "gattResult");
            this.commandId = bluetoothCommandId;
            this.resultCode = resultCode2;
            this.gattResult = gattResult2;
        }
    }

    @DexIgnore
    public final void a(Result result) {
        kd4.b(result, "<set-?>");
        this.e = result;
    }

    @DexIgnore
    public final BluetoothCommand b(Peripheral peripheral) {
        throw null;
        // kd4.b(peripheral, "peripheral");
        // if (!this.d) {
        //     Peripheral.a(peripheral, LogLevel.DEBUG, this.a, "Command started.", false, 8, (Object) null);
        //     ra0<GattOperationResult> f2 = f();
        //     if (f2 != null) {
        //         f2.a((sa0<GattOperationResult>) this);
        //     }
        //     a(peripheral);
        // }
        // return this;
    }

    @DexIgnore
    /* renamed from: c */
    public void a(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "value");
        e(gattOperationResult);
    }

    @DexIgnore
    public final void e(GattOperationResult gattOperationResult) {
        if (b(gattOperationResult)) {
            if (this.e.getResultCode() == Result.ResultCode.INTERRUPTED || this.e.getResultCode() == Result.ResultCode.INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT) {
                this.e = Result.copy$default(this.e, (BluetoothCommandId) null, (Result.ResultCode) null, gattOperationResult.a(), 3, (Object) null);
            } else {
                a(gattOperationResult);
            }
            a();
        }
    }

    @DexIgnore
    public final BluetoothCommand a(xc4<? super BluetoothCommand, qa4> xc4) {
        kd4.b(xc4, "actionOnCommandError");
        this.g = xc4;
        return this;
    }

    @DexIgnore
    public final BluetoothCommand a(wc4<qa4> wc4) {
        kd4.b(wc4, "actionOnFinal");
        this.h = wc4;
        return this;
    }

    @DexIgnore
    public final void a(Result.ResultCode resultCode) {
        throw null;
        // kd4.b(resultCode, "resultCode");
        // if (!this.d) {
        //     t90 t90 = t90.c;
        //     String str = this.a;
        //     t90.a(str, "command stop: " + resultCode);
        //     this.e = Result.copy$default(this.e, (BluetoothCommandId) null, resultCode, (GattOperationResult.GattResult) null, 5, (Object) null);
        //     if (h()) {
        //         this.b.postDelayed(new b(this), 5000);
        //     } else {
        //         a();
        //     }
        // }
    }

    @DexIgnore
    public final BluetoothCommand b(xc4<? super BluetoothCommand, qa4> xc4) {
        kd4.b(xc4, "actionOnCommandSuccess");
        this.f = xc4;
        return this;
    }

    // @DexIgnore
    // public void a(GattOperationResult gattOperationResult) {
    //     kd4.b(gattOperationResult, "gattOperationResult");
    //     d(gattOperationResult);
    //     Result a2 = Result.Companion.a(gattOperationResult.a());
    //     this.e = Result.copy$default(this.e, (BluetoothCommandId) null, a2.getResultCode(), a2.getGattResult(), 1, (Object) null);
    // }

    @DexIgnore
    public final void a() {
        throw null;
        // if (!this.d) {
        //     this.d = true;
        //     ra0<GattOperationResult> f2 = f();
        //     if (f2 != null) {
        //         f2.b(this);
        //     }
        //     if (Result.ResultCode.SUCCESS == this.e.getResultCode()) {
        //         t90 t90 = t90.c;
        //         String str = this.a;
        //         t90.a(str, "Command success: " + JSONAbleObject.toJSONString$default(this.e, 0, 1, (Object) null));
        //         xc4<? super BluetoothCommand, qa4> xc4 = this.f;
        //         if (xc4 != null) {
        //             qa4 invoke = xc4.invoke(this);
        //         }
        //     } else {
        //         t90 t902 = t90.c;
        //         String str2 = this.a;
        //         t902.b(str2, "Command error: " + JSONAbleObject.toJSONString$default(this.e, 0, 1, (Object) null));
        //         xc4<? super BluetoothCommand, qa4> xc42 = this.g;
        //         if (xc42 != null) {
        //             qa4 invoke2 = xc42.invoke(this);
        //         }
        //     }
        //     wc4<qa4> wc4 = this.h;
        //     if (wc4 != null) {
        //         qa4 invoke3 = wc4.invoke();
        //     }
        // }
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return new JSONObject();
    }
}
