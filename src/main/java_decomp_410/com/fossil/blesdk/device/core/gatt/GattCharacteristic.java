package com.fossil.blesdk.device.core.gatt;

import android.bluetooth.BluetoothGattCharacteristic;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.p10;
import com.fossil.blesdk.obfuscated.ua0;
import com.misfit.frameworks.common.constants.Constants;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattCharacteristic {
    @DexIgnore
    public static /* final */ a b; // = new a((fd4) null);
    @DexIgnore
    public /* final */ BluetoothGattCharacteristic a;

    @DexIgnore
    final static byte r2 = -1;  // added, probably not right

    @DexIgnore
    public enum CharacteristicId {
        UNKNOWN("unknown", r2),
        MODEL_NUMBER("model_number", r2),
        SERIAL_NUMBER(Constants.SERIAL_NUMBER, r2),
        FIRMWARE_VERSION(Constants.FIRMWARE_VERSION, r2),
        SOFTWARE_REVISION("software_revision", r2),
        DC("device_config", r2),
        FTC("file_transfer_control", r2),
        FTD("file_transfer_data", (byte) 0),
        ASYNC("async", r2),
        FTD_1("file_transfer_data_1", (byte) 1),
        AUTHENTICATION("authentication", r2);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ byte socketId;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final CharacteristicId a(byte b) {
                CharacteristicId characteristicId;
                CharacteristicId[] values = CharacteristicId.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        characteristicId = null;
                        break;
                    }
                    characteristicId = values[i];
                    if (characteristicId.getSocketId$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                return characteristicId != null ? characteristicId : CharacteristicId.UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        CharacteristicId(String str, byte b) {
            this.logName = str;
            this.socketId = b;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final ResourceType getResourceType$blesdk_productionRelease() {
            throw null;
            // switch (p10.a[ordinal()]) {
            //     case 1:
            //     case 2:
            //     case 3:
            //     case 4:
            //         return ResourceType.DEVICE_INFORMATION;
            //     case 5:
            //         return ResourceType.DEVICE_CONFIG;
            //     case 6:
            //         return ResourceType.FILE_CONFIG;
            //     case 7:
            //     case 8:
            //         return ResourceType.TRANSFER_DATA;
            //     case 9:
            //         return ResourceType.AUTHENTICATION;
            //     case 10:
            //         return ResourceType.ASYNC;
            //     default:
            //         return ResourceType.UNKNOWN;
            // }
        }

        @DexIgnore
        public final int getResourceTypeQuotaWeight$blesdk_productionRelease() {
            throw null;
            // switch (p10.b[ordinal()]) {
            //     case 1:
            //     case 2:
            //     case 3:
            //     case 4:
            //     case 5:
            //     case 6:
            //     case 7:
            //         return Integer.MAX_VALUE;
            //     case 8:
            //     case 9:
            //     case 10:
            //         return 1;
            //     default:
            //         return 0;
            // }
        }

        @DexIgnore
        public final byte getSocketId$blesdk_productionRelease() {
            return this.socketId;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CharacteristicId a(UUID uuid) {
            kd4.b(uuid, "uuid");
            if (kd4.a((Object) uuid, (Object) ua0.y.j())) {
                return CharacteristicId.MODEL_NUMBER;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.k())) {
                return CharacteristicId.SERIAL_NUMBER;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.i())) {
                return CharacteristicId.FIRMWARE_VERSION;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.l())) {
                return CharacteristicId.SOFTWARE_REVISION;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.q())) {
                return CharacteristicId.DC;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.r())) {
                return CharacteristicId.FTC;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.s())) {
                return CharacteristicId.FTD;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.o())) {
                return CharacteristicId.ASYNC;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.t())) {
                return CharacteristicId.FTD_1;
            }
            if (kd4.a((Object) uuid, (Object) ua0.y.p())) {
                return CharacteristicId.AUTHENTICATION;
            }
            return CharacteristicId.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public GattCharacteristic(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        kd4.b(bluetoothGattCharacteristic, "bluetoothGattCharacteristic");
        this.a = bluetoothGattCharacteristic;
        a aVar = b;
        UUID uuid = this.a.getUuid();
        kd4.a((Object) uuid, "this.bluetoothGattCharacteristic.uuid");
        aVar.a(uuid);
    }

    @DexIgnore
    public final BluetoothGattCharacteristic a() {
        return this.a;
    }
}
