package com.fossil.blesdk.device.core;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.bluetooth.BluetoothGattService, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1 INSTANCE; // = new com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1$1")
    /* renamed from: com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1$1 */
    public static final class C11001 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<android.bluetooth.BluetoothGattCharacteristic, java.lang.String> {
        @DexIgnore
        public static /* final */ com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1.C11001 INSTANCE; // = new com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1.C11001();

        @DexIgnore
        public C11001() {
            super(1);
        }

        @DexIgnore
        public final java.lang.String invoke(android.bluetooth.BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("    ");
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bluetoothGattCharacteristic, "characteristic");
            sb.append(bluetoothGattCharacteristic.getUuid().toString());
            return sb.toString();
        }
    }

    @DexIgnore
    public Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(android.bluetooth.BluetoothGattService bluetoothGattService) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bluetoothGattService, com.misfit.frameworks.common.constants.Constants.SERVICE);
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        java.util.List<android.bluetooth.BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) characteristics, "service.characteristics");
        sb.append(com.fossil.blesdk.obfuscated.kb4.m24365a(characteristics, "\n", (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1.C11001.INSTANCE, 30, (java.lang.Object) null));
        return sb.toString();
    }
}
