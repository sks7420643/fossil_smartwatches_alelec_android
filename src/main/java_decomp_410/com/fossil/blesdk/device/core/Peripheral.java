package com.fossil.blesdk.device.core;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.GattCharacteristicProperty;
import com.fossil.blesdk.device.core.gatt.GattDescriptor;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.hid.HIDProfile;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.a20;
import com.fossil.blesdk.obfuscated.b20;
import com.fossil.blesdk.obfuscated.c20;
import com.fossil.blesdk.obfuscated.d20;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.mb0;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.ra0;
import com.fossil.blesdk.obfuscated.s10;
import com.fossil.blesdk.obfuscated.t10;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.u10;
import com.fossil.blesdk.obfuscated.v10;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.w10;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.x10;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.y10;
import com.fossil.blesdk.obfuscated.z00;
import com.fossil.blesdk.obfuscated.z10;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral implements Parcelable {
    @DexIgnore
    public static /* final */ d CREATOR; // = new d((fd4) null);
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public BluetoothGatt f;
    @DexIgnore
    public /* final */ BluetoothGattCallback g;
    @DexIgnore
    public BluetoothCommandQueue h;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<a> i;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<b> j;
    @DexIgnore
    public /* final */ CopyOnWriteArraySet<f> k;
    @DexIgnore
    public /* final */ HashMap<GattCharacteristic.CharacteristicId, GattCharacteristic> l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public /* final */ BroadcastReceiver o;
    @DexIgnore
    public /* final */ BroadcastReceiver p;
    @DexIgnore
    public /* final */ BroadcastReceiver q;
    @DexIgnore
    public State r;
    @DexIgnore
    public /* final */ String s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ c u;
    @DexIgnore
    public /* final */ BluetoothDevice v;

    @DexIgnore
    public enum BondState {
        BOND_NONE(10),
        BONDING(11),
        BONDED(12);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ int value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final BondState a(int i) {
                switch (i) {
                    case 10:
                        return BondState.BOND_NONE;
                    case 11:
                        return BondState.BONDING;
                    case 12:
                        return BondState.BONDED;
                    default:
                        return BondState.BOND_NONE;
                }
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        BondState(int i) {
            this.value = i;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum HIDState {
        DISCONNECTED(0),
        CONNECTING(1),
        CONNECTED(2),
        DISCONNECTING(3);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ int value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final HIDState a(int i) {
                if (i == 0) {
                    return HIDState.DISCONNECTED;
                }
                if (i == 1) {
                    return HIDState.CONNECTING;
                }
                if (i == 2) {
                    return HIDState.CONNECTED;
                }
                if (i != 3) {
                    return HIDState.DISCONNECTED;
                }
                return HIDState.DISCONNECTING;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        HIDState(int i) {
            this.value = i;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum State {
        DISCONNECTED(0),
        CONNECTING(1),
        CONNECTED(2),
        DISCONNECTING(3);
        
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        State(int i) {
            this.value = i;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(BluetoothCommand bluetoothCommand);

        @DexIgnore
        void b(BluetoothCommand bluetoothCommand);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ GattCharacteristic.CharacteristicId g;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] h;

        @DexIgnore
        public a0(Peripheral peripheral, GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = characteristicId;
            this.h = bArr;
        }

        @DexIgnore
        public final void run() {
            this.e.h().k().c();
            this.e.h().k().a(new a20(this.f, this.g, this.h));
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(b20 b20);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ GattCharacteristic.CharacteristicId g;
        @DexIgnore
        public /* final */ /* synthetic */ GattDescriptor.DescriptorId h;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] i;

        @DexIgnore
        public b0(Peripheral peripheral, GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, GattDescriptor.DescriptorId descriptorId, byte[] bArr) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = characteristicId;
            this.h = descriptorId;
            this.i = bArr;
        }

        @DexIgnore
        public final void run() {
            this.e.h().l().c();
            this.e.h().l().a(new v10(this.f, this.g, this.h, this.i));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ ra0<GattOperationResult> a; // = new ra0<>();
        @DexIgnore
        public /* final */ ra0<GattOperationResult> b; // = new ra0<>();
        @DexIgnore
        public /* final */ ra0<GattOperationResult> c; // = new ra0<>();
        @DexIgnore
        public /* final */ ra0<GattOperationResult> d;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> e;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> f;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> g;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> h;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> i;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> j;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> k;
        @DexIgnore
        public /* final */ ra0<GattOperationResult> l;

        @DexIgnore
        public c() {
            new ra0();
            this.d = new ra0<>();
            this.e = new ra0<>();
            this.f = new ra0<>();
            this.g = new ra0<>();
            this.h = new ra0<>();
            this.i = new ra0<>();
            this.j = new ra0<>();
            this.k = new ra0<>();
            this.l = new ra0<>();
        }

        @DexIgnore
        public final ra0<GattOperationResult> a() {
            return this.k;
        }

        @DexIgnore
        public final ra0<GattOperationResult> b() {
            return this.e;
        }

        @DexIgnore
        public final ra0<GattOperationResult> c() {
            return this.i;
        }

        @DexIgnore
        public final ra0<GattOperationResult> d() {
            return this.f;
        }

        @DexIgnore
        public final ra0<GattOperationResult> e() {
            return this.l;
        }

        @DexIgnore
        public final ra0<GattOperationResult> f() {
            return this.g;
        }

        @DexIgnore
        public final ra0<GattOperationResult> g() {
            return this.d;
        }

        @DexIgnore
        public final ra0<GattOperationResult> h() {
            return this.h;
        }

        @DexIgnore
        public final ra0<GattOperationResult> i() {
            return this.j;
        }

        @DexIgnore
        public final ra0<GattOperationResult> j() {
            return this.c;
        }

        @DexIgnore
        public final ra0<GattOperationResult> k() {
            return this.a;
        }

        @DexIgnore
        public final ra0<GattOperationResult> l() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Parcelable.Creator<Peripheral> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public /* synthetic */ d(fd4 fd4) {
            this();
        }

        @DexIgnore
        public Peripheral createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            e eVar = e.b;
            Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
            if (readParcelable != null) {
                return eVar.a((BluetoothDevice) readParcelable);
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public Peripheral[] newArray(int i) {
            return new Peripheral[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public static /* final */ Hashtable<String, Peripheral> a; // = new Hashtable<>();
        @DexIgnore
        public static /* final */ e b; // = new e();

        @DexIgnore
        public final Peripheral a(BluetoothDevice bluetoothDevice) {
            Peripheral peripheral;
            kd4.b(bluetoothDevice, "bluetoothDevice");
            t90 t90 = t90.c;
            t90.a("Peripheral.Factory", "getPeripheral: MAC: " + bluetoothDevice.getAddress());
            synchronized (a) {
                peripheral = a.get(bluetoothDevice.getAddress());
                if (peripheral == null) {
                    peripheral = new Peripheral(bluetoothDevice, (fd4) null);
                    a.put(peripheral.g().getAddress(), peripheral);
                }
            }
            return peripheral;
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        void a(Peripheral peripheral, HIDState hIDState, HIDState hIDState2);

        @DexIgnore
        void a(Peripheral peripheral, State state);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral a;

        @DexIgnore
        public g(Peripheral peripheral) {
            this.a = peripheral;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action != null && action.hashCode() == -1323520325 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED")) {
                    Serializable serializableExtra = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE");
                    if (serializableExtra != null) {
                        BluetoothLeAdapter.State state = (BluetoothLeAdapter.State) serializableExtra;
                        Serializable serializableExtra2 = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE");
                        if (serializableExtra2 != null) {
                            this.a.a(state, (BluetoothLeAdapter.State) serializableExtra2);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral a;

        @DexIgnore
        public h(Peripheral peripheral) {
            this.a = peripheral;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (action != null && action.hashCode() == 2084501365 && action.equals("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED")) {
                    BondState a2 = BondState.Companion.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", 10));
                    BondState a3 = BondState.Companion.a(intent.getIntExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", 10));
                    if (kd4.a((Object) (BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE"), (Object) this.a.g())) {
                        this.a.a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), a2, a3);
                        this.a.b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), a2, a3);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ BluetoothCommandQueue e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public i(BluetoothCommandQueue bluetoothCommandQueue, Peripheral peripheral, int i, int i2) {
            this.e = bluetoothCommandQueue;
            this.f = i2;
        }

        @DexIgnore
        public final void run() {
            BluetoothCommand.Result.ResultCode resultCode;
            BluetoothCommandQueue bluetoothCommandQueue = this.e;
            if (this.f == -1) {
                resultCode = BluetoothCommand.Result.ResultCode.BLUETOOTH_OFF;
            } else {
                resultCode = BluetoothCommand.Result.ResultCode.CONNECTION_DROPPED;
            }
            bluetoothCommandQueue.a(resultCode);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral a;

        @DexIgnore
        public j(Peripheral peripheral) {
            this.a = peripheral;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Intent intent2 = intent;
            if (intent2 != null) {
                String action = intent.getAction();
                if (action != null && action.hashCode() == -1099894406 && action.equals("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED")) {
                    HIDState a2 = HIDState.Companion.a(intent2.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", 0));
                    HIDState a3 = HIDState.Companion.a(intent2.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", 0));
                    if (kd4.a((Object) (BluetoothDevice) intent2.getParcelableExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE"), (Object) this.a.g())) {
                        da0 da0 = da0.l;
                        SdkLogEntry sdkLogEntry = r4;
                        SdkLogEntry sdkLogEntry2 = new SdkLogEntry("hid_connection_state_changed", EventType.CENTRAL_EVENT, this.a.k(), "", "", true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(wa0.a(new JSONObject(), JSONKey.PREV_STATE, a2.getLogName$blesdk_productionRelease()), JSONKey.NEW_STATE, a3.getLogName$blesdk_productionRelease()), 448, (fd4) null);
                        da0.b(sdkLogEntry);
                        this.a.b(a2, a3);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ b e;
        @DexIgnore
        public /* final */ /* synthetic */ GattCharacteristic.CharacteristicId f;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] g;

        @DexIgnore
        public k(b bVar, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
            this.e = bVar;
            this.f = characteristicId;
            this.g = bArr;
        }

        @DexIgnore
        public final void run() {
            this.e.a(new c20(this.f, this.g));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ a e;
        @DexIgnore
        public /* final */ /* synthetic */ BluetoothCommand f;

        @DexIgnore
        public l(a aVar, BluetoothCommand bluetoothCommand) {
            this.e = aVar;
            this.f = bluetoothCommand;
        }

        @DexIgnore
        public final void run() {
            this.e.a(this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ a e;
        @DexIgnore
        public /* final */ /* synthetic */ BluetoothCommand f;

        @DexIgnore
        public m(a aVar, BluetoothCommand bluetoothCommand) {
            this.e = aVar;
            this.f = bluetoothCommand;
        }

        @DexIgnore
        public final void run() {
            this.e.b(this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ State g;

        @DexIgnore
        public n(Peripheral peripheral, GattOperationResult.GattResult gattResult, State state) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = state;
        }

        @DexIgnore
        public final void run() {
            this.e.h().b().c();
            this.e.h().b().a(new t10(this.f, this.g));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState g;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState h;

        @DexIgnore
        public o(Peripheral peripheral, GattOperationResult.GattResult gattResult, HIDState hIDState, HIDState hIDState2) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = hIDState;
            this.h = hIDState2;
        }

        @DexIgnore
        public final void run() {
            this.e.h().a().c();
            this.e.h().a().a(new s10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ b e;
        @DexIgnore
        public /* final */ /* synthetic */ State f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore
        public p(b bVar, State state, int i) {
            this.e = bVar;
            this.f = state;
            this.g = i;
        }

        @DexIgnore
        public final void run() {
            this.e.a(new d20(this.f, this.g));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ BondState g;
        @DexIgnore
        public /* final */ /* synthetic */ BondState h;

        @DexIgnore
        public q(Peripheral peripheral, GattOperationResult.GattResult gattResult, BondState bondState, BondState bondState2) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = bondState;
            this.h = bondState2;
        }

        @DexIgnore
        public final void run() {
            this.e.h().c().c();
            this.e.h().c().a(new u10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ State g;

        @DexIgnore
        public r(Peripheral peripheral, GattOperationResult.GattResult gattResult, State state) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = state;
        }

        @DexIgnore
        public final void run() {
            this.e.h().d().c();
            this.e.h().d().a(new t10(this.f, this.g));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState g;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState h;

        @DexIgnore
        public s(Peripheral peripheral, GattOperationResult.GattResult gattResult, HIDState hIDState, HIDState hIDState2) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = hIDState;
            this.h = hIDState2;
        }

        @DexIgnore
        public final void run() {
            this.e.h().e().c();
            this.e.h().e().a(new s10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ UUID[] g;
        @DexIgnore
        public /* final */ /* synthetic */ GattCharacteristic.CharacteristicId[] h;

        @DexIgnore
        public t(Peripheral peripheral, GattOperationResult.GattResult gattResult, UUID[] uuidArr, GattCharacteristic.CharacteristicId[] characteristicIdArr) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = uuidArr;
            this.h = characteristicIdArr;
        }

        @DexIgnore
        public final void run() {
            this.e.h().f().c();
            this.e.h().f().a(new w10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ f e;
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral f;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState g;
        @DexIgnore
        public /* final */ /* synthetic */ HIDState h;

        @DexIgnore
        public u(f fVar, Peripheral peripheral, HIDState hIDState, HIDState hIDState2) {
            this.e = fVar;
            this.f = peripheral;
            this.g = hIDState;
            this.h = hIDState2;
        }

        @DexIgnore
        public final void run() {
            this.e.a(this.f, this.g, this.h);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ f e;
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral f;
        @DexIgnore
        public /* final */ /* synthetic */ State g;

        @DexIgnore
        public v(f fVar, Peripheral peripheral, State state) {
            this.e = fVar;
            this.f = peripheral;
            this.g = state;
        }

        @DexIgnore
        public final void run() {
            this.e.a(this.f, this.g);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ GattCharacteristic.CharacteristicId g;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] h;

        @DexIgnore
        public w(Peripheral peripheral, GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = characteristicId;
            this.h = bArr;
        }

        @DexIgnore
        public final void run() {
            this.e.h().g().c();
            this.e.h().g().a(new x10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore
        public x(Peripheral peripheral, GattOperationResult.GattResult gattResult, int i) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = i;
        }

        @DexIgnore
        public final void run() {
            this.e.h().h().c();
            this.e.h().h().a(new y10(this.f, this.g));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class y implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ BondState g;
        @DexIgnore
        public /* final */ /* synthetic */ BondState h;

        @DexIgnore
        public y(Peripheral peripheral, GattOperationResult.GattResult gattResult, BondState bondState, BondState bondState2) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = bondState;
            this.h = bondState2;
        }

        @DexIgnore
        public final void run() {
            this.e.h().i().c();
            this.e.h().i().a(new u10(this.f, this.g, this.h));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class z implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Peripheral e;
        @DexIgnore
        public /* final */ /* synthetic */ GattOperationResult.GattResult f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore
        public z(Peripheral peripheral, GattOperationResult.GattResult gattResult, int i) {
            this.e = peripheral;
            this.f = gattResult;
            this.g = i;
        }

        @DexIgnore
        public final void run() {
            this.e.h().j().c();
            this.e.h().j().a(new z10(this.f, this.g));
        }
    }

    @DexIgnore
    public /* synthetic */ Peripheral(BluetoothDevice bluetoothDevice, fd4 fd4) {
        this(bluetoothDevice);
    }

    @DexIgnore
    public final void c(BluetoothCommand bluetoothCommand) {
        Iterator<a> it = this.i.iterator();
        while (it.hasNext()) {
            try {
                this.e.post(new m(it.next(), bluetoothCommand));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void d() {
        int i2 = z00.b[this.r.ordinal()];
        if (i2 == 1) {
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), State.DISCONNECTED);
        } else if (i2 == 2) {
            BluetoothGatt bluetoothGatt = this.f;
            if (bluetoothGatt == null) {
                a(0, 0);
                return;
            }
            if (bluetoothGatt != null) {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt.disconnect.", false, 8, (Object) null);
                BluetoothGatt bluetoothGatt2 = this.f;
                if (bluetoothGatt2 != null) {
                    bluetoothGatt2.disconnect();
                }
            } else {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt is null, not call BluetoothGatt.disconnect.", false, 8, (Object) null);
            }
            a(0, 0);
        } else if (i2 == 3 || i2 == 4) {
            if (this.f != null) {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt.disconnect.", false, 8, (Object) null);
                BluetoothGatt bluetoothGatt3 = this.f;
                if (bluetoothGatt3 != null) {
                    bluetoothGatt3.disconnect();
                }
            } else {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt is null, not call BluetoothGatt.disconnect.", false, 8, (Object) null);
            }
            a(0, 0);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final void e() {
        int i2 = z00.d[j().ordinal()];
        if (i2 == 1) {
            GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
            HIDState hIDState = HIDState.DISCONNECTED;
            b(gattResult, hIDState, hIDState);
        } else if (i2 == 2 || i2 == 3) {
            GattOperationResult.GattResult a2 = GattOperationResult.GattResult.Companion.a(HIDProfile.e.b(this.v));
            if (a2.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
                b(a2, HIDState.CONNECTED, HIDState.DISCONNECTED);
            }
        }
    }

    @DexIgnore
    public final void f() {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.discoverServices: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), new UUID[0], new GattCharacteristic.CharacteristicId[0]);
            return;
        }
        BluetoothGatt bluetoothGatt = this.f;
        if (bluetoothGatt == null) {
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null), new UUID[0], new GattCharacteristic.CharacteristicId[0]);
        } else if (bluetoothGatt == null || true != bluetoothGatt.discoverServices()) {
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), new UUID[0], new GattCharacteristic.CharacteristicId[0]);
        }
    }

    @DexIgnore
    public final BluetoothDevice g() {
        return this.v;
    }

    @DexIgnore
    public final BondState getBondState() {
        return BondState.Companion.a(this.v.getBondState());
    }

    @DexIgnore
    public final State getState() {
        return this.r;
    }

    @DexIgnore
    public final c h() {
        return this.u;
    }

    @DexIgnore
    public final String i() {
        String name = this.v.getName();
        return name != null ? name : "";
    }

    @DexIgnore
    public final HIDState j() {
        return HIDState.Companion.a(HIDProfile.e.c(this.v));
    }

    @DexIgnore
    public final String k() {
        return this.s;
    }

    @DexIgnore
    public final int l() {
        return Math.min(this.m, this.n);
    }

    @DexIgnore
    public final boolean m() {
        if (!y00.f.a()) {
            return false;
        }
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.internalRemoveBond: Bluetooth Off.", false, 8, (Object) null);
            return false;
        }
        try {
            a(this, LogLevel.DEBUG, "Peripheral", "Calling private method BluetoothDevice.removeBond()", false, 8, (Object) null);
            Object invoke = this.v.getClass().getMethod("removeBond", new Class[0]).invoke(this.v, new Object[0]);
            if (invoke != null) {
                return ((Boolean) invoke).booleanValue();
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
        } catch (Exception e2) {
            da0.l.a(e2);
            return false;
        }
    }

    @DexIgnore
    public final void n() {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.readRssi: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), 0);
            return;
        }
        GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
        BluetoothGatt bluetoothGatt = this.f;
        if (bluetoothGatt == null) {
            gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null);
        } else if (bluetoothGatt == null || true != bluetoothGatt.readRemoteRssi()) {
            gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null);
        }
        if (gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            a(gattResult, 0);
        }
    }

    @DexIgnore
    public final void o() {
        mb0.a.a(this.o, "com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
    }

    @DexIgnore
    public final void p() {
        mb0.a.a(this.p, "com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
    }

    @DexIgnore
    public final void q() {
        mb0.a.a(this.q, "com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED");
    }

    @DexIgnore
    public final void r() {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.removeBond: Bluetooth Off.", false, 8, (Object) null);
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), getBondState(), getBondState());
        } else if (BondState.BOND_NONE == getBondState()) {
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), getBondState(), getBondState());
        } else if (!m()) {
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), getBondState(), getBondState());
        }
    }

    @DexIgnore
    public final void s() {
        this.l.clear();
        this.m = 20;
        this.n = this.m;
        this.h = new BluetoothCommandQueue(this);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeParcelable(this.v, i2);
        }
    }

    @DexIgnore
    public Peripheral(BluetoothDevice bluetoothDevice) {
        this.v = bluetoothDevice;
        this.e = hb0.a.a();
        this.g = new Peripheral$bluetoothGattCallback$Anon1(this);
        this.h = new BluetoothCommandQueue(this);
        this.i = new CopyOnWriteArraySet<>();
        this.j = new CopyOnWriteArraySet<>();
        this.k = new CopyOnWriteArraySet<>();
        this.l = new HashMap<>();
        this.m = 20;
        this.n = this.m;
        this.o = new g(this);
        this.p = new h(this);
        this.q = new j(this);
        this.r = State.DISCONNECTED;
        String address = this.v.getAddress();
        kd4.a((Object) address, "bluetoothDevice.address");
        this.s = address;
        this.u = new c();
        o();
        p();
        q();
    }

    @DexIgnore
    public final State c(int i2) {
        if (i2 == 0) {
            return State.DISCONNECTED;
        }
        if (i2 == 1) {
            return State.CONNECTING;
        }
        if (i2 == 2) {
            return State.CONNECTED;
        }
        if (i2 != 3) {
            return State.DISCONNECTED;
        }
        return State.DISCONNECTING;
    }

    @DexIgnore
    public final void b(int i2) {
        this.n = i2;
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.t = z2;
    }

    @DexIgnore
    public final void b(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        this.e.post(new a0(this, gattResult, characteristicId, bArr));
    }

    @DexIgnore
    public final void b(GattOperationResult.GattResult gattResult, State state) {
        this.e.post(new r(this, gattResult, state));
    }

    @DexIgnore
    public final void c() {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.createBond: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), getBondState(), getBondState());
        } else if (BondState.BONDED == getBondState()) {
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), getBondState(), getBondState());
        } else if (!this.v.createBond()) {
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), getBondState(), getBondState());
        }
    }

    @DexIgnore
    public final void b(GattOperationResult.GattResult gattResult, HIDState hIDState, HIDState hIDState2) {
        this.e.post(new s(this, gattResult, hIDState, hIDState2));
    }

    @DexIgnore
    public final void b(GattOperationResult.GattResult gattResult, int i2) {
        this.e.post(new z(this, gattResult, i2));
    }

    @DexIgnore
    public final void b(GattOperationResult.GattResult gattResult, BondState bondState, BondState bondState2) {
        this.e.post(new y(this, gattResult, bondState, bondState2));
    }

    @DexIgnore
    public final void b(BluetoothCommand bluetoothCommand) {
        Iterator<a> it = this.i.iterator();
        while (it.hasNext()) {
            try {
                this.e.post(new l(it.next(), bluetoothCommand));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, int i2) {
        this.e.post(new x(this, gattResult, i2));
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        this.e.post(new w(this, gattResult, characteristicId, bArr));
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, GattDescriptor.DescriptorId descriptorId, byte[] bArr) {
        this.e.post(new b0(this, gattResult, characteristicId, descriptorId, bArr));
    }

    @DexIgnore
    public final void b(HIDState hIDState, HIDState hIDState2) {
        b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), hIDState, hIDState2);
        a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), hIDState, hIDState2);
        a(hIDState, hIDState2);
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, State state) {
        this.e.post(new n(this, gattResult, state));
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, HIDState hIDState, HIDState hIDState2) {
        this.e.post(new o(this, gattResult, hIDState, hIDState2));
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, UUID[] uuidArr, GattCharacteristic.CharacteristicId[] characteristicIdArr) {
        this.e.post(new t(this, gattResult, uuidArr, characteristicIdArr));
    }

    @DexIgnore
    public final void a(GattOperationResult.GattResult gattResult, BondState bondState, BondState bondState2) {
        this.e.post(new q(this, gattResult, bondState, bondState2));
    }

    @DexIgnore
    public final void a(int i2, State state) {
        Iterator<b> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.e.post(new p(it.next(), state, i2));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void b(a aVar) {
        kd4.b(aVar, "bleCommandExecutionCallback");
        this.i.remove(aVar);
    }

    @DexIgnore
    public final void a(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "Peripheral", "onCharacteristicChanged: " + characteristicId.name() + ": " + k90.a(bArr, (String) null, 1, (Object) null), false, 8, (Object) null);
        Iterator<b> it = this.j.iterator();
        while (it.hasNext()) {
            try {
                this.e.post(new k(it.next(), characteristicId, bArr));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void b(f fVar) {
        kd4.b(fVar, "peripheralEventListener");
        this.k.remove(fVar);
    }

    @DexIgnore
    public final void b(b bVar) {
        kd4.b(bVar, "bleNotificationCallback");
        this.j.remove(bVar);
    }

    @DexIgnore
    public final void b() {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.connectHID: Bluetooth Off.", false, 8, (Object) null);
            GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null);
            HIDState hIDState = HIDState.DISCONNECTED;
            a(gattResult, hIDState, hIDState);
            return;
        }
        int i2 = z00.c[j().ordinal()];
        if (i2 == 1) {
            GattOperationResult.GattResult a2 = GattOperationResult.GattResult.Companion.a(HIDProfile.e.a(this.v));
            if (a2.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
                HIDState hIDState2 = HIDState.DISCONNECTED;
                a(a2, hIDState2, hIDState2);
            }
        } else if (i2 != 2 && i2 == 3) {
            GattOperationResult.GattResult gattResult2 = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
            HIDState hIDState3 = HIDState.CONNECTED;
            a(gattResult2, hIDState3, hIDState3);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3) {
        synchronized (this.r) {
            State c2 = c(i2);
            if (c2 != this.r) {
                LogLevel logLevel = LogLevel.DEBUG;
                a(this, logLevel, "Peripheral", "Peripheral state changed: " + this.r + " to " + c2 + ", " + "status=" + i3 + '.', false, 8, (Object) null);
                this.r = c2;
                if (i2 == 0) {
                    a();
                }
                b(GattOperationResult.GattResult.Companion.a(i3), this.r);
                a(GattOperationResult.GattResult.Companion.a(i3), this.r);
                if (this.r == State.DISCONNECTED) {
                    this.e.post(new i(this.h, this, i2, i3));
                }
                a(i3, this.r);
                a(this.r);
                if (this.r == State.DISCONNECTED) {
                    s();
                }
                qa4 qa4 = qa4.a;
            }
        }
    }

    @DexIgnore
    public final void b(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.writeCharacteristic: Bluetooth Off.", false, 8, (Object) null);
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), characteristicId, new byte[0]);
            return;
        }
        GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
        if (this.f == null) {
            gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null);
        } else {
            GattCharacteristic gattCharacteristic = this.l.get(characteristicId);
            BluetoothGattCharacteristic a2 = gattCharacteristic != null ? gattCharacteristic.a() : null;
            if (a2 != null) {
                a2.setValue(bArr);
            }
            if (a2 == null) {
                gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.CHARACTERISTIC_NOT_FOUND, 0, 2, (fd4) null);
            } else {
                BluetoothGatt bluetoothGatt = this.f;
                if (bluetoothGatt == null || true != bluetoothGatt.writeCharacteristic(a2)) {
                    gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null);
                }
            }
        }
        LogLevel logLevel = LogLevel.DEBUG;
        a(this, logLevel, "Peripheral", "writeCharacteristic: " + characteristicId + ": " + bArr.length + " bytes: " + k90.a(bArr, (String) null, 1, (Object) null), false, 8, (Object) null);
        if (gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            b(gattResult, characteristicId, new byte[0]);
        }
    }

    @DexIgnore
    public final void a(State state) {
        for (f vVar : this.k) {
            try {
                this.e.post(new v(vVar, this, state));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(HIDState hIDState, HIDState hIDState2) {
        for (f uVar : this.k) {
            try {
                this.e.post(new u(uVar, this, hIDState, hIDState2));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final void a(BluetoothLeAdapter.State state, BluetoothLeAdapter.State state2) {
        if (state2 != BluetoothLeAdapter.State.ENABLED) {
            a();
            a(0, -1);
        }
    }

    @DexIgnore
    public final void a(List<? extends BluetoothGattService> list) {
        for (BluetoothGattService characteristics : list) {
            for (BluetoothGattCharacteristic next : characteristics.getCharacteristics()) {
                GattCharacteristic.a aVar = GattCharacteristic.b;
                UUID uuid = next.getUuid();
                kd4.a((Object) uuid, "characteristic.uuid");
                GattCharacteristic.CharacteristicId a2 = aVar.a(uuid);
                if (a2 != GattCharacteristic.CharacteristicId.UNKNOWN) {
                    this.l.put(a2, new GattCharacteristic(next));
                }
            }
        }
    }

    @DexIgnore
    public final void b(GattCharacteristic.CharacteristicId characteristicId) {
        kd4.b(characteristicId, "characteristicId");
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.readCharacteristic: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), characteristicId, new byte[0]);
            return;
        }
        GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
        if (this.f == null) {
            gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null);
        } else {
            GattCharacteristic gattCharacteristic = this.l.get(characteristicId);
            BluetoothGattCharacteristic a2 = gattCharacteristic != null ? gattCharacteristic.a() : null;
            if (a2 == null) {
                gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.CHARACTERISTIC_NOT_FOUND, 0, 2, (fd4) null);
            } else {
                BluetoothGatt bluetoothGatt = this.f;
                if (bluetoothGatt == null || true != bluetoothGatt.readCharacteristic(a2)) {
                    gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null);
                }
            }
        }
        if (gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            a(gattResult, characteristicId, new byte[0]);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(Peripheral peripheral, LogLevel logLevel, String str, String str2, boolean z2, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            z2 = true;
        }
        peripheral.a(logLevel, str, str2, z2);
    }

    @DexIgnore
    public final void a(LogLevel logLevel, String str, String str2, boolean z2) {
        kd4.b(logLevel, "logLevel");
        kd4.b(str, "tag");
        kd4.b(str2, "message");
        t90 t90 = t90.c;
        int priority$blesdk_productionRelease = logLevel.getPriority$blesdk_productionRelease();
        if (z2) {
            str2 = this.v.getAddress() + " - " + str2;
        }
        t90.a(priority$blesdk_productionRelease, str, str2);
    }

    @DexIgnore
    public final boolean a(a aVar) {
        kd4.b(aVar, "bleCommandExecutionCallback");
        if (!this.i.contains(aVar)) {
            return this.i.add(aVar);
        }
        return true;
    }

    @DexIgnore
    public final boolean a(f fVar) {
        kd4.b(fVar, "peripheralEventListener");
        if (!this.k.contains(fVar)) {
            return this.k.add(fVar);
        }
        return true;
    }

    @DexIgnore
    public final boolean a(b bVar) {
        kd4.b(bVar, "bleNotificationCallback");
        if (!this.j.contains(bVar)) {
            return this.j.add(bVar);
        }
        return true;
    }

    @DexIgnore
    public final void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, "bluetoothCommand");
        bluetoothCommand.b((xc4<? super BluetoothCommand, qa4>) new Peripheral$executeBluetoothCommand$Anon1(this, bluetoothCommand));
        bluetoothCommand.a((xc4<? super BluetoothCommand, qa4>) new Peripheral$executeBluetoothCommand$Anon2(this, bluetoothCommand));
        this.h.a(bluetoothCommand);
    }

    @DexIgnore
    public final void a(BluetoothCommand bluetoothCommand, BluetoothCommand.Result.ResultCode resultCode) {
        kd4.b(resultCode, "resultCode");
        if (bluetoothCommand != null) {
            this.h.a(bluetoothCommand, resultCode);
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        BluetoothGatt bluetoothGatt;
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.connect: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), State.DISCONNECTED);
            return;
        }
        int i2 = z00.a[this.r.ordinal()];
        if (i2 == 1) {
            BluetoothGatt bluetoothGatt2 = this.f;
            if (bluetoothGatt2 == null) {
                a(this, LogLevel.DEBUG, "Peripheral", "Device.connectGatt.", false, 8, (Object) null);
                if (Build.VERSION.SDK_INT >= 23) {
                    bluetoothGatt = this.v.connectGatt(va0.f.a(), z2, this.g, 2);
                } else {
                    bluetoothGatt = this.v.connectGatt(va0.f.a(), z2, this.g);
                }
                this.f = bluetoothGatt;
                if (this.f != null) {
                    a(1, 0);
                } else {
                    a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), State.DISCONNECTED);
                }
            } else if (bluetoothGatt2 == null || true != bluetoothGatt2.connect()) {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt.connect=false.", false, 8, (Object) null);
                a();
                a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), State.DISCONNECTED);
            } else {
                a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt.connect=true.", false, 8, (Object) null);
                a(1, 0);
            }
        } else if (i2 != 2 && i2 == 3) {
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), State.CONNECTED);
        }
    }

    @DexIgnore
    public final boolean a(BluetoothGatt bluetoothGatt) {
        boolean z2 = false;
        Object obj = null;
        if (y00.f.a()) {
            if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
                a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.refreshDeviceCache: Bluetooth Off.", false, 8, (Object) null);
                obj = "Peripheral.refreshDeviceCache: Bluetooth Off.";
            } else {
                try {
                    a(this, LogLevel.DEBUG, "Peripheral", "Calling private method gatt.refresh()", false, 8, (Object) null);
                    Object invoke = bluetoothGatt.getClass().getMethod("refresh", new Class[0]).invoke(bluetoothGatt, new Object[0]);
                    if (invoke != null) {
                        z2 = ((Boolean) invoke).booleanValue();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Boolean");
                    }
                } catch (Exception e2) {
                    obj = e2.getLocalizedMessage();
                    da0.l.a(e2);
                }
            }
        }
        EventType eventType = EventType.REQUEST;
        String str = this.s;
        String uuid = UUID.randomUUID().toString();
        kd4.a((Object) uuid, "UUID.randomUUID().toString()");
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.ERROR_DETAIL;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        da0.l.b(new SdkLogEntry("refresh_device_cache", eventType, str, "", uuid, z2, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(jSONObject, jSONKey, obj), 448, (fd4) null));
        return z2;
    }

    @DexIgnore
    public final void a() {
        BluetoothGatt bluetoothGatt = this.f;
        if (bluetoothGatt != null) {
            if (this.t) {
                a(bluetoothGatt);
            }
            a(this, LogLevel.DEBUG, "Peripheral", "BluetoothGatt.close.", false, 8, (Object) null);
            bluetoothGatt.close();
        }
        this.f = null;
    }

    @DexIgnore
    public final void a(int i2) {
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.requestMtu: Bluetooth Off.", false, 8, (Object) null);
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), 0);
        } else if (i2 == this.m) {
            b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null), this.m);
        } else {
            BluetoothGatt bluetoothGatt = this.f;
            if (bluetoothGatt == null) {
                b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null), this.m);
            } else if (bluetoothGatt == null || true != bluetoothGatt.requestMtu(i2)) {
                b(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null), this.m);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034  */
    public final GattCharacteristicProperty[] a(GattCharacteristic.CharacteristicId characteristicId) {
        Integer num;
        kd4.b(characteristicId, "characteristicId");
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED || this.f == null) {
            return new GattCharacteristicProperty[0];
        }
        GattCharacteristic gattCharacteristic = this.l.get(characteristicId);
        if (gattCharacteristic != null) {
            BluetoothGattCharacteristic a2 = gattCharacteristic.a();
            if (a2 != null) {
                num = Integer.valueOf(a2.getProperties());
                if (num != null) {
                    return new GattCharacteristicProperty[0];
                }
                return GattCharacteristicProperty.Companion.a(num.intValue());
            }
        }
        num = null;
        if (num != null) {
        }
    }

    @DexIgnore
    public final void a(GattCharacteristic.CharacteristicId characteristicId, GattDescriptor.DescriptorId descriptorId, byte[] bArr) {
        GattCharacteristic.CharacteristicId characteristicId2 = characteristicId;
        GattDescriptor.DescriptorId descriptorId2 = descriptorId;
        byte[] bArr2 = bArr;
        kd4.b(characteristicId, "characteristicId");
        kd4.b(descriptorId, "descriptorId");
        kd4.b(bArr2, "data");
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.writeDescriptor: Bluetooth Off.", false, 8, (Object) null);
            a(new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.BLUETOOTH_OFF, 0, 2, (fd4) null), characteristicId, descriptorId, new byte[0]);
            return;
        }
        GattOperationResult.GattResult gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.SUCCESS, 0, 2, (fd4) null);
        if (this.f == null) {
            gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.GATT_NULL, 0, 2, (fd4) null);
        } else {
            GattCharacteristic gattCharacteristic = this.l.get(characteristicId);
            BluetoothGattCharacteristic a2 = gattCharacteristic != null ? gattCharacteristic.a() : null;
            if (a2 == null) {
                gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.CHARACTERISTIC_NOT_FOUND, 0, 2, (fd4) null);
            } else {
                BluetoothGattDescriptor descriptor = a2.getDescriptor(descriptorId.getUuid());
                if (descriptor != null) {
                    descriptor.setValue(bArr2);
                }
                if (descriptor == null) {
                    gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.DESCRIPTOR_NOT_FOUND, 0, 2, (fd4) null);
                } else {
                    BluetoothGatt bluetoothGatt = this.f;
                    if (bluetoothGatt == null || true != bluetoothGatt.writeDescriptor(descriptor)) {
                        LogLevel logLevel = LogLevel.DEBUG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("bluetoothGatt.writeDescriptor fail: ");
                        sb.append("service: ");
                        BluetoothGattCharacteristic characteristic = descriptor.getCharacteristic();
                        kd4.a((Object) characteristic, "descriptor.characteristic");
                        BluetoothGattService service = characteristic.getService();
                        kd4.a((Object) service, "descriptor.characteristic.service");
                        sb.append(service.getUuid());
                        sb.append(", ");
                        sb.append("characteristic: ");
                        BluetoothGattCharacteristic characteristic2 = descriptor.getCharacteristic();
                        kd4.a((Object) characteristic2, "descriptor.characteristic");
                        sb.append(characteristic2.getUuid());
                        sb.append(", ");
                        sb.append("descriptor: ");
                        sb.append(descriptor.getUuid());
                        sb.append(", ");
                        sb.append("value: ");
                        byte[] value = descriptor.getValue();
                        kd4.a((Object) value, "descriptor.value");
                        sb.append(k90.a(value, (String) null, 1, (Object) null));
                        a(this, logLevel, "Peripheral", sb.toString(), false, 8, (Object) null);
                        gattResult = new GattOperationResult.GattResult(GattOperationResult.GattResult.ResultCode.START_FAIL, 0, 2, (fd4) null);
                    }
                }
            }
        }
        if (gattResult.getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            a(gattResult, characteristicId, descriptorId, new byte[0]);
        }
    }

    @DexIgnore
    public final boolean a(GattCharacteristic.CharacteristicId characteristicId, boolean z2) {
        kd4.b(characteristicId, "characteristicId");
        if (BluetoothLeAdapter.l.c() != BluetoothLeAdapter.State.ENABLED) {
            a(this, LogLevel.DEBUG, "Peripheral", "Peripheral.setCharacteristicNotification: Bluetooth Off.", false, 8, (Object) null);
            return false;
        }
        GattCharacteristic gattCharacteristic = this.l.get(characteristicId);
        BluetoothGattCharacteristic a2 = gattCharacteristic != null ? gattCharacteristic.a() : null;
        if (a2 == null) {
            return false;
        }
        BluetoothGatt bluetoothGatt = this.f;
        if (bluetoothGatt == null || true != bluetoothGatt.setCharacteristicNotification(a2, z2)) {
            return false;
        }
        return true;
    }
}
