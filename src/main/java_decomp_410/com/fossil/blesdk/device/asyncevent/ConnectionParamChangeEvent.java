package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionParamChangeEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ConnectionParamChangeEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ConnectionParamChangeEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ConnectionParamChangeEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public ConnectionParamChangeEvent[] newArray(int i) {
            return new ConnectionParamChangeEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ConnectionParamChangeEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public ConnectionParamChangeEvent(byte b) {
        super(AsyncEventType.CONNECTION_PARAM_CHANGE_EVENT, b);
    }

    @DexIgnore
    public ConnectionParamChangeEvent(Parcel parcel) {
        super(parcel);
    }
}
