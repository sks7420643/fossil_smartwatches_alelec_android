package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.device.logic.request.code.AsyncOperationCode;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.w00;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.NoWhenBranchMatchedException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class AsyncEvent extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ byte eventSequence;
    @DexIgnore
    public /* final */ AsyncEventType eventType;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AsyncEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AsyncEvent createFromParcel(Parcel parcel) {
            throw null;
            // kd4.b(parcel, "parcel");
            // String readString = parcel.readString();
            // if (readString != null) {
            //     AsyncEventType valueOf = AsyncEventType.valueOf(readString);
            //     parcel.setDataPosition(0);
            //     switch (w00.a[valueOf.ordinal()]) {
            //         case 1:
            //             return JSONRequestEvent.CREATOR.createFromParcel(parcel);
            //         case 2:
            //             return HeartbeatEvent.CREATOR.createFromParcel(parcel);
            //         case 3:
            //             return ConnectionParamChangeEvent.CREATOR.createFromParcel(parcel);
            //         case 4:
            //             return AppNotificationControlEvent.CREATOR.createFromParcel(parcel);
            //         case 5:
            //             return MusicAsyncEvent.CREATOR.createFromParcel(parcel);
            //         case 6:
            //             return BackgroundSyncEvent.CREATOR.createFromParcel(parcel);
            //         case 7:
            //             return ServiceChangeEvent.CREATOR.createFromParcel(parcel);
            //         case 8:
            //             return MicroAppAsyncEvent.CREATOR.createFromParcel(parcel);
            //         case 9:
            //             return TimeSyncEvent.CREATOR.createFromParcel(parcel);
            //         default:
            //             throw new NoWhenBranchMatchedException();
            //     }
            // } else {
            //     kd4.a();
            //     throw null;
            // }
        }

        @DexIgnore
        public AsyncEvent[] newArray(int i) {
            return new AsyncEvent[i];
        }
    }

    @DexIgnore
    public AsyncEvent(AsyncEventType asyncEventType, byte b) {
        kd4.b(asyncEventType, "eventType");
        this.eventType = asyncEventType;
        this.eventSequence = b;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getAckResponseData$blesdk_productionRelease() {
        byte[] eventResponseData = getEventResponseData();
        ByteBuffer order = ByteBuffer.allocate(eventResponseData.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(AsyncOperationCode.NOTIFY.getId$blesdk_productionRelease());
        order.put(this.eventType.getId$blesdk_productionRelease());
        order.put(this.eventSequence);
        order.put(eventResponseData);
        byte[] array = order.array();
        kd4.a((Object) array, "ackData.array()");
        return array;
    }

    @DexIgnore
    public byte[] getEventResponseData() {
        return new byte[0];
    }

    @DexIgnore
    public final byte getEventSequence$blesdk_productionRelease() {
        return this.eventSequence;
    }

    @DexIgnore
    public final AsyncEventType getEventType$blesdk_productionRelease() {
        return this.eventType;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.NAME, this.eventType.getLogName$blesdk_productionRelease()), JSONKey.SEQUENCE, Byte.valueOf(this.eventSequence));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.eventType.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.eventSequence);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public AsyncEvent(Parcel parcel) {
        throw null;
        // this(AsyncEventType.valueOf(r0), parcel.readByte());
        // kd4.b(parcel, "parcel");
        // String readString = parcel.readString();
        // if (readString != null) {
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
