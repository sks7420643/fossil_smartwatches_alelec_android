package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.notification.AppNotificationControlAction;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotificationControlEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int NOTIFICATION_EVENT_ACTION_INDEX; // = 4;
    @DexIgnore
    public static /* final */ int NOTIFICATION_EVENT_UID_INDEX; // = 0;
    @DexIgnore
    public /* final */ AppNotificationControlAction action;
    @DexIgnore
    public /* final */ int notificationUid;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AppNotificationControlEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AppNotificationControlEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AppNotificationControlEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public AppNotificationControlEvent[] newArray(int i) {
            return new AppNotificationControlEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationControlEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final AppNotificationControlAction getAction() {
        return this.action;
    }

    @DexIgnore
    public final int getNotificationUid() {
        return this.notificationUid;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(super.toJSONObject(), JSONKey.ACTION, this.action.getLogName$blesdk_productionRelease()), JSONKey.NOTIFICATION_UID, Integer.valueOf(this.notificationUid));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.action.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.notificationUid);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AppNotificationControlEvent(byte b, int i, AppNotificationControlAction appNotificationControlAction) {
        super(AsyncEventType.APP_NOTIFICATION_EVENT, b);
        kd4.b(appNotificationControlAction, "action");
        this.action = appNotificationControlAction;
        this.notificationUid = i;
    }

    @DexIgnore
    public AppNotificationControlEvent(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.action = AppNotificationControlAction.valueOf(readString);
            this.notificationUid = parcel.readInt();
            return;
        }
        kd4.a();
        throw null;
    }
}
