package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ServiceChangeEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ServiceChangeEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ServiceChangeEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ServiceChangeEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public ServiceChangeEvent[] newArray(int i) {
            return new ServiceChangeEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ServiceChangeEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public ServiceChangeEvent(byte b) {
        super(AsyncEventType.SERVICE_CHANGE_EVENT, b);
    }

    @DexIgnore
    public ServiceChangeEvent(Parcel parcel) {
        super(parcel);
    }
}
