package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class JSONRequestEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ int requestId;
    @DexIgnore
    public /* final */ JSONObject requestedApps;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<JSONRequestEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public JSONRequestEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new JSONRequestEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public JSONRequestEvent[] newArray(int i) {
            return new JSONRequestEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ JSONRequestEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) JSONRequestEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            JSONRequestEvent jSONRequestEvent = (JSONRequestEvent) obj;
            return getEventType$blesdk_productionRelease() == jSONRequestEvent.getEventType$blesdk_productionRelease() && getEventSequence$blesdk_productionRelease() == jSONRequestEvent.getEventSequence$blesdk_productionRelease() && this.requestId == jSONRequestEvent.requestId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public final int getRequestId$blesdk_productionRelease() {
        return this.requestId;
    }

    @DexIgnore
    public final JSONObject getRequestedApps$blesdk_productionRelease() {
        return this.requestedApps;
    }

    @DexIgnore
    public int hashCode() {
        return (((getEventType$blesdk_productionRelease().hashCode() * 31) + getEventSequence$blesdk_productionRelease()) * 31) + this.requestId;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(super.toJSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(this.requestId)), JSONKey.REQUEST_DATA, this.requestedApps);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.requestId);
        }
        if (parcel != null) {
            parcel.writeString(this.requestedApps.toString());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public JSONRequestEvent(byte b, int i, JSONObject jSONObject) {
        super(AsyncEventType.JSON_FILE_EVENT, b);
        kd4.b(jSONObject, "requestedApps");
        this.requestId = i;
        this.requestedApps = jSONObject;
    }

    @DexIgnore
    public JSONRequestEvent(Parcel parcel) {
        super(parcel);
        this.requestId = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            try {
                this.requestedApps = new JSONObject(readString);
            } catch (JSONException e) {
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
