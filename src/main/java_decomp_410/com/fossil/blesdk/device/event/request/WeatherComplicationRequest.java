package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherComplicationRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherComplicationRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherComplicationRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherComplicationRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public WeatherComplicationRequest[] newArray(int i) {
            return new WeatherComplicationRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherComplicationRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public WeatherComplicationRequest(byte b, int i) {
        super(DeviceEventId.WEATHER_COMPLICATION, b, i);
    }

    @DexIgnore
    public WeatherComplicationRequest(Parcel parcel) {
        super(parcel);
    }
}
