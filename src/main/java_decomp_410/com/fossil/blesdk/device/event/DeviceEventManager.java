package com.fossil.blesdk.device.event;

import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.asyncevent.AppNotificationControlEvent;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent;
import com.fossil.blesdk.device.asyncevent.JSONRequestEvent;
import com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent;
import com.fossil.blesdk.device.asyncevent.MusicAsyncEvent;
import com.fossil.blesdk.device.event.notification.AppNotificationControlNotification;
import com.fossil.blesdk.device.event.notification.CommuteTimeWatchAppNotification;
import com.fossil.blesdk.device.event.notification.MusicControlNotification;
import com.fossil.blesdk.device.event.request.BuddyChallengeStepUpdatedRequest;
import com.fossil.blesdk.device.event.request.ChanceOfRainComplicationRequest;
import com.fossil.blesdk.device.event.request.CommuteTimeETAMicroAppRequest;
import com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest;
import com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest;
import com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest;
import com.fossil.blesdk.device.event.request.RingMyPhoneMicroAppRequest;
import com.fossil.blesdk.device.event.request.RingPhoneRequest;
import com.fossil.blesdk.device.event.request.WeatherComplicationRequest;
import com.fossil.blesdk.device.event.request.WeatherWatchAppRequest;
import com.fossil.blesdk.model.enumerate.CommuteTimeWatchAppAction;
import com.fossil.blesdk.model.enumerate.RingPhoneAction;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.obfuscated.b30;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.xa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceEventManager {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation, AsyncEvent asyncEvent) {
        kd4.b(deviceImplementation, "device");
        kd4.b(asyncEvent, Constants.EVENT);
        switch (b30.d[asyncEvent.getEventType$blesdk_productionRelease().ordinal()]) {
            case 1:
                for (DeviceEvent a2 : a((JSONRequestEvent) asyncEvent)) {
                    deviceImplementation.a(a2);
                }
                return;
            case 2:
                deviceImplementation.a(asyncEvent);
                return;
            case 3:
                deviceImplementation.b(true);
                deviceImplementation.a(asyncEvent);
                return;
            case 5:
                AppNotificationControlEvent appNotificationControlEvent = (AppNotificationControlEvent) asyncEvent;
                deviceImplementation.a((DeviceEvent) new AppNotificationControlNotification(asyncEvent.getEventSequence$blesdk_productionRelease(), appNotificationControlEvent.getNotificationUid(), appNotificationControlEvent.getAction()));
                return;
            case 6:
                deviceImplementation.a((DeviceEvent) new MusicControlNotification(asyncEvent.getEventSequence$blesdk_productionRelease(), ((MusicAsyncEvent) asyncEvent).getAction()));
                return;
            case 7:
                if (xa0.c.a()) {
                    deviceImplementation.a((AsyncEvent) (BackgroundSyncEvent) asyncEvent).e(new DeviceEventManager$processDeviceEvent$Anon2(asyncEvent, deviceImplementation));
                    return;
                }
                return;
            case 8:
                MicroAppAsyncEvent microAppAsyncEvent = (MicroAppAsyncEvent) asyncEvent;
                int i = b30.c[microAppAsyncEvent.getMicroAppEvent$blesdk_productionRelease().getMicroAppId().ordinal()];
                if (i == 1) {
                    int i2 = b30.b[microAppAsyncEvent.getMicroAppEvent$blesdk_productionRelease().getVariant().ordinal()];
                    if (i2 == 1) {
                        deviceImplementation.a((DeviceEvent) new CommuteTimeETAMicroAppRequest(asyncEvent.getEventSequence$blesdk_productionRelease(), microAppAsyncEvent.getMicroAppEvent$blesdk_productionRelease()));
                        return;
                    } else if (i2 == 2) {
                        deviceImplementation.a((DeviceEvent) new CommuteTimeTravelMicroAppRequest(asyncEvent.getEventSequence$blesdk_productionRelease(), microAppAsyncEvent.getMicroAppEvent$blesdk_productionRelease()));
                        return;
                    } else {
                        return;
                    }
                } else if (i == 2) {
                    deviceImplementation.a((DeviceEvent) new RingMyPhoneMicroAppRequest(asyncEvent.getEventSequence$blesdk_productionRelease(), microAppAsyncEvent.getMicroAppEvent$blesdk_productionRelease()));
                    return;
                } else {
                    return;
                }
            case 9:
                deviceImplementation.a(asyncEvent);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final DeviceEvent[] a(JSONRequestEvent jSONRequestEvent) {
        t90 t90 = t90.c;
        t90.a("DeviceEventManager", "parseJSONRequestEvent invoked: " + jSONRequestEvent.toJSONString(2));
        byte eventSequence$blesdk_productionRelease = jSONRequestEvent.getEventSequence$blesdk_productionRelease();
        int requestId$blesdk_productionRelease = jSONRequestEvent.getRequestId$blesdk_productionRelease();
        ArrayList arrayList = new ArrayList();
        Iterator<String> keys = jSONRequestEvent.getRequestedApps$blesdk_productionRelease().keys();
        kd4.a((Object) keys, "jsonRequestEvent.requestedApps.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            JSONObject optJSONObject = jSONRequestEvent.getRequestedApps$blesdk_productionRelease().optJSONObject(next);
            if (optJSONObject != null) {
                Object obj = null;
                if (next != null) {
                    switch (next.hashCode()) {
                        case -1119206702:
                            if (next.equals(Constants.RING_MY_PHONE)) {
                                String optString = optJSONObject.optString("action");
                                if (optString != null) {
                                    RingPhoneAction a2 = RingPhoneAction.Companion.a(optString);
                                    if (a2 != null) {
                                        obj = new RingPhoneRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease, a2);
                                        break;
                                    }
                                }
                            }
                            break;
                        case -755744823:
                            if (next.equals("commuteApp._.config.commute_info")) {
                                String optString2 = optJSONObject.optString("dest");
                                CommuteTimeWatchAppAction.a aVar = CommuteTimeWatchAppAction.Companion;
                                String optString3 = optJSONObject.optString("action");
                                kd4.a((Object) optString3, "dataJson.optString(UIScriptConstant.ACTION)");
                                CommuteTimeWatchAppAction a3 = aVar.a(optString3);
                                if (!(optString2 == null || a3 == null)) {
                                    int i = b30.e[a3.ordinal()];
                                    if (i == 1) {
                                        obj = new CommuteTimeWatchAppRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease, optString2);
                                        break;
                                    } else if (i == 2) {
                                        obj = new CommuteTimeWatchAppNotification(eventSequence$blesdk_productionRelease, optString2, a3);
                                        break;
                                    } else {
                                        throw new NoWhenBranchMatchedException();
                                    }
                                }
                            }
                            break;
                        case -448354243:
                            if (next.equals("buddyChallenge")) {
                                String optString4 = optJSONObject.optString(JSONKey.CHALLENGE_ID.getLogName$blesdk_productionRelease());
                                BuddyChallengeStepUpdatedRequest.RequestType.a aVar2 = BuddyChallengeStepUpdatedRequest.RequestType.Companion;
                                String optString5 = optJSONObject.optString(JSONKey.TYPE.getLogName$blesdk_productionRelease(), "");
                                kd4.a((Object) optString5, "dataJson\n               \u2026JSONKey.TYPE.logName, \"\")");
                                BuddyChallengeStepUpdatedRequest.RequestType a4 = aVar2.a(optString5);
                                int optInt = optJSONObject.optInt(JSONKey.STEP.getLogName$blesdk_productionRelease());
                                if (!(optString4 == null || a4 == null)) {
                                    obj = new BuddyChallengeStepUpdatedRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease, a4, optString4, Integer.valueOf(optInt));
                                    break;
                                }
                            }
                            break;
                        case -186117117:
                            if (next.equals("chanceOfRainSSE._.config.info")) {
                                obj = new ChanceOfRainComplicationRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease);
                                break;
                            }
                            break;
                        case 65217736:
                            if (next.equals("iftttApp._.config.buttons")) {
                                ButtonEvent.a aVar3 = ButtonEvent.Companion;
                                String optString6 = optJSONObject.optString("button_evt");
                                kd4.a((Object) optString6, "dataJson.optString(\"button_evt\")");
                                ButtonEvent a5 = aVar3.a(optString6);
                                if (a5 != null) {
                                    String optString7 = optJSONObject.optString("alias", AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                                    kd4.a((Object) optString7, "alias");
                                    obj = new IFTTTWatchAppRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease, a5, optString7);
                                    break;
                                }
                            }
                            break;
                        case 126482114:
                            if (next.equals("weatherInfo")) {
                                obj = new WeatherComplicationRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease);
                                break;
                            }
                            break;
                        case 1255636834:
                            if (next.equals("weatherApp._.config.locations")) {
                                obj = new WeatherWatchAppRequest(eventSequence$blesdk_productionRelease, requestId$blesdk_productionRelease);
                                break;
                            }
                            break;
                    }
                }
                if (obj != null) {
                    arrayList.add(obj);
                }
            }
        }
        Object[] array = arrayList.toArray(new DeviceEvent[0]);
        if (array != null) {
            return (DeviceEvent[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
