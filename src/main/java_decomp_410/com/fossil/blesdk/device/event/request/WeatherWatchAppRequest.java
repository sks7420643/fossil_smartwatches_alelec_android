package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherWatchAppRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherWatchAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherWatchAppRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherWatchAppRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public WeatherWatchAppRequest[] newArray(int i) {
            return new WeatherWatchAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherWatchAppRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public WeatherWatchAppRequest(byte b, int i) {
        super(DeviceEventId.WEATHER_WATCH_APP, b, i);
    }

    @DexIgnore
    public WeatherWatchAppRequest(Parcel parcel) {
        super(parcel);
    }
}
