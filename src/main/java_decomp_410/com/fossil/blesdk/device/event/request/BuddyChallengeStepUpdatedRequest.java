package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BuddyChallengeStepUpdatedRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String challengeId;
    @DexIgnore
    public /* final */ RequestType requestType;
    @DexIgnore
    public /* final */ Integer steps;

    @DexIgnore
    public enum RequestType {
        UPDATE_STEP,
        REQUEST_INFO;
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String jsonValue;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final RequestType a(String str) {
                kd4.b(str, "jsonValue");
                for (RequestType requestType : RequestType.values()) {
                    if (kd4.a((Object) requestType.getJsonValue(), (Object) str)) {
                        return requestType;
                    }
                }
                return null;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        public final String getJsonValue() {
            return this.jsonValue;
        }

        @DexIgnore
        public final String getLogName() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BuddyChallengeStepUpdatedRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BuddyChallengeStepUpdatedRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BuddyChallengeStepUpdatedRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public BuddyChallengeStepUpdatedRequest[] newArray(int i) {
            return new BuddyChallengeStepUpdatedRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BuddyChallengeStepUpdatedRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BuddyChallengeStepUpdatedRequest.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BuddyChallengeStepUpdatedRequest buddyChallengeStepUpdatedRequest = (BuddyChallengeStepUpdatedRequest) obj;
            return !(kd4.a((Object) this.challengeId, (Object) buddyChallengeStepUpdatedRequest.challengeId) ^ true) && this.requestType == buddyChallengeStepUpdatedRequest.requestType && !(kd4.a((Object) this.steps, (Object) buddyChallengeStepUpdatedRequest.steps) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.BuddyChallengeStepUpdatedRequest");
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.challengeId;
    }

    @DexIgnore
    public final RequestType getRequestType() {
        return this.requestType;
    }

    @DexIgnore
    public final Integer getSteps() {
        return this.steps;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((super.hashCode() * 31) + this.challengeId.hashCode()) * 31) + this.requestType.hashCode()) * 31;
        Integer num = this.steps;
        return hashCode + (num != null ? num.hashCode() : 0);
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject a2 = wa0.a(wa0.a(super.toJSONObject(), JSONKey.CHALLENGE_ID, this.challengeId), JSONKey.REQUEST_TYPE, this.requestType.getLogName());
        JSONKey jSONKey = JSONKey.STEP;
        Object obj = this.steps;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        return wa0.a(a2, jSONKey, obj);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.challengeId);
        }
        if (parcel != null) {
            parcel.writeInt(this.requestType.ordinal());
        }
        if (parcel != null) {
            Integer num = this.steps;
            parcel.writeInt(num != null ? num.intValue() : 0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BuddyChallengeStepUpdatedRequest(byte b, int i, RequestType requestType2, String str, Integer num) {
        super(DeviceEventId.BUDDY_CHALLENGE_STEP_UPDATE, b, i);
        kd4.b(requestType2, "requestType");
        kd4.b(str, "challengeId");
        this.challengeId = str;
        this.requestType = requestType2;
        this.steps = num;
    }

    @DexIgnore
    public BuddyChallengeStepUpdatedRequest(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.challengeId = readString;
            this.requestType = RequestType.values()[parcel.readInt()];
            this.steps = Integer.valueOf(parcel.readInt());
            return;
        }
        kd4.a();
        throw null;
    }
}
