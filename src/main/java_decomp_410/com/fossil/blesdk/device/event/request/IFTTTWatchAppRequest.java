package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class IFTTTWatchAppRequest extends DeviceRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String alias;
    @DexIgnore
    public /* final */ ButtonEvent deviceEvent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<IFTTTWatchAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public IFTTTWatchAppRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new IFTTTWatchAppRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public IFTTTWatchAppRequest[] newArray(int i) {
            return new IFTTTWatchAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ IFTTTWatchAppRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!kd4.a((Object) IFTTTWatchAppRequest.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            IFTTTWatchAppRequest iFTTTWatchAppRequest = (IFTTTWatchAppRequest) obj;
            if (this.deviceEvent != iFTTTWatchAppRequest.deviceEvent) {
                return false;
            }
            String str = this.alias;
            String str2 = iFTTTWatchAppRequest.alias;
            if (str != null) {
                return !str.contentEquals(str2);
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.IFTTTWatchAppRequest");
    }

    @DexIgnore
    public final String getAlias() {
        return this.alias;
    }

    @DexIgnore
    public final ButtonEvent getDeviceEvent() {
        return this.deviceEvent;
    }

    @DexIgnore
    public int hashCode() {
        return (((super.hashCode() * 31) + this.deviceEvent.hashCode()) * 31) + this.alias.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.deviceEvent.ordinal());
        }
        if (parcel != null) {
            parcel.writeString(this.alias);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public IFTTTWatchAppRequest(byte b, int i, ButtonEvent buttonEvent, String str) {
        super(DeviceEventId.IFTTT_APP, b, i);
        kd4.b(buttonEvent, "deviceEvent");
        kd4.b(str, "alias");
        this.deviceEvent = buttonEvent;
        this.alias = str;
    }

    @DexIgnore
    public IFTTTWatchAppRequest(Parcel parcel) {
        super(parcel);
        this.deviceEvent = ButtonEvent.values()[parcel.readInt()];
        String readString = parcel.readString();
        if (readString != null) {
            this.alias = readString;
        } else {
            kd4.a();
            throw null;
        }
    }
}
