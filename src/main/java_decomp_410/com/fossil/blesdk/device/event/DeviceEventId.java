package com.fossil.blesdk.device.event;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceEventId {
    CHANCE_OF_RAIN_COMPLICATION,
    WEATHER_COMPLICATION,
    RING_PHONE,
    WEATHER_WATCH_APP,
    MUSIC_CONTROL,
    ALARM_SYNC,
    DEVICE_CONFIG_SYNC,
    NOTIFICATION_FILTER_SYNC,
    COMMUTE_TIME_ETA_MICRO_APP,
    COMMUTE_TIME_TRAVEL_MICRO_APP,
    RING_MY_PHONE_MICRO_APP,
    APP_NOTIFICATION_CONTROL,
    COMMUTE_TIME_WATCH_APP,
    IFTTT_APP,
    BUDDY_CHALLENGE_STEP_UPDATE;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
