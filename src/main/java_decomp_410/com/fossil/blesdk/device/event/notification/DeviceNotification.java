package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceNotification extends DeviceEvent {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceNotification(DeviceEventId deviceEventId, byte b) {
        super(deviceEventId, b);
        kd4.b(deviceEventId, "deviceEventId");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceNotification(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
    }
}
