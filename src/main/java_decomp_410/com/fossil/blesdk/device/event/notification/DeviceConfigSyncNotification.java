package com.fossil.blesdk.device.event.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.device.event.SynchronizationAction;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceConfigSyncNotification extends DeviceNotification {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ SynchronizationAction action;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DeviceConfigSyncNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DeviceConfigSyncNotification createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DeviceConfigSyncNotification(parcel, (fd4) null);
        }

        @DexIgnore
        public DeviceConfigSyncNotification[] newArray(int i) {
            return new DeviceConfigSyncNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DeviceConfigSyncNotification(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final SynchronizationAction getAction() {
        return this.action;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.action.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceConfigSyncNotification(byte b, BackgroundSyncAction backgroundSyncAction) {
        super(DeviceEventId.DEVICE_CONFIG_SYNC, b);
        kd4.b(backgroundSyncAction, "backgroundSyncAction");
        this.action = SynchronizationAction.Companion.a(backgroundSyncAction);
    }

    @DexIgnore
    public DeviceConfigSyncNotification(Parcel parcel) {
        super(parcel);
        this.action = SynchronizationAction.values()[parcel.readInt()];
    }
}
