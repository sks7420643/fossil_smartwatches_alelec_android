package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VerifySecretKeyPhase$sendPhoneRandomNumber$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ VerifySecretKeyPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifySecretKeyPhase$sendPhoneRandomNumber$Anon2(VerifySecretKeyPhase verifySecretKeyPhase) {
        super(1);
        this.this$Anon0 = verifySecretKeyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Phase.Result.ResultCode resultCode;
        kd4.b(request, "executedRequest");
        if (request.n().getResultCode() == Request.Result.ResultCode.RESPONSE_ERROR) {
            resultCode = Phase.Result.ResultCode.SUCCESS;
        } else {
            resultCode = Phase.Result.ResultCode.Companion.a(request.n());
        }
        Phase.Result.ResultCode resultCode2 = resultCode;
        VerifySecretKeyPhase verifySecretKeyPhase = this.this$Anon0;
        verifySecretKeyPhase.a(Phase.Result.copy$default(verifySecretKeyPhase.k(), (PhaseId) null, resultCode2, request.n(), 1, (Object) null));
    }
}
