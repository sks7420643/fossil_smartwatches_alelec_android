package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrySetConnectionParamsPhase$trySetConnectionParameters$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrySetConnectionParamsPhase$trySetConnectionParameters$2(com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase) {
        super(1);
        this.this$0 = trySetConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase = this.this$0;
        trySetConnectionParamsPhase.f3075z = trySetConnectionParamsPhase.f3075z + 1;
        this.this$0.mo7701B();
    }
}
