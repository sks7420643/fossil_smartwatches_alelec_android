package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeRequest$4 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.xc4 $actionOnError;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.xc4 $isTerminatedRequestError;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$executeRequest$4(com.fossil.blesdk.device.logic.phase.Phase phase, com.fossil.blesdk.obfuscated.xc4 xc4, com.fossil.blesdk.obfuscated.xc4 xc42) {
        super(1);
        this.this$0 = phase;
        this.$isTerminatedRequestError = xc4;
        this.$actionOnError = xc42;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        if (((java.lang.Boolean) this.$isTerminatedRequestError.invoke(request.mo7744n())).booleanValue()) {
            this.this$0.mo7547a(request.mo7744n());
        } else {
            this.$actionOnError.invoke(request);
        }
    }
}
