package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeSubPhase$Anon3 extends Lambda implements xc4<Phase.Result, Boolean> {
    @DexIgnore
    public static /* final */ Phase$executeSubPhase$Anon3 INSTANCE; // = new Phase$executeSubPhase$Anon3();

    @DexIgnore
    public Phase$executeSubPhase$Anon3() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((Phase.Result) obj));
    }

    @DexIgnore
    public final boolean invoke(Phase.Result result) {
        kd4.b(result, "phaseResult");
        return result.getResultCode() == Phase.Result.ResultCode.INTERRUPTED || result.getResultCode() == Phase.Result.ResultCode.CONNECTION_DROPPED || result.getResultCode() == Phase.Result.ResultCode.BLUETOOTH_OFF || result.getRequestResult().getCommandResult().getGattResult().getResultCode() == GattOperationResult.GattResult.ResultCode.GATT_NULL;
    }
}
