package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m70;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StartAuthenticationPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.AUTHENTICATION}));
    @DexIgnore
    public /* final */ AuthenticationKeyType B;
    @DexIgnore
    public /* final */ byte[] C;
    @DexIgnore
    public byte[] z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StartAuthenticationPhase(Peripheral peripheral, Phase.a aVar, AuthenticationKeyType authenticationKeyType, byte[] bArr) {
        super(peripheral, aVar, PhaseId.START_AUTHENTICATION, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(authenticationKeyType, "startAuthenticateKeyType");
        kd4.b(bArr, "randomNumber");
        this.B = authenticationKeyType;
        this.C = bArr;
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new m70(j(), this.B, this.C), (xc4) new StartAuthenticationPhase$sendPhoneRandomNumber$Anon1(this), (xc4) StartAuthenticationPhase$sendPhoneRandomNumber$Anon2.INSTANCE, (yc4) null, (xc4) new StartAuthenticationPhase$sendPhoneRandomNumber$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        if (this.C.length != 8) {
            a(Phase.Result.ResultCode.INVALID_PARAMETER);
        } else {
            A();
        }
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.AUTHENTICATION_KEY_TYPE, this.B.getLogName$blesdk_productionRelease()), JSONKey.PHONE_RANDOM_NUMBER, k90.a(this.C, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.BOTH_SIDES_RANDOM_NUMBERS;
        byte[] bArr = this.z;
        String str = null;
        if (bArr != null) {
            str = k90.a(bArr, (String) null, 1, (Object) null);
        }
        return wa0.a(x, jSONKey, str);
    }

    @DexIgnore
    public byte[] i() {
        byte[] bArr = this.z;
        return bArr != null ? bArr : new byte[0];
    }
}
