package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n70;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AuthenticationKeyType {
    PRE_SHARED_KEY((byte) 0),
    SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY((byte) 1),
    SIXTEEN_BYTES_LSB_ECDH_SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a(AuthenticationKeyType authenticationKeyType, byte[] bArr) {
            kd4.b(authenticationKeyType, "keyType");
            kd4.b(bArr, "data");
            int i = n70.a[authenticationKeyType.ordinal()];
            if (i == 1) {
                return za4.c(bArr);
            }
            if (i == 2 || i == 3) {
                return bArr;
            }
            throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    AuthenticationKeyType(byte b) {
        this.id = b;
        String name = name();
        if (name != null) {
            String lowerCase = name.toLowerCase();
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
