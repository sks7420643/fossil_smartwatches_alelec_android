package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildGetFileRequest$getFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.GetFilePhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildGetFileRequest$getFileRequest$1(com.fossil.blesdk.device.logic.phase.GetFilePhase getFilePhase) {
        super(1);
        this.this$0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0089, code lost:
        if (r4 != null) goto L_0x008f;
     */
    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        byte[] bArr;
        com.fossil.blesdk.device.logic.request.Request request2 = request;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request2, "executedRequest");
        com.fossil.blesdk.obfuscated.f80 f80 = (com.fossil.blesdk.obfuscated.f80) request2;
        com.fossil.blesdk.database.entity.DeviceFile c = this.this$0.mo7386H();
        java.lang.String str = null;
        if (c != null) {
            com.fossil.blesdk.device.data.file.FileHandle fileHandle = new com.fossil.blesdk.device.data.file.FileHandle(c.getFileHandle$blesdk_productionRelease());
            com.fossil.blesdk.database.entity.DeviceFile a = this.this$0.mo7394a(fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease());
            com.fossil.blesdk.obfuscated.t90 t90 = com.fossil.blesdk.obfuscated.t90.f9612c;
            java.lang.String r = this.this$0.mo7571r();
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("Get file in DB for ");
            sb.append(this.this$0.mo7564j().mo6457k());
            sb.append(", ");
            sb.append("fileType=");
            sb.append(fileHandle.getFileType$blesdk_productionRelease());
            sb.append(", ");
            sb.append("fileIndex=");
            sb.append(fileHandle.getFileIndex$blesdk_productionRelease());
            sb.append(", ");
            sb.append("result=");
            if (a != null) {
                str = a.toString();
            }
            sb.append(str);
            t90.mo16336a(r, sb.toString());
            if (a != null) {
                bArr = a.getRawData();
            }
            bArr = new byte[0];
            com.fossil.blesdk.database.entity.DeviceFile deviceFile = new com.fossil.blesdk.database.entity.DeviceFile(this.this$0.mo7564j().mo6457k(), fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease(), com.fossil.blesdk.obfuscated.k90.m9400a(bArr, f80.mo7824L()), f80.mo7823K(), f80.mo7822J(), this.this$0.mo7569p(), false);
            if (request.mo7744n().getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.SUCCESS) {
                this.this$0.mo7399d(deviceFile);
            } else {
                long unused = this.this$0.mo7393a(deviceFile);
            }
        } else {
            com.fossil.blesdk.obfuscated.kd4.m24405a();
            throw null;
        }
    }
}
