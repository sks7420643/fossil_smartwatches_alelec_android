package com.fossil.blesdk.device.logic;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.a90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.supportedclass.PriorityQueue;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PhaseManager {
    @DexIgnore
    public /* final */ PriorityQueue<Phase> a; // = new PriorityQueue<>(b.e);
    @DexIgnore
    public /* final */ PriorityQueue<Phase> b; // = new PriorityQueue<>(c.e);
    @DexIgnore
    public Hashtable<Phase, a> c; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public a90 e;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<Phase> {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        /* renamed from: a */
        public final int compare(Phase phase, Phase phase2) {
            return phase.m().ordinal() - phase2.m().ordinal();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<Phase> {
        @DexIgnore
        public static /* final */ c e; // = new c();

        @DexIgnore
        /* renamed from: a */
        public final int compare(Phase phase, Phase phase2) {
            return phase.m().ordinal() - phase2.m().ordinal();
        }
    }

    @DexIgnore
    public PhaseManager(a90 a90) {
        kd4.b(a90, "resourcePool");
        this.e = a90;
    }

    @DexIgnore
    public final void a(a90 a90) {
        kd4.b(a90, "resourcePool");
        synchronized (this.d) {
            this.e = a90;
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public final Phase[] b() {
        Phase[] phaseArr;
        synchronized (this.d) {
            Object[] array = this.a.toArray(new Phase[0]);
            if (array != null) {
                phaseArr = (Phase[]) array;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return phaseArr;
    }

    @DexIgnore
    public final Phase[] c() {
        Phase[] phaseArr;
        synchronized (this.d) {
            Object[] array = this.b.toArray(new Phase[0]);
            if (array != null) {
                phaseArr = (Phase[]) array;
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return phaseArr;
    }

    @DexIgnore
    public final void d() {
        for (Phase next : this.a) {
            if (next.k().getResultCode() == Phase.Result.ResultCode.NOT_START) {
                kd4.a((Object) next, "phase");
                if (a(next)) {
                    this.a.remove(next);
                    this.b.add(next);
                    next.a((xc4<? super Phase, qa4>) new PhaseManager$startPendingPhases$$inlined$forEach$lambda$Anon1(this));
                    a aVar = this.c.get(next);
                    if (aVar != null) {
                        aVar.a();
                    }
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(Phase phase) {
        Phase phase2;
        if (phase.n().contains(ResourceType.TRANSFER_DATA)) {
            boolean z = false;
            for (Phase next : this.b) {
                if (next.n().contains(ResourceType.TRANSFER_DATA) && next.m().compareTo(phase.m()) < 0) {
                    next.a(Phase.Result.ResultCode.INTERRUPTED);
                    z = true;
                }
                if (!next.a(phase)) {
                    z = true;
                }
            }
            if (z || !this.e.a(phase)) {
                return false;
            }
            return true;
        }
        Iterator<Phase> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                phase2 = null;
                break;
            }
            phase2 = it.next();
            if (!phase2.a(phase)) {
                break;
            }
        }
        if (phase2 != null || !this.e.a(phase)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(Phase.Result.ResultCode resultCode, PhaseId[] phaseIdArr) {
        kd4.b(resultCode, "resultCode");
        kd4.b(phaseIdArr, "exceptions");
        c(resultCode, new PhaseManager$stopAllRunningPhases$Anon1(phaseIdArr));
    }

    @DexIgnore
    public final void c(Phase.Result.ResultCode resultCode, xc4<? super Phase, Boolean> xc4) {
        kd4.b(resultCode, "resultCode");
        kd4.b(xc4, "condition");
        for (Phase next : this.b) {
            kd4.a((Object) next, "phase");
            if (xc4.invoke(next).booleanValue()) {
                next.a(resultCode);
                this.b.remove(next);
            }
        }
    }

    @DexIgnore
    public final void b(Phase.Result.ResultCode resultCode, xc4<? super Phase, Boolean> xc4) {
        kd4.b(resultCode, "resultCode");
        kd4.b(xc4, "condition");
        c(resultCode, xc4);
        a(resultCode, xc4);
    }

    @DexIgnore
    public final void b(Phase phase) {
        kd4.b(phase, "targetPhase");
        this.e.b(phase);
        this.a.remove(phase);
        this.b.remove(phase);
        this.c.remove(phase);
    }

    @DexIgnore
    public final ArrayList<Phase> a() {
        ArrayList<Phase> arrayList;
        synchronized (this.d) {
            arrayList = new ArrayList<>();
            arrayList.addAll(this.a);
            arrayList.addAll(this.b);
        }
        return arrayList;
    }

    @DexIgnore
    public final void a(Phase.Result.ResultCode resultCode, xc4<? super Phase, Boolean> xc4) {
        for (Phase next : this.a) {
            kd4.a((Object) next, "phase");
            if (xc4.invoke(next).booleanValue()) {
                next.a(resultCode);
                this.a.remove(next);
            }
        }
    }

    @DexIgnore
    public final void a(Phase.Result.ResultCode resultCode, PhaseId[] phaseIdArr) {
        kd4.b(resultCode, "resultCode");
        kd4.b(phaseIdArr, "exceptions");
        b(resultCode, (xc4<? super Phase, Boolean>) new PhaseManager$stopAllPhases$Anon1(phaseIdArr));
    }

    @DexIgnore
    public final void a(Phase phase, a aVar) {
        kd4.b(phase, "targetPhase");
        kd4.b(aVar, "resourceAllocationCallback");
        this.a.add(phase);
        this.c.put(phase, aVar);
        d();
    }
}
