package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AuthenticatePhase$sendPhoneRandomNumber$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ AuthenticatePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AuthenticatePhase$sendPhoneRandomNumber$Anon2(AuthenticatePhase authenticatePhase) {
        super(1);
        this.this$Anon0 = authenticatePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Phase.Result.ResultCode resultCode;
        kd4.b(request, "executedRequest");
        if (request.n().getResultCode() == Request.Result.ResultCode.RESPONSE_ERROR) {
            resultCode = Phase.Result.ResultCode.AUTHENTICATION_FAILED;
        } else {
            resultCode = Phase.Result.ResultCode.Companion.a(request.n());
        }
        Phase.Result.ResultCode resultCode2 = resultCode;
        AuthenticatePhase authenticatePhase = this.this$Anon0;
        authenticatePhase.a(Phase.Result.copy$default(authenticatePhase.k(), (PhaseId) null, resultCode2, request.n(), 1, (Object) null));
    }
}
