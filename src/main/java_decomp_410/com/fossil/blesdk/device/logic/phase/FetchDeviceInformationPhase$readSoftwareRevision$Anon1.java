package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.y60;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FetchDeviceInformationPhase$readSoftwareRevision$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ FetchDeviceInformationPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FetchDeviceInformationPhase$readSoftwareRevision$Anon1(FetchDeviceInformationPhase fetchDeviceInformationPhase) {
        super(1);
        this.this$Anon0 = fetchDeviceInformationPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        if (kd4.a((Object) Version.CREATOR.a(((y60) request).B()), (Object) y00.f.d())) {
            this.this$Anon0.A();
        } else {
            this.this$Anon0.B();
        }
    }
}
