package com.fossil.blesdk.device.logic.request.legacy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyTransferDataRequest$actionOnEOFTimeout$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.core.Peripheral $peripheral;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.request.legacy.LegacyTransferDataRequest this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyTransferDataRequest$actionOnEOFTimeout$1(com.fossil.blesdk.device.logic.request.legacy.LegacyTransferDataRequest legacyTransferDataRequest, com.fossil.blesdk.device.core.Peripheral peripheral) {
        super(0);
        this.this$0 = legacyTransferDataRequest;
        this.$peripheral = peripheral;
    }

    @DexIgnore
    public final void invoke() {
        com.fossil.blesdk.device.core.Peripheral.m3116a(this.$peripheral, com.fossil.blesdk.log.debuglog.LogLevel.DEBUG, this.this$0.mo7747q(), "Request EOF timeout.", false, 8, (java.lang.Object) null);
        com.fossil.blesdk.device.logic.request.legacy.LegacyTransferDataRequest legacyTransferDataRequest = this.this$0;
        legacyTransferDataRequest.mo7717a(com.fossil.blesdk.device.logic.request.Request.Result.copy$default(legacyTransferDataRequest.mo7744n(), (com.fossil.blesdk.device.logic.request.RequestId) null, com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.EOF_TIME_OUT, (com.fossil.blesdk.device.core.command.BluetoothCommand.Result) null, (com.fossil.blesdk.obfuscated.o70) null, 13, (java.lang.Object) null));
    }
}
