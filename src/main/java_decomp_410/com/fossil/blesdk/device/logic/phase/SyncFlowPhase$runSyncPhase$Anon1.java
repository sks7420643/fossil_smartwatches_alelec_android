package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncFlowPhase$runSyncPhase$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SyncFlowPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase$runSyncPhase$Anon1(SyncFlowPhase syncFlowPhase) {
        super(1);
        this.this$Anon0 = syncFlowPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "executedPhase");
        this.this$Anon0.A = ((SyncPhase) phase).i();
        if (y00.f.b(this.this$Anon0.e().getDeviceInformation())) {
            SyncFlowPhase syncFlowPhase = this.this$Anon0;
            syncFlowPhase.a(Phase.Result.copy$default(syncFlowPhase.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (this.this$Anon0.D > 0) {
            this.this$Anon0.D();
        } else {
            SyncFlowPhase syncFlowPhase2 = this.this$Anon0;
            syncFlowPhase2.a(Phase.Result.copy$default(syncFlowPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        }
    }
}
