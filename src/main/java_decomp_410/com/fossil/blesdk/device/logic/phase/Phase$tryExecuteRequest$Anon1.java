package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$tryExecuteRequest$Anon1 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ RequestId $nextRequestId;
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$tryExecuteRequest$Anon1(Phase phase, RequestId requestId) {
        super(0);
        this.this$Anon0 = phase;
        this.$nextRequestId = requestId;
    }

    @DexIgnore
    public final void invoke() {
        Phase.a(this.this$Anon0, this.$nextRequestId, (RequestId) null, 2, (Object) null);
    }
}
