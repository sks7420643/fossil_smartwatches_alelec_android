package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getListFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacySyncPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getListFileRequest$1(com.fossil.blesdk.device.logic.phase.LegacySyncPhase legacySyncPhase) {
        super(1);
        this.this$0 = legacySyncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        com.fossil.blesdk.obfuscated.u80 u80 = (com.fossil.blesdk.obfuscated.u80) request;
        this.this$0.f2954A = u80.mo16681K();
        this.this$0.f2955B = u80.mo16682L();
        this.this$0.f2956C = 0;
        if (this.this$0.f2954A <= 0) {
            com.fossil.blesdk.device.logic.phase.LegacySyncPhase legacySyncPhase = this.this$0;
            legacySyncPhase.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(legacySyncPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        } else if (this.this$0.f2962I) {
            this.this$0.mo7443B();
        } else {
            this.this$0.mo7445D();
        }
    }
}
