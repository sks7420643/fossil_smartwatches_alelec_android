package com.fossil.blesdk.device.logic.request.legacy;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.c20;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j60;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n10;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyTransferDataRequest extends Request {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ long C; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ wc4<qa4> E;
    @DexIgnore
    public b F;
    @DexIgnore
    public /* final */ short G;
    @DexIgnore
    public /* final */ j60 H;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Request.b {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(LegacyTransferDataRequest legacyTransferDataRequest, wc4<qa4> wc4) {
            super(legacyTransferDataRequest, wc4);
            kd4.b(wc4, "onTimeOut");
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyTransferDataRequest(short s, j60 j60, Peripheral peripheral) {
        super(RequestId.LEGACY_TRANSFER_DATA, peripheral, 0, 4, (fd4) null);
        kd4.b(j60, "dataTransferProtocol");
        kd4.b(peripheral, "peripheral");
        this.G = s;
        this.H = j60;
        this.E = new LegacyTransferDataRequest$actionOnEOFTimeout$Anon1(this, peripheral);
    }

    @DexIgnore
    public final void A() {
        B();
        this.F = new b(this, this.E);
        b bVar = this.F;
        if (bVar != null) {
            e().postDelayed(bVar, this.C);
        }
    }

    @DexIgnore
    public final void B() {
        b bVar = this.F;
        if (bVar != null) {
            e().removeCallbacks(bVar);
        }
        b bVar2 = this.F;
        if (bVar2 != null) {
            bVar2.a();
        }
        this.F = null;
    }

    @DexIgnore
    public final void C() {
        float min = Math.min((((float) this.H.g()) * 1.0f) / ((float) this.H.i()), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.H.g() >= this.H.i()) {
            this.D = min;
            a(this.D);
        }
    }

    @DexIgnore
    public void b(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
        if (c20.a() == GattCharacteristic.CharacteristicId.FTC && c20.b().length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(c20.b()).order(ByteOrder.LITTLE_ENDIAN);
            byte b2 = order.get(0);
            if (this.G != order.getShort(1)) {
                return;
            }
            if (LegacyFileControlOperationCode.LEGACY_PUT_FILE_EOF.getCode() == b2) {
                B();
                Request.Result a2 = Request.Result.Companion.a((o70) LegacyFileControlStatusCode.Companion.a(order.get(3)));
                b(Request.Result.copy$default(n(), (RequestId) null, a2.getResultCode(), (BluetoothCommand.Result) null, a2.getResponseStatus(), 5, (Object) null));
                this.A = n90.b(order.getInt(5));
                a(new Request.ResponseInfo(0, c20.a(), c20.b(), wa0.a(new JSONObject(), JSONKey.WRITTEN_SIZE, Long.valueOf(this.A)), 1, (fd4) null));
                a(n());
                return;
            }
            a(new Request.ResponseInfo(0, c20.a(), c20.b(), (JSONObject) null, 9, (fd4) null));
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.RESPONSE_ERROR, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
    }

    @DexIgnore
    public void d(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.Companion.a(bluetoothCommand.e()).getResultCode(), bluetoothCommand.e(), (o70) null, 9, (Object) null));
        SdkLogEntry l = l();
        if (l != null) {
            l.setSuccess(true);
        }
        SdkLogEntry l2 = l();
        if (l2 != null) {
            JSONObject extraData = l2.getExtraData();
            if (extraData != null) {
                wa0.a(extraData, JSONKey.MESSAGE, Request.Result.ResultCode.SUCCESS.getLogName$blesdk_productionRelease());
            }
        }
        if (n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
            a(bluetoothCommand);
        }
        a(d());
        SdkLogEntry l3 = l();
        if (l3 != null) {
            JSONObject extraData2 = l3.getExtraData();
            if (extraData2 != null) {
                wa0.a(extraData2, JSONKey.TRANSFERRED_DATA_SIZE, Integer.valueOf(this.H.g()));
            }
        }
        C();
        c();
    }

    @DexIgnore
    public BluetoothCommand h() {
        if (!this.H.j()) {
            return null;
        }
        byte[] e = this.H.e();
        if (g()) {
            e = d90.b.b(i().k(), this.H.a(), e);
        }
        return new n10(this.H.a(), e, i().h());
    }

    @DexIgnore
    public long m() {
        return this.B;
    }

    @DexIgnore
    public void s() {
        x();
        y();
        A();
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(super.t(), JSONKey.TRANSFERRED_DATA_SIZE, Integer.valueOf(this.H.i())), JSONKey.HEADER_LENGTH, Integer.valueOf(this.H.f()));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.ACTUAL_WRITTEN_SIZE, Long.valueOf(this.A));
    }

    @DexIgnore
    public void a(long j) {
        this.B = j;
    }
}
