package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildGetFileRequest$getFileRequest$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.request.Request, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.GetFilePhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildGetFileRequest$getFileRequest$2(com.fossil.blesdk.device.logic.phase.GetFilePhase getFilePhase) {
        super(2);
        this.this$0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "<anonymous parameter 0>");
        if (this.this$0.f2926F > 0) {
            float i = (float) (this.this$0.f2927G + this.this$0.f2930J);
            com.fossil.blesdk.database.entity.DeviceFile c = this.this$0.mo7386H();
            if (c != null) {
                f = (i + (((float) (c.getFileLength() - this.this$0.f2930J)) * f)) / ((float) this.this$0.f2926F);
            } else {
                com.fossil.blesdk.obfuscated.kd4.m24405a();
                throw null;
            }
        }
        if (java.lang.Math.abs(f - this.this$0.f2928H) > this.this$0.f2937Q || f == 1.0f) {
            this.this$0.f2928H = f;
            this.this$0.mo7541a(f);
        }
    }
}
