package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o70;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceConfigResponseStatusCode implements o70 {
    SUCCESS((byte) 0),
    UNKNOWN((byte) 1);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte code;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DeviceConfigResponseStatusCode a(byte b) {
            DeviceConfigResponseStatusCode deviceConfigResponseStatusCode;
            DeviceConfigResponseStatusCode[] values = DeviceConfigResponseStatusCode.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    deviceConfigResponseStatusCode = null;
                    break;
                }
                deviceConfigResponseStatusCode = values[i];
                if (deviceConfigResponseStatusCode.getCode() == b) {
                    break;
                }
                i++;
            }
            return deviceConfigResponseStatusCode != null ? deviceConfigResponseStatusCode : DeviceConfigResponseStatusCode.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    DeviceConfigResponseStatusCode(byte b) {
        this.code = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public byte getCode() {
        return this.code;
    }

    @DexIgnore
    public String getLogName() {
        return this.logName;
    }

    @DexIgnore
    public boolean isSuccessCode() {
        return this == SUCCESS;
    }
}
