package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getActivityFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ short $fileHandleGetFile;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacySyncPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getActivityFileRequest$1(com.fossil.blesdk.device.logic.phase.LegacySyncPhase legacySyncPhase, short s) {
        super(1);
        this.this$0 = legacySyncPhase;
        this.$fileHandleGetFile = s;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        byte[] K = ((com.fossil.blesdk.obfuscated.s80) request).mo15902K();
        if (K.length == 0) {
            com.fossil.blesdk.device.logic.phase.LegacySyncPhase legacySyncPhase = this.this$0;
            legacySyncPhase.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(legacySyncPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.INVALID_RESPONSE, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
            return;
        }
        java.util.ArrayList<com.fossil.blesdk.database.entity.DeviceFile> E = this.this$0.mo7446E();
        com.fossil.blesdk.database.entity.DeviceFile deviceFile = new com.fossil.blesdk.database.entity.DeviceFile(this.this$0.mo7564j().mo6457k(), new com.fossil.blesdk.device.data.file.FileHandle(this.$fileHandleGetFile).getFileType$blesdk_productionRelease(), new com.fossil.blesdk.device.data.file.FileHandle(this.$fileHandleGetFile).getFileIndex$blesdk_productionRelease(), K, (long) K.length, com.fossil.blesdk.utils.Crc32Calculator.f11683a.mo18646a(K, com.fossil.blesdk.utils.Crc32Calculator.CrcType.CRC32), this.this$0.mo7569p(), true);
        E.add(deviceFile);
        this.this$0.mo7448G();
    }
}
