package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransmitDataPhase$buildTransferDataRequest$transferDataRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.request.Request, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ long $dataLength;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.TransmitDataPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransmitDataPhase$buildTransferDataRequest$transferDataRequest$1(com.fossil.blesdk.device.logic.phase.TransmitDataPhase transmitDataPhase, long j) {
        super(2);
        this.this$0 = transmitDataPhase;
        this.$dataLength = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "<anonymous parameter 0>");
        com.fossil.blesdk.device.logic.phase.TransmitDataPhase transmitDataPhase = this.this$0;
        transmitDataPhase.f3059C = transmitDataPhase.f3062F + ((long) (f * ((float) this.$dataLength)));
        float f2 = (((float) this.this$0.f3059C) * 1.0f) / ((float) this.this$0.mo7684G());
        if (java.lang.Math.abs(f2 - this.this$0.f3060D) > this.this$0.f3071O || f2 == 1.0f) {
            this.this$0.f3060D = f2;
            this.this$0.mo7541a(f2);
        }
    }
}
