package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.database.SdkDatabaseWrapper;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.obfuscated.d80;
import com.fossil.blesdk.obfuscated.f60;
import com.fossil.blesdk.obfuscated.f80;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h50;
import com.fossil.blesdk.obfuscated.h80;
import com.fossil.blesdk.obfuscated.j00;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GetFilePhase extends f60 {
    @DexIgnore
    public /* final */ FileHandle B;
    @DexIgnore
    public /* final */ ArrayList<byte[]> C;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<DeviceFile> D;
    @DexIgnore
    public /* final */ ArrayList<DeviceFile> E;
    @DexIgnore
    public long F;
    @DexIgnore
    public long G;
    @DexIgnore
    public float H;
    @DexIgnore
    public int I;
    @DexIgnore
    public long J;
    @DexIgnore
    public int K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ boolean N;
    @DexIgnore
    public /* final */ int O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ float Q;

    @DexIgnore
    public enum GetFileOption {
        SKIP_LIST,
        SKIP_ERASE,
        SKIP_ERASE_CACHE_AFTER_SUCCESS,
        NUMBER_OF_FILE_REQUIRED,
        ERASE_CACHE_FILE_BEFORE_GET;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ GetFilePhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, short s, HashMap hashMap, float f, String str, int i, fd4 fd4) {
        this(peripheral, aVar, r4, s, r6, r7, r8);
        String str2;
        PhaseId phaseId2 = (i & 4) != 0 ? PhaseId.GET_FILE : phaseId;
        HashMap hashMap2 = (i & 16) != 0 ? new HashMap() : hashMap;
        float f2 = (i & 32) != 0 ? 0.001f : f;
        if ((i & 64) != 0) {
            String uuid = UUID.randomUUID().toString();
            kd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    public final Request B() {
        DeviceFile H2 = H();
        if (H2 != null) {
            d80 d80 = new d80(H2.getFileHandle$blesdk_productionRelease(), j(), 0, 4, (fd4) null);
            d80.c((xc4<? super Request, qa4>) new GetFilePhase$buildEraseFileRequest$Anon1(this));
            return d80;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Request C() {
        DeviceFile H2 = H();
        if (H2 != null) {
            f80 f80 = new f80(H2.getFileHandle$blesdk_productionRelease(), this.J, 4294967295L, j(), 0, 16, (fd4) null);
            f80.a((xc4<? super Request, qa4>) new GetFilePhase$buildGetFileRequest$getFileRequest$Anon1(this));
            f80.a((yc4<? super Request, ? super Float, qa4>) new GetFilePhase$buildGetFileRequest$getFileRequest$Anon2(this));
            f80.a(h());
            return f80;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Request D() {
        h80 h80 = new h80(A(), j(), 0, 4, (fd4) null);
        h80.c((xc4<? super Request, qa4>) new GetFilePhase$buildListFileRequest$Anon1(this));
        return h80;
    }

    @DexIgnore
    public final void E() {
        DeviceFile H2 = H();
        if (H2 != null) {
            FileHandle fileHandle = new FileHandle(H2.getFileHandle$blesdk_productionRelease());
            DeviceFile a2 = a(fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease());
            t90 t90 = t90.c;
            String r = r();
            StringBuilder sb = new StringBuilder();
            sb.append("Find file in DB for macAddress=");
            sb.append(j().k());
            sb.append(", ");
            sb.append("fileType=");
            sb.append(fileHandle.getFileType$blesdk_productionRelease());
            sb.append(", ");
            sb.append("fileIndex=");
            sb.append(fileHandle.getFileIndex$blesdk_productionRelease());
            sb.append(", ");
            sb.append("result=");
            sb.append(a2 != null ? a2.toString() : null);
            sb.append('.');
            t90.a(r, sb.toString());
            DeviceFile H3 = H();
            if (H3 == null) {
                kd4.a();
                throw null;
            } else if (H3.getFileLength() <= 0) {
                K();
            } else if (a2 != null) {
                DeviceFile H4 = H();
                if (H4 != null) {
                    long fileLength = H4.getFileLength();
                    DeviceFile H5 = H();
                    if (H5 != null) {
                        d(DeviceFile.copy$default(a2, (String) null, (byte) 0, (byte) 0, (byte[]) null, fileLength, H5.getFileCrc(), 0, false, 207, (Object) null));
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                this.J = 0;
                I();
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void F() {
        SdkDatabaseWrapper.b.a(j().k(), this.B.getFileType$blesdk_productionRelease());
    }

    @DexIgnore
    public final void G() {
        Phase.a(this, RequestId.ERASE_FILE, (RequestId) null, 2, (Object) null);
    }

    @DexIgnore
    public final DeviceFile H() {
        return (DeviceFile) kb4.a(this.D, this.I);
    }

    @DexIgnore
    public final void I() {
        DeviceFile H2 = H();
        if (H2 == null) {
            kd4.a();
            throw null;
        } else if (H2.getFileLength() > 0) {
            a(RequestId.GET_FILE, (wc4<qa4>) new GetFilePhase$getFile$Anon1(this));
        } else if (this.M) {
            K();
        } else {
            G();
        }
    }

    @DexIgnore
    public final ArrayList<DeviceFile> J() {
        return this.E;
    }

    @DexIgnore
    public final void K() {
        this.I++;
        if (this.I < this.D.size()) {
            E();
        } else if (this.C.size() < this.O) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.NOT_ENOUGH_FILE_TO_PROCESS, (Request.Result) null, 5, (Object) null));
        } else {
            this.E.clear();
            this.E.addAll(SdkDatabaseWrapper.b.b(j().k(), this.B.getFileType$blesdk_productionRelease()));
            a(this.E);
        }
    }

    @DexIgnore
    public final boolean L() {
        return this.M;
    }

    @DexIgnore
    public final void M() {
        Phase.a(this, RequestId.LIST_FILE, (RequestId) null, 2, (Object) null);
    }

    @DexIgnore
    public final void N() {
        T t;
        for (DeviceFile deviceFile : SdkDatabaseWrapper.b.c(j().k(), this.B.getFileType$blesdk_productionRelease())) {
            Iterator<T> it = this.D.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                boolean z = false;
                if (((DeviceFile) t).getFileHandle$blesdk_productionRelease() == ByteBuffer.allocate(2).put(deviceFile.getFileType()).put(deviceFile.getFileIndex()).getShort(0)) {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null && Crc32Calculator.a.a(deviceFile.getRawData(), Crc32Calculator.CrcType.CRC32) != deviceFile.getFileCrc()) {
                SdkDatabaseWrapper.b.a(j().k(), deviceFile.getFileType(), deviceFile.getFileIndex());
            }
        }
    }

    @DexIgnore
    public void t() {
        if (this.P) {
            F();
        }
        if (!this.N) {
            d((xc4<? super Phase, qa4>) new GetFilePhase$onStart$Anon1(this));
        }
        if (this.L) {
            this.D.add(new DeviceFile(j().k(), A(), 4294967295L, 0));
            K();
            return;
        }
        M();
    }

    @DexIgnore
    public JSONObject u() {
        JSONObject put = super.u().put(GetFileOption.SKIP_LIST.getLogName$blesdk_productionRelease(), this.L).put(GetFileOption.SKIP_ERASE.getLogName$blesdk_productionRelease(), this.M).put(GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS.getLogName$blesdk_productionRelease(), this.N).put(GetFileOption.NUMBER_OF_FILE_REQUIRED.getLogName$blesdk_productionRelease(), this.O).put(GetFileOption.ERASE_CACHE_FILE_BEFORE_GET.getLogName$blesdk_productionRelease(), this.P);
        kd4.a((Object) put, "super.optionDescription(\u2026 eraseCacheFileBeforeGet)");
        return put;
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject put = super.x().put(GetFileOption.SKIP_ERASE.getLogName$blesdk_productionRelease(), this.M);
        kd4.a((Object) put, "super.resultDescription(\u2026ERASE.logName, skipErase)");
        JSONKey jSONKey = JSONKey.FILES;
        Object[] array = this.E.toArray(new DeviceFile[0]);
        if (array != null) {
            return wa0.a(put, jSONKey, j00.a((JSONAbleObject[]) array));
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void d(DeviceFile deviceFile) {
        if (deviceFile.getFileCrc() != Crc32Calculator.a.a(deviceFile.getRawData(), Crc32Calculator.CrcType.CRC32) || ((int) deviceFile.getFileLength()) != deviceFile.getRawData().length) {
            int i = this.K;
            if (i < 3) {
                this.K = i + 1;
                b(deviceFile);
                return;
            }
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INVALID_FILE_CRC, (Request.Result) null, 5, (Object) null));
        } else if (a(DeviceFile.copy$default(deviceFile, (String) null, (byte) 0, (byte) 0, (byte[]) null, 0, 0, 0, true, 127, (Object) null)) < 0) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.DATABASE_ERROR, (Request.Result) null, 5, (Object) null));
        } else {
            this.G += deviceFile.getFileLength();
            this.C.add(deviceFile.getRawData());
            c(deviceFile);
            if (this.M) {
                K();
            } else {
                G();
            }
        }
    }

    @DexIgnore
    public Object i() {
        Object[] array = this.E.toArray(new DeviceFile[0]);
        if (array != null) {
            return array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void b(DeviceFile deviceFile) {
        Phase.a((Phase) this, (Phase) new FindCorrectOffsetPhase(j(), e(), deviceFile, l()), (xc4) new GetFilePhase$findCorrectOffset$Anon1(this), (xc4) new GetFilePhase$findCorrectOffset$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public void c(DeviceFile deviceFile) {
        kd4.b(deviceFile, "deviceFile");
        t90 t90 = t90.c;
        String r = r();
        t90.a(r, "onFileRead: " + deviceFile.toJSONString(2));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, short s, HashMap<GetFileOption, Object> hashMap, float f, String str) {
        super(peripheral, aVar, phaseId, s, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(phaseId, "phaseId");
        kd4.b(hashMap, "options");
        kd4.b(str, "phaseUuid");
        this.Q = f;
        this.B = new FileHandle(s);
        this.C = new ArrayList<>();
        this.D = new CopyOnWriteArrayList<>();
        this.E = new ArrayList<>();
        this.I = -1;
        Boolean bool = (Boolean) hashMap.get(GetFileOption.SKIP_LIST);
        boolean z = false;
        this.L = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) hashMap.get(GetFileOption.SKIP_ERASE);
        this.M = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) hashMap.get(GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.N = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) hashMap.get(GetFileOption.NUMBER_OF_FILE_REQUIRED);
        this.O = num != null ? num.intValue() : 0;
        Boolean bool4 = (Boolean) hashMap.get(GetFileOption.ERASE_CACHE_FILE_BEFORE_GET);
        this.P = bool4 != null ? bool4.booleanValue() : z;
    }

    @DexIgnore
    public Request a(RequestId requestId) {
        kd4.b(requestId, "requestId");
        int i = h50.a[requestId.ordinal()];
        if (i == 1) {
            return D();
        }
        if (i == 2) {
            return C();
        }
        if (i != 3) {
            return null;
        }
        return B();
    }

    @DexIgnore
    public final void a(FindCorrectOffsetPhase findCorrectOffsetPhase) {
        this.J = findCorrectOffsetPhase.B();
        a(DeviceFile.copy$default(findCorrectOffsetPhase.C(), (String) null, (byte) 0, (byte) 0, ya4.a(findCorrectOffsetPhase.C().getRawData(), 0, (int) findCorrectOffsetPhase.B()), 0, 0, 0, false, 119, (Object) null));
        long j = this.F;
        float f = j > 0 ? (((float) (this.G + this.J)) * 1.0f) / ((float) j) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (Math.abs(f - this.H) > this.Q || f == 1.0f) {
            this.H = f;
            a(f);
        }
        I();
    }

    @DexIgnore
    public final long a(DeviceFile deviceFile) {
        SdkDatabaseWrapper.b.a(deviceFile.getDeviceMacAddress(), deviceFile.getFileType(), deviceFile.getFileIndex());
        if (!(deviceFile.getRawData().length == 0)) {
            return SdkDatabaseWrapper.b.a(deviceFile);
        }
        t90 t90 = t90.c;
        String r = r();
        t90.a(r, "addOrUpdateIncompleteFile: SKIP file data is empty, " + "macAddress=" + j().k() + ", " + "fileType=" + deviceFile.getFileType() + ", " + "fileIndex=" + deviceFile.getFileIndex());
        return -1;
    }

    @DexIgnore
    public final DeviceFile a(byte b, byte b2) {
        return (DeviceFile) kb4.a(SdkDatabaseWrapper.b.b(j().k(), b, b2), 0);
    }

    @DexIgnore
    public void a(ArrayList<DeviceFile> arrayList) {
        kd4.b(arrayList, "filesData");
        a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }
}
