package com.fossil.blesdk.device.logic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PhaseManager$stopAllRunningPhases$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.PhaseId[] $exceptions;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PhaseManager$stopAllRunningPhases$1(com.fossil.blesdk.device.logic.phase.PhaseId[] phaseIdArr) {
        super(1);
        this.$exceptions = phaseIdArr;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.fossil.blesdk.device.logic.phase.Phase) obj));
    }

    @DexIgnore
    public final boolean invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "phase");
        return !com.fossil.blesdk.obfuscated.za4.m31309b((T[]) this.$exceptions, phase.mo7562g());
    }
}
