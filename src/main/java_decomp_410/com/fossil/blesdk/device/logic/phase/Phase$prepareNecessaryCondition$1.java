package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$prepareNecessaryCondition$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$prepareNecessaryCondition$1(com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.this$0 = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        com.fossil.blesdk.device.logic.phase.AuthenticatePhase authenticatePhase = (com.fossil.blesdk.device.logic.phase.AuthenticatePhase) phase;
        com.fossil.blesdk.obfuscated.c90.f4020b.mo9436a(this.this$0.mo7564j().mo6457k(), authenticatePhase.mo7304B(), authenticatePhase.mo7303A());
        this.this$0.mo7309t();
    }
}
