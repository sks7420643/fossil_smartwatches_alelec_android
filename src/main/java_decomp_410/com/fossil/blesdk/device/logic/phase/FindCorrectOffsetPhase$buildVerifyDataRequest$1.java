package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindCorrectOffsetPhase$buildVerifyDataRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.FindCorrectOffsetPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FindCorrectOffsetPhase$buildVerifyDataRequest$1(com.fossil.blesdk.device.logic.phase.FindCorrectOffsetPhase findCorrectOffsetPhase) {
        super(1);
        this.this$0 = findCorrectOffsetPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        com.fossil.blesdk.obfuscated.k80 k80 = (com.fossil.blesdk.obfuscated.k80) request;
        this.this$0.mo7378a(k80.mo12640L(), k80.mo12639K(), k80.mo12638J());
    }
}
