package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransmitDataPhase$buildTransferDataRequest$transferDataRequest$Anon1 extends Lambda implements yc4<Request, Float, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ long $dataLength;
    @DexIgnore
    public /* final */ /* synthetic */ TransmitDataPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransmitDataPhase$buildTransferDataRequest$transferDataRequest$Anon1(TransmitDataPhase transmitDataPhase, long j) {
        super(2);
        this.this$Anon0 = transmitDataPhase;
        this.$dataLength = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Request) obj, ((Number) obj2).floatValue());
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request, float f) {
        kd4.b(request, "<anonymous parameter 0>");
        TransmitDataPhase transmitDataPhase = this.this$Anon0;
        transmitDataPhase.C = transmitDataPhase.F + ((long) (f * ((float) this.$dataLength)));
        float f2 = (((float) this.this$Anon0.C) * 1.0f) / ((float) this.this$Anon0.G());
        if (Math.abs(f2 - this.this$Anon0.D) > this.this$Anon0.O || f2 == 1.0f) {
            this.this$Anon0.D = f2;
            this.this$Anon0.a(f2);
        }
    }
}
