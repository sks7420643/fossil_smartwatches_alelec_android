package com.fossil.blesdk.device.logic.resource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ResourceType {
    DEVICE_INFORMATION,
    DEVICE_CONFIG,
    FILE_CONFIG,
    TRANSFER_DATA,
    AUTHENTICATION,
    ASYNC,
    UNKNOWN
}
