package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.a50;
import com.fossil.blesdk.obfuscated.a80;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetConnectionParamsPhase$setConnectionParameters$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SetConnectionParamsPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetConnectionParamsPhase$setConnectionParameters$Anon1(SetConnectionParamsPhase setConnectionParamsPhase) {
        super(1);
        this.this$Anon0 = setConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "it");
        this.this$Anon0.z = ((a80) request).I();
        ConnectionParameters B = this.this$Anon0.B();
        if (B == null) {
            SetConnectionParamsPhase setConnectionParamsPhase = this.this$Anon0;
            setConnectionParamsPhase.a(Phase.Result.copy$default(setConnectionParamsPhase.k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else if (a50.a.a(B, this.this$Anon0.B)) {
            SetConnectionParamsPhase setConnectionParamsPhase2 = this.this$Anon0;
            setConnectionParamsPhase2.a(Phase.Result.copy$default(setConnectionParamsPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            SetConnectionParamsPhase setConnectionParamsPhase3 = this.this$Anon0;
            setConnectionParamsPhase3.a(Phase.Result.copy$default(setConnectionParamsPhase3.k(), (PhaseId) null, Phase.Result.ResultCode.EXCHANGED_VALUE_NOT_SATISFIED, (Request.Result) null, 5, (Object) null));
        }
    }
}
