package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$verifyFile$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$verifyFile$2(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        if (request.mo7744n().getResponseStatus() != com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode.VERIFICATION_FAIL) {
            this.this$0.mo7547a(request.mo7744n());
        } else if (this.this$0.f2947I < 5) {
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase = this.this$0;
            legacyOtaPhase.f2947I = legacyOtaPhase.f2947I + 1;
            if (this.this$0.f2947I > 2) {
                this.this$0.mo7419v();
                this.this$0.mo7413F();
                return;
            }
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase2 = this.this$0;
            legacyOtaPhase2.f2942D = legacyOtaPhase2.f2939A;
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase3 = this.this$0;
            legacyOtaPhase3.f2943E = legacyOtaPhase3.f2939A - (this.this$0.f2939A % 6144);
            this.this$0.mo7416I();
        } else {
            this.this$0.mo7547a(request.mo7744n());
        }
    }
}
