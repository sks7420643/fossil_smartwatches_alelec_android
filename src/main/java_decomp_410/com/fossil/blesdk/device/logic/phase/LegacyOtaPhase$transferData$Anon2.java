package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$transferData$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $dataToTransfer;
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$transferData$Anon2(LegacyOtaPhase legacyOtaPhase, byte[] bArr) {
        super(1);
        this.this$Anon0 = legacyOtaPhase;
        this.$dataToTransfer = bArr;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "it");
        LegacyOtaPhase legacyOtaPhase = this.this$Anon0;
        legacyOtaPhase.G = legacyOtaPhase.G + ((long) this.$dataToTransfer.length);
        if (this.this$Anon0.G + this.this$Anon0.F >= this.this$Anon0.A) {
            this.this$Anon0.G = 0;
            this.this$Anon0.H();
            return;
        }
        this.this$Anon0.F();
    }
}
