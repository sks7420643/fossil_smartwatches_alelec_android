package com.fossil.blesdk.device.logic;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.za4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class PhaseManager$stopAllRunningPhases$Anon1 extends Lambda implements xc4<Phase, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ PhaseId[] $exceptions;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PhaseManager$stopAllRunningPhases$Anon1(PhaseId[] phaseIdArr) {
        super(1);
        this.$exceptions = phaseIdArr;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((Phase) obj));
    }

    @DexIgnore
    public final boolean invoke(Phase phase) {
        kd4.b(phase, "phase");
        return !za4.b((T[]) this.$exceptions, phase.g());
    }
}
