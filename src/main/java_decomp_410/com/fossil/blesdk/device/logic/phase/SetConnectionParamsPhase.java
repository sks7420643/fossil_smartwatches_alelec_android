package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.a80;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.u70;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetConnectionParamsPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));
    @DexIgnore
    public /* final */ ConnectionParametersSet B;
    @DexIgnore
    public ConnectionParameters z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetConnectionParamsPhase(Peripheral peripheral, Phase.a aVar, ConnectionParametersSet connectionParametersSet, String str) {
        super(peripheral, aVar, PhaseId.SET_CONNECTION_PARAMS, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(connectionParametersSet, "connectionParametersSet");
        kd4.b(str, "phaseUuid");
        this.B = connectionParametersSet;
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new u70(j()), (xc4) new SetConnectionParamsPhase$getConnectionParameters$Anon1(this), (xc4) new SetConnectionParamsPhase$getConnectionParameters$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final ConnectionParameters B() {
        return this.z;
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new a80(this.B, j()), (xc4) new SetConnectionParamsPhase$setConnectionParameters$Anon1(this), (xc4) new SetConnectionParamsPhase$setConnectionParameters$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.REQUESTED_CONNECTION_PARAMS, this.B.toJSONObject());
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.ACCEPTED_CONNECTION_PARAMS;
        ConnectionParameters connectionParameters = this.z;
        return wa0.a(x, jSONKey, connectionParameters != null ? connectionParameters.toJSONObject() : null);
    }

    @DexIgnore
    public ConnectionParameters i() {
        ConnectionParameters connectionParameters = this.z;
        return connectionParameters != null ? connectionParameters : new ConnectionParameters(0, 0, 0);
    }
}
