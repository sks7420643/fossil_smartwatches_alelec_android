package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$verifyFile$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$verifyFile$Anon2(LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$Anon0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        if (request.n().getResponseStatus() != LegacyFileControlStatusCode.VERIFICATION_FAIL) {
            this.this$Anon0.a(request.n());
        } else if (this.this$Anon0.I < 5) {
            LegacyOtaPhase legacyOtaPhase = this.this$Anon0;
            legacyOtaPhase.I = legacyOtaPhase.I + 1;
            if (this.this$Anon0.I > 2) {
                this.this$Anon0.v();
                this.this$Anon0.F();
                return;
            }
            LegacyOtaPhase legacyOtaPhase2 = this.this$Anon0;
            legacyOtaPhase2.D = legacyOtaPhase2.A;
            LegacyOtaPhase legacyOtaPhase3 = this.this$Anon0;
            legacyOtaPhase3.E = legacyOtaPhase3.A - (this.this$Anon0.A % 6144);
            this.this$Anon0.I();
        } else {
            this.this$Anon0.a(request.n());
        }
    }
}
