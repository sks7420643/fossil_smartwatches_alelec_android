package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.c60;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h70;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StreamingPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A; // = new ArrayList<>();
    @DexIgnore
    public xc4<? super AsyncEvent, qa4> z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StreamingPhase(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.STREAMING, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
    }

    @DexIgnore
    public final void A() {
        h70 h70 = new h70(j());
        h70.d(new StreamingPhase$startStreaming$streamingRequest$Anon1(this));
        Phase.a((Phase) this, (Request) h70, (xc4) new StreamingPhase$startStreaming$Anon1(this), (xc4) new StreamingPhase$startStreaming$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final StreamingPhase e(xc4<? super AsyncEvent, qa4> xc4) {
        kd4.b(xc4, "actionOnEventReceived");
        this.z = xc4;
        return this;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    public void a(Peripheral.State state) {
        kd4.b(state, "newState");
        int i = c60.a[state.ordinal()];
    }
}
