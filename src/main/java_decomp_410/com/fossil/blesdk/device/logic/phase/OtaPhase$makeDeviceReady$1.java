package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaPhase$makeDeviceReady$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.OtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OtaPhase$makeDeviceReady$1(com.fossil.blesdk.device.logic.phase.OtaPhase otaPhase) {
        super(1);
        this.this$0 = otaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "it");
        this.this$0.f2984R = java.lang.System.currentTimeMillis();
        this.this$0.f2987U = ((com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase) phase).mo7471H();
        com.fossil.blesdk.device.logic.phase.OtaPhase otaPhase = this.this$0;
        otaPhase.f2986T = otaPhase.mo7523O().getFirmwareVersion();
        com.fossil.blesdk.device.logic.phase.OtaPhase otaPhase2 = this.this$0;
        otaPhase2.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(otaPhase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
    }
}
