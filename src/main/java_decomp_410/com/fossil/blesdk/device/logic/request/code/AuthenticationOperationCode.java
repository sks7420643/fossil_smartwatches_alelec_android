package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.kd4;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AuthenticationOperationCode {
    SEND_PHONE_RANDOM_NUMBER(AuthenticationOperationId.SET, AuthenticationPackageTypeId.SEND_PHONE_RANDOM_NUMBER, (AuthenticationOperationId) null, (AuthenticationPackageTypeId) null, 12, (AuthenticationPackageTypeId) null),
    SEND_BOTH_SIDES_RANDOM_NUMBERS(AuthenticationOperationId.SET, AuthenticationPackageTypeId.SEND_BOTH_RANDOM_NUMBER, (AuthenticationOperationId) null, (AuthenticationPackageTypeId) null, 12, (AuthenticationPackageTypeId) null),
    EXCHANGE_PUBLIC_KEYS(AuthenticationOperationId.SET, AuthenticationPackageTypeId.EXCHANGE_PUBLIC_KEY, (AuthenticationOperationId) null, (AuthenticationPackageTypeId) null, 12, (AuthenticationPackageTypeId) null);
    
    @DexIgnore
    public /* final */ AuthenticationOperationId operationId;
    @DexIgnore
    public /* final */ AuthenticationPackageTypeId packageTypeId;
    @DexIgnore
    public /* final */ AuthenticationOperationId responseOperationId;
    @DexIgnore
    public /* final */ AuthenticationPackageTypeId responseParameterId;

    @DexIgnore
    public enum AuthenticationOperationId {
        GET((byte) 1),
        SET((byte) 2),
        RESPONSE((byte) 3);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        AuthenticationOperationId(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId() {
            return this.id;
        }
    }

    @DexIgnore
    public enum AuthenticationPackageTypeId {
        SEND_PHONE_RANDOM_NUMBER((byte) 1),
        SEND_BOTH_RANDOM_NUMBER((byte) 2),
        EXCHANGE_PUBLIC_KEY((byte) 3);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        AuthenticationPackageTypeId(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId() {
            return this.id;
        }
    }

    @DexIgnore
    AuthenticationOperationCode(AuthenticationOperationId authenticationOperationId, AuthenticationPackageTypeId authenticationPackageTypeId, AuthenticationOperationId authenticationOperationId2, AuthenticationPackageTypeId authenticationPackageTypeId2) {
        this.operationId = authenticationOperationId;
        this.packageTypeId = authenticationPackageTypeId;
        this.responseOperationId = authenticationOperationId2;
        this.responseParameterId = authenticationPackageTypeId2;
    }

    @DexIgnore
    public final byte[] getOperationCode() {
        byte[] array = ByteBuffer.allocate(2).put(this.operationId.getId()).put(this.packageTypeId.getId()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] getResponseOperationCode() {
        byte[] array = ByteBuffer.allocate(2).put(this.responseOperationId.getId()).put(this.responseParameterId.getId()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }
}
