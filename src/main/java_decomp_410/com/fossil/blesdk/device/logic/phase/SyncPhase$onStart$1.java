package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncPhase$onStart$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.SyncPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncPhase$onStart$1(com.fossil.blesdk.device.logic.phase.SyncPhase syncPhase) {
        super(1);
        this.this$0 = syncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        this.this$0.mo7388J().clear();
        this.this$0.mo7388J().addAll(((com.fossil.blesdk.device.logic.phase.LegacySyncPhase) phase).mo7446E());
        com.fossil.blesdk.device.logic.phase.SyncPhase syncPhase = this.this$0;
        syncPhase.mo7675b(syncPhase.mo7388J());
        com.fossil.blesdk.device.logic.phase.SyncPhase syncPhase2 = this.this$0;
        syncPhase2.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(syncPhase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
    }
}
