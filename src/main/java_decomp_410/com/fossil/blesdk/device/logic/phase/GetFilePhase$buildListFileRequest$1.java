package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildListFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.GetFilePhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildListFileRequest$1(com.fossil.blesdk.device.logic.phase.GetFilePhase getFilePhase) {
        super(1);
        this.this$0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        int i;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "listFileRequest");
        java.util.concurrent.CopyOnWriteArrayList b = this.this$0.f2924D;
        java.util.ArrayList<com.fossil.blesdk.database.entity.DeviceFile> N = ((com.fossil.blesdk.obfuscated.h80) request).mo11605N();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.util.Iterator<T> it = N.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((com.fossil.blesdk.database.entity.DeviceFile) next).getFileLength() > 0) {
                i = 1;
            }
            if (i != 0) {
                arrayList.add(next);
            }
        }
        b.addAll(arrayList);
        com.fossil.blesdk.device.logic.phase.GetFilePhase getFilePhase = this.this$0;
        for (com.fossil.blesdk.database.entity.DeviceFile fileLength : getFilePhase.f2924D) {
            i += (int) fileLength.getFileLength();
        }
        getFilePhase.f2926F = com.fossil.blesdk.obfuscated.n90.m11117b(i);
        this.this$0.mo7392N();
        this.this$0.mo7389K();
    }
}
