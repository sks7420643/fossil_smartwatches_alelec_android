package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$subscribeNextCharacteristic$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$subscribeNextCharacteristic$2$a")
    /* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$subscribeNextCharacteristic$2$a */
    public static final class C1242a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$subscribeNextCharacteristic$2 f2981e;

        @DexIgnore
        public C1242a(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$subscribeNextCharacteristic$2 makeDeviceReadyPhase$subscribeNextCharacteristic$2) {
            this.f2981e = makeDeviceReadyPhase$subscribeNextCharacteristic$2;
        }

        @DexIgnore
        public final void run() {
            this.f2981e.this$0.mo7468E();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$subscribeNextCharacteristic$2(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        if (this.this$0.f2970D < 2) {
            this.this$0.mo7555b((com.fossil.blesdk.device.logic.request.Request) null);
            this.this$0.mo7561f().postDelayed(new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$subscribeNextCharacteristic$2.C1242a(this), com.misfit.frameworks.buttonservice.ButtonService.CONNECT_TIMEOUT);
            return;
        }
        com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$0;
        makeDeviceReadyPhase.mo7483c(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(makeDeviceReadyPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.Companion.mo7587a(request.mo7744n()), request.mo7744n(), 1, (java.lang.Object) null));
    }
}
