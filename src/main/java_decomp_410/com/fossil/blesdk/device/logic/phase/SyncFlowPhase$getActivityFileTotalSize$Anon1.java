package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.h80;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncFlowPhase$getActivityFileTotalSize$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SyncFlowPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase$getActivityFileTotalSize$Anon1(SyncFlowPhase syncFlowPhase) {
        super(1);
        this.this$Anon0 = syncFlowPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        SyncFlowPhase syncFlowPhase = this.this$Anon0;
        int i = 0;
        for (DeviceFile fileLength : ((h80) request).N()) {
            i += (int) fileLength.getFileLength();
        }
        syncFlowPhase.C = n90.b(i);
        this.this$Anon0.C();
    }
}
