package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$getActivityFileRequest$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.request.Request, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacySyncPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$getActivityFileRequest$3(com.fossil.blesdk.device.logic.phase.LegacySyncPhase legacySyncPhase) {
        super(2);
        this.this$0 = legacySyncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "<anonymous parameter 0>");
        int i = 0;
        for (com.fossil.blesdk.database.entity.DeviceFile fileLength : this.this$0.mo7446E()) {
            i += (int) fileLength.getFileLength();
        }
        float k = (((float) i) + (f * ((float) this.this$0.f2955B))) / ((float) this.this$0.f2955B);
        if (k - this.this$0.f2960G > this.this$0.f2964K || k == 1.0f) {
            this.this$0.f2960G = k;
            this.this$0.mo7541a(k);
        }
    }
}
