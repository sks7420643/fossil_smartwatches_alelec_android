package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.h80;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$buildListFileRequest$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ GetFilePhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$buildListFileRequest$Anon1(GetFilePhase getFilePhase) {
        super(1);
        this.this$Anon0 = getFilePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        int i;
        kd4.b(request, "listFileRequest");
        CopyOnWriteArrayList b = this.this$Anon0.D;
        ArrayList<DeviceFile> N = ((h80) request).N();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = N.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (((DeviceFile) next).getFileLength() > 0) {
                i = 1;
            }
            if (i != 0) {
                arrayList.add(next);
            }
        }
        b.addAll(arrayList);
        GetFilePhase getFilePhase = this.this$Anon0;
        for (DeviceFile fileLength : getFilePhase.D) {
            i += (int) fileLength.getFileLength();
        }
        getFilePhase.F = n90.b(i);
        this.this$Anon0.N();
        this.this$Anon0.K();
    }
}
