package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.c90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$prepareNecessaryCondition$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$prepareNecessaryCondition$Anon1(Phase phase) {
        super(1);
        this.this$Anon0 = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "executedPhase");
        AuthenticatePhase authenticatePhase = (AuthenticatePhase) phase;
        c90.b.a(this.this$Anon0.j().k(), authenticatePhase.B(), authenticatePhase.A());
        this.this$Anon0.t();
    }
}
