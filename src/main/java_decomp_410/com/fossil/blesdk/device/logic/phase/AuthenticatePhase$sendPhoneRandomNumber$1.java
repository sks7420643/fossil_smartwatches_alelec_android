package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AuthenticatePhase$sendPhoneRandomNumber$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.AuthenticatePhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AuthenticatePhase$sendPhoneRandomNumber$1(com.fossil.blesdk.device.logic.phase.AuthenticatePhase authenticatePhase) {
        super(1);
        this.this$0 = authenticatePhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        com.fossil.blesdk.device.logic.phase.AuthenticatePhase authenticatePhase = this.this$0;
        authenticatePhase.mo7554b(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(authenticatePhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, (com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode) null, request.mo7744n(), 3, (java.lang.Object) null));
        this.this$0.mo7307b(((com.fossil.blesdk.obfuscated.m70) request).mo13528I());
    }
}
