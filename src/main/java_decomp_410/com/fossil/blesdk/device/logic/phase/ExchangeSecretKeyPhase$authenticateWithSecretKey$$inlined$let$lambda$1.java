package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExchangeSecretKeyPhase$authenticateWithSecretKey$$inlined$let$lambda$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.ExchangeSecretKeyPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExchangeSecretKeyPhase$authenticateWithSecretKey$$inlined$let$lambda$1(com.fossil.blesdk.device.logic.phase.ExchangeSecretKeyPhase exchangeSecretKeyPhase) {
        super(1);
        this.this$0 = exchangeSecretKeyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.b(phase, "executedPhase");
        this.this$0.a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(phase.k(), this.this$0.k().getPhaseId(), (com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode) null, (com.fossil.blesdk.device.logic.request.Request.Result) null, 6, (java.lang.Object) null));
    }
}
