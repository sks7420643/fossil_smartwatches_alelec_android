package com.fossil.blesdk.device.logic.phase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.a90;
import com.fossil.blesdk.obfuscated.bb4;
import com.fossil.blesdk.obfuscated.c70;
import com.fossil.blesdk.obfuscated.c80;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.i70;
import com.fossil.blesdk.obfuscated.k50;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m60;
import com.fossil.blesdk.obfuscated.q60;
import com.fossil.blesdk.obfuscated.r60;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.w70;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.z70;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase extends Phase {
    @DexIgnore
    public static /* final */ ArrayList<GattCharacteristic.CharacteristicId> M; // = cb4.a((T[]) new GattCharacteristic.CharacteristicId[]{GattCharacteristic.CharacteristicId.DC, GattCharacteristic.CharacteristicId.FTC, GattCharacteristic.CharacteristicId.FTD, GattCharacteristic.CharacteristicId.AUTHENTICATION, GattCharacteristic.CharacteristicId.ASYNC, GattCharacteristic.CharacteristicId.FTD_1});
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ List<UUID> E;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<GattCharacteristic.CharacteristicId> F;
    @DexIgnore
    public int G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public /* final */ HashMap<MakeDeviceReadyOption, Object> L;
    @DexIgnore
    public DeviceInformation z;

    @DexIgnore
    public enum MakeDeviceReadyOption {
        CONNECTION_TIME_OUT,
        AUTO_CONNECT;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ MakeDeviceReadyPhase(Peripheral peripheral, Phase.a aVar, HashMap hashMap, String str, int i, fd4 fd4) {
        this(peripheral, aVar, hashMap, str);
        hashMap = (i & 4) != 0 ? new HashMap() : hashMap;
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new c80(FileType.ALL_FILE.getFileHandleMask$blesdk_productionRelease(), j(), 0, 4, (fd4) null), (xc4) MakeDeviceReadyPhase$abortAllFiles$Anon1.INSTANCE, (xc4) MakeDeviceReadyPhase$abortAllFiles$Anon2.INSTANCE, (yc4) null, (xc4) new MakeDeviceReadyPhase$abortAllFiles$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void B() {
        a90.b bVar = new a90.b();
        bVar.b(ResourceType.DEVICE_INFORMATION, Integer.MAX_VALUE);
        for (GattCharacteristic.CharacteristicId characteristicId : this.F) {
            bVar.a(characteristicId.getResourceType$blesdk_productionRelease(), characteristicId.getResourceTypeQuotaWeight$blesdk_productionRelease());
        }
        e().a().a(bVar.a());
    }

    @DexIgnore
    public final void C() {
        int i = k50.b[this.z.getBondRequired$blesdk_productionRelease().ordinal()];
        if (i == 1) {
            G();
        } else if (i == 2) {
            Phase.a((Phase) this, (Phase) new CreateBondPhase(j(), e(), l()), (xc4) new MakeDeviceReadyPhase$createBond$Anon1(this), (xc4) new MakeDeviceReadyPhase$createBond$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
        }
    }

    @DexIgnore
    public final void D() {
        Phase.a((Phase) this, (Request) new q60(j()), (xc4) MakeDeviceReadyPhase$disconnect$Anon1.INSTANCE, (xc4) MakeDeviceReadyPhase$disconnect$Anon2.INSTANCE, (yc4) null, (xc4) new MakeDeviceReadyPhase$disconnect$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void E() {
        this.D++;
        Phase.a((Phase) this, (Request) new r60(j()), (xc4) new MakeDeviceReadyPhase$discoverService$Anon1(this), (xc4) new MakeDeviceReadyPhase$discoverService$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final boolean F() {
        if (!this.J) {
            Boolean bool = (Boolean) this.L.get(MakeDeviceReadyOption.AUTO_CONNECT);
            return bool != null ? bool.booleanValue() : false;
        }
    }

    @DexIgnore
    public final void G() {
        Phase.a((Phase) this, (Request) new w70(j()), (xc4) new MakeDeviceReadyPhase$getOptimalPayload$Anon1(this), (xc4) MakeDeviceReadyPhase$getOptimalPayload$Anon2.INSTANCE, (yc4) null, (xc4) new MakeDeviceReadyPhase$getOptimalPayload$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final DeviceInformation H() {
        return this.z;
    }

    @DexIgnore
    public final void I() {
        Phase.a((Phase) this, (Phase) new FetchDeviceInformationPhase(j(), e(), l()), (xc4) new MakeDeviceReadyPhase$readDeviceInformation$Anon1(this), (xc4) new MakeDeviceReadyPhase$readDeviceInformation$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void J() {
        Phase.a((Phase) this, (Request) new z70(j()), (xc4) MakeDeviceReadyPhase$requestDiscoverService$Anon1.INSTANCE, (xc4) MakeDeviceReadyPhase$requestDiscoverService$Anon2.INSTANCE, (yc4) null, (xc4) new MakeDeviceReadyPhase$requestDiscoverService$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void K() {
        Phase.a((Phase) this, (Request) new c70(RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN, j()), (xc4) new MakeDeviceReadyPhase$requestMtu$Anon1(this), (xc4) new MakeDeviceReadyPhase$requestMtu$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void L() {
        t90.c.a(r(), "subscribeNextCharacteristic");
        if (this.G >= this.F.size()) {
            B();
            K();
            return;
        }
        GattCharacteristic.CharacteristicId characteristicId = this.F.get(this.G);
        kd4.a((Object) characteristicId, "characteristicsToSubscri\u2026ibingCharacteristicIndex]");
        Phase.a((Phase) this, (Request) new i70(characteristicId, true, j()), (xc4) new MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon1(this), (xc4) new MakeDeviceReadyPhase$subscribeNextCharacteristic$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void M() {
        boolean F2 = F();
        long a2 = a(F2);
        if (a2 < 0) {
            a(k());
            return;
        }
        this.H = System.currentTimeMillis();
        m60 m60 = new m60(j(), F2, a2);
        m60.b(this.K);
        this.K = false;
        Phase.a((Phase) this, (Request) m60, (xc4) new MakeDeviceReadyPhase$tryConnect$Anon1(this), (xc4) new MakeDeviceReadyPhase$tryConnect$Anon2(this), (yc4) null, (xc4) new MakeDeviceReadyPhase$tryConnect$Anon3(this), (xc4) MakeDeviceReadyPhase$tryConnect$Anon4.INSTANCE, 8, (Object) null);
    }

    @DexIgnore
    public final void N() {
        Phase.a((Phase) this, (Phase) new TrySetConnectionParamsPhase(j(), e(), l(), y00.f.a(this.z)), (xc4) MakeDeviceReadyPhase$trySetConnectionParams$Anon1.INSTANCE, (xc4) MakeDeviceReadyPhase$trySetConnectionParams$Anon2.INSTANCE, (yc4) null, (xc4) new MakeDeviceReadyPhase$trySetConnectionParams$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public void t() {
        this.B = System.currentTimeMillis();
        this.K = true;
        M();
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(wa0.a(super.x(), JSONKey.DEVICE_INFO, this.z.toJSONObject()), JSONKey.CONNECT_DURATION, Long.valueOf(this.C));
    }

    @DexIgnore
    public final void c(Phase.Result result) {
        if (result.getResultCode() == Phase.Result.ResultCode.SUCCESS) {
            a(result);
            return;
        }
        b(result);
        D();
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MakeDeviceReadyPhase(Peripheral peripheral, Phase.a aVar, HashMap<MakeDeviceReadyOption, Object> hashMap, String str) {
        super(r1, r2, PhaseId.MAKE_DEVICE_READY, r4);
        Peripheral peripheral2 = peripheral;
        Phase.a aVar2 = aVar;
        HashMap<MakeDeviceReadyOption, Object> hashMap2 = hashMap;
        String str2 = str;
        kd4.b(peripheral2, "peripheral");
        kd4.b(aVar2, "delegate");
        kd4.b(hashMap2, "options");
        kd4.b(str2, "phaseUuid");
        this.L = hashMap2;
        this.z = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (fd4) null);
        this.A = new ArrayList<>();
        this.B = System.currentTimeMillis();
        this.E = bb4.a(ua0.y.u());
        cb4.c(GattCharacteristic.CharacteristicId.MODEL_NUMBER, GattCharacteristic.CharacteristicId.SERIAL_NUMBER, GattCharacteristic.CharacteristicId.FIRMWARE_VERSION, GattCharacteristic.CharacteristicId.DC, GattCharacteristic.CharacteristicId.FTC, GattCharacteristic.CharacteristicId.FTD, GattCharacteristic.CharacteristicId.AUTHENTICATION, GattCharacteristic.CharacteristicId.ASYNC);
        this.F = new CopyOnWriteArrayList<>();
    }

    @DexIgnore
    public DeviceInformation i() {
        return this.z;
    }

    @DexIgnore
    public boolean a(Phase phase) {
        kd4.b(phase, "otherPhase");
        return b(phase) || phase.n().isEmpty();
    }

    @DexIgnore
    public void a(Peripheral.State state) {
        kd4.b(state, "newState");
        if (k50.a[state.ordinal()] == 1) {
            Request d = d();
            if (d == null || d.r()) {
                Phase q = q();
                if (q == null || q.s()) {
                    a(Phase.Result.ResultCode.CONNECTION_DROPPED);
                }
            }
        }
    }

    @DexIgnore
    public final long a(boolean z2) {
        Long l = (Long) this.L.get(MakeDeviceReadyOption.CONNECTION_TIME_OUT);
        long longValue = l != null ? l.longValue() : 30000;
        long b = y00.f.b(z2);
        return longValue == 0 ? b : Math.min(longValue - (System.currentTimeMillis() - this.B), b);
    }

    @DexIgnore
    public final void a(List<? extends GattCharacteristic.CharacteristicId> list) {
        this.F.clear();
        CopyOnWriteArrayList<GattCharacteristic.CharacteristicId> copyOnWriteArrayList = this.F;
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (M.contains((GattCharacteristic.CharacteristicId) next)) {
                arrayList.add(next);
            }
        }
        copyOnWriteArrayList.addAll(arrayList);
    }

    @DexIgnore
    public boolean a(Request request) {
        RequestId f = request != null ? request.f() : null;
        return f != null && k50.c[f.ordinal()] == 1;
    }
}
