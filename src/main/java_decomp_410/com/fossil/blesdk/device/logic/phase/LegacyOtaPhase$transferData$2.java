package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$transferData$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $dataToTransfer;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$transferData$2(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase, byte[] bArr) {
        super(1);
        this.this$0 = legacyOtaPhase;
        this.$dataToTransfer = bArr;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase = this.this$0;
        legacyOtaPhase.f2945G = legacyOtaPhase.f2945G + ((long) this.$dataToTransfer.length);
        if (this.this$0.f2945G + this.this$0.f2944F >= this.this$0.f2939A) {
            this.this$0.f2945G = 0;
            this.this$0.mo7415H();
            return;
        }
        this.this$0.mo7413F();
    }
}
