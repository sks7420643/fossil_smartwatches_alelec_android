package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrySetConnectionParamsPhase$trySetConnectionParameters$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrySetConnectionParamsPhase$trySetConnectionParameters$1(com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase) {
        super(1);
        this.this$0 = trySetConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        this.this$0.f3072A = ((com.fossil.blesdk.obfuscated.a80) request).mo8600I();
        com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters A = this.this$0.mo7700A();
        if (A == null) {
            com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase = this.this$0;
            trySetConnectionParamsPhase.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(trySetConnectionParamsPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.FLOW_BROKEN, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        } else if (com.fossil.blesdk.obfuscated.a50.f3362a.mo8540a(A, this.this$0.f3074C[this.this$0.f3075z])) {
            com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase2 = this.this$0;
            trySetConnectionParamsPhase2.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(trySetConnectionParamsPhase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        } else {
            com.fossil.blesdk.device.logic.phase.TrySetConnectionParamsPhase trySetConnectionParamsPhase3 = this.this$0;
            trySetConnectionParamsPhase3.f3075z = trySetConnectionParamsPhase3.f3075z + 1;
            this.this$0.mo7701B();
        }
    }
}
