package com.fossil.blesdk.device.logic.request;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransferDataRequest$actionOnEOFTimeout$Anon1 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Peripheral $peripheral;
    @DexIgnore
    public /* final */ /* synthetic */ TransferDataRequest this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransferDataRequest$actionOnEOFTimeout$Anon1(TransferDataRequest transferDataRequest, Peripheral peripheral) {
        super(0);
        this.this$Anon0 = transferDataRequest;
        this.$peripheral = peripheral;
    }

    @DexIgnore
    public final void invoke() {
        Peripheral.a(this.$peripheral, LogLevel.DEBUG, this.this$Anon0.q(), "Request EOF timeout.", false, 8, (Object) null);
        TransferDataRequest transferDataRequest = this.this$Anon0;
        transferDataRequest.a(Request.Result.copy$default(transferDataRequest.n(), (RequestId) null, Request.Result.ResultCode.EOF_TIME_OUT, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
    }
}
