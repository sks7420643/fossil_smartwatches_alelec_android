package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StreamingPhase$startStreaming$streamingRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.asyncevent.AsyncEvent, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.StreamingPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StreamingPhase$startStreaming$streamingRequest$1(com.fossil.blesdk.device.logic.phase.StreamingPhase streamingPhase) {
        super(1);
        this.this$0 = streamingPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.asyncevent.AsyncEvent) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.asyncevent.AsyncEvent asyncEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(asyncEvent, "asyncEvent");
        com.fossil.blesdk.obfuscated.xc4 a = this.this$0.f3038z;
        if (a != null) {
            com.fossil.blesdk.obfuscated.qa4 qa4 = (com.fossil.blesdk.obfuscated.qa4) a.invoke(asyncEvent);
        }
    }
}
