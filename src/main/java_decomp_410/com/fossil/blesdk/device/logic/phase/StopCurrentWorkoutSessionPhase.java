package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.b80;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import java.util.ArrayList;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StopCurrentWorkoutSessionPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ StopCurrentWorkoutSessionPhase(Peripheral peripheral, Phase.a aVar, String str, int i, fd4 fd4) {
        this(peripheral, aVar, str);
        if ((i & 4) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new b80(j()), (xc4) StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon1.INSTANCE, (xc4) StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon2.INSTANCE, (yc4) null, (xc4) new StopCurrentWorkoutSessionPhase$stopCurrentWorkoutSession$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        A();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StopCurrentWorkoutSessionPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(peripheral, aVar, PhaseId.STOP_CURRENT_WORKOUT_SESSION, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(str, "phaseUuid");
        this.z = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));
    }
}
