package com.fossil.blesdk.device.logic.request;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Request$actionOnRequestTimeout$Anon1 extends Lambda implements wc4<qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Request this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Request$actionOnRequestTimeout$Anon1(Request request) {
        super(0);
        this.this$Anon0 = request;
    }

    @DexIgnore
    public final void invoke() {
        Peripheral.a(this.this$Anon0.i(), LogLevel.DEBUG, this.this$Anon0.q(), "Request timeout", false, 8, (Object) null);
        this.this$Anon0.a(Request.Result.ResultCode.TIMEOUT);
    }
}
