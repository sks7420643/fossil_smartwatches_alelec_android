package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VerifySecretKeyPhase$sendPhoneRandomNumber$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.VerifySecretKeyPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifySecretKeyPhase$sendPhoneRandomNumber$2(com.fossil.blesdk.device.logic.phase.VerifySecretKeyPhase verifySecretKeyPhase) {
        super(1);
        this.this$0 = verifySecretKeyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode resultCode;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        if (request.mo7744n().getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.RESPONSE_ERROR) {
            resultCode = com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS;
        } else {
            resultCode = com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.Companion.mo7587a(request.mo7744n());
        }
        com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode resultCode2 = resultCode;
        com.fossil.blesdk.device.logic.phase.VerifySecretKeyPhase verifySecretKeyPhase = this.this$0;
        verifySecretKeyPhase.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(verifySecretKeyPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, resultCode2, request.mo7744n(), 1, (java.lang.Object) null));
    }
}
