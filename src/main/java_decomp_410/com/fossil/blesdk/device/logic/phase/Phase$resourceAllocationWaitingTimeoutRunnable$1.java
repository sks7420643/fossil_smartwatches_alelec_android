package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$resourceAllocationWaitingTimeoutRunnable$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$resourceAllocationWaitingTimeoutRunnable$1(com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(0);
        this.this$0 = phase;
    }

    @DexIgnore
    public final void invoke() {
        this.this$0.mo7544a(com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.WAITING_FOR_EXECUTION_TIMEOUT);
    }
}
