package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import java.util.UUID;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredServiceUuidString$Anon1 extends Lambda implements xc4<UUID, String> {
    @DexIgnore
    public static /* final */ MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredServiceUuidString$Anon1 INSTANCE; // = new MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredServiceUuidString$Anon1();

    @DexIgnore
    public MakeDeviceReadyPhase$discoverService$Anon1$resultCode$discoveredServiceUuidString$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(UUID uuid) {
        kd4.b(uuid, "it");
        String uuid2 = uuid.toString();
        kd4.a((Object) uuid2, "it.toString()");
        return uuid2;
    }
}
