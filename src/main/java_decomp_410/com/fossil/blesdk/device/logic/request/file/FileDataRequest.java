package com.fossil.blesdk.device.logic.request.file;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.obfuscated.c20;
import com.fossil.blesdk.obfuscated.c90;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.e80;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class FileDataRequest extends e80 {
    @DexIgnore
    public long M;
    @DexIgnore
    public byte[] N; // = new byte[0];
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public int R; // = -1;
    @DexIgnore
    public float S;
    @DexIgnore
    public GattCharacteristic.CharacteristicId T;
    @DexIgnore
    public long U;
    @DexIgnore
    public /* final */ Request.ResponseInfo V;
    @DexIgnore
    public /* final */ wc4<qa4> W;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileDataRequest(FileControlOperationCode fileControlOperationCode, short s, RequestId requestId, Peripheral peripheral, int i) {
        super(fileControlOperationCode, s, requestId, peripheral, i);
        kd4.b(fileControlOperationCode, "fileOperation");
        kd4.b(requestId, "requestId");
        kd4.b(peripheral, "peripheral");
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.UNKNOWN;
        this.T = characteristicId;
        this.V = new Request.ResponseInfo(0, characteristicId, new byte[0], (JSONObject) null, 9, (fd4) null);
        this.W = new FileDataRequest$actionOnEOFTimeout$Anon1(this, peripheral);
    }

    @DexIgnore
    public long J() {
        return this.P;
    }

    @DexIgnore
    public long K() {
        return this.O;
    }

    @DexIgnore
    public byte[] L() {
        return this.N;
    }

    @DexIgnore
    public void M() {
    }

    @DexIgnore
    public void b(long j) {
        this.P = j;
    }

    @DexIgnore
    public void c(long j) {
        this.O = j;
    }

    @DexIgnore
    public final JSONObject d(byte[] bArr) {
        GattCharacteristic.CharacteristicId characteristicId;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            this.M = n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            wa0.a(jSONObject, JSONKey.DATA_SIZE, Long.valueOf(this.M));
            if (this.M == 0) {
                a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
                return jSONObject;
            }
            if (bArr.length >= 5) {
                characteristicId = GattCharacteristic.CharacteristicId.Companion.a(bArr[4]);
            } else {
                characteristicId = GattCharacteristic.CharacteristicId.FTD;
            }
            this.T = characteristicId;
            wa0.a(jSONObject, JSONKey.SOCKET_ID, this.T.getLogName$blesdk_productionRelease());
            this.V.setCharacteristicId(this.T);
            if (g()) {
                c90.b.a(i().k()).c(this.T);
            }
        } else {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_LENGTH, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        return jSONObject;
    }

    @DexIgnore
    public final boolean d(byte b) {
        return ((byte) (b & ((byte) 128))) != ((byte) 0);
    }

    @DexIgnore
    public final void e(byte[] bArr) {
        f(k90.a(L(), bArr));
        float length = (((float) L().length) * 1.0f) / ((float) this.M);
        if (length - this.S > 0.001f || length == 1.0f) {
            this.S = length;
            a(length);
        }
    }

    @DexIgnore
    public void f(byte[] bArr) {
        kd4.b(bArr, "<set-?>");
        this.N = bArr;
    }

    @DexIgnore
    public final void g(c20 c20) {
        byte[] bArr;
        kd4.b(c20, "characteristicChangedNotification");
        if (g()) {
            bArr = d90.b.a(i().k(), this.T, c20.b());
        } else {
            bArr = c20.b();
        }
        if (c(bArr[0]) == (this.R + 1) % 64) {
            if (d(bArr[0])) {
                a(5000);
                a(this.W);
            } else {
                a(d());
            }
            this.R++;
            e(ya4.a(bArr, 1, bArr.length));
            if (this.U == 0) {
                a(this.V);
            }
            this.U += (long) bArr.length;
            wa0.a(wa0.a(wa0.a(this.V.getExtraInfo(), JSONKey.PACKAGE_COUNT, Integer.valueOf(this.R + 1)), JSONKey.TRANSFERRED_DATA_SIZE, Long.valueOf(this.U)), JSONKey.PROCESSED_DATA_LENGTH, Integer.valueOf(L().length));
            return;
        }
        b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.MISS_PACKAGE, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        c(true);
    }

    @DexIgnore
    public final JSONObject c(byte[] bArr) {
        x();
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        c(n90.b(order.getInt(0)));
        b(n90.b(order.getInt(4)));
        wa0.a(wa0.a(jSONObject, JSONKey.FILE_SIZE, Long.valueOf(K())), JSONKey.FILE_CRC, Long.valueOf(J()));
        M();
        return jSONObject;
    }

    @DexIgnore
    public final JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        boolean z = true;
        if (!this.Q) {
            JSONObject d = d(bArr);
            this.Q = true;
            if (n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
                z = false;
            }
            c(z);
            byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(FileControlOperationCode.EOF_REACH.responseCode()).putShort(I()).array();
            kd4.a((Object) array, "ByteBuffer.allocate(1 + \u2026                 .array()");
            b(array);
            return d;
        }
        JSONObject c = c(bArr);
        c(true);
        return c;
    }

    @DexIgnore
    public final boolean c(c20 c20) {
        kd4.b(c20, "characteristicChangeNotification");
        return c20.a() == this.T;
    }

    @DexIgnore
    public final int c(byte b) {
        return n90.b(n90.b((byte) (b & 63)));
    }
}
