package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$eraseSegment$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$eraseSegment$1(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        this.this$0.f2942D = ((com.fossil.blesdk.obfuscated.p80) request).mo14647J();
        if (this.this$0.f2944F >= this.this$0.f2942D) {
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase = this.this$0;
            legacyOtaPhase.f2944F = legacyOtaPhase.f2942D;
            this.this$0.mo7413F();
            return;
        }
        this.this$0.mo7409B();
    }
}
