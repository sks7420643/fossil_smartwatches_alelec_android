package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaPhase$makeDeviceReady$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ OtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public OtaPhase$makeDeviceReady$Anon1(OtaPhase otaPhase) {
        super(1);
        this.this$Anon0 = otaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "it");
        this.this$Anon0.R = System.currentTimeMillis();
        this.this$Anon0.U = ((MakeDeviceReadyPhase) phase).H();
        OtaPhase otaPhase = this.this$Anon0;
        otaPhase.T = otaPhase.O().getFirmwareVersion();
        OtaPhase otaPhase2 = this.this$Anon0;
        otaPhase2.a(Phase.Result.copy$default(otaPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }
}
