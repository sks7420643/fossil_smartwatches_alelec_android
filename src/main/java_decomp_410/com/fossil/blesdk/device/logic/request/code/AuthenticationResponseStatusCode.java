package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o70;
import java.util.Locale;
import kotlin.TypeCastException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum AuthenticationResponseStatusCode implements o70 {
    SUCCESS((byte) 0),
    WRONG_LENGTH((byte) 1),
    FAIL_TO_GENERATE_RANDOM_NUMBER((byte) 2),
    FAIL_TO_GET_KEY((byte) 3),
    FAIL_TO_ENCRYPT_FRAME((byte) 4),
    FAIL_TO_DECRYPT_FRAME((byte) 5),
    INVALID_KEY_TYPE((byte) 6),
    WRONG_CONFIRM_RANDOM_NUMBER((byte) 7),
    WRONG_STATE((byte) 8),
    FAIL_TO_STORE_KEY((byte) 9),
    WRONG_KEY_OPERATION((byte) 10),
    NOT_SUPPORTED((byte) 11),
    FAIL_TO_GENERATE_PUBLIC_KEY((byte) 12),
    FAIL_TO_GENERATE_SECRET_KEY(DateTimeFieldType.HALFDAY_OF_DAY),
    FAIL_BECAUSE_OF_INVALID_INPUT(DateTimeFieldType.HOUR_OF_HALFDAY),
    FAIL_BECAUSE_OF_NOT_SET_KEY(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    FAIL_TO_SEND_REQUEST_AUTHENTICATE_THROUGH_ASYNC(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    UNKNOWN((byte) 255);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte code;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final AuthenticationResponseStatusCode a(byte b) {
            AuthenticationResponseStatusCode authenticationResponseStatusCode;
            AuthenticationResponseStatusCode[] values = AuthenticationResponseStatusCode.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    authenticationResponseStatusCode = null;
                    break;
                }
                authenticationResponseStatusCode = values[i];
                if (authenticationResponseStatusCode.getCode() == b) {
                    break;
                }
                i++;
            }
            return authenticationResponseStatusCode != null ? authenticationResponseStatusCode : AuthenticationResponseStatusCode.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    AuthenticationResponseStatusCode(byte b) {
        this.code = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public byte getCode() {
        return this.code;
    }

    @DexIgnore
    public String getLogName() {
        return this.logName;
    }

    @DexIgnore
    public boolean isSuccessCode() {
        return this == SUCCESS;
    }
}
