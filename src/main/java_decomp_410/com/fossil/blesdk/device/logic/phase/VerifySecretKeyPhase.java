package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m70;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.fossil.blesdk.utils.EncryptionAES128;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VerifySecretKeyPhase extends Phase {
    @DexIgnore
    public Boolean A;
    @DexIgnore
    public /* final */ byte[] B; // = new byte[8];
    @DexIgnore
    public /* final */ byte[] C;
    @DexIgnore
    public /* final */ byte[] D;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.AUTHENTICATION}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VerifySecretKeyPhase(Peripheral peripheral, Phase.a aVar, byte[] bArr) {
        super(peripheral, aVar, PhaseId.VERIFY_SECRET_KEY, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(bArr, "secretKey");
        this.D = bArr;
        byte[] copyOf = Arrays.copyOf(this.D, 16);
        kd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
        this.C = copyOf;
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new m70(j(), ua0.y.e(), this.B), (xc4) new VerifySecretKeyPhase$sendPhoneRandomNumber$Anon1(this), (xc4) new VerifySecretKeyPhase$sendPhoneRandomNumber$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        if (this.D.length < 16) {
            a(Phase.Result.ResultCode.INVALID_PARAMETER);
            return;
        }
        new SecureRandom().nextBytes(this.B);
        A();
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.SECRET_KEY_CRC, Long.valueOf(Crc32Calculator.a.a(this.D, Crc32Calculator.CrcType.CRC32)));
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(super.x(), JSONKey.IS_VALID_SECRET_KEY, this.A);
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        Phase.Result.ResultCode resultCode;
        if (bArr.length != 16) {
            resultCode = Phase.Result.ResultCode.INVALID_DATA_LENGTH;
        } else {
            byte[] a = AuthenticationKeyType.Companion.a(ua0.y.e(), EncryptionAES128.a.a(AuthenticatePhase.H.b(), this.C, AuthenticatePhase.H.a(), bArr));
            if (a.length != 16) {
                resultCode = Phase.Result.ResultCode.INVALID_DATA_LENGTH;
            } else {
                if (Arrays.equals(this.B, ya4.a(a, 8, 16))) {
                    this.A = true;
                }
                resultCode = Phase.Result.ResultCode.SUCCESS;
            }
        }
        a(Phase.Result.copy$default(k(), (PhaseId) null, resultCode, (Request.Result) null, 5, (Object) null));
    }

    @DexIgnore
    public Boolean i() {
        Boolean bool = this.A;
        return Boolean.valueOf(bool != null ? bool.booleanValue() : false);
    }
}
