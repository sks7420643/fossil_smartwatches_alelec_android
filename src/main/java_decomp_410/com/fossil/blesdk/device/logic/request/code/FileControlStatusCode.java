package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FileControlStatusCode implements o70 {
    SUCCESS((byte) 0),
    INVALID_OPERATION_DATA((byte) 1),
    OPERATION_IN_PROGRESS((byte) 2),
    MISS_PACKET((byte) 3),
    SOCKET_BUSY((byte) 4),
    VERIFICATION_FAIL((byte) 5),
    OVERFLOW((byte) 6),
    SIZE_OVER_LIMIT((byte) 7),
    FIRMWARE_INTERNAL_ERROR((byte) 128),
    FIRMWARE_INTERNAL_ERROR_NOT_OPEN((byte) 129),
    FIRMWARE_INTERNAL_ERROR_ACCESS_ERROR((byte) 130),
    FIRMWARE_INTERNAL_ERROR_NOT_FOUND((byte) 131),
    FIRMWARE_INTERNAL_ERROR_NOT_VALID((byte) 132),
    FIRMWARE_INTERNAL_ERROR_ALREADY_CREATE((byte) 133),
    FIRMWARE_INTERNAL_ERROR_NOT_ENOUGH_MEMORY((byte) 134),
    FIRMWARE_INTERNAL_ERROR_NOT_IMPLEMENTED((byte) 135),
    FIRMWARE_INTERNAL_ERROR_NOT_SUPPORT((byte) 136),
    FIRMWARE_INTERNAL_ERROR_SOCKET_BUSY((byte) 137),
    FIRMWARE_INTERNAL_ERROR_SOCKET_ALREADY_OPEN((byte) 138),
    FIRMWARE_INTERNAL_ERROR_INPUT_DATA_INVALID((byte) 139),
    FIRMWARE_INTERNAL_NOT_AUTHENTICATE((byte) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL),
    FIRMWARE_INTERNAL_SIZE_OVER_LIMIT((byte) 141);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte code;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final FileControlStatusCode a(byte b) {
            FileControlStatusCode fileControlStatusCode;
            FileControlStatusCode[] values = FileControlStatusCode.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    fileControlStatusCode = null;
                    break;
                }
                fileControlStatusCode = values[i];
                if (fileControlStatusCode.getCode() == b) {
                    break;
                }
                i++;
            }
            return fileControlStatusCode != null ? fileControlStatusCode : FileControlStatusCode.FIRMWARE_INTERNAL_ERROR;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    FileControlStatusCode(byte b) {
        this.code = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public byte getCode() {
        return this.code;
    }

    @DexIgnore
    public String getLogName() {
        return this.logName;
    }

    @DexIgnore
    public boolean isSuccessCode() {
        return this == SUCCESS;
    }
}
