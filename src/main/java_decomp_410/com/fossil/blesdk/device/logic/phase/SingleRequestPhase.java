package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.TransferDataRequest;
import com.fossil.blesdk.device.logic.request.file.FileDataRequest;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.d70;
import com.fossil.blesdk.obfuscated.e80;
import com.fossil.blesdk.obfuscated.f70;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h70;
import com.fossil.blesdk.obfuscated.j70;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import com.fossil.blesdk.obfuscated.n80;
import com.fossil.blesdk.obfuscated.o60;
import com.fossil.blesdk.obfuscated.p70;
import com.fossil.blesdk.obfuscated.q80;
import com.fossil.blesdk.obfuscated.t60;
import com.fossil.blesdk.obfuscated.w60;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SingleRequestPhase extends Phase {
    @DexIgnore
    public /* final */ Request A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = j90.a(super.n(), A());

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SingleRequestPhase(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, Request request) {
        super(peripheral, aVar, phaseId, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(phaseId, "phaseId");
        kd4.b(request, "request");
        this.A = request;
    }

    @DexIgnore
    public final ArrayList<ResourceType> A() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Request request = this.A;
        if (!(request instanceof w60) && !(request instanceof h70)) {
            if (request instanceof p70) {
                linkedHashSet.add(ResourceType.DEVICE_CONFIG);
            } else if (request instanceof FileDataRequest) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof TransferDataRequest) {
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof e80) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
            } else if ((request instanceof q80) || (request instanceof n80)) {
                linkedHashSet.add(ResourceType.FILE_CONFIG);
                linkedHashSet.add(ResourceType.TRANSFER_DATA);
            } else if (request instanceof j70) {
                linkedHashSet.add(ResourceType.AUTHENTICATION);
            } else if ((request instanceof t60) || (request instanceof d70)) {
                linkedHashSet.add(ResourceType.ASYNC);
            } else if (request instanceof o60) {
                linkedHashSet.add(((o60) request).B().getResourceType$blesdk_productionRelease());
            } else if (request instanceof f70) {
                linkedHashSet.add(((f70) request).B().getResourceType$blesdk_productionRelease());
                linkedHashSet.add(((f70) this.A).E().getResourceType$blesdk_productionRelease());
            } else {
                linkedHashSet.add(ResourceType.DEVICE_INFORMATION);
            }
        }
        return new ArrayList<>(linkedHashSet);
    }

    @DexIgnore
    public void c(Request request) {
        kd4.b(request, "request");
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public final void t() {
        Phase.a((Phase) this, this.A, (xc4) new SingleRequestPhase$onStart$Anon1(this), (xc4) SingleRequestPhase$onStart$Anon2.INSTANCE, (yc4) null, (xc4) new SingleRequestPhase$onStart$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject u() {
        return m90.a(super.u(), this.A.t());
    }
}
