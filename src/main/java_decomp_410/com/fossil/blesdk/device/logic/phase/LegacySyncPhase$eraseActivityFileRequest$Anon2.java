package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacySyncPhase$eraseActivityFileRequest$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacySyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase$eraseActivityFileRequest$Anon2(LegacySyncPhase legacySyncPhase) {
        super(1);
        this.this$Anon0 = legacySyncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        o70 responseStatus = request.n().getResponseStatus();
        if (responseStatus == LegacyFileControlStatusCode.INVALID_FILE_HANDLE) {
            LegacySyncPhase legacySyncPhase = this.this$Anon0;
            legacySyncPhase.a(Phase.Result.copy$default(legacySyncPhase.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (responseStatus == LegacyFileControlStatusCode.INVALID_OPERATION) {
            this.this$Anon0.B();
        } else {
            this.this$Anon0.a(request.n());
        }
    }
}
