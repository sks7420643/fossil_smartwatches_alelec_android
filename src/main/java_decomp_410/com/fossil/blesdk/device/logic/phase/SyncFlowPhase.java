package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h80;
import com.fossil.blesdk.obfuscated.j50;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.z40;
import com.fossil.fitness.FitnessData;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SyncFlowPhase extends Phase {
    @DexIgnore
    public FitnessData[] A;
    @DexIgnore
    public byte[][] B;
    @DexIgnore
    public long C;
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public /* final */ boolean H;
    @DexIgnore
    public /* final */ boolean I;
    @DexIgnore
    public /* final */ boolean J;
    @DexIgnore
    public /* final */ int K;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> L;
    @DexIgnore
    public /* final */ BiometricProfile M;
    @DexIgnore
    public /* final */ HashMap<GetFilePhase.GetFileOption, Object> N;
    @DexIgnore
    public /* final */ boolean z;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SyncFlowPhase(Peripheral peripheral, Phase.a aVar, BiometricProfile biometricProfile, HashMap hashMap, int i, fd4 fd4) {
        this(peripheral, aVar, biometricProfile, (i & 8) != 0 ? new HashMap() : hashMap);
    }

    @DexIgnore
    public final void A() {
        long j = this.C;
        long j2 = this.D;
        this.E = j + j2;
        long j3 = this.E;
        if (j3 != 0) {
            this.F = ((float) j) / ((float) j3);
            this.G = ((float) j2) / ((float) j3);
        }
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Request) new h80(z40.b.a(j().k(), FileType.ACTIVITY_FILE), j(), 0, 4, (fd4) null), (xc4) new SyncFlowPhase$getActivityFileTotalSize$Anon1(this), (xc4) new SyncFlowPhase$getActivityFileTotalSize$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new h80(z40.b.a(j().k(), FileType.HARDWARE_LOG), j(), 0, 4, (fd4) null), (xc4) new SyncFlowPhase$getHWLogFileTotalSize$Anon1(this), (xc4) new SyncFlowPhase$getHWLogFileTotalSize$Anon2(this), (yc4) null, (xc4) new SyncFlowPhase$getHWLogFileTotalSize$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void D() {
        Phase.a((Phase) this, (Phase) new j50(j(), e(), (HashMap) null, l(), 4, (fd4) null), (xc4) new SyncFlowPhase$runGetHWLogPhase$Anon1(this), (xc4) new SyncFlowPhase$runGetHWLogPhase$Anon2(this), (yc4) new SyncFlowPhase$runGetHWLogPhase$Anon3(this), (xc4) null, (xc4) null, 48, (Object) null);
    }

    @DexIgnore
    public final void E() {
        Phase.a((Phase) this, (Phase) new SyncPhase(j(), e(), this.M, this.N, l()), (xc4) new SyncFlowPhase$runSyncPhase$Anon1(this), (xc4) new SyncFlowPhase$runSyncPhase$Anon2(this), (yc4) new SyncFlowPhase$runSyncPhase$Anon3(this), (xc4) null, (xc4) null, 48, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.L;
    }

    @DexIgnore
    public void t() {
        if (y00.f.b(e().getDeviceInformation())) {
            this.F = 1.0f;
            this.G = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            E();
            return;
        }
        B();
    }

    @DexIgnore
    public JSONObject u() {
        JSONObject put = super.u().put(DeviceConfigKey.BIOMETRIC_PROFILE.getLogName$blesdk_productionRelease(), this.M.valueDescription()).put(GetFilePhase.GetFileOption.SKIP_LIST.getLogName$blesdk_productionRelease(), this.H).put(GetFilePhase.GetFileOption.SKIP_ERASE.getLogName$blesdk_productionRelease(), this.I).put(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS.getLogName$blesdk_productionRelease(), this.J).put(GetFilePhase.GetFileOption.NUMBER_OF_FILE_REQUIRED.getLogName$blesdk_productionRelease(), this.K);
        kd4.a((Object) put, "super.optionDescription(\u2026me, numberOfFileRequired)");
        return put;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase(Peripheral peripheral, Phase.a aVar, BiometricProfile biometricProfile, HashMap<GetFilePhase.GetFileOption, Object> hashMap) {
        super(peripheral, aVar, PhaseId.SYNC_FLOW, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(biometricProfile, "biometricProfile");
        kd4.b(hashMap, "options");
        this.M = biometricProfile;
        this.N = hashMap;
        this.z = true;
        this.A = new FitnessData[0];
        this.F = 0.5f;
        this.G = 0.5f;
        Boolean bool = (Boolean) this.N.get(GetFilePhase.GetFileOption.SKIP_LIST);
        this.H = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) this.N.get(GetFilePhase.GetFileOption.SKIP_ERASE);
        this.I = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) this.N.get(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.J = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) this.N.get(GetFilePhase.GetFileOption.NUMBER_OF_FILE_REQUIRED);
        this.K = num != null ? num.intValue() : 0;
        this.L = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));
    }

    @DexIgnore
    public boolean c() {
        return this.z;
    }

    @DexIgnore
    public FitnessData[] i() {
        return this.A;
    }
}
