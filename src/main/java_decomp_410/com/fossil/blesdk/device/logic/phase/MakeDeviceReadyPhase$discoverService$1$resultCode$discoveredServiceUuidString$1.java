package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredServiceUuidString$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.util.UUID, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredServiceUuidString$1 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredServiceUuidString$1();

    @DexIgnore
    public MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredServiceUuidString$1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return invoke((java.util.UUID) obj);
    }

    @DexIgnore
    public final java.lang.String invoke(java.util.UUID uuid) {
        com.fossil.blesdk.obfuscated.kd4.b(uuid, "it");
        java.lang.String uuid2 = uuid.toString();
        com.fossil.blesdk.obfuscated.kd4.a((java.lang.Object) uuid2, "it.toString()");
        return uuid2;
    }
}
