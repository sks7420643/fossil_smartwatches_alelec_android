package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$tryExecuteRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.request.RequestId $nextRequestId;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$tryExecuteRequest$1(com.fossil.blesdk.device.logic.phase.Phase phase, com.fossil.blesdk.device.logic.request.RequestId requestId) {
        super(0);
        this.this$0 = phase;
        this.$nextRequestId = requestId;
    }

    @DexIgnore
    public final void invoke() {
        com.fossil.blesdk.device.logic.phase.Phase.m3630a(this.this$0, this.$nextRequestId, (com.fossil.blesdk.device.logic.request.RequestId) null, 2, (java.lang.Object) null);
    }
}
