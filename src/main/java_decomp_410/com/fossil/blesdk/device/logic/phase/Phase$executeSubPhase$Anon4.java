package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeSubPhase$Anon4 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ xc4 $actionOnError;
    @DexIgnore
    public /* final */ /* synthetic */ xc4 $isTerminatedPhaseError;
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$executeSubPhase$Anon4(Phase phase, xc4 xc4, xc4 xc42) {
        super(1);
        this.this$Anon0 = phase;
        this.$isTerminatedPhaseError = xc4;
        this.$actionOnError = xc42;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "executedPhase");
        if (((Boolean) this.$isTerminatedPhaseError.invoke(phase.k())).booleanValue()) {
            this.this$Anon0.a(Phase.Result.copy$default(phase.k(), this.this$Anon0.g(), (Phase.Result.ResultCode) null, (Request.Result) null, 6, (Object) null));
        } else {
            this.$actionOnError.invoke(phase);
        }
    }
}
