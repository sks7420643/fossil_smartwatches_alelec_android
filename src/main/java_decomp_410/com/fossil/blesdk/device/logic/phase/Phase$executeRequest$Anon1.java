package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeRequest$Anon1 extends Lambda implements yc4<Request, Float, qa4> {
    @DexIgnore
    public static /* final */ Phase$executeRequest$Anon1 INSTANCE; // = new Phase$executeRequest$Anon1();

    @DexIgnore
    public Phase$executeRequest$Anon1() {
        super(2);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Request) obj, ((Number) obj2).floatValue());
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request, float f) {
        kd4.b(request, "<anonymous parameter 0>");
    }
}
