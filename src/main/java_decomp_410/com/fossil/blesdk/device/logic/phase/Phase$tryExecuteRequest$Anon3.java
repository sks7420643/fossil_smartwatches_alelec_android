package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.TransferDataRequest;
import com.fossil.blesdk.obfuscated.c80;
import com.fossil.blesdk.obfuscated.e80;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l80;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$tryExecuteRequest$Anon3 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ wc4 $nextAction;
    @DexIgnore
    public /* final */ /* synthetic */ Phase this$Anon0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements xc4<Request, qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ Request $originalRequest;
        @DexIgnore
        public /* final */ /* synthetic */ Phase$tryExecuteRequest$Anon3 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(Phase$tryExecuteRequest$Anon3 phase$tryExecuteRequest$Anon3, Request request) {
            super(1);
            this.this$Anon0 = phase$tryExecuteRequest$Anon3;
            this.$originalRequest = request;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return qa4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            kd4.b(request, "it");
            Request request2 = this.$originalRequest;
            if ((request2 instanceof l80) || request2.n().getResultCode() == Request.Result.ResultCode.INTERRUPTED) {
                this.this$Anon0.this$Anon0.a(this.$originalRequest.n());
            } else {
                this.this$Anon0.$nextAction.invoke();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements xc4<Request, qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ Phase$tryExecuteRequest$Anon3 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(Phase$tryExecuteRequest$Anon3 phase$tryExecuteRequest$Anon3) {
            super(1);
            this.this$Anon0 = phase$tryExecuteRequest$Anon3;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Request) obj);
            return qa4.a;
        }

        @DexIgnore
        public final void invoke(Request request) {
            kd4.b(request, "abortRequest");
            if (this.this$Anon0.this$Anon0.k().getResultCode() == Phase.Result.ResultCode.INTERRUPTED) {
                Phase phase = this.this$Anon0.this$Anon0;
                phase.a(phase.k());
                return;
            }
            this.this$Anon0.this$Anon0.a(request.n());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$tryExecuteRequest$Anon3(Phase phase, wc4 wc4) {
        super(1);
        this.this$Anon0 = phase;
        this.$nextAction = wc4;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Short sh;
        kd4.b(request, "originalRequest");
        Phase phase = this.this$Anon0;
        phase.m = phase.m + 1;
        Phase phase2 = this.this$Anon0;
        phase2.b(Phase.Result.copy$default(phase2.k(), (PhaseId) null, Phase.Result.ResultCode.Companion.a(request.n()), request.n(), 1, (Object) null));
        if (request.n().getResultCode() == Request.Result.ResultCode.CONNECTION_DROPPED || request.n().getResultCode() == Request.Result.ResultCode.BLUETOOTH_OFF || request.n().getCommandResult().getGattResult().getResultCode() == GattOperationResult.GattResult.ResultCode.GATT_NULL) {
            this.this$Anon0.a(request.n());
            return;
        }
        if (request instanceof e80) {
            sh = Short.valueOf(((e80) request).I());
        } else {
            sh = request instanceof TransferDataRequest ? Short.valueOf(((TransferDataRequest) request).D()) : null;
        }
        if (sh != null) {
            Phase.a(this.this$Anon0, (Request) new c80(sh.shortValue(), this.this$Anon0.j(), 0, 4, (fd4) null), (xc4) new Anon1(this, request), (xc4) new Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
        } else {
            this.$nextAction.invoke();
        }
    }
}
