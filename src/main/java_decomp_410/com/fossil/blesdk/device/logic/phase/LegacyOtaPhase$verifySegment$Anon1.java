package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.z80;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$verifySegment$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ long $expectedSegmentCRC;
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$verifySegment$Anon1(LegacyOtaPhase legacyOtaPhase, long j) {
        super(1);
        this.this$Anon0 = legacyOtaPhase;
        this.$expectedSegmentCRC = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        if (this.$expectedSegmentCRC == ((z80) request).J()) {
            LegacyOtaPhase legacyOtaPhase = this.this$Anon0;
            legacyOtaPhase.F = legacyOtaPhase.E;
            if (this.this$Anon0.D != this.this$Anon0.F) {
                this.this$Anon0.B();
            } else if (this.this$Anon0.F == this.this$Anon0.A) {
                this.this$Anon0.H();
            } else {
                this.this$Anon0.F();
            }
        } else {
            LegacyOtaPhase legacyOtaPhase2 = this.this$Anon0;
            legacyOtaPhase2.E = Math.max(0, legacyOtaPhase2.E - 6144);
            if (this.this$Anon0.E == 0) {
                this.this$Anon0.F();
            } else {
                this.this$Anon0.I();
            }
        }
    }
}
