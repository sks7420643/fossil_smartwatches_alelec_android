package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.request.Request, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.Phase$executeRequest$1 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.Phase$executeRequest$1();

    @DexIgnore
    public Phase$executeRequest$1() {
        super(2);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "<anonymous parameter 0>");
    }
}
