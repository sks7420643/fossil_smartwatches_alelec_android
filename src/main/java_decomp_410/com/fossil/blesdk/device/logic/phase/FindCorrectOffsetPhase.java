package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.c50;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.k80;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FindCorrectOffsetPhase extends Phase {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public /* final */ long D; // = n90.b(this.E.getRawData().length);
    @DexIgnore
    public /* final */ DeviceFile E;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FindCorrectOffsetPhase(Peripheral peripheral, Phase.a aVar, DeviceFile deviceFile, String str) {
        super(peripheral, aVar, PhaseId.FIND_CORRECT_OFFSET, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(deviceFile, "deviceFile");
        kd4.b(str, "phaseUuid");
        this.E = deviceFile;
    }

    @DexIgnore
    public final k80 A() {
        long j = this.B;
        k80 k80 = new k80(j, this.C - j, this.E.getFileHandle$blesdk_productionRelease(), j(), 0, 16, (fd4) null);
        k80.c((xc4<? super Request, qa4>) new FindCorrectOffsetPhase$buildVerifyDataRequest$Anon1(this));
        return k80;
    }

    @DexIgnore
    public final long B() {
        return this.A;
    }

    @DexIgnore
    public final DeviceFile C() {
        return this.E;
    }

    @DexIgnore
    public final void D() {
        this.B = 0;
        this.C = n90.b(this.E.getRawData().length);
        if (this.B == this.C) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        D();
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.DEVICE_FILE, this.E.toJSONObject(false));
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(super.x(), JSONKey.CORRECT_OFFSET, Long.valueOf(this.A));
    }

    @DexIgnore
    public Request a(RequestId requestId) {
        kd4.b(requestId, "requestId");
        if (c50.a[requestId.ordinal()] != 1) {
            return null;
        }
        return A();
    }

    @DexIgnore
    public Long i() {
        return Long.valueOf(this.A);
    }

    @DexIgnore
    public final void a(long j, long j2, long j3) {
        if (0 == j2) {
            this.A = this.B;
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else if (j != this.B) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.FLOW_BROKEN, (Request.Result) null, 5, (Object) null));
        } else {
            this.C = j + j2;
            long a = Crc32Calculator.a.a(this.E.getRawData(), (int) j, (int) j2, Crc32Calculator.CrcType.CRC32);
            t90 t90 = t90.c;
            String r = r();
            t90.a(r, "onVerifyDataSuccess: transferredDataCrc=" + a);
            if (j3 == a) {
                long j4 = this.C;
                long j5 = this.D;
                if (j4 == j5) {
                    this.A = j4;
                    a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
                    return;
                }
                this.C = Math.min(((j4 - this.B) / ((long) 2)) + j4, j5);
                this.B = j4;
                Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
                return;
            }
            this.C = (this.C + this.B) / ((long) 2);
            Phase.a(this, RequestId.VERIFY_DATA, (RequestId) null, 2, (Object) null);
        }
    }
}
