package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$transferData$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.request.Request, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ byte[] $dataToTransfer;
    @DexIgnore
    public /* final */ /* synthetic */ long $offset;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$transferData$1(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase, byte[] bArr, long j) {
        super(2);
        this.this$0 = legacyOtaPhase;
        this.$dataToTransfer = bArr;
        this.$offset = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "<anonymous parameter 0>");
        this.this$0.f2940B = this.$offset + ((long) (f * ((float) this.$dataToTransfer.length)));
        float o = (((float) this.this$0.f2940B) * 1.0f) / ((float) this.this$0.f2939A);
        if (java.lang.Math.abs(o - this.this$0.f2941C) > this.this$0.f2952N || o == 1.0f) {
            this.this$0.f2941C = o;
            this.this$0.mo7541a(o);
        }
    }
}
