package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.x60;
import com.fossil.blesdk.obfuscated.xc4;
import java.util.LinkedHashMap;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadDeviceInformationByCharacteristicPhase$readSerialNumber$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ReadDeviceInformationByCharacteristicPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadDeviceInformationByCharacteristicPhase$readSerialNumber$Anon1(ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase) {
        super(1);
        this.this$Anon0 = readDeviceInformationByCharacteristicPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        Request request2 = request;
        kd4.b(request2, "executedRequest");
        String B = ((x60) request2).B();
        if (DeviceInformation.Companion.a(B)) {
            ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase = this.this$Anon0;
            readDeviceInformationByCharacteristicPhase.z = DeviceInformation.copy$default(readDeviceInformationByCharacteristicPhase.A(), (String) null, (String) null, B, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131067, (Object) null);
            this.this$Anon0.C();
            return;
        }
        ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase2 = this.this$Anon0;
        readDeviceInformationByCharacteristicPhase2.a(Phase.Result.copy$default(readDeviceInformationByCharacteristicPhase2.k(), (PhaseId) null, Phase.Result.ResultCode.INVALID_SERIAL_NUMBER, (Request.Result) null, 5, (Object) null));
    }
}
