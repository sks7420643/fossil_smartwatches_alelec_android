package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$discoverService$1(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode resultCode;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        com.fossil.blesdk.obfuscated.r60 r60 = (com.fossil.blesdk.obfuscated.r60) request;
        java.util.List e = com.fossil.blesdk.obfuscated.za4.m31316e(r60.mo15447C());
        java.util.List e2 = com.fossil.blesdk.obfuscated.za4.m31316e(r60.mo15446B());
        if (e.containsAll(this.this$0.f2971E)) {
            java.lang.String a = com.fossil.blesdk.obfuscated.kb4.m24365a(e2, (java.lang.CharSequence) null, (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.device.logic.phase.C1240x5ecb1267.INSTANCE, 31, (java.lang.Object) null);
            com.fossil.blesdk.obfuscated.t90 t90 = com.fossil.blesdk.obfuscated.t90.f9612c;
            java.lang.String r = this.this$0.mo7571r();
            t90.mo16336a(r, "Discovered Characteristic: " + a);
            this.this$0.mo7480a((java.util.List<? extends com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId>) e2);
            this.this$0.mo7475L();
            resultCode = com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS;
        } else {
            java.lang.String a2 = com.fossil.blesdk.obfuscated.kb4.m24365a(e, (java.lang.CharSequence) null, (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.device.logic.phase.C1241xb62a54bf.INSTANCE, 31, (java.lang.Object) null);
            com.fossil.blesdk.obfuscated.t90 t902 = com.fossil.blesdk.obfuscated.t90.f9612c;
            java.lang.String r2 = this.this$0.mo7571r();
            t902.mo16337b(r2, "Discovered Services: " + a2);
            resultCode = com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.LACK_OF_SERVICE;
        }
        com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode resultCode2 = resultCode;
        if (resultCode2 != com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$0;
            makeDeviceReadyPhase.mo7483c(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(makeDeviceReadyPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, resultCode2, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        }
    }
}
