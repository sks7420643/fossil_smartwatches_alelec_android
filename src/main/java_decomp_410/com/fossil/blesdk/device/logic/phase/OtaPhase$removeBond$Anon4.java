package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaPhase$removeBond$Anon4 extends Lambda implements xc4<Request.Result, Boolean> {
    @DexIgnore
    public static /* final */ OtaPhase$removeBond$Anon4 INSTANCE; // = new OtaPhase$removeBond$Anon4();

    @DexIgnore
    public OtaPhase$removeBond$Anon4() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((Request.Result) obj));
    }

    @DexIgnore
    public final boolean invoke(Request.Result result) {
        kd4.b(result, "requestResult");
        return result.getResultCode() == Request.Result.ResultCode.BLUETOOTH_OFF;
    }
}
