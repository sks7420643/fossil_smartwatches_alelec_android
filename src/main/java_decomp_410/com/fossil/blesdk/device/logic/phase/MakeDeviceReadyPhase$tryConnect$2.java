package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$tryConnect$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$1")
    /* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$1 */
    public static final class C12441 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public static /* final */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12441 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12441();

        @DexIgnore
        public C12441() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.device.logic.request.Request) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$2")
    /* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$2 */
    public static final class C12452 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public static /* final */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12452 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12452();

        @DexIgnore
        public C12452() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.device.logic.request.Request) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$3")
    /* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2$3 */
    public static final class C12463 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C12463(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2 makeDeviceReadyPhase$tryConnect$2) {
            super(1);
            this.this$0 = makeDeviceReadyPhase$tryConnect$2;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.device.logic.request.Request) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
            this.this$0.this$0.mo7476M();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$tryConnect$2(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        this.this$0.f2975I = java.lang.System.currentTimeMillis();
        com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$0;
        makeDeviceReadyPhase.mo7554b(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(makeDeviceReadyPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.Companion.mo7587a(request.mo7744n()), request.mo7744n(), 1, (java.lang.Object) null));
        com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase2 = this.this$0;
        makeDeviceReadyPhase2.f2976J = makeDeviceReadyPhase2.f2975I - this.this$0.f2974H < 1000 && request.mo7744n().getCommandResult().getGattResult().getBluetoothGattStatus() == 133;
        com.fossil.blesdk.obfuscated.q60 q60 = new com.fossil.blesdk.obfuscated.q60(this.this$0.mo7564j());
        q60.mo7729b(false);
        com.fossil.blesdk.device.logic.phase.Phase.m3629a((com.fossil.blesdk.device.logic.phase.Phase) this.this$0, (com.fossil.blesdk.device.logic.request.Request) q60, (com.fossil.blesdk.obfuscated.xc4) com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12441.INSTANCE, (com.fossil.blesdk.obfuscated.xc4) com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12452.INSTANCE, (com.fossil.blesdk.obfuscated.yc4) null, (com.fossil.blesdk.obfuscated.xc4) new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$2.C12463(this), (com.fossil.blesdk.obfuscated.xc4) null, 40, (java.lang.Object) null);
    }
}
