package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeSubPhase$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase.Result, java.lang.Boolean> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.Phase$executeSubPhase$3 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.Phase$executeSubPhase$3();

    @DexIgnore
    public Phase$executeSubPhase$3() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.fossil.blesdk.device.logic.phase.Phase.Result) obj));
    }

    @DexIgnore
    public final boolean invoke(com.fossil.blesdk.device.logic.phase.Phase.Result result) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(result, "phaseResult");
        return result.getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.INTERRUPTED || result.getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.CONNECTION_DROPPED || result.getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.BLUETOOTH_OFF || result.getRequestResult().getCommandResult().getGattResult().getResultCode() == com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.ResultCode.GATT_NULL;
    }
}
