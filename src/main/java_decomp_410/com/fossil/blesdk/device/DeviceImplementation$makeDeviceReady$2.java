package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$makeDeviceReady$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.v00 $task;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$makeDeviceReady$2(com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.obfuscated.v00 v00) {
        super(1);
        this.this$0 = deviceImplementation;
        this.$task = v00;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        this.this$0.f2348h = false;
        this.this$0.f2357q = ((com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase) phase).mo7471H();
        this.this$0.mo5915a(com.fossil.blesdk.device.Device.State.CONNECTED);
        this.$task.mo16948c(this.this$0.getDeviceInformation());
    }
}
