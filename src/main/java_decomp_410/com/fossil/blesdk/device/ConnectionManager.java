package com.fossil.blesdk.device;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lb0;
import com.fossil.blesdk.obfuscated.mb0;
import com.fossil.blesdk.obfuscated.n00;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.rb4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.y00;
import java.io.Serializable;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionManager {
    @DexIgnore
    public static /* final */ String a; // = ConnectionManager.class.getSimpleName();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<DeviceImplementation> b; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<DeviceImplementation> c; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<DeviceImplementation> d; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ CopyOnWriteArraySet<DeviceImplementation> e; // = new CopyOnWriteArraySet<>();
    @DexIgnore
    public static /* final */ Handler f; // = hb0.a.a();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new a();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new d();
    @DexIgnore
    public static /* final */ BroadcastReceiver i; // = new c();
    @DexIgnore
    public static /* final */ ConnectionManager j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                Serializable serializableExtra = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE");
                if (serializableExtra != null) {
                    BluetoothLeAdapter.State state = (BluetoothLeAdapter.State) serializableExtra;
                    Serializable serializableExtra2 = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE");
                    if (serializableExtra2 != null) {
                        ConnectionManager.j.a(state, (BluetoothLeAdapter.State) serializableExtra2);
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceImplementation e;

        @DexIgnore
        public b(DeviceImplementation deviceImplementation) {
            this.e = deviceImplementation;
        }

        @DexIgnore
        public final void run() {
            throw null;
            // if (this.e.getState() == Device.State.DISCONNECTED && BluetoothLeAdapter.l.c() == BluetoothLeAdapter.State.ENABLED) {
            //     boolean a = y00.f.a(this.e.s());
            //     this.e.b((HashMap<MakeDeviceReadyPhase.MakeDeviceReadyOption, Object>) rb4.a((Pair<? extends K, ? extends V>[]) new Pair[]{oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(a)), oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, Long.valueOf(ConnectionManager.j.a(a)))}));
            // }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            throw null;
            // String str = null;
            // String action = intent != null ? intent.getAction() : null;
            // if (action != null && action.hashCode() == -343131398 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED")) {
            //     DeviceImplementation deviceImplementation = (DeviceImplementation) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            //     Device.HIDState hIDState = (Device.HIDState) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE");
            //     Device.HIDState hIDState2 = (Device.HIDState) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE");
            //     t90 t90 = t90.c;
            //     String c = ConnectionManager.a;
            //     kd4.a((Object) c, "TAG");
            //     StringBuilder sb = new StringBuilder();
            //     sb.append("deviceHIDStateChangeReceiver: ");
            //     sb.append("device=");
            //     if (deviceImplementation != null) {
            //         DeviceInformation deviceInformation = deviceImplementation.getDeviceInformation();
            //         if (deviceInformation != null) {
            //             str = deviceInformation.getMacAddress();
            //         }
            //     }
            //     sb.append(str);
            //     sb.append(", ");
            //     sb.append("previousState=");
            //     sb.append(hIDState);
            //     sb.append(", newState=");
            //     sb.append(hIDState2);
            //     t90.a(c, sb.toString());
            //     if (deviceImplementation != null && hIDState != null && hIDState2 != null) {
            //         ConnectionManager.j.a(deviceImplementation, hIDState, hIDState2);
            //     }
            // }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            throw null;
            // String str = null;
            // String action = intent != null ? intent.getAction() : null;
            // if (action != null && action.hashCode() == 81199830 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED")) {
            //     DeviceImplementation deviceImplementation = (DeviceImplementation) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            //     Device.State state = (Device.State) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_STATE");
            //     Device.State state2 = (Device.State) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_STATE");
            //     t90 t90 = t90.c;
            //     String c = ConnectionManager.a;
            //     kd4.a((Object) c, "TAG");
            //     StringBuilder sb = new StringBuilder();
            //     sb.append("deviceStateChangeReceiver: ");
            //     sb.append("device=");
            //     if (deviceImplementation != null) {
            //         DeviceInformation deviceInformation = deviceImplementation.getDeviceInformation();
            //         if (deviceInformation != null) {
            //             str = deviceInformation.getMacAddress();
            //         }
            //     }
            //     sb.append(str);
            //     sb.append(", ");
            //     sb.append("previousState=");
            //     sb.append(state);
            //     sb.append(", newState=");
            //     sb.append(state2);
            //     t90.a(c, sb.toString());
            //     if (deviceImplementation != null && state != null && state2 != null) {
            //         ConnectionManager.j.a(deviceImplementation, state, state2);
            //     }
            // }
        }
    }

    /*
    static {
        ConnectionManager connectionManager = new ConnectionManager();
        j = connectionManager;
        connectionManager.c();
        connectionManager.e();
        connectionManager.d();
    }
    */

    @DexIgnore
    public final long a(boolean z) {
        return 0;
    }

    @DexIgnore
    public final long c(DeviceImplementation deviceImplementation) {
        return 200;
    }

    @DexIgnore
    public final void d() {
        mb0.a.a(i, "com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED");
    }

    @DexIgnore
    public final void e() {
        mb0.a.a(h, "com.fossil.blesdk.device.DeviceImplementation.action.STATE_CHANGED");
    }

    @DexIgnore
    public final void f(DeviceImplementation deviceImplementation) {
        b.remove(deviceImplementation);
    }

    @DexIgnore
    public final void g(DeviceImplementation deviceImplementation) {
        c.remove(deviceImplementation);
    }

    @DexIgnore
    public final boolean h(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        a(deviceImplementation);
        b(deviceImplementation);
        return true;
    }

    @DexIgnore
    public final boolean i(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        f(deviceImplementation);
        return true;
    }

    @DexIgnore
    public final void b(DeviceImplementation deviceImplementation) {
        if (d(deviceImplementation) && deviceImplementation.getState() == Device.State.DISCONNECTED) {
            f.postDelayed(new b(deviceImplementation), c(deviceImplementation));
        }
    }

    @DexIgnore
    public final void c() {
        mb0.a.a(g, "com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
    }

    @DexIgnore
    public final boolean d(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        return b.contains(deviceImplementation);
    }

    @DexIgnore
    public final boolean e(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        return c.contains(deviceImplementation);
    }

    @DexIgnore
    public final void b() {
        for (DeviceImplementation deviceImplementation : c) {
            ConnectionManager connectionManager = j;
            kd4.a((Object) deviceImplementation, "it");
            a(connectionManager, deviceImplementation, 0, 2, (Object) null);
        }
    }

    @DexIgnore
    public final void a(BluetoothLeAdapter.State state, BluetoothLeAdapter.State state2) {
        throw null;
        // if (n00.a[state2.ordinal()] == 1) {
        //     a();
        //     b();
        // }
    }

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation, Device.State state, Device.State state2) {
        b(deviceImplementation);
    }

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation, Device.HIDState hIDState, Device.HIDState hIDState2) {
        throw null;
        // int i2 = n00.b[hIDState2.ordinal()];
        // if (i2 == 1) {
        //     return;
        // }
        // if (i2 == 2) {
        //     a(this, deviceImplementation, 0, 2, (Object) null);
        // } else if (i2 == 3) {
        //     lb0.b.b("HID_EXPONENT_BACK_OFF_TAG");
        //     a(this, deviceImplementation, 0, 2, (Object) null);
        // }
    }

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation) {
        b.add(deviceImplementation);
    }

    @DexIgnore
    public final void a() {
        for (DeviceImplementation deviceImplementation : b) {
            ConnectionManager connectionManager = j;
            kd4.a((Object) deviceImplementation, "it");
            connectionManager.b(deviceImplementation);
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(ConnectionManager connectionManager, DeviceImplementation deviceImplementation, long j2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            j2 = 0;
        }
        connectionManager.a(deviceImplementation, j2);
    }

    @DexIgnore
    public final void a(DeviceImplementation deviceImplementation, long j2) {
        f.postDelayed(new ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1(deviceImplementation), j2);
    }
}
