package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.DailyDistanceConfig;
import com.fossil.blesdk.obfuscated.je4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.md4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.FunctionReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon8 extends FunctionReference implements xc4<byte[], DailyDistanceConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon8(DailyDistanceConfig.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final je4 getOwner() {
        return md4.a(DailyDistanceConfig.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyDistanceConfig;";
    }

    @DexIgnore
    public final DailyDistanceConfig invoke(byte[] bArr) {
        kd4.b(bArr, "p1");
        return ((DailyDistanceConfig.a) this.receiver).a(bArr);
    }
}
