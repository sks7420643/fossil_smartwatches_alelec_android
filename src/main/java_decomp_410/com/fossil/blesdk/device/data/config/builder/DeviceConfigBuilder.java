package com.fossil.blesdk.device.data.config.builder;

import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig;
import com.fossil.blesdk.device.data.config.DailyDistanceConfig;
import com.fossil.blesdk.device.data.config.DailySleepConfig;
import com.fossil.blesdk.device.data.config.DailyStepConfig;
import com.fossil.blesdk.device.data.config.DailyStepGoalConfig;
import com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DisplayUnitConfig;
import com.fossil.blesdk.device.data.config.HeartRateModeConfig;
import com.fossil.blesdk.device.data.config.InactiveNudgeConfig;
import com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig;
import com.fossil.blesdk.device.data.config.TimeConfig;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.device.data.enumerate.CaloriesUnit;
import com.fossil.blesdk.device.data.enumerate.DateFormat;
import com.fossil.blesdk.device.data.enumerate.DistanceUnit;
import com.fossil.blesdk.device.data.enumerate.TemperatureUnit;
import com.fossil.blesdk.device.data.enumerate.TimeFormat;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.portfolio.platform.data.model.MFUser;
import java.util.ArrayList;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceConfigBuilder {
    @DexIgnore
    public /* final */ ArrayList<DeviceConfigItem> a; // = new ArrayList<>();

    @DexIgnore
    public final void a(DeviceConfigItem deviceConfigItem) {
        hb4.a(this.a, new DeviceConfigBuilder$addConfig$Anon1(deviceConfigItem));
        this.a.add(deviceConfigItem);
    }

    @DexIgnore
    public final DeviceConfigBuilder b(long j) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyCalorieGoalConfig(j));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder c(long j) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyDistanceConfig(j));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder d(long j) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyStepConfig(j));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder e(long j) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyStepGoalConfig(j));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder b(int i) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyTotalActiveMinuteConfig(i));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(byte b, BiometricProfile.Gender gender, short s, short s2, BiometricProfile.WearingPosition wearingPosition) throws IllegalArgumentException {
        kd4.b(gender, "gender");
        kd4.b(wearingPosition, "wearingPosition");
        a((DeviceConfigItem) new BiometricProfile(b, gender, s, s2, wearingPosition));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(long j) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyCalorieConfig(j));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(int i) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailyActiveMinuteGoalConfig(i));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(byte b, byte b2, byte b3, byte b4, short s, InactiveNudgeConfig.State state) throws IllegalArgumentException {
        kd4.b(state, "state");
        a((DeviceConfigItem) new InactiveNudgeConfig(b, b2, b3, b4, s, state));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(VibeStrengthConfig.VibeStrengthLevel vibeStrengthLevel) {
        kd4.b(vibeStrengthLevel, "vibeStrengthLevel");
        a((DeviceConfigItem) new VibeStrengthConfig(vibeStrengthLevel));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(long j, short s, short s2) throws IllegalArgumentException {
        a((DeviceConfigItem) new TimeConfig(j, s, s2));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(HeartRateModeConfig.HeartRateMode heartRateMode) {
        kd4.b(heartRateMode, "heartRateMode");
        a((DeviceConfigItem) new HeartRateModeConfig(heartRateMode));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a((DeviceConfigItem) new DailySleepConfig(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(TemperatureUnit temperatureUnit, CaloriesUnit caloriesUnit, DistanceUnit distanceUnit, TimeFormat timeFormat, DateFormat dateFormat) {
        kd4.b(temperatureUnit, MFUser.TEMPERATURE_UNIT);
        kd4.b(caloriesUnit, "caloriesUnit");
        kd4.b(distanceUnit, MFUser.DISTANCE_UNIT);
        kd4.b(timeFormat, "timeFormat");
        kd4.b(dateFormat, "dateFormat");
        a((DeviceConfigItem) new DisplayUnitConfig(temperatureUnit, caloriesUnit, distanceUnit, timeFormat, dateFormat));
        return this;
    }

    @DexIgnore
    public final DeviceConfigBuilder a(short s) throws IllegalArgumentException {
        a((DeviceConfigItem) new SecondTimezoneOffsetConfig(s));
        return this;
    }

    @DexIgnore
    public final DeviceConfigItem[] a() {
        Object[] array = this.a.toArray(new DeviceConfigItem[0]);
        if (array != null) {
            return (DeviceConfigItem[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
