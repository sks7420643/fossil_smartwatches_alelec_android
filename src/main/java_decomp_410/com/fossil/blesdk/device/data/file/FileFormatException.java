package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FileFormatException extends Exception {
    @DexIgnore
    public /* final */ FileFormatErrorCode errorCode;

    @DexIgnore
    public enum FileFormatErrorCode {
        UNSUPPORTED_VERSION(1),
        INVALID_FILE_DATA(2);
        
        @DexIgnore
        public /* final */ int id;
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        FileFormatErrorCode(int i) {
            this.id = i;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final int getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ FileFormatException(FileFormatErrorCode fileFormatErrorCode, String str, Throwable th, int i, fd4 fd4) {
        this(fileFormatErrorCode, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : th);
    }

    @DexIgnore
    public final FileFormatErrorCode getErrorCode$blesdk_productionRelease() {
        return this.errorCode;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FileFormatException(FileFormatErrorCode fileFormatErrorCode, String str, Throwable th) {
        super(str, th);
        kd4.b(fileFormatErrorCode, "errorCode");
        this.errorCode = fileFormatErrorCode;
    }
}
