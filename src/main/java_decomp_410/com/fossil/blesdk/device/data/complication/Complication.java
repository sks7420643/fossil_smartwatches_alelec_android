package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.data.ComplicationEmptyDataConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.e20;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Complication extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ComplicationId id;
    @DexIgnore
    public /* final */ ComplicationDataConfig mDataConfig;
    @DexIgnore
    public ComplicationPositionConfig positionConfig;
    @DexIgnore
    public ComplicationThemeConfig themeConfig;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<Complication> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public Complication createFromParcel(Parcel parcel) {
            throw null;
            // kd4.b(parcel, "parcel");
            // String readString = parcel.readString();
            // if (readString != null) {
            //     ComplicationId valueOf = ComplicationId.valueOf(readString);
            //     parcel.setDataPosition(0);
            //     switch (e20.a[valueOf.ordinal()]) {
            //         case 1:
            //             return WeatherComplication.CREATOR.createFromParcel(parcel);
            //         case 2:
            //             return HeartRateComplication.CREATOR.createFromParcel(parcel);
            //         case 3:
            //             return StepComplication.CREATOR.createFromParcel(parcel);
            //         case 4:
            //             return DateComplication.CREATOR.createFromParcel(parcel);
            //         case 5:
            //             return ChanceOfRainComplication.CREATOR.createFromParcel(parcel);
            //         case 6:
            //             return TimeZoneTwoComplication.CREATOR.createFromParcel(parcel);
            //         case 7:
            //             return ActiveMinutesComplication.CREATOR.createFromParcel(parcel);
            //         case 8:
            //             return CaloriesComplication.CREATOR.createFromParcel(parcel);
            //         case 9:
            //             return EmptyComplication.CREATOR.createFromParcel(parcel);
            //         default:
            //             throw new NoWhenBranchMatchedException();
            //     }
            // } else {
            //     kd4.a();
            //     throw null;
            // }
        }

        @DexIgnore
        public Complication[] newArray(int i) {
            return new Complication[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Complication(ComplicationId complicationId, ComplicationDataConfig complicationDataConfig, ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig, int i, fd4 fd4) {
        this(complicationId, (i & 2) != 0 ? new ComplicationEmptyDataConfig() : complicationDataConfig, (i & 4) != 0 ? new ComplicationPositionConfig(0, 62) : complicationPositionConfig, (i & 8) != 0 ? new ComplicationThemeConfig(ComplicationThemeConfig.CREATOR.a()) : complicationThemeConfig);
    }

    @DexIgnore
    public static /* synthetic */ void mDataConfig$annotations() {
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Complication complication = (Complication) obj;
            return this.id == complication.id && !(kd4.a((Object) this.mDataConfig, (Object) complication.mDataConfig) ^ true) && !(kd4.a((Object) this.positionConfig, (Object) complication.positionConfig) ^ true) && !(kd4.a((Object) this.themeConfig, (Object) complication.themeConfig) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.Complication");
    }

    @DexIgnore
    public final ComplicationId getId() {
        return this.id;
    }

    @DexIgnore
    public final ComplicationDataConfig getMDataConfig() {
        return this.mDataConfig;
    }

    @DexIgnore
    public final ComplicationPositionConfig getPositionConfig() {
        return this.positionConfig;
    }

    @DexIgnore
    public final ComplicationThemeConfig getThemeConfig() {
        return this.themeConfig;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.id.hashCode() * 31) + this.mDataConfig.hashCode()) * 31) + this.positionConfig.hashCode()) * 31) + this.themeConfig.hashCode();
    }

    @DexIgnore
    public final void setPositionConfig$blesdk_productionRelease(ComplicationPositionConfig complicationPositionConfig) {
        kd4.b(complicationPositionConfig, "<set-?>");
        this.positionConfig = complicationPositionConfig;
    }

    @DexIgnore
    public final void setThemeConfig$blesdk_productionRelease(ComplicationThemeConfig complicationThemeConfig) {
        kd4.b(complicationThemeConfig, "<set-?>");
        this.themeConfig = complicationThemeConfig;
    }

    @DexIgnore
    public final JSONObject toJSONObject() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.id.getJsonName$blesdk_productionRelease()).put("pos", this.positionConfig.toJSONObject()).put("data", this.mDataConfig.toJSONObject()).put("theme", this.themeConfig.toJSONObject());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.id.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.mDataConfig, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.positionConfig, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.themeConfig, i);
        }
    }

    @DexIgnore
    public Complication(ComplicationId complicationId, ComplicationDataConfig complicationDataConfig, ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig) {
        kd4.b(complicationId, "id");
        kd4.b(complicationDataConfig, "dataConfig");
        kd4.b(complicationPositionConfig, "positionConfig");
        kd4.b(complicationThemeConfig, "themeConfig");
        this.id = complicationId;
        this.mDataConfig = complicationDataConfig;
        this.positionConfig = complicationPositionConfig;
        this.themeConfig = complicationThemeConfig;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Complication(Parcel parcel) {
        throw null;
        // this(r0, r2, r3, (ComplicationThemeConfig) r6);
        // kd4.b(parcel, "parcel");
        // String readString = parcel.readString();
        // if (readString != null) {
        //     ComplicationId valueOf = ComplicationId.valueOf(readString);
        //     Parcelable readParcelable = parcel.readParcelable(ComplicationDataConfig.class.getClassLoader());
        //     if (readParcelable != null) {
        //         ComplicationDataConfig complicationDataConfig = (ComplicationDataConfig) readParcelable;
        //         Parcelable readParcelable2 = parcel.readParcelable(ComplicationPositionConfig.class.getClassLoader());
        //         if (readParcelable2 != null) {
        //             ComplicationPositionConfig complicationPositionConfig = (ComplicationPositionConfig) readParcelable2;
        //             Parcelable readParcelable3 = parcel.readParcelable(ComplicationThemeConfig.class.getClassLoader());
        //             if (readParcelable3 != null) {
        //             } else {
        //                 kd4.a();
        //                 throw null;
        //             }
        //         } else {
        //             kd4.a();
        //             throw null;
        //         }
        //     } else {
        //         kd4.a();
        //         throw null;
        //     }
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
