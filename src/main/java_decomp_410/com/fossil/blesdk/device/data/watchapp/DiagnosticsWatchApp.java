package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DiagnosticsWatchApp extends WatchApp {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DiagnosticsWatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DiagnosticsWatchApp createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DiagnosticsWatchApp(parcel, (fd4) null);
        }

        @DexIgnore
        public DiagnosticsWatchApp[] newArray(int i) {
            return new DiagnosticsWatchApp[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DiagnosticsWatchApp(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public DiagnosticsWatchApp() {
        super(WatchAppId.DIAGNOSTICS, (ButtonEvent) null, (WatchAppDataConfig) null, 6, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DiagnosticsWatchApp(ButtonEvent buttonEvent) {
        super(WatchAppId.DIAGNOSTICS, buttonEvent, (WatchAppDataConfig) null, 4, (fd4) null);
        kd4.b(buttonEvent, "buttonEvent");
    }

    @DexIgnore
    public DiagnosticsWatchApp(Parcel parcel) {
        super(parcel);
    }
}
