package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.CurrentHeartRateConfig;
import com.fossil.blesdk.obfuscated.je4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.md4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.FunctionReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_2_0_SUPPORTED_CONFIG$Anon1 extends FunctionReference implements xc4<byte[], CurrentHeartRateConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_2_0_SUPPORTED_CONFIG$Anon1(CurrentHeartRateConfig.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final je4 getOwner() {
        return md4.a(CurrentHeartRateConfig.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/CurrentHeartRateConfig;";
    }

    @DexIgnore
    public final CurrentHeartRateConfig invoke(byte[] bArr) {
        kd4.b(bArr, "p1");
        return ((CurrentHeartRateConfig.a) this.receiver).a(bArr);
    }
}
