package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EmptyWatchApp extends WatchApp {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<EmptyWatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public EmptyWatchApp createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new EmptyWatchApp(parcel, (fd4) null);
        }

        @DexIgnore
        public EmptyWatchApp[] newArray(int i) {
            return new EmptyWatchApp[i];
        }
    }

    @DexIgnore
    public /* synthetic */ EmptyWatchApp(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public EmptyWatchApp() {
        super(WatchAppId.EMPTY, (ButtonEvent) null, (WatchAppDataConfig) null, 6, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EmptyWatchApp(ButtonEvent buttonEvent) {
        super(WatchAppId.EMPTY, buttonEvent, (WatchAppDataConfig) null, 4, (fd4) null);
        kd4.b(buttonEvent, "buttonEvent");
    }

    @DexIgnore
    public EmptyWatchApp(Parcel parcel) {
        super(parcel);
    }
}
