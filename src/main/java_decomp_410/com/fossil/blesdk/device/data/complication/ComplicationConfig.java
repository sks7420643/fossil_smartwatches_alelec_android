package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.preset.DevicePresetItem;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.HashSet;
import java.util.Iterator;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationConfig extends DevicePresetItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ HashSet<Complication> complicationAssignment;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ComplicationConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public ComplicationConfig[] newArray(int i) {
            return new ComplicationConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void bottomFace$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void leftFace$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void rightFace$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void topFace$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) ComplicationConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.complicationAssignment, (Object) ((ComplicationConfig) obj).complicationAssignment) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.ComplicationConfig");
    }

    @DexIgnore
    public JSONObject getAssignmentJSON$blesdk_productionRelease() {
        throw null;
        // JSONArray jSONArray = new JSONArray();
        // for (Complication jSONObject : this.complicationAssignment) {
        //     jSONArray.put(jSONObject.toJSONObject());
        // }
        // JSONObject put = new JSONObject().put("watchFace._.config.comps", jSONArray);
        // kd4.a((Object) put, "JSONObject().put(UIScrip\u2026ationAssignmentJsonArray)");
        // return put;
    }

    @DexIgnore
    public final Complication getBottomFace() {
        Complication t;
        boolean z;
        Iterator<Complication> it = this.complicationAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((Complication) t).getPositionConfig().getAngle() == 180) {
                z = true;
                // continue;
            } else {
                z = false;
                // continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (Complication) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Complication getLeftFace() {
        Complication t;
        boolean z;
        Iterator<Complication> it = this.complicationAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((Complication) t).getPositionConfig().getAngle() == 270) {
                z = true;
                // continue;
            } else {
                z = false;
                // continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (Complication) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Complication getRightFace() {
        Complication t;
        boolean z;
        Iterator<Complication> it = this.complicationAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((Complication) t).getPositionConfig().getAngle() == 90) {
                z = true;
                // continue;
            } else {
                z = false;
                // continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (Complication) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final Complication getTopFace() {
        Complication t;
        boolean z;
        Iterator<Complication> it = this.complicationAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((Complication) t).getPositionConfig().getAngle() == 0) {
                z = true;
                // continue;
            } else {
                z = false;
                // continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (Complication) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.complicationAssignment.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return getAssignmentJSON$blesdk_productionRelease();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.complicationAssignment.toArray(new Complication[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public ComplicationConfig(Complication[] complicationArr) {
        kd4.b(complicationArr, "complicationAssignment");
        this.complicationAssignment = new HashSet<>();
        hb4.a(this.complicationAssignment, (Complication[]) complicationArr);
    }

    @DexIgnore
    public ComplicationConfig(Complication complication, Complication complication2, Complication complication3, Complication complication4) {
        kd4.b(complication, "topFace");
        kd4.b(complication2, "rightFace");
        kd4.b(complication3, "bottomFace");
        kd4.b(complication4, "leftFace");
        this.complicationAssignment = new HashSet<>();
        complication.setPositionConfig$blesdk_productionRelease(new ComplicationPositionConfig(0, 62));
        complication2.setPositionConfig$blesdk_productionRelease(new ComplicationPositionConfig(90, 62));
        complication3.setPositionConfig$blesdk_productionRelease(new ComplicationPositionConfig((int) BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE, 62));
        complication4.setPositionConfig$blesdk_productionRelease(new ComplicationPositionConfig((int) BackgroundImageConfig.LEFT_BACKGROUND_ANGLE, 62));
        this.complicationAssignment.add(complication);
        this.complicationAssignment.add(complication2);
        this.complicationAssignment.add(complication3);
        this.complicationAssignment.add(complication4);
    }

    @DexIgnore
    public ComplicationConfig(Parcel parcel) {
        super(parcel);
        this.complicationAssignment = new HashSet<>();
        HashSet<Complication> hashSet = this.complicationAssignment;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(Complication.class.getClassLoader());
        if (readParcelableArray != null) {
            hb4.a(hashSet, (Object[]) (Complication[]) readParcelableArray);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.complication.Complication>");
    }
}
