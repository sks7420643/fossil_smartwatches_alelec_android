package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g20;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BiometricProfile extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_AGE; // = 110;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_HEIGHT_IN_CENTIMETER; // = 250;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_WEIGHT_IN_KILOGRAM; // = 250;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_AGE; // = 8;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_HEIGHT_IN_CENTIMETER; // = 100;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_WEIGHT_IN_KILOGRAM; // = 35;
    @DexIgnore
    public /* final */ byte age;
    @DexIgnore
    public /* final */ Gender gender;
    @DexIgnore
    public /* final */ short heightInCentimeter;
    @DexIgnore
    public /* final */ WearingPosition wearingPosition;
    @DexIgnore
    public /* final */ short weightInKilogram;

    @DexIgnore
    public enum Gender {
        UNSPECIFIED((byte) 0),
        MALE((byte) 1),
        FEMALE((byte) 2);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ byte id;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final Gender a(byte b) throws IllegalArgumentException {
                Gender gender;
                Gender[] values = Gender.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        gender = null;
                        break;
                    }
                    gender = values[i];
                    if (gender.getId$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                if (gender != null) {
                    return gender;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        Gender(byte b) {
            this.id = b;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final com.fossil.fitness.Gender toFitnessAlgorithmGender$blesdk_productionRelease() {
            throw null;
            // int i = g20.a[ordinal()];
            // if (i == 1) {
            //     return com.fossil.fitness.Gender.UNSPECIFIED;
            // }
            // if (i == 2) {
            //     return com.fossil.fitness.Gender.MALE;
            // }
            // if (i == 3) {
            //     return com.fossil.fitness.Gender.FEMALE;
            // }
            // throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public enum WearingPosition {
        UNSPECIFIED((byte) 0),
        LEFT_WRIST((byte) 1),
        RIGHT_WRIST((byte) 2),
        UNSPECIFIED_WRIST((byte) 3);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ byte id;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final WearingPosition a(byte b) throws IllegalArgumentException {
                WearingPosition wearingPosition;
                WearingPosition[] values = WearingPosition.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        wearingPosition = null;
                        break;
                    }
                    wearingPosition = values[i];
                    if (wearingPosition.getId$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                if (wearingPosition != null) {
                    return wearingPosition;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        WearingPosition(byte b) {
            this.id = b;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BiometricProfile> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final BiometricProfile a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 7) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new BiometricProfile(order.get(0), Gender.Companion.a(order.get(1)), order.getShort(2), order.getShort(4), WearingPosition.Companion.a(order.get(6)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 7");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BiometricProfile createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BiometricProfile(parcel, (fd4) null);
        }

        @DexIgnore
        public BiometricProfile[] newArray(int i) {
            return new BiometricProfile[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BiometricProfile(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BiometricProfile.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BiometricProfile biometricProfile = (BiometricProfile) obj;
            return this.age == biometricProfile.age && this.gender == biometricProfile.gender && this.heightInCentimeter == biometricProfile.heightInCentimeter && this.weightInKilogram == biometricProfile.weightInKilogram && this.wearingPosition == biometricProfile.wearingPosition;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BiometricProfile");
    }

    @DexIgnore
    public final byte getAge() {
        return this.age;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(7).order(ByteOrder.LITTLE_ENDIAN).put(this.age).put(this.gender.getId$blesdk_productionRelease()).putShort(this.heightInCentimeter).putShort(this.weightInKilogram).put(this.wearingPosition.getId$blesdk_productionRelease()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final Gender getGender() {
        return this.gender;
    }

    @DexIgnore
    public final short getHeightInCentimeter() {
        return this.heightInCentimeter;
    }

    @DexIgnore
    public final WearingPosition getWearingPosition() {
        return this.wearingPosition;
    }

    @DexIgnore
    public final short getWeightInKilogram() {
        return this.weightInKilogram;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.age * 31) + this.gender.hashCode()) * 31) + this.heightInCentimeter) * 31) + this.weightInKilogram) * 31) + this.wearingPosition.hashCode();
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        byte b = this.age;
        boolean z = true;
        if (8 <= b && 110 >= b) {
            short s = this.heightInCentimeter;
            if (100 <= s && 250 >= s) {
                short s2 = this.weightInKilogram;
                if (35 > s2 || 250 < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException("weightInKilogram (" + this.weightInKilogram + ") is out of range " + "[35, 250]" + " (in kilogram).");
                }
                return;
            }
            throw new IllegalArgumentException("heightInCentimeter (" + this.heightInCentimeter + ") is out of range " + "[100, 250]" + " (in centimeter.");
        }
        throw new IllegalArgumentException("age (" + this.age + ") is out of range [8, " + "110].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.age);
        }
        if (parcel != null) {
            parcel.writeString(this.gender.name());
        }
        if (parcel != null) {
            parcel.writeInt(n90.b(this.heightInCentimeter));
        }
        if (parcel != null) {
            parcel.writeInt(n90.b(this.weightInKilogram));
        }
        if (parcel != null) {
            parcel.writeString(this.wearingPosition.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BiometricProfile(byte b, Gender gender2, short s, short s2, WearingPosition wearingPosition2) throws IllegalArgumentException {
        super(DeviceConfigKey.BIOMETRIC_PROFILE);
        kd4.b(gender2, "gender");
        kd4.b(wearingPosition2, "wearingPosition");
        this.age = b;
        this.gender = gender2;
        this.heightInCentimeter = s;
        this.weightInKilogram = s2;
        this.wearingPosition = wearingPosition2;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("age", Byte.valueOf(this.age));
            jSONObject.put("gender", this.gender.getLogName$blesdk_productionRelease());
            jSONObject.put("height_in_centimeter", Short.valueOf(this.heightInCentimeter));
            jSONObject.put("weight_in_kilogram", Short.valueOf(this.weightInKilogram));
            jSONObject.put("wearing_position", this.wearingPosition.getLogName$blesdk_productionRelease());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public BiometricProfile(Parcel parcel) {
        super(parcel);
        this.age = parcel.readByte();
        String readString = parcel.readString();
        if (readString != null) {
            this.gender = Gender.valueOf(readString);
            this.heightInCentimeter = (short) parcel.readInt();
            this.weightInKilogram = (short) parcel.readInt();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                this.wearingPosition = WearingPosition.valueOf(readString2);
                j();
                return;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }
}
