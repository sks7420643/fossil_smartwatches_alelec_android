package com.fossil.blesdk.device.data.config.builder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceConfigBuilder$addConfig$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.data.config.DeviceConfigItem, java.lang.Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.data.config.DeviceConfigItem $config;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceConfigBuilder$addConfig$1(com.fossil.blesdk.device.data.config.DeviceConfigItem deviceConfigItem) {
        super(1);
        this.$config = deviceConfigItem;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.fossil.blesdk.device.data.config.DeviceConfigItem) obj));
    }

    @DexIgnore
    public final boolean invoke(com.fossil.blesdk.device.data.config.DeviceConfigItem deviceConfigItem) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceConfigItem, "deviceConfigItem");
        return deviceConfigItem.getKey() == this.$config.getKey();
    }
}
