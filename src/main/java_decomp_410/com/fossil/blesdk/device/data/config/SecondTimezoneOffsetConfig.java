package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SecondTimezoneOffsetConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ short DISABLE_TIMEZONE_OFFSET_IN_MINUTE; // = 1024;
    @DexIgnore
    @Keep
    public static /* final */ short MAXIMUM_TIMEZONE_OFFSET_IN_MINUTE; // = 720;
    @DexIgnore
    @Keep
    public static /* final */ short MINIMUM_TIMEZONE_OFFSET_IN_MINUTE; // = -720;
    @DexIgnore
    public /* final */ short secondTimezoneOffsetInMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<SecondTimezoneOffsetConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final SecondTimezoneOffsetConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 2) {
                return new SecondTimezoneOffsetConfig(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", " + "require: 2");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public SecondTimezoneOffsetConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new SecondTimezoneOffsetConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public SecondTimezoneOffsetConfig[] newArray(int i) {
            return new SecondTimezoneOffsetConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ SecondTimezoneOffsetConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) SecondTimezoneOffsetConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.secondTimezoneOffsetInMinute == ((SecondTimezoneOffsetConfig) obj).secondTimezoneOffsetInMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(this.secondTimezoneOffsetInMinute).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final short getSecondTimezoneOffsetInMinute() {
        return this.secondTimezoneOffsetInMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.secondTimezoneOffsetInMinute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        short s = this.secondTimezoneOffsetInMinute;
        if (!(s == 1024 || (-720 <= s && 720 >= s))) {
            throw new IllegalArgumentException("secondTimezoneOffsetInMinute (" + this.secondTimezoneOffsetInMinute + ") " + " must be equal to 1024 " + " or in range " + "[-720, " + "720].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(n90.b(this.secondTimezoneOffsetInMinute));
        }
    }

    @DexIgnore
    public SecondTimezoneOffsetConfig(short s) {
        super(DeviceConfigKey.SECOND_TIMEZONE_OFFSET);
        this.secondTimezoneOffsetInMinute = s;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            wa0.a(jSONObject, JSONKey.TIMEZONE_OFFSET_IN_MINUTE, Short.valueOf(this.secondTimezoneOffsetInMinute));
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public SecondTimezoneOffsetConfig(Parcel parcel) {
        super(parcel);
        this.secondTimezoneOffsetInMinute = (short) parcel.readInt();
        j();
    }
}
