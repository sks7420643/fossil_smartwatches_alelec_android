package com.fossil.blesdk.device.data.enumerate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum WeatherCondition {
    CLEAR_DAY,
    CLEAR_NIGHT,
    CLOUDY,
    PARTLY_CLOUDY_DAY,
    PARTLY_CLOUDY_NIGHT,
    RAIN,
    SNOW,
    SLEET,
    STORMY,
    FOG,
    WIND;
    
    @DexIgnore
    public /* final */ int id;
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final int getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
