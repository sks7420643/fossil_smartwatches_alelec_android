package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MusicWatchApp extends WatchApp {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MusicWatchApp> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MusicWatchApp createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MusicWatchApp(parcel, (fd4) null);
        }

        @DexIgnore
        public MusicWatchApp[] newArray(int i) {
            return new MusicWatchApp[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MusicWatchApp(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public MusicWatchApp() {
        super(WatchAppId.MUSIC, (ButtonEvent) null, (WatchAppDataConfig) null, 6, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicWatchApp(ButtonEvent buttonEvent) {
        super(WatchAppId.MUSIC, buttonEvent, (WatchAppDataConfig) null, 4, (fd4) null);
        kd4.b(buttonEvent, "buttonEvent");
    }

    @DexIgnore
    public MusicWatchApp(Parcel parcel) {
        super(parcel);
    }
}
