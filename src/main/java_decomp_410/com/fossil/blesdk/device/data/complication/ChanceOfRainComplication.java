package com.fossil.blesdk.device.data.complication;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig;
import com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig;
import com.fossil.blesdk.model.complication.config.theme.ComplicationThemeConfig;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ChanceOfRainComplication extends Complication {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ChanceOfRainComplication> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ChanceOfRainComplication createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ChanceOfRainComplication(parcel, (fd4) null);
        }

        @DexIgnore
        public ChanceOfRainComplication[] newArray(int i) {
            return new ChanceOfRainComplication[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ChanceOfRainComplication(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public ChanceOfRainComplication() {
        super(ComplicationId.CHANCE_OF_RAIN, (ComplicationDataConfig) null, (ComplicationPositionConfig) null, (ComplicationThemeConfig) null, 14, (fd4) null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ChanceOfRainComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig, int i, fd4 fd4) {
        this(complicationPositionConfig, (i & 2) != 0 ? new ComplicationThemeConfig(ComplicationThemeConfig.CREATOR.a()) : complicationThemeConfig);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplication(ComplicationPositionConfig complicationPositionConfig, ComplicationThemeConfig complicationThemeConfig) {
        super(ComplicationId.CHANCE_OF_RAIN, (ComplicationDataConfig) null, complicationPositionConfig, complicationThemeConfig, 2, (fd4) null);
        kd4.b(complicationPositionConfig, "positionConfig");
        kd4.b(complicationThemeConfig, "themeConfig");
    }

    @DexIgnore
    public ChanceOfRainComplication(Parcel parcel) {
        super(parcel);
    }
}
