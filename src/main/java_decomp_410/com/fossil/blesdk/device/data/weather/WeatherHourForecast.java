package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.WeatherCondition;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import java.io.Serializable;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherHourForecast extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_HOUR; // = 23;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_HOUR; // = 0;
    @DexIgnore
    public /* final */ int hourIn24Format;
    @DexIgnore
    public /* final */ float temperature;
    @DexIgnore
    public /* final */ WeatherCondition weatherCondition;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherHourForecast> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherHourForecast createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherHourForecast(parcel, (fd4) null);
        }

        @DexIgnore
        public WeatherHourForecast[] newArray(int i) {
            return new WeatherHourForecast[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherHourForecast(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void hourIn24Format$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void temperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void weatherCondition$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) WeatherHourForecast.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WeatherHourForecast weatherHourForecast = (WeatherHourForecast) obj;
            return this.hourIn24Format == weatherHourForecast.hourIn24Format && this.temperature == weatherHourForecast.temperature && this.weatherCondition == weatherHourForecast.weatherCondition;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherHourForecast");
    }

    @DexIgnore
    public final int getHourIn24Format() {
        return this.hourIn24Format;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() throws JSONException {
        JSONObject put = new JSONObject().put(AppFilter.COLUMN_HOUR, this.hourIn24Format).put("temp", Float.valueOf(this.temperature)).put("cond_id", this.weatherCondition.getId$blesdk_productionRelease());
        kd4.a((Object) put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.temperature;
    }

    @DexIgnore
    public final WeatherCondition getWeatherCondition() {
        return this.weatherCondition;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.hourIn24Format * 31) + Float.valueOf(this.temperature).hashCode()) * 31) + this.weatherCondition.hashCode();
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        int i = this.hourIn24Format;
        if (!(i >= 0 && 23 >= i)) {
            throw new IllegalArgumentException("hourIn24Format(" + this.hourIn24Format + ") is out of range " + "[0, 23].");
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.HOUR, Integer.valueOf(this.hourIn24Format)), JSONKey.TEMPERATURE, Float.valueOf(this.temperature)), JSONKey.WEATHER_CONDITION, this.weatherCondition.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.hourIn24Format);
        }
        if (parcel != null) {
            parcel.writeFloat(this.temperature);
        }
        if (parcel != null) {
            parcel.writeString(this.weatherCondition.name());
        }
    }

    @DexIgnore
    public WeatherHourForecast(int i, float f, WeatherCondition weatherCondition2) throws IllegalArgumentException {
        kd4.b(weatherCondition2, "weatherCondition");
        this.hourIn24Format = i;
        this.temperature = n90.a(f, 2);
        this.weatherCondition = weatherCondition2;
        i();
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WeatherHourForecast(Parcel parcel) {
        throw null;
/*        this(r0, r1, WeatherCondition.valueOf(r3));
        int readInt = parcel.readInt();
        float readFloat = parcel.readFloat();
        String readString = parcel.readString();
        if (readString != null) {
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
