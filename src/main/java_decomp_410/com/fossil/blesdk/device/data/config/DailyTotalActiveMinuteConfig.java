package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.od4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyTotalActiveMinuteConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MINUTE; // = 0;
    @DexIgnore
    public static /* final */ int e; // = n90.a(od4.a);
    @DexIgnore
    public /* final */ int minute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailyTotalActiveMinuteConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailyTotalActiveMinuteConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 2) {
                return new DailyTotalActiveMinuteConfig(n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 2");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DailyTotalActiveMinuteConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DailyTotalActiveMinuteConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public DailyTotalActiveMinuteConfig[] newArray(int i) {
            return new DailyTotalActiveMinuteConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailyTotalActiveMinuteConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DailyTotalActiveMinuteConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.minute == ((DailyTotalActiveMinuteConfig) obj).minute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.minute).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public int hashCode() {
        return this.minute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        int i = e;
        int i2 = this.minute;
        if (!(i2 >= 0 && i >= i2)) {
            throw new IllegalArgumentException("minute(" + this.minute + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.minute);
        }
    }

    @DexIgnore
    public DailyTotalActiveMinuteConfig(int i) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE);
        this.minute = i;
        j();
    }

    @DexIgnore
    public Integer valueDescription() {
        return Integer.valueOf(this.minute);
    }

    @DexIgnore
    public DailyTotalActiveMinuteConfig(Parcel parcel) {
        super(parcel);
        this.minute = parcel.readInt();
        j();
    }
}
