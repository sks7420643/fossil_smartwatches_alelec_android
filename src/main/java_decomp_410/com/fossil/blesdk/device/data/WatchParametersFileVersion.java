package com.fossil.blesdk.device.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchParametersFileVersion extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Version current;
    @DexIgnore
    public /* final */ Version supported;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchParametersFileVersion> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WatchParametersFileVersion createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WatchParametersFileVersion(parcel, (fd4) null);
        }

        @DexIgnore
        public WatchParametersFileVersion[] newArray(int i) {
            return new WatchParametersFileVersion[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WatchParametersFileVersion(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) WatchParametersFileVersion.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WatchParametersFileVersion watchParametersFileVersion = (WatchParametersFileVersion) obj;
            return !(kd4.a((Object) this.current, (Object) watchParametersFileVersion.current) ^ true) && !(kd4.a((Object) this.supported, (Object) watchParametersFileVersion.supported) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.WatchParametersFileVersion");
    }

    @DexIgnore
    public final Version getCurrent() {
        return this.current;
    }

    @DexIgnore
    public final Version getSupported() {
        return this.supported;
    }

    @DexIgnore
    public int hashCode() {
        return (this.current.hashCode() * 31) + this.supported.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.CURRENT_VERSION, this.current), JSONKey.SUPPORTED_VERSION, this.supported);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.current, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.supported, i);
        }
    }

    @DexIgnore
    public WatchParametersFileVersion(Version version, Version version2) {
        kd4.b(version, "current");
        kd4.b(version2, "supported");
        this.current = version;
        this.supported = version2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WatchParametersFileVersion(Parcel parcel) {
        throw null;
        // this(r0, (Version) r4);
        // Parcelable readParcelable = parcel.readParcelable(Version.class.getClassLoader());
        // if (readParcelable != null) {
        //     Version version = (Version) readParcelable;
        //     Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
        //     if (readParcelable2 != null) {
        //     } else {
        //         kd4.a();
        //         throw null;
        //     }
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
