package com.fossil.blesdk.device.data.notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum NotificationEntryId {
    APPLICATION_NAME((byte) 1),
    SENDER_NAME((byte) 2),
    APP_BUNDLE_CRC32((byte) 4),
    GROUP_ID((byte) 128),
    APP_DISPLAY_NAME((byte) 129),
    ICON_IMAGE((byte) 130),
    PRIORITY((byte) 193),
    HAND_MOVING((byte) 194),
    VIBE((byte) 195);
    
    @DexIgnore
    public /* final */ byte id;

    @DexIgnore
    NotificationEntryId(byte b) {
        this.id = b;
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }
}
