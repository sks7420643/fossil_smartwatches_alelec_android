package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1 */
public final class C0872xafd8eb62 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a */
    public static final class C0873a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0872xafd8eb62 f2507e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f2508f;

        @DexIgnore
        public C0873a(com.fossil.blesdk.device.C0872xafd8eb62 deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1, java.lang.Object obj) {
            this.f2507e = deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2508f = obj;
        }

        @DexIgnore
        public final void run() {
            this.f2507e.$progressTask.mo16948c(this.f2508f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b */
    public static final class C0874b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0872xafd8eb62 f2509e;

        @DexIgnore
        public C0874b(com.fossil.blesdk.device.C0872xafd8eb62 deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1) {
            this.f2509e = deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
        }

        @DexIgnore
        public final void run() {
            this.f2509e.$progressTask.mo16946b(new com.fossil.blesdk.device.FeatureError(com.fossil.blesdk.device.FeatureErrorCode.UNKNOWN_ERROR, (com.fossil.blesdk.device.logic.phase.Phase.Result) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c */
    public static final class C0875c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0872xafd8eb62 f2510e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2511f;

        @DexIgnore
        public C0875c(com.fossil.blesdk.device.C0872xafd8eb62 deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1, com.fossil.blesdk.device.logic.phase.Phase phase) {
            this.f2510e = deviceImplementation$setLocalizationFile$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2511f = phase;
        }

        @DexIgnore
        public final void run() {
            this.f2510e.$progressTask.mo16946b(com.fossil.blesdk.device.FeatureError.Companion.mo6343a(this.f2511f.mo7565k(), com.fossil.blesdk.obfuscated.rb4.m27313a((kotlin.Pair<? extends K, ? extends V>[]) new kotlin.Pair[]{com.fossil.blesdk.obfuscated.oa4.m26076a(com.fossil.blesdk.device.FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, java.lang.Boolean.valueOf(this.f2510e.this$0.mo5950q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0872xafd8eb62(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        if (phase.mo7565k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            java.lang.Object i = phase.mo7345i();
            if (i instanceof java.lang.String) {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0872xafd8eb62.C0873a(this, i));
            } else {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0872xafd8eb62.C0874b(this));
            }
        } else {
            this.this$0.f2346f.post(new com.fossil.blesdk.device.C0872xafd8eb62.C0875c(this, phase));
            this.this$0.mo5919a(phase.mo7565k());
        }
    }
}
