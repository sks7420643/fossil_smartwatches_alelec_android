package com.fossil.blesdk.device;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qf4;
import com.fossil.blesdk.obfuscated.y00;
import java.lang.reflect.Type;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceType {
    UNKNOWN(y00.f.h()),
    DIANA(y00.f.b()),
    SE1(y00.f.f()),
    SLIM(y00.f.g()),
    MINI(y00.f.e()),
    HELLAS(y00.f.c());
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ Type[] supportedFeature;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DeviceType a(String str) {
            throw null;
            // kd4.b(str, "modelNumber");
            // if (qf4.c(str, "DN", false, 2, (Object) null)) {
            //     return DeviceType.DIANA;
            // }
            // if (qf4.c(str, "HW", false, 2, (Object) null)) {
            //     return DeviceType.SE1;
            // }
            // if (qf4.c(str, "HL", false, 2, (Object) null)) {
            //     return DeviceType.SLIM;
            // }
            // if (qf4.c(str, "HM", false, 2, (Object) null)) {
            //     return DeviceType.MINI;
            // }
            // if (qf4.c(str, "AW", false, 2, (Object) null)) {
            //     return DeviceType.HELLAS;
            // }
            // return DeviceType.UNKNOWN;
        }

        @DexIgnore
        public final DeviceType b(String str) {
            throw null;
            // kd4.b(str, "serialNumber");
            // if (!DeviceInformation.Companion.a(str)) {
            //     return DeviceType.UNKNOWN;
            // }
            // if (qf4.c(str, "D", false, 2, (Object) null)) {
            //     return DeviceType.DIANA;
            // }
            // if (qf4.c(str, "W", false, 2, (Object) null)) {
            //     return DeviceType.SE1;
            // }
            // if (qf4.c(str, "L", false, 2, (Object) null)) {
            //     return DeviceType.SLIM;
            // }
            // if (qf4.c(str, "M", false, 2, (Object) null)) {
            //     return DeviceType.MINI;
            // }
            // if (qf4.c(str, "Y", false, 2, (Object) null)) {
            //     return DeviceType.HELLAS;
            // }
            // return DeviceType.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    DeviceType(Type[] typeArr) {
        this.supportedFeature = typeArr;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final Type[] getSupportedFeature$blesdk_productionRelease() {
        return this.supportedFeature;
    }
}
