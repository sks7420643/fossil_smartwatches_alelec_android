package com.fossil.blesdk.device;

import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.lb0;
import com.fossil.blesdk.obfuscated.n00;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import java.util.HashMap;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements xc4<qa4, qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1 connectionManager$checkAndMaintainDeviceHIDConnection$Anon1) {
            super(1);
            this.this$Anon0 = connectionManager$checkAndMaintainDeviceHIDConnection$Anon1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((qa4) obj);
            return qa4.a;
        }

        @DexIgnore
        public final void invoke(qa4 qa4) {
            kd4.b(qa4, "it");
            ConnectionManager.e.remove(this.this$Anon0.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2 extends Lambda implements xc4<Error, qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1 this$Anon0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2(ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1 connectionManager$checkAndMaintainDeviceHIDConnection$Anon1) {
            super(1);
            this.this$Anon0 = connectionManager$checkAndMaintainDeviceHIDConnection$Anon1;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Error) obj);
            return qa4.a;
        }

        @DexIgnore
        public final void invoke(Error error) {
            kd4.b(error, "error");
            ConnectionManager.e.remove(this.this$Anon0.e);
            FeatureError featureError = (FeatureError) error;
            if (featureError.getErrorCode() == FeatureErrorCode.REQUEST_UNSUPPORTED) {
                ConnectionManager.j.g(this.this$Anon0.e);
                this.this$Anon0.e.a(error);
            } else if (featureError.getErrorCode() == FeatureErrorCode.REQUEST_FAILED && featureError.getPhaseResult$blesdk_productionRelease().getResultCode() == Phase.Result.ResultCode.HID_INPUT_DEVICE_DISABLED) {
                ConnectionManager.d.add(this.this$Anon0.e);
            } else {
                ConnectionManager.j.a(this.this$Anon0.e, lb0.b.a("HID_EXPONENT_BACK_OFF_TAG"));
            }
        }
    }

    @DexIgnore
    public ConnectionManager$checkAndMaintainDeviceHIDConnection$Anon1(DeviceImplementation deviceImplementation) {
        this.e = deviceImplementation;
    }

    @DexIgnore
    public final void run() {
        ConnectionManager.d.remove(this.e);
        if (ConnectionManager.j.e(this.e) && BluetoothLeAdapter.l.c() == BluetoothLeAdapter.State.ENABLED) {
            if (n00.c[this.e.o().ordinal()] == 1 && ConnectionManager.e.add(this.e)) {
                DeviceImplementation.a(this.e, (HashMap) null, 1, (Object) null).e(new Anon1(this)).c(new Anon2(this));
            }
        }
    }
}
