package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setLocalizationFile$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$setLocalizationFile$1(com.fossil.blesdk.device.DeviceImplementation deviceImplementation) {
        super(1);
        this.this$0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.device.logic.phase.Phase phase2 = phase;
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase2, "executedPhase");
        com.fossil.blesdk.obfuscated.q50 q50 = (com.fossil.blesdk.obfuscated.q50) phase2;
        com.fossil.blesdk.device.DeviceImplementation deviceImplementation = this.this$0;
        deviceImplementation.f2357q = com.fossil.blesdk.device.DeviceInformation.copy$default(deviceImplementation.getDeviceInformation(), (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (com.fossil.blesdk.device.data.Version) null, (com.fossil.blesdk.device.data.Version) null, (com.fossil.blesdk.device.data.Version) null, (java.util.LinkedHashMap) null, (java.util.LinkedHashMap) null, (com.fossil.blesdk.device.DeviceInformation.BondRequirement) null, (com.fossil.blesdk.device.data.config.DeviceConfigKey[]) null, (com.fossil.blesdk.device.data.Version) null, q50.mo15036O().getLocaleString(), (com.fossil.blesdk.device.data.Version) null, 98303, (java.lang.Object) null);
        this.this$0.getDeviceInformation().getCurrentFilesVersion$blesdk_productionRelease().put((short) 1794, q50.mo15036O().getFileVersion$blesdk_productionRelease());
    }
}
