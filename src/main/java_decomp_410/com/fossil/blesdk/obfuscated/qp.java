package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DecodeJob;
import com.fossil.blesdk.obfuscated.ar;
import com.fossil.blesdk.obfuscated.tq;
import com.fossil.blesdk.obfuscated.vp;
import com.fossil.blesdk.obfuscated.vw;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qp implements sp, ar.a, vp.a {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);
    @DexIgnore
    public /* final */ xp a;
    @DexIgnore
    public /* final */ up b;
    @DexIgnore
    public /* final */ ar c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ dq e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ a g;
    @DexIgnore
    public /* final */ ip h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ DecodeJob.e a;
        @DexIgnore
        public /* final */ g8<DecodeJob<?>> b; // = vw.a(150, new C0031a());
        @DexIgnore
        public int c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qp$a$a  reason: collision with other inner class name */
        public class C0031a implements vw.d<DecodeJob<?>> {
            @DexIgnore
            public C0031a() {
            }

            @DexIgnore
            public DecodeJob<?> a() {
                a aVar = a.this;
                return new DecodeJob<>(aVar.a, aVar.b);
            }
        }

        @DexIgnore
        public a(DecodeJob.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public <R> DecodeJob<R> a(tn tnVar, Object obj, tp tpVar, jo joVar, int i, int i2, Class<?> cls, Class<R> cls2, Priority priority, pp ppVar, Map<Class<?>, oo<?>> map, boolean z, boolean z2, boolean z3, lo loVar, DecodeJob.b<R> bVar) {
            DecodeJob<R> a2 = this.b.a();
            tw.a(a2);
            DecodeJob<R> decodeJob = a2;
            int i3 = this.c;
            int i4 = i3;
            this.c = i3 + 1;
            decodeJob.a(tnVar, obj, tpVar, joVar, i, i2, cls, cls2, priority, ppVar, map, z, z2, z3, loVar, bVar, i4);
            return decodeJob;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ dr a;
        @DexIgnore
        public /* final */ dr b;
        @DexIgnore
        public /* final */ dr c;
        @DexIgnore
        public /* final */ dr d;
        @DexIgnore
        public /* final */ sp e;
        @DexIgnore
        public /* final */ vp.a f;
        @DexIgnore
        public /* final */ g8<rp<?>> g; // = vw.a(150, new a());

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements vw.d<rp<?>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public rp<?> a() {
                b bVar = b.this;
                return new rp(bVar.a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        @DexIgnore
        public b(dr drVar, dr drVar2, dr drVar3, dr drVar4, sp spVar, vp.a aVar) {
            this.a = drVar;
            this.b = drVar2;
            this.c = drVar3;
            this.d = drVar4;
            this.e = spVar;
            this.f = aVar;
        }

        @DexIgnore
        public <R> rp<R> a(jo joVar, boolean z, boolean z2, boolean z3, boolean z4) {
            rp<R> a2 = this.g.a();
            tw.a(a2);
            rp<R> rpVar = a2;
            rpVar.a(joVar, z, z2, z3, z4);
            return rpVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements DecodeJob.e {
        @DexIgnore
        public /* final */ tq.a a;
        @DexIgnore
        public volatile tq b;

        @DexIgnore
        public c(tq.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public tq a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new uq();
                    }
                }
            }
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d {
        @DexIgnore
        public /* final */ rp<?> a;
        @DexIgnore
        public /* final */ sv b;

        @DexIgnore
        public d(sv svVar, rp<?> rpVar) {
            this.b = svVar;
            this.a = rpVar;
        }

        @DexIgnore
        public void a() {
            synchronized (qp.this) {
                this.a.c(this.b);
            }
        }
    }

    @DexIgnore
    public qp(ar arVar, tq.a aVar, dr drVar, dr drVar2, dr drVar3, dr drVar4, boolean z) {
        this(arVar, aVar, drVar, drVar2, drVar3, drVar4, (xp) null, (up) null, (ip) null, (b) null, (a) null, (dq) null, z);
    }

    @DexIgnore
    public <R> d a(tn tnVar, Object obj, jo joVar, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, pp ppVar, Map<Class<?>, oo<?>> map, boolean z, boolean z2, lo loVar, boolean z3, boolean z4, boolean z5, boolean z6, sv svVar, Executor executor) {
        long a2 = i ? pw.a() : 0;
        tp a3 = this.b.a(obj, joVar, i2, i3, map, cls, cls2, loVar);
        synchronized (this) {
            vp<?> a4 = a(a3, z3, a2);
            if (a4 == null) {
                d a5 = a(tnVar, obj, joVar, i2, i3, cls, cls2, priority, ppVar, map, z, z2, loVar, z3, z4, z5, z6, svVar, executor, a3, a2);
                return a5;
            }
            svVar.a(a4, DataSource.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final vp<?> b(jo joVar) {
        vp<?> b2 = this.h.b(joVar);
        if (b2 != null) {
            b2.d();
        }
        return b2;
    }

    @DexIgnore
    public final vp<?> c(jo joVar) {
        vp<?> a2 = a(joVar);
        if (a2 != null) {
            a2.d();
            this.h.a(joVar, a2);
        }
        return a2;
    }

    @DexIgnore
    public qp(ar arVar, tq.a aVar, dr drVar, dr drVar2, dr drVar3, dr drVar4, xp xpVar, up upVar, ip ipVar, b bVar, a aVar2, dq dqVar, boolean z) {
        this.c = arVar;
        tq.a aVar3 = aVar;
        this.f = new c(aVar);
        ip ipVar2 = ipVar == null ? new ip(z) : ipVar;
        this.h = ipVar2;
        ipVar2.a((vp.a) this);
        this.b = upVar == null ? new up() : upVar;
        this.a = xpVar == null ? new xp() : xpVar;
        this.d = bVar == null ? new b(drVar, drVar2, drVar3, drVar4, this, this) : bVar;
        this.g = aVar2 == null ? new a(this.f) : aVar2;
        this.e = dqVar == null ? new dq() : dqVar;
        arVar.a((ar.a) this);
    }

    @DexIgnore
    public void b(aq<?> aqVar) {
        if (aqVar instanceof vp) {
            ((vp) aqVar).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> d a(tn tnVar, Object obj, jo joVar, int i2, int i3, Class<?> cls, Class<R> cls2, Priority priority, pp ppVar, Map<Class<?>, oo<?>> map, boolean z, boolean z2, lo loVar, boolean z3, boolean z4, boolean z5, boolean z6, sv svVar, Executor executor, tp tpVar, long j) {
        sv svVar2 = svVar;
        Executor executor2 = executor;
        tp tpVar2 = tpVar;
        long j2 = j;
        rp<?> a2 = this.a.a((jo) tpVar2, z6);
        if (a2 != null) {
            a2.a(svVar2, executor2);
            if (i) {
                a("Added to existing load", j2, (jo) tpVar2);
            }
            return new d(svVar2, a2);
        }
        rp a3 = this.d.a(tpVar, z3, z4, z5, z6);
        rp rpVar = a3;
        tp tpVar3 = tpVar2;
        DecodeJob<R> a4 = this.g.a(tnVar, obj, tpVar, joVar, i2, i3, cls, cls2, priority, ppVar, map, z, z2, z6, loVar, a3);
        this.a.a((jo) tpVar3, (rp<?>) rpVar);
        rp rpVar2 = rpVar;
        tp tpVar4 = tpVar3;
        sv svVar3 = svVar;
        rpVar2.a(svVar3, executor);
        rpVar2.b(a4);
        if (i) {
            a("Started new load", j, (jo) tpVar4);
        }
        return new d(svVar3, rpVar2);
    }

    @DexIgnore
    public final vp<?> a(tp tpVar, boolean z, long j) {
        if (!z) {
            return null;
        }
        vp<?> b2 = b((jo) tpVar);
        if (b2 != null) {
            if (i) {
                a("Loaded resource from active resources", j, (jo) tpVar);
            }
            return b2;
        }
        vp<?> c2 = c(tpVar);
        if (c2 == null) {
            return null;
        }
        if (i) {
            a("Loaded resource from cache", j, (jo) tpVar);
        }
        return c2;
    }

    @DexIgnore
    public static void a(String str, long j, jo joVar) {
        Log.v("Engine", str + " in " + pw.a(j) + "ms, key: " + joVar);
    }

    @DexIgnore
    public final vp<?> a(jo joVar) {
        aq<?> a2 = this.c.a(joVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof vp) {
            return (vp) a2;
        }
        return new vp<>(a2, true, true, joVar, this);
    }

    @DexIgnore
    public synchronized void a(rp<?> rpVar, jo joVar, vp<?> vpVar) {
        if (vpVar != null) {
            if (vpVar.f()) {
                this.h.a(joVar, vpVar);
            }
        }
        this.a.b(joVar, rpVar);
    }

    @DexIgnore
    public synchronized void a(rp<?> rpVar, jo joVar) {
        this.a.b(joVar, rpVar);
    }

    @DexIgnore
    public void a(aq<?> aqVar) {
        this.e.a(aqVar);
    }

    @DexIgnore
    public void a(jo joVar, vp<?> vpVar) {
        this.h.a(joVar);
        if (vpVar.f()) {
            this.c.a(joVar, vpVar);
        } else {
            this.e.a(vpVar);
        }
    }
}
