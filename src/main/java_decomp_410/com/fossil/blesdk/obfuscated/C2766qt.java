package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qt */
public class C2766qt implements com.fossil.blesdk.obfuscated.C2427mo<java.io.File, java.io.File> {
    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<java.io.File> mo9299a(java.io.File file, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2834rt(file);
    }
}
