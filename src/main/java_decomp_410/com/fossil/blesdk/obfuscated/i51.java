package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i51 extends a51 implements g51 {
    @DexIgnore
    public i51(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    @DexIgnore
    public final boolean a(g51 g51) throws RemoteException {
        Parcel o = o();
        c51.a(o, (IInterface) g51);
        Parcel a = a(16, o);
        boolean a2 = c51.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int k() throws RemoteException {
        Parcel a = a(17, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
