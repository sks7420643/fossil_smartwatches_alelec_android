package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fd */
public final class C1787fd {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.Set<java.io.File> f5131a; // = new java.util.HashSet();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ boolean f5132b; // = m6919a(java.lang.System.getProperty("java.vm.version"));

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fd$a */
    public static final class C1788a {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ int f5133b; // = 4;

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1789a f5134a;

        @DexIgnore
        /* renamed from: com.fossil.blesdk.obfuscated.fd$a$a */
        public interface C1789a {
            @DexIgnore
            /* renamed from: a */
            java.lang.Object mo10848a(java.io.File file, dalvik.system.DexFile dexFile) throws java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, java.io.IOException;
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$a$b")
        /* renamed from: com.fossil.blesdk.obfuscated.fd$a$b */
        public static class C1790b implements com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1789a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.lang.reflect.Constructor<?> f5135a;

            @DexIgnore
            public C1790b(java.lang.Class<?> cls) throws java.lang.SecurityException, java.lang.NoSuchMethodException {
                this.f5135a = cls.getConstructor(new java.lang.Class[]{java.io.File.class, java.util.zip.ZipFile.class, dalvik.system.DexFile.class});
                this.f5135a.setAccessible(true);
            }

            @DexIgnore
            /* renamed from: a */
            public java.lang.Object mo10848a(java.io.File file, dalvik.system.DexFile dexFile) throws java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, java.io.IOException {
                return this.f5135a.newInstance(new java.lang.Object[]{file, new java.util.zip.ZipFile(file), dexFile});
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$a$c")
        /* renamed from: com.fossil.blesdk.obfuscated.fd$a$c */
        public static class C1791c implements com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1789a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.lang.reflect.Constructor<?> f5136a;

            @DexIgnore
            public C1791c(java.lang.Class<?> cls) throws java.lang.SecurityException, java.lang.NoSuchMethodException {
                this.f5136a = cls.getConstructor(new java.lang.Class[]{java.io.File.class, java.io.File.class, dalvik.system.DexFile.class});
                this.f5136a.setAccessible(true);
            }

            @DexIgnore
            /* renamed from: a */
            public java.lang.Object mo10848a(java.io.File file, dalvik.system.DexFile dexFile) throws java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
                return this.f5136a.newInstance(new java.lang.Object[]{file, file, dexFile});
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$a$d")
        /* renamed from: com.fossil.blesdk.obfuscated.fd$a$d */
        public static class C1792d implements com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1789a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.lang.reflect.Constructor<?> f5137a;

            @DexIgnore
            public C1792d(java.lang.Class<?> cls) throws java.lang.SecurityException, java.lang.NoSuchMethodException {
                this.f5137a = cls.getConstructor(new java.lang.Class[]{java.io.File.class, java.lang.Boolean.TYPE, java.io.File.class, dalvik.system.DexFile.class});
                this.f5137a.setAccessible(true);
            }

            @DexIgnore
            /* renamed from: a */
            public java.lang.Object mo10848a(java.io.File file, dalvik.system.DexFile dexFile) throws java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
                return this.f5137a.newInstance(new java.lang.Object[]{file, java.lang.Boolean.FALSE, file, dexFile});
            }
        }

        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000f */
        public C1788a() throws java.lang.ClassNotFoundException, java.lang.SecurityException, java.lang.NoSuchMethodException {
            java.lang.Class<?> cls = java.lang.Class.forName("dalvik.system.DexPathList$Element");
            com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1789a aVar = new com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1790b(cls);
            try {
                aVar = new com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1791c(cls);
            } catch (java.lang.NoSuchMethodException unused) {
                aVar = new com.fossil.blesdk.obfuscated.C1787fd.C1788a.C1792d(cls);
            }
            this.f5134a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public static void m6927a(java.lang.ClassLoader classLoader, java.util.List<? extends java.io.File> list) throws java.io.IOException, java.lang.SecurityException, java.lang.IllegalArgumentException, java.lang.ClassNotFoundException, java.lang.NoSuchMethodException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchFieldException {
            java.lang.Object obj = com.fossil.blesdk.obfuscated.C1787fd.m6921b(classLoader, "pathList").get(classLoader);
            java.lang.Object[] a = new com.fossil.blesdk.obfuscated.C1787fd.C1788a().mo10847a(list);
            try {
                com.fossil.blesdk.obfuscated.C1787fd.m6923b(obj, "dexElements", a);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.w("MultiDex", "Failed find field 'dexElements' attempting 'pathElements'", e);
                com.fossil.blesdk.obfuscated.C1787fd.m6923b(obj, "pathElements", a);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final java.lang.Object[] mo10847a(java.util.List<? extends java.io.File> list) throws java.io.IOException, java.lang.SecurityException, java.lang.IllegalArgumentException, java.lang.InstantiationException, java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException {
            java.lang.Object[] objArr = new java.lang.Object[list.size()];
            for (int i = 0; i < objArr.length; i++) {
                java.io.File file = (java.io.File) list.get(i);
                objArr[i] = this.f5134a.mo10848a(file, dalvik.system.DexFile.loadDex(file.getPath(), m6926a(file), 0));
            }
            return objArr;
        }

        @DexIgnore
        /* renamed from: a */
        public static java.lang.String m6926a(java.io.File file) {
            java.io.File parentFile = file.getParentFile();
            java.lang.String name = file.getName();
            return new java.io.File(parentFile, name.substring(0, name.length() - f5133b) + ".dex").getPath();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fd$b */
    public static final class C1793b {
        @DexIgnore
        /* renamed from: a */
        public static void m6933a(java.lang.ClassLoader classLoader, java.util.List<? extends java.io.File> list, java.io.File file) throws java.lang.IllegalArgumentException, java.lang.IllegalAccessException, java.lang.NoSuchFieldException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException, java.io.IOException {
            java.io.IOException[] iOExceptionArr;
            java.lang.Object obj = com.fossil.blesdk.obfuscated.C1787fd.m6921b(classLoader, "pathList").get(classLoader);
            java.util.ArrayList arrayList = new java.util.ArrayList();
            com.fossil.blesdk.obfuscated.C1787fd.m6923b(obj, "dexElements", m6934a(obj, new java.util.ArrayList(list), file, arrayList));
            if (arrayList.size() > 0) {
                java.util.Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    android.util.Log.w("MultiDex", "Exception in makeDexElement", (java.io.IOException) it.next());
                }
                java.lang.reflect.Field a = com.fossil.blesdk.obfuscated.C1787fd.m6921b(obj, "dexElementsSuppressedExceptions");
                java.io.IOException[] iOExceptionArr2 = (java.io.IOException[]) a.get(obj);
                if (iOExceptionArr2 == null) {
                    iOExceptionArr = (java.io.IOException[]) arrayList.toArray(new java.io.IOException[arrayList.size()]);
                } else {
                    java.io.IOException[] iOExceptionArr3 = new java.io.IOException[(arrayList.size() + iOExceptionArr2.length)];
                    arrayList.toArray(iOExceptionArr3);
                    java.lang.System.arraycopy(iOExceptionArr2, 0, iOExceptionArr3, arrayList.size(), iOExceptionArr2.length);
                    iOExceptionArr = iOExceptionArr3;
                }
                a.set(obj, iOExceptionArr);
                java.io.IOException iOException = new java.io.IOException("I/O exception during makeDexElement");
                iOException.initCause((java.lang.Throwable) arrayList.get(0));
                throw iOException;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public static java.lang.Object[] m6934a(java.lang.Object obj, java.util.ArrayList<java.io.File> arrayList, java.io.File file, java.util.ArrayList<java.io.IOException> arrayList2) throws java.lang.IllegalAccessException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException {
            return (java.lang.Object[]) com.fossil.blesdk.obfuscated.C1787fd.m6922b(obj, "makeDexElements", (java.lang.Class<?>[]) new java.lang.Class[]{java.util.ArrayList.class, java.io.File.class, java.util.ArrayList.class}).invoke(obj, new java.lang.Object[]{arrayList, file, arrayList2});
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fd$c")
    /* renamed from: com.fossil.blesdk.obfuscated.fd$c */
    public static final class C1794c {
        @DexIgnore
        /* renamed from: a */
        public static void m6935a(java.lang.ClassLoader classLoader, java.util.List<? extends java.io.File> list) throws java.lang.IllegalArgumentException, java.lang.IllegalAccessException, java.lang.NoSuchFieldException, java.io.IOException {
            int size = list.size();
            java.lang.reflect.Field a = com.fossil.blesdk.obfuscated.C1787fd.m6921b(classLoader, "path");
            java.lang.StringBuilder sb = new java.lang.StringBuilder((java.lang.String) a.get(classLoader));
            java.lang.String[] strArr = new java.lang.String[size];
            java.io.File[] fileArr = new java.io.File[size];
            java.util.zip.ZipFile[] zipFileArr = new java.util.zip.ZipFile[size];
            dalvik.system.DexFile[] dexFileArr = new dalvik.system.DexFile[size];
            java.util.ListIterator<? extends java.io.File> listIterator = list.listIterator();
            while (listIterator.hasNext()) {
                java.io.File file = (java.io.File) listIterator.next();
                java.lang.String absolutePath = file.getAbsolutePath();
                sb.append(':');
                sb.append(absolutePath);
                int previousIndex = listIterator.previousIndex();
                strArr[previousIndex] = absolutePath;
                fileArr[previousIndex] = file;
                zipFileArr[previousIndex] = new java.util.zip.ZipFile(file);
                dexFileArr[previousIndex] = dalvik.system.DexFile.loadDex(absolutePath, absolutePath + ".dex", 0);
            }
            a.set(classLoader, sb.toString());
            com.fossil.blesdk.obfuscated.C1787fd.m6923b((java.lang.Object) classLoader, "mPaths", (java.lang.Object[]) strArr);
            com.fossil.blesdk.obfuscated.C1787fd.m6923b((java.lang.Object) classLoader, "mFiles", (java.lang.Object[]) fileArr);
            com.fossil.blesdk.obfuscated.C1787fd.m6923b((java.lang.Object) classLoader, "mZips", (java.lang.Object[]) zipFileArr);
            com.fossil.blesdk.obfuscated.C1787fd.m6923b((java.lang.Object) classLoader, "mDexs", (java.lang.Object[]) dexFileArr);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.pm.ApplicationInfo m6920b(android.content.Context context) {
        try {
            return context.getApplicationInfo();
        } catch (java.lang.RuntimeException e) {
            android.util.Log.w("MultiDex", "Failure while trying to obtain ApplicationInfo from Context. Must be running in test mode. Skip patching.", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static java.lang.ClassLoader m6924c(android.content.Context context) {
        try {
            java.lang.ClassLoader classLoader = context.getClassLoader();
            if (android.os.Build.VERSION.SDK_INT >= 14) {
                if (classLoader instanceof dalvik.system.BaseDexClassLoader) {
                    return classLoader;
                }
            } else if ((classLoader instanceof dalvik.system.DexClassLoader) || (classLoader instanceof dalvik.system.PathClassLoader)) {
                return classLoader;
            }
            android.util.Log.e("MultiDex", "Context class loader is null or not dex-capable. Must be running in test mode. Skip patching.");
            return null;
        } catch (java.lang.RuntimeException e) {
            android.util.Log.w("MultiDex", "Failure while trying to obtain Context class loader. Must be running in test mode. Skip patching.", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m6925d(android.content.Context context) {
        android.util.Log.i("MultiDex", "Installing application");
        if (f5132b) {
            android.util.Log.i("MultiDex", "VM has multidex support, MultiDex support library is disabled.");
        } else if (android.os.Build.VERSION.SDK_INT >= 4) {
            try {
                android.content.pm.ApplicationInfo b = m6920b(context);
                if (b == null) {
                    android.util.Log.i("MultiDex", "No ApplicationInfo available, i.e. running on a test Context: MultiDex support library is disabled.");
                    return;
                }
                m6915a(context, new java.io.File(b.sourceDir), new java.io.File(b.dataDir), "secondary-dexes", "", true);
                android.util.Log.i("MultiDex", "install done");
            } catch (java.lang.Exception e) {
                android.util.Log.e("MultiDex", "MultiDex installation failure", e);
                throw new java.lang.RuntimeException("MultiDex installation failed (" + e.getMessage() + ").");
            }
        } else {
            throw new java.lang.RuntimeException("MultiDex installation failed. SDK " + android.os.Build.VERSION.SDK_INT + " is unsupported. Min SDK version is " + 4 + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Field m6921b(java.lang.Object obj, java.lang.String str) throws java.lang.NoSuchFieldException {
        java.lang.Class cls = obj.getClass();
        while (cls != null) {
            try {
                java.lang.reflect.Field declaredField = cls.getDeclaredField(str);
                if (!declaredField.isAccessible()) {
                    declaredField.setAccessible(true);
                }
                return declaredField;
            } catch (java.lang.NoSuchFieldException unused) {
                cls = cls.getSuperclass();
            }
        }
        throw new java.lang.NoSuchFieldException("Field " + str + " not found in " + obj.getClass());
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(5:41|42|43|44|45) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x009d */
    /* renamed from: a */
    public static void m6915a(android.content.Context context, java.io.File file, java.io.File file2, java.lang.String str, java.lang.String str2, boolean z) throws java.io.IOException, java.lang.IllegalArgumentException, java.lang.IllegalAccessException, java.lang.NoSuchFieldException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException, java.lang.SecurityException, java.lang.ClassNotFoundException, java.lang.InstantiationException {
        synchronized (f5131a) {
            if (!f5131a.contains(file)) {
                f5131a.add(file);
                if (android.os.Build.VERSION.SDK_INT > 20) {
                    android.util.Log.w("MultiDex", "MultiDex is not guaranteed to work in SDK version " + android.os.Build.VERSION.SDK_INT + ": SDK version higher than " + 20 + " should be backed by " + "runtime with built-in multidex capabilty but it's not the " + "case here: java.vm.version=\"" + java.lang.System.getProperty("java.vm.version") + "\"");
                }
                java.lang.ClassLoader c = m6924c(context);
                if (c != null) {
                    try {
                        m6914a(context);
                    } catch (Throwable th) {
                        android.util.Log.w("MultiDex", "Something went wrong when trying to clear old MultiDex extraction, continuing without cleaning.", th);
                    }
                    java.io.File a = m6911a(context, file2, str);
                    androidx.multidex.MultiDexExtractor multiDexExtractor = new androidx.multidex.MultiDexExtractor(file, a);
                    java.io.IOException e = null;
                    try {
                        m6917a(c, a, multiDexExtractor.mo2330a(context, str2, false));
                    } catch (java.io.IOException e2) {
                        if (z) {
                            android.util.Log.w("MultiDex", "Failed to install extracted secondary dex files, retrying with forced extraction", e2);
                            m6917a(c, a, multiDexExtractor.mo2330a(context, str2, true));
                        } else {
                            throw e2;
                        }
                    } catch (Throwable th2) {
                        multiDexExtractor.close();
                        throw th2;
                    }
                    try {
                        multiDexExtractor.close();
                    } catch (java.io.IOException e3) {
                        e = e3;
                    }
                    if (e != null) {
                        throw e;
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Method m6922b(java.lang.Object obj, java.lang.String str, java.lang.Class<?>... clsArr) throws java.lang.NoSuchMethodException {
        java.lang.Class cls = obj.getClass();
        while (cls != null) {
            try {
                java.lang.reflect.Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                if (!declaredMethod.isAccessible()) {
                    declaredMethod.setAccessible(true);
                }
                return declaredMethod;
            } catch (java.lang.NoSuchMethodException unused) {
                cls = cls.getSuperclass();
            }
        }
        throw new java.lang.NoSuchMethodException("Method " + str + " with parameters " + java.util.Arrays.asList(clsArr) + " not found in " + obj.getClass());
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6923b(java.lang.Object obj, java.lang.String str, java.lang.Object[] objArr) throws java.lang.NoSuchFieldException, java.lang.IllegalArgumentException, java.lang.IllegalAccessException {
        java.lang.reflect.Field b = m6921b(obj, str);
        java.lang.Object[] objArr2 = (java.lang.Object[]) b.get(obj);
        java.lang.Object[] objArr3 = (java.lang.Object[]) java.lang.reflect.Array.newInstance(objArr2.getClass().getComponentType(), objArr2.length + objArr.length);
        java.lang.System.arraycopy(objArr2, 0, objArr3, 0, objArr2.length);
        java.lang.System.arraycopy(objArr, 0, objArr3, objArr2.length, objArr.length);
        b.set(obj, objArr3);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6919a(java.lang.String str) {
        boolean z = false;
        if (str != null) {
            java.util.StringTokenizer stringTokenizer = new java.util.StringTokenizer(str, com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
            java.lang.String str2 = null;
            java.lang.String nextToken = stringTokenizer.hasMoreTokens() ? stringTokenizer.nextToken() : null;
            if (stringTokenizer.hasMoreTokens()) {
                str2 = stringTokenizer.nextToken();
            }
            if (!(nextToken == null || str2 == null)) {
                try {
                    int parseInt = java.lang.Integer.parseInt(nextToken);
                    int parseInt2 = java.lang.Integer.parseInt(str2);
                    if (parseInt > 2 || (parseInt == 2 && parseInt2 >= 1)) {
                        z = true;
                    }
                } catch (java.lang.NumberFormatException unused) {
                }
            }
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("VM with version ");
        sb.append(str);
        sb.append(z ? " has multidex support" : " does not have multidex support");
        android.util.Log.i("MultiDex", sb.toString());
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6917a(java.lang.ClassLoader classLoader, java.io.File file, java.util.List<? extends java.io.File> list) throws java.lang.IllegalArgumentException, java.lang.IllegalAccessException, java.lang.NoSuchFieldException, java.lang.reflect.InvocationTargetException, java.lang.NoSuchMethodException, java.io.IOException, java.lang.SecurityException, java.lang.ClassNotFoundException, java.lang.InstantiationException {
        if (!list.isEmpty()) {
            int i = android.os.Build.VERSION.SDK_INT;
            if (i >= 19) {
                com.fossil.blesdk.obfuscated.C1787fd.C1793b.m6933a(classLoader, list, file);
            } else if (i >= 14) {
                com.fossil.blesdk.obfuscated.C1787fd.C1788a.m6927a(classLoader, list);
            } else {
                com.fossil.blesdk.obfuscated.C1787fd.C1794c.m6935a(classLoader, list);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6914a(android.content.Context context) throws java.lang.Exception {
        java.io.File file = new java.io.File(context.getFilesDir(), "secondary-dexes");
        if (file.isDirectory()) {
            android.util.Log.i("MultiDex", "Clearing old secondary dex dir (" + file.getPath() + ").");
            java.io.File[] listFiles = file.listFiles();
            if (listFiles == null) {
                android.util.Log.w("MultiDex", "Failed to list secondary dex dir content (" + file.getPath() + ").");
                return;
            }
            for (java.io.File length : listFiles) {
                android.util.Log.i("MultiDex", "Trying to delete old file " + length.getPath() + " of size " + length.length());
                if (!listFiles[r3].delete()) {
                    android.util.Log.w("MultiDex", "Failed to delete old file " + length.getPath());
                } else {
                    android.util.Log.i("MultiDex", "Deleted old file " + length.getPath());
                }
            }
            if (!file.delete()) {
                android.util.Log.w("MultiDex", "Failed to delete secondary dex dir " + file.getPath());
                return;
            }
            android.util.Log.i("MultiDex", "Deleted old secondary dex dir " + file.getPath());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.io.File m6911a(android.content.Context context, java.io.File file, java.lang.String str) throws java.io.IOException {
        java.io.File file2 = new java.io.File(file, "code_cache");
        try {
            m6916a(file2);
        } catch (java.io.IOException unused) {
            file2 = new java.io.File(context.getFilesDir(), "code_cache");
            m6916a(file2);
        }
        java.io.File file3 = new java.io.File(file2, str);
        m6916a(file3);
        return file3;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6916a(java.io.File file) throws java.io.IOException {
        file.mkdir();
        if (!file.isDirectory()) {
            java.io.File parentFile = file.getParentFile();
            if (parentFile == null) {
                android.util.Log.e("MultiDex", "Failed to create dir " + file.getPath() + ". Parent file is null.");
            } else {
                android.util.Log.e("MultiDex", "Failed to create dir " + file.getPath() + ". parent file is a dir " + parentFile.isDirectory() + ", a file " + parentFile.isFile() + ", exists " + parentFile.exists() + ", readable " + parentFile.canRead() + ", writable " + parentFile.canWrite());
            }
            throw new java.io.IOException("Failed to create directory " + file.getPath());
        }
    }
}
