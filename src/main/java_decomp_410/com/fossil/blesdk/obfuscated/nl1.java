package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.Utility;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nl1 extends ui1 {
    @DexIgnore
    public static /* final */ String[] g; // = {"firebase_", "google_", "ga_"};
    @DexIgnore
    public SecureRandom c;
    @DexIgnore
    public /* final */ AtomicLong d; // = new AtomicLong(0);
    @DexIgnore
    public int e;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public nl1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public static boolean e(String str) {
        bk0.b(str);
        if (str.charAt(0) != '_' || str.equals("_ep")) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static boolean f(String str) {
        bk0.a(str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    @DexIgnore
    public static int g(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        return FieldType.FOREIGN_ID_FIELD_SUFFIX.equals(str) ? 256 : 36;
    }

    @DexIgnore
    public static boolean h(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    @DexIgnore
    public static MessageDigest w() {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final Bundle a(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString("source", str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString("medium", str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e2) {
            d().v().a("Install referrer url isn't a hierarchical URI", e2);
            return null;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        if (str2 == null) {
            d().s().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            d().s().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                d().s().a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    d().s().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean c(String str, String str2) {
        if (str2 == null) {
            d().s().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            d().s().a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        d().s().a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            d().s().a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    public final boolean d(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (f(str)) {
                return true;
            }
            if (this.a.z()) {
                d().s().a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", tg1.a(str));
            }
            return false;
        } else if (TextUtils.isEmpty(str2)) {
            if (this.a.z()) {
                d().s().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
            }
            return false;
        } else if (f(str2)) {
            return true;
        } else {
            d().s().a("Invalid admob_app_id. Analytics disabled.", tg1.a(str2));
            return false;
        }
    }

    @DexIgnore
    public final boolean p() {
        return true;
    }

    @DexIgnore
    public final void q() {
        e();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                d().v().a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    @DexIgnore
    public final long s() {
        long andIncrement;
        long j;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                long nextLong = new Random(System.nanoTime() ^ c().b()).nextLong();
                int i = this.e + 1;
                this.e = i;
                j = nextLong + ((long) i);
            }
            return j;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1, 1);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    @DexIgnore
    public final SecureRandom t() {
        e();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    @DexIgnore
    public final int u() {
        if (this.f == null) {
            this.f = Integer.valueOf(yd0.a().b(getContext()) / 1000);
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final String v() {
        byte[] bArr = new byte[16];
        t().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new Object[]{new BigInteger(1, bArr)});
    }

    @DexIgnore
    public static boolean e(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    @DexIgnore
    public final int b(String str) {
        if (!c("user property", str)) {
            return 6;
        }
        if (!a("user property", yi1.a, str)) {
            return 15;
        }
        if (!a("user property", 24, str)) {
            return 6;
        }
        return 0;
    }

    @DexIgnore
    public final Object c(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return a(g(str), obj, true);
        }
        return a(g(str), obj, false);
    }

    @DexIgnore
    public final boolean d(String str) {
        e();
        if (bn0.b(getContext()).a(str) == 0) {
            return true;
        }
        d().z().a("Permission not granted", str);
        return false;
    }

    @DexIgnore
    public final int b(String str, Object obj) {
        boolean z;
        if ("_ldl".equals(str)) {
            z = a("user property referrer", str, g(str), obj, false);
        } else {
            z = a("user property", str, g(str), obj, false);
        }
        return z ? 0 : 7;
    }

    @DexIgnore
    public static boolean c(Context context, String str) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            ServiceInfo serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0);
            if (serviceInfo == null || !serviceInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final boolean b(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo b = bn0.b(context).b(str, 64);
            if (b == null || b.signatures == null || b.signatures.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(b.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (CertificateException e2) {
            d().s().a("Error obtaining certificate", e2);
            return true;
        } catch (PackageManager.NameNotFoundException e3) {
            d().s().a("Package name not found", e3);
            return true;
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String q = l().q();
        b();
        return q.equals(str);
    }

    @DexIgnore
    public static Bundle b(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String str : bundle2.keySet()) {
            Object obj = bundle2.get(str);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        if (parcelableArr[i] instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public static boolean a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    @DexIgnore
    public final boolean a(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            d().s().a("Name is required and can't be null. Type", str);
            return false;
        }
        bk0.a(str2);
        String[] strArr2 = g;
        int length = strArr2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            d().s().a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            bk0.a(strArr);
            int length2 = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    z2 = false;
                    break;
                } else if (e(str2, strArr[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                d().s().a("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean a(String str, int i, String str2) {
        if (str2 == null) {
            d().s().a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            d().s().a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    @DexIgnore
    public final int a(String str) {
        if (!c(Constants.EVENT, str)) {
            return 2;
        }
        if (!a(Constants.EVENT, wi1.a, str)) {
            return 13;
        }
        if (!a(Constants.EVENT, 40, str)) {
            return 2;
        }
        return 0;
    }

    @DexIgnore
    public final boolean a(String str, String str2, int i, Object obj, boolean z) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                String valueOf = String.valueOf(obj);
                if (valueOf.codePointCount(0, valueOf.length()) > i) {
                    d().v().a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                    return false;
                }
            } else if ((obj instanceof Bundle) && z) {
                return true;
            } else {
                if ((obj instanceof Parcelable[]) && z) {
                    for (Parcelable parcelable : (Parcelable[]) obj) {
                        if (!(parcelable instanceof Bundle)) {
                            d().v().a("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof ArrayList) || !z) {
                    return false;
                } else {
                    ArrayList arrayList = (ArrayList) obj;
                    int size = arrayList.size();
                    int i2 = 0;
                    while (i2 < size) {
                        Object obj2 = arrayList.get(i2);
                        i2++;
                        if (!(obj2 instanceof Bundle)) {
                            d().v().a("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean a(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    @DexIgnore
    public static Object a(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return a(String.valueOf(obj), i, z);
            }
            return null;
        }
    }

    @DexIgnore
    public static String a(String str, int i, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    @DexIgnore
    public final Object a(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return a(256, obj, true);
        }
        if (!h(str)) {
            i = 100;
        }
        return a(i, obj, false);
    }

    @DexIgnore
    public static Bundle[] a(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            return (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
        } else if (!(obj instanceof ArrayList)) {
            return null;
        } else {
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        if (a("event param", 40, r14) == false) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (a("event param", 40, r14) == false) goto L_0x0059;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0132  */
    public final Bundle a(String str, String str2, Bundle bundle, List<String> list, boolean z, boolean z2) {
        int i;
        String str3;
        boolean z3;
        int i2;
        boolean z4;
        boolean z5;
        int i3;
        int i4;
        Bundle bundle2 = bundle;
        List<String> list2 = list;
        String[] strArr = null;
        if (bundle2 == null) {
            return null;
        }
        Bundle bundle3 = new Bundle(bundle2);
        int i5 = 0;
        for (String str4 : bundle.keySet()) {
            if (list2 == null || !list2.contains(str4)) {
                i = 14;
                if (z) {
                    if (b("event param", str4)) {
                        if (!a("event param", strArr, str4)) {
                            i4 = 14;
                            if (i4 == 0) {
                                if (c("event param", str4)) {
                                    if (a("event param", strArr, str4)) {
                                    }
                                }
                                i = 3;
                            } else {
                                i = i4;
                            }
                            if (i == 0) {
                                if (a(bundle3, i)) {
                                    bundle3.putString("_ev", a(str4, 40, true));
                                    if (i == 3) {
                                        a(bundle3, (Object) str4);
                                    }
                                }
                                bundle3.remove(str4);
                            } else {
                                Object obj = bundle2.get(str4);
                                e();
                                if (z2) {
                                    if (obj instanceof Parcelable[]) {
                                        i3 = ((Parcelable[]) obj).length;
                                    } else {
                                        if (obj instanceof ArrayList) {
                                            i3 = ((ArrayList) obj).size();
                                        }
                                        z5 = true;
                                        if (!z5) {
                                            i2 = 17;
                                            str3 = "_ev";
                                            z3 = true;
                                            if (i2 != 0 || str3.equals(str4)) {
                                                if (e(str4)) {
                                                    i5++;
                                                    if (i5 > 25) {
                                                        StringBuilder sb = new StringBuilder(48);
                                                        sb.append("Event can't contain more than 25 params");
                                                        d().s().a(sb.toString(), i().a(str2), i().a(bundle2));
                                                        a(bundle3, 5);
                                                        bundle3.remove(str4);
                                                    }
                                                }
                                                String str5 = str2;
                                            } else {
                                                if (a(bundle3, i2)) {
                                                    bundle3.putString(str3, a(str4, 40, z3));
                                                    a(bundle3, bundle2.get(str4));
                                                }
                                                bundle3.remove(str4);
                                            }
                                        }
                                    }
                                    if (i3 > 1000) {
                                        d().v().a("Parameter array is too long; discarded. Value kind, name, array length", "param", str4, Integer.valueOf(i3));
                                        z5 = false;
                                        if (!z5) {
                                        }
                                    }
                                    z5 = true;
                                    if (!z5) {
                                    }
                                }
                                if ((!l().f(str) || !h(str2)) && !h(str4)) {
                                    str3 = "_ev";
                                    z3 = true;
                                    z4 = a("param", str4, 100, obj, z2);
                                } else {
                                    z3 = true;
                                    str3 = "_ev";
                                    z4 = a("param", str4, 256, obj, z2);
                                }
                                i2 = z4 ? 0 : 4;
                                if (i2 != 0) {
                                }
                                if (e(str4)) {
                                }
                                String str52 = str2;
                            }
                            strArr = null;
                        }
                    }
                    i4 = 3;
                    if (i4 == 0) {
                    }
                    if (i == 0) {
                    }
                    strArr = null;
                }
                i4 = 0;
                if (i4 == 0) {
                }
                if (i == 0) {
                }
                strArr = null;
            }
            i = 0;
            if (i == 0) {
            }
            strArr = null;
        }
        return bundle3;
    }

    @DexIgnore
    public static boolean a(Bundle bundle, int i) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    @DexIgnore
    public static void a(Bundle bundle, Object obj) {
        bk0.a(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                d().x().a("Not putting event parameter. Invalid value type. name, type", i().b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    @DexIgnore
    public final void a(int i, String str, String str2, int i2) {
        a((String) null, i, str, str2, i2);
    }

    @DexIgnore
    public final void a(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        a(bundle, i);
        if (l().d(str, jg1.r0)) {
            if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
                bundle.putString(str2, str3);
            }
        } else if (!TextUtils.isEmpty(str2)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.a.b();
        this.a.k().b("auto", "_err", bundle);
    }

    @DexIgnore
    public static long a(byte[] bArr) {
        bk0.a(bArr);
        int i = 0;
        bk0.b(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    @DexIgnore
    public static boolean a(Context context, boolean z) {
        bk0.a(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return c(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return c(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    @DexIgnore
    public final Bundle a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object a = a(str, bundle.get(str));
                if (a == null) {
                    d().v().a("Param value can't be null", i().b(str));
                } else {
                    a(bundle2, str, a);
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final hg1 a(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (a(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            Bundle bundle3 = bundle2;
            bundle3.putString("_o", str3);
            return new hg1(str2, new eg1(a(a(str, str2, bundle3, im0.a("_o"), false, false))), str3, j);
        }
        d().s().a("Invalid conditional property event name", i().c(str2));
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final long a(Context context, String str) {
        e();
        bk0.a(context);
        bk0.b(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest w = w();
        if (w == null) {
            d().s().a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!b(context, str)) {
                    PackageInfo b = bn0.b(context).b(getContext().getPackageName(), 64);
                    if (b.signatures != null && b.signatures.length > 0) {
                        return a(w.digest(b.signatures[0].toByteArray()));
                    }
                    d().v().a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                d().s().a("Package name not found", e2);
            }
        }
        return 0;
    }

    @DexIgnore
    public static byte[] a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public final int a(int i) {
        return yd0.a().a(getContext(), (int) zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE);
    }

    @DexIgnore
    public static long a(long j, long j2) {
        return (j + (j2 * 60000)) / LogBuilder.MAX_INTERVAL;
    }

    @DexIgnore
    public final void a(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            d().v().a("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }
}
