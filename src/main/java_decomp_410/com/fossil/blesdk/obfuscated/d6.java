package com.fossil.blesdk.obfuscated;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d6 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Bundle a;
        @DexIgnore
        public /* final */ h6[] b;
        @DexIgnore
        public /* final */ h6[] c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public CharSequence h;
        @DexIgnore
        public PendingIntent i;

        @DexIgnore
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this(i2, charSequence, pendingIntent, new Bundle(), (h6[]) null, (h6[]) null, true, 0, true);
        }

        @DexIgnore
        public PendingIntent a() {
            return this.i;
        }

        @DexIgnore
        public boolean b() {
            return this.d;
        }

        @DexIgnore
        public h6[] c() {
            return this.c;
        }

        @DexIgnore
        public Bundle d() {
            return this.a;
        }

        @DexIgnore
        public int e() {
            return this.g;
        }

        @DexIgnore
        public h6[] f() {
            return this.b;
        }

        @DexIgnore
        public int g() {
            return this.f;
        }

        @DexIgnore
        public boolean h() {
            return this.e;
        }

        @DexIgnore
        public CharSequence i() {
            return this.h;
        }

        @DexIgnore
        public a(int i2, CharSequence charSequence, PendingIntent pendingIntent, Bundle bundle, h6[] h6VarArr, h6[] h6VarArr2, boolean z, int i3, boolean z2) {
            this.e = true;
            this.g = i2;
            this.h = c.d(charSequence);
            this.i = pendingIntent;
            this.a = bundle == null ? new Bundle() : bundle;
            this.b = h6VarArr;
            this.c = h6VarArr2;
            this.d = z;
            this.f = i3;
            this.e = z2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends d {
        @DexIgnore
        public CharSequence e;

        @DexIgnore
        public b a(CharSequence charSequence) {
            this.e = c.d(charSequence);
            return this;
        }

        @DexIgnore
        public void a(c6 c6Var) {
            if (Build.VERSION.SDK_INT >= 16) {
                Notification.BigTextStyle bigText = new Notification.BigTextStyle(c6Var.a()).setBigContentTitle(this.b).bigText(this.e);
                if (this.d) {
                    bigText.setSummaryText(this.c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public String A;
        @DexIgnore
        public Bundle B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public Notification E;
        @DexIgnore
        public RemoteViews F;
        @DexIgnore
        public RemoteViews G;
        @DexIgnore
        public RemoteViews H;
        @DexIgnore
        public String I;
        @DexIgnore
        public int J;
        @DexIgnore
        public String K;
        @DexIgnore
        public long L;
        @DexIgnore
        public int M;
        @DexIgnore
        public Notification N;
        @DexIgnore
        @Deprecated
        public ArrayList<String> O;
        @DexIgnore
        public Context a;
        @DexIgnore
        public ArrayList<a> b;
        @DexIgnore
        public ArrayList<a> c;
        @DexIgnore
        public CharSequence d;
        @DexIgnore
        public CharSequence e;
        @DexIgnore
        public PendingIntent f;
        @DexIgnore
        public PendingIntent g;
        @DexIgnore
        public RemoteViews h;
        @DexIgnore
        public Bitmap i;
        @DexIgnore
        public CharSequence j;
        @DexIgnore
        public int k;
        @DexIgnore
        public int l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public d o;
        @DexIgnore
        public CharSequence p;
        @DexIgnore
        public CharSequence[] q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public String u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public String w;
        @DexIgnore
        public boolean x;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public boolean z;

        @DexIgnore
        public c(Context context, String str) {
            this.b = new ArrayList<>();
            this.c = new ArrayList<>();
            this.m = true;
            this.x = false;
            this.C = 0;
            this.D = 0;
            this.J = 0;
            this.M = 0;
            this.N = new Notification();
            this.a = context;
            this.I = str;
            this.N.when = System.currentTimeMillis();
            this.N.audioStreamType = -1;
            this.l = 0;
            this.O = new ArrayList<>();
        }

        @DexIgnore
        public c a(long j2) {
            this.N.when = j2;
            return this;
        }

        @DexIgnore
        public c b(CharSequence charSequence) {
            this.d = d(charSequence);
            return this;
        }

        @DexIgnore
        public c c(int i2) {
            this.N.icon = i2;
            return this;
        }

        @DexIgnore
        public c d(boolean z2) {
            this.m = z2;
            return this;
        }

        @DexIgnore
        public static CharSequence d(CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        @DexIgnore
        public c a(CharSequence charSequence) {
            this.e = d(charSequence);
            return this;
        }

        @DexIgnore
        public c b(PendingIntent pendingIntent) {
            this.N.deleteIntent = pendingIntent;
            return this;
        }

        @DexIgnore
        public c c(CharSequence charSequence) {
            this.N.tickerText = d(charSequence);
            return this;
        }

        @DexIgnore
        public c a(PendingIntent pendingIntent) {
            this.f = pendingIntent;
            return this;
        }

        @DexIgnore
        public c b(Bitmap bitmap) {
            this.i = a(bitmap);
            return this;
        }

        @DexIgnore
        public c c(boolean z2) {
            a(2, z2);
            return this;
        }

        @DexIgnore
        public final Bitmap a(Bitmap bitmap) {
            if (bitmap == null || Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            Resources resources = this.a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(s5.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(s5.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = Math.min(((double) dimensionPixelSize) / ((double) Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) Math.max(1, bitmap.getHeight())));
            return Bitmap.createScaledBitmap(bitmap, (int) Math.ceil(((double) bitmap.getWidth()) * min), (int) Math.ceil(((double) bitmap.getHeight()) * min), true);
        }

        @DexIgnore
        public c b(boolean z2) {
            this.x = z2;
            return this;
        }

        @DexIgnore
        public c b(int i2) {
            this.l = i2;
            return this;
        }

        @DexIgnore
        public Bundle b() {
            if (this.B == null) {
                this.B = new Bundle();
            }
            return this.B;
        }

        @DexIgnore
        public c b(String str) {
            this.I = str;
            return this;
        }

        @DexIgnore
        @Deprecated
        public c(Context context) {
            this(context, (String) null);
        }

        @DexIgnore
        public c a(Uri uri) {
            Notification notification = this.N;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        @DexIgnore
        public c a(long[] jArr) {
            this.N.vibrate = jArr;
            return this;
        }

        @DexIgnore
        public c a(boolean z2) {
            a(16, z2);
            return this;
        }

        @DexIgnore
        public c a(String str) {
            this.A = str;
            return this;
        }

        @DexIgnore
        public final void a(int i2, boolean z2) {
            if (z2) {
                Notification notification = this.N;
                notification.flags = i2 | notification.flags;
                return;
            }
            Notification notification2 = this.N;
            notification2.flags = (~i2) & notification2.flags;
        }

        @DexIgnore
        public c a(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
            this.b.add(new a(i2, charSequence, pendingIntent));
            return this;
        }

        @DexIgnore
        public c a(a aVar) {
            this.b.add(aVar);
            return this;
        }

        @DexIgnore
        public c a(d dVar) {
            if (this.o != dVar) {
                this.o = dVar;
                d dVar2 = this.o;
                if (dVar2 != null) {
                    dVar2.a(this);
                }
            }
            return this;
        }

        @DexIgnore
        public c a(int i2) {
            this.C = i2;
            return this;
        }

        @DexIgnore
        public Notification a() {
            return new e6(this).b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d {
        @DexIgnore
        public c a;
        @DexIgnore
        public CharSequence b;
        @DexIgnore
        public CharSequence c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public void a(Bundle bundle) {
        }

        @DexIgnore
        public abstract void a(c6 c6Var);

        @DexIgnore
        public void a(c cVar) {
            if (this.a != cVar) {
                this.a = cVar;
                c cVar2 = this.a;
                if (cVar2 != null) {
                    cVar2.a(this);
                }
            }
        }

        @DexIgnore
        public RemoteViews b(c6 c6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews c(c6 c6Var) {
            return null;
        }

        @DexIgnore
        public RemoteViews d(c6 c6Var) {
            return null;
        }
    }

    @DexIgnore
    public static Bundle a(Notification notification) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return f6.a(notification);
        }
        return null;
    }
}
