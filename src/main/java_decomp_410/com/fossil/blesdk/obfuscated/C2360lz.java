package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lz */
public class C2360lz {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.nio.charset.Charset f7372b; // = java.nio.charset.Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.io.File f7373a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lz$a")
    /* renamed from: com.fossil.blesdk.obfuscated.lz$a */
    public static class C2361a extends org.json.JSONObject {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f00 f7374a;

        @DexIgnore
        public C2361a(com.fossil.blesdk.obfuscated.f00 f00) throws org.json.JSONException {
            this.f7374a = f00;
            put(com.misfit.frameworks.buttonservice.ButtonService.USER_ID, this.f7374a.f4980a);
            put("userName", this.f7374a.f4981b);
            put("userEmail", this.f7374a.f4982c);
        }
    }

    @DexIgnore
    public C2360lz(java.io.File file) {
        this.f7373a = file;
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.f00 m10483d(java.lang.String str) throws org.json.JSONException {
        org.json.JSONObject jSONObject = new org.json.JSONObject(str);
        return new com.fossil.blesdk.obfuscated.f00(m10482a(jSONObject, com.misfit.frameworks.buttonservice.ButtonService.USER_ID), m10482a(jSONObject, "userName"), m10482a(jSONObject, "userEmail"));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13489a(java.lang.String str, com.fossil.blesdk.obfuscated.f00 f00) {
        java.io.File b = mo13491b(str);
        java.io.BufferedWriter bufferedWriter = null;
        try {
            java.lang.String a = m10480a(f00);
            java.io.BufferedWriter bufferedWriter2 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(b), f7372b));
            try {
                bufferedWriter2.write(a);
                bufferedWriter2.flush();
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter2, "Failed to close user metadata file.");
            } catch (java.lang.Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error serializing user metadata.", e);
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close user metadata file.");
                } catch (Throwable th) {
                    th = th;
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close user metadata file.");
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error serializing user metadata.", e);
            p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close user metadata file.");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.io.File mo13491b(java.lang.String str) {
        java.io.File file = this.f7373a;
        return new java.io.File(file, str + "user" + ".meta");
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.f00 mo13492c(java.lang.String str) {
        java.io.File b = mo13491b(str);
        if (!b.exists()) {
            return com.fossil.blesdk.obfuscated.f00.f4979d;
        }
        java.io.FileInputStream fileInputStream = null;
        try {
            java.io.FileInputStream fileInputStream2 = new java.io.FileInputStream(b);
            try {
                com.fossil.blesdk.obfuscated.f00 d = m10483d(p011io.fabric.sdk.android.services.common.CommonUtils.m36880b((java.io.InputStream) fileInputStream2));
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) fileInputStream2, "Failed to close user metadata file.");
                return d;
            } catch (java.lang.Exception e) {
                e = e;
                fileInputStream = fileInputStream2;
                try {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error deserializing user metadata.", e);
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) fileInputStream, "Failed to close user metadata file.");
                    return com.fossil.blesdk.obfuscated.f00.f4979d;
                } catch (Throwable th) {
                    th = th;
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) fileInputStream, "Failed to close user metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileInputStream = fileInputStream2;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) fileInputStream, "Failed to close user metadata file.");
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error deserializing user metadata.", e);
            p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) fileInputStream, "Failed to close user metadata file.");
            return com.fossil.blesdk.obfuscated.f00.f4979d;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13490a(java.lang.String str, java.util.Map<java.lang.String, java.lang.String> map) {
        java.io.File a = mo13488a(str);
        java.io.BufferedWriter bufferedWriter = null;
        try {
            java.lang.String a2 = m10481a(map);
            java.io.BufferedWriter bufferedWriter2 = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(a), f7372b));
            try {
                bufferedWriter2.write(a2);
                bufferedWriter2.flush();
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter2, "Failed to close key/value metadata file.");
            } catch (java.lang.Exception e) {
                e = e;
                bufferedWriter = bufferedWriter2;
                try {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error serializing key/value metadata.", e);
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                } catch (Throwable th) {
                    th = th;
                    p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedWriter = bufferedWriter2;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close key/value metadata file.");
                throw th;
            }
        } catch (java.lang.Exception e2) {
            e = e2;
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Error serializing key/value metadata.", e);
            p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) bufferedWriter, "Failed to close key/value metadata file.");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.io.File mo13488a(java.lang.String str) {
        java.io.File file = this.f7373a;
        return new java.io.File(file, str + "keys" + ".meta");
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m10480a(com.fossil.blesdk.obfuscated.f00 f00) throws org.json.JSONException {
        return new com.fossil.blesdk.obfuscated.C2360lz.C2361a(f00).toString();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m10481a(java.util.Map<java.lang.String, java.lang.String> map) throws org.json.JSONException {
        return new org.json.JSONObject(map).toString();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m10482a(org.json.JSONObject jSONObject, java.lang.String str) {
        if (!jSONObject.isNull(str)) {
            return jSONObject.optString(str, (java.lang.String) null);
        }
        return null;
    }
}
