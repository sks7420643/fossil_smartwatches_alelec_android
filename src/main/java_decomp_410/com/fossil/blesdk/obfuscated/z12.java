package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class z12 {
    @DexIgnore
    public static /* final */ z12 b; // = new x12((z12) null, 0, 0);
    @DexIgnore
    public /* final */ z12 a;

    @DexIgnore
    public z12(z12 z12) {
        this.a = z12;
    }

    @DexIgnore
    public final z12 a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(a22 a22, byte[] bArr);

    @DexIgnore
    public final z12 b(int i, int i2) {
        return new u12(this, i, i2);
    }

    @DexIgnore
    public final z12 a(int i, int i2) {
        return new x12(this, i, i2);
    }
}
