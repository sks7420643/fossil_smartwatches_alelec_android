package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uk2 {
    @DexIgnore
    public static boolean a(Context context) {
        return (context instanceof Activity) && !((Activity) context).isFinishing();
    }
}
