package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jx */
public class C2167jx implements com.fossil.blesdk.obfuscated.t64 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3472zx f6605a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3328xx f6606b;

    @DexIgnore
    public C2167jx(com.fossil.blesdk.obfuscated.C3472zx zxVar, com.fossil.blesdk.obfuscated.C3328xx xxVar) {
        this.f6605a = zxVar;
        this.f6606b = xxVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2167jx m9204a(com.fossil.blesdk.obfuscated.C3472zx zxVar) {
        return new com.fossil.blesdk.obfuscated.C2167jx(zxVar, new com.fossil.blesdk.obfuscated.C3328xx(new com.fossil.blesdk.obfuscated.n64(new com.fossil.blesdk.obfuscated.C3247wx(new com.fossil.blesdk.obfuscated.l64(1000, 8), 0.1d), new com.fossil.blesdk.obfuscated.k64(5))));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12506a(java.util.List<java.io.File> list) {
        long nanoTime = java.lang.System.nanoTime();
        if (this.f6606b.mo17922a(nanoTime)) {
            if (this.f6605a.mo12506a(list)) {
                this.f6606b.mo17921a();
                return true;
            }
            this.f6606b.mo17923b(nanoTime);
        }
        return false;
    }
}
