package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vf2 extends uf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.rv_sleeps, 1);
        y.put(R.id.cpi_sleep, 2);
        y.put(R.id.ftvViewMore, 3);
        y.put(R.id.ivViewMore, 4);
        y.put(R.id.tv_no_record, 5);
    }
    */

    @DexIgnore
    public vf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 6, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public vf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[2], objArr[3], objArr[4], objArr[1], objArr[5]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
