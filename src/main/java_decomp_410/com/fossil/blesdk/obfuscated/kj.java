package com.fossil.blesdk.obfuscated;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kj {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public hl b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore
    public kj(UUID uuid, hl hlVar, Set<String> set) {
        this.a = uuid;
        this.b = hlVar;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public hl d() {
        return this.b;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<B extends a, W extends kj> {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public hl c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            this.c = new hl(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        public final B a(yi yiVar) {
            this.c.j = yiVar;
            c();
            return this;
        }

        @DexIgnore
        public abstract W b();

        @DexIgnore
        public abstract B c();

        @DexIgnore
        public final B a(String str) {
            this.d.add(str);
            c();
            return this;
        }

        @DexIgnore
        public final W a() {
            W b2 = b();
            this.b = UUID.randomUUID();
            this.c = new hl(this.c);
            this.c.a = this.b.toString();
            return b2;
        }
    }
}
