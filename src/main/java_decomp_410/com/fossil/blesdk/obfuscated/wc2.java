package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ BottomNavigationView q;
    @DexIgnore
    public /* final */ RecyclerViewPager r;

    @DexIgnore
    public wc2(Object obj, View view, int i, BottomNavigationView bottomNavigationView, RecyclerViewPager recyclerViewPager) {
        super(obj, view, i);
        this.q = bottomNavigationView;
        this.r = recyclerViewPager;
    }
}
