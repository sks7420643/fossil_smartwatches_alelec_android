package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pd;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.sd;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kd<K, V> extends qd<V> implements sd.a {
    @DexIgnore
    public /* final */ jd<K, V> s;
    @DexIgnore
    public int t; // = 0;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public /* final */ boolean y;
    @DexIgnore
    public pd.a<V> z; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pd.a<V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, pd<V> pdVar) {
            if (pdVar.a()) {
                kd.this.c();
            } else if (!kd.this.h()) {
                List<T> list = pdVar.a;
                boolean z = true;
                if (i == 0) {
                    kd kdVar = kd.this;
                    kdVar.i.a(pdVar.b, list, pdVar.c, pdVar.d, kdVar);
                    kd kdVar2 = kd.this;
                    if (kdVar2.j == -1) {
                        kdVar2.j = pdVar.b + pdVar.d + (list.size() / 2);
                    }
                } else {
                    kd kdVar3 = kd.this;
                    boolean z2 = kdVar3.j > kdVar3.i.f();
                    kd kdVar4 = kd.this;
                    boolean z3 = kdVar4.y && kdVar4.i.b(kdVar4.h.d, kdVar4.l, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            kd kdVar5 = kd.this;
                            kdVar5.i.a(list, (sd.a) kdVar5);
                        } else {
                            kd kdVar6 = kd.this;
                            kdVar6.w = 0;
                            kdVar6.u = 0;
                        }
                    } else if (i != 2) {
                        throw new IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        kd kdVar7 = kd.this;
                        kdVar7.i.b(list, (sd.a) kdVar7);
                    } else {
                        kd kdVar8 = kd.this;
                        kdVar8.v = 0;
                        kdVar8.t = 0;
                    }
                    kd kdVar9 = kd.this;
                    if (kdVar9.y) {
                        if (z2) {
                            if (kdVar9.t != 1 && kdVar9.i.b(kdVar9.x, kdVar9.h.d, kdVar9.l, kdVar9)) {
                                kd.this.t = 0;
                            }
                        } else if (kdVar9.u != 1 && kdVar9.i.a(kdVar9.x, kdVar9.h.d, kdVar9.l, (sd.a) kdVar9)) {
                            kd.this.u = 0;
                        }
                    }
                }
                kd kdVar10 = kd.this;
                if (kdVar10.g != null) {
                    boolean z4 = kdVar10.i.size() == 0;
                    boolean z5 = !z4 && i == 2 && pdVar.a.size() == 0;
                    if (!(!z4 && i == 1 && pdVar.a.size() == 0)) {
                        z = false;
                    }
                    kd.this.a(z4, z5, z);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public b(int i, Object obj) {
            this.e = i;
            this.f = obj;
        }

        @DexIgnore
        public void run() {
            if (!kd.this.h()) {
                if (kd.this.s.isInvalid()) {
                    kd.this.c();
                    return;
                }
                kd kdVar = kd.this;
                kdVar.s.dispatchLoadBefore(this.e, this.f, kdVar.h.a, kdVar.e, kdVar.z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public c(int i, Object obj) {
            this.e = i;
            this.f = obj;
        }

        @DexIgnore
        public void run() {
            if (!kd.this.h()) {
                if (kd.this.s.isInvalid()) {
                    kd.this.c();
                    return;
                }
                kd kdVar = kd.this;
                kdVar.s.dispatchLoadAfter(this.e, this.f, kdVar.h.a, kdVar.e, kdVar.z);
            }
        }
    }

    @DexIgnore
    public kd(jd<K, V> jdVar, Executor executor, Executor executor2, qd.c<V> cVar, qd.f fVar, K k, int i) {
        super(new sd(), executor, executor2, cVar, fVar);
        boolean z2 = false;
        this.s = jdVar;
        this.j = i;
        if (this.s.isInvalid()) {
            c();
        } else {
            jd<K, V> jdVar2 = this.s;
            qd.f fVar2 = this.h;
            jdVar2.dispatchLoadInitial(k, fVar2.e, fVar2.a, fVar2.c, this.e, this.z);
        }
        if (this.s.supportsPageDropping() && this.h.d != Integer.MAX_VALUE) {
            z2 = true;
        }
        this.y = z2;
    }

    @DexIgnore
    public static int c(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    public static int d(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    public void a(qd<V> qdVar, qd.e eVar) {
        sd<T> sdVar = qdVar.i;
        int g = this.i.g() - sdVar.g();
        int h = this.i.h() - sdVar.h();
        int l = sdVar.l();
        int e = sdVar.e();
        if (sdVar.isEmpty() || g < 0 || h < 0 || this.i.l() != Math.max(l - g, 0) || this.i.e() != Math.max(e - h, 0) || this.i.k() != sdVar.k() + g + h) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (g != 0) {
            int min = Math.min(l, g);
            int i = g - min;
            int e2 = sdVar.e() + sdVar.k();
            if (min != 0) {
                eVar.a(e2, min);
            }
            if (i != 0) {
                eVar.b(e2 + min, i);
            }
        }
        if (h != 0) {
            int min2 = Math.min(e, h);
            int i2 = h - min2;
            if (min2 != 0) {
                eVar.a(e, min2);
            }
            if (i2 != 0) {
                eVar.b(0, i2);
            }
        }
    }

    @DexIgnore
    public void b() {
        this.t = 2;
    }

    @DexIgnore
    public void c(int i, int i2) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public ld<?, V> d() {
        return this.s;
    }

    @DexIgnore
    public Object e() {
        return this.s.getKey(this.j, this.k);
    }

    @DexIgnore
    public void f(int i) {
        throw new IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    public boolean g() {
        return true;
    }

    @DexIgnore
    public void h(int i) {
        int d = d(this.h.b, i, this.i.e());
        int c2 = c(this.h.b, i, this.i.e() + this.i.k());
        this.v = Math.max(d, this.v);
        if (this.v > 0) {
            l();
        }
        this.w = Math.max(c2, this.w);
        if (this.w > 0) {
            k();
        }
    }

    @DexIgnore
    public final void k() {
        if (this.u == 0) {
            this.u = 1;
            this.f.execute(new c(((this.i.e() + this.i.k()) - 1) + this.i.j(), this.i.d()));
        }
    }

    @DexIgnore
    public final void l() {
        if (this.t == 0) {
            this.t = 1;
            this.f.execute(new b(this.i.e() + this.i.j(), this.i.c()));
        }
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        this.w = (this.w - i2) - i3;
        this.u = 0;
        if (this.w > 0) {
            k();
        }
        d(i, i2);
        e(i + i2, i3);
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public void a(int i) {
        boolean z2 = false;
        e(0, i);
        if (this.i.e() > 0 || this.i.l() > 0) {
            z2 = true;
        }
        this.x = z2;
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        this.v = (this.v - i2) - i3;
        this.t = 0;
        if (this.v > 0) {
            l();
        }
        d(i, i2);
        e(0, i3);
        i(i3);
    }

    @DexIgnore
    public void a() {
        this.u = 2;
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }
}
