package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qx2 implements Factory<px2> {
    @DexIgnore
    public static /* final */ qx2 a; // = new qx2();

    @DexIgnore
    public static qx2 a() {
        return a;
    }

    @DexIgnore
    public static px2 b() {
        return new px2();
    }

    @DexIgnore
    public px2 get() {
        return b();
    }
}
