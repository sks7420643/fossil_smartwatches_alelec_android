package com.fossil.blesdk.obfuscated;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gk2 implements sr<hk2, InputStream> {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ a b; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gk2.a;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tr<hk2, InputStream> {
        @DexIgnore
        public gk2 a(wr wrVar) {
            kd4.b(wrVar, "multiFactory");
            return new gk2();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements so<InputStream> {
        @DexIgnore
        public volatile boolean e;
        @DexIgnore
        public /* final */ hk2 f;

        @DexIgnore
        public c(gk2 gk2, hk2 hk2) {
            this.f = hk2;
        }

        @DexIgnore
        public void a() {
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(2:38|39) */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            r4 = com.portfolio.platform.PortfolioApp.W.c().getPackageManager().getApplicationIcon(r4.packageName);
         */
        @DexIgnore
        /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x00e1 */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x023f  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x024b  */
        public void a(Priority priority, so.a<? super InputStream> aVar) {
            ByteArrayInputStream byteArrayInputStream;
            Bitmap bitmap;
            Drawable drawable;
            Bitmap bitmap2;
            kd4.b(priority, "priority");
            kd4.b(aVar, Constants.CALLBACK);
            ArrayList<Bitmap> arrayList = new ArrayList<>(5);
            hk2 hk2 = this.f;
            if (hk2 != null) {
                List<BaseFeatureModel> a = hk2.a();
                if (a != null) {
                    Iterator<BaseFeatureModel> it = a.iterator();
                    int i = 0;
                    while (true) {
                        byteArrayInputStream = null;
                        if (!it.hasNext()) {
                            break;
                        }
                        BaseFeatureModel next = it.next();
                        if (next instanceof ContactGroup) {
                            for (Contact next2 : ((ContactGroup) next).getContacts()) {
                                if (arrayList.size() < 5) {
                                    kd4.a((Object) next2, "contactItem");
                                    if (!TextUtils.isEmpty(next2.getPhotoThumbUri())) {
                                        bitmap2 = vr3.a(Long.valueOf((long) next2.getContactId()));
                                    } else if (next2.getContactId() == -100) {
                                        bitmap2 = vr3.a(k6.c(PortfolioApp.W.c(), R.drawable.ic_notifications_calls));
                                    } else if (next2.getContactId() == -200) {
                                        bitmap2 = vr3.a(k6.c(PortfolioApp.W.c(), R.drawable.ic_notifications_texts));
                                    } else {
                                        bitmap2 = vr3.a(next2);
                                    }
                                    if (bitmap2 != null) {
                                        arrayList.add(bitmap2);
                                    }
                                }
                                i++;
                            }
                        }
                        if (next instanceof AppFilter) {
                            if (arrayList.size() < 5) {
                                PackageManager packageManager = PortfolioApp.W.c().getPackageManager();
                                try {
                                    ApplicationInfo applicationInfo = packageManager.getApplicationInfo(((AppFilter) next).getType(), 128);
                                    kd4.a((Object) applicationInfo, "packageManager.getApplic\u2026ageManager.GET_META_DATA)");
                                    if (applicationInfo.icon != 0) {
                                        drawable = packageManager.getResourcesForApplication(applicationInfo).getDrawableForDensity(applicationInfo.icon, 480, (Resources.Theme) null);
                                    } else {
                                        drawable = PortfolioApp.W.c().getPackageManager().getApplicationIcon(applicationInfo.packageName);
                                    }
                                    Bitmap a2 = vr3.a(drawable);
                                    if (a2 != null) {
                                        arrayList.add(a2);
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                            i++;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: itemCounter = " + i);
                    ArrayList arrayList2 = new ArrayList(db4.a(arrayList, 10));
                    for (Bitmap bitmap3 : arrayList) {
                        arrayList2.add(Integer.valueOf(Math.max(bitmap3.getWidth(), bitmap3.getHeight())));
                    }
                    Integer num = (Integer) kb4.e(arrayList2);
                    int intValue = num != null ? num.intValue() : 0;
                    if (i != 0) {
                        if (i != 1) {
                            if (i != 2) {
                                if (i != 3) {
                                    if (i != 4) {
                                        StringBuilder sb = new StringBuilder();
                                        sb.append('+');
                                        sb.append((i - 4) + 1);
                                        Bitmap a3 = vr3.a(sb.toString());
                                        Bitmap a4 = vr3.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), a3, intValue);
                                        if (a3 != null) {
                                            arrayList.add(a3);
                                        }
                                        bitmap = a4;
                                    } else if (arrayList.size() > 3) {
                                        bitmap = vr3.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), (Bitmap) arrayList.get(3), intValue);
                                    }
                                    FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: result = " + bitmap);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    if (bitmap != null) {
                                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                                    }
                                    if (!this.e) {
                                        byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                                    }
                                    aVar.a(byteArrayInputStream);
                                } else if (arrayList.size() > 2) {
                                    bitmap = vr3.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), (Bitmap) arrayList.get(2), intValue);
                                    FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: result = " + bitmap);
                                    ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                                    if (bitmap != null) {
                                    }
                                    if (!this.e) {
                                    }
                                    aVar.a(byteArrayInputStream);
                                }
                            } else if (arrayList.size() > 1) {
                                bitmap = vr3.a((Bitmap) arrayList.get(0), (Bitmap) arrayList.get(1), intValue);
                                FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: result = " + bitmap);
                                ByteArrayOutputStream byteArrayOutputStream22 = new ByteArrayOutputStream();
                                if (bitmap != null) {
                                }
                                if (!this.e) {
                                }
                                aVar.a(byteArrayInputStream);
                            }
                        } else if (arrayList.size() > 0) {
                            bitmap = (Bitmap) arrayList.get(0);
                            FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: result = " + bitmap);
                            ByteArrayOutputStream byteArrayOutputStream222 = new ByteArrayOutputStream();
                            if (bitmap != null) {
                            }
                            if (!this.e) {
                            }
                            aVar.a(byteArrayInputStream);
                        }
                    }
                    bitmap = null;
                    FLogger.INSTANCE.getLocal().d(gk2.b.a(), "loadData: result = " + bitmap);
                    ByteArrayOutputStream byteArrayOutputStream2222 = new ByteArrayOutputStream();
                    if (bitmap != null) {
                    }
                    if (!this.e) {
                    }
                    aVar.a(byteArrayInputStream);
                }
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
            this.e = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    /*
    static {
        String simpleName = gk2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationLoader::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public boolean a(hk2 hk2) {
        kd4.b(hk2, "notificationModel");
        return true;
    }

    @DexIgnore
    public sr.a<InputStream> a(hk2 hk2, int i, int i2, lo loVar) {
        kd4.b(hk2, "notificationModel");
        kd4.b(loVar, "options");
        return new sr.a<>(hk2, new c(this, hk2));
    }
}
