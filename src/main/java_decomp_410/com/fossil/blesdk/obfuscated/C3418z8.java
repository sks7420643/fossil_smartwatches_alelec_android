package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z8 */
public interface C3418z8 {
    @DexIgnore
    boolean onNestedFling(android.view.View view, float f, float f2, boolean z);

    @DexIgnore
    boolean onNestedPreFling(android.view.View view, float f, float f2);

    @DexIgnore
    void onNestedPreScroll(android.view.View view, int i, int i2, int[] iArr);

    @DexIgnore
    void onNestedScroll(android.view.View view, int i, int i2, int i3, int i4);

    @DexIgnore
    void onNestedScrollAccepted(android.view.View view, android.view.View view2, int i);

    @DexIgnore
    boolean onStartNestedScroll(android.view.View view, android.view.View view2, int i);

    @DexIgnore
    void onStopNestedScroll(android.view.View view);
}
