package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f41 extends r41 {
    @DexIgnore
    public /* final */ y31 G;

    @DexIgnore
    public f41(Context context, Looper looper, ge0.b bVar, ge0.c cVar, String str, kj0 kj0) {
        super(context, looper, bVar, cVar, str, kj0);
        this.G = new y31(context, this.F);
    }

    @DexIgnore
    public final Location G() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.c();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final void a(i41 i41, ze0<qc1> ze0, r31 r31) throws RemoteException {
        synchronized (this.G) {
            this.G.a(i41, ze0, r31);
        }
    }

    @DexIgnore
    public final void a(tc1 tc1, ue0<vc1> ue0, String str) throws RemoteException {
        p();
        boolean z = true;
        bk0.a(tc1 != null, (Object) "locationSettingsRequest can't be null nor empty.");
        if (ue0 == null) {
            z = false;
        }
        bk0.a(z, (Object) "listener can't be null.");
        ((u31) x()).a(tc1, new h41(ue0), str);
    }

    @DexIgnore
    public final void a(ze0.a<rc1> aVar, r31 r31) throws RemoteException {
        this.G.a(aVar, r31);
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, ze0<rc1> ze0, r31 r31) throws RemoteException {
        synchronized (this.G) {
            this.G.a(locationRequest, ze0, r31);
        }
    }

    @DexIgnore
    public final void b(ze0.a<qc1> aVar, r31 r31) throws RemoteException {
        this.G.b(aVar, r31);
    }
}
