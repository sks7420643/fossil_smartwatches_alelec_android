package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jz {
    @DexIgnore
    public static /* final */ c d; // = new c();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public hz c;

    @DexIgnore
    public interface b {
        @DexIgnore
        File a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements hz {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(long j, String str) {
        }

        @DexIgnore
        public oy b() {
            return null;
        }

        @DexIgnore
        public byte[] c() {
            return null;
        }

        @DexIgnore
        public void d() {
        }
    }

    @DexIgnore
    public jz(Context context, b bVar) {
        this(context, bVar, (String) null);
    }

    @DexIgnore
    public void a(long j, String str) {
        this.c.a(j, str);
    }

    @DexIgnore
    public final void b(String str) {
        this.c.a();
        this.c = d;
        if (str != null) {
            if (!CommonUtils.a(this.a, "com.crashlytics.CollectCustomLogs", true)) {
                q44.g().d("CrashlyticsCore", "Preferences requested no custom logs. Aborting log file creation.");
            } else {
                a(a(str), 65536);
            }
        }
    }

    @DexIgnore
    public byte[] c() {
        return this.c.c();
    }

    @DexIgnore
    public jz(Context context, b bVar, String str) {
        this.a = context;
        this.b = bVar;
        this.c = d;
        b(str);
    }

    @DexIgnore
    public void a() {
        this.c.d();
    }

    @DexIgnore
    public void a(Set<String> set) {
        File[] listFiles = this.b.a().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (!set.contains(a(file))) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    public void a(File file, int i) {
        this.c = new vz(file, i);
    }

    @DexIgnore
    public oy b() {
        return this.c.b();
    }

    @DexIgnore
    public final File a(String str) {
        return new File(this.b.a(), "crashlytics-userlog-" + str + ".temp");
    }

    @DexIgnore
    public final String a(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        if (lastIndexOf == -1) {
            return name;
        }
        return name.substring(20, lastIndexOf);
    }
}
