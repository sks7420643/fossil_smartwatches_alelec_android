package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vi4<T> extends ki4<li4> {
    @DexIgnore
    public /* final */ eg4<T> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vi4(li4 li4, eg4<? super T> eg4) {
        super(li4);
        kd4.b(li4, "job");
        kd4.b(eg4, "continuation");
        this.i = eg4;
    }

    @DexIgnore
    public void b(Throwable th) {
        Object d = ((li4) this.h).d();
        if (ch4.a() && !(!(d instanceof ai4))) {
            throw new AssertionError();
        } else if (d instanceof ng4) {
            this.i.a(((ng4) d).a, 0);
        } else {
            eg4<T> eg4 = this.i;
            Object b = mi4.b(d);
            Result.a aVar = Result.Companion;
            eg4.resumeWith(Result.m3constructorimpl(b));
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.i + ']';
    }
}
