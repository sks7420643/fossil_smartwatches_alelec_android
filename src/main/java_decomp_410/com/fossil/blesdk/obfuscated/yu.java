package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yu implements ru {
    @DexIgnore
    public /* final */ Set<bw<?>> e; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void a(bw<?> bwVar) {
        this.e.add(bwVar);
    }

    @DexIgnore
    public void b(bw<?> bwVar) {
        this.e.remove(bwVar);
    }

    @DexIgnore
    public void c() {
        for (T c : uw.a(this.e)) {
            c.c();
        }
    }

    @DexIgnore
    public void e() {
        this.e.clear();
    }

    @DexIgnore
    public List<bw<?>> f() {
        return uw.a(this.e);
    }

    @DexIgnore
    public void a() {
        for (T a : uw.a(this.e)) {
            a.a();
        }
    }

    @DexIgnore
    public void b() {
        for (T b : uw.a(this.e)) {
            b.b();
        }
    }
}
