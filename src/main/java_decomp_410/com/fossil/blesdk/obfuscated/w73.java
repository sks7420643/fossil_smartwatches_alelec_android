package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w73 implements Factory<HomeDashboardPresenter> {
    @DexIgnore
    public static HomeDashboardPresenter a(v73 v73, PortfolioApp portfolioApp, DeviceRepository deviceRepository, vj2 vj2, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository) {
        return new HomeDashboardPresenter(v73, portfolioApp, deviceRepository, vj2, summariesRepository, goalTrackingRepository, sleepSummariesRepository, heartRateSampleRepository);
    }
}
