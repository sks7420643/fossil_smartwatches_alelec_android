package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ca */
public interface C1541ca {
    @DexIgnore
    void setSupportButtonTintList(android.content.res.ColorStateList colorStateList);

    @DexIgnore
    void setSupportButtonTintMode(android.graphics.PorterDuff.Mode mode);
}
