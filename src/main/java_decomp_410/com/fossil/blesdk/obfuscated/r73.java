package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r73 implements Factory<za3> {
    @DexIgnore
    public static za3 a(n73 n73) {
        za3 d = n73.d();
        n44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
