package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b1 */
public abstract class C1458b1 implements com.fossil.blesdk.obfuscated.C2618p1 {

    @DexIgnore
    /* renamed from: e */
    public android.content.Context f3636e;

    @DexIgnore
    /* renamed from: f */
    public android.content.Context f3637f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1915h1 f3638g;

    @DexIgnore
    /* renamed from: h */
    public android.view.LayoutInflater f3639h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f3640i;

    @DexIgnore
    /* renamed from: j */
    public int f3641j;

    @DexIgnore
    /* renamed from: k */
    public int f3642k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2697q1 f3643l;

    @DexIgnore
    /* renamed from: m */
    public int f3644m;

    @DexIgnore
    public C1458b1(android.content.Context context, int i, int i2) {
        this.f3636e = context;
        this.f3639h = android.view.LayoutInflater.from(context);
        this.f3641j = i;
        this.f3642k = i2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1158a(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        this.f3637f = context;
        android.view.LayoutInflater.from(this.f3637f);
        this.f3638g = h1Var;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9012a(com.fossil.blesdk.obfuscated.C2179k1 k1Var, com.fossil.blesdk.obfuscated.C2697q1.C2698a aVar);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo9014a(int i, com.fossil.blesdk.obfuscated.C2179k1 k1Var);

    @DexIgnore
    /* renamed from: a */
    public boolean mo1163a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2697q1 mo9016b(android.view.ViewGroup viewGroup) {
        if (this.f3643l == null) {
            this.f3643l = (com.fossil.blesdk.obfuscated.C2697q1) this.f3639h.inflate(this.f3641j, viewGroup, false);
            this.f3643l.mo374a(this.f3638g);
            mo1161a(true);
        }
        return this.f3643l;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo1166b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a mo9017c() {
        return this.f3640i;
    }

    @DexIgnore
    public int getId() {
        return this.f3644m;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1161a(boolean z) {
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f3643l;
        if (viewGroup != null) {
            com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f3638g;
            int i = 0;
            if (h1Var != null) {
                h1Var.mo11480b();
                java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> n = this.f3638g.mo11515n();
                int size = n.size();
                int i2 = 0;
                for (int i3 = 0; i3 < size; i3++) {
                    com.fossil.blesdk.obfuscated.C2179k1 k1Var = n.get(i3);
                    if (mo9014a(i2, k1Var)) {
                        android.view.View childAt = viewGroup.getChildAt(i2);
                        com.fossil.blesdk.obfuscated.C2179k1 itemData = childAt instanceof com.fossil.blesdk.obfuscated.C2697q1.C2698a ? ((com.fossil.blesdk.obfuscated.C2697q1.C2698a) childAt).getItemData() : null;
                        android.view.View a = mo9008a(k1Var, childAt, viewGroup);
                        if (k1Var != itemData) {
                            a.setPressed(false);
                            a.jumpDrawablesToCurrentState();
                        }
                        if (a != childAt) {
                            mo9011a(a, i2);
                        }
                        i2++;
                    }
                }
                i = i2;
            }
            while (i < viewGroup.getChildCount()) {
                if (!mo9015a(viewGroup, i)) {
                    i++;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9011a(android.view.View view, int i) {
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((android.view.ViewGroup) this.f3643l).addView(view, i);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9015a(android.view.ViewGroup viewGroup, int i) {
        viewGroup.removeViewAt(i);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9013a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        this.f3640i = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2697q1.C2698a mo9009a(android.view.ViewGroup viewGroup) {
        return (com.fossil.blesdk.obfuscated.C2697q1.C2698a) this.f3639h.inflate(this.f3642k, viewGroup, false);
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo9008a(com.fossil.blesdk.obfuscated.C2179k1 k1Var, android.view.View view, android.view.ViewGroup viewGroup) {
        com.fossil.blesdk.obfuscated.C2697q1.C2698a aVar;
        if (view instanceof com.fossil.blesdk.obfuscated.C2697q1.C2698a) {
            aVar = (com.fossil.blesdk.obfuscated.C2697q1.C2698a) view;
        } else {
            aVar = mo9009a(viewGroup);
        }
        mo9012a(k1Var, aVar);
        return (android.view.View) aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f3640i;
        if (aVar != null) {
            aVar.mo533a(h1Var, z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f3640i;
        if (aVar != null) {
            return aVar.mo534a(v1Var);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9010a(int i) {
        this.f3644m = i;
    }
}
