package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t4 */
public class C2941t4<T> implements com.fossil.blesdk.obfuscated.C2861s4<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object[] f9543a;

    @DexIgnore
    /* renamed from: b */
    public int f9544b;

    @DexIgnore
    public C2941t4(int i) {
        if (i > 0) {
            this.f9543a = new java.lang.Object[i];
            return;
        }
        throw new java.lang.IllegalArgumentException("The max pool size must be > 0");
    }

    @DexIgnore
    /* renamed from: a */
    public T mo15893a() {
        int i = this.f9544b;
        if (i <= 0) {
            return null;
        }
        int i2 = i - 1;
        T[] tArr = this.f9543a;
        T t = tArr[i2];
        tArr[i2] = null;
        this.f9544b = i - 1;
        return t;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo15895a(T t) {
        int i = this.f9544b;
        java.lang.Object[] objArr = this.f9543a;
        if (i >= objArr.length) {
            return false;
        }
        objArr[i] = t;
        this.f9544b = i + 1;
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15894a(T[] tArr, int i) {
        if (i > tArr.length) {
            i = tArr.length;
        }
        for (int i2 = 0; i2 < i; i2++) {
            T t = tArr[i2];
            int i3 = this.f9544b;
            java.lang.Object[] objArr = this.f9543a;
            if (i3 < objArr.length) {
                objArr[i3] = t;
                this.f9544b = i3 + 1;
            }
        }
    }
}
