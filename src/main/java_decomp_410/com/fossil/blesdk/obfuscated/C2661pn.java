package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pn */
public final class C2661pn implements com.fossil.blesdk.obfuscated.C3062uu.C3064b {
    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo14809a(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C2770qu quVar, com.fossil.blesdk.obfuscated.C3141vu vuVar, android.content.Context context) {
        return new com.fossil.blesdk.obfuscated.fk2(rnVar, quVar, vuVar, context);
    }
}
