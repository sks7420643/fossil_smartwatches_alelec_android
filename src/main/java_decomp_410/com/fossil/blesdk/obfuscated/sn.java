package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.br;
import com.fossil.blesdk.obfuscated.rn;
import com.fossil.blesdk.obfuscated.tq;
import com.fossil.blesdk.obfuscated.uu;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sn {
    @DexIgnore
    public /* final */ Map<Class<?>, yn<?, ?>> a; // = new g4();
    @DexIgnore
    public qp b;
    @DexIgnore
    public jq c;
    @DexIgnore
    public gq d;
    @DexIgnore
    public ar e;
    @DexIgnore
    public dr f;
    @DexIgnore
    public dr g;
    @DexIgnore
    public tq.a h;
    @DexIgnore
    public br i;
    @DexIgnore
    public mu j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public rn.a l; // = new a(this);
    @DexIgnore
    public uu.b m;
    @DexIgnore
    public dr n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<qv<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s; // = 700;
    @DexIgnore
    public int t; // = 128;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements rn.a {
        @DexIgnore
        public a(sn snVar) {
        }

        @DexIgnore
        public rv build() {
            return new rv();
        }
    }

    @DexIgnore
    public void a(uu.b bVar) {
        this.m = bVar;
    }

    @DexIgnore
    public rn a(Context context) {
        Context context2 = context;
        if (this.f == null) {
            this.f = dr.d();
        }
        if (this.g == null) {
            this.g = dr.c();
        }
        if (this.n == null) {
            this.n = dr.b();
        }
        if (this.i == null) {
            this.i = new br.a(context2).a();
        }
        if (this.j == null) {
            this.j = new ou();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new pq((long) b2);
            } else {
                this.c = new kq();
            }
        }
        if (this.d == null) {
            this.d = new oq(this.i.a());
        }
        if (this.e == null) {
            this.e = new zq((long) this.i.c());
        }
        if (this.h == null) {
            this.h = new yq(context2);
        }
        if (this.b == null) {
            this.b = new qp(this.e, this.h, this.g, this.f, dr.e(), this.n, this.o);
        }
        List<qv<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new rn(context, this.b, this.e, this.c, this.d, new uu(this.m), this.j, this.k, this.l, this.a, this.p, this.q, this.r, this.s, this.t);
    }
}
