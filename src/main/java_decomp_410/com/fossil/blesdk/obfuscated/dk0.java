package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.tj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dk0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dk0> CREATOR; // = new ll0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public IBinder f;
    @DexIgnore
    public ud0 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public dk0(int i2, IBinder iBinder, ud0 ud0, boolean z, boolean z2) {
        this.e = i2;
        this.f = iBinder;
        this.g = ud0;
        this.h = z;
        this.i = z2;
    }

    @DexIgnore
    public tj0 H() {
        return tj0.a.a(this.f);
    }

    @DexIgnore
    public ud0 I() {
        return this.g;
    }

    @DexIgnore
    public boolean J() {
        return this.h;
    }

    @DexIgnore
    public boolean K() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dk0)) {
            return false;
        }
        dk0 dk0 = (dk0) obj;
        return this.g.equals(dk0.g) && H().equals(dk0.H());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f, false);
        kk0.a(parcel, 3, (Parcelable) I(), i2, false);
        kk0.a(parcel, 4, J());
        kk0.a(parcel, 5, K());
        kk0.a(parcel, a);
    }
}
