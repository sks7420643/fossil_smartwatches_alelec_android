package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qy */
public class C2774qy extends java.io.FileOutputStream {

    @DexIgnore
    /* renamed from: h */
    public static /* final */ java.io.FilenameFilter f8807h; // = new com.fossil.blesdk.obfuscated.C2774qy.C2775a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f8808e;

    @DexIgnore
    /* renamed from: f */
    public java.io.File f8809f;

    @DexIgnore
    /* renamed from: g */
    public boolean f8810g; // = false;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qy$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qy$a */
    public static class C2775a implements java.io.FilenameFilter {
        @DexIgnore
        public boolean accept(java.io.File file, java.lang.String str) {
            return str.endsWith(".cls_temp");
        }
    }

    @DexIgnore
    public C2774qy(java.io.File file, java.lang.String str) throws java.io.FileNotFoundException {
        super(new java.io.File(file, str + ".cls_temp"));
        this.f8808e = file + java.io.File.separator + str;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append(this.f8808e);
        sb.append(".cls_temp");
        this.f8809f = new java.io.File(sb.toString());
    }

    @DexIgnore
    public synchronized void close() throws java.io.IOException {
        if (!this.f8810g) {
            this.f8810g = true;
            super.flush();
            super.close();
            java.io.File file = new java.io.File(this.f8808e + ".cls");
            if (this.f8809f.renameTo(file)) {
                this.f8809f = null;
                return;
            }
            java.lang.String str = "";
            if (file.exists()) {
                str = " (target already exists)";
            } else if (!this.f8809f.exists()) {
                str = " (source does not exist)";
            }
            throw new java.io.IOException("Could not rename temp file: " + this.f8809f + " -> " + file + str);
        }
    }

    @DexIgnore
    /* renamed from: y */
    public void mo15398y() throws java.io.IOException {
        if (!this.f8810g) {
            this.f8810g = true;
            super.flush();
            super.close();
        }
    }
}
