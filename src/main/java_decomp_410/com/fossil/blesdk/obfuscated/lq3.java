package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lq3 extends v52<kq3> {
    @DexIgnore
    void K0();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(long j);

    @DexIgnore
    void a(WatchSettingViewModel.c cVar);

    @DexIgnore
    void a(Double d, Double d2);

    @DexIgnore
    void b(boolean z, boolean z2);

    @DexIgnore
    void b0();

    @DexIgnore
    void k(int i);

    @DexIgnore
    void y(String str);
}
