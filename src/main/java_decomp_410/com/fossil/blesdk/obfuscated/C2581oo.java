package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oo */
public interface C2581oo<T> extends com.fossil.blesdk.obfuscated.C2143jo {
    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C1438aq<T> mo12877a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<T> aqVar, int i, int i2);
}
