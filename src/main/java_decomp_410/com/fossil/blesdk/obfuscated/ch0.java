package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ch0 {
    @DexIgnore
    void a(int i, boolean z);

    @DexIgnore
    void a(Bundle bundle);

    @DexIgnore
    void a(ud0 ud0);
}
