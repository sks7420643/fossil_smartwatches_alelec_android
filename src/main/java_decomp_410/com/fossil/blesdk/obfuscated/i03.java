package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i03 implements MembersInjector<NotificationHybridContactActivity> {
    @DexIgnore
    public static void a(NotificationHybridContactActivity notificationHybridContactActivity, NotificationHybridContactPresenter notificationHybridContactPresenter) {
        notificationHybridContactActivity.B = notificationHybridContactPresenter;
    }
}
