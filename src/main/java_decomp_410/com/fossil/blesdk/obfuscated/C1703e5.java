package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e5 */
public class C1703e5 extends com.fossil.blesdk.obfuscated.C1857g5 {

    @DexIgnore
    /* renamed from: c */
    public androidx.constraintlayout.solver.widgets.ConstraintAnchor f4700c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1703e5 f4701d;

    @DexIgnore
    /* renamed from: e */
    public float f4702e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1703e5 f4703f;

    @DexIgnore
    /* renamed from: g */
    public float f4704g;

    @DexIgnore
    /* renamed from: h */
    public int f4705h; // = 0;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C1703e5 f4706i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C1770f5 f4707j; // = null;

    @DexIgnore
    /* renamed from: k */
    public int f4708k; // = 1;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1770f5 f4709l; // = null;

    @DexIgnore
    /* renamed from: m */
    public int f4710m; // = 1;

    @DexIgnore
    public C1703e5(androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor) {
        this.f4700c = constraintAnchor;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo10323a(int i) {
        return i == 1 ? "DIRECT" : i == 2 ? "CENTER" : i == 3 ? "MATCH" : i == 4 ? "CHAIN" : i == 5 ? "BARRIER" : "UNCONNECTED";
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10325a(com.fossil.blesdk.obfuscated.C1703e5 e5Var, float f) {
        if (this.f5384b == 0 || !(this.f4703f == e5Var || this.f4704g == f)) {
            this.f4703f = e5Var;
            this.f4704g = f;
            if (this.f5384b == 1) {
                mo11139b();
            }
            mo11137a();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10329b(int i) {
        this.f4705h = i;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10332d() {
        super.mo10332d();
        this.f4701d = null;
        this.f4702e = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f4707j = null;
        this.f4708k = 1;
        this.f4709l = null;
        this.f4710m = 1;
        this.f4703f = null;
        this.f4704g = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f4706i = null;
        this.f4705h = 0;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo10333e() {
        float f;
        float f2;
        float f3;
        float f4;
        boolean z = true;
        if (this.f5384b != 1 && this.f4705h != 4) {
            com.fossil.blesdk.obfuscated.C1770f5 f5Var = this.f4707j;
            if (f5Var != null) {
                if (f5Var.f5384b == 1) {
                    this.f4702e = ((float) this.f4708k) * f5Var.f5028c;
                } else {
                    return;
                }
            }
            com.fossil.blesdk.obfuscated.C1770f5 f5Var2 = this.f4709l;
            if (f5Var2 != null) {
                if (f5Var2.f5384b == 1) {
                    float f5 = f5Var2.f5028c;
                } else {
                    return;
                }
            }
            if (this.f4705h == 1) {
                com.fossil.blesdk.obfuscated.C1703e5 e5Var = this.f4701d;
                if (e5Var == null || e5Var.f5384b == 1) {
                    com.fossil.blesdk.obfuscated.C1703e5 e5Var2 = this.f4701d;
                    if (e5Var2 == null) {
                        this.f4703f = this;
                        this.f4704g = this.f4702e;
                    } else {
                        this.f4703f = e5Var2.f4703f;
                        this.f4704g = e5Var2.f4704g + this.f4702e;
                    }
                    mo11137a();
                    return;
                }
            }
            if (this.f4705h == 2) {
                com.fossil.blesdk.obfuscated.C1703e5 e5Var3 = this.f4701d;
                if (e5Var3 != null && e5Var3.f5384b == 1) {
                    com.fossil.blesdk.obfuscated.C1703e5 e5Var4 = this.f4706i;
                    if (e5Var4 != null) {
                        com.fossil.blesdk.obfuscated.C1703e5 e5Var5 = e5Var4.f4701d;
                        if (e5Var5 != null && e5Var5.f5384b == 1) {
                            if (com.fossil.blesdk.obfuscated.C2703q4.m12575j() != null) {
                                com.fossil.blesdk.obfuscated.C2703q4.m12575j().f8852v++;
                            }
                            this.f4703f = this.f4701d.f4703f;
                            com.fossil.blesdk.obfuscated.C1703e5 e5Var6 = this.f4706i;
                            e5Var6.f4703f = e5Var6.f4701d.f4703f;
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type = this.f4700c.f651c;
                            int i = 0;
                            if (!(type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT || type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM)) {
                                z = false;
                            }
                            if (z) {
                                f2 = this.f4701d.f4704g;
                                f = this.f4706i.f4701d.f4704g;
                            } else {
                                f2 = this.f4706i.f4701d.f4704g;
                                f = this.f4701d.f4704g;
                            }
                            float f6 = f2 - f;
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = this.f4700c;
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type2 = constraintAnchor.f651c;
                            if (type2 == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT || type2 == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT) {
                                f4 = f6 - ((float) this.f4700c.f650b.mo1352t());
                                f3 = this.f4700c.f650b.f684V;
                            } else {
                                f4 = f6 - ((float) constraintAnchor.f650b.mo1332j());
                                f3 = this.f4700c.f650b.f685W;
                            }
                            int b = this.f4700c.mo1264b();
                            int b2 = this.f4706i.f4700c.mo1264b();
                            if (this.f4700c.mo1269g() == this.f4706i.f4700c.mo1269g()) {
                                f3 = 0.5f;
                                b2 = 0;
                            } else {
                                i = b;
                            }
                            float f7 = (float) i;
                            float f8 = (float) b2;
                            float f9 = (f4 - f7) - f8;
                            if (z) {
                                com.fossil.blesdk.obfuscated.C1703e5 e5Var7 = this.f4706i;
                                e5Var7.f4704g = e5Var7.f4701d.f4704g + f8 + (f9 * f3);
                                this.f4704g = (this.f4701d.f4704g - f7) - (f9 * (1.0f - f3));
                            } else {
                                this.f4704g = this.f4701d.f4704g + f7 + (f9 * f3);
                                com.fossil.blesdk.obfuscated.C1703e5 e5Var8 = this.f4706i;
                                e5Var8.f4704g = (e5Var8.f4701d.f4704g - f8) - (f9 * (1.0f - f3));
                            }
                            mo11137a();
                            this.f4706i.mo11137a();
                            return;
                        }
                    }
                }
            }
            if (this.f4705h == 3) {
                com.fossil.blesdk.obfuscated.C1703e5 e5Var9 = this.f4701d;
                if (e5Var9 != null && e5Var9.f5384b == 1) {
                    com.fossil.blesdk.obfuscated.C1703e5 e5Var10 = this.f4706i;
                    if (e5Var10 != null) {
                        com.fossil.blesdk.obfuscated.C1703e5 e5Var11 = e5Var10.f4701d;
                        if (e5Var11 != null && e5Var11.f5384b == 1) {
                            if (com.fossil.blesdk.obfuscated.C2703q4.m12575j() != null) {
                                com.fossil.blesdk.obfuscated.C2703q4.m12575j().f8853w++;
                            }
                            com.fossil.blesdk.obfuscated.C1703e5 e5Var12 = this.f4701d;
                            this.f4703f = e5Var12.f4703f;
                            com.fossil.blesdk.obfuscated.C1703e5 e5Var13 = this.f4706i;
                            com.fossil.blesdk.obfuscated.C1703e5 e5Var14 = e5Var13.f4701d;
                            e5Var13.f4703f = e5Var14.f4703f;
                            this.f4704g = e5Var12.f4704g + this.f4702e;
                            e5Var13.f4704g = e5Var14.f4704g + e5Var13.f4702e;
                            mo11137a();
                            this.f4706i.mo11137a();
                            return;
                        }
                    }
                }
            }
            if (this.f4705h == 5) {
                this.f4700c.f650b.mo1281H();
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public float mo10334f() {
        return this.f4704g;
    }

    @DexIgnore
    /* renamed from: g */
    public void mo10335g() {
        androidx.constraintlayout.solver.widgets.ConstraintAnchor g = this.f4700c.mo1269g();
        if (g != null) {
            if (g.mo1269g() == this.f4700c) {
                this.f4705h = 4;
                g.mo1266d().f4705h = 4;
            }
            int b = this.f4700c.mo1264b();
            androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type = this.f4700c.f651c;
            if (type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT || type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM) {
                b = -b;
            }
            mo10326a(g.mo1266d(), b);
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        if (this.f5384b != 1) {
            return "{ " + this.f4700c + " UNRESOLVED} type: " + mo10323a(this.f4705h);
        } else if (this.f4703f == this) {
            return "[" + this.f4700c + ", RESOLVED: " + this.f4704g + "]  type: " + mo10323a(this.f4705h);
        } else {
            return "[" + this.f4700c + ", RESOLVED: " + this.f4703f + ":" + this.f4704g + "] type: " + mo10323a(this.f4705h);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10330b(com.fossil.blesdk.obfuscated.C1703e5 e5Var, float f) {
        this.f4706i = e5Var;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10331b(com.fossil.blesdk.obfuscated.C1703e5 e5Var, int i, com.fossil.blesdk.obfuscated.C1770f5 f5Var) {
        this.f4706i = e5Var;
        this.f4709l = f5Var;
        this.f4710m = i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10324a(int i, com.fossil.blesdk.obfuscated.C1703e5 e5Var, int i2) {
        this.f4705h = i;
        this.f4701d = e5Var;
        this.f4702e = (float) i2;
        this.f4701d.mo11138a(this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10326a(com.fossil.blesdk.obfuscated.C1703e5 e5Var, int i) {
        this.f4701d = e5Var;
        this.f4702e = (float) i;
        this.f4701d.mo11138a(this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10327a(com.fossil.blesdk.obfuscated.C1703e5 e5Var, int i, com.fossil.blesdk.obfuscated.C1770f5 f5Var) {
        this.f4701d = e5Var;
        this.f4701d.mo11138a(this);
        this.f4707j = f5Var;
        this.f4708k = i;
        this.f4707j.mo11138a(this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10328a(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        androidx.constraintlayout.solver.SolverVariable e = this.f4700c.mo1267e();
        com.fossil.blesdk.obfuscated.C1703e5 e5Var = this.f4703f;
        if (e5Var == null) {
            q4Var.mo15013a(e, (int) (this.f4704g + 0.5f));
        } else {
            q4Var.mo15011a(e, q4Var.mo15010a((java.lang.Object) e5Var.f4700c), (int) (this.f4704g + 0.5f), 6);
        }
    }
}
