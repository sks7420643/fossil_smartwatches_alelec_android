package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iy0 extends te0<Status, my0> {
    @DexIgnore
    public /* final */ rd0 s;

    @DexIgnore
    public iy0(rd0 rd0, ge0 ge0) {
        super(md0.o, ge0);
        this.s = rd0;
    }

    @DexIgnore
    public final /* synthetic */ me0 a(Status status) {
        return status;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        my0 my0 = (my0) bVar;
        ly0 ly0 = new ly0(this);
        try {
            rd0 rd0 = this.s;
            if (rd0.n != null && rd0.m.o.length == 0) {
                rd0.m.o = rd0.n.zza();
            }
            if (rd0.o != null && rd0.m.v.length == 0) {
                rd0.m.v = rd0.o.zza();
            }
            jy0 jy0 = rd0.m;
            byte[] bArr = new byte[jy0.a()];
            vx0.a(jy0, bArr, 0, bArr.length);
            rd0.f = bArr;
            ((qy0) my0.x()).a(ly0, this.s);
        } catch (RuntimeException e) {
            Log.e("ClearcutLoggerApiImpl", "derived ClearcutLogger.MessageProducer ", e);
            c(new Status(10, "MessageProducer"));
        }
    }
}
