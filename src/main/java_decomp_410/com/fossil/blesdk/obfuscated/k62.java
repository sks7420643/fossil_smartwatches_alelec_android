package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface k62 {
    @DexIgnore
    <R extends i62.c, E extends i62.a> void a(E e, i62.d<R, E> dVar);

    @DexIgnore
    <R extends i62.c, E extends i62.a> void a(R r, i62.d<R, E> dVar);

    @DexIgnore
    void execute(Runnable runnable);
}
