package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q4 */
public class C2703q4 {

    @DexIgnore
    /* renamed from: p */
    public static int f8530p; // = 1000;

    @DexIgnore
    /* renamed from: q */
    public static com.fossil.blesdk.obfuscated.C2783r4 f8531q;

    @DexIgnore
    /* renamed from: a */
    public int f8532a; // = 0;

    @DexIgnore
    /* renamed from: b */
    public java.util.HashMap<java.lang.String, androidx.constraintlayout.solver.SolverVariable> f8533b; // = null;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2703q4.C2704a f8534c;

    @DexIgnore
    /* renamed from: d */
    public int f8535d; // = 32;

    @DexIgnore
    /* renamed from: e */
    public int f8536e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2459n4[] f8537f;

    @DexIgnore
    /* renamed from: g */
    public boolean f8538g;

    @DexIgnore
    /* renamed from: h */
    public boolean[] f8539h;

    @DexIgnore
    /* renamed from: i */
    public int f8540i;

    @DexIgnore
    /* renamed from: j */
    public int f8541j;

    @DexIgnore
    /* renamed from: k */
    public int f8542k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.fossil.blesdk.obfuscated.C2526o4 f8543l;

    @DexIgnore
    /* renamed from: m */
    public androidx.constraintlayout.solver.SolverVariable[] f8544m;

    @DexIgnore
    /* renamed from: n */
    public int f8545n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ com.fossil.blesdk.obfuscated.C2703q4.C2704a f8546o;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.q4$a */
    public interface C2704a {
        @DexIgnore
        /* renamed from: a */
        androidx.constraintlayout.solver.SolverVariable mo13830a(com.fossil.blesdk.obfuscated.C2703q4 q4Var, boolean[] zArr);

        @DexIgnore
        /* renamed from: a */
        void mo13840a(androidx.constraintlayout.solver.SolverVariable solverVariable);

        @DexIgnore
        /* renamed from: a */
        void mo13841a(com.fossil.blesdk.obfuscated.C2703q4.C2704a aVar);

        @DexIgnore
        void clear();

        @DexIgnore
        androidx.constraintlayout.solver.SolverVariable getKey();
    }

    @DexIgnore
    public C2703q4() {
        int i = this.f8535d;
        this.f8536e = i;
        this.f8537f = null;
        this.f8538g = false;
        this.f8539h = new boolean[i];
        this.f8540i = 1;
        this.f8541j = 0;
        this.f8542k = i;
        this.f8544m = new androidx.constraintlayout.solver.SolverVariable[f8530p];
        this.f8545n = 0;
        com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr = new com.fossil.blesdk.obfuscated.C2459n4[i];
        this.f8537f = new com.fossil.blesdk.obfuscated.C2459n4[i];
        mo15034h();
        this.f8543l = new com.fossil.blesdk.obfuscated.C2526o4();
        this.f8534c = new com.fossil.blesdk.obfuscated.C2627p4(this.f8543l);
        this.f8546o = new com.fossil.blesdk.obfuscated.C2459n4(this.f8543l);
    }

    @DexIgnore
    /* renamed from: j */
    public static com.fossil.blesdk.obfuscated.C2783r4 m12575j() {
        return f8531q;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable mo15010a(java.lang.Object obj) {
        androidx.constraintlayout.solver.SolverVariable solverVariable = null;
        if (obj == null) {
            return null;
        }
        if (this.f8540i + 1 >= this.f8536e) {
            mo15032f();
        }
        if (obj instanceof androidx.constraintlayout.solver.widgets.ConstraintAnchor) {
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = (androidx.constraintlayout.solver.widgets.ConstraintAnchor) obj;
            solverVariable = constraintAnchor.mo1267e();
            if (solverVariable == null) {
                constraintAnchor.mo1260a(this.f8543l);
                solverVariable = constraintAnchor.mo1267e();
            }
            int i = solverVariable.f639b;
            if (i == -1 || i > this.f8532a || this.f8543l.f7983c[i] == null) {
                if (solverVariable.f639b != -1) {
                    solverVariable.mo1253a();
                }
                this.f8532a++;
                this.f8540i++;
                int i2 = this.f8532a;
                solverVariable.f639b = i2;
                solverVariable.f644g = androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED;
                this.f8543l.f7983c[i2] = solverVariable;
            }
        }
        return solverVariable;
    }

    @DexIgnore
    /* renamed from: b */
    public androidx.constraintlayout.solver.SolverVariable mo15021b() {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8844n++;
        }
        if (this.f8540i + 1 >= this.f8536e) {
            mo15032f();
        }
        androidx.constraintlayout.solver.SolverVariable a = mo15009a(androidx.constraintlayout.solver.SolverVariable.Type.SLACK, (java.lang.String) null);
        this.f8532a++;
        this.f8540i++;
        int i = this.f8532a;
        a.f639b = i;
        this.f8543l.f7983c[i] = a;
        return a;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2459n4 mo15026c() {
        com.fossil.blesdk.obfuscated.C2459n4 a = this.f8543l.f7981a.mo15893a();
        if (a == null) {
            a = new com.fossil.blesdk.obfuscated.C2459n4(this.f8543l);
        } else {
            a.mo13852d();
        }
        androidx.constraintlayout.solver.SolverVariable.m557b();
        return a;
    }

    @DexIgnore
    /* renamed from: d */
    public androidx.constraintlayout.solver.SolverVariable mo15029d() {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8843m++;
        }
        if (this.f8540i + 1 >= this.f8536e) {
            mo15032f();
        }
        androidx.constraintlayout.solver.SolverVariable a = mo15009a(androidx.constraintlayout.solver.SolverVariable.Type.SLACK, (java.lang.String) null);
        this.f8532a++;
        this.f8540i++;
        int i = this.f8532a;
        a.f639b = i;
        this.f8543l.f7983c[i] = a;
        return a;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2526o4 mo15031e() {
        return this.f8543l;
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo15032f() {
        this.f8535d *= 2;
        this.f8537f = (com.fossil.blesdk.obfuscated.C2459n4[]) java.util.Arrays.copyOf(this.f8537f, this.f8535d);
        com.fossil.blesdk.obfuscated.C2526o4 o4Var = this.f8543l;
        o4Var.f7983c = (androidx.constraintlayout.solver.SolverVariable[]) java.util.Arrays.copyOf(o4Var.f7983c, this.f8535d);
        int i = this.f8535d;
        this.f8539h = new boolean[i];
        this.f8536e = i;
        this.f8542k = i;
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8834d++;
            r4Var.f8845o = java.lang.Math.max(r4Var.f8845o, (long) i);
            com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
            r4Var2.f8830A = r4Var2.f8845o;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo15033g() throws java.lang.Exception {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8835e++;
        }
        if (this.f8538g) {
            com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
            if (r4Var2 != null) {
                r4Var2.f8847q++;
            }
            boolean z = false;
            int i = 0;
            while (true) {
                if (i >= this.f8541j) {
                    z = true;
                    break;
                } else if (!this.f8537f[i].f7659e) {
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                mo15025b(this.f8534c);
                return;
            }
            com.fossil.blesdk.obfuscated.C2783r4 r4Var3 = f8531q;
            if (r4Var3 != null) {
                r4Var3.f8846p++;
            }
            mo15012a();
            return;
        }
        mo15025b(this.f8534c);
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo15034h() {
        int i = 0;
        while (true) {
            com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr = this.f8537f;
            if (i < n4VarArr.length) {
                com.fossil.blesdk.obfuscated.C2459n4 n4Var = n4VarArr[i];
                if (n4Var != null) {
                    this.f8543l.f7981a.mo15895a(n4Var);
                }
                this.f8537f[i] = null;
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo15035i() {
        com.fossil.blesdk.obfuscated.C2526o4 o4Var;
        int i = 0;
        while (true) {
            o4Var = this.f8543l;
            androidx.constraintlayout.solver.SolverVariable[] solverVariableArr = o4Var.f7983c;
            if (i >= solverVariableArr.length) {
                break;
            }
            androidx.constraintlayout.solver.SolverVariable solverVariable = solverVariableArr[i];
            if (solverVariable != null) {
                solverVariable.mo1253a();
            }
            i++;
        }
        o4Var.f7982b.mo15894a(this.f8544m, this.f8545n);
        this.f8545n = 0;
        java.util.Arrays.fill(this.f8543l.f7983c, (java.lang.Object) null);
        java.util.HashMap<java.lang.String, androidx.constraintlayout.solver.SolverVariable> hashMap = this.f8533b;
        if (hashMap != null) {
            hashMap.clear();
        }
        this.f8532a = 0;
        this.f8534c.clear();
        this.f8540i = 1;
        for (int i2 = 0; i2 < this.f8541j; i2++) {
            this.f8537f[i2].f7657c = false;
        }
        mo15034h();
        this.f8541j = 0;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo15028c(com.fossil.blesdk.obfuscated.C2459n4 n4Var) {
        com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr = this.f8537f;
        int i = this.f8541j;
        if (n4VarArr[i] != null) {
            this.f8543l.f7981a.mo15895a(n4VarArr[i]);
        }
        com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr2 = this.f8537f;
        int i2 = this.f8541j;
        n4VarArr2[i2] = n4Var;
        androidx.constraintlayout.solver.SolverVariable solverVariable = n4Var.f7655a;
        solverVariable.f640c = i2;
        this.f8541j = i2 + 1;
        solverVariable.mo1257c(n4Var);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo15024b(com.fossil.blesdk.obfuscated.C2459n4 n4Var) {
        n4Var.mo13838a(this, 0);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo15030d(com.fossil.blesdk.obfuscated.C2459n4 n4Var) {
        if (this.f8541j > 0) {
            n4Var.f7658d.mo13512a(n4Var, this.f8537f);
            if (n4Var.f7658d.f7388a == 0) {
                n4Var.f7659e = true;
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15020b(java.lang.Object obj) {
        androidx.constraintlayout.solver.SolverVariable e = ((androidx.constraintlayout.solver.widgets.ConstraintAnchor) obj).mo1267e();
        if (e != null) {
            return (int) (e.f642e + 0.5f);
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15027c(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i, int i2) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        androidx.constraintlayout.solver.SolverVariable d = mo15029d();
        d.f641d = 0;
        c.mo13844b(solverVariable, solverVariable2, d, i);
        if (i2 != 6) {
            mo15019a(c, (int) (c.f7658d.mo13516b(d) * -1.0f), i2);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15025b(com.fossil.blesdk.obfuscated.C2703q4.C2704a aVar) throws java.lang.Exception {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8849s++;
            r4Var.f8850t = java.lang.Math.max(r4Var.f8850t, (long) this.f8540i);
            com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
            r4Var2.f8851u = java.lang.Math.max(r4Var2.f8851u, (long) this.f8541j);
        }
        mo15030d((com.fossil.blesdk.obfuscated.C2459n4) aVar);
        mo15006a(aVar);
        mo15007a(aVar, false);
        mo15012a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15019a(com.fossil.blesdk.obfuscated.C2459n4 n4Var, int i, int i2) {
        n4Var.mo13832a(mo15008a(i2, (java.lang.String) null), i);
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable mo15008a(int i, java.lang.String str) {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8842l++;
        }
        if (this.f8540i + 1 >= this.f8536e) {
            mo15032f();
        }
        androidx.constraintlayout.solver.SolverVariable a = mo15009a(androidx.constraintlayout.solver.SolverVariable.Type.ERROR, str);
        this.f8532a++;
        this.f8540i++;
        int i2 = this.f8532a;
        a.f639b = i2;
        a.f641d = i;
        this.f8543l.f7983c[i2] = a;
        this.f8534c.mo13840a(a);
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15022b(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i, int i2) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        androidx.constraintlayout.solver.SolverVariable d = mo15029d();
        d.f641d = 0;
        c.mo13836a(solverVariable, solverVariable2, d, i);
        if (i2 != 6) {
            mo15019a(c, (int) (c.f7658d.mo13516b(d) * -1.0f), i2);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.constraintlayout.solver.SolverVariable mo15009a(androidx.constraintlayout.solver.SolverVariable.Type type, java.lang.String str) {
        androidx.constraintlayout.solver.SolverVariable a = this.f8543l.f7982b.mo15893a();
        if (a == null) {
            a = new androidx.constraintlayout.solver.SolverVariable(type, str);
            a.mo1254a(type, str);
        } else {
            a.mo1253a();
            a.mo1254a(type, str);
        }
        int i = this.f8545n;
        int i2 = f8530p;
        if (i >= i2) {
            f8530p = i2 * 2;
            this.f8544m = (androidx.constraintlayout.solver.SolverVariable[]) java.util.Arrays.copyOf(this.f8544m, f8530p);
        }
        androidx.constraintlayout.solver.SolverVariable[] solverVariableArr = this.f8544m;
        int i3 = this.f8545n;
        this.f8545n = i3 + 1;
        solverVariableArr[i3] = a;
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15023b(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, boolean z) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        androidx.constraintlayout.solver.SolverVariable d = mo15029d();
        d.f641d = 0;
        c.mo13844b(solverVariable, solverVariable2, d, 0);
        if (z) {
            mo15019a(c, (int) (c.f7658d.mo13516b(d) * -1.0f), 1);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15018a(com.fossil.blesdk.obfuscated.C2459n4 n4Var) {
        if (n4Var != null) {
            com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
            if (r4Var != null) {
                r4Var.f8836f++;
                if (n4Var.f7659e) {
                    r4Var.f8837g++;
                }
            }
            if (this.f8541j + 1 >= this.f8542k || this.f8540i + 1 >= this.f8536e) {
                mo15032f();
            }
            boolean z = false;
            if (!n4Var.f7659e) {
                mo15030d(n4Var);
                if (!n4Var.mo13850c()) {
                    n4Var.mo13839a();
                    if (n4Var.mo13842a(this)) {
                        androidx.constraintlayout.solver.SolverVariable b = mo15021b();
                        n4Var.f7655a = b;
                        mo15028c(n4Var);
                        this.f8546o.mo13841a((com.fossil.blesdk.obfuscated.C2703q4.C2704a) n4Var);
                        mo15007a(this.f8546o, true);
                        if (b.f640c == -1) {
                            if (n4Var.f7655a == b) {
                                androidx.constraintlayout.solver.SolverVariable c = n4Var.mo13848c(b);
                                if (c != null) {
                                    com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
                                    if (r4Var2 != null) {
                                        r4Var2.f8840j++;
                                    }
                                    n4Var.mo13853d(c);
                                }
                            }
                            if (!n4Var.f7659e) {
                                n4Var.f7655a.mo1257c(n4Var);
                            }
                            this.f8541j--;
                        }
                        z = true;
                    }
                    if (!n4Var.mo13846b()) {
                        return;
                    }
                } else {
                    return;
                }
            }
            if (!z) {
                mo15028c(n4Var);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo15007a(com.fossil.blesdk.obfuscated.C2703q4.C2704a aVar, boolean z) {
        com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
        if (r4Var != null) {
            r4Var.f8838h++;
        }
        for (int i = 0; i < this.f8540i; i++) {
            this.f8539h[i] = false;
        }
        boolean z2 = false;
        int i2 = 0;
        while (!z2) {
            com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
            if (r4Var2 != null) {
                r4Var2.f8839i++;
            }
            i2++;
            if (i2 >= this.f8540i * 2) {
                return i2;
            }
            if (aVar.getKey() != null) {
                this.f8539h[aVar.getKey().f639b] = true;
            }
            androidx.constraintlayout.solver.SolverVariable a = aVar.mo13830a(this, this.f8539h);
            if (a != null) {
                boolean[] zArr = this.f8539h;
                int i3 = a.f639b;
                if (zArr[i3]) {
                    return i2;
                }
                zArr[i3] = true;
            }
            if (a != null) {
                int i4 = -1;
                float f = Float.MAX_VALUE;
                for (int i5 = 0; i5 < this.f8541j; i5++) {
                    com.fossil.blesdk.obfuscated.C2459n4 n4Var = this.f8537f[i5];
                    if (n4Var.f7655a.f644g != androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED && !n4Var.f7659e && n4Var.mo13847b(a)) {
                        float b = n4Var.f7658d.mo13516b(a);
                        if (b < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            float f2 = (-n4Var.f7656b) / b;
                            if (f2 < f) {
                                i4 = i5;
                                f = f2;
                            }
                        }
                    }
                }
                if (i4 > -1) {
                    com.fossil.blesdk.obfuscated.C2459n4 n4Var2 = this.f8537f[i4];
                    n4Var2.f7655a.f640c = -1;
                    com.fossil.blesdk.obfuscated.C2783r4 r4Var3 = f8531q;
                    if (r4Var3 != null) {
                        r4Var3.f8840j++;
                    }
                    n4Var2.mo13853d(a);
                    androidx.constraintlayout.solver.SolverVariable solverVariable = n4Var2.f7655a;
                    solverVariable.f640c = i4;
                    solverVariable.mo1257c(n4Var2);
                }
            }
            z2 = true;
        }
        return i2;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo15006a(com.fossil.blesdk.obfuscated.C2703q4.C2704a aVar) throws java.lang.Exception {
        float f;
        boolean z;
        int i = 0;
        while (true) {
            int i2 = this.f8541j;
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (i >= i2) {
                z = false;
                break;
            }
            com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr = this.f8537f;
            if (n4VarArr[i].f7655a.f644g != androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED && n4VarArr[i].f7656b < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
                break;
            }
            i++;
        }
        if (!z) {
            return 0;
        }
        boolean z2 = false;
        int i3 = 0;
        while (!z2) {
            com.fossil.blesdk.obfuscated.C2783r4 r4Var = f8531q;
            if (r4Var != null) {
                r4Var.f8841k++;
            }
            i3++;
            int i4 = 0;
            int i5 = -1;
            int i6 = -1;
            float f2 = Float.MAX_VALUE;
            int i7 = 0;
            while (i4 < this.f8541j) {
                com.fossil.blesdk.obfuscated.C2459n4 n4Var = this.f8537f[i4];
                if (n4Var.f7655a.f644g != androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED && !n4Var.f7659e && n4Var.f7656b < f) {
                    int i8 = 1;
                    while (i8 < this.f8540i) {
                        androidx.constraintlayout.solver.SolverVariable solverVariable = this.f8543l.f7983c[i8];
                        float b = n4Var.f7658d.mo13516b(solverVariable);
                        if (b > f) {
                            int i9 = i7;
                            float f3 = f2;
                            int i10 = i6;
                            int i11 = i5;
                            for (int i12 = 0; i12 < 7; i12++) {
                                float f4 = solverVariable.f643f[i12] / b;
                                if ((f4 < f3 && i12 == i9) || i12 > i9) {
                                    i10 = i8;
                                    i11 = i4;
                                    f3 = f4;
                                    i9 = i12;
                                }
                            }
                            i5 = i11;
                            i6 = i10;
                            f2 = f3;
                            i7 = i9;
                        }
                        i8++;
                        f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                }
                i4++;
                f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            if (i5 != -1) {
                com.fossil.blesdk.obfuscated.C2459n4 n4Var2 = this.f8537f[i5];
                n4Var2.f7655a.f640c = -1;
                com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = f8531q;
                if (r4Var2 != null) {
                    r4Var2.f8840j++;
                }
                n4Var2.mo13853d(this.f8543l.f7983c[i6]);
                androidx.constraintlayout.solver.SolverVariable solverVariable2 = n4Var2.f7655a;
                solverVariable2.f640c = i5;
                solverVariable2.mo1257c(n4Var2);
            } else {
                z2 = true;
            }
            if (i3 > this.f8540i / 2) {
                z2 = true;
            }
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return i3;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15012a() {
        for (int i = 0; i < this.f8541j; i++) {
            com.fossil.blesdk.obfuscated.C2459n4 n4Var = this.f8537f[i];
            n4Var.f7655a.f642e = n4Var.f7656b;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15016a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, boolean z) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        androidx.constraintlayout.solver.SolverVariable d = mo15029d();
        d.f641d = 0;
        c.mo13836a(solverVariable, solverVariable2, d, 0);
        if (z) {
            mo15019a(c, (int) (c.f7658d.mo13516b(d) * -1.0f), 1);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15014a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i, float f, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4, int i2, int i3) {
        int i4 = i3;
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        c.mo13834a(solverVariable, solverVariable2, i, f, solverVariable3, solverVariable4, i2);
        if (i4 != 6) {
            c.mo13838a(this, i4);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15015a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4, float f, int i) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        c.mo13837a(solverVariable, solverVariable2, solverVariable3, solverVariable4, f);
        if (i != 6) {
            c.mo13838a(this, i);
        }
        mo15018a(c);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo15011a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i, int i2) {
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        c.mo13833a(solverVariable, solverVariable2, i);
        if (i2 != 6) {
            c.mo13838a(this, i2);
        }
        mo15018a(c);
        return c;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15013a(androidx.constraintlayout.solver.SolverVariable solverVariable, int i) {
        int i2 = solverVariable.f640c;
        if (i2 != -1) {
            com.fossil.blesdk.obfuscated.C2459n4 n4Var = this.f8537f[i2];
            if (n4Var.f7659e) {
                n4Var.f7656b = (float) i;
            } else if (n4Var.f7658d.f7388a == 0) {
                n4Var.f7659e = true;
                n4Var.f7656b = (float) i;
            } else {
                com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
                c.mo13849c(solverVariable, i);
                mo15018a(c);
            }
        } else {
            com.fossil.blesdk.obfuscated.C2459n4 c2 = mo15026c();
            c2.mo13843b(solverVariable, i);
            mo15018a(c2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2459n4 m12574a(com.fossil.blesdk.obfuscated.C2703q4 q4Var, androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, float f, boolean z) {
        com.fossil.blesdk.obfuscated.C2459n4 c = q4Var.mo15026c();
        if (z) {
            q4Var.mo15024b(c);
        }
        c.mo13835a(solverVariable, solverVariable2, solverVariable3, f);
        return c;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15017a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2, float f, int i) {
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = constraintWidget;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4 = constraintWidget2;
        androidx.constraintlayout.solver.SolverVariable a = mo15010a((java.lang.Object) constraintWidget3.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT));
        androidx.constraintlayout.solver.SolverVariable a2 = mo15010a((java.lang.Object) constraintWidget3.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP));
        androidx.constraintlayout.solver.SolverVariable a3 = mo15010a((java.lang.Object) constraintWidget3.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT));
        androidx.constraintlayout.solver.SolverVariable a4 = mo15010a((java.lang.Object) constraintWidget3.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM));
        androidx.constraintlayout.solver.SolverVariable a5 = mo15010a((java.lang.Object) constraintWidget4.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT));
        androidx.constraintlayout.solver.SolverVariable a6 = mo15010a((java.lang.Object) constraintWidget4.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP));
        androidx.constraintlayout.solver.SolverVariable a7 = mo15010a((java.lang.Object) constraintWidget4.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT));
        androidx.constraintlayout.solver.SolverVariable a8 = mo15010a((java.lang.Object) constraintWidget4.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM));
        com.fossil.blesdk.obfuscated.C2459n4 c = mo15026c();
        double d = (double) f;
        androidx.constraintlayout.solver.SolverVariable solverVariable = a3;
        double d2 = (double) i;
        c.mo13845b(a2, a4, a6, a8, (float) (java.lang.Math.sin(d) * d2));
        mo15018a(c);
        com.fossil.blesdk.obfuscated.C2459n4 c2 = mo15026c();
        c2.mo13845b(a, solverVariable, a5, a7, (float) (java.lang.Math.cos(d) * d2));
        mo15018a(c2);
    }
}
