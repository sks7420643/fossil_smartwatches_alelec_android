package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vq */
public final class C3130vq {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C3130vq.C3131a> f10358a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3130vq.C3132b f10359b; // = new com.fossil.blesdk.obfuscated.C3130vq.C3132b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vq$a */
    public static class C3131a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.concurrent.locks.Lock f10360a; // = new java.util.concurrent.locks.ReentrantLock();

        @DexIgnore
        /* renamed from: b */
        public int f10361b;
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vq$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vq$b */
    public static class C3132b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Queue<com.fossil.blesdk.obfuscated.C3130vq.C3131a> f10362a; // = new java.util.ArrayDeque();

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3130vq.C3131a mo17160a() {
            com.fossil.blesdk.obfuscated.C3130vq.C3131a poll;
            synchronized (this.f10362a) {
                poll = this.f10362a.poll();
            }
            return poll == null ? new com.fossil.blesdk.obfuscated.C3130vq.C3131a() : poll;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17161a(com.fossil.blesdk.obfuscated.C3130vq.C3131a aVar) {
            synchronized (this.f10362a) {
                if (this.f10362a.size() < 10) {
                    this.f10362a.offer(aVar);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17158a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3130vq.C3131a aVar;
        synchronized (this) {
            aVar = this.f10358a.get(str);
            if (aVar == null) {
                aVar = this.f10359b.mo17160a();
                this.f10358a.put(str, aVar);
            }
            aVar.f10361b++;
        }
        aVar.f10360a.lock();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17159b(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3130vq.C3131a aVar;
        synchronized (this) {
            com.fossil.blesdk.obfuscated.C3130vq.C3131a aVar2 = this.f10358a.get(str);
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(aVar2);
            aVar = aVar2;
            if (aVar.f10361b >= 1) {
                aVar.f10361b--;
                if (aVar.f10361b == 0) {
                    com.fossil.blesdk.obfuscated.C3130vq.C3131a remove = this.f10358a.remove(str);
                    if (remove.equals(aVar)) {
                        this.f10359b.mo17161a(remove);
                    } else {
                        throw new java.lang.IllegalStateException("Removed the wrong lock, expected to remove: " + aVar + ", but actually removed: " + remove + ", safeKey: " + str);
                    }
                }
            } else {
                throw new java.lang.IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + aVar.f10361b);
            }
        }
        aVar.f10360a.unlock();
    }
}
