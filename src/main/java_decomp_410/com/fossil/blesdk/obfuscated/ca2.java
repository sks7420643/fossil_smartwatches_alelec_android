package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ca2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerView A;
    @DexIgnore
    public /* final */ SwitchCompat B;
    @DexIgnore
    public /* final */ AppCompatAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ sh2 u;
    @DexIgnore
    public /* final */ sh2 v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ca2(Object obj, View view, int i, AppCompatAutoCompleteTextView appCompatAutoCompleteTextView, View view2, ConstraintLayout constraintLayout, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, sh2 sh2, sh2 sh22, ImageView imageView2, ImageView imageView3, View view3, LinearLayout linearLayout, LinearLayout linearLayout2, RecyclerView recyclerView, SwitchCompat switchCompat) {
        super(obj, view, i);
        this.q = appCompatAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = rTLImageView;
        this.u = sh2;
        a((ViewDataBinding) this.u);
        this.v = sh22;
        a((ViewDataBinding) this.v);
        this.w = imageView2;
        this.x = imageView3;
        this.y = view3;
        this.z = linearLayout2;
        this.A = recyclerView;
        this.B = switchCompat;
    }
}
