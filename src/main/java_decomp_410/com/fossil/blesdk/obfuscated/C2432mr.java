package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mr */
public interface C2432mr {

    @DexIgnore
    /* renamed from: a */
    public static final com.fossil.blesdk.obfuscated.C2432mr f7568a = new com.fossil.blesdk.obfuscated.C2587or.C2588a().mo14495a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mr$a */
    public class C2433a implements com.fossil.blesdk.obfuscated.C2432mr {
        @DexIgnore
        /* renamed from: a */
        public java.util.Map<java.lang.String, java.lang.String> mo13704a() {
            return java.util.Collections.emptyMap();
        }
    }

    /*
    static {
        new com.fossil.blesdk.obfuscated.C2432mr.C2433a();
    }
    */

    @DexIgnore
    /* renamed from: a */
    java.util.Map<java.lang.String, java.lang.String> mo13704a();
}
