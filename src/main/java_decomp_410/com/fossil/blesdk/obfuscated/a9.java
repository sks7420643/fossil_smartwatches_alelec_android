package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a9 {
    @DexIgnore
    public int a;

    @DexIgnore
    public a9(ViewGroup viewGroup) {
    }

    @DexIgnore
    public void a(View view, View view2, int i) {
        a(view, view2, i, 0);
    }

    @DexIgnore
    public void a(View view, View view2, int i, int i2) {
        this.a = i;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public void a(View view) {
        a(view, 0);
    }

    @DexIgnore
    public void a(View view, int i) {
        this.a = 0;
    }
}
