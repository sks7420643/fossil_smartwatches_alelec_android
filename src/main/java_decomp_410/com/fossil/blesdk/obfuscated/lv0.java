package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv0 implements rv0 {
    @DexIgnore
    public rv0[] a;

    @DexIgnore
    public lv0(rv0... rv0Arr) {
        this.a = rv0Arr;
    }

    @DexIgnore
    public final boolean zza(Class<?> cls) {
        for (rv0 zza : this.a) {
            if (zza.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final qv0 zzb(Class<?> cls) {
        for (rv0 rv0 : this.a) {
            if (rv0.zza(cls)) {
                return rv0.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
