package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ls<DataType> implements mo<DataType, BitmapDrawable> {
    @DexIgnore
    public /* final */ mo<DataType, Bitmap> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public ls(Resources resources, mo<DataType, Bitmap> moVar) {
        tw.a(resources);
        this.b = resources;
        tw.a(moVar);
        this.a = moVar;
    }

    @DexIgnore
    public boolean a(DataType datatype, lo loVar) throws IOException {
        return this.a.a(datatype, loVar);
    }

    @DexIgnore
    public aq<BitmapDrawable> a(DataType datatype, int i, int i2, lo loVar) throws IOException {
        return dt.a(this.b, this.a.a(datatype, i, i2, loVar));
    }
}
