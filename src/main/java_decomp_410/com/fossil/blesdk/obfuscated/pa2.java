package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pa2 extends oa2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(R.id.iv_back, 1);
        w.put(R.id.tv_title, 2);
        w.put(R.id.cl_under_amour, 3);
        w.put(R.id.iv_under_amour, 4);
        w.put(R.id.tv_under_amour, 5);
        w.put(R.id.cb_under_armour, 6);
        w.put(R.id.cl_jawbone, 7);
        w.put(R.id.iv_jawbone, 8);
        w.put(R.id.tv_jawbone, 9);
        w.put(R.id.cb_jawbone, 10);
        w.put(R.id.cl_googlefit, 11);
        w.put(R.id.iv_googlefit, 12);
        w.put(R.id.tv_googlefit, 13);
        w.put(R.id.cb_googlefit, 14);
    }
    */

    @DexIgnore
    public pa2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 15, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public pa2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[14], objArr[10], objArr[6], objArr[11], objArr[7], objArr[3], objArr[1], objArr[12], objArr[8], objArr[4], objArr[13], objArr[9], objArr[2], objArr[5]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
