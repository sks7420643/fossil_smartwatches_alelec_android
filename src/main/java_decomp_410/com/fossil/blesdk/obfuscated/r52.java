package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r52 implements Factory<MFLoginWechatManager> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public r52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static r52 a(n42 n42) {
        return new r52(n42);
    }

    @DexIgnore
    public static MFLoginWechatManager b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static MFLoginWechatManager c(n42 n42) {
        MFLoginWechatManager p = n42.p();
        n44.a(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    public MFLoginWechatManager get() {
        return b(this.a);
    }
}
