package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jl */
public final class C2131jl implements com.fossil.blesdk.obfuscated.C2036il {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.room.RoomDatabase f6527a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf f6528b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6529c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6530d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6531e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6532f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6533g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6534h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f6535i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$a */
    public class C2132a extends com.fossil.blesdk.obfuscated.C2322lf<com.fossil.blesdk.obfuscated.C1954hl> {
        @DexIgnore
        public C2132a(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.fossil.blesdk.obfuscated.C1954hl hlVar) {
            java.lang.String str = hlVar.f5771a;
            if (str == null) {
                kgVar.mo11930a(1);
            } else {
                kgVar.mo11932a(1, str);
            }
            kgVar.mo11934b(2, (long) com.fossil.blesdk.obfuscated.C2494nl.m11326a(hlVar.f5772b));
            java.lang.String str2 = hlVar.f5773c;
            if (str2 == null) {
                kgVar.mo11930a(3);
            } else {
                kgVar.mo11932a(3, str2);
            }
            java.lang.String str3 = hlVar.f5774d;
            if (str3 == null) {
                kgVar.mo11930a(4);
            } else {
                kgVar.mo11932a(4, str3);
            }
            byte[] a = com.fossil.blesdk.obfuscated.C1419aj.m4448a(hlVar.f5775e);
            if (a == null) {
                kgVar.mo11930a(5);
            } else {
                kgVar.mo11933a(5, a);
            }
            byte[] a2 = com.fossil.blesdk.obfuscated.C1419aj.m4448a(hlVar.f5776f);
            if (a2 == null) {
                kgVar.mo11930a(6);
            } else {
                kgVar.mo11933a(6, a2);
            }
            kgVar.mo11934b(7, hlVar.f5777g);
            kgVar.mo11934b(8, hlVar.f5778h);
            kgVar.mo11934b(9, hlVar.f5779i);
            kgVar.mo11934b(10, (long) hlVar.f5781k);
            kgVar.mo11934b(11, (long) com.fossil.blesdk.obfuscated.C2494nl.m11324a(hlVar.f5782l));
            kgVar.mo11934b(12, hlVar.f5783m);
            kgVar.mo11934b(13, hlVar.f5784n);
            kgVar.mo11934b(14, hlVar.f5785o);
            kgVar.mo11934b(15, hlVar.f5786p);
            com.fossil.blesdk.obfuscated.C3365yi yiVar = hlVar.f5780j;
            if (yiVar != null) {
                kgVar.mo11934b(16, (long) com.fossil.blesdk.obfuscated.C2494nl.m11325a(yiVar.mo18107b()));
                kgVar.mo11934b(17, yiVar.mo18117g() ? 1 : 0);
                kgVar.mo11934b(18, yiVar.mo18118h() ? 1 : 0);
                kgVar.mo11934b(19, yiVar.mo18116f() ? 1 : 0);
                kgVar.mo11934b(20, yiVar.mo18120i() ? 1 : 0);
                kgVar.mo11934b(21, yiVar.mo18110c());
                kgVar.mo11934b(22, yiVar.mo18112d());
                byte[] a3 = com.fossil.blesdk.obfuscated.C2494nl.m11329a(yiVar.mo18102a());
                if (a3 == null) {
                    kgVar.mo11930a(23);
                } else {
                    kgVar.mo11933a(23, a3);
                }
            } else {
                kgVar.mo11930a(16);
                kgVar.mo11930a(17);
                kgVar.mo11930a(18);
                kgVar.mo11930a(19);
                kgVar.mo11930a(20);
                kgVar.mo11930a(21);
                kgVar.mo11930a(22);
                kgVar.mo11930a(23);
            }
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec`(`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$b")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$b */
    public class C2133b extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2133b(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$c")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$c */
    public class C2134c extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2134c(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$d")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$d */
    public class C2135d extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2135d(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$e")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$e */
    public class C2136e extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2136e(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$f")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$f */
    public class C2137f extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2137f(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$g")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$g */
    public class C2138g extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2138g(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$h")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$h */
    public class C2139h extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2139h(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jl$i")
    /* renamed from: com.fossil.blesdk.obfuscated.jl$i */
    public class C2140i extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C2140i(com.fossil.blesdk.obfuscated.C2131jl jlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public C2131jl(androidx.room.RoomDatabase roomDatabase) {
        this.f6527a = roomDatabase;
        this.f6528b = new com.fossil.blesdk.obfuscated.C2131jl.C2132a(this, roomDatabase);
        this.f6529c = new com.fossil.blesdk.obfuscated.C2131jl.C2133b(this, roomDatabase);
        this.f6530d = new com.fossil.blesdk.obfuscated.C2131jl.C2134c(this, roomDatabase);
        this.f6531e = new com.fossil.blesdk.obfuscated.C2131jl.C2135d(this, roomDatabase);
        this.f6532f = new com.fossil.blesdk.obfuscated.C2131jl.C2136e(this, roomDatabase);
        this.f6533g = new com.fossil.blesdk.obfuscated.C2131jl.C2137f(this, roomDatabase);
        this.f6534h = new com.fossil.blesdk.obfuscated.C2131jl.C2138g(this, roomDatabase);
        this.f6535i = new com.fossil.blesdk.obfuscated.C2131jl.C2139h(this, roomDatabase);
        new com.fossil.blesdk.obfuscated.C2131jl.C2140i(this, roomDatabase);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12020a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        this.f6527a.assertNotSuspendingTransaction();
        this.f6527a.beginTransaction();
        try {
            this.f6528b.insert(hlVar);
            this.f6527a.setTransactionSuccessful();
        } finally {
            this.f6527a.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12023b(java.lang.String str) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6529c.acquire();
        if (str == null) {
            acquire.mo11930a(1);
        } else {
            acquire.mo11932a(1, str);
        }
        this.f6527a.beginTransaction();
        try {
            acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
        } finally {
            this.f6527a.endTransaction();
            this.f6529c.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.List<java.lang.String> mo12026c(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public int mo12027d() {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6535i.acquire();
        this.f6527a.beginTransaction();
        try {
            int n = acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
            return n;
        } finally {
            this.f6527a.endTransaction();
            this.f6535i.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1954hl mo12029e(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf ufVar;
        com.fossil.blesdk.obfuscated.C1954hl hlVar;
        java.lang.String str2 = str;
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM workspec WHERE id=?", 1);
        if (str2 == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str2);
        }
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "state");
            int b4 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "worker_class_name");
            int b5 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input_merger_class_name");
            int b6 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input");
            int b7 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "output");
            int b8 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "initial_delay");
            int b9 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "interval_duration");
            int b10 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "flex_duration");
            int b11 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "run_attempt_count");
            int b12 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_policy");
            int b13 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_delay_duration");
            int b14 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "period_start_time");
            int b15 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "minimum_retention_duration");
            ufVar = b;
            try {
                int b16 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "schedule_requested_at");
                int b17 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "required_network_type");
                int i = b15;
                int b18 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_charging");
                int i2 = b14;
                int b19 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_device_idle");
                int i3 = b13;
                int b20 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_battery_not_low");
                int i4 = b12;
                int b21 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_storage_not_low");
                int i5 = b11;
                int b22 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_content_update_delay");
                int i6 = b10;
                int b23 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_max_content_delay");
                int i7 = b9;
                int b24 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "content_uri_triggers");
                if (a.moveToFirst()) {
                    java.lang.String string = a.getString(b2);
                    java.lang.String string2 = a.getString(b4);
                    int i8 = b8;
                    com.fossil.blesdk.obfuscated.C3365yi yiVar = new com.fossil.blesdk.obfuscated.C3365yi();
                    yiVar.mo18104a(com.fossil.blesdk.obfuscated.C2494nl.m11330b(a.getInt(b17)));
                    yiVar.mo18109b(a.getInt(b18) != 0);
                    yiVar.mo18111c(a.getInt(b19) != 0);
                    yiVar.mo18106a(a.getInt(b20) != 0);
                    yiVar.mo18113d(a.getInt(b21) != 0);
                    yiVar.mo18103a(a.getLong(b22));
                    yiVar.mo18108b(a.getLong(b23));
                    yiVar.mo18105a(com.fossil.blesdk.obfuscated.C2494nl.m11328a(a.getBlob(b24)));
                    hlVar = new com.fossil.blesdk.obfuscated.C1954hl(string, string2);
                    hlVar.f5772b = com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(b3));
                    hlVar.f5774d = a.getString(b5);
                    hlVar.f5775e = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(b6));
                    hlVar.f5776f = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(b7));
                    hlVar.f5777g = a.getLong(i8);
                    hlVar.f5778h = a.getLong(i7);
                    hlVar.f5779i = a.getLong(i6);
                    hlVar.f5781k = a.getInt(i5);
                    hlVar.f5782l = com.fossil.blesdk.obfuscated.C2494nl.m11327a(a.getInt(i4));
                    hlVar.f5783m = a.getLong(i3);
                    hlVar.f5784n = a.getLong(i2);
                    hlVar.f5785o = a.getLong(i);
                    hlVar.f5786p = a.getLong(b16);
                    hlVar.f5780j = yiVar;
                } else {
                    hlVar = null;
                }
                a.close();
                ufVar.mo16767c();
                return hlVar;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.mo16767c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.mo16767c();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: f */
    public int mo12030f(java.lang.String str) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6533g.acquire();
        if (str == null) {
            acquire.mo11930a(1);
        } else {
            acquire.mo11932a(1, str);
        }
        this.f6527a.beginTransaction();
        try {
            int n = acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
            return n;
        } finally {
            this.f6527a.endTransaction();
            this.f6533g.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public java.util.List<com.fossil.blesdk.obfuscated.C1419aj> mo12031g(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(0)));
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: h */
    public int mo12032h(java.lang.String str) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6532f.acquire();
        if (str == null) {
            acquire.mo11930a(1);
        } else {
            acquire.mo11932a(1, str);
        }
        this.f6527a.beginTransaction();
        try {
            int n = acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
            return n;
        } finally {
            this.f6527a.endTransaction();
            this.f6532f.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12021a(java.lang.String str, com.fossil.blesdk.obfuscated.C1419aj ajVar) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6530d.acquire();
        byte[] a = com.fossil.blesdk.obfuscated.C1419aj.m4448a(ajVar);
        if (a == null) {
            acquire.mo11930a(1);
        } else {
            acquire.mo11933a(1, a);
        }
        if (str == null) {
            acquire.mo11930a(2);
        } else {
            acquire.mo11932a(2, str);
        }
        this.f6527a.beginTransaction();
        try {
            acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
        } finally {
            this.f6527a.endTransaction();
            this.f6530d.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public androidx.work.WorkInfo.State mo12028d(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            return a.moveToFirst() ? com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(0)) : null;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12024b(java.lang.String str, long j) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6531e.acquire();
        acquire.mo11934b(1, j);
        if (str == null) {
            acquire.mo11930a(2);
        } else {
            acquire.mo11932a(2, str);
        }
        this.f6527a.beginTransaction();
        try {
            acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
        } finally {
            this.f6527a.endTransaction();
            this.f6531e.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.List<java.lang.String> mo12025c() {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo12016a(java.lang.String str, long j) {
        this.f6527a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f6534h.acquire();
        acquire.mo11934b(1, j);
        if (str == null) {
            acquire.mo11930a(2);
        } else {
            acquire.mo11932a(2, str);
        }
        this.f6527a.beginTransaction();
        try {
            int n = acquire.mo12781n();
            this.f6527a.setTransactionSuccessful();
            return n;
        } finally {
            this.f6527a.endTransaction();
            this.f6534h.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12022b() {
        com.fossil.blesdk.obfuscated.C3035uf ufVar;
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM workspec WHERE state=1", 0);
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "state");
            int b4 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "worker_class_name");
            int b5 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input_merger_class_name");
            int b6 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input");
            int b7 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "output");
            int b8 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "initial_delay");
            int b9 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "interval_duration");
            int b10 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "flex_duration");
            int b11 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "run_attempt_count");
            int b12 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_policy");
            int b13 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_delay_duration");
            int b14 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "period_start_time");
            int b15 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "minimum_retention_duration");
            ufVar = b;
            try {
                int b16 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "schedule_requested_at");
                int b17 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "required_network_type");
                int i = b15;
                int b18 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_charging");
                int i2 = b14;
                int b19 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_device_idle");
                int i3 = b13;
                int b20 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_battery_not_low");
                int i4 = b12;
                int b21 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_storage_not_low");
                int i5 = b11;
                int b22 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_content_update_delay");
                int i6 = b10;
                int b23 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_max_content_delay");
                int i7 = b9;
                int b24 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "content_uri_triggers");
                int i8 = b8;
                int i9 = b7;
                java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
                while (a.moveToNext()) {
                    java.lang.String string = a.getString(b2);
                    int i10 = b2;
                    java.lang.String string2 = a.getString(b4);
                    int i11 = b4;
                    com.fossil.blesdk.obfuscated.C3365yi yiVar = new com.fossil.blesdk.obfuscated.C3365yi();
                    int i12 = b17;
                    yiVar.mo18104a(com.fossil.blesdk.obfuscated.C2494nl.m11330b(a.getInt(b17)));
                    yiVar.mo18109b(a.getInt(b18) != 0);
                    yiVar.mo18111c(a.getInt(b19) != 0);
                    yiVar.mo18106a(a.getInt(b20) != 0);
                    yiVar.mo18113d(a.getInt(b21) != 0);
                    int i13 = b19;
                    int i14 = b18;
                    yiVar.mo18103a(a.getLong(b22));
                    yiVar.mo18108b(a.getLong(b23));
                    yiVar.mo18105a(com.fossil.blesdk.obfuscated.C2494nl.m11328a(a.getBlob(b24)));
                    com.fossil.blesdk.obfuscated.C1954hl hlVar = new com.fossil.blesdk.obfuscated.C1954hl(string, string2);
                    hlVar.f5772b = com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(b3));
                    hlVar.f5774d = a.getString(b5);
                    hlVar.f5775e = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(b6));
                    int i15 = i9;
                    hlVar.f5776f = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(i15));
                    i9 = i15;
                    int i16 = i14;
                    int i17 = i8;
                    hlVar.f5777g = a.getLong(i17);
                    i8 = i17;
                    int i18 = i7;
                    hlVar.f5778h = a.getLong(i18);
                    i7 = i18;
                    int i19 = i6;
                    hlVar.f5779i = a.getLong(i19);
                    int i20 = i5;
                    hlVar.f5781k = a.getInt(i20);
                    int i21 = i4;
                    i5 = i20;
                    hlVar.f5782l = com.fossil.blesdk.obfuscated.C2494nl.m11327a(a.getInt(i21));
                    i6 = i19;
                    int i22 = i3;
                    int i23 = b3;
                    hlVar.f5783m = a.getLong(i22);
                    int i24 = i22;
                    i4 = i21;
                    int i25 = i2;
                    hlVar.f5784n = a.getLong(i25);
                    i2 = i25;
                    int i26 = i;
                    hlVar.f5785o = a.getLong(i26);
                    i = i26;
                    int i27 = i24;
                    int i28 = b16;
                    hlVar.f5786p = a.getLong(i28);
                    hlVar.f5780j = yiVar;
                    arrayList.add(hlVar);
                    b16 = i28;
                    b18 = i16;
                    b2 = i10;
                    b4 = i11;
                    b19 = i13;
                    b17 = i12;
                    int i29 = i23;
                    i3 = i27;
                    b3 = i29;
                }
                a.close();
                ufVar.mo16767c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.mo16767c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.mo16767c();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C1954hl.C1956b> mo12019a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "state");
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                com.fossil.blesdk.obfuscated.C1954hl.C1956b bVar = new com.fossil.blesdk.obfuscated.C1954hl.C1956b();
                bVar.f5787a = a.getString(b2);
                bVar.f5788b = com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(b3));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12018a(int i) {
        com.fossil.blesdk.obfuscated.C3035uf ufVar;
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b.mo11934b(1, (long) i);
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "state");
            int b4 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "worker_class_name");
            int b5 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input_merger_class_name");
            int b6 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input");
            int b7 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "output");
            int b8 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "initial_delay");
            int b9 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "interval_duration");
            int b10 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "flex_duration");
            int b11 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "run_attempt_count");
            int b12 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_policy");
            int b13 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_delay_duration");
            int b14 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "period_start_time");
            int b15 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "minimum_retention_duration");
            ufVar = b;
            try {
                int b16 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "schedule_requested_at");
                int b17 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "required_network_type");
                int i2 = b15;
                int b18 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_charging");
                int i3 = b14;
                int b19 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_device_idle");
                int i4 = b13;
                int b20 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_battery_not_low");
                int i5 = b12;
                int b21 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_storage_not_low");
                int i6 = b11;
                int b22 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_content_update_delay");
                int i7 = b10;
                int b23 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_max_content_delay");
                int i8 = b9;
                int b24 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "content_uri_triggers");
                int i9 = b8;
                int i10 = b7;
                java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
                while (a.moveToNext()) {
                    java.lang.String string = a.getString(b2);
                    int i11 = b2;
                    java.lang.String string2 = a.getString(b4);
                    int i12 = b4;
                    com.fossil.blesdk.obfuscated.C3365yi yiVar = new com.fossil.blesdk.obfuscated.C3365yi();
                    int i13 = b17;
                    yiVar.mo18104a(com.fossil.blesdk.obfuscated.C2494nl.m11330b(a.getInt(b17)));
                    yiVar.mo18109b(a.getInt(b18) != 0);
                    yiVar.mo18111c(a.getInt(b19) != 0);
                    yiVar.mo18106a(a.getInt(b20) != 0);
                    yiVar.mo18113d(a.getInt(b21) != 0);
                    int i14 = b20;
                    int i15 = b18;
                    yiVar.mo18103a(a.getLong(b22));
                    yiVar.mo18108b(a.getLong(b23));
                    yiVar.mo18105a(com.fossil.blesdk.obfuscated.C2494nl.m11328a(a.getBlob(b24)));
                    com.fossil.blesdk.obfuscated.C1954hl hlVar = new com.fossil.blesdk.obfuscated.C1954hl(string, string2);
                    hlVar.f5772b = com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(b3));
                    hlVar.f5774d = a.getString(b5);
                    hlVar.f5775e = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(b6));
                    int i16 = i10;
                    hlVar.f5776f = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    hlVar.f5777g = a.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    hlVar.f5778h = a.getLong(i19);
                    i8 = i19;
                    int i20 = b19;
                    int i21 = i7;
                    hlVar.f5779i = a.getLong(i21);
                    int i22 = i6;
                    hlVar.f5781k = a.getInt(i22);
                    int i23 = i5;
                    i6 = i22;
                    hlVar.f5782l = com.fossil.blesdk.obfuscated.C2494nl.m11327a(a.getInt(i23));
                    i7 = i21;
                    int i24 = i4;
                    int i25 = i20;
                    hlVar.f5783m = a.getLong(i24);
                    int i26 = i24;
                    i5 = i23;
                    int i27 = i3;
                    hlVar.f5784n = a.getLong(i27);
                    i3 = i27;
                    int i28 = i2;
                    hlVar.f5785o = a.getLong(i28);
                    i2 = i28;
                    int i29 = i26;
                    int i30 = b16;
                    hlVar.f5786p = a.getLong(i30);
                    hlVar.f5780j = yiVar;
                    arrayList.add(hlVar);
                    b16 = i30;
                    b18 = i17;
                    b19 = i25;
                    b4 = i12;
                    b20 = i14;
                    b17 = i13;
                    i4 = i29;
                    b2 = i11;
                }
                a.close();
                ufVar.mo16767c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.mo16767c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.mo16767c();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12017a() {
        com.fossil.blesdk.obfuscated.C3035uf ufVar;
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.f6527a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f6527a, b, false);
        try {
            int b2 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "id");
            int b3 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "state");
            int b4 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "worker_class_name");
            int b5 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input_merger_class_name");
            int b6 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "input");
            int b7 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "output");
            int b8 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "initial_delay");
            int b9 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "interval_duration");
            int b10 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "flex_duration");
            int b11 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "run_attempt_count");
            int b12 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_policy");
            int b13 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "backoff_delay_duration");
            int b14 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "period_start_time");
            int b15 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "minimum_retention_duration");
            ufVar = b;
            try {
                int b16 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "schedule_requested_at");
                int b17 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "required_network_type");
                int i = b15;
                int b18 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_charging");
                int i2 = b14;
                int b19 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_device_idle");
                int i3 = b13;
                int b20 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_battery_not_low");
                int i4 = b12;
                int b21 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "requires_storage_not_low");
                int i5 = b11;
                int b22 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_content_update_delay");
                int i6 = b10;
                int b23 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "trigger_max_content_delay");
                int i7 = b9;
                int b24 = com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "content_uri_triggers");
                int i8 = b8;
                int i9 = b7;
                java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
                while (a.moveToNext()) {
                    java.lang.String string = a.getString(b2);
                    int i10 = b2;
                    java.lang.String string2 = a.getString(b4);
                    int i11 = b4;
                    com.fossil.blesdk.obfuscated.C3365yi yiVar = new com.fossil.blesdk.obfuscated.C3365yi();
                    int i12 = b17;
                    yiVar.mo18104a(com.fossil.blesdk.obfuscated.C2494nl.m11330b(a.getInt(b17)));
                    yiVar.mo18109b(a.getInt(b18) != 0);
                    yiVar.mo18111c(a.getInt(b19) != 0);
                    yiVar.mo18106a(a.getInt(b20) != 0);
                    yiVar.mo18113d(a.getInt(b21) != 0);
                    int i13 = b19;
                    int i14 = b18;
                    yiVar.mo18103a(a.getLong(b22));
                    yiVar.mo18108b(a.getLong(b23));
                    yiVar.mo18105a(com.fossil.blesdk.obfuscated.C2494nl.m11328a(a.getBlob(b24)));
                    com.fossil.blesdk.obfuscated.C1954hl hlVar = new com.fossil.blesdk.obfuscated.C1954hl(string, string2);
                    hlVar.f5772b = com.fossil.blesdk.obfuscated.C2494nl.m11331c(a.getInt(b3));
                    hlVar.f5774d = a.getString(b5);
                    hlVar.f5775e = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(b6));
                    int i15 = i9;
                    hlVar.f5776f = com.fossil.blesdk.obfuscated.C1419aj.m4455b(a.getBlob(i15));
                    i9 = i15;
                    int i16 = i14;
                    int i17 = i8;
                    hlVar.f5777g = a.getLong(i17);
                    i8 = i17;
                    int i18 = i7;
                    hlVar.f5778h = a.getLong(i18);
                    i7 = i18;
                    int i19 = i6;
                    hlVar.f5779i = a.getLong(i19);
                    int i20 = i5;
                    hlVar.f5781k = a.getInt(i20);
                    int i21 = i4;
                    i5 = i20;
                    hlVar.f5782l = com.fossil.blesdk.obfuscated.C2494nl.m11327a(a.getInt(i21));
                    i6 = i19;
                    int i22 = i3;
                    int i23 = b3;
                    hlVar.f5783m = a.getLong(i22);
                    int i24 = i22;
                    i4 = i21;
                    int i25 = i2;
                    hlVar.f5784n = a.getLong(i25);
                    i2 = i25;
                    int i26 = i;
                    hlVar.f5785o = a.getLong(i26);
                    i = i26;
                    int i27 = i24;
                    int i28 = b16;
                    hlVar.f5786p = a.getLong(i28);
                    hlVar.f5780j = yiVar;
                    arrayList.add(hlVar);
                    b16 = i28;
                    b18 = i16;
                    b2 = i10;
                    b4 = i11;
                    b19 = i13;
                    b17 = i12;
                    int i29 = i23;
                    i3 = i27;
                    b3 = i29;
                }
                a.close();
                ufVar.mo16767c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                ufVar.mo16767c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b;
            a.close();
            ufVar.mo16767c();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo12015a(androidx.work.WorkInfo.State state, java.lang.String... strArr) {
        this.f6527a.assertNotSuspendingTransaction();
        java.lang.StringBuilder a = com.fossil.blesdk.obfuscated.C1632dg.m5847a();
        a.append("UPDATE workspec SET state=");
        a.append("?");
        a.append(" WHERE id IN (");
        com.fossil.blesdk.obfuscated.C1632dg.m5848a(a, strArr.length);
        a.append(")");
        com.fossil.blesdk.obfuscated.C2221kg compileStatement = this.f6527a.compileStatement(a.toString());
        compileStatement.mo11934b(1, (long) com.fossil.blesdk.obfuscated.C2494nl.m11326a(state));
        int i = 2;
        for (java.lang.String str : strArr) {
            if (str == null) {
                compileStatement.mo11930a(i);
            } else {
                compileStatement.mo11932a(i, str);
            }
            i++;
        }
        this.f6527a.beginTransaction();
        try {
            int n = compileStatement.mo12781n();
            this.f6527a.setTransactionSuccessful();
            return n;
        } finally {
            this.f6527a.endTransaction();
        }
    }
}
