package com.fossil.blesdk.obfuscated;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rz {
    @DexIgnore
    String a();

    @DexIgnore
    InputStream b();

    @DexIgnore
    String[] c();
}
