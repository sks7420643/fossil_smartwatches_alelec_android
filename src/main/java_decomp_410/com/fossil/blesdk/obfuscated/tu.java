package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public class tu extends Fragment {
    @DexIgnore
    public /* final */ ju e;
    @DexIgnore
    public /* final */ vu f;
    @DexIgnore
    public /* final */ Set<tu> g;
    @DexIgnore
    public xn h;
    @DexIgnore
    public tu i;
    @DexIgnore
    public Fragment j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vu {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Set<xn> a() {
            Set<tu> a2 = tu.this.a();
            HashSet hashSet = new HashSet(a2.size());
            for (tu next : a2) {
                if (next.d() != null) {
                    hashSet.add(next.d());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + tu.this + "}";
        }
    }

    @DexIgnore
    public tu() {
        this(new ju());
    }

    @DexIgnore
    public void a(xn xnVar) {
        this.h = xnVar;
    }

    @DexIgnore
    public ju b() {
        return this.e;
    }

    @DexIgnore
    @TargetApi(17)
    public final Fragment c() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.j;
    }

    @DexIgnore
    public xn d() {
        return this.h;
    }

    @DexIgnore
    public vu e() {
        return this.f;
    }

    @DexIgnore
    public final void f() {
        tu tuVar = this.i;
        if (tuVar != null) {
            tuVar.b(this);
            this.i = null;
        }
    }

    @DexIgnore
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            a(activity);
        } catch (IllegalStateException e2) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e2);
            }
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.e.a();
        f();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        f();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.e.b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.e.c();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{parent=" + c() + "}";
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public tu(ju juVar) {
        this.f = new a();
        this.g = new HashSet();
        this.e = juVar;
    }

    @DexIgnore
    public final void a(tu tuVar) {
        this.g.add(tuVar);
    }

    @DexIgnore
    public final void b(tu tuVar) {
        this.g.remove(tuVar);
    }

    @DexIgnore
    @TargetApi(17)
    public Set<tu> a() {
        if (equals(this.i)) {
            return Collections.unmodifiableSet(this.g);
        }
        if (this.i == null || Build.VERSION.SDK_INT < 17) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet();
        for (tu next : this.i.a()) {
            if (a(next.getParentFragment())) {
                hashSet.add(next);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public void b(Fragment fragment) {
        this.j = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            a(fragment.getActivity());
        }
    }

    @DexIgnore
    @TargetApi(17)
    public final boolean a(Fragment fragment) {
        Fragment parentFragment = getParentFragment();
        while (true) {
            Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        f();
        this.i = rn.a((Context) activity).h().b(activity);
        if (!equals(this.i)) {
            this.i.a(this);
        }
    }
}
