package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w0 */
public class C3159w0 extends android.view.ActionMode {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f10440a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ androidx.appcompat.view.ActionMode f10441b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.w0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.w0$a */
    public static class C3160a implements androidx.appcompat.view.ActionMode.Callback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.ActionMode.Callback f10442a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.content.Context f10443b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C3159w0> f10444c; // = new java.util.ArrayList<>();

        @DexIgnore
        /* renamed from: d */
        public /* final */ androidx.collection.SimpleArrayMap<android.view.Menu, android.view.Menu> f10445d; // = new androidx.collection.SimpleArrayMap<>();

        @DexIgnore
        public C3160a(android.content.Context context, android.view.ActionMode.Callback callback) {
            this.f10443b = context;
            this.f10442a = callback;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo346a(androidx.appcompat.view.ActionMode actionMode, android.view.Menu menu) {
            return this.f10442a.onCreateActionMode(mo17262b(actionMode), mo17261a(menu));
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo348b(androidx.appcompat.view.ActionMode actionMode, android.view.Menu menu) {
            return this.f10442a.onPrepareActionMode(mo17262b(actionMode), mo17261a(menu));
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo347a(androidx.appcompat.view.ActionMode actionMode, android.view.MenuItem menuItem) {
            return this.f10442a.onActionItemClicked(mo17262b(actionMode), com.fossil.blesdk.obfuscated.C2780r1.m13039a(this.f10443b, (com.fossil.blesdk.obfuscated.C2001i7) menuItem));
        }

        @DexIgnore
        /* renamed from: b */
        public android.view.ActionMode mo17262b(androidx.appcompat.view.ActionMode actionMode) {
            int size = this.f10444c.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C3159w0 w0Var = this.f10444c.get(i);
                if (w0Var != null && w0Var.f10441b == actionMode) {
                    return w0Var;
                }
            }
            com.fossil.blesdk.obfuscated.C3159w0 w0Var2 = new com.fossil.blesdk.obfuscated.C3159w0(this.f10443b, actionMode);
            this.f10444c.add(w0Var2);
            return w0Var2;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo345a(androidx.appcompat.view.ActionMode actionMode) {
            this.f10442a.onDestroyActionMode(mo17262b(actionMode));
        }

        @DexIgnore
        /* renamed from: a */
        public final android.view.Menu mo17261a(android.view.Menu menu) {
            android.view.Menu menu2 = this.f10445d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            android.view.Menu a = com.fossil.blesdk.obfuscated.C2780r1.m13038a(this.f10443b, (com.fossil.blesdk.obfuscated.C1927h7) menu);
            this.f10445d.put(menu, a);
            return a;
        }
    }

    @DexIgnore
    public C3159w0(android.content.Context context, androidx.appcompat.view.ActionMode actionMode) {
        this.f10440a = context;
        this.f10441b = actionMode;
    }

    @DexIgnore
    public void finish() {
        this.f10441b.mo328a();
    }

    @DexIgnore
    public android.view.View getCustomView() {
        return this.f10441b.mo334b();
    }

    @DexIgnore
    public android.view.Menu getMenu() {
        return com.fossil.blesdk.obfuscated.C2780r1.m13038a(this.f10440a, (com.fossil.blesdk.obfuscated.C1927h7) this.f10441b.mo337c());
    }

    @DexIgnore
    public android.view.MenuInflater getMenuInflater() {
        return this.f10441b.mo338d();
    }

    @DexIgnore
    public java.lang.CharSequence getSubtitle() {
        return this.f10441b.mo339e();
    }

    @DexIgnore
    public java.lang.Object getTag() {
        return this.f10441b.mo340f();
    }

    @DexIgnore
    public java.lang.CharSequence getTitle() {
        return this.f10441b.mo341g();
    }

    @DexIgnore
    public boolean getTitleOptionalHint() {
        return this.f10441b.mo342h();
    }

    @DexIgnore
    public void invalidate() {
        this.f10441b.mo343i();
    }

    @DexIgnore
    public boolean isTitleOptional() {
        return this.f10441b.mo344j();
    }

    @DexIgnore
    public void setCustomView(android.view.View view) {
        this.f10441b.mo330a(view);
    }

    @DexIgnore
    public void setSubtitle(java.lang.CharSequence charSequence) {
        this.f10441b.mo331a(charSequence);
    }

    @DexIgnore
    public void setTag(java.lang.Object obj) {
        this.f10441b.mo332a(obj);
    }

    @DexIgnore
    public void setTitle(java.lang.CharSequence charSequence) {
        this.f10441b.mo336b(charSequence);
    }

    @DexIgnore
    public void setTitleOptionalHint(boolean z) {
        this.f10441b.mo333a(z);
    }

    @DexIgnore
    public void setSubtitle(int i) {
        this.f10441b.mo329a(i);
    }

    @DexIgnore
    public void setTitle(int i) {
        this.f10441b.mo335b(i);
    }
}
