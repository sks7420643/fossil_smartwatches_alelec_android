package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bl0 extends mj0 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ Fragment f;
    @DexIgnore
    public /* final */ /* synthetic */ int g;

    @DexIgnore
    public bl0(Intent intent, Fragment fragment, int i) {
        this.e = intent;
        this.f = fragment;
        this.g = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.e;
        if (intent != null) {
            this.f.startActivityForResult(intent, this.g);
        }
    }
}
