package com.fossil.blesdk.obfuscated;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;
import org.slf4j.helpers.NOPLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iq4 implements cq4 {
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public volatile cq4 f;
    @DexIgnore
    public Boolean g;
    @DexIgnore
    public Method h;
    @DexIgnore
    public eq4 i;
    @DexIgnore
    public Queue<gq4> j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore
    public iq4(String str, Queue<gq4> queue, boolean z) {
        this.e = str;
        this.j = queue;
        this.k = z;
    }

    @DexIgnore
    public cq4 a() {
        if (this.f != null) {
            return this.f;
        }
        if (this.k) {
            return NOPLogger.NOP_LOGGER;
        }
        return b();
    }

    @DexIgnore
    public final cq4 b() {
        if (this.i == null) {
            this.i = new eq4(this, this.j);
        }
        return this.i;
    }

    @DexIgnore
    public String c() {
        return this.e;
    }

    @DexIgnore
    public boolean d() {
        Boolean bool = this.g;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.h = this.f.getClass().getMethod("log", new Class[]{fq4.class});
            this.g = Boolean.TRUE;
        } catch (NoSuchMethodException unused) {
            this.g = Boolean.FALSE;
        }
        return this.g.booleanValue();
    }

    @DexIgnore
    public void debug(String str) {
        a().debug(str);
    }

    @DexIgnore
    public boolean e() {
        return this.f instanceof NOPLogger;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && iq4.class == obj.getClass() && this.e.equals(((iq4) obj).e);
    }

    @DexIgnore
    public void error(String str) {
        a().error(str);
    }

    @DexIgnore
    public boolean f() {
        return this.f == null;
    }

    @DexIgnore
    public int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public void info(String str) {
        a().info(str);
    }

    @DexIgnore
    public boolean isDebugEnabled() {
        return a().isDebugEnabled();
    }

    @DexIgnore
    public boolean isErrorEnabled() {
        return a().isErrorEnabled();
    }

    @DexIgnore
    public boolean isInfoEnabled() {
        return a().isInfoEnabled();
    }

    @DexIgnore
    public boolean isTraceEnabled() {
        return a().isTraceEnabled();
    }

    @DexIgnore
    public boolean isWarnEnabled() {
        return a().isWarnEnabled();
    }

    @DexIgnore
    public void trace(String str) {
        a().trace(str);
    }

    @DexIgnore
    public void warn(String str) {
        a().warn(str);
    }

    @DexIgnore
    public void debug(String str, Throwable th) {
        a().debug(str, th);
    }

    @DexIgnore
    public void error(String str, Object... objArr) {
        a().error(str, objArr);
    }

    @DexIgnore
    public void info(String str, Object obj) {
        a().info(str, obj);
    }

    @DexIgnore
    public void trace(String str, Throwable th) {
        a().trace(str, th);
    }

    @DexIgnore
    public void warn(String str, Object obj, Object obj2) {
        a().warn(str, obj, obj2);
    }

    @DexIgnore
    public void error(String str, Throwable th) {
        a().error(str, th);
    }

    @DexIgnore
    public void info(String str, Throwable th) {
        a().info(str, th);
    }

    @DexIgnore
    public void warn(String str, Throwable th) {
        a().warn(str, th);
    }

    @DexIgnore
    public void a(cq4 cq4) {
        this.f = cq4;
    }

    @DexIgnore
    public void a(fq4 fq4) {
        if (d()) {
            try {
                this.h.invoke(this.f, new Object[]{fq4});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
    }
}
