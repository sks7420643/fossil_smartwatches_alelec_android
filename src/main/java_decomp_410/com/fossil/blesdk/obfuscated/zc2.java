package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zc2 extends yc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j T; // = new ViewDataBinding.j(40);
    @DexIgnore
    public static /* final */ SparseIntArray U; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout R;
    @DexIgnore
    public long S;

    /*
    static {
        T.a(1, new String[]{"view_tab_custom", "view_tab_custom", "view_tab_custom", "view_tab_custom", "view_tab_custom", "view_tab_custom"}, new int[]{2, 3, 4, 5, 6, 7}, new int[]{R.layout.view_tab_custom, R.layout.view_tab_custom, R.layout.view_tab_custom, R.layout.view_tab_custom, R.layout.view_tab_custom, R.layout.view_tab_custom});
        U.put(R.id.iv_title, 8);
        U.put(R.id.iv_no_watch_found, 9);
        U.put(R.id.cl_update_fw, 10);
        U.put(R.id.pb_progress, 11);
        U.put(R.id.tv_update_fw_title, 12);
        U.put(R.id.tv_update_fw_detail, 13);
        U.put(R.id.srlPullToSync, 14);
        U.put(R.id.v_border_bottom, 15);
        U.put(R.id.sync_progress, 16);
        U.put(R.id.cdl_home_fragment, 17);
        U.put(R.id.abl_home_fragment, 18);
        U.put(R.id.collapsing_toolbar, 19);
        U.put(R.id.cl_visualization, 20);
        U.put(R.id.rpb_biggest, 21);
        U.put(R.id.rpb_big, 22);
        U.put(R.id.rpb_medium, 23);
        U.put(R.id.rpb_smallest, 24);
        U.put(R.id.cl_no_device, 25);
        U.put(R.id.v_center, 26);
        U.put(R.id.ftv_description, 27);
        U.put(R.id.ftv_pair_watch, 28);
        U.put(R.id.cl_model, 29);
        U.put(R.id.tv_today, 30);
        U.put(R.id.nsv_low_battery, 31);
        U.put(R.id.ftv_low_battery, 32);
        U.put(R.id.ftv_description_low_battery, 33);
        U.put(R.id.view_tabs, 34);
        U.put(R.id.rv_tabs, 35);
        U.put(R.id.cl_fw_available, 36);
        U.put(R.id.tv_title, 37);
        U.put(R.id.bt_remind, 38);
        U.put(R.id.bt_install_now, 39);
    }
    */

    @DexIgnore
    public zc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 40, T, U));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.S = 0;
        }
        ViewDataBinding.d(this.y);
        ViewDataBinding.d(this.x);
        ViewDataBinding.d(this.z);
        ViewDataBinding.d(this.B);
        ViewDataBinding.d(this.C);
        ViewDataBinding.d(this.A);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r6.x.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0025, code lost:
        if (r6.z.e() == false) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0027, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002e, code lost:
        if (r6.B.e() == false) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0030, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0037, code lost:
        if (r6.C.e() == false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0039, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        if (r6.A.e() == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0042, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0043, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.y.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.S != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.S = 64;
        }
        this.y.f();
        this.x.f();
        this.z.f();
        this.B.f();
        this.C.f();
        this.A.f();
        g();
    }

    @DexIgnore
    public zc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 6, objArr[18], objArr[39], objArr[38], objArr[1], objArr[17], objArr[36], objArr[29], objArr[25], objArr[10], objArr[20], objArr[19], objArr[27], objArr[33], objArr[32], objArr[28], objArr[3], objArr[2], objArr[4], objArr[7], objArr[5], objArr[6], objArr[9], objArr[8], objArr[31], objArr[11], objArr[22], objArr[21], objArr[23], objArr[24], objArr[35], objArr[14], objArr[16], objArr[37], objArr[30], objArr[13], objArr[12], objArr[15], objArr[26], objArr[34]);
        this.S = -1;
        this.r.setTag((Object) null);
        this.R = objArr[0];
        this.R.setTag((Object) null);
        a(view);
        f();
    }
}
