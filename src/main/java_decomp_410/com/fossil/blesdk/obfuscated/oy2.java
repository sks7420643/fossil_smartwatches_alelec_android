package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oy2 {
    @DexIgnore
    public /* final */ ux2 a;
    @DexIgnore
    public /* final */ jy2 b;

    @DexIgnore
    public oy2(ux2 ux2, jy2 jy2) {
        kd4.b(ux2, "mInActivityNudgeTimeContractView");
        kd4.b(jy2, "mRemindTimeContractView");
        this.a = ux2;
        this.b = jy2;
    }

    @DexIgnore
    public final ux2 a() {
        return this.a;
    }

    @DexIgnore
    public final jy2 b() {
        return this.b;
    }
}
