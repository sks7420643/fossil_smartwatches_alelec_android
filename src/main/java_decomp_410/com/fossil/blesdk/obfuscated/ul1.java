package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ul1 {
    @DexIgnore
    public ul1(Context context) {
    }

    @DexIgnore
    public static boolean a() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
