package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.zendesk.belvedere.BelvedereSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a34 extends g0 {
    @DexIgnore
    public ListView e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;

        @DexIgnore
        public a(a34 a34, Fragment fragment) {
            this.a = fragment;
        }

        @DexIgnore
        public void a(c34 c34) {
            c34.a(this.a);
        }

        @DexIgnore
        public Context getContext() {
            return this.a.getContext();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements g {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public b(a34 a34, FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        public void a(c34 c34) {
            c34.a((Activity) this.a);
        }

        @DexIgnore
        public Context getContext() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g e;

        @DexIgnore
        public c(g gVar) {
            this.e = gVar;
        }

        @DexIgnore
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (view.getTag() instanceof c34) {
                this.e.a((c34) view.getTag());
                a34.this.dismiss();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[BelvedereSource.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[BelvedereSource.Camera.ordinal()] = 1;
            a[BelvedereSource.Gallery.ordinal()] = 2;
        }
        */
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ArrayAdapter<c34> {
        @DexIgnore
        public Context e;

        @DexIgnore
        public e(a34 a34, Context context, int i, List<c34> list) {
            super(context, i, list);
            this.e = context;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.e).inflate(j34.belvedere_dialog_row, viewGroup, false);
            }
            c34 c34 = (c34) getItem(i);
            f a = f.a(c34, this.e);
            ((ImageView) view.findViewById(i34.belvedere_dialog_row_image)).setImageDrawable(k6.c(this.e, a.a()));
            ((TextView) view.findViewById(i34.belvedere_dialog_row_text)).setText(a.b());
            view.setTag(c34);
            return view;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(c34 c34);

        @DexIgnore
        Context getContext();
    }

    @DexIgnore
    public static void a(FragmentManager fragmentManager, List<c34> list) {
        if (list != null && list.size() != 0) {
            a34 a34 = new a34();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("extra_intent", new ArrayList(list));
            a34.setArguments(bundle);
            a34.show(fragmentManager.a(), "BelvedereDialog");
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("extra_intent");
        if (getParentFragment() != null) {
            a((g) new a(this, getParentFragment()), (List<c34>) parcelableArrayList);
        } else if (getActivity() != null) {
            a((g) new b(this, getActivity()), (List<c34>) parcelableArrayList);
        } else {
            Log.w("BelvedereDialog", "Not able to find a valid context for starting an BelvedereIntent");
            dismiss();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, getTheme());
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(j34.belvedere_dialog, viewGroup, false);
        this.e = (ListView) inflate.findViewById(i34.belvedere_dialog_listview);
        return inflate;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public f(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public static f a(c34 c34, Context context) {
            int i = d.a[c34.a().ordinal()];
            if (i == 1) {
                return new f(h34.ic_camera, context.getString(k34.belvedere_dialog_camera));
            }
            if (i != 2) {
                return new f(-1, context.getString(k34.belvedere_dialog_unknown));
            }
            return new f(h34.ic_image, context.getString(k34.belvedere_dialog_gallery));
        }

        @DexIgnore
        public String b() {
            return this.b;
        }

        @DexIgnore
        public int a() {
            return this.a;
        }
    }

    @DexIgnore
    public final void a(g gVar, List<c34> list) {
        this.e.setAdapter(new e(this, gVar.getContext(), j34.belvedere_dialog_row, list));
        this.e.setOnItemClickListener(new c(gVar));
    }
}
