package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lq */
public final class C2339lq implements com.fossil.blesdk.obfuscated.C1820fq<byte[]> {
    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo11011a() {
        return "ByteArrayPool";
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11012b() {
        return 1;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11010a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public byte[] newArray(int i) {
        return new byte[i];
    }
}
