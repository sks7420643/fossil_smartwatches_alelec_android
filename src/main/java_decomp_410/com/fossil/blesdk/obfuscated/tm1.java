package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.google.android.gms.dynamic.RemoteCreator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tm1 extends RemoteCreator<rm1> {
    @DexIgnore
    public static /* final */ tm1 c; // = new tm1();

    @DexIgnore
    public tm1() {
        super("com.google.android.gms.plus.plusone.PlusOneButtonCreatorImpl");
    }

    @DexIgnore
    public static View a(Context context, int i, int i2, String str, int i3) {
        if (str != null) {
            try {
                return (View) un0.d(((rm1) c.a(context)).a(un0.a(context), i, i2, str, i3));
            } catch (Exception unused) {
                return new qm1(context, i);
            }
        } else {
            throw new NullPointerException();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.plus.internal.IPlusOneButtonCreator");
        return queryLocalInterface instanceof rm1 ? (rm1) queryLocalInterface : new sm1(iBinder);
    }
}
