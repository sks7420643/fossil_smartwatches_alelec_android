package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rj */
public class C2811rj extends com.fossil.blesdk.obfuscated.C2022ij {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ java.lang.String f8993j; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkContinuationImpl");

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2968tj f8994a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f8995b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ androidx.work.ExistingWorkPolicy f8996c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.List<? extends com.fossil.blesdk.obfuscated.C2224kj> f8997d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<java.lang.String> f8998e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.List<java.lang.String> f8999f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2811rj> f9000g;

    @DexIgnore
    /* renamed from: h */
    public boolean f9001h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C1804fj f9002i;

    @DexIgnore
    public C2811rj(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy, java.util.List<? extends com.fossil.blesdk.obfuscated.C2224kj> list) {
        this(tjVar, str, existingWorkPolicy, list, (java.util.List<com.fossil.blesdk.obfuscated.C2811rj>) null);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1804fj mo15591a() {
        if (!this.f9001h) {
            com.fossil.blesdk.obfuscated.C2659pl plVar = new com.fossil.blesdk.obfuscated.C2659pl(this);
            this.f8994a.mo16463h().mo8788a(plVar);
            this.f9002i = plVar.mo14739b();
        } else {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9966e(f8993j, java.lang.String.format("Already enqueued work ids (%s)", new java.lang.Object[]{android.text.TextUtils.join(", ", this.f8998e)}), new java.lang.Throwable[0]);
        }
        return this.f9002i;
    }

    @DexIgnore
    /* renamed from: b */
    public androidx.work.ExistingWorkPolicy mo15592b() {
        return this.f8996c;
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.List<java.lang.String> mo15593c() {
        return this.f8998e;
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo15594d() {
        return this.f8995b;
    }

    @DexIgnore
    /* renamed from: e */
    public java.util.List<com.fossil.blesdk.obfuscated.C2811rj> mo15595e() {
        return this.f9000g;
    }

    @DexIgnore
    /* renamed from: f */
    public java.util.List<? extends com.fossil.blesdk.obfuscated.C2224kj> mo15596f() {
        return this.f8997d;
    }

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2968tj mo15597g() {
        return this.f8994a;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo15598h() {
        return m13221a(this, new java.util.HashSet());
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo15599i() {
        return this.f9001h;
    }

    @DexIgnore
    /* renamed from: j */
    public void mo15600j() {
        this.f9001h = true;
    }

    @DexIgnore
    public C2811rj(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy, java.util.List<? extends com.fossil.blesdk.obfuscated.C2224kj> list, java.util.List<com.fossil.blesdk.obfuscated.C2811rj> list2) {
        this.f8994a = tjVar;
        this.f8995b = str;
        this.f8996c = existingWorkPolicy;
        this.f8997d = list;
        this.f9000g = list2;
        this.f8998e = new java.util.ArrayList(this.f8997d.size());
        this.f8999f = new java.util.ArrayList();
        if (list2 != null) {
            for (com.fossil.blesdk.obfuscated.C2811rj rjVar : list2) {
                this.f8999f.addAll(rjVar.f8999f);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            java.lang.String b = ((com.fossil.blesdk.obfuscated.C2224kj) list.get(i)).mo12806b();
            this.f8998e.add(b);
            this.f8999f.add(b);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m13221a(com.fossil.blesdk.obfuscated.C2811rj rjVar, java.util.Set<java.lang.String> set) {
        set.addAll(rjVar.mo15593c());
        java.util.Set<java.lang.String> a = m13220a(rjVar);
        for (java.lang.String contains : set) {
            if (a.contains(contains)) {
                return true;
            }
        }
        java.util.List<com.fossil.blesdk.obfuscated.C2811rj> e = rjVar.mo15595e();
        if (e != null && !e.isEmpty()) {
            for (com.fossil.blesdk.obfuscated.C2811rj a2 : e) {
                if (m13221a(a2, set)) {
                    return true;
                }
            }
        }
        set.removeAll(rjVar.mo15593c());
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.Set<java.lang.String> m13220a(com.fossil.blesdk.obfuscated.C2811rj rjVar) {
        java.util.HashSet hashSet = new java.util.HashSet();
        java.util.List<com.fossil.blesdk.obfuscated.C2811rj> e = rjVar.mo15595e();
        if (e != null && !e.isEmpty()) {
            for (com.fossil.blesdk.obfuscated.C2811rj c : e) {
                hashSet.addAll(c.mo15593c());
            }
        }
        return hashSet;
    }
}
