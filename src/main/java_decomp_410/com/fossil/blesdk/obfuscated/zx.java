package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zx extends e54 implements t64 {
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public zx(v44 v44, String str, String str2, z64 z64, String str3) {
        super(v44, str, str2, z64, HttpMethod.POST);
        this.g = str3;
    }

    @DexIgnore
    public boolean a(List<File> list) {
        HttpRequest a = a();
        a.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        a.c("X-CRASHLYTICS-API-KEY", this.g);
        int i = 0;
        for (File next : list) {
            a.a("session_analytics_file_" + i, next.getName(), "application/vnd.crashlytics.android.events", next);
            i++;
        }
        y44 g2 = q44.g();
        g2.d("Answers", "Sending " + list.size() + " analytics files to " + b());
        int g3 = a.g();
        y44 g4 = q44.g();
        g4.d("Answers", "Response code for analytics file send is " + g3);
        if (w54.a(g3) == 0) {
            return true;
        }
        return false;
    }
}
