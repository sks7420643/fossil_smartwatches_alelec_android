package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v9 */
public final class C3100v9 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f10188a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f10189b;

    @DexIgnore
    /* renamed from: a */
    public static void m15115a(android.widget.CompoundButton compoundButton, android.content.res.ColorStateList colorStateList) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            compoundButton.setButtonTintList(colorStateList);
        } else if (compoundButton instanceof com.fossil.blesdk.obfuscated.C1541ca) {
            ((com.fossil.blesdk.obfuscated.C1541ca) compoundButton).setSupportButtonTintList(colorStateList);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15116a(android.widget.CompoundButton compoundButton, android.graphics.PorterDuff.Mode mode) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            compoundButton.setButtonTintMode(mode);
        } else if (compoundButton instanceof com.fossil.blesdk.obfuscated.C1541ca) {
            ((com.fossil.blesdk.obfuscated.C1541ca) compoundButton).setSupportButtonTintMode(mode);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable m15114a(android.widget.CompoundButton compoundButton) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return compoundButton.getButtonDrawable();
        }
        if (!f10189b) {
            try {
                f10188a = android.widget.CompoundButton.class.getDeclaredField("mButtonDrawable");
                f10188a.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.i("CompoundButtonCompat", "Failed to retrieve mButtonDrawable field", e);
            }
            f10189b = true;
        }
        java.lang.reflect.Field field = f10188a;
        if (field != null) {
            try {
                return (android.graphics.drawable.Drawable) field.get(compoundButton);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.i("CompoundButtonCompat", "Failed to get button drawable via reflection", e2);
                f10188a = null;
            }
        }
        return null;
    }
}
