package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yc */
public class C3352yc {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yc$a */
    public static class C3353a extends com.fossil.blesdk.obfuscated.C3273xc.C3275b {
        @DexIgnore
        public C3353a(android.content.Context context, com.fossil.blesdk.obfuscated.C3352yc.C3354b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        public void onLoadItem(java.lang.String str, android.service.media.MediaBrowserService.Result<android.media.browse.MediaBrowser.MediaItem> result) {
            ((com.fossil.blesdk.obfuscated.C3352yc.C3354b) this.f10895e).mo17380a(str, new com.fossil.blesdk.obfuscated.C3273xc.C3276c(result));
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.yc$b */
    public interface C3354b extends com.fossil.blesdk.obfuscated.C3273xc.C3277d {
        @DexIgnore
        /* renamed from: a */
        void mo17380a(java.lang.String str, com.fossil.blesdk.obfuscated.C3273xc.C3276c<android.os.Parcel> cVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m16800a(android.content.Context context, com.fossil.blesdk.obfuscated.C3352yc.C3354b bVar) {
        return new com.fossil.blesdk.obfuscated.C3352yc.C3353a(context, bVar);
    }
}
