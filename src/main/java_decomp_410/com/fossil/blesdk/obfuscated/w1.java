package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class w1 extends s1 implements SubMenu {
    @DexIgnore
    public w1(Context context, j7 j7Var) {
        super(context, j7Var);
    }

    @DexIgnore
    public j7 c() {
        return (j7) this.a;
    }

    @DexIgnore
    public void clearHeader() {
        c().clearHeader();
    }

    @DexIgnore
    public MenuItem getItem() {
        return a(c().getItem());
    }

    @DexIgnore
    public SubMenu setHeaderIcon(int i) {
        c().setHeaderIcon(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(int i) {
        c().setHeaderTitle(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        c().setHeaderView(view);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(int i) {
        c().setIcon(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderIcon(Drawable drawable) {
        c().setHeaderIcon(drawable);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        c().setHeaderTitle(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(Drawable drawable) {
        c().setIcon(drawable);
        return this;
    }
}
