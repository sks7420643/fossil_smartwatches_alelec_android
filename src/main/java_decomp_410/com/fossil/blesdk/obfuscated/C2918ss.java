package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ss */
public final class C2918ss implements com.fossil.blesdk.obfuscated.C2427mo<java.nio.ByteBuffer, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2590os f9462a; // = new com.fossil.blesdk.obfuscated.C2590os();

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.nio.ByteBuffer byteBuffer, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(java.nio.ByteBuffer byteBuffer, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return this.f9462a.mo9299a(android.graphics.ImageDecoder.createSource(byteBuffer), i, i2, loVar);
    }
}
