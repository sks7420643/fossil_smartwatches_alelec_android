package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hf2 extends gf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public long t;

    /*
    static {
        v.put(R.id.ftv_title, 1);
        v.put(R.id.ll_time_container, 2);
        v.put(R.id.numberPicker, 3);
        v.put(R.id.fb_save, 4);
    }
    */

    @DexIgnore
    public hf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 5, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public hf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[0], objArr[4], objArr[1], objArr[2], objArr[3]);
        this.t = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
