package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ht implements mo<Bitmap, Bitmap> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements aq<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap e;

        @DexIgnore
        public a(Bitmap bitmap) {
            this.e = bitmap;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public int b() {
            return uw.a(this.e);
        }

        @DexIgnore
        public Class<Bitmap> c() {
            return Bitmap.class;
        }

        @DexIgnore
        public Bitmap get() {
            return this.e;
        }
    }

    @DexIgnore
    public boolean a(Bitmap bitmap, lo loVar) {
        return true;
    }

    @DexIgnore
    public aq<Bitmap> a(Bitmap bitmap, int i, int i2, lo loVar) {
        return new a(bitmap);
    }
}
