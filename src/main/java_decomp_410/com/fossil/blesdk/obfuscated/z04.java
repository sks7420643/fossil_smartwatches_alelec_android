package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ k04 f;

    @DexIgnore
    public z04(Context context, k04 k04) {
        this.e = context;
        this.f = k04;
    }

    @DexIgnore
    public final void run() {
        try {
            j04.a(this.e, false, this.f);
        } catch (Throwable th) {
            j04.m.a(th);
        }
    }
}
