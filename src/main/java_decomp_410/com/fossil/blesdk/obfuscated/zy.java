package com.fossil.blesdk.obfuscated;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zy implements b74 {
    @DexIgnore
    public /* final */ rz a;

    @DexIgnore
    public zy(rz rzVar) {
        this.a = rzVar;
    }

    @DexIgnore
    public String a() {
        return this.a.a();
    }

    @DexIgnore
    public InputStream b() {
        return this.a.b();
    }

    @DexIgnore
    public String[] c() {
        return this.a.c();
    }

    @DexIgnore
    public long d() {
        return -1;
    }
}
