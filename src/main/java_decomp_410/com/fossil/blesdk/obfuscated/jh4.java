package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jh4<T> extends mh4<T> implements fc4, yb4<T> {
    @DexIgnore
    public Object h; // = lh4.a;
    @DexIgnore
    public /* final */ fc4 i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public /* final */ ug4 k;
    @DexIgnore
    public /* final */ yb4<T> l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jh4(ug4 ug4, yb4<? super T> yb4) {
        super(0);
        kd4.b(ug4, "dispatcher");
        kd4.b(yb4, "continuation");
        this.k = ug4;
        this.l = yb4;
        yb4<T> yb42 = this.l;
        this.i = (fc4) (!(yb42 instanceof fc4) ? null : yb42);
        this.j = ThreadContextKt.a(getContext());
    }

    @DexIgnore
    public yb4<T> b() {
        return this;
    }

    @DexIgnore
    public Object c() {
        Object obj = this.h;
        if (ch4.a()) {
            if (!(obj != lh4.a)) {
                throw new AssertionError();
            }
        }
        this.h = lh4.a;
        return obj;
    }

    @DexIgnore
    public final void d(T t) {
        CoroutineContext context = this.l.getContext();
        this.h = t;
        this.g = 1;
        this.k.b(context, this);
    }

    @DexIgnore
    public fc4 getCallerFrame() {
        return this.i;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.l.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        CoroutineContext context;
        Object b;
        CoroutineContext context2 = this.l.getContext();
        Object a = og4.a(obj);
        if (this.k.b(context2)) {
            this.h = a;
            this.g = 0;
            this.k.a(context2, this);
            return;
        }
        sh4 b2 = bj4.b.b();
        if (b2.D()) {
            this.h = a;
            this.g = 0;
            b2.a((mh4<?>) this);
            return;
        }
        b2.c(true);
        try {
            context = getContext();
            b = ThreadContextKt.b(context, this.j);
            this.l.resumeWith(obj);
            qa4 qa4 = qa4.a;
            ThreadContextKt.a(context, b);
            do {
            } while (b2.G());
        } catch (Throwable th) {
            try {
                a(th, (Throwable) null);
            } catch (Throwable th2) {
                b2.a(true);
                throw th2;
            }
        }
        b2.a(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.k + ", " + dh4.a((yb4<?>) this.l) + ']';
    }
}
