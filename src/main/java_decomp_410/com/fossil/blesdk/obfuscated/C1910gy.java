package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gy */
public class C1910gy {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2081iy f5586a;

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011  */
    /*
    static {
        com.fossil.blesdk.obfuscated.C2081iy iyVar;
        try {
            iyVar = com.fossil.blesdk.obfuscated.C1844fy.m7184a();
        } catch (java.lang.IllegalStateException | java.lang.NoClassDefFoundError unused) {
            iyVar = null;
            if (iyVar == null) {
            }
            f5586a = iyVar;
        } catch (Throwable th) {
            android.util.Log.w("AnswersOptionalLogger", "Unexpected error creating AnswersKitEventLogger", th);
            iyVar = null;
            if (iyVar == null) {
            }
            f5586a = iyVar;
        }
        if (iyVar == null) {
            iyVar = com.fossil.blesdk.obfuscated.C2168jy.m9219a();
        }
        f5586a = iyVar;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2081iy m7634a() {
        return f5586a;
    }
}
