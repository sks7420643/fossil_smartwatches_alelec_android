package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lp1 implements Parcelable.Creator<kp1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        ArrayList<zp1> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                arrayList = SafeParcelReader.c(parcel, a, zp1.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new kp1(str, arrayList);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new kp1[i];
    }
}
