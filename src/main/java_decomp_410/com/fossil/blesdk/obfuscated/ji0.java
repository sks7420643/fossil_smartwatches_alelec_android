package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ji0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ii0 e;

    @DexIgnore
    public ji0(ii0 ii0) {
        this.e = ii0;
    }

    @DexIgnore
    public final void run() {
        this.e.q.lock();
        try {
            this.e.i();
        } finally {
            this.e.q.unlock();
        }
    }
}
