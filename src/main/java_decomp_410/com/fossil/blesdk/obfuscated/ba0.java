package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import kotlin.text.StringsKt__StringsKt;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ba0 implements v90 {
    @DexIgnore
    public JSONObject a(File file) {
        kd4.b(file, "logFile");
        JSONObject jSONObject = new JSONObject();
        JSONArray b = b(file);
        if (b.length() > 0) {
            jSONObject.put("data", b);
        }
        return jSONObject;
    }

    @DexIgnore
    public final JSONArray b(File file) {
        JSONArray jSONArray = new JSONArray();
        String a = gb0.a.a(file);
        if (a != null) {
            List a2 = StringsKt__StringsKt.a((CharSequence) a, new String[]{ua0.y.w()}, false, 0, 6, (Object) null);
            ArrayList<String> arrayList = new ArrayList<>();
            for (Object next : a2) {
                if (!qf4.a((String) next)) {
                    arrayList.add(next);
                }
            }
            for (String a3 : arrayList) {
                String a4 = kb0.c.a(a3);
                if (a4 != null) {
                    try {
                        jSONArray.put(new JSONObject(a4));
                    } catch (Exception e) {
                        da0.l.a(e);
                        qa4 qa4 = qa4.a;
                    }
                }
            }
        }
        return jSONArray;
    }
}
