package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o31 extends jk0 implements me0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<o31> CREATOR; // = new p31();
    @DexIgnore
    public /* final */ Status e;

    /*
    static {
        new o31(Status.i);
    }
    */

    @DexIgnore
    public o31(Status status) {
        this.e = status;
    }

    @DexIgnore
    public final Status G() {
        return this.e;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) G(), i, false);
        kk0.a(parcel, a);
    }
}
