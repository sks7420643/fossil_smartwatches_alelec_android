package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lr implements jo {
    @DexIgnore
    public /* final */ mr b;
    @DexIgnore
    public /* final */ URL c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public URL f;
    @DexIgnore
    public volatile byte[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public lr(URL url) {
        this(url, mr.a);
    }

    @DexIgnore
    public String a() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        tw.a(url);
        return url.toString();
    }

    @DexIgnore
    public final byte[] b() {
        if (this.g == null) {
            this.g = a().getBytes(jo.a);
        }
        return this.g;
    }

    @DexIgnore
    public Map<String, String> c() {
        return this.b.a();
    }

    @DexIgnore
    public final String d() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                tw.a(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    @DexIgnore
    public final URL e() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(d());
        }
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof lr)) {
            return false;
        }
        lr lrVar = (lr) obj;
        if (!a().equals(lrVar.a()) || !this.b.equals(lrVar.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public URL f() throws MalformedURLException {
        return e();
    }

    @DexIgnore
    public int hashCode() {
        if (this.h == 0) {
            this.h = a().hashCode();
            this.h = (this.h * 31) + this.b.hashCode();
        }
        return this.h;
    }

    @DexIgnore
    public String toString() {
        return a();
    }

    @DexIgnore
    public lr(String str) {
        this(str, mr.a);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b());
    }

    @DexIgnore
    public lr(URL url, mr mrVar) {
        tw.a(url);
        this.c = url;
        this.d = null;
        tw.a(mrVar);
        this.b = mrVar;
    }

    @DexIgnore
    public lr(String str, mr mrVar) {
        this.c = null;
        tw.a(str);
        this.d = str;
        tw.a(mrVar);
        this.b = mrVar;
    }
}
