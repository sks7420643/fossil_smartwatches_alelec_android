package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cq implements jo {
    @DexIgnore
    public static /* final */ qw<Class<?>, byte[]> j; // = new qw<>(50);
    @DexIgnore
    public /* final */ gq b;
    @DexIgnore
    public /* final */ jo c;
    @DexIgnore
    public /* final */ jo d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Class<?> g;
    @DexIgnore
    public /* final */ lo h;
    @DexIgnore
    public /* final */ oo<?> i;

    @DexIgnore
    public cq(gq gqVar, jo joVar, jo joVar2, int i2, int i3, oo<?> ooVar, Class<?> cls, lo loVar) {
        this.b = gqVar;
        this.c = joVar;
        this.d = joVar2;
        this.e = i2;
        this.f = i3;
        this.i = ooVar;
        this.g = cls;
        this.h = loVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.e).putInt(this.f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        oo<?> ooVar = this.i;
        if (ooVar != null) {
            ooVar.a(messageDigest);
        }
        this.h.a(messageDigest);
        messageDigest.update(a());
        this.b.put(bArr);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof cq)) {
            return false;
        }
        cq cqVar = (cq) obj;
        if (this.f != cqVar.f || this.e != cqVar.e || !uw.b((Object) this.i, (Object) cqVar.i) || !this.g.equals(cqVar.g) || !this.c.equals(cqVar.c) || !this.d.equals(cqVar.d) || !this.h.equals(cqVar.h)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((this.c.hashCode() * 31) + this.d.hashCode()) * 31) + this.e) * 31) + this.f;
        oo<?> ooVar = this.i;
        if (ooVar != null) {
            hashCode = (hashCode * 31) + ooVar.hashCode();
        }
        return (((hashCode * 31) + this.g.hashCode()) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.c + ", signature=" + this.d + ", width=" + this.e + ", height=" + this.f + ", decodedResourceClass=" + this.g + ", transformation='" + this.i + '\'' + ", options=" + this.h + '}';
    }

    @DexIgnore
    public final byte[] a() {
        byte[] a = j.a(this.g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.g.getName().getBytes(jo.a);
        j.b(this.g, bytes);
        return bytes;
    }
}
