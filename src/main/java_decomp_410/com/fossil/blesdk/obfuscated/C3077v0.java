package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v0 */
public class C3077v0 extends androidx.appcompat.view.ActionMode implements com.fossil.blesdk.obfuscated.C1915h1.C1916a {

    @DexIgnore
    /* renamed from: g */
    public android.content.Context f10114g;

    @DexIgnore
    /* renamed from: h */
    public androidx.appcompat.widget.ActionBarContextView f10115h;

    @DexIgnore
    /* renamed from: i */
    public androidx.appcompat.view.ActionMode.Callback f10116i;

    @DexIgnore
    /* renamed from: j */
    public java.lang.ref.WeakReference<android.view.View> f10117j;

    @DexIgnore
    /* renamed from: k */
    public boolean f10118k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1915h1 f10119l;

    @DexIgnore
    public C3077v0(android.content.Context context, androidx.appcompat.widget.ActionBarContextView actionBarContextView, androidx.appcompat.view.ActionMode.Callback callback, boolean z) {
        this.f10114g = context;
        this.f10115h = actionBarContextView;
        this.f10116i = callback;
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = new com.fossil.blesdk.obfuscated.C1915h1(actionBarContextView.getContext());
        h1Var.mo11485c(1);
        this.f10119l = h1Var;
        this.f10119l.mo11460a((com.fossil.blesdk.obfuscated.C1915h1.C1916a) this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo331a(java.lang.CharSequence charSequence) {
        this.f10115h.setSubtitle(charSequence);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo336b(java.lang.CharSequence charSequence) {
        this.f10115h.setTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: c */
    public android.view.Menu mo337c() {
        return this.f10119l;
    }

    @DexIgnore
    /* renamed from: d */
    public android.view.MenuInflater mo338d() {
        return new com.fossil.blesdk.obfuscated.C3251x0(this.f10115h.getContext());
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.CharSequence mo339e() {
        return this.f10115h.getSubtitle();
    }

    @DexIgnore
    /* renamed from: g */
    public java.lang.CharSequence mo341g() {
        return this.f10115h.getTitle();
    }

    @DexIgnore
    /* renamed from: i */
    public void mo343i() {
        this.f10116i.mo348b(this, this.f10119l);
    }

    @DexIgnore
    /* renamed from: j */
    public boolean mo344j() {
        return this.f10115h.mo417c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo329a(int i) {
        mo331a((java.lang.CharSequence) this.f10114g.getString(i));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo335b(int i) {
        mo336b((java.lang.CharSequence) this.f10114g.getString(i));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo333a(boolean z) {
        super.mo333a(z);
        this.f10115h.setTitleOptional(z);
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo334b() {
        java.lang.ref.WeakReference<android.view.View> weakReference = this.f10117j;
        if (weakReference != null) {
            return (android.view.View) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo330a(android.view.View view) {
        this.f10115h.setCustomView(view);
        this.f10117j = view != null ? new java.lang.ref.WeakReference<>(view) : null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo328a() {
        if (!this.f10118k) {
            this.f10118k = true;
            this.f10115h.sendAccessibilityEvent(32);
            this.f10116i.mo345a(this);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo536a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        return this.f10116i.mo347a((androidx.appcompat.view.ActionMode) this, menuItem);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo535a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        mo343i();
        this.f10115h.mo419e();
    }
}
