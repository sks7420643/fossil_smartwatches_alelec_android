package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import com.crashlytics.android.answers.SessionEvent;
import com.fossil.blesdk.obfuscated.lx;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ay implements lx.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ fx b;
    @DexIgnore
    public /* final */ o44 c;
    @DexIgnore
    public /* final */ lx d;
    @DexIgnore
    public /* final */ ix e;

    @DexIgnore
    public ay(fx fxVar, o44 o44, lx lxVar, ix ixVar, long j) {
        this.b = fxVar;
        this.c = o44;
        this.d = lxVar;
        this.e = ixVar;
        this.a = j;
    }

    @DexIgnore
    public static ay a(v44 v44, Context context, IdManager idManager, String str, String str2, long j) {
        Context context2 = context;
        IdManager idManager2 = idManager;
        ey eyVar = new ey(context, idManager, str, str2);
        gx gxVar = new gx(context, new f74(v44));
        y64 y64 = new y64(q44.g());
        o44 o44 = new o44(context);
        ScheduledExecutorService b2 = q54.b("Answers Events Handler");
        lx lxVar = new lx(b2);
        return new ay(new fx(v44, context, gxVar, eyVar, y64, b2, new rx(context)), o44, lxVar, ix.a(context), j);
    }

    @DexIgnore
    public void b() {
        this.c.a();
        this.b.a();
    }

    @DexIgnore
    public void c() {
        this.b.b();
        this.c.a(new hx(this, this.d));
        this.d.a((lx.b) this);
        if (d()) {
            a(this.a);
            this.e.b();
        }
    }

    @DexIgnore
    public boolean d() {
        return !this.e.a();
    }

    @DexIgnore
    public void a(mx mxVar) {
        y44 g = q44.g();
        g.d("Answers", "Logged custom event: " + mxVar);
        this.b.a(SessionEvent.a(mxVar));
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            q44.g().d("Answers", "Logged crash");
            this.b.c(SessionEvent.a(str, str2));
            return;
        }
        throw new IllegalStateException("onCrash called from main thread!!!");
    }

    @DexIgnore
    public void a(long j) {
        q44.g().d("Answers", "Logged install");
        this.b.b(SessionEvent.a(j));
    }

    @DexIgnore
    public void a(Activity activity, SessionEvent.Type type) {
        y44 g = q44.g();
        g.d("Answers", "Logged lifecycle event: " + type.name());
        this.b.a(SessionEvent.a(type, activity));
    }

    @DexIgnore
    public void a() {
        q44.g().d("Answers", "Flush events when app is backgrounded");
        this.b.c();
    }

    @DexIgnore
    public void a(j74 j74, String str) {
        this.d.a(j74.i);
        this.b.a(j74, str);
    }
}
