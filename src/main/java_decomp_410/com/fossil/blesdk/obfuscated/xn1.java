package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xn1<TResult> {
    @DexIgnore
    public /* final */ uo1<TResult> a; // = new uo1<>();

    @DexIgnore
    public xn1() {
    }

    @DexIgnore
    public void a(TResult tresult) {
        this.a.a(tresult);
    }

    @DexIgnore
    public boolean b(TResult tresult) {
        return this.a.b(tresult);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.a.a(exc);
    }

    @DexIgnore
    public boolean b(Exception exc) {
        return this.a.b(exc);
    }

    @DexIgnore
    public xn1(nn1 nn1) {
        nn1.a(new so1(this));
    }

    @DexIgnore
    public wn1<TResult> a() {
        return this.a;
    }
}
