package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hk0 {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ String b; // = this.a.getResourcePackageName(be0.common_google_play_services_unknown_issue);

    @DexIgnore
    public hk0(Context context) {
        bk0.a(context);
        this.a = context.getResources();
    }

    @DexIgnore
    public String a(String str) {
        int identifier = this.a.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
