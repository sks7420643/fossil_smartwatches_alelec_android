package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fq<T> {
    @DexIgnore
    int a(T t);

    @DexIgnore
    String a();

    @DexIgnore
    int b();

    @DexIgnore
    T newArray(int i);
}
