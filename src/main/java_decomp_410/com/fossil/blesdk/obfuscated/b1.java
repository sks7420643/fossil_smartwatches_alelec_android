package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.p1;
import com.fossil.blesdk.obfuscated.q1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class b1 implements p1 {
    @DexIgnore
    public Context e;
    @DexIgnore
    public Context f;
    @DexIgnore
    public h1 g;
    @DexIgnore
    public LayoutInflater h;
    @DexIgnore
    public p1.a i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public q1 l;
    @DexIgnore
    public int m;

    @DexIgnore
    public b1(Context context, int i2, int i3) {
        this.e = context;
        this.h = LayoutInflater.from(context);
        this.j = i2;
        this.k = i3;
    }

    @DexIgnore
    public void a(Context context, h1 h1Var) {
        this.f = context;
        LayoutInflater.from(this.f);
        this.g = h1Var;
    }

    @DexIgnore
    public abstract void a(k1 k1Var, q1.a aVar);

    @DexIgnore
    public abstract boolean a(int i2, k1 k1Var);

    @DexIgnore
    public boolean a(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public q1 b(ViewGroup viewGroup) {
        if (this.l == null) {
            this.l = (q1) this.h.inflate(this.j, viewGroup, false);
            this.l.a(this.g);
            a(true);
        }
        return this.l;
    }

    @DexIgnore
    public boolean b(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public p1.a c() {
        return this.i;
    }

    @DexIgnore
    public int getId() {
        return this.m;
    }

    @DexIgnore
    public void a(boolean z) {
        ViewGroup viewGroup = (ViewGroup) this.l;
        if (viewGroup != null) {
            h1 h1Var = this.g;
            int i2 = 0;
            if (h1Var != null) {
                h1Var.b();
                ArrayList<k1> n = this.g.n();
                int size = n.size();
                int i3 = 0;
                for (int i4 = 0; i4 < size; i4++) {
                    k1 k1Var = n.get(i4);
                    if (a(i3, k1Var)) {
                        View childAt = viewGroup.getChildAt(i3);
                        k1 itemData = childAt instanceof q1.a ? ((q1.a) childAt).getItemData() : null;
                        View a = a(k1Var, childAt, viewGroup);
                        if (k1Var != itemData) {
                            a.setPressed(false);
                            a.jumpDrawablesToCurrentState();
                        }
                        if (a != childAt) {
                            a(a, i3);
                        }
                        i3++;
                    }
                }
                i2 = i3;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.l).addView(view, i2);
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    @DexIgnore
    public void a(p1.a aVar) {
        this.i = aVar;
    }

    @DexIgnore
    public q1.a a(ViewGroup viewGroup) {
        return (q1.a) this.h.inflate(this.k, viewGroup, false);
    }

    @DexIgnore
    public View a(k1 k1Var, View view, ViewGroup viewGroup) {
        q1.a aVar;
        if (view instanceof q1.a) {
            aVar = (q1.a) view;
        } else {
            aVar = a(viewGroup);
        }
        a(k1Var, aVar);
        return (View) aVar;
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z) {
        p1.a aVar = this.i;
        if (aVar != null) {
            aVar.a(h1Var, z);
        }
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        p1.a aVar = this.i;
        if (aVar != null) {
            return aVar.a(v1Var);
        }
        return false;
    }

    @DexIgnore
    public void a(int i2) {
        this.m = i2;
    }
}
