package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.hg;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jf {
    @DexIgnore
    public /* final */ hg.c a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ RoomDatabase.c d;
    @DexIgnore
    public /* final */ List<RoomDatabase.b> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ RoomDatabase.JournalMode g;
    @DexIgnore
    public /* final */ Executor h;
    @DexIgnore
    public /* final */ Executor i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Set<Integer> m;

    @DexIgnore
    public jf(Context context, String str, hg.c cVar, RoomDatabase.c cVar2, List<RoomDatabase.b> list, boolean z, RoomDatabase.JournalMode journalMode, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set) {
        this.a = cVar;
        this.b = context;
        this.c = str;
        this.d = cVar2;
        this.e = list;
        this.f = z;
        this.g = journalMode;
        this.h = executor;
        this.i = executor2;
        this.j = z2;
        this.k = z3;
        this.l = z4;
        this.m = set;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        if ((!(i2 > i3) || !this.l) && this.k) {
            Set<Integer> set = this.m;
            if (set == null || !set.contains(Integer.valueOf(i2))) {
                return true;
            }
        }
        return false;
    }
}
