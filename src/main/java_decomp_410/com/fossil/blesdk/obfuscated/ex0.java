package com.fossil.blesdk.obfuscated;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ex0 extends AbstractList<String> implements dv0, RandomAccess {
    @DexIgnore
    public /* final */ dv0 e;

    @DexIgnore
    public ex0(dv0 dv0) {
        this.e = dv0;
    }

    @DexIgnore
    public final List<?> D() {
        return this.e.D();
    }

    @DexIgnore
    public final dv0 F() {
        return this;
    }

    @DexIgnore
    public final Object e(int i) {
        return this.e.e(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return (String) this.e.get(i);
    }

    @DexIgnore
    public final Iterator<String> iterator() {
        return new gx0(this);
    }

    @DexIgnore
    public final ListIterator<String> listIterator(int i) {
        return new fx0(this, i);
    }

    @DexIgnore
    public final int size() {
        return this.e.size();
    }
}
