package com.fossil.blesdk.obfuscated;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f22 implements r12 {
    @DexIgnore
    public b22 a(String str, BarcodeFormat barcodeFormat, int i, int i2, Map<EncodeHintType, ?> map) {
        o12 o12;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (barcodeFormat != BarcodeFormat.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + barcodeFormat);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            SymbolShapeHint symbolShapeHint = SymbolShapeHint.FORCE_NONE;
            o12 o122 = null;
            if (map != null) {
                SymbolShapeHint symbolShapeHint2 = (SymbolShapeHint) map.get(EncodeHintType.DATA_MATRIX_SHAPE);
                if (symbolShapeHint2 != null) {
                    symbolShapeHint = symbolShapeHint2;
                }
                o12 = (o12) map.get(EncodeHintType.MIN_SIZE);
                if (o12 == null) {
                    o12 = null;
                }
                o12 o123 = (o12) map.get(EncodeHintType.MAX_SIZE);
                if (o123 != null) {
                    o122 = o123;
                }
            } else {
                o12 = null;
            }
            String a = p22.a(str, symbolShapeHint, o12, o122);
            q22 a2 = q22.a(a.length(), symbolShapeHint, o12, o122, true);
            k22 k22 = new k22(o22.a(a, a2), a2.f(), a2.e());
            k22.a();
            return a(k22, a2);
        }
    }

    @DexIgnore
    public static b22 a(k22 k22, q22 q22) {
        int f = q22.f();
        int e = q22.e();
        w32 w32 = new w32(q22.h(), q22.g());
        int i = 0;
        for (int i2 = 0; i2 < e; i2++) {
            if (i2 % q22.e == 0) {
                int i3 = 0;
                for (int i4 = 0; i4 < q22.h(); i4++) {
                    w32.a(i3, i, i4 % 2 == 0);
                    i3++;
                }
                i++;
            }
            int i5 = 0;
            for (int i6 = 0; i6 < f; i6++) {
                if (i6 % q22.d == 0) {
                    w32.a(i5, i, true);
                    i5++;
                }
                w32.a(i5, i, k22.a(i6, i2));
                i5++;
                int i7 = q22.d;
                if (i6 % i7 == i7 - 1) {
                    w32.a(i5, i, i2 % 2 == 0);
                    i5++;
                }
            }
            i++;
            int i8 = q22.e;
            if (i2 % i8 == i8 - 1) {
                int i9 = 0;
                for (int i10 = 0; i10 < q22.h(); i10++) {
                    w32.a(i9, i, true);
                    i9++;
                }
                i++;
            }
        }
        return a(w32);
    }

    @DexIgnore
    public static b22 a(w32 w32) {
        int c = w32.c();
        int b = w32.b();
        b22 b22 = new b22(c, b);
        b22.a();
        for (int i = 0; i < c; i++) {
            for (int i2 = 0; i2 < b; i2++) {
                if (w32.a(i, i2) == 1) {
                    b22.b(i, i2);
                }
            }
        }
        return b22;
    }
}
