package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bo */
public final class C1496bo {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.nio.charset.Charset f3793a; // = java.nio.charset.Charset.forName("US-ASCII");

    /*
    static {
        java.nio.charset.Charset.forName("UTF-8");
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static void m5048a(java.io.File file) throws java.io.IOException {
        java.io.File[] listFiles = file.listFiles();
        if (listFiles != null) {
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                java.io.File file2 = listFiles[i];
                if (file2.isDirectory()) {
                    m5048a(file2);
                }
                if (file2.delete()) {
                    i++;
                } else {
                    throw new java.io.IOException("failed to delete file: " + file2);
                }
            }
            return;
        }
        throw new java.io.IOException("not a readable directory: " + file);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5047a(java.io.Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (java.lang.RuntimeException e) {
                throw e;
            } catch (java.lang.Exception unused) {
            }
        }
    }
}
