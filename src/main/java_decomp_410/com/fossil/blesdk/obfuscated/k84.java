package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class k84<T> implements zp4<T> {
    @DexIgnore
    public static /* final */ int a; // = Math.max(1, Integer.getInteger("rx2.buffer-size", 128).intValue());

    @DexIgnore
    public static int d() {
        return a;
    }

    @DexIgnore
    public final k84<T> a() {
        return a(d(), false, true);
    }

    @DexIgnore
    public final k84<T> b() {
        return ia4.a(new w94(this));
    }

    @DexIgnore
    public final k84<T> c() {
        return ia4.a(new y94(this));
    }

    @DexIgnore
    public final k84<T> a(int i, boolean z, boolean z2) {
        k94.a(i, "bufferSize");
        return ia4.a(new v94(this, i, z2, z, j94.a));
    }
}
