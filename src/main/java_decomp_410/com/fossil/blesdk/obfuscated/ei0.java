package com.fossil.blesdk.obfuscated;

import android.app.Dialog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ei0 extends ah0 {
    @DexIgnore
    public /* final */ /* synthetic */ Dialog a;
    @DexIgnore
    public /* final */ /* synthetic */ di0 b;

    @DexIgnore
    public ei0(di0 di0, Dialog dialog) {
        this.b = di0;
        this.a = dialog;
    }

    @DexIgnore
    public final void a() {
        this.b.f.g();
        if (this.a.isShowing()) {
            this.a.dismiss();
        }
    }
}
