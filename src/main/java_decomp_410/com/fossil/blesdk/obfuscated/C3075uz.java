package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uz */
public final class C3075uz {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.regex.Pattern f10113a; // = java.util.regex.Pattern.compile("\\s*(\\p{XDigit}+)-\\s*(\\p{XDigit}+)\\s+(.{4})\\s+\\p{XDigit}+\\s+.+\\s+\\d+\\s+(.*)");

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2997tz m14982a(java.lang.String str) {
        java.util.regex.Matcher matcher = f10113a.matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        try {
            long longValue = java.lang.Long.valueOf(matcher.group(1), 16).longValue();
            com.fossil.blesdk.obfuscated.C2997tz tzVar = new com.fossil.blesdk.obfuscated.C2997tz(longValue, java.lang.Long.valueOf(matcher.group(2), 16).longValue() - longValue, matcher.group(3), matcher.group(4));
            return tzVar;
        } catch (java.lang.Exception unused) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("CrashlyticsCore", "Could not parse map entry: " + str);
            return null;
        }
    }
}
