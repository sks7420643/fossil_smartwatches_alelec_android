package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a21 extends IInterface {
    @DexIgnore
    void a(kq0 kq0, g11 g11) throws RemoteException;

    @DexIgnore
    void a(w11 w11, o01 o01) throws RemoteException;

    @DexIgnore
    void a(y11 y11, g11 g11) throws RemoteException;
}
