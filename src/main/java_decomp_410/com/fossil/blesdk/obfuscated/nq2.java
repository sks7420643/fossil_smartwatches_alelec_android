package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nq2 extends lq2 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nq2(String str, String str2, boolean z) {
        super(str, str2);
        kd4.b(str, "tagName");
        kd4.b(str2, "title");
        this.d = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
