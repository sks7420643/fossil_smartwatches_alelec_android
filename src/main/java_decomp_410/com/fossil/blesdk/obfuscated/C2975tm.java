package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tm */
public class C2975tm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.atomic.AtomicInteger f9714a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Set<com.android.volley.Request<?>> f9715b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.concurrent.PriorityBlockingQueue<com.android.volley.Request<?>> f9716c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.concurrent.PriorityBlockingQueue<com.android.volley.Request<?>> f9717d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2334lm f9718e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2748qm f9719f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C3124vm f9720g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2814rm[] f9721h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2423mm f9722i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2975tm.C2976a> f9723j;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.tm$a */
    public interface C2976a<T> {
        @DexIgnore
        /* renamed from: a */
        void mo16508a(com.android.volley.Request<T> request);
    }

    @DexIgnore
    public C2975tm(com.fossil.blesdk.obfuscated.C2334lm lmVar, com.fossil.blesdk.obfuscated.C2748qm qmVar, int i, com.fossil.blesdk.obfuscated.C3124vm vmVar) {
        this.f9714a = new java.util.concurrent.atomic.AtomicInteger();
        this.f9715b = new java.util.HashSet();
        this.f9716c = new java.util.concurrent.PriorityBlockingQueue<>();
        this.f9717d = new java.util.concurrent.PriorityBlockingQueue<>();
        this.f9723j = new java.util.ArrayList();
        this.f9718e = lmVar;
        this.f9719f = qmVar;
        this.f9721h = new com.fossil.blesdk.obfuscated.C2814rm[i];
        this.f9720g = vmVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo16503a() {
        return this.f9714a.incrementAndGet();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16505b() {
        mo16507c();
        this.f9722i = new com.fossil.blesdk.obfuscated.C2423mm(this.f9716c, this.f9717d, this.f9718e, this.f9720g);
        this.f9722i.start();
        for (int i = 0; i < this.f9721h.length; i++) {
            com.fossil.blesdk.obfuscated.C2814rm rmVar = new com.fossil.blesdk.obfuscated.C2814rm(this.f9717d, this.f9719f, this.f9718e, this.f9720g);
            this.f9721h[i] = rmVar;
            rmVar.start();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16507c() {
        com.fossil.blesdk.obfuscated.C2423mm mmVar = this.f9722i;
        if (mmVar != null) {
            mmVar.mo13682b();
        }
        for (com.fossil.blesdk.obfuscated.C2814rm rmVar : this.f9721h) {
            if (rmVar != null) {
                rmVar.mo15635b();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <T> com.android.volley.Request<T> mo16504a(com.android.volley.Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.f9715b) {
            this.f9715b.add(request);
        }
        request.setSequence(mo16503a());
        request.addMarker("add-to-queue");
        if (!request.shouldCache()) {
            this.f9717d.add(request);
            return request;
        }
        this.f9716c.add(request);
        return request;
    }

    @DexIgnore
    /* renamed from: b */
    public <T> void mo16506b(com.android.volley.Request<T> request) {
        synchronized (this.f9715b) {
            this.f9715b.remove(request);
        }
        synchronized (this.f9723j) {
            for (com.fossil.blesdk.obfuscated.C2975tm.C2976a a : this.f9723j) {
                a.mo16508a(request);
            }
        }
    }

    @DexIgnore
    public C2975tm(com.fossil.blesdk.obfuscated.C2334lm lmVar, com.fossil.blesdk.obfuscated.C2748qm qmVar, int i) {
        this(lmVar, qmVar, i, new com.fossil.blesdk.obfuscated.C2577om(new android.os.Handler(android.os.Looper.getMainLooper())));
    }

    @DexIgnore
    public C2975tm(com.fossil.blesdk.obfuscated.C2334lm lmVar, com.fossil.blesdk.obfuscated.C2748qm qmVar) {
        this(lmVar, qmVar, 4);
    }
}
