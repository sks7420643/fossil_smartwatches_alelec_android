package com.fossil.blesdk.obfuscated;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uv1 {
    @DexIgnore
    public static <T> T a(Class<T> cls, InvocationHandler invocationHandler) {
        st1.a(invocationHandler);
        st1.a(cls.isInterface(), "%s is not an interface", (Object) cls);
        return cls.cast(Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, invocationHandler));
    }
}
