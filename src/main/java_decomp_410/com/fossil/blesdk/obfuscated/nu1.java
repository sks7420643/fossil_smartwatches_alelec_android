package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.qt1;
import com.google.common.base.Equivalence;
import com.google.common.collect.MapMakerInternalMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nu1 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public MapMakerInternalMap.Strength d;
    @DexIgnore
    public MapMakerInternalMap.Strength e;
    @DexIgnore
    public Equivalence<Object> f;

    @DexIgnore
    public nu1 a(Equivalence<Object> equivalence) {
        st1.b(this.f == null, "key equivalence was already set to %s", (Object) this.f);
        st1.a(equivalence);
        this.f = equivalence;
        this.a = true;
        return this;
    }

    @DexIgnore
    public nu1 b(int i) {
        boolean z = true;
        st1.b(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        st1.a(z);
        this.b = i;
        return this;
    }

    @DexIgnore
    public Equivalence<Object> c() {
        return (Equivalence) qt1.a(this.f, d().defaultEquivalence());
    }

    @DexIgnore
    public MapMakerInternalMap.Strength d() {
        return (MapMakerInternalMap.Strength) qt1.a(this.d, MapMakerInternalMap.Strength.STRONG);
    }

    @DexIgnore
    public MapMakerInternalMap.Strength e() {
        return (MapMakerInternalMap.Strength) qt1.a(this.e, MapMakerInternalMap.Strength.STRONG);
    }

    @DexIgnore
    public <K, V> ConcurrentMap<K, V> f() {
        if (!this.a) {
            return new ConcurrentHashMap(b(), 0.75f, a());
        }
        return MapMakerInternalMap.create(this);
    }

    @DexIgnore
    public nu1 g() {
        a(MapMakerInternalMap.Strength.WEAK);
        return this;
    }

    @DexIgnore
    public String toString() {
        qt1.b a2 = qt1.a(this);
        int i = this.b;
        if (i != -1) {
            a2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            a2.a("concurrencyLevel", i2);
        }
        MapMakerInternalMap.Strength strength = this.d;
        if (strength != null) {
            a2.a("keyStrength", (Object) mt1.a(strength.toString()));
        }
        MapMakerInternalMap.Strength strength2 = this.e;
        if (strength2 != null) {
            a2.a("valueStrength", (Object) mt1.a(strength2.toString()));
        }
        if (this.f != null) {
            a2.b("keyEquivalence");
        }
        return a2.toString();
    }

    @DexIgnore
    public nu1 a(int i) {
        boolean z = true;
        st1.b(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        st1.a(z);
        this.c = i;
        return this;
    }

    @DexIgnore
    public int b() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    @DexIgnore
    public nu1 b(MapMakerInternalMap.Strength strength) {
        st1.b(this.e == null, "Value strength was already set to %s", (Object) this.e);
        st1.a(strength);
        this.e = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }

    @DexIgnore
    public int a() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    @DexIgnore
    public nu1 a(MapMakerInternalMap.Strength strength) {
        st1.b(this.d == null, "Key strength was already set to %s", (Object) this.d);
        st1.a(strength);
        this.d = strength;
        if (strength != MapMakerInternalMap.Strength.STRONG) {
            this.a = true;
        }
        return this;
    }
}
