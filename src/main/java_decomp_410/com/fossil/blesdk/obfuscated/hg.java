package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;
import android.util.Pair;
import java.io.File;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hg {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public void a(gg ggVar) {
        }

        @DexIgnore
        public abstract void a(gg ggVar, int i, int i2);

        @DexIgnore
        public final void a(String str) {
            if (!str.equalsIgnoreCase(":memory:") && str.trim().length() != 0) {
                Log.w("SupportSQLite", "deleting the database file: " + str);
                try {
                    if (Build.VERSION.SDK_INT >= 16) {
                        SQLiteDatabase.deleteDatabase(new File(str));
                        return;
                    }
                    try {
                        if (!new File(str).delete()) {
                            Log.e("SupportSQLite", "Could not delete the database file " + str);
                        }
                    } catch (Exception e) {
                        Log.e("SupportSQLite", "error while deleting corrupted database file", e);
                    }
                } catch (Exception e2) {
                    Log.w("SupportSQLite", "delete failed: ", e2);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
            if (r0 != null) goto L_0x0036;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
            r3 = r0.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
            if (r3.hasNext() != false) goto L_0x0040;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
            a((java.lang.String) r3.next().second);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x004e, code lost:
            a(r3.w());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
            throw r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x002e, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0030 */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0071  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x002e A[ExcHandler: all (r1v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r0 
  PHI: (r0v10 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) = (r0v3 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v4 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v4 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) binds: [B:5:0x0029, B:8:0x0030, B:9:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0029] */
        @DexIgnore
        public void b(gg ggVar) {
            Log.e("SupportSQLite", "Corruption reported by sqlite on database: " + ggVar.w());
            if (!ggVar.isOpen()) {
                a(ggVar.w());
                return;
            }
            List<Pair<String, String>> list = null;
            try {
                list = ggVar.t();
                ggVar.close();
            } catch (IOException unused) {
            } catch (Throwable th) {
            }
            if (list == null) {
                for (Pair<String, String> pair : list) {
                    a((String) pair.second);
                }
                return;
            }
            a(ggVar.w());
        }

        @DexIgnore
        public abstract void b(gg ggVar, int i, int i2);

        @DexIgnore
        public abstract void c(gg ggVar);

        @DexIgnore
        public void d(gg ggVar) {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        hg a(b bVar);
    }

    @DexIgnore
    gg a();

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void close();

    @DexIgnore
    String getDatabaseName();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ a c;

        @DexIgnore
        public b(Context context, String str, a aVar) {
            this.a = context;
            this.b = str;
            this.c = aVar;
        }

        @DexIgnore
        public static a a(Context context) {
            return new a(context);
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a {
            @DexIgnore
            public Context a;
            @DexIgnore
            public String b;
            @DexIgnore
            public a c;

            @DexIgnore
            public a(Context context) {
                this.a = context;
            }

            @DexIgnore
            public b a() {
                a aVar = this.c;
                if (aVar != null) {
                    Context context = this.a;
                    if (context != null) {
                        return new b(context, this.b, aVar);
                    }
                    throw new IllegalArgumentException("Must set a non-null context to create the configuration.");
                }
                throw new IllegalArgumentException("Must set a callback to create the configuration.");
            }

            @DexIgnore
            public a a(String str) {
                this.b = str;
                return this;
            }

            @DexIgnore
            public a a(a aVar) {
                this.c = aVar;
                return this;
            }
        }
    }
}
