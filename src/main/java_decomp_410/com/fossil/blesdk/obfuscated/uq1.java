package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uq1 {
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_dark; // = 2131099652;
    @DexIgnore
    public static /* final */ int abc_background_cache_hint_selector_material_light; // = 2131099653;
    @DexIgnore
    public static /* final */ int abc_btn_colored_borderless_text_material; // = 2131099654;
    @DexIgnore
    public static /* final */ int abc_btn_colored_text_material; // = 2131099655;
    @DexIgnore
    public static /* final */ int abc_color_highlight_material; // = 2131099656;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_dark; // = 2131099657;
    @DexIgnore
    public static /* final */ int abc_hint_foreground_material_light; // = 2131099658;
    @DexIgnore
    public static /* final */ int abc_input_method_navigation_guard; // = 2131099659;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_dark; // = 2131099660;
    @DexIgnore
    public static /* final */ int abc_primary_text_disable_only_material_light; // = 2131099661;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_dark; // = 2131099662;
    @DexIgnore
    public static /* final */ int abc_primary_text_material_light; // = 2131099663;
    @DexIgnore
    public static /* final */ int abc_search_url_text; // = 2131099664;
    @DexIgnore
    public static /* final */ int abc_search_url_text_normal; // = 2131099665;
    @DexIgnore
    public static /* final */ int abc_search_url_text_pressed; // = 2131099666;
    @DexIgnore
    public static /* final */ int abc_search_url_text_selected; // = 2131099667;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_dark; // = 2131099668;
    @DexIgnore
    public static /* final */ int abc_secondary_text_material_light; // = 2131099669;
    @DexIgnore
    public static /* final */ int abc_tint_btn_checkable; // = 2131099670;
    @DexIgnore
    public static /* final */ int abc_tint_default; // = 2131099671;
    @DexIgnore
    public static /* final */ int abc_tint_edittext; // = 2131099672;
    @DexIgnore
    public static /* final */ int abc_tint_seek_thumb; // = 2131099673;
    @DexIgnore
    public static /* final */ int abc_tint_spinner; // = 2131099674;
    @DexIgnore
    public static /* final */ int abc_tint_switch_track; // = 2131099675;
    @DexIgnore
    public static /* final */ int accent_material_dark; // = 2131099677;
    @DexIgnore
    public static /* final */ int accent_material_light; // = 2131099678;
    @DexIgnore
    public static /* final */ int background_floating_material_dark; // = 2131099693;
    @DexIgnore
    public static /* final */ int background_floating_material_light; // = 2131099694;
    @DexIgnore
    public static /* final */ int background_material_dark; // = 2131099695;
    @DexIgnore
    public static /* final */ int background_material_light; // = 2131099696;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_dark; // = 2131099705;
    @DexIgnore
    public static /* final */ int bright_foreground_disabled_material_light; // = 2131099706;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_dark; // = 2131099707;
    @DexIgnore
    public static /* final */ int bright_foreground_inverse_material_light; // = 2131099708;
    @DexIgnore
    public static /* final */ int bright_foreground_material_dark; // = 2131099709;
    @DexIgnore
    public static /* final */ int bright_foreground_material_light; // = 2131099710;
    @DexIgnore
    public static /* final */ int button_material_dark; // = 2131099716;
    @DexIgnore
    public static /* final */ int button_material_light; // = 2131099717;
    @DexIgnore
    public static /* final */ int cardview_dark_background; // = 2131099718;
    @DexIgnore
    public static /* final */ int cardview_light_background; // = 2131099719;
    @DexIgnore
    public static /* final */ int cardview_shadow_end_color; // = 2131099720;
    @DexIgnore
    public static /* final */ int cardview_shadow_start_color; // = 2131099721;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_shadow_color; // = 2131099766;
    @DexIgnore
    public static /* final */ int design_default_color_primary; // = 2131099767;
    @DexIgnore
    public static /* final */ int design_default_color_primary_dark; // = 2131099768;
    @DexIgnore
    public static /* final */ int design_error; // = 2131099769;
    @DexIgnore
    public static /* final */ int design_fab_shadow_end_color; // = 2131099770;
    @DexIgnore
    public static /* final */ int design_fab_shadow_mid_color; // = 2131099771;
    @DexIgnore
    public static /* final */ int design_fab_shadow_start_color; // = 2131099772;
    @DexIgnore
    public static /* final */ int design_fab_stroke_end_inner_color; // = 2131099773;
    @DexIgnore
    public static /* final */ int design_fab_stroke_end_outer_color; // = 2131099774;
    @DexIgnore
    public static /* final */ int design_fab_stroke_top_inner_color; // = 2131099775;
    @DexIgnore
    public static /* final */ int design_fab_stroke_top_outer_color; // = 2131099776;
    @DexIgnore
    public static /* final */ int design_snackbar_background_color; // = 2131099777;
    @DexIgnore
    public static /* final */ int design_tint_password_toggle; // = 2131099778;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_dark; // = 2131099792;
    @DexIgnore
    public static /* final */ int dim_foreground_disabled_material_light; // = 2131099793;
    @DexIgnore
    public static /* final */ int dim_foreground_material_dark; // = 2131099794;
    @DexIgnore
    public static /* final */ int dim_foreground_material_light; // = 2131099795;
    @DexIgnore
    public static /* final */ int error_color_material_dark; // = 2131099802;
    @DexIgnore
    public static /* final */ int error_color_material_light; // = 2131099803;
    @DexIgnore
    public static /* final */ int foreground_material_dark; // = 2131099805;
    @DexIgnore
    public static /* final */ int foreground_material_light; // = 2131099806;
    @DexIgnore
    public static /* final */ int highlighted_text_material_dark; // = 2131099851;
    @DexIgnore
    public static /* final */ int highlighted_text_material_light; // = 2131099852;
    @DexIgnore
    public static /* final */ int material_blue_grey_800; // = 2131099865;
    @DexIgnore
    public static /* final */ int material_blue_grey_900; // = 2131099866;
    @DexIgnore
    public static /* final */ int material_blue_grey_950; // = 2131099867;
    @DexIgnore
    public static /* final */ int material_deep_teal_200; // = 2131099868;
    @DexIgnore
    public static /* final */ int material_deep_teal_500; // = 2131099869;
    @DexIgnore
    public static /* final */ int material_grey_100; // = 2131099870;
    @DexIgnore
    public static /* final */ int material_grey_300; // = 2131099871;
    @DexIgnore
    public static /* final */ int material_grey_50; // = 2131099872;
    @DexIgnore
    public static /* final */ int material_grey_600; // = 2131099873;
    @DexIgnore
    public static /* final */ int material_grey_800; // = 2131099874;
    @DexIgnore
    public static /* final */ int material_grey_850; // = 2131099875;
    @DexIgnore
    public static /* final */ int material_grey_900; // = 2131099876;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_colored_item_tint; // = 2131099878;
    @DexIgnore
    public static /* final */ int mtrl_bottom_nav_item_tint; // = 2131099879;
    @DexIgnore
    public static /* final */ int mtrl_btn_bg_color_disabled; // = 2131099880;
    @DexIgnore
    public static /* final */ int mtrl_btn_bg_color_selector; // = 2131099881;
    @DexIgnore
    public static /* final */ int mtrl_btn_ripple_color; // = 2131099882;
    @DexIgnore
    public static /* final */ int mtrl_btn_stroke_color_selector; // = 2131099883;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_ripple_color; // = 2131099884;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_color_disabled; // = 2131099885;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_color_selector; // = 2131099886;
    @DexIgnore
    public static /* final */ int mtrl_btn_transparent_bg_color; // = 2131099887;
    @DexIgnore
    public static /* final */ int mtrl_chip_background_color; // = 2131099888;
    @DexIgnore
    public static /* final */ int mtrl_chip_close_icon_tint; // = 2131099889;
    @DexIgnore
    public static /* final */ int mtrl_chip_ripple_color; // = 2131099890;
    @DexIgnore
    public static /* final */ int mtrl_chip_text_color; // = 2131099891;
    @DexIgnore
    public static /* final */ int mtrl_fab_ripple_color; // = 2131099892;
    @DexIgnore
    public static /* final */ int mtrl_scrim_color; // = 2131099893;
    @DexIgnore
    public static /* final */ int mtrl_tabs_colored_ripple_color; // = 2131099894;
    @DexIgnore
    public static /* final */ int mtrl_tabs_icon_color_selector; // = 2131099895;
    @DexIgnore
    public static /* final */ int mtrl_tabs_icon_color_selector_colored; // = 2131099896;
    @DexIgnore
    public static /* final */ int mtrl_tabs_legacy_text_color_selector; // = 2131099897;
    @DexIgnore
    public static /* final */ int mtrl_tabs_ripple_color; // = 2131099898;
    @DexIgnore
    public static /* final */ int mtrl_text_btn_text_color_selector; // = 2131099899;
    @DexIgnore
    public static /* final */ int mtrl_textinput_default_box_stroke_color; // = 2131099900;
    @DexIgnore
    public static /* final */ int mtrl_textinput_disabled_color; // = 2131099901;
    @DexIgnore
    public static /* final */ int mtrl_textinput_filled_box_default_background_color; // = 2131099902;
    @DexIgnore
    public static /* final */ int mtrl_textinput_hovered_box_stroke_color; // = 2131099903;
    @DexIgnore
    public static /* final */ int notification_action_color_filter; // = 2131099908;
    @DexIgnore
    public static /* final */ int notification_icon_bg_color; // = 2131099909;
    @DexIgnore
    public static /* final */ int primary_dark_material_dark; // = 2131099944;
    @DexIgnore
    public static /* final */ int primary_dark_material_light; // = 2131099945;
    @DexIgnore
    public static /* final */ int primary_material_dark; // = 2131099946;
    @DexIgnore
    public static /* final */ int primary_material_light; // = 2131099947;
    @DexIgnore
    public static /* final */ int primary_text_default_material_dark; // = 2131099948;
    @DexIgnore
    public static /* final */ int primary_text_default_material_light; // = 2131099949;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_dark; // = 2131099950;
    @DexIgnore
    public static /* final */ int primary_text_disabled_material_light; // = 2131099951;
    @DexIgnore
    public static /* final */ int ripple_material_dark; // = 2131100304;
    @DexIgnore
    public static /* final */ int ripple_material_light; // = 2131100305;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_dark; // = 2131100307;
    @DexIgnore
    public static /* final */ int secondary_text_default_material_light; // = 2131100308;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_dark; // = 2131100309;
    @DexIgnore
    public static /* final */ int secondary_text_disabled_material_light; // = 2131100310;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_dark; // = 2131100324;
    @DexIgnore
    public static /* final */ int switch_thumb_disabled_material_light; // = 2131100325;
    @DexIgnore
    public static /* final */ int switch_thumb_material_dark; // = 2131100326;
    @DexIgnore
    public static /* final */ int switch_thumb_material_light; // = 2131100327;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_dark; // = 2131100328;
    @DexIgnore
    public static /* final */ int switch_thumb_normal_material_light; // = 2131100329;
    @DexIgnore
    public static /* final */ int tooltip_background_dark; // = 2131100330;
    @DexIgnore
    public static /* final */ int tooltip_background_light; // = 2131100331;
}
