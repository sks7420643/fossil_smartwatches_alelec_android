package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xq */
public class C3304xq implements com.fossil.blesdk.obfuscated.C2981tq {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1574cr f11045a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.io.File f11046b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ long f11047c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3130vq f11048d; // = new com.fossil.blesdk.obfuscated.C3130vq();

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3446zn f11049e;

    @DexIgnore
    @java.lang.Deprecated
    public C3304xq(java.io.File file, long j) {
        this.f11046b = file;
        this.f11047c = j;
        this.f11045a = new com.fossil.blesdk.obfuscated.C1574cr();
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2981tq m16474a(java.io.File file, long j) {
        return new com.fossil.blesdk.obfuscated.C3304xq(file, j);
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized com.fossil.blesdk.obfuscated.C3446zn mo17852a() throws java.io.IOException {
        if (this.f11049e == null) {
            this.f11049e = com.fossil.blesdk.obfuscated.C3446zn.m17419a(this.f11046b, 1, 1, this.f11047c);
        }
        return this.f11049e;
    }

    @DexIgnore
    /* renamed from: a */
    public java.io.File mo16535a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        java.lang.String b = this.f11045a.mo9618b(joVar);
        if (android.util.Log.isLoggable("DiskLruCacheWrapper", 2)) {
            android.util.Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b + " for for Key: " + joVar);
        }
        try {
            com.fossil.blesdk.obfuscated.C3446zn.C3451e f = mo17852a().mo18526f(b);
            if (f != null) {
                return f.mo18542a(0);
            }
            return null;
        } catch (java.io.IOException e) {
            if (!android.util.Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            android.util.Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16536a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2981tq.C2983b bVar) {
        com.fossil.blesdk.obfuscated.C3446zn.C3449c e;
        java.lang.String b = this.f11045a.mo9618b(joVar);
        this.f11048d.mo17158a(b);
        try {
            if (android.util.Log.isLoggable("DiskLruCacheWrapper", 2)) {
                android.util.Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b + " for for Key: " + joVar);
            }
            try {
                com.fossil.blesdk.obfuscated.C3446zn a = mo17852a();
                if (a.mo18526f(b) == null) {
                    e = a.mo18525e(b);
                    if (e != null) {
                        if (bVar.mo13328a(e.mo18533a(0))) {
                            e.mo18536c();
                        }
                        e.mo18535b();
                        this.f11048d.mo17159b(b);
                        return;
                    }
                    throw new java.lang.IllegalStateException("Had two simultaneous puts for: " + b);
                }
            } catch (java.io.IOException e2) {
                if (android.util.Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    android.util.Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e2);
                }
            } catch (Throwable th) {
                e.mo18535b();
                throw th;
            }
        } finally {
            this.f11048d.mo17159b(b);
        }
    }
}
