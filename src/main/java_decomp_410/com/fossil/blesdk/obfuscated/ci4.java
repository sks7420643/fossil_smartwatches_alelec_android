package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ci4 extends bg4 {
    @DexIgnore
    public /* final */ xc4<Throwable, qa4> e;

    @DexIgnore
    public ci4(xc4<? super Throwable, qa4> xc4) {
        kd4.b(xc4, "handler");
        this.e = xc4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.invoke(th);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + dh4.a((Object) this.e) + '@' + dh4.b(this) + ']';
    }
}
