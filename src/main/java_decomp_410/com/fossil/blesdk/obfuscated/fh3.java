package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fh3 extends zr2 implements eh3 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public tr3<if2> j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final fh3 a() {
            return new fh3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fh3 e;

        @DexIgnore
        public b(fh3 fh3) {
            this.e = fh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            pd4 pd4 = pd4.a;
            Locale a = sm2.a();
            kd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/shop_battery", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Purchase Battery URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fh3 e;

        @DexIgnore
        public c(fh3 fh3) {
            this.e = fh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            pd4 pd4 = pd4.a;
            Locale a = sm2.a();
            kd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/low_battery", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Need Help URL = " + format);
            this.e.U(format);
        }
    }

    /*
    static {
        String simpleName = fh3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "ReplaceBatteryFragment::class.java.simpleName!!");
            l = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void U(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), l);
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        if2 if2 = (if2) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_replace_battery, (ViewGroup) null, false, O0());
        this.j = new tr3<>(this, if2);
        kd4.a((Object) if2, "binding");
        return if2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<if2> tr3 = this.j;
        if (tr3 != null) {
            if2 a2 = tr3.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new b(this));
                a2.q.setOnClickListener(new c(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(dh3 dh3) {
        kd4.b(dh3, "presenter");
        st1.a(dh3);
        kd4.a((Object) dh3, "Preconditions.checkNotNull(presenter)");
        dh3 dh32 = dh3;
    }
}
