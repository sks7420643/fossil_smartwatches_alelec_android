package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.ew;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dw<R> implements ew<R> {
    @DexIgnore
    public static /* final */ dw<?> a; // = new dw<>();
    @DexIgnore
    public static /* final */ fw<?> b; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements fw<R> {
        @DexIgnore
        public ew<R> a(DataSource dataSource, boolean z) {
            return dw.a;
        }
    }

    @DexIgnore
    public static <R> fw<R> a() {
        return b;
    }

    @DexIgnore
    public boolean a(Object obj, ew.a aVar) {
        return false;
    }
}
