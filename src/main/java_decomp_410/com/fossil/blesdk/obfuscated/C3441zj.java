package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zj */
public class C3441zj implements com.fossil.blesdk.obfuscated.C1953hk, com.fossil.blesdk.obfuscated.C2419mj, com.fossil.blesdk.obfuscated.C1560ck.C1562b {

    @DexIgnore
    /* renamed from: n */
    public static /* final */ java.lang.String f11565n; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("DelayMetCommandHandler");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f11566e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f11567f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.String f11568g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C1421ak f11569h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2035ik f11570i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.lang.Object f11571j; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: k */
    public int f11572k; // = 0;

    @DexIgnore
    /* renamed from: l */
    public android.os.PowerManager.WakeLock f11573l;

    @DexIgnore
    /* renamed from: m */
    public boolean f11574m; // = false;

    @DexIgnore
    public C3441zj(android.content.Context context, int i, java.lang.String str, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        this.f11566e = context;
        this.f11567f = i;
        this.f11569h = akVar;
        this.f11568g = str;
        this.f11570i = new com.fossil.blesdk.obfuscated.C2035ik(this.f11566e, akVar.mo8768d(), this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3804a(java.lang.String str, boolean z) {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("onExecuted %s, %s", new java.lang.Object[]{str, java.lang.Boolean.valueOf(z)}), new java.lang.Throwable[0]);
        mo18504a();
        if (z) {
            android.content.Intent b = com.fossil.blesdk.obfuscated.C3293xj.m16364b(this.f11566e, this.f11568g);
            com.fossil.blesdk.obfuscated.C1421ak akVar = this.f11569h;
            akVar.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(akVar, b, this.f11567f));
        }
        if (this.f11574m) {
            android.content.Intent a = com.fossil.blesdk.obfuscated.C3293xj.m16359a(this.f11566e);
            com.fossil.blesdk.obfuscated.C1421ak akVar2 = this.f11569h;
            akVar2.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(akVar2, a, this.f11567f));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo3845b(java.util.List<java.lang.String> list) {
        if (list.contains(this.f11568g)) {
            synchronized (this.f11571j) {
                if (this.f11572k == 0) {
                    this.f11572k = 1;
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("onAllConstraintsMet for %s", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
                    if (this.f11569h.mo8767c().mo14429c(this.f11568g)) {
                        this.f11569h.mo8770f().mo9547a(this.f11568g, 600000, this);
                    } else {
                        mo18504a();
                    }
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Already started work for %s", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo18506c() {
        synchronized (this.f11571j) {
            if (this.f11572k < 2) {
                this.f11572k = 2;
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Stopping work for WorkSpec %s", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
                this.f11569h.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(this.f11569h, com.fossil.blesdk.obfuscated.C3293xj.m16365c(this.f11566e, this.f11568g), this.f11567f));
                if (this.f11569h.mo8767c().mo14428b(this.f11568g)) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("WorkSpec %s needs to be rescheduled", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
                    this.f11569h.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(this.f11569h, com.fossil.blesdk.obfuscated.C3293xj.m16364b(this.f11566e, this.f11568g), this.f11567f));
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Processor does not have WorkSpec %s. No need to reschedule ", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
                }
            } else {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Already stopped work for %s", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9549a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Exceeded time limits on execution for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
        mo18506c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3844a(java.util.List<java.lang.String> list) {
        mo18506c();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18505b() {
        this.f11573l = com.fossil.blesdk.obfuscated.C3228wl.m15862a(this.f11566e, java.lang.String.format("%s (%s)", new java.lang.Object[]{this.f11568g, java.lang.Integer.valueOf(this.f11567f)}));
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Acquiring wakelock %s for WorkSpec %s", new java.lang.Object[]{this.f11573l, this.f11568g}), new java.lang.Throwable[0]);
        this.f11573l.acquire();
        com.fossil.blesdk.obfuscated.C1954hl e = this.f11569h.mo8769e().mo16462g().mo3780d().mo12029e(this.f11568g);
        if (e == null) {
            mo18506c();
            return;
        }
        this.f11574m = e.mo11671b();
        if (!this.f11574m) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("No constraints for %s", new java.lang.Object[]{this.f11568g}), new java.lang.Throwable[0]);
            mo3845b(java.util.Collections.singletonList(this.f11568g));
            return;
        }
        this.f11570i.mo12013c(java.util.Collections.singletonList(e));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo18504a() {
        synchronized (this.f11571j) {
            this.f11570i.mo12009a();
            this.f11569h.mo8770f().mo9546a(this.f11568g);
            if (this.f11573l != null && this.f11573l.isHeld()) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11565n, java.lang.String.format("Releasing wakelock %s for WorkSpec %s", new java.lang.Object[]{this.f11573l, this.f11568g}), new java.lang.Throwable[0]);
                this.f11573l.release();
            }
        }
    }
}
