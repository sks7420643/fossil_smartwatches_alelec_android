package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r71 implements Comparator<zzte> {
    @DexIgnore
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        zzte zzte = (zzte) obj;
        zzte zzte2 = (zzte) obj2;
        v71 v71 = (v71) zzte.iterator();
        v71 v712 = (v71) zzte2.iterator();
        while (v71.hasNext() && v712.hasNext()) {
            int compare = Integer.compare(zzte.a(v71.nextByte()), zzte.a(v712.nextByte()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(zzte.size(), zzte2.size());
    }
}
