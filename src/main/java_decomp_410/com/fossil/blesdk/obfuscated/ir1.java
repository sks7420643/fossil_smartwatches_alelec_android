package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.util.Property;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ir1 extends Property<ImageView, Matrix> {
    @DexIgnore
    public /* final */ Matrix a; // = new Matrix();

    @DexIgnore
    public ir1() {
        super(Matrix.class, "imageMatrixProperty");
    }

    @DexIgnore
    /* renamed from: a */
    public void set(ImageView imageView, Matrix matrix) {
        imageView.setImageMatrix(matrix);
    }

    @DexIgnore
    /* renamed from: a */
    public Matrix get(ImageView imageView) {
        this.a.set(imageView.getImageMatrix());
        return this.a;
    }
}
