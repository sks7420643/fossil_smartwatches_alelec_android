package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cq */
public final class C1573cq implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ com.fossil.blesdk.obfuscated.C2772qw<java.lang.Class<?>, byte[]> f4173j; // = new com.fossil.blesdk.obfuscated.C2772qw<>(50);

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f4174b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f4175c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f4176d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f4177e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f4178f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.Class<?> f4179g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2337lo f4180h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2581oo<?> f4181i;

    @DexIgnore
    public C1573cq(com.fossil.blesdk.obfuscated.C1885gq gqVar, com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2143jo joVar2, int i, int i2, com.fossil.blesdk.obfuscated.C2581oo<?> ooVar, java.lang.Class<?> cls, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        this.f4174b = gqVar;
        this.f4175c = joVar;
        this.f4176d = joVar2;
        this.f4177e = i;
        this.f4178f = i2;
        this.f4181i = ooVar;
        this.f4179g = cls;
        this.f4180h = loVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.f4174b.mo11282a(8, byte[].class);
        java.nio.ByteBuffer.wrap(bArr).putInt(this.f4177e).putInt(this.f4178f).array();
        this.f4176d.mo8934a(messageDigest);
        this.f4175c.mo8934a(messageDigest);
        messageDigest.update(bArr);
        com.fossil.blesdk.obfuscated.C2581oo<?> ooVar = this.f4181i;
        if (ooVar != null) {
            ooVar.mo8934a(messageDigest);
        }
        this.f4180h.mo8934a(messageDigest);
        messageDigest.update(mo9609a());
        this.f4174b.put(bArr);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1573cq)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1573cq cqVar = (com.fossil.blesdk.obfuscated.C1573cq) obj;
        if (this.f4178f != cqVar.f4178f || this.f4177e != cqVar.f4177e || !com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f4181i, (java.lang.Object) cqVar.f4181i) || !this.f4179g.equals(cqVar.f4179g) || !this.f4175c.equals(cqVar.f4175c) || !this.f4176d.equals(cqVar.f4176d) || !this.f4180h.equals(cqVar.f4180h)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((this.f4175c.hashCode() * 31) + this.f4176d.hashCode()) * 31) + this.f4177e) * 31) + this.f4178f;
        com.fossil.blesdk.obfuscated.C2581oo<?> ooVar = this.f4181i;
        if (ooVar != null) {
            hashCode = (hashCode * 31) + ooVar.hashCode();
        }
        return (((hashCode * 31) + this.f4179g.hashCode()) * 31) + this.f4180h.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "ResourceCacheKey{sourceKey=" + this.f4175c + ", signature=" + this.f4176d + ", width=" + this.f4177e + ", height=" + this.f4178f + ", decodedResourceClass=" + this.f4179g + ", transformation='" + this.f4181i + '\'' + ", options=" + this.f4180h + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public final byte[] mo9609a() {
        byte[] a = f4173j.mo15388a(this.f4179g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.f4179g.getName().getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a);
        f4173j.mo15393b(this.f4179g, bytes);
        return bytes;
    }
}
