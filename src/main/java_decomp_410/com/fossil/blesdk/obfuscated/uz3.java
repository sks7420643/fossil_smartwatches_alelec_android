package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uz3 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public byte[] d;
    @DexIgnore
    public b e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static Bundle a(uz3 uz3) {
            Bundle bundle = new Bundle();
            bundle.putInt("_wxobject_sdkVer", uz3.a);
            bundle.putString("_wxobject_title", uz3.b);
            bundle.putString("_wxobject_description", uz3.c);
            bundle.putByteArray("_wxobject_thumbdata", uz3.d);
            b bVar = uz3.e;
            if (bVar != null) {
                bundle.putString("_wxobject_identifier_", a(bVar.getClass().getName()));
                uz3.e.b(bundle);
            }
            bundle.putString("_wxobject_mediatagname", uz3.f);
            bundle.putString("_wxobject_message_action", uz3.g);
            bundle.putString("_wxobject_message_ext", uz3.h);
            return bundle;
        }

        @DexIgnore
        public static uz3 a(Bundle bundle) {
            uz3 uz3 = new uz3();
            uz3.a = bundle.getInt("_wxobject_sdkVer");
            uz3.b = bundle.getString("_wxobject_title");
            uz3.c = bundle.getString("_wxobject_description");
            uz3.d = bundle.getByteArray("_wxobject_thumbdata");
            uz3.f = bundle.getString("_wxobject_mediatagname");
            uz3.g = bundle.getString("_wxobject_message_action");
            uz3.h = bundle.getString("_wxobject_message_ext");
            String b = b(bundle.getString("_wxobject_identifier_"));
            if (b != null && b.length() > 0) {
                try {
                    uz3.e = (b) Class.forName(b).newInstance();
                    uz3.e.a(bundle);
                    return uz3;
                } catch (Exception e) {
                    e.printStackTrace();
                    cz3.a("MicroMsg.SDK.WXMediaMessage", "get media object from bundle failed: unknown ident " + b + ", ex = " + e.getMessage());
                }
            }
            return uz3;
        }

        @DexIgnore
        public static String a(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
            }
            cz3.a("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
            return str;
        }

        @DexIgnore
        public static String b(String str) {
            if (str != null && str.length() != 0) {
                return str.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
            }
            cz3.a("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
            return str;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        int a();

        @DexIgnore
        void a(Bundle bundle);

        @DexIgnore
        void b(Bundle bundle);

        @DexIgnore
        boolean b();
    }

    @DexIgnore
    public uz3() {
        this((b) null);
    }

    @DexIgnore
    public uz3(b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public final boolean a() {
        String str;
        if (b() == 8) {
            byte[] bArr = this.d;
            if (bArr == null || bArr.length == 0) {
                str = "checkArgs fail, thumbData should not be null when send emoji";
                cz3.a("MicroMsg.SDK.WXMediaMessage", str);
                return false;
            }
        }
        byte[] bArr2 = this.d;
        if (bArr2 == null || bArr2.length <= 32768) {
            String str2 = this.b;
            if (str2 == null || str2.length() <= 512) {
                String str3 = this.c;
                if (str3 == null || str3.length() <= 1024) {
                    if (this.e == null) {
                        str = "checkArgs fail, mediaObject is null";
                    } else {
                        String str4 = this.f;
                        if (str4 == null || str4.length() <= 64) {
                            String str5 = this.g;
                            if (str5 == null || str5.length() <= 2048) {
                                String str6 = this.h;
                                if (str6 == null || str6.length() <= 2048) {
                                    return this.e.b();
                                }
                                str = "checkArgs fail, messageExt is too long";
                            } else {
                                str = "checkArgs fail, messageAction is too long";
                            }
                        } else {
                            str = "checkArgs fail, mediaTagName is too long";
                        }
                    }
                    cz3.a("MicroMsg.SDK.WXMediaMessage", str);
                    return false;
                }
                str = "checkArgs fail, description is invalid";
                cz3.a("MicroMsg.SDK.WXMediaMessage", str);
                return false;
            }
            str = "checkArgs fail, title is invalid";
            cz3.a("MicroMsg.SDK.WXMediaMessage", str);
            return false;
        }
        str = "checkArgs fail, thumbData is invalid";
        cz3.a("MicroMsg.SDK.WXMediaMessage", str);
        return false;
    }

    @DexIgnore
    public final int b() {
        b bVar = this.e;
        if (bVar == null) {
            return 0;
        }
        return bVar.a();
    }
}
