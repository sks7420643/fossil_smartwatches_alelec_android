package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ck */
public class C1560ck {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f4113f; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkTimer");

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.ThreadFactory f4114a; // = new com.fossil.blesdk.obfuscated.C1560ck.C1561a(this);

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.concurrent.ScheduledExecutorService f4115b; // = java.util.concurrent.Executors.newSingleThreadScheduledExecutor(this.f4114a);

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1560ck.C1563c> f4116c; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1560ck.C1562b> f4117d; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.Object f4118e; // = new java.lang.Object();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ck$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ck$a */
    public class C1561a implements java.util.concurrent.ThreadFactory {

        @DexIgnore
        /* renamed from: a */
        public int f4119a; // = 0;

        @DexIgnore
        public C1561a(com.fossil.blesdk.obfuscated.C1560ck ckVar) {
        }

        @DexIgnore
        public java.lang.Thread newThread(java.lang.Runnable runnable) {
            java.lang.Thread newThread = java.util.concurrent.Executors.defaultThreadFactory().newThread(runnable);
            newThread.setName("WorkManager-WorkTimer-thread-" + this.f4119a);
            this.f4119a = this.f4119a + 1;
            return newThread;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ck$b */
    public interface C1562b {
        @DexIgnore
        /* renamed from: a */
        void mo9549a(java.lang.String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ck$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ck$c */
    public static class C1563c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C1560ck f4120e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ java.lang.String f4121f;

        @DexIgnore
        public C1563c(com.fossil.blesdk.obfuscated.C1560ck ckVar, java.lang.String str) {
            this.f4120e = ckVar;
            this.f4121f = str;
        }

        @DexIgnore
        public void run() {
            synchronized (this.f4120e.f4118e) {
                if (this.f4120e.f4116c.remove(this.f4121f) != null) {
                    com.fossil.blesdk.obfuscated.C1560ck.C1562b remove = this.f4120e.f4117d.remove(this.f4121f);
                    if (remove != null) {
                        remove.mo9549a(this.f4121f);
                    }
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a("WrkTimerRunnable", java.lang.String.format("Timer with %s is already marked as complete.", new java.lang.Object[]{this.f4121f}), new java.lang.Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9547a(java.lang.String str, long j, com.fossil.blesdk.obfuscated.C1560ck.C1562b bVar) {
        synchronized (this.f4118e) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f4113f, java.lang.String.format("Starting timer for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            mo9546a(str);
            com.fossil.blesdk.obfuscated.C1560ck.C1563c cVar = new com.fossil.blesdk.obfuscated.C1560ck.C1563c(this, str);
            this.f4116c.put(str, cVar);
            this.f4117d.put(str, bVar);
            this.f4115b.schedule(cVar, j, java.util.concurrent.TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9546a(java.lang.String str) {
        synchronized (this.f4118e) {
            if (this.f4116c.remove(str) != null) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f4113f, java.lang.String.format("Stopping timer for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
                this.f4117d.remove(str);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9545a() {
        if (!this.f4115b.isShutdown()) {
            this.f4115b.shutdownNow();
        }
    }
}
