package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kb3 {
    @DexIgnore
    public /* final */ fb3 a;
    @DexIgnore
    public /* final */ ub3 b;
    @DexIgnore
    public /* final */ pb3 c;

    @DexIgnore
    public kb3(fb3 fb3, ub3 ub3, pb3 pb3) {
        kd4.b(fb3, "mGoalTrackingOverviewDayView");
        kd4.b(ub3, "mGoalTrackingOverviewWeekView");
        kd4.b(pb3, "mGoalTrackingOverviewMonthView");
        this.a = fb3;
        this.b = ub3;
        this.c = pb3;
    }

    @DexIgnore
    public final fb3 a() {
        return this.a;
    }

    @DexIgnore
    public final pb3 b() {
        return this.c;
    }

    @DexIgnore
    public final ub3 c() {
        return this.b;
    }
}
