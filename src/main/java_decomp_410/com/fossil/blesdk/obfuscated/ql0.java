package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.blesdk.obfuscated.qj0;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ql0 extends qj0 implements Handler.Callback {
    @DexIgnore
    public /* final */ HashMap<qj0.a, rl0> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ dm0 f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public ql0(Context context) {
        this.d = context.getApplicationContext();
        this.e = new bz0(context.getMainLooper(), this);
        this.f = dm0.a();
        this.g = 5000;
        this.h = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
    }

    @DexIgnore
    public final boolean a(qj0.a aVar, ServiceConnection serviceConnection, String str) {
        boolean d2;
        bk0.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            rl0 rl0 = this.c.get(aVar);
            if (rl0 == null) {
                rl0 = new rl0(this, aVar);
                rl0.a(serviceConnection, str);
                rl0.a(str);
                this.c.put(aVar, rl0);
            } else {
                this.e.removeMessages(0, aVar);
                if (!rl0.a(serviceConnection)) {
                    rl0.a(serviceConnection, str);
                    int c2 = rl0.c();
                    if (c2 == 1) {
                        serviceConnection.onServiceConnected(rl0.b(), rl0.a());
                    } else if (c2 == 2) {
                        rl0.a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d2 = rl0.d();
        }
        return d2;
    }

    @DexIgnore
    public final void b(qj0.a aVar, ServiceConnection serviceConnection, String str) {
        bk0.a(serviceConnection, (Object) "ServiceConnection must not be null");
        synchronized (this.c) {
            rl0 rl0 = this.c.get(aVar);
            if (rl0 == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (rl0.a(serviceConnection)) {
                rl0.b(serviceConnection, str);
                if (rl0.e()) {
                    this.e.sendMessageDelayed(this.e.obtainMessage(0, aVar), this.g);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.c) {
                qj0.a aVar = (qj0.a) message.obj;
                rl0 rl0 = this.c.get(aVar);
                if (rl0 != null && rl0.e()) {
                    if (rl0.d()) {
                        rl0.b("GmsClientSupervisor");
                    }
                    this.c.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.c) {
                qj0.a aVar2 = (qj0.a) message.obj;
                rl0 rl02 = this.c.get(aVar2);
                if (rl02 != null && rl02.c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = rl02.b();
                    if (b == null) {
                        b = aVar2.a();
                    }
                    if (b == null) {
                        b = new ComponentName(aVar2.b(), "unknown");
                    }
                    rl02.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
