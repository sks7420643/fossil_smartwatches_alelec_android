package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import com.fossil.blesdk.obfuscated.h1;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v0 extends ActionMode implements h1.a {
    @DexIgnore
    public Context g;
    @DexIgnore
    public ActionBarContextView h;
    @DexIgnore
    public ActionMode.Callback i;
    @DexIgnore
    public WeakReference<View> j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public h1 l;

    @DexIgnore
    public v0(Context context, ActionBarContextView actionBarContextView, ActionMode.Callback callback, boolean z) {
        this.g = context;
        this.h = actionBarContextView;
        this.i = callback;
        h1 h1Var = new h1(actionBarContextView.getContext());
        h1Var.c(1);
        this.l = h1Var;
        this.l.a((h1.a) this);
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.h.setSubtitle(charSequence);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.h.setTitle(charSequence);
    }

    @DexIgnore
    public Menu c() {
        return this.l;
    }

    @DexIgnore
    public MenuInflater d() {
        return new x0(this.h.getContext());
    }

    @DexIgnore
    public CharSequence e() {
        return this.h.getSubtitle();
    }

    @DexIgnore
    public CharSequence g() {
        return this.h.getTitle();
    }

    @DexIgnore
    public void i() {
        this.i.b(this, this.l);
    }

    @DexIgnore
    public boolean j() {
        return this.h.c();
    }

    @DexIgnore
    public void a(int i2) {
        a((CharSequence) this.g.getString(i2));
    }

    @DexIgnore
    public void b(int i2) {
        b((CharSequence) this.g.getString(i2));
    }

    @DexIgnore
    public void a(boolean z) {
        super.a(z);
        this.h.setTitleOptional(z);
    }

    @DexIgnore
    public View b() {
        WeakReference<View> weakReference = this.j;
        if (weakReference != null) {
            return (View) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public void a(View view) {
        this.h.setCustomView(view);
        this.j = view != null ? new WeakReference<>(view) : null;
    }

    @DexIgnore
    public void a() {
        if (!this.k) {
            this.k = true;
            this.h.sendAccessibilityEvent(32);
            this.i.a(this);
        }
    }

    @DexIgnore
    public boolean a(h1 h1Var, MenuItem menuItem) {
        return this.i.a((ActionMode) this, menuItem);
    }

    @DexIgnore
    public void a(h1 h1Var) {
        i();
        this.h.e();
    }
}
