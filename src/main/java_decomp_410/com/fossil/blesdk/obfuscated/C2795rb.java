package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rb */
public interface C2795rb extends com.fossil.blesdk.obfuscated.C3179wb {
    @DexIgnore
    /* renamed from: a */
    void mo15476a(androidx.lifecycle.LifecycleOwner lifecycleOwner);

    @DexIgnore
    /* renamed from: b */
    void mo15477b(androidx.lifecycle.LifecycleOwner lifecycleOwner);

    @DexIgnore
    /* renamed from: c */
    void mo15478c(androidx.lifecycle.LifecycleOwner lifecycleOwner);

    @DexIgnore
    /* renamed from: d */
    void mo15479d(androidx.lifecycle.LifecycleOwner lifecycleOwner);

    @DexIgnore
    /* renamed from: e */
    void mo15480e(androidx.lifecycle.LifecycleOwner lifecycleOwner);

    @DexIgnore
    /* renamed from: f */
    void mo15481f(androidx.lifecycle.LifecycleOwner lifecycleOwner);
}
