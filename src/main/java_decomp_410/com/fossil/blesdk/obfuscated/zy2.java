package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.m62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zy2 extends as2 implements yy2 {
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<uc2> k;
    @DexIgnore
    public xy2 l;
    @DexIgnore
    public m62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final zy2 a() {
            return new zy2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m62.b {
        @DexIgnore
        public /* final */ /* synthetic */ zy2 a;

        @DexIgnore
        public b(zy2 zy2) {
            this.a = zy2;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            zy2.a(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            zy2.a(this.a).a(alarm);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zy2 e;

        @DexIgnore
        public c(zy2 zy2) {
            this.e = zy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zy2 e;

        @DexIgnore
        public d(zy2 zy2) {
            this.e = zy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            zy2.a(this.e).a((Alarm) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zy2 e;

        @DexIgnore
        public e(zy2 zy2) {
            this.e = zy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (bn2.a(bn2.d, this.e.getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null) && bn2.a(bn2.d, this.e.getContext(), "NOTIFICATION_APPS", false, 4, (Object) null)) {
                NotificationDialLandingActivity.C.a(this.e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zy2 e;

        @DexIgnore
        public f(zy2 zy2) {
            this.e = zy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            zy2.a(this.e).h();
        }
    }

    @DexIgnore
    public static final /* synthetic */ xy2 a(zy2 zy2) {
        xy2 xy2 = zy2.l;
        if (xy2 != null) {
            return xy2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void d(List<Alarm> list) {
        kd4.b(list, "alarms");
        m62 m62 = this.m;
        if (m62 != null) {
            m62.a(list);
        } else {
            kd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void i(boolean z) {
        Context context;
        int i;
        tr3<uc2> tr3 = this.k;
        if (tr3 != null) {
            uc2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.x;
                kd4.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                FlexibleTextView flexibleTextView = a2.t;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i = R.color.primaryText;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i = R.color.warmGrey;
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                flexibleTextView.setTextColor(k6.a(context, i));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        tr3<uc2> tr3 = this.k;
        if (tr3 != null) {
            uc2 a2 = tr3.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.v;
                if (linearLayout != null) {
                    kd4.a((Object) linearLayout, "it");
                    linearLayout.setAlpha(z ? 0.5f : 1.0f);
                    linearLayout.setClickable(!z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        uc2 uc2 = (uc2) qa.a(layoutInflater, R.layout.fragment_home_alerts_hybrid, viewGroup, false, O0());
        R("alert_view");
        uc2.r.setOnClickListener(new d(this));
        uc2.v.setOnClickListener(new e(this));
        uc2.x.setOnClickListener(new f(this));
        ui2 ui2 = uc2.u;
        if (ui2 != null) {
            ConstraintLayout constraintLayout = ui2.q;
            kd4.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            ui2.t.setImageResource(R.drawable.alerts_no_device);
            FlexibleTextView flexibleTextView = ui2.r;
            kd4.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(sm2.a(getContext(), (int) R.string.Onboarding_WithoutDevice_Alerts_Text__SetAlarmsAndGetNotificationUpdates));
            ui2.s.setOnClickListener(new c(this));
        }
        m62 m62 = new m62();
        m62.a((m62.b) new b(this));
        this.m = m62;
        RecyclerView recyclerView = uc2.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        m62 m622 = this.m;
        if (m622 != null) {
            recyclerView.setAdapter(m622);
            this.k = new tr3<>(this, uc2);
            tr3<uc2> tr3 = this.k;
            if (tr3 != null) {
                uc2 a2 = tr3.a();
                if (a2 != null) {
                    kd4.a((Object) a2, "mBinding.get()!!");
                    return a2.d();
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        xy2 xy2 = this.l;
        if (xy2 != null) {
            if (xy2 != null) {
                xy2.g();
                vl2 Q0 = Q0();
                if (Q0 != null) {
                    Q0.a("");
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        super.onPause();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        xy2 xy2 = this.l;
        if (xy2 == null) {
            return;
        }
        if (xy2 != null) {
            xy2.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void q(boolean z) {
        tr3<uc2> tr3 = this.k;
        if (tr3 != null) {
            uc2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.w;
                kd4.a((Object) recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "binding.ftvAlarmsSection");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.r;
                kd4.a((Object) flexibleTextView2, "binding.ftvAdd");
                flexibleTextView2.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.w;
            kd4.a((Object) recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            FlexibleTextView flexibleTextView3 = a2.s;
            kd4.a((Object) flexibleTextView3, "binding.ftvAlarmsSection");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = a2.r;
            kd4.a((Object) flexibleTextView4, "binding.ftvAdd");
            flexibleTextView4.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.y(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        m62 m62 = this.m;
        if (m62 != null) {
            m62.notifyDataSetChanged();
        } else {
            kd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(xy2 xy2) {
        kd4.b(xy2, "presenter");
        this.l = xy2;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        kd4.b(str, "deviceId");
        kd4.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            kd4.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        tr3<uc2> tr3 = this.k;
        if (tr3 != null) {
            uc2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
