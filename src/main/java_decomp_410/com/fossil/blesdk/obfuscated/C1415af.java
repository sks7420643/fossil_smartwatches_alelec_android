package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.af */
public abstract class C1415af extends androidx.recyclerview.widget.RecyclerView.C0230j {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ java.lang.String TAG; // = "SimpleItemAnimator";
    @DexIgnore
    public boolean mSupportsChangeAnimations; // = true;

    @DexIgnore
    public abstract boolean animateAdd(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder);

    @DexIgnore
    public boolean animateAppearance(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2) {
        if (cVar == null || (cVar.f1379a == cVar2.f1379a && cVar.f1380b == cVar2.f1380b)) {
            return animateAdd(viewHolder);
        }
        return animateMove(viewHolder, cVar.f1379a, cVar.f1380b, cVar2.f1379a, cVar2.f1380b);
    }

    @DexIgnore
    public abstract boolean animateChange(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4);

    @DexIgnore
    public boolean animateChange(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2) {
        int i;
        int i2;
        int i3 = cVar.f1379a;
        int i4 = cVar.f1380b;
        if (viewHolder2.shouldIgnore()) {
            int i5 = cVar.f1379a;
            i = cVar.f1380b;
            i2 = i5;
        } else {
            i2 = cVar2.f1379a;
            i = cVar2.f1380b;
        }
        return animateChange(viewHolder, viewHolder2, i3, i4, i2, i);
    }

    @DexIgnore
    public boolean animateDisappearance(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2) {
        int i = cVar.f1379a;
        int i2 = cVar.f1380b;
        android.view.View view = viewHolder.itemView;
        int left = cVar2 == null ? view.getLeft() : cVar2.f1379a;
        int top = cVar2 == null ? view.getTop() : cVar2.f1380b;
        if (viewHolder.isRemoved() || (i == left && i2 == top)) {
            return animateRemove(viewHolder);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return animateMove(viewHolder, i, i2, left, top);
    }

    @DexIgnore
    public abstract boolean animateMove(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4);

    @DexIgnore
    public boolean animatePersistence(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar, androidx.recyclerview.widget.RecyclerView.C0230j.C0233c cVar2) {
        if (cVar.f1379a == cVar2.f1379a && cVar.f1380b == cVar2.f1380b) {
            dispatchMoveFinished(viewHolder);
            return false;
        }
        return animateMove(viewHolder, cVar.f1379a, cVar.f1380b, cVar2.f1379a, cVar2.f1380b);
    }

    @DexIgnore
    public abstract boolean animateRemove(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder);

    @DexIgnore
    public boolean canReuseUpdatedViewHolder(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        return !this.mSupportsChangeAnimations || viewHolder.isInvalid();
    }

    @DexIgnore
    public final void dispatchAddFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onAddFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchAddStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onAddStarting(viewHolder);
    }

    @DexIgnore
    public final void dispatchChangeFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, boolean z) {
        onChangeFinished(viewHolder, z);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchChangeStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, boolean z) {
        onChangeStarting(viewHolder, z);
    }

    @DexIgnore
    public final void dispatchMoveFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onMoveFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchMoveStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onMoveStarting(viewHolder);
    }

    @DexIgnore
    public final void dispatchRemoveFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onRemoveFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchRemoveStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        onRemoveStarting(viewHolder);
    }

    @DexIgnore
    public boolean getSupportsChangeAnimations() {
        return this.mSupportsChangeAnimations;
    }

    @DexIgnore
    public void onAddFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onAddStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onChangeFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    @DexIgnore
    public void onChangeStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    @DexIgnore
    public void onMoveFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onMoveStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onRemoveFinished(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onRemoveStarting(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void setSupportsChangeAnimations(boolean z) {
        this.mSupportsChangeAnimations = z;
    }
}
