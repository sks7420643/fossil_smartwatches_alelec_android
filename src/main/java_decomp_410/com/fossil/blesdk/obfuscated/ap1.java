package com.fossil.blesdk.obfuscated;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ap1 extends fe0<Object> {

    @DexIgnore
    public interface a extends Parcelable {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public void a(a aVar) {
            throw null;
        }

        @DexIgnore
        public void a(a aVar, int i, int i2) {
            throw null;
        }

        @DexIgnore
        public void b(a aVar, int i, int i2) {
            throw null;
        }

        @DexIgnore
        public void c(a aVar, int i, int i2) {
            throw null;
        }
    }
}
