package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s0 */
public class C2849s0 {

    @DexIgnore
    /* renamed from: a */
    public android.content.Context f9179a;

    @DexIgnore
    public C2849s0(android.content.Context context) {
        this.f9179a = context;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2849s0 m13459a(android.content.Context context) {
        return new com.fossil.blesdk.obfuscated.C2849s0(context);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15803b() {
        return this.f9179a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo15804c() {
        android.content.res.Configuration configuration = this.f9179a.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600) {
            return 5;
        }
        if (i > 960 && i2 > 720) {
            return 5;
        }
        if (i > 720 && i2 > 960) {
            return 5;
        }
        if (i >= 500) {
            return 4;
        }
        if (i > 640 && i2 > 480) {
            return 4;
        }
        if (i <= 480 || i2 <= 640) {
            return i >= 360 ? 3 : 2;
        }
        return 4;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo15805d() {
        return this.f9179a.getResources().getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2998u.abc_action_bar_stacked_tab_max_width);
    }

    @DexIgnore
    /* renamed from: e */
    public int mo15806e() {
        android.content.res.TypedArray obtainStyledAttributes = this.f9179a.obtainStyledAttributes((android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C1368a0.ActionBar, com.fossil.blesdk.obfuscated.C2777r.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_height, 0);
        android.content.res.Resources resources = this.f9179a.getResources();
        if (!mo15807f()) {
            layoutDimension = java.lang.Math.min(layoutDimension, resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2998u.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo15807f() {
        return this.f9179a.getResources().getBoolean(com.fossil.blesdk.obfuscated.C2848s.abc_action_bar_embed_tabs);
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo15808g() {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !android.view.ViewConfiguration.get(this.f9179a).hasPermanentMenuKey();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo15802a() {
        return this.f9179a.getApplicationInfo().targetSdkVersion < 14;
    }
}
