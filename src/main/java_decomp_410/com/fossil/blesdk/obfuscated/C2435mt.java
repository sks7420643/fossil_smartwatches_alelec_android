package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mt */
public abstract class C2435mt<T extends android.graphics.drawable.Drawable> implements com.fossil.blesdk.obfuscated.C1438aq<T>, com.fossil.blesdk.obfuscated.C3233wp {

    @DexIgnore
    /* renamed from: e */
    public /* final */ T f7580e;

    @DexIgnore
    public C2435mt(T t) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(t);
        this.f7580e = (android.graphics.drawable.Drawable) t;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10116d() {
        T t = this.f7580e;
        if (t instanceof android.graphics.drawable.BitmapDrawable) {
            ((android.graphics.drawable.BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof com.fossil.blesdk.obfuscated.C3060ut) {
            ((com.fossil.blesdk.obfuscated.C3060ut) t).mo16862e().prepareToDraw();
        }
    }

    @DexIgnore
    public final T get() {
        android.graphics.drawable.Drawable.ConstantState constantState = this.f7580e.getConstantState();
        if (constantState == null) {
            return this.f7580e;
        }
        return constantState.newDrawable();
    }
}
