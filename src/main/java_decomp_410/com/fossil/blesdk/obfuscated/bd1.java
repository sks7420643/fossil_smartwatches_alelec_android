package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.location.LocationResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bd1 implements Parcelable.Creator<LocationResult> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        List<Location> list = LocationResult.f;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                list = SafeParcelReader.c(parcel, a, Location.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new LocationResult(list);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationResult[i];
    }
}
