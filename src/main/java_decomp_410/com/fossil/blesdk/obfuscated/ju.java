package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ju implements qu {
    @DexIgnore
    public /* final */ Set<ru> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public void a(ru ruVar) {
        this.a.add(ruVar);
        if (this.c) {
            ruVar.b();
        } else if (this.b) {
            ruVar.a();
        } else {
            ruVar.c();
        }
    }

    @DexIgnore
    public void b(ru ruVar) {
        this.a.remove(ruVar);
    }

    @DexIgnore
    public void c() {
        this.b = false;
        for (T c2 : uw.a(this.a)) {
            c2.c();
        }
    }

    @DexIgnore
    public void b() {
        this.b = true;
        for (T a2 : uw.a(this.a)) {
            a2.a();
        }
    }

    @DexIgnore
    public void a() {
        this.c = true;
        for (T b2 : uw.a(this.a)) {
            b2.b();
        }
    }
}
