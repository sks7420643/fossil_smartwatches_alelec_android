package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n6 */
public final class C2462n6 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.graphics.Shader f7661a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.ColorStateList f7662b;

    @DexIgnore
    /* renamed from: c */
    public int f7663c;

    @DexIgnore
    public C2462n6(android.graphics.Shader shader, android.content.res.ColorStateList colorStateList, int i) {
        this.f7661a = shader;
        this.f7662b = colorStateList;
        this.f7663c = i;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2462n6 m11058a(android.graphics.Shader shader) {
        return new com.fossil.blesdk.obfuscated.C2462n6(shader, (android.content.res.ColorStateList) null, 0);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2462n6 m11059b(int i) {
        return new com.fossil.blesdk.obfuscated.C2462n6((android.graphics.Shader) null, (android.content.res.ColorStateList) null, i);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo13863c() {
        return this.f7661a != null;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo13864d() {
        if (this.f7661a == null) {
            android.content.res.ColorStateList colorStateList = this.f7662b;
            return colorStateList != null && colorStateList.isStateful();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo13865e() {
        return mo13863c() || this.f7663c != 0;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2462n6 m11056a(android.content.res.ColorStateList colorStateList) {
        return new com.fossil.blesdk.obfuscated.C2462n6((android.graphics.Shader) null, colorStateList, colorStateList.getDefaultColor());
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Shader mo13862b() {
        return this.f7661a;
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2462n6 m11060b(android.content.res.Resources resources, int i, android.content.res.Resources.Theme theme) {
        try {
            return m11057a(resources, i, theme);
        } catch (java.lang.Exception e) {
            android.util.Log.e("ComplexColorCompat", "Failed to inflate ComplexColor.", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo13859a() {
        return this.f7663c;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13860a(int i) {
        this.f7663c = i;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13861a(int[] iArr) {
        if (mo13864d()) {
            android.content.res.ColorStateList colorStateList = this.f7662b;
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (colorForState != this.f7663c) {
                this.f7663c = colorForState;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2462n6 m11057a(android.content.res.Resources resources, int i, android.content.res.Resources.Theme theme) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        int next;
        android.content.res.XmlResourceParser xml = resources.getXml(i);
        android.util.AttributeSet asAttributeSet = android.util.Xml.asAttributeSet(xml);
        do {
            next = xml.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            java.lang.String name = xml.getName();
            char c = 65535;
            int hashCode = name.hashCode();
            if (hashCode != 89650992) {
                if (hashCode == 1191572447 && name.equals("selector")) {
                    c = 0;
                }
            } else if (name.equals("gradient")) {
                c = 1;
            }
            if (c == 0) {
                return m11056a(com.fossil.blesdk.obfuscated.C2377m6.m10556a(resources, (org.xmlpull.v1.XmlPullParser) xml, asAttributeSet, theme));
            }
            if (c == 1) {
                return m11058a(com.fossil.blesdk.obfuscated.C2629p6.m12108a(resources, xml, asAttributeSet, theme));
            }
            throw new org.xmlpull.v1.XmlPullParserException(xml.getPositionDescription() + ": unsupported complex color tag " + name);
        }
        throw new org.xmlpull.v1.XmlPullParserException("No start tag found");
    }
}
