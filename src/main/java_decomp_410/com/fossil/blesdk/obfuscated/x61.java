package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class x61 implements v61 {
    @DexIgnore
    public /* final */ w61 a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public x61(w61 w61, String str) {
        this.a = w61;
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        return this.a.c(this.b);
    }
}
