package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e42 {
    @DexIgnore
    public static boolean a(Context context) {
        return k6.a(context, "android.permission.ACCESS_FINE_LOCATION") == 0;
    }
}
