package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l */
public class C2261l {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.l$a */
    public interface C2262a {
        @DexIgnore
        /* renamed from: a */
        void mo127a(int i, int i2, int i3, int i4, int i5);

        @DexIgnore
        /* renamed from: a */
        void mo128a(java.lang.CharSequence charSequence);

        @DexIgnore
        /* renamed from: a */
        void mo129a(java.lang.Object obj);

        @DexIgnore
        /* renamed from: a */
        void mo130a(java.lang.String str, android.os.Bundle bundle);

        @DexIgnore
        /* renamed from: a */
        void mo131a(java.util.List<?> list);

        @DexIgnore
        /* renamed from: b */
        void mo132b(java.lang.Object obj);

        @DexIgnore
        /* renamed from: c */
        void mo133c(android.os.Bundle bundle);

        @DexIgnore
        /* renamed from: i */
        void mo134i();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l$b */
    public static class C2263b<T extends com.fossil.blesdk.obfuscated.C2261l.C2262a> extends android.media.session.MediaController.Callback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ T f7043a;

        @DexIgnore
        public C2263b(T t) {
            this.f7043a = t;
        }

        @DexIgnore
        public void onAudioInfoChanged(android.media.session.MediaController.PlaybackInfo playbackInfo) {
            this.f7043a.mo127a(playbackInfo.getPlaybackType(), com.fossil.blesdk.obfuscated.C2261l.C2264c.m9929b(playbackInfo), playbackInfo.getVolumeControl(), playbackInfo.getMaxVolume(), playbackInfo.getCurrentVolume());
        }

        @DexIgnore
        public void onExtrasChanged(android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            this.f7043a.mo133c(bundle);
        }

        @DexIgnore
        public void onMetadataChanged(android.media.MediaMetadata mediaMetadata) {
            this.f7043a.mo129a((java.lang.Object) mediaMetadata);
        }

        @DexIgnore
        public void onPlaybackStateChanged(android.media.session.PlaybackState playbackState) {
            this.f7043a.mo132b(playbackState);
        }

        @DexIgnore
        public void onQueueChanged(java.util.List<android.media.session.MediaSession.QueueItem> list) {
            this.f7043a.mo131a((java.util.List<?>) list);
        }

        @DexIgnore
        public void onQueueTitleChanged(java.lang.CharSequence charSequence) {
            this.f7043a.mo128a(charSequence);
        }

        @DexIgnore
        public void onSessionDestroyed() {
            this.f7043a.mo134i();
        }

        @DexIgnore
        public void onSessionEvent(java.lang.String str, android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            this.f7043a.mo130a(str, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l$c")
    /* renamed from: com.fossil.blesdk.obfuscated.l$c */
    public static class C2264c {
        @DexIgnore
        /* renamed from: a */
        public static android.media.AudioAttributes m9928a(java.lang.Object obj) {
            return ((android.media.session.MediaController.PlaybackInfo) obj).getAudioAttributes();
        }

        @DexIgnore
        /* renamed from: b */
        public static int m9929b(java.lang.Object obj) {
            return m9927a(m9928a(obj));
        }

        @DexIgnore
        /* renamed from: a */
        public static int m9927a(android.media.AudioAttributes audioAttributes) {
            if ((audioAttributes.getFlags() & 1) == 1) {
                return 7;
            }
            if ((audioAttributes.getFlags() & 4) == 4) {
                return 6;
            }
            switch (audioAttributes.getUsage()) {
                case 1:
                case 11:
                case 12:
                case 14:
                    return 3;
                case 2:
                    return 0;
                case 3:
                    return 8;
                case 4:
                    return 4;
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    return 5;
                case 6:
                    return 2;
                case 13:
                    return 1;
                default:
                    return 3;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m9912a(android.content.Context context, java.lang.Object obj) {
        return new android.media.session.MediaController(context, (android.media.session.MediaSession.Token) obj);
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.Object m9918b(java.lang.Object obj) {
        return ((android.media.session.MediaController) obj).getPlaybackState();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m9913a(com.fossil.blesdk.obfuscated.C2261l.C2262a aVar) {
        return new com.fossil.blesdk.obfuscated.C2261l.C2263b(aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m9915a(java.lang.Object obj, java.lang.Object obj2, android.os.Handler handler) {
        ((android.media.session.MediaController) obj).registerCallback((android.media.session.MediaController.Callback) obj2, handler);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m9914a(java.lang.Object obj) {
        return ((android.media.session.MediaController) obj).getMetadata();
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m9917a(java.lang.Object obj, android.view.KeyEvent keyEvent) {
        return ((android.media.session.MediaController) obj).dispatchMediaButtonEvent(keyEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m9916a(java.lang.Object obj, java.lang.String str, android.os.Bundle bundle, android.os.ResultReceiver resultReceiver) {
        ((android.media.session.MediaController) obj).sendCommand(str, bundle, resultReceiver);
    }
}
