package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k43 implements MembersInjector<CustomizeThemeFragment> {
    @DexIgnore
    public static void a(CustomizeThemeFragment customizeThemeFragment, j42 j42) {
        customizeThemeFragment.n = j42;
    }
}
