package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lh4 {
    @DexIgnore
    public static /* final */ dk4 a; // = new dk4("UNDEFINED");

    @DexIgnore
    public static final <T> void b(yb4<? super T> yb4, T t) {
        kd4.b(yb4, "$this$resumeDirect");
        if (yb4 instanceof jh4) {
            yb4<T> yb42 = ((jh4) yb4).l;
            Result.a aVar = Result.Companion;
            yb42.resumeWith(Result.m3constructorimpl(t));
            return;
        }
        Result.a aVar2 = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(t));
    }

    @DexIgnore
    public static final void a(mh4<?> mh4) {
        sh4 b = bj4.b.b();
        if (b.D()) {
            b.a(mh4);
            return;
        }
        b.c(true);
        try {
            a(mh4, mh4.b(), 3);
            do {
            } while (b.G());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
    }

    @DexIgnore
    public static final <T> void b(yb4<? super T> yb4, Throwable th) {
        kd4.b(yb4, "$this$resumeDirectWithException");
        kd4.b(th, "exception");
        if (yb4 instanceof jh4) {
            yb4<T> yb42 = ((jh4) yb4).l;
            Result.a aVar = Result.Companion;
            yb42.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(th, (yb4<?>) yb42))));
            return;
        }
        Result.a aVar2 = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(th, (yb4<?>) yb4))));
    }

    @DexIgnore
    public static final <T> void a(yb4<? super T> yb4, T t) {
        boolean z;
        CoroutineContext context;
        Object b;
        kd4.b(yb4, "$this$resumeCancellable");
        if (yb4 instanceof jh4) {
            jh4 jh4 = (jh4) yb4;
            if (jh4.k.b(jh4.getContext())) {
                jh4.h = t;
                jh4.g = 1;
                jh4.k.a(jh4.getContext(), jh4);
                return;
            }
            sh4 b2 = bj4.b.b();
            if (b2.D()) {
                jh4.h = t;
                jh4.g = 1;
                b2.a((mh4<?>) jh4);
                return;
            }
            b2.c(true);
            try {
                fi4 fi4 = (fi4) jh4.getContext().get(fi4.d);
                if (fi4 == null || fi4.isActive()) {
                    z = false;
                } else {
                    CancellationException y = fi4.y();
                    Result.a aVar = Result.Companion;
                    jh4.resumeWith(Result.m3constructorimpl(na4.a((Throwable) y)));
                    z = true;
                }
                if (!z) {
                    context = jh4.getContext();
                    b = ThreadContextKt.b(context, jh4.j);
                    yb4<T> yb42 = jh4.l;
                    Result.a aVar2 = Result.Companion;
                    yb42.resumeWith(Result.m3constructorimpl(t));
                    qa4 qa4 = qa4.a;
                    ThreadContextKt.a(context, b);
                }
                do {
                } while (b2.G());
            } catch (Throwable th) {
                try {
                    jh4.a(th, (Throwable) null);
                } catch (Throwable th2) {
                    b2.a(true);
                    throw th2;
                }
            }
            b2.a(true);
            return;
        }
        Result.a aVar3 = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(t));
    }

    @DexIgnore
    public static final <T> void a(yb4<? super T> yb4, Throwable th) {
        CoroutineContext context;
        Object b;
        kd4.b(yb4, "$this$resumeCancellableWithException");
        kd4.b(th, "exception");
        if (yb4 instanceof jh4) {
            jh4 jh4 = (jh4) yb4;
            CoroutineContext context2 = jh4.l.getContext();
            boolean z = false;
            ng4 ng4 = new ng4(th, false, 2, (fd4) null);
            if (jh4.k.b(context2)) {
                jh4.h = new ng4(th, false, 2, (fd4) null);
                jh4.g = 1;
                jh4.k.a(context2, jh4);
                return;
            }
            sh4 b2 = bj4.b.b();
            if (b2.D()) {
                jh4.h = ng4;
                jh4.g = 1;
                b2.a((mh4<?>) jh4);
                return;
            }
            b2.c(true);
            try {
                fi4 fi4 = (fi4) jh4.getContext().get(fi4.d);
                if (fi4 != null && !fi4.isActive()) {
                    CancellationException y = fi4.y();
                    Result.a aVar = Result.Companion;
                    jh4.resumeWith(Result.m3constructorimpl(na4.a((Throwable) y)));
                    z = true;
                }
                if (!z) {
                    context = jh4.getContext();
                    b = ThreadContextKt.b(context, jh4.j);
                    yb4<T> yb42 = jh4.l;
                    Result.a aVar2 = Result.Companion;
                    yb42.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(th, (yb4<?>) yb42))));
                    qa4 qa4 = qa4.a;
                    ThreadContextKt.a(context, b);
                }
                do {
                } while (b2.G());
            } catch (Throwable th2) {
                try {
                    jh4.a(th2, (Throwable) null);
                } catch (Throwable th3) {
                    b2.a(true);
                    throw th3;
                }
            }
            b2.a(true);
            return;
        }
        Result.a aVar3 = Result.Companion;
        yb4.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(th, (yb4<?>) yb4))));
    }

    @DexIgnore
    public static final boolean a(jh4<? super qa4> jh4) {
        kd4.b(jh4, "$this$yieldUndispatched");
        qa4 qa4 = qa4.a;
        sh4 b = bj4.b.b();
        if (b.E()) {
            return false;
        }
        if (b.D()) {
            jh4.h = qa4;
            jh4.g = 1;
            b.a((mh4<?>) jh4);
            return true;
        }
        b.c(true);
        try {
            jh4.run();
            do {
            } while (b.G());
        } catch (Throwable th) {
            b.a(true);
            throw th;
        }
        b.a(true);
        return false;
    }

    @DexIgnore
    public static final <T> void a(mh4<? super T> mh4, int i) {
        kd4.b(mh4, "$this$dispatch");
        yb4<? super T> b = mh4.b();
        if (!wi4.b(i) || !(b instanceof jh4) || wi4.a(i) != wi4.a(mh4.g)) {
            a(mh4, b, i);
            return;
        }
        ug4 ug4 = ((jh4) b).k;
        CoroutineContext context = b.getContext();
        if (ug4.b(context)) {
            ug4.a(context, mh4);
        } else {
            a((mh4<?>) mh4);
        }
    }

    @DexIgnore
    public static final <T> void a(mh4<? super T> mh4, yb4<? super T> yb4, int i) {
        kd4.b(mh4, "$this$resume");
        kd4.b(yb4, "delegate");
        Object c = mh4.c();
        Throwable a2 = mh4.a(c);
        if (a2 != null) {
            if (!(yb4 instanceof mh4)) {
                a2 = ck4.a(a2, (yb4<?>) yb4);
            }
            wi4.b(yb4, a2, i);
            return;
        }
        wi4.a(yb4, mh4.c(c), i);
    }
}
