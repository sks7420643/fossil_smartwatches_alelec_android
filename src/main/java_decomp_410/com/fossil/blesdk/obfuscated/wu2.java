package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.blesdk.obfuscated.zj3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.inappnotification.InAppNotificationUtils;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wu2 extends as2 implements ru2, ws3.g, zj3.a {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a((fd4) null);
    @DexIgnore
    public HomeDashboardPresenter k;
    @DexIgnore
    public HomeDianaCustomizePresenter l;
    @DexIgnore
    public HomeHybridCustomizePresenter m;
    @DexIgnore
    public HomeProfilePresenter n;
    @DexIgnore
    public HomeAlertsPresenter o;
    @DexIgnore
    public HomeAlertsHybridPresenter p;
    @DexIgnore
    public ig3 q;
    @DexIgnore
    public qu2 r;
    @DexIgnore
    public tr3<wc2> s;
    @DexIgnore
    public /* final */ ArrayList<Fragment> t; // = new ArrayList<>();
    @DexIgnore
    public iu3 u;
    @DexIgnore
    public InAppNotificationUtils v; // = InAppNotificationUtils.b.a();
    @DexIgnore
    public HashMap w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final wu2 a() {
            return new wu2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ wu2 a;

        @DexIgnore
        public b(wu2 wu2) {
            this.a = wu2;
        }

        @DexIgnore
        public final void a(Integer num) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String Z0 = wu2.x;
            local.d(Z0, "dashboardTab: " + num);
            int intValue = num != null ? num.intValue() : 0;
            this.a.p(intValue);
            wu2.b(this.a).a(intValue);
            this.a.q(intValue);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements BottomNavigationView.c {
        @DexIgnore
        public /* final */ /* synthetic */ wu2 a;

        @DexIgnore
        public c(wu2 wu2) {
            this.a = wu2;
        }

        @DexIgnore
        public final boolean a(MenuItem menuItem) {
            int i;
            kd4.b(menuItem, "item");
            wc2 wc2 = (wc2) wu2.a(this.a).a();
            BottomNavigationView bottomNavigationView = wc2 != null ? wc2.q : null;
            if (bottomNavigationView != null) {
                kd4.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                kd4.a((Object) menu, "mBinding.get()?.bottomNavigation!!.menu");
                menu.findItem(R.id.dashboardFragment).setIcon(R.drawable.ic_home);
                menu.findItem(R.id.customizeFragment).setIcon(R.drawable.ic_customization);
                menu.findItem(R.id.profileFragment).setIcon(R.drawable.ic_profile);
                menu.findItem(R.id.alertsFragment).setIcon(R.drawable.ic_alerts);
                switch (menuItem.getItemId()) {
                    case R.id.alertsFragment /*2131361845*/:
                        menuItem.setIcon(R.drawable.ic_alerts_active);
                        i = 2;
                        break;
                    case R.id.customizeFragment /*2131362044*/:
                        menuItem.setIcon(R.drawable.ic_customization_active);
                        i = 1;
                        break;
                    case R.id.dashboardFragment /*2131362051*/:
                        Context context = this.a.getContext();
                        if (context != null) {
                            Drawable c = k6.c(context, R.drawable.ic_home_active);
                            if (c != null) {
                                Context context2 = this.a.getContext();
                                if (context2 != null) {
                                    c.setColorFilter(k6.a(context2, (int) R.color.primaryColor), PorterDuff.Mode.SRC_ATOP);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            menuItem.setIcon(c);
                            break;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    case R.id.profileFragment /*2131362605*/:
                        menuItem.setIcon(R.drawable.ic_profile_active);
                        i = 3;
                        break;
                }
                i = 0;
                wu2 wu2 = this.a;
                if (wu2.m != null) {
                    wu2.V0().b(i);
                }
                wu2 wu22 = this.a;
                if (wu22.l != null) {
                    wu22.U0().b(i);
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String Z0 = wu2.x;
                local.d(Z0, "show tab with tab=" + i);
                wu2.b(this.a).a(i);
                this.a.q(i);
                return true;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements cc<String> {
        @DexIgnore
        public /* final */ /* synthetic */ wu2 a;

        @DexIgnore
        public d(wu2 wu2) {
            this.a = wu2;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String Z0 = wu2.x;
            local.d(Z0, "activeDeviceSerialLiveData onChange " + str);
            qu2 b = wu2.b(this.a);
            if (str != null) {
                b.a(str);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
            return true;
        }
    }

    /*
    static {
        String simpleName = wu2.class.getSimpleName();
        kd4.a((Object) simpleName, "HomeFragment::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ tr3 a(wu2 wu2) {
        tr3<wc2> tr3 = wu2.s;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qu2 b(wu2 wu2) {
        qu2 qu2 = wu2.r;
        if (qu2 != null) {
            return qu2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void G() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        qu2 qu2 = this.r;
        if (qu2 != null) {
            sb.append(qu2.h());
            local.d(str, sb.toString());
            qu2 qu22 = this.r;
            if (qu22 != null) {
                q(qu22.h());
                HomeDashboardPresenter homeDashboardPresenter = this.k;
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.m();
                } else {
                    kd4.d("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void M() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.m(childFragmentManager);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        Resources resources = getResources();
        kd4.a((Object) resources, "resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 34.0f, resources.getDisplayMetrics());
        tr3<wc2> tr3 = this.s;
        if (tr3 != null) {
            wc2 a2 = tr3.a();
            if (a2 != null) {
                int i = 0;
                View childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount() - 1;
                    if (childCount >= 0) {
                        while (true) {
                            View findViewById = bottomNavigationMenuView.getChildAt(i).findViewById(R.id.icon);
                            kd4.a((Object) findViewById, "icon");
                            findViewById.getLayoutParams().width = applyDimension;
                            findViewById.getLayoutParams().height = applyDimension;
                            if (i != childCount) {
                                i++;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final HomeDianaCustomizePresenter U0() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.l;
        if (homeDianaCustomizePresenter != null) {
            return homeDianaCustomizePresenter;
        }
        kd4.d("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final HomeHybridCustomizePresenter V0() {
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.m;
        if (homeHybridCustomizePresenter != null) {
            return homeHybridCustomizePresenter;
        }
        kd4.d("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final void W0() {
        X0();
        qu2 qu2 = this.r;
        if (qu2 != null) {
            int h = qu2.h();
            if (h == 0) {
                tr3<wc2> tr3 = this.s;
                if (tr3 != null) {
                    wc2 a2 = tr3.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        kd4.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(R.id.dashboardFragment);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            } else if (h == 1) {
                tr3<wc2> tr32 = this.s;
                if (tr32 != null) {
                    wc2 a3 = tr32.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        kd4.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(R.id.customizeFragment);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            } else if (h == 2) {
                tr3<wc2> tr33 = this.s;
                if (tr33 != null) {
                    wc2 a4 = tr33.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        kd4.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(R.id.alertsFragment);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            } else if (h == 3) {
                tr3<wc2> tr34 = this.s;
                if (tr34 != null) {
                    wc2 a5 = tr34.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        kd4.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(R.id.profileFragment);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void X0() {
        qu2 qu2 = this.r;
        if (qu2 == null) {
            return;
        }
        if (qu2 != null) {
            int h = qu2.h();
            int size = this.t.size();
            int i = 0;
            while (i < size) {
                if (this.t.get(i) instanceof ks2) {
                    Fragment fragment = this.t.get(i);
                    if (fragment != null) {
                        ((ks2) fragment).N(i == h);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i++;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void Y0() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showNoActiveDeviceFlow");
        su2 b2 = su2.z.b();
        Object a2 = getChildFragmentManager().a("HomeDianaCustomizeFragment");
        Object a3 = getChildFragmentManager().a("HomeHybridCustomizeFragment");
        Object a4 = getChildFragmentManager().a("HomeAlertsFragment");
        LifecycleOwner a5 = getChildFragmentManager().a("HomeAlertsHybridFragment");
        Object a6 = getChildFragmentManager().a("HomeProfileFragment");
        Object a7 = getChildFragmentManager().a("HomeUpdateFirmwareFragment");
        if (a2 == null) {
            a2 = vu2.s.a();
        }
        if (a3 == null) {
            a3 = zu2.s.a();
        }
        if (a4 == null) {
            a4 = tv2.q.a();
        }
        if (a5 == null) {
            a5 = zy2.o.a();
        }
        if (a6 == null) {
            a6 = ev2.p.a();
        }
        if (a7 == null) {
            a7 = hg3.n.a();
        }
        this.t.clear();
        this.t.add(b2);
        this.t.add(a3);
        this.t.add(a4);
        this.t.add(a6);
        this.t.add(a7);
        tr3<wc2> tr3 = this.s;
        if (tr3 != null) {
            wc2 a8 = tr3.a();
            if (a8 != null) {
                RecyclerViewPager recyclerViewPager = a8.r;
                kd4.a((Object) recyclerViewPager, "binding.rvTabs");
                recyclerViewPager.setAdapter(new cu3(getChildFragmentManager(), this.t));
                a8.r.setItemViewCacheSize(3);
                a8.r.a((RecyclerView.p) new f());
            }
            l42 g = PortfolioApp.W.c().g();
            if (a2 != null) {
                vu2 vu2 = (vu2) a2;
                if (a3 != null) {
                    zu2 zu2 = (zu2) a3;
                    if (a6 != null) {
                        ev2 ev2 = (ev2) a6;
                        if (a4 != null) {
                            tv2 tv2 = (tv2) a4;
                            if (a5 != null) {
                                zy2 zy2 = (zy2) a5;
                                if (a7 != null) {
                                    g.a(new gv2(b2, vu2, zu2, ev2, tv2, zy2, (hg3) a7)).a(this);
                                    return;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onActivityResult " + i + ' ' + i);
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.l;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.a(i, i2, intent);
            } else {
                kd4.d("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.m;
        if (homeHybridCustomizePresenter == null) {
            return;
        }
        if (homeHybridCustomizePresenter != null) {
            homeHybridCustomizePresenter.a(i, i2, intent);
        } else {
            kd4.d("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        wc2 wc2 = (wc2) qa.a(layoutInflater, R.layout.fragment_home, viewGroup, false, O0());
        this.s = new tr3<>(this, wc2);
        kd4.a((Object) wc2, "binding");
        return wc2.d();
    }

    @DexIgnore
    public void onDestroy() {
        qu2 qu2 = this.r;
        if (qu2 != null) {
            qu2.j();
            super.onDestroy();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        qu2 qu2 = this.r;
        if (qu2 != null) {
            qu2.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        qu2 qu2 = this.r;
        if (qu2 != null) {
            qu2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ic a2 = lc.a(activity).a(iu3.class);
            kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            this.u = (iu3) a2;
            iu3 iu3 = this.u;
            if (iu3 != null) {
                iu3.c().a(activity, new b(this));
            } else {
                kd4.d("mHomeDashboardViewModel");
                throw null;
            }
        }
        tr3<wc2> tr3 = this.s;
        if (tr3 != null) {
            wc2 a3 = tr3.a();
            BottomNavigationView bottomNavigationView = a3 != null ? a3.q : null;
            if (bottomNavigationView != null) {
                kd4.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                bottomNavigationView.setItemIconTintList((ColorStateList) null);
                tr3<wc2> tr32 = this.s;
                if (tr32 != null) {
                    wc2 a4 = tr32.a();
                    BottomNavigationView bottomNavigationView2 = a4 != null ? a4.q : null;
                    if (bottomNavigationView2 != null) {
                        bottomNavigationView2.setOnNavigationItemSelectedListener(new c(this));
                        T0();
                        PortfolioApp.W.c().f().a(this, new d(this));
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "showBottomTab currentDashboardTab=" + i);
        if (i == 0) {
            tr3<wc2> tr3 = this.s;
            if (tr3 != null) {
                wc2 a2 = tr3.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    kd4.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(R.id.dashboardFragment);
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i == 1) {
            tr3<wc2> tr32 = this.s;
            if (tr32 != null) {
                wc2 a3 = tr32.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    kd4.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(R.id.customizeFragment);
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i == 2) {
            tr3<wc2> tr33 = this.s;
            if (tr33 != null) {
                wc2 a4 = tr33.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    kd4.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(R.id.alertsFragment);
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i == 3) {
            tr3<wc2> tr34 = this.s;
            if (tr34 != null) {
                wc2 a5 = tr34.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    kd4.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(R.id.profileFragment);
                    return;
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void q(int i) {
        qu2 qu2 = this.r;
        if (qu2 != null) {
            boolean i2 = qu2.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "scroll to position=" + i + " fragment " + this.t.get(i) + " isOtaing " + i2);
            if (!i2) {
                tr3<wc2> tr3 = this.s;
                if (tr3 != null) {
                    wc2 a2 = tr3.a();
                    if (a2 != null) {
                        a2.r.i(i);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else if (i > 0) {
                tr3<wc2> tr32 = this.s;
                if (tr32 != null) {
                    wc2 a3 = tr32.a();
                    if (a3 != null) {
                        a3.r.i(4);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else {
                tr3<wc2> tr33 = this.s;
                if (tr33 != null) {
                    wc2 a4 = tr33.a();
                    if (a4 != null) {
                        a4.r.i(i);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            }
            X0();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(InAppNotification inAppNotification) {
        qu2 qu2 = this.r;
        if (qu2 == null) {
            kd4.d("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            qu2.b(inAppNotification);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(qu2 qu2) {
        kd4.b(qu2, "presenter");
        this.r = qu2;
    }

    @DexIgnore
    public void b(int i) {
        HomeDashboardPresenter homeDashboardPresenter = this.k;
        if (homeDashboardPresenter != null) {
            homeDashboardPresenter.c(i);
        } else {
            kd4.d("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(FossilDeviceSerialPatternUtil.DEVICE device, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i);
        if (i != 1) {
            a(device);
        } else {
            Y0();
        }
        W0();
    }

    @DexIgnore
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        qu2 qu2 = this.r;
        if (qu2 != null) {
            sb.append(qu2.h());
            local.d(str, sb.toString());
            HomeDashboardPresenter homeDashboardPresenter = this.k;
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.b(z);
                qu2 qu22 = this.r;
                if (qu22 != null) {
                    q(qu22.h());
                    if (!z && isActive()) {
                        ds3 ds3 = ds3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager, "childFragmentManager");
                        ds3.J(childFragmentManager);
                        return;
                    }
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            kd4.d("mHomeDashboardPresenter");
            throw null;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(InAppNotification inAppNotification) {
        kd4.b(inAppNotification, "inAppNotification");
        if (isActive()) {
            InAppNotificationUtils inAppNotificationUtils = this.v;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                inAppNotificationUtils.a(activity, inAppNotification);
            } else {
                kd4.a();
                throw null;
            }
        }
        b(inAppNotification);
    }

    @DexIgnore
    public final void a(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showMainFlow");
        su2 b2 = su2.z.b();
        Object a2 = getChildFragmentManager().a("HomeDianaCustomizeFragment");
        Object a3 = getChildFragmentManager().a("HomeHybridCustomizeFragment");
        Object a4 = getChildFragmentManager().a("HomeAlertsFragment");
        Object a5 = getChildFragmentManager().a("HomeAlertsHybridFragment");
        Object a6 = getChildFragmentManager().a("HomeProfileFragment");
        Object a7 = getChildFragmentManager().a("HomeUpdateFirmwareFragment");
        if (a2 == null) {
            a2 = vu2.s.a();
        }
        if (a3 == null) {
            a3 = zu2.s.a();
        }
        if (a4 == null) {
            a4 = tv2.q.a();
        }
        if (a5 == null) {
            a5 = zy2.o.a();
        }
        if (a6 == null) {
            a6 = ev2.p.a();
        }
        if (a7 == null) {
            a7 = hg3.n.a();
        }
        this.t.clear();
        this.t.add(b2);
        if (device != null && xu2.a[device.ordinal()] == 1) {
            this.t.add(a2);
            this.t.add(a4);
        } else {
            this.t.add(a3);
            this.t.add(a5);
        }
        this.t.add(a6);
        this.t.add(a7);
        tr3<wc2> tr3 = this.s;
        if (tr3 != null) {
            wc2 a8 = tr3.a();
            if (a8 != null) {
                RecyclerViewPager recyclerViewPager = a8.r;
                kd4.a((Object) recyclerViewPager, "binding.rvTabs");
                recyclerViewPager.setAdapter(new cu3(getChildFragmentManager(), this.t));
                a8.r.setItemViewCacheSize(3);
                a8.r.a((RecyclerView.p) new e());
            }
            l42 g = PortfolioApp.W.c().g();
            if (a2 != null) {
                vu2 vu2 = (vu2) a2;
                if (a3 != null) {
                    zu2 zu2 = (zu2) a3;
                    if (a6 != null) {
                        ev2 ev2 = (ev2) a6;
                        if (a4 != null) {
                            tv2 tv2 = (tv2) a4;
                            if (a5 != null) {
                                zy2 zy2 = (zy2) a5;
                                if (a7 != null) {
                                    g.a(new gv2(b2, vu2, zu2, ev2, tv2, zy2, (hg3) a7)).a(this);
                                    return;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        kd4.b(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i == R.id.tv_ok) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.D;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        kd4.a((Object) activity, "activity!!");
                        aVar.a(activity, false);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Cancel Zendesk feedback");
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Zendesk feedback");
                    qu2 qu2 = this.r;
                    if (qu2 != null) {
                        qu2.b("Feedback - From app [Fossil] - [Android]");
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Send Zendesk feedback");
                    ds3 ds3 = ds3.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    kd4.a((Object) childFragmentManager, "childFragmentManager");
                    ds3.i(childFragmentManager);
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Open app rating dialog");
                    ds3 ds32 = ds3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    kd4.a((Object) childFragmentManager2, "childFragmentManager");
                    ds32.d(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i == R.id.tv_cancel) {
                    FLogger.INSTANCE.getLocal().d(x, "Stay in app");
                    return;
                } else if (i == R.id.tv_ok) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Play Store");
                    PortfolioApp c2 = PortfolioApp.W.c();
                    AppHelper.Companion companion = AppHelper.f;
                    String packageName = c2.getPackageName();
                    kd4.a((Object) packageName, "packageName");
                    companion.b(c2, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public void a(Intent intent) {
        kd4.b(intent, "intent");
        HomeDashboardPresenter homeDashboardPresenter = this.k;
        if (homeDashboardPresenter != null) {
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.a(intent);
            } else {
                kd4.d("mHomeDashboardPresenter");
                throw null;
            }
        }
        HomeProfilePresenter homeProfilePresenter = this.n;
        if (homeProfilePresenter == null) {
            return;
        }
        if (homeProfilePresenter != null) {
            homeProfilePresenter.b(intent);
        } else {
            kd4.d("mHomeProfilePresenter");
            throw null;
        }
    }
}
