package com.fossil.blesdk.obfuscated;

import android.app.Activity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e04 extends Activity {
    @DexIgnore
    public void onPause() {
        super.onPause();
        i04.a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        i04.b(this);
    }
}
