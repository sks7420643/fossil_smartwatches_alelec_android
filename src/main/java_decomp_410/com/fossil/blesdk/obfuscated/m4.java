package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m4 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ n4 b;
    @DexIgnore
    public /* final */ o4 c;
    @DexIgnore
    public int d; // = 8;
    @DexIgnore
    public SolverVariable e; // = null;
    @DexIgnore
    public int[] f;
    @DexIgnore
    public int[] g;
    @DexIgnore
    public float[] h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k;

    @DexIgnore
    public m4(n4 n4Var, o4 o4Var) {
        int i2 = this.d;
        this.f = new int[i2];
        this.g = new int[i2];
        this.h = new float[i2];
        this.i = -1;
        this.j = -1;
        this.k = false;
        this.b = n4Var;
        this.c = o4Var;
    }

    @DexIgnore
    public final void a(SolverVariable solverVariable, float f2) {
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            a(solverVariable, true);
            return;
        }
        int i2 = this.i;
        if (i2 == -1) {
            this.i = 0;
            float[] fArr = this.h;
            int i3 = this.i;
            fArr[i3] = f2;
            this.f[i3] = solverVariable.b;
            this.g[i3] = -1;
            solverVariable.j++;
            solverVariable.a(this.b);
            this.a++;
            if (!this.k) {
                this.j++;
                int i4 = this.j;
                int[] iArr = this.f;
                if (i4 >= iArr.length) {
                    this.k = true;
                    this.j = iArr.length - 1;
                    return;
                }
                return;
            }
            return;
        }
        int i5 = 0;
        int i6 = -1;
        while (i2 != -1 && i5 < this.a) {
            int[] iArr2 = this.f;
            int i7 = iArr2[i2];
            int i8 = solverVariable.b;
            if (i7 == i8) {
                this.h[i2] = f2;
                return;
            }
            if (iArr2[i2] < i8) {
                i6 = i2;
            }
            i2 = this.g[i2];
            i5++;
        }
        int i9 = this.j;
        int i10 = i9 + 1;
        if (this.k) {
            int[] iArr3 = this.f;
            if (iArr3[i9] != -1) {
                i9 = iArr3.length;
            }
        } else {
            i9 = i10;
        }
        int[] iArr4 = this.f;
        if (i9 >= iArr4.length && this.a < iArr4.length) {
            int i11 = 0;
            while (true) {
                int[] iArr5 = this.f;
                if (i11 >= iArr5.length) {
                    break;
                } else if (iArr5[i11] == -1) {
                    i9 = i11;
                    break;
                } else {
                    i11++;
                }
            }
        }
        int[] iArr6 = this.f;
        if (i9 >= iArr6.length) {
            i9 = iArr6.length;
            this.d *= 2;
            this.k = false;
            this.j = i9 - 1;
            this.h = Arrays.copyOf(this.h, this.d);
            this.f = Arrays.copyOf(this.f, this.d);
            this.g = Arrays.copyOf(this.g, this.d);
        }
        this.f[i9] = solverVariable.b;
        this.h[i9] = f2;
        if (i6 != -1) {
            int[] iArr7 = this.g;
            iArr7[i9] = iArr7[i6];
            iArr7[i6] = i9;
        } else {
            this.g[i9] = this.i;
            this.i = i9;
        }
        solverVariable.j++;
        solverVariable.a(this.b);
        this.a++;
        if (!this.k) {
            this.j++;
        }
        if (this.a >= this.f.length) {
            this.k = true;
        }
        int i12 = this.j;
        int[] iArr8 = this.f;
        if (i12 >= iArr8.length) {
            this.k = true;
            this.j = iArr8.length - 1;
        }
    }

    @DexIgnore
    public void b() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] * -1.0f;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public String toString() {
        int i2 = this.i;
        String str = "";
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            str = ((str + " -> ") + this.h[i2] + " : ") + this.c.c[this.f[i2]];
            i2 = this.g[i2];
            i3++;
        }
        return str;
    }

    @DexIgnore
    public final float b(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.h[i3];
            }
            i3 = this.g[i3];
            i4++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float b(SolverVariable solverVariable) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == solverVariable.b) {
                return this.h[i2];
            }
            i2 = this.g[i2];
            i3++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void a(SolverVariable solverVariable, float f2, boolean z) {
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            int i2 = this.i;
            if (i2 == -1) {
                this.i = 0;
                float[] fArr = this.h;
                int i3 = this.i;
                fArr[i3] = f2;
                this.f[i3] = solverVariable.b;
                this.g[i3] = -1;
                solverVariable.j++;
                solverVariable.a(this.b);
                this.a++;
                if (!this.k) {
                    this.j++;
                    int i4 = this.j;
                    int[] iArr = this.f;
                    if (i4 >= iArr.length) {
                        this.k = true;
                        this.j = iArr.length - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            int i5 = 0;
            int i6 = -1;
            while (i2 != -1 && i5 < this.a) {
                int[] iArr2 = this.f;
                int i7 = iArr2[i2];
                int i8 = solverVariable.b;
                if (i7 == i8) {
                    float[] fArr2 = this.h;
                    fArr2[i2] = fArr2[i2] + f2;
                    if (fArr2[i2] == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (i2 == this.i) {
                            this.i = this.g[i2];
                        } else {
                            int[] iArr3 = this.g;
                            iArr3[i6] = iArr3[i2];
                        }
                        if (z) {
                            solverVariable.b(this.b);
                        }
                        if (this.k) {
                            this.j = i2;
                        }
                        solverVariable.j--;
                        this.a--;
                        return;
                    }
                    return;
                }
                if (iArr2[i2] < i8) {
                    i6 = i2;
                }
                i2 = this.g[i2];
                i5++;
            }
            int i9 = this.j;
            int i10 = i9 + 1;
            if (this.k) {
                int[] iArr4 = this.f;
                if (iArr4[i9] != -1) {
                    i9 = iArr4.length;
                }
            } else {
                i9 = i10;
            }
            int[] iArr5 = this.f;
            if (i9 >= iArr5.length && this.a < iArr5.length) {
                int i11 = 0;
                while (true) {
                    int[] iArr6 = this.f;
                    if (i11 >= iArr6.length) {
                        break;
                    } else if (iArr6[i11] == -1) {
                        i9 = i11;
                        break;
                    } else {
                        i11++;
                    }
                }
            }
            int[] iArr7 = this.f;
            if (i9 >= iArr7.length) {
                i9 = iArr7.length;
                this.d *= 2;
                this.k = false;
                this.j = i9 - 1;
                this.h = Arrays.copyOf(this.h, this.d);
                this.f = Arrays.copyOf(this.f, this.d);
                this.g = Arrays.copyOf(this.g, this.d);
            }
            this.f[i9] = solverVariable.b;
            this.h[i9] = f2;
            if (i6 != -1) {
                int[] iArr8 = this.g;
                iArr8[i9] = iArr8[i6];
                iArr8[i6] = i9;
            } else {
                this.g[i9] = this.i;
                this.i = i9;
            }
            solverVariable.j++;
            solverVariable.a(this.b);
            this.a++;
            if (!this.k) {
                this.j++;
            }
            int i12 = this.j;
            int[] iArr9 = this.f;
            if (i12 >= iArr9.length) {
                this.k = true;
                this.j = iArr9.length - 1;
            }
        }
    }

    @DexIgnore
    public final float a(SolverVariable solverVariable, boolean z) {
        if (this.e == solverVariable) {
            this.e = null;
        }
        int i2 = this.i;
        if (i2 == -1) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i3 = 0;
        int i4 = -1;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == solverVariable.b) {
                if (i2 == this.i) {
                    this.i = this.g[i2];
                } else {
                    int[] iArr = this.g;
                    iArr[i4] = iArr[i2];
                }
                if (z) {
                    solverVariable.b(this.b);
                }
                solverVariable.j--;
                this.a--;
                this.f[i2] = -1;
                if (this.k) {
                    this.j = i2;
                }
                return this.h[i2];
            }
            i3++;
            i4 = i2;
            i2 = this.g[i2];
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void a() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            SolverVariable solverVariable = this.c.c[this.f[i2]];
            if (solverVariable != null) {
                solverVariable.b(this.b);
            }
            i2 = this.g[i2];
            i3++;
        }
        this.i = -1;
        this.j = -1;
        this.k = false;
        this.a = 0;
    }

    @DexIgnore
    public final boolean a(SolverVariable solverVariable) {
        int i2 = this.i;
        if (i2 == -1) {
            return false;
        }
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == solverVariable.b) {
                return true;
            }
            i2 = this.g[i2];
            i3++;
        }
        return false;
    }

    @DexIgnore
    public void a(float f2) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] / f2;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public final boolean a(SolverVariable solverVariable, q4 q4Var) {
        return solverVariable.j <= 1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x008f A[SYNTHETIC] */
    public SolverVariable a(q4 q4Var) {
        boolean a2;
        boolean a3;
        int i2 = this.i;
        SolverVariable solverVariable = null;
        int i3 = 0;
        SolverVariable solverVariable2 = null;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = false;
        while (i2 != -1 && i3 < this.a) {
            float[] fArr = this.h;
            float f4 = fArr[i2];
            SolverVariable solverVariable3 = this.c.c[this.f[i2]];
            if (f4 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (f4 > -0.001f) {
                    fArr[i2] = 0.0f;
                    solverVariable3.b(this.b);
                }
                if (f4 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    if (solverVariable3.g == SolverVariable.Type.UNRESTRICTED) {
                        if (solverVariable2 == null) {
                            a3 = a(solverVariable3, q4Var);
                        } else if (f2 > f4) {
                            a3 = a(solverVariable3, q4Var);
                        } else if (!z && a(solverVariable3, q4Var)) {
                            f2 = f4;
                            solverVariable2 = solverVariable3;
                            z = true;
                        }
                        z = a3;
                        f2 = f4;
                        solverVariable2 = solverVariable3;
                    } else if (solverVariable2 == null && f4 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (solverVariable == null) {
                            a2 = a(solverVariable3, q4Var);
                        } else if (f3 > f4) {
                            a2 = a(solverVariable3, q4Var);
                        } else if (!z2 && a(solverVariable3, q4Var)) {
                            f3 = f4;
                            solverVariable = solverVariable3;
                            z2 = true;
                        }
                        z2 = a2;
                        f3 = f4;
                        solverVariable = solverVariable3;
                    }
                }
                i2 = this.g[i2];
                i3++;
            } else {
                if (f4 < 0.001f) {
                    fArr[i2] = 0.0f;
                    solverVariable3.b(this.b);
                }
                if (f4 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                }
                i2 = this.g[i2];
                i3++;
            }
            f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (f4 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            }
            i2 = this.g[i2];
            i3++;
        }
        return solverVariable2 != null ? solverVariable2 : solverVariable;
    }

    @DexIgnore
    public final void a(n4 n4Var, n4 n4Var2, boolean z) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            while (i2 != -1 && i3 < this.a) {
                int i4 = this.f[i2];
                SolverVariable solverVariable = n4Var2.a;
                if (i4 == solverVariable.b) {
                    float f2 = this.h[i2];
                    a(solverVariable, z);
                    m4 m4Var = n4Var2.d;
                    int i5 = m4Var.i;
                    int i6 = 0;
                    while (i5 != -1 && i6 < m4Var.a) {
                        a(this.c.c[m4Var.f[i5]], m4Var.h[i5] * f2, z);
                        i5 = m4Var.g[i5];
                        i6++;
                    }
                    n4Var.b += n4Var2.b * f2;
                    if (z) {
                        n4Var2.a.b(n4Var);
                    }
                    i2 = this.i;
                } else {
                    i2 = this.g[i2];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public void a(n4 n4Var, n4[] n4VarArr) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            while (i2 != -1 && i3 < this.a) {
                SolverVariable solverVariable = this.c.c[this.f[i2]];
                if (solverVariable.c != -1) {
                    float f2 = this.h[i2];
                    a(solverVariable, true);
                    n4 n4Var2 = n4VarArr[solverVariable.c];
                    if (!n4Var2.e) {
                        m4 m4Var = n4Var2.d;
                        int i4 = m4Var.i;
                        int i5 = 0;
                        while (i4 != -1 && i5 < m4Var.a) {
                            a(this.c.c[m4Var.f[i4]], m4Var.h[i4] * f2, true);
                            i4 = m4Var.g[i4];
                            i5++;
                        }
                    }
                    n4Var.b += n4Var2.b * f2;
                    n4Var2.a.b(n4Var);
                    i2 = this.i;
                } else {
                    i2 = this.g[i2];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public SolverVariable a(boolean[] zArr, SolverVariable solverVariable) {
        int i2 = this.i;
        int i3 = 0;
        SolverVariable solverVariable2 = null;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (i2 != -1 && i3 < this.a) {
            if (this.h[i2] < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                SolverVariable solverVariable3 = this.c.c[this.f[i2]];
                if ((zArr == null || !zArr[solverVariable3.b]) && solverVariable3 != solverVariable) {
                    SolverVariable.Type type = solverVariable3.g;
                    if (type == SolverVariable.Type.SLACK || type == SolverVariable.Type.ERROR) {
                        float f3 = this.h[i2];
                        if (f3 < f2) {
                            solverVariable2 = solverVariable3;
                            f2 = f3;
                        }
                    }
                }
            }
            i2 = this.g[i2];
            i3++;
        }
        return solverVariable2;
    }

    @DexIgnore
    public final SolverVariable a(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.c.c[this.f[i3]];
            }
            i3 = this.g[i3];
            i4++;
        }
        return null;
    }
}
