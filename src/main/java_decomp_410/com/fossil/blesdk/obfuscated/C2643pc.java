package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pc */
public class C2643pc extends com.fossil.blesdk.obfuscated.C2542oc<android.database.Cursor> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2719qc<android.database.Cursor>.a f8330a; // = new com.fossil.blesdk.obfuscated.C2719qc.C2720a();

    @DexIgnore
    /* renamed from: b */
    public android.net.Uri f8331b;

    @DexIgnore
    /* renamed from: c */
    public java.lang.String[] f8332c;

    @DexIgnore
    /* renamed from: d */
    public java.lang.String f8333d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.String[] f8334e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.String f8335f;

    @DexIgnore
    /* renamed from: g */
    public android.database.Cursor f8336g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2379m7 f8337h;

    @DexIgnore
    public C2643pc(android.content.Context context, android.net.Uri uri, java.lang.String[] strArr, java.lang.String str, java.lang.String[] strArr2, java.lang.String str2) {
        super(context);
        this.f8331b = uri;
        this.f8332c = strArr;
        this.f8333d = str;
        this.f8334e = strArr2;
        this.f8335f = str2;
    }

    @DexIgnore
    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            if (this.f8337h != null) {
                this.f8337h.mo13523a();
            }
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    public void dump(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.print("mUri=");
        printWriter.println(this.f8331b);
        printWriter.print(str);
        printWriter.print("mProjection=");
        printWriter.println(java.util.Arrays.toString(this.f8332c));
        printWriter.print(str);
        printWriter.print("mSelection=");
        printWriter.println(this.f8333d);
        printWriter.print(str);
        printWriter.print("mSelectionArgs=");
        printWriter.println(java.util.Arrays.toString(this.f8334e));
        printWriter.print(str);
        printWriter.print("mSortOrder=");
        printWriter.println(this.f8335f);
        printWriter.print(str);
        printWriter.print("mCursor=");
        printWriter.println(this.f8336g);
        printWriter.print(str);
        printWriter.print("mContentChanged=");
        printWriter.println(this.mContentChanged);
    }

    @DexIgnore
    public void onReset() {
        super.onReset();
        onStopLoading();
        android.database.Cursor cursor = this.f8336g;
        if (cursor != null && !cursor.isClosed()) {
            this.f8336g.close();
        }
        this.f8336g = null;
    }

    @DexIgnore
    public void onStartLoading() {
        android.database.Cursor cursor = this.f8336g;
        if (cursor != null) {
            deliverResult(cursor);
        }
        if (takeContentChanged() || this.f8336g == null) {
            forceLoad();
        }
    }

    @DexIgnore
    public void onStopLoading() {
        cancelLoad();
    }

    @DexIgnore
    public void deliverResult(android.database.Cursor cursor) {
        if (!isReset()) {
            android.database.Cursor cursor2 = this.f8336g;
            this.f8336g = cursor;
            if (isStarted()) {
                super.deliverResult(cursor);
            }
            if (cursor2 != null && cursor2 != cursor && !cursor2.isClosed()) {
                cursor2.close();
            }
        } else if (cursor != null) {
            cursor.close();
        }
    }

    @DexIgnore
    public android.database.Cursor loadInBackground() {
        android.database.Cursor a;
        synchronized (this) {
            if (!isLoadInBackgroundCanceled()) {
                this.f8337h = new com.fossil.blesdk.obfuscated.C2379m7();
            } else {
                throw new androidx.core.p001os.OperationCanceledException();
            }
        }
        try {
            a = com.fossil.blesdk.obfuscated.C2107j6.m8847a(getContext().getContentResolver(), this.f8331b, this.f8332c, this.f8333d, this.f8334e, this.f8335f, this.f8337h);
            if (a != null) {
                a.getCount();
                a.registerContentObserver(this.f8330a);
            }
            synchronized (this) {
                this.f8337h = null;
            }
            return a;
        } catch (java.lang.RuntimeException e) {
            a.close();
            throw e;
        } catch (Throwable th) {
            synchronized (this) {
                this.f8337h = null;
                throw th;
            }
        }
    }

    @DexIgnore
    public void onCanceled(android.database.Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
