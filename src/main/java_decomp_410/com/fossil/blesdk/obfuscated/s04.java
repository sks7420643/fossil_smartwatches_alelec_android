package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s04 extends o04 {
    @DexIgnore
    public v14 m;
    @DexIgnore
    public JSONObject n; // = null;

    @DexIgnore
    public s04(Context context, int i, JSONObject jSONObject, k04 k04) {
        super(context, i, k04);
        this.m = new v14(context);
        this.n = jSONObject;
    }

    @DexIgnore
    public f a() {
        return f.SESSION_ENV;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        u14 u14 = this.d;
        if (u14 != null) {
            jSONObject.put("ut", u14.d());
        }
        JSONObject jSONObject2 = this.n;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (e24.B(this.j)) {
            jSONObject.put("ncts", 1);
        }
        this.m.a(jSONObject, (Thread) null);
        return true;
    }
}
