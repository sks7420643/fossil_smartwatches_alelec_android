package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.dn */
public class C1644dn implements com.fossil.blesdk.obfuscated.C1882gn {

    @DexIgnore
    /* renamed from: a */
    public /* final */ org.apache.http.client.HttpClient f4458a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dn$a */
    public static final class C1645a extends org.apache.http.client.methods.HttpEntityEnclosingRequestBase {
        @DexIgnore
        public C1645a(java.lang.String str) {
            setURI(java.net.URI.create(str));
        }

        @DexIgnore
        public java.lang.String getMethod() {
            return "PATCH";
        }
    }

    @DexIgnore
    public C1644dn(org.apache.http.client.HttpClient httpClient) {
        this.f4458a = httpClient;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6009a(org.apache.http.client.methods.HttpUriRequest httpUriRequest, java.util.Map<java.lang.String, java.lang.String> map) {
        for (java.lang.String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static org.apache.http.client.methods.HttpUriRequest m6010b(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws com.android.volley.AuthFailureError {
        switch (request.getMethod()) {
            case -1:
                byte[] postBody = request.getPostBody();
                if (postBody == null) {
                    return new org.apache.http.client.methods.HttpGet(request.getUrl());
                }
                org.apache.http.client.methods.HttpPost httpPost = new org.apache.http.client.methods.HttpPost(request.getUrl());
                httpPost.addHeader(com.facebook.GraphRequest.CONTENT_TYPE_HEADER, request.getPostBodyContentType());
                httpPost.setEntity(new org.apache.http.entity.ByteArrayEntity(postBody));
                return httpPost;
            case 0:
                return new org.apache.http.client.methods.HttpGet(request.getUrl());
            case 1:
                org.apache.http.client.methods.HttpPost httpPost2 = new org.apache.http.client.methods.HttpPost(request.getUrl());
                httpPost2.addHeader(com.facebook.GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                m6008a((org.apache.http.client.methods.HttpEntityEnclosingRequestBase) httpPost2, request);
                return httpPost2;
            case 2:
                org.apache.http.client.methods.HttpPut httpPut = new org.apache.http.client.methods.HttpPut(request.getUrl());
                httpPut.addHeader(com.facebook.GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                m6008a((org.apache.http.client.methods.HttpEntityEnclosingRequestBase) httpPut, request);
                return httpPut;
            case 3:
                return new org.apache.http.client.methods.HttpDelete(request.getUrl());
            case 4:
                return new org.apache.http.client.methods.HttpHead(request.getUrl());
            case 5:
                return new org.apache.http.client.methods.HttpOptions(request.getUrl());
            case 6:
                return new org.apache.http.client.methods.HttpTrace(request.getUrl());
            case 7:
                com.fossil.blesdk.obfuscated.C1644dn.C1645a aVar = new com.fossil.blesdk.obfuscated.C1644dn.C1645a(request.getUrl());
                aVar.addHeader(com.facebook.GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                m6008a((org.apache.http.client.methods.HttpEntityEnclosingRequestBase) aVar, request);
                return aVar;
            default:
                throw new java.lang.IllegalStateException("Unknown request method.");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10081a(org.apache.http.client.methods.HttpUriRequest httpUriRequest) throws java.io.IOException {
    }

    @DexIgnore
    /* renamed from: a */
    public org.apache.http.HttpResponse mo10080a(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException, com.android.volley.AuthFailureError {
        org.apache.http.client.methods.HttpUriRequest b = m6010b(request, map);
        m6009a(b, map);
        m6009a(b, request.getHeaders());
        mo10081a(b);
        org.apache.http.params.HttpParams params = b.getParams();
        int timeoutMs = request.getTimeoutMs();
        org.apache.http.params.HttpConnectionParams.setConnectionTimeout(params, com.facebook.share.internal.VideoUploader.RETRY_DELAY_UNIT_MS);
        org.apache.http.params.HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.f4458a.execute(b);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6008a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, com.android.volley.Request<?> request) throws com.android.volley.AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity(new org.apache.http.entity.ByteArrayEntity(body));
        }
    }
}
