package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bz */
public class C1520bz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f3946a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.crashlytics.android.core.Report f3947b;

    @DexIgnore
    public C1520bz(java.lang.String str, com.crashlytics.android.core.Report report) {
        this.f3946a = str;
        this.f3947b = report;
    }
}
