package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sx0 implements Cloneable {
    @DexIgnore
    public int[] e;
    @DexIgnore
    public tx0[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new tx0();
    }
    */

    @DexIgnore
    public sx0() {
        this(10);
    }

    @DexIgnore
    public sx0(int i) {
        int i2 = i << 2;
        int i3 = 4;
        while (true) {
            if (i3 >= 32) {
                break;
            }
            int i4 = (1 << i3) - 12;
            if (i2 <= i4) {
                i2 = i4;
                break;
            }
            i3++;
        }
        int i5 = i2 / 4;
        this.e = new int[i5];
        this.f = new tx0[i5];
        this.g = 0;
    }

    @DexIgnore
    public final tx0 a(int i) {
        return this.f[i];
    }

    @DexIgnore
    public final boolean a() {
        return this.g == 0;
    }

    @DexIgnore
    public final int b() {
        return this.g;
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        int i = this.g;
        sx0 sx0 = new sx0(i);
        System.arraycopy(this.e, 0, sx0.e, 0, i);
        for (int i2 = 0; i2 < i; i2++) {
            tx0[] tx0Arr = this.f;
            if (tx0Arr[i2] != null) {
                sx0.f[i2] = (tx0) tx0Arr[i2].clone();
            }
        }
        sx0.g = i;
        return sx0;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof sx0)) {
            return false;
        }
        sx0 sx0 = (sx0) obj;
        int i = this.g;
        if (i != sx0.g) {
            return false;
        }
        int[] iArr = this.e;
        int[] iArr2 = sx0.e;
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = true;
                break;
            } else if (iArr[i2] != iArr2[i2]) {
                z = false;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            tx0[] tx0Arr = this.f;
            tx0[] tx0Arr2 = sx0.f;
            int i3 = this.g;
            int i4 = 0;
            while (true) {
                if (i4 >= i3) {
                    z2 = true;
                    break;
                } else if (!tx0Arr[i4].equals(tx0Arr2[i4])) {
                    z2 = false;
                    break;
                } else {
                    i4++;
                }
            }
            if (z2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int i = 17;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (((i * 31) + this.e[i2]) * 31) + this.f[i2].hashCode();
        }
        return i;
    }
}
