package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;
import okio.ByteString;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bm4 extends RequestBody {
    @DexIgnore
    public static /* final */ am4 e; // = am4.a("multipart/mixed");
    @DexIgnore
    public static /* final */ am4 f; // = am4.a("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {DateTimeFieldType.HALFDAY_OF_DAY, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {45, 45};
    @DexIgnore
    public /* final */ ByteString a;
    @DexIgnore
    public /* final */ am4 b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public long d; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ByteString a;
        @DexIgnore
        public am4 b;
        @DexIgnore
        public /* final */ List<b> c;

        @DexIgnore
        public a() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public a a(am4 am4) {
            if (am4 == null) {
                throw new NullPointerException("type == null");
            } else if (am4.c().equals("multipart")) {
                this.b = am4;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + am4);
            }
        }

        @DexIgnore
        public a(String str) {
            this.b = bm4.e;
            this.c = new ArrayList();
            this.a = ByteString.encodeUtf8(str);
        }

        @DexIgnore
        public a a(yl4 yl4, RequestBody requestBody) {
            a(b.a(yl4, requestBody));
            return this;
        }

        @DexIgnore
        public a a(b bVar) {
            if (bVar != null) {
                this.c.add(bVar);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public bm4 a() {
            if (!this.c.isEmpty()) {
                return new bm4(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ yl4 a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public b(yl4 yl4, RequestBody requestBody) {
            this.a = yl4;
            this.b = requestBody;
        }

        @DexIgnore
        public static b a(yl4 yl4, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (yl4 != null && yl4.a(GraphRequest.CONTENT_TYPE_HEADER) != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (yl4 == null || yl4.a("Content-Length") == null) {
                return new b(yl4, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }
    }

    /*
    static {
        am4.a("multipart/alternative");
        am4.a("multipart/digest");
        am4.a("multipart/parallel");
    }
    */

    @DexIgnore
    public bm4(ByteString byteString, am4 am4, List<b> list) {
        this.a = byteString;
        this.b = am4.a(am4 + "; boundary=" + byteString.utf8());
        this.c = jm4.a(list);
    }

    @DexIgnore
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long a2 = a((ko4) null, true);
        this.d = a2;
        return a2;
    }

    @DexIgnore
    public am4 b() {
        return this.b;
    }

    @DexIgnore
    public void a(ko4 ko4) throws IOException {
        a(ko4, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: com.fossil.blesdk.obfuscated.ko4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.fossil.blesdk.obfuscated.jo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.fossil.blesdk.obfuscated.jo4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: com.fossil.blesdk.obfuscated.ko4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.fossil.blesdk.obfuscated.jo4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final long a(ko4 ko4, boolean z) throws IOException {
        jo4 jo4;
        if (z) {
            ko4 = new jo4();
            jo4 = ko4;
        } else {
            jo4 = 0;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            yl4 yl4 = bVar.a;
            RequestBody requestBody = bVar.b;
            ko4.write(i);
            ko4.a(this.a);
            ko4.write(h);
            if (yl4 != null) {
                int b2 = yl4.b();
                for (int i3 = 0; i3 < b2; i3++) {
                    ko4.a(yl4.a(i3)).write(g).a(yl4.b(i3)).write(h);
                }
            }
            am4 b3 = requestBody.b();
            if (b3 != null) {
                ko4.a("Content-Type: ").a(b3.toString()).write(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                ko4.a("Content-Length: ").b(a2).write(h);
            } else if (z) {
                jo4.w();
                return -1;
            }
            ko4.write(h);
            if (z) {
                j += a2;
            } else {
                requestBody.a(ko4);
            }
            ko4.write(h);
        }
        ko4.write(i);
        ko4.a(this.a);
        ko4.write(i);
        ko4.write(h);
        if (!z) {
            return j;
        }
        long B = j + jo4.B();
        jo4.w();
        return B;
    }
}
