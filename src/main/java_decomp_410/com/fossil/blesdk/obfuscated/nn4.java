package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.on4;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.http2.ConnectionShutdownException;
import okhttp3.internal.http2.ErrorCode;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn4 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService y; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), jm4.a("OkHttp Http2Connection", true));
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ h f;
    @DexIgnore
    public /* final */ Map<Integer, pn4> g; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ ScheduledExecutorService l;
    @DexIgnore
    public /* final */ ExecutorService m;
    @DexIgnore
    public /* final */ sn4 n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p; // = 0;
    @DexIgnore
    public long q;
    @DexIgnore
    public tn4 r; // = new tn4();
    @DexIgnore
    public /* final */ tn4 s; // = new tn4();
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public /* final */ Socket u;
    @DexIgnore
    public /* final */ qn4 v;
    @DexIgnore
    public /* final */ j w;
    @DexIgnore
    public /* final */ Set<Integer> x; // = new LinkedHashSet();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            try {
                nn4.this.b(this.f, this.g);
            } catch (IOException unused) {
                nn4.this.y();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        public void b() {
            try {
                nn4.this.v.a(this.f, this.g);
            } catch (IOException unused) {
                nn4.this.y();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.f = i;
            this.g = list;
        }

        @DexIgnore
        public void b() {
            if (nn4.this.n.a(this.f, (List<jn4>) this.g)) {
                try {
                    nn4.this.v.a(this.f, ErrorCode.CANCEL);
                    synchronized (nn4.this) {
                        nn4.this.x.remove(Integer.valueOf(this.f));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i2, List list, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = list;
            this.h = z;
        }

        @DexIgnore
        public void b() {
            boolean a = nn4.this.n.a(this.f, this.g, this.h);
            if (a) {
                try {
                    nn4.this.v.a(this.f, ErrorCode.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.h) {
                synchronized (nn4.this) {
                    nn4.this.x.remove(Integer.valueOf(this.f));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ jo4 g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i2, jo4 jo4, int i3, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = jo4;
            this.h = i3;
            this.i = z;
        }

        @DexIgnore
        public void b() {
            try {
                boolean a = nn4.this.n.a(this.f, this.g, this.h, this.i);
                if (a) {
                    nn4.this.v.a(this.f, ErrorCode.CANCEL);
                }
                if (a || this.i) {
                    synchronized (nn4.this) {
                        nn4.this.x.remove(Integer.valueOf(this.f));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends im4 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            nn4.this.n.a(this.f, this.g);
            synchronized (nn4.this) {
                nn4.this.x.remove(Integer.valueOf(this.f));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class h {
        @DexIgnore
        public static /* final */ h a; // = new a();

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h {
            @DexIgnore
            public void a(pn4 pn4) throws IOException {
                pn4.a(ErrorCode.REFUSED_STREAM);
            }
        }

        @DexIgnore
        public void a(nn4 nn4) {
        }

        @DexIgnore
        public abstract void a(pn4 pn4) throws IOException;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i extends im4 {
        @DexIgnore
        public /* final */ boolean f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public i(boolean z, int i2, int i3) {
            super("OkHttp %s ping %08x%08x", nn4.this.h, Integer.valueOf(i2), Integer.valueOf(i3));
            this.f = z;
            this.g = i2;
            this.h = i3;
        }

        @DexIgnore
        public void b() {
            nn4.this.b(this.f, this.g, this.h);
        }
    }

    /*
    static {
        Class<nn4> cls = nn4.class;
    }
    */

    @DexIgnore
    public nn4(g gVar) {
        g gVar2 = gVar;
        this.n = gVar2.f;
        boolean z = gVar2.g;
        this.e = z;
        this.f = gVar2.e;
        this.j = z ? 1 : 2;
        if (gVar2.g) {
            this.j += 2;
        }
        if (gVar2.g) {
            this.r.a(7, 16777216);
        }
        this.h = gVar2.b;
        this.l = new ScheduledThreadPoolExecutor(1, jm4.a(jm4.a("OkHttp %s Writer", this.h), false));
        if (gVar2.h != 0) {
            ScheduledExecutorService scheduledExecutorService = this.l;
            i iVar = new i(false, 0, 0);
            int i2 = gVar2.h;
            scheduledExecutorService.scheduleAtFixedRate(iVar, (long) i2, (long) i2, TimeUnit.MILLISECONDS);
        }
        this.m = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), jm4.a(jm4.a("OkHttp %s Push Observer", this.h), true));
        this.s.a(7, 65535);
        this.s.a(5, RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        this.q = (long) this.s.c();
        this.u = gVar2.a;
        this.v = new qn4(gVar2.d, this.e);
        this.w = new j(new on4(gVar2.c, this.e));
    }

    @DexIgnore
    public synchronized int A() {
        return this.s.b(Integer.MAX_VALUE);
    }

    @DexIgnore
    public void B() throws IOException {
        a(true);
    }

    @DexIgnore
    public void c(int i2, ErrorCode errorCode) {
        try {
            this.l.execute(new a("OkHttp %s stream %d", new Object[]{this.h, Integer.valueOf(i2)}, i2, errorCode));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public boolean c(int i2) {
        return i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public void close() throws IOException {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    @DexIgnore
    public synchronized pn4 d(int i2) {
        pn4 remove;
        remove = this.g.remove(Integer.valueOf(i2));
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void flush() throws IOException {
        this.v.flush();
    }

    @DexIgnore
    public synchronized void h(long j2) {
        this.p += j2;
        if (this.p >= ((long) (this.r.c() / 2))) {
            c(0, this.p);
            this.p = 0;
        }
    }

    @DexIgnore
    public final void y() {
        try {
            a(ErrorCode.PROTOCOL_ERROR, ErrorCode.PROTOCOL_ERROR);
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public synchronized boolean z() {
        return this.k;
    }

    @DexIgnore
    public synchronized pn4 b(int i2) {
        return this.g.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public void c(int i2, long j2) {
        try {
            this.l.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.h, Integer.valueOf(i2)}, i2, j2));
        } catch (RejectedExecutionException unused) {
        }
    }

    @DexIgnore
    public pn4 a(List<jn4> list, boolean z) throws IOException {
        return a(0, list, z);
    }

    @DexIgnore
    public void b(int i2, ErrorCode errorCode) throws IOException {
        this.v.a(i2, errorCode);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public Socket a;
        @DexIgnore
        public String b;
        @DexIgnore
        public lo4 c;
        @DexIgnore
        public ko4 d;
        @DexIgnore
        public h e; // = h.a;
        @DexIgnore
        public sn4 f; // = sn4.a;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public int h;

        @DexIgnore
        public g(boolean z) {
            this.g = z;
        }

        @DexIgnore
        public g a(Socket socket, String str, lo4 lo4, ko4 ko4) {
            this.a = socket;
            this.b = str;
            this.c = lo4;
            this.d = ko4;
            return this;
        }

        @DexIgnore
        public g a(h hVar) {
            this.e = hVar;
            return this;
        }

        @DexIgnore
        public g a(int i) {
            this.h = i;
            return this;
        }

        @DexIgnore
        public nn4 a() {
            return new nn4(this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0043  */
    public final pn4 a(int i2, List<jn4> list, boolean z) throws IOException {
        int i3;
        pn4 pn4;
        boolean z2;
        boolean z3 = !z;
        synchronized (this.v) {
            synchronized (this) {
                if (this.j > 1073741823) {
                    a(ErrorCode.REFUSED_STREAM);
                }
                if (!this.k) {
                    i3 = this.j;
                    this.j += 2;
                    pn4 = new pn4(i3, this, z3, false, (yl4) null);
                    if (z && this.q != 0) {
                        if (pn4.b != 0) {
                            z2 = false;
                            if (pn4.g()) {
                                this.g.put(Integer.valueOf(i3), pn4);
                            }
                        }
                    }
                    z2 = true;
                    if (pn4.g()) {
                    }
                } else {
                    throw new ConnectionShutdownException();
                }
            }
            if (i2 == 0) {
                this.v.a(z3, i3, i2, list);
            } else if (!this.e) {
                this.v.a(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (z2) {
            this.v.flush();
        }
        return pn4;
    }

    @DexIgnore
    public void b(boolean z, int i2, int i3) {
        boolean z2;
        if (!z) {
            synchronized (this) {
                z2 = this.o;
                this.o = true;
            }
            if (z2) {
                y();
                return;
            }
        }
        try {
            this.v.a(z, i2, i3);
        } catch (IOException unused) {
            y();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends im4 implements on4.b {
        @DexIgnore
        public /* final */ on4 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends im4 {
            @DexIgnore
            public /* final */ /* synthetic */ pn4 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, pn4 pn4) {
                super(str, objArr);
                this.f = pn4;
            }

            @DexIgnore
            public void b() {
                try {
                    nn4.this.f.a(this.f);
                } catch (IOException e) {
                    ao4 d = ao4.d();
                    d.a(4, "Http2Connection.Listener failure for " + nn4.this.h, (Throwable) e);
                    try {
                        this.f.a(ErrorCode.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends im4 {
            @DexIgnore
            public b(String str, Object... objArr) {
                super(str, objArr);
            }

            @DexIgnore
            public void b() {
                nn4 nn4 = nn4.this;
                nn4.f.a(nn4);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class c extends im4 {
            @DexIgnore
            public /* final */ /* synthetic */ tn4 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(String str, Object[] objArr, tn4 tn4) {
                super(str, objArr);
                this.f = tn4;
            }

            @DexIgnore
            public void b() {
                try {
                    nn4.this.v.a(this.f);
                } catch (IOException unused) {
                    nn4.this.y();
                }
            }
        }

        @DexIgnore
        public j(on4 on4) {
            super("OkHttp %s", nn4.this.h);
            this.f = on4;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        public void a(boolean z, int i, lo4 lo4, int i2) throws IOException {
            if (nn4.this.c(i)) {
                nn4.this.a(i, lo4, i2, z);
                return;
            }
            pn4 b2 = nn4.this.b(i);
            if (b2 == null) {
                nn4.this.c(i, ErrorCode.PROTOCOL_ERROR);
                long j = (long) i2;
                nn4.this.h(j);
                lo4.skip(j);
                return;
            }
            b2.a(lo4, i2);
            if (z) {
                b2.i();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1 = okhttp3.internal.http2.ErrorCode.PROTOCOL_ERROR;
            r0 = okhttp3.internal.http2.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r2 = r4.g;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
            r2 = th;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x001c */
        public void b() {
            ErrorCode errorCode;
            nn4 nn4;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f.a((on4.b) this);
                while (this.f.a(false, (on4.b) this)) {
                }
                errorCode = ErrorCode.NO_ERROR;
                errorCode2 = ErrorCode.CANCEL;
                try {
                    nn4 = nn4.this;
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                errorCode = errorCode2;
            } catch (Throwable th) {
                th = th;
                errorCode = errorCode2;
                try {
                    nn4.this.a(errorCode, errorCode2);
                } catch (IOException unused3) {
                }
                jm4.a((Closeable) this.f);
                throw th;
            }
            nn4.a(errorCode, errorCode2);
            jm4.a((Closeable) this.f);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0074, code lost:
            r0.a(r13);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0077, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0079, code lost:
            r0.i();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
            return;
         */
        @DexIgnore
        public void a(boolean z, int i, int i2, List<jn4> list) {
            if (nn4.this.c(i)) {
                nn4.this.b(i, list, z);
                return;
            }
            synchronized (nn4.this) {
                pn4 b2 = nn4.this.b(i);
                if (b2 == null) {
                    if (!nn4.this.k) {
                        if (i > nn4.this.i) {
                            if (i % 2 != nn4.this.j % 2) {
                                int i3 = i;
                                pn4 pn4 = new pn4(i3, nn4.this, false, z, jm4.b(list));
                                nn4.this.i = i;
                                nn4.this.g.put(Integer.valueOf(i), pn4);
                                nn4.y.execute(new a("OkHttp %s stream %d", new Object[]{nn4.this.h, Integer.valueOf(i)}, pn4));
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
            if (nn4.this.c(i)) {
                nn4.this.a(i, errorCode);
                return;
            }
            pn4 d = nn4.this.d(i);
            if (d != null) {
                d.d(errorCode);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v13, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        public void a(boolean z, tn4 tn4) {
            pn4[] pn4Arr;
            long j;
            int i;
            synchronized (nn4.this) {
                int c2 = nn4.this.s.c();
                if (z) {
                    nn4.this.s.a();
                }
                nn4.this.s.a(tn4);
                a(tn4);
                int c3 = nn4.this.s.c();
                pn4Arr = null;
                if (c3 == -1 || c3 == c2) {
                    j = 0;
                } else {
                    j = (long) (c3 - c2);
                    if (!nn4.this.t) {
                        nn4.this.t = true;
                    }
                    if (!nn4.this.g.isEmpty()) {
                        pn4Arr = nn4.this.g.values().toArray(new pn4[nn4.this.g.size()]);
                    }
                }
                nn4.y.execute(new b("OkHttp %s settings", nn4.this.h));
            }
            if (pn4Arr != null && j != 0) {
                for (pn4 pn4 : pn4Arr) {
                    synchronized (pn4) {
                        pn4.a(j);
                    }
                }
            }
        }

        @DexIgnore
        public final void a(tn4 tn4) {
            try {
                nn4.this.l.execute(new c("OkHttp %s ACK Settings", new Object[]{nn4.this.h}, tn4));
            } catch (RejectedExecutionException unused) {
            }
        }

        @DexIgnore
        public void a(boolean z, int i, int i2) {
            if (z) {
                synchronized (nn4.this) {
                    boolean unused = nn4.this.o = false;
                    nn4.this.notifyAll();
                }
                return;
            }
            try {
                nn4.this.l.execute(new i(true, i, i2));
            } catch (RejectedExecutionException unused2) {
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            pn4[] pn4Arr;
            byteString.size();
            synchronized (nn4.this) {
                pn4Arr = (pn4[]) nn4.this.g.values().toArray(new pn4[nn4.this.g.size()]);
                nn4.this.k = true;
            }
            for (pn4 pn4 : pn4Arr) {
                if (pn4.c() > i && pn4.f()) {
                    pn4.d(ErrorCode.REFUSED_STREAM);
                    nn4.this.d(pn4.c());
                }
            }
        }

        @DexIgnore
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (nn4.this) {
                    nn4.this.q += j;
                    nn4.this.notifyAll();
                }
                return;
            }
            pn4 b2 = nn4.this.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.a(j);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, List<jn4> list) {
            nn4.this.a(i2, list);
        }
    }

    @DexIgnore
    public void b(int i2, List<jn4> list, boolean z) {
        try {
            a((im4) new d("OkHttp %s Push Headers[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, list, z));
        } catch (RejectedExecutionException unused) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.q), r8.v.r());
        r6 = (long) r3;
        r8.q -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        throw new java.io.InterruptedIOException();
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    public void a(int i2, boolean z, jo4 jo4, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.v.a(z, i2, jo4, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (true) {
                    if (this.q > 0) {
                        break;
                    } else if (this.g.containsKey(Integer.valueOf(i2))) {
                        wait();
                    } else {
                        throw new IOException("stream closed");
                    }
                }
            }
            j2 -= j3;
            this.v.a(z && j2 == 0, i2, jo4, min);
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        synchronized (this.v) {
            synchronized (this) {
                if (!this.k) {
                    this.k = true;
                    int i2 = this.i;
                    this.v.a(i2, errorCode, jm4.a);
                }
            }
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode, ErrorCode errorCode2) throws IOException {
        pn4[] pn4Arr = null;
        try {
            a(errorCode);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.g.isEmpty()) {
                pn4Arr = (pn4[]) this.g.values().toArray(new pn4[this.g.size()]);
                this.g.clear();
            }
        }
        if (pn4Arr != null) {
            for (pn4 a2 : pn4Arr) {
                try {
                    a2.a(errorCode2);
                } catch (IOException e3) {
                    if (e != null) {
                        e = e3;
                    }
                }
            }
        }
        try {
            this.v.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.u.close();
        } catch (IOException e5) {
            e = e5;
        }
        this.l.shutdown();
        this.m.shutdown();
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public void a(boolean z) throws IOException {
        if (z) {
            this.v.p();
            this.v.b(this.r);
            int c2 = this.r.c();
            if (c2 != 65535) {
                this.v.a(0, (long) (c2 - 65535));
            }
        }
        new Thread(this.w).start();
    }

    @DexIgnore
    public void a(int i2, List<jn4> list) {
        synchronized (this) {
            if (this.x.contains(Integer.valueOf(i2))) {
                c(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.x.add(Integer.valueOf(i2));
            try {
                a((im4) new c("OkHttp %s Push Request[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, list));
            } catch (RejectedExecutionException unused) {
            }
        }
    }

    @DexIgnore
    public void a(int i2, lo4 lo4, int i3, boolean z) throws IOException {
        jo4 jo4 = new jo4();
        long j2 = (long) i3;
        lo4.g(j2);
        lo4.b(jo4, j2);
        if (jo4.B() == j2) {
            a((im4) new e("OkHttp %s Push Data[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, jo4, i3, z));
            return;
        }
        throw new IOException(jo4.B() + " != " + i3);
    }

    @DexIgnore
    public void a(int i2, ErrorCode errorCode) {
        a((im4) new f("OkHttp %s Push Reset[%s]", new Object[]{this.h, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public final synchronized void a(im4 im4) {
        if (!z()) {
            this.m.execute(im4);
        }
    }
}
