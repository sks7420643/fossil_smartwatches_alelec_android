package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.SharedElementCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManagerImpl;
import com.fossil.blesdk.obfuscated.va;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cb {
    @DexIgnore
    public static /* final */ int[] a; // = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8};
    @DexIgnore
    public static /* final */ eb b; // = (Build.VERSION.SDK_INT >= 21 ? new db() : null);
    @DexIgnore
    public static /* final */ eb c; // = a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;

        @DexIgnore
        public a(ArrayList arrayList) {
            this.e = arrayList;
        }

        @DexIgnore
        public void run() {
            cb.a((ArrayList<View>) this.e, 4);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object e;
        @DexIgnore
        public /* final */ /* synthetic */ eb f;
        @DexIgnore
        public /* final */ /* synthetic */ View g;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment h;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList i;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList j;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList k;
        @DexIgnore
        public /* final */ /* synthetic */ Object l;

        @DexIgnore
        public b(Object obj, eb ebVar, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.e = obj;
            this.f = ebVar;
            this.g = view;
            this.h = fragment;
            this.i = arrayList;
            this.j = arrayList2;
            this.k = arrayList3;
            this.l = obj2;
        }

        @DexIgnore
        public void run() {
            Object obj = this.e;
            if (obj != null) {
                this.f.b(obj, this.g);
                this.j.addAll(cb.a(this.f, this.e, this.h, (ArrayList<View>) this.i, this.g));
            }
            if (this.k != null) {
                if (this.l != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(this.g);
                    this.f.a(this.l, (ArrayList<View>) this.k, (ArrayList<View>) arrayList);
                }
                this.k.clear();
                this.k.add(this.g);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment e;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;
        @DexIgnore
        public /* final */ /* synthetic */ g4 h;
        @DexIgnore
        public /* final */ /* synthetic */ View i;
        @DexIgnore
        public /* final */ /* synthetic */ eb j;
        @DexIgnore
        public /* final */ /* synthetic */ Rect k;

        @DexIgnore
        public c(Fragment fragment, Fragment fragment2, boolean z, g4 g4Var, View view, eb ebVar, Rect rect) {
            this.e = fragment;
            this.f = fragment2;
            this.g = z;
            this.h = g4Var;
            this.i = view;
            this.j = ebVar;
            this.k = rect;
        }

        @DexIgnore
        public void run() {
            cb.a(this.e, this.f, this.g, (g4<String, View>) this.h, false);
            View view = this.i;
            if (view != null) {
                this.j.a(view, this.k);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ eb e;
        @DexIgnore
        public /* final */ /* synthetic */ g4 f;
        @DexIgnore
        public /* final */ /* synthetic */ Object g;
        @DexIgnore
        public /* final */ /* synthetic */ e h;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList i;
        @DexIgnore
        public /* final */ /* synthetic */ View j;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment k;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment l;
        @DexIgnore
        public /* final */ /* synthetic */ boolean m;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList n;
        @DexIgnore
        public /* final */ /* synthetic */ Object o;
        @DexIgnore
        public /* final */ /* synthetic */ Rect p;

        @DexIgnore
        public d(eb ebVar, g4 g4Var, Object obj, e eVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.e = ebVar;
            this.f = g4Var;
            this.g = obj;
            this.h = eVar;
            this.i = arrayList;
            this.j = view;
            this.k = fragment;
            this.l = fragment2;
            this.m = z;
            this.n = arrayList2;
            this.o = obj2;
            this.p = rect;
        }

        @DexIgnore
        public void run() {
            g4<String, View> a = cb.a(this.e, (g4<String, String>) this.f, this.g, this.h);
            if (a != null) {
                this.i.addAll(a.values());
                this.i.add(this.j);
            }
            cb.a(this.k, this.l, this.m, a, false);
            Object obj = this.g;
            if (obj != null) {
                this.e.b(obj, (ArrayList<View>) this.n, (ArrayList<View>) this.i);
                View a2 = cb.a(a, this.h, this.o, this.m);
                if (a2 != null) {
                    this.e.a(a2, this.p);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public Fragment a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public va c;
        @DexIgnore
        public Fragment d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public va f;
    }

    @DexIgnore
    public static eb a() {
        try {
            return (eb) Class.forName("com.fossil.blesdk.obfuscated.wg").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static void b(FragmentManagerImpl fragmentManagerImpl, int i, e eVar, View view, g4<String, String> g4Var) {
        Object obj;
        FragmentManagerImpl fragmentManagerImpl2 = fragmentManagerImpl;
        e eVar2 = eVar;
        View view2 = view;
        ViewGroup viewGroup = fragmentManagerImpl2.r.a() ? (ViewGroup) fragmentManagerImpl2.r.a(i) : null;
        if (viewGroup != null) {
            Fragment fragment = eVar2.a;
            Fragment fragment2 = eVar2.d;
            eb a2 = a(fragment2, fragment);
            if (a2 != null) {
                boolean z = eVar2.b;
                boolean z2 = eVar2.e;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                Object a3 = a(a2, fragment, z);
                Object b2 = b(a2, fragment2, z2);
                ViewGroup viewGroup2 = viewGroup;
                ArrayList arrayList3 = arrayList2;
                Object b3 = b(a2, viewGroup, view, g4Var, eVar, arrayList2, arrayList, a3, b2);
                Object obj2 = a3;
                if (obj2 == null && b3 == null) {
                    obj = b2;
                    if (obj == null) {
                        return;
                    }
                } else {
                    obj = b2;
                }
                ArrayList<View> a4 = a(a2, obj, fragment2, (ArrayList<View>) arrayList3, view2);
                ArrayList<View> a5 = a(a2, obj2, fragment, (ArrayList<View>) arrayList, view2);
                a(a5, 4);
                Fragment fragment3 = fragment;
                ArrayList<View> arrayList4 = a4;
                Object a6 = a(a2, obj2, obj, b3, fragment3, z);
                if (a6 != null) {
                    a(a2, obj, fragment2, arrayList4);
                    ArrayList<String> a7 = a2.a((ArrayList<View>) arrayList);
                    a2.a(a6, obj2, a5, obj, arrayList4, b3, arrayList);
                    ViewGroup viewGroup3 = viewGroup2;
                    a2.a(viewGroup3, a6);
                    a2.a(viewGroup3, arrayList3, arrayList, a7, g4Var);
                    a(a5, 0);
                    a2.b(b3, (ArrayList<View>) arrayList3, (ArrayList<View>) arrayList);
                }
            }
        }
    }

    @DexIgnore
    public static void a(FragmentManagerImpl fragmentManagerImpl, ArrayList<va> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z) {
        if (fragmentManagerImpl.p >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                va vaVar = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    b(vaVar, (SparseArray<e>) sparseArray, z);
                } else {
                    a(vaVar, (SparseArray<e>) sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(fragmentManagerImpl.q.c());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    g4<String, String> a2 = a(keyAt, arrayList, arrayList2, i, i2);
                    e eVar = (e) sparseArray.valueAt(i4);
                    if (z) {
                        b(fragmentManagerImpl, keyAt, eVar, view, a2);
                    } else {
                        a(fragmentManagerImpl, keyAt, eVar, view, a2);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static g4<String, String> a(int i, ArrayList<va> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        g4<String, String> g4Var = new g4<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            va vaVar = arrayList.get(i4);
            if (vaVar.b(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = vaVar.r;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = vaVar.r;
                        arrayList4 = vaVar.s;
                    } else {
                        ArrayList<String> arrayList6 = vaVar.r;
                        arrayList3 = vaVar.s;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = g4Var.remove(str2);
                        if (remove != null) {
                            g4Var.put(str, remove);
                        } else {
                            g4Var.put(str, str2);
                        }
                    }
                }
            }
        }
        return g4Var;
    }

    @DexIgnore
    public static Object b(eb ebVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return ebVar.b(obj);
    }

    @DexIgnore
    public static Object b(eb ebVar, ViewGroup viewGroup, View view, g4<String, String> g4Var, e eVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        Rect rect;
        View view2;
        eb ebVar2 = ebVar;
        View view3 = view;
        g4<String, String> g4Var2 = g4Var;
        e eVar2 = eVar;
        ArrayList<View> arrayList3 = arrayList;
        ArrayList<View> arrayList4 = arrayList2;
        Object obj5 = obj;
        Fragment fragment = eVar2.a;
        Fragment fragment2 = eVar2.d;
        if (fragment != null) {
            fragment.getView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.b;
        if (g4Var.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = a(ebVar, fragment, fragment2, z);
        }
        g4<String, View> b2 = b(ebVar, g4Var2, obj3, eVar2);
        g4<String, View> a2 = a(ebVar, g4Var2, obj3, eVar2);
        if (g4Var.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (a2 != null) {
                a2.clear();
            }
            obj4 = null;
        } else {
            a(arrayList3, b2, (Collection<String>) g4Var.keySet());
            a(arrayList4, a2, g4Var.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            arrayList4.add(view3);
            ebVar.b(obj4, view3, arrayList3);
            a(ebVar, obj4, obj2, b2, eVar2.e, eVar2.f);
            Rect rect2 = new Rect();
            View a3 = a(a2, eVar2, obj5, z);
            if (a3 != null) {
                ebVar.a(obj5, rect2);
            }
            rect = rect2;
            view2 = a3;
        } else {
            view2 = null;
            rect = null;
        }
        gb.a(viewGroup, new c(fragment, fragment2, z, a2, view2, ebVar, rect));
        return obj4;
    }

    @DexIgnore
    public static void a(eb ebVar, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            ebVar.a(obj, fragment.getView(), arrayList);
            gb.a(fragment.mContainer, new a(arrayList));
        }
    }

    @DexIgnore
    public static void a(FragmentManagerImpl fragmentManagerImpl, int i, e eVar, View view, g4<String, String> g4Var) {
        Object obj;
        FragmentManagerImpl fragmentManagerImpl2 = fragmentManagerImpl;
        e eVar2 = eVar;
        View view2 = view;
        g4<String, String> g4Var2 = g4Var;
        ViewGroup viewGroup = fragmentManagerImpl2.r.a() ? (ViewGroup) fragmentManagerImpl2.r.a(i) : null;
        if (viewGroup != null) {
            Fragment fragment = eVar2.a;
            Fragment fragment2 = eVar2.d;
            eb a2 = a(fragment2, fragment);
            if (a2 != null) {
                boolean z = eVar2.b;
                boolean z2 = eVar2.e;
                Object a3 = a(a2, fragment, z);
                Object b2 = b(a2, fragment2, z2);
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = arrayList;
                Object obj2 = b2;
                eb ebVar = a2;
                Object a4 = a(a2, viewGroup, view, g4Var, eVar, (ArrayList<View>) arrayList, (ArrayList<View>) arrayList2, a3, obj2);
                Object obj3 = a3;
                if (obj3 == null && a4 == null) {
                    obj = obj2;
                    if (obj == null) {
                        return;
                    }
                } else {
                    obj = obj2;
                }
                ArrayList<View> a5 = a(ebVar, obj, fragment2, (ArrayList<View>) arrayList3, view2);
                Object obj4 = (a5 == null || a5.isEmpty()) ? null : obj;
                ebVar.a(obj3, view2);
                Object a6 = a(ebVar, obj3, obj4, a4, fragment, eVar2.b);
                if (a6 != null) {
                    ArrayList arrayList4 = new ArrayList();
                    eb ebVar2 = ebVar;
                    ebVar2.a(a6, obj3, arrayList4, obj4, a5, a4, arrayList2);
                    a(ebVar2, viewGroup, fragment, view, (ArrayList<View>) arrayList2, obj3, (ArrayList<View>) arrayList4, obj4, a5);
                    ArrayList arrayList5 = arrayList2;
                    ebVar.a((View) viewGroup, (ArrayList<View>) arrayList5, (Map<String, String>) g4Var2);
                    ebVar.a(viewGroup, a6);
                    ebVar.a(viewGroup, (ArrayList<View>) arrayList5, (Map<String, String>) g4Var2);
                }
            }
        }
    }

    @DexIgnore
    public static g4<String, View> b(eb ebVar, g4<String, String> g4Var, Object obj, e eVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        if (g4Var.isEmpty() || obj == null) {
            g4Var.clear();
            return null;
        }
        Fragment fragment = eVar.d;
        g4<String, View> g4Var2 = new g4<>();
        ebVar.a((Map<String, View>) g4Var2, fragment.getView());
        va vaVar = eVar.f;
        if (eVar.e) {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = vaVar.s;
        } else {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = vaVar.r;
        }
        g4Var2.a(arrayList);
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) g4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = g4Var2.get(str);
                if (view == null) {
                    g4Var.remove(str);
                } else if (!str.equals(f9.q(view))) {
                    g4Var.put(f9.q(view), g4Var.remove(str));
                }
            }
        } else {
            g4Var.a(g4Var2.keySet());
        }
        return g4Var2;
    }

    @DexIgnore
    public static void a(eb ebVar, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        ViewGroup viewGroup2 = viewGroup;
        gb.a(viewGroup, new b(obj, ebVar, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }

    @DexIgnore
    public static eb a(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        eb ebVar = b;
        if (ebVar != null && a(ebVar, (List<Object>) arrayList)) {
            return b;
        }
        eb ebVar2 = c;
        if (ebVar2 != null && a(ebVar2, (List<Object>) arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    @DexIgnore
    public static void b(va vaVar, SparseArray<e> sparseArray, boolean z) {
        if (vaVar.a.r.a()) {
            for (int size = vaVar.b.size() - 1; size >= 0; size--) {
                a(vaVar, vaVar.b.get(size), sparseArray, true, z);
            }
        }
    }

    @DexIgnore
    public static boolean a(eb ebVar, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!ebVar.a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static Object a(eb ebVar, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return ebVar.c(ebVar.b(obj));
    }

    @DexIgnore
    public static Object a(eb ebVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return ebVar.b(obj);
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, g4<String, View> g4Var, Collection<String> collection) {
        for (int size = g4Var.size() - 1; size >= 0; size--) {
            View e2 = g4Var.e(size);
            if (collection.contains(f9.q(e2))) {
                arrayList.add(e2);
            }
        }
    }

    @DexIgnore
    public static Object a(eb ebVar, ViewGroup viewGroup, View view, g4<String, String> g4Var, e eVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        g4<String, String> g4Var2;
        Object obj3;
        Object obj4;
        Rect rect;
        eb ebVar2 = ebVar;
        e eVar2 = eVar;
        ArrayList<View> arrayList3 = arrayList;
        Object obj5 = obj;
        Fragment fragment = eVar2.a;
        Fragment fragment2 = eVar2.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.b;
        if (g4Var.isEmpty()) {
            g4Var2 = g4Var;
            obj3 = null;
        } else {
            obj3 = a(ebVar2, fragment, fragment2, z);
            g4Var2 = g4Var;
        }
        g4<String, View> b2 = b(ebVar2, g4Var2, obj3, eVar2);
        if (g4Var.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(b2.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            rect = new Rect();
            ebVar2.b(obj4, view, arrayList3);
            a(ebVar, obj4, obj2, b2, eVar2.e, eVar2.f);
            if (obj5 != null) {
                ebVar2.a(obj5, rect);
            }
        } else {
            rect = null;
        }
        d dVar = r0;
        d dVar2 = new d(ebVar, g4Var, obj4, eVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect);
        gb.a(viewGroup, dVar);
        return obj4;
    }

    @DexIgnore
    public static g4<String, View> a(eb ebVar, g4<String, String> g4Var, Object obj, e eVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        Fragment fragment = eVar.a;
        View view = fragment.getView();
        if (g4Var.isEmpty() || obj == null || view == null) {
            g4Var.clear();
            return null;
        }
        g4<String, View> g4Var2 = new g4<>();
        ebVar.a((Map<String, View>) g4Var2, view);
        va vaVar = eVar.c;
        if (eVar.b) {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = vaVar.r;
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = vaVar.s;
        }
        if (arrayList != null) {
            g4Var2.a(arrayList);
            g4Var2.a(g4Var.values());
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) g4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = g4Var2.get(str);
                if (view2 == null) {
                    String a2 = a(g4Var, str);
                    if (a2 != null) {
                        g4Var.remove(a2);
                    }
                } else if (!str.equals(f9.q(view2))) {
                    String a3 = a(g4Var, str);
                    if (a3 != null) {
                        g4Var.put(a3, f9.q(view2));
                    }
                }
            }
        } else {
            a(g4Var, g4Var2);
        }
        return g4Var2;
    }

    @DexIgnore
    public static String a(g4<String, String> g4Var, String str) {
        int size = g4Var.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(g4Var.e(i))) {
                return g4Var.c(i);
            }
        }
        return null;
    }

    @DexIgnore
    public static View a(g4<String, View> g4Var, e eVar, Object obj, boolean z) {
        String str;
        va vaVar = eVar.c;
        if (obj == null || g4Var == null) {
            return null;
        }
        ArrayList<String> arrayList = vaVar.r;
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = vaVar.r.get(0);
        } else {
            str = vaVar.s.get(0);
        }
        return g4Var.get(str);
    }

    @DexIgnore
    public static void a(eb ebVar, Object obj, Object obj2, g4<String, View> g4Var, boolean z, va vaVar) {
        String str;
        ArrayList<String> arrayList = vaVar.r;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = vaVar.s.get(0);
            } else {
                str = vaVar.r.get(0);
            }
            View view = g4Var.get(str);
            ebVar.c(obj, view);
            if (obj2 != null) {
                ebVar.c(obj2, view);
            }
        }
    }

    @DexIgnore
    public static void a(g4<String, String> g4Var, g4<String, View> g4Var2) {
        for (int size = g4Var.size() - 1; size >= 0; size--) {
            if (!g4Var2.containsKey(g4Var.e(size))) {
                g4Var.d(size);
            }
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, Fragment fragment2, boolean z, g4<String, View> g4Var, boolean z2) {
        SharedElementCallback sharedElementCallback;
        int i;
        if (z) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (g4Var == null) {
                i = 0;
            } else {
                i = g4Var.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(g4Var.c(i2));
                arrayList.add(g4Var.e(i2));
            }
            if (z2) {
                sharedElementCallback.b(arrayList2, arrayList, (List<View>) null);
            } else {
                sharedElementCallback.a((List<String>) arrayList2, (List<View>) arrayList, (List<View>) null);
            }
        }
    }

    @DexIgnore
    public static ArrayList<View> a(eb ebVar, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            ebVar.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        ebVar.a(obj, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    @DexIgnore
    public static Object a(eb ebVar, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return ebVar.b(obj2, obj, obj3);
        }
        return ebVar.a(obj2, obj, obj3);
    }

    @DexIgnore
    public static void a(va vaVar, SparseArray<e> sparseArray, boolean z) {
        int size = vaVar.b.size();
        for (int i = 0; i < size; i++) {
            a(vaVar, vaVar.b.get(i), sparseArray, false, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r10.mAdded != false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0074, code lost:
        if (r10.mPostponedAlpha >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0076, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0080, code lost:
        if (r10.mHidden == false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0092, code lost:
        if (r10.mHidden == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0094, code lost:
        r1 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00e7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public static void a(va vaVar, va.a aVar, SparseArray<e> sparseArray, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        e eVar;
        FragmentManagerImpl fragmentManagerImpl;
        boolean z6;
        va vaVar2 = vaVar;
        va.a aVar2 = aVar;
        SparseArray<e> sparseArray2 = sparseArray;
        boolean z7 = z;
        Fragment fragment = aVar2.b;
        if (fragment != null) {
            int i = fragment.mContainerId;
            if (i != 0) {
                int i2 = z7 ? a[aVar2.a] : aVar2.a;
                boolean z8 = false;
                if (i2 != 1) {
                    if (i2 != 3) {
                        if (i2 != 4) {
                            if (i2 != 5) {
                                if (i2 != 6) {
                                    if (i2 != 7) {
                                        z5 = false;
                                        z4 = false;
                                        z3 = false;
                                        e eVar2 = sparseArray2.get(i);
                                        if (z8) {
                                            eVar2 = a(eVar2, sparseArray2, i);
                                            eVar2.a = fragment;
                                            eVar2.b = z7;
                                            eVar2.c = vaVar2;
                                        }
                                        eVar = eVar2;
                                        if (!z2 && z5) {
                                            if (eVar != null && eVar.d == fragment) {
                                                eVar.d = null;
                                            }
                                            fragmentManagerImpl = vaVar2.a;
                                            if (fragment.mState < 1 && fragmentManagerImpl.p >= 1 && !vaVar2.t) {
                                                fragmentManagerImpl.g(fragment);
                                                fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                                            }
                                        }
                                        if (z3 && (eVar == null || eVar.d == null)) {
                                            eVar = a(eVar, sparseArray2, i);
                                            eVar.d = fragment;
                                            eVar.e = z7;
                                            eVar.f = vaVar2;
                                        }
                                        if (z2 && z4 && eVar != null && eVar.a == fragment) {
                                            eVar.a = null;
                                            return;
                                        }
                                        return;
                                    }
                                }
                            } else if (z2) {
                                if (fragment.mHiddenChanged) {
                                    if (!fragment.mHidden) {
                                    }
                                }
                                z6 = false;
                                z8 = z6;
                                z5 = true;
                                z4 = false;
                                z3 = false;
                                e eVar22 = sparseArray2.get(i);
                                if (z8) {
                                }
                                eVar = eVar22;
                                eVar.d = null;
                                fragmentManagerImpl = vaVar2.a;
                                fragmentManagerImpl.g(fragment);
                                fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                                eVar = a(eVar, sparseArray2, i);
                                eVar.d = fragment;
                                eVar.e = z7;
                                eVar.f = vaVar2;
                                if (z2) {
                                    return;
                                }
                                return;
                            } else {
                                z6 = fragment.mHidden;
                                z8 = z6;
                                z5 = true;
                                z4 = false;
                                z3 = false;
                                e eVar222 = sparseArray2.get(i);
                                if (z8) {
                                }
                                eVar = eVar222;
                                eVar.d = null;
                                fragmentManagerImpl = vaVar2.a;
                                fragmentManagerImpl.g(fragment);
                                fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                                eVar = a(eVar, sparseArray2, i);
                                eVar.d = fragment;
                                eVar.e = z7;
                                eVar.f = vaVar2;
                                if (z2) {
                                }
                            }
                        } else if (!z2) {
                            boolean z9 = false;
                            z3 = z9;
                            z5 = false;
                            z4 = true;
                            e eVar2222 = sparseArray2.get(i);
                            if (z8) {
                            }
                            eVar = eVar2222;
                            eVar.d = null;
                            fragmentManagerImpl = vaVar2.a;
                            fragmentManagerImpl.g(fragment);
                            fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                            eVar = a(eVar, sparseArray2, i);
                            eVar.d = fragment;
                            eVar.e = z7;
                            eVar.f = vaVar2;
                            if (z2) {
                            }
                        } else {
                            boolean z92 = false;
                            z3 = z92;
                            z5 = false;
                            z4 = true;
                            e eVar22222 = sparseArray2.get(i);
                            if (z8) {
                            }
                            eVar = eVar22222;
                            eVar.d = null;
                            fragmentManagerImpl = vaVar2.a;
                            fragmentManagerImpl.g(fragment);
                            fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                            eVar = a(eVar, sparseArray2, i);
                            eVar.d = fragment;
                            eVar.e = z7;
                            eVar.f = vaVar2;
                            if (z2) {
                            }
                        }
                    }
                    if (z2) {
                        if (!fragment.mAdded) {
                            View view = fragment.mView;
                            if (view != null) {
                                if (view.getVisibility() == 0) {
                                }
                            }
                        }
                        boolean z922 = false;
                        z3 = z922;
                        z5 = false;
                        z4 = true;
                        e eVar222222 = sparseArray2.get(i);
                        if (z8) {
                        }
                        eVar = eVar222222;
                        eVar.d = null;
                        fragmentManagerImpl = vaVar2.a;
                        fragmentManagerImpl.g(fragment);
                        fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                        eVar = a(eVar, sparseArray2, i);
                        eVar.d = fragment;
                        eVar.e = z7;
                        eVar.f = vaVar2;
                        if (z2) {
                        }
                    } else {
                        if (fragment.mAdded) {
                        }
                        boolean z9222 = false;
                        z3 = z9222;
                        z5 = false;
                        z4 = true;
                        e eVar2222222 = sparseArray2.get(i);
                        if (z8) {
                        }
                        eVar = eVar2222222;
                        eVar.d = null;
                        fragmentManagerImpl = vaVar2.a;
                        fragmentManagerImpl.g(fragment);
                        fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                        eVar = a(eVar, sparseArray2, i);
                        eVar.d = fragment;
                        eVar.e = z7;
                        eVar.f = vaVar2;
                        if (z2) {
                        }
                    }
                }
                if (z2) {
                    z6 = fragment.mIsNewlyAdded;
                    z8 = z6;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    e eVar22222222 = sparseArray2.get(i);
                    if (z8) {
                    }
                    eVar = eVar22222222;
                    eVar.d = null;
                    fragmentManagerImpl = vaVar2.a;
                    fragmentManagerImpl.g(fragment);
                    fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                    eVar = a(eVar, sparseArray2, i);
                    eVar.d = fragment;
                    eVar.e = z7;
                    eVar.f = vaVar2;
                    if (z2) {
                    }
                } else {
                    if (!fragment.mAdded) {
                    }
                    z6 = false;
                    z8 = z6;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    e eVar222222222 = sparseArray2.get(i);
                    if (z8) {
                    }
                    eVar = eVar222222222;
                    eVar.d = null;
                    fragmentManagerImpl = vaVar2.a;
                    fragmentManagerImpl.g(fragment);
                    fragmentManagerImpl.a(fragment, 1, 0, 0, false);
                    eVar = a(eVar, sparseArray2, i);
                    eVar.d = fragment;
                    eVar.e = z7;
                    eVar.f = vaVar2;
                    if (z2) {
                    }
                }
            }
        }
    }

    @DexIgnore
    public static e a(e eVar, SparseArray<e> sparseArray, int i) {
        if (eVar != null) {
            return eVar;
        }
        e eVar2 = new e();
        sparseArray.put(i, eVar2);
        return eVar2;
    }
}
