package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a93 extends zr2 implements z83, jt2, ks2 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<wa2> j;
    @DexIgnore
    public y83 k;
    @DexIgnore
    public et2 l;
    @DexIgnore
    public ActivityOverviewFragment m;
    @DexIgnore
    public bu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return a93.p;
        }

        @DexIgnore
        public final a93 b() {
            return new a93();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends bu3 {
        @DexIgnore
        public /* final */ /* synthetic */ a93 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, a93 a93, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = a93;
        }

        @DexIgnore
        public void a(int i) {
            a93.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = a93.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "DashboardActivityFragment::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ y83 a(a93 a93) {
        y83 y83 = a93.k;
        if (y83 != null) {
            return y83;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
            }
            if (isVisible() && this.j != null) {
                wa2 T0 = T0();
                if (T0 != null) {
                    RecyclerView recyclerView = T0.q;
                    if (recyclerView != null) {
                        RecyclerView.ViewHolder c = recyclerView.c(0);
                        if (c != null) {
                            View view = c.itemView;
                            if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                return;
                            }
                        }
                        recyclerView.j(0);
                        bu3 bu3 = this.n;
                        if (bu3 != null) {
                            bu3.a();
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        vl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final wa2 T0() {
        tr3<wa2> tr3 = this.j;
        if (tr3 != null) {
            return tr3.a();
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001a  */
    public void b(Unit unit) {
        RecyclerView.m mVar;
        kd4.b(unit, MFUser.DISTANCE_UNIT);
        wa2 T0 = T0();
        if (T0 != null) {
            RecyclerView recyclerView = T0.q;
            if (recyclerView != null) {
                mVar = recyclerView.getLayoutManager();
                if (!(mVar instanceof LinearLayoutManager)) {
                    et2 et2 = this.l;
                    if (et2 != null) {
                        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mVar;
                        et2.a(unit, linearLayoutManager.H(), linearLayoutManager.J());
                        return;
                    }
                    kd4.d("mDashboardActivitiesAdapter");
                    throw null;
                }
                return;
            }
        }
        mVar = null;
        if (!(mVar instanceof LinearLayoutManager)) {
        }
    }

    @DexIgnore
    public void f() {
        bu3 bu3 = this.n;
        if (bu3 != null) {
            bu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (wa2) qa.a(layoutInflater, R.layout.fragment_dashboard_activity, viewGroup, false, O0()));
        tr3<wa2> tr3 = this.j;
        if (tr3 != null) {
            wa2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(p, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        y83 y83 = this.k;
        if (y83 != null) {
            y83.i();
            super.onDestroyView();
            N0();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        y83 y83 = this.k;
        if (y83 != null) {
            y83.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        y83 y83 = this.k;
        if (y83 != null) {
            y83.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m = (ActivityOverviewFragment) getChildFragmentManager().a("ActivityOverviewFragment");
        if (this.m == null) {
            this.m = new ActivityOverviewFragment();
        }
        ct2 ct2 = new ct2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        ActivityOverviewFragment activityOverviewFragment = this.m;
        if (activityOverviewFragment != null) {
            this.l = new et2(ct2, c, this, childFragmentManager, activityOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            wa2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    et2 et2 = this.l;
                    if (et2 != null) {
                        recyclerView.setAdapter(et2);
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            this.n = new b(recyclerView, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                            bu3 bu3 = this.n;
                            if (bu3 != null) {
                                recyclerView.a((RecyclerView.q) bu3);
                                recyclerView.setItemViewCacheSize(0);
                                l73 l73 = new l73(linearLayoutManager.M());
                                Drawable c2 = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_dashboard_line_1w);
                                if (c2 != null) {
                                    kd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                    l73.a(c2);
                                    recyclerView.a((RecyclerView.l) l73);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                    } else {
                        kd4.d("mDashboardActivitiesAdapter");
                        throw null;
                    }
                }
            }
            wa2 T02 = T0();
            if (T02 != null) {
                RecyclerView recyclerView2 = T02.q;
                if (recyclerView2 != null) {
                    kd4.a((Object) recyclerView2, "recyclerView");
                    RecyclerView.j itemAnimator = recyclerView2.getItemAnimator();
                    if (itemAnimator instanceof af) {
                        ((af) itemAnimator).setSupportsChangeAnimations(false);
                    }
                }
            }
            R("steps_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ic a2 = lc.a(activity).a(iu3.class);
                kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                iu3 iu3 = (iu3) a2;
            }
            y83 y83 = this.k;
            if (y83 != null) {
                y83.h();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(qd<ActivitySummary> qdVar) {
        et2 et2 = this.l;
        if (et2 != null) {
            et2.c(qdVar);
        } else {
            kd4.d("mDashboardActivitiesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(y83 y83) {
        kd4.b(y83, "presenter");
        this.k = y83;
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked - date=" + date);
        Context context = getContext();
        if (context != null) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.D;
            kd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        kd4.b(date, "startWeekDate");
        kd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
