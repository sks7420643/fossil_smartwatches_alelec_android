package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xk */
public class C3294xk {

    @DexIgnore
    /* renamed from: e */
    public static com.fossil.blesdk.obfuscated.C3294xk f10996e;

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2812rk f10997a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2895sk f10998b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C3120vk f10999c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3227wk f11000d;

    @DexIgnore
    public C3294xk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        android.content.Context applicationContext = context.getApplicationContext();
        this.f10997a = new com.fossil.blesdk.obfuscated.C2812rk(applicationContext, zlVar);
        this.f10998b = new com.fossil.blesdk.obfuscated.C2895sk(applicationContext, zlVar);
        this.f10999c = new com.fossil.blesdk.obfuscated.C3120vk(applicationContext, zlVar);
        this.f11000d = new com.fossil.blesdk.obfuscated.C3227wk(applicationContext, zlVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized com.fossil.blesdk.obfuscated.C3294xk m16376a(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        com.fossil.blesdk.obfuscated.C3294xk xkVar;
        synchronized (com.fossil.blesdk.obfuscated.C3294xk.class) {
            if (f10996e == null) {
                f10996e = new com.fossil.blesdk.obfuscated.C3294xk(context, zlVar);
            }
            xkVar = f10996e;
        }
        return xkVar;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2895sk mo17769b() {
        return this.f10998b;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C3120vk mo17770c() {
        return this.f10999c;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3227wk mo17771d() {
        return this.f11000d;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2812rk mo17768a() {
        return this.f10997a;
    }
}
