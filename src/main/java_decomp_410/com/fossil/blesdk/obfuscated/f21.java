package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface f21<DT> {
    @DexIgnore
    boolean a(DT dt, int i);

    @DexIgnore
    boolean a(String str);

    @DexIgnore
    String b(DT dt, int i);

    @DexIgnore
    int c(DT dt, int i);

    @DexIgnore
    int zzc(DT dt);

    @DexIgnore
    String zzd(DT dt);
}
