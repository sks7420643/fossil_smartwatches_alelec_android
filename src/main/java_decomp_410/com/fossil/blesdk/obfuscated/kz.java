package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kz implements e00 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public kz(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    @DexIgnore
    public String a() {
        try {
            Bundle bundle = this.a.getPackageManager().getApplicationInfo(this.b, 128).metaData;
            if (bundle != null) {
                return bundle.getString("io.fabric.unity.crashlytics.version");
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
