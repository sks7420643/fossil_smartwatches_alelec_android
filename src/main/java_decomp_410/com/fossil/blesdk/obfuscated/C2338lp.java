package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lp */
public class C2338lp<DataType> implements com.fossil.blesdk.obfuscated.C2981tq.C2983b {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1963ho<DataType> f7253a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ DataType f7254b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2337lo f7255c;

    @DexIgnore
    public C2338lp(com.fossil.blesdk.obfuscated.C1963ho<DataType> hoVar, DataType datatype, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        this.f7253a = hoVar;
        this.f7254b = datatype;
        this.f7255c = loVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13328a(java.io.File file) {
        return this.f7253a.mo11698a(this.f7254b, file, this.f7255c);
    }
}
