package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eh0 {
    @DexIgnore
    public /* final */ bf0<de0.b, ?> a;
    @DexIgnore
    public /* final */ hf0<de0.b, ?> b;

    @DexIgnore
    public eh0(bf0<de0.b, ?> bf0, hf0<de0.b, ?> hf0) {
        this.a = bf0;
        this.b = hf0;
    }
}
