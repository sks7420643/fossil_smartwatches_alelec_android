package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zr3 implements MembersInjector<xr3> {
    @DexIgnore
    public static void a(xr3 xr3, DeviceRepository deviceRepository) {
        xr3.h = deviceRepository;
    }

    @DexIgnore
    public static void a(xr3 xr3, NotificationSettingsDatabase notificationSettingsDatabase) {
        xr3.i = notificationSettingsDatabase;
    }
}
