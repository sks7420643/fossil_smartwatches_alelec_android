package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fr0<T> implements ir0<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public fr0(T t) {
        this.a = t;
    }

    @DexIgnore
    public final T get() {
        return this.a;
    }
}
