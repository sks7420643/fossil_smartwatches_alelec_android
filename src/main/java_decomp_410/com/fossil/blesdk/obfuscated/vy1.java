package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.facebook.appevents.AppEventsConstants;
import com.google.firebase.FirebaseApp;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.io.IOException;
import java.util.concurrent.Executor;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vy1 implements dx1 {
    @DexIgnore
    public /* final */ FirebaseApp a;
    @DexIgnore
    public /* final */ rx1 b;
    @DexIgnore
    public /* final */ xx1 c;
    @DexIgnore
    public /* final */ Executor d;

    @DexIgnore
    public vy1(FirebaseApp firebaseApp, rx1 rx1, Executor executor) {
        this(firebaseApp, rx1, executor, new xx1(firebaseApp.a(), rx1));
    }

    @DexIgnore
    public final wn1<String> a(String str, String str2, String str3, String str4) {
        return b(a(str, str3, str4, new Bundle()));
    }

    @DexIgnore
    public final boolean a() {
        return false;
    }

    @DexIgnore
    public final boolean b() {
        return this.b.a() != 0;
    }

    @DexIgnore
    public vy1(FirebaseApp firebaseApp, rx1 rx1, Executor executor, xx1 xx1) {
        this.a = firebaseApp;
        this.b = rx1;
        this.c = xx1;
        this.d = executor;
    }

    @DexIgnore
    public final wn1<Void> b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @DexIgnore
    public final wn1<Void> a(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("iid-operation", "delete");
        bundle.putString("delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        return a(b(a(str, Marker.ANY_MARKER, Marker.ANY_MARKER, bundle)));
    }

    @DexIgnore
    public final wn1<String> b(wn1<Bundle> wn1) {
        return wn1.a(this.d, (pn1<Bundle, TContinuationResult>) new yy1(this));
    }

    @DexIgnore
    public final wn1<Void> a(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @DexIgnore
    public final wn1<Bundle> a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString(RemoteFLogger.MESSAGE_SENDER_KEY, str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.a.c().a());
        bundle.putString("gmsv", Integer.toString(this.b.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.b());
        bundle.putString("app_ver_name", this.b.c());
        bundle.putString("cliv", "fiid-12451000");
        xn1 xn1 = new xn1();
        this.d.execute(new wy1(this, bundle, xn1));
        return xn1.a();
    }

    @DexIgnore
    public static String a(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null) {
                return string;
            }
            String string2 = bundle.getString("unregistered");
            if (string2 != null) {
                return string2;
            }
            String string3 = bundle.getString("error");
            if ("RST".equals(string3)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string3 != null) {
                throw new IOException(string3);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    @DexIgnore
    public final <T> wn1<Void> a(wn1<T> wn1) {
        return wn1.a(my1.a(), (pn1<T, TContinuationResult>) new xy1(this));
    }

    @DexIgnore
    public final /* synthetic */ void a(Bundle bundle, xn1 xn1) {
        try {
            xn1.a(this.c.a(bundle));
        } catch (IOException e) {
            xn1.a((Exception) e);
        }
    }
}
