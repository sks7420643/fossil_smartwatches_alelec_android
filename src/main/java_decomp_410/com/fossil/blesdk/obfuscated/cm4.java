package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cm4 implements jl4 {
    @DexIgnore
    public /* final */ OkHttpClient e;
    @DexIgnore
    public /* final */ gn4 f;
    @DexIgnore
    public /* final */ ho4 g; // = new a();
    @DexIgnore
    public vl4 h;
    @DexIgnore
    public /* final */ dm4 i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public boolean k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ho4 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void i() {
            cm4.this.cancel();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends im4 {
        @DexIgnore
        public /* final */ kl4 f;

        /*
        static {
            Class<cm4> cls = cm4.class;
        }
        */

        @DexIgnore
        public b(kl4 kl4) {
            super("OkHttp %s", cm4.this.c());
            this.f = kl4;
        }

        @DexIgnore
        public void a(ExecutorService executorService) {
            try {
                executorService.execute(this);
            } catch (RejectedExecutionException e) {
                InterruptedIOException interruptedIOException = new InterruptedIOException("executor rejected");
                interruptedIOException.initCause(e);
                cm4.this.h.a((jl4) cm4.this, (IOException) interruptedIOException);
                this.f.onFailure(cm4.this, interruptedIOException);
                cm4.this.e.h().b(this);
            } catch (Throwable th) {
                cm4.this.e.h().b(this);
                throw th;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004a A[Catch:{ all -> 0x003d }] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006a A[Catch:{ all -> 0x003d }] */
        public void b() {
            IOException e;
            cm4.this.g.g();
            boolean z = true;
            try {
                Response b = cm4.this.b();
                if (cm4.this.f.b()) {
                    try {
                        this.f.onFailure(cm4.this, new IOException("Canceled"));
                    } catch (IOException e2) {
                        e = e2;
                        try {
                            IOException a = cm4.this.a(e);
                            if (!z) {
                                ao4 d = ao4.d();
                                d.a(4, "Callback failure for " + cm4.this.d(), (Throwable) a);
                            } else {
                                cm4.this.h.a((jl4) cm4.this, a);
                                this.f.onFailure(cm4.this, a);
                            }
                            cm4.this.e.h().b(this);
                        } catch (Throwable th) {
                            cm4.this.e.h().b(this);
                            throw th;
                        }
                    }
                } else {
                    this.f.onResponse(cm4.this, b);
                }
            } catch (IOException e3) {
                e = e3;
                z = false;
                IOException a2 = cm4.this.a(e);
                if (!z) {
                }
                cm4.this.e.h().b(this);
            }
            cm4.this.e.h().b(this);
        }

        @DexIgnore
        public cm4 c() {
            return cm4.this;
        }

        @DexIgnore
        public String d() {
            return cm4.this.i.g().g();
        }
    }

    @DexIgnore
    public cm4(OkHttpClient okHttpClient, dm4 dm4, boolean z) {
        this.e = okHttpClient;
        this.i = dm4;
        this.j = z;
        this.f = new gn4(okHttpClient, z);
        this.g.a((long) okHttpClient.b(), TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public Response b() throws IOException {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.e.w());
        arrayList.add(this.f);
        arrayList.add(new xm4(this.e.g()));
        arrayList.add(new lm4(this.e.x()));
        arrayList.add(new rm4(this.e));
        if (!this.j) {
            arrayList.addAll(this.e.y());
        }
        arrayList.add(new ym4(this.j));
        return new dn4(arrayList, (wm4) null, (zm4) null, (tm4) null, 0, this.i, this, this.h, this.e.d(), this.e.E(), this.e.I()).a(this.i);
    }

    @DexIgnore
    public String c() {
        return this.i.g().m();
    }

    @DexIgnore
    public void cancel() {
        this.f.a();
    }

    @DexIgnore
    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append(o() ? "canceled " : "");
        sb.append(this.j ? "web socket" : "call");
        sb.append(" to ");
        sb.append(c());
        return sb.toString();
    }

    @DexIgnore
    public dm4 n() {
        return this.i;
    }

    @DexIgnore
    public boolean o() {
        return this.f.b();
    }

    @DexIgnore
    public Response r() throws IOException {
        synchronized (this) {
            if (!this.k) {
                this.k = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.g.g();
        this.h.b(this);
        try {
            this.e.h().a(this);
            Response b2 = b();
            if (b2 != null) {
                this.e.h().b(this);
                return b2;
            }
            throw new IOException("Canceled");
        } catch (IOException e2) {
            IOException a2 = a(e2);
            this.h.a((jl4) this, a2);
            throw a2;
        } catch (Throwable th) {
            this.e.h().b(this);
            throw th;
        }
    }

    @DexIgnore
    public static cm4 a(OkHttpClient okHttpClient, dm4 dm4, boolean z) {
        cm4 cm4 = new cm4(okHttpClient, dm4, z);
        cm4.h = okHttpClient.j().a(cm4);
        return cm4;
    }

    @DexIgnore
    public cm4 clone() {
        return a(this.e, this.i, this.j);
    }

    @DexIgnore
    public IOException a(IOException iOException) {
        if (!this.g.h()) {
            return iOException;
        }
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final void a() {
        this.f.a(ao4.d().a("response.body().close()"));
    }

    @DexIgnore
    public void a(kl4 kl4) {
        synchronized (this) {
            if (!this.k) {
                this.k = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        a();
        this.h.b(this);
        this.e.h().a(new b(kl4));
    }
}
