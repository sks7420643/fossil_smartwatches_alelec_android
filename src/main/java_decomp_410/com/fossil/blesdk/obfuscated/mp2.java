package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.usecase.GetWeather;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mp2 implements MembersInjector<ComplicationWeatherService> {
    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, GetWeather getWeather) {
        complicationWeatherService.l = getWeather;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, j62 j62) {
        complicationWeatherService.m = j62;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, en2 en2) {
        complicationWeatherService.n = en2;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, PortfolioApp portfolioApp) {
        complicationWeatherService.o = portfolioApp;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, CustomizeRealDataRepository customizeRealDataRepository) {
        complicationWeatherService.p = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, UserRepository userRepository) {
        complicationWeatherService.q = userRepository;
    }
}
