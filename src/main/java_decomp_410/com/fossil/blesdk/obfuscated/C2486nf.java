package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nf */
public interface C2486nf extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nf$a */
    public static abstract class C2487a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C2486nf {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.nf$a$a */
        public static class C2488a implements com.fossil.blesdk.obfuscated.C2486nf {

            @DexIgnore
            /* renamed from: e */
            public android.os.IBinder f7750e;

            @DexIgnore
            public C2488a(android.os.IBinder iBinder) {
                this.f7750e = iBinder;
            }

            @DexIgnore
            /* renamed from: a */
            public int mo3258a(com.fossil.blesdk.obfuscated.C2406mf mfVar, java.lang.String str) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongBinder(mfVar != null ? mfVar.asBinder() : null);
                    obtain.writeString(str);
                    this.f7750e.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public android.os.IBinder asBinder() {
                return this.f7750e;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo3260a(com.fossil.blesdk.obfuscated.C2406mf mfVar, int i) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeStrongBinder(mfVar != null ? mfVar.asBinder() : null);
                    obtain.writeInt(i);
                    this.f7750e.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            /* renamed from: a */
            public void mo3259a(int i, java.lang.String[] strArr) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationService");
                    obtain.writeInt(i);
                    obtain.writeStringArray(strArr);
                    this.f7750e.transact(3, obtain, (android.os.Parcel) null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        public C2487a() {
            attachInterface(this, "androidx.room.IMultiInstanceInvalidationService");
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2486nf m11229a(android.os.IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            android.os.IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof com.fossil.blesdk.obfuscated.C2486nf)) {
                return new com.fossil.blesdk.obfuscated.C2486nf.C2487a.C2488a(iBinder);
            }
            return (com.fossil.blesdk.obfuscated.C2486nf) queryLocalInterface;
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                int a = mo3258a(com.fossil.blesdk.obfuscated.C2406mf.C2407a.m10716a(parcel.readStrongBinder()), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeInt(a);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                mo3260a(com.fossil.blesdk.obfuscated.C2406mf.C2407a.m10716a(parcel.readStrongBinder()), parcel.readInt());
                parcel2.writeNoException();
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationService");
                mo3259a(parcel.readInt(), parcel.createStringArray());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationService");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    int mo3258a(com.fossil.blesdk.obfuscated.C2406mf mfVar, java.lang.String str) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo3259a(int i, java.lang.String[] strArr) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo3260a(com.fossil.blesdk.obfuscated.C2406mf mfVar, int i) throws android.os.RemoteException;
}
