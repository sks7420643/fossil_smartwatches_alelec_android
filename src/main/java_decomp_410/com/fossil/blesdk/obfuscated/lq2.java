package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lq2 extends hq2 {
    @DexIgnore
    public String c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lq2(String str, String str2) {
        super(str);
        kd4.b(str, "tagName");
        kd4.b(str2, "title");
        this.c = str2;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final String c() {
        return this.c;
    }
}
