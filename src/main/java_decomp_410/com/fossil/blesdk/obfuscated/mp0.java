package com.fossil.blesdk.obfuscated;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mp0 implements f21<DataType> {
    @DexIgnore
    public static /* final */ mp0 a; // = new mp0();

    @DexIgnore
    public final boolean a(String str) {
        return sp0.a(str) != null;
    }

    @DexIgnore
    public final /* synthetic */ String b(Object obj, int i) {
        return a((DataType) obj, i).I();
    }

    @DexIgnore
    public final /* synthetic */ int c(Object obj, int i) {
        return a((DataType) obj, i).H();
    }

    @DexIgnore
    public final /* synthetic */ int zzc(Object obj) {
        return ((DataType) obj).H().size();
    }

    @DexIgnore
    public final /* synthetic */ String zzd(Object obj) {
        return ((DataType) obj).I();
    }

    @DexIgnore
    public static bp0 a(DataType dataType, int i) {
        return dataType.H().get(i);
    }

    @DexIgnore
    public final /* synthetic */ boolean a(Object obj, int i) {
        return Boolean.TRUE.equals(a((DataType) obj, i).J());
    }
}
