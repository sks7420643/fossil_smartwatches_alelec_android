package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yu2 implements MembersInjector<wu2> {
    @DexIgnore
    public static void a(wu2 wu2, HomeDashboardPresenter homeDashboardPresenter) {
        wu2.k = homeDashboardPresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
        wu2.l = homeDianaCustomizePresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
        wu2.m = homeHybridCustomizePresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, HomeProfilePresenter homeProfilePresenter) {
        wu2.n = homeProfilePresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, HomeAlertsPresenter homeAlertsPresenter) {
        wu2.o = homeAlertsPresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
        wu2.p = homeAlertsHybridPresenter;
    }

    @DexIgnore
    public static void a(wu2 wu2, ig3 ig3) {
        wu2.q = ig3;
    }
}
