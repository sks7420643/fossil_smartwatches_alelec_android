package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh4 {
    @DexIgnore
    public static /* final */ ug4 a; // = tg4.a();
    @DexIgnore
    public static /* final */ ug4 b; // = nk4.k.D();

    /*
    static {
        new nh4();
        fj4 fj4 = fj4.e;
    }
    */

    @DexIgnore
    public static final ug4 a() {
        return a;
    }

    @DexIgnore
    public static final ug4 b() {
        return b;
    }

    @DexIgnore
    public static final pi4 c() {
        return vj4.b;
    }
}
