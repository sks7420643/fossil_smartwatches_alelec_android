package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l21 extends k21<r01> {
    @DexIgnore
    public static /* final */ de0.g<l21> E; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0<Object> F; // = new de0<>("Fitness.BLE_API", new n21(), E);

    /*
    static {
        new de0("Fitness.BLE_CLIENT", new o21(), E);
    }
    */

    @DexIgnore
    public l21(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 59, bVar, cVar, kj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitBleApi");
        if (queryLocalInterface instanceof r01) {
            return (r01) queryLocalInterface;
        }
        return new s01(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitBleApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.BleApi";
    }
}
