package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sy */
public class C2927sy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2927sy.C2932e f9489a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.app.AlertDialog.Builder f9490b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sy$a")
    /* renamed from: com.fossil.blesdk.obfuscated.sy$a */
    public static class C2928a implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2927sy.C2932e f9491e;

        @DexIgnore
        public C2928a(com.fossil.blesdk.obfuscated.C2927sy.C2932e eVar) {
            this.f9491e = eVar;
        }

        @DexIgnore
        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.f9491e.mo16230a(true);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sy$b")
    /* renamed from: com.fossil.blesdk.obfuscated.sy$b */
    public static class C2929b implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2927sy.C2932e f9492e;

        @DexIgnore
        public C2929b(com.fossil.blesdk.obfuscated.C2927sy.C2932e eVar) {
            this.f9492e = eVar;
        }

        @DexIgnore
        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.f9492e.mo16230a(false);
            dialogInterface.dismiss();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sy$c")
    /* renamed from: com.fossil.blesdk.obfuscated.sy$c */
    public static class C2930c implements android.content.DialogInterface.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2927sy.C2931d f9493e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2927sy.C2932e f9494f;

        @DexIgnore
        public C2930c(com.fossil.blesdk.obfuscated.C2927sy.C2931d dVar, com.fossil.blesdk.obfuscated.C2927sy.C2932e eVar) {
            this.f9493e = dVar;
            this.f9494f = eVar;
        }

        @DexIgnore
        public void onClick(android.content.DialogInterface dialogInterface, int i) {
            this.f9493e.mo4165a(true);
            this.f9494f.mo16230a(true);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.sy$d */
    public interface C2931d {
        @DexIgnore
        /* renamed from: a */
        void mo4165a(boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sy$e")
    /* renamed from: com.fossil.blesdk.obfuscated.sy$e */
    public static class C2932e {

        @DexIgnore
        /* renamed from: a */
        public boolean f9495a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.concurrent.CountDownLatch f9496b;

        @DexIgnore
        public C2932e() {
            this.f9495a = false;
            this.f9496b = new java.util.concurrent.CountDownLatch(1);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16230a(boolean z) {
            this.f9495a = z;
            this.f9496b.countDown();
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo16231b() {
            return this.f9495a;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16229a() {
            try {
                this.f9496b.await();
            } catch (java.lang.InterruptedException unused) {
            }
        }

        @DexIgnore
        public /* synthetic */ C2932e(com.fossil.blesdk.obfuscated.C2927sy.C2928a aVar) {
            this();
        }
    }

    @DexIgnore
    public C2927sy(android.app.AlertDialog.Builder builder, com.fossil.blesdk.obfuscated.C2927sy.C2932e eVar) {
        this.f9489a = eVar;
        this.f9490b = builder;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m13949a(float f, int i) {
        return (int) (f * ((float) i));
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2927sy m13951a(android.app.Activity activity, com.fossil.blesdk.obfuscated.x74 x74, com.fossil.blesdk.obfuscated.C2927sy.C2931d dVar) {
        com.fossil.blesdk.obfuscated.C2927sy.C2932e eVar = new com.fossil.blesdk.obfuscated.C2927sy.C2932e((com.fossil.blesdk.obfuscated.C2927sy.C2928a) null);
        com.fossil.blesdk.obfuscated.C1911gz gzVar = new com.fossil.blesdk.obfuscated.C1911gz(activity, x74);
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        android.widget.ScrollView a = m13950a(activity, gzVar.mo11442c());
        builder.setView(a).setTitle(gzVar.mo11444e()).setCancelable(false).setNeutralButton(gzVar.mo11443d(), new com.fossil.blesdk.obfuscated.C2927sy.C2928a(eVar));
        if (x74.f20200d) {
            builder.setNegativeButton(gzVar.mo11440b(), new com.fossil.blesdk.obfuscated.C2927sy.C2929b(eVar));
        }
        if (x74.f20202f) {
            builder.setPositiveButton(gzVar.mo11437a(), new com.fossil.blesdk.obfuscated.C2927sy.C2930c(dVar, eVar));
        }
        return new com.fossil.blesdk.obfuscated.C2927sy(builder, eVar);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo16224b() {
        return this.f9489a.mo16231b();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16225c() {
        this.f9490b.show();
    }

    @DexIgnore
    /* renamed from: a */
    public static android.widget.ScrollView m13950a(android.app.Activity activity, java.lang.String str) {
        float f = activity.getResources().getDisplayMetrics().density;
        int a = m13949a(f, 5);
        android.widget.TextView textView = new android.widget.TextView(activity);
        textView.setAutoLinkMask(15);
        textView.setText(str);
        textView.setTextAppearance(activity, 16973892);
        textView.setPadding(a, a, a, a);
        textView.setFocusable(false);
        android.widget.ScrollView scrollView = new android.widget.ScrollView(activity);
        scrollView.setPadding(m13949a(f, 14), m13949a(f, 2), m13949a(f, 10), m13949a(f, 12));
        scrollView.addView(textView);
        return scrollView;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16223a() {
        this.f9489a.mo16229a();
    }
}
