package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ls */
public class C2341ls<DataType> implements com.fossil.blesdk.obfuscated.C2427mo<DataType, android.graphics.drawable.BitmapDrawable> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2427mo<DataType, android.graphics.Bitmap> f7274a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.Resources f7275b;

    @DexIgnore
    public C2341ls(android.content.res.Resources resources, com.fossil.blesdk.obfuscated.C2427mo<DataType, android.graphics.Bitmap> moVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(resources);
        this.f7275b = resources;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(moVar);
        this.f7274a = moVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(DataType datatype, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return this.f7274a.mo9301a(datatype, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.BitmapDrawable> mo9299a(DataType datatype, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return com.fossil.blesdk.obfuscated.C1659dt.m6045a(this.f7275b, this.f7274a.mo9299a(datatype, i, i2, loVar));
    }
}
