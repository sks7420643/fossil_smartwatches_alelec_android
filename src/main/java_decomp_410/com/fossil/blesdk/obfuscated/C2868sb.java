package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sb */
public interface C2868sb {
    @DexIgnore
    /* renamed from: a */
    void mo15909a(androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.lifecycle.Lifecycle.Event event, boolean z, com.fossil.blesdk.obfuscated.C1473bc bcVar);
}
