package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z11 implements Parcelable.Creator<y11> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        zo0 zo0 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                zo0 = (zo0) SafeParcelReader.a(parcel, a, zo0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new y11(zo0);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new y11[i];
    }
}
