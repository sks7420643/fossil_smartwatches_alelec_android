package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dc1 {
    @DexIgnore
    public static /* final */ int[] a; // = new int[0];
    @DexIgnore
    public static /* final */ long[] b; // = new long[0];
    @DexIgnore
    public static /* final */ String[] c; // = new String[0];
    @DexIgnore
    public static /* final */ byte[] d; // = new byte[0];

    @DexIgnore
    public static final int a(tb1 tb1, int i) throws IOException {
        int a2 = tb1.a();
        tb1.b(i);
        int i2 = 1;
        while (tb1.c() == i) {
            tb1.b(i);
            i2++;
        }
        tb1.b(a2, i);
        return i2;
    }
}
