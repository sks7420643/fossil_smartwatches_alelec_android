package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fl4 {
    @DexIgnore
    public static /* final */ dk4 a; // = new dk4("UNLOCK_FAIL");
    @DexIgnore
    public static /* final */ dk4 b; // = new dk4("LOCKED");
    @DexIgnore
    public static /* final */ dk4 c; // = new dk4("UNLOCKED");
    @DexIgnore
    public static /* final */ cl4 d; // = new cl4(b);
    @DexIgnore
    public static /* final */ cl4 e; // = new cl4(c);

    /*
    static {
        new dk4("LOCK_FAIL");
        new dk4("ENQUEUE_FAIL");
        new dk4("SELECT_SUCCESS");
    }
    */

    @DexIgnore
    public static /* synthetic */ dl4 a(boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(z);
    }

    @DexIgnore
    public static final dl4 a(boolean z) {
        return new el4(z);
    }
}
