package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.de0;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dg0 implements mg0 {
    @DexIgnore
    public /* final */ ng0 a;

    @DexIgnore
    public dg0(ng0 ng0) {
        this.a = ng0;
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final void a(ud0 ud0, de0<?> de0, boolean z) {
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t) {
        this.a.r.i.add(t);
        return t;
    }

    @DexIgnore
    public final void c() {
        for (de0.f a2 : this.a.j.values()) {
            a2.a();
        }
        this.a.r.q = Collections.emptySet();
    }

    @DexIgnore
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    public final void f(int i) {
    }

    @DexIgnore
    public final void b() {
        this.a.h();
    }
}
