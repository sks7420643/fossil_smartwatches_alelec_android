package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fossil.blesdk.obfuscated.lu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nu implements lu {
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ lu.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ BroadcastReceiver i; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            nu nuVar = nu.this;
            boolean z = nuVar.g;
            nuVar.g = nuVar.a(context);
            if (z != nu.this.g) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + nu.this.g);
                }
                nu nuVar2 = nu.this;
                nuVar2.f.a(nuVar2.g);
            }
        }
    }

    @DexIgnore
    public nu(Context context, lu.a aVar) {
        this.e = context.getApplicationContext();
        this.f = aVar;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public boolean a(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        tw.a(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (RuntimeException e2) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e2);
            }
            return true;
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void c() {
        e();
    }

    @DexIgnore
    public final void d() {
        if (!this.h) {
            this.g = a(this.e);
            try {
                this.e.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.h = true;
            } catch (SecurityException e2) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e2);
                }
            }
        }
    }

    @DexIgnore
    public final void e() {
        if (this.h) {
            this.e.unregisterReceiver(this.i);
            this.h = false;
        }
    }

    @DexIgnore
    public void a() {
        d();
    }
}
