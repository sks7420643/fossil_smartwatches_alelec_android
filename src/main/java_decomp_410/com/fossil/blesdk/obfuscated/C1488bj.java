package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bj */
public abstract class C1488bj {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f3755a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("InputMerger");

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1488bj m5008a(java.lang.String str) {
        try {
            return (com.fossil.blesdk.obfuscated.C1488bj) java.lang.Class.forName(str).newInstance();
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.C1635dj a = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
            java.lang.String str2 = f3755a;
            a.mo9963b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1419aj mo3731a(java.util.List<com.fossil.blesdk.obfuscated.C1419aj> list);
}
