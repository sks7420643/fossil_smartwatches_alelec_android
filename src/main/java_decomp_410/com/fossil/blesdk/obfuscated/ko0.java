package com.fossil.blesdk.obfuscated;

import dalvik.system.PathClassLoader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ko0 extends PathClassLoader {
    @DexIgnore
    public ko0(String str, ClassLoader classLoader) {
        super(str, classLoader);
    }

    @DexIgnore
    public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
        if (!str.startsWith("java.") && !str.startsWith("android.")) {
            try {
                return findClass(str);
            } catch (ClassNotFoundException unused) {
            }
        }
        return super.loadClass(str, z);
    }
}
