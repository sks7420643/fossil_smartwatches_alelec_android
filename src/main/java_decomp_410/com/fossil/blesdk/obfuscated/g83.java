package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g83 extends zr2 implements f83 {
    @DexIgnore
    public tr3<u82> j;
    @DexIgnore
    public e83 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.D;
            Date date = new Date();
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void b(wr2 wr2, ArrayList<String> arrayList) {
        kd4.b(wr2, "baseModel");
        kd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + wr2);
        tr3<u82> tr3 = this.j;
        if (tr3 != null) {
            u82 a2 = tr3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.q;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ll2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        u82 u82 = (u82) qa.a(layoutInflater, R.layout.fragment_active_time_overview_day, viewGroup, false, O0());
        u82.r.setOnClickListener(b.e);
        this.j = new tr3<>(this, u82);
        tr3<u82> tr3 = this.j;
        if (tr3 != null) {
            u82 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        e83 e83 = this.k;
        if (e83 != null) {
            e83.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        e83 e83 = this.k;
        if (e83 != null) {
            e83.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        kd4.b(list, "workoutSessions");
        tr3<u82> tr3 = this.j;
        if (tr3 != null) {
            u82 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.u;
                kd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    sg2 sg2 = a2.t;
                    if (sg2 != null) {
                        View d = sg2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                sg2 sg22 = a2.t;
                if (sg22 != null) {
                    View d2 = sg22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            kd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(e83 e83) {
        kd4.b(e83, "presenter");
        this.k = e83;
    }

    @DexIgnore
    public final void a(sg2 sg2, WorkoutSession workoutSession) {
        String str;
        if (sg2 != null) {
            View d = sg2.d();
            kd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = sm2.a(context, a2.getSecond().intValue());
            sg2.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = sg2.r;
            kd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            Triple<Integer, Integer, Integer> c = rk2.c(workoutSession.getDuration());
            kd4.a((Object) c, "DateHelper.getTimeValues(workoutSession.duration)");
            FlexibleTextView flexibleTextView2 = sg2.s;
            kd4.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            if (kd4.a(c.getFirst().intValue(), 0) > 0) {
                pd4 pd4 = pd4.a;
                String a4 = sm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberHrsNumberMinsNumberSecs);
                kd4.a((Object) a4, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                Object[] objArr = {c.getFirst(), c.getSecond(), c.getThird()};
                str = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) str, "java.lang.String.format(format, *args)");
            } else if (kd4.a(c.getSecond().intValue(), 0) > 0) {
                pd4 pd42 = pd4.a;
                String a5 = sm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberMinsNumberSecs);
                kd4.a((Object) a5, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                Object[] objArr2 = {c.getSecond(), c.getThird()};
                str = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                kd4.a((Object) str, "java.lang.String.format(format, *args)");
            } else {
                pd4 pd43 = pd4.a;
                String a6 = sm2.a(context, (int) R.string.DashboardDiana_Sleep_DetailPage_Text__NumberSecs);
                kd4.a((Object) a6, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                Object[] objArr3 = {c.getThird()};
                str = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                kd4.a((Object) str, "java.lang.String.format(format, *args)");
            }
            flexibleTextView2.setText(str);
            FlexibleTextView flexibleTextView3 = sg2.q;
            kd4.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(rk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
