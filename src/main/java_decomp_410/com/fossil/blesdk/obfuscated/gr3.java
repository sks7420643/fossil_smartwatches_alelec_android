package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.dr4;
import com.fossil.blesdk.obfuscated.gr4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.underamour.UASharePref;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gr3 {
    @DexIgnore
    public Retrofit a;
    @DexIgnore
    public cr3 b;
    @DexIgnore
    public hr3 c; // = new hr3("https://www.mapmyfitness.com/v7.1/");
    @DexIgnore
    public er3 d;
    @DexIgnore
    public fr3 e;
    @DexIgnore
    public dr3 f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void onSuccess();
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (r2 != null) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0023, code lost:
        if (r0 != null) goto L_0x0027;
     */
    @DexIgnore
    public final void a() {
        String str;
        String str2;
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
        if (a2 != null) {
            str = a2.getB();
        }
        str = "";
        this.g = str;
        if (a2 != null) {
            str2 = a2.getC();
        }
        str2 = "";
        this.h = str2;
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.a(HttpLoggingInterceptor.Level.NONE);
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.a((Interceptor) httpLoggingInterceptor);
        OkHttpClient a3 = bVar.a();
        Retrofit.b bVar2 = new Retrofit.b();
        bVar2.a("https://api.ua.com/v7.1/");
        bVar2.a((dr4.a) bs4.a());
        bVar2.a((gr4.a) GsonConverterFactory.a());
        bVar2.a(a3);
        this.a = bVar2.a();
        Retrofit retrofit3 = this.a;
        if (retrofit3 != null) {
            this.b = (cr3) retrofit3.a(cr3.class);
            cr3 cr3 = this.b;
            if (cr3 != null) {
                String str3 = this.g;
                if (str3 != null) {
                    String str4 = this.h;
                    if (str4 != null) {
                        this.d = new er3(cr3, str3, str4);
                        cr3 cr32 = this.b;
                        if (cr32 != null) {
                            er3 er3 = this.d;
                            if (er3 != null) {
                                this.e = new fr3(cr32, er3);
                                cr3 cr33 = this.b;
                                if (cr33 != null) {
                                    er3 er32 = this.d;
                                    if (er32 != null) {
                                        String str5 = this.g;
                                        if (str5 != null) {
                                            this.f = new dr3(cr33, er32, str5);
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final Object b(String str, yb4<? super qa4> yb4) {
        if (this.e == null) {
            a();
        }
        fr3 fr3 = this.e;
        if (fr3 != null) {
            String str2 = this.g;
            if (str2 == null) {
                str2 = "";
            }
            fr3.a(str2);
            fr3.b(str);
            fr3.b();
        }
        return qa4.a;
    }

    @DexIgnore
    public final boolean b() {
        String a2 = UASharePref.c.a().a();
        return !(a2 == null || a2.length() == 0);
    }

    @DexIgnore
    public final Object a(String str, yb4<? super String> yb4) {
        if (TextUtils.isEmpty(this.g)) {
            a();
        }
        hr3 hr3 = this.c;
        String str2 = this.g;
        if (str2 == null) {
            str2 = "";
        }
        String url = hr3.a(str2, str).toString();
        kd4.a((Object) url, "uaUrlBuilder.buildOAuth2\u2026, redirectUrl).toString()");
        return url;
    }

    @DexIgnore
    public final Object a(String str, b bVar, yb4<? super qa4> yb4) {
        if (this.d == null) {
            a();
        }
        er3 er3 = this.d;
        if (er3 != null) {
            er3.a(str, bVar);
        }
        return qa4.a;
    }

    @DexIgnore
    public final Object a(UAActivityTimeSeries uAActivityTimeSeries, d dVar, yb4<? super qa4> yb4) {
        if (this.f == null) {
            a();
        }
        dr3 dr3 = this.f;
        if (dr3 != null) {
            dr3.a(uAActivityTimeSeries, dVar);
        }
        return qa4.a;
    }

    @DexIgnore
    public final Object a(c cVar, yb4<? super qa4> yb4) {
        if (this.d == null) {
            a();
        }
        er3 er3 = this.d;
        if (er3 != null) {
            er3.a(cVar);
        }
        return qa4.a;
    }
}
