package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zk3 extends vk3 {
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ wk3 g;
    @DexIgnore
    public /* final */ ResetPasswordUseCase h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<ResetPasswordUseCase.d, ResetPasswordUseCase.c> {
        @DexIgnore
        public /* final */ /* synthetic */ zk3 a;

        @DexIgnore
        public b(zk3 zk3) {
            this.a = zk3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ResetPasswordUseCase.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.h().i();
            this.a.h().u0();
        }

        @DexIgnore
        public void a(ResetPasswordUseCase.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.h().i();
            int a2 = cVar.a();
            if (a2 == 400005) {
                wk3 h = this.a.h();
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_InputError_Text__InvalidEmailAddress);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                h.H(a3);
                this.a.h().G(false);
            } else if (a2 != 404001) {
                wk3 h2 = this.a.h();
                int a4 = cVar.a();
                String b = cVar.b();
                if (b == null) {
                    b = "";
                }
                h2.a(a4, b);
                this.a.h().G(false);
            } else {
                wk3 h3 = this.a.h();
                String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_EmailNotRegistered_Text__EmailIsNotRegistered);
                kd4.a((Object) a5, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                h3.H(a5);
                this.a.h().G(true);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public zk3(wk3 wk3, ResetPasswordUseCase resetPasswordUseCase) {
        kd4.b(wk3, "mView");
        kd4.b(resetPasswordUseCase, "mResetPasswordUseCase");
        this.g = wk3;
        this.h = resetPasswordUseCase;
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, "email");
        this.f = str;
        if (str.length() == 0) {
            this.g.l0();
        } else {
            this.g.a0();
        }
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, "email");
        if (i()) {
            this.g.k();
            this.h.a(new ResetPasswordUseCase.b(str), new b(this));
        }
    }

    @DexIgnore
    public final void c(String str) {
        kd4.b(str, "email");
        this.f = str;
    }

    @DexIgnore
    public void f() {
        this.g.O(this.f);
    }

    @DexIgnore
    public final wk3 h() {
        return this.g;
    }

    @DexIgnore
    public final boolean i() {
        if (this.f.length() == 0) {
            this.g.H("");
            return false;
        } else if (!ks3.a(this.f)) {
            wk3 wk3 = this.g;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_InputError_Text__InvalidEmailAddress);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            wk3.H(a2);
            return false;
        } else {
            this.g.H("");
            return true;
        }
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        kd4.b(str, "key");
        kd4.b(bundle, "outState");
        bundle.putString(str, this.f);
    }
}
