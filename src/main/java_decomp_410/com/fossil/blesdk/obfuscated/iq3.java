package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iq3 implements MembersInjector<FindDeviceActivity> {
    @DexIgnore
    public static void a(FindDeviceActivity findDeviceActivity, FindDevicePresenter findDevicePresenter) {
        findDeviceActivity.B = findDevicePresenter;
    }
}
