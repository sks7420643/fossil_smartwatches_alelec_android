package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.fossil.blesdk.obfuscated.co;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tt implements co.a {
    @DexIgnore
    public /* final */ jq a;
    @DexIgnore
    public /* final */ gq b;

    @DexIgnore
    public tt(jq jqVar, gq gqVar) {
        this.a = jqVar;
        this.b = gqVar;
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return this.a.b(i, i2, config);
    }

    @DexIgnore
    public byte[] b(int i) {
        gq gqVar = this.b;
        if (gqVar == null) {
            return new byte[i];
        }
        return (byte[]) gqVar.b(i, byte[].class);
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.a.a(bitmap);
    }

    @DexIgnore
    public void a(byte[] bArr) {
        gq gqVar = this.b;
        if (gqVar != null) {
            gqVar.put(bArr);
        }
    }

    @DexIgnore
    public int[] a(int i) {
        gq gqVar = this.b;
        if (gqVar == null) {
            return new int[i];
        }
        return (int[]) gqVar.b(i, int[].class);
    }

    @DexIgnore
    public void a(int[] iArr) {
        gq gqVar = this.b;
        if (gqVar != null) {
            gqVar.put(iArr);
        }
    }
}
