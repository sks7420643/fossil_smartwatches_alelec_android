package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a3 */
public class C1378a3 implements com.fossil.blesdk.obfuscated.C2101j2 {

    @DexIgnore
    /* renamed from: a */
    public androidx.appcompat.widget.Toolbar f3325a;

    @DexIgnore
    /* renamed from: b */
    public int f3326b;

    @DexIgnore
    /* renamed from: c */
    public android.view.View f3327c;

    @DexIgnore
    /* renamed from: d */
    public android.view.View f3328d;

    @DexIgnore
    /* renamed from: e */
    public android.graphics.drawable.Drawable f3329e;

    @DexIgnore
    /* renamed from: f */
    public android.graphics.drawable.Drawable f3330f;

    @DexIgnore
    /* renamed from: g */
    public android.graphics.drawable.Drawable f3331g;

    @DexIgnore
    /* renamed from: h */
    public boolean f3332h;

    @DexIgnore
    /* renamed from: i */
    public java.lang.CharSequence f3333i;

    @DexIgnore
    /* renamed from: j */
    public java.lang.CharSequence f3334j;

    @DexIgnore
    /* renamed from: k */
    public java.lang.CharSequence f3335k;

    @DexIgnore
    /* renamed from: l */
    public android.view.Window.Callback f3336l;

    @DexIgnore
    /* renamed from: m */
    public boolean f3337m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C3400z1 f3338n;

    @DexIgnore
    /* renamed from: o */
    public int f3339o;

    @DexIgnore
    /* renamed from: p */
    public int f3340p;

    @DexIgnore
    /* renamed from: q */
    public android.graphics.drawable.Drawable f3341q;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a3$a */
    public class C1379a implements android.view.View.OnClickListener {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C1369a1 f3342e;

        @DexIgnore
        public C1379a() {
            com.fossil.blesdk.obfuscated.C1369a1 a1Var = new com.fossil.blesdk.obfuscated.C1369a1(com.fossil.blesdk.obfuscated.C1378a3.this.f3325a.getContext(), 0, 16908332, 0, 0, com.fossil.blesdk.obfuscated.C1378a3.this.f3333i);
            this.f3342e = a1Var;
        }

        @DexIgnore
        public void onClick(android.view.View view) {
            com.fossil.blesdk.obfuscated.C1378a3 a3Var = com.fossil.blesdk.obfuscated.C1378a3.this;
            android.view.Window.Callback callback = a3Var.f3336l;
            if (callback != null && a3Var.f3337m) {
                callback.onMenuItemSelected(0, this.f3342e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a3$b")
    /* renamed from: com.fossil.blesdk.obfuscated.a3$b */
    public class C1380b extends com.fossil.blesdk.obfuscated.C2302l9 {

        @DexIgnore
        /* renamed from: a */
        public boolean f3344a; // = false;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ int f3345b;

        @DexIgnore
        public C1380b(int i) {
            this.f3345b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8505a(android.view.View view) {
            this.f3344a = true;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            if (!this.f3344a) {
                com.fossil.blesdk.obfuscated.C1378a3.this.f3325a.setVisibility(this.f3345b);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8507c(android.view.View view) {
            com.fossil.blesdk.obfuscated.C1378a3.this.f3325a.setVisibility(0);
        }
    }

    @DexIgnore
    public C1378a3(androidx.appcompat.widget.Toolbar toolbar, boolean z) {
        this(toolbar, z, com.fossil.blesdk.obfuscated.C3336y.abc_action_bar_up_description, com.fossil.blesdk.obfuscated.C3076v.abc_ic_ab_back_material);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8463a(android.graphics.drawable.Drawable drawable) {
        this.f3330f = drawable;
        mo8497r();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8469a(boolean z) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8474b(java.lang.CharSequence charSequence) {
        this.f3334j = charSequence;
        if ((this.f3326b & 8) != 0) {
            this.f3325a.setSubtitle(charSequence);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8476c(int i) {
        if (i != this.f3340p) {
            this.f3340p = i;
            if (android.text.TextUtils.isEmpty(this.f3325a.getNavigationContentDescription())) {
                mo8480d(this.f3340p);
            }
        }
    }

    @DexIgnore
    public void collapseActionView() {
        this.f3325a.mo1074c();
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo8481d() {
        return this.f3325a.mo1116m();
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo8482e() {
        return this.f3325a.mo1115l();
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo8483f() {
        return this.f3325a.mo1129r();
    }

    @DexIgnore
    /* renamed from: g */
    public void mo8484g() {
        this.f3325a.mo1077d();
    }

    @DexIgnore
    public android.content.Context getContext() {
        return this.f3325a.getContext();
    }

    @DexIgnore
    public java.lang.CharSequence getTitle() {
        return this.f3325a.getTitle();
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo8487h() {
        return this.f3325a.mo1114k();
    }

    @DexIgnore
    /* renamed from: i */
    public android.view.Menu mo8488i() {
        return this.f3325a.getMenu();
    }

    @DexIgnore
    /* renamed from: j */
    public int mo8489j() {
        return this.f3339o;
    }

    @DexIgnore
    /* renamed from: k */
    public android.view.ViewGroup mo8490k() {
        return this.f3325a;
    }

    @DexIgnore
    /* renamed from: l */
    public int mo8491l() {
        return this.f3326b;
    }

    @DexIgnore
    /* renamed from: m */
    public void mo8492m() {
        android.util.Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    /* renamed from: n */
    public void mo8493n() {
        android.util.Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    /* renamed from: o */
    public final int mo8494o() {
        if (this.f3325a.getNavigationIcon() == null) {
            return 11;
        }
        this.f3341q = this.f3325a.getNavigationIcon();
        return 15;
    }

    @DexIgnore
    /* renamed from: p */
    public final void mo8495p() {
        if ((this.f3326b & 4) == 0) {
            return;
        }
        if (android.text.TextUtils.isEmpty(this.f3335k)) {
            this.f3325a.setNavigationContentDescription(this.f3340p);
        } else {
            this.f3325a.setNavigationContentDescription(this.f3335k);
        }
    }

    @DexIgnore
    /* renamed from: q */
    public final void mo8496q() {
        if ((this.f3326b & 4) != 0) {
            androidx.appcompat.widget.Toolbar toolbar = this.f3325a;
            android.graphics.drawable.Drawable drawable = this.f3331g;
            if (drawable == null) {
                drawable = this.f3341q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.f3325a.setNavigationIcon((android.graphics.drawable.Drawable) null);
    }

    @DexIgnore
    /* renamed from: r */
    public final void mo8497r() {
        android.graphics.drawable.Drawable drawable;
        int i = this.f3326b;
        if ((i & 2) == 0) {
            drawable = null;
        } else if ((i & 1) != 0) {
            drawable = this.f3330f;
            if (drawable == null) {
                drawable = this.f3329e;
            }
        } else {
            drawable = this.f3329e;
        }
        this.f3325a.setLogo(drawable);
    }

    @DexIgnore
    public void setIcon(int i) {
        setIcon(i != 0 ? com.fossil.blesdk.obfuscated.C2364m0.m10497c(getContext(), i) : null);
    }

    @DexIgnore
    public void setTitle(java.lang.CharSequence charSequence) {
        this.f3332h = true;
        mo8477c(charSequence);
    }

    @DexIgnore
    public void setVisibility(int i) {
        this.f3325a.setVisibility(i);
    }

    @DexIgnore
    public void setWindowCallback(android.view.Window.Callback callback) {
        this.f3336l = callback;
    }

    @DexIgnore
    public void setWindowTitle(java.lang.CharSequence charSequence) {
        if (!this.f3332h) {
            mo8477c(charSequence);
        }
    }

    @DexIgnore
    public C1378a3(androidx.appcompat.widget.Toolbar toolbar, boolean z, int i, int i2) {
        this.f3339o = 0;
        this.f3340p = 0;
        this.f3325a = toolbar;
        this.f3333i = toolbar.getTitle();
        this.f3334j = toolbar.getSubtitle();
        this.f3332h = this.f3333i != null;
        this.f3331g = toolbar.getNavigationIcon();
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17205a(toolbar.getContext(), (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C1368a0.ActionBar, com.fossil.blesdk.obfuscated.C2777r.actionBarStyle, 0);
        this.f3341q = a.mo18422b(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_homeAsUpIndicator);
        if (z) {
            java.lang.CharSequence e = a.mo18428e(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_title);
            if (!android.text.TextUtils.isEmpty(e)) {
                setTitle(e);
            }
            java.lang.CharSequence e2 = a.mo18428e(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_subtitle);
            if (!android.text.TextUtils.isEmpty(e2)) {
                mo8474b(e2);
            }
            android.graphics.drawable.Drawable b = a.mo18422b(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_logo);
            if (b != null) {
                mo8463a(b);
            }
            android.graphics.drawable.Drawable b2 = a.mo18422b(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_icon);
            if (b2 != null) {
                setIcon(b2);
            }
            if (this.f3331g == null) {
                android.graphics.drawable.Drawable drawable = this.f3341q;
                if (drawable != null) {
                    mo8473b(drawable);
                }
            }
            mo8462a(a.mo18425d(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_displayOptions, 0));
            int g = a.mo18431g(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_customNavigationLayout, 0);
            if (g != 0) {
                mo8465a(android.view.LayoutInflater.from(this.f3325a.getContext()).inflate(g, this.f3325a, false));
                mo8462a(this.f3326b | 16);
            }
            int f = a.mo18429f(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_height, 0);
            if (f > 0) {
                android.view.ViewGroup.LayoutParams layoutParams = this.f3325a.getLayoutParams();
                layoutParams.height = f;
                this.f3325a.setLayoutParams(layoutParams);
            }
            int b3 = a.mo18421b(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_contentInsetStart, -1);
            int b4 = a.mo18421b(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_contentInsetEnd, -1);
            if (b3 >= 0 || b4 >= 0) {
                this.f3325a.mo1071b(java.lang.Math.max(b3, 0), java.lang.Math.max(b4, 0));
            }
            int g2 = a.mo18431g(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_titleTextStyle, 0);
            if (g2 != 0) {
                androidx.appcompat.widget.Toolbar toolbar2 = this.f3325a;
                toolbar2.mo1072b(toolbar2.getContext(), g2);
            }
            int g3 = a.mo18431g(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_subtitleTextStyle, 0);
            if (g3 != 0) {
                androidx.appcompat.widget.Toolbar toolbar3 = this.f3325a;
                toolbar3.mo1062a(toolbar3.getContext(), g3);
            }
            int g4 = a.mo18431g(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_popupTheme, 0);
            if (g4 != 0) {
                this.f3325a.setPopupTheme(g4);
            }
        } else {
            this.f3326b = mo8494o();
        }
        a.mo18418a();
        mo8476c(i);
        this.f3335k = this.f3325a.getNavigationContentDescription();
        this.f3325a.setNavigationOnClickListener(new com.fossil.blesdk.obfuscated.C1378a3.C1379a());
    }

    @DexIgnore
    /* renamed from: d */
    public void mo8480d(int i) {
        mo8468a((java.lang.CharSequence) i == 0 ? null : getContext().getString(i));
    }

    @DexIgnore
    public void setIcon(android.graphics.drawable.Drawable drawable) {
        this.f3329e = drawable;
        mo8497r();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8470a() {
        return this.f3325a.mo1117n();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8464a(android.view.Menu menu, com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        if (this.f3338n == null) {
            this.f3338n = new com.fossil.blesdk.obfuscated.C3400z1(this.f3325a.getContext());
            this.f3338n.mo9010a(com.fossil.blesdk.obfuscated.C3158w.action_menu_presenter);
        }
        this.f3338n.mo9013a(aVar);
        this.f3325a.mo1065a((com.fossil.blesdk.obfuscated.C1915h1) menu, this.f3338n);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8472b(int i) {
        mo8463a(i != 0 ? com.fossil.blesdk.obfuscated.C2364m0.m10497c(getContext(), i) : null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8471b() {
        this.f3337m = true;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo8477c(java.lang.CharSequence charSequence) {
        this.f3333i = charSequence;
        if ((this.f3326b & 8) != 0) {
            this.f3325a.setTitle(charSequence);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8475b(boolean z) {
        this.f3325a.setCollapsible(z);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8473b(android.graphics.drawable.Drawable drawable) {
        this.f3331g = drawable;
        mo8496q();
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo8478c() {
        return this.f3325a.mo1073b();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8462a(int i) {
        int i2 = this.f3326b ^ i;
        this.f3326b = i;
        if (i2 != 0) {
            if ((i2 & 4) != 0) {
                if ((i & 4) != 0) {
                    mo8495p();
                }
                mo8496q();
            }
            if ((i2 & 3) != 0) {
                mo8497r();
            }
            if ((i2 & 8) != 0) {
                if ((i & 8) != 0) {
                    this.f3325a.setTitle(this.f3333i);
                    this.f3325a.setSubtitle(this.f3334j);
                } else {
                    this.f3325a.setTitle((java.lang.CharSequence) null);
                    this.f3325a.setSubtitle((java.lang.CharSequence) null);
                }
            }
            if ((i2 & 16) != 0) {
                android.view.View view = this.f3328d;
                if (view == null) {
                    return;
                }
                if ((i & 16) != 0) {
                    this.f3325a.addView(view);
                } else {
                    this.f3325a.removeView(view);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8467a(com.fossil.blesdk.obfuscated.C2852s2 s2Var) {
        android.view.View view = this.f3327c;
        if (view != null) {
            android.view.ViewParent parent = view.getParent();
            androidx.appcompat.widget.Toolbar toolbar = this.f3325a;
            if (parent == toolbar) {
                toolbar.removeView(this.f3327c);
            }
        }
        this.f3327c = s2Var;
        if (s2Var != null && this.f3339o == 2) {
            this.f3325a.addView(this.f3327c, 0);
            androidx.appcompat.widget.Toolbar.LayoutParams layoutParams = (androidx.appcompat.widget.Toolbar.LayoutParams) this.f3327c.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.f116a = 8388691;
            s2Var.setAllowCollapse(true);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8465a(android.view.View view) {
        android.view.View view2 = this.f3328d;
        if (!(view2 == null || (this.f3326b & 16) == 0)) {
            this.f3325a.removeView(view2);
        }
        this.f3328d = view;
        if (view != null && (this.f3326b & 16) != 0) {
            this.f3325a.addView(this.f3328d);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo8461a(int i, long j) {
        com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this.f3325a);
        a.mo12275a(i == 0 ? 1.0f : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a.mo12276a(j);
        a.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) new com.fossil.blesdk.obfuscated.C1378a3.C1380b(i));
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8468a(java.lang.CharSequence charSequence) {
        this.f3335k = charSequence;
        mo8495p();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8466a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar, com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar2) {
        this.f3325a.mo1066a(aVar, aVar2);
    }
}
