package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tq */
public interface C2981tq {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.tq$a */
    public interface C2982a {
        @DexIgnore
        com.fossil.blesdk.obfuscated.C2981tq build();
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.tq$b */
    public interface C2983b {
        @DexIgnore
        /* renamed from: a */
        boolean mo13328a(java.io.File file);
    }

    @DexIgnore
    /* renamed from: a */
    java.io.File mo16535a(com.fossil.blesdk.obfuscated.C2143jo joVar);

    @DexIgnore
    /* renamed from: a */
    void mo16536a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2981tq.C2983b bVar);
}
