package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zg {
    @DexIgnore
    void a(ViewGroup viewGroup, View view);

    @DexIgnore
    void setVisibility(int i);
}
