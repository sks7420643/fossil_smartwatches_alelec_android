package com.fossil.blesdk.obfuscated;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.facebook.GraphRequest;
import com.facebook.internal.Utility;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hn extends zm {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ SSLSocketFactory b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends FilterInputStream {
        @DexIgnore
        public /* final */ HttpURLConnection e;

        @DexIgnore
        public a(HttpURLConnection httpURLConnection) {
            super(hn.b(httpURLConnection));
            this.e = httpURLConnection;
        }

        @DexIgnore
        public void close() throws IOException {
            super.close();
            this.e.disconnect();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        String a(String str);
    }

    @DexIgnore
    public hn() {
        this((b) null);
    }

    @DexIgnore
    public static boolean a(int i, int i2) {
        return (i == 4 || (100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    @DexIgnore
    public fn b(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError {
        String str;
        String url = request.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.putAll(request.getHeaders());
        b bVar = this.a;
        if (bVar != null) {
            str = bVar.a(url);
            if (str == null) {
                throw new IOException("URL blocked by rewriter: " + url);
            }
        } else {
            str = url;
        }
        HttpURLConnection a2 = a(new URL(str), request);
        boolean z = false;
        try {
            for (String str2 : hashMap.keySet()) {
                a2.setRequestProperty(str2, (String) hashMap.get(str2));
            }
            b(a2, request);
            int responseCode = a2.getResponseCode();
            if (responseCode == -1) {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            } else if (!a(request.getMethod(), responseCode)) {
                fn fnVar = new fn(responseCode, a((Map<String, List<String>>) a2.getHeaderFields()));
                a2.disconnect();
                return fnVar;
            } else {
                z = true;
                return new fn(responseCode, a((Map<String, List<String>>) a2.getHeaderFields()), a2.getContentLength(), new a(a2));
            }
        } catch (Throwable th) {
            if (!z) {
                a2.disconnect();
            }
            throw th;
        }
    }

    @DexIgnore
    public hn(b bVar) {
        this(bVar, (SSLSocketFactory) null);
    }

    @DexIgnore
    public static List<pm> a(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            if (next.getKey() != null) {
                for (String pmVar : (List) next.getValue()) {
                    arrayList.add(new pm((String) next.getKey(), pmVar));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public hn(b bVar, SSLSocketFactory sSLSocketFactory) {
        this.a = bVar;
        this.b = sSLSocketFactory;
    }

    @DexIgnore
    public HttpURLConnection a(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }

    @DexIgnore
    public final HttpURLConnection a(URL url, Request<?> request) throws IOException {
        HttpURLConnection a2 = a(url);
        int timeoutMs = request.getTimeoutMs();
        a2.setConnectTimeout(timeoutMs);
        a2.setReadTimeout(timeoutMs);
        a2.setUseCaches(false);
        a2.setDoInput(true);
        if (Utility.URL_SCHEME.equals(url.getProtocol())) {
            SSLSocketFactory sSLSocketFactory = this.b;
            if (sSLSocketFactory != null) {
                ((HttpsURLConnection) a2).setSSLSocketFactory(sSLSocketFactory);
            }
        }
        return a2;
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, Request<?> request) throws IOException, AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            a(httpURLConnection, request, body);
        }
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, Request<?> request, byte[] bArr) throws IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey(GraphRequest.CONTENT_TYPE_HEADER)) {
            httpURLConnection.setRequestProperty(GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(bArr);
        dataOutputStream.close();
    }

    @DexIgnore
    public static InputStream b(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException unused) {
            return httpURLConnection.getErrorStream();
        }
    }

    @DexIgnore
    public static void b(HttpURLConnection httpURLConnection, Request<?> request) throws IOException, AuthFailureError {
        switch (request.getMethod()) {
            case -1:
                byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    a(httpURLConnection, request, postBody);
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                a(httpURLConnection, request);
                return;
            case 2:
                httpURLConnection.setRequestMethod("PUT");
                a(httpURLConnection, request);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                a(httpURLConnection, request);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }
}
