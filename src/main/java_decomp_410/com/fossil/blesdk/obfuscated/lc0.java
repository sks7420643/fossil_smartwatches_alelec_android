package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lc0 extends ec0 {
    @DexIgnore
    public /* final */ /* synthetic */ kc0 e;

    @DexIgnore
    public lc0(kc0 kc0) {
        this.e = kc0;
    }

    @DexIgnore
    public final void a(Status status) throws RemoteException {
        this.e.a(status);
    }
}
