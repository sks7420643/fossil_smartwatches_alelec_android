package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nh */
public abstract class C2490nh {
    @DexIgnore
    /* renamed from: a */
    public abstract long mo12355a(android.view.ViewGroup viewGroup, androidx.transition.Transition transition, com.fossil.blesdk.obfuscated.C2654ph phVar, com.fossil.blesdk.obfuscated.C2654ph phVar2);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11242a(com.fossil.blesdk.obfuscated.C2654ph phVar);

    @DexIgnore
    /* renamed from: a */
    public abstract java.lang.String[] mo11243a();
}
