package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i80 extends l80 {
    @DexIgnore
    public long M; // = 5000;
    @DexIgnore
    public boolean N;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i80(short s, Peripheral peripheral) {
        super(s, RequestId.VERIFY_FILE, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void a(long j) {
        this.M = j;
    }

    @DexIgnore
    public long m() {
        return this.M;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        if (n().getResultCode() != Request.Result.ResultCode.SUCCESS) {
            c(true);
        } else {
            this.N = true;
        }
        return new JSONObject();
    }

    @DexIgnore
    public void a(d20 d20) {
        kd4.b(d20, "connectionStateChangedNotification");
        if (d20.a() != Peripheral.State.DISCONNECTED) {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.UNKNOWN_ERROR, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        } else if (d20.b() == 19 || this.N) {
            a(n());
        } else {
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.CONNECTION_DROPPED, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
    }
}
