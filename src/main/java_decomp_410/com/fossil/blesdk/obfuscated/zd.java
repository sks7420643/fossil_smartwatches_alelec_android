package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.ud;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd<A, B> extends ud<B> {
    @DexIgnore
    public /* final */ ud<A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ud.b<A> {
        @DexIgnore
        public /* final */ /* synthetic */ ud.b a;

        @DexIgnore
        public a(ud.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(List<A> list, int i, int i2) {
            this.a.a(ld.convert(zd.this.b, list), i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ud.e<A> {
        @DexIgnore
        public /* final */ /* synthetic */ ud.e a;

        @DexIgnore
        public b(ud.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(ld.convert(zd.this.b, list));
        }
    }

    @DexIgnore
    public zd(ud<A> udVar, m3<List<A>, List<B>> m3Var) {
        this.a = udVar;
        this.b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(ld.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadInitial(ud.d dVar, ud.b<B> bVar) {
        this.a.loadInitial(dVar, new a(bVar));
    }

    @DexIgnore
    public void loadRange(ud.g gVar, ud.e<B> eVar) {
        this.a.loadRange(gVar, new b(eVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(ld.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
