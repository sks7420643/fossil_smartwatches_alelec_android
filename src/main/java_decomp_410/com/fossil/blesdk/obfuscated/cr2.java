package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.MFDeviceModel;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ en2 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ FirmwareData b;

        @DexIgnore
        public b(String str, FirmwareData firmwareData) {
            kd4.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            kd4.b(firmwareData, "firmwareData");
            this.a = str;
            this.b = firmwareData;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final FirmwareData b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        kd4.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public cr2(DeviceRepository deviceRepository, en2 en2) {
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = en2;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        FLogger.INSTANCE.getLocal().d(f, "running UseCase");
        if (bVar == null) {
            FLogger.INSTANCE.getLocal().e(f, "Error when update firmware, requestValues is NULL");
            return new c();
        }
        try {
            Device deviceBySerial = this.d.getDeviceBySerial(PortfolioApp.W.c().e());
            if (deviceBySerial == null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f;
                local.e(str, "Error when update firmware, no device exists in DB with serial=" + bVar.a());
                return new c();
            }
            FirmwareData b2 = bVar.b();
            String deviceModel = b2.getDeviceModel();
            String sku = deviceBySerial.getSku();
            if (sku == null) {
                sku = "";
            }
            if (!MFDeviceModel.isSame(deviceModel, sku)) {
                FLogger.INSTANCE.getLocal().e(f, "Firmware is not comparable with device model. Cannot OTA.");
                return new c();
            }
            this.e.a(deviceBySerial.getDeviceId(), deviceBySerial.getFirmwareRevision());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "Start update firmware with version=" + b2.getFirmwareVersion() + ", currentVersion=" + deviceBySerial.getFirmwareRevision());
            PortfolioApp c2 = PortfolioApp.W.c();
            String a2 = bVar.a();
            UserProfile j = PortfolioApp.W.c().j();
            if (j != null) {
                c2.a(a2, b2, j);
                return new d();
            }
            kd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = f;
            local3.d(str3, "running UseCase failed with exception=" + e2);
            e2.printStackTrace();
            return new c();
        }
    }
}
