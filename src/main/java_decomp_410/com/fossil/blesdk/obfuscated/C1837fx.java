package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fx */
public class C1837fx implements com.fossil.blesdk.obfuscated.r64 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.v44 f5305a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.Context f5306b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1909gx f5307c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1753ey f5308d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.z64 f5309e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2844rx f5310f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.util.concurrent.ScheduledExecutorService f5311g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C1519by f5312h; // = new com.fossil.blesdk.obfuscated.C2509nx();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$a */
    public class C1838a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.j74 f5313e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.String f5314f;

        @DexIgnore
        public C1838a(com.fossil.blesdk.obfuscated.j74 j74, java.lang.String str) {
            this.f5313e = j74;
            this.f5314f = str;
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1837fx.this.f5312h.mo9346a(this.f5313e, this.f5314f);
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to set analytics settings data", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$b */
    public class C1839b implements java.lang.Runnable {
        @DexIgnore
        public C1839b() {
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1519by byVar = com.fossil.blesdk.obfuscated.C1837fx.this.f5312h;
                com.fossil.blesdk.obfuscated.C1837fx.this.f5312h = new com.fossil.blesdk.obfuscated.C2509nx();
                byVar.mo9347d();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to disable events", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$c")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$c */
    public class C1840c implements java.lang.Runnable {
        @DexIgnore
        public C1840c() {
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1837fx.this.f5312h.mo9344a();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to send events files", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$d")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$d */
    public class C1841d implements java.lang.Runnable {
        @DexIgnore
        public C1841d() {
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1587cy a = com.fossil.blesdk.obfuscated.C1837fx.this.f5308d.mo10642a();
                com.fossil.blesdk.obfuscated.C3395yx a2 = com.fossil.blesdk.obfuscated.C1837fx.this.f5307c.mo11420a();
                a2.mo30082a((com.fossil.blesdk.obfuscated.r64) com.fossil.blesdk.obfuscated.C1837fx.this);
                com.fossil.blesdk.obfuscated.C1837fx fxVar = com.fossil.blesdk.obfuscated.C1837fx.this;
                com.fossil.blesdk.obfuscated.C2609ox oxVar = new com.fossil.blesdk.obfuscated.C2609ox(com.fossil.blesdk.obfuscated.C1837fx.this.f5305a, com.fossil.blesdk.obfuscated.C1837fx.this.f5306b, com.fossil.blesdk.obfuscated.C1837fx.this.f5311g, a2, com.fossil.blesdk.obfuscated.C1837fx.this.f5309e, a, com.fossil.blesdk.obfuscated.C1837fx.this.f5310f);
                fxVar.f5312h = oxVar;
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to enable events", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$e")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$e */
    public class C1842e implements java.lang.Runnable {
        @DexIgnore
        public C1842e() {
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1837fx.this.f5312h.mo14142b();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to flush events", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fx$f")
    /* renamed from: com.fossil.blesdk.obfuscated.fx$f */
    public class C1843f implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.crashlytics.android.answers.SessionEvent.C0393b f5320e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ boolean f5321f;

        @DexIgnore
        public C1843f(com.crashlytics.android.answers.SessionEvent.C0393b bVar, boolean z) {
            this.f5320e = bVar;
            this.f5321f = z;
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1837fx.this.f5312h.mo9345a(this.f5320e);
                if (this.f5321f) {
                    com.fossil.blesdk.obfuscated.C1837fx.this.f5312h.mo14142b();
                }
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to process event", e);
            }
        }
    }

    @DexIgnore
    public C1837fx(com.fossil.blesdk.obfuscated.v44 v44, android.content.Context context, com.fossil.blesdk.obfuscated.C1909gx gxVar, com.fossil.blesdk.obfuscated.C1753ey eyVar, com.fossil.blesdk.obfuscated.z64 z64, java.util.concurrent.ScheduledExecutorService scheduledExecutorService, com.fossil.blesdk.obfuscated.C2844rx rxVar) {
        this.f5305a = v44;
        this.f5306b = context;
        this.f5307c = gxVar;
        this.f5308d = eyVar;
        this.f5309e = z64;
        this.f5311g = scheduledExecutorService;
        this.f5310f = rxVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11061a(com.crashlytics.android.answers.SessionEvent.C0393b bVar) {
        mo11062a(bVar, false, false);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11067b(com.crashlytics.android.answers.SessionEvent.C0393b bVar) {
        mo11062a(bVar, false, true);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11070c(com.crashlytics.android.answers.SessionEvent.C0393b bVar) {
        mo11062a(bVar, true, false);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11063a(com.fossil.blesdk.obfuscated.j74 j74, java.lang.String str) {
        mo11064a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1837fx.C1838a(j74, str));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11066b() {
        mo11064a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1837fx.C1841d());
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11069c() {
        mo11064a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1837fx.C1842e());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11060a() {
        mo11064a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1837fx.C1839b());
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo11068b(java.lang.Runnable runnable) {
        try {
            this.f5311g.submit(runnable).get();
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to run events task", e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11065a(java.lang.String str) {
        mo11064a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1837fx.C1840c());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11062a(com.crashlytics.android.answers.SessionEvent.C0393b bVar, boolean z, boolean z2) {
        com.fossil.blesdk.obfuscated.C1837fx.C1843f fVar = new com.fossil.blesdk.obfuscated.C1837fx.C1843f(bVar, z2);
        if (z) {
            mo11068b((java.lang.Runnable) fVar);
        } else {
            mo11064a((java.lang.Runnable) fVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11064a(java.lang.Runnable runnable) {
        try {
            this.f5311g.submit(runnable);
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Failed to submit events task", e);
        }
    }
}
