package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;
import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k81 extends j81<Object> {
    @DexIgnore
    public final boolean a(w91 w91) {
        return w91 instanceof t81.c;
    }

    @DexIgnore
    public final m81<Object> b(Object obj) {
        t81.c cVar = (t81.c) obj;
        if (cVar.zzbyl.c()) {
            cVar.zzbyl = (m81) cVar.zzbyl.clone();
        }
        return cVar.zzbyl;
    }

    @DexIgnore
    public final void c(Object obj) {
        a(obj).f();
    }

    @DexIgnore
    public final m81<Object> a(Object obj) {
        return ((t81.c) obj).zzbyl;
    }

    @DexIgnore
    public final <UT, UB> UB a(ma1 ma1, Object obj, i81 i81, m81<Object> m81, UB ub, eb1<UT, UB> eb1) throws IOException {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final int a(Map.Entry<?, ?> entry) {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final void a(sb1 sb1, Map.Entry<?, ?> entry) throws IOException {
        entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Object a(i81 i81, w91 w91, int i) {
        return i81.a(w91, i);
    }

    @DexIgnore
    public final void a(ma1 ma1, Object obj, i81 i81, m81<Object> m81) throws IOException {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final void a(zzte zzte, Object obj, i81 i81, m81<Object> m81) throws IOException {
        throw new NoSuchMethodError();
    }
}
