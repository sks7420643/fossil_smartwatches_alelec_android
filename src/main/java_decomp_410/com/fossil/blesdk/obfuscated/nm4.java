package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.yl4;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.misfit.frameworks.common.enums.Action;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nm4 {
    @DexIgnore
    public /* final */ dm4 a;
    @DexIgnore
    public /* final */ Response b;

    @DexIgnore
    public nm4(dm4 dm4, Response response) {
        this.a = dm4;
        this.b = response;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        if (r3.z().b() == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        return false;
     */
    @DexIgnore
    public static boolean a(Response response, dm4 dm4) {
        int B = response.B();
        if (!(B == 200 || B == 410 || B == 414 || B == 501 || B == 203 || B == 204)) {
            if (B != 307) {
                if (!(B == 308 || B == 404 || B == 405)) {
                    switch (B) {
                        case 300:
                        case Action.Presenter.NEXT /*301*/:
                            break;
                        case Action.Presenter.PREVIOUS /*302*/:
                            break;
                    }
                }
            }
            if (response.e("Expires") == null) {
                if (response.z().d() == -1) {
                    if (!response.z().c()) {
                    }
                }
            }
        }
        if (response.z().i() || dm4.b().i()) {
            return false;
        }
        return true;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ dm4 b;
        @DexIgnore
        public /* final */ Response c;
        @DexIgnore
        public Date d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Date h;
        @DexIgnore
        public long i;
        @DexIgnore
        public long j;
        @DexIgnore
        public String k;
        @DexIgnore
        public int l; // = -1;

        @DexIgnore
        public a(long j2, dm4 dm4, Response response) {
            this.a = j2;
            this.b = dm4;
            this.c = response;
            if (response != null) {
                this.i = response.M();
                this.j = response.K();
                yl4 D = response.D();
                int b2 = D.b();
                for (int i2 = 0; i2 < b2; i2++) {
                    String a2 = D.a(i2);
                    String b3 = D.b(i2);
                    if (ConfigItem.KEY_DATE.equalsIgnoreCase(a2)) {
                        this.d = an4.a(b3);
                        this.e = b3;
                    } else if ("Expires".equalsIgnoreCase(a2)) {
                        this.h = an4.a(b3);
                    } else if ("Last-Modified".equalsIgnoreCase(a2)) {
                        this.f = an4.a(b3);
                        this.g = b3;
                    } else if ("ETag".equalsIgnoreCase(a2)) {
                        this.k = b3;
                    } else if ("Age".equalsIgnoreCase(a2)) {
                        this.l = bn4.a(b3, -1);
                    }
                }
            }
        }

        @DexIgnore
        public final long a() {
            Date date = this.d;
            long j2 = 0;
            if (date != null) {
                j2 = Math.max(0, this.j - date.getTime());
            }
            int i2 = this.l;
            if (i2 != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) i2));
            }
            long j3 = this.j;
            return j2 + (j3 - this.i) + (this.a - j3);
        }

        @DexIgnore
        public final long b() {
            long j2;
            long j3;
            il4 z = this.c.z();
            if (z.d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) z.d());
            }
            if (this.h != null) {
                Date date = this.d;
                if (date != null) {
                    j3 = date.getTime();
                } else {
                    j3 = this.j;
                }
                long time = this.h.getTime() - j3;
                if (time > 0) {
                    return time;
                }
                return 0;
            } else if (this.f == null || this.c.L().g().l() != null) {
                return 0;
            } else {
                Date date2 = this.d;
                if (date2 != null) {
                    j2 = date2.getTime();
                } else {
                    j2 = this.i;
                }
                long time2 = j2 - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        @DexIgnore
        public nm4 c() {
            nm4 d2 = d();
            return (d2.a == null || !this.b.b().j()) ? d2 : new nm4((dm4) null, (Response) null);
        }

        @DexIgnore
        public final nm4 d() {
            if (this.c == null) {
                return new nm4(this.b, (Response) null);
            }
            if (this.b.d() && this.c.C() == null) {
                return new nm4(this.b, (Response) null);
            }
            if (!nm4.a(this.c, this.b)) {
                return new nm4(this.b, (Response) null);
            }
            il4 b2 = this.b.b();
            if (b2.h() || a(this.b)) {
                return new nm4(this.b, (Response) null);
            }
            il4 z = this.c.z();
            long a2 = a();
            long b3 = b();
            if (b2.d() != -1) {
                b3 = Math.min(b3, TimeUnit.SECONDS.toMillis((long) b2.d()));
            }
            long j2 = 0;
            long millis = b2.f() != -1 ? TimeUnit.SECONDS.toMillis((long) b2.f()) : 0;
            if (!z.g() && b2.e() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) b2.e());
            }
            if (!z.h()) {
                long j3 = millis + a2;
                if (j3 < j2 + b3) {
                    Response.a H = this.c.H();
                    if (j3 >= b3) {
                        H.a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (a2 > LogBuilder.MAX_INTERVAL && e()) {
                        H.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new nm4((dm4) null, H.a());
                }
            }
            String str = this.k;
            String str2 = "If-Modified-Since";
            if (str != null) {
                str2 = "If-None-Match";
            } else if (this.f != null) {
                str = this.g;
            } else if (this.d == null) {
                return new nm4(this.b, (Response) null);
            } else {
                str = this.e;
            }
            yl4.a a3 = this.b.c().a();
            hm4.a.a(a3, str2, str);
            dm4.a f2 = this.b.f();
            f2.a(a3.a());
            return new nm4(f2.a(), this.c);
        }

        @DexIgnore
        public final boolean e() {
            return this.c.z().d() == -1 && this.h == null;
        }

        @DexIgnore
        public static boolean a(dm4 dm4) {
            return (dm4.a("If-Modified-Since") == null && dm4.a("If-None-Match") == null) ? false : true;
        }
    }
}
