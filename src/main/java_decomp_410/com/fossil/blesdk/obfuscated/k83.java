package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k83 {
    @DexIgnore
    public /* final */ f83 a;
    @DexIgnore
    public /* final */ u83 b;
    @DexIgnore
    public /* final */ p83 c;

    @DexIgnore
    public k83(f83 f83, u83 u83, p83 p83) {
        kd4.b(f83, "mActiveTimeOverviewDayView");
        kd4.b(u83, "mActiveTimeOverviewWeekView");
        kd4.b(p83, "mActiveTimeOverviewMonthView");
        this.a = f83;
        this.b = u83;
        this.c = p83;
    }

    @DexIgnore
    public final f83 a() {
        return this.a;
    }

    @DexIgnore
    public final p83 b() {
        return this.c;
    }

    @DexIgnore
    public final u83 c() {
        return this.b;
    }
}
