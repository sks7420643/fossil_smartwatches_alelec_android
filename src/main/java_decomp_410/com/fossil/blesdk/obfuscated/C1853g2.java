package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g2 */
public class C1853g2 extends com.fossil.blesdk.obfuscated.C1765f2 {

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.widget.SeekBar f5374d;

    @DexIgnore
    /* renamed from: e */
    public android.graphics.drawable.Drawable f5375e;

    @DexIgnore
    /* renamed from: f */
    public android.content.res.ColorStateList f5376f; // = null;

    @DexIgnore
    /* renamed from: g */
    public android.graphics.PorterDuff.Mode f5377g; // = null;

    @DexIgnore
    /* renamed from: h */
    public boolean f5378h; // = false;

    @DexIgnore
    /* renamed from: i */
    public boolean f5379i; // = false;

    @DexIgnore
    public C1853g2(android.widget.SeekBar seekBar) {
        super(seekBar);
        this.f5374d = seekBar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10698a(android.util.AttributeSet attributeSet, int i) {
        super.mo10698a(attributeSet, i);
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17205a(this.f5374d.getContext(), attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar, i, 0);
        android.graphics.drawable.Drawable c = a.mo18424c(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_android_thumb);
        if (c != null) {
            this.f5374d.setThumb(c);
        }
        mo11111b(a.mo18422b(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_tickMark));
        if (a.mo18432g(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_tickMarkTintMode)) {
            this.f5377g = com.fossil.blesdk.obfuscated.C2181k2.m9311a(a.mo18425d(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_tickMarkTintMode, -1), this.f5377g);
            this.f5379i = true;
        }
        if (a.mo18432g(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_tickMarkTint)) {
            this.f5376f = a.mo18416a(com.fossil.blesdk.obfuscated.C1368a0.AppCompatSeekBar_tickMarkTint);
            this.f5378h = true;
        }
        a.mo18418a();
        mo11112c();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11111b(android.graphics.drawable.Drawable drawable) {
        android.graphics.drawable.Drawable drawable2 = this.f5375e;
        if (drawable2 != null) {
            drawable2.setCallback((android.graphics.drawable.Drawable.Callback) null);
        }
        this.f5375e = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f5374d);
            com.fossil.blesdk.obfuscated.C1538c7.m5305a(drawable, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f5374d));
            if (drawable.isStateful()) {
                drawable.setState(this.f5374d.getDrawableState());
            }
            mo11112c();
        }
        this.f5374d.invalidate();
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo11112c() {
        if (this.f5375e == null) {
            return;
        }
        if (this.f5378h || this.f5379i) {
            this.f5375e = com.fossil.blesdk.obfuscated.C1538c7.m5314i(this.f5375e.mutate());
            if (this.f5378h) {
                com.fossil.blesdk.obfuscated.C1538c7.m5299a(this.f5375e, this.f5376f);
            }
            if (this.f5379i) {
                com.fossil.blesdk.obfuscated.C1538c7.m5302a(this.f5375e, this.f5377g);
            }
            if (this.f5375e.isStateful()) {
                this.f5375e.setState(this.f5374d.getDrawableState());
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11113d() {
        android.graphics.drawable.Drawable drawable = this.f5375e;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f5374d.getDrawableState())) {
            this.f5374d.invalidateDrawable(drawable);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo11114e() {
        android.graphics.drawable.Drawable drawable = this.f5375e;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11110a(android.graphics.Canvas canvas) {
        if (this.f5375e != null) {
            int max = this.f5374d.getMax();
            int i = 1;
            if (max > 1) {
                int intrinsicWidth = this.f5375e.getIntrinsicWidth();
                int intrinsicHeight = this.f5375e.getIntrinsicHeight();
                int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
                if (intrinsicHeight >= 0) {
                    i = intrinsicHeight / 2;
                }
                this.f5375e.setBounds(-i2, -i, i2, i);
                float width = ((float) ((this.f5374d.getWidth() - this.f5374d.getPaddingLeft()) - this.f5374d.getPaddingRight())) / ((float) max);
                int save = canvas.save();
                canvas.translate((float) this.f5374d.getPaddingLeft(), (float) (this.f5374d.getHeight() / 2));
                for (int i3 = 0; i3 <= max; i3++) {
                    this.f5375e.draw(canvas);
                    canvas.translate(width, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                canvas.restoreToCount(save);
            }
        }
    }
}
