package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hs2 extends zr2 implements vl3 {
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public ul3 j;
    @DexIgnore
    public tr3<ef2> k;
    @DexIgnore
    public cs2 l;
    @DexIgnore
    public fm3 m;
    @DexIgnore
    public ju3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final hs2 a() {
            return new hs2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 a;

        @DexIgnore
        public b(hs2 hs2) {
            this.a = hs2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            hs2.a(this.a).b(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public c(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hs2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public d(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            hs2.a(this.e).a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public e(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            hs2.a(this.e).b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public f(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            hs2.a(this.e).c(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public g(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ef2 e;
        @DexIgnore
        public /* final */ /* synthetic */ hs2 f;

        @DexIgnore
        public h(ef2 ef2, hs2 hs2) {
            this.e = ef2;
            this.f = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputEditText textInputEditText = this.e.s;
            kd4.a((Object) textInputEditText, "binding.etBirthday");
            Editable text = textInputEditText.getText();
            if (text == null || text.length() == 0) {
                hs2.a(this.f).k();
            } else {
                hs2.a(this.f).j();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public i(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hs2.a(this.e).a(Gender.MALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public j(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hs2.a(this.e).a(Gender.FEMALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 e;

        @DexIgnore
        public k(hs2 hs2) {
            this.e = hs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hs2.a(this.e).a(Gender.OTHER);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 a;

        @DexIgnore
        public l(hs2 hs2) {
            this.a = hs2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            hs2.a(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements cc<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ hs2 a;

        @DexIgnore
        public m(hs2 hs2) {
            this.a = hs2;
        }

        @DexIgnore
        public final void a(Date date) {
            if (date != null) {
                Calendar i = hs2.a(this.a).i();
                i.setTime(date);
                hs2.a(this.a).a(date, i);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ul3 a(hs2 hs2) {
        ul3 ul3 = hs2.j;
        if (ul3 != null) {
            return ul3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(boolean z) {
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.G;
                    if (imageView != null) {
                        kd4.a((Object) imageView, "it");
                        imageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void B(boolean z) {
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.F;
                    if (imageView != null) {
                        kd4.a((Object) imageView, "it");
                        imageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void H0() {
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    kd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.w;
                    kd4.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.w;
                    kd4.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(false);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void K(String str) {
        kd4.b(str, "birthdate");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputEditText textInputEditText = a2.s;
                    if (textInputEditText != null) {
                        textInputEditText.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(boolean z, String str) {
        kd4.b(str, "message");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.B;
                    kd4.a((Object) textInputLayout, "it.inputBirthday");
                    textInputLayout.setErrorEnabled(z);
                    TextInputLayout textInputLayout2 = a2.B;
                    kd4.a((Object) textInputLayout2, "it.inputBirthday");
                    textInputLayout2.setError(str);
                    ImageView imageView = a2.D;
                    kd4.a((Object) imageView, "it.ivCheckedBirthday");
                    imageView.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void c(SignUpEmailAuth signUpEmailAuth) {
        kd4.b(signUpEmailAuth, "auth");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    kd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_ProfileSetup_CTA__CreateAccount));
                    TextInputLayout textInputLayout = a2.C;
                    kd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setVisibility(8);
                    ImageView imageView = a2.E;
                    kd4.a((Object) imageView, "it.ivCheckedEmail");
                    imageView.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.t.setText(signUpEmailAuth.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        kd4.a((Object) textInputEditText, "it.etEmail");
                        textInputEditText.setKeyListener((KeyListener) null);
                        TextInputLayout textInputLayout2 = a2.C;
                        kd4.a((Object) textInputLayout2, "it.inputEmail");
                        textInputLayout2.setFocusable(false);
                        TextInputLayout textInputLayout3 = a2.C;
                        kd4.a((Object) textInputLayout3, "it.inputEmail");
                        textInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        tr3<ef2> tr3 = this.k;
        if (tr3 != null) {
            ef2 a2 = tr3.a();
            if (a2 != null) {
                DashBar dashBar = a2.I;
                if (dashBar != null) {
                    gs3.a aVar = gs3.a;
                    kd4.a((Object) dashBar, "this");
                    aVar.b(dashBar, 500);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g0() {
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public void k() {
        if (isActive()) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AgreedTerms_Text__PleaseWait);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            S(a2);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.k = new tr3<>(this, (ef2) qa.a(layoutInflater, R.layout.fragment_profile_setup, viewGroup, false, O0()));
        tr3<ef2> tr3 = this.k;
        if (tr3 != null) {
            ef2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ul3 ul3 = this.j;
        if (ul3 != null) {
            ul3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ul3 ul3 = this.j;
        if (ul3 != null) {
            ul3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ic a2 = lc.a(activity).a(ju3.class);
            kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.n = (ju3) a2;
            this.l = (cs2) getChildFragmentManager().a(cs2.r.a());
            if (this.l == null) {
                this.l = cs2.r.b();
            }
            l42 g2 = PortfolioApp.W.c().g();
            cs2 cs2 = this.l;
            if (cs2 != null) {
                g2.a(new dm3(cs2)).a(this);
                ju3 ju3 = this.n;
                if (ju3 != null) {
                    ju3.c().a(this, new m(this));
                    tr3<ef2> tr3 = this.k;
                    if (tr3 != null) {
                        ef2 a3 = tr3.a();
                        if (a3 != null) {
                            a3.t.addTextChangedListener(new d(this));
                            a3.u.addTextChangedListener(new e(this));
                            a3.v.addTextChangedListener(new f(this));
                            a3.H.setOnClickListener(new g(this));
                            a3.s.setOnClickListener(new h(a3, this));
                            a3.y.setOnClickListener(new i(this));
                            a3.x.setOnClickListener(new j(this));
                            a3.z.setOnClickListener(new k(this));
                            a3.q.setOnCheckedChangeListener(new l(this));
                            a3.r.setOnCheckedChangeListener(new b(this));
                            a3.w.setOnClickListener(new c(this));
                            FlexibleTextView flexibleTextView = a3.A;
                            kd4.a((Object) flexibleTextView, "binding.ftvTermsService");
                            flexibleTextView.setMovementMethod(new LinkMovementMethod());
                        }
                        R("profile_setup_view");
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
                kd4.d("mUserBirthDayViewModel");
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void t0() {
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    kd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.w;
                    kd4.a((Object) flexibleButton2, "it.fbCreateAccount");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.w;
                    kd4.a((Object) flexibleButton3, "it.fbCreateAccount");
                    flexibleButton3.setFocusable(true);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ul3 ul3) {
        kd4.b(ul3, "presenter");
        this.j = ul3;
    }

    @DexIgnore
    public void a(boolean z, boolean z2, String str) {
        int i2;
        kd4.b(str, "errorMessage");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.C;
                    kd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setErrorEnabled(z2);
                    TextInputLayout textInputLayout2 = a2.C;
                    kd4.a((Object) textInputLayout2, "it.inputEmail");
                    textInputLayout2.setError(str);
                    ImageView imageView = a2.E;
                    kd4.a((Object) imageView, "it.ivCheckedEmail");
                    if (z) {
                        TextInputLayout textInputLayout3 = a2.C;
                        kd4.a((Object) textInputLayout3, "it.inputEmail");
                        if (textInputLayout3.getVisibility() == 0) {
                            i2 = 0;
                            imageView.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    imageView.setVisibility(i2);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g(int i2, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void b(MFUser mFUser) {
        Date date;
        kd4.b(mFUser, "user");
        if (isActive()) {
            H0();
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    kd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_CTA__Continue));
                    Gender gender = mFUser.getGender();
                    kd4.a((Object) gender, "user.gender");
                    a(gender);
                    String email = mFUser.getEmail();
                    boolean z = false;
                    if (!(email == null || qf4.a(email))) {
                        a2.t.setText(mFUser.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        kd4.a((Object) textInputEditText, "it.etEmail");
                        a((TextView) textInputEditText);
                    }
                    String firstName = mFUser.getFirstName();
                    kd4.a((Object) firstName, "user.firstName");
                    if (!qf4.a(firstName)) {
                        a2.u.setText(mFUser.getFirstName());
                        TextInputEditText textInputEditText2 = a2.u;
                        kd4.a((Object) textInputEditText2, "it.etFirstName");
                        a((TextView) textInputEditText2);
                    }
                    String lastName = mFUser.getLastName();
                    kd4.a((Object) lastName, "user.lastName");
                    if (!qf4.a(lastName)) {
                        a2.v.setText(mFUser.getLastName());
                        TextInputEditText textInputEditText3 = a2.v;
                        kd4.a((Object) textInputEditText3, "it.etLastName");
                        a((TextView) textInputEditText3);
                    }
                    String birthday = mFUser.getBirthday();
                    if (birthday == null || qf4.a(birthday)) {
                        z = true;
                    }
                    if (!z) {
                        TextInputEditText textInputEditText4 = a2.s;
                        kd4.a((Object) textInputEditText4, "it.etBirthday");
                        a((TextView) textInputEditText4);
                        try {
                            SimpleDateFormat simpleDateFormat = rk2.a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                ul3 ul3 = this.j;
                                if (ul3 != null) {
                                    Calendar i2 = ul3.i();
                                    i2.setTime(date);
                                    ul3 ul32 = this.j;
                                    if (ul32 == null) {
                                        kd4.d("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        ul32.a(date, i2);
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e2);
                            date = new Date();
                        }
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        kd4.b(bundle, "data");
        cs2 cs2 = this.l;
        if (cs2 != null) {
            cs2.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            cs2.show(childFragmentManager, cs2.r.a());
        }
    }

    @DexIgnore
    public void a(Spanned spanned) {
        kd4.b(spanned, "message");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.A;
                    kd4.a((Object) flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "auth");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.w;
                    kd4.a((Object) flexibleButton, "it.fbCreateAccount");
                    flexibleButton.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_ProfileSetup_CTA__CreateAccount));
                    a2.u.setText(signUpSocialAuth.getFirstName());
                    a2.v.setText(signUpSocialAuth.getLastName());
                    TextInputLayout textInputLayout = a2.C;
                    kd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.t.setText(signUpSocialAuth.getEmail());
                        TextInputEditText textInputEditText = a2.t;
                        kd4.a((Object) textInputEditText, "it.etEmail");
                        textInputEditText.setKeyListener((KeyListener) null);
                        TextInputLayout textInputLayout2 = a2.C;
                        kd4.a((Object) textInputLayout2, "it.inputEmail");
                        textInputLayout2.setFocusable(false);
                        TextInputLayout textInputLayout3 = a2.C;
                        kd4.a((Object) textInputLayout3, "it.inputEmail");
                        textInputLayout3.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(TextView textView) {
        Drawable c2 = k6.c(PortfolioApp.W.c(), R.drawable.bg_border_disable_color_primary);
        textView.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.blueGrey50));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(c2);
    }

    @DexIgnore
    public void a(Gender gender) {
        kd4.b(gender, "gender");
        if (isActive()) {
            tr3<ef2> tr3 = this.k;
            if (tr3 != null) {
                ef2 a2 = tr3.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.W.c();
                    int a3 = k6.a((Context) c2, (int) R.color.activeColorPrimary);
                    int a4 = k6.a((Context) c2, (int) R.color.white);
                    Drawable c3 = k6.c(c2, R.drawable.bg_border_disable_color_primary);
                    Drawable c4 = k6.c(c2, R.drawable.bg_border_active_color_primary);
                    a2.z.setTextColor(a3);
                    a2.x.setTextColor(a3);
                    a2.y.setTextColor(a3);
                    FlexibleButton flexibleButton = a2.z;
                    kd4.a((Object) flexibleButton, "it.fbOther");
                    flexibleButton.setBackground(c3);
                    FlexibleButton flexibleButton2 = a2.x;
                    kd4.a((Object) flexibleButton2, "it.fbFemale");
                    flexibleButton2.setBackground(c3);
                    FlexibleButton flexibleButton3 = a2.y;
                    kd4.a((Object) flexibleButton3, "it.fbMale");
                    flexibleButton3.setBackground(c3);
                    int i2 = is2.a[gender.ordinal()];
                    if (i2 == 1) {
                        a2.y.setTextColor(a4);
                        FlexibleButton flexibleButton4 = a2.y;
                        kd4.a((Object) flexibleButton4, "it.fbMale");
                        flexibleButton4.setBackground(c4);
                    } else if (i2 == 2) {
                        a2.x.setTextColor(a4);
                        FlexibleButton flexibleButton5 = a2.x;
                        kd4.a((Object) flexibleButton5, "it.fbFemale");
                        flexibleButton5.setBackground(c4);
                    } else if (i2 == 3) {
                        a2.z.setTextColor(a4);
                        FlexibleButton flexibleButton6 = a2.z;
                        kd4.a((Object) flexibleButton6, "it.fbOther");
                        flexibleButton6.setBackground(c4);
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }
}
