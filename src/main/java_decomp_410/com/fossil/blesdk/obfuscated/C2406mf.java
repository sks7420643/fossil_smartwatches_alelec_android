package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mf */
public interface C2406mf extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mf$a */
    public static abstract class C2407a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C2406mf {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mf$a$a */
        public static class C2408a implements com.fossil.blesdk.obfuscated.C2406mf {

            @DexIgnore
            /* renamed from: e */
            public android.os.IBinder f7484e;

            @DexIgnore
            public C2408a(android.os.IBinder iBinder) {
                this.f7484e = iBinder;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo13602a(java.lang.String[] strArr) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                    obtain.writeStringArray(strArr);
                    this.f7484e.transact(1, obtain, (android.os.Parcel) null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public android.os.IBinder asBinder() {
                return this.f7484e;
            }
        }

        @DexIgnore
        public C2407a() {
            attachInterface(this, "androidx.room.IMultiInstanceInvalidationCallback");
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2406mf m10716a(android.os.IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            android.os.IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof com.fossil.blesdk.obfuscated.C2406mf)) {
                return new com.fossil.blesdk.obfuscated.C2406mf.C2407a.C2408a(iBinder);
            }
            return (com.fossil.blesdk.obfuscated.C2406mf) queryLocalInterface;
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationCallback");
                mo13602a(parcel.createStringArray());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationCallback");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo13602a(java.lang.String[] strArr) throws android.os.RemoteException;
}
