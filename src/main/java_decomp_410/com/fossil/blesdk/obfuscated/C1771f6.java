package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f6 */
public class C1771f6 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.Object f5029a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Field f5030b;

    @DexIgnore
    /* renamed from: c */
    public static boolean f5031c;

    @DexIgnore
    /* renamed from: a */
    public static android.util.SparseArray<android.os.Bundle> m6747a(java.util.List<android.os.Bundle> list) {
        int size = list.size();
        android.util.SparseArray<android.os.Bundle> sparseArray = null;
        for (int i = 0; i < size; i++) {
            android.os.Bundle bundle = list.get(i);
            if (bundle != null) {
                if (sparseArray == null) {
                    sparseArray = new android.util.SparseArray<>();
                }
                sparseArray.put(i, bundle);
            }
        }
        return sparseArray;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m6744a(android.app.Notification notification) {
        synchronized (f5029a) {
            if (f5031c) {
                return null;
            }
            try {
                if (f5030b == null) {
                    java.lang.reflect.Field declaredField = android.app.Notification.class.getDeclaredField(com.facebook.applinks.AppLinkData.ARGUMENTS_EXTRAS_KEY);
                    if (!android.os.Bundle.class.isAssignableFrom(declaredField.getType())) {
                        android.util.Log.e("NotificationCompat", "Notification.extras field is not of type Bundle");
                        f5031c = true;
                        return null;
                    }
                    declaredField.setAccessible(true);
                    f5030b = declaredField;
                }
                android.os.Bundle bundle = (android.os.Bundle) f5030b.get(notification);
                if (bundle == null) {
                    bundle = new android.os.Bundle();
                    f5030b.set(notification, bundle);
                }
                return bundle;
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e("NotificationCompat", "Unable to access notification extras", e);
                f5031c = true;
                return null;
            } catch (java.lang.NoSuchFieldException e2) {
                android.util.Log.e("NotificationCompat", "Unable to access notification extras", e2);
                f5031c = true;
                return null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m6743a(android.app.Notification.Builder builder, com.fossil.blesdk.obfuscated.C1597d6.C1598a aVar) {
        builder.addAction(aVar.mo9763e(), aVar.mo9767i(), aVar.mo9759a());
        android.os.Bundle bundle = new android.os.Bundle(aVar.mo9762d());
        if (aVar.mo9764f() != null) {
            bundle.putParcelableArray("android.support.remoteInputs", m6748a(aVar.mo9764f()));
        }
        if (aVar.mo9761c() != null) {
            bundle.putParcelableArray("android.support.dataRemoteInputs", m6748a(aVar.mo9761c()));
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.mo9760b());
        return bundle;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m6745a(com.fossil.blesdk.obfuscated.C1597d6.C1598a aVar) {
        android.os.Bundle bundle;
        android.os.Bundle bundle2 = new android.os.Bundle();
        bundle2.putInt("icon", aVar.mo9763e());
        bundle2.putCharSequence("title", aVar.mo9767i());
        bundle2.putParcelable("actionIntent", aVar.mo9759a());
        if (aVar.mo9762d() != null) {
            bundle = new android.os.Bundle(aVar.mo9762d());
        } else {
            bundle = new android.os.Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", aVar.mo9760b());
        bundle2.putBundle(com.facebook.applinks.AppLinkData.ARGUMENTS_EXTRAS_KEY, bundle);
        bundle2.putParcelableArray("remoteInputs", m6748a(aVar.mo9764f()));
        bundle2.putBoolean("showsUserInterface", aVar.mo9766h());
        bundle2.putInt("semanticAction", aVar.mo9765g());
        return bundle2;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m6746a(com.fossil.blesdk.obfuscated.C1926h6 h6Var) {
        new android.os.Bundle();
        h6Var.mo11598a();
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle[] m6748a(com.fossil.blesdk.obfuscated.C1926h6[] h6VarArr) {
        if (h6VarArr == null) {
            return null;
        }
        android.os.Bundle[] bundleArr = new android.os.Bundle[h6VarArr.length];
        if (h6VarArr.length <= 0) {
            return bundleArr;
        }
        m6746a(h6VarArr[0]);
        throw null;
    }
}
