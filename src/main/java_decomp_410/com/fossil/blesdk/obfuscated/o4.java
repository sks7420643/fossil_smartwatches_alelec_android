package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o4 {
    @DexIgnore
    public s4<n4> a; // = new t4(256);
    @DexIgnore
    public s4<SolverVariable> b; // = new t4(256);
    @DexIgnore
    public SolverVariable[] c; // = new SolverVariable[32];
}
