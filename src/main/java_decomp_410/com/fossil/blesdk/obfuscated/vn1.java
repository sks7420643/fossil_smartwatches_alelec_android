package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vn1<TResult, TContinuationResult> {
    @DexIgnore
    wn1<TContinuationResult> then(TResult tresult) throws Exception;
}
