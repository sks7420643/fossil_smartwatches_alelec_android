package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.rn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yn0 implements rn0.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ rn0 b;

    @DexIgnore
    public yn0(rn0 rn0, Bundle bundle) {
        this.b = rn0;
        this.a = bundle;
    }

    @DexIgnore
    public final void a(tn0 tn0) {
        this.b.a.b(this.a);
    }

    @DexIgnore
    public final int getState() {
        return 1;
    }
}
