package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vs */
public final class C3135vs implements com.bumptech.glide.load.ImageHeaderParser {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ byte[] f10399a; // = "Exif\u0000\u0000".getBytes(java.nio.charset.Charset.forName("UTF-8"));

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int[] f10400b; // = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vs$a */
    public static final class C3136a implements com.fossil.blesdk.obfuscated.C3135vs.C3138c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.nio.ByteBuffer f10401a;

        @DexIgnore
        public C3136a(java.nio.ByteBuffer byteBuffer) {
            this.f10401a = byteBuffer;
            byteBuffer.order(java.nio.ByteOrder.BIG_ENDIAN);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17200a() {
            return ((mo17203c() << 8) & 65280) | (mo17203c() & 255);
        }

        @DexIgnore
        /* renamed from: b */
        public short mo17202b() {
            return (short) (mo17203c() & 255);
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17203c() {
            if (this.f10401a.remaining() < 1) {
                return -1;
            }
            return this.f10401a.get();
        }

        @DexIgnore
        public long skip(long j) {
            int min = (int) java.lang.Math.min((long) this.f10401a.remaining(), j);
            java.nio.ByteBuffer byteBuffer = this.f10401a;
            byteBuffer.position(byteBuffer.position() + min);
            return (long) min;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17201a(byte[] bArr, int i) {
            int min = java.lang.Math.min(i, this.f10401a.remaining());
            if (min == 0) {
                return -1;
            }
            this.f10401a.get(bArr, 0, min);
            return min;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vs$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vs$b */
    public static final class C3137b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.nio.ByteBuffer f10402a;

        @DexIgnore
        public C3137b(byte[] bArr, int i) {
            this.f10402a = (java.nio.ByteBuffer) java.nio.ByteBuffer.wrap(bArr).order(java.nio.ByteOrder.BIG_ENDIAN).limit(i);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17207a(java.nio.ByteOrder byteOrder) {
            this.f10402a.order(byteOrder);
        }

        @DexIgnore
        /* renamed from: b */
        public int mo17209b(int i) {
            if (mo17208a(i, 4)) {
                return this.f10402a.getInt(i);
            }
            return -1;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17205a() {
            return this.f10402a.remaining();
        }

        @DexIgnore
        /* renamed from: a */
        public short mo17206a(int i) {
            if (mo17208a(i, 2)) {
                return this.f10402a.getShort(i);
            }
            return -1;
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo17208a(int i, int i2) {
            return this.f10402a.remaining() - i >= i2;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.vs$c */
    public interface C3138c {
        @DexIgnore
        /* renamed from: a */
        int mo17200a() throws java.io.IOException;

        @DexIgnore
        /* renamed from: a */
        int mo17201a(byte[] bArr, int i) throws java.io.IOException;

        @DexIgnore
        /* renamed from: b */
        short mo17202b() throws java.io.IOException;

        @DexIgnore
        /* renamed from: c */
        int mo17203c() throws java.io.IOException;

        @DexIgnore
        long skip(long j) throws java.io.IOException;
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vs$d")
    /* renamed from: com.fossil.blesdk.obfuscated.vs$d */
    public static final class C3139d implements com.fossil.blesdk.obfuscated.C3135vs.C3138c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.io.InputStream f10403a;

        @DexIgnore
        public C3139d(java.io.InputStream inputStream) {
            this.f10403a = inputStream;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17200a() throws java.io.IOException {
            return ((this.f10403a.read() << 8) & 65280) | (this.f10403a.read() & 255);
        }

        @DexIgnore
        /* renamed from: b */
        public short mo17202b() throws java.io.IOException {
            return (short) (this.f10403a.read() & 255);
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17203c() throws java.io.IOException {
            return this.f10403a.read();
        }

        @DexIgnore
        public long skip(long j) throws java.io.IOException {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f10403a.skip(j2);
                if (skip <= 0) {
                    if (this.f10403a.read() == -1) {
                        break;
                    }
                    skip = 1;
                }
                j2 -= skip;
            }
            return j - j2;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17201a(byte[] bArr, int i) throws java.io.IOException {
            int i2 = i;
            while (i2 > 0) {
                int read = this.f10403a.read(bArr, i - i2, i2);
                if (read == -1) {
                    break;
                }
                i2 -= read;
            }
            return i - i2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m15424a(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15426a(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.ImageHeaderParser.ImageType mo3939a(java.io.InputStream inputStream) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(inputStream);
        return mo17197a((com.fossil.blesdk.obfuscated.C3135vs.C3138c) new com.fossil.blesdk.obfuscated.C3135vs.C3139d(inputStream));
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo17199b(com.fossil.blesdk.obfuscated.C3135vs.C3138c cVar) throws java.io.IOException {
        short b;
        int a;
        long j;
        do {
            if (cVar.mo17202b() != 255) {
                if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                    android.util.Log.d("DfltImageHeaderParser", "Unknown segmentId=" + r0);
                }
                return -1;
            }
            b = cVar.mo17202b();
            if (b == 218) {
                return -1;
            }
            if (b == 217) {
                if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                    android.util.Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return -1;
            }
            a = cVar.mo17200a() - 2;
            if (b == 225) {
                return a;
            }
            j = (long) a;
        } while (cVar.skip(j) == j);
        if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
            android.util.Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + b + ", wanted to skip: " + a + ", but actually skipped: " + r7);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.ImageHeaderParser.ImageType mo3940a(java.nio.ByteBuffer byteBuffer) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(byteBuffer);
        return mo17197a((com.fossil.blesdk.obfuscated.C3135vs.C3138c) new com.fossil.blesdk.obfuscated.C3135vs.C3136a(byteBuffer));
    }

    @DexIgnore
    /* renamed from: a */
    public int mo3938a(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(inputStream);
        com.fossil.blesdk.obfuscated.C3135vs.C3139d dVar = new com.fossil.blesdk.obfuscated.C3135vs.C3139d(inputStream);
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(gqVar);
        return mo17195a((com.fossil.blesdk.obfuscated.C3135vs.C3138c) dVar, gqVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.bumptech.glide.load.ImageHeaderParser.ImageType mo17197a(com.fossil.blesdk.obfuscated.C3135vs.C3138c cVar) throws java.io.IOException {
        int a = cVar.mo17200a();
        if (a == 65496) {
            return com.bumptech.glide.load.ImageHeaderParser.ImageType.JPEG;
        }
        int a2 = ((a << 16) & -65536) | (cVar.mo17200a() & 65535);
        if (a2 == -1991225785) {
            cVar.skip(21);
            return cVar.mo17203c() >= 3 ? com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG_A : com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG;
        } else if ((a2 >> 8) == 4671814) {
            return com.bumptech.glide.load.ImageHeaderParser.ImageType.GIF;
        } else {
            if (a2 != 1380533830) {
                return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
            }
            cVar.skip(4);
            if ((((cVar.mo17200a() << 16) & -65536) | (cVar.mo17200a() & 65535)) != 1464156752) {
                return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
            }
            int a3 = ((cVar.mo17200a() << 16) & -65536) | (cVar.mo17200a() & 65535);
            if ((a3 & -256) != 1448097792) {
                return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
            }
            int i = a3 & 255;
            if (i == 88) {
                cVar.skip(4);
                return (cVar.mo17203c() & 16) != 0 ? com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP_A : com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP;
            } else if (i != 76) {
                return com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP;
            } else {
                cVar.skip(4);
                return (cVar.mo17203c() & 8) != 0 ? com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP_A : com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo17195a(com.fossil.blesdk.obfuscated.C3135vs.C3138c cVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) throws java.io.IOException {
        int a = cVar.mo17200a();
        if (!m15426a(a)) {
            if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                android.util.Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a);
            }
            return -1;
        }
        int b = mo17199b(cVar);
        if (b == -1) {
            if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                android.util.Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
            }
            return -1;
        }
        byte[] bArr = (byte[]) gqVar.mo11285b(b, byte[].class);
        try {
            return mo17196a(cVar, bArr, b);
        } finally {
            gqVar.put(bArr);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo17196a(com.fossil.blesdk.obfuscated.C3135vs.C3138c cVar, byte[] bArr, int i) throws java.io.IOException {
        int a = cVar.mo17201a(bArr, i);
        if (a != i) {
            if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                android.util.Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + a);
            }
            return -1;
        } else if (mo17198a(bArr, i)) {
            return m15425a(new com.fossil.blesdk.obfuscated.C3135vs.C3137b(bArr, i));
        } else {
            if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                android.util.Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            }
            return -1;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo17198a(byte[] bArr, int i) {
        boolean z = bArr != null && i > f10399a.length;
        if (!z) {
            return z;
        }
        int i2 = 0;
        while (true) {
            byte[] bArr2 = f10399a;
            if (i2 >= bArr2.length) {
                return z;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m15425a(com.fossil.blesdk.obfuscated.C3135vs.C3137b bVar) {
        java.nio.ByteOrder byteOrder;
        short a = bVar.mo17206a(6);
        if (a == 18761) {
            byteOrder = java.nio.ByteOrder.LITTLE_ENDIAN;
        } else if (a != 19789) {
            if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                android.util.Log.d("DfltImageHeaderParser", "Unknown endianness = " + a);
            }
            byteOrder = java.nio.ByteOrder.BIG_ENDIAN;
        } else {
            byteOrder = java.nio.ByteOrder.BIG_ENDIAN;
        }
        bVar.mo17207a(byteOrder);
        int b = bVar.mo17209b(10) + 6;
        short a2 = bVar.mo17206a(b);
        for (int i = 0; i < a2; i++) {
            int a3 = m15424a(b, i);
            short a4 = bVar.mo17206a(a3);
            if (a4 == 274) {
                short a5 = bVar.mo17206a(a3 + 2);
                if (a5 >= 1 && a5 <= 12) {
                    int b2 = bVar.mo17209b(a3 + 4);
                    if (b2 >= 0) {
                        if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                            android.util.Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + a4 + " formatCode=" + a5 + " componentCount=" + b2);
                        }
                        int i2 = b2 + f10400b[a5];
                        if (i2 <= 4) {
                            int i3 = a3 + 8;
                            if (i3 < 0 || i3 > bVar.mo17205a()) {
                                if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    android.util.Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + a4);
                                }
                            } else if (i2 >= 0 && i2 + i3 <= bVar.mo17205a()) {
                                return bVar.mo17206a(i3);
                            } else {
                                if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    android.util.Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + a4);
                                }
                            }
                        } else if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                            android.util.Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + a5);
                        }
                    } else if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                        android.util.Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (android.util.Log.isLoggable("DfltImageHeaderParser", 3)) {
                    android.util.Log.d("DfltImageHeaderParser", "Got invalid format code = " + a5);
                }
            }
        }
        return -1;
    }
}
