package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class en4 extends em4 {
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ lo4 h;

    @DexIgnore
    public en4(String str, long j, lo4 lo4) {
        this.f = str;
        this.g = j;
        this.h = lo4;
    }

    @DexIgnore
    public long C() {
        return this.g;
    }

    @DexIgnore
    public am4 D() {
        String str = this.f;
        if (str != null) {
            return am4.b(str);
        }
        return null;
    }

    @DexIgnore
    public lo4 E() {
        return this.h;
    }
}
