package com.fossil.blesdk.obfuscated;

import android.location.Location;
import com.fossil.blesdk.obfuscated.f42;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ f42 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public d(DeviceLocation deviceLocation) {
            kd4.b(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements f42.b {
        @DexIgnore
        public /* final */ /* synthetic */ fr2 a;

        @DexIgnore
        public e(fr2 fr2) {
            this.a = fr2;
        }

        @DexIgnore
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.W.c().e(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            dn2.p.a().g().saveDeviceLocation(deviceLocation);
                            this.a.a(new d(deviceLocation));
                        } catch (Exception unused) {
                            this.a.a(new c());
                        }
                        this.a.d().b((f42.b) this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.a.d().b((f42.b) this);
            this.a.a(new c());
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public fr2(f42 f42) {
        kd4.b(f42, "mfLocationService");
        st1.a(f42, "mfLocationService cannot be null!", new Object[0]);
        kd4.a((Object) f42, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = f42;
    }

    @DexIgnore
    public String c() {
        return "LoadLocation";
    }

    @DexIgnore
    public final f42 d() {
        return this.d;
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.a((f42.b) new e(this));
        return new Object();
    }
}
