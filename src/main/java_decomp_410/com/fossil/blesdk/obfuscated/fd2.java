package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fd2 extends ed2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j Y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Z; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView W;
    @DexIgnore
    public long X;

    /*
    static {
        Z.put(R.id.iv_edit_profile, 1);
        Z.put(R.id.iv_user_avatar, 2);
        Z.put(R.id.iv_user_avatar_edit, 3);
        Z.put(R.id.tv_user_name, 4);
        Z.put(R.id.tv_height_weight, 5);
        Z.put(R.id.tv_member_since, 6);
        Z.put(R.id.horizontal_guide, 7);
        Z.put(R.id.tv_record_breaker, 8);
        Z.put(R.id.card_profile_info, 9);
        Z.put(R.id.vertical_guideline, 10);
        Z.put(R.id.horizontal_guideline, 11);
        Z.put(R.id.cl_avg_sleep, 12);
        Z.put(R.id.id_avg_sleep, 13);
        Z.put(R.id.tv_avg_sleep, 14);
        Z.put(R.id.tv_avg_sleep_desc, 15);
        Z.put(R.id.tv_avg_sleep_date, 16);
        Z.put(R.id.cl_active_time, 17);
        Z.put(R.id.id_active_time, 18);
        Z.put(R.id.tv_avg_active_time, 19);
        Z.put(R.id.tv_avg_active_time_desc, 20);
        Z.put(R.id.tv_avg_active_time_date, 21);
        Z.put(R.id.cl_avg_step, 22);
        Z.put(R.id.id_avg_step, 23);
        Z.put(R.id.tv_avg_activity, 24);
        Z.put(R.id.tv_avg_activity_desc, 25);
        Z.put(R.id.tv_avg_activity_date, 26);
        Z.put(R.id.cl_calories, 27);
        Z.put(R.id.id_calories, 28);
        Z.put(R.id.tv_avg_calories, 29);
        Z.put(R.id.tv_avg_calories_desc, 30);
        Z.put(R.id.tv_avg_calories_date, 31);
        Z.put(R.id.cl_low_battery, 32);
        Z.put(R.id.ftv_title_low_battery, 33);
        Z.put(R.id.ftv_description_low_battery, 34);
        Z.put(R.id.bt_replace_battery, 35);
        Z.put(R.id.tv_device, 36);
        Z.put(R.id.iv_add_device, 37);
        Z.put(R.id.rv_devices, 38);
        Z.put(R.id.cv_pair_first_watch, 39);
        Z.put(R.id.br_device, 40);
        Z.put(R.id.tv_setting, 41);
        Z.put(R.id.ll_settings, 42);
        Z.put(R.id.bt_set_goals, 43);
        Z.put(R.id.bt_change_password, 44);
        Z.put(R.id.v_change_password_separator_line, 45);
        Z.put(R.id.bt_units, 46);
        Z.put(R.id.bt_connectedapps, 47);
        Z.put(R.id.bt_opt_in, 48);
        Z.put(R.id.bt_help, 49);
        Z.put(R.id.bt_about, 50);
        Z.put(R.id.tv_logout, 51);
    }
    */

    @DexIgnore
    public fd2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 52, Y, Z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.X = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.X != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.X = 1;
        }
        g();
    }

    @DexIgnore
    public fd2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[40], objArr[50], objArr[44], objArr[47], objArr[49], objArr[48], objArr[35], objArr[43], objArr[46], objArr[9], objArr[17], objArr[12], objArr[22], objArr[27], objArr[32], objArr[39], objArr[34], objArr[33], objArr[7], objArr[11], objArr[18], objArr[13], objArr[23], objArr[28], objArr[37], objArr[1], objArr[2], objArr[3], objArr[42], objArr[38], objArr[19], objArr[21], objArr[20], objArr[24], objArr[26], objArr[25], objArr[29], objArr[31], objArr[30], objArr[14], objArr[16], objArr[15], objArr[36], objArr[5], objArr[51], objArr[6], objArr[8], objArr[41], objArr[4], objArr[45], objArr[10]);
        this.X = -1;
        this.W = objArr[0];
        this.W.setTag((Object) null);
        a(view);
        f();
    }
}
