package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vz */
public class C3155vz implements com.fossil.blesdk.obfuscated.C1984hz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.io.File f10432a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f10433b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.v54 f10434c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vz$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vz$a */
    public class C3156a implements com.fossil.blesdk.obfuscated.v54.C5258d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ byte[] f10435a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ int[] f10436b;

        @DexIgnore
        public C3156a(com.fossil.blesdk.obfuscated.C3155vz vzVar, byte[] bArr, int[] iArr) {
            this.f10435a = bArr;
            this.f10436b = iArr;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17242a(java.io.InputStream inputStream, int i) throws java.io.IOException {
            try {
                inputStream.read(this.f10435a, this.f10436b[0], i);
                int[] iArr = this.f10436b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vz$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vz$b */
    public class C3157b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ byte[] f10437a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f10438b;

        @DexIgnore
        public C3157b(com.fossil.blesdk.obfuscated.C3155vz vzVar, byte[] bArr, int i) {
            this.f10437a = bArr;
            this.f10438b = i;
        }
    }

    @DexIgnore
    public C3155vz(java.io.File file, int i) {
        this.f10432a = file;
        this.f10433b = i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11796a(long j, java.lang.String str) {
        mo17241f();
        mo17239b(j, str);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2610oy mo11797b() {
        com.fossil.blesdk.obfuscated.C3155vz.C3157b e = mo17240e();
        if (e == null) {
            return null;
        }
        return com.fossil.blesdk.obfuscated.C2610oy.m12010a(e.f10437a, 0, e.f10438b);
    }

    @DexIgnore
    /* renamed from: c */
    public byte[] mo11798c() {
        com.fossil.blesdk.obfuscated.C3155vz.C3157b e = mo17240e();
        if (e == null) {
            return null;
        }
        return e.f10437a;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11799d() {
        mo11795a();
        this.f10432a.delete();
    }

    @DexIgnore
    /* renamed from: e */
    public final com.fossil.blesdk.obfuscated.C3155vz.C3157b mo17240e() {
        if (!this.f10432a.exists()) {
            return null;
        }
        mo17241f();
        com.fossil.blesdk.obfuscated.v54 v54 = this.f10434c;
        if (v54 == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[v54.mo31744D()];
        try {
            this.f10434c.mo31747a((com.fossil.blesdk.obfuscated.v54.C5258d) new com.fossil.blesdk.obfuscated.C3155vz.C3156a(this, bArr, iArr));
        } catch (java.io.IOException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "A problem occurred while reading the Crashlytics log file.", e);
        }
        return new com.fossil.blesdk.obfuscated.C3155vz.C3157b(this, bArr, iArr[0]);
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo17241f() {
        if (this.f10434c == null) {
            try {
                this.f10434c = new com.fossil.blesdk.obfuscated.v54(this.f10432a);
            } catch (java.io.IOException e) {
                com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
                g.mo30063e("CrashlyticsCore", "Could not open log file: " + this.f10432a, e);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11795a() {
        p011io.fabric.sdk.android.services.common.CommonUtils.m36873a((java.io.Closeable) this.f10434c, "There was a problem closing the Crashlytics log file.");
        this.f10434c = null;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17239b(long j, java.lang.String str) {
        if (this.f10434c != null) {
            if (str == null) {
                str = "null";
            }
            try {
                if (str.length() > this.f10433b / 4) {
                    str = "..." + str.substring(str.length() - r1);
                }
                this.f10434c.mo31748a(java.lang.String.format(java.util.Locale.US, "%d %s%n", new java.lang.Object[]{java.lang.Long.valueOf(j), str.replaceAll("\r", " ").replaceAll("\n", " ")}).getBytes("UTF-8"));
                while (!this.f10434c.mo31759z() && this.f10434c.mo31744D() > this.f10433b) {
                    this.f10434c.mo31743C();
                }
            } catch (java.io.IOException e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "There was a problem writing to the Crashlytics log.", e);
            }
        }
    }
}
