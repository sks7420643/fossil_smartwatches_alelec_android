package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r51 extends vb1<r51> {
    @DexIgnore
    public static volatile r51[] h;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public v51[] d; // = v51.e();
    @DexIgnore
    public s51[] e; // = s51.e();
    @DexIgnore
    public Boolean f; // = null;
    @DexIgnore
    public Boolean g; // = null;

    @DexIgnore
    public r51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static r51[] e() {
        if (h == null) {
            synchronized (zb1.b) {
                if (h == null) {
                    h = new r51[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        v51[] v51Arr = this.d;
        int i = 0;
        if (v51Arr != null && v51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                v51[] v51Arr2 = this.d;
                if (i2 >= v51Arr2.length) {
                    break;
                }
                v51 v51 = v51Arr2[i2];
                if (v51 != null) {
                    ub1.a(2, (ac1) v51);
                }
                i2++;
            }
        }
        s51[] s51Arr = this.e;
        if (s51Arr != null && s51Arr.length > 0) {
            while (true) {
                s51[] s51Arr2 = this.e;
                if (i >= s51Arr2.length) {
                    break;
                }
                s51 s51 = s51Arr2[i];
                if (s51 != null) {
                    ub1.a(3, (ac1) s51);
                }
                i++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            ub1.a(4, bool.booleanValue());
        }
        Boolean bool2 = this.g;
        if (bool2 != null) {
            ub1.a(5, bool2.booleanValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof r51)) {
            return false;
        }
        r51 r51 = (r51) obj;
        Integer num = this.c;
        if (num == null) {
            if (r51.c != null) {
                return false;
            }
        } else if (!num.equals(r51.c)) {
            return false;
        }
        if (!zb1.a((Object[]) this.d, (Object[]) r51.d) || !zb1.a((Object[]) this.e, (Object[]) r51.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (r51.f != null) {
                return false;
            }
        } else if (!bool.equals(r51.f)) {
            return false;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            if (r51.g != null) {
                return false;
            }
        } else if (!bool2.equals(r51.g)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(r51.b);
        }
        xb1 xb12 = r51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (r51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (((((hashCode + (num == null ? 0 : num.hashCode())) * 31) + zb1.a((Object[]) this.d)) * 31) + zb1.a((Object[]) this.e)) * 31;
        Boolean bool = this.f;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.g;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        v51[] v51Arr = this.d;
        int i = 0;
        if (v51Arr != null && v51Arr.length > 0) {
            int i2 = a;
            int i3 = 0;
            while (true) {
                v51[] v51Arr2 = this.d;
                if (i3 >= v51Arr2.length) {
                    break;
                }
                v51 v51 = v51Arr2[i3];
                if (v51 != null) {
                    i2 += ub1.b(2, (ac1) v51);
                }
                i3++;
            }
            a = i2;
        }
        s51[] s51Arr = this.e;
        if (s51Arr != null && s51Arr.length > 0) {
            while (true) {
                s51[] s51Arr2 = this.e;
                if (i >= s51Arr2.length) {
                    break;
                }
                s51 s51 = s51Arr2[i];
                if (s51 != null) {
                    a += ub1.b(3, (ac1) s51);
                }
                i++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(4) + 1;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            return a;
        }
        bool2.booleanValue();
        return a + ub1.c(5) + 1;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 18) {
                int a = dc1.a(tb1, 18);
                v51[] v51Arr = this.d;
                int length = v51Arr == null ? 0 : v51Arr.length;
                v51[] v51Arr2 = new v51[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.d, 0, v51Arr2, 0, length);
                }
                while (length < v51Arr2.length - 1) {
                    v51Arr2[length] = new v51();
                    tb1.a((ac1) v51Arr2[length]);
                    tb1.c();
                    length++;
                }
                v51Arr2[length] = new v51();
                tb1.a((ac1) v51Arr2[length]);
                this.d = v51Arr2;
            } else if (c2 == 26) {
                int a2 = dc1.a(tb1, 26);
                s51[] s51Arr = this.e;
                int length2 = s51Arr == null ? 0 : s51Arr.length;
                s51[] s51Arr2 = new s51[(a2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.e, 0, s51Arr2, 0, length2);
                }
                while (length2 < s51Arr2.length - 1) {
                    s51Arr2[length2] = new s51();
                    tb1.a((ac1) s51Arr2[length2]);
                    tb1.c();
                    length2++;
                }
                s51Arr2[length2] = new s51();
                tb1.a((ac1) s51Arr2[length2]);
                this.e = s51Arr2;
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(tb1.d());
            } else if (c2 == 40) {
                this.g = Boolean.valueOf(tb1.d());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
