package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c7 */
public final class C1538c7 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f4011a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f4012b;

    @DexIgnore
    /* renamed from: c */
    public static java.lang.reflect.Method f4013c;

    @DexIgnore
    /* renamed from: d */
    public static boolean f4014d;

    @DexIgnore
    /* renamed from: a */
    public static void m5303a(android.graphics.drawable.Drawable drawable, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            drawable.setAutoMirrored(z);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m5307b(android.graphics.drawable.Drawable drawable, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.setTint(i);
        } else if (drawable instanceof com.fossil.blesdk.obfuscated.C1603d7) {
            ((com.fossil.blesdk.obfuscated.C1603d7) drawable).setTint(i);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static int m5308c(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return drawable.getAlpha();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: d */
    public static android.graphics.ColorFilter m5309d(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return drawable.getColorFilter();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: e */
    public static int m5310e(android.graphics.drawable.Drawable drawable) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 23) {
            return drawable.getLayoutDirection();
        }
        if (i >= 17) {
            if (!f4014d) {
                try {
                    f4013c = android.graphics.drawable.Drawable.class.getDeclaredMethod("getLayoutDirection", new java.lang.Class[0]);
                    f4013c.setAccessible(true);
                } catch (java.lang.NoSuchMethodException e) {
                    android.util.Log.i("DrawableCompat", "Failed to retrieve getLayoutDirection() method", e);
                }
                f4014d = true;
            }
            java.lang.reflect.Method method = f4013c;
            if (method != null) {
                try {
                    return ((java.lang.Integer) method.invoke(drawable, new java.lang.Object[0])).intValue();
                } catch (java.lang.Exception e2) {
                    android.util.Log.i("DrawableCompat", "Failed to invoke getLayoutDirection() via reflection", e2);
                    f4013c = null;
                }
            }
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: f */
    public static boolean m5311f(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return drawable.isAutoMirrored();
        }
        return false;
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: g */
    public static void m5312g(android.graphics.drawable.Drawable drawable) {
        drawable.jumpToCurrentState();
    }

    @DexIgnore
    /* renamed from: h */
    public static <T extends android.graphics.drawable.Drawable> T m5313h(android.graphics.drawable.Drawable drawable) {
        return drawable instanceof com.fossil.blesdk.obfuscated.C1705e7 ? ((com.fossil.blesdk.obfuscated.C1705e7) drawable).mo10343a() : drawable;
    }

    @DexIgnore
    /* renamed from: i */
    public static android.graphics.drawable.Drawable m5314i(android.graphics.drawable.Drawable drawable) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 23) {
            return drawable;
        }
        return i >= 21 ? !(drawable instanceof com.fossil.blesdk.obfuscated.C1603d7) ? new com.fossil.blesdk.obfuscated.C1860g7(drawable) : drawable : !(drawable instanceof com.fossil.blesdk.obfuscated.C1603d7) ? new com.fossil.blesdk.obfuscated.C1772f7(drawable) : drawable;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5297a(android.graphics.drawable.Drawable drawable, float f, float f2) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.setHotspot(f, f2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5298a(android.graphics.drawable.Drawable drawable, int i, int i2, int i3, int i4) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.setHotspotBounds(i, i2, i3, i4);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m5306b(android.graphics.drawable.Drawable drawable) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 23) {
            drawable.clearColorFilter();
        } else if (i >= 21) {
            drawable.clearColorFilter();
            if (drawable instanceof android.graphics.drawable.InsetDrawable) {
                m5306b(((android.graphics.drawable.InsetDrawable) drawable).getDrawable());
            } else if (drawable instanceof com.fossil.blesdk.obfuscated.C1705e7) {
                m5306b(((com.fossil.blesdk.obfuscated.C1705e7) drawable).mo10343a());
            } else if (drawable instanceof android.graphics.drawable.DrawableContainer) {
                android.graphics.drawable.DrawableContainer.DrawableContainerState drawableContainerState = (android.graphics.drawable.DrawableContainer.DrawableContainerState) ((android.graphics.drawable.DrawableContainer) drawable).getConstantState();
                if (drawableContainerState != null) {
                    int childCount = drawableContainerState.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        android.graphics.drawable.Drawable child = drawableContainerState.getChild(i2);
                        if (child != null) {
                            m5306b(child);
                        }
                    }
                }
            }
        } else {
            drawable.clearColorFilter();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5299a(android.graphics.drawable.Drawable drawable, android.content.res.ColorStateList colorStateList) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.setTintList(colorStateList);
        } else if (drawable instanceof com.fossil.blesdk.obfuscated.C1603d7) {
            ((com.fossil.blesdk.obfuscated.C1603d7) drawable).setTintList(colorStateList);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5302a(android.graphics.drawable.Drawable drawable, android.graphics.PorterDuff.Mode mode) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.setTintMode(mode);
        } else if (drawable instanceof com.fossil.blesdk.obfuscated.C1603d7) {
            ((com.fossil.blesdk.obfuscated.C1603d7) drawable).setTintMode(mode);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5300a(android.graphics.drawable.Drawable drawable, android.content.res.Resources.Theme theme) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.applyTheme(theme);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5304a(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return drawable.canApplyTheme();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5301a(android.graphics.drawable.Drawable drawable, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawable.inflate(resources, xmlPullParser, attributeSet, theme);
        } else {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5305a(android.graphics.drawable.Drawable drawable, int i) {
        int i2 = android.os.Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            return drawable.setLayoutDirection(i);
        }
        if (i2 >= 17) {
            if (!f4012b) {
                try {
                    f4011a = android.graphics.drawable.Drawable.class.getDeclaredMethod("setLayoutDirection", new java.lang.Class[]{java.lang.Integer.TYPE});
                    f4011a.setAccessible(true);
                } catch (java.lang.NoSuchMethodException e) {
                    android.util.Log.i("DrawableCompat", "Failed to retrieve setLayoutDirection(int) method", e);
                }
                f4012b = true;
            }
            java.lang.reflect.Method method = f4011a;
            if (method != null) {
                try {
                    method.invoke(drawable, new java.lang.Object[]{java.lang.Integer.valueOf(i)});
                    return true;
                } catch (java.lang.Exception e2) {
                    android.util.Log.i("DrawableCompat", "Failed to invoke setLayoutDirection(int) via reflection", e2);
                    f4011a = null;
                }
            }
        }
        return false;
    }
}
