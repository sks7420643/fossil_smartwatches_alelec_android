package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lg */
public class C2324lg implements com.fossil.blesdk.obfuscated.C1874gg {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String[] f7217f; // = new java.lang.String[0];

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.database.sqlite.SQLiteDatabase f7218e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.lg$a */
    public class C2325a implements android.database.sqlite.SQLiteDatabase.CursorFactory {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2125jg f7219a;

        @DexIgnore
        public C2325a(com.fossil.blesdk.obfuscated.C2324lg lgVar, com.fossil.blesdk.obfuscated.C2125jg jgVar) {
            this.f7219a = jgVar;
        }

        @DexIgnore
        public android.database.Cursor newCursor(android.database.sqlite.SQLiteDatabase sQLiteDatabase, android.database.sqlite.SQLiteCursorDriver sQLiteCursorDriver, java.lang.String str, android.database.sqlite.SQLiteQuery sQLiteQuery) {
            this.f7219a.mo10919a(new com.fossil.blesdk.obfuscated.C2567og(sQLiteQuery));
            return new android.database.sqlite.SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    /*
    static {
        new java.lang.String[]{"", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE "};
    }
    */

    @DexIgnore
    public C2324lg(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
        this.f7218e = sQLiteDatabase;
    }

    @DexIgnore
    /* renamed from: a */
    public android.database.Cursor mo11229a(com.fossil.blesdk.obfuscated.C2125jg jgVar) {
        return this.f7218e.rawQueryWithFactory(new com.fossil.blesdk.obfuscated.C2324lg.C2325a(this, jgVar), jgVar.mo10920b(), f7217f, (java.lang.String) null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11230b(java.lang.String str) throws android.database.SQLException {
        this.f7218e.execSQL(str);
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2221kg mo11231c(java.lang.String str) {
        return new com.fossil.blesdk.obfuscated.C2653pg(this.f7218e.compileStatement(str));
    }

    @DexIgnore
    public void close() throws java.io.IOException {
        this.f7218e.close();
    }

    @DexIgnore
    /* renamed from: d */
    public android.database.Cursor mo11232d(java.lang.String str) {
        return mo11229a((com.fossil.blesdk.obfuscated.C2125jg) new com.fossil.blesdk.obfuscated.C1801fg(str));
    }

    @DexIgnore
    public boolean isOpen() {
        return this.f7218e.isOpen();
    }

    @DexIgnore
    /* renamed from: s */
    public void mo11234s() {
        this.f7218e.beginTransaction();
    }

    @DexIgnore
    /* renamed from: t */
    public java.util.List<android.util.Pair<java.lang.String, java.lang.String>> mo11235t() {
        return this.f7218e.getAttachedDbs();
    }

    @DexIgnore
    /* renamed from: u */
    public void mo11236u() {
        this.f7218e.setTransactionSuccessful();
    }

    @DexIgnore
    /* renamed from: v */
    public void mo11237v() {
        this.f7218e.endTransaction();
    }

    @DexIgnore
    /* renamed from: w */
    public java.lang.String mo11238w() {
        return this.f7218e.getPath();
    }

    @DexIgnore
    /* renamed from: x */
    public boolean mo11239x() {
        return this.f7218e.inTransaction();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13288a(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
        return this.f7218e == sQLiteDatabase;
    }
}
