package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.pq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vq4 extends g0 {
    @DexIgnore
    public pq4.a e;
    @DexIgnore
    public pq4.b f;

    @DexIgnore
    public static vq4 a(String str, String str2, String str3, int i, int i2, String[] strArr) {
        vq4 vq4 = new vq4();
        vq4.setArguments(new tq4(str2, str3, str, i, i2, strArr).a());
        return vq4;
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() != null) {
            if (getParentFragment() instanceof pq4.a) {
                this.e = (pq4.a) getParentFragment();
            }
            if (getParentFragment() instanceof pq4.b) {
                this.f = (pq4.b) getParentFragment();
            }
        }
        if (context instanceof pq4.a) {
            this.e = (pq4.a) context;
        }
        if (context instanceof pq4.b) {
            this.f = (pq4.b) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        setCancelable(false);
        tq4 tq4 = new tq4(getArguments());
        return tq4.b(getContext(), new sq4(this, tq4, this.e, this.f));
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.e = null;
        this.f = null;
    }

    @DexIgnore
    public void a(FragmentManager fragmentManager, String str) {
        if (!fragmentManager.e()) {
            show(fragmentManager, str);
        }
    }
}
