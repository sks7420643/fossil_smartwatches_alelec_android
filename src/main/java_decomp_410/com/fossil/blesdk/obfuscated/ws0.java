package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ws0 extends ContentObserver {
    @DexIgnore
    public /* final */ /* synthetic */ vs0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ws0(vs0 vs0, Handler handler) {
        super((Handler) null);
        this.a = vs0;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.a.b();
        this.a.d();
    }
}
