package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class db4 extends cb4 {
    @DexIgnore
    public static final <T> int a(Iterable<? extends T> iterable, int i) {
        kd4.b(iterable, "$this$collectionSizeOrDefault");
        return iterable instanceof Collection ? ((Collection) iterable).size() : i;
    }

    @DexIgnore
    public static final <T> boolean b(Collection<? extends T> collection) {
        return collection.size() > 2 && (collection instanceof ArrayList);
    }

    @DexIgnore
    public static final <T> Collection<T> a(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        kd4.b(iterable, "$this$convertToSetForSetOperationWith");
        kd4.b(iterable2, "source");
        if (iterable instanceof Set) {
            return (Collection) iterable;
        }
        if (!(iterable instanceof Collection)) {
            return kb4.j(iterable);
        }
        if ((iterable2 instanceof Collection) && ((Collection) iterable2).size() < 2) {
            return (Collection) iterable;
        }
        Collection<T> collection = (Collection) iterable;
        return b(collection) ? kb4.j(iterable) : collection;
    }

    @DexIgnore
    public static final <T> List<T> a(Iterable<? extends Iterable<? extends T>> iterable) {
        kd4.b(iterable, "$this$flatten");
        ArrayList arrayList = new ArrayList();
        for (Iterable a : iterable) {
            hb4.a(arrayList, a);
        }
        return arrayList;
    }
}
