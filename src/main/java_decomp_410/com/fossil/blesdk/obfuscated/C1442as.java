package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.as */
public class C1442as<Model> implements com.fossil.blesdk.obfuscated.C2912sr<Model, Model> {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C1442as<?> f3574a; // = new com.fossil.blesdk.obfuscated.C1442as<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.as$a")
    /* renamed from: com.fossil.blesdk.obfuscated.as$a */
    public static class C1443a<Model> implements com.fossil.blesdk.obfuscated.C2984tr<Model, Model> {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ com.fossil.blesdk.obfuscated.C1442as.C1443a<?> f3575a; // = new com.fossil.blesdk.obfuscated.C1442as.C1443a<>();

        @DexIgnore
        /* renamed from: a */
        public static <T> com.fossil.blesdk.obfuscated.C1442as.C1443a<T> m4619a() {
            return f3575a;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<Model, Model> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return com.fossil.blesdk.obfuscated.C1442as.m4616a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.as$b")
    /* renamed from: com.fossil.blesdk.obfuscated.as$b */
    public static class C1444b<Model> implements com.fossil.blesdk.obfuscated.C2902so<Model> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ Model f3576e;

        @DexIgnore
        public C1444b(Model model) {
            this.f3576e = model;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Model> aVar) {
            aVar.mo9252a(this.f3576e);
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<Model> getDataClass() {
            return this.f3576e.getClass();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C1442as<T> m4616a() {
        return f3574a;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(Model model) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Model> mo8911a(Model model, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(model), new com.fossil.blesdk.obfuscated.C1442as.C1444b(model));
    }
}
