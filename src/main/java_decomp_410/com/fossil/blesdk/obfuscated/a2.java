package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a2 extends DataSetObservable {
    @DexIgnore
    public static /* final */ String n; // = a2.class.getSimpleName();
    @DexIgnore
    public static /* final */ Object o; // = new Object();
    @DexIgnore
    public static /* final */ Map<String, a2> p; // = new HashMap();
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ List<b> b; // = new ArrayList();
    @DexIgnore
    public /* final */ List<e> c; // = new ArrayList();
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public Intent f;
    @DexIgnore
    public c g; // = new d();
    @DexIgnore
    public int h; // = 50;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public f m;

    @DexIgnore
    public interface a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Comparable<b> {
        @DexIgnore
        public /* final */ ResolveInfo e;
        @DexIgnore
        public float f;

        @DexIgnore
        public b(ResolveInfo resolveInfo) {
            this.e = resolveInfo;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(b bVar) {
            return Float.floatToIntBits(bVar.f) - Float.floatToIntBits(this.f);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && b.class == obj.getClass() && Float.floatToIntBits(this.f) == Float.floatToIntBits(((b) obj).f);
        }

        @DexIgnore
        public int hashCode() {
            return Float.floatToIntBits(this.f) + 31;
        }

        @DexIgnore
        public String toString() {
            return "[" + "resolveInfo:" + this.e.toString() + "; weight:" + new BigDecimal((double) this.f) + "]";
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Intent intent, List<b> list, List<e> list2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c {
        @DexIgnore
        public /* final */ Map<ComponentName, b> a; // = new HashMap();

        @DexIgnore
        public void a(Intent intent, List<b> list, List<e> list2) {
            Map<ComponentName, b> map = this.a;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                b bVar = list.get(i);
                bVar.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                map.put(new ComponentName(bVar.e.activityInfo.packageName, bVar.e.activityInfo.name), bVar);
            }
            float f = 1.0f;
            for (int size2 = list2.size() - 1; size2 >= 0; size2--) {
                e eVar = list2.get(size2);
                b bVar2 = map.get(eVar.a);
                if (bVar2 != null) {
                    bVar2.f += eVar.c * f;
                    f *= 0.95f;
                }
            }
            Collections.sort(list);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ float c;

        @DexIgnore
        public e(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || e.class != obj.getClass()) {
                return false;
            }
            e eVar = (e) obj;
            ComponentName componentName = this.a;
            if (componentName == null) {
                if (eVar.a != null) {
                    return false;
                }
            } else if (!componentName.equals(eVar.a)) {
                return false;
            }
            return this.b == eVar.b && Float.floatToIntBits(this.c) == Float.floatToIntBits(eVar.c);
        }

        @DexIgnore
        public int hashCode() {
            ComponentName componentName = this.a;
            int hashCode = componentName == null ? 0 : componentName.hashCode();
            long j = this.b;
            return ((((hashCode + 31) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        @DexIgnore
        public String toString() {
            return "[" + "; activity:" + this.a + "; time:" + this.b + "; weight:" + new BigDecimal((double) this.c) + "]";
        }

        @DexIgnore
        public e(ComponentName componentName, long j, float f) {
            this.a = componentName;
            this.b = j;
            this.c = f;
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        boolean a(a2 a2Var, Intent intent);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends AsyncTask<Object, Void, Void> {
        @DexIgnore
        public g() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x006d, code lost:
            if (r15 != null) goto L_0x006f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
            r15.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0092, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d2, code lost:
            if (r15 == null) goto L_0x00d5;
         */
        @DexIgnore
        public Void doInBackground(Object... objArr) {
            List list = objArr[0];
            String str = objArr[1];
            try {
                FileOutputStream openFileOutput = a2.this.d.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, (String) null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag((String) null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        e eVar = (e) list.remove(0);
                        newSerializer.startTag((String) null, "historical-record");
                        newSerializer.attribute((String) null, Constants.ACTIVITY, eVar.a.flattenToString());
                        newSerializer.attribute((String) null, LogBuilder.KEY_TIME, String.valueOf(eVar.b));
                        newSerializer.attribute((String) null, Constants.PROFILE_KEY_UNITS_WEIGHT, String.valueOf(eVar.c));
                        newSerializer.endTag((String) null, "historical-record");
                    }
                    newSerializer.endTag((String) null, "historical-records");
                    newSerializer.endDocument();
                    a2.this.i = true;
                } catch (IllegalArgumentException e) {
                    String str2 = a2.n;
                    Log.e(str2, "Error writing historical record file: " + a2.this.e, e);
                    a2.this.i = true;
                } catch (IllegalStateException e2) {
                    String str3 = a2.n;
                    Log.e(str3, "Error writing historical record file: " + a2.this.e, e2);
                    a2.this.i = true;
                } catch (IOException e3) {
                    String str4 = a2.n;
                    Log.e(str4, "Error writing historical record file: " + a2.this.e, e3);
                    a2.this.i = true;
                } catch (Throwable th) {
                    a2.this.i = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException unused) {
                        }
                    }
                    throw th;
                }
                return null;
            } catch (FileNotFoundException e4) {
                String str5 = a2.n;
                Log.e(str5, "Error writing historical record file: " + str, e4);
                return null;
            }
        }
    }

    @DexIgnore
    public a2(Context context, String str) {
        this.d = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.e = str;
            return;
        }
        this.e = str + ".xml";
    }

    @DexIgnore
    public static a2 a(Context context, String str) {
        a2 a2Var;
        synchronized (o) {
            a2Var = p.get(str);
            if (a2Var == null) {
                a2Var = new a2(context, str);
                p.put(str, a2Var);
            }
        }
        return a2Var;
    }

    @DexIgnore
    public int b() {
        int size;
        synchronized (this.a) {
            a();
            size = this.b.size();
        }
        return size;
    }

    @DexIgnore
    public ResolveInfo c() {
        synchronized (this.a) {
            a();
            if (this.b.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.b.get(0).e;
            return resolveInfo;
        }
    }

    @DexIgnore
    public int d() {
        int size;
        synchronized (this.a) {
            a();
            size = this.c.size();
        }
        return size;
    }

    @DexIgnore
    public final boolean e() {
        if (!this.l || this.f == null) {
            return false;
        }
        this.l = false;
        this.b.clear();
        List<ResolveInfo> queryIntentActivities = this.d.getPackageManager().queryIntentActivities(this.f, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.b.add(new b(queryIntentActivities.get(i2)));
        }
        return true;
    }

    @DexIgnore
    public final void f() {
        if (!this.j) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.k) {
            this.k = false;
            if (!TextUtils.isEmpty(this.e)) {
                new g().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[]{new ArrayList(this.c), this.e});
            }
        }
    }

    @DexIgnore
    public final void g() {
        int size = this.c.size() - this.h;
        if (size > 0) {
            this.k = true;
            for (int i2 = 0; i2 < size; i2++) {
                e remove = this.c.remove(0);
            }
        }
    }

    @DexIgnore
    public final boolean h() {
        if (!this.i || !this.k || TextUtils.isEmpty(this.e)) {
            return false;
        }
        this.i = false;
        this.j = true;
        i();
        return true;
    }

    @DexIgnore
    public final void i() {
        try {
            FileInputStream openFileInput = this.d.openFileInput(this.e);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if ("historical-records".equals(newPullParser.getName())) {
                    List<e> list = this.c;
                    list.clear();
                    while (true) {
                        int next = newPullParser.next();
                        if (next == 1) {
                            if (openFileInput == null) {
                                return;
                            }
                        } else if (!(next == 3 || next == 4)) {
                            if ("historical-record".equals(newPullParser.getName())) {
                                list.add(new e(newPullParser.getAttributeValue((String) null, Constants.ACTIVITY), Long.parseLong(newPullParser.getAttributeValue((String) null, LogBuilder.KEY_TIME)), Float.parseFloat(newPullParser.getAttributeValue((String) null, Constants.PROFILE_KEY_UNITS_WEIGHT))));
                            } else {
                                throw new XmlPullParserException("Share records file not well-formed.");
                            }
                        }
                    }
                    try {
                        openFileInput.close();
                    } catch (IOException unused) {
                    }
                } else {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
            } catch (XmlPullParserException e2) {
                String str = n;
                Log.e(str, "Error reading historical recrod file: " + this.e, e2);
                if (openFileInput == null) {
                }
            } catch (IOException e3) {
                String str2 = n;
                Log.e(str2, "Error reading historical recrod file: " + this.e, e3);
                if (openFileInput == null) {
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException unused3) {
        }
    }

    @DexIgnore
    public final boolean j() {
        if (this.g == null || this.f == null || this.b.isEmpty() || this.c.isEmpty()) {
            return false;
        }
        this.g.a(this.f, this.b, Collections.unmodifiableList(this.c));
        return true;
    }

    @DexIgnore
    public ResolveInfo b(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.a) {
            a();
            resolveInfo = this.b.get(i2).e;
        }
        return resolveInfo;
    }

    @DexIgnore
    public void c(int i2) {
        synchronized (this.a) {
            a();
            b bVar = this.b.get(i2);
            b bVar2 = this.b.get(0);
            a(new e(new ComponentName(bVar.e.activityInfo.packageName, bVar.e.activityInfo.name), System.currentTimeMillis(), bVar2 != null ? (bVar2.f - bVar.f) + 5.0f : 1.0f));
        }
    }

    @DexIgnore
    public void a(Intent intent) {
        synchronized (this.a) {
            if (this.f != intent) {
                this.f = intent;
                this.l = true;
                a();
            }
        }
    }

    @DexIgnore
    public int a(ResolveInfo resolveInfo) {
        synchronized (this.a) {
            a();
            List<b> list = this.b;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).e == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public Intent a(int i2) {
        synchronized (this.a) {
            if (this.f == null) {
                return null;
            }
            a();
            b bVar = this.b.get(i2);
            ComponentName componentName = new ComponentName(bVar.e.activityInfo.packageName, bVar.e.activityInfo.name);
            Intent intent = new Intent(this.f);
            intent.setComponent(componentName);
            if (this.m != null) {
                if (this.m.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new e(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    @DexIgnore
    public void a(f fVar) {
        synchronized (this.a) {
            this.m = fVar;
        }
    }

    @DexIgnore
    public final void a() {
        boolean e2 = e() | h();
        g();
        if (e2) {
            j();
            notifyChanged();
        }
    }

    @DexIgnore
    public final boolean a(e eVar) {
        boolean add = this.c.add(eVar);
        if (add) {
            this.k = true;
            g();
            f();
            j();
            notifyChanged();
        }
        return add;
    }
}
