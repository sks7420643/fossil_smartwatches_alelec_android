package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s9 */
public class C2866s9 {
    @DexIgnore
    /* renamed from: a */
    public static void m13573a(android.view.accessibility.AccessibilityRecord accessibilityRecord, android.view.View view, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            accessibilityRecord.setSource(view, i);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m13574b(android.view.accessibility.AccessibilityRecord accessibilityRecord, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 15) {
            accessibilityRecord.setMaxScrollY(i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13572a(android.view.accessibility.AccessibilityRecord accessibilityRecord, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 15) {
            accessibilityRecord.setMaxScrollX(i);
        }
    }
}
