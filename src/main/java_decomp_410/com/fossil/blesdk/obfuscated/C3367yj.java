package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yj */
public class C3367yj {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f11279e; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("ConstraintsCmdHandler");

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f11280a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f11281b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1421ak f11282c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2035ik f11283d; // = new com.fossil.blesdk.obfuscated.C2035ik(this.f11280a, this.f11282c.mo8768d(), (com.fossil.blesdk.obfuscated.C1953hk) null);

    @DexIgnore
    public C3367yj(android.content.Context context, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        this.f11280a = context;
        this.f11281b = i;
        this.f11282c = akVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18124a() {
        java.util.List<com.fossil.blesdk.obfuscated.C1954hl> a = this.f11282c.mo8769e().mo16462g().mo3780d().mo12017a();
        androidx.work.impl.background.systemalarm.ConstraintProxy.m2502a(this.f11280a, a);
        this.f11283d.mo12013c(a);
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C1954hl> arrayList = new java.util.ArrayList<>(a.size());
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        for (com.fossil.blesdk.obfuscated.C1954hl next : a) {
            java.lang.String str = next.f5771a;
            if (currentTimeMillis >= next.mo11670a() && (!next.mo11671b() || this.f11283d.mo12011a(str))) {
                arrayList.add(next);
            }
        }
        for (com.fossil.blesdk.obfuscated.C1954hl hlVar : arrayList) {
            java.lang.String str2 = hlVar.f5771a;
            android.content.Intent a2 = com.fossil.blesdk.obfuscated.C3293xj.m16360a(this.f11280a, str2);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f11279e, java.lang.String.format("Creating a delay_met command for workSpec with id (%s)", new java.lang.Object[]{str2}), new java.lang.Throwable[0]);
            com.fossil.blesdk.obfuscated.C1421ak akVar = this.f11282c;
            akVar.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(akVar, a2, this.f11281b));
        }
        this.f11283d.mo12009a();
    }
}
