package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jx implements t64 {
    @DexIgnore
    public /* final */ zx a;
    @DexIgnore
    public /* final */ xx b;

    @DexIgnore
    public jx(zx zxVar, xx xxVar) {
        this.a = zxVar;
        this.b = xxVar;
    }

    @DexIgnore
    public static jx a(zx zxVar) {
        return new jx(zxVar, new xx(new n64(new wx(new l64(1000, 8), 0.1d), new k64(5))));
    }

    @DexIgnore
    public boolean a(List<File> list) {
        long nanoTime = System.nanoTime();
        if (this.b.a(nanoTime)) {
            if (this.a.a(list)) {
                this.b.a();
                return true;
            }
            this.b.b(nanoTime);
        }
        return false;
    }
}
