package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o0 */
public class C2514o0 extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable.Callback {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2514o0.C2517c f7901e;

    @DexIgnore
    /* renamed from: f */
    public android.graphics.Rect f7902f;

    @DexIgnore
    /* renamed from: g */
    public android.graphics.drawable.Drawable f7903g;

    @DexIgnore
    /* renamed from: h */
    public android.graphics.drawable.Drawable f7904h;

    @DexIgnore
    /* renamed from: i */
    public int f7905i; // = 255;

    @DexIgnore
    /* renamed from: j */
    public boolean f7906j;

    @DexIgnore
    /* renamed from: k */
    public int f7907k; // = -1;

    @DexIgnore
    /* renamed from: l */
    public boolean f7908l;

    @DexIgnore
    /* renamed from: m */
    public java.lang.Runnable f7909m;

    @DexIgnore
    /* renamed from: n */
    public long f7910n;

    @DexIgnore
    /* renamed from: o */
    public long f7911o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C2514o0.C2516b f7912p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.o0$a */
    public class C2515a implements java.lang.Runnable {
        @DexIgnore
        public C2515a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2514o0.this.mo14164a(true);
            com.fossil.blesdk.obfuscated.C2514o0.this.invalidateSelf();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.o0$b */
    public static class C2516b implements android.graphics.drawable.Drawable.Callback {

        @DexIgnore
        /* renamed from: e */
        public android.graphics.drawable.Drawable.Callback f7914e;

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2514o0.C2516b mo14200a(android.graphics.drawable.Drawable.Callback callback) {
            this.f7914e = callback;
            return this;
        }

        @DexIgnore
        public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
        }

        @DexIgnore
        public void scheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable, long j) {
            android.graphics.drawable.Drawable.Callback callback = this.f7914e;
            if (callback != null) {
                callback.scheduleDrawable(drawable, runnable, j);
            }
        }

        @DexIgnore
        public void unscheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable) {
            android.graphics.drawable.Drawable.Callback callback = this.f7914e;
            if (callback != null) {
                callback.unscheduleDrawable(drawable, runnable);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.drawable.Drawable.Callback mo14199a() {
            android.graphics.drawable.Drawable.Callback callback = this.f7914e;
            this.f7914e = null;
            return callback;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o0$c")
    /* renamed from: com.fossil.blesdk.obfuscated.o0$c */
    public static abstract class C2517c extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: A */
        public int f7915A;

        @DexIgnore
        /* renamed from: B */
        public int f7916B;

        @DexIgnore
        /* renamed from: C */
        public boolean f7917C;

        @DexIgnore
        /* renamed from: D */
        public android.graphics.ColorFilter f7918D;

        @DexIgnore
        /* renamed from: E */
        public boolean f7919E;

        @DexIgnore
        /* renamed from: F */
        public android.content.res.ColorStateList f7920F;

        @DexIgnore
        /* renamed from: G */
        public android.graphics.PorterDuff.Mode f7921G;

        @DexIgnore
        /* renamed from: H */
        public boolean f7922H;

        @DexIgnore
        /* renamed from: I */
        public boolean f7923I;

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2514o0 f7924a;

        @DexIgnore
        /* renamed from: b */
        public android.content.res.Resources f7925b;

        @DexIgnore
        /* renamed from: c */
        public int f7926c; // = 160;

        @DexIgnore
        /* renamed from: d */
        public int f7927d;

        @DexIgnore
        /* renamed from: e */
        public int f7928e;

        @DexIgnore
        /* renamed from: f */
        public android.util.SparseArray<android.graphics.drawable.Drawable.ConstantState> f7929f;

        @DexIgnore
        /* renamed from: g */
        public android.graphics.drawable.Drawable[] f7930g;

        @DexIgnore
        /* renamed from: h */
        public int f7931h;

        @DexIgnore
        /* renamed from: i */
        public boolean f7932i;

        @DexIgnore
        /* renamed from: j */
        public boolean f7933j;

        @DexIgnore
        /* renamed from: k */
        public android.graphics.Rect f7934k;

        @DexIgnore
        /* renamed from: l */
        public boolean f7935l;

        @DexIgnore
        /* renamed from: m */
        public boolean f7936m;

        @DexIgnore
        /* renamed from: n */
        public int f7937n;

        @DexIgnore
        /* renamed from: o */
        public int f7938o;

        @DexIgnore
        /* renamed from: p */
        public int f7939p;

        @DexIgnore
        /* renamed from: q */
        public int f7940q;

        @DexIgnore
        /* renamed from: r */
        public boolean f7941r;

        @DexIgnore
        /* renamed from: s */
        public int f7942s;

        @DexIgnore
        /* renamed from: t */
        public boolean f7943t;

        @DexIgnore
        /* renamed from: u */
        public boolean f7944u;

        @DexIgnore
        /* renamed from: v */
        public boolean f7945v;

        @DexIgnore
        /* renamed from: w */
        public boolean f7946w;

        @DexIgnore
        /* renamed from: x */
        public boolean f7947x;

        @DexIgnore
        /* renamed from: y */
        public boolean f7948y;

        @DexIgnore
        /* renamed from: z */
        public int f7949z;

        @DexIgnore
        public C2517c(com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar, com.fossil.blesdk.obfuscated.C2514o0 o0Var, android.content.res.Resources resources) {
            android.content.res.Resources resources2;
            this.f7932i = false;
            this.f7935l = false;
            this.f7947x = true;
            this.f7915A = 0;
            this.f7916B = 0;
            this.f7924a = o0Var;
            if (resources != null) {
                resources2 = resources;
            } else {
                resources2 = cVar != null ? cVar.f7925b : null;
            }
            this.f7925b = resources2;
            this.f7926c = com.fossil.blesdk.obfuscated.C2514o0.m11513a(resources, cVar != null ? cVar.f7926c : 0);
            if (cVar != null) {
                this.f7927d = cVar.f7927d;
                this.f7928e = cVar.f7928e;
                this.f7945v = true;
                this.f7946w = true;
                this.f7932i = cVar.f7932i;
                this.f7935l = cVar.f7935l;
                this.f7947x = cVar.f7947x;
                this.f7948y = cVar.f7948y;
                this.f7949z = cVar.f7949z;
                this.f7915A = cVar.f7915A;
                this.f7916B = cVar.f7916B;
                this.f7917C = cVar.f7917C;
                this.f7918D = cVar.f7918D;
                this.f7919E = cVar.f7919E;
                this.f7920F = cVar.f7920F;
                this.f7921G = cVar.f7921G;
                this.f7922H = cVar.f7922H;
                this.f7923I = cVar.f7923I;
                if (cVar.f7926c == this.f7926c) {
                    if (cVar.f7933j) {
                        this.f7934k = new android.graphics.Rect(cVar.f7934k);
                        this.f7933j = true;
                    }
                    if (cVar.f7936m) {
                        this.f7937n = cVar.f7937n;
                        this.f7938o = cVar.f7938o;
                        this.f7939p = cVar.f7939p;
                        this.f7940q = cVar.f7940q;
                        this.f7936m = true;
                    }
                }
                if (cVar.f7941r) {
                    this.f7942s = cVar.f7942s;
                    this.f7941r = true;
                }
                if (cVar.f7943t) {
                    this.f7944u = cVar.f7944u;
                    this.f7943t = true;
                }
                android.graphics.drawable.Drawable[] drawableArr = cVar.f7930g;
                this.f7930g = new android.graphics.drawable.Drawable[drawableArr.length];
                this.f7931h = cVar.f7931h;
                android.util.SparseArray<android.graphics.drawable.Drawable.ConstantState> sparseArray = cVar.f7929f;
                if (sparseArray != null) {
                    this.f7929f = sparseArray.clone();
                } else {
                    this.f7929f = new android.util.SparseArray<>(this.f7931h);
                }
                int i = this.f7931h;
                for (int i2 = 0; i2 < i; i2++) {
                    if (drawableArr[i2] != null) {
                        android.graphics.drawable.Drawable.ConstantState constantState = drawableArr[i2].getConstantState();
                        if (constantState != null) {
                            this.f7929f.put(i2, constantState);
                        } else {
                            this.f7930g[i2] = drawableArr[i2];
                        }
                    }
                }
                return;
            }
            this.f7930g = new android.graphics.drawable.Drawable[10];
            this.f7931h = 0;
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo14204a(android.graphics.drawable.Drawable drawable) {
            int i = this.f7931h;
            if (i >= this.f7930g.length) {
                mo14206a(i, i + 10);
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback(this.f7924a);
            this.f7930g[i] = drawable;
            this.f7931h++;
            this.f7928e = drawable.getChangingConfigurations() | this.f7928e;
            mo14228l();
            this.f7934k = null;
            this.f7933j = false;
            this.f7936m = false;
            this.f7945v = false;
            return i;
        }

        @DexIgnore
        /* renamed from: b */
        public final android.graphics.drawable.Drawable mo14211b(android.graphics.drawable.Drawable drawable) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(this.f7949z);
            }
            android.graphics.drawable.Drawable mutate = drawable.mutate();
            mutate.setCallback(this.f7924a);
            return mutate;
        }

        @DexIgnore
        /* renamed from: c */
        public final void mo14216c() {
            android.util.SparseArray<android.graphics.drawable.Drawable.ConstantState> sparseArray = this.f7929f;
            if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i = 0; i < size; i++) {
                    this.f7930g[this.f7929f.keyAt(i)] = mo14211b(this.f7929f.valueAt(i).newDrawable(this.f7925b));
                }
                this.f7929f = null;
            }
        }

        @DexIgnore
        public boolean canApplyTheme() {
            int i = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            for (int i2 = 0; i2 < i; i2++) {
                android.graphics.drawable.Drawable drawable = drawableArr[i2];
                if (drawable == null) {
                    android.graphics.drawable.Drawable.ConstantState constantState = this.f7929f.get(i2);
                    if (constantState != null && constantState.canApplyTheme()) {
                        return true;
                    }
                } else if (drawable.canApplyTheme()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: d */
        public final int mo14219d() {
            return this.f7930g.length;
        }

        @DexIgnore
        /* renamed from: e */
        public final int mo14220e() {
            return this.f7931h;
        }

        @DexIgnore
        /* renamed from: f */
        public final int mo14221f() {
            if (!this.f7936m) {
                mo14212b();
            }
            return this.f7938o;
        }

        @DexIgnore
        /* renamed from: g */
        public final int mo14222g() {
            if (!this.f7936m) {
                mo14212b();
            }
            return this.f7940q;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f7927d | this.f7928e;
        }

        @DexIgnore
        /* renamed from: h */
        public final int mo14224h() {
            if (!this.f7936m) {
                mo14212b();
            }
            return this.f7939p;
        }

        @DexIgnore
        /* renamed from: i */
        public final android.graphics.Rect mo14225i() {
            if (this.f7932i) {
                return null;
            }
            if (this.f7934k != null || this.f7933j) {
                return this.f7934k;
            }
            mo14216c();
            android.graphics.Rect rect = new android.graphics.Rect();
            int i = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            android.graphics.Rect rect2 = null;
            for (int i2 = 0; i2 < i; i2++) {
                if (drawableArr[i2].getPadding(rect)) {
                    if (rect2 == null) {
                        rect2 = new android.graphics.Rect(0, 0, 0, 0);
                    }
                    int i3 = rect.left;
                    if (i3 > rect2.left) {
                        rect2.left = i3;
                    }
                    int i4 = rect.top;
                    if (i4 > rect2.top) {
                        rect2.top = i4;
                    }
                    int i5 = rect.right;
                    if (i5 > rect2.right) {
                        rect2.right = i5;
                    }
                    int i6 = rect.bottom;
                    if (i6 > rect2.bottom) {
                        rect2.bottom = i6;
                    }
                }
            }
            this.f7933j = true;
            this.f7934k = rect2;
            return rect2;
        }

        @DexIgnore
        /* renamed from: j */
        public final int mo14226j() {
            if (!this.f7936m) {
                mo14212b();
            }
            return this.f7937n;
        }

        @DexIgnore
        /* renamed from: k */
        public final int mo14227k() {
            if (this.f7941r) {
                return this.f7942s;
            }
            mo14216c();
            int i = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            int opacity = i > 0 ? drawableArr[0].getOpacity() : -2;
            for (int i2 = 1; i2 < i; i2++) {
                opacity = android.graphics.drawable.Drawable.resolveOpacity(opacity, drawableArr[i2].getOpacity());
            }
            this.f7942s = opacity;
            this.f7941r = true;
            return opacity;
        }

        @DexIgnore
        /* renamed from: l */
        public void mo14228l() {
            this.f7941r = false;
            this.f7943t = false;
        }

        @DexIgnore
        /* renamed from: m */
        public final boolean mo14229m() {
            return this.f7935l;
        }

        @DexIgnore
        /* renamed from: n */
        public abstract void mo13780n();

        @DexIgnore
        /* renamed from: b */
        public final boolean mo14215b(int i, int i2) {
            int i3 = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            boolean z = false;
            for (int i4 = 0; i4 < i3; i4++) {
                if (drawableArr[i4] != null) {
                    boolean layoutDirection = android.os.Build.VERSION.SDK_INT >= 23 ? drawableArr[i4].setLayoutDirection(i) : false;
                    if (i4 == i2) {
                        z = layoutDirection;
                    }
                }
            }
            this.f7949z = i;
            return z;
        }

        @DexIgnore
        /* renamed from: c */
        public final void mo14217c(int i) {
            this.f7916B = i;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo14214b(boolean z) {
            this.f7932i = z;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14212b() {
            this.f7936m = true;
            mo14216c();
            int i = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            this.f7938o = -1;
            this.f7937n = -1;
            this.f7940q = 0;
            this.f7939p = 0;
            for (int i2 = 0; i2 < i; i2++) {
                android.graphics.drawable.Drawable drawable = drawableArr[i2];
                int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.f7937n) {
                    this.f7937n = intrinsicWidth;
                }
                int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.f7938o) {
                    this.f7938o = intrinsicHeight;
                }
                int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.f7939p) {
                    this.f7939p = minimumWidth;
                }
                int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.f7940q) {
                    this.f7940q = minimumHeight;
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final android.graphics.drawable.Drawable mo14205a(int i) {
            android.graphics.drawable.Drawable drawable = this.f7930g[i];
            if (drawable != null) {
                return drawable;
            }
            android.util.SparseArray<android.graphics.drawable.Drawable.ConstantState> sparseArray = this.f7929f;
            if (sparseArray != null) {
                int indexOfKey = sparseArray.indexOfKey(i);
                if (indexOfKey >= 0) {
                    android.graphics.drawable.Drawable b = mo14211b(this.f7929f.valueAt(indexOfKey).newDrawable(this.f7925b));
                    this.f7930g[i] = b;
                    this.f7929f.removeAt(indexOfKey);
                    if (this.f7929f.size() == 0) {
                        this.f7929f = null;
                    }
                    return b;
                }
            }
            return null;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo14208a(android.content.res.Resources resources) {
            if (resources != null) {
                this.f7925b = resources;
                int a = com.fossil.blesdk.obfuscated.C2514o0.m11513a(resources, this.f7926c);
                int i = this.f7926c;
                this.f7926c = a;
                if (i != a) {
                    this.f7936m = false;
                    this.f7933j = false;
                }
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo14213b(int i) {
            this.f7915A = i;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo14207a(android.content.res.Resources.Theme theme) {
            if (theme != null) {
                mo14216c();
                int i = this.f7931h;
                android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
                for (int i2 = 0; i2 < i; i2++) {
                    if (drawableArr[i2] != null && drawableArr[i2].canApplyTheme()) {
                        drawableArr[i2].applyTheme(theme);
                        this.f7928e |= drawableArr[i2].getChangingConfigurations();
                    }
                }
                mo14208a(theme.getResources());
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo14209a(boolean z) {
            this.f7935l = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14206a(int i, int i2) {
            android.graphics.drawable.Drawable[] drawableArr = new android.graphics.drawable.Drawable[i2];
            java.lang.System.arraycopy(this.f7930g, 0, drawableArr, 0, i);
            this.f7930g = drawableArr;
        }

        @DexIgnore
        /* renamed from: a */
        public synchronized boolean mo14210a() {
            if (this.f7945v) {
                return this.f7946w;
            }
            mo14216c();
            this.f7945v = true;
            int i = this.f7931h;
            android.graphics.drawable.Drawable[] drawableArr = this.f7930g;
            for (int i2 = 0; i2 < i; i2++) {
                if (drawableArr[i2].getConstantState() == null) {
                    this.f7946w = false;
                    return false;
                }
            }
            this.f7946w = true;
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2514o0.C2517c mo13757a() {
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0073  */
    /* renamed from: a */
    public boolean mo14165a(int i) {
        java.lang.Runnable runnable;
        if (i == this.f7907k) {
            return false;
        }
        long uptimeMillis = android.os.SystemClock.uptimeMillis();
        if (this.f7901e.f7916B > 0) {
            android.graphics.drawable.Drawable drawable = this.f7904h;
            if (drawable != null) {
                drawable.setVisible(false, false);
            }
            android.graphics.drawable.Drawable drawable2 = this.f7903g;
            if (drawable2 != null) {
                this.f7904h = drawable2;
                this.f7911o = ((long) this.f7901e.f7916B) + uptimeMillis;
            } else {
                this.f7904h = null;
                this.f7911o = 0;
            }
        } else {
            android.graphics.drawable.Drawable drawable3 = this.f7903g;
            if (drawable3 != null) {
                drawable3.setVisible(false, false);
            }
        }
        if (i >= 0) {
            com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
            if (i < cVar.f7931h) {
                android.graphics.drawable.Drawable a = cVar.mo14205a(i);
                this.f7903g = a;
                this.f7907k = i;
                if (a != null) {
                    int i2 = this.f7901e.f7915A;
                    if (i2 > 0) {
                        this.f7910n = uptimeMillis + ((long) i2);
                    }
                    mo14163a(a);
                }
                if (!(this.f7910n == 0 && this.f7911o == 0)) {
                    runnable = this.f7909m;
                    if (runnable != null) {
                        this.f7909m = new com.fossil.blesdk.obfuscated.C2514o0.C2515a();
                    } else {
                        unscheduleSelf(runnable);
                    }
                    mo14164a(true);
                }
                invalidateSelf();
                return true;
            }
        }
        this.f7903g = null;
        this.f7907k = -1;
        runnable = this.f7909m;
        if (runnable != null) {
        }
        mo14164a(true);
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void applyTheme(android.content.res.Resources.Theme theme) {
        this.f7901e.mo14207a(theme);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo14167b() {
        return this.f7907k;
    }

    @DexIgnore
    @android.annotation.SuppressLint({"WrongConstant"})
    @android.annotation.TargetApi(23)
    /* renamed from: c */
    public final boolean mo14168c() {
        return isAutoMirrored() && getLayoutDirection() == 1;
    }

    @DexIgnore
    public boolean canApplyTheme() {
        return this.f7901e.canApplyTheme();
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        android.graphics.drawable.Drawable drawable2 = this.f7904h;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    @DexIgnore
    public int getAlpha() {
        return this.f7905i;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.f7901e.getChangingConfigurations();
    }

    @DexIgnore
    public final android.graphics.drawable.Drawable.ConstantState getConstantState() {
        if (!this.f7901e.mo14210a()) {
            return null;
        }
        this.f7901e.f7927d = getChangingConfigurations();
        return this.f7901e;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getCurrent() {
        return this.f7903g;
    }

    @DexIgnore
    public void getHotspotBounds(android.graphics.Rect rect) {
        android.graphics.Rect rect2 = this.f7902f;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        if (this.f7901e.mo14229m()) {
            return this.f7901e.mo14221f();
        }
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return -1;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        if (this.f7901e.mo14229m()) {
            return this.f7901e.mo14226j();
        }
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return -1;
    }

    @DexIgnore
    public int getMinimumHeight() {
        if (this.f7901e.mo14229m()) {
            return this.f7901e.mo14222g();
        }
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return 0;
    }

    @DexIgnore
    public int getMinimumWidth() {
        if (this.f7901e.mo14229m()) {
            return this.f7901e.mo14224h();
        }
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return 0;
    }

    @DexIgnore
    public int getOpacity() {
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable == null || !drawable.isVisible()) {
            return -2;
        }
        return this.f7901e.mo14227k();
    }

    @DexIgnore
    public void getOutline(android.graphics.Outline outline) {
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            drawable.getOutline(outline);
        }
    }

    @DexIgnore
    public boolean getPadding(android.graphics.Rect rect) {
        boolean z;
        android.graphics.Rect i = this.f7901e.mo14225i();
        if (i != null) {
            rect.set(i);
            z = (i.right | ((i.left | i.top) | i.bottom)) != 0;
        } else {
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable != null) {
                z = drawable.getPadding(rect);
            } else {
                z = super.getPadding(rect);
            }
        }
        if (mo14168c()) {
            int i2 = rect.left;
            rect.left = rect.right;
            rect.right = i2;
        }
        return z;
    }

    @DexIgnore
    public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        if (cVar != null) {
            cVar.mo14228l();
        }
        if (drawable == this.f7903g && getCallback() != null) {
            getCallback().invalidateDrawable(this);
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.f7901e.f7917C;
    }

    @DexIgnore
    public void jumpToCurrentState() {
        boolean z;
        android.graphics.drawable.Drawable drawable = this.f7904h;
        if (drawable != null) {
            drawable.jumpToCurrentState();
            this.f7904h = null;
            z = true;
        } else {
            z = false;
        }
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
            if (this.f7906j) {
                this.f7903g.setAlpha(this.f7905i);
            }
        }
        if (this.f7911o != 0) {
            this.f7911o = 0;
            z = true;
        }
        if (this.f7910n != 0) {
            this.f7910n = 0;
            z = true;
        }
        if (z) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        if (!this.f7908l && super.mutate() == this) {
            com.fossil.blesdk.obfuscated.C2514o0.C2517c a = mo13757a();
            a.mo13780n();
            mo13760a(a);
            this.f7908l = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        android.graphics.drawable.Drawable drawable = this.f7904h;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            drawable2.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i) {
        return this.f7901e.mo14215b(i, mo14167b());
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        android.graphics.drawable.Drawable drawable = this.f7904h;
        if (drawable != null) {
            return drawable.setLevel(i);
        }
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            return drawable2.setLevel(i);
        }
        return false;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        android.graphics.drawable.Drawable drawable = this.f7904h;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            return drawable2.setState(iArr);
        }
        return false;
    }

    @DexIgnore
    public void scheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable, long j) {
        if (drawable == this.f7903g && getCallback() != null) {
            getCallback().scheduleDrawable(this, runnable, j);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
        if (!this.f7906j || this.f7905i != i) {
            this.f7906j = true;
            this.f7905i = i;
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable == null) {
                return;
            }
            if (this.f7910n == 0) {
                drawable.setAlpha(i);
            } else {
                mo14164a(false);
            }
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        if (cVar.f7917C != z) {
            cVar.f7917C = z;
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable != null) {
                com.fossil.blesdk.obfuscated.C1538c7.m5303a(drawable, cVar.f7917C);
            }
        }
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        cVar.f7919E = true;
        if (cVar.f7918D != colorFilter) {
            cVar.f7918D = colorFilter;
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
        }
    }

    @DexIgnore
    public void setDither(boolean z) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        if (cVar.f7947x != z) {
            cVar.f7947x = z;
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable != null) {
                drawable.setDither(cVar.f7947x);
            }
        }
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5297a(drawable, f, f2);
        }
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        android.graphics.Rect rect = this.f7902f;
        if (rect == null) {
            this.f7902f = new android.graphics.Rect(i, i2, i3, i4);
        } else {
            rect.set(i, i2, i3, i4);
        }
        android.graphics.drawable.Drawable drawable = this.f7903g;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5298a(drawable, i, i2, i3, i4);
        }
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        cVar.f7922H = true;
        if (cVar.f7920F != colorStateList) {
            cVar.f7920F = colorStateList;
            com.fossil.blesdk.obfuscated.C1538c7.m5299a(this.f7903g, colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar = this.f7901e;
        cVar.f7923I = true;
        if (cVar.f7921G != mode) {
            cVar.f7921G = mode;
            com.fossil.blesdk.obfuscated.C1538c7.m5302a(this.f7903g, mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        android.graphics.drawable.Drawable drawable = this.f7904h;
        if (drawable != null) {
            drawable.setVisible(z, z2);
        }
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            drawable2.setVisible(z, z2);
        }
        return visible;
    }

    @DexIgnore
    public void unscheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable) {
        if (drawable == this.f7903g && getCallback() != null) {
            getCallback().unscheduleDrawable(this, runnable);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14163a(android.graphics.drawable.Drawable drawable) {
        if (this.f7912p == null) {
            this.f7912p = new com.fossil.blesdk.obfuscated.C2514o0.C2516b();
        }
        com.fossil.blesdk.obfuscated.C2514o0.C2516b bVar = this.f7912p;
        bVar.mo14200a(drawable.getCallback());
        drawable.setCallback(bVar);
        try {
            if (this.f7901e.f7915A <= 0 && this.f7906j) {
                drawable.setAlpha(this.f7905i);
            }
            if (this.f7901e.f7919E) {
                drawable.setColorFilter(this.f7901e.f7918D);
            } else {
                if (this.f7901e.f7922H) {
                    com.fossil.blesdk.obfuscated.C1538c7.m5299a(drawable, this.f7901e.f7920F);
                }
                if (this.f7901e.f7923I) {
                    com.fossil.blesdk.obfuscated.C1538c7.m5302a(drawable, this.f7901e.f7921G);
                }
            }
            drawable.setVisible(isVisible(), true);
            drawable.setDither(this.f7901e.f7947x);
            drawable.setState(getState());
            drawable.setLevel(getLevel());
            drawable.setBounds(getBounds());
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(getLayoutDirection());
            }
            if (android.os.Build.VERSION.SDK_INT >= 19) {
                drawable.setAutoMirrored(this.f7901e.f7917C);
            }
            android.graphics.Rect rect = this.f7902f;
            if (android.os.Build.VERSION.SDK_INT >= 21 && rect != null) {
                drawable.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            drawable.setCallback(this.f7912p.mo14199a());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    public void mo14164a(boolean z) {
        boolean z2;
        android.graphics.drawable.Drawable drawable;
        boolean z3 = true;
        this.f7906j = true;
        long uptimeMillis = android.os.SystemClock.uptimeMillis();
        android.graphics.drawable.Drawable drawable2 = this.f7903g;
        if (drawable2 != null) {
            long j = this.f7910n;
            if (j != 0) {
                if (j <= uptimeMillis) {
                    drawable2.setAlpha(this.f7905i);
                    this.f7910n = 0;
                } else {
                    drawable2.setAlpha(((255 - (((int) ((j - uptimeMillis) * 255)) / this.f7901e.f7915A)) * this.f7905i) / 255);
                    z2 = true;
                    drawable = this.f7904h;
                    if (drawable == null) {
                        long j2 = this.f7911o;
                        if (j2 != 0) {
                            if (j2 <= uptimeMillis) {
                                drawable.setVisible(false, false);
                                this.f7904h = null;
                                this.f7911o = 0;
                            } else {
                                drawable.setAlpha(((((int) ((j2 - uptimeMillis) * 255)) / this.f7901e.f7916B) * this.f7905i) / 255);
                                if (!z && z3) {
                                    scheduleSelf(this.f7909m, uptimeMillis + 16);
                                    return;
                                }
                                return;
                            }
                        }
                    } else {
                        this.f7911o = 0;
                    }
                    z3 = z2;
                    if (!z) {
                        return;
                    }
                    return;
                }
            }
        } else {
            this.f7910n = 0;
        }
        z2 = false;
        drawable = this.f7904h;
        if (drawable == null) {
        }
        z3 = z2;
        if (!z) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14162a(android.content.res.Resources resources) {
        this.f7901e.mo14208a(resources);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13760a(com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar) {
        this.f7901e = cVar;
        int i = this.f7907k;
        if (i >= 0) {
            this.f7903g = cVar.mo14205a(i);
            android.graphics.drawable.Drawable drawable = this.f7903g;
            if (drawable != null) {
                mo14163a(drawable);
            }
        }
        this.f7904h = null;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11513a(android.content.res.Resources resources, int i) {
        if (resources != null) {
            i = resources.getDisplayMetrics().densityDpi;
        }
        if (i == 0) {
            return 160;
        }
        return i;
    }
}
