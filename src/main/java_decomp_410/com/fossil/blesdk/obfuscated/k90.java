package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k90 {
    @DexIgnore
    public static /* synthetic */ String a(byte[] bArr, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        return a(bArr, str);
    }

    @DexIgnore
    public static final byte[] b(byte[] bArr, int i) {
        kd4.b(bArr, "$this$increaseAsNumber");
        long b = n90.b(i);
        for (int length = bArr.length - 1; length >= 0; length--) {
            long b2 = b + ((long) n90.b(bArr[length]));
            long j = (long) 256;
            long j2 = b2 % j;
            b = b2 / j;
            bArr[length] = (byte) ((int) j2);
            if (b == 0) {
                break;
            }
        }
        return bArr;
    }

    @DexIgnore
    public static final String a(byte[] bArr, String str) {
        kd4.b(bArr, "$this$toHexString");
        kd4.b(str, "separator");
        int i = 0;
        if (bArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        while (i < bArr.length - 1) {
            sb.append(n90.a(bArr[i]));
            sb.append(str);
            i++;
        }
        if (i == bArr.length - 1) {
            sb.append(n90.a(bArr[i]));
        }
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "sb.toString()");
        return sb2;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        kd4.b(bArr, "$this$concatenate");
        kd4.b(bArr2, FacebookRequestErrorClassification.KEY_OTHER);
        if (bArr2.length == 0) {
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            kd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
            return copyOf;
        }
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        System.arraycopy(bArr, 0, bArr3, 0, bArr.length);
        System.arraycopy(bArr2, 0, bArr3, bArr.length, bArr2.length);
        return bArr3;
    }

    @DexIgnore
    public static final byte[][] a(byte[] bArr, int i) {
        kd4.b(bArr, "$this$divide");
        if (i <= 0) {
            return new byte[][]{bArr};
        }
        ArrayList arrayList = new ArrayList();
        wd4 a = ee4.a((wd4) ee4.d(0, bArr.length), i);
        int a2 = a.a();
        int b = a.b();
        int c = a.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                arrayList.add(ya4.a(bArr, a2, Math.min(a2 + i, bArr.length)));
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
