package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.by */
public interface C1519by extends com.fossil.blesdk.obfuscated.s64 {
    @DexIgnore
    /* renamed from: a */
    void mo9344a();

    @DexIgnore
    /* renamed from: a */
    void mo9345a(com.crashlytics.android.answers.SessionEvent.C0393b bVar);

    @DexIgnore
    /* renamed from: a */
    void mo9346a(com.fossil.blesdk.obfuscated.j74 j74, java.lang.String str);

    @DexIgnore
    /* renamed from: d */
    void mo9347d();
}
