package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wq */
public class C3234wq implements com.fossil.blesdk.obfuscated.C2981tq.C2982a {

    @DexIgnore
    /* renamed from: a */
    public /* final */ long f10680a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3234wq.C3235a f10681b;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.wq$a */
    public interface C3235a {
        @DexIgnore
        /* renamed from: a */
        java.io.File mo17506a();
    }

    @DexIgnore
    public C3234wq(com.fossil.blesdk.obfuscated.C3234wq.C3235a aVar, long j) {
        this.f10680a = j;
        this.f10681b = aVar;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2981tq build() {
        java.io.File a = this.f10681b.mo17506a();
        if (a == null) {
            return null;
        }
        if (a.mkdirs() || (a.exists() && a.isDirectory())) {
            return com.fossil.blesdk.obfuscated.C3304xq.m16474a(a, this.f10680a);
        }
        return null;
    }
}
