package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.blesdk.obfuscated.o6;
import com.fossil.blesdk.obfuscated.u7;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y6 extends w6 {
    @DexIgnore
    public /* final */ Class a;
    @DexIgnore
    public /* final */ Constructor b;
    @DexIgnore
    public /* final */ Method c;
    @DexIgnore
    public /* final */ Method d;
    @DexIgnore
    public /* final */ Method e;
    @DexIgnore
    public /* final */ Method f;
    @DexIgnore
    public /* final */ Method g;

    @DexIgnore
    public y6() {
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Constructor constructor;
        Method method5;
        Class cls = null;
        try {
            Class c2 = c();
            constructor = e(c2);
            method4 = b(c2);
            method3 = c(c2);
            method2 = f(c2);
            method = a(c2);
            Class cls2 = c2;
            method5 = d(c2);
            cls = cls2;
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e2.getClass().getName(), e2);
            method5 = null;
            constructor = null;
            method4 = null;
            method3 = null;
            method2 = null;
            method = null;
        }
        this.a = cls;
        this.b = constructor;
        this.c = method4;
        this.d = method3;
        this.e = method2;
        this.f = method;
        this.g = method5;
    }

    @DexIgnore
    public final boolean a() {
        if (this.c == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return this.c != null;
    }

    @DexIgnore
    public final Object b() {
        try {
            return this.b.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final boolean c(Object obj) {
        try {
            return ((Boolean) this.e.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public Method d(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", new Class[]{Array.newInstance(cls, 1).getClass(), cls2, cls2});
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    @DexIgnore
    public Constructor e(Class cls) throws NoSuchMethodException {
        return cls.getConstructor(new Class[0]);
    }

    @DexIgnore
    public Method f(Class cls) throws NoSuchMethodException {
        return cls.getMethod("freeze", new Class[0]);
    }

    @DexIgnore
    public Typeface b(Object obj) {
        try {
            Object newInstance = Array.newInstance(this.a, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) this.g.invoke((Object) null, new Object[]{newInstance, -1, -1});
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final boolean a(Context context, Object obj, String str, int i, int i2, int i3, FontVariationAxis[] fontVariationAxisArr) {
        try {
            return ((Boolean) this.c.invoke(obj, new Object[]{context.getAssets(), str, 0, false, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), fontVariationAxisArr})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public Class c() throws ClassNotFoundException {
        return Class.forName("android.graphics.FontFamily");
    }

    @DexIgnore
    public Method c(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        return cls.getMethod("addFontFromBuffer", new Class[]{ByteBuffer.class, cls2, FontVariationAxis[].class, cls2, cls2});
    }

    @DexIgnore
    public Method b(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        return cls.getMethod("addFontFromAssetManager", new Class[]{AssetManager.class, String.class, Integer.TYPE, Boolean.TYPE, cls2, cls2, cls2, FontVariationAxis[].class});
    }

    @DexIgnore
    public final boolean a(Object obj, ByteBuffer byteBuffer, int i, int i2, int i3) {
        try {
            return ((Boolean) this.d.invoke(obj, new Object[]{byteBuffer, Integer.valueOf(i), null, Integer.valueOf(i2), Integer.valueOf(i3)})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final void a(Object obj) {
        try {
            this.f.invoke(obj, new Object[0]);
        } catch (IllegalAccessException | InvocationTargetException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public Typeface a(Context context, o6.b bVar, Resources resources, int i) {
        if (!a()) {
            return super.a(context, bVar, resources, i);
        }
        Object b2 = b();
        for (o6.c cVar : bVar.a()) {
            if (!a(context, b2, cVar.a(), cVar.c(), cVar.e(), cVar.f() ? 1 : 0, FontVariationAxis.fromFontVariationSettings(cVar.d()))) {
                a(b2);
                return null;
            }
        }
        if (!c(b2)) {
            return null;
        }
        return b(b2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004b, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        if (r11 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        throw r13;
     */
    @DexIgnore
    public Typeface a(Context context, CancellationSignal cancellationSignal, u7.f[] fVarArr, int i) {
        if (fVarArr.length < 1) {
            return null;
        }
        if (!a()) {
            u7.f a2 = a(fVarArr, i);
            try {
                ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(a2.c(), "r", cancellationSignal);
                if (openFileDescriptor == null) {
                    if (openFileDescriptor != null) {
                        openFileDescriptor.close();
                    }
                    return null;
                }
                Typeface build = new Typeface.Builder(openFileDescriptor.getFileDescriptor()).setWeight(a2.d()).setItalic(a2.e()).build();
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                return build;
            } catch (IOException unused) {
                return null;
            } catch (Throwable th) {
                r12.addSuppressed(th);
            }
        } else {
            Map<Uri, ByteBuffer> a3 = u7.a(context, fVarArr, cancellationSignal);
            Object b2 = b();
            boolean z = false;
            for (u7.f fVar : fVarArr) {
                ByteBuffer byteBuffer = a3.get(fVar.c());
                if (byteBuffer != null) {
                    if (!a(b2, byteBuffer, fVar.b(), fVar.d(), fVar.e() ? 1 : 0)) {
                        a(b2);
                        return null;
                    }
                    z = true;
                }
            }
            if (!z) {
                a(b2);
                return null;
            } else if (!c(b2)) {
                return null;
            } else {
                return Typeface.create(b(b2), i);
            }
        }
    }

    @DexIgnore
    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        if (!a()) {
            return super.a(context, resources, i, str, i2);
        }
        Object b2 = b();
        if (!a(context, b2, str, 0, -1, -1, (FontVariationAxis[]) null)) {
            a(b2);
            return null;
        } else if (!c(b2)) {
            return null;
        } else {
            return b(b2);
        }
    }

    @DexIgnore
    public Method a(Class cls) throws NoSuchMethodException {
        return cls.getMethod("abortCreation", new Class[0]);
    }
}
