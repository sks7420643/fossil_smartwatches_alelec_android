package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ak0;
import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.Status;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hl0 implements he0.a {
    @DexIgnore
    public /* final */ /* synthetic */ he0 a;
    @DexIgnore
    public /* final */ /* synthetic */ xn1 b;
    @DexIgnore
    public /* final */ /* synthetic */ ak0.a c;
    @DexIgnore
    public /* final */ /* synthetic */ ak0.b d;

    @DexIgnore
    public hl0(he0 he0, xn1 xn1, ak0.a aVar, ak0.b bVar) {
        this.a = he0;
        this.b = xn1;
        this.c = aVar;
        this.d = bVar;
    }

    @DexIgnore
    public final void a(Status status) {
        if (status.L()) {
            this.b.a(this.c.a(this.a.a(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.b.a((Exception) this.d.a(status));
    }
}
