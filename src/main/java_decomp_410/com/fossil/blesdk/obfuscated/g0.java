package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g0 extends xa {
    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        return new f0(getContext(), getTheme());
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i) {
        if (dialog instanceof f0) {
            f0 f0Var = (f0) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            f0Var.a(1);
            return;
        }
        super.setupDialog(dialog, i);
    }
}
