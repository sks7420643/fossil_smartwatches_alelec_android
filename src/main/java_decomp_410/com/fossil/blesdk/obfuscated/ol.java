package com.fossil.blesdk.obfuscated;

import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import com.fossil.blesdk.obfuscated.fj;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ol implements Runnable {
    @DexIgnore
    public /* final */ nj e; // = new nj();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ol {
        @DexIgnore
        public /* final */ /* synthetic */ tj f;
        @DexIgnore
        public /* final */ /* synthetic */ String g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        public a(tj tjVar, String str, boolean z) {
            this.f = tjVar;
            this.g = str;
            this.h = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public void a() {
            WorkDatabase g2 = this.f.g();
            g2.beginTransaction();
            try {
                for (String a : g2.d().c(this.g)) {
                    a(this.f, a);
                }
                g2.setTransactionSuccessful();
                g2.endTransaction();
                if (this.h) {
                    a(this.f);
                }
            } catch (Throwable th) {
                g2.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void a(tj tjVar, String str) {
        a(tjVar.g(), str);
        tjVar.e().d(str);
        for (pj a2 : tjVar.f()) {
            a2.a(str);
        }
    }

    @DexIgnore
    public void run() {
        try {
            a();
            this.e.a(fj.a);
        } catch (Throwable th) {
            this.e.a(new fj.b.a(th));
        }
    }

    @DexIgnore
    public void a(tj tjVar) {
        qj.a(tjVar.c(), tjVar.g(), tjVar.f());
    }

    @DexIgnore
    public final void a(WorkDatabase workDatabase, String str) {
        il d = workDatabase.d();
        zk a2 = workDatabase.a();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            WorkInfo.State d2 = d.d(str2);
            if (!(d2 == WorkInfo.State.SUCCEEDED || d2 == WorkInfo.State.FAILED)) {
                d.a(WorkInfo.State.CANCELLED, str2);
            }
            linkedList.addAll(a2.a(str2));
        }
    }

    @DexIgnore
    public static ol a(String str, tj tjVar, boolean z) {
        return new a(tjVar, str, z);
    }
}
