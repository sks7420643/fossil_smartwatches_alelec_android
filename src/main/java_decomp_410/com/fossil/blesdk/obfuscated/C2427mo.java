package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mo */
public interface C2427mo<T, Z> {
    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C1438aq<Z> mo9299a(T t, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException;

    @DexIgnore
    /* renamed from: a */
    boolean mo9301a(T t, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException;
}
