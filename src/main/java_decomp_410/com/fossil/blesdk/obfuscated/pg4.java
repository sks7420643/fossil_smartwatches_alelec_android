package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pg4 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ si4 c;

    @DexIgnore
    public pg4(Object obj, Object obj2, si4 si4) {
        kd4.b(si4, "token");
        this.a = obj;
        this.b = obj2;
        this.c = si4;
    }

    @DexIgnore
    public String toString() {
        return "CompletedIdempotentResult[" + this.b + ']';
    }
}
