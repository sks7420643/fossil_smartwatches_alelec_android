package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lg0 extends ah0 {
    @DexIgnore
    public WeakReference<eg0> a;

    @DexIgnore
    public lg0(eg0 eg0) {
        this.a = new WeakReference<>(eg0);
    }

    @DexIgnore
    public final void a() {
        eg0 eg0 = (eg0) this.a.get();
        if (eg0 != null) {
            eg0.l();
        }
    }
}
