package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.log.generic.LogEntry;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class aa0 extends x90 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aa0(String str, int i, String str2, String str3, na0 na0, int i2, v90 v90, SharedPreferenceFileName sharedPreferenceFileName, boolean z) {
        super(str, i, str2, str3, na0, i2, v90, sharedPreferenceFileName, z);
        kd4.b(str, "logDir");
        kd4.b(str2, "logFileNamePrefix");
        kd4.b(str3, "logFileNameExtension");
        kd4.b(na0, "endpoint");
        kd4.b(v90, "logFileParser");
        kd4.b(sharedPreferenceFileName, "sharedPreferenceFileName");
    }

    @DexIgnore
    public void a(LogEntry logEntry) {
        kd4.b(logEntry, "logEntry");
        super.a(logEntry);
        if (logEntry instanceof SdkLogEntry) {
            SdkLogEntry sdkLogEntry = (SdkLogEntry) logEntry;
            sdkLogEntry.setSystemInformation(ca0.e.a());
            DeviceInformation a = ca0.e.a(sdkLogEntry.getMacAddress());
            if (a != null) {
                sdkLogEntry.setDeviceInformation(a);
            }
            sdkLogEntry.setSessionUuid(ca0.e.b(sdkLogEntry.getMacAddress()));
        }
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public boolean b(LogEntry logEntry) {
        kd4.b(logEntry, "logEntry");
        boolean b = super.b(logEntry);
        if (b() > 0) {
            a(b());
        }
        return b;
    }
}
