package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zs */
public final class C3465zs implements com.bumptech.glide.load.ImageHeaderParser {
    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.ImageHeaderParser.ImageType mo3939a(java.io.InputStream inputStream) throws java.io.IOException {
        return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.ImageHeaderParser.ImageType mo3940a(java.nio.ByteBuffer byteBuffer) throws java.io.IOException {
        return com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo3938a(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) throws java.io.IOException {
        int attributeInt = new android.media.ExifInterface(inputStream).getAttributeInt("Orientation", 1);
        if (attributeInt == 0) {
            return -1;
        }
        return attributeInt;
    }
}
