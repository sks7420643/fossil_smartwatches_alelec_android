package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bl1 extends ti1 implements vi1 {
    @DexIgnore
    public /* final */ dl1 b;

    @DexIgnore
    public bl1(dl1 dl1) {
        super(dl1.B());
        bk0.a(dl1);
        this.b = dl1;
    }

    @DexIgnore
    public jl1 m() {
        return this.b.j();
    }

    @DexIgnore
    public tl1 n() {
        return this.b.k();
    }

    @DexIgnore
    public am1 o() {
        return this.b.l();
    }
}
