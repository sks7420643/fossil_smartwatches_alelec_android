package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bx */
public class C1518bx extends com.fossil.blesdk.obfuscated.v44<java.lang.Boolean> {

    @DexIgnore
    /* renamed from: k */
    public boolean f3934k; // = false;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1451ay f3935l;

    @DexIgnore
    /* renamed from: w */
    public static com.fossil.blesdk.obfuscated.C1518bx m5147w() {
        return (com.fossil.blesdk.obfuscated.C1518bx) com.fossil.blesdk.obfuscated.q44.m26795a(com.fossil.blesdk.obfuscated.C1518bx.class);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9326a(com.fossil.blesdk.obfuscated.C2439mx mxVar) {
        if (mxVar == null) {
            throw new java.lang.NullPointerException("event must not be null");
        } else if (this.f3934k) {
            mo9327a("logCustom");
        } else {
            com.fossil.blesdk.obfuscated.C1451ay ayVar = this.f3935l;
            if (ayVar != null) {
                ayVar.mo8978a(mxVar);
            }
        }
    }

    @DexIgnore
    /* renamed from: p */
    public java.lang.String mo9329p() {
        return "com.crashlytics.sdk.android:answers";
    }

    @DexIgnore
    /* renamed from: r */
    public java.lang.String mo9330r() {
        return "1.4.7.32";
    }

    @DexIgnore
    @android.annotation.SuppressLint({"NewApi"})
    /* renamed from: u */
    public boolean mo9331u() {
        long lastModified;
        try {
            android.content.Context l = mo31733l();
            android.content.pm.PackageManager packageManager = l.getPackageManager();
            java.lang.String packageName = l.getPackageName();
            android.content.pm.PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            java.lang.String num = java.lang.Integer.toString(packageInfo.versionCode);
            java.lang.String str = packageInfo.versionName == null ? "0.0" : packageInfo.versionName;
            if (android.os.Build.VERSION.SDK_INT >= 9) {
                lastModified = packageInfo.firstInstallTime;
            } else {
                lastModified = new java.io.File(packageManager.getApplicationInfo(packageName, 0).sourceDir).lastModified();
            }
            this.f3935l = com.fossil.blesdk.obfuscated.C1451ay.m4682a(this, l, mo31736o(), num, str, lastModified);
            this.f3935l.mo8981c();
            this.f3934k = new com.fossil.blesdk.obfuscated.t54().mo31152e(l);
            return true;
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Error retrieving app properties", e);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: v */
    public java.lang.String mo9332v() {
        return p011io.fabric.sdk.android.services.common.CommonUtils.m36879b(mo31733l(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    /* renamed from: k */
    public java.lang.Boolean m5152k() {
        if (!com.fossil.blesdk.obfuscated.o54.m26050a(mo31733l()).mo29818a()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Fabric", "Analytics collection disabled, because data collection is disabled by Firebase.");
            this.f3935l.mo8980b();
            return false;
        }
        try {
            com.fossil.blesdk.obfuscated.b84 a = com.fossil.blesdk.obfuscated.z74.m31276d().mo33017a();
            if (a == null) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30062e("Answers", "Failed to retrieve settings");
                return false;
            } else if (a.f13601d.f19365c) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Analytics collection enabled");
                this.f3935l.mo8977a(a.f13602e, mo9332v());
                return true;
            } else {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Analytics collection disabled");
                this.f3935l.mo8980b();
                return false;
            }
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Error dealing with settings", e);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9325a(com.fossil.blesdk.obfuscated.m54.C4653a aVar) {
        com.fossil.blesdk.obfuscated.C1451ay ayVar = this.f3935l;
        if (ayVar != null) {
            ayVar.mo8979a(aVar.mo29333b(), aVar.mo29332a());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9327a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30066w("Answers", "Method " + str + " is not supported when using Crashlytics through Firebase.");
    }
}
