package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k91 extends i91 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public k91() {
        super();
    }

    @DexIgnore
    public static <E> List<E> c(Object obj, long j) {
        return (List) kb1.f(obj, j);
    }

    @DexIgnore
    public final <L> List<L> a(Object obj, long j) {
        return a(obj, j, 10);
    }

    @DexIgnore
    public final void b(Object obj, long j) {
        Object obj2;
        List list = (List) kb1.f(obj, j);
        if (list instanceof h91) {
            obj2 = ((h91) list).C();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof ia1) || !(list instanceof z81)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                z81 z81 = (z81) list;
                if (z81.A()) {
                    z81.B();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        kb1.a(obj, j, obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: com.fossil.blesdk.obfuscated.g91} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: com.fossil.blesdk.obfuscated.g91} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: com.fossil.blesdk.obfuscated.g91} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static <L> List<L> a(Object obj, long j, int i) {
        g91 g91;
        List<L> list;
        List<L> c2 = c(obj, j);
        if (c2.isEmpty()) {
            if (c2 instanceof h91) {
                list = new g91(i);
            } else if (!(c2 instanceof ia1) || !(c2 instanceof z81)) {
                list = new ArrayList<>(i);
            } else {
                list = ((z81) c2).b(i);
            }
            kb1.a(obj, j, (Object) list);
            return list;
        }
        if (c.isAssignableFrom(c2.getClass())) {
            ArrayList arrayList = new ArrayList(c2.size() + i);
            arrayList.addAll(c2);
            kb1.a(obj, j, (Object) arrayList);
            g91 = arrayList;
        } else if (c2 instanceof hb1) {
            g91 g912 = new g91(c2.size() + i);
            g912.addAll((hb1) c2);
            kb1.a(obj, j, (Object) g912);
            g91 = g912;
        } else if (!(c2 instanceof ia1) || !(c2 instanceof z81)) {
            return c2;
        } else {
            z81 z81 = (z81) c2;
            if (z81.A()) {
                return c2;
            }
            z81 b = z81.b(c2.size() + i);
            kb1.a(obj, j, (Object) b);
            return b;
        }
        return g91;
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        List c2 = c(obj2, j);
        List a = a(obj, j, c2.size());
        int size = a.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            a.addAll(c2);
        }
        if (size > 0) {
            c2 = a;
        }
        kb1.a(obj, j, (Object) c2);
    }
}
