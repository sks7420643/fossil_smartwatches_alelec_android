package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qd */
public abstract class C2723qd<T> extends java.util.AbstractList<T> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.Executor f8604e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.concurrent.Executor f8605f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2723qd.C2726c<T> f8606g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2723qd.C2729f f8607h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2871sd<T> f8608i;

    @DexIgnore
    /* renamed from: j */
    public int f8609j; // = 0;

    @DexIgnore
    /* renamed from: k */
    public T f8610k; // = null;

    @DexIgnore
    /* renamed from: l */
    public /* final */ int f8611l;

    @DexIgnore
    /* renamed from: m */
    public boolean f8612m; // = false;

    @DexIgnore
    /* renamed from: n */
    public boolean f8613n; // = false;

    @DexIgnore
    /* renamed from: o */
    public int f8614o; // = Integer.MAX_VALUE;

    @DexIgnore
    /* renamed from: p */
    public int f8615p; // = Integer.MIN_VALUE;

    @DexIgnore
    /* renamed from: q */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f8616q; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: r */
    public /* final */ java.util.ArrayList<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2723qd.C2728e>> f8617r; // = new java.util.ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$a */
    public class C2724a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ boolean f8618e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ boolean f8619f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ boolean f8620g;

        @DexIgnore
        public C2724a(boolean z, boolean z2, boolean z3) {
            this.f8618e = z;
            this.f8619f = z2;
            this.f8620g = z3;
        }

        @DexIgnore
        public void run() {
            if (this.f8618e) {
                com.fossil.blesdk.obfuscated.C2723qd.this.f8606g.mo15199a();
            }
            if (this.f8619f) {
                com.fossil.blesdk.obfuscated.C2723qd.this.f8612m = true;
            }
            if (this.f8620g) {
                com.fossil.blesdk.obfuscated.C2723qd.this.f8613n = true;
            }
            com.fossil.blesdk.obfuscated.C2723qd.this.mo15182a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$b */
    public class C2725b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ boolean f8622e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ boolean f8623f;

        @DexIgnore
        public C2725b(boolean z, boolean z2) {
            this.f8622e = z;
            this.f8623f = z2;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2723qd.this.mo15183a(this.f8622e, this.f8623f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$c")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$c */
    public static abstract class C2726c<T> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo15199a();

        @DexIgnore
        /* renamed from: a */
        public abstract void mo15200a(T t);

        @DexIgnore
        /* renamed from: b */
        public abstract void mo15201b(T t);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$d")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$d */
    public static final class C2727d<Key, Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld<Key, Value> f8625a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2723qd.C2729f f8626b;

        @DexIgnore
        /* renamed from: c */
        public java.util.concurrent.Executor f8627c;

        @DexIgnore
        /* renamed from: d */
        public java.util.concurrent.Executor f8628d;

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C2723qd.C2726c f8629e;

        @DexIgnore
        /* renamed from: f */
        public Key f8630f;

        @DexIgnore
        public C2727d(com.fossil.blesdk.obfuscated.C2307ld<Key, Value> ldVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar) {
            if (ldVar == null) {
                throw new java.lang.IllegalArgumentException("DataSource may not be null");
            } else if (fVar != null) {
                this.f8625a = ldVar;
                this.f8626b = fVar;
            } else {
                throw new java.lang.IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2723qd.C2727d<Key, Value> mo15204a(java.util.concurrent.Executor executor) {
            this.f8628d = executor;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2723qd.C2727d<Key, Value> mo15206b(java.util.concurrent.Executor executor) {
            this.f8627c = executor;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2723qd.C2727d<Key, Value> mo15202a(com.fossil.blesdk.obfuscated.C2723qd.C2726c cVar) {
            this.f8629e = cVar;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2723qd.C2727d<Key, Value> mo15203a(Key key) {
            this.f8630f = key;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2723qd<Value> mo15205a() {
            java.util.concurrent.Executor executor = this.f8627c;
            if (executor != null) {
                java.util.concurrent.Executor executor2 = this.f8628d;
                if (executor2 != null) {
                    return com.fossil.blesdk.obfuscated.C2723qd.m12764a(this.f8625a, executor, executor2, this.f8629e, this.f8626b, this.f8630f);
                }
                throw new java.lang.IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new java.lang.IllegalArgumentException("MainThreadExecutor required");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$e")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$e */
    public static abstract class C2728e {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo11912a(int i, int i2);

        @DexIgnore
        /* renamed from: b */
        public abstract void mo11913b(int i, int i2);

        @DexIgnore
        /* renamed from: c */
        public abstract void mo11914c(int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$f")
    /* renamed from: com.fossil.blesdk.obfuscated.qd$f */
    public static class C2729f {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f8631a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f8632b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ boolean f8633c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ int f8634d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f8635e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qd$f$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qd$f$a */
        public static final class C2730a {

            @DexIgnore
            /* renamed from: a */
            public int f8636a; // = -1;

            @DexIgnore
            /* renamed from: b */
            public int f8637b; // = -1;

            @DexIgnore
            /* renamed from: c */
            public int f8638c; // = -1;

            @DexIgnore
            /* renamed from: d */
            public boolean f8639d; // = true;

            @DexIgnore
            /* renamed from: e */
            public int f8640e; // = Integer.MAX_VALUE;

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C2723qd.C2729f.C2730a mo15208a(boolean z) {
                this.f8639d = z;
                return this;
            }

            @DexIgnore
            /* renamed from: b */
            public com.fossil.blesdk.obfuscated.C2723qd.C2729f.C2730a mo15210b(int i) {
                if (i >= 1) {
                    this.f8636a = i;
                    return this;
                }
                throw new java.lang.IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            /* renamed from: c */
            public com.fossil.blesdk.obfuscated.C2723qd.C2729f.C2730a mo15211c(int i) {
                this.f8637b = i;
                return this;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C2723qd.C2729f.C2730a mo15207a(int i) {
                this.f8638c = i;
                return this;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C2723qd.C2729f mo15209a() {
                if (this.f8637b < 0) {
                    this.f8637b = this.f8636a;
                }
                if (this.f8638c < 0) {
                    this.f8638c = this.f8636a * 3;
                }
                if (this.f8639d || this.f8637b != 0) {
                    int i = this.f8640e;
                    if (i == Integer.MAX_VALUE || i >= this.f8636a + (this.f8637b * 2)) {
                        com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar = new com.fossil.blesdk.obfuscated.C2723qd.C2729f(this.f8636a, this.f8637b, this.f8639d, this.f8638c, this.f8640e);
                        return fVar;
                    }
                    throw new java.lang.IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.f8636a + ", prefetchDist=" + this.f8637b + ", maxSize=" + this.f8640e);
                }
                throw new java.lang.IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }
        }

        @DexIgnore
        public C2729f(int i, int i2, boolean z, int i3, int i4) {
            this.f8631a = i;
            this.f8632b = i2;
            this.f8633c = z;
            this.f8635e = i3;
            this.f8634d = i4;
        }
    }

    @DexIgnore
    public C2723qd(com.fossil.blesdk.obfuscated.C2871sd<T> sdVar, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, com.fossil.blesdk.obfuscated.C2723qd.C2726c<T> cVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar) {
        this.f8608i = sdVar;
        this.f8604e = executor;
        this.f8605f = executor2;
        this.f8606g = cVar;
        this.f8607h = fVar;
        com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar2 = this.f8607h;
        this.f8611l = (fVar2.f8632b * 2) + fVar2.f8631a;
    }

    @DexIgnore
    /* renamed from: a */
    public static <K, T> com.fossil.blesdk.obfuscated.C2723qd<T> m12764a(com.fossil.blesdk.obfuscated.C2307ld<K, T> ldVar, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, com.fossil.blesdk.obfuscated.C2723qd.C2726c<T> cVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar, K k) {
        int i;
        if (ldVar.isContiguous() || !fVar.f8633c) {
            if (!ldVar.isContiguous()) {
                ldVar = ((com.fossil.blesdk.obfuscated.C3026ud) ldVar).wrapAsContiguousWithoutPlaceholders();
                if (k != null) {
                    i = ((java.lang.Integer) k).intValue();
                    com.fossil.blesdk.obfuscated.C2205kd kdVar = new com.fossil.blesdk.obfuscated.C2205kd((com.fossil.blesdk.obfuscated.C2120jd) ldVar, executor, executor2, cVar, fVar, k, i);
                    return kdVar;
                }
            }
            i = -1;
            com.fossil.blesdk.obfuscated.C2205kd kdVar2 = new com.fossil.blesdk.obfuscated.C2205kd((com.fossil.blesdk.obfuscated.C2120jd) ldVar, executor, executor2, cVar, fVar, k, i);
            return kdVar2;
        }
        com.fossil.blesdk.obfuscated.C3211wd wdVar = new com.fossil.blesdk.obfuscated.C3211wd((com.fossil.blesdk.obfuscated.C3026ud) ldVar, executor, executor2, cVar, fVar, k != null ? ((java.lang.Integer) k).intValue() : 0);
        return wdVar;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo12718a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar);

    @DexIgnore
    /* renamed from: c */
    public void mo15185c() {
        this.f8616q.set(true);
    }

    @DexIgnore
    /* renamed from: d */
    public abstract com.fossil.blesdk.obfuscated.C2307ld<?, T> mo12723d();

    @DexIgnore
    /* renamed from: d */
    public void mo15186d(int i, int i2) {
        if (i2 != 0) {
            for (int size = this.f8617r.size() - 1; size >= 0; size--) {
                com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar = (com.fossil.blesdk.obfuscated.C2723qd.C2728e) this.f8617r.get(size).get();
                if (eVar != null) {
                    eVar.mo11912a(i, i2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public abstract java.lang.Object mo12724e();

    @DexIgnore
    /* renamed from: e */
    public void mo15187e(int i, int i2) {
        if (i2 != 0) {
            for (int size = this.f8617r.size() - 1; size >= 0; size--) {
                com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar = (com.fossil.blesdk.obfuscated.C2723qd.C2728e) this.f8617r.get(size).get();
                if (eVar != null) {
                    eVar.mo11913b(i, i2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public int mo15188f() {
        return this.f8608i.mo15937j();
    }

    @DexIgnore
    /* renamed from: g */
    public void mo15190g(int i) {
        if (i < 0 || i >= size()) {
            throw new java.lang.IndexOutOfBoundsException("Index: " + i + ", Size: " + size());
        }
        this.f8609j = mo15188f() + i;
        mo12727h(i);
        this.f8614o = java.lang.Math.min(this.f8614o, i);
        this.f8615p = java.lang.Math.max(this.f8615p, i);
        mo15182a(true);
    }

    @DexIgnore
    /* renamed from: g */
    public abstract boolean mo12726g();

    @DexIgnore
    public T get(int i) {
        T t = this.f8608i.get(i);
        if (t != null) {
            this.f8610k = t;
        }
        return t;
    }

    @DexIgnore
    /* renamed from: h */
    public abstract void mo12727h(int i);

    @DexIgnore
    /* renamed from: h */
    public boolean mo15192h() {
        return this.f8616q.get();
    }

    @DexIgnore
    /* renamed from: i */
    public void mo15193i(int i) {
        this.f8609j += i;
        this.f8614o += i;
        this.f8615p += i;
    }

    @DexIgnore
    /* renamed from: j */
    public java.util.List<T> mo15195j() {
        if (mo15194i()) {
            return this;
        }
        return new com.fossil.blesdk.obfuscated.C3105vd(this);
    }

    @DexIgnore
    public int size() {
        return this.f8608i.size();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo15189f(int i, int i2) {
        if (i2 != 0) {
            for (int size = this.f8617r.size() - 1; size >= 0; size--) {
                com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar = (com.fossil.blesdk.obfuscated.C2723qd.C2728e) this.f8617r.get(size).get();
                if (eVar != null) {
                    eVar.mo11914c(i, i2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo15194i() {
        return mo15192h();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15184a(boolean z, boolean z2, boolean z3) {
        if (this.f8606g != null) {
            if (this.f8614o == Integer.MAX_VALUE) {
                this.f8614o = this.f8608i.size();
            }
            if (this.f8615p == Integer.MIN_VALUE) {
                this.f8615p = 0;
            }
            if (z || z2 || z3) {
                this.f8604e.execute(new com.fossil.blesdk.obfuscated.C2723qd.C2724a(z, z2, z3));
                return;
            }
            return;
        }
        throw new java.lang.IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15182a(boolean z) {
        boolean z2 = true;
        boolean z3 = this.f8612m && this.f8614o <= this.f8607h.f8632b;
        if (!this.f8613n || this.f8615p < (size() - 1) - this.f8607h.f8632b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.f8612m = false;
            }
            if (z2) {
                this.f8613n = false;
            }
            if (z) {
                this.f8604e.execute(new com.fossil.blesdk.obfuscated.C2723qd.C2725b(z3, z2));
            } else {
                mo15183a(z3, z2);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15183a(boolean z, boolean z2) {
        if (z) {
            this.f8606g.mo15201b(this.f8608i.mo15927c());
        }
        if (z2) {
            this.f8606g.mo15200a(this.f8608i.mo15929d());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15181a(java.util.List<T> list, com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                mo12718a((com.fossil.blesdk.obfuscated.C2723qd) list, eVar);
            } else if (!this.f8608i.isEmpty()) {
                eVar.mo11913b(0, this.f8608i.size());
            }
        }
        for (int size = this.f8617r.size() - 1; size >= 0; size--) {
            if (((com.fossil.blesdk.obfuscated.C2723qd.C2728e) this.f8617r.get(size).get()) == null) {
                this.f8617r.remove(size);
            }
        }
        this.f8617r.add(new java.lang.ref.WeakReference(eVar));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15180a(com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar) {
        for (int size = this.f8617r.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar2 = (com.fossil.blesdk.obfuscated.C2723qd.C2728e) this.f8617r.get(size).get();
            if (eVar2 == null || eVar2 == eVar) {
                this.f8617r.remove(size);
            }
        }
    }
}
