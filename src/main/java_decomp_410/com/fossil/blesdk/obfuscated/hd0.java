package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hd0 extends de0.a<ic0, GoogleSignInOptions> {
    @DexIgnore
    public final /* synthetic */ de0.f a(Context context, Looper looper, kj0 kj0, Object obj, ge0.b bVar, ge0.c cVar) {
        return new ic0(context, looper, kj0, (GoogleSignInOptions) obj, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ List a(Object obj) {
        GoogleSignInOptions googleSignInOptions = (GoogleSignInOptions) obj;
        if (googleSignInOptions == null) {
            return Collections.emptyList();
        }
        return googleSignInOptions.J();
    }
}
