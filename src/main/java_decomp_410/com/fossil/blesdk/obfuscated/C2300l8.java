package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l8 */
public class C2300l8 {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ android.view.View.AccessibilityDelegate f7163b; // = new android.view.View.AccessibilityDelegate();

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.View.AccessibilityDelegate f7164a; // = new com.fossil.blesdk.obfuscated.C2300l8.C2301a(this);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l8$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l8$a */
    public static final class C2301a extends android.view.View.AccessibilityDelegate {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2300l8 f7165a;

        @DexIgnore
        public C2301a(com.fossil.blesdk.obfuscated.C2300l8 l8Var) {
            this.f7165a = l8Var;
        }

        @DexIgnore
        public boolean dispatchPopulateAccessibilityEvent(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            return this.f7165a.mo1799a(view, accessibilityEvent);
        }

        @DexIgnore
        public android.view.accessibility.AccessibilityNodeProvider getAccessibilityNodeProvider(android.view.View view) {
            com.fossil.blesdk.obfuscated.C2791r9 a = this.f7165a.mo11848a(view);
            if (a != null) {
                return (android.view.accessibility.AccessibilityNodeProvider) a.mo15454a();
            }
            return null;
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            this.f7165a.mo1698b(view, accessibilityEvent);
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(android.view.View view, android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo) {
            this.f7165a.mo1696a(view, com.fossil.blesdk.obfuscated.C2711q9.m12653a(accessibilityNodeInfo));
        }

        @DexIgnore
        public void onPopulateAccessibilityEvent(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            this.f7165a.mo13208c(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean onRequestSendAccessibilityEvent(android.view.ViewGroup viewGroup, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            return this.f7165a.mo1800a(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public boolean performAccessibilityAction(android.view.View view, int i, android.os.Bundle bundle) {
            return this.f7165a.mo1697a(view, i, bundle);
        }

        @DexIgnore
        public void sendAccessibilityEvent(android.view.View view, int i) {
            this.f7165a.mo13207a(view, i);
        }

        @DexIgnore
        public void sendAccessibilityEventUnchecked(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            this.f7165a.mo13209d(view, accessibilityEvent);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View.AccessibilityDelegate mo13206a() {
        return this.f7164a;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo1698b(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        f7163b.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo13208c(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        f7163b.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo13209d(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        f7163b.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13207a(android.view.View view, int i) {
        f7163b.sendAccessibilityEvent(view, i);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1799a(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        return f7163b.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1696a(android.view.View view, com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
        f7163b.onInitializeAccessibilityNodeInfo(view, q9Var.mo15121w());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1800a(android.view.ViewGroup viewGroup, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        return f7163b.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2791r9 mo11848a(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT < 16) {
            return null;
        }
        android.view.accessibility.AccessibilityNodeProvider accessibilityNodeProvider = f7163b.getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new com.fossil.blesdk.obfuscated.C2791r9(accessibilityNodeProvider);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1697a(android.view.View view, int i, android.os.Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return f7163b.performAccessibilityAction(view, i, bundle);
        }
        return false;
    }
}
