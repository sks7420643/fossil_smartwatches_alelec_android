package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d63 {
    @DexIgnore
    public /* final */ u63 a;

    @DexIgnore
    public d63(u63 u63) {
        kd4.b(u63, "mMicroAppView");
        this.a = u63;
    }

    @DexIgnore
    public final u63 a() {
        return this.a;
    }
}
