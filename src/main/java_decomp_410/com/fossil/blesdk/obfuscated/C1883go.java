package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.go */
public class C1883go implements com.fossil.blesdk.obfuscated.C1570co {

    @DexIgnore
    /* renamed from: u */
    public static /* final */ java.lang.String f5476u; // = "go";

    @DexIgnore
    /* renamed from: a */
    public int[] f5477a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int[] f5478b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1570co.C1571a f5479c;

    @DexIgnore
    /* renamed from: d */
    public java.nio.ByteBuffer f5480d;

    @DexIgnore
    /* renamed from: e */
    public byte[] f5481e;

    @DexIgnore
    /* renamed from: f */
    public short[] f5482f;

    @DexIgnore
    /* renamed from: g */
    public byte[] f5483g;

    @DexIgnore
    /* renamed from: h */
    public byte[] f5484h;

    @DexIgnore
    /* renamed from: i */
    public byte[] f5485i;

    @DexIgnore
    /* renamed from: j */
    public int[] f5486j;

    @DexIgnore
    /* renamed from: k */
    public int f5487k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1739eo f5488l;

    @DexIgnore
    /* renamed from: m */
    public android.graphics.Bitmap f5489m;

    @DexIgnore
    /* renamed from: n */
    public boolean f5490n;

    @DexIgnore
    /* renamed from: o */
    public int f5491o;

    @DexIgnore
    /* renamed from: p */
    public int f5492p;

    @DexIgnore
    /* renamed from: q */
    public int f5493q;

    @DexIgnore
    /* renamed from: r */
    public int f5494r;

    @DexIgnore
    /* renamed from: s */
    public java.lang.Boolean f5495s;

    @DexIgnore
    /* renamed from: t */
    public android.graphics.Bitmap.Config f5496t;

    @DexIgnore
    public C1883go(com.fossil.blesdk.obfuscated.C1570co.C1571a aVar, com.fossil.blesdk.obfuscated.C1739eo eoVar, java.nio.ByteBuffer byteBuffer, int i) {
        this(aVar);
        mo11276a(eoVar, byteBuffer, i);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11272a(int i) {
        if (i >= 0) {
            com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5488l;
            if (i < eoVar.f4874c) {
                return eoVar.f4876e.get(i).f4467i;
            }
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9591b() {
        this.f5487k = (this.f5487k + 1) % this.f5488l.f4874c;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo9592c() {
        return this.f5488l.f4874c;
    }

    @DexIgnore
    public void clear() {
        this.f5488l = null;
        byte[] bArr = this.f5485i;
        if (bArr != null) {
            this.f5479c.mo9601a(bArr);
        }
        int[] iArr = this.f5486j;
        if (iArr != null) {
            this.f5479c.mo9602a(iArr);
        }
        android.graphics.Bitmap bitmap = this.f5489m;
        if (bitmap != null) {
            this.f5479c.mo9600a(bitmap);
        }
        this.f5489m = null;
        this.f5480d = null;
        this.f5495s = null;
        byte[] bArr2 = this.f5481e;
        if (bArr2 != null) {
            this.f5479c.mo9601a(bArr2);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public int mo9594d() {
        if (this.f5488l.f4874c <= 0) {
            return 0;
        }
        int i = this.f5487k;
        if (i < 0) {
            return 0;
        }
        return mo11272a(i);
    }

    @DexIgnore
    /* renamed from: e */
    public java.nio.ByteBuffer mo9595e() {
        return this.f5480d;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo9596f() {
        this.f5487k = -1;
    }

    @DexIgnore
    /* renamed from: g */
    public int mo9597g() {
        return this.f5487k;
    }

    @DexIgnore
    /* renamed from: h */
    public int mo9598h() {
        return this.f5480d.limit() + this.f5485i.length + (this.f5486j.length * 4);
    }

    @DexIgnore
    /* renamed from: i */
    public final android.graphics.Bitmap mo11279i() {
        java.lang.Boolean bool = this.f5495s;
        android.graphics.Bitmap a = this.f5479c.mo9599a(this.f5494r, this.f5493q, (bool == null || bool.booleanValue()) ? android.graphics.Bitmap.Config.ARGB_8888 : this.f5496t);
        a.setHasAlpha(true);
        return a;
    }

    @DexIgnore
    /* renamed from: j */
    public final int mo11280j() {
        int k = mo11281k();
        if (k <= 0) {
            return k;
        }
        java.nio.ByteBuffer byteBuffer = this.f5480d;
        byteBuffer.get(this.f5481e, 0, java.lang.Math.min(k, byteBuffer.remaining()));
        return k;
    }

    @DexIgnore
    /* renamed from: k */
    public final int mo11281k() {
        return this.f5480d.get() & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo11277b(com.fossil.blesdk.obfuscated.C1646do doVar) {
        com.fossil.blesdk.obfuscated.C1646do doVar2 = doVar;
        int[] iArr = this.f5486j;
        int i = doVar2.f4462d;
        int i2 = doVar2.f4460b;
        int i3 = doVar2.f4461c;
        int i4 = doVar2.f4459a;
        boolean z = this.f5487k == 0;
        int i5 = this.f5494r;
        byte[] bArr = this.f5485i;
        int[] iArr2 = this.f5477a;
        int i6 = 0;
        byte b = -1;
        while (i6 < i) {
            int i7 = (i6 + i2) * i5;
            int i8 = i7 + i4;
            int i9 = i8 + i3;
            int i10 = i7 + i5;
            if (i10 < i9) {
                i9 = i10;
            }
            int i11 = doVar2.f4461c * i6;
            int i12 = i8;
            while (i12 < i9) {
                byte b2 = bArr[i11];
                byte b3 = b2 & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
                if (b3 != b) {
                    int i13 = iArr2[b3];
                    if (i13 != 0) {
                        iArr[i12] = i13;
                    } else {
                        b = b2;
                    }
                }
                i11++;
                i12++;
                com.fossil.blesdk.obfuscated.C1646do doVar3 = doVar;
            }
            i6++;
            doVar2 = doVar;
        }
        java.lang.Boolean bool = this.f5495s;
        this.f5495s = java.lang.Boolean.valueOf((bool != null && bool.booleanValue()) || (this.f5495s == null && z && b != -1));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r24v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v5, resolved type: byte} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r4v16, types: [short] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: c */
    public final void mo11278c(com.fossil.blesdk.obfuscated.C1646do doVar) {
        int i;
        int i2;
        int i3;
        com.fossil.blesdk.obfuscated.C1883go goVar = this;
        com.fossil.blesdk.obfuscated.C1646do doVar2 = doVar;
        if (doVar2 != null) {
            goVar.f5480d.position(doVar2.f4468j);
        }
        if (doVar2 == null) {
            com.fossil.blesdk.obfuscated.C1739eo eoVar = goVar.f5488l;
            i = eoVar.f4877f;
            i2 = eoVar.f4878g;
        } else {
            i = doVar2.f4461c;
            i2 = doVar2.f4462d;
        }
        int i4 = i * i2;
        byte[] bArr = goVar.f5485i;
        if (bArr == null || bArr.length < i4) {
            goVar.f5485i = goVar.f5479c.mo9604b(i4);
        }
        byte[] bArr2 = goVar.f5485i;
        if (goVar.f5482f == null) {
            goVar.f5482f = new short[4096];
        }
        short[] sArr = goVar.f5482f;
        if (goVar.f5483g == null) {
            goVar.f5483g = new byte[4096];
        }
        byte[] bArr3 = goVar.f5483g;
        if (goVar.f5484h == null) {
            goVar.f5484h = new byte[4097];
        }
        byte[] bArr4 = goVar.f5484h;
        int k = mo11281k();
        int i5 = 1 << k;
        int i6 = i5 + 1;
        int i7 = i5 + 2;
        int i8 = k + 1;
        int i9 = (1 << i8) - 1;
        int i10 = 0;
        for (int i11 = 0; i11 < i5; i11++) {
            sArr[i11] = 0;
            bArr3[i11] = (byte) i11;
        }
        byte[] bArr5 = goVar.f5481e;
        int i12 = i8;
        int i13 = i7;
        int i14 = i9;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = -1;
        int i21 = 0;
        int i22 = 0;
        while (true) {
            if (i10 < i4) {
                if (i15 == 0) {
                    i15 = mo11280j();
                    if (i15 <= 0) {
                        goVar.f5491o = 3;
                        break;
                    }
                    i18 = 0;
                }
                i17 += (bArr5[i18] & 255) << i16;
                i18++;
                i15--;
                int i23 = i16 + 8;
                int i24 = i20;
                int i25 = i21;
                int i26 = i13;
                int i27 = i19;
                int i28 = i10;
                int i29 = i12;
                while (true) {
                    if (i23 < i29) {
                        i12 = i29;
                        i21 = i25;
                        i10 = i28;
                        i19 = i27;
                        i16 = i23;
                        i13 = i26;
                        i20 = i24;
                        goVar = this;
                        break;
                    }
                    int i30 = i17 & i14;
                    i17 >>= i29;
                    i23 -= i29;
                    if (i30 == i5) {
                        i29 = i8;
                        i26 = i7;
                        i14 = i9;
                        i24 = -1;
                    } else if (i30 == i6) {
                        i16 = i23;
                        i12 = i29;
                        i10 = i28;
                        i19 = i27;
                        i13 = i26;
                        i21 = i25;
                        i20 = i24;
                        break;
                    } else if (i24 == -1) {
                        bArr2[i27] = bArr3[i30];
                        i27++;
                        i28++;
                        goVar = this;
                        i24 = i30;
                        i25 = i24;
                    } else {
                        int i31 = i26;
                        int i32 = i23;
                        if (i30 >= i31) {
                            bArr4[i22] = (byte) i25;
                            i22++;
                            i3 = i24;
                        } else {
                            i3 = i30;
                        }
                        while (i3 >= i5) {
                            bArr4[i22] = bArr3[i3];
                            i22++;
                            i3 = sArr[i3];
                        }
                        int i33 = bArr3[i3] & 255;
                        int i34 = i8;
                        byte b = (byte) i33;
                        bArr2[i27] = b;
                        while (true) {
                            i27++;
                            i28++;
                            if (i22 <= 0) {
                                break;
                            }
                            i22--;
                            bArr2[i27] = bArr4[i22];
                        }
                        int i35 = i33;
                        if (i31 < 4096) {
                            sArr[i31] = (short) i24;
                            bArr3[i31] = b;
                            i31++;
                            if ((i31 & i14) == 0 && i31 < 4096) {
                                i29++;
                                i14 += i31;
                            }
                        }
                        i24 = i30;
                        i23 = i32;
                        i8 = i34;
                        i25 = i35;
                        i26 = i31;
                        goVar = this;
                    }
                }
            } else {
                break;
            }
        }
        java.util.Arrays.fill(bArr2, i19, i4, (byte) 0);
    }

    @DexIgnore
    public C1883go(com.fossil.blesdk.obfuscated.C1570co.C1571a aVar) {
        this.f5478b = new int[256];
        this.f5496t = android.graphics.Bitmap.Config.ARGB_8888;
        this.f5479c = aVar;
        this.f5488l = new com.fossil.blesdk.obfuscated.C1739eo();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00f7, code lost:
        return null;
     */
    @DexIgnore
    /* renamed from: a */
    public synchronized android.graphics.Bitmap mo9589a() {
        if (this.f5488l.f4874c <= 0 || this.f5487k < 0) {
            if (android.util.Log.isLoggable(f5476u, 3)) {
                java.lang.String str = f5476u;
                android.util.Log.d(str, "Unable to decode frame, frameCount=" + this.f5488l.f4874c + ", framePointer=" + this.f5487k);
            }
            this.f5491o = 1;
        }
        if (this.f5491o != 1) {
            if (this.f5491o != 2) {
                this.f5491o = 0;
                if (this.f5481e == null) {
                    this.f5481e = this.f5479c.mo9604b(255);
                }
                com.fossil.blesdk.obfuscated.C1646do doVar = this.f5488l.f4876e.get(this.f5487k);
                int i = this.f5487k - 1;
                com.fossil.blesdk.obfuscated.C1646do doVar2 = i >= 0 ? this.f5488l.f4876e.get(i) : null;
                this.f5477a = doVar.f4469k != null ? doVar.f4469k : this.f5488l.f4872a;
                if (this.f5477a == null) {
                    if (android.util.Log.isLoggable(f5476u, 3)) {
                        java.lang.String str2 = f5476u;
                        android.util.Log.d(str2, "No valid color table found for frame #" + this.f5487k);
                    }
                    this.f5491o = 1;
                    return null;
                }
                if (doVar.f4464f) {
                    java.lang.System.arraycopy(this.f5477a, 0, this.f5478b, 0, this.f5477a.length);
                    this.f5477a = this.f5478b;
                    this.f5477a[doVar.f4466h] = 0;
                    if (doVar.f4465g == 2 && this.f5487k == 0) {
                        this.f5495s = true;
                    }
                }
                return mo11274a(doVar, doVar2);
            }
        }
        if (android.util.Log.isLoggable(f5476u, 3)) {
            java.lang.String str3 = f5476u;
            android.util.Log.d(str3, "Unable to decode frame, status=" + this.f5491o);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo11276a(com.fossil.blesdk.obfuscated.C1739eo eoVar, java.nio.ByteBuffer byteBuffer, int i) {
        if (i > 0) {
            int highestOneBit = java.lang.Integer.highestOneBit(i);
            this.f5491o = 0;
            this.f5488l = eoVar;
            this.f5487k = -1;
            this.f5480d = byteBuffer.asReadOnlyBuffer();
            this.f5480d.position(0);
            this.f5480d.order(java.nio.ByteOrder.LITTLE_ENDIAN);
            this.f5490n = false;
            java.util.Iterator<com.fossil.blesdk.obfuscated.C1646do> it = eoVar.f4876e.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().f4465g == 3) {
                        this.f5490n = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.f5492p = highestOneBit;
            this.f5494r = eoVar.f4877f / highestOneBit;
            this.f5493q = eoVar.f4878g / highestOneBit;
            this.f5485i = this.f5479c.mo9604b(eoVar.f4877f * eoVar.f4878g);
            this.f5486j = this.f5479c.mo9603a(this.f5494r * this.f5493q);
        } else {
            throw new java.lang.IllegalArgumentException("Sample size must be >=0, not: " + i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9590a(android.graphics.Bitmap.Config config) {
        if (config == android.graphics.Bitmap.Config.ARGB_8888 || config == android.graphics.Bitmap.Config.RGB_565) {
            this.f5496t = config;
            return;
        }
        throw new java.lang.IllegalArgumentException("Unsupported format: " + config + ", must be one of " + android.graphics.Bitmap.Config.ARGB_8888 + " or " + android.graphics.Bitmap.Config.RGB_565);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.Bitmap mo11274a(com.fossil.blesdk.obfuscated.C1646do doVar, com.fossil.blesdk.obfuscated.C1646do doVar2) {
        int[] iArr = this.f5486j;
        int i = 0;
        if (doVar2 == null) {
            android.graphics.Bitmap bitmap = this.f5489m;
            if (bitmap != null) {
                this.f5479c.mo9600a(bitmap);
            }
            this.f5489m = null;
            java.util.Arrays.fill(iArr, 0);
        }
        if (doVar2 != null && doVar2.f4465g == 3 && this.f5489m == null) {
            java.util.Arrays.fill(iArr, 0);
        }
        if (doVar2 != null) {
            int i2 = doVar2.f4465g;
            if (i2 > 0) {
                if (i2 == 2) {
                    if (!doVar.f4464f) {
                        com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5488l;
                        int i3 = eoVar.f4883l;
                        if (doVar.f4469k == null || eoVar.f4881j != doVar.f4466h) {
                            i = i3;
                        }
                    }
                    int i4 = doVar2.f4462d;
                    int i5 = this.f5492p;
                    int i6 = i4 / i5;
                    int i7 = doVar2.f4460b / i5;
                    int i8 = doVar2.f4461c / i5;
                    int i9 = doVar2.f4459a / i5;
                    int i10 = this.f5494r;
                    int i11 = (i7 * i10) + i9;
                    int i12 = (i6 * i10) + i11;
                    while (i11 < i12) {
                        int i13 = i11 + i8;
                        for (int i14 = i11; i14 < i13; i14++) {
                            iArr[i14] = i;
                        }
                        i11 += this.f5494r;
                    }
                } else if (i2 == 3) {
                    android.graphics.Bitmap bitmap2 = this.f5489m;
                    if (bitmap2 != null) {
                        int i15 = this.f5494r;
                        bitmap2.getPixels(iArr, 0, i15, 0, 0, i15, this.f5493q);
                    }
                }
            }
        }
        mo11278c(doVar);
        if (doVar.f4463e || this.f5492p != 1) {
            mo11275a(doVar);
        } else {
            mo11277b(doVar);
        }
        if (this.f5490n) {
            int i16 = doVar.f4465g;
            if (i16 == 0 || i16 == 1) {
                if (this.f5489m == null) {
                    this.f5489m = mo11279i();
                }
                android.graphics.Bitmap bitmap3 = this.f5489m;
                int i17 = this.f5494r;
                bitmap3.setPixels(iArr, 0, i17, 0, 0, i17, this.f5493q);
            }
        }
        android.graphics.Bitmap i18 = mo11279i();
        int i19 = this.f5494r;
        i18.setPixels(iArr, 0, i19, 0, 0, i19, this.f5493q);
        return i18;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11275a(com.fossil.blesdk.obfuscated.C1646do doVar) {
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        com.fossil.blesdk.obfuscated.C1646do doVar2 = doVar;
        int[] iArr = this.f5486j;
        int i6 = doVar2.f4462d;
        int i7 = this.f5492p;
        int i8 = i6 / i7;
        int i9 = doVar2.f4460b / i7;
        int i10 = doVar2.f4461c / i7;
        int i11 = doVar2.f4459a / i7;
        boolean z2 = true;
        boolean z3 = this.f5487k == 0;
        int i12 = this.f5492p;
        int i13 = this.f5494r;
        int i14 = this.f5493q;
        byte[] bArr = this.f5485i;
        int[] iArr2 = this.f5477a;
        java.lang.Boolean bool = this.f5495s;
        int i15 = 0;
        int i16 = 0;
        int i17 = 1;
        int i18 = 8;
        while (i15 < i8) {
            java.lang.Boolean bool2 = z2;
            if (doVar2.f4463e) {
                if (i16 >= i8) {
                    i = i8;
                    i5 = i17 + 1;
                    if (i5 == 2) {
                        i16 = 4;
                    } else if (i5 == 3) {
                        i16 = 2;
                        i18 = 4;
                    } else if (i5 == 4) {
                        i16 = 1;
                        i18 = 2;
                    }
                } else {
                    i = i8;
                    i5 = i17;
                }
                i2 = i16 + i18;
                i17 = i5;
            } else {
                i = i8;
                i2 = i16;
                i16 = i15;
            }
            int i19 = i16 + i9;
            boolean z4 = i12 == 1;
            if (i19 < i14) {
                int i20 = i19 * i13;
                int i21 = i20 + i11;
                int i22 = i21 + i10;
                int i23 = i20 + i13;
                if (i23 < i22) {
                    i22 = i23;
                }
                i3 = i9;
                int i24 = i15 * i12 * doVar2.f4461c;
                if (z4) {
                    int i25 = i21;
                    while (i25 < i22) {
                        int i26 = i10;
                        int i27 = iArr2[bArr[i24] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX];
                        if (i27 != 0) {
                            iArr[i25] = i27;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i24 += i12;
                        i25++;
                        i10 = i26;
                    }
                } else {
                    i4 = i10;
                    int i28 = ((i22 - i21) * i12) + i24;
                    int i29 = i21;
                    while (i29 < i22) {
                        int i30 = i22;
                        int a = mo11273a(i24, i28, doVar2.f4461c);
                        if (a != 0) {
                            iArr[i29] = a;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i24 += i12;
                        i29++;
                        i22 = i30;
                    }
                    i15++;
                    i16 = i2;
                    i10 = i4;
                    z2 = bool2;
                    i8 = i;
                    i9 = i3;
                }
            } else {
                i3 = i9;
            }
            i4 = i10;
            i15++;
            i16 = i2;
            i10 = i4;
            z2 = bool2;
            i8 = i;
            i9 = i3;
        }
        if (this.f5495s == null) {
            if (bool == null) {
                z = false;
            } else {
                z = bool.booleanValue();
            }
            this.f5495s = java.lang.Boolean.valueOf(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo11273a(int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = i; i9 < this.f5492p + i; i9++) {
            byte[] bArr = this.f5485i;
            if (i9 >= bArr.length || i9 >= i2) {
                break;
            }
            int i10 = this.f5477a[bArr[i9] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX];
            if (i10 != 0) {
                i4 += (i10 >> 24) & 255;
                i5 += (i10 >> 16) & 255;
                i6 += (i10 >> 8) & 255;
                i7 += i10 & 255;
                i8++;
            }
        }
        int i11 = i + i3;
        for (int i12 = i11; i12 < this.f5492p + i11; i12++) {
            byte[] bArr2 = this.f5485i;
            if (i12 >= bArr2.length || i12 >= i2) {
                break;
            }
            int i13 = this.f5477a[bArr2[i12] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX];
            if (i13 != 0) {
                i4 += (i13 >> 24) & 255;
                i5 += (i13 >> 16) & 255;
                i6 += (i13 >> 8) & 255;
                i7 += i13 & 255;
                i8++;
            }
        }
        if (i8 == 0) {
            return 0;
        }
        return ((i4 / i8) << 24) | ((i5 / i8) << 16) | ((i6 / i8) << 8) | (i7 / i8);
    }
}
