package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fj2 extends cj2 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public fj2(String str, boolean z) {
        this.b = str;
        this.a = z;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }
}
