package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.Xml;
import android.view.View;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintHelper;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;
import androidx.constraintlayout.widget.Guideline;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j5 {
    @DexIgnore
    public static /* final */ int[] b; // = {0, 4, 8};
    @DexIgnore
    public static SparseIntArray c; // = new SparseIntArray();
    @DexIgnore
    public HashMap<Integer, b> a; // = new HashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public int A;
        @DexIgnore
        public int B;
        @DexIgnore
        public int C;
        @DexIgnore
        public int D;
        @DexIgnore
        public int E;
        @DexIgnore
        public int F;
        @DexIgnore
        public int G;
        @DexIgnore
        public int H;
        @DexIgnore
        public int I;
        @DexIgnore
        public int J;
        @DexIgnore
        public int K;
        @DexIgnore
        public int L;
        @DexIgnore
        public int M;
        @DexIgnore
        public int N;
        @DexIgnore
        public int O;
        @DexIgnore
        public int P;
        @DexIgnore
        public float Q;
        @DexIgnore
        public float R;
        @DexIgnore
        public int S;
        @DexIgnore
        public int T;
        @DexIgnore
        public float U;
        @DexIgnore
        public boolean V;
        @DexIgnore
        public float W;
        @DexIgnore
        public float X;
        @DexIgnore
        public float Y;
        @DexIgnore
        public float Z;
        @DexIgnore
        public boolean a;
        @DexIgnore
        public float a0;
        @DexIgnore
        public int b;
        @DexIgnore
        public float b0;
        @DexIgnore
        public int c;
        @DexIgnore
        public float c0;
        @DexIgnore
        public int d;
        @DexIgnore
        public float d0;
        @DexIgnore
        public int e;
        @DexIgnore
        public float e0;
        @DexIgnore
        public int f;
        @DexIgnore
        public float f0;
        @DexIgnore
        public float g;
        @DexIgnore
        public float g0;
        @DexIgnore
        public int h;
        @DexIgnore
        public boolean h0;
        @DexIgnore
        public int i;
        @DexIgnore
        public boolean i0;
        @DexIgnore
        public int j;
        @DexIgnore
        public int j0;
        @DexIgnore
        public int k;
        @DexIgnore
        public int k0;
        @DexIgnore
        public int l;
        @DexIgnore
        public int l0;
        @DexIgnore
        public int m;
        @DexIgnore
        public int m0;
        @DexIgnore
        public int n;
        @DexIgnore
        public int n0;
        @DexIgnore
        public int o;
        @DexIgnore
        public int o0;
        @DexIgnore
        public int p;
        @DexIgnore
        public float p0;
        @DexIgnore
        public int q;
        @DexIgnore
        public float q0;
        @DexIgnore
        public int r;
        @DexIgnore
        public boolean r0;
        @DexIgnore
        public int s;
        @DexIgnore
        public int s0;
        @DexIgnore
        public int t;
        @DexIgnore
        public int t0;
        @DexIgnore
        public float u;
        @DexIgnore
        public int[] u0;
        @DexIgnore
        public float v;
        @DexIgnore
        public String v0;
        @DexIgnore
        public String w;
        @DexIgnore
        public int x;
        @DexIgnore
        public int y;
        @DexIgnore
        public float z;

        @DexIgnore
        public b() {
            this.a = false;
            this.e = -1;
            this.f = -1;
            this.g = -1.0f;
            this.h = -1;
            this.i = -1;
            this.j = -1;
            this.k = -1;
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = -1;
            this.q = -1;
            this.r = -1;
            this.s = -1;
            this.t = -1;
            this.u = 0.5f;
            this.v = 0.5f;
            this.w = null;
            this.x = -1;
            this.y = 0;
            this.z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.A = -1;
            this.B = -1;
            this.C = -1;
            this.D = -1;
            this.E = -1;
            this.F = -1;
            this.G = -1;
            this.H = -1;
            this.I = -1;
            this.J = 0;
            this.K = -1;
            this.L = -1;
            this.M = -1;
            this.N = -1;
            this.O = -1;
            this.P = -1;
            this.Q = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.R = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.S = 0;
            this.T = 0;
            this.U = 1.0f;
            this.V = false;
            this.W = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.X = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.Y = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.Z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.a0 = 1.0f;
            this.b0 = 1.0f;
            this.c0 = Float.NaN;
            this.d0 = Float.NaN;
            this.e0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.g0 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.h0 = false;
            this.i0 = false;
            this.j0 = 0;
            this.k0 = 0;
            this.l0 = -1;
            this.m0 = -1;
            this.n0 = -1;
            this.o0 = -1;
            this.p0 = 1.0f;
            this.q0 = 1.0f;
            this.r0 = false;
            this.s0 = -1;
            this.t0 = -1;
        }

        @DexIgnore
        public b clone() {
            b bVar = new b();
            bVar.a = this.a;
            bVar.b = this.b;
            bVar.c = this.c;
            bVar.e = this.e;
            bVar.f = this.f;
            bVar.g = this.g;
            bVar.h = this.h;
            bVar.i = this.i;
            bVar.j = this.j;
            bVar.k = this.k;
            bVar.l = this.l;
            bVar.m = this.m;
            bVar.n = this.n;
            bVar.o = this.o;
            bVar.p = this.p;
            bVar.q = this.q;
            bVar.r = this.r;
            bVar.s = this.s;
            bVar.t = this.t;
            bVar.u = this.u;
            bVar.v = this.v;
            bVar.w = this.w;
            bVar.A = this.A;
            bVar.B = this.B;
            bVar.u = this.u;
            bVar.u = this.u;
            bVar.u = this.u;
            bVar.u = this.u;
            bVar.u = this.u;
            bVar.C = this.C;
            bVar.D = this.D;
            bVar.E = this.E;
            bVar.F = this.F;
            bVar.G = this.G;
            bVar.H = this.H;
            bVar.I = this.I;
            bVar.J = this.J;
            bVar.K = this.K;
            bVar.L = this.L;
            bVar.M = this.M;
            bVar.N = this.N;
            bVar.O = this.O;
            bVar.P = this.P;
            bVar.Q = this.Q;
            bVar.R = this.R;
            bVar.S = this.S;
            bVar.T = this.T;
            bVar.U = this.U;
            bVar.V = this.V;
            bVar.W = this.W;
            bVar.X = this.X;
            bVar.Y = this.Y;
            bVar.Z = this.Z;
            bVar.a0 = this.a0;
            bVar.b0 = this.b0;
            bVar.c0 = this.c0;
            bVar.d0 = this.d0;
            bVar.e0 = this.e0;
            bVar.f0 = this.f0;
            bVar.g0 = this.g0;
            bVar.h0 = this.h0;
            bVar.i0 = this.i0;
            bVar.j0 = this.j0;
            bVar.k0 = this.k0;
            bVar.l0 = this.l0;
            bVar.m0 = this.m0;
            bVar.n0 = this.n0;
            bVar.o0 = this.o0;
            bVar.p0 = this.p0;
            bVar.q0 = this.q0;
            bVar.s0 = this.s0;
            bVar.t0 = this.t0;
            int[] iArr = this.u0;
            if (iArr != null) {
                bVar.u0 = Arrays.copyOf(iArr, iArr.length);
            }
            bVar.x = this.x;
            bVar.y = this.y;
            bVar.z = this.z;
            bVar.r0 = this.r0;
            return bVar;
        }

        @DexIgnore
        public final void a(ConstraintHelper constraintHelper, int i2, Constraints.LayoutParams layoutParams) {
            a(i2, layoutParams);
            if (constraintHelper instanceof Barrier) {
                this.t0 = 1;
                Barrier barrier = (Barrier) constraintHelper;
                this.s0 = barrier.getType();
                this.u0 = barrier.getReferencedIds();
            }
        }

        @DexIgnore
        public final void a(int i2, Constraints.LayoutParams layoutParams) {
            a(i2, (ConstraintLayout.LayoutParams) layoutParams);
            this.U = layoutParams.m0;
            this.X = layoutParams.p0;
            this.Y = layoutParams.q0;
            this.Z = layoutParams.r0;
            this.a0 = layoutParams.s0;
            this.b0 = layoutParams.t0;
            this.c0 = layoutParams.u0;
            this.d0 = layoutParams.v0;
            this.e0 = layoutParams.w0;
            this.f0 = layoutParams.x0;
            this.g0 = layoutParams.y0;
            this.W = layoutParams.o0;
            this.V = layoutParams.n0;
        }

        @DexIgnore
        public final void a(int i2, ConstraintLayout.LayoutParams layoutParams) {
            this.d = i2;
            this.h = layoutParams.d;
            this.i = layoutParams.e;
            this.j = layoutParams.f;
            this.k = layoutParams.g;
            this.l = layoutParams.h;
            this.m = layoutParams.i;
            this.n = layoutParams.j;
            this.o = layoutParams.k;
            this.p = layoutParams.l;
            this.q = layoutParams.p;
            this.r = layoutParams.q;
            this.s = layoutParams.r;
            this.t = layoutParams.s;
            this.u = layoutParams.z;
            this.v = layoutParams.A;
            this.w = layoutParams.B;
            this.x = layoutParams.m;
            this.y = layoutParams.n;
            this.z = layoutParams.o;
            this.A = layoutParams.P;
            this.B = layoutParams.Q;
            this.C = layoutParams.R;
            this.g = layoutParams.c;
            this.e = layoutParams.a;
            this.f = layoutParams.b;
            this.b = layoutParams.width;
            this.c = layoutParams.height;
            this.D = layoutParams.leftMargin;
            this.E = layoutParams.rightMargin;
            this.F = layoutParams.topMargin;
            this.G = layoutParams.bottomMargin;
            this.Q = layoutParams.E;
            this.R = layoutParams.D;
            this.T = layoutParams.G;
            this.S = layoutParams.F;
            boolean z2 = layoutParams.S;
            this.h0 = z2;
            this.i0 = layoutParams.T;
            this.j0 = layoutParams.H;
            this.k0 = layoutParams.I;
            this.h0 = z2;
            this.l0 = layoutParams.L;
            this.m0 = layoutParams.M;
            this.n0 = layoutParams.J;
            this.o0 = layoutParams.K;
            this.p0 = layoutParams.N;
            this.q0 = layoutParams.O;
            if (Build.VERSION.SDK_INT >= 17) {
                this.H = layoutParams.getMarginEnd();
                this.I = layoutParams.getMarginStart();
            }
        }

        @DexIgnore
        public void a(ConstraintLayout.LayoutParams layoutParams) {
            layoutParams.d = this.h;
            layoutParams.e = this.i;
            layoutParams.f = this.j;
            layoutParams.g = this.k;
            layoutParams.h = this.l;
            layoutParams.i = this.m;
            layoutParams.j = this.n;
            layoutParams.k = this.o;
            layoutParams.l = this.p;
            layoutParams.p = this.q;
            layoutParams.q = this.r;
            layoutParams.r = this.s;
            layoutParams.s = this.t;
            layoutParams.leftMargin = this.D;
            layoutParams.rightMargin = this.E;
            layoutParams.topMargin = this.F;
            layoutParams.bottomMargin = this.G;
            layoutParams.x = this.P;
            layoutParams.y = this.O;
            layoutParams.z = this.u;
            layoutParams.A = this.v;
            layoutParams.m = this.x;
            layoutParams.n = this.y;
            layoutParams.o = this.z;
            layoutParams.B = this.w;
            layoutParams.P = this.A;
            layoutParams.Q = this.B;
            layoutParams.E = this.Q;
            layoutParams.D = this.R;
            layoutParams.G = this.T;
            layoutParams.F = this.S;
            layoutParams.S = this.h0;
            layoutParams.T = this.i0;
            layoutParams.H = this.j0;
            layoutParams.I = this.k0;
            layoutParams.L = this.l0;
            layoutParams.M = this.m0;
            layoutParams.J = this.n0;
            layoutParams.K = this.o0;
            layoutParams.N = this.p0;
            layoutParams.O = this.q0;
            layoutParams.R = this.C;
            layoutParams.c = this.g;
            layoutParams.a = this.e;
            layoutParams.b = this.f;
            layoutParams.width = this.b;
            layoutParams.height = this.c;
            if (Build.VERSION.SDK_INT >= 17) {
                layoutParams.setMarginStart(this.I);
                layoutParams.setMarginEnd(this.H);
            }
            layoutParams.a();
        }
    }

    /*
    static {
        c.append(l5.ConstraintSet_layout_constraintLeft_toLeftOf, 25);
        c.append(l5.ConstraintSet_layout_constraintLeft_toRightOf, 26);
        c.append(l5.ConstraintSet_layout_constraintRight_toLeftOf, 29);
        c.append(l5.ConstraintSet_layout_constraintRight_toRightOf, 30);
        c.append(l5.ConstraintSet_layout_constraintTop_toTopOf, 36);
        c.append(l5.ConstraintSet_layout_constraintTop_toBottomOf, 35);
        c.append(l5.ConstraintSet_layout_constraintBottom_toTopOf, 4);
        c.append(l5.ConstraintSet_layout_constraintBottom_toBottomOf, 3);
        c.append(l5.ConstraintSet_layout_constraintBaseline_toBaselineOf, 1);
        c.append(l5.ConstraintSet_layout_editor_absoluteX, 6);
        c.append(l5.ConstraintSet_layout_editor_absoluteY, 7);
        c.append(l5.ConstraintSet_layout_constraintGuide_begin, 17);
        c.append(l5.ConstraintSet_layout_constraintGuide_end, 18);
        c.append(l5.ConstraintSet_layout_constraintGuide_percent, 19);
        c.append(l5.ConstraintSet_android_orientation, 27);
        c.append(l5.ConstraintSet_layout_constraintStart_toEndOf, 32);
        c.append(l5.ConstraintSet_layout_constraintStart_toStartOf, 33);
        c.append(l5.ConstraintSet_layout_constraintEnd_toStartOf, 10);
        c.append(l5.ConstraintSet_layout_constraintEnd_toEndOf, 9);
        c.append(l5.ConstraintSet_layout_goneMarginLeft, 13);
        c.append(l5.ConstraintSet_layout_goneMarginTop, 16);
        c.append(l5.ConstraintSet_layout_goneMarginRight, 14);
        c.append(l5.ConstraintSet_layout_goneMarginBottom, 11);
        c.append(l5.ConstraintSet_layout_goneMarginStart, 15);
        c.append(l5.ConstraintSet_layout_goneMarginEnd, 12);
        c.append(l5.ConstraintSet_layout_constraintVertical_weight, 40);
        c.append(l5.ConstraintSet_layout_constraintHorizontal_weight, 39);
        c.append(l5.ConstraintSet_layout_constraintHorizontal_chainStyle, 41);
        c.append(l5.ConstraintSet_layout_constraintVertical_chainStyle, 42);
        c.append(l5.ConstraintSet_layout_constraintHorizontal_bias, 20);
        c.append(l5.ConstraintSet_layout_constraintVertical_bias, 37);
        c.append(l5.ConstraintSet_layout_constraintDimensionRatio, 5);
        c.append(l5.ConstraintSet_layout_constraintLeft_creator, 75);
        c.append(l5.ConstraintSet_layout_constraintTop_creator, 75);
        c.append(l5.ConstraintSet_layout_constraintRight_creator, 75);
        c.append(l5.ConstraintSet_layout_constraintBottom_creator, 75);
        c.append(l5.ConstraintSet_layout_constraintBaseline_creator, 75);
        c.append(l5.ConstraintSet_android_layout_marginLeft, 24);
        c.append(l5.ConstraintSet_android_layout_marginRight, 28);
        c.append(l5.ConstraintSet_android_layout_marginStart, 31);
        c.append(l5.ConstraintSet_android_layout_marginEnd, 8);
        c.append(l5.ConstraintSet_android_layout_marginTop, 34);
        c.append(l5.ConstraintSet_android_layout_marginBottom, 2);
        c.append(l5.ConstraintSet_android_layout_width, 23);
        c.append(l5.ConstraintSet_android_layout_height, 21);
        c.append(l5.ConstraintSet_android_visibility, 22);
        c.append(l5.ConstraintSet_android_alpha, 43);
        c.append(l5.ConstraintSet_android_elevation, 44);
        c.append(l5.ConstraintSet_android_rotationX, 45);
        c.append(l5.ConstraintSet_android_rotationY, 46);
        c.append(l5.ConstraintSet_android_rotation, 60);
        c.append(l5.ConstraintSet_android_scaleX, 47);
        c.append(l5.ConstraintSet_android_scaleY, 48);
        c.append(l5.ConstraintSet_android_transformPivotX, 49);
        c.append(l5.ConstraintSet_android_transformPivotY, 50);
        c.append(l5.ConstraintSet_android_translationX, 51);
        c.append(l5.ConstraintSet_android_translationY, 52);
        c.append(l5.ConstraintSet_android_translationZ, 53);
        c.append(l5.ConstraintSet_layout_constraintWidth_default, 54);
        c.append(l5.ConstraintSet_layout_constraintHeight_default, 55);
        c.append(l5.ConstraintSet_layout_constraintWidth_max, 56);
        c.append(l5.ConstraintSet_layout_constraintHeight_max, 57);
        c.append(l5.ConstraintSet_layout_constraintWidth_min, 58);
        c.append(l5.ConstraintSet_layout_constraintHeight_min, 59);
        c.append(l5.ConstraintSet_layout_constraintCircle, 61);
        c.append(l5.ConstraintSet_layout_constraintCircleRadius, 62);
        c.append(l5.ConstraintSet_layout_constraintCircleAngle, 63);
        c.append(l5.ConstraintSet_android_id, 38);
        c.append(l5.ConstraintSet_layout_constraintWidth_percent, 69);
        c.append(l5.ConstraintSet_layout_constraintHeight_percent, 70);
        c.append(l5.ConstraintSet_chainUseRtl, 71);
        c.append(l5.ConstraintSet_barrierDirection, 72);
        c.append(l5.ConstraintSet_constraint_referenced_ids, 73);
        c.append(l5.ConstraintSet_barrierAllowsGoneWidgets, 74);
    }
    */

    @DexIgnore
    public void a(Constraints constraints) {
        int childCount = constraints.getChildCount();
        this.a.clear();
        int i = 0;
        while (i < childCount) {
            View childAt = constraints.getChildAt(i);
            Constraints.LayoutParams layoutParams = (Constraints.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.a.containsKey(Integer.valueOf(id))) {
                    this.a.put(Integer.valueOf(id), new b());
                }
                b bVar = this.a.get(Integer.valueOf(id));
                if (childAt instanceof ConstraintHelper) {
                    bVar.a((ConstraintHelper) childAt, id, layoutParams);
                }
                bVar.a(id, layoutParams);
                i++;
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    public final String b(int i) {
        switch (i) {
            case 1:
                return ViewHierarchy.DIMENSION_LEFT_KEY;
            case 2:
                return "right";
            case 3:
                return ViewHierarchy.DIMENSION_TOP_KEY;
            case 4:
                return "bottom";
            case 5:
                return "baseline";
            case 6:
                return VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE;
            case 7:
                return "end";
            default:
                return "undefined";
        }
    }

    @DexIgnore
    public void b(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        HashSet hashSet = new HashSet(this.a.keySet());
        int i = 0;
        while (i < childCount) {
            View childAt = constraintLayout.getChildAt(i);
            int id = childAt.getId();
            if (id != -1) {
                if (this.a.containsKey(Integer.valueOf(id))) {
                    hashSet.remove(Integer.valueOf(id));
                    b bVar = this.a.get(Integer.valueOf(id));
                    if (childAt instanceof Barrier) {
                        bVar.t0 = 1;
                    }
                    int i2 = bVar.t0;
                    if (i2 != -1 && i2 == 1) {
                        Barrier barrier = (Barrier) childAt;
                        barrier.setId(id);
                        barrier.setType(bVar.s0);
                        barrier.setAllowsGoneWidget(bVar.r0);
                        int[] iArr = bVar.u0;
                        if (iArr != null) {
                            barrier.setReferencedIds(iArr);
                        } else {
                            String str = bVar.v0;
                            if (str != null) {
                                bVar.u0 = a((View) barrier, str);
                                barrier.setReferencedIds(bVar.u0);
                            }
                        }
                    }
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
                    bVar.a(layoutParams);
                    childAt.setLayoutParams(layoutParams);
                    childAt.setVisibility(bVar.J);
                    if (Build.VERSION.SDK_INT >= 17) {
                        childAt.setAlpha(bVar.U);
                        childAt.setRotation(bVar.X);
                        childAt.setRotationX(bVar.Y);
                        childAt.setRotationY(bVar.Z);
                        childAt.setScaleX(bVar.a0);
                        childAt.setScaleY(bVar.b0);
                        if (!Float.isNaN(bVar.c0)) {
                            childAt.setPivotX(bVar.c0);
                        }
                        if (!Float.isNaN(bVar.d0)) {
                            childAt.setPivotY(bVar.d0);
                        }
                        childAt.setTranslationX(bVar.e0);
                        childAt.setTranslationY(bVar.f0);
                        if (Build.VERSION.SDK_INT >= 21) {
                            childAt.setTranslationZ(bVar.g0);
                            if (bVar.V) {
                                childAt.setElevation(bVar.W);
                            }
                        }
                    }
                }
                i++;
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            Integer num = (Integer) it.next();
            b bVar2 = this.a.get(num);
            int i3 = bVar2.t0;
            if (i3 != -1 && i3 == 1) {
                Barrier barrier2 = new Barrier(constraintLayout.getContext());
                barrier2.setId(num.intValue());
                int[] iArr2 = bVar2.u0;
                if (iArr2 != null) {
                    barrier2.setReferencedIds(iArr2);
                } else {
                    String str2 = bVar2.v0;
                    if (str2 != null) {
                        bVar2.u0 = a((View) barrier2, str2);
                        barrier2.setReferencedIds(bVar2.u0);
                    }
                }
                barrier2.setType(bVar2.s0);
                ConstraintLayout.LayoutParams generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                barrier2.a();
                bVar2.a(generateDefaultLayoutParams);
                constraintLayout.addView(barrier2, generateDefaultLayoutParams);
            }
            if (bVar2.a) {
                Guideline guideline = new Guideline(constraintLayout.getContext());
                guideline.setId(num.intValue());
                ConstraintLayout.LayoutParams generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
                bVar2.a(generateDefaultLayoutParams2);
                constraintLayout.addView(guideline, generateDefaultLayoutParams2);
            }
        }
    }

    @DexIgnore
    public void c(ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        this.a.clear();
        int i = 0;
        while (i < childCount) {
            View childAt = constraintLayout.getChildAt(i);
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.a.containsKey(Integer.valueOf(id))) {
                    this.a.put(Integer.valueOf(id), new b());
                }
                b bVar = this.a.get(Integer.valueOf(id));
                bVar.a(id, layoutParams);
                bVar.J = childAt.getVisibility();
                if (Build.VERSION.SDK_INT >= 17) {
                    bVar.U = childAt.getAlpha();
                    bVar.X = childAt.getRotation();
                    bVar.Y = childAt.getRotationX();
                    bVar.Z = childAt.getRotationY();
                    bVar.a0 = childAt.getScaleX();
                    bVar.b0 = childAt.getScaleY();
                    float pivotX = childAt.getPivotX();
                    float pivotY = childAt.getPivotY();
                    if (!(((double) pivotX) == 0.0d && ((double) pivotY) == 0.0d)) {
                        bVar.c0 = pivotX;
                        bVar.d0 = pivotY;
                    }
                    bVar.e0 = childAt.getTranslationX();
                    bVar.f0 = childAt.getTranslationY();
                    if (Build.VERSION.SDK_INT >= 21) {
                        bVar.g0 = childAt.getTranslationZ();
                        if (bVar.V) {
                            bVar.W = childAt.getElevation();
                        }
                    }
                }
                if (childAt instanceof Barrier) {
                    Barrier barrier = (Barrier) childAt;
                    bVar.r0 = barrier.b();
                    bVar.u0 = barrier.getReferencedIds();
                    bVar.s0 = barrier.getType();
                }
                i++;
            } else {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    public void a(ConstraintLayout constraintLayout) {
        b(constraintLayout);
        constraintLayout.setConstraintSet((j5) null);
    }

    @DexIgnore
    public void a(int i, int i2, int i3, int i4) {
        if (!this.a.containsKey(Integer.valueOf(i))) {
            this.a.put(Integer.valueOf(i), new b());
        }
        b bVar = this.a.get(Integer.valueOf(i));
        switch (i2) {
            case 1:
                if (i4 == 1) {
                    bVar.h = i3;
                    bVar.i = -1;
                    return;
                } else if (i4 == 2) {
                    bVar.i = i3;
                    bVar.h = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("left to " + b(i4) + " undefined");
                }
            case 2:
                if (i4 == 1) {
                    bVar.j = i3;
                    bVar.k = -1;
                    return;
                } else if (i4 == 2) {
                    bVar.k = i3;
                    bVar.j = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + b(i4) + " undefined");
                }
            case 3:
                if (i4 == 3) {
                    bVar.l = i3;
                    bVar.m = -1;
                    bVar.p = -1;
                    return;
                } else if (i4 == 4) {
                    bVar.m = i3;
                    bVar.l = -1;
                    bVar.p = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + b(i4) + " undefined");
                }
            case 4:
                if (i4 == 4) {
                    bVar.o = i3;
                    bVar.n = -1;
                    bVar.p = -1;
                    return;
                } else if (i4 == 3) {
                    bVar.n = i3;
                    bVar.o = -1;
                    bVar.p = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + b(i4) + " undefined");
                }
            case 5:
                if (i4 == 5) {
                    bVar.p = i3;
                    bVar.o = -1;
                    bVar.n = -1;
                    bVar.l = -1;
                    bVar.m = -1;
                    return;
                }
                throw new IllegalArgumentException("right to " + b(i4) + " undefined");
            case 6:
                if (i4 == 6) {
                    bVar.r = i3;
                    bVar.q = -1;
                    return;
                } else if (i4 == 7) {
                    bVar.q = i3;
                    bVar.r = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + b(i4) + " undefined");
                }
            case 7:
                if (i4 == 7) {
                    bVar.t = i3;
                    bVar.s = -1;
                    return;
                } else if (i4 == 6) {
                    bVar.s = i3;
                    bVar.t = -1;
                    return;
                } else {
                    throw new IllegalArgumentException("right to " + b(i4) + " undefined");
                }
            default:
                throw new IllegalArgumentException(b(i2) + " to " + b(i4) + " unknown");
        }
    }

    @DexIgnore
    public void a(int i, float f) {
        a(i).u = f;
    }

    @DexIgnore
    public final b a(int i) {
        if (!this.a.containsKey(Integer.valueOf(i))) {
            this.a.put(Integer.valueOf(i), new b());
        }
        return this.a.get(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(Context context, int i) {
        XmlResourceParser xml = context.getResources().getXml(i);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                if (eventType == 0) {
                    xml.getName();
                } else if (eventType == 2) {
                    String name = xml.getName();
                    b a2 = a(context, Xml.asAttributeSet(xml));
                    if (name.equalsIgnoreCase("Guideline")) {
                        a2.a = true;
                    }
                    this.a.put(Integer.valueOf(a2.d), a2);
                }
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i, int i2) {
        int resourceId = typedArray.getResourceId(i, i2);
        return resourceId == -1 ? typedArray.getInt(i, -1) : resourceId;
    }

    @DexIgnore
    public final b a(Context context, AttributeSet attributeSet) {
        b bVar = new b();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l5.ConstraintSet);
        a(bVar, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return bVar;
    }

    @DexIgnore
    public final void a(b bVar, TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = typedArray.getIndex(i);
            int i2 = c.get(index);
            switch (i2) {
                case 1:
                    bVar.p = a(typedArray, index, bVar.p);
                    break;
                case 2:
                    bVar.G = typedArray.getDimensionPixelSize(index, bVar.G);
                    break;
                case 3:
                    bVar.o = a(typedArray, index, bVar.o);
                    break;
                case 4:
                    bVar.n = a(typedArray, index, bVar.n);
                    break;
                case 5:
                    bVar.w = typedArray.getString(index);
                    break;
                case 6:
                    bVar.A = typedArray.getDimensionPixelOffset(index, bVar.A);
                    break;
                case 7:
                    bVar.B = typedArray.getDimensionPixelOffset(index, bVar.B);
                    break;
                case 8:
                    bVar.H = typedArray.getDimensionPixelSize(index, bVar.H);
                    break;
                case 9:
                    bVar.t = a(typedArray, index, bVar.t);
                    break;
                case 10:
                    bVar.s = a(typedArray, index, bVar.s);
                    break;
                case 11:
                    bVar.N = typedArray.getDimensionPixelSize(index, bVar.N);
                    break;
                case 12:
                    bVar.O = typedArray.getDimensionPixelSize(index, bVar.O);
                    break;
                case 13:
                    bVar.K = typedArray.getDimensionPixelSize(index, bVar.K);
                    break;
                case 14:
                    bVar.M = typedArray.getDimensionPixelSize(index, bVar.M);
                    break;
                case 15:
                    bVar.P = typedArray.getDimensionPixelSize(index, bVar.P);
                    break;
                case 16:
                    bVar.L = typedArray.getDimensionPixelSize(index, bVar.L);
                    break;
                case 17:
                    bVar.e = typedArray.getDimensionPixelOffset(index, bVar.e);
                    break;
                case 18:
                    bVar.f = typedArray.getDimensionPixelOffset(index, bVar.f);
                    break;
                case 19:
                    bVar.g = typedArray.getFloat(index, bVar.g);
                    break;
                case 20:
                    bVar.u = typedArray.getFloat(index, bVar.u);
                    break;
                case 21:
                    bVar.c = typedArray.getLayoutDimension(index, bVar.c);
                    break;
                case 22:
                    bVar.J = typedArray.getInt(index, bVar.J);
                    bVar.J = b[bVar.J];
                    break;
                case 23:
                    bVar.b = typedArray.getLayoutDimension(index, bVar.b);
                    break;
                case 24:
                    bVar.D = typedArray.getDimensionPixelSize(index, bVar.D);
                    break;
                case 25:
                    bVar.h = a(typedArray, index, bVar.h);
                    break;
                case 26:
                    bVar.i = a(typedArray, index, bVar.i);
                    break;
                case 27:
                    bVar.C = typedArray.getInt(index, bVar.C);
                    break;
                case 28:
                    bVar.E = typedArray.getDimensionPixelSize(index, bVar.E);
                    break;
                case 29:
                    bVar.j = a(typedArray, index, bVar.j);
                    break;
                case 30:
                    bVar.k = a(typedArray, index, bVar.k);
                    break;
                case 31:
                    bVar.I = typedArray.getDimensionPixelSize(index, bVar.I);
                    break;
                case 32:
                    bVar.q = a(typedArray, index, bVar.q);
                    break;
                case 33:
                    bVar.r = a(typedArray, index, bVar.r);
                    break;
                case 34:
                    bVar.F = typedArray.getDimensionPixelSize(index, bVar.F);
                    break;
                case 35:
                    bVar.m = a(typedArray, index, bVar.m);
                    break;
                case 36:
                    bVar.l = a(typedArray, index, bVar.l);
                    break;
                case 37:
                    bVar.v = typedArray.getFloat(index, bVar.v);
                    break;
                case 38:
                    bVar.d = typedArray.getResourceId(index, bVar.d);
                    break;
                case 39:
                    bVar.R = typedArray.getFloat(index, bVar.R);
                    break;
                case 40:
                    bVar.Q = typedArray.getFloat(index, bVar.Q);
                    break;
                case 41:
                    bVar.S = typedArray.getInt(index, bVar.S);
                    break;
                case 42:
                    bVar.T = typedArray.getInt(index, bVar.T);
                    break;
                case 43:
                    bVar.U = typedArray.getFloat(index, bVar.U);
                    break;
                case 44:
                    bVar.V = true;
                    bVar.W = typedArray.getDimension(index, bVar.W);
                    break;
                case 45:
                    bVar.Y = typedArray.getFloat(index, bVar.Y);
                    break;
                case 46:
                    bVar.Z = typedArray.getFloat(index, bVar.Z);
                    break;
                case 47:
                    bVar.a0 = typedArray.getFloat(index, bVar.a0);
                    break;
                case 48:
                    bVar.b0 = typedArray.getFloat(index, bVar.b0);
                    break;
                case 49:
                    bVar.c0 = typedArray.getFloat(index, bVar.c0);
                    break;
                case 50:
                    bVar.d0 = typedArray.getFloat(index, bVar.d0);
                    break;
                case 51:
                    bVar.e0 = typedArray.getDimension(index, bVar.e0);
                    break;
                case 52:
                    bVar.f0 = typedArray.getDimension(index, bVar.f0);
                    break;
                case 53:
                    bVar.g0 = typedArray.getDimension(index, bVar.g0);
                    break;
                default:
                    switch (i2) {
                        case 60:
                            bVar.X = typedArray.getFloat(index, bVar.X);
                            break;
                        case 61:
                            bVar.x = a(typedArray, index, bVar.x);
                            break;
                        case 62:
                            bVar.y = typedArray.getDimensionPixelSize(index, bVar.y);
                            break;
                        case 63:
                            bVar.z = typedArray.getFloat(index, bVar.z);
                            break;
                        default:
                            switch (i2) {
                                case 69:
                                    bVar.p0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 70:
                                    bVar.q0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 71:
                                    Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                    break;
                                case 72:
                                    bVar.s0 = typedArray.getInt(index, bVar.s0);
                                    break;
                                case 73:
                                    bVar.v0 = typedArray.getString(index);
                                    break;
                                case 74:
                                    bVar.r0 = typedArray.getBoolean(index, bVar.r0);
                                    break;
                                case 75:
                                    Log.w("ConstraintSet", "unused attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    break;
                                default:
                                    Log.w("ConstraintSet", "Unknown attribute 0x" + Integer.toHexString(index) + "   " + c.get(index));
                                    break;
                            }
                    }
            }
        }
    }

    @DexIgnore
    public final int[] a(View view, String str) {
        int i;
        String[] split = str.split(",");
        Context context = view.getContext();
        int[] iArr = new int[split.length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < split.length) {
            String trim = split[i2].trim();
            try {
                i = k5.class.getField(trim).getInt((Object) null);
            } catch (Exception unused) {
                i = 0;
            }
            if (i == 0) {
                i = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            if (i == 0 && view.isInEditMode() && (view.getParent() instanceof ConstraintLayout)) {
                Object a2 = ((ConstraintLayout) view.getParent()).a(0, (Object) trim);
                if (a2 != null && (a2 instanceof Integer)) {
                    i = ((Integer) a2).intValue();
                }
            }
            iArr[i3] = i;
            i2++;
            i3++;
        }
        return i3 != split.length ? Arrays.copyOf(iArr, i3) : iArr;
    }
}
