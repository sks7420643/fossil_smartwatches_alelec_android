package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cu */
public class C1580cu implements com.fossil.blesdk.obfuscated.C1906gu<android.graphics.Bitmap, byte[]> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.graphics.Bitmap.CompressFormat f4193a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f4194b;

    @DexIgnore
    public C1580cu() {
        this(android.graphics.Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<byte[]> mo9632a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        aqVar.get().compress(this.f4193a, this.f4194b, byteArrayOutputStream);
        aqVar.mo8887a();
        return new com.fossil.blesdk.obfuscated.C2246kt(byteArrayOutputStream.toByteArray());
    }

    @DexIgnore
    public C1580cu(android.graphics.Bitmap.CompressFormat compressFormat, int i) {
        this.f4193a = compressFormat;
        this.f4194b = i;
    }
}
