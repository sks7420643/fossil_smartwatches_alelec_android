package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.oc1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nd1 extends bf0<f41, qc1> {
    @DexIgnore
    public /* final */ /* synthetic */ i41 d;
    @DexIgnore
    public /* final */ /* synthetic */ ze0 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nd1(oc1 oc1, ze0 ze0, i41 i41, ze0 ze02) {
        super(ze0);
        this.d = i41;
        this.e = ze02;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar, xn1 xn1) throws RemoteException {
        ((f41) bVar).a(this.d, (ze0<qc1>) this.e, (r31) new oc1.a(xn1));
    }
}
