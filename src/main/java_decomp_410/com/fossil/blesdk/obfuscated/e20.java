package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.complication.ComplicationId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class e20 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[ComplicationId.values().length];

    /*
    static {
        a[ComplicationId.WEATHER.ordinal()] = 1;
        a[ComplicationId.HEART_RATE.ordinal()] = 2;
        a[ComplicationId.STEPS.ordinal()] = 3;
        a[ComplicationId.DATE.ordinal()] = 4;
        a[ComplicationId.CHANCE_OF_RAIN.ordinal()] = 5;
        a[ComplicationId.SECOND_TIMEZONE.ordinal()] = 6;
        a[ComplicationId.ACTIVE_MINUTES.ordinal()] = 7;
        a[ComplicationId.CALORIES.ordinal()] = 8;
        a[ComplicationId.EMPTY.ordinal()] = 9;
    }
    */
}
