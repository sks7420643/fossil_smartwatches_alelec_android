package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mb0 {
    @DexIgnore
    public static /* final */ mb0 a; // = new mb0();

    @DexIgnore
    public final void a(BroadcastReceiver broadcastReceiver, String... strArr) {
        kd4.b(broadcastReceiver, "receiver");
        kd4.b(strArr, "actions");
        Context a2 = va0.f.a();
        if (a2 != null) {
            IntentFilter intentFilter = new IntentFilter();
            for (String addAction : strArr) {
                intentFilter.addAction(addAction);
            }
            rc.a(a2).a(broadcastReceiver, intentFilter);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        kd4.b(intent, "intent");
        Context a2 = va0.f.a();
        if (a2 != null) {
            return rc.a(a2).a(intent);
        }
        return false;
    }
}
