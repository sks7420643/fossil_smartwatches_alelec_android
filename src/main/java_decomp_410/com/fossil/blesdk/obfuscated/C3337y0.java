package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y0 */
public class C3337y0 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C2110j9> f11126a; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: b */
    public long f11127b; // = -1;

    @DexIgnore
    /* renamed from: c */
    public android.view.animation.Interpolator f11128c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2190k9 f11129d;

    @DexIgnore
    /* renamed from: e */
    public boolean f11130e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2302l9 f11131f; // = new com.fossil.blesdk.obfuscated.C3337y0.C3338a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.y0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.y0$a */
    public class C3338a extends com.fossil.blesdk.obfuscated.C2302l9 {

        @DexIgnore
        /* renamed from: a */
        public boolean f11132a; // = false;

        @DexIgnore
        /* renamed from: b */
        public int f11133b; // = 0;

        @DexIgnore
        public C3338a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17956a() {
            this.f11133b = 0;
            this.f11132a = false;
            com.fossil.blesdk.obfuscated.C3337y0.this.mo17954b();
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            int i = this.f11133b + 1;
            this.f11133b = i;
            if (i == com.fossil.blesdk.obfuscated.C3337y0.this.f11126a.size()) {
                com.fossil.blesdk.obfuscated.C2190k9 k9Var = com.fossil.blesdk.obfuscated.C3337y0.this.f11129d;
                if (k9Var != null) {
                    k9Var.mo8506b((android.view.View) null);
                }
                mo17956a();
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8507c(android.view.View view) {
            if (!this.f11132a) {
                this.f11132a = true;
                com.fossil.blesdk.obfuscated.C2190k9 k9Var = com.fossil.blesdk.obfuscated.C3337y0.this.f11129d;
                if (k9Var != null) {
                    k9Var.mo8507c((android.view.View) null);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3337y0 mo17950a(com.fossil.blesdk.obfuscated.C2110j9 j9Var) {
        if (!this.f11130e) {
            this.f11126a.add(j9Var);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17954b() {
        this.f11130e = false;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo17955c() {
        if (!this.f11130e) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2110j9> it = this.f11126a.iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C2110j9 next = it.next();
                long j = this.f11127b;
                if (j >= 0) {
                    next.mo12276a(j);
                }
                android.view.animation.Interpolator interpolator = this.f11128c;
                if (interpolator != null) {
                    next.mo12277a(interpolator);
                }
                if (this.f11129d != null) {
                    next.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) this.f11131f);
                }
                next.mo12285c();
            }
            this.f11130e = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3337y0 mo17951a(com.fossil.blesdk.obfuscated.C2110j9 j9Var, com.fossil.blesdk.obfuscated.C2110j9 j9Var2) {
        this.f11126a.add(j9Var);
        j9Var2.mo12284b(j9Var.mo12282b());
        this.f11126a.add(j9Var2);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17953a() {
        if (this.f11130e) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2110j9> it = this.f11126a.iterator();
            while (it.hasNext()) {
                it.next().mo12280a();
            }
            this.f11130e = false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3337y0 mo17948a(long j) {
        if (!this.f11130e) {
            this.f11127b = j;
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3337y0 mo17949a(android.view.animation.Interpolator interpolator) {
        if (!this.f11130e) {
            this.f11128c = interpolator;
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3337y0 mo17952a(com.fossil.blesdk.obfuscated.C2190k9 k9Var) {
        if (!this.f11130e) {
            this.f11129d = k9Var;
        }
        return this;
    }
}
