package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uj4;
import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pk4 extends tj4<tk4> {
    @DexIgnore
    public pk4() {
        super(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008f, code lost:
        r7 = r9;
     */
    @DexIgnore
    public final tk4 a(TaskMode taskMode) {
        Object obj;
        kd4.b(taskMode, "mode");
        while (true) {
            uj4 uj4 = (uj4) this._cur$internal;
            while (true) {
                long j = uj4._state$internal;
                obj = null;
                if ((1152921504606846976L & j) == 0) {
                    uj4.a aVar = uj4.h;
                    boolean z = false;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((uj4.a & ((int) ((1152921503533105152L & j) >> 30))) != (uj4.a & i)) {
                        Object obj2 = uj4.b.get(uj4.a & i);
                        if (obj2 != null) {
                            if (obj2 instanceof uj4.b) {
                                break;
                            }
                            if (((tk4) obj2).a() == taskMode) {
                                z = true;
                            }
                            if (z) {
                                int i2 = (i + 1) & 1073741823;
                                if (!uj4.f.compareAndSet(uj4, j, uj4.h.a(j, i2))) {
                                    if (uj4.d) {
                                        uj4 uj42 = uj4;
                                        do {
                                            uj42 = uj42.a(i, i2);
                                        } while (uj42 != null);
                                        break;
                                    }
                                } else {
                                    uj4.b.set(uj4.a & i, (Object) null);
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (uj4.d) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    obj = uj4.g;
                    break;
                }
            }
            if (obj != uj4.g) {
                return (tk4) obj;
            }
            tj4.a.compareAndSet(this, uj4, uj4.e());
        }
    }
}
