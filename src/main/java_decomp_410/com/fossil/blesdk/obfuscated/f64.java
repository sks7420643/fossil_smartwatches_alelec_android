package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.Priority;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface f64<T> extends Comparable<T> {
    @DexIgnore
    Priority getPriority();
}
