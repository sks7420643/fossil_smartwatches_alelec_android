package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class al3 implements Factory<zk3> {
    @DexIgnore
    public static zk3 a(wk3 wk3, ResetPasswordUseCase resetPasswordUseCase) {
        return new zk3(wk3, resetPasswordUseCase);
    }
}
