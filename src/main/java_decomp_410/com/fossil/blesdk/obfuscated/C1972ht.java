package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ht */
public final class C1972ht implements com.fossil.blesdk.obfuscated.C2427mo<android.graphics.Bitmap, android.graphics.Bitmap> {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ht$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ht$a */
    public static final class C1973a implements com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.graphics.Bitmap f5834e;

        @DexIgnore
        public C1973a(android.graphics.Bitmap bitmap) {
            this.f5834e = bitmap;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8887a() {
        }

        @DexIgnore
        /* renamed from: b */
        public int mo8888b() {
            return com.fossil.blesdk.obfuscated.C3066uw.m14922a(this.f5834e);
        }

        @DexIgnore
        /* renamed from: c */
        public java.lang.Class<android.graphics.Bitmap> mo8889c() {
            return android.graphics.Bitmap.class;
        }

        @DexIgnore
        public android.graphics.Bitmap get() {
            return this.f5834e;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(android.graphics.Bitmap bitmap, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C1972ht.C1973a(bitmap);
    }
}
