package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ed1 implements Parcelable.Creator<dd1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = "";
        String str2 = str;
        String str3 = str2;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str2 = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                str3 = SafeParcelReader.f(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                str = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new dd1(str, str2, str3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new dd1[i];
    }
}
