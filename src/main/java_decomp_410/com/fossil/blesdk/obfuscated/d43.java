package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d43 {
    @DexIgnore
    public /* final */ b43 a;

    @DexIgnore
    public d43(b43 b43) {
        kd4.b(b43, "mView");
        this.a = b43;
    }

    @DexIgnore
    public final b43 a() {
        return this.a;
    }
}
