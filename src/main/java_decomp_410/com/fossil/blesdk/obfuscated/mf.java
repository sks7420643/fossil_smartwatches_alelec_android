package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mf extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements mf {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mf$a$a  reason: collision with other inner class name */
        public static class C0021a implements mf {
            @DexIgnore
            public IBinder e;

            @DexIgnore
            public C0021a(IBinder iBinder) {
                this.e = iBinder;
            }

            @DexIgnore
            public void a(String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                    obtain.writeStringArray(strArr);
                    this.e.transact(1, obtain, (Parcel) null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.e;
            }
        }

        @DexIgnore
        public a() {
            attachInterface(this, "androidx.room.IMultiInstanceInvalidationCallback");
        }

        @DexIgnore
        public static mf a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof mf)) {
                return new C0021a(iBinder);
            }
            return (mf) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationCallback");
                a(parcel.createStringArray());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("androidx.room.IMultiInstanceInvalidationCallback");
                return true;
            }
        }
    }

    @DexIgnore
    void a(String[] strArr) throws RemoteException;
}
