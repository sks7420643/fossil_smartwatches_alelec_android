package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g13 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<h13> c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public g13(String str, String str2, ArrayList<h13> arrayList, boolean z) {
        kd4.b(str, "mPresetId");
        kd4.b(str2, "mPresetName");
        kd4.b(arrayList, "mMicroApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = z;
    }

    @DexIgnore
    public final ArrayList<h13> a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
