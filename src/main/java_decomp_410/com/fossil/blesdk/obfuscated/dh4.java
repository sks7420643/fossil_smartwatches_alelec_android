package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dh4 {
    @DexIgnore
    public static final String a(yb4<?> yb4) {
        Object obj;
        kd4.b(yb4, "$this$toDebugString");
        if (yb4 instanceof jh4) {
            return yb4.toString();
        }
        try {
            Result.a aVar = Result.Companion;
            obj = Result.m3constructorimpl(yb4 + '@' + b(yb4));
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            obj = Result.m3constructorimpl(na4.a(th));
        }
        Throwable r2 = Result.m6exceptionOrNullimpl(obj);
        String str = obj;
        if (r2 != null) {
            str = yb4.getClass().getName() + '@' + b(yb4);
        }
        return (String) str;
    }

    @DexIgnore
    public static final String b(Object obj) {
        kd4.b(obj, "$this$hexAddress");
        String hexString = Integer.toHexString(System.identityHashCode(obj));
        kd4.a((Object) hexString, "Integer.toHexString(System.identityHashCode(this))");
        return hexString;
    }

    @DexIgnore
    public static final String a(Object obj) {
        kd4.b(obj, "$this$classSimpleName");
        String simpleName = obj.getClass().getSimpleName();
        kd4.a((Object) simpleName, "this::class.java.simpleName");
        return simpleName;
    }
}
