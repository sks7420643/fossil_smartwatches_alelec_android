package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.model.file.WatchParameterFile;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r50 extends g60 {
    @DexIgnore
    public /* final */ WatchParameterFile Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r50(Peripheral peripheral, Phase.a aVar, WatchParameterFile watchParameterFile) {
        super(peripheral, aVar, PhaseId.PUT_WATCH_PARAMETERS_FILE, true, watchParameterFile.getFileHandle$blesdk_productionRelease(), watchParameterFile.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(watchParameterFile, "watchParameterFile");
        this.Q = watchParameterFile;
    }

    @DexIgnore
    public final WatchParameterFile O() {
        return this.Q;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.WATCH_PARAMETERS_FILE, this.Q.toJSONObject());
    }
}
