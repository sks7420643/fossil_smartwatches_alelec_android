package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s74 implements a84 {
    @DexIgnore
    public /* final */ e84 a;
    @DexIgnore
    public /* final */ d84 b;
    @DexIgnore
    public /* final */ n54 c;
    @DexIgnore
    public /* final */ p74 d;
    @DexIgnore
    public /* final */ f84 e;
    @DexIgnore
    public /* final */ v44 f;
    @DexIgnore
    public /* final */ g74 g; // = new h74(this.f);
    @DexIgnore
    public /* final */ o54 h;

    @DexIgnore
    public s74(v44 v44, e84 e84, n54 n54, d84 d84, p74 p74, f84 f84, o54 o54) {
        this.f = v44;
        this.a = e84;
        this.c = n54;
        this.b = d84;
        this.d = p74;
        this.e = f84;
        this.h = o54;
    }

    @DexIgnore
    public b84 a() {
        return a(SettingsCacheBehavior.USE_CACHE);
    }

    @DexIgnore
    public final b84 b(SettingsCacheBehavior settingsCacheBehavior) {
        b84 b84 = null;
        try {
            if (SettingsCacheBehavior.SKIP_CACHE_LOOKUP.equals(settingsCacheBehavior)) {
                return null;
            }
            JSONObject a2 = this.d.a();
            if (a2 != null) {
                b84 a3 = this.b.a(this.c, a2);
                if (a3 != null) {
                    a(a2, "Loaded cached settings: ");
                    long a4 = this.c.a();
                    if (!SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION.equals(settingsCacheBehavior)) {
                        if (a3.a(a4)) {
                            q44.g().d("Fabric", "Cached settings have expired.");
                            return null;
                        }
                    }
                    try {
                        q44.g().d("Fabric", "Returning cached settings.");
                        return a3;
                    } catch (Exception e2) {
                        e = e2;
                        b84 = a3;
                        q44.g().e("Fabric", "Failed to get cached settings", e);
                        return b84;
                    }
                } else {
                    q44.g().e("Fabric", "Failed to transform cached settings data.", (Throwable) null);
                    return null;
                }
            } else {
                q44.g().d("Fabric", "No cached settings data found.");
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            q44.g().e("Fabric", "Failed to get cached settings", e);
            return b84;
        }
    }

    @DexIgnore
    public String c() {
        return CommonUtils.a(CommonUtils.n(this.f.l()));
    }

    @DexIgnore
    public String d() {
        return this.g.get().getString("existing_instance_identifier", "");
    }

    @DexIgnore
    public b84 a(SettingsCacheBehavior settingsCacheBehavior) {
        b84 b84 = null;
        if (!this.h.a()) {
            q44.g().d("Fabric", "Not fetching settings, because data collection is disabled by Firebase.");
            return null;
        }
        try {
            if (!q44.h() && !b()) {
                b84 = b(settingsCacheBehavior);
            }
            if (b84 == null) {
                JSONObject a2 = this.e.a(this.a);
                if (a2 != null) {
                    b84 = this.b.a(this.c, a2);
                    this.d.a(b84.f, a2);
                    a(a2, "Loaded settings: ");
                    a(c());
                }
            }
            if (b84 == null) {
                return b(SettingsCacheBehavior.IGNORE_CACHE_EXPIRATION);
            }
            return b84;
        } catch (Exception e2) {
            q44.g().e("Fabric", "Unknown error while loading Crashlytics settings. Crashes will be cached until settings can be retrieved.", e2);
            return null;
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject, String str) throws JSONException {
        y44 g2 = q44.g();
        g2.d("Fabric", str + jSONObject.toString());
    }

    @DexIgnore
    public boolean b() {
        return !d().equals(c());
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a(String str) {
        SharedPreferences.Editor edit = this.g.edit();
        edit.putString("existing_instance_identifier", str);
        return this.g.a(edit);
    }
}
