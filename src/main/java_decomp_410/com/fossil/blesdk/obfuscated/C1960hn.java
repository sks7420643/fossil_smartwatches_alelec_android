package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hn */
public class C1960hn extends com.fossil.blesdk.obfuscated.C3445zm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1960hn.C1962b f5810a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ javax.net.ssl.SSLSocketFactory f5811b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hn$a */
    public static class C1961a extends java.io.FilterInputStream {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.net.HttpURLConnection f5812e;

        @DexIgnore
        public C1961a(java.net.HttpURLConnection httpURLConnection) {
            super(com.fossil.blesdk.obfuscated.C1960hn.m7924b(httpURLConnection));
            this.f5812e = httpURLConnection;
        }

        @DexIgnore
        public void close() throws java.io.IOException {
            super.close();
            this.f5812e.disconnect();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.hn$b */
    public interface C1962b {
        @DexIgnore
        /* renamed from: a */
        java.lang.String mo11695a(java.lang.String str);
    }

    @DexIgnore
    public C1960hn() {
        this((com.fossil.blesdk.obfuscated.C1960hn.C1962b) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m7923a(int i, int i2) {
        return (i == 4 || (100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1815fn mo11693b(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException, com.android.volley.AuthFailureError {
        java.lang.String str;
        java.lang.String url = request.getUrl();
        java.util.HashMap hashMap = new java.util.HashMap();
        hashMap.putAll(map);
        hashMap.putAll(request.getHeaders());
        com.fossil.blesdk.obfuscated.C1960hn.C1962b bVar = this.f5810a;
        if (bVar != null) {
            str = bVar.mo11695a(url);
            if (str == null) {
                throw new java.io.IOException("URL blocked by rewriter: " + url);
            }
        } else {
            str = url;
        }
        java.net.HttpURLConnection a = mo11692a(new java.net.URL(str), request);
        boolean z = false;
        try {
            for (java.lang.String str2 : hashMap.keySet()) {
                a.setRequestProperty(str2, (java.lang.String) hashMap.get(str2));
            }
            m7925b(a, request);
            int responseCode = a.getResponseCode();
            if (responseCode == -1) {
                throw new java.io.IOException("Could not retrieve response code from HttpUrlConnection.");
            } else if (!m7923a(request.getMethod(), responseCode)) {
                com.fossil.blesdk.obfuscated.C1815fn fnVar = new com.fossil.blesdk.obfuscated.C1815fn(responseCode, m7920a((java.util.Map<java.lang.String, java.util.List<java.lang.String>>) a.getHeaderFields()));
                a.disconnect();
                return fnVar;
            } else {
                z = true;
                return new com.fossil.blesdk.obfuscated.C1815fn(responseCode, m7920a((java.util.Map<java.lang.String, java.util.List<java.lang.String>>) a.getHeaderFields()), a.getContentLength(), new com.fossil.blesdk.obfuscated.C1960hn.C1961a(a));
            }
        } catch (Throwable th) {
            if (!z) {
                a.disconnect();
            }
            throw th;
        }
    }

    @DexIgnore
    public C1960hn(com.fossil.blesdk.obfuscated.C1960hn.C1962b bVar) {
        this(bVar, (javax.net.ssl.SSLSocketFactory) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<com.fossil.blesdk.obfuscated.C2660pm> m7920a(java.util.Map<java.lang.String, java.util.List<java.lang.String>> map) {
        java.util.ArrayList arrayList = new java.util.ArrayList(map.size());
        for (java.util.Map.Entry next : map.entrySet()) {
            if (next.getKey() != null) {
                for (java.lang.String pmVar : (java.util.List) next.getValue()) {
                    arrayList.add(new com.fossil.blesdk.obfuscated.C2660pm((java.lang.String) next.getKey(), pmVar));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public C1960hn(com.fossil.blesdk.obfuscated.C1960hn.C1962b bVar, javax.net.ssl.SSLSocketFactory sSLSocketFactory) {
        this.f5810a = bVar;
        this.f5811b = sSLSocketFactory;
    }

    @DexIgnore
    /* renamed from: a */
    public java.net.HttpURLConnection mo11691a(java.net.URL url) throws java.io.IOException {
        java.net.HttpURLConnection httpURLConnection = (java.net.HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(java.net.HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.net.HttpURLConnection mo11692a(java.net.URL url, com.android.volley.Request<?> request) throws java.io.IOException {
        java.net.HttpURLConnection a = mo11691a(url);
        int timeoutMs = request.getTimeoutMs();
        a.setConnectTimeout(timeoutMs);
        a.setReadTimeout(timeoutMs);
        a.setUseCaches(false);
        a.setDoInput(true);
        if (com.facebook.internal.Utility.URL_SCHEME.equals(url.getProtocol())) {
            javax.net.ssl.SSLSocketFactory sSLSocketFactory = this.f5811b;
            if (sSLSocketFactory != null) {
                ((javax.net.ssl.HttpsURLConnection) a).setSSLSocketFactory(sSLSocketFactory);
            }
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7921a(java.net.HttpURLConnection httpURLConnection, com.android.volley.Request<?> request) throws java.io.IOException, com.android.volley.AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            m7922a(httpURLConnection, request, body);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7922a(java.net.HttpURLConnection httpURLConnection, com.android.volley.Request<?> request, byte[] bArr) throws java.io.IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey(com.facebook.GraphRequest.CONTENT_TYPE_HEADER)) {
            httpURLConnection.setRequestProperty(com.facebook.GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
        }
        java.io.DataOutputStream dataOutputStream = new java.io.DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(bArr);
        dataOutputStream.close();
    }

    @DexIgnore
    /* renamed from: b */
    public static java.io.InputStream m7924b(java.net.HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (java.io.IOException unused) {
            return httpURLConnection.getErrorStream();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m7925b(java.net.HttpURLConnection httpURLConnection, com.android.volley.Request<?> request) throws java.io.IOException, com.android.volley.AuthFailureError {
        switch (request.getMethod()) {
            case -1:
                byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    m7922a(httpURLConnection, request, postBody);
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                m7921a(httpURLConnection, request);
                return;
            case 2:
                httpURLConnection.setRequestMethod("PUT");
                m7921a(httpURLConnection, request);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                m7921a(httpURLConnection, request);
                return;
            default:
                throw new java.lang.IllegalStateException("Unknown method type.");
        }
    }
}
