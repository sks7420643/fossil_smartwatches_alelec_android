package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mm */
public class C2423mm extends java.lang.Thread {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ boolean f7539k; // = com.fossil.blesdk.obfuscated.C3296xm.f11007b;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> f7540e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> f7541f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2334lm f7542g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3124vm f7543h;

    @DexIgnore
    /* renamed from: i */
    public volatile boolean f7544i; // = false;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C2423mm.C2425b f7545j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mm$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mm$a */
    public class C2424a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.android.volley.Request f7546e;

        @DexIgnore
        public C2424a(com.android.volley.Request request) {
            this.f7546e = request;
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C2423mm.this.f7541f.put(this.f7546e);
            } catch (java.lang.InterruptedException unused) {
                java.lang.Thread.currentThread().interrupt();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mm$b")
    /* renamed from: com.fossil.blesdk.obfuscated.mm$b */
    public static class C2425b implements com.android.volley.Request.C0375b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Map<java.lang.String, java.util.List<com.android.volley.Request<?>>> f7548a; // = new java.util.HashMap();

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2423mm f7549b;

        @DexIgnore
        public C2425b(com.fossil.blesdk.obfuscated.C2423mm mmVar) {
            this.f7549b = mmVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
            return false;
         */
        @DexIgnore
        /* renamed from: b */
        public final synchronized boolean mo13685b(com.android.volley.Request<?> request) {
            java.lang.String cacheKey = request.getCacheKey();
            if (this.f7548a.containsKey(cacheKey)) {
                java.util.List list = this.f7548a.get(cacheKey);
                if (list == null) {
                    list = new java.util.ArrayList();
                }
                request.addMarker("waiting-for-response");
                list.add(request);
                this.f7548a.put(cacheKey, list);
                if (com.fossil.blesdk.obfuscated.C3296xm.f11007b) {
                    com.fossil.blesdk.obfuscated.C3296xm.m16421b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                }
            } else {
                this.f7548a.put(cacheKey, (java.lang.Object) null);
                request.setNetworkRequestCompleteListener(this);
                if (com.fossil.blesdk.obfuscated.C3296xm.f11007b) {
                    com.fossil.blesdk.obfuscated.C3296xm.m16421b("new request, sending to network %s", cacheKey);
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo3901a(com.android.volley.Request<?> request, com.fossil.blesdk.obfuscated.C3047um<?> umVar) {
            java.util.List<com.android.volley.Request> remove;
            com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar = umVar.f10028b;
            if (aVar == null || aVar.mo13312a()) {
                mo3900a(request);
                return;
            }
            java.lang.String cacheKey = request.getCacheKey();
            synchronized (this) {
                remove = this.f7548a.remove(cacheKey);
            }
            if (remove != null) {
                if (com.fossil.blesdk.obfuscated.C3296xm.f11007b) {
                    com.fossil.blesdk.obfuscated.C3296xm.m16423d("Releasing %d waiting requests for cacheKey=%s.", java.lang.Integer.valueOf(remove.size()), cacheKey);
                }
                for (com.android.volley.Request a : remove) {
                    this.f7549b.f7543h.mo14456a((com.android.volley.Request<?>) a, umVar);
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public synchronized void mo3900a(com.android.volley.Request<?> request) {
            java.lang.String cacheKey = request.getCacheKey();
            java.util.List remove = this.f7548a.remove(cacheKey);
            if (remove != null && !remove.isEmpty()) {
                if (com.fossil.blesdk.obfuscated.C3296xm.f11007b) {
                    com.fossil.blesdk.obfuscated.C3296xm.m16423d("%d waiting requests for cacheKey=%s; resend to network", java.lang.Integer.valueOf(remove.size()), cacheKey);
                }
                com.android.volley.Request request2 = (com.android.volley.Request) remove.remove(0);
                this.f7548a.put(cacheKey, remove);
                request2.setNetworkRequestCompleteListener(this);
                try {
                    this.f7549b.f7541f.put(request2);
                } catch (java.lang.InterruptedException e) {
                    com.fossil.blesdk.obfuscated.C3296xm.m16422c("Couldn't add request to queue. %s", e.toString());
                    java.lang.Thread.currentThread().interrupt();
                    this.f7549b.mo13682b();
                }
            }
            return;
        }
    }

    @DexIgnore
    public C2423mm(java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> blockingQueue, java.util.concurrent.BlockingQueue<com.android.volley.Request<?>> blockingQueue2, com.fossil.blesdk.obfuscated.C2334lm lmVar, com.fossil.blesdk.obfuscated.C3124vm vmVar) {
        this.f7540e = blockingQueue;
        this.f7541f = blockingQueue2;
        this.f7542g = lmVar;
        this.f7543h = vmVar;
        this.f7545j = new com.fossil.blesdk.obfuscated.C2423mm.C2425b(this);
    }

    @DexIgnore
    public void run() {
        if (f7539k) {
            com.fossil.blesdk.obfuscated.C3296xm.m16423d("start new dispatcher", new java.lang.Object[0]);
        }
        android.os.Process.setThreadPriority(10);
        this.f7542g.mo9576d();
        while (true) {
            try {
                mo13680a();
            } catch (java.lang.InterruptedException unused) {
                if (this.f7544i) {
                    java.lang.Thread.currentThread().interrupt();
                    return;
                }
                com.fossil.blesdk.obfuscated.C3296xm.m16422c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new java.lang.Object[0]);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13680a() throws java.lang.InterruptedException {
        mo13681a(this.f7540e.take());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13682b() {
        this.f7544i = true;
        interrupt();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13681a(com.android.volley.Request<?> request) throws java.lang.InterruptedException {
        request.addMarker("cache-queue-take");
        if (request.isCanceled()) {
            request.finish("cache-discard-canceled");
            return;
        }
        com.fossil.blesdk.obfuscated.C2334lm.C2335a a = this.f7542g.mo9568a(request.getCacheKey());
        if (a == null) {
            request.addMarker("cache-miss");
            if (!this.f7545j.mo13685b(request)) {
                this.f7541f.put(request);
            }
        } else if (a.mo13312a()) {
            request.addMarker("cache-hit-expired");
            request.setCacheEntry(a);
            if (!this.f7545j.mo13685b(request)) {
                this.f7541f.put(request);
            }
        } else {
            request.addMarker("cache-hit");
            com.fossil.blesdk.obfuscated.C3047um<?> parseNetworkResponse = request.parseNetworkResponse(new com.fossil.blesdk.obfuscated.C2897sm(a.f7239a, a.f7245g));
            request.addMarker("cache-hit-parsed");
            if (!a.mo13313b()) {
                this.f7543h.mo14456a(request, parseNetworkResponse);
                return;
            }
            request.addMarker("cache-hit-refresh-needed");
            request.setCacheEntry(a);
            parseNetworkResponse.f10030d = true;
            if (!this.f7545j.mo13685b(request)) {
                this.f7543h.mo14457a(request, parseNetworkResponse, new com.fossil.blesdk.obfuscated.C2423mm.C2424a(request));
            } else {
                this.f7543h.mo14456a(request, parseNetworkResponse);
            }
        }
    }
}
