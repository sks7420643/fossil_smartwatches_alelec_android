package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv2 implements Factory<z53> {
    @DexIgnore
    public static z53 a(gv2 gv2) {
        z53 e = gv2.e();
        n44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
