package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.dm4;
import com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.util.UserUtils;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n42 {
    @DexIgnore
    public /* final */ PortfolioApp a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ n42 a;

        @DexIgnore
        public a(n42 n42) {
            this.a = n42;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            dm4.a f = chain.n().f();
            f.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
            f.a("User-Agent", yi2.b.a());
            f.a("locale", this.a.a.m());
            return chain.a(f.a());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Interceptor {
        @DexIgnore
        public /* final */ /* synthetic */ n42 a;

        @DexIgnore
        public b(n42 n42) {
            this.a = n42;
        }

        @DexIgnore
        public final Response intercept(Interceptor.Chain chain) {
            dm4.a f = chain.n().f();
            f.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
            f.a("User-Agent", yi2.b.a());
            f.a("locale", this.a.a.m());
            return chain.a(f.a());
        }
    }

    @DexIgnore
    public n42(PortfolioApp portfolioApp) {
        kd4.b(portfolioApp, "mApplication");
        this.a = portfolioApp;
    }

    @DexIgnore
    public final PortfolioApp b() {
        return this.a;
    }

    @DexIgnore
    public final Context c() {
        Context applicationContext = this.a.getApplicationContext();
        kd4.a((Object) applicationContext, "mApplication.applicationContext");
        return applicationContext;
    }

    @DexIgnore
    public final ShortcutApiService d(xo2 xo2, bp2 bp2) {
        kd4.b(xo2, "interceptor");
        kd4.b(bp2, "authenticator");
        return (ShortcutApiService) a(xo2, bp2, ShortcutApiService.class);
    }

    @DexIgnore
    public final ContentResolver e() {
        ContentResolver contentResolver = this.a.getContentResolver();
        kd4.a((Object) contentResolver, "mApplication.contentResolver");
        return contentResolver;
    }

    @DexIgnore
    public final in2 f() {
        return new in2();
    }

    @DexIgnore
    public final FirmwareFileRepository g() {
        Context applicationContext = this.a.getApplicationContext();
        kd4.a((Object) applicationContext, "mApplication.applicationContext");
        return new FirmwareFileRepository(applicationContext, new FirmwareFileLocalSource());
    }

    @DexIgnore
    public final GuestApiService h() {
        b bVar = new b(this);
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((Interceptor) bVar);
        return (GuestApiService) ap2.g.a(GuestApiService.class);
    }

    @DexIgnore
    public final rc i() {
        rc a2 = rc.a((Context) this.a);
        kd4.a((Object) a2, "LocalBroadcastManager.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final jn2 j() {
        return new jn2();
    }

    @DexIgnore
    public final f42 k() {
        f42 a2 = f42.a((Context) this.a);
        kd4.a((Object) a2, "MFLocationService.getInstance(mApplication)");
        return a2;
    }

    @DexIgnore
    public final dn2 l() {
        return dn2.p.a();
    }

    @DexIgnore
    public final j62 m() {
        return new j62(new l62());
    }

    @DexIgnore
    public final UserUtils n() {
        return UserUtils.d.a();
    }

    @DexIgnore
    public final ql2 o() {
        ql2 b2 = ql2.b();
        kd4.a((Object) b2, "WatchHelper.getInstance()");
        return b2;
    }

    @DexIgnore
    public final MFLoginWechatManager p() {
        return new MFLoginWechatManager();
    }

    @DexIgnore
    public final kn2 q() {
        return new kn2();
    }

    @DexIgnore
    public final en2 a(Context context) {
        kd4.b(context, "context");
        return new en2(context);
    }

    @DexIgnore
    public final AuthApiUserService b(xo2 xo2, bp2 bp2) {
        kd4.b(xo2, "interceptor");
        kd4.b(bp2, "authenticator");
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((hl4) bp2);
        ap2.g.a((Interceptor) xo2);
        return (AuthApiUserService) ap2.g.a(AuthApiUserService.class);
    }

    @DexIgnore
    public final GoogleApiService c(xo2 xo2, bp2 bp2) {
        kd4.b(xo2, "interceptor");
        kd4.b(bp2, "authenticator");
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 4) + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((hl4) bp2);
        ap2.g.a((Interceptor) xo2);
        return (GoogleApiService) ap2.g.a(GoogleApiService.class);
    }

    @DexIgnore
    public final AuthApiGuestService d() {
        a aVar = new a(this);
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 0, "2.1") + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((Interceptor) aVar);
        return (AuthApiGuestService) ap2.g.a(AuthApiGuestService.class);
    }

    @DexIgnore
    public final ApplicationEventListener a(PortfolioApp portfolioApp, en2 en2, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, vj2 vj2, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository) {
        PortfolioApp portfolioApp2 = portfolioApp;
        kd4.b(portfolioApp2, "app");
        en2 en22 = en2;
        kd4.b(en22, "sharedPreferencesManager");
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        kd4.b(hybridPresetRepository2, "hybridPresetRepository");
        CategoryRepository categoryRepository2 = categoryRepository;
        kd4.b(categoryRepository2, "categoryRepository");
        WatchAppRepository watchAppRepository2 = watchAppRepository;
        kd4.b(watchAppRepository2, "watchAppRepository");
        ComplicationRepository complicationRepository2 = complicationRepository;
        kd4.b(complicationRepository2, "complicationRepository");
        MicroAppRepository microAppRepository2 = microAppRepository;
        kd4.b(microAppRepository2, "microAppRepository");
        DianaPresetRepository dianaPresetRepository2 = dianaPresetRepository;
        kd4.b(dianaPresetRepository2, "dianaPresetRepository");
        DeviceRepository deviceRepository2 = deviceRepository;
        kd4.b(deviceRepository2, "deviceRepository");
        UserRepository userRepository2 = userRepository;
        kd4.b(userRepository2, "userRepository");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        kd4.b(alarmsRepository2, "alarmsRepository");
        vj2 vj22 = vj2;
        kd4.b(vj22, "deviceSettingFactory");
        WatchFaceRepository watchFaceRepository2 = watchFaceRepository;
        kd4.b(watchFaceRepository2, "watchFaceRepository");
        WatchLocalizationRepository watchLocalizationRepository2 = watchLocalizationRepository;
        kd4.b(watchLocalizationRepository2, "watchLocalizationRepository");
        return new ApplicationEventListener(portfolioApp2, en22, hybridPresetRepository2, categoryRepository2, watchAppRepository2, complicationRepository2, microAppRepository2, dianaPresetRepository2, deviceRepository2, userRepository2, vj22, alarmsRepository2, watchFaceRepository2, watchLocalizationRepository2);
    }

    @DexIgnore
    public final WatchParamHelper a(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        kd4.b(deviceRepository, "deviceRepository");
        kd4.b(portfolioApp, "app");
        return new WatchParamHelper(deviceRepository, portfolioApp);
    }

    @DexIgnore
    public final AnalyticsHelper a() {
        return AnalyticsHelper.f.c();
    }

    @DexIgnore
    public final AlarmHelper a(Context context, en2 en2, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        kd4.b(context, "context");
        kd4.b(en2, "sharedPreferencesManager");
        kd4.b(userRepository, "userRepository");
        kd4.b(alarmsRepository, "alarmsRepository");
        return new AlarmHelper(context, en2, userRepository, alarmsRepository);
    }

    @DexIgnore
    public final zk2 a(Context context, h42 h42, en2 en2) {
        kd4.b(context, "context");
        kd4.b(h42, "appExecutors");
        kd4.b(en2, "sharedPreferencesManager");
        return new zk2(context, h42.b(), en2);
    }

    @DexIgnore
    public final <S> S a(xo2 xo2, bp2 bp2, Class<S> cls) {
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 0) + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((hl4) bp2);
        ap2.g.a((Interceptor) xo2);
        return ap2.g.a(cls);
    }

    @DexIgnore
    public final ApiServiceV2 a(xo2 xo2, bp2 bp2) {
        kd4.b(xo2, "interceptor");
        kd4.b(bp2, "authenticator");
        ap2 ap2 = ap2.g;
        ap2.a(ur3.b.a(this.a, 0, "2") + ZendeskConfig.SLASH);
        ap2 ap22 = ap2.g;
        File cacheDir = this.a.getCacheDir();
        kd4.a((Object) cacheDir, "mApplication.cacheDir");
        ap22.a(cacheDir);
        ap2.g.a((hl4) bp2);
        ap2.g.a((Interceptor) xo2);
        return (ApiServiceV2) ap2.g.a(ApiServiceV2.class);
    }

    @DexIgnore
    public final MigrationManager a(en2 en2, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, vj2 vj2, AlarmsRepository alarmsRepository) {
        kd4.b(en2, "mSharedPrefs");
        UserRepository userRepository2 = userRepository;
        kd4.b(userRepository2, "mUserRepository");
        GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase2 = getHybridDeviceSettingUseCase;
        kd4.b(getHybridDeviceSettingUseCase2, "getHybridUseCase");
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        kd4.b(notificationsRepository2, "mNotificationRepository");
        PortfolioApp portfolioApp2 = portfolioApp;
        kd4.b(portfolioApp2, "mApp");
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        kd4.b(goalTrackingRepository2, "goalTrackingRepo");
        GoalTrackingDatabase goalTrackingDatabase2 = goalTrackingDatabase;
        kd4.b(goalTrackingDatabase2, "goalTrackingDatabase");
        DeviceDao deviceDao2 = deviceDao;
        kd4.b(deviceDao2, "deviceDao");
        HybridCustomizeDatabase hybridCustomizeDatabase2 = hybridCustomizeDatabase;
        kd4.b(hybridCustomizeDatabase2, "mHybridCustomizeDatabase");
        MicroAppLastSettingRepository microAppLastSettingRepository2 = microAppLastSettingRepository;
        kd4.b(microAppLastSettingRepository2, "microAppLastSettingRepository");
        vj2 vj22 = vj2;
        kd4.b(vj22, "deviceSettingFactory");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        kd4.b(alarmsRepository2, "alarmsRepository");
        return new MigrationManager(en2, userRepository2, notificationsRepository2, getHybridDeviceSettingUseCase2, portfolioApp2, goalTrackingRepository2, goalTrackingDatabase2, deviceDao2, hybridCustomizeDatabase2, microAppLastSettingRepository2, vj22, alarmsRepository2);
    }

    @DexIgnore
    public final b62 a(en2 en2, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        kd4.b(en2, "mSharedPrefs");
        kd4.b(userRepository, "userRepository");
        kd4.b(hybridPresetDao, "hybridPresetDao");
        kd4.b(notificationsRepository, "notificationsRepository");
        kd4.b(deviceDao, "deviceDao");
        kd4.b(portfolioApp, "app");
        return new b62(en2, userRepository, hybridPresetDao, deviceDao, notificationsRepository, portfolioApp);
    }

    @DexIgnore
    public final MigrationHelper a(en2 en2, MigrationManager migrationManager, b62 b62) {
        kd4.b(en2, "mSharedPrefs");
        kd4.b(migrationManager, "migrationManager");
        kd4.b(b62, "legacyMigrationManager");
        return new MigrationHelper(en2, migrationManager, b62);
    }

    @DexIgnore
    public final LinkStreamingManager a(HybridPresetRepository hybridPresetRepository, vy2 vy2, px2 px2, NotificationSettingsDatabase notificationSettingsDatabase, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, en2 en2, AlarmsRepository alarmsRepository, SetNotificationUseCase setNotificationUseCase) {
        kd4.b(hybridPresetRepository, "hybridPresetRepository");
        kd4.b(vy2, "getApps");
        kd4.b(px2, "getAllContactGroups");
        kd4.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        NotificationsRepository notificationsRepository2 = notificationsRepository;
        kd4.b(notificationsRepository2, "notificationsRepository");
        DeviceRepository deviceRepository2 = deviceRepository;
        kd4.b(deviceRepository2, "deviceRepository");
        en2 en22 = en2;
        kd4.b(en22, "sharedPref");
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        kd4.b(alarmsRepository2, "alarmsRepository");
        SetNotificationUseCase setNotificationUseCase2 = setNotificationUseCase;
        kd4.b(setNotificationUseCase2, "setNotificationUseCase");
        return new LinkStreamingManager(hybridPresetRepository, vy2, px2, notificationSettingsDatabase, notificationsRepository2, deviceRepository2, en22, alarmsRepository2, setNotificationUseCase2);
    }

    @DexIgnore
    public final ak3 a(InAppNotificationRepository inAppNotificationRepository) {
        kd4.b(inAppNotificationRepository, "repository");
        return new ak3(inAppNotificationRepository);
    }
}
