package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.obfuscated.iy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zx3 extends wx3 {
    @DexIgnore
    public zx3(Context context) {
        super(context);
    }

    @DexIgnore
    public boolean a(gy3 gy3) {
        return "file".equals(gy3.d.getScheme());
    }

    @DexIgnore
    public iy3.a a(gy3 gy3, int i) throws IOException {
        return new iy3.a((Bitmap) null, c(gy3), Picasso.LoadedFrom.DISK, a(gy3.d));
    }

    @DexIgnore
    public static int a(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE;
        }
        if (attributeInt == 6) {
            return 90;
        }
        if (attributeInt != 8) {
            return 0;
        }
        return BackgroundImageConfig.LEFT_BACKGROUND_ANGLE;
    }
}
