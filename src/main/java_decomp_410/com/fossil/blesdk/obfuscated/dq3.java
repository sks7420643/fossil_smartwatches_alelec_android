package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dq3 implements Factory<bq3> {
    @DexIgnore
    public static bq3 a(cq3 cq3) {
        bq3 a = cq3.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
