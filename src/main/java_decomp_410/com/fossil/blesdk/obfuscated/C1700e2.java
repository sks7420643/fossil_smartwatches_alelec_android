package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e2 */
public class C1700e2 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.widget.ImageView f4675a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C3256x2 f4676b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C3256x2 f4677c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3256x2 f4678d;

    @DexIgnore
    public C1700e2(android.widget.ImageView imageView) {
        this.f4675a = imageView;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10287a(android.util.AttributeSet attributeSet, int i) {
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17205a(this.f4675a.getContext(), attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView, i, 0);
        try {
            android.graphics.drawable.Drawable drawable = this.f4675a.getDrawable();
            if (drawable == null) {
                int g = a.mo18431g(com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView_srcCompat, -1);
                if (g != -1) {
                    drawable = com.fossil.blesdk.obfuscated.C2364m0.m10497c(this.f4675a.getContext(), g);
                    if (drawable != null) {
                        this.f4675a.setImageDrawable(drawable);
                    }
                }
            }
            if (drawable != null) {
                com.fossil.blesdk.obfuscated.C2181k2.m9313b(drawable);
            }
            if (a.mo18432g(com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView_tint)) {
                com.fossil.blesdk.obfuscated.C3267x9.m16200a(this.f4675a, a.mo18416a(com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView_tint));
            }
            if (a.mo18432g(com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView_tintMode)) {
                com.fossil.blesdk.obfuscated.C3267x9.m16201a(this.f4675a, com.fossil.blesdk.obfuscated.C2181k2.m9311a(a.mo18425d(com.fossil.blesdk.obfuscated.C1368a0.AppCompatImageView_tintMode, -1), (android.graphics.PorterDuff.Mode) null));
            }
        } finally {
            a.mo18418a();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.res.ColorStateList mo10289b() {
        com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4677c;
        if (x2Var != null) {
            return x2Var.f10818a;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public android.graphics.PorterDuff.Mode mo10290c() {
        com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4677c;
        if (x2Var != null) {
            return x2Var.f10819b;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo10291d() {
        return android.os.Build.VERSION.SDK_INT < 21 || !(this.f4675a.getBackground() instanceof android.graphics.drawable.RippleDrawable);
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo10292e() {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.f4676b != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10284a(int i) {
        if (i != 0) {
            android.graphics.drawable.Drawable c = com.fossil.blesdk.obfuscated.C2364m0.m10497c(this.f4675a.getContext(), i);
            if (c != null) {
                com.fossil.blesdk.obfuscated.C2181k2.m9313b(c);
            }
            this.f4675a.setImageDrawable(c);
        } else {
            this.f4675a.setImageDrawable((android.graphics.drawable.Drawable) null);
        }
        mo10283a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10285a(android.content.res.ColorStateList colorStateList) {
        if (this.f4677c == null) {
            this.f4677c = new com.fossil.blesdk.obfuscated.C3256x2();
        }
        com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4677c;
        x2Var.f10818a = colorStateList;
        x2Var.f10821d = true;
        mo10283a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10286a(android.graphics.PorterDuff.Mode mode) {
        if (this.f4677c == null) {
            this.f4677c = new com.fossil.blesdk.obfuscated.C3256x2();
        }
        com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4677c;
        x2Var.f10819b = mode;
        x2Var.f10820c = true;
        mo10283a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10283a() {
        android.graphics.drawable.Drawable drawable = this.f4675a.getDrawable();
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C2181k2.m9313b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!mo10292e() || !mo10288a(drawable)) {
            com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4677c;
            if (x2Var != null) {
                com.fossil.blesdk.obfuscated.C1526c2.m5219a(drawable, x2Var, this.f4675a.getDrawableState());
                return;
            }
            com.fossil.blesdk.obfuscated.C3256x2 x2Var2 = this.f4676b;
            if (x2Var2 != null) {
                com.fossil.blesdk.obfuscated.C1526c2.m5219a(drawable, x2Var2, this.f4675a.getDrawableState());
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo10288a(android.graphics.drawable.Drawable drawable) {
        if (this.f4678d == null) {
            this.f4678d = new com.fossil.blesdk.obfuscated.C3256x2();
        }
        com.fossil.blesdk.obfuscated.C3256x2 x2Var = this.f4678d;
        x2Var.mo17595a();
        android.content.res.ColorStateList a = com.fossil.blesdk.obfuscated.C3267x9.m16199a(this.f4675a);
        if (a != null) {
            x2Var.f10821d = true;
            x2Var.f10818a = a;
        }
        android.graphics.PorterDuff.Mode b = com.fossil.blesdk.obfuscated.C3267x9.m16202b(this.f4675a);
        if (b != null) {
            x2Var.f10820c = true;
            x2Var.f10819b = b;
        }
        if (!x2Var.f10821d && !x2Var.f10820c) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1526c2.m5219a(drawable, x2Var, this.f4675a.getDrawableState());
        return true;
    }
}
