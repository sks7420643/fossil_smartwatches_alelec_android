package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n52 implements Factory<j62> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public n52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static n52 a(n42 n42) {
        return new n52(n42);
    }

    @DexIgnore
    public static j62 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static j62 c(n42 n42) {
        j62 m = n42.m();
        n44.a(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    public j62 get() {
        return b(this.a);
    }
}
