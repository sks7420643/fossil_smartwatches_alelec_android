package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ju0 {
    @DexIgnore
    public static /* final */ gu0<?> a; // = new hu0();
    @DexIgnore
    public static /* final */ gu0<?> b; // = a();

    @DexIgnore
    public static gu0<?> a() {
        try {
            return (gu0) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static gu0<?> b() {
        return a;
    }

    @DexIgnore
    public static gu0<?> c() {
        gu0<?> gu0 = b;
        if (gu0 != null) {
            return gu0;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
