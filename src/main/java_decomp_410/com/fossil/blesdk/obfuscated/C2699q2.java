package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q2 */
public class C2699q2 extends android.content.res.Resources {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.res.Resources f8506a;

    @DexIgnore
    public C2699q2(android.content.res.Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f8506a = resources;
    }

    @DexIgnore
    public android.content.res.XmlResourceParser getAnimation(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getAnimation(i);
    }

    @DexIgnore
    public boolean getBoolean(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getBoolean(i);
    }

    @DexIgnore
    public int getColor(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getColor(i);
    }

    @DexIgnore
    public android.content.res.ColorStateList getColorStateList(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getColorStateList(i);
    }

    @DexIgnore
    public android.content.res.Configuration getConfiguration() {
        return this.f8506a.getConfiguration();
    }

    @DexIgnore
    public float getDimension(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDimension(i);
    }

    @DexIgnore
    public int getDimensionPixelOffset(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDimensionPixelOffset(i);
    }

    @DexIgnore
    public int getDimensionPixelSize(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDimensionPixelSize(i);
    }

    @DexIgnore
    public android.util.DisplayMetrics getDisplayMetrics() {
        return this.f8506a.getDisplayMetrics();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawable(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDrawable(i);
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawableForDensity(int i, int i2) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDrawableForDensity(i, i2);
    }

    @DexIgnore
    public float getFraction(int i, int i2, int i3) {
        return this.f8506a.getFraction(i, i2, i3);
    }

    @DexIgnore
    public int getIdentifier(java.lang.String str, java.lang.String str2, java.lang.String str3) {
        return this.f8506a.getIdentifier(str, str2, str3);
    }

    @DexIgnore
    public int[] getIntArray(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getIntArray(i);
    }

    @DexIgnore
    public int getInteger(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getInteger(i);
    }

    @DexIgnore
    public android.content.res.XmlResourceParser getLayout(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getLayout(i);
    }

    @DexIgnore
    public android.graphics.Movie getMovie(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getMovie(i);
    }

    @DexIgnore
    public java.lang.String getQuantityString(int i, int i2, java.lang.Object... objArr) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getQuantityString(i, i2, objArr);
    }

    @DexIgnore
    public java.lang.CharSequence getQuantityText(int i, int i2) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getQuantityText(i, i2);
    }

    @DexIgnore
    public java.lang.String getResourceEntryName(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getResourceEntryName(i);
    }

    @DexIgnore
    public java.lang.String getResourceName(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getResourceName(i);
    }

    @DexIgnore
    public java.lang.String getResourcePackageName(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getResourcePackageName(i);
    }

    @DexIgnore
    public java.lang.String getResourceTypeName(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getResourceTypeName(i);
    }

    @DexIgnore
    public java.lang.String getString(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getString(i);
    }

    @DexIgnore
    public java.lang.String[] getStringArray(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getStringArray(i);
    }

    @DexIgnore
    public java.lang.CharSequence getText(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getText(i);
    }

    @DexIgnore
    public java.lang.CharSequence[] getTextArray(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getTextArray(i);
    }

    @DexIgnore
    public void getValue(int i, android.util.TypedValue typedValue, boolean z) throws android.content.res.Resources.NotFoundException {
        this.f8506a.getValue(i, typedValue, z);
    }

    @DexIgnore
    public void getValueForDensity(int i, int i2, android.util.TypedValue typedValue, boolean z) throws android.content.res.Resources.NotFoundException {
        this.f8506a.getValueForDensity(i, i2, typedValue, z);
    }

    @DexIgnore
    public android.content.res.XmlResourceParser getXml(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getXml(i);
    }

    @DexIgnore
    public android.content.res.TypedArray obtainAttributes(android.util.AttributeSet attributeSet, int[] iArr) {
        return this.f8506a.obtainAttributes(attributeSet, iArr);
    }

    @DexIgnore
    public android.content.res.TypedArray obtainTypedArray(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.obtainTypedArray(i);
    }

    @DexIgnore
    public java.io.InputStream openRawResource(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.openRawResource(i);
    }

    @DexIgnore
    public android.content.res.AssetFileDescriptor openRawResourceFd(int i) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.openRawResourceFd(i);
    }

    @DexIgnore
    public void parseBundleExtra(java.lang.String str, android.util.AttributeSet attributeSet, android.os.Bundle bundle) throws org.xmlpull.v1.XmlPullParserException {
        this.f8506a.parseBundleExtra(str, attributeSet, bundle);
    }

    @DexIgnore
    public void parseBundleExtras(android.content.res.XmlResourceParser xmlResourceParser, android.os.Bundle bundle) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        this.f8506a.parseBundleExtras(xmlResourceParser, bundle);
    }

    @DexIgnore
    public void updateConfiguration(android.content.res.Configuration configuration, android.util.DisplayMetrics displayMetrics) {
        super.updateConfiguration(configuration, displayMetrics);
        android.content.res.Resources resources = this.f8506a;
        if (resources != null) {
            resources.updateConfiguration(configuration, displayMetrics);
        }
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawable(int i, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getDrawable(i, theme);
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawableForDensity(int i, int i2, android.content.res.Resources.Theme theme) {
        return this.f8506a.getDrawableForDensity(i, i2, theme);
    }

    @DexIgnore
    public java.lang.String getQuantityString(int i, int i2) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getQuantityString(i, i2);
    }

    @DexIgnore
    public java.lang.String getString(int i, java.lang.Object... objArr) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.getString(i, objArr);
    }

    @DexIgnore
    public java.lang.CharSequence getText(int i, java.lang.CharSequence charSequence) {
        return this.f8506a.getText(i, charSequence);
    }

    @DexIgnore
    public void getValue(java.lang.String str, android.util.TypedValue typedValue, boolean z) throws android.content.res.Resources.NotFoundException {
        this.f8506a.getValue(str, typedValue, z);
    }

    @DexIgnore
    public java.io.InputStream openRawResource(int i, android.util.TypedValue typedValue) throws android.content.res.Resources.NotFoundException {
        return this.f8506a.openRawResource(i, typedValue);
    }
}
