package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v7 */
public class C3092v7 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f10164a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public android.os.HandlerThread f10165b;

    @DexIgnore
    /* renamed from: c */
    public android.os.Handler f10166c;

    @DexIgnore
    /* renamed from: d */
    public int f10167d;

    @DexIgnore
    /* renamed from: e */
    public android.os.Handler.Callback f10168e; // = new com.fossil.blesdk.obfuscated.C3092v7.C3093a();

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f10169f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f10170g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.lang.String f10171h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.v7$a */
    public class C3093a implements android.os.Handler.Callback {
        @DexIgnore
        public C3093a() {
        }

        @DexIgnore
        public boolean handleMessage(android.os.Message message) {
            int i = message.what;
            if (i == 0) {
                com.fossil.blesdk.obfuscated.C3092v7.this.mo16991a();
                return true;
            } else if (i != 1) {
                return true;
            } else {
                com.fossil.blesdk.obfuscated.C3092v7.this.mo16992a((java.lang.Runnable) message.obj);
                return true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.v7$b */
    public class C3094b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.concurrent.Callable f10173e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ android.os.Handler f10174f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3092v7.C3097d f10175g;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v7$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.v7$b$a */
        public class C3095a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ java.lang.Object f10176e;

            @DexIgnore
            public C3095a(java.lang.Object obj) {
                this.f10176e = obj;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3092v7.C3094b.this.f10175g.mo16669a(this.f10176e);
            }
        }

        @DexIgnore
        public C3094b(com.fossil.blesdk.obfuscated.C3092v7 v7Var, java.util.concurrent.Callable callable, android.os.Handler handler, com.fossil.blesdk.obfuscated.C3092v7.C3097d dVar) {
            this.f10173e = callable;
            this.f10174f = handler;
            this.f10175g = dVar;
        }

        @DexIgnore
        public void run() {
            java.lang.Object obj;
            try {
                obj = this.f10173e.call();
            } catch (java.lang.Exception unused) {
                obj = null;
            }
            this.f10174f.post(new com.fossil.blesdk.obfuscated.C3092v7.C3094b.C3095a(obj));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.v7$c")
    /* renamed from: com.fossil.blesdk.obfuscated.v7$c */
    public class C3096c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.concurrent.atomic.AtomicReference f10178e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.concurrent.Callable f10179f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ java.util.concurrent.locks.ReentrantLock f10180g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ java.util.concurrent.atomic.AtomicBoolean f10181h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ java.util.concurrent.locks.Condition f10182i;

        @DexIgnore
        public C3096c(com.fossil.blesdk.obfuscated.C3092v7 v7Var, java.util.concurrent.atomic.AtomicReference atomicReference, java.util.concurrent.Callable callable, java.util.concurrent.locks.ReentrantLock reentrantLock, java.util.concurrent.atomic.AtomicBoolean atomicBoolean, java.util.concurrent.locks.Condition condition) {
            this.f10178e = atomicReference;
            this.f10179f = callable;
            this.f10180g = reentrantLock;
            this.f10181h = atomicBoolean;
            this.f10182i = condition;
        }

        @DexIgnore
        public void run() {
            try {
                this.f10178e.set(this.f10179f.call());
            } catch (java.lang.Exception unused) {
            }
            this.f10180g.lock();
            try {
                this.f10181h.set(false);
                this.f10182i.signal();
            } finally {
                this.f10180g.unlock();
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.v7$d */
    public interface C3097d<T> {
        @DexIgnore
        /* renamed from: a */
        void mo16669a(T t);
    }

    @DexIgnore
    public C3092v7(java.lang.String str, int i, int i2) {
        this.f10171h = str;
        this.f10170g = i;
        this.f10169f = i2;
        this.f10167d = 0;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> void mo16993a(java.util.concurrent.Callable<T> callable, com.fossil.blesdk.obfuscated.C3092v7.C3097d<T> dVar) {
        mo16994b(new com.fossil.blesdk.obfuscated.C3092v7.C3094b(this, callable, new android.os.Handler(), dVar));
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo16994b(java.lang.Runnable runnable) {
        synchronized (this.f10164a) {
            if (this.f10165b == null) {
                this.f10165b = new android.os.HandlerThread(this.f10171h, this.f10170g);
                this.f10165b.start();
                this.f10166c = new android.os.Handler(this.f10165b.getLooper(), this.f10168e);
                this.f10167d++;
            }
            this.f10166c.removeMessages(0);
            this.f10166c.sendMessage(this.f10166c.obtainMessage(1, runnable));
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(5:9|10|11|12|(4:25|14|15|16)(1:17)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003f */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[SYNTHETIC] */
    /* renamed from: a */
    public <T> T mo16990a(java.util.concurrent.Callable<T> callable, int i) throws java.lang.InterruptedException {
        java.util.concurrent.locks.ReentrantLock reentrantLock = new java.util.concurrent.locks.ReentrantLock();
        java.util.concurrent.locks.Condition newCondition = reentrantLock.newCondition();
        java.util.concurrent.atomic.AtomicReference atomicReference = new java.util.concurrent.atomic.AtomicReference();
        java.util.concurrent.atomic.AtomicBoolean atomicBoolean = new java.util.concurrent.atomic.AtomicBoolean(true);
        com.fossil.blesdk.obfuscated.C3092v7.C3096c cVar = new com.fossil.blesdk.obfuscated.C3092v7.C3096c(this, atomicReference, callable, reentrantLock, atomicBoolean, newCondition);
        mo16994b(cVar);
        reentrantLock.lock();
        try {
            if (!atomicBoolean.get()) {
                return atomicReference.get();
            }
            long nanos = java.util.concurrent.TimeUnit.MILLISECONDS.toNanos((long) i);
            do {
                nanos = newCondition.awaitNanos(nanos);
                if (atomicBoolean.get()) {
                    T t = atomicReference.get();
                    reentrantLock.unlock();
                    return t;
                }
            } while (nanos > 0);
            throw new java.lang.InterruptedException("timeout");
        } finally {
            reentrantLock.unlock();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16992a(java.lang.Runnable runnable) {
        runnable.run();
        synchronized (this.f10164a) {
            this.f10166c.removeMessages(0);
            this.f10166c.sendMessageDelayed(this.f10166c.obtainMessage(0), (long) this.f10169f);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16991a() {
        synchronized (this.f10164a) {
            if (!this.f10166c.hasMessages(1)) {
                this.f10165b.quit();
                this.f10165b = null;
                this.f10166c = null;
            }
        }
    }
}
