package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vz3 extends iz3 {
    @DexIgnore
    public vz3(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public int a() {
        return 5;
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        bundle.getString("_wxapi_payresp_prepayid");
        bundle.getString("_wxapi_payresp_returnkey");
        bundle.getString("_wxapi_payresp_extdata");
    }
}
