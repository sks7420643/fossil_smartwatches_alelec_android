package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s8 */
public final class C2865s8 {
    @DexIgnore
    /* renamed from: a */
    public static int m13554a(android.view.ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return marginLayoutParams.getMarginEnd();
        }
        return marginLayoutParams.rightMargin;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m13556b(android.view.ViewGroup.MarginLayoutParams marginLayoutParams) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return marginLayoutParams.getMarginStart();
        }
        return marginLayoutParams.leftMargin;
    }

    @DexIgnore
    /* renamed from: c */
    public static void m13558c(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            marginLayoutParams.setMarginStart(i);
        } else {
            marginLayoutParams.leftMargin = i;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13555a(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            marginLayoutParams.resolveLayoutDirection(i);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m13557b(android.view.ViewGroup.MarginLayoutParams marginLayoutParams, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            marginLayoutParams.setMarginEnd(i);
        } else {
            marginLayoutParams.rightMargin = i;
        }
    }
}
