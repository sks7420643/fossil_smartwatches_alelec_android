package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"CommitPrefEdits"})
public class sz {
    @DexIgnore
    public /* final */ g74 a;
    @DexIgnore
    public /* final */ uy b;

    @DexIgnore
    public sz(g74 g74, uy uyVar) {
        this.a = g74;
        this.b = uyVar;
    }

    @DexIgnore
    public static sz a(g74 g74, uy uyVar) {
        return new sz(g74, uyVar);
    }

    @DexIgnore
    public void a(boolean z) {
        g74 g74 = this.a;
        g74.a(g74.edit().putBoolean("always_send_reports_opt_in", z));
    }

    @DexIgnore
    public boolean a() {
        if (!this.a.get().contains("preferences_migration_complete")) {
            h74 h74 = new h74(this.b);
            if (!this.a.get().contains("always_send_reports_opt_in") && h74.get().contains("always_send_reports_opt_in")) {
                boolean z = h74.get().getBoolean("always_send_reports_opt_in", false);
                g74 g74 = this.a;
                g74.a(g74.edit().putBoolean("always_send_reports_opt_in", z));
            }
            g74 g742 = this.a;
            g742.a(g742.edit().putBoolean("preferences_migration_complete", true));
        }
        return this.a.get().getBoolean("always_send_reports_opt_in", false);
    }
}
