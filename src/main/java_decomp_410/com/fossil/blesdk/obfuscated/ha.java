package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ha implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ha> CREATOR; // = new b();
    @DexIgnore
    public static /* final */ ha f; // = new a();
    @DexIgnore
    public /* final */ Parcelable e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ha {
        @DexIgnore
        public a() {
            super((a) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Parcelable.ClassLoaderCreator<ha> {
        @DexIgnore
        public ha[] newArray(int i) {
            return new ha[i];
        }

        @DexIgnore
        public ha createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return ha.f;
            }
            throw new IllegalStateException("superState must be null");
        }

        @DexIgnore
        public ha createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, (ClassLoader) null);
        }
    }

    @DexIgnore
    public /* synthetic */ ha(a aVar) {
        this();
    }

    @DexIgnore
    public final Parcelable a() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.e, i);
    }

    @DexIgnore
    public ha() {
        this.e = null;
    }

    @DexIgnore
    public ha(Parcelable parcelable) {
        if (parcelable != null) {
            this.e = parcelable == f ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public ha(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.e = readParcelable == null ? f : readParcelable;
    }
}
