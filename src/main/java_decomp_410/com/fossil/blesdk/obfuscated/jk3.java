package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jk3 implements Factory<LoginPresenter> {
    @DexIgnore
    public static LoginPresenter a(ek3 ek3, BaseActivity baseActivity) {
        return new LoginPresenter(ek3, baseActivity);
    }
}
