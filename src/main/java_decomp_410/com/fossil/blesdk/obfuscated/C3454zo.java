package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zo */
public final class C3454zo implements com.fossil.blesdk.obfuscated.C2978to<java.io.InputStream> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream f11618a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zo$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zo$a */
    public static final class C3455a implements com.fossil.blesdk.obfuscated.C2978to.C2979a<java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1885gq f11619a;

        @DexIgnore
        public C3455a(com.fossil.blesdk.obfuscated.C1885gq gqVar) {
            this.f11619a = gqVar;
        }

        @DexIgnore
        public java.lang.Class<java.io.InputStream> getDataClass() {
            return java.io.InputStream.class;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2978to<java.io.InputStream> mo12468a(java.io.InputStream inputStream) {
            return new com.fossil.blesdk.obfuscated.C3454zo(inputStream, this.f11619a);
        }
    }

    @DexIgnore
    public C3454zo(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f11618a = new com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream(inputStream, gqVar);
        this.f11618a.mark(5242880);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12466a() {
        this.f11618a.mo4017z();
    }

    @DexIgnore
    /* renamed from: b */
    public java.io.InputStream m17476b() throws java.io.IOException {
        this.f11618a.reset();
        return this.f11618a;
    }
}
