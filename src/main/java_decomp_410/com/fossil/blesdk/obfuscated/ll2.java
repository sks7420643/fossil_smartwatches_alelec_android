package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ll2 {
    @DexIgnore
    public static /* final */ PortfolioApp a; // = PortfolioApp.W.c();
    @DexIgnore
    public static /* final */ ll2 b; // = new ll2();

    @DexIgnore
    public final SpannableString a(String str, String str2, float f) {
        kd4.b(str, "bigText");
        kd4.b(str2, "smallText");
        SpannableString spannableString = new SpannableString(str + str2);
        spannableString.setSpan(new RelativeSizeSpan(f), str.length(), str.length() + str2.length(), 0);
        return spannableString;
    }

    @DexIgnore
    public final String b(int i) {
        switch (i) {
            case 1:
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Sun);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sun)");
                return a2;
            case 2:
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Mon);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026ain_StepsToday_Text__Mon)");
                return a3;
            case 3:
                String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Tues);
                kd4.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Text__Tues)");
                return a4;
            case 4:
                String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Wed);
                kd4.a((Object) a5, "LanguageHelper.getString\u2026ain_StepsToday_Text__Wed)");
                return a5;
            case 5:
                String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Thur);
                kd4.a((Object) a6, "LanguageHelper.getString\u2026in_StepsToday_Text__Thur)");
                return a6;
            case 6:
                String a7 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Fri);
                kd4.a((Object) a7, "LanguageHelper.getString\u2026ain_StepsToday_Text__Fri)");
                return a7;
            case 7:
                String a8 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Text__Sat);
                kd4.a((Object) a8, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sat)");
                return a8;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String c(int i) {
        String b2 = il2.b(i);
        kd4.a((Object) b2, "NumberHelper.formatNumber(steps)");
        return b2;
    }

    @DexIgnore
    public final CharSequence d(int i) {
        String a2 = il2.a(i / 60);
        kd4.a((Object) a2, "NumberHelper.formatBigNumber(hours)");
        String a3 = sm2.a((Context) a, (int) R.string.Profile_MyProfileDiana_SetGoalsSleep_Label__Hr);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026_SetGoalsSleep_Label__Hr)");
        String a4 = sm2.a((Context) a, (int) R.string.Profile_MyProfileDiana_SetGoalsSleep_Label__Min);
        kd4.a((Object) a4, "LanguageHelper.getString\u2026SetGoalsSleep_Label__Min)");
        CharSequence concat = TextUtils.concat(new CharSequence[]{a(a2, a3, 0.7f), a(' ' + il2.a(i % 60), a4, 0.7f)});
        kd4.a((Object) concat, "TextUtils.concat(activeH\u2026ing, remainMinutesString)");
        return concat;
    }

    @DexIgnore
    public final char a(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final String a(int i, float f) {
        float c = il2.c(f * ((float) 100), 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getSleepDaySummaryText", "roundedPercent : " + c);
        StringBuilder sb = new StringBuilder();
        pd4 pd4 = pd4.a;
        String string = PortfolioApp.W.c().getString(i);
        kd4.a((Object) string, "PortfolioApp.instance.getString(stringId)");
        Object[] objArr = {String.valueOf(c)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(" %");
        return sb.toString();
    }

    @DexIgnore
    public final String b(String str) {
        kd4.b(str, LogBuilder.KEY_TIME);
        String string = PortfolioApp.W.c().getString(R.string.AM);
        kd4.a((Object) string, "PortfolioApp.instance.getString(R.string.AM)");
        if (qf4.a(str, string, false, 2, (Object) null)) {
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, str.length() - 2);
            kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_SleepToday_Label__Am));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        String substring2 = str.substring(0, str.length() - 2);
        kd4.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb2.append(substring2);
        sb2.append(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_Main_SleepToday_Label__Pm));
        return sb2.toString();
    }

    @DexIgnore
    public final ArrayList<String> a() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.add(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Label__12a));
        arrayList.add(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Label__6a));
        arrayList.add(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Label__12p));
        arrayList.add(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Label__6p));
        arrayList.add(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardDiana_Main_StepsToday_Label__12a_1));
        return arrayList;
    }

    @DexIgnore
    public final String a(int i) {
        switch (i) {
            case 1:
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__S);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__S)");
                return a2;
            case 2:
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__M);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__M)");
                return a3;
            case 3:
                String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__T);
                kd4.a((Object) a4, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__T)");
                return a4;
            case 4:
                String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__W);
                kd4.a((Object) a5, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__W)");
                return a5;
            case 5:
                String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__T_1);
                kd4.a((Object) a6, "LanguageHelper.getString\u2026RepeatEnabled_Label__T_1)");
                return a6;
            case 6:
                String a7 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__F);
                kd4.a((Object) a7, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__F)");
                return a7;
            case 7:
                String a8 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AddAlarm_AddAlarmRepeatEnabled_Label__S_1);
                kd4.a((Object) a8, "LanguageHelper.getString\u2026RepeatEnabled_Label__S_1)");
                return a8;
            default:
                return "";
        }
    }
}
