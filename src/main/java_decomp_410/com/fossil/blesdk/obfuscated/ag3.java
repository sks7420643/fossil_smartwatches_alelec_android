package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ag3 implements Factory<SleepDetailPresenter> {
    @DexIgnore
    public static SleepDetailPresenter a(wf3 wf3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        return new SleepDetailPresenter(wf3, sleepSummariesRepository, sleepSessionsRepository);
    }
}
