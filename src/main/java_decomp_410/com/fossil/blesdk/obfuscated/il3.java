package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class il3 implements Factory<OnboardingHeightWeightPresenter> {
    @DexIgnore
    public static OnboardingHeightWeightPresenter a(fl3 fl3, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        return new OnboardingHeightWeightPresenter(fl3, userRepository, getRecommendedGoalUseCase);
    }
}
