package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d7 */
public interface C1603d7 {
    @DexIgnore
    void setTint(int i);

    @DexIgnore
    void setTintList(android.content.res.ColorStateList colorStateList);

    @DexIgnore
    void setTintMode(android.graphics.PorterDuff.Mode mode);
}
