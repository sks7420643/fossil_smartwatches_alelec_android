package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.LocaleList;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p7 {
    @DexIgnore
    public static /* final */ r7 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements r7 {
        @DexIgnore
        public LocaleList a; // = new LocaleList(new Locale[0]);

        @DexIgnore
        public void a(Locale... localeArr) {
            this.a = new LocaleList(localeArr);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this.a.equals(((p7) obj).a());
        }

        @DexIgnore
        public Locale get(int i) {
            return this.a.get(i);
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public Object a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements r7 {
        @DexIgnore
        public q7 a; // = new q7(new Locale[0]);

        @DexIgnore
        public void a(Locale... localeArr) {
            this.a = new q7(localeArr);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this.a.equals(((p7) obj).a());
        }

        @DexIgnore
        public Locale get(int i) {
            return this.a.a(i);
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public Object a() {
            return this.a;
        }
    }

    /*
    static {
        new p7();
        if (Build.VERSION.SDK_INT >= 24) {
            a = new a();
        } else {
            a = new b();
        }
    }
    */

    @DexIgnore
    public static p7 a(Object obj) {
        p7 p7Var = new p7();
        if (obj instanceof LocaleList) {
            p7Var.a((LocaleList) obj);
        }
        return p7Var;
    }

    @DexIgnore
    public static p7 b(Locale... localeArr) {
        p7 p7Var = new p7();
        p7Var.a(localeArr);
        return p7Var;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a.equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return a.toString();
    }

    @DexIgnore
    public Object a() {
        return a.a();
    }

    @DexIgnore
    public Locale a(int i) {
        return a.get(i);
    }

    @DexIgnore
    public final void a(LocaleList localeList) {
        int size = localeList.size();
        if (size > 0) {
            Locale[] localeArr = new Locale[size];
            for (int i = 0; i < size; i++) {
                localeArr[i] = localeList.get(i);
            }
            a.a(localeArr);
        }
    }

    @DexIgnore
    public final void a(Locale... localeArr) {
        a.a(localeArr);
    }
}
