package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewCalendar q;

    @DexIgnore
    public wf2(Object obj, View view, int i, RecyclerViewCalendar recyclerViewCalendar) {
        super(obj, view, i);
        this.q = recyclerViewCalendar;
    }
}
