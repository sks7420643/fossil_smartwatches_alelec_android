package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qg */
public class C2740qg extends android.widget.ImageView {

    @DexIgnore
    /* renamed from: e */
    public android.view.animation.Animation.AnimationListener f8664e;

    @DexIgnore
    /* renamed from: f */
    public int f8665f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qg$a */
    public class C2741a extends android.graphics.drawable.shapes.OvalShape {

        @DexIgnore
        /* renamed from: e */
        public android.graphics.RadialGradient f8666e;

        @DexIgnore
        /* renamed from: f */
        public android.graphics.Paint f8667f; // = new android.graphics.Paint();

        @DexIgnore
        public C2741a(int i) {
            com.fossil.blesdk.obfuscated.C2740qg.this.f8665f = i;
            mo15229a((int) rect().width());
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo15229a(int i) {
            float f = (float) (i / 2);
            android.graphics.RadialGradient radialGradient = new android.graphics.RadialGradient(f, f, (float) com.fossil.blesdk.obfuscated.C2740qg.this.f8665f, new int[]{1023410176, 0}, (float[]) null, android.graphics.Shader.TileMode.CLAMP);
            this.f8666e = radialGradient;
            this.f8667f.setShader(this.f8666e);
        }

        @DexIgnore
        public void draw(android.graphics.Canvas canvas, android.graphics.Paint paint) {
            int width = com.fossil.blesdk.obfuscated.C2740qg.this.getWidth() / 2;
            float f = (float) width;
            float height = (float) (com.fossil.blesdk.obfuscated.C2740qg.this.getHeight() / 2);
            canvas.drawCircle(f, height, f, this.f8667f);
            canvas.drawCircle(f, height, (float) (width - com.fossil.blesdk.obfuscated.C2740qg.this.f8665f), paint);
        }

        @DexIgnore
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            mo15229a((int) f);
        }
    }

    @DexIgnore
    public C2740qg(android.content.Context context, int i) {
        super(context);
        android.graphics.drawable.ShapeDrawable shapeDrawable;
        float f = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES * f);
        this.f8665f = (int) (3.5f * f);
        if (mo15224a()) {
            shapeDrawable = new android.graphics.drawable.ShapeDrawable(new android.graphics.drawable.shapes.OvalShape());
            com.fossil.blesdk.obfuscated.C1776f9.m6824b((android.view.View) this, f * 4.0f);
        } else {
            android.graphics.drawable.ShapeDrawable shapeDrawable2 = new android.graphics.drawable.ShapeDrawable(new com.fossil.blesdk.obfuscated.C2740qg.C2741a(this.f8665f));
            setLayerType(1, shapeDrawable2.getPaint());
            shapeDrawable2.getPaint().setShadowLayer((float) this.f8665f, (float) i3, (float) i2, 503316480);
            int i4 = this.f8665f;
            setPadding(i4, i4, i4, i4);
            shapeDrawable = shapeDrawable2;
        }
        shapeDrawable.getPaint().setColor(i);
        com.fossil.blesdk.obfuscated.C1776f9.m6811a((android.view.View) this, (android.graphics.drawable.Drawable) shapeDrawable);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo15224a() {
        return android.os.Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public void onAnimationEnd() {
        super.onAnimationEnd();
        android.view.animation.Animation.AnimationListener animationListener = this.f8664e;
        if (animationListener != null) {
            animationListener.onAnimationEnd(getAnimation());
        }
    }

    @DexIgnore
    public void onAnimationStart() {
        super.onAnimationStart();
        android.view.animation.Animation.AnimationListener animationListener = this.f8664e;
        if (animationListener != null) {
            animationListener.onAnimationStart(getAnimation());
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!mo15224a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.f8665f * 2), getMeasuredHeight() + (this.f8665f * 2));
        }
    }

    @DexIgnore
    public void setBackgroundColor(int i) {
        if (getBackground() instanceof android.graphics.drawable.ShapeDrawable) {
            ((android.graphics.drawable.ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15223a(android.view.animation.Animation.AnimationListener animationListener) {
        this.f8664e = animationListener;
    }
}
