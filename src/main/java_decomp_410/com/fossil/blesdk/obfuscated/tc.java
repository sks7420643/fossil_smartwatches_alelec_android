package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.media.AudioAttributes;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@TargetApi(21)
public class tc implements sc {
    @DexIgnore
    public AudioAttributes a;
    @DexIgnore
    public int b; // = -1;

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof tc)) {
            return false;
        }
        return this.a.equals(((tc) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "AudioAttributesCompat: audioattributes=" + this.a;
    }
}
