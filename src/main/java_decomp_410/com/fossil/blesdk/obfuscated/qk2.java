package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.codeword.CodeWordProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.fitness.FitnessProviderFactory;
import com.fossil.wearables.fsl.goal.GoalProviderImpl;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingProviderImpl;
import com.fossil.wearables.fsl.history.HistoryProviderImpl;
import com.fossil.wearables.fsl.keyvalue.KeyValueProviderImpl;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.db.DataLogServiceProvider;
import com.misfit.frameworks.buttonservice.db.HeartRateProvider;
import com.misfit.frameworks.buttonservice.db.HwLogProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qk2 {
    @DexIgnore
    public static /* final */ qk2 a; // = new qk2();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Context e;

        @DexIgnore
        public a(Context context) {
            this.e = context;
        }

        @DexIgnore
        public final void run() {
            Toast.makeText(this.e, "DB Exported!", 1).show();
        }
    }

    @DexIgnore
    public final String a(Context context, String str) {
        kd4.b(context, "context");
        kd4.b(str, "outputDir");
        return b(context, HwLogProvider.DB_NAME, str);
    }

    @DexIgnore
    public final String b(Context context, String str, String str2) {
        File databasePath = context.getDatabasePath(str);
        kd4.a((Object) databasePath, "context.getDatabasePath(databaseName)");
        File file = new File(databasePath.getAbsolutePath());
        File file2 = new File(str2, str);
        try {
            FileChannel channel = new FileInputStream(file).getChannel();
            kd4.a((Object) channel, "FileInputStream(currentDB).channel");
            FileChannel channel2 = new FileOutputStream(file2).getChannel();
            kd4.a((Object) channel2, "FileOutputStream(backupDB).channel");
            channel2.transferFrom(channel, 0, channel.size());
            channel.close();
            channel2.close();
            String absolutePath = file2.getAbsolutePath();
            kd4.a((Object) absolutePath, "backupDB.absolutePath");
            return absolutePath;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final List<String> a(Context context, String str, String str2) {
        Context context2 = context;
        String str3 = str;
        String str4 = str2;
        kd4.b(context2, "context");
        kd4.b(str3, ButtonService.USER_ID);
        kd4.b(str4, "outputDir");
        FLogger.INSTANCE.getLocal().d("TAG", "------------ userId=" + str3 + ", pkg=" + context.getPackageName());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("------------ userId=");
        sb.append(str3);
        sb.append(", path=");
        sb.append(context2.getDatabasePath(str3 + "_" + FitnessProviderFactory.DB_NAME));
        local.d("TAG", sb.toString());
        File file = new File("/data/data/" + context.getPackageName() + "/databases/");
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("FileName: ");
                kd4.a((Object) file2, "file");
                sb2.append(file2.getName());
                local2.d("Files", sb2.toString());
            }
        }
        String[] strArr = {"alarm.db", SecondTimezoneProviderImp.DB_NAME, ContactProviderImpl.DB_NAME, "user.db", AppFilterProviderImpl.DB_NAME, FitnessProviderFactory.DB_NAME, GoalProviderImpl.DB_NAME, HistoryProviderImpl.DB_NAME, LocationProviderImpl.DB_NAME, CodeWordProviderImpl.DB_NAME, KeyValueProviderImpl.DB_NAME, "pin.db", "firmwares.db", DeviceProviderImp.DB_NAME, GoalTrackingProviderImpl.DB_NAME, "microAppSetting.db", "serverSetting.db"};
        String[] strArr2 = {DataLogServiceProvider.DB_NAME, DataFileProvider.DB_NAME, HeartRateProvider.DB_NAME, HwLogProvider.DB_NAME, "ParseOfflineStore"};
        for (String str5 : strArr) {
            if (!arrayList.contains(str5)) {
                arrayList.add(b(context2, str3 + "_" + str5, str4));
            }
        }
        arrayList.add(b(context2, "hybridCustomize.db", str4));
        arrayList.add(b(context2, "hybridCustomize.db-wal", str4));
        arrayList.add(b(context2, "dianaCustomize.db", str4));
        arrayList.add(b(context2, "dianaCustomize.db-wal", str4));
        arrayList.add(b(context2, "devices.db", str4));
        arrayList.add(b(context2, "devices.db-wal", str4));
        arrayList.add(b(context2, "category.db", str4));
        arrayList.add(b(context2, "category.db-wal", str4));
        arrayList.add(b(context2, "goalTracking.db", str4));
        arrayList.add(b(context2, "goalTracking.db-wal", str4));
        arrayList.add(b(context2, "heartRate.db", str4));
        arrayList.add(b(context2, "heartRate.db-wal", str4));
        arrayList.add(b(context2, "fitnessData.db", str4));
        arrayList.add(b(context2, "fitnessData.db-wal", str4));
        arrayList.add(b(context2, DBConstants.LOG_DB_NAME, str4));
        arrayList.add(b(context2, "log_db.db-wal", str4));
        String str6 = str3 + "_";
        arrayList.add(b(context2, str6 + FitnessProviderFactory.DB_NAME, str4));
        arrayList.add(b(context2, str6 + "fitness.db-wal", str4));
        arrayList.add(b(context2, str6 + MFSleepSessionProviderImp.DB_NAME, str4));
        arrayList.add(b(context2, str6 + "sleep.db-wal", str4));
        if (!kd4.a((Object) str3, (Object) "Anonymous")) {
            arrayList.add(b(context2, "Anonymous_fitness.db", str4));
            arrayList.add(b(context2, "Anonymous_fitness.db-wal", str4));
            arrayList.add(b(context2, "Anonymous_sleep.db", str4));
            arrayList.add(b(context2, "Anonymous_sleep.db-wal", str4));
        }
        arrayList.add(b(context2, "thirdParty.db", str4));
        arrayList.add(b(context2, "thirdParty.db-wal", str4));
        for (String b : strArr2) {
            arrayList.add(b(context2, b, str4));
        }
        ((Activity) context2).runOnUiThread(new a(context2));
        return arrayList;
    }
}
