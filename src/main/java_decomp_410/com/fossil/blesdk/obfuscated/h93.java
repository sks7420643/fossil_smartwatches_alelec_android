package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h93 implements Factory<ActivityOverviewDayPresenter> {
    @DexIgnore
    public static ActivityOverviewDayPresenter a(f93 f93, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new ActivityOverviewDayPresenter(f93, summariesRepository, activitiesRepository, workoutSessionRepository);
    }
}
