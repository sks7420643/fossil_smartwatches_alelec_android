package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Collection;
import java.util.Iterator;
import kotlin.TypeCastException;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qf4 extends pf4 {
    @DexIgnore
    public static /* synthetic */ String a(String str, char c, char c2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, c, c2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean b(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(str, str2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean c(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(str, str2, z);
    }

    @DexIgnore
    public static final String d(String str) {
        kd4.b(str, "$this$capitalize");
        if (!(str.length() > 0) || !Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        String substring = str.substring(0, 1);
        kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring != null) {
            String upperCase = substring.toUpperCase();
            kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            sb.append(upperCase);
            String substring2 = str.substring(1);
            kd4.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring2);
            return sb.toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(String str, char c, char c2, boolean z) {
        kd4.b(str, "$this$replace");
        if (!z) {
            String replace = str.replace(c, c2);
            kd4.a((Object) replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return SequencesKt___SequencesKt.a(StringsKt__StringsKt.a((CharSequence) str, new char[]{c}, z, 0, 4, (Object) null), String.valueOf(c2), (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (xc4) null, 62, (Object) null);
    }

    @DexIgnore
    public static final boolean b(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        }
        if (!z) {
            return str.equals(str2);
        }
        return str.equalsIgnoreCase(str2);
    }

    @DexIgnore
    public static final boolean c(String str, String str2, boolean z) {
        kd4.b(str, "$this$startsWith");
        kd4.b(str2, "prefix");
        if (!z) {
            return str.startsWith(str2);
        }
        return a(str, 0, str2, 0, str2.length(), z);
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, String str2, String str3, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, str2, str3, z);
    }

    @DexIgnore
    public static final String a(String str, String str2, String str3, boolean z) {
        kd4.b(str, "$this$replace");
        kd4.b(str2, "oldValue");
        kd4.b(str3, "newValue");
        return SequencesKt___SequencesKt.a(StringsKt__StringsKt.b((CharSequence) str, new String[]{str2}, z, 0, 4, (Object) null), str3, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (xc4) null, 62, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(str, str2, z);
    }

    @DexIgnore
    public static final boolean a(String str, String str2, boolean z) {
        kd4.b(str, "$this$endsWith");
        kd4.b(str2, "suffix");
        if (!z) {
            return str.endsWith(str2);
        }
        return a(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    public static final boolean a(CharSequence charSequence) {
        boolean z;
        kd4.b(charSequence, "$this$isBlank");
        if (charSequence.length() != 0) {
            yd4 b = StringsKt__StringsKt.b(charSequence);
            if (!(b instanceof Collection) || !((Collection) b).isEmpty()) {
                Iterator it = b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!ze4.a(charSequence.charAt(((mb4) it).a()))) {
                            z = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!z) {
                    return true;
                }
                return false;
            }
            z = true;
            if (!z) {
            }
        }
        return true;
    }

    @DexIgnore
    public static final boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        kd4.b(str, "$this$regionMatches");
        kd4.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
        if (!z) {
            return str.regionMatches(i, str2, i2, i3);
        }
        return str.regionMatches(z, i, str2, i2, i3);
    }
}
