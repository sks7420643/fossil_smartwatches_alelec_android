package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class io1<TResult> implements qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public rn1<TResult> c;

    @DexIgnore
    public io1(Executor executor, rn1<TResult> rn1) {
        this.a = executor;
        this.c = rn1;
    }

    @DexIgnore
    public final void onComplete(wn1<TResult> wn1) {
        synchronized (this.b) {
            if (this.c != null) {
                this.a.execute(new jo1(this, wn1));
            }
        }
    }
}
