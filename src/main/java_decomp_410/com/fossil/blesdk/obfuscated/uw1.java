package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class uw1 implements Runnable {
    @DexIgnore
    public /* final */ Map.Entry e;
    @DexIgnore
    public /* final */ yw1 f;

    @DexIgnore
    public uw1(Map.Entry entry, yw1 yw1) {
        this.e = entry;
        this.f = yw1;
    }

    @DexIgnore
    public static Runnable a(Map.Entry entry, yw1 yw1) {
        return new uw1(entry, yw1);
    }

    @DexIgnore
    public final void run() {
        ((zw1) this.e.getKey()).a(this.f);
    }
}
