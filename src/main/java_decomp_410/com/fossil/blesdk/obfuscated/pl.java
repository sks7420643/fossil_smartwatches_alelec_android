package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import androidx.work.impl.workers.ConstraintTrackingWorker;
import com.fossil.blesdk.obfuscated.aj;
import com.fossil.blesdk.obfuscated.fj;
import com.fossil.blesdk.obfuscated.hl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pl implements Runnable {
    @DexIgnore
    public static /* final */ String g; // = dj.a("EnqueueRunnable");
    @DexIgnore
    public /* final */ rj e;
    @DexIgnore
    public /* final */ nj f; // = new nj();

    @DexIgnore
    public pl(rj rjVar) {
        this.e = rjVar;
    }

    @DexIgnore
    public boolean a() {
        WorkDatabase g2 = this.e.g().g();
        g2.beginTransaction();
        try {
            boolean b = b(this.e);
            g2.setTransactionSuccessful();
            return b;
        } finally {
            g2.endTransaction();
        }
    }

    @DexIgnore
    public fj b() {
        return this.f;
    }

    @DexIgnore
    public void c() {
        tj g2 = this.e.g();
        qj.a(g2.c(), g2.g(), g2.f());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.e.h()) {
                if (a()) {
                    rl.a(this.e.g().b(), RescheduleReceiver.class, true);
                    c();
                }
                this.f.a(fj.a);
                return;
            }
            throw new IllegalStateException(String.format("WorkContinuation has cycles (%s)", new Object[]{this.e}));
        } catch (Throwable th) {
            this.f.a(new fj.b.a(th));
        }
    }

    @DexIgnore
    public static boolean b(rj rjVar) {
        List<rj> e2 = rjVar.e();
        boolean z = false;
        if (e2 != null) {
            boolean z2 = false;
            for (rj next : e2) {
                if (!next.i()) {
                    z2 |= b(next);
                } else {
                    dj.a().e(g, String.format("Already enqueued work ids (%s).", new Object[]{TextUtils.join(", ", next.c())}), new Throwable[0]);
                }
            }
            z = z2;
        }
        return a(rjVar) | z;
    }

    @DexIgnore
    public static boolean a(rj rjVar) {
        boolean a = a(rjVar.g(), rjVar.f(), (String[]) rj.a(rjVar).toArray(new String[0]), rjVar.d(), rjVar.b());
        rjVar.j();
        return a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01a8 A[LOOP:6: B:107:0x01a2->B:109:0x01a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011f  */
    public static boolean a(tj tjVar, List<? extends kj> list, String[] strArr, String str, ExistingWorkPolicy existingWorkPolicy) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j;
        int i;
        tj tjVar2 = tjVar;
        String[] strArr2 = strArr;
        String str2 = str;
        ExistingWorkPolicy existingWorkPolicy2 = existingWorkPolicy;
        long currentTimeMillis = System.currentTimeMillis();
        WorkDatabase g2 = tjVar.g();
        boolean z5 = strArr2 != null && strArr2.length > 0;
        if (z5) {
            z3 = true;
            z2 = false;
            z = false;
            for (String str3 : strArr2) {
                hl e2 = g2.d().e(str3);
                if (e2 == null) {
                    dj.a().b(g, String.format("Prerequisite %s doesn't exist; not enqueuing", new Object[]{str3}), new Throwable[0]);
                    return false;
                }
                WorkInfo.State state = e2.b;
                z3 &= state == WorkInfo.State.SUCCEEDED;
                if (state == WorkInfo.State.FAILED) {
                    z2 = true;
                } else if (state == WorkInfo.State.CANCELLED) {
                    z = true;
                }
            }
        } else {
            z3 = true;
            z2 = false;
            z = false;
        }
        boolean z6 = !TextUtils.isEmpty(str);
        if (z6 && !z5) {
            List<hl.b> a = g2.d().a(str2);
            if (!a.isEmpty()) {
                if (existingWorkPolicy2 == ExistingWorkPolicy.APPEND) {
                    zk a2 = g2.a();
                    ArrayList arrayList = new ArrayList();
                    for (hl.b next : a) {
                        if (!a2.c(next.a)) {
                            boolean z7 = (next.b == WorkInfo.State.SUCCEEDED) & z3;
                            WorkInfo.State state2 = next.b;
                            if (state2 == WorkInfo.State.FAILED) {
                                z2 = true;
                            } else if (state2 == WorkInfo.State.CANCELLED) {
                                z = true;
                            }
                            arrayList.add(next.a);
                            z3 = z7;
                        }
                    }
                    strArr2 = (String[]) arrayList.toArray(strArr2);
                    z5 = strArr2.length > 0;
                } else {
                    if (existingWorkPolicy2 == ExistingWorkPolicy.KEEP) {
                        for (hl.b bVar : a) {
                            WorkInfo.State state3 = bVar.b;
                            if (state3 == WorkInfo.State.ENQUEUED) {
                                return false;
                            }
                            if (state3 == WorkInfo.State.RUNNING) {
                                return false;
                            }
                        }
                    }
                    ol.a(str2, tjVar2, false).run();
                    il d = g2.d();
                    for (hl.b bVar2 : a) {
                        d.b(bVar2.a);
                    }
                    z4 = true;
                    for (kj kjVar : list) {
                        hl d2 = kjVar.d();
                        if (!z5 || z3) {
                            if (!d2.d()) {
                                d2.n = currentTimeMillis;
                            } else {
                                j = currentTimeMillis;
                                d2.n = 0;
                                i = Build.VERSION.SDK_INT;
                                if (i < 23 && i <= 25) {
                                    a(d2);
                                } else if (Build.VERSION.SDK_INT <= 22 && a(tjVar2, "androidx.work.impl.background.gcm.GcmScheduler")) {
                                    a(d2);
                                }
                                if (d2.b == WorkInfo.State.ENQUEUED) {
                                    z4 = true;
                                }
                                g2.d().a(d2);
                                if (z5) {
                                    for (String ykVar : strArr2) {
                                        g2.a().a(new yk(kjVar.b(), ykVar));
                                    }
                                }
                                for (String klVar : kjVar.c()) {
                                    g2.e().a(new kl(klVar, kjVar.b()));
                                }
                                if (!z6) {
                                    g2.c().a(new el(str2, kjVar.b()));
                                }
                                currentTimeMillis = j;
                            }
                        } else if (z2) {
                            d2.b = WorkInfo.State.FAILED;
                        } else if (z) {
                            d2.b = WorkInfo.State.CANCELLED;
                        } else {
                            d2.b = WorkInfo.State.BLOCKED;
                        }
                        j = currentTimeMillis;
                        i = Build.VERSION.SDK_INT;
                        if (i < 23) {
                        }
                        a(d2);
                        if (d2.b == WorkInfo.State.ENQUEUED) {
                        }
                        g2.d().a(d2);
                        if (z5) {
                        }
                        while (r3.hasNext()) {
                        }
                        if (!z6) {
                        }
                        currentTimeMillis = j;
                    }
                    return z4;
                }
            }
        }
        z4 = false;
        while (r8.hasNext()) {
        }
        return z4;
    }

    @DexIgnore
    public static void a(hl hlVar) {
        yi yiVar = hlVar.j;
        if (yiVar.f() || yiVar.i()) {
            String str = hlVar.c;
            aj.a aVar = new aj.a();
            aVar.a(hlVar.e);
            aVar.a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            hlVar.c = ConstraintTrackingWorker.class.getName();
            hlVar.e = aVar.a();
        }
    }

    @DexIgnore
    public static boolean a(tj tjVar, String str) {
        try {
            Class<?> cls = Class.forName(str);
            for (pj pjVar : tjVar.f()) {
                if (cls.isAssignableFrom(pjVar.getClass())) {
                    return true;
                }
            }
        } catch (ClassNotFoundException unused) {
        }
        return false;
    }
}
