package com.fossil.blesdk.obfuscated;

import java.util.NoSuchElementException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sf4 extends rf4 {
    @DexIgnore
    public static final char e(CharSequence charSequence) {
        kd4.b(charSequence, "$this$last");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(StringsKt__StringsKt.c(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
}
