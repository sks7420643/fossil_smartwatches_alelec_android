package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.tu */
public class C2987tu extends android.app.Fragment {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2160ju f9756e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3141vu f9757f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C2987tu> f9758g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3299xn f9759h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2987tu f9760i;

    @DexIgnore
    /* renamed from: j */
    public android.app.Fragment f9761j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tu$a")
    /* renamed from: com.fossil.blesdk.obfuscated.tu$a */
    public class C2988a implements com.fossil.blesdk.obfuscated.C3141vu {
        @DexIgnore
        public C2988a() {
        }

        @DexIgnore
        /* renamed from: a */
        public java.util.Set<com.fossil.blesdk.obfuscated.C3299xn> mo14860a() {
            java.util.Set<com.fossil.blesdk.obfuscated.C2987tu> a = com.fossil.blesdk.obfuscated.C2987tu.this.mo16547a();
            java.util.HashSet hashSet = new java.util.HashSet(a.size());
            for (com.fossil.blesdk.obfuscated.C2987tu next : a) {
                if (next.mo16556d() != null) {
                    hashSet.add(next.mo16556d());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public java.lang.String toString() {
            return super.toString() + "{fragment=" + com.fossil.blesdk.obfuscated.C2987tu.this + "}";
        }
    }

    @DexIgnore
    public C2987tu() {
        this(new com.fossil.blesdk.obfuscated.C2160ju());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16550a(com.fossil.blesdk.obfuscated.C3299xn xnVar) {
        this.f9759h = xnVar;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2160ju mo16552b() {
        return this.f9756e;
    }

    @DexIgnore
    @android.annotation.TargetApi(17)
    /* renamed from: c */
    public final android.app.Fragment mo16555c() {
        android.app.Fragment parentFragment = android.os.Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? parentFragment : this.f9761j;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3299xn mo16556d() {
        return this.f9759h;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3141vu mo16557e() {
        return this.f9757f;
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo16558f() {
        com.fossil.blesdk.obfuscated.C2987tu tuVar = this.f9760i;
        if (tuVar != null) {
            tuVar.mo16554b(this);
            this.f9760i = null;
        }
    }

    @DexIgnore
    public void onAttach(android.app.Activity activity) {
        super.onAttach(activity);
        try {
            mo16548a(activity);
        } catch (java.lang.IllegalStateException e) {
            if (android.util.Log.isLoggable("RMFragment", 5)) {
                android.util.Log.w("RMFragment", "Unable to register fragment with root", e);
            }
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.f9756e.mo12478a();
        mo16558f();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        mo16558f();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.f9756e.mo12480b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.f9756e.mo12482c();
    }

    @DexIgnore
    public java.lang.String toString() {
        return super.toString() + "{parent=" + mo16555c() + "}";
    }

    @DexIgnore
    @android.annotation.SuppressLint({"ValidFragment"})
    public C2987tu(com.fossil.blesdk.obfuscated.C2160ju juVar) {
        this.f9757f = new com.fossil.blesdk.obfuscated.C2987tu.C2988a();
        this.f9758g = new java.util.HashSet();
        this.f9756e = juVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16549a(com.fossil.blesdk.obfuscated.C2987tu tuVar) {
        this.f9758g.add(tuVar);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo16554b(com.fossil.blesdk.obfuscated.C2987tu tuVar) {
        this.f9758g.remove(tuVar);
    }

    @DexIgnore
    @android.annotation.TargetApi(17)
    /* renamed from: a */
    public java.util.Set<com.fossil.blesdk.obfuscated.C2987tu> mo16547a() {
        if (equals(this.f9760i)) {
            return java.util.Collections.unmodifiableSet(this.f9758g);
        }
        if (this.f9760i == null || android.os.Build.VERSION.SDK_INT < 17) {
            return java.util.Collections.emptySet();
        }
        java.util.HashSet hashSet = new java.util.HashSet();
        for (com.fossil.blesdk.obfuscated.C2987tu next : this.f9760i.mo16547a()) {
            if (mo16551a(next.getParentFragment())) {
                hashSet.add(next);
            }
        }
        return java.util.Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16553b(android.app.Fragment fragment) {
        this.f9761j = fragment;
        if (fragment != null && fragment.getActivity() != null) {
            mo16548a(fragment.getActivity());
        }
    }

    @DexIgnore
    @android.annotation.TargetApi(17)
    /* renamed from: a */
    public final boolean mo16551a(android.app.Fragment fragment) {
        android.app.Fragment parentFragment = getParentFragment();
        while (true) {
            android.app.Fragment parentFragment2 = fragment.getParentFragment();
            if (parentFragment2 == null) {
                return false;
            }
            if (parentFragment2.equals(parentFragment)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16548a(android.app.Activity activity) {
        mo16558f();
        this.f9760i = com.fossil.blesdk.obfuscated.C2815rn.m13272a((android.content.Context) activity).mo15650h().mo16902b(activity);
        if (!equals(this.f9760i)) {
            this.f9760i.mo16549a(this);
        }
    }
}
