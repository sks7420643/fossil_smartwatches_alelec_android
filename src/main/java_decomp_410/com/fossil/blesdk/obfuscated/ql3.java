package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ql3 implements Factory<UpdateFirmwarePresenter> {
    @DexIgnore
    public static UpdateFirmwarePresenter a(ml3 ml3, DeviceRepository deviceRepository, UserRepository userRepository, vj2 vj2, en2 en2, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        return new UpdateFirmwarePresenter(ml3, deviceRepository, userRepository, vj2, en2, updateFirmwareUsecase, downloadFirmwareByDeviceModelUsecase);
    }
}
