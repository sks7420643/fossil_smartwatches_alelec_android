package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z51 extends vb1<z51> {
    @DexIgnore
    public static volatile z51[] e;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = null;

    @DexIgnore
    public z51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static z51[] e() {
        if (e == null) {
            synchronized (zb1.b) {
                if (e == null) {
                    e = new z51[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        String str = this.c;
        if (str != null) {
            ub1.a(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            ub1.a(2, str2);
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof z51)) {
            return false;
        }
        z51 z51 = (z51) obj;
        String str = this.c;
        if (str == null) {
            if (z51.c != null) {
                return false;
            }
        } else if (!str.equals(z51.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 == null) {
            if (z51.d != null) {
                return false;
            }
        } else if (!str2.equals(z51.d)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(z51.b);
        }
        xb1 xb12 = z51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (z51.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.d;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += ub1.b(1, str);
        }
        String str2 = this.d;
        return str2 != null ? a + ub1.b(2, str2) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = tb1.b();
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
