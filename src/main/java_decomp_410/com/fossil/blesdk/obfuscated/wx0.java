package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wx0 extends py0 {
    @DexIgnore
    public wx0() {
    }

    @DexIgnore
    public /* synthetic */ wx0(dx0 dx0) {
        this();
    }

    @DexIgnore
    public final void a(Status status, long j) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(Status status, pd0 pd0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(Status status, rd0[] rd0Arr) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void a(DataHolder dataHolder) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void b(Status status, long j) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void b(Status status, pd0 pd0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void g(Status status) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final void h(Status status) {
        throw new UnsupportedOperationException();
    }
}
