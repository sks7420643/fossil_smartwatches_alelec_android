package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import com.fossil.blesdk.obfuscated.bs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zr1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ bs1 a;

        @DexIgnore
        public a(bs1 bs1) {
            this.a = bs1;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.a();
        }
    }

    @DexIgnore
    public static Animator a(bs1 bs1, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(bs1, bs1.c.a, bs1.b.b, new bs1.e[]{new bs1.e(f, f2, f3)});
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        bs1.e revealInfo = bs1.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) bs1, (int) f, (int) f2, revealInfo.c, f3);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(new Animator[]{ofObject, createCircularReveal});
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    @DexIgnore
    public static Animator.AnimatorListener a(bs1 bs1) {
        return new a(bs1);
    }
}
