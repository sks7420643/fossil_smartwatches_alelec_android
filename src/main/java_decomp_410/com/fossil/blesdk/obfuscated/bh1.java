package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bh1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ long f;
    @DexIgnore
    public /* final */ /* synthetic */ ag1 g;

    @DexIgnore
    public bh1(ag1 ag1, String str, long j) {
        this.g = ag1;
        this.e = str;
        this.f = j;
    }

    @DexIgnore
    public final void run() {
        this.g.c(this.e, this.f);
    }
}
