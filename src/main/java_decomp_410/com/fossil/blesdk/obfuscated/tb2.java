package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.FrameLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tb2 extends sb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public /* final */ FrameLayout A;
    @DexIgnore
    public long B;

    /*
    static {
        D.put(R.id.cl_reset_pw, 1);
        D.put(R.id.iv_back_to_login, 2);
        D.put(R.id.scroll_layout, 3);
        D.put(R.id.ftv_title, 4);
        D.put(R.id.ftv_desc, 5);
        D.put(R.id.input_email, 6);
        D.put(R.id.et_email, 7);
        D.put(R.id.divider, 8);
        D.put(R.id.ftv_sign_up, 9);
        D.put(R.id.fb_send_link, 10);
        D.put(R.id.cl_reset_pw_success, 11);
        D.put(R.id.iv_back_to_reset_password, 12);
        D.put(R.id.tv_title, 13);
        D.put(R.id.iv_check, 14);
        D.put(R.id.ftv_sent_email, 15);
        D.put(R.id.ftv_not_receive_email, 16);
        D.put(R.id.fb_login, 17);
    }
    */

    @DexIgnore
    public tb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 18, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public tb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[1], objArr[11], objArr[8], objArr[7], objArr[17], objArr[10], objArr[5], objArr[16], objArr[15], objArr[9], objArr[4], objArr[6], objArr[2], objArr[12], objArr[14], objArr[3], objArr[13]);
        this.B = -1;
        this.A = objArr[0];
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
