package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l04 extends o04 {
    @DexIgnore
    public m04 m; // = new m04();
    @DexIgnore
    public long n; // = -1;

    @DexIgnore
    public l04(Context context, int i, String str, k04 k04) {
        super(context, i, k04);
        this.m.a = str;
    }

    @DexIgnore
    public f a() {
        return f.CUSTOM;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        String str;
        jSONObject.put("ei", this.m.a);
        long j = this.n;
        if (j > 0) {
            jSONObject.put("du", j);
        }
        Object obj = this.m.b;
        if (obj == null) {
            h();
            obj = this.m.c;
            str = "kv";
        } else {
            str = "ar";
        }
        jSONObject.put(str, obj);
        return true;
    }

    @DexIgnore
    public m04 g() {
        return this.m;
    }

    @DexIgnore
    public final void h() {
        String str = this.m.a;
        if (str != null) {
            Properties d = j04.d(str);
            if (d != null && d.size() > 0) {
                JSONObject jSONObject = this.m.c;
                if (jSONObject == null || jSONObject.length() == 0) {
                    this.m.c = new JSONObject(d);
                    return;
                }
                for (Map.Entry entry : d.entrySet()) {
                    try {
                        this.m.c.put(entry.getKey().toString(), entry.getValue());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
