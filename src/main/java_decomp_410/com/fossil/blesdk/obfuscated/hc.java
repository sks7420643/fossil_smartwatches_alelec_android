package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hc {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements cc<X> {
        @DexIgnore
        public /* final */ /* synthetic */ ac a;
        @DexIgnore
        public /* final */ /* synthetic */ m3 b;

        @DexIgnore
        public a(ac acVar, m3 m3Var) {
            this.a = acVar;
            this.b = m3Var;
        }

        @DexIgnore
        public void a(X x) {
            this.a.b(this.b.apply(x));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements cc<X> {
        @DexIgnore
        public LiveData<Y> a;
        @DexIgnore
        public /* final */ /* synthetic */ m3 b;
        @DexIgnore
        public /* final */ /* synthetic */ ac c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements cc<Y> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void a(Y y) {
                b.this.c.b(y);
            }
        }

        @DexIgnore
        public b(m3 m3Var, ac acVar) {
            this.b = m3Var;
            this.c = acVar;
        }

        @DexIgnore
        public void a(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.a(liveData2);
                }
                this.a = liveData;
                LiveData<Y> liveData3 = this.a;
                if (liveData3 != null) {
                    this.c.a(liveData3, new a());
                }
            }
        }
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> a(LiveData<X> liveData, m3<X, Y> m3Var) {
        ac acVar = new ac();
        acVar.a(liveData, new a(acVar, m3Var));
        return acVar;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, m3<X, LiveData<Y>> m3Var) {
        ac acVar = new ac();
        acVar.a(liveData, new b(m3Var, acVar));
        return acVar;
    }
}
