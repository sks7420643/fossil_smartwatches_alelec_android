package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface sr<Model, Data> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> {
        @DexIgnore
        public /* final */ jo a;
        @DexIgnore
        public /* final */ List<jo> b;
        @DexIgnore
        public /* final */ so<Data> c;

        @DexIgnore
        public a(jo joVar, so<Data> soVar) {
            this(joVar, Collections.emptyList(), soVar);
        }

        @DexIgnore
        public a(jo joVar, List<jo> list, so<Data> soVar) {
            tw.a(joVar);
            this.a = joVar;
            tw.a(list);
            this.b = list;
            tw.a(soVar);
            this.c = soVar;
        }
    }

    @DexIgnore
    a<Data> a(Model model, int i, int i2, lo loVar);

    @DexIgnore
    boolean a(Model model);
}
