package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nv<R> extends Future<R>, bw<R> {
}
