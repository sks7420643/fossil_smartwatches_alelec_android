package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ks<T> implements oo<T> {
    @DexIgnore
    public static /* final */ oo<?> b; // = new ks();

    @DexIgnore
    public static <T> ks<T> a() {
        return (ks) b;
    }

    @DexIgnore
    public aq<T> a(Context context, aq<T> aqVar, int i, int i2) {
        return aqVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
    }
}
