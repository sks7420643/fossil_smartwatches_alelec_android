package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.o44;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q44 {
    @DexIgnore
    public static volatile q44 l;
    @DexIgnore
    public static /* final */ y44 m; // = new p44();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Map<Class<? extends v44>, v44> b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ t44<q44> d;
    @DexIgnore
    public /* final */ t44<?> e;
    @DexIgnore
    public /* final */ IdManager f;
    @DexIgnore
    public o44 g;
    @DexIgnore
    public WeakReference<Activity> h;
    @DexIgnore
    public AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ y44 j;
    @DexIgnore
    public /* final */ boolean k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends o44.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Activity activity, Bundle bundle) {
            q44.this.a(activity);
        }

        @DexIgnore
        public void c(Activity activity) {
            q44.this.a(activity);
        }

        @DexIgnore
        public void d(Activity activity) {
            q44.this.a(activity);
        }
    }

    @DexIgnore
    public q44(Context context, Map<Class<? extends v44>, v44> map, h64 h64, Handler handler, y44 y44, boolean z, t44 t44, IdManager idManager, Activity activity) {
        this.a = context;
        this.b = map;
        this.c = h64;
        this.j = y44;
        this.k = z;
        this.d = t44;
        this.e = a(map.size());
        this.f = idManager;
        a(activity);
    }

    @DexIgnore
    public static q44 d(q44 q44) {
        if (l == null) {
            synchronized (q44.class) {
                if (l == null) {
                    c(q44);
                }
            }
        }
        return l;
    }

    @DexIgnore
    public static y44 g() {
        if (l == null) {
            return m;
        }
        return l.j;
    }

    @DexIgnore
    public static boolean h() {
        if (l == null) {
            return false;
        }
        return l.k;
    }

    @DexIgnore
    public static q44 i() {
        if (l != null) {
            return l;
        }
        throw new IllegalStateException("Must Initialize Fabric before using singleton()");
    }

    @DexIgnore
    public String c() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String e() {
        return "1.4.8.32";
    }

    @DexIgnore
    public final void f() {
        this.g = new o44(this.a);
        this.g.a(new a());
        b(this.a);
    }

    @DexIgnore
    public static void c(q44 q44) {
        l = q44;
        q44.f();
    }

    @DexIgnore
    public void b(Context context) {
        StringBuilder sb;
        Future<Map<String, x44>> a2 = a(context);
        Collection<v44> d2 = d();
        z44 z44 = new z44(a2, d2);
        ArrayList<v44> arrayList = new ArrayList<>(d2);
        Collections.sort(arrayList);
        z44.a(context, this, t44.a, this.f);
        for (v44 a3 : arrayList) {
            a3.a(context, this, this.e, this.f);
        }
        z44.t();
        if (g().a("Fabric", 3)) {
            sb = new StringBuilder("Initializing ");
            sb.append(c());
            sb.append(" [Version: ");
            sb.append(e());
            sb.append("], with the following kits:\n");
        } else {
            sb = null;
        }
        for (v44 v44 : arrayList) {
            v44.f.a((i64) z44.f);
            a(this.b, v44);
            v44.t();
            if (sb != null) {
                sb.append(v44.p());
                sb.append(" [Version: ");
                sb.append(v44.r());
                sb.append("]\n");
            }
        }
        if (sb != null) {
            g().d("Fabric", sb.toString());
        }
    }

    @DexIgnore
    public static q44 a(Context context, v44... v44Arr) {
        if (l == null) {
            synchronized (q44.class) {
                if (l == null) {
                    c cVar = new c(context);
                    cVar.a(v44Arr);
                    c(cVar.a());
                }
            }
        }
        return l;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements t44 {
        @DexIgnore
        public /* final */ CountDownLatch b; // = new CountDownLatch(this.c);
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public b(int i) {
            this.c = i;
        }

        @DexIgnore
        public void a(Object obj) {
            this.b.countDown();
            if (this.b.getCount() == 0) {
                q44.this.i.set(true);
                q44.this.d.a(q44.this);
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            q44.this.d.a(exc);
        }
    }

    @DexIgnore
    public static Activity d(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        return null;
    }

    @DexIgnore
    public q44 a(Activity activity) {
        this.h = new WeakReference<>(activity);
        return this;
    }

    @DexIgnore
    public Collection<v44> d() {
        return this.b.values();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public v44[] b;
        @DexIgnore
        public h64 c;
        @DexIgnore
        public Handler d;
        @DexIgnore
        public y44 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public t44<q44> i;

        @DexIgnore
        public c(Context context) {
            if (context != null) {
                this.a = context;
                return;
            }
            throw new IllegalArgumentException("Context must not be null.");
        }

        @DexIgnore
        public c a(v44... v44Arr) {
            if (this.b == null) {
                if (!o54.a(this.a).a()) {
                    ArrayList arrayList = new ArrayList();
                    boolean z = false;
                    for (v44 v44 : v44Arr) {
                        String p = v44.p();
                        char c2 = 65535;
                        int hashCode = p.hashCode();
                        if (hashCode != 607220212) {
                            if (hashCode == 1830452504 && p.equals("com.crashlytics.sdk.android:crashlytics")) {
                                c2 = 0;
                            }
                        } else if (p.equals("com.crashlytics.sdk.android:answers")) {
                            c2 = 1;
                        }
                        if (c2 == 0 || c2 == 1) {
                            arrayList.add(v44);
                        } else if (!z) {
                            q44.g().w("Fabric", "Fabric will not initialize any kits when Firebase automatic data collection is disabled; to use Third-party kits with automatic data collection disabled, initialize these kits via non-Fabric means.");
                            z = true;
                        }
                    }
                    v44Arr = (v44[]) arrayList.toArray(new v44[0]);
                }
                this.b = v44Arr;
                return this;
            }
            throw new IllegalStateException("Kits already set.");
        }

        @DexIgnore
        public c a(boolean z) {
            this.f = z;
            return this;
        }

        @DexIgnore
        public q44 a() {
            Map map;
            if (this.c == null) {
                this.c = h64.a();
            }
            if (this.d == null) {
                this.d = new Handler(Looper.getMainLooper());
            }
            if (this.e == null) {
                if (this.f) {
                    this.e = new p44(3);
                } else {
                    this.e = new p44();
                }
            }
            if (this.h == null) {
                this.h = this.a.getPackageName();
            }
            if (this.i == null) {
                this.i = t44.a;
            }
            v44[] v44Arr = this.b;
            if (v44Arr == null) {
                map = new HashMap();
            } else {
                map = q44.b((Collection<? extends v44>) Arrays.asList(v44Arr));
            }
            Map map2 = map;
            Context applicationContext = this.a.getApplicationContext();
            return new q44(applicationContext, map2, this.c, this.d, this.e, this.f, this.i, new IdManager(applicationContext, this.h, this.g, map2.values()), q44.d(this.a));
        }
    }

    @DexIgnore
    public Activity a() {
        WeakReference<Activity> weakReference = this.h;
        if (weakReference != null) {
            return (Activity) weakReference.get();
        }
        return null;
    }

    @DexIgnore
    public void a(Map<Class<? extends v44>, v44> map, v44 v44) {
        b64 b64 = v44.j;
        if (b64 != null) {
            for (Class cls : b64.value()) {
                if (cls.isInterface()) {
                    for (v44 next : map.values()) {
                        if (cls.isAssignableFrom(next.getClass())) {
                            v44.f.a((i64) next.f);
                        }
                    }
                } else if (map.get(cls) != null) {
                    v44.f.a((i64) map.get(cls).f);
                } else {
                    throw new UnmetDependencyException("Referenced Kit was null, does the kit exist?");
                }
            }
        }
    }

    @DexIgnore
    public static <T extends v44> T a(Class<T> cls) {
        return (v44) i().b.get(cls);
    }

    @DexIgnore
    public static void a(Map<Class<? extends v44>, v44> map, Collection<? extends v44> collection) {
        for (v44 v44 : collection) {
            map.put(v44.getClass(), v44);
            if (v44 instanceof w44) {
                a(map, ((w44) v44).i());
            }
        }
    }

    @DexIgnore
    public ExecutorService b() {
        return this.c;
    }

    @DexIgnore
    public static Map<Class<? extends v44>, v44> b(Collection<? extends v44> collection) {
        HashMap hashMap = new HashMap(collection.size());
        a((Map<Class<? extends v44>, v44>) hashMap, collection);
        return hashMap;
    }

    @DexIgnore
    public t44<?> a(int i2) {
        return new b(i2);
    }

    @DexIgnore
    public Future<Map<String, x44>> a(Context context) {
        return b().submit(new s44(context.getPackageCodePath()));
    }
}
