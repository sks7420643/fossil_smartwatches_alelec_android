package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.fossil.blesdk.obfuscated.jr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lj3 extends gj3 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((fd4) null);
    @DexIgnore
    public /* final */ hj3 f;
    @DexIgnore
    public /* final */ jr2 g;
    @DexIgnore
    public /* final */ j62 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lj3.i;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.d<jr2.d, jr2.c> {
        @DexIgnore
        public /* final */ /* synthetic */ lj3 a;

        @DexIgnore
        public b(lj3 lj3) {
            this.a = lj3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(jr2.d dVar) {
            kd4.b(dVar, "successResponse");
            if (this.a.f.isActive()) {
                this.a.f.d();
                this.a.f.J0();
            }
        }

        @DexIgnore
        public void a(jr2.c cVar) {
            kd4.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lj3.j.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + cVar);
            if (this.a.f.isActive()) {
                this.a.f.d();
                int a3 = cVar.a();
                if (a3 == 400004) {
                    this.a.f.m0();
                } else if (a3 != 403005) {
                    this.a.f.e(cVar.a(), cVar.b());
                } else {
                    this.a.f.A0();
                }
            }
        }
    }

    /*
    static {
        String simpleName = lj3.class.getSimpleName();
        kd4.a((Object) simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public lj3(hj3 hj3, jr2 jr2, j62 j62) {
        kd4.b(hj3, "mView");
        kd4.b(jr2, "mChangePasswordUseCase");
        kd4.b(j62, "mUseCaseHandler");
        this.f = hj3;
        this.g = jr2;
        this.h = j62;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "presenter starts");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(String str, String str2) {
        kd4.b(str, "oldPass");
        kd4.b(str2, "newPass");
        if (!as3.b(PortfolioApp.W.c())) {
            this.f.e(601, (String) null);
            return;
        }
        this.f.e();
        this.h.a(this.g, new jr2.b(str, str2), new b(this));
    }
}
