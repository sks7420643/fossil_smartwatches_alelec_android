package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w13 {
    @DexIgnore
    public /* final */ t13 a;
    @DexIgnore
    public /* final */ int b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public w13(t13 t13, String str, int i, String str2, String str3) {
        kd4.b(t13, "mView");
        kd4.b(str, "mPresetId");
        this.a = t13;
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final t13 b() {
        return this.a;
    }
}
