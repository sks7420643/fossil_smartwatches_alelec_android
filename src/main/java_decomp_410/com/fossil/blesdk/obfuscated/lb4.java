package com.fossil.blesdk.obfuscated;

import java.util.ListIterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lb4 implements ListIterator, rd4 {
    @DexIgnore
    public static /* final */ lb4 e; // = new lb4();

    @DexIgnore
    public /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean hasNext() {
        return false;
    }

    @DexIgnore
    public boolean hasPrevious() {
        return false;
    }

    @DexIgnore
    public int nextIndex() {
        return 0;
    }

    @DexIgnore
    public int previousIndex() {
        return -1;
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void next() {
        throw new NoSuchElementException();
    }

    @DexIgnore
    public Void previous() {
        throw new NoSuchElementException();
    }
}
