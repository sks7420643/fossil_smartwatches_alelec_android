package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kp implements jo {
    @DexIgnore
    public /* final */ jo b;
    @DexIgnore
    public /* final */ jo c;

    @DexIgnore
    public kp(jo joVar, jo joVar2) {
        this.b = joVar;
        this.c = joVar2;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof kp)) {
            return false;
        }
        kp kpVar = (kp) obj;
        if (!this.b.equals(kpVar.b) || !this.c.equals(kpVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
