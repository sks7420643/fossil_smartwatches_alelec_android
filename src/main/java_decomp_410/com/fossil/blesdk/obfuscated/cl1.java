package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cl1 extends bl1 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    public cl1(dl1 dl1) {
        super(dl1);
        this.b.a(this);
    }

    @DexIgnore
    public final boolean p() {
        return this.c;
    }

    @DexIgnore
    public final void q() {
        if (!p()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public abstract boolean r();

    @DexIgnore
    public final void s() {
        if (!this.c) {
            r();
            this.b.A();
            this.c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }
}
