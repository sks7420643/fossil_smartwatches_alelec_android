package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zm */
public abstract class C3445zm implements com.fossil.blesdk.obfuscated.C1882gn {
    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public final org.apache.http.HttpResponse mo10080a(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException, com.android.volley.AuthFailureError {
        com.fossil.blesdk.obfuscated.C1815fn b = mo11693b(request, map);
        org.apache.http.message.BasicHttpResponse basicHttpResponse = new org.apache.http.message.BasicHttpResponse(new org.apache.http.message.BasicStatusLine(new org.apache.http.ProtocolVersion("HTTP", 1, 1), b.mo10973d(), ""));
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (com.fossil.blesdk.obfuscated.C2660pm next : b.mo10972c()) {
            arrayList.add(new org.apache.http.message.BasicHeader(next.mo14804a(), next.mo14805b()));
        }
        basicHttpResponse.setHeaders((org.apache.http.Header[]) arrayList.toArray(new org.apache.http.Header[arrayList.size()]));
        java.io.InputStream a = b.mo10970a();
        if (a != null) {
            org.apache.http.entity.BasicHttpEntity basicHttpEntity = new org.apache.http.entity.BasicHttpEntity();
            basicHttpEntity.setContent(a);
            basicHttpEntity.setContentLength((long) b.mo10971b());
            basicHttpResponse.setEntity(basicHttpEntity);
        }
        return basicHttpResponse;
    }

    @DexIgnore
    /* renamed from: b */
    public abstract com.fossil.blesdk.obfuscated.C1815fn mo11693b(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException, com.android.volley.AuthFailureError;
}
