package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pc0 {
    @DexIgnore
    public static pc0 c;
    @DexIgnore
    public cc0 a;
    @DexIgnore
    public GoogleSignInAccount b; // = this.a.b();

    @DexIgnore
    public pc0(Context context) {
        this.a = cc0.a(context);
        this.a.c();
    }

    @DexIgnore
    public static synchronized pc0 a(Context context) {
        pc0 b2;
        synchronized (pc0.class) {
            b2 = b(context.getApplicationContext());
        }
        return b2;
    }

    @DexIgnore
    public static synchronized pc0 b(Context context) {
        pc0 pc0;
        synchronized (pc0.class) {
            if (c == null) {
                c = new pc0(context);
            }
            pc0 = c;
        }
        return pc0;
    }

    @DexIgnore
    public final synchronized void a() {
        this.a.a();
        this.b = null;
    }

    @DexIgnore
    public final synchronized GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final synchronized void a(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        this.a.a(googleSignInAccount, googleSignInOptions);
        this.b = googleSignInAccount;
    }
}
