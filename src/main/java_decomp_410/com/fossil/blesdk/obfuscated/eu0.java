package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eu0 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a();

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static fu0 b() {
        Class<?> cls = a;
        if (cls != null) {
            try {
                return (fu0) cls.getDeclaredMethod("getEmptyRegistry", new Class[0]).invoke((Object) null, new Object[0]);
            } catch (Exception unused) {
            }
        }
        return fu0.a;
    }
}
