package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r41 extends oj0<u31> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ m41<u31> F; // = new s41(this);

    @DexIgnore
    public r41(Context context, Looper looper, ge0.b bVar, ge0.c cVar, String str, kj0 kj0) {
        super(context, looper, 23, kj0, bVar, cVar);
        this.E = str;
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof u31 ? (u31) queryLocalInterface : new v31(iBinder);
    }

    @DexIgnore
    public int i() {
        return 11925000;
    }

    @DexIgnore
    public Bundle u() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }

    @DexIgnore
    public String y() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @DexIgnore
    public String z() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }
}
