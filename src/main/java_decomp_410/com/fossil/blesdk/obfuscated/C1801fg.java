package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fg */
public final class C1801fg implements com.fossil.blesdk.obfuscated.C2125jg {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f5166e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.Object[] f5167f;

    @DexIgnore
    public C1801fg(java.lang.String str, java.lang.Object[] objArr) {
        this.f5166e = str;
        this.f5167f = objArr;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10919a(com.fossil.blesdk.obfuscated.C2019ig igVar) {
        m7008a(igVar, this.f5167f);
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo10920b() {
        return this.f5166e;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo10918a() {
        java.lang.Object[] objArr = this.f5167f;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7008a(com.fossil.blesdk.obfuscated.C2019ig igVar, java.lang.Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                java.lang.Object obj = objArr[i];
                i++;
                m7007a(igVar, i, obj);
            }
        }
    }

    @DexIgnore
    public C1801fg(java.lang.String str) {
        this(str, (java.lang.Object[]) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7007a(com.fossil.blesdk.obfuscated.C2019ig igVar, int i, java.lang.Object obj) {
        if (obj == null) {
            igVar.mo11930a(i);
        } else if (obj instanceof byte[]) {
            igVar.mo11933a(i, (byte[]) obj);
        } else if (obj instanceof java.lang.Float) {
            igVar.mo11931a(i, (double) ((java.lang.Float) obj).floatValue());
        } else if (obj instanceof java.lang.Double) {
            igVar.mo11931a(i, ((java.lang.Double) obj).doubleValue());
        } else if (obj instanceof java.lang.Long) {
            igVar.mo11934b(i, ((java.lang.Long) obj).longValue());
        } else if (obj instanceof java.lang.Integer) {
            igVar.mo11934b(i, (long) ((java.lang.Integer) obj).intValue());
        } else if (obj instanceof java.lang.Short) {
            igVar.mo11934b(i, (long) ((java.lang.Short) obj).shortValue());
        } else if (obj instanceof java.lang.Byte) {
            igVar.mo11934b(i, (long) ((java.lang.Byte) obj).byteValue());
        } else if (obj instanceof java.lang.String) {
            igVar.mo11932a(i, (java.lang.String) obj);
        } else if (obj instanceof java.lang.Boolean) {
            igVar.mo11934b(i, ((java.lang.Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new java.lang.IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte," + " string");
        }
    }
}
