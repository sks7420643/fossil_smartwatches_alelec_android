package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.IInterface;
import android.telecom.TelecomManager;
import com.facebook.appevents.internal.InAppPurchaseEventManager;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.lang.reflect.Method;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cn2 {
    @DexIgnore
    public static cn2 a;
    @DexIgnore
    public static /* final */ a b; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(cn2 cn2) {
            cn2.a = cn2;
        }

        @DexIgnore
        public final cn2 b() {
            return cn2.a;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final synchronized cn2 a() {
            cn2 b;
            if (cn2.b.b() == null) {
                cn2.b.a(new cn2((fd4) null));
            }
            b = cn2.b.b();
            if (b == null) {
                kd4.a();
                throw null;
            }
            return b;
        }
    }

    /*
    static {
        kd4.a((Object) cn2.class.getSimpleName(), "PhoneCallManager::class.java.simpleName");
    }
    */

    @DexIgnore
    public cn2() {
    }

    @DexIgnore
    @SuppressLint({"MissingPermission", "PrivateApi"})
    public final void b() {
        if (Build.VERSION.SDK_INT >= 28) {
            try {
                Object systemService = PortfolioApp.W.c().getSystemService("telecom");
                if (systemService != null) {
                    ((TelecomManager) systemService).endCall();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.telecom.TelecomManager");
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e("reject >= P", e.getStackTrace().toString());
            }
        }
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.ITelephony");
            kd4.a((Object) cls, "Class.forName(\"com.andro\u2026al.telephony.ITelephony\")");
            Class cls2 = cls.getClasses()[0];
            Class<?> cls3 = Class.forName("android.os.ServiceManager");
            kd4.a((Object) cls3, "Class.forName(\"android.os.ServiceManager\")");
            Class<?> cls4 = Class.forName("android.os.ServiceManagerNative");
            kd4.a((Object) cls4, "Class.forName(\"android.os.ServiceManagerNative\")");
            Method method = cls3.getMethod("getService", new Class[]{String.class});
            kd4.a((Object) method, "serviceManagerClass.getM\u2026ice\", String::class.java)");
            Method method2 = cls4.getMethod(InAppPurchaseEventManager.AS_INTERFACE, new Class[]{IBinder.class});
            kd4.a((Object) method2, "serviceManagerNativeClas\u2026ce\", IBinder::class.java)");
            Binder binder = new Binder();
            binder.attachInterface((IInterface) null, "fake");
            Object invoke = method.invoke(method2.invoke((Object) null, new Object[]{binder}), new Object[]{PlaceFields.PHONE});
            if (invoke != null) {
                Method method3 = cls2.getMethod(InAppPurchaseEventManager.AS_INTERFACE, new Class[]{IBinder.class});
                kd4.a((Object) method3, "telephonyStubClass.getMe\u2026ce\", IBinder::class.java)");
                Object invoke2 = method3.invoke((Object) null, new Object[]{(IBinder) invoke});
                Method method4 = cls.getMethod("endCall", new Class[0]);
                kd4.a((Object) method4, "telephonyClass.getMethod(\"endCall\")");
                method4.invoke(invoke2, new Object[0]);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.os.IBinder");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public /* synthetic */ cn2(fd4 fd4) {
        this();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    @TargetApi(26)
    public final void a() {
        Object systemService = PortfolioApp.W.c().getSystemService("telecom");
        if (systemService != null) {
            ((TelecomManager) systemService).acceptRingingCall();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.telecom.TelecomManager");
    }
}
