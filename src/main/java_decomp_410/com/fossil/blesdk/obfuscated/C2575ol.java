package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ol */
public abstract class C2575ol implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2492nj f8157e; // = new com.fossil.blesdk.obfuscated.C2492nj();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ol$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ol$a */
    public static class C2576a extends com.fossil.blesdk.obfuscated.C2575ol {

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2968tj f8158f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ java.lang.String f8159g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ boolean f8160h;

        @DexIgnore
        public C2576a(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str, boolean z) {
            this.f8158f = tjVar;
            this.f8159g = str;
            this.f8160h = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* renamed from: a */
        public void mo14439a() {
            androidx.work.impl.WorkDatabase g = this.f8158f.mo16462g();
            g.beginTransaction();
            try {
                for (java.lang.String a : g.mo3780d().mo12026c(this.f8159g)) {
                    mo14442a(this.f8158f, a);
                }
                g.setTransactionSuccessful();
                g.endTransaction();
                if (this.f8160h) {
                    mo14441a(this.f8158f);
                }
            } catch (Throwable th) {
                g.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo14439a();

    @DexIgnore
    /* renamed from: a */
    public void mo14442a(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str) {
        mo14440a(tjVar.mo16462g(), str);
        tjVar.mo16460e().mo14430d(str);
        for (com.fossil.blesdk.obfuscated.C2656pj a : tjVar.mo16461f()) {
            a.mo9222a(str);
        }
    }

    @DexIgnore
    public void run() {
        try {
            mo14439a();
            this.f8157e.mo13990a(com.fossil.blesdk.obfuscated.C1804fj.f5195a);
        } catch (Throwable th) {
            this.f8157e.mo13990a(new com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1807a(th));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14441a(com.fossil.blesdk.obfuscated.C2968tj tjVar) {
        com.fossil.blesdk.obfuscated.C2744qj.m12826a(tjVar.mo16458c(), tjVar.mo16462g(), tjVar.mo16461f());
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14440a(androidx.work.impl.WorkDatabase workDatabase, java.lang.String str) {
        com.fossil.blesdk.obfuscated.C2036il d = workDatabase.mo3780d();
        com.fossil.blesdk.obfuscated.C3443zk a = workDatabase.mo3777a();
        java.util.LinkedList linkedList = new java.util.LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            java.lang.String str2 = (java.lang.String) linkedList.remove();
            androidx.work.WorkInfo.State d2 = d.mo12028d(str2);
            if (!(d2 == androidx.work.WorkInfo.State.SUCCEEDED || d2 == androidx.work.WorkInfo.State.FAILED)) {
                d.mo12015a(androidx.work.WorkInfo.State.CANCELLED, str2);
            }
            linkedList.addAll(a.mo8779a(str2));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2575ol m11833a(java.lang.String str, com.fossil.blesdk.obfuscated.C2968tj tjVar, boolean z) {
        return new com.fossil.blesdk.obfuscated.C2575ol.C2576a(tjVar, str, z);
    }
}
