package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vc0 extends yr0 implements uc0 {
    @DexIgnore
    public vc0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    @DexIgnore
    public final void a(sc0 sc0, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel o = o();
        as0.a(o, (IInterface) sc0);
        as0.a(o, (Parcelable) googleSignInOptions);
        a(102, o);
    }

    @DexIgnore
    public final void b(sc0 sc0, GoogleSignInOptions googleSignInOptions) throws RemoteException {
        Parcel o = o();
        as0.a(o, (IInterface) sc0);
        as0.a(o, (Parcelable) googleSignInOptions);
        a(103, o);
    }
}
