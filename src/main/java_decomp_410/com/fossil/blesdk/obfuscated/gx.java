package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gx {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ e74 b;

    @DexIgnore
    public gx(Context context, e74 e74) {
        this.a = context;
        this.b = e74;
    }

    @DexIgnore
    public yx a() throws IOException {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return new yx(this.a, new dy(), new x54(), new u64(this.a, this.b.a(), "session_analytics.tap", "session_analytics_to_send"));
        }
        throw new IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
