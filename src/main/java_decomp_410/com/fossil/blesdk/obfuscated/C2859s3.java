package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s3 */
public class C2859s3 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1365a f9211a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.s3$a */
    public class C2860a extends com.fossil.blesdk.obfuscated.C2525o3 {
        @DexIgnore
        public C2860a(com.fossil.blesdk.obfuscated.C2859s3 s3Var) {
        }
    }

    @DexIgnore
    public C2859s3(com.fossil.blesdk.obfuscated.C1365a aVar) {
        this.f9211a = aVar;
        new com.fossil.blesdk.obfuscated.C2859s3.C2860a(this);
    }

    @DexIgnore
    /* renamed from: a */
    public android.os.IBinder mo15890a() {
        return this.f9211a.asBinder();
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2859s3)) {
            return false;
        }
        return ((com.fossil.blesdk.obfuscated.C2859s3) obj).mo15890a().equals(this.f9211a.asBinder());
    }

    @DexIgnore
    public int hashCode() {
        return mo15890a().hashCode();
    }
}
