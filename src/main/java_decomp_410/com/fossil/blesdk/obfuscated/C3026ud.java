package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ud */
public abstract class C3026ud<T> extends com.fossil.blesdk.obfuscated.C2307ld<java.lang.Integer, T> {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$a */
    public static class C3027a<Value> extends com.fossil.blesdk.obfuscated.C2120jd<java.lang.Integer, Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3026ud<Value> f9932a;

        @DexIgnore
        public C3027a(com.fossil.blesdk.obfuscated.C3026ud<Value> udVar) {
            this.f9932a = udVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void dispatchLoadInitial(java.lang.Integer num, int i, int i2, boolean z, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f9932a.dispatchLoadInitial(false, num == null ? 0 : num.intValue(), i, i2, executor, aVar);
        }

        @DexIgnore
        public void addInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
            this.f9932a.addInvalidatedCallback(cVar);
        }

        @DexIgnore
        public void dispatchLoadAfter(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f9932a.dispatchLoadRange(1, i + 1, i2, executor, aVar);
        }

        @DexIgnore
        public void dispatchLoadBefore(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            int i3 = i - 1;
            if (i3 < 0) {
                this.f9932a.dispatchLoadRange(2, i3, 0, executor, aVar);
                return;
            }
            int min = java.lang.Math.min(i2, i3 + 1);
            this.f9932a.dispatchLoadRange(2, (i3 - min) + 1, min, executor, aVar);
        }

        @DexIgnore
        public void invalidate() {
            this.f9932a.invalidate();
        }

        @DexIgnore
        public boolean isInvalid() {
            return this.f9932a.isInvalid();
        }

        @DexIgnore
        public <ToValue> com.fossil.blesdk.obfuscated.C2307ld<java.lang.Integer, ToValue> map(com.fossil.blesdk.obfuscated.C2374m3<Value, ToValue> m3Var) {
            throw new java.lang.UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        public <ToValue> com.fossil.blesdk.obfuscated.C2307ld<java.lang.Integer, ToValue> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<Value>, java.util.List<ToValue>> m3Var) {
            throw new java.lang.UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        public void removeInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
            this.f9932a.removeInvalidatedCallback(cVar);
        }

        @DexIgnore
        public java.lang.Integer getKey(int i, Value value) {
            return java.lang.Integer.valueOf(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$b */
    public static abstract class C3028b<T> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo16753a(java.util.List<T> list, int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$c */
    public static class C3029c<T> extends com.fossil.blesdk.obfuscated.C3026ud.C3028b<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2312d<T> f9933a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f9934b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f9935c;

        @DexIgnore
        public C3029c(com.fossil.blesdk.obfuscated.C3026ud udVar, boolean z, int i, com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> aVar) {
            this.f9933a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(udVar, 0, (java.util.concurrent.Executor) null, aVar);
            this.f9934b = z;
            this.f9935c = i;
            if (this.f9935c < 1) {
                throw new java.lang.IllegalArgumentException("Page size must be non-negative");
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16753a(java.util.List<T> list, int i, int i2) {
            if (!this.f9933a.mo13247a()) {
                com.fossil.blesdk.obfuscated.C2307ld.C2312d.m10136a(list, i, i2);
                if (list.size() + i != i2 && list.size() % this.f9935c != 0) {
                    throw new java.lang.IllegalArgumentException("PositionalDataSource requires initial load size to be a multiple of page size to support internal tiling. loadSize " + list.size() + ", position " + i + ", totalCount " + i2 + ", pageSize " + this.f9935c);
                } else if (this.f9934b) {
                    this.f9933a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, i, (i2 - i) - list.size(), 0));
                } else {
                    this.f9933a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, i));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$d")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$d */
    public static class C3030d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f9936a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f9937b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f9938c;

        @DexIgnore
        public C3030d(int i, int i2, int i3, boolean z) {
            this.f9936a = i;
            this.f9937b = i2;
            this.f9938c = i3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$e")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$e */
    public static abstract class C3031e<T> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo16754a(java.util.List<T> list);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$f")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$f */
    public static class C3032f<T> extends com.fossil.blesdk.obfuscated.C3026ud.C3031e<T> {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2307ld.C2312d<T> f9939a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f9940b;

        @DexIgnore
        public C3032f(com.fossil.blesdk.obfuscated.C3026ud udVar, int i, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> aVar) {
            this.f9939a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(udVar, i, executor, aVar);
            this.f9940b = i2;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16754a(java.util.List<T> list) {
            if (!this.f9939a.mo13247a()) {
                this.f9939a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, 0, 0, this.f9940b));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ud$g")
    /* renamed from: com.fossil.blesdk.obfuscated.ud$g */
    public static class C3033g {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f9941a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f9942b;

        @DexIgnore
        public C3033g(int i, int i2) {
            this.f9941a = i;
            this.f9942b = i2;
        }
    }

    @DexIgnore
    public static int computeInitialLoadPosition(com.fossil.blesdk.obfuscated.C3026ud.C3030d dVar, int i) {
        int i2 = dVar.f9936a;
        int i3 = dVar.f9937b;
        int i4 = dVar.f9938c;
        return java.lang.Math.max(0, java.lang.Math.min(((((i - i3) + i4) - 1) / i4) * i4, (i2 / i4) * i4));
    }

    @DexIgnore
    public static int computeInitialLoadSize(com.fossil.blesdk.obfuscated.C3026ud.C3030d dVar, int i, int i2) {
        return java.lang.Math.min(i2 - i, dVar.f9937b);
    }

    @DexIgnore
    public final void dispatchLoadInitial(boolean z, int i, int i2, int i3, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> aVar) {
        com.fossil.blesdk.obfuscated.C3026ud.C3029c cVar = new com.fossil.blesdk.obfuscated.C3026ud.C3029c(this, z, i3, aVar);
        loadInitial(new com.fossil.blesdk.obfuscated.C3026ud.C3030d(i, i2, i3, z), cVar);
        cVar.f9933a.mo13246a(executor);
    }

    @DexIgnore
    public final void dispatchLoadRange(int i, int i2, int i3, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> aVar) {
        com.fossil.blesdk.obfuscated.C3026ud.C3032f fVar = new com.fossil.blesdk.obfuscated.C3026ud.C3032f(this, i, i2, executor, aVar);
        if (i3 == 0) {
            fVar.mo16754a(java.util.Collections.emptyList());
        } else {
            loadRange(new com.fossil.blesdk.obfuscated.C3026ud.C3033g(i2, i3), fVar);
        }
    }

    @DexIgnore
    public boolean isContiguous() {
        return false;
    }

    @DexIgnore
    public abstract void loadInitial(com.fossil.blesdk.obfuscated.C3026ud.C3030d dVar, com.fossil.blesdk.obfuscated.C3026ud.C3028b<T> bVar);

    @DexIgnore
    public abstract void loadRange(com.fossil.blesdk.obfuscated.C3026ud.C3033g gVar, com.fossil.blesdk.obfuscated.C3026ud.C3031e<T> eVar);

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2120jd<java.lang.Integer, T> wrapAsContiguousWithoutPlaceholders() {
        return new com.fossil.blesdk.obfuscated.C3026ud.C3027a(this);
    }

    @DexIgnore
    public final <V> com.fossil.blesdk.obfuscated.C3026ud<V> map(com.fossil.blesdk.obfuscated.C2374m3<T, V> m3Var) {
        return mapByPage((com.fossil.blesdk.obfuscated.C2374m3) com.fossil.blesdk.obfuscated.C2307ld.createListFunction(m3Var));
    }

    @DexIgnore
    public final <V> com.fossil.blesdk.obfuscated.C3026ud<V> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<T>, java.util.List<V>> m3Var) {
        return new com.fossil.blesdk.obfuscated.C3427zd(this, m3Var);
    }
}
