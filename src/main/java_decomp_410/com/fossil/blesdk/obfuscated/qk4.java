package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qk4 extends xh4 implements uk4, Executor {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater i; // = AtomicIntegerFieldUpdater.newUpdater(qk4.class, "inFlightTasks");
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<Runnable> e; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ ok4 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ TaskMode h;
    @DexIgnore
    public volatile int inFlightTasks; // = 0;

    @DexIgnore
    public qk4(ok4 ok4, int i2, TaskMode taskMode) {
        kd4.b(ok4, "dispatcher");
        kd4.b(taskMode, "taskMode");
        this.f = ok4;
        this.g = i2;
        this.h = taskMode;
    }

    @DexIgnore
    public void A() {
        Runnable poll = this.e.poll();
        if (poll != null) {
            this.f.a(poll, this, true);
            return;
        }
        i.decrementAndGet(this);
        Runnable poll2 = this.e.poll();
        if (poll2 != null) {
            a(poll2, true);
        }
    }

    @DexIgnore
    public TaskMode B() {
        return this.h;
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        a(runnable, false);
    }

    @DexIgnore
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        kd4.b(runnable, Constants.COMMAND);
        a(runnable, false);
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "[dispatcher = " + this.f + ']';
    }

    @DexIgnore
    public final void a(Runnable runnable, boolean z) {
        while (i.incrementAndGet(this) > this.g) {
            this.e.add(runnable);
            if (i.decrementAndGet(this) < this.g) {
                runnable = this.e.poll();
                if (runnable == null) {
                    return;
                }
            } else {
                return;
            }
        }
        this.f.a(runnable, this, z);
    }
}
