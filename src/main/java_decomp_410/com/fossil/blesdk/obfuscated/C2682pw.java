package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pw */
public final class C2682pw {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ double f8470a;

    /*
    static {
        double d = 1.0d;
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            d = 1.0d / java.lang.Math.pow(10.0d, 6.0d);
        }
        f8470a = d;
    }
    */

    @DexIgnore
    @android.annotation.TargetApi(17)
    /* renamed from: a */
    public static long m12452a() {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return android.os.SystemClock.elapsedRealtimeNanos();
        }
        return android.os.SystemClock.uptimeMillis();
    }

    @DexIgnore
    /* renamed from: a */
    public static double m12451a(long j) {
        return ((double) (m12452a() - j)) * f8470a;
    }
}
