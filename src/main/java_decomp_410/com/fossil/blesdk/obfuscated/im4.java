package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class im4 implements Runnable {
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public im4(String str, Object... objArr) {
        this.e = jm4.a(str, objArr);
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public final void run() {
        String name = Thread.currentThread().getName();
        Thread.currentThread().setName(this.e);
        try {
            b();
        } finally {
            Thread.currentThread().setName(name);
        }
    }
}
