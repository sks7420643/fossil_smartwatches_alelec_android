package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qb0 {
    @DexIgnore
    public static /* final */ de0.g<cs0> a; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0.g<ic0> b; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0.a<cs0, a> c; // = new gd0();
    @DexIgnore
    public static /* final */ de0.a<ic0, GoogleSignInOptions> d; // = new hd0();
    @DexIgnore
    public static /* final */ de0<GoogleSignInOptions> e; // = new de0<>("Auth.GOOGLE_SIGN_IN_API", d, b);
    @DexIgnore
    public static /* final */ wb0 f; // = new hc0();

    @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class a implements de0.d.e {
        @DexIgnore
        public /* final */ boolean e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qb0$a$a")
        @Deprecated
        /* renamed from: com.fossil.blesdk.obfuscated.qb0$a$a  reason: collision with other inner class name */
        public static class C0029a {
            @DexIgnore
            public Boolean a; // = false;

            @DexIgnore
            public a a() {
                return new a(this);
            }
        }

        /*
        static {
            new C0029a().a();
        }
        */

        @DexIgnore
        public a(C0029a aVar) {
            this.e = aVar.a.booleanValue();
        }

        @DexIgnore
        public final Bundle a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", (String) null);
            bundle.putBoolean("force_save_dialog", this.e);
            return bundle;
        }
    }

    /*
    static {
        de0<sb0> de0 = rb0.c;
        new de0("Auth.CREDENTIALS_API", c, a);
        ub0 ub0 = rb0.d;
        new bs0();
    }
    */
}
