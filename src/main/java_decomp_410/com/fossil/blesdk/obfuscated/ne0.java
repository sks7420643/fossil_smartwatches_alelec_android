package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.me0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ne0<R extends me0> {
    @DexIgnore
    void onResult(R r);
}
