package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class s13 extends u52 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(DianaCustomizeViewModel dianaCustomizeViewModel);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(String str, String str2);

    @DexIgnore
    public abstract void c(String str, String str2);

    @DexIgnore
    public abstract void d(String str, String str2);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();
}
