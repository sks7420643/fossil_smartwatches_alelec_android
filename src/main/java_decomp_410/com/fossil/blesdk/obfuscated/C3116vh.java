package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vh */
public class C3116vh {
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3038uh m15288a(android.view.ViewGroup viewGroup) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return new com.fossil.blesdk.obfuscated.C2966th(viewGroup);
        }
        return com.fossil.blesdk.obfuscated.C2878sh.m13714a(viewGroup);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15289a(android.view.ViewGroup viewGroup, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            com.fossil.blesdk.obfuscated.C3289xh.m16298a(viewGroup, z);
        } else {
            com.fossil.blesdk.obfuscated.C3223wh.m15821a(viewGroup, z);
        }
    }
}
