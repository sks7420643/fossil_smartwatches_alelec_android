package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yq0 extends u21 implements xq0 {
    @DexIgnore
    public yq0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.gcm.INetworkTaskCallback");
    }

    @DexIgnore
    public final void d(int i) throws RemoteException {
        Parcel o = o();
        o.writeInt(i);
        a(2, o);
    }
}
