package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q9 */
public class C2711q9 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.accessibility.AccessibilityNodeInfo f8572a;

    @DexIgnore
    /* renamed from: b */
    public int f8573b; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q9$a")
    /* renamed from: com.fossil.blesdk.obfuscated.q9$a */
    public static class C2712a {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ com.fossil.blesdk.obfuscated.C2711q9.C2712a f8574b; // = new com.fossil.blesdk.obfuscated.C2711q9.C2712a(1, (java.lang.CharSequence) null);

        @DexIgnore
        /* renamed from: c */
        public static /* final */ com.fossil.blesdk.obfuscated.C2711q9.C2712a f8575c; // = new com.fossil.blesdk.obfuscated.C2711q9.C2712a(2, (java.lang.CharSequence) null);

        @DexIgnore
        /* renamed from: d */
        public static /* final */ com.fossil.blesdk.obfuscated.C2711q9.C2712a f8576d; // = new com.fossil.blesdk.obfuscated.C2711q9.C2712a(16, (java.lang.CharSequence) null);

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Object f8577a;

        /*
        static {
            android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(4, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(8, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(32, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(64, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(128, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(256, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(1024, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(2048, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(4096, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(8192, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(32768, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(65536, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(131072, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(262144, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(524288, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(1048576, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(com.sina.weibo.sdk.api.ImageObject.DATA_SIZE, (java.lang.CharSequence) null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 23 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 24 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 26 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null);
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(android.os.Build.VERSION.SDK_INT >= 28 ? android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null);
            if (android.os.Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new com.fossil.blesdk.obfuscated.C2711q9.C2712a(accessibilityAction);
        }
        */

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public C2712a(int i, java.lang.CharSequence charSequence) {
            this(android.os.Build.VERSION.SDK_INT >= 21 ? new android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction(i, charSequence) : null);
        }

        @DexIgnore
        public C2712a(java.lang.Object obj) {
            this.f8577a = obj;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q9$b")
    /* renamed from: com.fossil.blesdk.obfuscated.q9$b */
    public static class C2713b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Object f8578a;

        @DexIgnore
        public C2713b(java.lang.Object obj) {
            this.f8578a = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2711q9.C2713b m12719a(int i, int i2, boolean z, int i3) {
            int i4 = android.os.Build.VERSION.SDK_INT;
            if (i4 >= 21) {
                return new com.fossil.blesdk.obfuscated.C2711q9.C2713b(android.view.accessibility.AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3));
            }
            if (i4 >= 19) {
                return new com.fossil.blesdk.obfuscated.C2711q9.C2713b(android.view.accessibility.AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new com.fossil.blesdk.obfuscated.C2711q9.C2713b((java.lang.Object) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q9$c")
    /* renamed from: com.fossil.blesdk.obfuscated.q9$c */
    public static class C2714c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Object f8579a;

        @DexIgnore
        public C2714c(java.lang.Object obj) {
            this.f8579a = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2711q9.C2714c m12720a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = android.os.Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new com.fossil.blesdk.obfuscated.C2711q9.C2714c(android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2));
            }
            if (i5 >= 19) {
                return new com.fossil.blesdk.obfuscated.C2711q9.C2714c(android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new com.fossil.blesdk.obfuscated.C2711q9.C2714c((java.lang.Object) null);
        }
    }

    @DexIgnore
    public C2711q9(android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo) {
        this.f8572a = accessibilityNodeInfo;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2711q9 m12653a(android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo) {
        return new com.fossil.blesdk.obfuscated.C2711q9(accessibilityNodeInfo);
    }

    @DexIgnore
    /* renamed from: c */
    public static java.lang.String m12655c(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            default:
                return "ACTION_UNKNOWN";
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C2711q9 m12656d(android.view.View view) {
        return m12653a(android.view.accessibility.AccessibilityNodeInfo.obtain(view));
    }

    @DexIgnore
    /* renamed from: x */
    public static com.fossil.blesdk.obfuscated.C2711q9 m12657x() {
        return m12653a(android.view.accessibility.AccessibilityNodeInfo.obtain());
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15068b() {
        return this.f8572a.getChildCount();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15079c(android.view.View view) {
        this.f8572a.setSource(view);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo15089e(boolean z) {
        this.f8572a.setClickable(z);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C2711q9.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2711q9 q9Var = (com.fossil.blesdk.obfuscated.C2711q9) obj;
        android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo = this.f8572a;
        if (accessibilityNodeInfo == null) {
            if (q9Var.f8572a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(q9Var.f8572a)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo15091f() {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return this.f8572a.getMovementGranularities();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: g */
    public java.lang.CharSequence mo15094g() {
        return this.f8572a.getPackageName();
    }

    @DexIgnore
    /* renamed from: h */
    public void mo15097h(boolean z) {
        this.f8572a.setEnabled(z);
    }

    @DexIgnore
    public int hashCode() {
        android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo = this.f8572a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    @DexIgnore
    /* renamed from: i */
    public void mo15100i(boolean z) {
        this.f8572a.setFocusable(z);
    }

    @DexIgnore
    /* renamed from: j */
    public void mo15101j(boolean z) {
        this.f8572a.setFocused(z);
    }

    @DexIgnore
    /* renamed from: k */
    public boolean mo15104k() {
        return this.f8572a.isCheckable();
    }

    @DexIgnore
    /* renamed from: l */
    public boolean mo15106l() {
        return this.f8572a.isChecked();
    }

    @DexIgnore
    /* renamed from: m */
    public void mo15107m(boolean z) {
        this.f8572a.setSelected(z);
    }

    @DexIgnore
    /* renamed from: n */
    public boolean mo15110n() {
        return this.f8572a.isEnabled();
    }

    @DexIgnore
    /* renamed from: o */
    public boolean mo15112o() {
        return this.f8572a.isFocusable();
    }

    @DexIgnore
    /* renamed from: p */
    public boolean mo15113p() {
        return this.f8572a.isFocused();
    }

    @DexIgnore
    /* renamed from: q */
    public boolean mo15114q() {
        return this.f8572a.isLongClickable();
    }

    @DexIgnore
    /* renamed from: r */
    public boolean mo15115r() {
        return this.f8572a.isPassword();
    }

    @DexIgnore
    /* renamed from: s */
    public boolean mo15116s() {
        return this.f8572a.isScrollable();
    }

    @DexIgnore
    /* renamed from: t */
    public boolean mo15117t() {
        return this.f8572a.isSelected();
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append(super.toString());
        android.graphics.Rect rect = new android.graphics.Rect();
        mo15061a(rect);
        sb.append("; boundsInParent: " + rect);
        mo15070b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(mo15094g());
        sb.append("; className: ");
        sb.append(mo15077c());
        sb.append("; text: ");
        sb.append(mo15096h());
        sb.append("; contentDescription: ");
        sb.append(mo15083d());
        sb.append("; viewId: ");
        sb.append(mo15099i());
        sb.append("; checkable: ");
        sb.append(mo15104k());
        sb.append("; checked: ");
        sb.append(mo15106l());
        sb.append("; focusable: ");
        sb.append(mo15112o());
        sb.append("; focused: ");
        sb.append(mo15113p());
        sb.append("; selected: ");
        sb.append(mo15117t());
        sb.append("; clickable: ");
        sb.append(mo15108m());
        sb.append("; longClickable: ");
        sb.append(mo15114q());
        sb.append("; enabled: ");
        sb.append(mo15110n());
        sb.append("; password: ");
        sb.append(mo15115r());
        sb.append("; scrollable: " + mo15116s());
        sb.append("; [");
        int a = mo15058a();
        while (a != 0) {
            int numberOfTrailingZeros = 1 << java.lang.Integer.numberOfTrailingZeros(a);
            a &= ~numberOfTrailingZeros;
            sb.append(m12655c(numberOfTrailingZeros));
            if (a != 0) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: u */
    public boolean mo15119u() {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return this.f8572a.isVisibleToUser();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: v */
    public void mo15120v() {
        this.f8572a.recycle();
    }

    @DexIgnore
    /* renamed from: w */
    public android.view.accessibility.AccessibilityNodeInfo mo15121w() {
        return this.f8572a;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2711q9 m12654a(com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
        return m12653a(android.view.accessibility.AccessibilityNodeInfo.obtain(q9Var.f8572a));
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo15076b(com.fossil.blesdk.obfuscated.C2711q9.C2712a aVar) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return this.f8572a.removeAction((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) aVar.f8577a);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15080c(android.view.View view, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.setSource(view, i);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo15084d(android.graphics.Rect rect) {
        this.f8572a.setBoundsInScreen(rect);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo15088e(java.lang.CharSequence charSequence) {
        this.f8572a.setPackageName(charSequence);
    }

    @DexIgnore
    /* renamed from: g */
    public void mo15095g(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f8572a.setDismissable(z);
        }
    }

    @DexIgnore
    /* renamed from: h */
    public java.lang.CharSequence mo15096h() {
        return this.f8572a.getText();
    }

    @DexIgnore
    /* renamed from: i */
    public java.lang.String mo15099i() {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return this.f8572a.getViewIdResourceName();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: j */
    public boolean mo15102j() {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return this.f8572a.isAccessibilityFocused();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: k */
    public void mo15103k(boolean z) {
        this.f8572a.setLongClickable(z);
    }

    @DexIgnore
    /* renamed from: l */
    public void mo15105l(boolean z) {
        this.f8572a.setScrollable(z);
    }

    @DexIgnore
    /* renamed from: m */
    public boolean mo15108m() {
        return this.f8572a.isClickable();
    }

    @DexIgnore
    /* renamed from: n */
    public void mo15109n(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            this.f8572a.setShowingHintText(z);
        } else {
            mo15060a(4, z);
        }
    }

    @DexIgnore
    /* renamed from: o */
    public void mo15111o(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.setVisibleToUser(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15062a(android.view.View view) {
        this.f8572a.addChild(view);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo15086d(boolean z) {
        this.f8572a.setChecked(z);
    }

    @DexIgnore
    /* renamed from: e */
    public android.os.Bundle mo15087e() {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return this.f8572a.getExtras();
        }
        return new android.os.Bundle();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo15092f(java.lang.CharSequence charSequence) {
        this.f8572a.setText(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15063a(android.view.View view, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.addChild(view, i);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15069b(int i) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.setMovementGranularities(i);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15078c(android.graphics.Rect rect) {
        this.f8572a.setBoundsInParent(rect);
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.CharSequence mo15083d() {
        return this.f8572a.getContentDescription();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo15093f(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f8572a.setContentInvalid(z);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15082c(boolean z) {
        this.f8572a.setCheckable(z);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo15085d(java.lang.CharSequence charSequence) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.f8572a.setHintText(charSequence);
        } else if (i >= 19) {
            this.f8572a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15058a() {
        return this.f8572a.getActions();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15071b(android.view.View view) {
        this.f8572a.setParent(view);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.CharSequence mo15077c() {
        return this.f8572a.getClassName();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15059a(int i) {
        this.f8572a.addAction(i);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15072b(android.view.View view, int i) {
        this.f8573b = i;
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.setParent(view, i);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo15081c(java.lang.CharSequence charSequence) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.f8572a.setError(charSequence);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15064a(com.fossil.blesdk.obfuscated.C2711q9.C2712a aVar) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.f8572a.addAction((android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction) aVar.f8577a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15061a(android.graphics.Rect rect) {
        this.f8572a.getBoundsInParent(rect);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15070b(android.graphics.Rect rect) {
        this.f8572a.getBoundsInScreen(rect);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15067a(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f8572a.setAccessibilityFocused(z);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15073b(java.lang.CharSequence charSequence) {
        this.f8572a.setContentDescription(charSequence);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15074b(java.lang.Object obj) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f8572a.setCollectionItemInfo(obj == null ? null : (android.view.accessibility.AccessibilityNodeInfo.CollectionItemInfo) ((com.fossil.blesdk.obfuscated.C2711q9.C2714c) obj).f8579a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15065a(java.lang.CharSequence charSequence) {
        this.f8572a.setClassName(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15066a(java.lang.Object obj) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f8572a.setCollectionInfo(obj == null ? null : (android.view.accessibility.AccessibilityNodeInfo.CollectionInfo) ((com.fossil.blesdk.obfuscated.C2711q9.C2713b) obj).f8578a);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15075b(boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f8572a.setCanOpenPopup(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15060a(int i, boolean z) {
        android.os.Bundle e = mo15087e();
        if (e != null) {
            int i2 = e.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (~i);
            if (!z) {
                i = 0;
            }
            e.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
