package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.mp;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bq implements mp, so.a<Object> {
    @DexIgnore
    public /* final */ mp.a e;
    @DexIgnore
    public /* final */ np<?> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public jo i;
    @DexIgnore
    public List<sr<File, ?>> j;
    @DexIgnore
    public int k;
    @DexIgnore
    public volatile sr.a<?> l;
    @DexIgnore
    public File m;
    @DexIgnore
    public cq n;

    @DexIgnore
    public bq(np<?> npVar, mp.a aVar) {
        this.f = npVar;
        this.e = aVar;
    }

    @DexIgnore
    public boolean a() {
        List<jo> c = this.f.c();
        boolean z = false;
        if (c.isEmpty()) {
            return false;
        }
        List<Class<?>> k2 = this.f.k();
        if (!k2.isEmpty()) {
            while (true) {
                if (this.j == null || !b()) {
                    this.h++;
                    if (this.h >= k2.size()) {
                        this.g++;
                        if (this.g >= c.size()) {
                            return false;
                        }
                        this.h = 0;
                    }
                    jo joVar = c.get(this.g);
                    Class cls = k2.get(this.h);
                    this.n = new cq(this.f.b(), joVar, this.f.l(), this.f.n(), this.f.f(), this.f.b(cls), cls, this.f.i());
                    this.m = this.f.d().a(this.n);
                    File file = this.m;
                    if (file != null) {
                        this.i = joVar;
                        this.j = this.f.a(file);
                        this.k = 0;
                    }
                } else {
                    this.l = null;
                    while (!z && b()) {
                        List<sr<File, ?>> list = this.j;
                        int i2 = this.k;
                        this.k = i2 + 1;
                        this.l = list.get(i2).a(this.m, this.f.n(), this.f.f(), this.f.i());
                        if (this.l != null && this.f.c(this.l.c.getDataClass())) {
                            this.l.c.a(this.f.j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (File.class.equals(this.f.m())) {
            return false;
        } else {
            throw new IllegalStateException("Failed to find any load path from " + this.f.h() + " to " + this.f.m());
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.k < this.j.size();
    }

    @DexIgnore
    public void cancel() {
        sr.a<?> aVar = this.l;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.e.a(this.i, obj, this.l.c, DataSource.RESOURCE_DISK_CACHE, this.n);
    }

    @DexIgnore
    public void a(Exception exc) {
        this.e.a(this.n, exc, this.l.c, DataSource.RESOURCE_DISK_CACHE);
    }
}
