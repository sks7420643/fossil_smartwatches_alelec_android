package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hz */
public interface C1984hz {
    @DexIgnore
    /* renamed from: a */
    void mo11795a();

    @DexIgnore
    /* renamed from: a */
    void mo11796a(long j, java.lang.String str);

    @DexIgnore
    /* renamed from: b */
    com.fossil.blesdk.obfuscated.C2610oy mo11797b();

    @DexIgnore
    /* renamed from: c */
    byte[] mo11798c();

    @DexIgnore
    /* renamed from: d */
    void mo11799d();
}
