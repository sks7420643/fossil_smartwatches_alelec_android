package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dx;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dx<T extends dx> {
    @DexIgnore
    public /* final */ ex a; // = new ex(20, 100, q44.h());
    @DexIgnore
    public /* final */ cx b; // = new cx(this.a);

    @DexIgnore
    public Map<String, Object> a() {
        return this.b.b;
    }

    @DexIgnore
    public T a(String str, String str2) {
        this.b.a(str, str2);
        return this;
    }

    @DexIgnore
    public T a(String str, Number number) {
        this.b.a(str, number);
        return this;
    }
}
