package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.Unit;
import java.util.Date;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nf3 extends v52<mf3> {
    @DexIgnore
    void a(int i, List<rt3> list, List<Triple<Integer, Pair<Integer, Float>, String>> list2);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void a(boolean z, Unit unit, qd<WorkoutSession> qdVar);

    @DexIgnore
    void c(int i, int i2);
}
