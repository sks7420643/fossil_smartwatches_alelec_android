package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yf {
    @DexIgnore
    public /* final */ int endVersion;
    @DexIgnore
    public /* final */ int startVersion;

    @DexIgnore
    public yf(int i, int i2) {
        this.startVersion = i;
        this.endVersion = i2;
    }

    @DexIgnore
    public abstract void migrate(gg ggVar);
}
