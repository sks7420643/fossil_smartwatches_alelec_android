package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r6 */
public final class C2785r6 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.r6$a")
    /* renamed from: com.fossil.blesdk.obfuscated.r6$a */
    public static abstract class C2786a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.r6$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.r6$a$a */
        public class C2787a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ android.graphics.Typeface f8866e;

            @DexIgnore
            public C2787a(android.graphics.Typeface typeface) {
                this.f8866e = typeface;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C2785r6.C2786a.this.mo751a(this.f8866e);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.r6$a$b")
        /* renamed from: com.fossil.blesdk.obfuscated.r6$a$b */
        public class C2788b implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ int f8868e;

            @DexIgnore
            public C2788b(int i) {
                this.f8868e = i;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C2785r6.C2786a.this.mo750a(this.f8868e);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public abstract void mo750a(int i);

        @DexIgnore
        /* renamed from: a */
        public abstract void mo751a(android.graphics.Typeface typeface);

        @DexIgnore
        /* renamed from: a */
        public final void mo15443a(android.graphics.Typeface typeface, android.os.Handler handler) {
            if (handler == null) {
                handler = new android.os.Handler(android.os.Looper.getMainLooper());
            }
            handler.post(new com.fossil.blesdk.obfuscated.C2785r6.C2786a.C2787a(typeface));
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo15442a(int i, android.os.Handler handler) {
            if (handler == null) {
                handler = new android.os.Handler(android.os.Looper.getMainLooper());
            }
            handler.post(new com.fossil.blesdk.obfuscated.C2785r6.C2786a.C2788b(i));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable m13075a(android.content.res.Resources resources, int i, android.content.res.Resources.Theme theme) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return resources.getDrawable(i, theme);
        }
        return resources.getDrawable(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m13071a(android.content.Context context, int i) throws android.content.res.Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return m13073a(context, i, new android.util.TypedValue(), 0, (com.fossil.blesdk.obfuscated.C2785r6.C2786a) null, (android.os.Handler) null, false);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13076a(android.content.Context context, int i, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar, android.os.Handler handler) throws android.content.res.Resources.NotFoundException {
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(aVar);
        if (context.isRestricted()) {
            aVar.mo15442a(-4, handler);
            return;
        }
        m13073a(context, i, new android.util.TypedValue(), 0, aVar, handler, false);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m13072a(android.content.Context context, int i, android.util.TypedValue typedValue, int i2, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar) throws android.content.res.Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return m13073a(context, i, typedValue, i2, aVar, (android.os.Handler) null, true);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m13073a(android.content.Context context, int i, android.util.TypedValue typedValue, int i2, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar, android.os.Handler handler, boolean z) {
        android.content.res.Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        android.graphics.Typeface a = m13074a(context, resources, typedValue, i, i2, aVar, handler, z);
        if (a != null || aVar != null) {
            return a;
        }
        throw new android.content.res.Resources.NotFoundException("Font resource ID #0x" + java.lang.Integer.toHexString(i) + " could not be retrieved.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a3  */
    /* renamed from: a */
    public static android.graphics.Typeface m13074a(android.content.Context context, android.content.res.Resources resources, android.util.TypedValue typedValue, int i, int i2, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar, android.os.Handler handler, boolean z) {
        android.content.res.Resources resources2 = resources;
        android.util.TypedValue typedValue2 = typedValue;
        int i3 = i;
        int i4 = i2;
        com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar2 = aVar;
        android.os.Handler handler2 = handler;
        java.lang.CharSequence charSequence = typedValue2.string;
        if (charSequence != null) {
            java.lang.String charSequence2 = charSequence.toString();
            if (!charSequence2.startsWith("res/")) {
                if (aVar2 != null) {
                    aVar2.mo15442a(-3, handler2);
                }
                return null;
            }
            android.graphics.Typeface b = com.fossil.blesdk.obfuscated.C3091v6.m15085b(resources2, i3, i4);
            if (b != null) {
                if (aVar2 != null) {
                    aVar2.mo15443a(b, handler2);
                }
                return b;
            }
            try {
                if (charSequence2.toLowerCase().endsWith(".xml")) {
                    com.fossil.blesdk.obfuscated.C2528o6.C2529a a = com.fossil.blesdk.obfuscated.C2528o6.m11602a((org.xmlpull.v1.XmlPullParser) resources2.getXml(i3), resources2);
                    if (a != null) {
                        return com.fossil.blesdk.obfuscated.C3091v6.m15083a(context, a, resources, i, i2, aVar, handler, z);
                    }
                    android.util.Log.e("ResourcesCompat", "Failed to find font-family tag");
                    if (aVar2 != null) {
                        aVar2.mo15442a(-3, handler2);
                    }
                    return null;
                }
                android.content.Context context2 = context;
                android.graphics.Typeface a2 = com.fossil.blesdk.obfuscated.C3091v6.m15081a(context, resources2, i3, charSequence2, i4);
                if (aVar2 != null) {
                    if (a2 != null) {
                        aVar2.mo15443a(a2, handler2);
                    } else {
                        aVar2.mo15442a(-3, handler2);
                    }
                }
                return a2;
            } catch (org.xmlpull.v1.XmlPullParserException e) {
                android.util.Log.e("ResourcesCompat", "Failed to parse xml resource " + charSequence2, e);
                if (aVar2 != null) {
                    aVar2.mo15442a(-3, handler2);
                }
                return null;
            } catch (java.io.IOException e2) {
                android.util.Log.e("ResourcesCompat", "Failed to read xml resource " + charSequence2, e2);
                if (aVar2 != null) {
                }
                return null;
            }
        } else {
            throw new android.content.res.Resources.NotFoundException("Resource \"" + resources2.getResourceName(i3) + "\" (" + java.lang.Integer.toHexString(i) + ") is not a Font: " + typedValue2);
        }
    }
}
