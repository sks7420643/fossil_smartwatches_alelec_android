package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ue2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ RecyclerView s;

    @DexIgnore
    public ue2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = recyclerView;
    }
}
