package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ns implements no<Bitmap> {
    @DexIgnore
    public static /* final */ ko<Integer> b; // = ko.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    @DexIgnore
    public static /* final */ ko<Bitmap.CompressFormat> c; // = ko.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    @DexIgnore
    public /* final */ gq a;

    @DexIgnore
    public ns(gq gqVar) {
        this.a = gqVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|(2:38|39)|40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r6 == null) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00bf */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bc A[SYNTHETIC, Splitter:B:38:0x00bc] */
    public boolean a(aq<Bitmap> aqVar, File file, lo loVar) {
        Bitmap bitmap = aqVar.get();
        Bitmap.CompressFormat a2 = a(bitmap, loVar);
        ww.a("encode: [%dx%d] %s", Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()), a2);
        try {
            long a3 = pw.a();
            int intValue = ((Integer) loVar.a(b)).intValue();
            boolean z = false;
            ro roVar = null;
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                try {
                    roVar = this.a != null ? new ro(fileOutputStream, this.a) : fileOutputStream;
                    bitmap.compress(a2, intValue, roVar);
                    roVar.close();
                    z = true;
                } catch (IOException e) {
                    e = e;
                    roVar = fileOutputStream;
                    try {
                        if (Log.isLoggable("BitmapEncoder", 3)) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (roVar != null) {
                            roVar.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    roVar = fileOutputStream;
                    if (roVar != null) {
                    }
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                if (Log.isLoggable("BitmapEncoder", 3)) {
                    Log.d("BitmapEncoder", "Failed to encode Bitmap", e);
                }
            }
            try {
                roVar.close();
            } catch (IOException unused) {
            }
            if (Log.isLoggable("BitmapEncoder", 2)) {
                Log.v("BitmapEncoder", "Compressed with type: " + a2 + " of size " + uw.a(bitmap) + " in " + pw.a(a3) + ", options format: " + loVar.a(c) + ", hasAlpha: " + bitmap.hasAlpha());
            }
            return z;
        } finally {
            ww.a();
        }
    }

    @DexIgnore
    public final Bitmap.CompressFormat a(Bitmap bitmap, lo loVar) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) loVar.a(c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @DexIgnore
    public EncodeStrategy a(lo loVar) {
        return EncodeStrategy.TRANSFORMED;
    }
}
