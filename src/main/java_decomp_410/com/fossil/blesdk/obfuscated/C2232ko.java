package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ko */
public final class C2232ko<T> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko.C2234b<java.lang.Object> f6927e; // = new com.fossil.blesdk.obfuscated.C2232ko.C2233a();

    @DexIgnore
    /* renamed from: a */
    public /* final */ T f6928a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2232ko.C2234b<T> f6929b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f6930c;

    @DexIgnore
    /* renamed from: d */
    public volatile byte[] f6931d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ko$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ko$a */
    public class C2233a implements com.fossil.blesdk.obfuscated.C2232ko.C2234b<java.lang.Object> {
        @DexIgnore
        /* renamed from: a */
        public void mo12100a(byte[] bArr, java.lang.Object obj, java.security.MessageDigest messageDigest) {
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ko$b */
    public interface C2234b<T> {
        @DexIgnore
        /* renamed from: a */
        void mo12100a(byte[] bArr, T t, java.security.MessageDigest messageDigest);
    }

    @DexIgnore
    public C2232ko(java.lang.String str, T t, com.fossil.blesdk.obfuscated.C2232ko.C2234b<T> bVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14459a(str);
        this.f6930c = str;
        this.f6928a = t;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(bVar);
        this.f6929b = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C2232ko<T> m9711a(java.lang.String str) {
        return new com.fossil.blesdk.obfuscated.C2232ko<>(str, (T) null, m9714c());
    }

    @DexIgnore
    /* renamed from: c */
    public static <T> com.fossil.blesdk.obfuscated.C2232ko.C2234b<T> m9714c() {
        return f6927e;
    }

    @DexIgnore
    /* renamed from: b */
    public final byte[] mo12841b() {
        if (this.f6931d == null) {
            this.f6931d = this.f6930c.getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a);
        }
        return this.f6931d;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C2232ko) {
            return this.f6930c.equals(((com.fossil.blesdk.obfuscated.C2232ko) obj).f6930c);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f6930c.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Option{key='" + this.f6930c + '\'' + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C2232ko<T> m9712a(java.lang.String str, T t) {
        return new com.fossil.blesdk.obfuscated.C2232ko<>(str, t, m9714c());
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C2232ko<T> m9713a(java.lang.String str, T t, com.fossil.blesdk.obfuscated.C2232ko.C2234b<T> bVar) {
        return new com.fossil.blesdk.obfuscated.C2232ko<>(str, t, bVar);
    }

    @DexIgnore
    /* renamed from: a */
    public T mo12839a() {
        return this.f6928a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12840a(T t, java.security.MessageDigest messageDigest) {
        this.f6929b.mo12100a(mo12841b(), t, messageDigest);
    }
}
