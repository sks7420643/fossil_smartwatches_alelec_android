package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y80 extends q80 {
    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ y80(short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        c(true);
        return a;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y80(short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_VERIFY_FILE, s, RequestId.LEGACY_VERIFY_FILE, peripheral, i);
        kd4.b(peripheral, "peripheral");
    }
}
