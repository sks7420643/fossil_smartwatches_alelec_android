package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zk */
public interface C3443zk {
    @DexIgnore
    /* renamed from: a */
    java.util.List<java.lang.String> mo8779a(java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    void mo8780a(com.fossil.blesdk.obfuscated.C3368yk ykVar);

    @DexIgnore
    /* renamed from: b */
    boolean mo8781b(java.lang.String str);

    @DexIgnore
    /* renamed from: c */
    boolean mo8782c(java.lang.String str);
}
