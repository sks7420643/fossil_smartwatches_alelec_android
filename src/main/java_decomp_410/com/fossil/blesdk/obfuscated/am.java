package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class am implements zl {
    @DexIgnore
    public /* final */ tl a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            am.this.b(runnable);
        }
    }

    @DexIgnore
    public am(Executor executor) {
        this.a = new tl(executor);
    }

    @DexIgnore
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    public void b(Runnable runnable) {
        this.b.post(runnable);
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    public tl b() {
        return this.a;
    }
}
