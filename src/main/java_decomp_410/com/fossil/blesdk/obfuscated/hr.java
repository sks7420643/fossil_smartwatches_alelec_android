package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hr implements ho<ByteBuffer> {
    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, File file, lo loVar) {
        try {
            kw.a(byteBuffer, file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("ByteBufferEncoder", 3)) {
                Log.d("ByteBufferEncoder", "Failed to write data", e);
            }
            return false;
        }
    }
}
