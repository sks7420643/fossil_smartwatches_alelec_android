package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;
import java.util.HashMap;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ia0 {
    @DexIgnore
    public static /* final */ HashMap<Integer, Pair<MicroAppId, MicroAppVariantId>> a; // = rb4.a((Pair<? extends K, ? extends V>[]) new Pair[]{oa4.a(3073, new Pair(MicroAppId.RING_PHONE, MicroAppVariantId.STANDARD)), oa4.a(6657, new Pair(MicroAppId.ALARM, MicroAppVariantId.STANDARD)), oa4.a(6658, new Pair(MicroAppId.ALARM, MicroAppVariantId.SEQUENCED)), oa4.a(7169, new Pair(MicroAppId.PROGRESS, MicroAppVariantId.STANDARD)), oa4.a(7170, new Pair(MicroAppId.PROGRESS, MicroAppVariantId.SWEEP)), oa4.a(7681, new Pair(MicroAppId.TWENTY_FOUR_HOUR, MicroAppVariantId.STANDARD)), oa4.a(7682, new Pair(MicroAppId.TWENTY_FOUR_HOUR, MicroAppVariantId.SEQUENCED)), oa4.a(1025, new Pair(MicroAppId.GOAL_TRACKING, MicroAppVariantId.STANDARD)), oa4.a(4097, new Pair(MicroAppId.SELFIE, MicroAppVariantId.STANDARD)), oa4.a(4609, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.PLAY_PAUSE)), oa4.a(4610, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.NEXT)), oa4.a(4611, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.PREVIOUS)), oa4.a(4612, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.VOLUME_UP)), oa4.a(4613, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.VOLUME_DOWN)), oa4.a(4614, new Pair(MicroAppId.MUSIC_CONTROL, MicroAppVariantId.STANDARD)), oa4.a(5121, new Pair(MicroAppId.DATE, MicroAppVariantId.STANDARD)), oa4.a(5122, new Pair(MicroAppId.DATE, MicroAppVariantId.SEQUENCED)), oa4.a(5633, new Pair(MicroAppId.TIME2, MicroAppVariantId.STANDARD)), oa4.a(5634, new Pair(MicroAppId.TIME2, MicroAppVariantId.SEQUENCED)), oa4.a(6145, new Pair(MicroAppId.ALERT, MicroAppVariantId.STANDARD)), oa4.a(6146, new Pair(MicroAppId.ALERT, MicroAppVariantId.SEQUENCED)), oa4.a(8193, new Pair(MicroAppId.STOPWATCH, MicroAppVariantId.STANDARD)), oa4.a(9217, new Pair(MicroAppId.COMMUTE_TIME, MicroAppVariantId.TRAVEL)), oa4.a(9218, new Pair(MicroAppId.COMMUTE_TIME, MicroAppVariantId.ETA))});
    @DexIgnore
    public static /* final */ ia0 b; // = new ia0();

    @DexIgnore
    public final MicroAppId a(short s) {
        Pair pair = a.get(Integer.valueOf(s));
        if (pair != null) {
            return (MicroAppId) pair.getFirst();
        }
        return MicroAppId.UNDEFINED;
    }

    @DexIgnore
    public final MicroAppVariantId b(short s) {
        Pair pair = a.get(Integer.valueOf(s));
        if (pair != null) {
            return (MicroAppVariantId) pair.getSecond();
        }
        return MicroAppVariantId.UNDEFINED;
    }
}
