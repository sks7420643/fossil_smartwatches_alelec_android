package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ao3 implements Factory<wn3> {
    @DexIgnore
    public static wn3 a(yn3 yn3) {
        wn3 b = yn3.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
