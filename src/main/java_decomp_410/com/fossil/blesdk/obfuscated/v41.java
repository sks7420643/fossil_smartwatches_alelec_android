package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v41 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<v41> CREATOR; // = new w41();
    @DexIgnore
    public int e;
    @DexIgnore
    public t41 f;
    @DexIgnore
    public qd1 g;
    @DexIgnore
    public r31 h;

    @DexIgnore
    public v41(int i, t41 t41, IBinder iBinder, IBinder iBinder2) {
        this.e = i;
        this.f = t41;
        r31 r31 = null;
        this.g = iBinder == null ? null : rd1.a(iBinder);
        if (!(iBinder2 == null || iBinder2 == null)) {
            IInterface queryLocalInterface = iBinder2.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            r31 = queryLocalInterface instanceof r31 ? (r31) queryLocalInterface : new t31(iBinder2);
        }
        this.h = r31;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, (Parcelable) this.f, i, false);
        qd1 qd1 = this.g;
        IBinder iBinder = null;
        kk0.a(parcel, 3, qd1 == null ? null : qd1.asBinder(), false);
        r31 r31 = this.h;
        if (r31 != null) {
            iBinder = r31.asBinder();
        }
        kk0.a(parcel, 4, iBinder, false);
        kk0.a(parcel, a);
    }
}
