package com.fossil.blesdk.obfuscated;

import io.reactivex.internal.util.ExceptionHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a94 {
    @DexIgnore
    public static RuntimeException a(Throwable th) {
        throw ExceptionHelper.a(th);
    }

    @DexIgnore
    public static void b(Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }
}
