package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x4 {
    @DexIgnore
    public ConstraintWidget a;
    @DexIgnore
    public ConstraintWidget b;
    @DexIgnore
    public ConstraintWidget c;
    @DexIgnore
    public ConstraintWidget d;
    @DexIgnore
    public ConstraintWidget e;
    @DexIgnore
    public ConstraintWidget f;
    @DexIgnore
    public ConstraintWidget g;
    @DexIgnore
    public ArrayList<ConstraintWidget> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;

    @DexIgnore
    public x4(ConstraintWidget constraintWidget, int i2, boolean z) {
        this.a = constraintWidget;
        this.l = i2;
        this.m = z;
    }

    @DexIgnore
    public static boolean a(ConstraintWidget constraintWidget, int i2) {
        if (constraintWidget.s() != 8 && constraintWidget.C[i2] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int[] iArr = constraintWidget.g;
            if (iArr[i2] == 0 || iArr[i2] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.l * 2;
        boolean z = false;
        ConstraintWidget constraintWidget = this.a;
        ConstraintWidget constraintWidget2 = constraintWidget;
        boolean z2 = false;
        while (!z2) {
            this.i++;
            ConstraintWidget[] constraintWidgetArr = constraintWidget.i0;
            int i3 = this.l;
            ConstraintWidget constraintWidget3 = null;
            constraintWidgetArr[i3] = null;
            constraintWidget.h0[i3] = null;
            if (constraintWidget.s() != 8) {
                if (this.b == null) {
                    this.b = constraintWidget;
                }
                this.d = constraintWidget;
                ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.C;
                int i4 = this.l;
                if (dimensionBehaviourArr[i4] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    int[] iArr = constraintWidget.g;
                    if (iArr[i4] == 0 || iArr[i4] == 3 || iArr[i4] == 2) {
                        this.j++;
                        float[] fArr = constraintWidget.g0;
                        int i5 = this.l;
                        float f2 = fArr[i5];
                        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            this.k += fArr[i5];
                        }
                        if (a(constraintWidget, this.l)) {
                            if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                this.n = true;
                            } else {
                                this.o = true;
                            }
                            if (this.h == null) {
                                this.h = new ArrayList<>();
                            }
                            this.h.add(constraintWidget);
                        }
                        if (this.f == null) {
                            this.f = constraintWidget;
                        }
                        ConstraintWidget constraintWidget4 = this.g;
                        if (constraintWidget4 != null) {
                            constraintWidget4.h0[this.l] = constraintWidget;
                        }
                        this.g = constraintWidget;
                    }
                }
            }
            if (constraintWidget2 != constraintWidget) {
                constraintWidget2.i0[this.l] = constraintWidget;
            }
            ConstraintAnchor constraintAnchor = constraintWidget.A[i2 + 1].d;
            if (constraintAnchor != null) {
                ConstraintWidget constraintWidget5 = constraintAnchor.b;
                ConstraintAnchor[] constraintAnchorArr = constraintWidget5.A;
                if (constraintAnchorArr[i2].d != null && constraintAnchorArr[i2].d.b == constraintWidget) {
                    constraintWidget3 = constraintWidget5;
                }
            }
            if (constraintWidget3 == null) {
                constraintWidget3 = constraintWidget;
                z2 = true;
            }
            constraintWidget2 = constraintWidget;
            constraintWidget = constraintWidget3;
        }
        this.c = constraintWidget;
        if (this.l != 0 || !this.m) {
            this.e = this.a;
        } else {
            this.e = this.c;
        }
        if (this.o && this.n) {
            z = true;
        }
        this.p = z;
    }

    @DexIgnore
    public void a() {
        if (!this.q) {
            b();
        }
        this.q = true;
    }
}
