package com.fossil.blesdk.obfuscated;

import com.zendesk.service.ErrorResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m34 implements ErrorResponse {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public m34(String str) {
        this.a = str;
    }

    @DexIgnore
    public int G() {
        return -1;
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return false;
    }
}
