package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rb4 extends qb4 {
    @DexIgnore
    public static final <K, V> HashMap<K, V> a(Pair<? extends K, ? extends V>... pairArr) {
        kd4.b(pairArr, "pairs");
        HashMap<K, V> hashMap = new HashMap<>(a(pairArr.length));
        a(hashMap, pairArr);
        return hashMap;
    }

    @DexIgnore
    public static final <K, V> V b(Map<K, ? extends V> map, K k) {
        kd4.b(map, "$this$getValue");
        return pb4.a(map, k);
    }

    @DexIgnore
    public static final int a(int i) {
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return i + (i / 3);
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public static final <K, V> void a(Map<? super K, ? super V> map, Pair<? extends K, ? extends V>[] pairArr) {
        kd4.b(map, "$this$putAll");
        kd4.b(pairArr, "pairs");
        for (Pair<? extends K, ? extends V> pair : pairArr) {
            map.put(pair.component1(), pair.component2());
        }
    }
}
