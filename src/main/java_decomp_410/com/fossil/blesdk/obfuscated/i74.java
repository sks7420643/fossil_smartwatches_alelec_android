package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import com.zendesk.sdk.attachment.AttachmentHelper;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Collection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i74 extends e54 implements n74 {
    @DexIgnore
    public i74(v44 v44, String str, String str2, z64 z64, HttpMethod httpMethod) {
        super(v44, str, str2, z64, httpMethod);
    }

    @DexIgnore
    public boolean a(l74 l74) {
        HttpRequest a = a();
        a(a, l74);
        b(a, l74);
        y44 g = q44.g();
        g.d("Fabric", "Sending app info to " + b());
        if (l74.j != null) {
            y44 g2 = q44.g();
            g2.d("Fabric", "App icon hash is " + l74.j.a);
            y44 g3 = q44.g();
            g3.d("Fabric", "App icon size is " + l74.j.c + "x" + l74.j.d);
        }
        int g4 = a.g();
        String str = "POST".equals(a.m()) ? "Create" : "Update";
        y44 g5 = q44.g();
        g5.d("Fabric", str + " app request ID: " + a.c("X-REQUEST-ID"));
        y44 g6 = q44.g();
        g6.d("Fabric", "Result was " + g4);
        return w54.a(g4) == 0;
    }

    @DexIgnore
    public final HttpRequest b(HttpRequest httpRequest, l74 l74) {
        httpRequest.e("app[identifier]", l74.b);
        httpRequest.e("app[name]", l74.f);
        httpRequest.e("app[display_version]", l74.c);
        httpRequest.e("app[build_version]", l74.d);
        httpRequest.a("app[source]", (Number) Integer.valueOf(l74.g));
        httpRequest.e("app[minimum_sdk_version]", l74.h);
        httpRequest.e("app[built_sdk_version]", l74.i);
        if (!CommonUtils.b(l74.e)) {
            httpRequest.e("app[instance_identifier]", l74.e);
        }
        if (l74.j != null) {
            InputStream inputStream = null;
            try {
                inputStream = this.e.l().getResources().openRawResource(l74.j.b);
                httpRequest.e("app[icon][hash]", l74.j.a);
                httpRequest.a("app[icon][data]", "icon.png", AttachmentHelper.DEFAULT_MIMETYPE, inputStream);
                httpRequest.a("app[icon][width]", (Number) Integer.valueOf(l74.j.c));
                httpRequest.a("app[icon][height]", (Number) Integer.valueOf(l74.j.d));
            } catch (Resources.NotFoundException e) {
                y44 g = q44.g();
                g.e("Fabric", "Failed to find app icon with resource ID: " + l74.j.b, e);
            } catch (Throwable th) {
                CommonUtils.a((Closeable) inputStream, "Failed to close app icon InputStream.");
                throw th;
            }
            CommonUtils.a((Closeable) inputStream, "Failed to close app icon InputStream.");
        }
        Collection<x44> collection = l74.k;
        if (collection != null) {
            for (x44 next : collection) {
                httpRequest.e(b(next), next.c());
                httpRequest.e(a(next), next.a());
            }
        }
        return httpRequest;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, l74 l74) {
        httpRequest.c("X-CRASHLYTICS-API-KEY", l74.a);
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        return httpRequest;
    }

    @DexIgnore
    public String a(x44 x44) {
        return String.format(Locale.US, "app[build][libraries][%s][type]", new Object[]{x44.b()});
    }

    @DexIgnore
    public String b(x44 x44) {
        return String.format(Locale.US, "app[build][libraries][%s][version]", new Object[]{x44.b()});
    }
}
