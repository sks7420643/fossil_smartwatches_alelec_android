package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface no0 extends IInterface {
    @DexIgnore
    sn0 a(sn0 sn0, String str, int i, sn0 sn02) throws RemoteException;

    @DexIgnore
    sn0 b(sn0 sn0, String str, int i, sn0 sn02) throws RemoteException;
}
