package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ra1 extends qa1<FieldDescriptorType, Object> {
    @DexIgnore
    public ra1(int i) {
        super(i, (ra1) null);
    }

    @DexIgnore
    public final void b() {
        if (!a()) {
            for (int i = 0; i < c(); i++) {
                Map.Entry a = a(i);
                if (((o81) a.getKey()).g()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : d()) {
                if (((o81) entry.getKey()).g()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.b();
    }
}
