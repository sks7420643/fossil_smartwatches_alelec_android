package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yq1 {
    @DexIgnore
    public static /* final */ int abc_config_activityDefaultDur; // = 2131427328;
    @DexIgnore
    public static /* final */ int abc_config_activityShortDur; // = 2131427329;
    @DexIgnore
    public static /* final */ int app_bar_elevation_anim_duration; // = 2131427331;
    @DexIgnore
    public static /* final */ int bottom_sheet_slide_duration; // = 2131427332;
    @DexIgnore
    public static /* final */ int cancel_button_image_alpha; // = 2131427333;
    @DexIgnore
    public static /* final */ int config_tooltipAnimTime; // = 2131427334;
    @DexIgnore
    public static /* final */ int design_snackbar_text_max_lines; // = 2131427335;
    @DexIgnore
    public static /* final */ int design_tab_indicator_anim_duration_ms; // = 2131427336;
    @DexIgnore
    public static /* final */ int hide_password_duration; // = 2131427339;
    @DexIgnore
    public static /* final */ int mtrl_btn_anim_delay_ms; // = 2131427340;
    @DexIgnore
    public static /* final */ int mtrl_btn_anim_duration_ms; // = 2131427341;
    @DexIgnore
    public static /* final */ int mtrl_chip_anim_duration; // = 2131427342;
    @DexIgnore
    public static /* final */ int mtrl_tab_indicator_anim_duration_ms; // = 2131427343;
    @DexIgnore
    public static /* final */ int show_password_duration; // = 2131427344;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_maxnum; // = 2131427345;
}
