package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mn0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mn0> CREATOR; // = new nn0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ gn0 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore
    public mn0(String str, IBinder iBinder, boolean z, boolean z2) {
        this.e = str;
        this.f = a(iBinder);
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public static gn0 a(IBinder iBinder) {
        byte[] bArr;
        if (iBinder == null) {
            return null;
        }
        try {
            sn0 zzb = vl0.a(iBinder).zzb();
            if (zzb == null) {
                bArr = null;
            } else {
                bArr = (byte[]) un0.d(zzb);
            }
            if (bArr != null) {
                return new hn0(bArr);
            }
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
            return null;
        } catch (RemoteException e2) {
            Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e2);
            return null;
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e, false);
        gn0 gn0 = this.f;
        if (gn0 == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            gn0 = null;
        } else {
            gn0.asBinder();
        }
        kk0.a(parcel, 2, (IBinder) gn0, false);
        kk0.a(parcel, 3, this.g);
        kk0.a(parcel, 4, this.h);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public mn0(String str, gn0 gn0, boolean z, boolean z2) {
        this.e = str;
        this.f = gn0;
        this.g = z;
        this.h = z2;
    }
}
