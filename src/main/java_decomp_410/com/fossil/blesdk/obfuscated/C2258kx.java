package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kx */
public class C2258kx implements com.fossil.blesdk.obfuscated.C2773qx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.reflect.Method f7029a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.Object f7030b;

    @DexIgnore
    public C2258kx(java.lang.Object obj, java.lang.reflect.Method method) {
        this.f7030b = obj;
        this.f7029a = method;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Class m9877a(android.content.Context context) {
        try {
            return context.getClassLoader().loadClass("com.google.android.gms.measurement.AppMeasurement");
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2773qx m9879b(android.content.Context context) {
        java.lang.Class a = m9877a(context);
        if (a == null) {
            return null;
        }
        java.lang.Object a2 = m9878a(context, a);
        if (a2 == null) {
            return null;
        }
        java.lang.reflect.Method b = m9880b(context, a);
        if (b == null) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2258kx(a2, b);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m9878a(android.content.Context context, java.lang.Class cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new java.lang.Class[]{android.content.Context.class}).invoke(cls, new java.lang.Object[]{context});
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12972a(java.lang.String str, android.os.Bundle bundle) {
        mo12973a("fab", str, bundle);
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Method m9880b(android.content.Context context, java.lang.Class cls) {
        try {
            return cls.getDeclaredMethod("logEventInternal", new java.lang.Class[]{java.lang.String.class, java.lang.String.class, android.os.Bundle.class});
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12973a(java.lang.String str, java.lang.String str2, android.os.Bundle bundle) {
        try {
            this.f7029a.invoke(this.f7030b, new java.lang.Object[]{str, str2, bundle});
        } catch (java.lang.Exception unused) {
        }
    }
}
