package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ub */
public class C3023ub {

    @DexIgnore
    /* renamed from: a */
    public static java.util.concurrent.atomic.AtomicBoolean f9924a; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ub$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ub$a */
    public static class C3024a extends com.fossil.blesdk.obfuscated.C2716qb {
        @DexIgnore
        public void onActivityCreated(android.app.Activity activity, android.os.Bundle bundle) {
            com.fossil.blesdk.obfuscated.C1785fc.m6901b(activity);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStopped(android.app.Activity activity) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14671a(android.content.Context context) {
        if (!f9924a.getAndSet(true)) {
            ((android.app.Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new com.fossil.blesdk.obfuscated.C3023ub.C3024a());
        }
    }
}
