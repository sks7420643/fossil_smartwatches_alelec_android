package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.fossil.blesdk.obfuscated.ve0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ig0 {
    @DexIgnore
    public ig0(int i) {
    }

    @DexIgnore
    public static Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (pm0.b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    @DexIgnore
    public abstract void a(jf0 jf0, boolean z);

    @DexIgnore
    public abstract void a(ve0.a<?> aVar) throws DeadObjectException;

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public abstract void a(RuntimeException runtimeException);
}
