package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ab */
public final class C1409ab implements android.os.Parcelable {
    @DexIgnore
    public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C1409ab> CREATOR; // = new com.fossil.blesdk.obfuscated.C1409ab.C1410a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f3451e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f3452f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ boolean f3453g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ int f3454h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f3455i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.lang.String f3456j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ boolean f3457k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ boolean f3458l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ android.os.Bundle f3459m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ boolean f3460n;

    @DexIgnore
    /* renamed from: o */
    public android.os.Bundle f3461o;

    @DexIgnore
    /* renamed from: p */
    public androidx.fragment.app.Fragment f3462p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ab$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ab$a */
    public static class C1410a implements android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C1409ab> {
        @DexIgnore
        public com.fossil.blesdk.obfuscated.C1409ab createFromParcel(android.os.Parcel parcel) {
            return new com.fossil.blesdk.obfuscated.C1409ab(parcel);
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C1409ab[] newArray(int i) {
            return new com.fossil.blesdk.obfuscated.C1409ab[i];
        }
    }

    @DexIgnore
    public C1409ab(androidx.fragment.app.Fragment fragment) {
        this.f3451e = fragment.getClass().getName();
        this.f3452f = fragment.mIndex;
        this.f3453g = fragment.mFromLayout;
        this.f3454h = fragment.mFragmentId;
        this.f3455i = fragment.mContainerId;
        this.f3456j = fragment.mTag;
        this.f3457k = fragment.mRetainInstance;
        this.f3458l = fragment.mDetached;
        this.f3459m = fragment.mArguments;
        this.f3460n = fragment.mHidden;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.fragment.app.Fragment mo8677a(androidx.fragment.app.FragmentHostCallback fragmentHostCallback, com.fossil.blesdk.obfuscated.C3350ya yaVar, androidx.fragment.app.Fragment fragment, androidx.fragment.app.FragmentManagerNonConfig fragmentManagerNonConfig, androidx.lifecycle.ViewModelStore viewModelStore) {
        if (this.f3462p == null) {
            android.content.Context c = fragmentHostCallback.mo2073c();
            android.os.Bundle bundle = this.f3459m;
            if (bundle != null) {
                bundle.setClassLoader(c.getClassLoader());
            }
            if (yaVar != null) {
                this.f3462p = yaVar.mo1994a(c, this.f3451e, this.f3459m);
            } else {
                this.f3462p = androidx.fragment.app.Fragment.instantiate(c, this.f3451e, this.f3459m);
            }
            android.os.Bundle bundle2 = this.f3461o;
            if (bundle2 != null) {
                bundle2.setClassLoader(c.getClassLoader());
                this.f3462p.mSavedFragmentState = this.f3461o;
            }
            this.f3462p.setIndex(this.f3452f, fragment);
            androidx.fragment.app.Fragment fragment2 = this.f3462p;
            fragment2.mFromLayout = this.f3453g;
            fragment2.mRestored = true;
            fragment2.mFragmentId = this.f3454h;
            fragment2.mContainerId = this.f3455i;
            fragment2.mTag = this.f3456j;
            fragment2.mRetainInstance = this.f3457k;
            fragment2.mDetached = this.f3458l;
            fragment2.mHidden = this.f3460n;
            fragment2.mFragmentManager = fragmentHostCallback.f1045e;
            if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
                android.util.Log.v("FragmentManager", "Instantiated fragment " + this.f3462p);
            }
        }
        androidx.fragment.app.Fragment fragment3 = this.f3462p;
        fragment3.mChildNonConfig = fragmentManagerNonConfig;
        fragment3.mViewModelStore = viewModelStore;
        return fragment3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(android.os.Parcel parcel, int i) {
        parcel.writeString(this.f3451e);
        parcel.writeInt(this.f3452f);
        parcel.writeInt(this.f3453g ? 1 : 0);
        parcel.writeInt(this.f3454h);
        parcel.writeInt(this.f3455i);
        parcel.writeString(this.f3456j);
        parcel.writeInt(this.f3457k ? 1 : 0);
        parcel.writeInt(this.f3458l ? 1 : 0);
        parcel.writeBundle(this.f3459m);
        parcel.writeInt(this.f3460n ? 1 : 0);
        parcel.writeBundle(this.f3461o);
    }

    @DexIgnore
    public C1409ab(android.os.Parcel parcel) {
        this.f3451e = parcel.readString();
        this.f3452f = parcel.readInt();
        boolean z = true;
        this.f3453g = parcel.readInt() != 0;
        this.f3454h = parcel.readInt();
        this.f3455i = parcel.readInt();
        this.f3456j = parcel.readString();
        this.f3457k = parcel.readInt() != 0;
        this.f3458l = parcel.readInt() != 0;
        this.f3459m = parcel.readBundle();
        this.f3460n = parcel.readInt() == 0 ? false : z;
        this.f3461o = parcel.readBundle();
    }
}
