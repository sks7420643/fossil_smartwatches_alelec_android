package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ly2 implements Factory<RemindTimePresenter> {
    @DexIgnore
    public static RemindTimePresenter a(jy2 jy2, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new RemindTimePresenter(jy2, remindersSettingsDatabase);
    }
}
