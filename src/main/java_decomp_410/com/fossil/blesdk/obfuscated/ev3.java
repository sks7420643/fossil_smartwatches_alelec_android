package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ev3 {

    @DexIgnore
    public interface a {
        @DexIgnore
        jv3 a(hv3 hv3) throws IOException;
    }

    @DexIgnore
    jv3 a(a aVar) throws IOException;
}
