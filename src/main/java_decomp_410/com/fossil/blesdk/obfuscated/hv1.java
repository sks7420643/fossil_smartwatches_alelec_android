package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.nio.CharBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hv1 {
    @DexIgnore
    public static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    @DexIgnore
    public static <T> T a(Readable readable, mv1<T> mv1) throws IOException {
        String a;
        st1.a(readable);
        st1.a(mv1);
        nv1 nv1 = new nv1(readable);
        do {
            a = nv1.a();
            if (a == null) {
                break;
            }
        } while (mv1.a(a));
        return mv1.getResult();
    }
}
