package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.lb */
public abstract class C2304lb extends android.content.BroadcastReceiver {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ android.util.SparseArray<android.os.PowerManager.WakeLock> f7173a; // = new android.util.SparseArray<>();

    @DexIgnore
    /* renamed from: b */
    public static int f7174b; // = 1;

    @DexIgnore
    /* renamed from: a */
    public static boolean m10118a(android.content.Intent intent) {
        int intExtra = intent.getIntExtra("androidx.contentpager.content.wakelockid", 0);
        if (intExtra == 0) {
            return false;
        }
        synchronized (f7173a) {
            android.os.PowerManager.WakeLock wakeLock = f7173a.get(intExtra);
            if (wakeLock != null) {
                wakeLock.release();
                f7173a.remove(intExtra);
                return true;
            }
            android.util.Log.w("WakefulBroadcastReceiv.", "No active wake lock id #" + intExtra);
            return true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.ComponentName m10119b(android.content.Context context, android.content.Intent intent) {
        synchronized (f7173a) {
            int i = f7174b;
            f7174b++;
            if (f7174b <= 0) {
                f7174b = 1;
            }
            intent.putExtra("androidx.contentpager.content.wakelockid", i);
            android.content.ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            android.os.PowerManager.WakeLock newWakeLock = ((android.os.PowerManager) context.getSystemService("power")).newWakeLock(1, "androidx.core:wake:" + startService.flattenToShortString());
            newWakeLock.setReferenceCounted(false);
            newWakeLock.acquire(60000);
            f7173a.put(i, newWakeLock);
            return startService;
        }
    }
}
