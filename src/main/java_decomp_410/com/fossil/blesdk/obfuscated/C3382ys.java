package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ys */
public class C3382ys implements com.fossil.blesdk.obfuscated.C2581oo<android.graphics.drawable.Drawable> {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> f11375b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ boolean f11376c;

    @DexIgnore
    public C3382ys(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar, boolean z) {
        this.f11375b = ooVar;
        this.f11376c = z;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> mo12877a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> aqVar, int i, int i2) {
        com.fossil.blesdk.obfuscated.C2149jq c = com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15645c();
        android.graphics.drawable.Drawable drawable = aqVar.get();
        com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> a = com.fossil.blesdk.obfuscated.C3310xs.m16497a(c, drawable, i, i2);
        if (a != null) {
            com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> a2 = this.f11375b.mo12877a(context, a, i, i2);
            if (!a2.equals(a)) {
                return mo18296a(context, a2);
            }
            a2.mo8887a();
            return aqVar;
        } else if (!this.f11376c) {
            return aqVar;
        } else {
            throw new java.lang.IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2581oo<android.graphics.drawable.BitmapDrawable> mo18297a() {
        return this;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C3382ys) {
            return this.f11375b.equals(((com.fossil.blesdk.obfuscated.C3382ys) obj).f11375b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f11375b.hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> mo18296a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar) {
        return com.fossil.blesdk.obfuscated.C1659dt.m6045a(context.getResources(), aqVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        this.f11375b.mo8934a(messageDigest);
    }
}
