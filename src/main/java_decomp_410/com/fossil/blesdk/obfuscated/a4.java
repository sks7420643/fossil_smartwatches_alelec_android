package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a4 implements d4 {
    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(c4 c4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        c4Var.a(new e4(colorStateList, f));
        View d = c4Var.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        c(c4Var, f3);
    }

    @DexIgnore
    public float b(c4 c4Var) {
        return j(c4Var).c();
    }

    @DexIgnore
    public void c(c4 c4Var, float f) {
        j(c4Var).a(f, c4Var.b(), c4Var.a());
        f(c4Var);
    }

    @DexIgnore
    public float d(c4 c4Var) {
        return j(c4Var).b();
    }

    @DexIgnore
    public ColorStateList e(c4 c4Var) {
        return j(c4Var).a();
    }

    @DexIgnore
    public void f(c4 c4Var) {
        if (!c4Var.b()) {
            c4Var.a(0, 0, 0, 0);
            return;
        }
        float d = d(c4Var);
        float b = b(c4Var);
        int ceil = (int) Math.ceil((double) f4.a(d, b, c4Var.a()));
        int ceil2 = (int) Math.ceil((double) f4.b(d, b, c4Var.a()));
        c4Var.a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    public float g(c4 c4Var) {
        return b(c4Var) * 2.0f;
    }

    @DexIgnore
    public float h(c4 c4Var) {
        return b(c4Var) * 2.0f;
    }

    @DexIgnore
    public void i(c4 c4Var) {
        c(c4Var, d(c4Var));
    }

    @DexIgnore
    public final e4 j(c4 c4Var) {
        return (e4) c4Var.c();
    }

    @DexIgnore
    public void b(c4 c4Var, float f) {
        c4Var.d().setElevation(f);
    }

    @DexIgnore
    public void c(c4 c4Var) {
        c(c4Var, d(c4Var));
    }

    @DexIgnore
    public void a(c4 c4Var, float f) {
        j(c4Var).a(f);
    }

    @DexIgnore
    public float a(c4 c4Var) {
        return c4Var.d().getElevation();
    }

    @DexIgnore
    public void a(c4 c4Var, ColorStateList colorStateList) {
        j(c4Var).b(colorStateList);
    }
}
