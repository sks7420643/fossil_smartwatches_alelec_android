package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zr4<T> extends o84<yr4<T>> {
    @DexIgnore
    public /* final */ o84<qr4<T>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements q84<qr4<R>> {
        @DexIgnore
        public /* final */ q84<? super yr4<R>> e;

        @DexIgnore
        public a(q84<? super yr4<R>> q84) {
            this.e = q84;
        }

        @DexIgnore
        /* renamed from: a */
        public void onNext(qr4<R> qr4) {
            this.e.onNext(yr4.a(qr4));
        }

        @DexIgnore
        public void onComplete() {
            this.e.onComplete();
        }

        @DexIgnore
        public void onError(Throwable th) {
            try {
                this.e.onNext(yr4.a(th));
                this.e.onComplete();
            } catch (Throwable th2) {
                a94.b(th2);
                ia4.b(new CompositeException(th, th2));
            }
        }

        @DexIgnore
        public void onSubscribe(y84 y84) {
            this.e.onSubscribe(y84);
        }
    }

    @DexIgnore
    public zr4(o84<qr4<T>> o84) {
        this.e = o84;
    }

    @DexIgnore
    public void b(q84<? super yr4<T>> q84) {
        this.e.a(new a(q84));
    }
}
