package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ih4 {
    @DexIgnore
    public static final Object a(long j, yb4<? super qa4> yb4) {
        if (j <= 0) {
            return qa4.a;
        }
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 1);
        a(eg4.getContext()).a(j, (dg4<? super qa4>) eg4);
        Object e = eg4.e();
        if (e == cc4.a()) {
            ic4.c(yb4);
        }
        return e;
    }

    @DexIgnore
    public static final hh4 a(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "$this$delay");
        CoroutineContext.a aVar = coroutineContext.get(zb4.b);
        if (!(aVar instanceof hh4)) {
            aVar = null;
        }
        hh4 hh4 = (hh4) aVar;
        return hh4 != null ? hh4 : fh4.a();
    }
}
