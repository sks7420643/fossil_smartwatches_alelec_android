package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.ps2;
import com.fossil.blesdk.obfuscated.us2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g63 extends zr2 implements u63, ws3.g {
    @DexIgnore
    public tr3<kd2> j;
    @DexIgnore
    public t63 k;
    @DexIgnore
    public ps2 l;
    @DexIgnore
    public us2 m;
    @DexIgnore
    public j42 n;
    @DexIgnore
    public HybridCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements us2.c {
        @DexIgnore
        public /* final */ /* synthetic */ g63 a;

        @DexIgnore
        public b(g63 g63) {
            this.a = g63;
        }

        @DexIgnore
        public void a(MicroApp microApp) {
            kd4.b(microApp, "microApp");
            g63.a(this.a).a(microApp.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ps2.c {
        @DexIgnore
        public /* final */ /* synthetic */ g63 a;

        @DexIgnore
        public c(g63 g63) {
            this.a = g63;
        }

        @DexIgnore
        public void a() {
            g63.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            kd4.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "onItemClicked category=" + category);
            g63.a(this.a).a(category);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g63 e;

        @DexIgnore
        public d(g63 g63) {
            this.e = g63;
        }

        @DexIgnore
        public final void onClick(View view) {
            g63.a(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ g63 e;

        @DexIgnore
        public e(g63 g63) {
            this.e = g63;
        }

        @DexIgnore
        public final void onClick(View view) {
            g63.a(this.e).i();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ t63 a(g63 g63) {
        t63 t63 = g63.k;
        if (t63 != null) {
            return t63;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ds3 ds3 = ds3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager, "childFragmentManager");
                        ds3.B(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    ds3 ds32 = ds3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    kd4.a((Object) childFragmentManager2, "childFragmentManager");
                    ds32.A(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                ds3 ds33 = ds3.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                kd4.a((Object) childFragmentManager3, "childFragmentManager");
                ds33.u(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    public void F(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        SearchRingPhoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "MicroAppsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            us2 us2 = this.m;
            if (us2 != null) {
                us2.b();
            } else {
                kd4.d("mMicroAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(MicroApp microApp) {
        if (microApp != null) {
            us2 us2 = this.m;
            if (us2 != null) {
                us2.b(microApp.getId());
                c(microApp);
                return;
            }
            kd4.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(MicroApp microApp) {
        tr3<kd2> tr3 = this.j;
        if (tr3 != null) {
            kd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                kd4.a((Object) flexibleTextView, "binding.tvSelectedMicroApp");
                flexibleTextView.setText(sm2.a(PortfolioApp.W.c(), microApp.getNameKey(), microApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.s;
                kd4.a((Object) flexibleTextView2, "binding.tvMicroAppDetail");
                flexibleTextView2.setText(sm2.a(PortfolioApp.W.c(), microApp.getDescriptionKey(), microApp.getDescription()));
                us2 us2 = this.m;
                if (us2 != null) {
                    int a3 = us2.a(microApp.getId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "updateDetailMicroApp microAppId=" + microApp.getId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.r.i(a3);
                        return;
                    }
                    return;
                }
                kd4.d("mMicroAppAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(String str) {
        kd4.b(str, "category");
        tr3<kd2> tr3 = this.j;
        if (tr3 != null) {
            kd2 a2 = tr3.a();
            if (a2 != null) {
                ps2 ps2 = this.l;
                if (ps2 != null) {
                    int a3 = ps2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("MicroAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        ps2 ps22 = this.l;
                        if (ps22 != null) {
                            ps22.a(a3);
                            a2.q.j(a3);
                            return;
                        }
                        kd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        ns3.a aVar = ns3.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            kd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ns3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        PermissionActivity.a aVar2 = PermissionActivity.C;
                        Context context = getContext();
                        if (context != null) {
                            kd4.a((Object) context, "context!!");
                            aVar2.a(context, cb4.a((T[]) new Integer[]{10}));
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            j42 j42 = this.n;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) hybridCustomizeEditActivity, (kc.b) j42).a(HybridCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (HybridCustomizeViewModel) a2;
                t63 t63 = this.k;
                if (t63 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.o;
                    if (hybridCustomizeViewModel != null) {
                        t63.a(hybridCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onActivityResult requestCode " + i + " resultCode " + i2);
        if (i != 100) {
            if (i != 102) {
                if (i != 104) {
                    if (i == 106 && i2 == -1 && intent != null) {
                        CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING");
                        if (commuteTimeSetting != null) {
                            t63 t63 = this.k;
                            if (t63 != null) {
                                t63.a(MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), rj2.a(commuteTimeSetting));
                            } else {
                                kd4.d("mPresenter");
                                throw null;
                            }
                        }
                    }
                } else if (intent != null && i2 == -1) {
                    Ringtone ringtone = (Ringtone) intent.getParcelableExtra("KEY_SELECTED_RINGPHONE");
                    if (ringtone != null) {
                        t63 t632 = this.k;
                        if (t632 != null) {
                            t632.a(MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue(), rj2.a(ringtone));
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_MICRO_APP_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    t63 t633 = this.k;
                    if (t633 != null) {
                        kd4.a((Object) stringExtra, "selectedMicroAppId");
                        t633.a(stringExtra);
                        return;
                    }
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (i2 == -1 && intent != null) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE");
            if (secondTimezoneSetting != null) {
                t63 t634 = this.k;
                if (t634 != null) {
                    t634.a(MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), rj2.a(secondTimezoneSetting));
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        kd2 kd2 = (kd2) qa.a(layoutInflater, R.layout.fragment_micro_app, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new v63(this)).a(this);
        this.j = new tr3<>(this, kd2);
        kd4.a((Object) kd2, "binding");
        return kd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        t63 t63 = this.k;
        if (t63 != null) {
            t63.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        t63 t63 = this.k;
        if (t63 != null) {
            t63.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        us2 us2 = new us2((ArrayList) null, (us2.c) null, 3, (fd4) null);
        us2.a((us2.c) new b(this));
        this.m = us2;
        ps2 ps2 = new ps2((ArrayList) null, (ps2.c) null, 3, (fd4) null);
        ps2.a((ps2.c) new c(this));
        this.l = ps2;
        tr3<kd2> tr3 = this.j;
        if (tr3 != null) {
            kd2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                ps2 ps22 = this.l;
                if (ps22 != null) {
                    recyclerView.setAdapter(ps22);
                    RecyclerView recyclerView2 = a2.r;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    us2 us22 = this.m;
                    if (us22 != null) {
                        recyclerView2.setAdapter(us22);
                        a2.t.setOnClickListener(new d(this));
                        a2.u.setOnClickListener(new e(this));
                        return;
                    }
                    kd4.d("mMicroAppAdapter");
                    throw null;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void w(List<MicroApp> list) {
        kd4.b(list, "microApps");
        us2 us2 = this.m;
        if (us2 != null) {
            us2.a(list);
        } else {
            kd4.d("mMicroAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void z(String str) {
        kd4.b(str, "content");
        tr3<kd2> tr3 = this.j;
        if (tr3 != null) {
            kd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.tvMicroAppDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(t63 t63) {
        kd4.b(t63, "presenter");
        this.k = t63;
    }

    @DexIgnore
    public void a(List<Category> list) {
        kd4.b(list, "categories");
        ps2 ps2 = this.l;
        if (ps2 != null) {
            ps2.a(list);
        } else {
            kd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, String str3) {
        kd4.b(str, "microAppId");
        kd4.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "updateSetting of microAppId " + str + " requestContent " + str2 + " setting " + str3);
        tr3<kd2> tr3 = this.j;
        if (tr3 != null) {
            kd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                kd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.t;
                kd4.a((Object) flexibleTextView2, "it.tvMicroAppPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.u;
                    kd4.a((Object) flexibleTextView3, "it.tvMicroAppSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.u;
                        kd4.a((Object) flexibleTextView4, "it.tvMicroAppSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.u;
                    kd4.a((Object) flexibleTextView5, "it.tvMicroAppSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.u;
                kd4.a((Object) flexibleTextView6, "it.tvMicroAppSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (pq4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        kd4.b(str, "title");
        kd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            tr3<kd2> tr3 = this.j;
            if (tr3 != null) {
                kd2 a2 = tr3.a();
                if (a2 == null) {
                    return;
                }
                if (i2 == 0 || i == i2) {
                    FlexibleTextView flexibleTextView = a2.t;
                    kd4.a((Object) flexibleTextView, "it.tvMicroAppPermission");
                    flexibleTextView.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.v;
                    kd4.a((Object) flexibleTextView2, "it.tvPermissionOrder");
                    flexibleTextView2.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView3 = a2.u;
                kd4.a((Object) flexibleTextView3, "it.tvMicroAppSetting");
                flexibleTextView3.setVisibility(8);
                FlexibleTextView flexibleTextView4 = a2.t;
                kd4.a((Object) flexibleTextView4, "it.tvMicroAppPermission");
                flexibleTextView4.setVisibility(0);
                FlexibleTextView flexibleTextView5 = a2.t;
                kd4.a((Object) flexibleTextView5, "it.tvMicroAppPermission");
                flexibleTextView5.setText(str);
                if (str2.length() > 0) {
                    FlexibleTextView flexibleTextView6 = a2.s;
                    kd4.a((Object) flexibleTextView6, "it.tvMicroAppDetail");
                    flexibleTextView6.setText(str2);
                }
                if (i2 > 1) {
                    FlexibleTextView flexibleTextView7 = a2.v;
                    kd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                    flexibleTextView7.setVisibility(0);
                    FlexibleTextView flexibleTextView8 = a2.v;
                    kd4.a((Object) flexibleTextView8, "it.tvPermissionOrder");
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                    Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView8.setText(format);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (kd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (activity != null) {
                    ((BaseActivity) activity).l();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (kd4.a((Object) str, (Object) ds3.c.a()) && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (!pq4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
            } else if (activity2 != null) {
                ((BaseActivity) activity2).l();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        kd4.b(str, "topMicroApp");
        kd4.b(str2, "middleMicroApp");
        kd4.b(str3, "bottomMicroApp");
        SearchMicroAppActivity.C.a(this, str, str2, str3);
    }
}
