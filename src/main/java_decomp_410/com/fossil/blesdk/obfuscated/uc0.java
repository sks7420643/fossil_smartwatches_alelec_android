package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface uc0 extends IInterface {
    @DexIgnore
    void a(sc0 sc0, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    @DexIgnore
    void b(sc0 sc0, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
