package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vd0 extends DialogFragment {
    @DexIgnore
    public Dialog e; // = null;
    @DexIgnore
    public DialogInterface.OnCancelListener f; // = null;

    @DexIgnore
    public static vd0 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        vd0 vd0 = new vd0();
        bk0.a(dialog, (Object) "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        vd0.e = dialog2;
        if (onCancelListener != null) {
            vd0.f = onCancelListener;
        }
        return vd0;
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.e == null) {
            setShowsDialog(false);
        }
        return this.e;
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
