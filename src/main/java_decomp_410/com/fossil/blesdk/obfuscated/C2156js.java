package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.js */
public class C2156js<T> implements com.fossil.blesdk.obfuscated.C1438aq<T> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ T f6582e;

    @DexIgnore
    public C2156js(T t) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(t);
        this.f6582e = t;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo8888b() {
        return 1;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<T> mo8889c() {
        return this.f6582e.getClass();
    }

    @DexIgnore
    public final T get() {
        return this.f6582e;
    }
}
