package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.fossil.blesdk.obfuscated.li;
import com.fossil.blesdk.obfuscated.yt;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ut extends Drawable implements yt.b, Animatable, li {
    @DexIgnore
    public /* final */ a e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public Paint m;
    @DexIgnore
    public Rect n;
    @DexIgnore
    public List<li.a> o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ yt a;

        @DexIgnore
        public a(yt ytVar) {
            this.a = ytVar;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new ut(this);
        }
    }

    @DexIgnore
    public ut(Context context, co coVar, oo<Bitmap> ooVar, int i2, int i3, Bitmap bitmap) {
        this(new a(new yt(rn.a(context), coVar, i2, i3, ooVar, bitmap)));
    }

    @DexIgnore
    public void a(oo<Bitmap> ooVar, Bitmap bitmap) {
        this.e.a.a(ooVar, bitmap);
    }

    @DexIgnore
    public final Drawable.Callback b() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    @DexIgnore
    public ByteBuffer c() {
        return this.e.a.b();
    }

    @DexIgnore
    public final Rect d() {
        if (this.n == null) {
            this.n = new Rect();
        }
        return this.n;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!this.h) {
            if (this.l) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), d());
                this.l = false;
            }
            canvas.drawBitmap(this.e.a.c(), (Rect) null, d(), h());
        }
    }

    @DexIgnore
    public Bitmap e() {
        return this.e.a.e();
    }

    @DexIgnore
    public int f() {
        return this.e.a.f();
    }

    @DexIgnore
    public int g() {
        return this.e.a.d();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.e;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.e.a.g();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.e.a.i();
    }

    @DexIgnore
    public int getOpacity() {
        return -2;
    }

    @DexIgnore
    public final Paint h() {
        if (this.m == null) {
            this.m = new Paint(2);
        }
        return this.m;
    }

    @DexIgnore
    public int i() {
        return this.e.a.h();
    }

    @DexIgnore
    public boolean isRunning() {
        return this.f;
    }

    @DexIgnore
    public final void j() {
        List<li.a> list = this.o;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.o.get(i2).a(this);
            }
        }
    }

    @DexIgnore
    public void k() {
        this.h = true;
        this.e.a.a();
    }

    @DexIgnore
    public final void l() {
        this.j = 0;
    }

    @DexIgnore
    public final void m() {
        tw.a(!this.h, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.e.a.f() == 1) {
            invalidateSelf();
        } else if (!this.f) {
            this.f = true;
            this.e.a.a((yt.b) this);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void n() {
        this.f = false;
        this.e.a.b(this);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.l = true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        h().setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        h().setColorFilter(colorFilter);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        tw.a(!this.h, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.i = z;
        if (!z) {
            n();
        } else if (this.g) {
            m();
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        this.g = true;
        l();
        if (this.i) {
            m();
        }
    }

    @DexIgnore
    public void stop() {
        this.g = false;
        n();
    }

    @DexIgnore
    public void a() {
        if (b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (g() == f() - 1) {
            this.j++;
        }
        int i2 = this.k;
        if (i2 != -1 && this.j >= i2) {
            j();
            stop();
        }
    }

    @DexIgnore
    public ut(a aVar) {
        this.i = true;
        this.k = -1;
        tw.a(aVar);
        this.e = aVar;
    }
}
