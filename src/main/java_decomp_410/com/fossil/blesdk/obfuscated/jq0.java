package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jq0 extends Service {
    @DexIgnore
    public a e;

    @DexIgnore
    public abstract List<zo0> a(List<DataType> list);

    @DexIgnore
    @TargetApi(19)
    public final void a() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (pm0.e()) {
            ((AppOpsManager) getSystemService("appops")).checkPackage(callingUid, "com.google.android.gms");
            return;
        }
        String[] packagesForUid = getPackageManager().getPackagesForUid(callingUid);
        if (packagesForUid != null) {
            int length = packagesForUid.length;
            int i = 0;
            while (i < length) {
                if (!packagesForUid[i].equals("com.google.android.gms")) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new SecurityException("Unauthorized caller");
    }

    @DexIgnore
    public abstract boolean a(kq0 kq0);

    @DexIgnore
    public abstract boolean a(zo0 zo0);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!"com.google.android.gms.fitness.service.FitnessSensorService".equals(intent.getAction())) {
            return null;
        }
        if (Log.isLoggable("FitnessSensorService", 3)) {
            String valueOf = String.valueOf(intent);
            String name = jq0.class.getName();
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 20 + String.valueOf(name).length());
            sb.append("Intent ");
            sb.append(valueOf);
            sb.append(" received by ");
            sb.append(name);
            Log.d("FitnessSensorService", sb.toString());
        }
        a aVar = this.e;
        aVar.asBinder();
        return aVar;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.e = new a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends b21 {
        @DexIgnore
        public /* final */ jq0 e;

        @DexIgnore
        public a(jq0 jq0) {
            this.e = jq0;
        }

        @DexIgnore
        public final void a(w11 w11, o01 o01) throws RemoteException {
            this.e.a();
            o01.a(new hq0(this.e.a(w11.H()), Status.i));
        }

        @DexIgnore
        public final void a(kq0 kq0, g11 g11) throws RemoteException {
            this.e.a();
            if (this.e.a(kq0)) {
                g11.c(Status.i);
            } else {
                g11.c(new Status(13));
            }
        }

        @DexIgnore
        public final void a(y11 y11, g11 g11) throws RemoteException {
            this.e.a();
            if (this.e.a(y11.H())) {
                g11.c(Status.i);
            } else {
                g11.c(new Status(13));
            }
        }
    }
}
