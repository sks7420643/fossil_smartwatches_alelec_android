package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.um */
public class C3047um<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ T f10027a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2334lm.C2335a f10028b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.android.volley.VolleyError f10029c;

    @DexIgnore
    /* renamed from: d */
    public boolean f10030d;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.um$a */
    public interface C3048a {
        @DexIgnore
        void onErrorResponse(com.android.volley.VolleyError volleyError);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.um$b */
    public interface C3049b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public C3047um(T t, com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar) {
        this.f10030d = false;
        this.f10027a = t;
        this.f10028b = aVar;
        this.f10029c = null;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C3047um<T> m14806a(T t, com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar) {
        return new com.fossil.blesdk.obfuscated.C3047um<>(t, aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C3047um<T> m14805a(com.android.volley.VolleyError volleyError) {
        return new com.fossil.blesdk.obfuscated.C3047um<>(volleyError);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16808a() {
        return this.f10029c == null;
    }

    @DexIgnore
    public C3047um(com.android.volley.VolleyError volleyError) {
        this.f10030d = false;
        this.f10027a = null;
        this.f10028b = null;
        this.f10029c = volleyError;
    }
}
