package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.nr2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uj3 extends xj3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((fd4) null);
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ yj3 g;
    @DexIgnore
    public /* final */ UpdateUser h;
    @DexIgnore
    public /* final */ nr2 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return uj3.j;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ uj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public b(uj3 uj3, Unit unit) {
            this.a = uj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.e(this.b);
            MFUser h = this.a.h();
            if (h != null) {
                UserDisplayUnit a2 = qj2.a(h);
                if (a2 != null) {
                    PortfolioApp.W.c().a(a2, PortfolioApp.W.c().e());
                }
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ uj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public c(uj3 uj3, Unit unit) {
            this.a = uj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.c(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ uj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public d(uj3 uj3, Unit unit) {
            this.a = uj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.a(this.b);
            MFUser h = this.a.h();
            if (h != null) {
                UserDisplayUnit a2 = qj2.a(h);
                if (a2 != null) {
                    PortfolioApp.W.c().a(a2, PortfolioApp.W.c().e());
                }
            }
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.e<UpdateUser.d, UpdateUser.c> {
        @DexIgnore
        public /* final */ /* synthetic */ uj3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Unit b;

        @DexIgnore
        public e(uj3 uj3, Unit unit) {
            this.a = uj3;
            this.b = unit;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(UpdateUser.d dVar) {
            kd4.b(dVar, "responseValue");
            this.a.g.a();
            this.a.g.d(this.b);
        }

        @DexIgnore
        public void a(UpdateUser.c cVar) {
            kd4.b(cVar, "errorValue");
            this.a.g.a();
            this.a.g.a(cVar.a(), "");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CoroutineUseCase.e<nr2.a, CoroutineUseCase.a> {
        @DexIgnore
        public /* final */ /* synthetic */ uj3 a;

        @DexIgnore
        public f(uj3 uj3) {
            this.a = uj3;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(nr2.a aVar) {
            kd4.b(aVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(uj3.k.a(), "GetUser information onSuccess");
            this.a.g.a();
            this.a.a(aVar.a());
            if (this.a.g.isActive() && this.a.h() != null) {
                yj3 a2 = this.a.g;
                MFUser h = this.a.h();
                if (h != null) {
                    a2.c(h.getHeightUnit());
                    yj3 a3 = this.a.g;
                    MFUser h2 = this.a.h();
                    if (h2 != null) {
                        a3.d(h2.getWeightUnit());
                        yj3 a4 = this.a.g;
                        MFUser h3 = this.a.h();
                        if (h3 != null) {
                            a4.e(h3.getDistanceUnit());
                            yj3 a5 = this.a.g;
                            MFUser h4 = this.a.h();
                            if (h4 != null) {
                                a5.a(h4.getTemperatureUnit());
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            kd4.b(aVar, "errorValue");
            this.a.g.a();
            FLogger.INSTANCE.getLocal().d(uj3.k.a(), "GetUser information onError");
        }
    }

    /*
    static {
        String simpleName = uj3.class.getSimpleName();
        kd4.a((Object) simpleName, "PreferredUnitPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public uj3(yj3 yj3, UpdateUser updateUser, nr2 nr2) {
        kd4.b(yj3, "mView");
        kd4.b(updateUser, "mUpdateUser");
        kd4.b(nr2, "mGetUser");
        this.g = yj3;
        this.h = updateUser;
        this.i = nr2;
    }

    @DexIgnore
    public void b(Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setHeightUnit() called with: unit = [" + unit + ']');
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save height with null user");
        } else if (mFUser != null) {
            mFUser.setHeightUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new c(this, unit));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void c(Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setTemperatureUnit() called with: unit = [" + unit + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local2.d(str2, "setTemperatureUnit: unit = " + unit);
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            mFUser.setTemperatureUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new d(this, unit));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void d(Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setWeightUnit() called with: unit = [" + unit + ']');
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save weight with null user");
        } else if (mFUser != null) {
            mFUser.setWeightUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new e(this, unit));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(j, "presenter starts: Get user information");
        this.g.b();
        this.i.a(null, new f(this));
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    public final MFUser h() {
        return this.f;
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        this.f = mFUser;
    }

    @DexIgnore
    public void a(Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "setDistanceUnit() called with: unit = [" + unit + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local2.d(str2, "setDistanceUnit: unit = " + unit);
        MFUser mFUser = this.f;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(j, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            mFUser.setDistanceUnit(unit.getValue());
            this.g.b();
            UpdateUser updateUser = this.h;
            MFUser mFUser2 = this.f;
            if (mFUser2 != null) {
                updateUser.a(new UpdateUser.b(mFUser2), new b(this, unit));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
