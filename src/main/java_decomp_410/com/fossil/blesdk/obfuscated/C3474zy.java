package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zy */
public class C3474zy implements com.fossil.blesdk.obfuscated.b74 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2847rz f11673a;

    @DexIgnore
    public C3474zy(com.fossil.blesdk.obfuscated.C2847rz rzVar) {
        this.f11673a = rzVar;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo18621a() {
        return this.f11673a.mo15792a();
    }

    @DexIgnore
    /* renamed from: b */
    public java.io.InputStream mo18622b() {
        return this.f11673a.mo15793b();
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String[] mo18623c() {
        return this.f11673a.mo15794c();
    }

    @DexIgnore
    /* renamed from: d */
    public long mo18624d() {
        return -1;
    }
}
