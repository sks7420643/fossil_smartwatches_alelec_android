package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.network.impl.ZendeskConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public class rq0 extends lb {
    @DexIgnore
    public static qr0 c;
    @DexIgnore
    public static qr0 d;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bf A[Catch:{ SecurityException -> 0x00ff, IllegalStateException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c4 A[Catch:{ SecurityException -> 0x00ff, IllegalStateException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00cf A[Catch:{ SecurityException -> 0x00ff, IllegalStateException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d7 A[RETURN] */
    public static int d(Context context, Intent intent) {
        ComponentName componentName;
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "Starting service");
        }
        ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
        if (resolveService != null) {
            ServiceInfo serviceInfo = resolveService.serviceInfo;
            if (serviceInfo != null) {
                if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
                    String str = serviceInfo.packageName;
                    String str2 = serviceInfo.name;
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 94 + String.valueOf(str2).length());
                    sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
                    sb.append(str);
                    sb.append(ZendeskConfig.SLASH);
                    sb.append(str2);
                    Log.e("GcmReceiver", sb.toString());
                    if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
                        componentName = lb.b(context, intent);
                    } else {
                        componentName = context.startService(intent);
                        Log.d("GcmReceiver", "Missing wake lock permission, service start may be delayed");
                    }
                    if (componentName != null) {
                        return -1;
                    }
                    Log.e("GcmReceiver", "Error while delivering the message: ServiceIntent not found.");
                    return MFNetworkReturnCode.NOT_FOUND;
                }
                String str3 = serviceInfo.name;
                if (str3.startsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                    String valueOf = String.valueOf(context.getPackageName());
                    String valueOf2 = String.valueOf(str3);
                    str3 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                }
                if (Log.isLoggable("GcmReceiver", 3)) {
                    String valueOf3 = String.valueOf(str3);
                    Log.d("GcmReceiver", valueOf3.length() != 0 ? "Restricting intent to a specific service: ".concat(valueOf3) : new String("Restricting intent to a specific service: "));
                }
                intent.setClassName(context.getPackageName(), str3);
                if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
                }
                if (componentName != null) {
                }
            }
        }
        Log.e("GcmReceiver", "Failed to resolve target intent service, skipping classname enforcement");
        try {
            if (context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0) {
            }
            if (componentName != null) {
            }
        } catch (SecurityException e) {
            Log.e("GcmReceiver", "Error while delivering the message to the serviceIntent", e);
            return 401;
        } catch (IllegalStateException e2) {
            String valueOf4 = String.valueOf(e2);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf4).length() + 45);
            sb2.append("Failed to start service while in background: ");
            sb2.append(valueOf4);
            Log.e("GcmReceiver", sb2.toString());
            return Action.ActivityTracker.TAG_ACTIVITY;
        }
    }

    @DexIgnore
    public final synchronized qr0 a(Context context, String str) {
        if ("com.google.android.c2dm.intent.RECEIVE".equals(str)) {
            if (d == null) {
                d = new qr0(context, str);
            }
            return d;
        }
        if (c == null) {
            c = new qr0(context, str);
        }
        return c;
    }

    @DexIgnore
    public final int c(Context context, Intent intent) {
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "Binding to service");
        }
        if (isOrderedBroadcast()) {
            setResultCode(-1);
        }
        a(context, intent.getAction()).a(intent, goAsync());
        return -1;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        int i;
        int i2;
        if (Log.isLoggable("GcmReceiver", 3)) {
            Log.d("GcmReceiver", "received new intent");
        }
        intent.setComponent((ComponentName) null);
        intent.setPackage(context.getPackageName());
        if (Build.VERSION.SDK_INT <= 18) {
            intent.removeCategory(context.getPackageName());
        }
        if ("google.com/iid".equals(intent.getStringExtra("from"))) {
            intent.setAction("com.google.android.gms.iid.InstanceID");
        }
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        if (isOrderedBroadcast()) {
            setResultCode(500);
        }
        boolean z = true;
        boolean z2 = pm0.i() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & 268435456) == 0) {
            z = false;
        }
        if (!z2 || z) {
            if ("com.google.android.c2dm.intent.RECEIVE".equals(intent.getAction())) {
                i2 = d(context, intent);
            } else {
                i2 = d(context, intent);
            }
            if (!pm0.i() || i2 != 402) {
                i = i2;
            } else {
                c(context, intent);
                i = MFNetworkReturnCode.WRONG_PASSWORD;
            }
        } else {
            i = c(context, intent);
        }
        if (isOrderedBroadcast()) {
            setResultCode(i);
        }
    }
}
