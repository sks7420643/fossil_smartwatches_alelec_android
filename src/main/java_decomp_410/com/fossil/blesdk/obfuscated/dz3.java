package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.cz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dz3 implements cz3.a {
    @DexIgnore
    public dz3() {
        new Handler(Looper.getMainLooper());
    }

    @DexIgnore
    public final int a() {
        return cz3.a;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (cz3.a <= 1) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        if (cz3.a <= 2) {
            Log.i(str, str2);
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        if (cz3.a <= 3) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    public final void i(String str, String str2) {
        if (cz3.a <= 4) {
            Log.e(str, str2);
        }
    }
}
