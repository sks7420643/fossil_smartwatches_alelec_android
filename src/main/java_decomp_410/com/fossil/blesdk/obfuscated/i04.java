package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i04 {
    @DexIgnore
    public static void a(Context context) {
        j04.b(context, (k04) null);
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties) {
        j04.a(context, str, properties, (k04) null);
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2) {
        return j04.a(context, str, str2, (k04) null);
    }

    @DexIgnore
    public static void b(Context context) {
        j04.c(context, (k04) null);
    }
}
