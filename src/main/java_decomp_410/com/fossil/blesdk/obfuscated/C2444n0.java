package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n0 */
public class C2444n0 extends com.fossil.blesdk.obfuscated.C2691q0 {

    @DexIgnore
    /* renamed from: s */
    public com.fossil.blesdk.obfuscated.C2444n0.C2447c f7607s;

    @DexIgnore
    /* renamed from: t */
    public com.fossil.blesdk.obfuscated.C2444n0.C2451g f7608t;

    @DexIgnore
    /* renamed from: u */
    public int f7609u;

    @DexIgnore
    /* renamed from: v */
    public int f7610v;

    @DexIgnore
    /* renamed from: w */
    public boolean f7611w;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$b */
    public static class C2446b extends com.fossil.blesdk.obfuscated.C2444n0.C2451g {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.drawable.Animatable f7612a;

        @DexIgnore
        public C2446b(android.graphics.drawable.Animatable animatable) {
            super();
            this.f7612a = animatable;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo13771c() {
            this.f7612a.start();
        }

        @DexIgnore
        /* renamed from: d */
        public void mo13772d() {
            this.f7612a.stop();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$c")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$c */
    public static class C2447c extends com.fossil.blesdk.obfuscated.C2691q0.C2692a {

        @DexIgnore
        /* renamed from: K */
        public com.fossil.blesdk.obfuscated.C2103j4<java.lang.Long> f7613K;

        @DexIgnore
        /* renamed from: L */
        public androidx.collection.SparseArrayCompat<java.lang.Integer> f7614L;

        @DexIgnore
        public C2447c(com.fossil.blesdk.obfuscated.C2444n0.C2447c cVar, com.fossil.blesdk.obfuscated.C2444n0 n0Var, android.content.res.Resources resources) {
            super(cVar, n0Var, resources);
            if (cVar != null) {
                this.f7613K = cVar.f7613K;
                this.f7614L = cVar.f7614L;
                return;
            }
            this.f7613K = new com.fossil.blesdk.obfuscated.C2103j4<>();
            this.f7614L = new androidx.collection.SparseArrayCompat<>();
        }

        @DexIgnore
        /* renamed from: f */
        public static long m10934f(int i, int i2) {
            return ((long) i2) | (((long) i) << 32);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13773a(int i, int i2, android.graphics.drawable.Drawable drawable, boolean z) {
            int a = super.mo14204a(drawable);
            long f = m10934f(i, i2);
            long j = z ? 8589934592L : 0;
            long j2 = (long) a;
            this.f7613K.mo12215a(f, java.lang.Long.valueOf(j2 | j));
            if (z) {
                this.f7613K.mo12215a(m10934f(i2, i), java.lang.Long.valueOf(4294967296L | j2 | j));
            }
            return a;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo13775b(int[] iArr) {
            int a = super.mo14930a(iArr);
            if (a >= 0) {
                return a;
            }
            return super.mo14930a(android.util.StateSet.WILD_CARD);
        }

        @DexIgnore
        /* renamed from: c */
        public int mo13776c(int i, int i2) {
            return (int) this.f7613K.mo12217b(m10934f(i, i2), -1L).longValue();
        }

        @DexIgnore
        /* renamed from: d */
        public int mo13777d(int i) {
            if (i < 0) {
                return 0;
            }
            return this.f7614L.mo1243b(i, 0).intValue();
        }

        @DexIgnore
        /* renamed from: e */
        public boolean mo13779e(int i, int i2) {
            return (this.f7613K.mo12217b(m10934f(i, i2), -1L).longValue() & 8589934592L) != 0;
        }

        @DexIgnore
        /* renamed from: n */
        public void mo13780n() {
            this.f7613K = this.f7613K.clone();
            this.f7614L = this.f7614L.clone();
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            return new com.fossil.blesdk.obfuscated.C2444n0(this, (android.content.res.Resources) null);
        }

        @DexIgnore
        /* renamed from: d */
        public boolean mo13778d(int i, int i2) {
            return (this.f7613K.mo12217b(m10934f(i, i2), -1L).longValue() & 4294967296L) != 0;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return new com.fossil.blesdk.obfuscated.C2444n0(this, resources);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13774a(int[] iArr, android.graphics.drawable.Drawable drawable, int i) {
            int a = super.mo14931a(iArr, drawable);
            this.f7614L.mo1247c(a, java.lang.Integer.valueOf(i));
            return a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$d")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$d */
    public static class C2448d extends com.fossil.blesdk.obfuscated.C2444n0.C2451g {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2415mi f7615a;

        @DexIgnore
        public C2448d(com.fossil.blesdk.obfuscated.C2415mi miVar) {
            super();
            this.f7615a = miVar;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo13771c() {
            this.f7615a.start();
        }

        @DexIgnore
        /* renamed from: d */
        public void mo13772d() {
            this.f7615a.stop();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$e")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$e */
    public static class C2449e extends com.fossil.blesdk.obfuscated.C2444n0.C2451g {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.animation.ObjectAnimator f7616a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f7617b;

        @DexIgnore
        public C2449e(android.graphics.drawable.AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            com.fossil.blesdk.obfuscated.C2444n0.C2450f fVar = new com.fossil.blesdk.obfuscated.C2444n0.C2450f(animationDrawable, z);
            android.animation.ObjectAnimator ofInt = android.animation.ObjectAnimator.ofInt(animationDrawable, "currentIndex", new int[]{i, i2});
            if (android.os.Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration((long) fVar.mo13785a());
            ofInt.setInterpolator(fVar);
            this.f7617b = z2;
            this.f7616a = ofInt;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13783a() {
            return this.f7617b;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13784b() {
            this.f7616a.reverse();
        }

        @DexIgnore
        /* renamed from: c */
        public void mo13771c() {
            this.f7616a.start();
        }

        @DexIgnore
        /* renamed from: d */
        public void mo13772d() {
            this.f7616a.cancel();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$f")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$f */
    public static class C2450f implements android.animation.TimeInterpolator {

        @DexIgnore
        /* renamed from: a */
        public int[] f7618a;

        @DexIgnore
        /* renamed from: b */
        public int f7619b;

        @DexIgnore
        /* renamed from: c */
        public int f7620c;

        @DexIgnore
        public C2450f(android.graphics.drawable.AnimationDrawable animationDrawable, boolean z) {
            mo13786a(animationDrawable, z);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13786a(android.graphics.drawable.AnimationDrawable animationDrawable, boolean z) {
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.f7619b = numberOfFrames;
            int[] iArr = this.f7618a;
            if (iArr == null || iArr.length < numberOfFrames) {
                this.f7618a = new int[numberOfFrames];
            }
            int[] iArr2 = this.f7618a;
            int i = 0;
            for (int i2 = 0; i2 < numberOfFrames; i2++) {
                int duration = animationDrawable.getDuration(z ? (numberOfFrames - i2) - 1 : i2);
                iArr2[i2] = duration;
                i += duration;
            }
            this.f7620c = i;
            return i;
        }

        @DexIgnore
        public float getInterpolation(float f) {
            int i = (int) ((f * ((float) this.f7620c)) + 0.5f);
            int i2 = this.f7619b;
            int[] iArr = this.f7618a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            return (((float) i3) / ((float) i2)) + (i3 < i2 ? ((float) i) / ((float) this.f7620c) : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13785a() {
            return this.f7620c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n0$g")
    /* renamed from: com.fossil.blesdk.obfuscated.n0$g */
    public static abstract class C2451g {
        @DexIgnore
        public C2451g() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13783a() {
            return false;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13784b() {
        }

        @DexIgnore
        /* renamed from: c */
        public abstract void mo13771c();

        @DexIgnore
        /* renamed from: d */
        public abstract void mo13772d();
    }

    /*
    static {
        java.lang.Class<com.fossil.blesdk.obfuscated.C2444n0> cls = com.fossil.blesdk.obfuscated.C2444n0.class;
    }
    */

    @DexIgnore
    public C2444n0() {
        this((com.fossil.blesdk.obfuscated.C2444n0.C2447c) null, (android.content.res.Resources) null);
    }

    @DexIgnore
    /* renamed from: e */
    public static com.fossil.blesdk.obfuscated.C2444n0 m10920e(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        java.lang.String name = xmlPullParser.getName();
        if (name.equals("animated-selector")) {
            com.fossil.blesdk.obfuscated.C2444n0 n0Var = new com.fossil.blesdk.obfuscated.C2444n0();
            n0Var.mo13758a(context, resources, xmlPullParser, attributeSet, theme);
            return n0Var;
        }
        throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo13762b(int i) {
        int i2;
        com.fossil.blesdk.obfuscated.C2444n0.C2451g gVar;
        com.fossil.blesdk.obfuscated.C2444n0.C2451g gVar2 = this.f7608t;
        if (gVar2 == null) {
            i2 = mo14167b();
        } else if (i == this.f7609u) {
            return true;
        } else {
            if (i != this.f7610v || !gVar2.mo13783a()) {
                i2 = this.f7609u;
                gVar2.mo13772d();
            } else {
                gVar2.mo13784b();
                this.f7609u = this.f7610v;
                this.f7610v = i;
                return true;
            }
        }
        this.f7608t = null;
        this.f7610v = -1;
        this.f7609u = -1;
        com.fossil.blesdk.obfuscated.C2444n0.C2447c cVar = this.f7607s;
        int d = cVar.mo13777d(i2);
        int d2 = cVar.mo13777d(i);
        if (!(d2 == 0 || d == 0)) {
            int c = cVar.mo13776c(d, d2);
            if (c < 0) {
                return false;
            }
            boolean e = cVar.mo13779e(d, d2);
            mo14165a(c);
            android.graphics.drawable.Drawable current = getCurrent();
            if (current instanceof android.graphics.drawable.AnimationDrawable) {
                gVar = new com.fossil.blesdk.obfuscated.C2444n0.C2449e((android.graphics.drawable.AnimationDrawable) current, cVar.mo13778d(d, d2), e);
            } else if (current instanceof com.fossil.blesdk.obfuscated.C2415mi) {
                gVar = new com.fossil.blesdk.obfuscated.C2444n0.C2448d((com.fossil.blesdk.obfuscated.C2415mi) current);
            } else if (current instanceof android.graphics.drawable.Animatable) {
                gVar = new com.fossil.blesdk.obfuscated.C2444n0.C2446b((android.graphics.drawable.Animatable) current);
            }
            gVar.mo13771c();
            this.f7608t = gVar;
            this.f7610v = i2;
            this.f7609u = i;
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public final int mo13763c(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int next;
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableItem);
        int resourceId = a.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableItem_android_id, 0);
        int resourceId2 = a.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableItem_android_drawable, -1);
        android.graphics.drawable.Drawable c = resourceId2 > 0 ? com.fossil.blesdk.obfuscated.C2364m0.m10497c(context, resourceId2) : null;
        a.recycle();
        int[] a2 = mo14929a(attributeSet);
        if (c == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("vector")) {
                c = com.fossil.blesdk.obfuscated.C2879si.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else if (android.os.Build.VERSION.SDK_INT >= 21) {
                c = android.graphics.drawable.Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                c = android.graphics.drawable.Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (c != null) {
            return this.f7607s.mo13774a(a2, c, resourceId);
        }
        throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo13765d() {
        onStateChange(getState());
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        com.fossil.blesdk.obfuscated.C2444n0.C2451g gVar = this.f7608t;
        if (gVar != null) {
            gVar.mo13772d();
            this.f7608t = null;
            mo14165a(this.f7609u);
            this.f7609u = -1;
            this.f7610v = -1;
        }
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        if (!this.f7611w) {
            super.mutate();
            if (this == this) {
                this.f7607s.mo13780n();
                this.f7611w = true;
            }
        }
        return this;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        int b = this.f7607s.mo13775b(iArr);
        boolean z = b != mo14167b() && (mo13762b(b) || mo14165a(b));
        android.graphics.drawable.Drawable current = getCurrent();
        return current != null ? z | current.setState(iArr) : z;
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (this.f7608t != null && (visible || z2)) {
            if (z) {
                this.f7608t.mo13771c();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    @DexIgnore
    public C2444n0(com.fossil.blesdk.obfuscated.C2444n0.C2447c cVar, android.content.res.Resources resources) {
        super((com.fossil.blesdk.obfuscated.C2691q0.C2692a) null);
        this.f7609u = -1;
        this.f7610v = -1;
        mo13760a((com.fossil.blesdk.obfuscated.C2514o0.C2517c) new com.fossil.blesdk.obfuscated.C2444n0.C2447c(cVar, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo13764d(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int next;
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableTransition);
        int resourceId = a.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = a.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableTransition_android_toId, -1);
        int resourceId3 = a.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableTransition_android_drawable, -1);
        android.graphics.drawable.Drawable c = resourceId3 > 0 ? com.fossil.blesdk.obfuscated.C2364m0.m10497c(context, resourceId3) : null;
        boolean z = a.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableTransition_android_reversible, false);
        a.recycle();
        if (c == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("animated-vector")) {
                c = com.fossil.blesdk.obfuscated.C2415mi.m10769a(context, resources, xmlPullParser, attributeSet, theme);
            } else if (android.os.Build.VERSION.SDK_INT >= 21) {
                c = android.graphics.drawable.Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                c = android.graphics.drawable.Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (c == null) {
            throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.f7607s.mo13773a(resourceId, resourceId2, c, z);
        } else {
            throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13758a(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat);
        setVisible(a.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_visible, true), true);
        mo13759a(a);
        mo14162a(resources);
        a.recycle();
        mo13761b(context, resources, xmlPullParser, attributeSet, theme);
        mo13765d();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13759a(android.content.res.TypedArray typedArray) {
        com.fossil.blesdk.obfuscated.C2444n0.C2447c cVar = this.f7607s;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            cVar.f7927d |= typedArray.getChangingConfigurations();
        }
        cVar.mo14214b(typedArray.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_variablePadding, cVar.f7932i));
        cVar.mo14209a(typedArray.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_constantSize, cVar.f7935l));
        cVar.mo14213b(typedArray.getInt(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_enterFadeDuration, cVar.f7915A));
        cVar.mo14217c(typedArray.getInt(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_exitFadeDuration, cVar.f7916B));
        setDither(typedArray.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AnimatedStateListDrawableCompat_android_dither, cVar.f7947x));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2444n0.C2447c m10923a() {
        return new com.fossil.blesdk.obfuscated.C2444n0.C2447c(this.f7607s, this, (android.content.res.Resources) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13760a(com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar) {
        super.mo13760a(cVar);
        if (cVar instanceof com.fossil.blesdk.obfuscated.C2444n0.C2447c) {
            this.f7607s = (com.fossil.blesdk.obfuscated.C2444n0.C2447c) cVar;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13761b(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                int depth2 = xmlPullParser.getDepth();
                if (depth2 < depth && next == 3) {
                    return;
                }
                if (next == 2 && depth2 <= depth) {
                    if (xmlPullParser.getName().equals("item")) {
                        mo13763c(context, resources, xmlPullParser, attributeSet, theme);
                    } else if (xmlPullParser.getName().equals("transition")) {
                        mo13764d(context, resources, xmlPullParser, attributeSet, theme);
                    }
                }
            } else {
                return;
            }
        }
    }
}
