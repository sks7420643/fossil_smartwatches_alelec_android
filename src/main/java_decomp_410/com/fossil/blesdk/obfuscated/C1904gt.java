package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gt */
public final class C1904gt {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ android.graphics.Paint f5556a; // = new android.graphics.Paint(6);

    @DexIgnore
    /* renamed from: b */
    public static /* final */ android.graphics.Paint f5557b; // = new android.graphics.Paint(7);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.util.Set<java.lang.String> f5558c; // = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"XT1085", "XT1092", "XT1093", "XT1094", "XT1095", "XT1096", "XT1097", "XT1098", "XT1031", "XT1028", "XT937C", "XT1032", "XT1008", "XT1033", "XT1035", "XT1034", "XT939G", "XT1039", "XT1040", "XT1042", "XT1045", "XT1063", "XT1064", "XT1068", "XT1069", "XT1072", "XT1077", "XT1078", "XT1079"}));

    @DexIgnore
    /* renamed from: d */
    public static /* final */ java.util.concurrent.locks.Lock f5559d; // = (f5558c.contains(android.os.Build.MODEL) ? new java.util.concurrent.locks.ReentrantLock() : new com.fossil.blesdk.obfuscated.C1904gt.C1905a());

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gt$a")
    /* renamed from: com.fossil.blesdk.obfuscated.gt$a */
    public static final class C1905a implements java.util.concurrent.locks.Lock {
        @DexIgnore
        public void lock() {
        }

        @DexIgnore
        public void lockInterruptibly() throws java.lang.InterruptedException {
        }

        @DexIgnore
        public java.util.concurrent.locks.Condition newCondition() {
            throw new java.lang.UnsupportedOperationException("Should not be called");
        }

        @DexIgnore
        public boolean tryLock() {
            return true;
        }

        @DexIgnore
        public boolean tryLock(long j, java.util.concurrent.TimeUnit timeUnit) throws java.lang.InterruptedException {
            return true;
        }

        @DexIgnore
        public void unlock() {
        }
    }

    /*
    static {
        new android.graphics.Paint(7);
        f5557b.setXfermode(new android.graphics.PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static int m7576a(int i) {
        switch (i) {
            case 3:
            case 4:
                return com.fossil.blesdk.device.data.background.BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return com.fossil.blesdk.device.data.background.BackgroundImageConfig.LEFT_BACKGROUND_ANGLE;
            default:
                return 0;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.concurrent.locks.Lock m7580a() {
        return f5559d;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.Bitmap m7585b(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() > i || bitmap.getHeight() > i2) {
            if (android.util.Log.isLoggable("TransformationUtils", 2)) {
                android.util.Log.v("TransformationUtils", "requested target size too big for input, fit centering instead");
            }
            return m7587c(jqVar, bitmap, i, i2);
        }
        if (android.util.Log.isLoggable("TransformationUtils", 2)) {
            android.util.Log.v("TransformationUtils", "requested target size larger or equal to input, returning input");
        }
        return bitmap;
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m7586b(int i) {
        switch (i) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return true;
            default:
                return false;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static android.graphics.Bitmap m7587c(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() == i && bitmap.getHeight() == i2) {
            if (android.util.Log.isLoggable("TransformationUtils", 2)) {
                android.util.Log.v("TransformationUtils", "requested target size matches input, returning input");
            }
            return bitmap;
        }
        float min = java.lang.Math.min(((float) i) / ((float) bitmap.getWidth()), ((float) i2) / ((float) bitmap.getHeight()));
        int round = java.lang.Math.round(((float) bitmap.getWidth()) * min);
        int round2 = java.lang.Math.round(((float) bitmap.getHeight()) * min);
        if (bitmap.getWidth() == round && bitmap.getHeight() == round2) {
            if (android.util.Log.isLoggable("TransformationUtils", 2)) {
                android.util.Log.v("TransformationUtils", "adjusted target size matches input, returning input");
            }
            return bitmap;
        }
        android.graphics.Bitmap a = jqVar.mo12443a((int) (((float) bitmap.getWidth()) * min), (int) (((float) bitmap.getHeight()) * min), m7577a(bitmap));
        m7582a(bitmap, a);
        if (android.util.Log.isLoggable("TransformationUtils", 2)) {
            android.util.Log.v("TransformationUtils", "request: " + i + "x" + i2);
            android.util.Log.v("TransformationUtils", "toFit:   " + bitmap.getWidth() + "x" + bitmap.getHeight());
            android.util.Log.v("TransformationUtils", "toReuse: " + a.getWidth() + "x" + a.getHeight());
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("minPct:   ");
            sb.append(min);
            android.util.Log.v("TransformationUtils", sb.toString());
        }
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        matrix.setScale(min, min);
        m7583a(bitmap, a, matrix);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Bitmap m7579a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2) {
        float f;
        float f2;
        if (bitmap.getWidth() == i && bitmap.getHeight() == i2) {
            return bitmap;
        }
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        int width = bitmap.getWidth() * i2;
        int height = bitmap.getHeight() * i;
        float f3 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (width > height) {
            f2 = ((float) i2) / ((float) bitmap.getHeight());
            f = (((float) i) - (((float) bitmap.getWidth()) * f2)) * 0.5f;
        } else {
            f2 = ((float) i) / ((float) bitmap.getWidth());
            f3 = (((float) i2) - (((float) bitmap.getHeight()) * f2)) * 0.5f;
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        matrix.setScale(f2, f2);
        matrix.postTranslate((float) ((int) (f + 0.5f)), (float) ((int) (f3 + 0.5f)));
        android.graphics.Bitmap a = jqVar.mo12443a(i, i2, m7577a(bitmap));
        m7582a(bitmap, a);
        m7583a(bitmap, a, matrix);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7582a(android.graphics.Bitmap bitmap, android.graphics.Bitmap bitmap2) {
        bitmap2.setHasAlpha(bitmap.hasAlpha());
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Bitmap m7578a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i) {
        if (!m7586b(i)) {
            return bitmap;
        }
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        m7581a(i, matrix);
        android.graphics.RectF rectF = new android.graphics.RectF(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix.mapRect(rectF);
        android.graphics.Bitmap a = jqVar.mo12443a(java.lang.Math.round(rectF.width()), java.lang.Math.round(rectF.height()), m7577a(bitmap));
        matrix.postTranslate(-rectF.left, -rectF.top);
        a.setHasAlpha(bitmap.hasAlpha());
        m7583a(bitmap, a, matrix);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7584a(android.graphics.Canvas canvas) {
        canvas.setBitmap((android.graphics.Bitmap) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Bitmap.Config m7577a(android.graphics.Bitmap bitmap) {
        return bitmap.getConfig() != null ? bitmap.getConfig() : android.graphics.Bitmap.Config.ARGB_8888;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7583a(android.graphics.Bitmap bitmap, android.graphics.Bitmap bitmap2, android.graphics.Matrix matrix) {
        f5559d.lock();
        try {
            android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap2);
            canvas.drawBitmap(bitmap, matrix, f5556a);
            m7584a(canvas);
        } finally {
            f5559d.unlock();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7581a(int i, android.graphics.Matrix matrix) {
        switch (i) {
            case 2:
                matrix.setScale(-1.0f, 1.0f);
                return;
            case 3:
                matrix.setRotate(180.0f);
                return;
            case 4:
                matrix.setRotate(180.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 5:
                matrix.setRotate(90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 6:
                matrix.setRotate(90.0f);
                return;
            case 7:
                matrix.setRotate(-90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 8:
                matrix.setRotate(-90.0f);
                return;
            default:
                return;
        }
    }
}
