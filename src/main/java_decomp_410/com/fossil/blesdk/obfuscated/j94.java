package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.Comparator;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j94 {
    @DexIgnore
    public static /* final */ b94 a; // = new a();
    @DexIgnore
    public static /* final */ e94<Object> b; // = new b();
    @DexIgnore
    public static /* final */ h94<Object> c; // = new l();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b94 {
        @DexIgnore
        public void run() {
        }

        @DexIgnore
        public String toString() {
            return "EmptyAction";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements e94<Object> {
        @DexIgnore
        public void a(Object obj) {
        }

        @DexIgnore
        public String toString() {
            return "EmptyConsumer";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements g94 {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public void run() {
        }

        @DexIgnore
        public String toString() {
            return "EmptyRunnable";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements e94<Throwable> {
        @DexIgnore
        public void a(Throwable th) {
            ia4.b(th);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements h94<Object> {
        @DexIgnore
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements f94<Object, Object> {
        @DexIgnore
        public Object apply(Object obj) {
            return obj;
        }

        @DexIgnore
        public String toString() {
            return "IdentityFunction";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements e94<aq4> {
        @DexIgnore
        public void a(aq4 aq4) throws Exception {
            aq4.c(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Comparator<Object> {
        @DexIgnore
        public int compare(Object obj, Object obj2) {
            return ((Comparable) obj).compareTo(obj2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements Callable<Object> {
        @DexIgnore
        public Object call() {
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements e94<Throwable> {
        @DexIgnore
        public void a(Throwable th) {
            ia4.b(new OnErrorNotImplementedException(th));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements h94<Object> {
        @DexIgnore
        public boolean a(Object obj) {
            return true;
        }
    }

    /*
    static {
        new g();
        new d();
        new e();
        new k();
        new c();
        new f();
        new j();
        new i();
        new h();
    }
    */

    @DexIgnore
    public static <T> h94<T> a() {
        return c;
    }

    @DexIgnore
    public static <T> e94<T> b() {
        return b;
    }
}
