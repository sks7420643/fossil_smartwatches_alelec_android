package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ef1 extends a51 implements je1 {
    @DexIgnore
    public ef1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    public final void a(sn0 sn0) throws RemoteException {
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        b(4, o);
    }

    @DexIgnore
    public final void b(sn0 sn0) throws RemoteException {
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        b(5, o);
    }

    @DexIgnore
    public final void clear() throws RemoteException {
        b(14, o());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final oe1 m() throws RemoteException {
        oe1 oe1;
        Parcel a = a(25, o());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            oe1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            if (queryLocalInterface instanceof oe1) {
                oe1 = queryLocalInterface;
            } else {
                oe1 = new ze1(readStrongBinder);
            }
        }
        a.recycle();
        return oe1;
    }

    @DexIgnore
    public final g51 a(kf1 kf1) throws RemoteException {
        Parcel o = o();
        c51.a(o, (Parcelable) kf1);
        Parcel a = a(11, o);
        g51 a2 = h51.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
