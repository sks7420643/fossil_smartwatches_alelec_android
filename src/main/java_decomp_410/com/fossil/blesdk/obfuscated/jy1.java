package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jy1 extends Binder {
    @DexIgnore
    public /* final */ ey1 e;

    @DexIgnore
    public jy1(ey1 ey1) {
        this.e = ey1;
    }

    @DexIgnore
    public final void a(hy1 hy1) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (this.e.c(hy1.a)) {
                hy1.a();
                return;
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.e.e.execute(new ky1(this, hy1));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
