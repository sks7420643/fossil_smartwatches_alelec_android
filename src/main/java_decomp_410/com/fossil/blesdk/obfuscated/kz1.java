package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.d6;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kz1 {
    @DexIgnore
    public static /* final */ AtomicInteger c; // = new AtomicInteger((int) SystemClock.elapsedRealtime());
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public Bundle b;

    @DexIgnore
    public kz1(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static boolean b(Bundle bundle) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(b(bundle, "gcm.n.e")) || b(bundle, "gcm.n.icon") != null;
    }

    @DexIgnore
    public static String c(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        return b(bundle, "_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
    }

    @DexIgnore
    public static Object[] d(Bundle bundle, String str) {
        String valueOf = String.valueOf(str);
        String b2 = b(bundle, "_loc_args".length() != 0 ? valueOf.concat("_loc_args") : new String(valueOf));
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(b2);
            Object[] objArr = new String[jSONArray.length()];
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return objArr;
        } catch (JSONException unused) {
            String valueOf2 = String.valueOf(str);
            String substring = ("_loc_args".length() != 0 ? valueOf2.concat("_loc_args") : new String(valueOf2)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 41 + String.valueOf(b2).length());
            sb.append("Malformed ");
            sb.append(substring);
            sb.append(": ");
            sb.append(b2);
            sb.append("  Default value will be used.");
            Log.w("FirebaseMessaging", sb.toString());
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x030e  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x031e  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0327  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x032c  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0331  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0349  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x035e  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x021d  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x021f  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x022b  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x025d  */
    public final boolean a(Bundle bundle) {
        boolean z;
        int i;
        Integer a2;
        String d;
        Uri uri;
        String b2;
        Intent intent;
        PendingIntent pendingIntent;
        boolean z2;
        PendingIntent pendingIntent2;
        String str;
        String b3;
        Bundle bundle2 = bundle;
        if (AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(b(bundle2, "gcm.n.noui"))) {
            return true;
        }
        if (!((KeyguardManager) this.a.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            if (!pm0.g()) {
                SystemClock.sleep(10);
            }
            int myPid = Process.myPid();
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.a.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ActivityManager.RunningAppProcessInfo next = it.next();
                    if (next.pid == myPid) {
                        if (next.importance == 100) {
                            z = true;
                        }
                    }
                }
            }
        }
        z = false;
        if (z) {
            return false;
        }
        CharSequence a3 = a(bundle2, "gcm.n.title");
        if (TextUtils.isEmpty(a3)) {
            a3 = this.a.getApplicationInfo().loadLabel(this.a.getPackageManager());
        }
        String a4 = a(bundle2, "gcm.n.body");
        String b4 = b(bundle2, "gcm.n.icon");
        if (!TextUtils.isEmpty(b4)) {
            Resources resources = this.a.getResources();
            i = resources.getIdentifier(b4, ResourceManager.DRAWABLE, this.a.getPackageName());
            if (i == 0 || !a(i)) {
                i = resources.getIdentifier(b4, "mipmap", this.a.getPackageName());
                if (i == 0 || !a(i)) {
                    StringBuilder sb = new StringBuilder(String.valueOf(b4).length() + 61);
                    sb.append("Icon resource ");
                    sb.append(b4);
                    sb.append(" not found. Notification will use default icon.");
                    Log.w("FirebaseMessaging", sb.toString());
                }
            }
            a2 = a(b(bundle2, "gcm.n.color"));
            d = d(bundle);
            if (!TextUtils.isEmpty(d)) {
                uri = null;
            } else if (Endpoints.DEFAULT_NAME.equals(d) || this.a.getResources().getIdentifier(d, OrmLiteConfigUtil.RAW_DIR_NAME, this.a.getPackageName()) == 0) {
                uri = RingtoneManager.getDefaultUri(2);
            } else {
                String packageName = this.a.getPackageName();
                StringBuilder sb2 = new StringBuilder(String.valueOf(packageName).length() + 24 + String.valueOf(d).length());
                sb2.append("android.resource://");
                sb2.append(packageName);
                sb2.append("/raw/");
                sb2.append(d);
                uri = Uri.parse(sb2.toString());
            }
            b2 = b(bundle2, "gcm.n.click_action");
            if (TextUtils.isEmpty(b2)) {
                intent = new Intent(b2);
                intent.setPackage(this.a.getPackageName());
                intent.setFlags(268435456);
            } else {
                Uri c2 = c(bundle);
                if (c2 != null) {
                    intent = new Intent("android.intent.action.VIEW");
                    intent.setPackage(this.a.getPackageName());
                    intent.setData(c2);
                } else {
                    intent = this.a.getPackageManager().getLaunchIntentForPackage(this.a.getPackageName());
                    if (intent == null) {
                        Log.w("FirebaseMessaging", "No activity found to launch app");
                    }
                }
            }
            if (intent != null) {
                pendingIntent = null;
            } else {
                intent.addFlags(67108864);
                Bundle bundle3 = new Bundle(bundle2);
                FirebaseMessagingService.a(bundle3);
                intent.putExtras(bundle3);
                for (String str2 : bundle3.keySet()) {
                    if (str2.startsWith("gcm.n.") || str2.startsWith("gcm.notification.")) {
                        intent.removeExtra(str2);
                    }
                }
                pendingIntent = PendingIntent.getActivity(this.a, c.incrementAndGet(), intent, 1073741824);
            }
            if (bundle2 != null) {
                z2 = false;
            } else {
                z2 = AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(bundle2.getString("google.c.a.e"));
            }
            if (!z2) {
                Intent intent2 = new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN");
                a(intent2, bundle2);
                intent2.putExtra("pending_intent", pendingIntent);
                pendingIntent = zx1.a(this.a, c.incrementAndGet(), intent2, 1073741824);
                Intent intent3 = new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS");
                a(intent3, bundle2);
                pendingIntent2 = zx1.a(this.a, c.incrementAndGet(), intent3, 1073741824);
            } else {
                pendingIntent2 = null;
            }
            String b5 = b(bundle2, "gcm.n.android_channel_id");
            str = "fcm_fallback_notification_channel";
            if (pm0.i() || this.a.getApplicationInfo().targetSdkVersion < 26) {
                str = null;
            } else {
                NotificationManager notificationManager = (NotificationManager) this.a.getSystemService(NotificationManager.class);
                if (!TextUtils.isEmpty(b5)) {
                    if (notificationManager.getNotificationChannel(b5) != null) {
                        str = b5;
                    } else {
                        StringBuilder sb3 = new StringBuilder(String.valueOf(b5).length() + 122);
                        sb3.append("Notification Channel requested (");
                        sb3.append(b5);
                        sb3.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                        Log.w("FirebaseMessaging", sb3.toString());
                    }
                }
                String string = a().getString("com.google.firebase.messaging.default_notification_channel_id");
                if (TextUtils.isEmpty(string)) {
                    Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
                } else if (notificationManager.getNotificationChannel(string) != null) {
                    str = string;
                } else {
                    Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
                }
                if (notificationManager.getNotificationChannel(str) == null) {
                    notificationManager.createNotificationChannel(new NotificationChannel(str, this.a.getString(iz1.fcm_fallback_notification_channel_label), 3));
                }
            }
            d6.c cVar = new d6.c(this.a);
            cVar.a(true);
            cVar.c(i);
            if (!TextUtils.isEmpty(a3)) {
                cVar.b(a3);
            }
            if (!TextUtils.isEmpty(a4)) {
                cVar.a((CharSequence) a4);
                d6.b bVar = new d6.b();
                bVar.a((CharSequence) a4);
                cVar.a((d6.d) bVar);
            }
            if (a2 != null) {
                cVar.a(a2.intValue());
            }
            if (uri != null) {
                cVar.a(uri);
            }
            if (pendingIntent != null) {
                cVar.a(pendingIntent);
            }
            if (pendingIntent2 != null) {
                cVar.b(pendingIntent2);
            }
            if (str != null) {
                cVar.b(str);
            }
            Notification a5 = cVar.a();
            b3 = b(bundle2, "gcm.n.tag");
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Showing notification");
            }
            NotificationManager notificationManager2 = (NotificationManager) this.a.getSystemService("notification");
            if (TextUtils.isEmpty(b3)) {
                long uptimeMillis = SystemClock.uptimeMillis();
                StringBuilder sb4 = new StringBuilder(37);
                sb4.append("FCM-Notification:");
                sb4.append(uptimeMillis);
                b3 = sb4.toString();
            }
            notificationManager2.notify(b3, 0, a5);
            return true;
        }
        int i2 = a().getInt("com.google.firebase.messaging.default_notification_icon", 0);
        if (i2 == 0 || !a(i2)) {
            i2 = this.a.getApplicationInfo().icon;
        }
        i = (i2 == 0 || !a(i2)) ? 17301651 : i2;
        a2 = a(b(bundle2, "gcm.n.color"));
        d = d(bundle);
        if (!TextUtils.isEmpty(d)) {
        }
        b2 = b(bundle2, "gcm.n.click_action");
        if (TextUtils.isEmpty(b2)) {
        }
        if (intent != null) {
        }
        if (bundle2 != null) {
        }
        if (!z2) {
        }
        String b52 = b(bundle2, "gcm.n.android_channel_id");
        str = "fcm_fallback_notification_channel";
        if (pm0.i()) {
        }
        str = null;
        d6.c cVar2 = new d6.c(this.a);
        cVar2.a(true);
        cVar2.c(i);
        if (!TextUtils.isEmpty(a3)) {
        }
        if (!TextUtils.isEmpty(a4)) {
        }
        if (a2 != null) {
        }
        if (uri != null) {
        }
        if (pendingIntent != null) {
        }
        if (pendingIntent2 != null) {
        }
        if (str != null) {
        }
        Notification a52 = cVar2.a();
        b3 = b(bundle2, "gcm.n.tag");
        if (Log.isLoggable("FirebaseMessaging", 3)) {
        }
        NotificationManager notificationManager22 = (NotificationManager) this.a.getSystemService("notification");
        if (TextUtils.isEmpty(b3)) {
        }
        notificationManager22.notify(b3, 0, a52);
        return true;
    }

    @DexIgnore
    public static Uri c(Bundle bundle) {
        String b2 = b(bundle, "gcm.n.link_android");
        if (TextUtils.isEmpty(b2)) {
            b2 = b(bundle, "gcm.n.link");
        }
        if (!TextUtils.isEmpty(b2)) {
            return Uri.parse(b2);
        }
        return null;
    }

    @DexIgnore
    public static String b(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    @DexIgnore
    public static String d(Bundle bundle) {
        String b2 = b(bundle, "gcm.n.sound2");
        return TextUtils.isEmpty(b2) ? b(bundle, "gcm.n.sound") : b2;
    }

    @DexIgnore
    public final String a(Bundle bundle, String str) {
        String b2 = b(bundle, str);
        if (!TextUtils.isEmpty(b2)) {
            return b2;
        }
        String c2 = c(bundle, str);
        if (TextUtils.isEmpty(c2)) {
            return null;
        }
        Resources resources = this.a.getResources();
        int identifier = resources.getIdentifier(c2, LegacyTokenHelper.TYPE_STRING, this.a.getPackageName());
        if (identifier == 0) {
            String valueOf = String.valueOf(str);
            String substring = ("_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 49 + String.valueOf(c2).length());
            sb.append(substring);
            sb.append(" resource not found: ");
            sb.append(c2);
            sb.append(" Default value will be used.");
            Log.w("FirebaseMessaging", sb.toString());
            return null;
        }
        Object[] d = d(bundle, str);
        if (d == null) {
            return resources.getString(identifier);
        }
        try {
            return resources.getString(identifier, d);
        } catch (MissingFormatArgumentException e) {
            String arrays = Arrays.toString(d);
            StringBuilder sb2 = new StringBuilder(String.valueOf(c2).length() + 58 + String.valueOf(arrays).length());
            sb2.append("Missing format argument for ");
            sb2.append(c2);
            sb2.append(": ");
            sb2.append(arrays);
            sb2.append(" Default value will be used.");
            Log.w("FirebaseMessaging", sb2.toString(), e);
            return null;
        }
    }

    @DexIgnore
    @TargetApi(26)
    public final boolean a(int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (!(this.a.getResources().getDrawable(i, (Resources.Theme) null) instanceof AdaptiveIconDrawable)) {
                return true;
            }
            StringBuilder sb = new StringBuilder(77);
            sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
            sb.append(i);
            Log.e("FirebaseMessaging", sb.toString());
            return false;
        } catch (Resources.NotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public final Integer a(String str) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException unused) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 54);
                sb.append("Color ");
                sb.append(str);
                sb.append(" not valid. Notification will use default color.");
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        int i = a().getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i != 0) {
            try {
                return Integer.valueOf(k6.a(this.a, i));
            } catch (Resources.NotFoundException unused2) {
                Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            }
        }
        return null;
    }

    @DexIgnore
    public static void a(Intent intent, Bundle bundle) {
        for (String str : bundle.keySet()) {
            if (str.startsWith("google.c.a.") || str.equals("from")) {
                intent.putExtra(str, bundle.getString(str));
            }
        }
    }

    @DexIgnore
    public final Bundle a() {
        Bundle bundle = this.b;
        if (bundle != null) {
            return bundle;
        }
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException unused) {
        }
        if (applicationInfo == null || applicationInfo.metaData == null) {
            return Bundle.EMPTY;
        }
        this.b = applicationInfo.metaData;
        return this.b;
    }
}
