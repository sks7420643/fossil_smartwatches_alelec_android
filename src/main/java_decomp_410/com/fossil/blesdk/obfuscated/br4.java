package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class br4 extends yq4<Fragment> {
    @DexIgnore
    public br4(Fragment fragment) {
        super(fragment);
    }

    @DexIgnore
    public void a(int i, String... strArr) {
        ((Fragment) b()).requestPermissions(strArr, i);
    }

    @DexIgnore
    public boolean b(String str) {
        return ((Fragment) b()).shouldShowRequestPermissionRationale(str);
    }

    @DexIgnore
    public FragmentManager c() {
        return ((Fragment) b()).getChildFragmentManager();
    }

    @DexIgnore
    public Context a() {
        return ((Fragment) b()).getActivity();
    }
}
