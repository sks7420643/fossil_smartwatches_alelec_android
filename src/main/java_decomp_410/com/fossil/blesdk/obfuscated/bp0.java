package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bp0 extends jk0 {
    @DexIgnore
    public static /* final */ bp0 A; // = g("rpm");
    @DexIgnore
    public static /* final */ bp0 B; // = j("google.android.fitness.GoalV2");
    @DexIgnore
    public static /* final */ bp0 C; // = j("prescription_event");
    @DexIgnore
    public static /* final */ Parcelable.Creator<bp0> CREATOR; // = new up0();
    @DexIgnore
    public static /* final */ bp0 D; // = j("symptom");
    @DexIgnore
    public static /* final */ bp0 E; // = j("google.android.fitness.StrideModel");
    @DexIgnore
    public static /* final */ bp0 F; // = j("google.android.fitness.Device");
    @DexIgnore
    public static /* final */ bp0 G; // = e("revolutions");
    @DexIgnore
    public static /* final */ bp0 H; // = g("calories");
    @DexIgnore
    public static /* final */ bp0 I; // = g("watts");
    @DexIgnore
    public static /* final */ bp0 J; // = g("volume");
    @DexIgnore
    public static /* final */ bp0 K; // = e("meal_type");
    @DexIgnore
    public static /* final */ bp0 L; // = h("food_item");
    @DexIgnore
    public static /* final */ bp0 M; // = i("nutrients");
    @DexIgnore
    public static /* final */ bp0 N; // = g("elevation.change");
    @DexIgnore
    public static /* final */ bp0 O; // = i("elevation.gain");
    @DexIgnore
    public static /* final */ bp0 P; // = i("elevation.loss");
    @DexIgnore
    public static /* final */ bp0 Q; // = g("floors");
    @DexIgnore
    public static /* final */ bp0 R; // = i("floor.gain");
    @DexIgnore
    public static /* final */ bp0 S; // = i("floor.loss");
    @DexIgnore
    public static /* final */ bp0 T; // = h("exercise");
    @DexIgnore
    public static /* final */ bp0 U; // = e("repetitions");
    @DexIgnore
    public static /* final */ bp0 V; // = g("resistance");
    @DexIgnore
    public static /* final */ bp0 W; // = e("resistance_type");
    @DexIgnore
    public static /* final */ bp0 X; // = e("num_segments");
    @DexIgnore
    public static /* final */ bp0 Y; // = g(GoalTrackingSummary.COLUMN_AVERAGE);
    @DexIgnore
    public static /* final */ bp0 Z; // = g("max");
    @DexIgnore
    public static /* final */ bp0 a0; // = g("min");
    @DexIgnore
    public static /* final */ bp0 b0; // = g("low_latitude");
    @DexIgnore
    public static /* final */ bp0 c0; // = g("low_longitude");
    @DexIgnore
    public static /* final */ bp0 d0; // = g("high_latitude");
    @DexIgnore
    public static /* final */ bp0 e0; // = g("high_longitude");
    @DexIgnore
    public static /* final */ bp0 f0; // = e("occurrences");
    @DexIgnore
    public static /* final */ bp0 g0; // = e("sensor_type");
    @DexIgnore
    public static /* final */ bp0 h; // = e(Constants.ACTIVITY);
    @DexIgnore
    public static /* final */ bp0 h0; // = e("sensor_types");
    @DexIgnore
    public static /* final */ bp0 i; // = g("confidence");
    @DexIgnore
    public static /* final */ bp0 i0; // = new bp0("timestamps", 5);
    @DexIgnore
    public static /* final */ bp0 j; // = i("activity_confidence");
    @DexIgnore
    public static /* final */ bp0 j0; // = e("sample_period");
    @DexIgnore
    public static /* final */ bp0 k; // = e("steps");
    @DexIgnore
    public static /* final */ bp0 k0; // = e("num_samples");
    @DexIgnore
    public static /* final */ bp0 l; // = g("step_length");
    @DexIgnore
    public static /* final */ bp0 l0; // = e("num_dimensions");
    @DexIgnore
    public static /* final */ bp0 m; // = e("duration");
    @DexIgnore
    public static /* final */ bp0 m0; // = new bp0("sensor_values", 6);
    @DexIgnore
    public static /* final */ bp0 n; // = i("activity_duration.ascending");
    @DexIgnore
    public static /* final */ bp0 n0; // = g("intensity");
    @DexIgnore
    public static /* final */ bp0 o; // = i("activity_duration.descending");
    @DexIgnore
    public static /* final */ bp0 o0; // = g("probability");
    @DexIgnore
    public static /* final */ bp0 p; // = g("bpm");
    @DexIgnore
    public static /* final */ bp0 q; // = g("latitude");
    @DexIgnore
    public static /* final */ bp0 r; // = g("longitude");
    @DexIgnore
    public static /* final */ bp0 s; // = g(PlaceManager.PARAM_ACCURACY);
    @DexIgnore
    public static /* final */ bp0 t; // = new bp0(PlaceManager.PARAM_ALTITUDE, 2, true);
    @DexIgnore
    public static /* final */ bp0 u; // = g("distance");
    @DexIgnore
    public static /* final */ bp0 v; // = g("height");
    @DexIgnore
    public static /* final */ bp0 w; // = g(Constants.PROFILE_KEY_UNITS_WEIGHT);
    @DexIgnore
    public static /* final */ bp0 x; // = g("circumference");
    @DexIgnore
    public static /* final */ bp0 y; // = g("percentage");
    @DexIgnore
    public static /* final */ bp0 z; // = g(PlaceManager.PARAM_SPEED);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Boolean g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ bp0 a; // = bp0.g("x");
        @DexIgnore
        public static /* final */ bp0 b; // = bp0.g("y");
        @DexIgnore
        public static /* final */ bp0 c; // = bp0.g("z");
        @DexIgnore
        public static /* final */ bp0 d; // = bp0.k("debug_session");
        @DexIgnore
        public static /* final */ bp0 e; // = bp0.k("google.android.fitness.SessionV2");
    }

    /*
    static {
        f("duration");
        i("activity_duration");
    }
    */

    @DexIgnore
    public bp0(String str, int i2) {
        this(str, i2, (Boolean) null);
    }

    @DexIgnore
    public static bp0 e(String str) {
        return new bp0(str, 1);
    }

    @DexIgnore
    public static bp0 f(String str) {
        return new bp0(str, 1, true);
    }

    @DexIgnore
    public static bp0 g(String str) {
        return new bp0(str, 2);
    }

    @DexIgnore
    public static bp0 h(String str) {
        return new bp0(str, 3);
    }

    @DexIgnore
    public static bp0 i(String str) {
        return new bp0(str, 4);
    }

    @DexIgnore
    public static bp0 j(String str) {
        return new bp0(str, 7);
    }

    @DexIgnore
    public static bp0 k(String str) {
        return new bp0(str, 7, true);
    }

    @DexIgnore
    public final int H() {
        return this.f;
    }

    @DexIgnore
    public final String I() {
        return this.e;
    }

    @DexIgnore
    public final Boolean J() {
        return this.g;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bp0)) {
            return false;
        }
        bp0 bp0 = (bp0) obj;
        return this.e.equals(bp0.e) && this.f == bp0.f;
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final String toString() {
        Object[] objArr = new Object[2];
        objArr[0] = this.e;
        objArr[1] = this.f == 1 ? "i" : "f";
        return String.format("%s(%s)", objArr);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = kk0.a(parcel);
        kk0.a(parcel, 1, I(), false);
        kk0.a(parcel, 2, H());
        kk0.a(parcel, 3, J(), false);
        kk0.a(parcel, a2);
    }

    @DexIgnore
    public bp0(String str, int i2, Boolean bool) {
        bk0.a(str);
        this.e = str;
        this.f = i2;
        this.g = bool;
    }
}
