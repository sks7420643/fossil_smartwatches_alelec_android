package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.qx0;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qx0<M extends qx0<M>> extends vx0 {
    @DexIgnore
    public sx0 f;

    @DexIgnore
    public void a(px0 px0) throws IOException {
        if (this.f != null) {
            for (int i = 0; i < this.f.b(); i++) {
                this.f.a(i).a(px0);
            }
        }
    }

    @DexIgnore
    public int b() {
        if (this.f != null) {
            for (int i = 0; i < this.f.b(); i++) {
                this.f.a(i).b();
            }
        }
        return 0;
    }

    @DexIgnore
    public /* synthetic */ vx0 c() throws CloneNotSupportedException {
        return (qx0) clone();
    }

    @DexIgnore
    /* renamed from: d */
    public M clone() throws CloneNotSupportedException {
        M m = (qx0) super.clone();
        ux0.a(this, (qx0) m);
        return m;
    }
}
