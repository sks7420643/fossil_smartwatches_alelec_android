package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.zj0;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w11 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<w11> CREATOR; // = new x11();
    @DexIgnore
    public /* final */ List<DataType> e;

    @DexIgnore
    public w11(List<DataType> list) {
        this.e = list;
    }

    @DexIgnore
    public final List<DataType> H() {
        return Collections.unmodifiableList(this.e);
    }

    @DexIgnore
    public final String toString() {
        zj0.a a = zj0.a((Object) this);
        a.a("dataTypes", this.e);
        return a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.b(parcel, 1, Collections.unmodifiableList(this.e), false);
        kk0.a(parcel, a);
    }
}
