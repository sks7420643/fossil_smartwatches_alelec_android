package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.io.FileNotFoundException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr implements sr<Uri, File> {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tr<Uri, File> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public sr<Uri, File> a(wr wrVar) {
            return new pr(this.a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements so<File> {
        @DexIgnore
        public static /* final */ String[] g; // = {"_data"};
        @DexIgnore
        public /* final */ Context e;
        @DexIgnore
        public /* final */ Uri f;

        @DexIgnore
        public b(Context context, Uri uri) {
            this.e = context;
            this.f = uri;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super File> aVar) {
            Cursor query = this.e.getContentResolver().query(this.f, g, (String) null, (String[]) null, (String) null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.a((Exception) new FileNotFoundException("Failed to find file path for: " + this.f));
                return;
            }
            aVar.a(new File(str));
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<File> getDataClass() {
            return File.class;
        }
    }

    @DexIgnore
    public pr(Context context) {
        this.a = context;
    }

    @DexIgnore
    public sr.a<File> a(Uri uri, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(uri), new b(this.a, uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ep.b(uri);
    }
}
