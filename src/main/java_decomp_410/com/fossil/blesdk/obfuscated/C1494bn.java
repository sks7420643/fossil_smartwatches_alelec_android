package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bn */
public class C1494bn {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.util.Comparator<byte[]> f3786e; // = new com.fossil.blesdk.obfuscated.C1494bn.C1495a();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<byte[]> f3787a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<byte[]> f3788b; // = new java.util.ArrayList(64);

    @DexIgnore
    /* renamed from: c */
    public int f3789c; // = 0;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f3790d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.bn$a */
    public class C1495a implements java.util.Comparator<byte[]> {
        @DexIgnore
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            return bArr.length - bArr2.length;
        }
    }

    @DexIgnore
    public C1494bn(int i) {
        this.f3790d = i;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized byte[] mo9233a(int i) {
        for (int i2 = 0; i2 < this.f3788b.size(); i2++) {
            byte[] bArr = this.f3788b.get(i2);
            if (bArr.length >= i) {
                this.f3789c -= bArr.length;
                this.f3788b.remove(i2);
                this.f3787a.remove(bArr);
                return bArr;
            }
        }
        return new byte[i];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: a */
    public synchronized void mo9232a(byte[] bArr) {
        if (bArr != null) {
            if (bArr.length <= this.f3790d) {
                this.f3787a.add(bArr);
                int binarySearch = java.util.Collections.binarySearch(this.f3788b, bArr, f3786e);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                this.f3788b.add(binarySearch, bArr);
                this.f3789c += bArr.length;
                mo9231a();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized void mo9231a() {
        while (this.f3789c > this.f3790d) {
            byte[] remove = this.f3787a.remove(0);
            this.f3788b.remove(remove);
            this.f3789c -= remove.length;
        }
    }
}
