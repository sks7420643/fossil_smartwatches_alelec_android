package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dt implements aq<BitmapDrawable>, wp {
    @DexIgnore
    public /* final */ Resources e;
    @DexIgnore
    public /* final */ aq<Bitmap> f;

    @DexIgnore
    public dt(Resources resources, aq<Bitmap> aqVar) {
        tw.a(resources);
        this.e = resources;
        tw.a(aqVar);
        this.f = aqVar;
    }

    @DexIgnore
    public static aq<BitmapDrawable> a(Resources resources, aq<Bitmap> aqVar) {
        if (aqVar == null) {
            return null;
        }
        return new dt(resources, aqVar);
    }

    @DexIgnore
    public int b() {
        return this.f.b();
    }

    @DexIgnore
    public Class<BitmapDrawable> c() {
        return BitmapDrawable.class;
    }

    @DexIgnore
    public void d() {
        aq<Bitmap> aqVar = this.f;
        if (aqVar instanceof wp) {
            ((wp) aqVar).d();
        }
    }

    @DexIgnore
    public void a() {
        this.f.a();
    }

    @DexIgnore
    public BitmapDrawable get() {
        return new BitmapDrawable(this.e, this.f.get());
    }
}
