package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j80 extends e80 {
    @DexIgnore
    public GattCharacteristic.CharacteristicId M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ j80(long j, long j2, long j3, short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(j, j2, j3, s, peripheral, (i2 & 32) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        kd4.a((Object) array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId J() {
        return this.M;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        GattCharacteristic.CharacteristicId characteristicId;
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (!(bArr.length == 0)) {
            characteristicId = GattCharacteristic.CharacteristicId.Companion.a(bArr[0]);
        } else {
            characteristicId = GattCharacteristic.CharacteristicId.FTD;
        }
        this.M = characteristicId;
        return wa0.a(a, JSONKey.SOCKET_ID, this.M.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(wa0.a(super.t(), JSONKey.OFFSET, Long.valueOf(this.N)), JSONKey.LENGTH, Long.valueOf(this.O)), JSONKey.TOTAL_LENGTH, Long.valueOf(this.P));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.SOCKET_ID, this.M.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public j80(long j, long j2, long j3, short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.PUT_FILE, r2, RequestId.PUT_FILE, peripheral, i);
        kd4.b(peripheral, "peripheral");
        short s2 = s;
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = GattCharacteristic.CharacteristicId.UNKNOWN;
    }
}
