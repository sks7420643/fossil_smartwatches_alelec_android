package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jf2 extends if2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ScrollView s;
    @DexIgnore
    public long t;

    /*
    static {
        v.put(R.id.cl_low_battery, 1);
        v.put(R.id.ftv_title, 2);
        v.put(R.id.ftv_description_1, 3);
        v.put(R.id.ftv_description_2, 4);
        v.put(R.id.bt_purchase, 5);
        v.put(R.id.ftv_description_3, 6);
        v.put(R.id.bt_need_help, 7);
    }
    */

    @DexIgnore
    public jf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 8, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public jf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[7], objArr[5], objArr[1], objArr[3], objArr[4], objArr[6], objArr[2]);
        this.t = -1;
        this.s = objArr[0];
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
