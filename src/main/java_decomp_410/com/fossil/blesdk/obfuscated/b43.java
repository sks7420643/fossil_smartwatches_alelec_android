package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b43 extends v52<a43> {
    @DexIgnore
    void a(Complication complication);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void b(List<Pair<Complication, String>> list);

    @DexIgnore
    void e(List<Pair<Complication, String>> list);

    @DexIgnore
    void u();
}
