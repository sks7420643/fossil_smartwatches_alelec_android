package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.measurement.AppMeasurement;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dj1 extends pk1 {
    @DexIgnore
    public nj1 c;
    @DexIgnore
    public zi1 d;
    @DexIgnore
    public /* final */ Set<aj1> e; // = new CopyOnWriteArraySet();
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ AtomicReference<String> g; // = new AtomicReference<>();
    @DexIgnore
    public boolean h; // = true;

    @DexIgnore
    public dj1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final String A() {
        qj1 B = this.a.n().B();
        if (B != null) {
            return B.b;
        }
        return null;
    }

    @DexIgnore
    public final String B() {
        qj1 B = this.a.n().B();
        if (B != null) {
            return B.a;
        }
        return null;
    }

    @DexIgnore
    public final String C() {
        if (this.a.A() != null) {
            return this.a.A();
        }
        try {
            return we0.a();
        } catch (IllegalStateException e2) {
            this.a.d().s().a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    @DexIgnore
    public final String D() {
        f();
        return this.g.get();
    }

    @DexIgnore
    public final void E() {
        if (!l().i(p().B()) || !this.a.e() || !this.h) {
            d().z().a("Updating Scion state (FE)");
            q().G();
            return;
        }
        d().z().a("Recording app launch after enabling measurement for the first time (FE)");
        F();
    }

    @DexIgnore
    public final void F() {
        e();
        f();
        v();
        if (this.a.H()) {
            q().F();
            this.h = false;
            String y = k().y();
            if (!TextUtils.isEmpty(y)) {
                h().n();
                if (!y.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", y);
                    b("auto", "_ou", bundle);
                }
            }
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        v();
        f();
        a().a((Runnable) new lj1(this, z));
    }

    @DexIgnore
    public final void b(boolean z) {
        v();
        f();
        a().a((Runnable) new mj1(this, z));
    }

    @DexIgnore
    public final void c(boolean z) {
        e();
        f();
        v();
        d().z().a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        k().a(z);
        E();
    }

    @DexIgnore
    public final void d(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = conditionalUserProperty;
        e();
        v();
        bk0.a(conditionalUserProperty);
        bk0.b(conditionalUserProperty2.mName);
        bk0.b(conditionalUserProperty2.mOrigin);
        bk0.a(conditionalUserProperty2.mValue);
        if (!this.a.e()) {
            d().z().a("Conditional property not sent since collection is disabled");
            return;
        }
        kl1 kl1 = new kl1(conditionalUserProperty2.mName, conditionalUserProperty2.mTriggeredTimestamp, conditionalUserProperty2.mValue, conditionalUserProperty2.mOrigin);
        try {
            hg1 a = j().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mTriggeredEventName, conditionalUserProperty2.mTriggeredEventParams, conditionalUserProperty2.mOrigin, 0, true, false);
            hg1 a2 = j().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mTimedOutEventName, conditionalUserProperty2.mTimedOutEventParams, conditionalUserProperty2.mOrigin, 0, true, false);
            hg1 a3 = j().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mExpiredEventName, conditionalUserProperty2.mExpiredEventParams, conditionalUserProperty2.mOrigin, 0, true, false);
            String str = conditionalUserProperty2.mAppId;
            String str2 = conditionalUserProperty2.mOrigin;
            long j = conditionalUserProperty2.mCreationTimestamp;
            String str3 = conditionalUserProperty2.mTriggerEventName;
            long j2 = conditionalUserProperty2.mTriggerTimeout;
            vl1 vl1 = r3;
            vl1 vl12 = new vl1(str, str2, kl1, j, false, str3, a2, j2, a, conditionalUserProperty2.mTimeToLive, a3);
            q().a(vl1);
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final void e(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = conditionalUserProperty;
        e();
        v();
        bk0.a(conditionalUserProperty);
        bk0.b(conditionalUserProperty2.mName);
        if (!this.a.e()) {
            d().z().a("Conditional property not cleared since collection is disabled");
            return;
        }
        kl1 kl1 = new kl1(conditionalUserProperty2.mName, 0, (Object) null, (String) null);
        try {
            hg1 a = j().a(conditionalUserProperty2.mAppId, conditionalUserProperty2.mExpiredEventName, conditionalUserProperty2.mExpiredEventParams, conditionalUserProperty2.mOrigin, conditionalUserProperty2.mCreationTimestamp, true, false);
            vl1 vl1 = r3;
            vl1 vl12 = new vl1(conditionalUserProperty2.mAppId, conditionalUserProperty2.mOrigin, kl1, conditionalUserProperty2.mCreationTimestamp, conditionalUserProperty2.mActive, conditionalUserProperty2.mTriggerEventName, (hg1) null, conditionalUserProperty2.mTriggerTimeout, (hg1) null, conditionalUserProperty2.mTimeToLive, a);
            q().a(vl1);
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z) {
        a(str, str2, bundle, false, true, c().b());
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, c().b());
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Bundle bundle) {
        f();
        e();
        a(str, str2, j, bundle, true, this.d == null || nl1.h(str2), false, (String) null);
    }

    @DexIgnore
    public final void b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        a().a((Runnable) new fj1(this, str, str2, j, nl1.b(bundle), z, z2, z3, str3));
    }

    @DexIgnore
    public final void c(String str, String str2, Bundle bundle) {
        f();
        e();
        a(str, str2, c().b(), bundle);
    }

    @DexIgnore
    public final void b(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        bk0.a(conditionalUserProperty);
        bk0.b(conditionalUserProperty.mAppId);
        m();
        throw null;
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        dj1 dj1;
        String str4;
        String str5;
        String str6;
        String[] strArr;
        String str7;
        qj1 qj1;
        int i;
        ArrayList arrayList;
        Bundle bundle2;
        String str8 = str;
        String str9 = str2;
        long j2 = j;
        Bundle bundle3 = bundle;
        String str10 = str3;
        bk0.b(str);
        if (!l().d(str10, jg1.r0)) {
            bk0.b(str2);
        }
        bk0.a(bundle);
        e();
        v();
        if (!this.a.e()) {
            d().z().a("Event not sent since app measurement is disabled");
            return;
        }
        int i2 = 0;
        if (!this.f) {
            this.f = true;
            try {
                try {
                    Class.forName("com.google.android.gms.tagmanager.TagManagerService").getDeclaredMethod("initialize", new Class[]{Context.class}).invoke((Object) null, new Object[]{getContext()});
                } catch (Exception e2) {
                    d().v().a("Failed to invoke Tag Manager's initialize() method", e2);
                }
            } catch (ClassNotFoundException unused) {
                d().y().a("Tag Manager is not found and thus will not be used");
            }
        }
        if (z3) {
            b();
            if (!"_iap".equals(str9)) {
                nl1 s = this.a.s();
                int i3 = 2;
                if (s.b(Constants.EVENT, str9)) {
                    if (!s.a(Constants.EVENT, wi1.a, str9)) {
                        i3 = 13;
                    } else if (s.a(Constants.EVENT, 40, str9)) {
                        i3 = 0;
                    }
                }
                if (i3 != 0) {
                    d().u().a("Invalid public event name. Event will not be logged (FE)", i().a(str9));
                    this.a.s();
                    this.a.s().a(i3, "_ev", nl1.a(str9, 40, true), str9 != null ? str2.length() : 0);
                    return;
                }
            }
        }
        b();
        qj1 A = r().A();
        if (A != null && !bundle3.containsKey("_sc")) {
            A.d = true;
        }
        rj1.a(A, bundle3, z && z3);
        boolean equals = "am".equals(str8);
        boolean h2 = nl1.h(str2);
        if (z && this.d != null && !h2 && !equals) {
            d().z().a("Passing event to registered event handler (FE)", i().a(str9), i().a(bundle3));
            this.d.a(str, str2, bundle, j);
        } else if (this.a.H()) {
            int a = j().a(str9);
            if (a != 0) {
                d().u().a("Invalid event name. Event will not be logged (FE)", i().a(str9));
                j();
                String a2 = nl1.a(str9, 40, true);
                if (str9 != null) {
                    i2 = str2.length();
                }
                this.a.s().a(str3, a, "_ev", a2, i2);
                return;
            }
            List a3 = im0.a((T[]) new String[]{"_o", "_sn", "_sc", "_si"});
            String str11 = "_si";
            String str12 = "_o";
            String str13 = "_sc";
            long j3 = j2;
            Bundle a4 = j().a(str3, str2, bundle, a3, z3, true);
            qj1 qj12 = (a4 == null || !a4.containsKey(str13) || !a4.containsKey(str11)) ? null : new qj1(a4.getString("_sn"), a4.getString(str13), Long.valueOf(a4.getLong(str11)).longValue());
            qj1 qj13 = qj12 == null ? A : qj12;
            String str14 = "_ae";
            if (l().s(str10)) {
                b();
                if (r().A() != null && str14.equals(str9)) {
                    long C = t().C();
                    if (C > 0) {
                        j().a(a4, C);
                    }
                }
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(a4);
            long nextLong = j().t().nextLong();
            if (l().d(p().B(), jg1.j0) && k().s.a() > 0 && k().a(j3) && k().v.a()) {
                d().A().a("Current session is expired, remove the session number and Id");
                if (l().d(p().B(), jg1.f0)) {
                    a("auto", "_sid", (Object) null, c().b());
                }
                if (l().d(p().B(), jg1.g0)) {
                    a("auto", "_sno", (Object) null, c().b());
                }
            }
            if (!l().r(p().B()) || a4.getLong("extend_session", 0) != 1) {
                long j4 = j3;
                dj1 = this;
            } else {
                d().A().a("EXTEND_SESSION param attached: initiate a new session or extend the current active session");
                long j5 = j3;
                dj1 = this;
                dj1.a.p().a(j5, true);
            }
            String[] strArr2 = (String[]) a4.keySet().toArray(new String[bundle.size()]);
            Arrays.sort(strArr2);
            int length = strArr2.length;
            int i4 = 0;
            int i5 = 0;
            while (true) {
                str4 = "_eid";
                if (i4 >= length) {
                    break;
                }
                String str15 = strArr2[i4];
                Object obj = a4.get(str15);
                j();
                Bundle[] a5 = nl1.a(obj);
                if (a5 != null) {
                    strArr = strArr2;
                    a4.putInt(str15, a5.length);
                    i = length;
                    int i6 = 0;
                    while (i6 < a5.length) {
                        Bundle bundle4 = a5[i6];
                        rj1.a(qj13, bundle4, true);
                        String str16 = str4;
                        Bundle bundle5 = bundle4;
                        ArrayList arrayList3 = arrayList2;
                        Bundle a6 = j().a(str3, "_ep", bundle5, a3, z3, false);
                        a6.putString("_en", str2);
                        nextLong = nextLong;
                        a6.putLong(str16, nextLong);
                        String str17 = str15;
                        a6.putString("_gn", str17);
                        a6.putInt("_ll", a5.length);
                        a6.putInt("_i", i6);
                        arrayList3.add(a6);
                        i6++;
                        a4 = a4;
                        str4 = str16;
                        str15 = str17;
                        qj13 = qj13;
                        str14 = str14;
                        long j6 = j;
                        arrayList2 = arrayList3;
                    }
                    String str18 = str2;
                    qj1 = qj13;
                    arrayList = arrayList2;
                    str7 = str14;
                    bundle2 = a4;
                    i5 += a5.length;
                } else {
                    String str19 = str2;
                    qj1 = qj13;
                    strArr = strArr2;
                    i = length;
                    arrayList = arrayList2;
                    str7 = str14;
                    bundle2 = a4;
                }
                i4++;
                long j7 = j;
                strArr2 = strArr;
                a4 = bundle2;
                arrayList2 = arrayList;
                length = i;
                qj13 = qj1;
                str14 = str7;
            }
            String str20 = str4;
            ArrayList arrayList4 = arrayList2;
            String str21 = str14;
            Bundle bundle6 = a4;
            String str22 = str2;
            if (i5 != 0) {
                bundle6.putLong(str20, nextLong);
                bundle6.putInt("_epc", i5);
            }
            int i7 = 0;
            while (i7 < arrayList4.size()) {
                Bundle bundle7 = (Bundle) arrayList4.get(i7);
                if (i7 != 0) {
                    str6 = "_ep";
                    str5 = str;
                } else {
                    str5 = str;
                    str6 = str22;
                }
                String str23 = str12;
                bundle7.putString(str23, str5);
                if (z2) {
                    bundle7 = j().a(bundle7);
                }
                Bundle bundle8 = bundle7;
                d().z().a("Logging event (FE)", i().a(str22), i().a(bundle8));
                ArrayList arrayList5 = arrayList4;
                q().a(new hg1(str6, new eg1(bundle8), str, j), str3);
                if (!equals) {
                    for (aj1 onEvent : dj1.e) {
                        onEvent.onEvent(str, str2, new Bundle(bundle8), j);
                    }
                }
                i7++;
                str12 = str23;
                arrayList4 = arrayList5;
            }
            b();
            if (r().A() != null && str21.equals(str22)) {
                t().a(true, true);
            }
        }
    }

    @DexIgnore
    public final void c(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        long b = c().b();
        bk0.a(conditionalUserProperty);
        bk0.b(conditionalUserProperty.mName);
        bk0.b(conditionalUserProperty.mOrigin);
        bk0.a(conditionalUserProperty.mValue);
        conditionalUserProperty.mCreationTimestamp = b;
        String str = conditionalUserProperty.mName;
        Object obj = conditionalUserProperty.mValue;
        if (j().b(str) != 0) {
            d().s().a("Invalid conditional user property name", i().c(str));
        } else if (j().b(str, obj) != 0) {
            d().s().a("Invalid conditional user property value", i().c(str), obj);
        } else {
            Object c2 = j().c(str, obj);
            if (c2 == null) {
                d().s().a("Unable to normalize conditional user property value", i().c(str), obj);
                return;
            }
            conditionalUserProperty.mValue = c2;
            long j = conditionalUserProperty.mTriggerTimeout;
            if (TextUtils.isEmpty(conditionalUserProperty.mTriggerEventName) || (j <= 15552000000L && j >= 1)) {
                long j2 = conditionalUserProperty.mTimeToLive;
                if (j2 > 15552000000L || j2 < 1) {
                    d().s().a("Invalid conditional user property time to live", i().c(str), Long.valueOf(j2));
                } else {
                    a().a((Runnable) new hj1(this, conditionalUserProperty));
                }
            } else {
                d().s().a("Invalid conditional user property timeout", i().c(str), Long.valueOf(j));
            }
        }
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Bundle bundle) {
        long b = c().b();
        bk0.b(str2);
        AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
        conditionalUserProperty.mAppId = str;
        conditionalUserProperty.mName = str2;
        conditionalUserProperty.mCreationTimestamp = b;
        if (str3 != null) {
            conditionalUserProperty.mExpiredEventName = str3;
            conditionalUserProperty.mExpiredEventParams = bundle;
        }
        a().a((Runnable) new ij1(this, conditionalUserProperty));
    }

    @DexIgnore
    public final List<AppMeasurement.ConditionalUserProperty> b(String str, String str2) {
        f();
        return b((String) null, str, str2);
    }

    @DexIgnore
    public final List<AppMeasurement.ConditionalUserProperty> b(String str, String str2, String str3) {
        if (a().s()) {
            d().s().a("Cannot get conditional user properties from analytics worker thread");
            return Collections.emptyList();
        } else if (ul1.a()) {
            d().s().a("Cannot get conditional user properties from main thread");
            return Collections.emptyList();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.a.a().a((Runnable) new jj1(this, atomicReference, str, str2, str3));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    d().v().a("Interrupted waiting for get conditional user properties", str, e2);
                }
            }
            List<vl1> list = (List) atomicReference.get();
            if (list == null) {
                d().v().a("Timed out waiting for get conditional user properties", str);
                return Collections.emptyList();
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (vl1 vl1 : list) {
                AppMeasurement.ConditionalUserProperty conditionalUserProperty = new AppMeasurement.ConditionalUserProperty();
                conditionalUserProperty.mAppId = vl1.e;
                conditionalUserProperty.mOrigin = vl1.f;
                conditionalUserProperty.mCreationTimestamp = vl1.h;
                kl1 kl1 = vl1.g;
                conditionalUserProperty.mName = kl1.f;
                conditionalUserProperty.mValue = kl1.H();
                conditionalUserProperty.mActive = vl1.i;
                conditionalUserProperty.mTriggerEventName = vl1.j;
                hg1 hg1 = vl1.k;
                if (hg1 != null) {
                    conditionalUserProperty.mTimedOutEventName = hg1.e;
                    eg1 eg1 = hg1.f;
                    if (eg1 != null) {
                        conditionalUserProperty.mTimedOutEventParams = eg1.H();
                    }
                }
                conditionalUserProperty.mTriggerTimeout = vl1.l;
                hg1 hg12 = vl1.m;
                if (hg12 != null) {
                    conditionalUserProperty.mTriggeredEventName = hg12.e;
                    eg1 eg12 = hg12.f;
                    if (eg12 != null) {
                        conditionalUserProperty.mTriggeredEventParams = eg12.H();
                    }
                }
                conditionalUserProperty.mTriggeredTimestamp = vl1.g.g;
                conditionalUserProperty.mTimeToLive = vl1.n;
                hg1 hg13 = vl1.o;
                if (hg13 != null) {
                    conditionalUserProperty.mExpiredEventName = hg13.e;
                    eg1 eg13 = hg13.f;
                    if (eg13 != null) {
                        conditionalUserProperty.mExpiredEventParams = eg13.H();
                    }
                }
                arrayList.add(conditionalUserProperty);
            }
            return arrayList;
        }
    }

    @DexIgnore
    public final Map<String, Object> b(String str, String str2, String str3, boolean z) {
        if (a().s()) {
            d().s().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (ul1.a()) {
            d().s().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                this.a.a().a((Runnable) new kj1(this, atomicReference, str, str2, str3, z));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    d().v().a("Interrupted waiting for get user properties", e2);
                }
            }
            List<kl1> list = (List) atomicReference.get();
            if (list == null) {
                d().v().a("Timed out waiting for get user properties");
                return Collections.emptyMap();
            }
            g4 g4Var = new g4(list.size());
            for (kl1 kl1 : list) {
                g4Var.put(kl1.f, kl1.H());
            }
            return g4Var;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        f();
        b(str == null ? "app" : str, str2, j, bundle == null ? new Bundle() : bundle, z2, !z2 || this.d == null || nl1.h(str2), !z, (String) null);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z) {
        a(str, str2, obj, z, c().b());
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i = 6;
        int i2 = 0;
        if (z) {
            i = j().b(str2);
        } else {
            nl1 j2 = j();
            if (j2.b("user property", str2)) {
                if (!j2.a("user property", yi1.a, str2)) {
                    i = 15;
                } else if (j2.a("user property", 24, str2)) {
                    i = 0;
                }
            }
        }
        if (i != 0) {
            j();
            String a = nl1.a(str2, 24, true);
            if (str2 != null) {
                i2 = str2.length();
            }
            this.a.s().a(i, "_ev", a, i2);
        } else if (obj != null) {
            int b = j().b(str2, obj);
            if (b != 0) {
                j();
                String a2 = nl1.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i2 = String.valueOf(obj).length();
                }
                this.a.s().a(b, "_ev", a2, i2);
                return;
            }
            Object c2 = j().c(str2, obj);
            if (c2 != null) {
                a(str3, str2, j, c2);
            }
        } else {
            a(str3, str2, j, (Object) null);
        }
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Object obj) {
        a().a((Runnable) new gj1(this, str, str2, obj, j));
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, long j) {
        bk0.b(str);
        bk0.b(str2);
        e();
        f();
        v();
        if (!this.a.e()) {
            d().z().a("User property not set since app measurement is disabled");
        } else if (this.a.H()) {
            d().z().a("Setting user property (FE)", i().a(str2), obj);
            q().a(new kl1(str2, j, obj, str));
        }
    }

    @DexIgnore
    public final void a(String str) {
        this.g.set(str);
    }

    @DexIgnore
    public final void a(aj1 aj1) {
        f();
        v();
        bk0.a(aj1);
        if (!this.e.add(aj1)) {
            d().v().a("OnEventListener already registered");
        }
    }

    @DexIgnore
    public final void a(AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        bk0.a(conditionalUserProperty);
        f();
        AppMeasurement.ConditionalUserProperty conditionalUserProperty2 = new AppMeasurement.ConditionalUserProperty(conditionalUserProperty);
        if (!TextUtils.isEmpty(conditionalUserProperty2.mAppId)) {
            d().v().a("Package name should be null when calling setConditionalUserProperty");
        }
        conditionalUserProperty2.mAppId = null;
        c(conditionalUserProperty2);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        f();
        b((String) null, str, str2, bundle);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, Bundle bundle) {
        bk0.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final List<AppMeasurement.ConditionalUserProperty> a(String str, String str2, String str3) {
        bk0.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        f();
        return b((String) null, str, str2, z);
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        bk0.b(str);
        m();
        throw null;
    }
}
