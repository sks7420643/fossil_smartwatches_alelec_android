package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p50 extends SingleRequestPhase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> B; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p50(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.PLAY_ANIMATION, new y70(peripheral));
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.B;
    }
}
