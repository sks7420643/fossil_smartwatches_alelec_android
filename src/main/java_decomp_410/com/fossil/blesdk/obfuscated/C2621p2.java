package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p2 */
public class C2621p2 extends androidx.appcompat.widget.ListPopupWindow implements com.fossil.blesdk.obfuscated.C2520o2 {

    @DexIgnore
    /* renamed from: N */
    public static java.lang.reflect.Method f8269N;

    @DexIgnore
    /* renamed from: M */
    public com.fossil.blesdk.obfuscated.C2520o2 f8270M;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.p2$a */
    public static class C2622a extends com.fossil.blesdk.obfuscated.C2279l2 {

        @DexIgnore
        /* renamed from: s */
        public /* final */ int f8271s;

        @DexIgnore
        /* renamed from: t */
        public /* final */ int f8272t;

        @DexIgnore
        /* renamed from: u */
        public com.fossil.blesdk.obfuscated.C2520o2 f8273u;

        @DexIgnore
        /* renamed from: v */
        public android.view.MenuItem f8274v;

        @DexIgnore
        public C2622a(android.content.Context context, boolean z) {
            super(context, z);
            android.content.res.Configuration configuration = context.getResources().getConfiguration();
            if (android.os.Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.f8271s = 22;
                this.f8272t = 21;
                return;
            }
            this.f8271s = 21;
            this.f8272t = 22;
        }

        @DexIgnore
        public boolean onHoverEvent(android.view.MotionEvent motionEvent) {
            int i;
            com.fossil.blesdk.obfuscated.C1852g1 g1Var;
            if (this.f8273u != null) {
                android.widget.ListAdapter adapter = getAdapter();
                if (adapter instanceof android.widget.HeaderViewListAdapter) {
                    android.widget.HeaderViewListAdapter headerViewListAdapter = (android.widget.HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    g1Var = (com.fossil.blesdk.obfuscated.C1852g1) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    g1Var = (com.fossil.blesdk.obfuscated.C1852g1) adapter;
                }
                com.fossil.blesdk.obfuscated.C2179k1 k1Var = null;
                if (motionEvent.getAction() != 10) {
                    int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (pointToPosition != -1) {
                        int i2 = pointToPosition - i;
                        if (i2 >= 0 && i2 < g1Var.getCount()) {
                            k1Var = g1Var.getItem(i2);
                        }
                    }
                }
                android.view.MenuItem menuItem = this.f8274v;
                if (menuItem != k1Var) {
                    com.fossil.blesdk.obfuscated.C1915h1 b = g1Var.mo11101b();
                    if (menuItem != null) {
                        this.f8273u.mo10278b(b, menuItem);
                    }
                    this.f8274v = k1Var;
                    if (k1Var != null) {
                        this.f8273u.mo10277a(b, k1Var);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        @DexIgnore
        public boolean onKeyDown(int i, android.view.KeyEvent keyEvent) {
            androidx.appcompat.view.menu.ListMenuItemView listMenuItemView = (androidx.appcompat.view.menu.ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.f8271s) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.f8272t) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((com.fossil.blesdk.obfuscated.C1852g1) getAdapter()).mo11101b().mo11464a(false);
                return true;
            }
        }

        @DexIgnore
        public void setHoverListener(com.fossil.blesdk.obfuscated.C2520o2 o2Var) {
            this.f8273u = o2Var;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ void setSelector(android.graphics.drawable.Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    /*
    static {
        try {
            f8269N = android.widget.PopupWindow.class.getDeclaredMethod("setTouchModal", new java.lang.Class[]{java.lang.Boolean.TYPE});
        } catch (java.lang.NoSuchMethodException unused) {
            android.util.Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    */

    @DexIgnore
    public C2621p2(android.content.Context context, android.util.AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2279l2 mo848a(android.content.Context context, boolean z) {
        com.fossil.blesdk.obfuscated.C2621p2.C2622a aVar = new com.fossil.blesdk.obfuscated.C2621p2.C2622a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14608b(java.lang.Object obj) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.f460I.setExitTransition((android.transition.Transition) obj);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo14609d(boolean z) {
        java.lang.reflect.Method method = f8269N;
        if (method != null) {
            try {
                method.invoke(this.f460I, new java.lang.Object[]{java.lang.Boolean.valueOf(z)});
            } catch (java.lang.Exception unused) {
                android.util.Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14607a(java.lang.Object obj) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            this.f460I.setEnterTransition((android.transition.Transition) obj);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10278b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        com.fossil.blesdk.obfuscated.C2520o2 o2Var = this.f8270M;
        if (o2Var != null) {
            o2Var.mo10278b(h1Var, menuItem);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14606a(com.fossil.blesdk.obfuscated.C2520o2 o2Var) {
        this.f8270M = o2Var;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10277a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        com.fossil.blesdk.obfuscated.C2520o2 o2Var = this.f8270M;
        if (o2Var != null) {
            o2Var.mo10277a(h1Var, menuItem);
        }
    }
}
