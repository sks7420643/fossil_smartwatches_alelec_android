package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class md2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleEditText s;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport t;
    @DexIgnore
    public /* final */ FlexibleTextView u;

    @DexIgnore
    public md2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, ConstraintLayout constraintLayout, FlexibleEditText flexibleEditText, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = flexibleEditText;
        this.t = recyclerViewEmptySupport;
        this.u = flexibleTextView2;
    }
}
