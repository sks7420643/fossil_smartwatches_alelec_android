package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fn */
public final class C1815fn {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f5225a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2660pm> f5226b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f5227c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.io.InputStream f5228d;

    @DexIgnore
    public C1815fn(int i, java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list) {
        this(i, list, -1, (java.io.InputStream) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final java.io.InputStream mo10970a() {
        return this.f5228d;
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo10971b() {
        return this.f5227c;
    }

    @DexIgnore
    /* renamed from: c */
    public final java.util.List<com.fossil.blesdk.obfuscated.C2660pm> mo10972c() {
        return java.util.Collections.unmodifiableList(this.f5226b);
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo10973d() {
        return this.f5225a;
    }

    @DexIgnore
    public C1815fn(int i, java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list, int i2, java.io.InputStream inputStream) {
        this.f5225a = i;
        this.f5226b = list;
        this.f5227c = i2;
        this.f5228d = inputStream;
    }
}
