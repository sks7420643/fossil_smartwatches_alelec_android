package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jv {
    @DexIgnore
    public /* final */ List<String> a; // = new ArrayList();
    @DexIgnore
    public /* final */ Map<String, List<a<?, ?>>> b; // = new HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T, R> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ mo<T, R> c;

        @DexIgnore
        public a(Class<T> cls, Class<R> cls2, mo<T, R> moVar) {
            this.a = cls;
            this.b = cls2;
            this.c = moVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public synchronized void a(List<String> list) {
        ArrayList<String> arrayList = new ArrayList<>(this.a);
        this.a.clear();
        this.a.addAll(list);
        for (String str : arrayList) {
            if (!list.contains(str)) {
                this.a.add(str);
            }
        }
    }

    @DexIgnore
    public synchronized <T, R> List<Class<R>> b(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.a) {
            List<a> list = this.b.get(str);
            if (list != null) {
                for (a aVar : list) {
                    if (aVar.a(cls, cls2) && !arrayList.contains(aVar.b)) {
                        arrayList.add(aVar.b);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <T, R> List<mo<T, R>> a(Class<T> cls, Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.a) {
            List<a> list = this.b.get(str);
            if (list != null) {
                for (a aVar : list) {
                    if (aVar.a(cls, cls2)) {
                        arrayList.add(aVar.c);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <T, R> void a(String str, mo<T, R> moVar, Class<T> cls, Class<R> cls2) {
        a(str).add(new a(cls, cls2, moVar));
    }

    @DexIgnore
    public final synchronized List<a<?, ?>> a(String str) {
        List<a<?, ?>> list;
        if (!this.a.contains(str)) {
            this.a.add(str);
        }
        list = this.b.get(str);
        if (list == null) {
            list = new ArrayList<>();
            this.b.put(str, list);
        }
        return list;
    }
}
