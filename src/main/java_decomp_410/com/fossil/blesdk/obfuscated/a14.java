package com.fossil.blesdk.obfuscated;

import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a14 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        if (h04.s() && j04.r != null) {
            if (h04.p()) {
                g14.b(j04.r).a((o04) new n04(j04.r, j04.a(j04.r, false, (k04) null), 2, th, thread, (k04) null), (p24) null, false, true);
                j04.m.b((Object) "MTA has caught the following uncaught exception:");
                j04.m.b(th);
            }
            j04.f(j04.r);
            if (j04.n != null) {
                j04.m.a((Object) "Call the original uncaught exception handler.");
                if (!(j04.n instanceof a14)) {
                    j04.n.uncaughtException(thread, th);
                }
            }
        }
    }
}
