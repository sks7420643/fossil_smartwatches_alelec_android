package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import androidx.work.WorkInfo;
import com.fossil.blesdk.obfuscated.hl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jl implements il {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ lf b;
    @DexIgnore
    public /* final */ wf c;
    @DexIgnore
    public /* final */ wf d;
    @DexIgnore
    public /* final */ wf e;
    @DexIgnore
    public /* final */ wf f;
    @DexIgnore
    public /* final */ wf g;
    @DexIgnore
    public /* final */ wf h;
    @DexIgnore
    public /* final */ wf i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lf<hl> {
        @DexIgnore
        public a(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(kg kgVar, hl hlVar) {
            String str = hlVar.a;
            if (str == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, str);
            }
            kgVar.b(2, (long) nl.a(hlVar.b));
            String str2 = hlVar.c;
            if (str2 == null) {
                kgVar.a(3);
            } else {
                kgVar.a(3, str2);
            }
            String str3 = hlVar.d;
            if (str3 == null) {
                kgVar.a(4);
            } else {
                kgVar.a(4, str3);
            }
            byte[] a = aj.a(hlVar.e);
            if (a == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, a);
            }
            byte[] a2 = aj.a(hlVar.f);
            if (a2 == null) {
                kgVar.a(6);
            } else {
                kgVar.a(6, a2);
            }
            kgVar.b(7, hlVar.g);
            kgVar.b(8, hlVar.h);
            kgVar.b(9, hlVar.i);
            kgVar.b(10, (long) hlVar.k);
            kgVar.b(11, (long) nl.a(hlVar.l));
            kgVar.b(12, hlVar.m);
            kgVar.b(13, hlVar.n);
            kgVar.b(14, hlVar.o);
            kgVar.b(15, hlVar.p);
            yi yiVar = hlVar.j;
            if (yiVar != null) {
                kgVar.b(16, (long) nl.a(yiVar.b()));
                kgVar.b(17, yiVar.g() ? 1 : 0);
                kgVar.b(18, yiVar.h() ? 1 : 0);
                kgVar.b(19, yiVar.f() ? 1 : 0);
                kgVar.b(20, yiVar.i() ? 1 : 0);
                kgVar.b(21, yiVar.c());
                kgVar.b(22, yiVar.d());
                byte[] a3 = nl.a(yiVar.a());
                if (a3 == null) {
                    kgVar.a(23);
                } else {
                    kgVar.a(23, a3);
                }
            } else {
                kgVar.a(16);
                kgVar.a(17);
                kgVar.a(18);
                kgVar.a(19);
                kgVar.a(20);
                kgVar.a(21);
                kgVar.a(22);
                kgVar.a(23);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec`(`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends wf {
        @DexIgnore
        public b(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends wf {
        @DexIgnore
        public c(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends wf {
        @DexIgnore
        public d(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends wf {
        @DexIgnore
        public e(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends wf {
        @DexIgnore
        public f(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends wf {
        @DexIgnore
        public g(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends wf {
        @DexIgnore
        public h(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends wf {
        @DexIgnore
        public i(jl jlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public jl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
        this.e = new d(this, roomDatabase);
        this.f = new e(this, roomDatabase);
        this.g = new f(this, roomDatabase);
        this.h = new g(this, roomDatabase);
        this.i = new h(this, roomDatabase);
        new i(this, roomDatabase);
    }

    @DexIgnore
    public void a(hl hlVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(hlVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c(String str) {
        uf b2 = uf.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int d() {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.i.acquire();
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.i.release(acquire);
        }
    }

    @DexIgnore
    public hl e(String str) {
        uf ufVar;
        hl hlVar;
        String str2 = str;
        uf b2 = uf.b("SELECT * FROM workspec WHERE id=?", 1);
        if (str2 == null) {
            b2.a(1);
        } else {
            b2.a(1, str2);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, "state");
            int b5 = ag.b(a2, "worker_class_name");
            int b6 = ag.b(a2, "input_merger_class_name");
            int b7 = ag.b(a2, "input");
            int b8 = ag.b(a2, "output");
            int b9 = ag.b(a2, "initial_delay");
            int b10 = ag.b(a2, "interval_duration");
            int b11 = ag.b(a2, "flex_duration");
            int b12 = ag.b(a2, "run_attempt_count");
            int b13 = ag.b(a2, "backoff_policy");
            int b14 = ag.b(a2, "backoff_delay_duration");
            int b15 = ag.b(a2, "period_start_time");
            int b16 = ag.b(a2, "minimum_retention_duration");
            ufVar = b2;
            try {
                int b17 = ag.b(a2, "schedule_requested_at");
                int b18 = ag.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ag.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ag.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ag.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ag.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ag.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ag.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ag.b(a2, "content_uri_triggers");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b3);
                    String string2 = a2.getString(b5);
                    int i9 = b9;
                    yi yiVar = new yi();
                    yiVar.a(nl.b(a2.getInt(b18)));
                    yiVar.b(a2.getInt(b19) != 0);
                    yiVar.c(a2.getInt(b20) != 0);
                    yiVar.a(a2.getInt(b21) != 0);
                    yiVar.d(a2.getInt(b22) != 0);
                    yiVar.a(a2.getLong(b23));
                    yiVar.b(a2.getLong(b24));
                    yiVar.a(nl.a(a2.getBlob(b25)));
                    hlVar = new hl(string, string2);
                    hlVar.b = nl.c(a2.getInt(b4));
                    hlVar.d = a2.getString(b6);
                    hlVar.e = aj.b(a2.getBlob(b7));
                    hlVar.f = aj.b(a2.getBlob(b8));
                    hlVar.g = a2.getLong(i9);
                    hlVar.h = a2.getLong(i8);
                    hlVar.i = a2.getLong(i7);
                    hlVar.k = a2.getInt(i6);
                    hlVar.l = nl.a(a2.getInt(i5));
                    hlVar.m = a2.getLong(i4);
                    hlVar.n = a2.getLong(i3);
                    hlVar.o = a2.getLong(i2);
                    hlVar.p = a2.getLong(b17);
                    hlVar.j = yiVar;
                } else {
                    hlVar = null;
                }
                a2.close();
                ufVar.c();
                return hlVar;
            } catch (Throwable th) {
                th = th;
                a2.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b2;
            a2.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int f(String str) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.g.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    public List<aj> g(String str) {
        uf b2 = uf.b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(aj.b(a2.getBlob(0)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int h(String str) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.f.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    public void a(String str, aj ajVar) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.d.acquire();
        byte[] a2 = aj.a(ajVar);
        if (a2 == null) {
            acquire.a(1);
        } else {
            acquire.a(1, a2);
        }
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    public WorkInfo.State d(String str) {
        uf b2 = uf.b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? nl.c(a2.getInt(0)) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public void b(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.e.acquire();
        acquire.b(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c() {
        uf b2 = uf.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int a(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.h.acquire();
        acquire.b(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    public List<hl> b() {
        uf ufVar;
        uf b2 = uf.b("SELECT * FROM workspec WHERE state=1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, "state");
            int b5 = ag.b(a2, "worker_class_name");
            int b6 = ag.b(a2, "input_merger_class_name");
            int b7 = ag.b(a2, "input");
            int b8 = ag.b(a2, "output");
            int b9 = ag.b(a2, "initial_delay");
            int b10 = ag.b(a2, "interval_duration");
            int b11 = ag.b(a2, "flex_duration");
            int b12 = ag.b(a2, "run_attempt_count");
            int b13 = ag.b(a2, "backoff_policy");
            int b14 = ag.b(a2, "backoff_delay_duration");
            int b15 = ag.b(a2, "period_start_time");
            int b16 = ag.b(a2, "minimum_retention_duration");
            ufVar = b2;
            try {
                int b17 = ag.b(a2, "schedule_requested_at");
                int b18 = ag.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ag.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ag.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ag.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ag.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ag.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ag.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ag.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    yi yiVar = new yi();
                    int i13 = b18;
                    yiVar.a(nl.b(a2.getInt(b18)));
                    yiVar.b(a2.getInt(b19) != 0);
                    yiVar.c(a2.getInt(b20) != 0);
                    yiVar.a(a2.getInt(b21) != 0);
                    yiVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    yiVar.a(a2.getLong(b23));
                    yiVar.b(a2.getLong(b24));
                    yiVar.a(nl.a(a2.getBlob(b25)));
                    hl hlVar = new hl(string, string2);
                    hlVar.b = nl.c(a2.getInt(b4));
                    hlVar.d = a2.getString(b6);
                    hlVar.e = aj.b(a2.getBlob(b7));
                    int i16 = i10;
                    hlVar.f = aj.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    hlVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    hlVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    hlVar.i = a2.getLong(i20);
                    int i21 = i6;
                    hlVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    hlVar.l = nl.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    hlVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    hlVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    hlVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    hlVar.p = a2.getLong(i29);
                    hlVar.j = yiVar;
                    arrayList.add(hlVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b2;
            a2.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<hl.b> a(String str) {
        uf b2 = uf.b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, "state");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                hl.b bVar = new hl.b();
                bVar.a = a2.getString(b3);
                bVar.b = nl.c(a2.getInt(b4));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public List<hl> a(int i2) {
        uf ufVar;
        uf b2 = uf.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b2.b(1, (long) i2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, "state");
            int b5 = ag.b(a2, "worker_class_name");
            int b6 = ag.b(a2, "input_merger_class_name");
            int b7 = ag.b(a2, "input");
            int b8 = ag.b(a2, "output");
            int b9 = ag.b(a2, "initial_delay");
            int b10 = ag.b(a2, "interval_duration");
            int b11 = ag.b(a2, "flex_duration");
            int b12 = ag.b(a2, "run_attempt_count");
            int b13 = ag.b(a2, "backoff_policy");
            int b14 = ag.b(a2, "backoff_delay_duration");
            int b15 = ag.b(a2, "period_start_time");
            int b16 = ag.b(a2, "minimum_retention_duration");
            ufVar = b2;
            try {
                int b17 = ag.b(a2, "schedule_requested_at");
                int b18 = ag.b(a2, "required_network_type");
                int i3 = b16;
                int b19 = ag.b(a2, "requires_charging");
                int i4 = b15;
                int b20 = ag.b(a2, "requires_device_idle");
                int i5 = b14;
                int b21 = ag.b(a2, "requires_battery_not_low");
                int i6 = b13;
                int b22 = ag.b(a2, "requires_storage_not_low");
                int i7 = b12;
                int b23 = ag.b(a2, "trigger_content_update_delay");
                int i8 = b11;
                int b24 = ag.b(a2, "trigger_max_content_delay");
                int i9 = b10;
                int b25 = ag.b(a2, "content_uri_triggers");
                int i10 = b9;
                int i11 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i12 = b3;
                    String string2 = a2.getString(b5);
                    int i13 = b5;
                    yi yiVar = new yi();
                    int i14 = b18;
                    yiVar.a(nl.b(a2.getInt(b18)));
                    yiVar.b(a2.getInt(b19) != 0);
                    yiVar.c(a2.getInt(b20) != 0);
                    yiVar.a(a2.getInt(b21) != 0);
                    yiVar.d(a2.getInt(b22) != 0);
                    int i15 = b21;
                    int i16 = b19;
                    yiVar.a(a2.getLong(b23));
                    yiVar.b(a2.getLong(b24));
                    yiVar.a(nl.a(a2.getBlob(b25)));
                    hl hlVar = new hl(string, string2);
                    hlVar.b = nl.c(a2.getInt(b4));
                    hlVar.d = a2.getString(b6);
                    hlVar.e = aj.b(a2.getBlob(b7));
                    int i17 = i11;
                    hlVar.f = aj.b(a2.getBlob(i17));
                    i11 = i17;
                    int i18 = i16;
                    int i19 = i10;
                    hlVar.g = a2.getLong(i19);
                    i10 = i19;
                    int i20 = i9;
                    hlVar.h = a2.getLong(i20);
                    i9 = i20;
                    int i21 = b20;
                    int i22 = i8;
                    hlVar.i = a2.getLong(i22);
                    int i23 = i7;
                    hlVar.k = a2.getInt(i23);
                    int i24 = i6;
                    i7 = i23;
                    hlVar.l = nl.a(a2.getInt(i24));
                    i8 = i22;
                    int i25 = i5;
                    int i26 = i21;
                    hlVar.m = a2.getLong(i25);
                    int i27 = i25;
                    i6 = i24;
                    int i28 = i4;
                    hlVar.n = a2.getLong(i28);
                    i4 = i28;
                    int i29 = i3;
                    hlVar.o = a2.getLong(i29);
                    i3 = i29;
                    int i30 = i27;
                    int i31 = b17;
                    hlVar.p = a2.getLong(i31);
                    hlVar.j = yiVar;
                    arrayList.add(hlVar);
                    b17 = i31;
                    b19 = i18;
                    b20 = i26;
                    b5 = i13;
                    b21 = i15;
                    b18 = i14;
                    i5 = i30;
                    b3 = i12;
                }
                a2.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b2;
            a2.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<hl> a() {
        uf ufVar;
        uf b2 = uf.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            int b3 = ag.b(a2, "id");
            int b4 = ag.b(a2, "state");
            int b5 = ag.b(a2, "worker_class_name");
            int b6 = ag.b(a2, "input_merger_class_name");
            int b7 = ag.b(a2, "input");
            int b8 = ag.b(a2, "output");
            int b9 = ag.b(a2, "initial_delay");
            int b10 = ag.b(a2, "interval_duration");
            int b11 = ag.b(a2, "flex_duration");
            int b12 = ag.b(a2, "run_attempt_count");
            int b13 = ag.b(a2, "backoff_policy");
            int b14 = ag.b(a2, "backoff_delay_duration");
            int b15 = ag.b(a2, "period_start_time");
            int b16 = ag.b(a2, "minimum_retention_duration");
            ufVar = b2;
            try {
                int b17 = ag.b(a2, "schedule_requested_at");
                int b18 = ag.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ag.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ag.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ag.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ag.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ag.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ag.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ag.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    yi yiVar = new yi();
                    int i13 = b18;
                    yiVar.a(nl.b(a2.getInt(b18)));
                    yiVar.b(a2.getInt(b19) != 0);
                    yiVar.c(a2.getInt(b20) != 0);
                    yiVar.a(a2.getInt(b21) != 0);
                    yiVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    yiVar.a(a2.getLong(b23));
                    yiVar.b(a2.getLong(b24));
                    yiVar.a(nl.a(a2.getBlob(b25)));
                    hl hlVar = new hl(string, string2);
                    hlVar.b = nl.c(a2.getInt(b4));
                    hlVar.d = a2.getString(b6);
                    hlVar.e = aj.b(a2.getBlob(b7));
                    int i16 = i10;
                    hlVar.f = aj.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    hlVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    hlVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    hlVar.i = a2.getLong(i20);
                    int i21 = i6;
                    hlVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    hlVar.l = nl.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    hlVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    hlVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    hlVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    hlVar.p = a2.getLong(i29);
                    hlVar.j = yiVar;
                    arrayList.add(hlVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                ufVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                ufVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            ufVar = b2;
            a2.close();
            ufVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int a(WorkInfo.State state, String... strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = dg.a();
        a2.append("UPDATE workspec SET state=");
        a2.append("?");
        a2.append(" WHERE id IN (");
        dg.a(a2, strArr.length);
        a2.append(")");
        kg compileStatement = this.a.compileStatement(a2.toString());
        compileStatement.b(1, (long) nl.a(state));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.a(i2);
            } else {
                compileStatement.a(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            int n = compileStatement.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
        }
    }
}
