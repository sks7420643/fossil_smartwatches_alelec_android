package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mf1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<mf1> CREATOR; // = new tf1();
    @DexIgnore
    public static /* final */ mf1 f; // = new mf1(0);
    @DexIgnore
    public /* final */ int e;

    /*
    static {
        Class<mf1> cls = mf1.class;
        new mf1(1);
    }
    */

    @DexIgnore
    public mf1(int i) {
        this.e = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof mf1) && this.e == ((mf1) obj).e;
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(Integer.valueOf(this.e));
    }

    @DexIgnore
    public final String toString() {
        String str;
        int i = this.e;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", new Object[]{Integer.valueOf(i)});
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", new Object[]{str});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e);
        kk0.a(parcel, a);
    }
}
