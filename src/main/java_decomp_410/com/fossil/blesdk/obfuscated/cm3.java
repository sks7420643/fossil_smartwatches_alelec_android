package com.fossil.blesdk.obfuscated;

import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cm3 extends v52<bm3> {
    @DexIgnore
    void a(Pair<Integer, Integer> pair, Pair<Integer, Integer> pair2, Pair<Integer, Integer> pair3);

    @DexIgnore
    void b(int i, int i2);

    @DexIgnore
    void c(Date date);
}
