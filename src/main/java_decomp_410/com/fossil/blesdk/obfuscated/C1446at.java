package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.at */
public class C1446at extends com.fossil.blesdk.obfuscated.C2764qs {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ byte[] f3587b; // = "com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a);

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo8933a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2) {
        return com.fossil.blesdk.obfuscated.C1904gt.m7587c(jqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        return obj instanceof com.fossil.blesdk.obfuscated.C1446at;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.FitCenter".hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        messageDigest.update(f3587b);
    }
}
