package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s52 implements Factory<kn2> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public s52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static s52 a(n42 n42) {
        return new s52(n42);
    }

    @DexIgnore
    public static kn2 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static kn2 c(n42 n42) {
        kn2 q = n42.q();
        n44.a(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    public kn2 get() {
        return b(this.a);
    }
}
