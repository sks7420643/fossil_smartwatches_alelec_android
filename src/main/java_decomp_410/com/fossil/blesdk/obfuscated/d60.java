package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d60 extends GetFilePhase {
    @DexIgnore
    public static /* final */ a T; // = new a((fd4) null);
    @DexIgnore
    public Priority R;
    @DexIgnore
    public /* final */ boolean S;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final HashMap<GetFilePhase.GetFileOption, Object> a(HashMap<GetFilePhase.GetFileOption, Object> hashMap) {
            hashMap.put(GetFilePhase.GetFileOption.SKIP_ERASE_CACHE_AFTER_SUCCESS, true);
            return hashMap;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ d60(Peripheral peripheral, Phase.a aVar, HashMap hashMap, int i, fd4 fd4) {
        this(peripheral, aVar, (i & 4) != 0 ? new HashMap() : hashMap);
    }

    @DexIgnore
    public boolean c() {
        return this.S;
    }

    @DexIgnore
    public Priority m() {
        return this.R;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public d60(Peripheral peripheral, Phase.a aVar, HashMap<GetFilePhase.GetFileOption, Object> hashMap) {
        super(peripheral, aVar, r4, r5, hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 96, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(hashMap, "options");
        PhaseId phaseId = PhaseId.SYNC_IN_BACKGROUND;
        short a2 = z40.b.a(peripheral.k(), FileType.ACTIVITY_FILE);
        HashMap unused = T.a(hashMap);
        this.R = Priority.LOW;
        this.S = true;
    }
}
