package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.zl4;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Protocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gl4 {
    @DexIgnore
    public /* final */ zl4 a;
    @DexIgnore
    public /* final */ ul4 b;
    @DexIgnore
    public /* final */ SocketFactory c;
    @DexIgnore
    public /* final */ hl4 d;
    @DexIgnore
    public /* final */ List<Protocol> e;
    @DexIgnore
    public /* final */ List<pl4> f;
    @DexIgnore
    public /* final */ ProxySelector g;
    @DexIgnore
    public /* final */ Proxy h;
    @DexIgnore
    public /* final */ SSLSocketFactory i;
    @DexIgnore
    public /* final */ HostnameVerifier j;
    @DexIgnore
    public /* final */ ll4 k;

    @DexIgnore
    public gl4(String str, int i2, ul4 ul4, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, ll4 ll4, hl4 hl4, Proxy proxy, List<Protocol> list, List<pl4> list2, ProxySelector proxySelector) {
        zl4.a aVar = new zl4.a();
        aVar.f(sSLSocketFactory != null ? Utility.URL_SCHEME : "http");
        aVar.b(str);
        aVar.a(i2);
        this.a = aVar.a();
        if (ul4 != null) {
            this.b = ul4;
            if (socketFactory != null) {
                this.c = socketFactory;
                if (hl4 != null) {
                    this.d = hl4;
                    if (list != null) {
                        this.e = jm4.a(list);
                        if (list2 != null) {
                            this.f = jm4.a(list2);
                            if (proxySelector != null) {
                                this.g = proxySelector;
                                this.h = proxy;
                                this.i = sSLSocketFactory;
                                this.j = hostnameVerifier;
                                this.k = ll4;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    @DexIgnore
    public ll4 a() {
        return this.k;
    }

    @DexIgnore
    public List<pl4> b() {
        return this.f;
    }

    @DexIgnore
    public ul4 c() {
        return this.b;
    }

    @DexIgnore
    public HostnameVerifier d() {
        return this.j;
    }

    @DexIgnore
    public List<Protocol> e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof gl4) {
            gl4 gl4 = (gl4) obj;
            return this.a.equals(gl4.a) && a(gl4);
        }
    }

    @DexIgnore
    public Proxy f() {
        return this.h;
    }

    @DexIgnore
    public hl4 g() {
        return this.d;
    }

    @DexIgnore
    public ProxySelector h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31;
        Proxy proxy = this.h;
        int i2 = 0;
        int hashCode2 = (hashCode + (proxy != null ? proxy.hashCode() : 0)) * 31;
        SSLSocketFactory sSLSocketFactory = this.i;
        int hashCode3 = (hashCode2 + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.j;
        int hashCode4 = (hashCode3 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        ll4 ll4 = this.k;
        if (ll4 != null) {
            i2 = ll4.hashCode();
        }
        return hashCode4 + i2;
    }

    @DexIgnore
    public SocketFactory i() {
        return this.c;
    }

    @DexIgnore
    public SSLSocketFactory j() {
        return this.i;
    }

    @DexIgnore
    public zl4 k() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.a.g());
        sb.append(":");
        sb.append(this.a.k());
        if (this.h != null) {
            sb.append(", proxy=");
            sb.append(this.h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.g);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public boolean a(gl4 gl4) {
        return this.b.equals(gl4.b) && this.d.equals(gl4.d) && this.e.equals(gl4.e) && this.f.equals(gl4.f) && this.g.equals(gl4.g) && jm4.a((Object) this.h, (Object) gl4.h) && jm4.a((Object) this.i, (Object) gl4.i) && jm4.a((Object) this.j, (Object) gl4.j) && jm4.a((Object) this.k, (Object) gl4.k) && k().k() == gl4.k().k();
    }
}
