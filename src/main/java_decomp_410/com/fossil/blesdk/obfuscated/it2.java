package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class it2 extends le.d<GoalTrackingSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        kd4.b(goalTrackingSummary, "oldItem");
        kd4.b(goalTrackingSummary2, "newItem");
        return kd4.a((Object) goalTrackingSummary, (Object) goalTrackingSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        kd4.b(goalTrackingSummary, "oldItem");
        kd4.b(goalTrackingSummary2, "newItem");
        return rk2.d(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
