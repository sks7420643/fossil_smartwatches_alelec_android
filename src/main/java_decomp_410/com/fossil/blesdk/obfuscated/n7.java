package com.fossil.blesdk.obfuscated;

import android.content.res.Configuration;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n7 {
    @DexIgnore
    public static p7 a(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 24) {
            return p7.a((Object) configuration.getLocales());
        }
        return p7.b(configuration.locale);
    }
}
