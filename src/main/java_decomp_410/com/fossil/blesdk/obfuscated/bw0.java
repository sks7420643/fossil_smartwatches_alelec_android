package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bw0 {
    @DexIgnore
    public static /* final */ zv0 a; // = c();
    @DexIgnore
    public static /* final */ zv0 b; // = new aw0();

    @DexIgnore
    public static zv0 a() {
        return a;
    }

    @DexIgnore
    public static zv0 b() {
        return b;
    }

    @DexIgnore
    public static zv0 c() {
        try {
            return (zv0) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
