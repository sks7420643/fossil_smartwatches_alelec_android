package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ph1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xh1 e;
    @DexIgnore
    public /* final */ /* synthetic */ tg1 f;

    @DexIgnore
    public ph1(oh1 oh1, xh1 xh1, tg1 tg1) {
        this.e = xh1;
        this.f = tg1;
    }

    @DexIgnore
    public final void run() {
        if (this.e.w() == null) {
            this.f.s().a("Install Referrer Reporter is null");
            return;
        }
        lh1 w = this.e.w();
        w.a.i();
        w.a(w.a.getContext().getPackageName());
    }
}
