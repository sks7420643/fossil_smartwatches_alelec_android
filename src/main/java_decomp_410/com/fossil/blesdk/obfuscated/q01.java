package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q01 extends nz0 implements o01 {
    @DexIgnore
    public q01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    public final void a(hq0 hq0) throws RemoteException {
        Parcel o = o();
        y01.a(o, (Parcelable) hq0);
        b(1, o);
    }
}
