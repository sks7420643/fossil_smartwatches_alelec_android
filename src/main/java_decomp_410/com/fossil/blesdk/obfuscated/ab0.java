package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ab0 {
    @DexIgnore
    public static /* final */ ab0 a; // = new ab0();

    @DexIgnore
    public final SharedPreferences a(String str) {
        kd4.b(str, "name");
        Context a2 = va0.f.a();
        if (a2 != null) {
            return a2.getSharedPreferences(str, 0);
        }
        return null;
    }
}
