package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fk2 extends xn {
    @DexIgnore
    public fk2(rn rnVar, qu quVar, vu vuVar, Context context) {
        super(rnVar, quVar, vuVar, context);
    }

    @DexIgnore
    public ek2<Bitmap> e() {
        return (ek2) super.e();
    }

    @DexIgnore
    public ek2<Drawable> f() {
        return (ek2) super.f();
    }

    @DexIgnore
    public <ResourceType> ek2<ResourceType> a(Class<ResourceType> cls) {
        return new ek2<>(this.e, this, cls, this.f);
    }

    @DexIgnore
    public ek2<Drawable> a(String str) {
        return (ek2) super.a(str);
    }

    @DexIgnore
    public ek2<Drawable> a(Uri uri) {
        return (ek2) super.a(uri);
    }

    @DexIgnore
    public ek2<Drawable> a(Integer num) {
        return (ek2) super.a(num);
    }

    @DexIgnore
    public ek2<Drawable> a(byte[] bArr) {
        return (ek2) super.a(bArr);
    }

    @DexIgnore
    public ek2<Drawable> a(Object obj) {
        return (ek2) super.a(obj);
    }

    @DexIgnore
    public void a(rv rvVar) {
        if (rvVar instanceof dk2) {
            super.a(rvVar);
        } else {
            super.a((rv) new dk2().a((lv<?>) rvVar));
        }
    }
}
