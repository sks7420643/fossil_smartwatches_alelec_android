package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sf1 implements Parcelable.Creator<lf1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                f = SafeParcelReader.n(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                f2 = SafeParcelReader.n(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new lf1(f, f2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new lf1[i];
    }
}
