package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q5 */
public class C2705q5 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.ThreadLocal<android.graphics.Matrix> f8547a; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.lang.ThreadLocal<android.graphics.RectF> f8548b; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: a */
    public static void m12614a(android.view.ViewGroup viewGroup, android.view.View view, android.graphics.Rect rect) {
        rect.set(0, 0, view.getWidth(), view.getHeight());
        m12616b(viewGroup, view, rect);
    }

    @DexIgnore
    /* renamed from: b */
    public static void m12616b(android.view.ViewGroup viewGroup, android.view.View view, android.graphics.Rect rect) {
        android.graphics.Matrix matrix = f8547a.get();
        if (matrix == null) {
            matrix = new android.graphics.Matrix();
            f8547a.set(matrix);
        } else {
            matrix.reset();
        }
        m12615a((android.view.ViewParent) viewGroup, view, matrix);
        android.graphics.RectF rectF = f8548b.get();
        if (rectF == null) {
            rectF = new android.graphics.RectF();
            f8548b.set(rectF);
        }
        rectF.set(rect);
        matrix.mapRect(rectF);
        rect.set((int) (rectF.left + 0.5f), (int) (rectF.top + 0.5f), (int) (rectF.right + 0.5f), (int) (rectF.bottom + 0.5f));
    }

    @DexIgnore
    /* renamed from: a */
    public static void m12615a(android.view.ViewParent viewParent, android.view.View view, android.graphics.Matrix matrix) {
        android.view.ViewParent parent = view.getParent();
        if ((parent instanceof android.view.View) && parent != viewParent) {
            android.view.View view2 = (android.view.View) parent;
            m12615a(viewParent, view2, matrix);
            matrix.preTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        }
        matrix.preTranslate((float) view.getLeft(), (float) view.getTop());
        if (!view.getMatrix().isIdentity()) {
            matrix.preConcat(view.getMatrix());
        }
    }
}
