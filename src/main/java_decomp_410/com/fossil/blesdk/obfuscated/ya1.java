package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ya1 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> g;
    @DexIgnore
    public /* final */ /* synthetic */ qa1 h;

    @DexIgnore
    public ya1(qa1 qa1) {
        this.h = qa1;
        this.e = -1;
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.g == null) {
            this.g = this.h.g.entrySet().iterator();
        }
        return this.g;
    }

    @DexIgnore
    public final boolean hasNext() {
        if (this.e + 1 < this.h.f.size() || (!this.h.g.isEmpty() && a().hasNext())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        this.f = true;
        int i = this.e + 1;
        this.e = i;
        if (i < this.h.f.size()) {
            return (Map.Entry) this.h.f.get(this.e);
        }
        return (Map.Entry) a().next();
    }

    @DexIgnore
    public final void remove() {
        if (this.f) {
            this.f = false;
            this.h.f();
            if (this.e < this.h.f.size()) {
                qa1 qa1 = this.h;
                int i = this.e;
                this.e = i - 1;
                Object unused = qa1.b(i);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    @DexIgnore
    public /* synthetic */ ya1(qa1 qa1, ra1 ra1) {
        this(qa1);
    }
}
