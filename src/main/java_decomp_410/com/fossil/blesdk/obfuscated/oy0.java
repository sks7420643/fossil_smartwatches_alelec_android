package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface oy0 extends IInterface {
    @DexIgnore
    void a(Status status, long j) throws RemoteException;

    @DexIgnore
    void a(Status status, pd0 pd0) throws RemoteException;

    @DexIgnore
    void a(Status status, rd0[] rd0Arr) throws RemoteException;

    @DexIgnore
    void a(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void b(Status status, long j) throws RemoteException;

    @DexIgnore
    void b(Status status, pd0 pd0) throws RemoteException;

    @DexIgnore
    void f(Status status) throws RemoteException;

    @DexIgnore
    void g(Status status) throws RemoteException;

    @DexIgnore
    void h(Status status) throws RemoteException;
}
