package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s4 */
public interface C2861s4<T> {
    @DexIgnore
    /* renamed from: a */
    T mo15893a();

    @DexIgnore
    /* renamed from: a */
    void mo15894a(T[] tArr, int i);

    @DexIgnore
    /* renamed from: a */
    boolean mo15895a(T t);
}
