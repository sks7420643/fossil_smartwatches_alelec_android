package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kg1 extends IInterface {
    @DexIgnore
    List<kl1> a(rl1 rl1, boolean z) throws RemoteException;

    @DexIgnore
    List<vl1> a(String str, String str2, rl1 rl1) throws RemoteException;

    @DexIgnore
    List<vl1> a(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<kl1> a(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    List<kl1> a(String str, String str2, boolean z, rl1 rl1) throws RemoteException;

    @DexIgnore
    void a(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void a(hg1 hg1, rl1 rl1) throws RemoteException;

    @DexIgnore
    void a(hg1 hg1, String str, String str2) throws RemoteException;

    @DexIgnore
    void a(kl1 kl1, rl1 rl1) throws RemoteException;

    @DexIgnore
    void a(rl1 rl1) throws RemoteException;

    @DexIgnore
    void a(vl1 vl1) throws RemoteException;

    @DexIgnore
    void a(vl1 vl1, rl1 rl1) throws RemoteException;

    @DexIgnore
    byte[] a(hg1 hg1, String str) throws RemoteException;

    @DexIgnore
    void b(rl1 rl1) throws RemoteException;

    @DexIgnore
    void c(rl1 rl1) throws RemoteException;

    @DexIgnore
    String d(rl1 rl1) throws RemoteException;
}
