package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gg1 implements Parcelable.Creator<eg1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                bundle = SafeParcelReader.a(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new eg1(bundle);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new eg1[i];
    }
}
