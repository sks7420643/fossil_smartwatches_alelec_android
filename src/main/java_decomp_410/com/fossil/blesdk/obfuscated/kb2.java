package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kb2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ NumberPicker t;
    @DexIgnore
    public /* final */ NumberPicker u;
    @DexIgnore
    public /* final */ NumberPicker v;

    @DexIgnore
    public kb2(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, LinearLayout linearLayout, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = flexibleTextView;
        this.t = numberPicker;
        this.u = numberPicker2;
        this.v = numberPicker3;
    }
}
