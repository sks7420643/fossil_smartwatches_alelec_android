package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ri4 implements oh4, ig4 {
    @DexIgnore
    public static /* final */ ri4 e; // = new ri4();

    @DexIgnore
    public boolean a(Throwable th) {
        kd4.b(th, "cause");
        return false;
    }

    @DexIgnore
    public void dispose() {
    }

    @DexIgnore
    public String toString() {
        return "NonDisposableHandle";
    }
}
