package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzxx;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ca1<T> implements na1<T> {
    @DexIgnore
    public /* final */ w91 a;
    @DexIgnore
    public /* final */ eb1<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ j81<?> d;

    @DexIgnore
    public ca1(eb1<?, ?> eb1, j81<?> j81, w91 w91) {
        this.b = eb1;
        this.c = j81.a(w91);
        this.d = j81;
        this.a = w91;
    }

    @DexIgnore
    public static <T> ca1<T> a(eb1<?, ?> eb1, j81<?> j81, w91 w91) {
        return new ca1<>(eb1, j81, w91);
    }

    @DexIgnore
    public final void b(T t, T t2) {
        pa1.a(this.b, t, t2);
        if (this.c) {
            pa1.a(this.d, t, t2);
        }
    }

    @DexIgnore
    public final boolean c(T t) {
        return this.d.a((Object) t).d();
    }

    @DexIgnore
    public final void d(T t) {
        this.b.f(t);
        this.d.c(t);
    }

    @DexIgnore
    public final T a() {
        return this.a.f().u();
    }

    @DexIgnore
    public final boolean a(T t, T t2) {
        if (!this.b.c(t).equals(this.b.c(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    public final int b(T t) {
        eb1<?, ?> eb1 = this.b;
        int e = eb1.e(eb1.c(t)) + 0;
        return this.c ? e + this.d.a((Object) t).h() : e;
    }

    @DexIgnore
    public final int a(T t) {
        int hashCode = this.b.c(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    public final void a(T t, sb1 sb1) throws IOException {
        Iterator<Map.Entry<?, Object>> e = this.d.a((Object) t).e();
        while (e.hasNext()) {
            Map.Entry next = e.next();
            o81 o81 = (o81) next.getKey();
            if (o81.h() != zzxx.MESSAGE || o81.g() || o81.e()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (next instanceof d91) {
                sb1.zza(o81.zzc(), (Object) ((d91) next).a().a());
            } else {
                sb1.zza(o81.zzc(), next.getValue());
            }
        }
        eb1<?, ?> eb1 = this.b;
        eb1.b(eb1.c(t), sb1);
    }

    @DexIgnore
    public final void a(T t, ma1 ma1, i81 i81) throws IOException {
        boolean z;
        eb1<?, ?> eb1 = this.b;
        j81<?> j81 = this.d;
        Object d2 = eb1.d(t);
        m81<?> b2 = j81.b(t);
        do {
            try {
                if (ma1.j() == Integer.MAX_VALUE) {
                    eb1.b((Object) t, d2);
                    return;
                }
                int a2 = ma1.a();
                if (a2 == 11) {
                    Object obj = null;
                    zzte zzte = null;
                    int i = 0;
                    while (ma1.j() != Integer.MAX_VALUE) {
                        int a3 = ma1.a();
                        if (a3 == 16) {
                            i = ma1.b();
                            obj = j81.a(i81, this.a, i);
                        } else if (a3 == 26) {
                            if (obj == null) {
                                zzte = ma1.d();
                            } else {
                                j81.a(ma1, obj, i81, b2);
                                throw null;
                            }
                        } else if (!ma1.n()) {
                            break;
                        }
                    }
                    if (ma1.a() == 12) {
                        if (zzte != null) {
                            if (obj == null) {
                                eb1.a(d2, i, zzte);
                            } else {
                                j81.a(zzte, obj, i81, b2);
                                throw null;
                            }
                        }
                        z = true;
                        continue;
                    } else {
                        throw zzuv.zzwt();
                    }
                } else if ((a2 & 7) == 2) {
                    Object a4 = j81.a(i81, this.a, a2 >>> 3);
                    if (a4 == null) {
                        z = eb1.a(d2, ma1);
                        continue;
                    } else {
                        j81.a(ma1, a4, i81, b2);
                        throw null;
                    }
                } else {
                    z = ma1.n();
                    continue;
                }
            } finally {
                eb1.b((Object) t, d2);
            }
        } while (z);
    }
}
