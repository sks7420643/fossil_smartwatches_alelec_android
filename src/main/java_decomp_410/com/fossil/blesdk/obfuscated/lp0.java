package com.fossil.blesdk.obfuscated;

import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lp0 implements e21<DataPoint, DataType> {
    @DexIgnore
    public static /* final */ lp0 a; // = new lp0();

    @DexIgnore
    public final /* synthetic */ double a(Object obj, int i) {
        return (double) ((DataPoint) obj).zzb(i).H();
    }

    @DexIgnore
    public final /* synthetic */ int b(Object obj, int i) {
        return ((DataPoint) obj).zzb(i).I();
    }

    @DexIgnore
    public final /* synthetic */ boolean c(Object obj, int i) {
        return ((DataPoint) obj).zzb(i).K();
    }

    @DexIgnore
    public final /* synthetic */ Object zza(Object obj) {
        return ((DataPoint) obj).I();
    }

    @DexIgnore
    public final f21<DataType> zzb() {
        return mp0.a;
    }

    @DexIgnore
    public final /* synthetic */ String zzb(Object obj) {
        return ((DataPoint) obj).I().I();
    }

    @DexIgnore
    public final /* synthetic */ long a(Object obj, TimeUnit timeUnit) {
        DataPoint dataPoint = (DataPoint) obj;
        return dataPoint.a(timeUnit) - dataPoint.b(timeUnit);
    }
}
