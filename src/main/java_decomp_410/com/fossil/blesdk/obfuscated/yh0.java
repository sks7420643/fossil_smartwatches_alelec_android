package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yh0<O extends de0.d> {
    @DexIgnore
    public /* final */ boolean a; // = true;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ de0<O> c;
    @DexIgnore
    public /* final */ O d;

    @DexIgnore
    public yh0(de0<O> de0, O o) {
        this.c = de0;
        this.d = o;
        this.b = zj0.a(this.c, this.d);
    }

    @DexIgnore
    public static <O extends de0.d> yh0<O> a(de0<O> de0, O o) {
        return new yh0<>(de0, o);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yh0)) {
            return false;
        }
        yh0 yh0 = (yh0) obj;
        return !this.a && !yh0.a && zj0.a(this.c, yh0.c) && zj0.a(this.d, yh0.d);
    }

    @DexIgnore
    public final int hashCode() {
        return this.b;
    }

    @DexIgnore
    public static <O extends de0.d> yh0<O> a(de0<O> de0) {
        return new yh0<>(de0);
    }

    @DexIgnore
    public final String a() {
        return this.c.b();
    }

    @DexIgnore
    public yh0(de0<O> de0) {
        this.c = de0;
        this.d = null;
        this.b = System.identityHashCode(this);
    }
}
