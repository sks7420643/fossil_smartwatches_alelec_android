package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.uj4;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tj4<E> {
    @DexIgnore
    public static /* final */ /* synthetic */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(tj4.class, Object.class, "_cur$internal");
    @DexIgnore
    public volatile /* synthetic */ Object _cur$internal;

    @DexIgnore
    public tj4(boolean z) {
        this._cur$internal = new uj4(8, z);
    }

    @DexIgnore
    public final void a() {
        while (true) {
            uj4 uj4 = (uj4) this._cur$internal;
            if (!uj4.a()) {
                a.compareAndSet(this, uj4, uj4.e());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int b() {
        return ((uj4) this._cur$internal).b();
    }

    @DexIgnore
    public final E c() {
        E e;
        E e2;
        while (true) {
            uj4 uj4 = (uj4) this._cur$internal;
            while (true) {
                long j = uj4._state$internal;
                e = null;
                if ((1152921504606846976L & j) == 0) {
                    uj4.a aVar = uj4.h;
                    int i = (int) ((1073741823 & j) >> 0);
                    if ((uj4.a & ((int) ((1152921503533105152L & j) >> 30))) != (uj4.a & i)) {
                        e2 = uj4.b.get(uj4.a & i);
                        if (e2 != null) {
                            if (!(e2 instanceof uj4.b)) {
                                int i2 = (i + 1) & 1073741823;
                                if (!uj4.f.compareAndSet(uj4, j, uj4.h.a(j, i2))) {
                                    if (uj4.d) {
                                        uj4 uj42 = uj4;
                                        do {
                                            uj42 = uj42.a(i, i2);
                                        } while (uj42 != null);
                                        break;
                                    }
                                } else {
                                    uj4.b.set(uj4.a & i, (Object) null);
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (uj4.d) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    e = uj4.g;
                    break;
                }
            }
            e = e2;
            if (e != uj4.g) {
                return e;
            }
            a.compareAndSet(this, uj4, uj4.e());
        }
    }

    @DexIgnore
    public final boolean a(E e) {
        kd4.b(e, "element");
        while (true) {
            uj4 uj4 = (uj4) this._cur$internal;
            int a2 = uj4.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, uj4, uj4.e());
            } else if (a2 == 2) {
                return false;
            }
        }
    }
}
