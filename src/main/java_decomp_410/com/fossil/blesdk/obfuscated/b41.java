package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b41 implements ze0.b<qc1> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationAvailability a;

    @DexIgnore
    public b41(z31 z31, LocationAvailability locationAvailability) {
        this.a = locationAvailability;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((qc1) obj).onLocationAvailability(this.a);
    }
}
