package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sh1 extends cl1 implements zl1 {
    @DexIgnore
    public static int j; // = 65535;
    @DexIgnore
    public static int k; // = 2;
    @DexIgnore
    public /* final */ Map<String, Map<String, String>> d; // = new g4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> e; // = new g4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> f; // = new g4();
    @DexIgnore
    public /* final */ Map<String, y51> g; // = new g4();
    @DexIgnore
    public /* final */ Map<String, Map<String, Integer>> h; // = new g4();
    @DexIgnore
    public /* final */ Map<String, String> i; // = new g4();

    @DexIgnore
    public sh1(dl1 dl1) {
        super(dl1);
    }

    @DexIgnore
    public final void a(String str) {
        q();
        e();
        bk0.b(str);
        if (this.g.get(str) == null) {
            byte[] d2 = o().d(str);
            if (d2 == null) {
                this.d.put(str, (Object) null);
                this.e.put(str, (Object) null);
                this.f.put(str, (Object) null);
                this.g.put(str, (Object) null);
                this.i.put(str, (Object) null);
                this.h.put(str, (Object) null);
                return;
            }
            y51 a = a(str, d2);
            this.d.put(str, a(a));
            a(str, a);
            this.g.put(str, a);
            this.i.put(str, (Object) null);
        }
    }

    @DexIgnore
    public final y51 b(String str) {
        q();
        e();
        bk0.b(str);
        a(str);
        return this.g.get(str);
    }

    @DexIgnore
    public final String c(String str) {
        e();
        return this.i.get(str);
    }

    @DexIgnore
    public final void d(String str) {
        e();
        this.i.put(str, (Object) null);
    }

    @DexIgnore
    public final void e(String str) {
        e();
        this.g.remove(str);
    }

    @DexIgnore
    public final long f(String str) {
        String a = a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a)) {
            return 0;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e2) {
            d().v().a("Unable to parse timezone offset. appId", tg1.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final boolean g(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(a(str, "measurement.upload.blacklist_internal"));
    }

    @DexIgnore
    public final boolean h(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(a(str, "measurement.upload.blacklist_public"));
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final boolean c(String str, String str2) {
        e();
        a(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final int d(String str, String str2) {
        e();
        a(str);
        Map map = this.h.get(str);
        if (map == null) {
            return 1;
        }
        Integer num = (Integer) map.get(str2);
        if (num == null) {
            return 1;
        }
        return num.intValue();
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        e();
        a(str);
        if (g(str) && nl1.h(str2)) {
            return true;
        }
        if (h(str) && nl1.e(str2)) {
            return true;
        }
        Map map = this.e.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final String a(String str, String str2) {
        e();
        a(str);
        Map map = this.d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public static Map<String, String> a(y51 y51) {
        g4 g4Var = new g4();
        if (y51 != null) {
            z51[] z51Arr = y51.f;
            if (z51Arr != null) {
                for (z51 z51 : z51Arr) {
                    if (z51 != null) {
                        g4Var.put(z51.c, z51.d);
                    }
                }
            }
        }
        return g4Var;
    }

    @DexIgnore
    public final void a(String str, y51 y51) {
        g4 g4Var = new g4();
        g4 g4Var2 = new g4();
        g4 g4Var3 = new g4();
        if (y51 != null) {
            x51[] x51Arr = y51.g;
            if (x51Arr != null) {
                for (x51 x51 : x51Arr) {
                    if (TextUtils.isEmpty(x51.c)) {
                        d().v().a("EventConfig contained null event name");
                    } else {
                        String a = wi1.a(x51.c);
                        if (!TextUtils.isEmpty(a)) {
                            x51.c = a;
                        }
                        g4Var.put(x51.c, x51.d);
                        g4Var2.put(x51.c, x51.e);
                        Integer num = x51.f;
                        if (num != null) {
                            if (num.intValue() < k || x51.f.intValue() > j) {
                                d().v().a("Invalid sampling rate. Event name, sample rate", x51.c, x51.f);
                            } else {
                                g4Var3.put(x51.c, x51.f);
                            }
                        }
                    }
                }
            }
        }
        this.e.put(str, g4Var);
        this.f.put(str, g4Var2);
        this.h.put(str, g4Var3);
    }

    @DexIgnore
    public final boolean a(String str, byte[] bArr, String str2) {
        byte[] bArr2;
        String str3 = str;
        q();
        e();
        bk0.b(str);
        y51 a = a(str, bArr);
        if (a == null) {
            return false;
        }
        a(str3, a);
        this.g.put(str3, a);
        this.i.put(str3, str2);
        this.d.put(str3, a(a));
        tl1 n = n();
        r51[] r51Arr = a.h;
        bk0.a(r51Arr);
        for (r51 r51 : r51Arr) {
            for (s51 s51 : r51.e) {
                String a2 = wi1.a(s51.d);
                if (a2 != null) {
                    s51.d = a2;
                }
                for (t51 t51 : s51.e) {
                    String a3 = xi1.a(t51.f);
                    if (a3 != null) {
                        t51.f = a3;
                    }
                }
            }
            for (v51 v51 : r51.d) {
                String a4 = yi1.a(v51.d);
                if (a4 != null) {
                    v51.d = a4;
                }
            }
        }
        n.o().a(str3, r51Arr);
        try {
            a.h = null;
            bArr2 = new byte[a.b()];
            a.a(ub1.a(bArr2, 0, bArr2.length));
        } catch (IOException e2) {
            d().v().a("Unable to serialize reduced-size config. Storing full config instead. appId", tg1.a(str), e2);
            bArr2 = bArr;
        }
        am1 o = o();
        bk0.b(str);
        o.e();
        o.q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr2);
        try {
            if (((long) o.v().update("apps", contentValues, "app_id = ?", new String[]{str3})) == 0) {
                o.d().s().a("Failed to update remote config (got 0). appId", tg1.a(str));
            }
        } catch (SQLiteException e3) {
            o.d().s().a("Error storing remote config. appId", tg1.a(str), e3);
        }
        return true;
    }

    @DexIgnore
    public final y51 a(String str, byte[] bArr) {
        if (bArr == null) {
            return new y51();
        }
        tb1 a = tb1.a(bArr, 0, bArr.length);
        y51 y51 = new y51();
        try {
            y51.a(a);
            d().A().a("Parsed config. version, gmp_app_id", y51.c, y51.d);
            return y51;
        } catch (IOException e2) {
            d().v().a("Unable to merge remote config. appId", tg1.a(str), e2);
            return new y51();
        }
    }
}
