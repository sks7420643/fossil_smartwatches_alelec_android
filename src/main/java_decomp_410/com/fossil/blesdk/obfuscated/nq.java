package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nq implements fq<int[]> {
    @DexIgnore
    public String a() {
        return "IntegerArrayPool";
    }

    @DexIgnore
    public int b() {
        return 4;
    }

    @DexIgnore
    public int a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public int[] newArray(int i) {
        return new int[i];
    }
}
