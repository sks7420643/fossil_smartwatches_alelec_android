package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sl */
public class C2896sl {

    @DexIgnore
    /* renamed from: a */
    public android.content.Context f9388a;

    @DexIgnore
    /* renamed from: b */
    public android.content.SharedPreferences f9389b;

    @DexIgnore
    public C2896sl(android.content.Context context) {
        this.f9388a = context;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16116a(boolean z) {
        mo16115a().edit().putBoolean("reschedule_needed", z).apply();
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo16117b() {
        return mo16115a().getBoolean("reschedule_needed", false);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.content.SharedPreferences mo16115a() {
        android.content.SharedPreferences sharedPreferences;
        synchronized (com.fossil.blesdk.obfuscated.C2896sl.class) {
            if (this.f9389b == null) {
                this.f9389b = this.f9388a.getSharedPreferences("androidx.work.util.preferences", 0);
            }
            sharedPreferences = this.f9389b;
        }
        return sharedPreferences;
    }
}
