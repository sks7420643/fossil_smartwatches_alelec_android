package com.fossil.blesdk.obfuscated;

import android.view.ViewGroup;
import androidx.transition.Transition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class nh {
    @DexIgnore
    public abstract long a(ViewGroup viewGroup, Transition transition, ph phVar, ph phVar2);

    @DexIgnore
    public abstract void a(ph phVar);

    @DexIgnore
    public abstract String[] a();
}
