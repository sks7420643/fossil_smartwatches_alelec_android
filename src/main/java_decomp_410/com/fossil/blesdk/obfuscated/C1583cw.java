package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.cw */
public abstract class C1583cw<T extends android.view.View, Z> extends com.fossil.blesdk.obfuscated.C3065uv<Z> {

    @DexIgnore
    /* renamed from: j */
    public static int f4200j; // = com.fossil.blesdk.obfuscated.C3126vn.glide_custom_view_target_tag;

    @DexIgnore
    /* renamed from: e */
    public /* final */ T f4201e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1583cw.C1584a f4202f;

    @DexIgnore
    /* renamed from: g */
    public android.view.View.OnAttachStateChangeListener f4203g;

    @DexIgnore
    /* renamed from: h */
    public boolean f4204h;

    @DexIgnore
    /* renamed from: i */
    public boolean f4205i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cw$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cw$a */
    public static final class C1584a {

        @DexIgnore
        /* renamed from: e */
        public static java.lang.Integer f4206e;

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.View f4207a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1449aw> f4208b; // = new java.util.ArrayList();

        @DexIgnore
        /* renamed from: c */
        public boolean f4209c;

        @DexIgnore
        /* renamed from: d */
        public com.fossil.blesdk.obfuscated.C1583cw.C1584a.C1585a f4210d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cw$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.cw$a$a */
        public static final class C1585a implements android.view.ViewTreeObserver.OnPreDrawListener {

            @DexIgnore
            /* renamed from: e */
            public /* final */ java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C1583cw.C1584a> f4211e;

            @DexIgnore
            public C1585a(com.fossil.blesdk.obfuscated.C1583cw.C1584a aVar) {
                this.f4211e = new java.lang.ref.WeakReference<>(aVar);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (android.util.Log.isLoggable("ViewTarget", 2)) {
                    android.util.Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                com.fossil.blesdk.obfuscated.C1583cw.C1584a aVar = (com.fossil.blesdk.obfuscated.C1583cw.C1584a) this.f4211e.get();
                if (aVar == null) {
                    return true;
                }
                aVar.mo9692a();
                return true;
            }
        }

        @DexIgnore
        public C1584a(android.view.View view) {
            this.f4207a = view;
        }

        @DexIgnore
        /* renamed from: a */
        public static int m5572a(android.content.Context context) {
            if (f4206e == null) {
                android.view.WindowManager windowManager = (android.view.WindowManager) context.getSystemService("window");
                com.fossil.blesdk.obfuscated.C2992tw.m14457a(windowManager);
                android.view.Display defaultDisplay = windowManager.getDefaultDisplay();
                android.graphics.Point point = new android.graphics.Point();
                defaultDisplay.getSize(point);
                f4206e = java.lang.Integer.valueOf(java.lang.Math.max(point.x, point.y));
            }
            return f4206e.intValue();
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo9694a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo9697b(int i, int i2) {
            java.util.Iterator it = new java.util.ArrayList(this.f4208b).iterator();
            while (it.hasNext()) {
                ((com.fossil.blesdk.obfuscated.C1449aw) it.next()).mo4027a(i, i2);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public final int mo9699c() {
            int paddingTop = this.f4207a.getPaddingTop() + this.f4207a.getPaddingBottom();
            android.view.ViewGroup.LayoutParams layoutParams = this.f4207a.getLayoutParams();
            return mo9691a(this.f4207a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        @DexIgnore
        /* renamed from: d */
        public final int mo9700d() {
            int paddingLeft = this.f4207a.getPaddingLeft() + this.f4207a.getPaddingRight();
            android.view.ViewGroup.LayoutParams layoutParams = this.f4207a.getLayoutParams();
            return mo9691a(this.f4207a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo9698b(com.fossil.blesdk.obfuscated.C1449aw awVar) {
            this.f4208b.remove(awVar);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo9696b() {
            android.view.ViewTreeObserver viewTreeObserver = this.f4207a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.f4210d);
            }
            this.f4210d = null;
            this.f4208b.clear();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9692a() {
            if (!this.f4208b.isEmpty()) {
                int d = mo9700d();
                int c = mo9699c();
                if (mo9695a(d, c)) {
                    mo9697b(d, c);
                    mo9696b();
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9693a(com.fossil.blesdk.obfuscated.C1449aw awVar) {
            int d = mo9700d();
            int c = mo9699c();
            if (mo9695a(d, c)) {
                awVar.mo4027a(d, c);
                return;
            }
            if (!this.f4208b.contains(awVar)) {
                this.f4208b.add(awVar);
            }
            if (this.f4210d == null) {
                android.view.ViewTreeObserver viewTreeObserver = this.f4207a.getViewTreeObserver();
                this.f4210d = new com.fossil.blesdk.obfuscated.C1583cw.C1584a.C1585a(this);
                viewTreeObserver.addOnPreDrawListener(this.f4210d);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo9695a(int i, int i2) {
            return mo9694a(i) && mo9694a(i2);
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo9691a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.f4209c && this.f4207a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.f4207a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (android.util.Log.isLoggable("ViewTarget", 4)) {
                android.util.Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return m5572a(this.f4207a.getContext());
        }
    }

    @DexIgnore
    public C1583cw(T t) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(t);
        this.f4201e = (android.view.View) t;
        this.f4202f = new com.fossil.blesdk.obfuscated.C1583cw.C1584a(t);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9313a(com.fossil.blesdk.obfuscated.C1449aw awVar) {
        this.f4202f.mo9698b(awVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9316b(android.graphics.drawable.Drawable drawable) {
        super.mo9316b(drawable);
        mo9688f();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9318c(android.graphics.drawable.Drawable drawable) {
        super.mo9318c(drawable);
        this.f4202f.mo9696b();
        if (!this.f4204h) {
            mo9689g();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2604ov mo9319d() {
        java.lang.Object e = mo9687e();
        if (e == null) {
            return null;
        }
        if (e instanceof com.fossil.blesdk.obfuscated.C2604ov) {
            return (com.fossil.blesdk.obfuscated.C2604ov) e;
        }
        throw new java.lang.IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    /* renamed from: e */
    public final java.lang.Object mo9687e() {
        return this.f4201e.getTag(f4200j);
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo9688f() {
        android.view.View.OnAttachStateChangeListener onAttachStateChangeListener = this.f4203g;
        if (onAttachStateChangeListener != null && !this.f4205i) {
            this.f4201e.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f4205i = true;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo9689g() {
        android.view.View.OnAttachStateChangeListener onAttachStateChangeListener = this.f4203g;
        if (onAttachStateChangeListener != null && this.f4205i) {
            this.f4201e.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f4205i = false;
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Target for: " + this.f4201e;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9314a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        mo9686a((java.lang.Object) ovVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9686a(java.lang.Object obj) {
        this.f4201e.setTag(f4200j, obj);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9317b(com.fossil.blesdk.obfuscated.C1449aw awVar) {
        this.f4202f.mo9693a(awVar);
    }
}
