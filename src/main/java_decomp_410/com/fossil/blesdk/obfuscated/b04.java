package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.yy3;
import com.fossil.blesdk.obfuscated.zy3;
import com.tencent.wxop.stat.MtaSDkException;
import com.tencent.wxop.stat.StatReportStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b04 implements wz3 {
    @DexIgnore
    public static b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public boolean d; // = false;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Handler f;
        @DexIgnore
        public Context g;
        @DexIgnore
        public Runnable h;
        @DexIgnore
        public Runnable i;

        @DexIgnore
        public b(Context context) {
            this.e = false;
            this.f = new Handler(Looper.getMainLooper());
            this.h = new c04(this);
            this.i = new d04(this);
            this.g = context;
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityPaused");
            this.f.removeCallbacks(this.i);
            this.f.postDelayed(this.h, 800);
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", activity.getComponentName().getClassName() + "  onActivityResumed");
            this.f.removeCallbacks(this.h);
            this.f.postDelayed(this.i, 800);
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public b04(Context context, String str, boolean z) {
        cz3.d("MicroMsg.SDK.WXApiImplV10", "<init>, appId = " + str + ", checkSignature = " + z);
        this.a = context;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public final void a(Context context, String str) {
        String str2 = "AWXOP" + str;
        h04.b(context, str2);
        h04.a(true);
        h04.a(StatReportStrategy.PERIOD);
        h04.c(60);
        h04.c(context, "Wechat_Sdk");
        try {
            i04.a(context, str2, "2.0.3");
        } catch (MtaSDkException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final boolean a() {
        if (!this.d) {
            try {
                PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo("com.tencent.mm", 64);
                if (packageInfo == null) {
                    return false;
                }
                return a04.a(this.a, packageInfo.signatures, this.c);
            } catch (PackageManager.NameNotFoundException unused) {
                return false;
            }
        } else {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
    }

    @DexIgnore
    public final boolean a(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/createChatroom"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_create_chatroom_group_id"), bundle.getString("_wxapi_create_chatroom_chatroom_name"), bundle.getString("_wxapi_create_chatroom_chatroom_nickname"), bundle.getString("_wxapi_create_chatroom_ext_msg")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean a(Intent intent, xz3 xz3) {
        try {
            if (!a04.a(intent, "com.tencent.mm.openapi.token")) {
                cz3.c("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, intent not from weixin msg");
                return false;
            } else if (!this.d) {
                String stringExtra = intent.getStringExtra("_mmessage_content");
                int intExtra = intent.getIntExtra("_mmessage_sdkVersion", 0);
                String stringExtra2 = intent.getStringExtra("_mmessage_appPackage");
                if (stringExtra2 != null) {
                    if (stringExtra2.length() != 0) {
                        if (!a(intent.getByteArrayExtra("_mmessage_checksum"), az3.a(stringExtra, intExtra, stringExtra2))) {
                            cz3.a("MicroMsg.SDK.WXApiImplV10", "checksum fail");
                            return false;
                        }
                        int intExtra2 = intent.getIntExtra("_wxapi_command_type", 0);
                        if (intExtra2 == 9) {
                            xz3.a((iz3) new jz3(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 12) {
                            xz3.a((iz3) new nz3(intent.getExtras()));
                            return true;
                        } else if (intExtra2 == 14) {
                            xz3.a((iz3) new lz3(intent.getExtras()));
                            return true;
                        } else if (intExtra2 != 15) {
                            switch (intExtra2) {
                                case 1:
                                    xz3.a((iz3) new rz3(intent.getExtras()));
                                    return true;
                                case 2:
                                    xz3.a((iz3) new sz3(intent.getExtras()));
                                    return true;
                                case 3:
                                    xz3.a((hz3) new oz3(intent.getExtras()));
                                    return true;
                                case 4:
                                    xz3.a((hz3) new tz3(intent.getExtras()));
                                    return true;
                                case 5:
                                    xz3.a((iz3) new vz3(intent.getExtras()));
                                    return true;
                                case 6:
                                    xz3.a((hz3) new pz3(intent.getExtras()));
                                    return true;
                                default:
                                    cz3.a("MicroMsg.SDK.WXApiImplV10", "unknown cmd = " + intExtra2);
                                    break;
                            }
                            return false;
                        } else {
                            xz3.a((iz3) new mz3(intent.getExtras()));
                            return true;
                        }
                    }
                }
                cz3.a("MicroMsg.SDK.WXApiImplV10", "invalid argument");
                return false;
            } else {
                throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
            }
        } catch (Exception e2) {
            cz3.a("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, ex = %s", e2.getMessage());
        }
    }

    @DexIgnore
    public final boolean a(hz3 hz3) {
        String str;
        if (!this.d) {
            if (!a04.a(this.a, "com.tencent.mm", this.c)) {
                str = "sendReq failed for wechat app signature check failed";
            } else if (!hz3.a()) {
                str = "sendReq checkArgs fail";
            } else {
                cz3.d("MicroMsg.SDK.WXApiImplV10", "sendReq, req type = " + hz3.b());
                Bundle bundle = new Bundle();
                hz3.b(bundle);
                if (hz3.b() == 5) {
                    return j(this.a, bundle);
                }
                if (hz3.b() == 7) {
                    return d(this.a, bundle);
                }
                if (hz3.b() == 8) {
                    return f(this.a, bundle);
                }
                if (hz3.b() == 10) {
                    return e(this.a, bundle);
                }
                if (hz3.b() == 9) {
                    return c(this.a, bundle);
                }
                if (hz3.b() == 11) {
                    return h(this.a, bundle);
                }
                if (hz3.b() == 12) {
                    return i(this.a, bundle);
                }
                if (hz3.b() == 13) {
                    return g(this.a, bundle);
                }
                if (hz3.b() == 14) {
                    return a(this.a, bundle);
                }
                if (hz3.b() == 15) {
                    return b(this.a, bundle);
                }
                yy3.a aVar = new yy3.a();
                aVar.e = bundle;
                aVar.c = "weixin://sendreq?appid=" + this.b;
                aVar.a = "com.tencent.mm";
                aVar.b = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
                return yy3.a(this.a, aVar);
            }
            cz3.a("MicroMsg.SDK.WXApiImplV10", str);
            return false;
        }
        throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
    }

    @DexIgnore
    public final boolean a(String str) {
        Application application;
        if (this.d) {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        } else if (!a04.a(this.a, "com.tencent.mm", this.c)) {
            cz3.a("MicroMsg.SDK.WXApiImplV10", "register app failed for wechat app signature check failed");
            return false;
        } else {
            if (e == null && Build.VERSION.SDK_INT >= 14) {
                Context context = this.a;
                if (context instanceof Activity) {
                    a(context, str);
                    e = new b(this.a);
                    application = ((Activity) this.a).getApplication();
                } else if (context instanceof Service) {
                    a(context, str);
                    e = new b(this.a);
                    application = ((Service) this.a).getApplication();
                } else {
                    cz3.b("MicroMsg.SDK.WXApiImplV10", "context is not instanceof Activity or Service, disable WXStat");
                }
                application.registerActivityLifecycleCallbacks(e);
            }
            cz3.d("MicroMsg.SDK.WXApiImplV10", "registerApp, appId = " + str);
            if (str != null) {
                this.b = str;
            }
            cz3.d("MicroMsg.SDK.WXApiImplV10", "register app " + this.a.getPackageName());
            zy3.a aVar = new zy3.a();
            aVar.a = "com.tencent.mm";
            aVar.b = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
            aVar.c = "weixin://registerapp?appid=" + this.b;
            return zy3.a(this.a, aVar);
        }
    }

    @DexIgnore
    public final boolean a(byte[] bArr, byte[] bArr2) {
        String str;
        if (bArr == null || bArr.length == 0 || bArr2 == null || bArr2.length == 0) {
            str = "checkSumConsistent fail, invalid arguments";
        } else if (bArr.length != bArr2.length) {
            str = "checkSumConsistent fail, length is different";
        } else {
            for (int i = 0; i < bArr.length; i++) {
                if (bArr[i] != bArr2[i]) {
                    return false;
                }
            }
            return true;
        }
        cz3.a("MicroMsg.SDK.WXApiImplV10", str);
        return false;
    }

    @DexIgnore
    public final boolean b(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/joinChatroom"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_basereq_transaction"), bundle.getString("_wxapi_join_chatroom_group_id"), bundle.getString("_wxapi_join_chatroom_chatroom_nickname"), bundle.getString("_wxapi_join_chatroom_ext_msg")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean c(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean d(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene"));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg"), sb.toString(), sb2.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean e(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizTempSession");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_show_type"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_session_from"), sb.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean f(Context context, Bundle bundle) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri parse = Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile");
        StringBuilder sb = new StringBuilder();
        sb.append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene"));
        Cursor query = contentResolver.query(parse, (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg"), sb.toString()}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean g(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openBusiLuckyMoney"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_open_busi_lucky_money_timeStamp"), bundle.getString("_wxapi_open_busi_lucky_money_nonceStr"), bundle.getString("_wxapi_open_busi_lucky_money_signType"), bundle.getString("_wxapi_open_busi_lucky_money_signature"), bundle.getString("_wxapi_open_busi_lucky_money_package")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean h(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openRankList"), (String[]) null, (String) null, new String[0], (String) null);
        if (query == null) {
            return true;
        }
        query.close();
        return true;
    }

    @DexIgnore
    public final boolean i(Context context, Bundle bundle) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/openWebview"), (String[]) null, (String) null, new String[]{this.b, bundle.getString("_wxapi_jump_to_webview_url"), bundle.getString("_wxapi_basereq_transaction")}, (String) null);
        if (query != null) {
            query.close();
        }
        return true;
    }

    @DexIgnore
    public final boolean j(Context context, Bundle bundle) {
        if (f == null) {
            f = new yz3(context).getString("_wxapp_pay_entry_classname_", (String) null);
            cz3.d("MicroMsg.SDK.WXApiImplV10", "pay, set wxappPayEntryClassname = " + f);
            if (f == null) {
                cz3.a("MicroMsg.SDK.WXApiImplV10", "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        yy3.a aVar = new yy3.a();
        aVar.e = bundle;
        aVar.a = "com.tencent.mm";
        aVar.b = f;
        return yy3.a(context, aVar);
    }
}
