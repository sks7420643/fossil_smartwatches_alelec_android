package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qj */
public class C2744qj {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f8690a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("Schedulers");

    @DexIgnore
    /* renamed from: a */
    public static void m12826a(com.fossil.blesdk.obfuscated.C3290xi xiVar, androidx.work.impl.WorkDatabase workDatabase, java.util.List<com.fossil.blesdk.obfuscated.C2656pj> list) {
        if (list != null && list.size() != 0) {
            com.fossil.blesdk.obfuscated.C2036il d = workDatabase.mo3780d();
            workDatabase.beginTransaction();
            try {
                java.util.List<com.fossil.blesdk.obfuscated.C1954hl> a = d.mo12018a(xiVar.mo17749e());
                if (a != null && a.size() > 0) {
                    long currentTimeMillis = java.lang.System.currentTimeMillis();
                    for (com.fossil.blesdk.obfuscated.C1954hl hlVar : a) {
                        d.mo12016a(hlVar.f5771a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (a != null && a.size() > 0) {
                    com.fossil.blesdk.obfuscated.C1954hl[] hlVarArr = (com.fossil.blesdk.obfuscated.C1954hl[]) a.toArray(new com.fossil.blesdk.obfuscated.C1954hl[0]);
                    for (com.fossil.blesdk.obfuscated.C2656pj a2 : list) {
                        a2.mo9223a(hlVarArr);
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2656pj m12825a(android.content.Context context, com.fossil.blesdk.obfuscated.C2968tj tjVar) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            com.fossil.blesdk.obfuscated.C1735ek ekVar = new com.fossil.blesdk.obfuscated.C1735ek(context, tjVar);
            com.fossil.blesdk.obfuscated.C2813rl.m13254a(context, androidx.work.impl.background.systemjob.SystemJobService.class, true);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8690a, "Created SystemJobScheduler and enabled SystemJobService", new java.lang.Throwable[0]);
            return ekVar;
        }
        com.fossil.blesdk.obfuscated.C2656pj a = m12824a(context);
        if (a != null) {
            return a;
        }
        com.fossil.blesdk.obfuscated.C1489bk bkVar = new com.fossil.blesdk.obfuscated.C1489bk(context);
        com.fossil.blesdk.obfuscated.C2813rl.m13254a(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class, true);
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8690a, "Created SystemAlarmScheduler", new java.lang.Throwable[0]);
        return bkVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2656pj m12824a(android.content.Context context) {
        try {
            com.fossil.blesdk.obfuscated.C2656pj pjVar = (com.fossil.blesdk.obfuscated.C2656pj) java.lang.Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(new java.lang.Class[]{android.content.Context.class}).newInstance(new java.lang.Object[]{context});
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8690a, java.lang.String.format("Created %s", new java.lang.Object[]{"androidx.work.impl.background.gcm.GcmScheduler"}), new java.lang.Throwable[0]);
            return pjVar;
        } catch (Throwable th) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8690a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
