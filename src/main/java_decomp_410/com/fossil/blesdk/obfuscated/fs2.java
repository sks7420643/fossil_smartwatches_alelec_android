package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fs2 extends zr2 implements fl3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public el3 j;
    @DexIgnore
    public tr3<ke2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return fs2.m;
        }

        @DexIgnore
        public final fs2 b() {
            return new fs2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ fs2 a;

        @DexIgnore
        public c(fs2 fs2) {
            this.a = fs2;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
        }

        @DexIgnore
        public void b(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
            CharSequence d = gVar.d();
            Unit unit = Unit.METRIC;
            if (kd4.a((Object) d, (Object) PortfolioApp.W.c().getString(R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Ft))) {
                unit = Unit.IMPERIAL;
            }
            fs2.a(this.a).a(unit);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ fs2 a;

        @DexIgnore
        public e(fs2 fs2) {
            this.a = fs2;
        }

        @DexIgnore
        public void a(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
        }

        @DexIgnore
        public void b(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
            CharSequence d = gVar.d();
            Unit unit = Unit.METRIC;
            if (kd4.a((Object) d, (Object) PortfolioApp.W.c().getString(R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs))) {
                unit = Unit.IMPERIAL;
            }
            fs2.a(this.a).b(unit);
        }

        @DexIgnore
        public void c(TabLayout.g gVar) {
            kd4.b(gVar, "tab");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fs2 e;

        @DexIgnore
        public f(fs2 fs2) {
            this.e = fs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.j0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fs2 e;

        @DexIgnore
        public g(fs2 fs2) {
            this.e = fs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            fs2.a(this.e).a(true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fs2 e;

        @DexIgnore
        public h(fs2 fs2) {
            this.e = fs2;
        }

        @DexIgnore
        public final void onClick(View view) {
            fs2.a(this.e).a(false);
        }
    }

    /*
    static {
        String simpleName = fs2.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ el3 a(fs2 fs2) {
        el3 el3 = fs2.j;
        if (el3 != null) {
            return el3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(int i, Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        int i2 = gs2.a[unit.ordinal()];
        if (i2 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "updateData weight=" + i + " metric");
            tr3<ke2> tr3 = this.k;
            if (tr3 != null) {
                ke2 a2 = tr3.a();
                if (a2 != null) {
                    TabLayout.g c2 = a2.w.c(0);
                    if (c2 != null) {
                        c2.g();
                    }
                    RulerValuePicker rulerValuePicker = a2.u;
                    kd4.a((Object) rulerValuePicker, "it.rvpWeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.u.setFormatter(new ProfileFormatter(4));
                    a2.u.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.d(str2, "updateData weight=" + i + " imperial");
            tr3<ke2> tr32 = this.k;
            if (tr32 != null) {
                ke2 a3 = tr32.a();
                if (a3 != null) {
                    TabLayout.g c3 = a3.w.c(1);
                    if (c3 != null) {
                        c3.g();
                    }
                    float f2 = pk2.f((float) i);
                    RulerValuePicker rulerValuePicker2 = a3.u;
                    kd4.a((Object) rulerValuePicker2, "it.rvpWeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.u.setFormatter(new ProfileFormatter(4));
                    a3.u.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g() {
        tr3<ke2> tr3 = this.k;
        if (tr3 != null) {
            ke2 a2 = tr3.a();
            if (a2 != null) {
                DashBar dashBar = a2.s;
                if (dashBar != null) {
                    gs3.a aVar = gs3.a;
                    kd4.a((Object) dashBar, "this");
                    aVar.a(dashBar, 500);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity, true);
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.k = new tr3<>(this, (ke2) qa.a(layoutInflater, R.layout.fragment_onboarding_height_weight, viewGroup, false, O0()));
        tr3<ke2> tr3 = this.k;
        if (tr3 != null) {
            ke2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        el3 el3 = this.j;
        if (el3 != null) {
            el3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        el3 el3 = this.j;
        if (el3 != null) {
            el3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<ke2> tr3 = this.k;
        if (tr3 != null) {
            ke2 a2 = tr3.a();
            if (a2 != null) {
                a2.t.setValuePickerListener(new b(a2, this));
                a2.v.a((TabLayout.c) new c(this));
                a2.u.setValuePickerListener(new d(a2, this));
                a2.w.a((TabLayout.c) new e(this));
                a2.r.setOnClickListener(new f(this));
                a2.x.setOnClickListener(new g(this));
                a2.q.setOnClickListener(new h(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(el3 el3) {
        kd4.b(el3, "presenter");
        this.j = el3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ke2 a;
        @DexIgnore
        public /* final */ /* synthetic */ fs2 b;

        @DexIgnore
        public b(ke2 ke2, fs2 fs2) {
            this.a = ke2;
            this.b = fs2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.a.t;
            kd4.a((Object) rulerValuePicker, "it.rvpHeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                fs2.a(this.b).a(i);
                return;
            }
            fs2.a(this.b).a(Math.round(pk2.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.v;
            kd4.a((Object) tabLayout, "it.tlHeightUnit");
            ft3.a(tabLayout, !z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements gu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ke2 a;
        @DexIgnore
        public /* final */ /* synthetic */ fs2 b;

        @DexIgnore
        public d(ke2 ke2, fs2 fs2) {
            this.a = ke2;
            this.b = fs2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.a.u;
            kd4.a((Object) rulerValuePicker, "it.rvpWeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                fs2.a(this.b).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            fs2.a(this.b).b(Math.round(pk2.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void b(int i) {
        }

        @DexIgnore
        public void a(boolean z) {
            TabLayout tabLayout = this.a.w;
            kd4.a((Object) tabLayout, "it.tlWeightUnit");
            ft3.a(tabLayout, !z);
        }
    }

    @DexIgnore
    public void a(int i, Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        int i2 = gs2.b[unit.ordinal()];
        if (i2 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "updateData height=" + i + " metric");
            tr3<ke2> tr3 = this.k;
            if (tr3 != null) {
                ke2 a2 = tr3.a();
                if (a2 != null) {
                    TabLayout.g c2 = a2.v.c(0);
                    if (c2 != null) {
                        c2.g();
                    }
                    RulerValuePicker rulerValuePicker = a2.t;
                    kd4.a((Object) rulerValuePicker, "it.rvpHeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.t.setFormatter(new ProfileFormatter(-1));
                    a2.t.a(100, 251, i);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local2.d(str2, "updateData height=" + i + " imperial");
            tr3<ke2> tr32 = this.k;
            if (tr32 != null) {
                ke2 a3 = tr32.a();
                if (a3 != null) {
                    TabLayout.g c3 = a3.v.c(1);
                    if (c3 != null) {
                        c3.g();
                    }
                    Pair<Integer, Integer> b2 = pk2.b((float) i);
                    RulerValuePicker rulerValuePicker2 = a3.t;
                    kd4.a((Object) rulerValuePicker2, "it.rvpHeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.t.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker3 = a3.t;
                    Integer first = b2.getFirst();
                    kd4.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = pk2.a(first.intValue());
                    Integer second = b2.getSecond();
                    kd4.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker3.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }
}
