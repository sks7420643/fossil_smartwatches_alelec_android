package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nz0 implements IInterface {
    @DexIgnore
    public /* final */ IBinder e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public nz0(IBinder iBinder, String str) {
        this.e = iBinder;
        this.f = str;
    }

    @DexIgnore
    public final void a(int i, Parcel parcel) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        try {
            this.e.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.e;
    }

    @DexIgnore
    public final void b(int i, Parcel parcel) throws RemoteException {
        try {
            this.e.transact(i, parcel, (Parcel) null, 1);
        } finally {
            parcel.recycle();
        }
    }

    @DexIgnore
    public final Parcel o() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f);
        return obtain;
    }
}
