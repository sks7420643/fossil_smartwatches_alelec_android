package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ap1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class np1 implements zo1 {
    @DexIgnore
    public /* final */ ap1.b e;

    @DexIgnore
    public np1(ap1.b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public final void a(yo1 yo1) {
        this.e.a(mp1.a(yo1));
    }

    @DexIgnore
    public final void b(yo1 yo1, int i, int i2) {
        this.e.b(mp1.a(yo1), i, i2);
    }

    @DexIgnore
    public final void c(yo1 yo1, int i, int i2) {
        this.e.c(mp1.a(yo1), i, i2);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || np1.class != obj.getClass()) {
            return false;
        }
        return this.e.equals(((np1) obj).e);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final void a(yo1 yo1, int i, int i2) {
        this.e.a(mp1.a(yo1), i, i2);
    }
}
