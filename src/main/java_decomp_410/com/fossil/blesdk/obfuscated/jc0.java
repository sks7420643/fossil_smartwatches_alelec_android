package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.auth.api.signin.internal.SignInHubActivity;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jc0 {
    @DexIgnore
    public static cm0 a; // = new cm0("GoogleSignInCommon", new String[0]);

    @DexIgnore
    public static Intent a(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getFallbackSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return a2;
    }

    @DexIgnore
    public static Intent c(Context context, GoogleSignInOptions googleSignInOptions) {
        a.a("getNoImplementationSignInIntent()", new Object[0]);
        Intent a2 = a(context, googleSignInOptions);
        a2.setAction("com.google.android.gms.auth.NO_IMPL");
        return a2;
    }

    @DexIgnore
    public static he0<Status> b(ge0 ge0, Context context, boolean z) {
        a.a("Revoking access", new Object[0]);
        String d = cc0.a(context).d();
        a(context);
        if (z) {
            return fc0.a(d);
        }
        return ge0.b(new mc0(ge0));
    }

    @DexIgnore
    public static he0<Status> a(ge0 ge0, Context context, boolean z) {
        a.a("Signing out", new Object[0]);
        a(context);
        if (z) {
            return ie0.a(Status.i, ge0);
        }
        return ge0.b(new kc0(ge0));
    }

    @DexIgnore
    public static void a(Context context) {
        pc0.a(context).a();
        for (ge0 h : ge0.i()) {
            h.h();
        }
        ve0.d();
    }

    @DexIgnore
    public static zb0 a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount")) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.i;
        }
        return new zb0(googleSignInAccount, status);
    }
}
