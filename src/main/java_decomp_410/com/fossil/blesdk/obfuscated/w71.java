package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzto;
import com.google.android.gms.internal.measurement.zztv;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w71 {
    @DexIgnore
    public /* final */ zztv a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public w71(int i) {
        this.b = new byte[i];
        this.a = zztv.a(this.b);
    }

    @DexIgnore
    public final zzte a() {
        if (this.a.b() == 0) {
            return new zzto(this.b);
        }
        throw new IllegalStateException("Did not write as much data as expected.");
    }

    @DexIgnore
    public final zztv b() {
        return this.a;
    }

    @DexIgnore
    public /* synthetic */ w71(int i, q71 q71) {
        this(i);
    }
}
