package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mr2 implements Factory<lr2> {
    @DexIgnore
    public static /* final */ mr2 a; // = new mr2();

    @DexIgnore
    public static mr2 a() {
        return a;
    }

    @DexIgnore
    public static lr2 b() {
        return new lr2();
    }

    @DexIgnore
    public lr2 get() {
        return b();
    }
}
