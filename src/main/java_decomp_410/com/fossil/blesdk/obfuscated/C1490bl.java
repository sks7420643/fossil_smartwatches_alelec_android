package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bl */
public class C1490bl {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f3770a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f3771b;

    @DexIgnore
    public C1490bl(java.lang.String str, int i) {
        this.f3770a = str;
        this.f3771b = i;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C1490bl.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1490bl blVar = (com.fossil.blesdk.obfuscated.C1490bl) obj;
        if (this.f3771b != blVar.f3771b) {
            return false;
        }
        return this.f3770a.equals(blVar.f3770a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.f3770a.hashCode() * 31) + this.f3771b;
    }
}
