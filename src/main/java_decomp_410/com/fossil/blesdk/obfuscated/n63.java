package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n63 implements MembersInjector<m63> {
    @DexIgnore
    public static void a(m63 m63, MicroAppPresenter microAppPresenter) {
        m63.o = microAppPresenter;
    }

    @DexIgnore
    public static void a(m63 m63, j42 j42) {
        m63.p = j42;
    }
}
