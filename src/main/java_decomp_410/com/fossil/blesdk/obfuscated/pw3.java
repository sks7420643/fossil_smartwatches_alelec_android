package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.hv3;
import com.fossil.blesdk.obfuscated.jv3;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.misfit.frameworks.common.enums.Action;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pw3 {
    @DexIgnore
    public /* final */ hv3 a;
    @DexIgnore
    public /* final */ jv3 b;

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        if (r3.b().b() == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0059, code lost:
        return false;
     */
    @DexIgnore
    public static boolean a(jv3 jv3, hv3 hv3) {
        int e = jv3.e();
        if (!(e == 200 || e == 410 || e == 414 || e == 501 || e == 203 || e == 204)) {
            if (e != 307) {
                if (!(e == 308 || e == 404 || e == 405)) {
                    switch (e) {
                        case 300:
                        case Action.Presenter.NEXT /*301*/:
                            break;
                        case Action.Presenter.PREVIOUS /*302*/:
                            break;
                    }
                }
            }
            if (jv3.a("Expires") == null) {
                if (jv3.b().d() == -1) {
                    if (!jv3.b().c()) {
                    }
                }
            }
        }
        if (jv3.b().i() || hv3.b().i()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public pw3(hv3 hv3, jv3 jv3) {
        this.a = hv3;
        this.b = jv3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ hv3 b;
        @DexIgnore
        public /* final */ jv3 c;
        @DexIgnore
        public Date d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Date h;
        @DexIgnore
        public long i;
        @DexIgnore
        public long j;
        @DexIgnore
        public String k;
        @DexIgnore
        public int l; // = -1;

        @DexIgnore
        public b(long j2, hv3 hv3, jv3 jv3) {
            this.a = j2;
            this.b = hv3;
            this.c = jv3;
            if (jv3 != null) {
                cv3 g2 = jv3.g();
                int b2 = g2.b();
                for (int i2 = 0; i2 < b2; i2++) {
                    String a2 = g2.a(i2);
                    String b3 = g2.b(i2);
                    if (ConfigItem.KEY_DATE.equalsIgnoreCase(a2)) {
                        this.d = tw3.a(b3);
                        this.e = b3;
                    } else if ("Expires".equalsIgnoreCase(a2)) {
                        this.h = tw3.a(b3);
                    } else if ("Last-Modified".equalsIgnoreCase(a2)) {
                        this.f = tw3.a(b3);
                        this.g = b3;
                    } else if ("ETag".equalsIgnoreCase(a2)) {
                        this.k = b3;
                    } else if ("Age".equalsIgnoreCase(a2)) {
                        this.l = rw3.a(b3, -1);
                    } else if (xw3.c.equalsIgnoreCase(a2)) {
                        this.i = Long.parseLong(b3);
                    } else if (xw3.d.equalsIgnoreCase(a2)) {
                        this.j = Long.parseLong(b3);
                    }
                }
            }
        }

        @DexIgnore
        public final long a() {
            Date date = this.d;
            long j2 = 0;
            if (date != null) {
                j2 = Math.max(0, this.j - date.getTime());
            }
            int i2 = this.l;
            if (i2 != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) i2));
            }
            long j3 = this.j;
            return j2 + (j3 - this.i) + (this.a - j3);
        }

        @DexIgnore
        public final long b() {
            su3 b2 = this.c.b();
            if (b2.d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) b2.d());
            }
            if (this.h != null) {
                Date date = this.d;
                long time = this.h.getTime() - (date != null ? date.getTime() : this.j);
                if (time > 0) {
                    return time;
                }
                return 0;
            } else if (this.f == null || this.c.l().d().i() != null) {
                return 0;
            } else {
                Date date2 = this.d;
                long time2 = (date2 != null ? date2.getTime() : this.i) - this.f.getTime();
                if (time2 > 0) {
                    return time2 / 10;
                }
                return 0;
            }
        }

        @DexIgnore
        public pw3 c() {
            pw3 d2 = d();
            return (d2.a == null || !this.b.b().j()) ? d2 : new pw3((hv3) null, (jv3) null);
        }

        @DexIgnore
        public final pw3 d() {
            if (this.c == null) {
                return new pw3(this.b, (jv3) null);
            }
            if (this.b.e() && this.c.f() == null) {
                return new pw3(this.b, (jv3) null);
            }
            if (!pw3.a(this.c, this.b)) {
                return new pw3(this.b, (jv3) null);
            }
            su3 b2 = this.b.b();
            if (b2.h() || a(this.b)) {
                return new pw3(this.b, (jv3) null);
            }
            long a2 = a();
            long b3 = b();
            if (b2.d() != -1) {
                b3 = Math.min(b3, TimeUnit.SECONDS.toMillis((long) b2.d()));
            }
            long j2 = 0;
            long millis = b2.f() != -1 ? TimeUnit.SECONDS.toMillis((long) b2.f()) : 0;
            su3 b4 = this.c.b();
            if (!b4.g() && b2.e() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) b2.e());
            }
            if (!b4.h()) {
                long j3 = millis + a2;
                if (j3 < j2 + b3) {
                    jv3.b j4 = this.c.j();
                    if (j3 >= b3) {
                        j4.a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (a2 > LogBuilder.MAX_INTERVAL && e()) {
                        j4.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new pw3((hv3) null, j4.a());
                }
            }
            hv3.b g2 = this.b.g();
            String str = this.k;
            if (str != null) {
                g2.b("If-None-Match", str);
            } else if (this.f != null) {
                g2.b("If-Modified-Since", this.g);
            } else if (this.d != null) {
                g2.b("If-Modified-Since", this.e);
            }
            hv3 a3 = g2.a();
            return a(a3) ? new pw3(a3, this.c) : new pw3(a3, (jv3) null);
        }

        @DexIgnore
        public final boolean e() {
            return this.c.b().d() == -1 && this.h == null;
        }

        @DexIgnore
        public static boolean a(hv3 hv3) {
            return (hv3.a("If-Modified-Since") == null && hv3.a("If-None-Match") == null) ? false : true;
        }
    }
}
