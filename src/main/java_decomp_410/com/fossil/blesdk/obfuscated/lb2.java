package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lb2 extends kb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.ftv_title, 1);
        y.put(R.id.ll_time_container, 2);
        y.put(R.id.numberPickerOne, 3);
        y.put(R.id.numberPickerTwo, 4);
        y.put(R.id.numberPickerThree, 5);
        y.put(R.id.fb_save, 6);
    }
    */

    @DexIgnore
    public lb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 7, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public lb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[0], objArr[6], objArr[1], objArr[2], objArr[3], objArr[5], objArr[4]);
        this.w = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
