package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.Utility;
import com.fossil.blesdk.device.data.file.FileType;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.net.IDN;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dv3 {
    @DexIgnore
    public static /* final */ char[] h; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ String g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b; // = "";
        @DexIgnore
        public String c; // = "";
        @DexIgnore
        public String d;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public /* final */ List<String> f; // = new ArrayList();
        @DexIgnore
        public List<String> g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b() {
            this.f.add("");
        }

        @DexIgnore
        public static String f(String str) {
            try {
                String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
                if (lowerCase.isEmpty()) {
                    return null;
                }
                return lowerCase;
            } catch (IllegalArgumentException unused) {
                return null;
            }
        }

        @DexIgnore
        public static int g(String str, int i, int i2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt == ':') {
                    return i;
                }
                if (charAt == '[') {
                    do {
                        i++;
                        if (i >= i2) {
                            break;
                        }
                    } while (str.charAt(i) != ']');
                }
                i++;
            }
            return i2;
        }

        @DexIgnore
        public static int h(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt >= 'a' && charAt <= 'z') || (charAt >= 'A' && charAt <= 'Z')) {
                while (true) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                    char charAt2 = str.charAt(i);
                    if ((charAt2 < 'a' || charAt2 > 'z') && !((charAt2 >= 'A' && charAt2 <= 'Z') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.')) {
                        if (charAt2 == ':') {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        @DexIgnore
        public static int i(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        @DexIgnore
        public b a(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.e = i;
            return this;
        }

        @DexIgnore
        public b b(String str) {
            if (str != null) {
                String d2 = d(str, 0, str.length());
                if (d2 != null) {
                    this.d = d2;
                    return this;
                }
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            throw new IllegalArgumentException("host == null");
        }

        @DexIgnore
        public final boolean c(String str) {
            return str.equals(CodelessMatcher.CURRENT_CLASS_NAME) || str.equalsIgnoreCase("%2e");
        }

        @DexIgnore
        public final boolean d(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        @DexIgnore
        public b e(String str) {
            if (str != null) {
                if (str.equalsIgnoreCase("http")) {
                    this.a = "http";
                } else if (str.equalsIgnoreCase(Utility.URL_SCHEME)) {
                    this.a = Utility.URL_SCHEME;
                } else {
                    throw new IllegalArgumentException("unexpected scheme: " + str);
                }
                return this;
            }
            throw new IllegalArgumentException("scheme == null");
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("://");
            if (!this.b.isEmpty() || !this.c.isEmpty()) {
                sb.append(this.b);
                if (!this.c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.c);
                }
                sb.append('@');
            }
            if (this.d.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.d);
                sb.append(']');
            } else {
                sb.append(this.d);
            }
            int b2 = b();
            if (b2 != dv3.b(this.a)) {
                sb.append(':');
                sb.append(b2);
            }
            dv3.b(sb, this.f);
            if (this.g != null) {
                sb.append('?');
                dv3.a(sb, this.g);
            }
            if (this.h != null) {
                sb.append('#');
                sb.append(this.h);
            }
            return sb.toString();
        }

        @DexIgnore
        public final void c() {
            List<String> list = this.f;
            if (!list.remove(list.size() - 1).isEmpty() || this.f.isEmpty()) {
                this.f.add("");
                return;
            }
            List<String> list2 = this.f;
            list2.set(list2.size() - 1, "");
        }

        @DexIgnore
        public static int f(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(dv3.a(str, i, i2, "", false, false));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        }

        @DexIgnore
        public b a(String str) {
            this.g = str != null ? dv3.e(dv3.a(str, " \"'<>#", true, true)) : null;
            return this;
        }

        @DexIgnore
        public dv3 a() {
            if (this.a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.d != null) {
                return new dv3(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        @DexIgnore
        public static String d(String str, int i, int i2) {
            String a2 = dv3.a(str, i, i2);
            if (!a2.startsWith("[") || !a2.endsWith("]")) {
                String f2 = f(a2);
                if (f2 == null) {
                    return null;
                }
                int length = f2.length();
                if (dv3.b(f2, 0, length, "\u0000\t\n\r #%/:?@[\\]") != length) {
                    return null;
                }
                return f2;
            }
            InetAddress e2 = e(a2, 1, a2.length() - 1);
            if (e2 == null) {
                return null;
            }
            byte[] address = e2.getAddress();
            if (address.length == 16) {
                return a(address);
            }
            throw new AssertionError();
        }

        @DexIgnore
        public int b() {
            int i = this.e;
            return i != -1 ? i : dv3.b(this.a);
        }

        @DexIgnore
        public final int b(String str, int i, int i2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != 9 && charAt != 10 && charAt != 12 && charAt != 13 && charAt != ' ') {
                    return i;
                }
                i++;
            }
            return i2;
        }

        @DexIgnore
        public final int c(String str, int i, int i2) {
            for (int i3 = i2 - 1; i3 >= i; i3--) {
                char charAt = str.charAt(i3);
                if (charAt != 9 && charAt != 10 && charAt != 12 && charAt != 13 && charAt != ' ') {
                    return i3 + 1;
                }
            }
            return i;
        }

        @DexIgnore
        public static InetAddress e(String str, int i, int i2) {
            byte[] bArr = new byte[16];
            int i3 = 0;
            int i4 = -1;
            int i5 = -1;
            while (true) {
                if (i >= i2) {
                    break;
                } else if (i3 == bArr.length) {
                    return null;
                } else {
                    int i6 = i + 2;
                    if (i6 > i2 || !str.regionMatches(i, "::", 0, 2)) {
                        if (i3 != 0) {
                            if (str.regionMatches(i, ":", 0, 1)) {
                                i++;
                            } else if (!str.regionMatches(i, CodelessMatcher.CURRENT_CLASS_NAME, 0, 1) || !a(str, i5, i2, bArr, i3 - 2)) {
                                return null;
                            } else {
                                i3 += 2;
                            }
                        }
                        i5 = i;
                    } else if (i4 != -1) {
                        return null;
                    } else {
                        i3 += 2;
                        i4 = i3;
                        if (i6 == i2) {
                            break;
                        }
                        i5 = i6;
                    }
                    i = i5;
                    int i7 = 0;
                    while (i < i2) {
                        int a2 = dv3.a(str.charAt(i));
                        if (a2 == -1) {
                            break;
                        }
                        i7 = (i7 << 4) + a2;
                        i++;
                    }
                    int i8 = i - i5;
                    if (i8 == 0 || i8 > 4) {
                        return null;
                    }
                    int i9 = i3 + 1;
                    bArr[i3] = (byte) ((i7 >>> 8) & 255);
                    i3 = i9 + 1;
                    bArr[i9] = (byte) (i7 & 255);
                }
            }
            if (i3 != bArr.length) {
                if (i4 == -1) {
                    return null;
                }
                int i10 = i3 - i4;
                System.arraycopy(bArr, i4, bArr, bArr.length - i10, i10);
                Arrays.fill(bArr, i4, (bArr.length - i3) + i4, (byte) 0);
            }
            try {
                return InetAddress.getByAddress(bArr);
            } catch (UnknownHostException unused) {
                throw new AssertionError();
            }
        }

        @DexIgnore
        public dv3 a(dv3 dv3, String str) {
            int i;
            int a2;
            int i2;
            String str2 = str;
            boolean z = false;
            int b2 = b(str2, 0, str.length());
            int c2 = c(str2, b2, str.length());
            if (h(str2, b2, c2) != -1) {
                if (str.regionMatches(true, b2, "https:", 0, 6)) {
                    this.a = Utility.URL_SCHEME;
                    b2 += 6;
                } else if (!str.regionMatches(true, b2, "http:", 0, 5)) {
                    return null;
                } else {
                    this.a = "http";
                    b2 += 5;
                }
            } else if (dv3 == null) {
                return null;
            } else {
                this.a = dv3.a;
            }
            int i3 = i(str2, b2, c2);
            char c3 = '?';
            char c4 = '#';
            if (i3 >= 2 || dv3 == null || !dv3.a.equals(this.a)) {
                int i4 = b2 + i3;
                boolean z2 = false;
                while (true) {
                    a2 = dv3.b(str2, i4, c2, "@/\\?#");
                    char charAt = a2 != c2 ? str2.charAt(a2) : 65535;
                    if (charAt == 65535 || charAt == c4 || charAt == '/' || charAt == '\\' || charAt == c3) {
                        i = a2;
                        int g2 = g(str2, i4, i);
                        int i5 = g2 + 1;
                    } else {
                        if (charAt == '@') {
                            if (!z) {
                                int a3 = dv3.b(str2, i4, a2, ":");
                                int i6 = a3;
                                String str3 = "%40";
                                i2 = a2;
                                String a4 = dv3.a(str, i4, a3, " \"':;<=>@[]^`{}|/\\?#", true, false);
                                if (z2) {
                                    a4 = this.b + str3 + a4;
                                }
                                this.b = a4;
                                if (i6 != i2) {
                                    this.c = dv3.a(str, i6 + 1, i2, " \"':;<=>@[]^`{}|/\\?#", true, false);
                                    z = true;
                                }
                                z2 = true;
                            } else {
                                i2 = a2;
                                this.c += "%40" + dv3.a(str, i4, i2, " \"':;<=>@[]^`{}|/\\?#", true, false);
                            }
                            i4 = i2 + 1;
                        }
                        c3 = '?';
                        c4 = '#';
                    }
                }
                i = a2;
                int g22 = g(str2, i4, i);
                int i52 = g22 + 1;
                if (i52 < i) {
                    this.d = d(str2, i4, g22);
                    this.e = f(str2, i52, i);
                    if (this.e == -1) {
                        return null;
                    }
                } else {
                    this.d = d(str2, i4, g22);
                    this.e = dv3.b(this.a);
                }
                if (this.d == null) {
                    return null;
                }
            } else {
                this.b = dv3.e();
                this.c = dv3.a();
                this.d = dv3.d;
                this.e = dv3.e;
                this.f.clear();
                this.f.addAll(dv3.c());
                if (b2 == c2 || str2.charAt(b2) == '#') {
                    a(dv3.d());
                }
                i = b2;
            }
            int a5 = dv3.b(str2, i, c2, "?#");
            a(str2, i, a5);
            if (a5 < c2 && str2.charAt(a5) == '?') {
                int a6 = dv3.b(str2, a5, c2, "#");
                this.g = dv3.e(dv3.a(str, a5 + 1, a6, " \"'<>#", true, true));
                a5 = a6;
            }
            if (a5 < c2 && str2.charAt(a5) == '#') {
                this.h = dv3.a(str, 1 + a5, c2, "", true, false);
            }
            return a();
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0044 A[SYNTHETIC] */
        public final void a(java.lang.String r11, int r12, int r13) {
            /*
                r10 = this;
                if (r12 != r13) goto L_0x0003
                return
            L_0x0003:
                char r0 = r11.charAt(r12)
                r1 = 47
                java.lang.String r2 = ""
                r3 = 1
                if (r0 == r1) goto L_0x001e
                r1 = 92
                if (r0 != r1) goto L_0x0013
                goto L_0x001e
            L_0x0013:
                java.util.List<java.lang.String> r0 = r10.f
                int r1 = r0.size()
                int r1 = r1 - r3
                r0.set(r1, r2)
                goto L_0x0029
            L_0x001e:
                java.util.List<java.lang.String> r0 = r10.f
                r0.clear()
                java.util.List<java.lang.String> r0 = r10.f
                r0.add(r2)
                goto L_0x0041
            L_0x0029:
                r6 = r12
                if (r6 >= r13) goto L_0x0044
                java.lang.String r12 = "/\\"
                int r12 = com.fossil.blesdk.obfuscated.dv3.b(r11, r6, r13, r12)
                if (r12 >= r13) goto L_0x0036
                r0 = 1
                goto L_0x0037
            L_0x0036:
                r0 = 0
            L_0x0037:
                r9 = 1
                r4 = r10
                r5 = r11
                r7 = r12
                r8 = r0
                r4.a((java.lang.String) r5, (int) r6, (int) r7, (boolean) r8, (boolean) r9)
                if (r0 == 0) goto L_0x0029
            L_0x0041:
                int r12 = r12 + 1
                goto L_0x0029
            L_0x0044:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.obfuscated.dv3.b.a(java.lang.String, int, int):void");
        }

        @DexIgnore
        public final void a(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = dv3.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false);
            if (!c(a2)) {
                if (d(a2)) {
                    c();
                    return;
                }
                List<String> list = this.f;
                if (list.get(list.size() - 1).isEmpty()) {
                    List<String> list2 = this.f;
                    list2.set(list2.size() - 1, a2);
                } else {
                    this.f.add(a2);
                }
                if (z) {
                    this.f.add("");
                }
            }
        }

        @DexIgnore
        public static boolean a(String str, int i, int i2, byte[] bArr, int i3) {
            int i4 = i3;
            while (i < i2) {
                if (i4 == bArr.length) {
                    return false;
                }
                if (i4 != i3) {
                    if (str.charAt(i) != '.') {
                        return false;
                    }
                    i++;
                }
                int i5 = i;
                int i6 = 0;
                while (i5 < i2) {
                    char charAt = str.charAt(i5);
                    if (charAt < '0' || charAt > '9') {
                        break;
                    } else if (i6 == 0 && i != i5) {
                        return false;
                    } else {
                        i6 = ((i6 * 10) + charAt) - 48;
                        if (i6 > 255) {
                            return false;
                        }
                        i5++;
                    }
                }
                if (i5 - i == 0) {
                    return false;
                }
                bArr[i4] = (byte) i6;
                i4++;
                i = i5;
            }
            if (i4 != i3 + 4) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public static String a(byte[] bArr) {
            int i = 0;
            int i2 = 0;
            int i3 = -1;
            int i4 = 0;
            while (i2 < bArr.length) {
                int i5 = i2;
                while (i5 < 16 && bArr[i5] == 0 && bArr[i5 + 1] == 0) {
                    i5 += 2;
                }
                int i6 = i5 - i2;
                if (i6 > i4) {
                    i3 = i2;
                    i4 = i6;
                }
                i2 = i5 + 2;
            }
            jo4 jo4 = new jo4();
            while (i < bArr.length) {
                if (i == i3) {
                    jo4.writeByte(58);
                    i += i4;
                    if (i == 16) {
                        jo4.writeByte(58);
                    }
                } else {
                    if (i > 0) {
                        jo4.writeByte(58);
                    }
                    jo4.a((long) (((bArr[i] & FileType.MASKED_INDEX) << 8) | (bArr[i + 1] & FileType.MASKED_INDEX)));
                    i += 2;
                }
            }
            return jo4.z();
        }
    }

    @DexIgnore
    public static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        char c3 = 'a';
        if (c2 < 'a' || c2 > 'f') {
            c3 = 'A';
            if (c2 < 'A' || c2 > 'F') {
                return -1;
            }
        }
        return (c2 - c3) + 10;
    }

    @DexIgnore
    public String d() {
        if (this.f == null) {
            return null;
        }
        int indexOf = this.g.indexOf(63) + 1;
        String str = this.g;
        return this.g.substring(indexOf, b(str, indexOf + 1, str.length(), "#"));
    }

    @DexIgnore
    public String e() {
        if (this.b.isEmpty()) {
            return "";
        }
        int length = this.a.length() + 3;
        String str = this.g;
        return this.g.substring(length, b(str, length, str.length(), ":@"));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof dv3) && ((dv3) obj).g.equals(this.g);
    }

    @DexIgnore
    public String f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.a.equals(Utility.URL_SCHEME);
    }

    @DexIgnore
    public int h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return this.g.hashCode();
    }

    @DexIgnore
    public String i() {
        if (this.f == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        a(sb, this.f);
        return sb.toString();
    }

    @DexIgnore
    public String j() {
        return this.a;
    }

    @DexIgnore
    public URI k() {
        try {
            return new URI(this.g);
        } catch (URISyntaxException unused) {
            throw new IllegalStateException("not valid as a java.net.URI: " + this.g);
        }
    }

    @DexIgnore
    public URL l() {
        try {
            return new URL(this.g);
        } catch (MalformedURLException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public String toString() {
        return this.g;
    }

    @DexIgnore
    public dv3(b bVar) {
        this.a = bVar.a;
        this.b = d(bVar.b);
        this.c = d(bVar.c);
        this.d = bVar.d;
        this.e = bVar.b();
        a(bVar.f);
        List<String> list = bVar.g;
        this.f = list != null ? a(list) : null;
        String str = bVar.h;
        if (str != null) {
            d(str);
        }
        this.g = bVar.toString();
    }

    @DexIgnore
    public static int b(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals(Utility.URL_SCHEME) ? 443 : -1;
    }

    @DexIgnore
    public List<String> c() {
        int indexOf = this.g.indexOf(47, this.a.length() + 3);
        String str = this.g;
        int b2 = b(str, indexOf, str.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < b2) {
            int i = indexOf + 1;
            int b3 = b(this.g, i, b2, ZendeskConfig.SLASH);
            arrayList.add(this.g.substring(i, b3));
            indexOf = b3;
        }
        return arrayList;
    }

    @DexIgnore
    public String a() {
        if (this.c.isEmpty()) {
            return "";
        }
        int indexOf = this.g.indexOf(64);
        return this.g.substring(this.g.indexOf(58, this.a.length() + 3) + 1, indexOf);
    }

    @DexIgnore
    public String b() {
        int indexOf = this.g.indexOf(47, this.a.length() + 3);
        String str = this.g;
        return this.g.substring(indexOf, b(str, indexOf, str.length(), "?#"));
    }

    @DexIgnore
    public static String d(String str) {
        return a(str, 0, str.length());
    }

    @DexIgnore
    public static List<String> e(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i <= str.length()) {
            int indexOf = str.indexOf(38, i);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i, indexOf));
                arrayList.add((Object) null);
            } else {
                arrayList.add(str.substring(i, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i = indexOf + 1;
        }
        return arrayList;
    }

    @DexIgnore
    public static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i += 2) {
            String str = list.get(i);
            String str2 = list.get(i + 1);
            if (i > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    @DexIgnore
    public static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append('/');
            sb.append(list.get(i));
        }
    }

    @DexIgnore
    public static dv3 c(String str) {
        return new b().a((dv3) null, str);
    }

    @DexIgnore
    public static int b(String str, int i, int i2, String str2) {
        while (i < i2) {
            if (str2.indexOf(str.charAt(i)) != -1) {
                return i;
            }
            i++;
        }
        return i2;
    }

    @DexIgnore
    public dv3 a(String str) {
        return new b().a(this, str);
    }

    @DexIgnore
    public final List<String> a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String next = it.next();
            arrayList.add(next != null ? d(next) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    @DexIgnore
    public static String a(String str, int i, int i2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str.charAt(i3) == '%') {
                jo4 jo4 = new jo4();
                jo4.a(str, i, i3);
                a(jo4, str, i3, i2);
                return jo4.z();
            }
        }
        return str.substring(i, i2);
    }

    @DexIgnore
    public static void a(jo4 jo4, String str, int i, int i2) {
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (codePointAt == 37) {
                int i3 = i + 2;
                if (i3 < i2) {
                    int a2 = a(str.charAt(i + 1));
                    int a3 = a(str.charAt(i3));
                    if (!(a2 == -1 || a3 == -1)) {
                        jo4.writeByte((a2 << 4) + a3);
                        i = i3;
                        i += Character.charCount(codePointAt);
                    }
                }
            }
            jo4.c(codePointAt);
            i += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public static String a(String str, int i, int i2, String str2, boolean z, boolean z2) {
        int i3 = i;
        while (i3 < i2) {
            int codePointAt = str.codePointAt(i3);
            if (codePointAt < 32 || codePointAt >= 127 || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && !z) || (z2 && codePointAt == 43))) {
                jo4 jo4 = new jo4();
                jo4.a(str, i, i3);
                a(jo4, str, i3, i2, str2, z, z2);
                return jo4.z();
            }
            i3 += Character.charCount(codePointAt);
        }
        return str.substring(i, i2);
    }

    @DexIgnore
    public static void a(jo4 jo4, String str, int i, int i2, String str2, boolean z, boolean z2) {
        jo4 jo42 = null;
        while (i < i2) {
            int codePointAt = str.codePointAt(i);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (z2 && codePointAt == 43) {
                    jo4.a(z ? "%20" : "%2B");
                } else if (codePointAt < 32 || codePointAt >= 127 || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && !z)) {
                    if (jo42 == null) {
                        jo42 = new jo4();
                    }
                    jo42.c(codePointAt);
                    while (!jo42.g()) {
                        byte readByte = jo42.readByte() & FileType.MASKED_INDEX;
                        jo4.writeByte(37);
                        jo4.writeByte((int) h[(readByte >> 4) & 15]);
                        jo4.writeByte((int) h[readByte & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
                    }
                } else {
                    jo4.c(codePointAt);
                }
            }
            i += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public static String a(String str, String str2, boolean z, boolean z2) {
        return a(str, 0, str.length(), str2, z, z2);
    }
}
