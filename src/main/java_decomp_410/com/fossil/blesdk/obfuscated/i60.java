package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.nio.charset.Charset;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i60 extends g60 {
    @DexIgnore
    public static /* final */ a Q; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final byte[] a(JSONObject jSONObject) {
            String jSONObject2 = jSONObject.toString();
            kd4.a((Object) jSONObject2, "jsonFileContent.toString()");
            Charset f = ua0.y.f();
            if (jSONObject2 != null) {
                byte[] bytes = jSONObject2.getBytes(f);
                kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                return bytes;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ i60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, JSONObject jSONObject, boolean z, float f, String str, int i, fd4 fd4) {
        this(peripheral, aVar, phaseId, jSONObject, r6, r7, r8);
        String str2;
        boolean z2 = (i & 16) != 0 ? true : z;
        float f2 = (i & 32) != 0 ? 1.0f : f;
        if ((i & 64) != 0) {
            String uuid = UUID.randomUUID().toString();
            kd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public i60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, JSONObject jSONObject, boolean z, float f, String str) {
        super(peripheral, aVar, phaseId, z, z40.b.b(peripheral.k(), FileType.UI_SCRIPT), Q.a(jSONObject), f, r10);
        JSONObject jSONObject2 = jSONObject;
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(phaseId, "phaseId");
        kd4.b(jSONObject, "jsonFileContent");
        String str2 = str;
        kd4.b(str2, "phaseUuid");
    }
}
