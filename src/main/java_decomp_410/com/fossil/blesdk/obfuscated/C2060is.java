package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.is */
public abstract class C2060is<T> implements com.fossil.blesdk.obfuscated.C2427mo<android.graphics.ImageDecoder.Source, T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1514bt f6167a; // = com.fossil.blesdk.obfuscated.C1514bt.m5110b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.is$a")
    /* renamed from: com.fossil.blesdk.obfuscated.is$a */
    public class C2061a implements android.graphics.ImageDecoder.OnHeaderDecodedListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ int f6168a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ int f6169b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ boolean f6170c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ com.bumptech.glide.load.DecodeFormat f6171d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.bumptech.glide.load.resource.bitmap.DownsampleStrategy f6172e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.bumptech.glide.load.PreferredColorSpace f6173f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.is$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.is$a$a */
        public class C2062a implements android.graphics.ImageDecoder.OnPartialImageListener {
            @DexIgnore
            public C2062a(com.fossil.blesdk.obfuscated.C2060is.C2061a aVar) {
            }

            @DexIgnore
            public boolean onPartialImage(android.graphics.ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        @DexIgnore
        public C2061a(int i, int i2, boolean z, com.bumptech.glide.load.DecodeFormat decodeFormat, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.bumptech.glide.load.PreferredColorSpace preferredColorSpace) {
            this.f6168a = i;
            this.f6169b = i2;
            this.f6170c = z;
            this.f6171d = decodeFormat;
            this.f6172e = downsampleStrategy;
            this.f6173f = preferredColorSpace;
        }

        @DexIgnore
        @android.annotation.SuppressLint({"Override"})
        public void onHeaderDecoded(android.graphics.ImageDecoder imageDecoder, android.graphics.ImageDecoder.ImageInfo imageInfo, android.graphics.ImageDecoder.Source source) {
            boolean z = true;
            if (com.fossil.blesdk.obfuscated.C2060is.this.f6167a.mo9293a(this.f6168a, this.f6169b, this.f6170c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.f6171d == com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new com.fossil.blesdk.obfuscated.C2060is.C2061a.C2062a(this));
            android.util.Size size = imageInfo.getSize();
            int i = this.f6168a;
            if (i == Integer.MIN_VALUE) {
                i = size.getWidth();
            }
            int i2 = this.f6169b;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getHeight();
            }
            float b = this.f6172e.mo4006b(size.getWidth(), size.getHeight(), i, i2);
            int round = java.lang.Math.round(((float) size.getWidth()) * b);
            int round2 = java.lang.Math.round(((float) size.getHeight()) * b);
            if (android.util.Log.isLoggable("ImageDecoder", 2)) {
                android.util.Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b);
            }
            imageDecoder.setTargetSize(round, round2);
            int i3 = android.os.Build.VERSION.SDK_INT;
            if (i3 >= 28) {
                if (this.f6173f != com.bumptech.glide.load.PreferredColorSpace.DISPLAY_P3 || imageInfo.getColorSpace() == null || !imageInfo.getColorSpace().isWideGamut()) {
                    z = false;
                }
                imageDecoder.setTargetColorSpace(android.graphics.ColorSpace.get(z ? android.graphics.ColorSpace.Named.DISPLAY_P3 : android.graphics.ColorSpace.Named.SRGB));
            } else if (i3 >= 26) {
                imageDecoder.setTargetColorSpace(android.graphics.ColorSpace.get(android.graphics.ColorSpace.Named.SRGB));
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1438aq<T> mo12078a(android.graphics.ImageDecoder.Source source, int i, int i2, android.graphics.ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws java.io.IOException;

    @DexIgnore
    /* renamed from: a */
    public final boolean mo9301a(android.graphics.ImageDecoder.Source source, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<T> mo9299a(android.graphics.ImageDecoder.Source source, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C2060is.C2061a aVar = new com.fossil.blesdk.obfuscated.C2060is.C2061a(i, i2, loVar.mo13319a(com.fossil.blesdk.obfuscated.C3240ws.f10695i) != null && ((java.lang.Boolean) loVar.mo13319a(com.fossil.blesdk.obfuscated.C3240ws.f10695i)).booleanValue(), (com.bumptech.glide.load.DecodeFormat) loVar.mo13319a(com.fossil.blesdk.obfuscated.C3240ws.f10692f), (com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) loVar.mo13319a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2121f), (com.bumptech.glide.load.PreferredColorSpace) loVar.mo13319a(com.fossil.blesdk.obfuscated.C3240ws.f10693g));
        return mo12078a(source, i, i2, (android.graphics.ImageDecoder.OnHeaderDecodedListener) aVar);
    }
}
