package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ov {
    @DexIgnore
    boolean a(ov ovVar);

    @DexIgnore
    void c();

    @DexIgnore
    void clear();

    @DexIgnore
    void d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    boolean isRunning();
}
