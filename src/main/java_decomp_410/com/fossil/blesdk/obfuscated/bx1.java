package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bx1 {
    @DexIgnore
    <T> void a(Class<T> cls, zw1<? super T> zw1);

    @DexIgnore
    <T> void a(Class<T> cls, Executor executor, zw1<? super T> zw1);
}
