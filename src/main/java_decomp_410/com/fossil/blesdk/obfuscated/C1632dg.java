package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dg */
public class C1632dg {
    @DexIgnore
    /* renamed from: a */
    public static java.lang.StringBuilder m5847a() {
        return new java.lang.StringBuilder();
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5848a(java.lang.StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("?");
            if (i2 < i - 1) {
                sb.append(",");
            }
        }
    }
}
