package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class au2 {
    @DexIgnore
    public /* final */ yt2 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public au2(yt2 yt2, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        kd4.b(yt2, "mView");
        kd4.b(str, "mDeviceId");
        kd4.b(arrayList, "mAlarms");
        this.a = yt2;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final yt2 d() {
        return this.a;
    }
}
