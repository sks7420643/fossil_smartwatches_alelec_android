package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a52 implements Factory<zk2> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<h42> c;
    @DexIgnore
    public /* final */ Provider<en2> d;

    @DexIgnore
    public a52(n42 n42, Provider<Context> provider, Provider<h42> provider2, Provider<en2> provider3) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static a52 a(n42 n42, Provider<Context> provider, Provider<h42> provider2, Provider<en2> provider3) {
        return new a52(n42, provider, provider2, provider3);
    }

    @DexIgnore
    public static zk2 b(n42 n42, Provider<Context> provider, Provider<h42> provider2, Provider<en2> provider3) {
        return a(n42, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static zk2 a(n42 n42, Context context, h42 h42, en2 en2) {
        zk2 a2 = n42.a(context, h42, en2);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public zk2 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
