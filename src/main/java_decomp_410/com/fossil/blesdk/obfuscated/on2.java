package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AlarmHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class on2 implements MembersInjector<nn2> {
    @DexIgnore
    public static void a(nn2 nn2, en2 en2) {
        nn2.a = en2;
    }

    @DexIgnore
    public static void a(nn2 nn2, ir3 ir3) {
        nn2.b = ir3;
    }

    @DexIgnore
    public static void a(nn2 nn2, AlarmHelper alarmHelper) {
        nn2.c = alarmHelper;
    }
}
