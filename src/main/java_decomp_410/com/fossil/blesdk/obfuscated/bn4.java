package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.yl4;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.Response;
import okio.ByteString;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bn4 {
    /*
    static {
        ByteString.encodeUtf8("\"\\");
        ByteString.encodeUtf8("\t ,=");
    }
    */

    @DexIgnore
    public static long a(Response response) {
        return a(response.D());
    }

    @DexIgnore
    public static boolean b(yl4 yl4) {
        return c(yl4).contains(Marker.ANY_MARKER);
    }

    @DexIgnore
    public static boolean c(Response response) {
        return b(response.D());
    }

    @DexIgnore
    public static Set<String> d(Response response) {
        return c(response.D());
    }

    @DexIgnore
    public static yl4 e(Response response) {
        return a(response.G().L().c(), response.D());
    }

    @DexIgnore
    public static long a(yl4 yl4) {
        return a(yl4.a("Content-Length"));
    }

    @DexIgnore
    public static boolean b(Response response) {
        if (response.L().e().equals("HEAD")) {
            return false;
        }
        int B = response.B();
        if (((B >= 100 && B < 200) || B == 204 || B == 304) && a(response) == -1 && !"chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static Set<String> c(yl4 yl4) {
        Set<String> emptySet = Collections.emptySet();
        int b = yl4.b();
        Set<String> set = emptySet;
        for (int i = 0; i < b; i++) {
            if ("Vary".equalsIgnoreCase(yl4.a(i))) {
                String b2 = yl4.b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b2.split(",")) {
                    set.add(trim.trim());
                }
            }
        }
        return set;
    }

    @DexIgnore
    public static long a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static boolean a(Response response, yl4 yl4, dm4 dm4) {
        for (String next : d(response)) {
            if (!jm4.a((Object) yl4.b(next), (Object) dm4.b(next))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static yl4 a(yl4 yl4, yl4 yl42) {
        Set<String> c = c(yl42);
        if (c.isEmpty()) {
            return new yl4.a().a();
        }
        yl4.a aVar = new yl4.a();
        int b = yl4.b();
        for (int i = 0; i < b; i++) {
            String a = yl4.a(i);
            if (c.contains(a)) {
                aVar.a(a, yl4.b(i));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static int b(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt != ' ' && charAt != 9) {
                break;
            }
            i++;
        }
        return i;
    }

    @DexIgnore
    public static void a(rl4 rl4, zl4 zl4, yl4 yl4) {
        if (rl4 != rl4.a) {
            List<ql4> a = ql4.a(zl4, yl4);
            if (!a.isEmpty()) {
                rl4.a(zl4, a);
            }
        }
    }

    @DexIgnore
    public static int a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static int a(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
