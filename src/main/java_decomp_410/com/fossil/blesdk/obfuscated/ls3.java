package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.NotificationVibePattern;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.CRC32;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ls3 {
    @DexIgnore
    public static /* synthetic */ List a(List list, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(list, z);
    }

    @DexIgnore
    public static final AppNotificationFilter a() {
        String packageName = DianaNotificationObj.AApplicationName.FOSSIL.getPackageName();
        String appName = DianaNotificationObj.AApplicationName.FOSSIL.getAppName();
        CRC32 crc32 = new CRC32();
        Charset charset = bf4.a;
        if (packageName != null) {
            byte[] bytes = packageName.getBytes(charset);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            crc32.update(bytes);
            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(appName, crc32.getValue(), (byte) 2, NotificationBaseObj.ANotificationType.NOTIFICATION.name(), R.mipmap.ic_launcher, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
            appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
            return appNotificationFilter;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x006f A[SYNTHETIC] */
    public static final List<AppNotificationFilter> a(List<AppWrapper> list, boolean z) {
        AppNotificationFilter appNotificationFilter;
        DianaNotificationObj.AApplicationName aApplicationName;
        boolean z2 = z;
        kd4.b(list, "$this$toAppNotificationFilter");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            String str = null;
            if (!it.hasNext()) {
                break;
            }
            InstalledApp installedApp = ((AppWrapper) it.next()).getInstalledApp();
            if (installedApp != null) {
                str = installedApp.getIdentifier() + installedApp.isSelected();
            }
            if (str != null) {
                arrayList.add(str);
            }
        }
        FLogger.INSTANCE.getLocal().d("NotificationExtension", "toAppNotificationFilter(), inputAppWrapper = " + arrayList + ", isAllAppEnabled=" + z2);
        ArrayList arrayList2 = new ArrayList();
        for (AppWrapper installedApp2 : list) {
            InstalledApp installedApp3 = installedApp2.getInstalledApp();
            if (installedApp3 != null) {
                String identifier = installedApp3.getIdentifier();
                Boolean isSelected = installedApp3.isSelected();
                kd4.a((Object) isSelected, "installedApp.isSelected");
                if (isSelected.booleanValue() || z2) {
                    DianaNotificationObj.AApplicationName.Companion companion = DianaNotificationObj.AApplicationName.Companion;
                    kd4.a((Object) identifier, "packageNameApp");
                    if (companion.isPackageNameSupportedBySDK(identifier)) {
                        DianaNotificationObj.AApplicationName[] values = DianaNotificationObj.AApplicationName.values();
                        int length = values.length;
                        int i = 0;
                        while (true) {
                            if (i >= length) {
                                aApplicationName = null;
                                break;
                            }
                            aApplicationName = values[i];
                            if (kd4.a((Object) aApplicationName.getPackageName(), (Object) identifier)) {
                                break;
                            }
                            i++;
                        }
                        if (aApplicationName != null) {
                            appNotificationFilter = new AppNotificationFilter(new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), aApplicationName.getIconResourceId(), aApplicationName.getIconFwPath(), aApplicationName.getNotificationType()));
                            if (appNotificationFilter == null) {
                                arrayList2.add(appNotificationFilter);
                            }
                        }
                    }
                }
            }
            appNotificationFilter = null;
            if (appNotificationFilter == null) {
            }
        }
        ArrayList arrayList3 = new ArrayList(arrayList2);
        arrayList3.add(a());
        FLogger.INSTANCE.getLocal().d("NotificationExtension", "toAppNotificationFilter(), output = " + arrayList2);
        return arrayList3;
    }
}
