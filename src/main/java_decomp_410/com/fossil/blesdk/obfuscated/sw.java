package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sw {
    @DexIgnore
    public Class<?> a;
    @DexIgnore
    public Class<?> b;
    @DexIgnore
    public Class<?> c;

    @DexIgnore
    public sw() {
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || sw.class != obj.getClass()) {
            return false;
        }
        sw swVar = (sw) obj;
        return this.a.equals(swVar.a) && this.b.equals(swVar.b) && uw.b((Object) this.c, (Object) swVar.c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.a.hashCode() * 31) + this.b.hashCode()) * 31;
        Class<?> cls = this.c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    @DexIgnore
    public String toString() {
        return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
    }

    @DexIgnore
    public sw(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }
}
