package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fw<R> {
    @DexIgnore
    ew<R> a(DataSource dataSource, boolean z);
}
