package com.fossil.blesdk.obfuscated;

import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bb {
    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract bb a(int i, Fragment fragment);

    @DexIgnore
    public abstract bb a(int i, Fragment fragment, String str);

    @DexIgnore
    public abstract bb a(Fragment fragment);

    @DexIgnore
    public abstract bb a(Fragment fragment, String str);

    @DexIgnore
    public abstract bb a(String str);

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract bb b(int i, Fragment fragment, String str);

    @DexIgnore
    public abstract bb b(Fragment fragment);

    @DexIgnore
    public abstract bb c(Fragment fragment);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract bb d(Fragment fragment);

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public abstract bb e(Fragment fragment);
}
