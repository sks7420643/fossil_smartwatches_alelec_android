package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class am1 extends cl1 {
    @DexIgnore
    public static /* final */ String[] f; // = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;"};
    @DexIgnore
    public static /* final */ String[] g; // = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    @DexIgnore
    public static /* final */ String[] h; // = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;"};
    @DexIgnore
    public static /* final */ String[] i; // = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    @DexIgnore
    public static /* final */ String[] j; // = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] k; // = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    @DexIgnore
    public /* final */ dm1 d; // = new dm1(this, getContext(), "google_app_measurement.db");
    @DexIgnore
    public /* final */ yk1 e; // = new yk1(c());

    @DexIgnore
    public am1(dl1 dl1) {
        super(dl1);
    }

    @DexIgnore
    public final long A() {
        return a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @DexIgnore
    public final long B() {
        return a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    @DexIgnore
    public final boolean C() {
        return a("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    @DexIgnore
    public final boolean D() {
        return a("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final long E() {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery("select rowid from raw_events order by rowid desc limit 1;", (String[]) null);
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            long j2 = cursor.getLong(0);
            if (cursor != null) {
                cursor.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            d().s().a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean F() {
        return getContext().getDatabasePath("google_app_measurement.db").exists();
    }

    @DexIgnore
    public final long a(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j2 = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            d().s().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x011f  */
    public final dg1 b(String str, String str2) {
        Cursor cursor;
        Boolean bool;
        String str3 = str2;
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        try {
            boolean z = false;
            Cursor query = v().query("events", new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp", "last_bundled_timestamp", "last_bundled_day", "last_sampled_complex_event_id", "last_sampling_rate", "last_exempt_from_sampling"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                long j2 = query.getLong(0);
                long j3 = query.getLong(1);
                long j4 = query.getLong(2);
                long j5 = query.isNull(3) ? 0 : query.getLong(3);
                Long valueOf = query.isNull(4) ? null : Long.valueOf(query.getLong(4));
                Long valueOf2 = query.isNull(5) ? null : Long.valueOf(query.getLong(5));
                Long valueOf3 = query.isNull(6) ? null : Long.valueOf(query.getLong(6));
                if (!query.isNull(7)) {
                    if (query.getLong(7) == 1) {
                        z = true;
                    }
                    bool = Boolean.valueOf(z);
                } else {
                    bool = null;
                }
                long j6 = j5;
                cursor = query;
                try {
                    dg1 dg1 = new dg1(str, str2, j2, j3, j4, j6, valueOf, valueOf2, valueOf3, bool);
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for event aggregates, expected one. appId", tg1.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return dg1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying events. appId", tg1.a(str), i().a(str2), e);
                        if (cursor != null) {
                            cursor.close();
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                cursor = query;
                d().s().a("Error querying events. appId", tg1.a(str), i().a(str2), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor = query;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying events. appId", tg1.a(str), i().a(str2), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void c(String str, String str2) {
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        try {
            d().A().a("Deleted user attribute rows", Integer.valueOf(v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e2) {
            d().s().a("Error deleting user attribute. appId", tg1.a(str), i().c(str2), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9  */
    public final ml1 d(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        try {
            cursor = v().query("user_attributes", new String[]{"set_timestamp", "value", "origin"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    ml1 ml1 = new ml1(str, cursor.getString(2), str2, cursor.getLong(0), a(cursor, 1));
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for user property, expected one. appId", tg1.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return ml1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying user property. appId", tg1.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying user property. appId", tg1.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying user property. appId", tg1.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0125  */
    public final vl1 e(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        try {
            cursor = v().query("conditional_properties", new String[]{"origin", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                try {
                    Object a = a(cursor, 1);
                    boolean z = cursor.getInt(2) != 0;
                    String str4 = str;
                    vl1 vl1 = new vl1(str4, string, new kl1(str2, cursor.getLong(8), a, string), cursor.getLong(6), z, cursor.getString(3), (hg1) m().a(cursor.getBlob(5), hg1.CREATOR), cursor.getLong(4), (hg1) m().a(cursor.getBlob(7), hg1.CREATOR), cursor.getLong(9), (hg1) m().a(cursor.getBlob(10), hg1.CREATOR));
                    if (cursor.moveToNext()) {
                        d().s().a("Got multiple records for conditional property, expected one", tg1.a(str), i().c(str3));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return vl1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying conditional property", tg1.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying conditional property", tg1.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying conditional property", tg1.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final int f(String str, String str2) {
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        try {
            return v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            d().s().a("Error deleting conditional property", tg1.a(str), i().c(str2), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<s51>> g(String str, String str2) {
        Cursor cursor;
        q();
        e();
        bk0.b(str);
        bk0.b(str2);
        g4 g4Var = new g4();
        try {
            cursor = v().query("event_filters", new String[]{"audience_id", "data"}, "app_id=? AND event_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<s51>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    tb1 a = tb1.a(blob, 0, blob.length);
                    s51 s51 = new s51();
                    try {
                        s51.a(a);
                        int i2 = cursor.getInt(0);
                        List list = (List) g4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            g4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(s51);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter. appId", tg1.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filters. appId", tg1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filters. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2  */
    public final Map<Integer, List<v51>> h(String str, String str2) {
        Cursor cursor;
        q();
        e();
        bk0.b(str);
        bk0.b(str2);
        g4 g4Var = new g4();
        try {
            cursor = v().query("property_filters", new String[]{"audience_id", "data"}, "app_id=? AND property_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<v51>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    tb1 a = tb1.a(blob, 0, blob.length);
                    v51 v51 = new v51();
                    try {
                        v51.a(a);
                        int i2 = cursor.getInt(0);
                        List list = (List) g4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            g4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(v51);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter", tg1.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filters. appId", tg1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filters. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long i(String str, String str2) {
        long j2;
        String str3 = str;
        String str4 = str2;
        bk0.b(str);
        bk0.b(str2);
        e();
        q();
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str4);
            sb.append(" from app2 where app_id=?");
            try {
                j2 = a(sb.toString(), new String[]{str3}, -1);
                if (j2 == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str3);
                    contentValues.put("first_open_count", 0);
                    contentValues.put("previous_install_count", 0);
                    if (v.insertWithOnConflict("app2", (String) null, contentValues, 5) == -1) {
                        d().s().a("Failed to insert column (got -1). appId", tg1.a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    j2 = 0;
                }
            } catch (SQLiteException e2) {
                e = e2;
                j2 = 0;
                try {
                    d().s().a("Error inserting column. appId", tg1.a(str), str4, e);
                    v.endTransaction();
                    return j2;
                } catch (Throwable th) {
                    th = th;
                    v.endTransaction();
                    throw th;
                }
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str3);
                contentValues2.put(str4, Long.valueOf(1 + j2));
                if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str3})) == 0) {
                    d().s().a("Failed to update column (got 0). appId", tg1.a(str), str4);
                    v.endTransaction();
                    return -1;
                }
                v.setTransactionSuccessful();
                v.endTransaction();
                return j2;
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error inserting column. appId", tg1.a(str), str4, e);
                v.endTransaction();
                return j2;
            }
        } catch (SQLiteException e4) {
            e = e4;
            j2 = 0;
            d().s().a("Error inserting column. appId", tg1.a(str), str4, e);
            v.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final void t() {
        q();
        v().beginTransaction();
    }

    @DexIgnore
    public final void u() {
        q();
        v().endTransaction();
    }

    @DexIgnore
    public final SQLiteDatabase v() {
        e();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            d().v().a("Error opening database", e2);
            throw e2;
        }
    }

    @DexIgnore
    public final void w() {
        q();
        v().setTransactionSuccessful();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    public final String x() {
        Cursor cursor;
        try {
            cursor = v().rawQuery("select app_id from queue order by has_realtime desc, rowid asc limit 1;", (String[]) null);
            try {
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Database error getting next bundle app id", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Database error getting next bundle app id", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean y() {
        return a("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final void z() {
        e();
        q();
        if (F()) {
            long a = k().h.a();
            long c = c().c();
            if (Math.abs(c - a) > jg1.I.a().longValue()) {
                k().h.a(c);
                e();
                q();
                if (F()) {
                    int delete = v().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(c().b()), String.valueOf(xl1.t())});
                    if (delete > 0) {
                        d().A().a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                    }
                }
            }
        }
    }

    @DexIgnore
    public final long a(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j3 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j3;
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            d().s().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long f(String str) {
        bk0.b(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    @DexIgnore
    public final long c(String str) {
        bk0.b(str);
        e();
        q();
        try {
            return (long) v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, l().b(str, jg1.z))))});
        } catch (SQLiteException e2) {
            d().s().a("Error deleting over the limit events. appId", tg1.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final void a(dg1 dg1) {
        bk0.a(dg1);
        e();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", dg1.a);
        contentValues.put("name", dg1.b);
        contentValues.put("lifetime_count", Long.valueOf(dg1.c));
        contentValues.put("current_bundle_count", Long.valueOf(dg1.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(dg1.e));
        contentValues.put("last_bundled_timestamp", Long.valueOf(dg1.f));
        contentValues.put("last_bundled_day", dg1.g);
        contentValues.put("last_sampled_complex_event_id", dg1.h);
        contentValues.put("last_sampling_rate", dg1.i);
        Boolean bool = dg1.j;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (v().insertWithOnConflict("events", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update event aggregates (got -1). appId", tg1.a(dg1.a));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing event aggregates. appId", tg1.a(dg1.a), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    public final byte[] d(String str) {
        Cursor cursor;
        bk0.b(str);
        e();
        q();
        try {
            cursor = v().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    d().s().a("Got multiple records for app config, expected one. appId", tg1.a(str));
                }
                if (cursor != null) {
                    cursor.close();
                }
                return blob;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error querying remote config. appId", tg1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error querying remote config. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final List<vl1> b(String str, String str2, String str3) {
        bk0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat(Marker.ANY_MARKER));
            sb.append(" and name glob ?");
        }
        return b(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0098  */
    public final Map<Integer, g61> e(String str) {
        Cursor cursor;
        q();
        e();
        bk0.b(str);
        try {
            cursor = v().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                g4 g4Var = new g4();
                do {
                    int i2 = cursor.getInt(0);
                    byte[] blob = cursor.getBlob(1);
                    tb1 a = tb1.a(blob, 0, blob.length);
                    g61 g61 = new g61();
                    try {
                        g61.a(a);
                        g4Var.put(Integer.valueOf(i2), g61);
                    } catch (IOException e2) {
                        d().s().a("Failed to merge filter results. appId, audienceId, error", tg1.a(str), Integer.valueOf(i2), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return g4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Database error querying filter results. appId", tg1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Database error querying filter results. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(ml1 ml1) {
        bk0.a(ml1);
        e();
        q();
        if (d(ml1.a, ml1.c) == null) {
            if (nl1.e(ml1.c)) {
                if (a("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{ml1.a}) >= 25) {
                    return false;
                }
            } else {
                if (a("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{ml1.a, ml1.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", ml1.a);
        contentValues.put("origin", ml1.b);
        contentValues.put("name", ml1.c);
        contentValues.put("set_timestamp", Long.valueOf(ml1.d));
        a(contentValues, "value", ml1.e);
        try {
            if (v().insertWithOnConflict("user_attributes", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update user property (got -1). appId", tg1.a(ml1.a));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing user property. appId", tg1.a(ml1.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final List<vl1> b(String str, String[] strArr) {
        e();
        q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = v().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, (String) null, (String) null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j2 = cursor.getLong(6);
                    long j3 = cursor.getLong(8);
                    long j4 = cursor.getLong(10);
                    boolean z2 = z;
                    vl1 vl1 = r3;
                    vl1 vl12 = new vl1(string, string2, new kl1(string3, j4, a, string2), j3, z2, string4, (hg1) m().a(cursor.getBlob(7), hg1.CREATOR), j2, (hg1) m().a(cursor.getBlob(9), hg1.CREATOR), cursor.getLong(11), (hg1) m().a(cursor.getBlob(12), hg1.CREATOR));
                    arrayList.add(vl1);
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    d().s().a("Read more than the max allowed conditional properties, ignoring extra", 1000);
                    break;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e2) {
            d().s().a("Error querying conditional user property value", e2);
            List<vl1> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    public final List<ml1> a(String str) {
        Cursor cursor;
        bk0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList();
        try {
            cursor = v().query("user_attributes", new String[]{"name", "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", "1000");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                do {
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    String str2 = string2;
                    long j2 = cursor.getLong(2);
                    Object a = a(cursor, 3);
                    if (a == null) {
                        d().s().a("Read invalid user property value, ignoring it. appId", tg1.a(str));
                    } else {
                        arrayList.add(new ml1(str, str2, string, j2, a));
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error querying user properties. appId", tg1.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error querying user properties. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0115 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0119 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x014d A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0150 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x015f A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0174 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0191 A[Catch:{ SQLiteException -> 0x01a8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01d5  */
    public final ql1 b(String str) {
        Cursor cursor;
        boolean z;
        boolean z2;
        String str2 = str;
        bk0.b(str);
        e();
        q();
        try {
            boolean z3 = true;
            cursor = v().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", "app_version", "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count", "daily_realtime_events_count", "health_monitor_sample", "android_id", "adid_reporting_enabled", "ssaid_reporting_enabled", "admob_app_id"}, "app_id=?", new String[]{str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    ql1 ql1 = new ql1(this.b.B(), str2);
                    ql1.b(cursor.getString(0));
                    ql1.c(cursor.getString(1));
                    ql1.e(cursor.getString(2));
                    ql1.i(cursor.getLong(3));
                    ql1.d(cursor.getLong(4));
                    ql1.e(cursor.getLong(5));
                    ql1.a(cursor.getString(6));
                    ql1.g(cursor.getString(7));
                    ql1.g(cursor.getLong(8));
                    ql1.h(cursor.getLong(9));
                    if (!cursor.isNull(10)) {
                        if (cursor.getInt(10) == 0) {
                            z = false;
                            ql1.a(z);
                            ql1.l(cursor.getLong(11));
                            ql1.m(cursor.getLong(12));
                            ql1.n(cursor.getLong(13));
                            ql1.o(cursor.getLong(14));
                            ql1.j(cursor.getLong(15));
                            ql1.k(cursor.getLong(16));
                            ql1.f(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                            ql1.f(cursor.getString(18));
                            ql1.b(cursor.getLong(19));
                            ql1.a(cursor.getLong(20));
                            ql1.h(cursor.getString(21));
                            ql1.c(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                            if (!cursor.isNull(23)) {
                                if (cursor.getInt(23) == 0) {
                                    z2 = false;
                                    ql1.b(z2);
                                    if (!cursor.isNull(24)) {
                                        if (cursor.getInt(24) == 0) {
                                            z3 = false;
                                        }
                                    }
                                    ql1.c(z3);
                                    ql1.d(cursor.getString(25));
                                    ql1.g();
                                    if (cursor.moveToNext()) {
                                        d().s().a("Got multiple records for app, expected one. appId", tg1.a(str));
                                    }
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return ql1;
                                }
                            }
                            z2 = true;
                            ql1.b(z2);
                            if (!cursor.isNull(24)) {
                            }
                            ql1.c(z3);
                            ql1.d(cursor.getString(25));
                            ql1.g();
                            if (cursor.moveToNext()) {
                            }
                            if (cursor != null) {
                            }
                            return ql1;
                        }
                    }
                    z = true;
                    ql1.a(z);
                    ql1.l(cursor.getLong(11));
                    ql1.m(cursor.getLong(12));
                    ql1.n(cursor.getLong(13));
                    ql1.o(cursor.getLong(14));
                    ql1.j(cursor.getLong(15));
                    ql1.k(cursor.getLong(16));
                    ql1.f(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                    ql1.f(cursor.getString(18));
                    ql1.b(cursor.getLong(19));
                    ql1.a(cursor.getLong(20));
                    ql1.h(cursor.getString(21));
                    ql1.c(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                    if (!cursor.isNull(23)) {
                    }
                    z2 = true;
                    ql1.b(z2);
                    if (!cursor.isNull(24)) {
                    }
                    ql1.c(z3);
                    ql1.d(cursor.getString(25));
                    ql1.g();
                    if (cursor.moveToNext()) {
                    }
                    if (cursor != null) {
                    }
                    return ql1;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        d().s().a("Error querying app. appId", tg1.a(str), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                d().s().a("Error querying app. appId", tg1.a(str), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error querying app. appId", tg1.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f9, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0100, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0101, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0104, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0105, code lost:
        r12 = r21;
        r11 = r22;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0100 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0127  */
    public final List<ml1> a(String str, String str2, String str3) {
        Cursor cursor;
        String str4;
        bk0.b(str);
        e();
        q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor2 = null;
        try {
            ArrayList arrayList2 = new ArrayList(3);
            arrayList2.add(str);
            StringBuilder sb = new StringBuilder("app_id=?");
            if (!TextUtils.isEmpty(str2)) {
                str4 = str2;
                arrayList2.add(str4);
                sb.append(" and origin=?");
            } else {
                str4 = str2;
            }
            if (!TextUtils.isEmpty(str3)) {
                arrayList2.add(String.valueOf(str3).concat(Marker.ANY_MARKER));
                sb.append(" and name glob ?");
            }
            cursor = v().query("user_attributes", new String[]{"name", "set_timestamp", "value", "origin"}, sb.toString(), (String[]) arrayList2.toArray(new String[arrayList2.size()]), (String) null, (String) null, "rowid", "1001");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() >= 1000) {
                        d().s().a("Read more than the max allowed user properties, ignoring excess", 1000);
                        break;
                    }
                    String string = cursor.getString(0);
                    long j2 = cursor.getLong(1);
                    try {
                        Object a = a(cursor, 2);
                        String string2 = cursor.getString(3);
                        if (a == null) {
                            try {
                                d().s().a("(2)Read invalid user property value, ignoring it", tg1.a(str), string2, str3);
                            } catch (SQLiteException e2) {
                                e = e2;
                                str4 = string2;
                                try {
                                    d().s().a("(2)Error querying user properties", tg1.a(str), str4, e);
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return null;
                                } catch (Throwable th) {
                                    th = th;
                                    cursor2 = cursor;
                                    if (cursor2 != null) {
                                        cursor2.close();
                                    }
                                    throw th;
                                }
                            }
                        } else {
                            String str5 = str3;
                            arrayList.add(new ml1(str, string2, string, j2, a));
                        }
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        str4 = string2;
                    } catch (SQLiteException e3) {
                        e = e3;
                        d().s().a("(2)Error querying user properties", tg1.a(str), str4, e);
                        if (cursor != null) {
                        }
                        return null;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e4) {
                e = e4;
                d().s().a("(2)Error querying user properties", tg1.a(str), str4, e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            e = e5;
            str4 = str2;
            cursor = null;
            d().s().a("(2)Error querying user properties", tg1.a(str), str4, e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
        }
    }

    @DexIgnore
    public final boolean a(vl1 vl1) {
        bk0.a(vl1);
        e();
        q();
        if (d(vl1.e, vl1.g.f) == null) {
            if (a("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{vl1.e}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", vl1.e);
        contentValues.put("origin", vl1.f);
        contentValues.put("name", vl1.g.f);
        a(contentValues, "value", vl1.g.H());
        contentValues.put("active", Boolean.valueOf(vl1.i));
        contentValues.put("trigger_event_name", vl1.j);
        contentValues.put("trigger_timeout", Long.valueOf(vl1.l));
        j();
        contentValues.put("timed_out_event", nl1.a((Parcelable) vl1.k));
        contentValues.put("creation_timestamp", Long.valueOf(vl1.h));
        j();
        contentValues.put("triggered_event", nl1.a((Parcelable) vl1.m));
        contentValues.put("triggered_timestamp", Long.valueOf(vl1.g.g));
        contentValues.put("time_to_live", Long.valueOf(vl1.n));
        j();
        contentValues.put("expired_event", nl1.a((Parcelable) vl1.o));
        try {
            if (v().insertWithOnConflict("conditional_properties", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update conditional user property (got -1)", tg1.a(vl1.e));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing conditional user property", tg1.a(vl1.e), e2);
        }
        return true;
    }

    @DexIgnore
    public final void a(ql1 ql1) {
        bk0.a(ql1);
        e();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", ql1.f());
        contentValues.put("app_instance_id", ql1.a());
        contentValues.put("gmp_app_id", ql1.c());
        contentValues.put("resettable_device_id_hash", ql1.i());
        contentValues.put("last_bundle_index", Long.valueOf(ql1.p()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(ql1.j()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(ql1.k()));
        contentValues.put("app_version", ql1.e());
        contentValues.put("app_store", ql1.m());
        contentValues.put("gmp_version", Long.valueOf(ql1.n()));
        contentValues.put("dev_cert_hash", Long.valueOf(ql1.o()));
        contentValues.put("measurement_enabled", Boolean.valueOf(ql1.d()));
        contentValues.put("day", Long.valueOf(ql1.t()));
        contentValues.put("daily_public_events_count", Long.valueOf(ql1.u()));
        contentValues.put("daily_events_count", Long.valueOf(ql1.v()));
        contentValues.put("daily_conversions_count", Long.valueOf(ql1.w()));
        contentValues.put("config_fetched_time", Long.valueOf(ql1.q()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(ql1.r()));
        contentValues.put("app_version_int", Long.valueOf(ql1.l()));
        contentValues.put("firebase_instance_id", ql1.b());
        contentValues.put("daily_error_events_count", Long.valueOf(ql1.y()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(ql1.x()));
        contentValues.put("health_monitor_sample", ql1.z());
        contentValues.put("android_id", Long.valueOf(ql1.B()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(ql1.C()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(ql1.D()));
        contentValues.put("admob_app_id", ql1.h());
        try {
            SQLiteDatabase v = v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{ql1.f()})) == 0 && v.insertWithOnConflict("apps", (String) null, contentValues, 5) == -1) {
                d().s().a("Failed to insert/update app (got -1). appId", tg1.a(ql1.f()));
            }
        } catch (SQLiteException e2) {
            d().s().a("Error storing app. appId", tg1.a(ql1.f()), e2);
        }
    }

    @DexIgnore
    public final bm1 a(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        bk0.b(str);
        e();
        q();
        String[] strArr = {str};
        bm1 bm1 = new bm1();
        Cursor cursor = null;
        try {
            SQLiteDatabase v = v();
            cursor = v.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            if (!cursor.moveToFirst()) {
                d().v().a("Not updating daily counts, app is not known. appId", tg1.a(str));
                if (cursor != null) {
                    cursor.close();
                }
                return bm1;
            }
            if (cursor.getLong(0) == j2) {
                bm1.b = cursor.getLong(1);
                bm1.a = cursor.getLong(2);
                bm1.c = cursor.getLong(3);
                bm1.d = cursor.getLong(4);
                bm1.e = cursor.getLong(5);
            }
            if (z) {
                bm1.b++;
            }
            if (z2) {
                bm1.a++;
            }
            if (z3) {
                bm1.c++;
            }
            if (z4) {
                bm1.d++;
            }
            if (z5) {
                bm1.e++;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j2));
            contentValues.put("daily_public_events_count", Long.valueOf(bm1.a));
            contentValues.put("daily_events_count", Long.valueOf(bm1.b));
            contentValues.put("daily_conversions_count", Long.valueOf(bm1.c));
            contentValues.put("daily_error_events_count", Long.valueOf(bm1.d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(bm1.e));
            v.update("apps", contentValues, "app_id=?", strArr);
            if (cursor != null) {
                cursor.close();
            }
            return bm1;
        } catch (SQLiteException e2) {
            d().s().a("Error updating daily counts. appId", tg1.a(str), e2);
            if (cursor != null) {
                cursor.close();
            }
            return bm1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(f61 f61, boolean z) {
        e();
        q();
        bk0.a(f61);
        bk0.b(f61.q);
        bk0.a(f61.h);
        z();
        long b = c().b();
        if (f61.h.longValue() < b - xl1.t() || f61.h.longValue() > xl1.t() + b) {
            d().v().a("Storing bundle outside of the max uploading time span. appId, now, timestamp", tg1.a(f61.q), Long.valueOf(b), f61.h);
        }
        try {
            byte[] bArr = new byte[f61.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            f61.a(a);
            a.b();
            byte[] b2 = m().b(bArr);
            d().A().a("Saving bundle, size", Integer.valueOf(b2.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", f61.q);
            contentValues.put("bundle_end_timestamp", f61.h);
            contentValues.put("data", b2);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            Integer num = f61.M;
            if (num != null) {
                contentValues.put("retry_count", num);
            }
            try {
                if (v().insert("queue", (String) null, contentValues) != -1) {
                    return true;
                }
                d().s().a("Failed to insert bundle (got -1). appId", tg1.a(f61.q));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing bundle. appId", tg1.a(f61.q), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize bundle. appId", tg1.a(f61.q), e3);
            return false;
        }
    }

    @DexIgnore
    public final List<Pair<f61, Long>> a(String str, int i2, int i3) {
        e();
        q();
        bk0.a(i2 > 0);
        bk0.a(i3 > 0);
        bk0.b(str);
        Cursor cursor = null;
        try {
            cursor = v().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", String.valueOf(i2));
            if (!cursor.moveToFirst()) {
                List<Pair<f61, Long>> emptyList = Collections.emptyList();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            do {
                long j2 = cursor.getLong(0);
                try {
                    byte[] a = m().a(cursor.getBlob(1));
                    if (!arrayList.isEmpty() && a.length + i4 > i3) {
                        break;
                    }
                    tb1 a2 = tb1.a(a, 0, a.length);
                    f61 f61 = new f61();
                    try {
                        f61.a(a2);
                        if (!cursor.isNull(2)) {
                            f61.M = Integer.valueOf(cursor.getInt(2));
                        }
                        i4 += a.length;
                        arrayList.add(Pair.create(f61, Long.valueOf(j2)));
                    } catch (IOException e2) {
                        d().s().a("Failed to merge queued bundle. appId", tg1.a(str), e2);
                    }
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } catch (IOException e3) {
                    d().s().a("Failed to unzip queued bundle. appId", tg1.a(str), e3);
                }
            } while (i4 <= i3);
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e4) {
            d().s().a("Error querying bundles. appId", tg1.a(str), e4);
            List<Pair<f61, Long>> emptyList2 = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(List<Long> list) {
        e();
        q();
        bk0.a(list);
        bk0.a(list.size());
        if (F()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (a(sb3.toString(), (String[]) null) > 0) {
                d().v().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                d().s().a("Error incrementing retry count. error", e2);
            }
        }
    }

    @DexIgnore
    public final void a(String str, r51[] r51Arr) {
        boolean z;
        String str2 = str;
        r51[] r51Arr2 = r51Arr;
        q();
        e();
        bk0.b(str);
        bk0.a(r51Arr);
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            q();
            e();
            bk0.b(str);
            SQLiteDatabase v2 = v();
            v2.delete("property_filters", "app_id=?", new String[]{str2});
            v2.delete("event_filters", "app_id=?", new String[]{str2});
            for (r51 r51 : r51Arr2) {
                q();
                e();
                bk0.b(str);
                bk0.a(r51);
                bk0.a(r51.e);
                bk0.a(r51.d);
                if (r51.c == null) {
                    d().v().a("Audience with no ID. appId", tg1.a(str));
                } else {
                    int intValue = r51.c.intValue();
                    s51[] s51Arr = r51.e;
                    int length = s51Arr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            v51[] v51Arr = r51.d;
                            int length2 = v51Arr.length;
                            int i3 = 0;
                            while (true) {
                                if (i3 >= length2) {
                                    s51[] s51Arr2 = r51.e;
                                    int length3 = s51Arr2.length;
                                    int i4 = 0;
                                    while (true) {
                                        if (i4 >= length3) {
                                            z = true;
                                            break;
                                        } else if (!a(str2, intValue, s51Arr2[i4])) {
                                            z = false;
                                            break;
                                        } else {
                                            i4++;
                                        }
                                    }
                                    if (z) {
                                        v51[] v51Arr2 = r51.d;
                                        int length4 = v51Arr2.length;
                                        int i5 = 0;
                                        while (true) {
                                            if (i5 >= length4) {
                                                break;
                                            } else if (!a(str2, intValue, v51Arr2[i5])) {
                                                z = false;
                                                break;
                                            } else {
                                                i5++;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        q();
                                        e();
                                        bk0.b(str);
                                        SQLiteDatabase v3 = v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(intValue)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(intValue)});
                                    }
                                } else if (v51Arr[i3].c == null) {
                                    d().v().a("Property filter with no ID. Audience definition ignored. appId, audienceId", tg1.a(str), r51.c);
                                    break;
                                } else {
                                    i3++;
                                }
                            }
                        } else if (s51Arr[i2].c == null) {
                            d().v().a("Event filter with no ID. Audience definition ignored. appId, audienceId", tg1.a(str), r51.c);
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (r51 r512 : r51Arr2) {
                arrayList.add(r512.c);
            }
            a(str2, (List<Integer>) arrayList);
            v.setTransactionSuccessful();
        } finally {
            v.endTransaction();
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, s51 s51) {
        q();
        e();
        bk0.b(str);
        bk0.a(s51);
        if (TextUtils.isEmpty(s51.d)) {
            d().v().a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", tg1.a(str), Integer.valueOf(i2), String.valueOf(s51.c));
            return false;
        }
        try {
            byte[] bArr = new byte[s51.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            s51.a(a);
            a.b();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", s51.c);
            contentValues.put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, s51.d);
            contentValues.put("data", bArr);
            try {
                if (v().insertWithOnConflict("event_filters", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert event filter (got -1). appId", tg1.a(str));
                return true;
            } catch (SQLiteException e2) {
                d().s().a("Error storing event filter. appId", tg1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Configuration loss. Failed to serialize event filter. appId", tg1.a(str), e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, v51 v51) {
        q();
        e();
        bk0.b(str);
        bk0.a(v51);
        if (TextUtils.isEmpty(v51.d)) {
            d().v().a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", tg1.a(str), Integer.valueOf(i2), String.valueOf(v51.c));
            return false;
        }
        try {
            byte[] bArr = new byte[v51.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            v51.a(a);
            a.b();
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put("audience_id", Integer.valueOf(i2));
            contentValues.put("filter_id", v51.c);
            contentValues.put("property_name", v51.d);
            contentValues.put("data", bArr);
            try {
                if (v().insertWithOnConflict("property_filters", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert property filter (got -1). appId", tg1.a(str));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing property filter. appId", tg1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Configuration loss. Failed to serialize property filter. appId", tg1.a(str), e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, List<Integer> list) {
        bk0.b(str);
        q();
        e();
        SQLiteDatabase v = v();
        try {
            long a = a("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION, l().b(str, jg1.P)));
            if (a <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            d().s().a("Database error querying filters. appId", tg1.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public static void a(ContentValues contentValues, String str, Object obj) {
        bk0.b(str);
        bk0.a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @DexIgnore
    public final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            d().s().a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                d().s().a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            d().s().a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    @DexIgnore
    public final long a(f61 f61) throws IOException {
        long j2;
        e();
        q();
        bk0.a(f61);
        bk0.b(f61.q);
        try {
            byte[] bArr = new byte[f61.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            f61.a(a);
            a.b();
            jl1 m = m();
            bk0.a(bArr);
            m.j().e();
            MessageDigest w = nl1.w();
            if (w == null) {
                m.d().s().a("Failed to get MD5");
                j2 = 0;
            } else {
                j2 = nl1.a(w.digest(bArr));
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", f61.q);
            contentValues.put("metadata_fingerprint", Long.valueOf(j2));
            contentValues.put("metadata", bArr);
            try {
                v().insertWithOnConflict("raw_events_metadata", (String) null, contentValues, 4);
                return j2;
            } catch (SQLiteException e2) {
                d().s().a("Error storing raw event metadata. appId", tg1.a(f61.q), e2);
                throw e2;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event metadata. appId", tg1.a(f61.q), e3);
            throw e3;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    public final String a(long j2) {
        Cursor cursor;
        e();
        q();
        try {
            cursor = v().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j2)});
            try {
                if (!cursor.moveToFirst()) {
                    d().A().a("No expired configs for apps with pending events");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                if (cursor != null) {
                    cursor.close();
                }
                return string;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    d().s().a("Error selecting expired configs", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            d().s().a("Error selecting expired configs", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008f  */
    public final Pair<c61, Long> a(String str, Long l) {
        Cursor cursor;
        e();
        q();
        try {
            cursor = v().rawQuery("select main_event, children_to_process from main_event_params where app_id=? and event_id=?", new String[]{str, String.valueOf(l)});
            try {
                if (!cursor.moveToFirst()) {
                    d().A().a("Main event not found");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                Long valueOf = Long.valueOf(cursor.getLong(1));
                tb1 a = tb1.a(blob, 0, blob.length);
                c61 c61 = new c61();
                try {
                    c61.a(a);
                    Pair<c61, Long> create = Pair.create(c61, valueOf);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return create;
                } catch (IOException e2) {
                    d().s().a("Failed to merge main event. appId, eventId", tg1.a(str), l, e2);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    d().s().a("Error selecting main event", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            d().s().a("Error selecting main event", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(String str, Long l, long j2, c61 c61) {
        e();
        q();
        bk0.a(c61);
        bk0.b(str);
        bk0.a(l);
        try {
            byte[] bArr = new byte[c61.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            c61.a(a);
            a.b();
            d().A().a("Saving complex main event, appId, data size", i().a(str), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", str);
            contentValues.put(LogBuilder.KEY_EVENT_ID, l);
            contentValues.put("children_to_process", Long.valueOf(j2));
            contentValues.put("main_event", bArr);
            try {
                if (v().insertWithOnConflict("main_event_params", (String) null, contentValues, 5) != -1) {
                    return true;
                }
                d().s().a("Failed to insert complex main event (got -1). appId", tg1.a(str));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing complex main event. appId", tg1.a(str), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event params/data. appId, eventId", tg1.a(str), l, e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(cg1 cg1, long j2, boolean z) {
        e();
        q();
        bk0.a(cg1);
        bk0.b(cg1.a);
        c61 c61 = new c61();
        c61.f = Long.valueOf(cg1.e);
        c61.c = new d61[cg1.f.size()];
        Iterator<String> it = cg1.f.iterator();
        int i2 = 0;
        while (it.hasNext()) {
            String next = it.next();
            d61 d61 = new d61();
            int i3 = i2 + 1;
            c61.c[i2] = d61;
            d61.c = next;
            m().a(d61, cg1.f.e(next));
            i2 = i3;
        }
        try {
            byte[] bArr = new byte[c61.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            c61.a(a);
            a.b();
            d().A().a("Saving event, name, data size", i().a(cg1.b), Integer.valueOf(bArr.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", cg1.a);
            contentValues.put("name", cg1.b);
            contentValues.put("timestamp", Long.valueOf(cg1.d));
            contentValues.put("metadata_fingerprint", Long.valueOf(j2));
            contentValues.put("data", bArr);
            contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
            try {
                if (v().insert("raw_events", (String) null, contentValues) != -1) {
                    return true;
                }
                d().s().a("Failed to insert raw event (got -1). appId", tg1.a(cg1.a));
                return false;
            } catch (SQLiteException e2) {
                d().s().a("Error storing raw event. appId", tg1.a(cg1.a), e2);
                return false;
            }
        } catch (IOException e3) {
            d().s().a("Data loss. Failed to serialize event params/data. appId", tg1.a(cg1.a), e3);
            return false;
        }
    }
}
