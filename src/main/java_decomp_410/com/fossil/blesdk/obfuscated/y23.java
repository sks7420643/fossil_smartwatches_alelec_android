package com.fossil.blesdk.obfuscated;

import com.google.android.libraries.places.api.net.PlacesClient;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface y23 extends v52<x23> {
    @DexIgnore
    void B(String str);

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void j(List<String> list);

    @DexIgnore
    void setTitle(String str);

    @DexIgnore
    void v(String str);
}
