package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ti */
public abstract class C2967ti {
    @DexIgnore
    /* renamed from: c */
    public static <T extends com.fossil.blesdk.obfuscated.C3117vi> java.lang.Class m14247c(T t) throws java.lang.ClassNotFoundException {
        return m14245a((java.lang.Class<? extends com.fossil.blesdk.obfuscated.C3117vi>) t.getClass());
    }

    @DexIgnore
    /* renamed from: a */
    public int mo16417a(int i, int i2) {
        if (!mo16427a(i2)) {
            return i;
        }
        return mo16440e();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo16421a();

    @DexIgnore
    /* renamed from: a */
    public abstract void mo16422a(android.os.Parcelable parcelable);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo16424a(java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    public void mo16425a(boolean z, boolean z2) {
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo16426a(byte[] bArr);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo16427a(int i);

    @DexIgnore
    /* renamed from: b */
    public abstract com.fossil.blesdk.obfuscated.C2967ti mo16429b();

    @DexIgnore
    /* renamed from: b */
    public abstract void mo16430b(int i);

    @DexIgnore
    /* renamed from: b */
    public void mo16436b(byte[] bArr, int i) {
        mo16430b(i);
        mo16426a(bArr);
    }

    @DexIgnore
    /* renamed from: c */
    public abstract void mo16437c(int i);

    @DexIgnore
    /* renamed from: c */
    public boolean mo16438c() {
        return false;
    }

    @DexIgnore
    /* renamed from: d */
    public abstract byte[] mo16439d();

    @DexIgnore
    /* renamed from: e */
    public abstract int mo16440e();

    @DexIgnore
    /* renamed from: f */
    public abstract <T extends android.os.Parcelable> T mo16441f();

    @DexIgnore
    /* renamed from: g */
    public abstract java.lang.String mo16442g();

    @DexIgnore
    /* renamed from: h */
    public <T extends com.fossil.blesdk.obfuscated.C3117vi> T mo16443h() {
        java.lang.String g = mo16442g();
        if (g == null) {
            return null;
        }
        return m14244a(g, mo16429b());
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo16420a(java.lang.String str, int i) {
        if (!mo16427a(i)) {
            return str;
        }
        return mo16442g();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16431b(int i, int i2) {
        mo16430b(i2);
        mo16437c(i);
    }

    @DexIgnore
    /* renamed from: a */
    public byte[] mo16428a(byte[] bArr, int i) {
        if (!mo16427a(i)) {
            return bArr;
        }
        return mo16439d();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16435b(java.lang.String str, int i) {
        mo16430b(i);
        mo16424a(str);
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends android.os.Parcelable> T mo16418a(T t, int i) {
        if (!mo16427a(i)) {
            return t;
        }
        return mo16441f();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16432b(android.os.Parcelable parcelable, int i) {
        mo16430b(i);
        mo16422a(parcelable);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16423a(com.fossil.blesdk.obfuscated.C3117vi viVar) {
        if (viVar == null) {
            mo16424a((java.lang.String) null);
            return;
        }
        mo16433b(viVar);
        com.fossil.blesdk.obfuscated.C2967ti b = mo16429b();
        m14246a(viVar, b);
        b.mo16421a();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16434b(com.fossil.blesdk.obfuscated.C3117vi viVar, int i) {
        mo16430b(i);
        mo16423a(viVar);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo16433b(com.fossil.blesdk.obfuscated.C3117vi viVar) {
        try {
            mo16424a(m14245a((java.lang.Class<? extends com.fossil.blesdk.obfuscated.C3117vi>) viVar.getClass()).getName());
        } catch (java.lang.ClassNotFoundException e) {
            throw new java.lang.RuntimeException(viVar.getClass().getSimpleName() + " does not have a Parcelizer", e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends com.fossil.blesdk.obfuscated.C3117vi> T mo16419a(T t, int i) {
        if (!mo16427a(i)) {
            return t;
        }
        return mo16443h();
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends com.fossil.blesdk.obfuscated.C3117vi> T m14244a(java.lang.String str, com.fossil.blesdk.obfuscated.C2967ti tiVar) {
        java.lang.Class<com.fossil.blesdk.obfuscated.C2967ti> cls = com.fossil.blesdk.obfuscated.C2967ti.class;
        try {
            return (com.fossil.blesdk.obfuscated.C3117vi) java.lang.Class.forName(str, true, cls.getClassLoader()).getDeclaredMethod(com.misfit.frameworks.buttonservice.p003db.HardwareLog.COLUMN_READ, new java.lang.Class[]{cls}).invoke((java.lang.Object) null, new java.lang.Object[]{tiVar});
        } catch (java.lang.IllegalAccessException e) {
            throw new java.lang.RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            if (e2.getCause() instanceof java.lang.RuntimeException) {
                throw ((java.lang.RuntimeException) e2.getCause());
            }
            throw new java.lang.RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (java.lang.NoSuchMethodException e3) {
            throw new java.lang.RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (java.lang.ClassNotFoundException e4) {
            throw new java.lang.RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends com.fossil.blesdk.obfuscated.C3117vi> void m14246a(T t, com.fossil.blesdk.obfuscated.C2967ti tiVar) {
        try {
            m14247c(t).getDeclaredMethod("write", new java.lang.Class[]{t.getClass(), com.fossil.blesdk.obfuscated.C2967ti.class}).invoke((java.lang.Object) null, new java.lang.Object[]{t, tiVar});
        } catch (java.lang.IllegalAccessException e) {
            throw new java.lang.RuntimeException("VersionedParcel encountered IllegalAccessException", e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            if (e2.getCause() instanceof java.lang.RuntimeException) {
                throw ((java.lang.RuntimeException) e2.getCause());
            }
            throw new java.lang.RuntimeException("VersionedParcel encountered InvocationTargetException", e2);
        } catch (java.lang.NoSuchMethodException e3) {
            throw new java.lang.RuntimeException("VersionedParcel encountered NoSuchMethodException", e3);
        } catch (java.lang.ClassNotFoundException e4) {
            throw new java.lang.RuntimeException("VersionedParcel encountered ClassNotFoundException", e4);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Class m14245a(java.lang.Class<? extends com.fossil.blesdk.obfuscated.C3117vi> cls) throws java.lang.ClassNotFoundException {
        return java.lang.Class.forName(java.lang.String.format("%s.%sParcelizer", new java.lang.Object[]{cls.getPackage().getName(), cls.getSimpleName()}), false, cls.getClassLoader());
    }
}
