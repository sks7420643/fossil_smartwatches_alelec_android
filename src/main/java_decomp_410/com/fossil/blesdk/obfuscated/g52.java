package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g52 implements Factory<jn2> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public g52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static g52 a(n42 n42) {
        return new g52(n42);
    }

    @DexIgnore
    public static jn2 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static jn2 c(n42 n42) {
        jn2 j = n42.j();
        n44.a(j, "Cannot return null from a non-@Nullable @Provides method");
        return j;
    }

    @DexIgnore
    public jn2 get() {
        return b(this.a);
    }
}
