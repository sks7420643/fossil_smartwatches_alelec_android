package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lj */
public abstract class C2329lj {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f7226a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkerFactory");

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.lj$a */
    public static class C2330a extends com.fossil.blesdk.obfuscated.C2329lj {
        @DexIgnore
        /* renamed from: a */
        public androidx.work.ListenableWorker mo13297a(android.content.Context context, java.lang.String str, androidx.work.WorkerParameters workerParameters) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2329lj m10192a() {
        return new com.fossil.blesdk.obfuscated.C2329lj.C2330a();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract androidx.work.ListenableWorker mo13297a(android.content.Context context, java.lang.String str, androidx.work.WorkerParameters workerParameters);

    @DexIgnore
    /* renamed from: b */
    public final androidx.work.ListenableWorker mo13298b(android.content.Context context, java.lang.String str, androidx.work.WorkerParameters workerParameters) {
        androidx.work.ListenableWorker listenableWorker;
        androidx.work.ListenableWorker a = mo13297a(context, str, workerParameters);
        if (a == null) {
            java.lang.Class<? extends U> cls = null;
            try {
                cls = java.lang.Class.forName(str).asSubclass(androidx.work.ListenableWorker.class);
            } catch (java.lang.ClassNotFoundException unused) {
                com.fossil.blesdk.obfuscated.C1635dj a2 = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                java.lang.String str2 = f7226a;
                a2.mo9963b(str2, "Class not found: " + str, new java.lang.Throwable[0]);
            }
            if (cls != null) {
                try {
                    listenableWorker = (androidx.work.ListenableWorker) cls.getDeclaredConstructor(new java.lang.Class[]{android.content.Context.class, androidx.work.WorkerParameters.class}).newInstance(new java.lang.Object[]{context, workerParameters});
                } catch (java.lang.Exception e) {
                    com.fossil.blesdk.obfuscated.C1635dj a3 = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                    java.lang.String str3 = f7226a;
                    a3.mo9963b(str3, "Could not instantiate " + str, e);
                }
                if (listenableWorker != null || !listenableWorker.mo3752g()) {
                    return listenableWorker;
                }
                throw new java.lang.IllegalStateException(java.lang.String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", new java.lang.Object[]{getClass().getName(), str}));
            }
        }
        listenableWorker = a;
        if (listenableWorker != null) {
        }
        return listenableWorker;
    }
}
