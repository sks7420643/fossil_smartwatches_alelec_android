package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.text.PrecomputedText;
import android.text.Spannable;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.MetricAffectingSpan;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y7 implements Spannable {
    @DexIgnore
    public /* final */ Spannable e;
    @DexIgnore
    public /* final */ a f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ TextPaint a;
        @DexIgnore
        public /* final */ TextDirectionHeuristic b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ PrecomputedText.Params e; // = null;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.y7$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.y7$a$a  reason: collision with other inner class name */
        public static class C0038a {
            @DexIgnore
            public /* final */ TextPaint a;
            @DexIgnore
            public TextDirectionHeuristic b;
            @DexIgnore
            public int c;
            @DexIgnore
            public int d;

            @DexIgnore
            public C0038a(TextPaint textPaint) {
                this.a = textPaint;
                if (Build.VERSION.SDK_INT >= 23) {
                    this.c = 1;
                    this.d = 1;
                } else {
                    this.d = 0;
                    this.c = 0;
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    this.b = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.b = null;
                }
            }

            @DexIgnore
            public C0038a a(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public C0038a b(int i) {
                this.d = i;
                return this;
            }

            @DexIgnore
            public C0038a a(TextDirectionHeuristic textDirectionHeuristic) {
                this.b = textDirectionHeuristic;
                return this;
            }

            @DexIgnore
            public a a() {
                return new a(this.a, this.b, this.c, this.d);
            }
        }

        @DexIgnore
        public a(TextPaint textPaint, TextDirectionHeuristic textDirectionHeuristic, int i, int i2) {
            this.a = textPaint;
            this.b = textDirectionHeuristic;
            this.c = i;
            this.d = i2;
        }

        @DexIgnore
        public int a() {
            return this.c;
        }

        @DexIgnore
        public int b() {
            return this.d;
        }

        @DexIgnore
        public TextDirectionHeuristic c() {
            return this.b;
        }

        @DexIgnore
        public TextPaint d() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (!a(aVar)) {
                return false;
            }
            return Build.VERSION.SDK_INT < 18 || this.b == aVar.c();
        }

        @DexIgnore
        public int hashCode() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                return e8.a(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Float.valueOf(this.a.getLetterSpacing()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocales(), this.a.getTypeface(), Boolean.valueOf(this.a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 21) {
                return e8.a(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Float.valueOf(this.a.getLetterSpacing()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), Boolean.valueOf(this.a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 18) {
                return e8.a(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 17) {
                return e8.a(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else {
                return e8.a(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            sb.append("textSize=" + this.a.getTextSize());
            sb.append(", textScaleX=" + this.a.getTextScaleX());
            sb.append(", textSkewX=" + this.a.getTextSkewX());
            if (Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.a.isElegantTextHeight());
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                sb.append(", textLocale=" + this.a.getTextLocales());
            } else if (i >= 17) {
                sb.append(", textLocale=" + this.a.getTextLocale());
            }
            sb.append(", typeface=" + this.a.getTypeface());
            if (Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.b);
            sb.append(", breakStrategy=" + this.c);
            sb.append(", hyphenationFrequency=" + this.d);
            sb.append("}");
            return sb.toString();
        }

        @DexIgnore
        public boolean a(a aVar) {
            PrecomputedText.Params params = this.e;
            if (params != null) {
                return params.equals(aVar.e);
            }
            if ((Build.VERSION.SDK_INT >= 23 && (this.c != aVar.a() || this.d != aVar.b())) || this.a.getTextSize() != aVar.d().getTextSize() || this.a.getTextScaleX() != aVar.d().getTextScaleX() || this.a.getTextSkewX() != aVar.d().getTextSkewX()) {
                return false;
            }
            if ((Build.VERSION.SDK_INT >= 21 && (this.a.getLetterSpacing() != aVar.d().getLetterSpacing() || !TextUtils.equals(this.a.getFontFeatureSettings(), aVar.d().getFontFeatureSettings()))) || this.a.getFlags() != aVar.d().getFlags()) {
                return false;
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                if (!this.a.getTextLocales().equals(aVar.d().getTextLocales())) {
                    return false;
                }
            } else if (i >= 17 && !this.a.getTextLocale().equals(aVar.d().getTextLocale())) {
                return false;
            }
            if (this.a.getTypeface() == null) {
                if (aVar.d().getTypeface() != null) {
                    return false;
                }
                return true;
            } else if (!this.a.getTypeface().equals(aVar.d().getTypeface())) {
                return false;
            } else {
                return true;
            }
        }

        @DexIgnore
        public a(PrecomputedText.Params params) {
            this.a = params.getTextPaint();
            this.b = params.getTextDirection();
            this.c = params.getBreakStrategy();
            this.d = params.getHyphenationFrequency();
        }
    }

    @DexIgnore
    public a a() {
        return this.f;
    }

    @DexIgnore
    public char charAt(int i) {
        return this.e.charAt(i);
    }

    @DexIgnore
    public int getSpanEnd(Object obj) {
        return this.e.getSpanEnd(obj);
    }

    @DexIgnore
    public int getSpanFlags(Object obj) {
        return this.e.getSpanFlags(obj);
    }

    @DexIgnore
    public int getSpanStart(Object obj) {
        return this.e.getSpanStart(obj);
    }

    @DexIgnore
    public <T> T[] getSpans(int i, int i2, Class<T> cls) {
        return this.e.getSpans(i, i2, cls);
    }

    @DexIgnore
    public int length() {
        return this.e.length();
    }

    @DexIgnore
    public int nextSpanTransition(int i, int i2, Class cls) {
        return this.e.nextSpanTransition(i, i2, cls);
    }

    @DexIgnore
    public void removeSpan(Object obj) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.e.removeSpan(obj);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
    }

    @DexIgnore
    public void setSpan(Object obj, int i, int i2, int i3) {
        if (!(obj instanceof MetricAffectingSpan)) {
            this.e.setSpan(obj, i, i2, i3);
            return;
        }
        throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
    }

    @DexIgnore
    public CharSequence subSequence(int i, int i2) {
        return this.e.subSequence(i, i2);
    }

    @DexIgnore
    public String toString() {
        return this.e.toString();
    }
}
