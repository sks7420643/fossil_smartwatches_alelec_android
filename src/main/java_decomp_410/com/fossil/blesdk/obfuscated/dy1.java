package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dy1 extends BroadcastReceiver {
    @DexIgnore
    public cy1 a;

    @DexIgnore
    public dy1(cy1 cy1) {
        this.a = cy1;
    }

    @DexIgnore
    public final void a() {
        if (FirebaseInstanceId.o()) {
            Log.d("FirebaseInstanceId", "Connectivity change received registered");
        }
        this.a.a().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        cy1 cy1 = this.a;
        if (cy1 != null && cy1.c()) {
            if (FirebaseInstanceId.o()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.a((Runnable) this.a, 0);
            this.a.a().unregisterReceiver(this);
            this.a = null;
        }
    }
}
