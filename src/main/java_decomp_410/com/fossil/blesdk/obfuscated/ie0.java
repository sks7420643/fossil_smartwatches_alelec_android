package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ie0 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends me0> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public a(ge0 ge0, R r) {
            super(ge0);
            this.q = r;
        }

        @DexIgnore
        public final R a(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static he0<Status> a(Status status, ge0 ge0) {
        bk0.a(status, (Object) "Result must not be null");
        ef0 ef0 = new ef0(ge0);
        ef0.a(status);
        return ef0;
    }

    @DexIgnore
    public static <R extends me0> he0<R> a(R r, ge0 ge0) {
        bk0.a(r, (Object) "Result must not be null");
        bk0.a(!r.G().L(), (Object) "Status code must not be SUCCESS");
        a aVar = new a(ge0, r);
        aVar.a(r);
        return aVar;
    }
}
