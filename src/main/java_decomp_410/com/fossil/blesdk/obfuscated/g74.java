package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface g74 {
    @DexIgnore
    boolean a(SharedPreferences.Editor editor);

    @DexIgnore
    SharedPreferences.Editor edit();

    @DexIgnore
    SharedPreferences get();
}
