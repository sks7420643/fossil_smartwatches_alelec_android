package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ra extends na {
    @DexIgnore
    public Set<Class<? extends na>> a; // = new HashSet();
    @DexIgnore
    public List<na> b; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<String> c; // = new CopyOnWriteArrayList();

    @DexIgnore
    public void a(na naVar) {
        if (this.a.add(naVar.getClass())) {
            this.b.add(naVar);
            for (na a2 : naVar.a()) {
                a(a2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        boolean z = false;
        for (String next : this.c) {
            try {
                Class<?> cls = Class.forName(next);
                if (na.class.isAssignableFrom(cls)) {
                    a((na) cls.newInstance());
                    this.c.remove(next);
                    z = true;
                }
            } catch (ClassNotFoundException unused) {
            } catch (IllegalAccessException e) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e);
            } catch (InstantiationException e2) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e2);
            }
        }
        return z;
    }

    @DexIgnore
    public ViewDataBinding a(pa paVar, View view, int i) {
        for (na a2 : this.b) {
            ViewDataBinding a3 = a2.a(paVar, view, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(paVar, view, i);
        }
        return null;
    }

    @DexIgnore
    public ViewDataBinding a(pa paVar, View[] viewArr, int i) {
        for (na a2 : this.b) {
            ViewDataBinding a3 = a2.a(paVar, viewArr, i);
            if (a3 != null) {
                return a3;
            }
        }
        if (b()) {
            return a(paVar, viewArr, i);
        }
        return null;
    }
}
