package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hw */
public final class C1977hw {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.concurrent.ConcurrentMap<java.lang.String, com.fossil.blesdk.obfuscated.C2143jo> f5842a; // = new java.util.concurrent.ConcurrentHashMap();

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m8007a(android.content.pm.PackageInfo packageInfo) {
        if (packageInfo != null) {
            return java.lang.String.valueOf(packageInfo.versionCode);
        }
        return java.util.UUID.randomUUID().toString();
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2143jo m8008b(android.content.Context context) {
        java.lang.String packageName = context.getPackageName();
        com.fossil.blesdk.obfuscated.C2143jo joVar = (com.fossil.blesdk.obfuscated.C2143jo) f5842a.get(packageName);
        if (joVar != null) {
            return joVar;
        }
        com.fossil.blesdk.obfuscated.C2143jo c = m8009c(context);
        com.fossil.blesdk.obfuscated.C2143jo putIfAbsent = f5842a.putIfAbsent(packageName, c);
        return putIfAbsent == null ? c : putIfAbsent;
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C2143jo m8009c(android.content.Context context) {
        return new com.fossil.blesdk.obfuscated.C2166jw(m8007a(m8006a(context)));
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.pm.PackageInfo m8006a(android.content.Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            android.util.Log.e("AppVersionSignature", "Cannot resolve info for" + context.getPackageName(), e);
            return null;
        }
    }
}
