package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class on {
    @DexIgnore
    public static tm a(Context context, zm zmVar) {
        an anVar;
        String str;
        if (zmVar != null) {
            anVar = new an(zmVar);
        } else if (Build.VERSION.SDK_INT >= 9) {
            anVar = new an((zm) new hn());
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + ZendeskConfig.SLASH + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException unused) {
                str = "volley/0";
            }
            anVar = new an((gn) new dn(AndroidHttpClient.newInstance(str)));
        }
        return a(context, (qm) anVar);
    }

    @DexIgnore
    public static tm a(Context context, qm qmVar) {
        tm tmVar = new tm(new cn(new File(context.getCacheDir(), "volley")), qmVar);
        tmVar.b();
        return tmVar;
    }

    @DexIgnore
    public static tm a(Context context) {
        return a(context, (zm) null);
    }
}
