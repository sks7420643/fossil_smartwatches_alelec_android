package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.blesdk.obfuscated.id;
import com.fossil.blesdk.obfuscated.le;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class rd<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ id<T> a;
    @DexIgnore
    public /* final */ id.c<T> b; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements id.c<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(qd<T> qdVar, qd<T> qdVar2) {
            rd.this.a(qdVar2);
            rd.this.a(qdVar, qdVar2);
        }
    }

    @DexIgnore
    public rd(le.d<T> dVar) {
        this.a = new id<>(this, dVar);
        this.a.a(this.b);
    }

    @DexIgnore
    public T a(int i) {
        return this.a.a(i);
    }

    @DexIgnore
    @Deprecated
    public void a(qd<T> qdVar) {
    }

    @DexIgnore
    public void a(qd<T> qdVar, qd<T> qdVar2) {
    }

    @DexIgnore
    public void b(qd<T> qdVar) {
        this.a.a(qdVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.a();
    }
}
