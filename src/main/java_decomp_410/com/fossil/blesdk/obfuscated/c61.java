package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c61 extends vb1<c61> {
    @DexIgnore
    public static volatile c61[] h;
    @DexIgnore
    public d61[] c; // = d61.e();
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Long e; // = null;
    @DexIgnore
    public Long f; // = null;
    @DexIgnore
    public Integer g; // = null;

    @DexIgnore
    public c61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static c61[] e() {
        if (h == null) {
            synchronized (zb1.b) {
                if (h == null) {
                    h = new c61[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        d61[] d61Arr = this.c;
        if (d61Arr != null && d61Arr.length > 0) {
            int i = 0;
            while (true) {
                d61[] d61Arr2 = this.c;
                if (i >= d61Arr2.length) {
                    break;
                }
                d61 d61 = d61Arr2[i];
                if (d61 != null) {
                    ub1.a(1, (ac1) d61);
                }
                i++;
            }
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        Long l = this.e;
        if (l != null) {
            ub1.b(3, l.longValue());
        }
        Long l2 = this.f;
        if (l2 != null) {
            ub1.b(4, l2.longValue());
        }
        Integer num = this.g;
        if (num != null) {
            ub1.b(5, num.intValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c61)) {
            return false;
        }
        c61 c61 = (c61) obj;
        if (!zb1.a((Object[]) this.c, (Object[]) c61.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (c61.d != null) {
                return false;
            }
        } else if (!str.equals(c61.d)) {
            return false;
        }
        Long l = this.e;
        if (l == null) {
            if (c61.e != null) {
                return false;
            }
        } else if (!l.equals(c61.e)) {
            return false;
        }
        Long l2 = this.f;
        if (l2 == null) {
            if (c61.f != null) {
                return false;
            }
        } else if (!l2.equals(c61.f)) {
            return false;
        }
        Integer num = this.g;
        if (num == null) {
            if (c61.g != null) {
                return false;
            }
        } else if (!num.equals(c61.g)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(c61.b);
        }
        xb1 xb12 = c61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((c61.class.getName().hashCode() + 527) * 31) + zb1.a((Object[]) this.c)) * 31;
        String str = this.d;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Long l = this.e;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        Long l2 = this.f;
        int hashCode4 = (hashCode3 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Integer num = this.g;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        d61[] d61Arr = this.c;
        if (d61Arr != null && d61Arr.length > 0) {
            int i = 0;
            while (true) {
                d61[] d61Arr2 = this.c;
                if (i >= d61Arr2.length) {
                    break;
                }
                d61 d61 = d61Arr2[i];
                if (d61 != null) {
                    a += ub1.b(1, (ac1) d61);
                }
                i++;
            }
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        Long l = this.e;
        if (l != null) {
            a += ub1.c(3, l.longValue());
        }
        Long l2 = this.f;
        if (l2 != null) {
            a += ub1.c(4, l2.longValue());
        }
        Integer num = this.g;
        return num != null ? a + ub1.c(5, num.intValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                int a = dc1.a(tb1, 10);
                d61[] d61Arr = this.c;
                int length = d61Arr == null ? 0 : d61Arr.length;
                d61[] d61Arr2 = new d61[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, d61Arr2, 0, length);
                }
                while (length < d61Arr2.length - 1) {
                    d61Arr2[length] = new d61();
                    tb1.a((ac1) d61Arr2[length]);
                    tb1.c();
                    length++;
                }
                d61Arr2[length] = new d61();
                tb1.a((ac1) d61Arr2[length]);
                this.c = d61Arr2;
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 24) {
                this.e = Long.valueOf(tb1.f());
            } else if (c2 == 32) {
                this.f = Long.valueOf(tb1.f());
            } else if (c2 == 40) {
                this.g = Integer.valueOf(tb1.e());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
