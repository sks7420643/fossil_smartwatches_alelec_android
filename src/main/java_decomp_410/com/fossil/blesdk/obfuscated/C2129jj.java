package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jj */
public abstract class C2129jj {
    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2129jj m9016a() {
        com.fossil.blesdk.obfuscated.C2968tj a = com.fossil.blesdk.obfuscated.C2968tj.m14287a();
        if (a != null) {
            return a;
        }
        throw new java.lang.IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1804fj mo12365a(java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy, java.util.List<com.fossil.blesdk.obfuscated.C1733ej> list);

    @DexIgnore
    /* renamed from: a */
    public static void m9017a(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar) {
        com.fossil.blesdk.obfuscated.C2968tj.m14289a(context, xiVar);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1804fj mo12364a(java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy, com.fossil.blesdk.obfuscated.C1733ej ejVar) {
        return mo12365a(str, existingWorkPolicy, (java.util.List<com.fossil.blesdk.obfuscated.C1733ej>) java.util.Collections.singletonList(ejVar));
    }
}
