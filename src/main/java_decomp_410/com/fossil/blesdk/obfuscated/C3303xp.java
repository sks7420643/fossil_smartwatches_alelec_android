package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xp */
public final class C3303xp {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C2819rp<?>> f11039a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C2819rp<?>> f11040b; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2819rp<?> mo17842a(com.fossil.blesdk.obfuscated.C2143jo joVar, boolean z) {
        return mo17843a(z).get(joVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17845b(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2819rp<?> rpVar) {
        java.util.Map<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C2819rp<?>> a = mo17843a(rpVar.mo15694g());
        if (rpVar.equals(a.get(joVar))) {
            a.remove(joVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17844a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2819rp<?> rpVar) {
        mo17843a(rpVar.mo15694g()).put(joVar, rpVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final java.util.Map<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C2819rp<?>> mo17843a(boolean z) {
        return z ? this.f11040b : this.f11039a;
    }
}
