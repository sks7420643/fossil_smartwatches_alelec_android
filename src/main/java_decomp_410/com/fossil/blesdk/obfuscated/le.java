package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class le {
    @DexIgnore
    public static /* final */ Comparator<g> a; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<g> {
        @DexIgnore
        /* renamed from: a */
        public int compare(g gVar, g gVar2) {
            int i = gVar.a - gVar2.a;
            return i == 0 ? gVar.b - gVar2.b : i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public abstract int a();

        @DexIgnore
        public abstract boolean a(int i, int i2);

        @DexIgnore
        public abstract int b();

        @DexIgnore
        public abstract boolean b(int i, int i2);

        @DexIgnore
        public abstract Object c(int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<T> {
        @DexIgnore
        public abstract boolean areContentsTheSame(T t, T t2);

        @DexIgnore
        public abstract boolean areItemsTheSame(T t, T t2);

        @DexIgnore
        public Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public e(int i, int i2, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = z;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public f(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
    }

    @DexIgnore
    public static c a(b bVar, boolean z) {
        f fVar;
        int b2 = bVar.b();
        int a2 = bVar.a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new f(0, b2, 0, a2));
        int abs = Math.abs(b2 - a2) + b2 + a2;
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            f fVar2 = (f) arrayList2.remove(arrayList2.size() - 1);
            g a3 = a(bVar, fVar2.a, fVar2.b, fVar2.c, fVar2.d, iArr, iArr2, abs);
            if (a3 != null) {
                if (a3.c > 0) {
                    arrayList.add(a3);
                }
                a3.a += fVar2.a;
                a3.b += fVar2.c;
                if (arrayList3.isEmpty()) {
                    fVar = new f();
                } else {
                    fVar = (f) arrayList3.remove(arrayList3.size() - 1);
                }
                fVar.a = fVar2.a;
                fVar.c = fVar2.c;
                if (a3.e) {
                    fVar.b = a3.a;
                    fVar.d = a3.b;
                } else if (a3.d) {
                    fVar.b = a3.a - 1;
                    fVar.d = a3.b;
                } else {
                    fVar.b = a3.a;
                    fVar.d = a3.b - 1;
                }
                arrayList2.add(fVar);
                if (!a3.e) {
                    int i2 = a3.a;
                    int i3 = a3.c;
                    fVar2.a = i2 + i3;
                    fVar2.c = a3.b + i3;
                } else if (a3.d) {
                    int i4 = a3.a;
                    int i5 = a3.c;
                    fVar2.a = i4 + i5 + 1;
                    fVar2.c = a3.b + i5;
                } else {
                    int i6 = a3.a;
                    int i7 = a3.c;
                    fVar2.a = i6 + i7;
                    fVar2.c = a3.b + i7 + 1;
                }
                arrayList2.add(fVar2);
            } else {
                arrayList3.add(fVar2);
            }
        }
        Collections.sort(arrayList, a);
        return new c(bVar, arrayList, iArr, iArr2, z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ List<g> a;
        @DexIgnore
        public /* final */ int[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public /* final */ b d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;

        @DexIgnore
        public c(b bVar, List<g> list, int[] iArr, int[] iArr2, boolean z) {
            this.a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(this.b, 0);
            Arrays.fill(this.c, 0);
            this.d = bVar;
            this.e = bVar.b();
            this.f = bVar.a();
            this.g = z;
            a();
            b();
        }

        @DexIgnore
        public final void a() {
            g gVar = this.a.isEmpty() ? null : this.a.get(0);
            if (gVar == null || gVar.a != 0 || gVar.b != 0) {
                g gVar2 = new g();
                gVar2.a = 0;
                gVar2.b = 0;
                gVar2.d = false;
                gVar2.c = 0;
                gVar2.e = false;
                this.a.add(0, gVar2);
            }
        }

        @DexIgnore
        public final void b() {
            int i = this.e;
            int i2 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                g gVar = this.a.get(size);
                int i3 = gVar.a;
                int i4 = gVar.c;
                int i5 = i3 + i4;
                int i6 = gVar.b + i4;
                if (this.g) {
                    while (i > i5) {
                        a(i, i2, size);
                        i--;
                    }
                    while (i2 > i6) {
                        b(i, i2, size);
                        i2--;
                    }
                }
                for (int i7 = 0; i7 < gVar.c; i7++) {
                    int i8 = gVar.a + i7;
                    int i9 = gVar.b + i7;
                    int i10 = this.d.a(i8, i9) ? 1 : 2;
                    this.b[i8] = (i9 << 5) | i10;
                    this.c[i9] = (i8 << 5) | i10;
                }
                i = gVar.a;
                i2 = gVar.b;
            }
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            if (this.b[i - 1] == 0) {
                a(i, i2, i3, false);
            }
        }

        @DexIgnore
        public int a(int i) {
            if (i >= 0) {
                int[] iArr = this.b;
                if (i < iArr.length) {
                    int i2 = iArr[i];
                    if ((i2 & 31) == 0) {
                        return -1;
                    }
                    return i2 >> 5;
                }
            }
            throw new IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.b.length);
        }

        @DexIgnore
        public final boolean a(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                i2--;
                i5 = i;
                i4 = i2;
            } else {
                i5 = i - 1;
                i4 = i5;
            }
            while (i3 >= 0) {
                g gVar = this.a.get(i3);
                int i6 = gVar.a;
                int i7 = gVar.c;
                int i8 = i6 + i7;
                int i9 = gVar.b + i7;
                int i10 = 8;
                if (z) {
                    for (int i11 = i5 - 1; i11 >= i8; i11--) {
                        if (this.d.b(i11, i4)) {
                            if (!this.d.a(i11, i4)) {
                                i10 = 4;
                            }
                            this.c[i4] = (i11 << 5) | 16;
                            this.b[i11] = (i4 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                } else {
                    for (int i12 = i2 - 1; i12 >= i9; i12--) {
                        if (this.d.b(i4, i12)) {
                            if (!this.d.a(i4, i12)) {
                                i10 = 4;
                            }
                            int i13 = i - 1;
                            this.b[i13] = (i12 << 5) | 16;
                            this.c[i12] = (i13 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                }
                i5 = gVar.a;
                i2 = gVar.b;
                i3--;
            }
            return false;
        }

        @DexIgnore
        public final void b(int i, int i2, int i3) {
            if (this.c[i2 - 1] == 0) {
                a(i, i2, i3, true);
            }
        }

        @DexIgnore
        public final void b(List<e> list, ue ueVar, int i, int i2, int i3) {
            if (!this.g) {
                ueVar.c(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.b[i5] & 31;
                if (i6 == 0) {
                    ueVar.c(i + i4, 1);
                    for (e eVar : list) {
                        eVar.b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.b[i5] >> 5;
                    e a2 = a(list, i7, false);
                    ueVar.a(i + i4, a2.b - 1);
                    if (i6 == 4) {
                        ueVar.a(a2.b - 1, 1, this.d.c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i + i4, true));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public void a(ue ueVar) {
            ie ieVar;
            if (ueVar instanceof ie) {
                ieVar = (ie) ueVar;
            } else {
                ieVar = new ie(ueVar);
            }
            ArrayList arrayList = new ArrayList();
            int i = this.e;
            int i2 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                g gVar = this.a.get(size);
                int i3 = gVar.c;
                int i4 = gVar.a + i3;
                int i5 = gVar.b + i3;
                if (i4 < i) {
                    b(arrayList, ieVar, i4, i - i4, i4);
                }
                if (i5 < i2) {
                    a(arrayList, ieVar, i4, i2 - i5, i5);
                }
                for (int i6 = i3 - 1; i6 >= 0; i6--) {
                    int[] iArr = this.b;
                    int i7 = gVar.a;
                    if ((iArr[i7 + i6] & 31) == 2) {
                        ieVar.a(i7 + i6, 1, this.d.c(i7 + i6, gVar.b + i6));
                    }
                }
                i = gVar.a;
                i2 = gVar.b;
            }
            ieVar.a();
        }

        @DexIgnore
        public static e a(List<e> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                e eVar = list.get(size);
                if (eVar.a == i && eVar.c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        list.get(size).b += z ? 1 : -1;
                        size++;
                    }
                    return eVar;
                }
                size--;
            }
            return null;
        }

        @DexIgnore
        public final void a(List<e> list, ue ueVar, int i, int i2, int i3) {
            if (!this.g) {
                ueVar.b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.c[i5] & 31;
                if (i6 == 0) {
                    ueVar.b(i, 1);
                    for (e eVar : list) {
                        eVar.b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.c[i5] >> 5;
                    ueVar.a(a(list, i7, true).b, i);
                    if (i6 == 4) {
                        ueVar.a(i, 1, this.d.c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i, false));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r1[r13 - 1] < r1[r13 + r5]) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ba, code lost:
        if (r2[r13 - 1] < r2[r13 + 1]) goto L_0x00c7;
     */
    @DexIgnore
    public static g a(b bVar, int i, int i2, int i3, int i4, int[] iArr, int[] iArr2, int i5) {
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        int i10;
        int i11;
        b bVar2 = bVar;
        int[] iArr3 = iArr;
        int[] iArr4 = iArr2;
        int i12 = i2 - i;
        int i13 = i4 - i3;
        int i14 = 1;
        if (i12 < 1 || i13 < 1) {
            return null;
        }
        int i15 = i12 - i13;
        int i16 = ((i12 + i13) + 1) / 2;
        int i17 = (i5 - i16) - 1;
        int i18 = i5 + i16 + 1;
        Arrays.fill(iArr3, i17, i18, 0);
        Arrays.fill(iArr4, i17 + i15, i18 + i15, i12);
        boolean z3 = i15 % 2 != 0;
        int i19 = 0;
        while (i19 <= i16) {
            int i20 = -i19;
            int i21 = i20;
            while (i21 <= i19) {
                if (i21 != i20) {
                    if (i21 != i19) {
                        int i22 = i5 + i21;
                    }
                    i10 = iArr3[(i5 + i21) - i14] + i14;
                    z2 = true;
                    i11 = i10 - i21;
                    while (i10 < i12 && i11 < i13 && bVar2.b(i + i10, i3 + i11)) {
                        i10++;
                        i11++;
                    }
                    int i23 = i5 + i21;
                    iArr3[i23] = i10;
                    if (z3 || i21 < (i15 - i19) + 1 || i21 > (i15 + i19) - 1 || iArr3[i23] < iArr4[i23]) {
                        i21 += 2;
                        i14 = 1;
                    } else {
                        g gVar = new g();
                        gVar.a = iArr4[i23];
                        gVar.b = gVar.a - i21;
                        gVar.c = iArr3[i23] - iArr4[i23];
                        gVar.d = z2;
                        gVar.e = false;
                        return gVar;
                    }
                }
                i10 = iArr3[i5 + i21 + i14];
                z2 = false;
                i11 = i10 - i21;
                while (i10 < i12) {
                    i10++;
                    i11++;
                }
                int i232 = i5 + i21;
                iArr3[i232] = i10;
                if (z3) {
                }
                i21 += 2;
                i14 = 1;
            }
            int i24 = i20;
            while (i24 <= i19) {
                int i25 = i24 + i15;
                if (i25 != i19 + i15) {
                    if (i25 != i20 + i15) {
                        int i26 = i5 + i25;
                        i9 = 1;
                    } else {
                        i9 = 1;
                    }
                    i6 = iArr4[(i5 + i25) + i9] - i9;
                    z = true;
                    i7 = i6 - i25;
                    while (true) {
                        if (i6 > 0 && i7 > 0) {
                            i8 = i12;
                            if (!bVar2.b((i + i6) - 1, (i3 + i7) - 1)) {
                                break;
                            }
                            i6--;
                            i7--;
                            i12 = i8;
                        } else {
                            i8 = i12;
                        }
                    }
                    i8 = i12;
                    int i27 = i5 + i25;
                    iArr4[i27] = i6;
                    if (!z3 || i25 < i20 || i25 > i19 || iArr3[i27] < iArr4[i27]) {
                        i24 += 2;
                        i12 = i8;
                    } else {
                        g gVar2 = new g();
                        gVar2.a = iArr4[i27];
                        gVar2.b = gVar2.a - i25;
                        gVar2.c = iArr3[i27] - iArr4[i27];
                        gVar2.d = z;
                        gVar2.e = true;
                        return gVar2;
                    }
                } else {
                    i9 = 1;
                }
                i6 = iArr4[(i5 + i25) - i9];
                z = false;
                i7 = i6 - i25;
                while (true) {
                    if (i6 > 0) {
                        break;
                    }
                    break;
                    i6--;
                    i7--;
                    i12 = i8;
                }
                i8 = i12;
                int i272 = i5 + i25;
                iArr4[i272] = i6;
                if (!z3) {
                }
                i24 += 2;
                i12 = i8;
            }
            i19++;
            i12 = i12;
            i14 = 1;
        }
        throw new IllegalStateException("DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation.");
    }
}
