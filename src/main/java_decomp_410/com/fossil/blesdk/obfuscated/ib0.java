package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import com.facebook.places.PlaceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ib0 {
    @DexIgnore
    public static /* final */ ib0 a; // = new ib0();

    @DexIgnore
    public final boolean a(Context context) {
        Object obj = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (context != null) {
                obj = context.getSystemService("connectivity");
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) obj;
            if (connectivityManager != null) {
                Network activeNetwork = connectivityManager.getActiveNetwork();
                if (activeNetwork != null) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                    return networkCapabilities != null && networkCapabilities.hasTransport(1);
                }
            }
        }
        if (context != null) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                obj = applicationContext.getSystemService(PlaceManager.PARAM_WIFI);
            }
        }
        WifiManager wifiManager = (WifiManager) obj;
        if (wifiManager == null) {
            return false;
        }
        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
        if (!wifiManager.isWifiEnabled() || connectionInfo == null || connectionInfo.getNetworkId() == -1) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static /* synthetic */ boolean a(ib0 ib0, Context context, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return ib0.a(context, z);
    }

    @DexIgnore
    public final boolean a(Context context, boolean z) {
        NetworkInfo networkInfo = null;
        ConnectivityManager connectivityManager = (ConnectivityManager) (context != null ? context.getSystemService("connectivity") : null);
        if (Build.VERSION.SDK_INT >= 23) {
            if (connectivityManager != null) {
                Network activeNetwork = connectivityManager.getActiveNetwork();
                if (activeNetwork != null) {
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
                    return networkCapabilities != null && (z || networkCapabilities.hasCapability(18));
                }
            }
        }
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        if (!z || networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }
        return true;
    }
}
