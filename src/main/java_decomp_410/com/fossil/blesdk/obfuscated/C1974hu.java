package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hu */
public class C1974hu {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1974hu.C1975a<?, ?>> f5835a; // = new java.util.ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hu$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hu$a */
    public static final class C1975a<Z, R> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Class<Z> f5836a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.Class<R> f5837b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C1906gu<Z, R> f5838c;

        @DexIgnore
        public C1975a(java.lang.Class<Z> cls, java.lang.Class<R> cls2, com.fossil.blesdk.obfuscated.C1906gu<Z, R> guVar) {
            this.f5836a = cls;
            this.f5837b = cls2;
            this.f5838c = guVar;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo11742a(java.lang.Class<?> cls, java.lang.Class<?> cls2) {
            return this.f5836a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.f5837b);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Z, R> void mo11740a(java.lang.Class<Z> cls, java.lang.Class<R> cls2, com.fossil.blesdk.obfuscated.C1906gu<Z, R> guVar) {
        this.f5835a.add(new com.fossil.blesdk.obfuscated.C1974hu.C1975a(cls, cls2, guVar));
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized <Z, R> java.util.List<java.lang.Class<R>> mo11741b(java.lang.Class<Z> cls, java.lang.Class<R> cls2) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList.add(cls2);
            return arrayList;
        }
        for (com.fossil.blesdk.obfuscated.C1974hu.C1975a<?, ?> a : this.f5835a) {
            if (a.mo11742a(cls, cls2)) {
                arrayList.add(cls2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Z, R> com.fossil.blesdk.obfuscated.C1906gu<Z, R> mo11739a(java.lang.Class<Z> cls, java.lang.Class<R> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return com.fossil.blesdk.obfuscated.C2075iu.m8653a();
        }
        for (com.fossil.blesdk.obfuscated.C1974hu.C1975a next : this.f5835a) {
            if (next.mo11742a(cls, cls2)) {
                return next.f5838c;
            }
        }
        throw new java.lang.IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
    }
}
