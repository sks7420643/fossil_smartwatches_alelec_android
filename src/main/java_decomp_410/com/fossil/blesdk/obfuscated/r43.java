package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.ps2;
import com.fossil.blesdk.obfuscated.t62;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r43 extends zr2 implements q43, ws3.g {
    @DexIgnore
    public tr3<ig2> j;
    @DexIgnore
    public p43 k;
    @DexIgnore
    public ps2 l;
    @DexIgnore
    public t62 m;
    @DexIgnore
    public j42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements t62.b {
        @DexIgnore
        public /* final */ /* synthetic */ r43 a;

        @DexIgnore
        public b(r43 r43) {
            this.a = r43;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            kd4.b(watchApp, "watchApp");
            r43.a(this.a).a(watchApp.getWatchappId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ps2.c {
        @DexIgnore
        public /* final */ /* synthetic */ r43 a;

        @DexIgnore
        public c(r43 r43) {
            this.a = r43;
        }

        @DexIgnore
        public void a() {
            r43.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            kd4.b(category, "category");
            r43.a(this.a).a(category);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r43 e;

        @DexIgnore
        public d(r43 r43) {
            this.e = r43;
        }

        @DexIgnore
        public final void onClick(View view) {
            r43.a(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r43 e;

        @DexIgnore
        public e(r43 r43) {
            this.e = r43;
        }

        @DexIgnore
        public final void onClick(View view) {
            r43.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ r43 e;

        @DexIgnore
        public f(r43 r43) {
            this.e = r43;
        }

        @DexIgnore
        public final void onClick(View view) {
            r43.a(this.e).j();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ p43 a(r43 r43) {
        p43 p43 = r43.k;
        if (p43 != null) {
            return p43;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        ds3 ds3 = ds3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager, "childFragmentManager");
                        ds3.u(childFragmentManager);
                        return;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        ds3 ds32 = ds3.c;
                        FragmentManager childFragmentManager2 = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager2, "childFragmentManager");
                        ds32.A(childFragmentManager2);
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ds3 ds33 = ds3.c;
                        FragmentManager childFragmentManager3 = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager3, "childFragmentManager");
                        ds33.B(childFragmentManager3);
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        ds3 ds34 = ds3.c;
                        FragmentManager childFragmentManager4 = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager4, "childFragmentManager");
                        ds34.t(childFragmentManager4);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void E(String str) {
        kd4.b(str, "content");
        tr3<ig2> tr3 = this.j;
        if (tr3 != null) {
            ig2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                kd4.a((Object) flexibleTextView, "it.tvWatchappsDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void M(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        WeatherSettingActivity.C.a(this, str);
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "WatchAppsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            t62 t62 = this.m;
            if (t62 != null) {
                t62.b();
            } else {
                kd4.d("mWatchAppAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(WatchApp watchApp) {
        if (watchApp != null) {
            t62 t62 = this.m;
            if (t62 != null) {
                t62.b(watchApp.getWatchappId());
                c(watchApp);
                return;
            }
            kd4.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void c(String str) {
        kd4.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            kd4.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void e(String str) {
        kd4.b(str, "category");
        tr3<ig2> tr3 = this.j;
        if (tr3 != null) {
            ig2 a2 = tr3.a();
            if (a2 != null) {
                ps2 ps2 = this.l;
                if (ps2 != null) {
                    int a3 = ps2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        ps2 ps22 = this.l;
                        if (ps22 != null) {
                            ps22.a(a3);
                            a2.q.j(a3);
                            return;
                        }
                        kd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        ns3.a aVar = ns3.a;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            kd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    return;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                        return;
                    }
                    return;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ns3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                        return;
                    }
                    return;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            j42 j42 = this.n;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) dianaCustomizeEditActivity, (kc.b) j42).a(DianaCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                p43 p43 = this.k;
                if (p43 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        p43.a(dianaCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 101) {
            if (i != 105) {
                if (i == 106 && i2 == -1 && intent != null) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) intent.getParcelableExtra("COMMUTE_TIME_WATCH_APP_SETTING");
                    if (commuteTimeWatchAppSetting != null) {
                        p43 p43 = this.k;
                        if (p43 != null) {
                            p43.a("commute-time", commuteTimeWatchAppSetting);
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (i2 == -1 && intent != null) {
                WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) intent.getParcelableExtra("WEATHER_WATCH_APP_SETTING");
                if (weatherWatchAppSetting != null) {
                    p43 p432 = this.k;
                    if (p432 != null) {
                        p432.a("weather", weatherWatchAppSetting);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("SEARCH_WATCH_APP_RESULT_ID");
            if (!TextUtils.isEmpty(stringExtra)) {
                p43 p433 = this.k;
                if (p433 != null) {
                    kd4.a((Object) stringExtra, "selectedWatchAppId");
                    p433.a(stringExtra);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ig2 ig2 = (ig2) qa.a(layoutInflater, R.layout.fragment_watch_apps, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new t43(this)).a(this);
        this.j = new tr3<>(this, ig2);
        kd4.a((Object) ig2, "binding");
        return ig2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        p43 p43 = this.k;
        if (p43 != null) {
            p43.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        p43 p43 = this.k;
        if (p43 != null) {
            p43.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        t62 t62 = new t62((ArrayList) null, (t62.b) null, 3, (fd4) null);
        t62.a((t62.b) new b(this));
        this.m = t62;
        ps2 ps2 = new ps2((ArrayList) null, (ps2.c) null, 3, (fd4) null);
        ps2.a((ps2.c) new c(this));
        this.l = ps2;
        tr3<ig2> tr3 = this.j;
        if (tr3 != null) {
            ig2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                ps2 ps22 = this.l;
                if (ps22 != null) {
                    recyclerView.setAdapter(ps22);
                    RecyclerView recyclerView2 = a2.r;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    t62 t622 = this.m;
                    if (t622 != null) {
                        recyclerView2.setAdapter(t622);
                        a2.v.setOnClickListener(new d(this));
                        a2.w.setOnClickListener(new e(this));
                        a2.t.setOnClickListener(new f(this));
                        return;
                    }
                    kd4.d("mWatchAppAdapter");
                    throw null;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t(boolean z) {
        if (isActive()) {
            tr3<ig2> tr3 = this.j;
            if (tr3 != null) {
                ig2 a2 = tr3.a();
                if (a2 != null) {
                    a2.t.setCompoundDrawablesWithIntrinsicBounds(0, 0, z ? R.drawable.ic_help : 0, 0);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void v(List<WatchApp> list) {
        kd4.b(list, "watchApps");
        t62 t62 = this.m;
        if (t62 != null) {
            t62.a(list);
        } else {
            kd4.d("mWatchAppAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(p43 p43) {
        kd4.b(p43, "presenter");
        this.k = p43;
    }

    @DexIgnore
    public final void c(WatchApp watchApp) {
        tr3<ig2> tr3 = this.j;
        if (tr3 != null) {
            ig2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                kd4.a((Object) flexibleTextView, "binding.tvSelectedWatchapps");
                flexibleTextView.setText(sm2.a(PortfolioApp.W.c(), watchApp.getNameKey(), watchApp.getName()));
                FlexibleTextView flexibleTextView2 = a2.u;
                kd4.a((Object) flexibleTextView2, "binding.tvWatchappsDetail");
                flexibleTextView2.setText(sm2.a(PortfolioApp.W.c(), watchApp.getDescriptionKey(), watchApp.getDescription()));
                t62 t62 = this.m;
                if (t62 != null) {
                    int a3 = t62.a(watchApp.getWatchappId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppsFragment", "updateDetailComplication watchAppId=" + watchApp.getWatchappId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.r.i(a3);
                        return;
                    }
                    return;
                }
                kd4.d("mWatchAppAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(List<Category> list) {
        kd4.b(list, "categories");
        ps2 ps2 = this.l;
        if (ps2 != null) {
            ps2.a(list);
        } else {
            kd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeWatchAppSettingsActivity.B.a(this, str);
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, String str3) {
        kd4.b(str, "watchAppId");
        kd4.b(str2, "emptySettingRequestContent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "updateSetting of watchAppId " + str + " requestContent " + str2 + " setting " + str3);
        tr3<ig2> tr3 = this.j;
        if (tr3 != null) {
            ig2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.v;
                kd4.a((Object) flexibleTextView2, "it.tvWatchappsPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "it.tvWatchappsSetting");
                    flexibleTextView3.setVisibility(0);
                    if (!TextUtils.isEmpty(str3)) {
                        FlexibleTextView flexibleTextView4 = a2.w;
                        kd4.a((Object) flexibleTextView4, "it.tvWatchappsSetting");
                        flexibleTextView4.setText(str3);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.w;
                    kd4.a((Object) flexibleTextView5, "it.tvWatchappsSetting");
                    flexibleTextView5.setText(str2);
                    return;
                }
                FlexibleTextView flexibleTextView6 = a2.w;
                kd4.a((Object) flexibleTextView6, "it.tvWatchappsSetting");
                flexibleTextView6.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        kd4.b(str, "topWatchApp");
        kd4.b(str2, "middleWatchApp");
        kd4.b(str3, "bottomWatchApp");
        WatchAppSearchActivity.C.a(this, str, str2, str3);
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (pq4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        kd4.b(str, "title");
        kd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            tr3<ig2> tr3 = this.j;
            if (tr3 != null) {
                ig2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    kd4.a((Object) flexibleTextView, "it.tvWatchappsSetting");
                    flexibleTextView.setVisibility(8);
                    if (i2 == 0 || i == i2) {
                        FlexibleTextView flexibleTextView2 = a2.v;
                        kd4.a((Object) flexibleTextView2, "it.tvWatchappsPermission");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = a2.s;
                        kd4.a((Object) flexibleTextView3, "it.tvPermissionOrder");
                        flexibleTextView3.setVisibility(8);
                        return;
                    }
                    FlexibleTextView flexibleTextView4 = a2.v;
                    kd4.a((Object) flexibleTextView4, "it.tvWatchappsPermission");
                    flexibleTextView4.setVisibility(0);
                    FlexibleTextView flexibleTextView5 = a2.v;
                    kd4.a((Object) flexibleTextView5, "it.tvWatchappsPermission");
                    flexibleTextView5.setText(str);
                    if (str2.length() > 0) {
                        FlexibleTextView flexibleTextView6 = a2.u;
                        kd4.a((Object) flexibleTextView6, "it.tvWatchappsDetail");
                        flexibleTextView6.setText(str2);
                    }
                    if (i2 > 1) {
                        FlexibleTextView flexibleTextView7 = a2.s;
                        kd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                        flexibleTextView7.setVisibility(0);
                        FlexibleTextView flexibleTextView8 = a2.s;
                        kd4.a((Object) flexibleTextView8, "it.tvPermissionOrder");
                        pd4 pd4 = pd4.a;
                        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView8.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (kd4.a((Object) str, (Object) "REQUEST_NOTIFICATION_ACCESS")) {
            if (i == R.id.tv_cancel) {
                startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
            }
        } else if (kd4.a((Object) str, (Object) ds3.c.a())) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (!pq4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                    ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                } else if (activity != null) {
                    ((BaseActivity) activity).l();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                }
            }
        } else if (kd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION") && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (activity2 != null) {
                ((BaseActivity) activity2).l();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
