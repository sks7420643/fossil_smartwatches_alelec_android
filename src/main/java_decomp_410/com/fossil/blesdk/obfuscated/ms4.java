package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ms4 implements gr4<em4, Short> {
    @DexIgnore
    public static /* final */ ms4 a; // = new ms4();

    @DexIgnore
    public Short a(em4 em4) throws IOException {
        return Short.valueOf(em4.F());
    }
}
