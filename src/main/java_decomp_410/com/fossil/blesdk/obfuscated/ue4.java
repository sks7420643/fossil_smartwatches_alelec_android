package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ue4 {
    @DexIgnore
    public static final <T> Iterator<T> a(yc4<? super te4<? super T>, ? super yb4<? super qa4>, ? extends Object> yc4) {
        kd4.b(yc4, "block");
        se4 se4 = new se4();
        se4.a(IntrinsicsKt__IntrinsicsJvmKt.a(yc4, se4, se4));
        return se4;
    }
}
