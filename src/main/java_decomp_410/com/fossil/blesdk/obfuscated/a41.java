package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a41 implements ze0.b<qc1> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationResult a;

    @DexIgnore
    public a41(z31 z31, LocationResult locationResult) {
        this.a = locationResult;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((qc1) obj).onLocationResult(this.a);
    }
}
