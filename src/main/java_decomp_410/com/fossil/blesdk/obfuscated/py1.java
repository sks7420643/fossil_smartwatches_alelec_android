package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class py1 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<py1> CREATOR; // = new qy1();
    @DexIgnore
    public Messenger e;
    @DexIgnore
    public zy1 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ClassLoader {
        @DexIgnore
        public final Class<?> loadClass(String str, boolean z) throws ClassNotFoundException {
            if (!"com.google.android.gms.iid.MessengerCompat".equals(str)) {
                return super.loadClass(str, z);
            }
            if (!FirebaseInstanceId.o()) {
                return py1.class;
            }
            Log.d("FirebaseInstanceId", "Using renamed FirebaseIidMessengerCompat class");
            return py1.class;
        }
    }

    @DexIgnore
    public py1(IBinder iBinder) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.e = new Messenger(iBinder);
        } else {
            this.f = new az1(iBinder);
        }
    }

    @DexIgnore
    public final void a(Message message) throws RemoteException {
        Messenger messenger = this.e;
        if (messenger != null) {
            messenger.send(message);
        } else {
            this.f.a(message);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a().equals(((py1) obj).a());
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Messenger messenger = this.e;
        if (messenger != null) {
            parcel.writeStrongBinder(messenger.getBinder());
        } else {
            parcel.writeStrongBinder(this.f.asBinder());
        }
    }

    @DexIgnore
    public final IBinder a() {
        Messenger messenger = this.e;
        return messenger != null ? messenger.getBinder() : this.f.asBinder();
    }
}
