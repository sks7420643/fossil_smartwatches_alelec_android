package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a7 */
public class C1394a7 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a7$a */
    public class C1395a implements com.fossil.blesdk.obfuscated.C1394a7.C1397c<com.fossil.blesdk.obfuscated.C3012u7.C3018f> {
        @DexIgnore
        public C1395a(com.fossil.blesdk.obfuscated.C1394a7 a7Var) {
        }

        @DexIgnore
        /* renamed from: a */
        public int mo8583a(com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar) {
            return fVar.mo16678d();
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo8585b(com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar) {
            return fVar.mo16679e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.a7$b */
    public class C1396b implements com.fossil.blesdk.obfuscated.C1394a7.C1397c<com.fossil.blesdk.obfuscated.C2528o6.C2531c> {
        @DexIgnore
        public C1396b(com.fossil.blesdk.obfuscated.C1394a7 a7Var) {
        }

        @DexIgnore
        /* renamed from: a */
        public int mo8583a(com.fossil.blesdk.obfuscated.C2528o6.C2531c cVar) {
            return cVar.mo14264e();
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo8585b(com.fossil.blesdk.obfuscated.C2528o6.C2531c cVar) {
            return cVar.mo14265f();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a7$c */
    public interface C1397c<T> {
        @DexIgnore
        /* renamed from: a */
        int mo8583a(T t);

        @DexIgnore
        /* renamed from: b */
        boolean mo8585b(T t);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T m4247a(T[] tArr, int i, com.fossil.blesdk.obfuscated.C1394a7.C1397c<T> cVar) {
        int i2 = (i & 1) == 0 ? com.misfit.frameworks.common.constants.MFNetworkReturnCode.BAD_REQUEST : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (java.lang.Math.abs(cVar.mo8583a(t2) - i2) * 2) + (cVar.mo8585b(t2) == z ? 0 : 1);
            if (t == null || i3 > abs) {
                t = t2;
                i3 = abs;
            }
        }
        return t;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3012u7.C3018f mo8581a(com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        return (com.fossil.blesdk.obfuscated.C3012u7.C3018f) m4247a(fVarArr, i, new com.fossil.blesdk.obfuscated.C1394a7.C1395a(this));
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8579a(android.content.Context context, java.io.InputStream inputStream) {
        java.io.File a = com.fossil.blesdk.obfuscated.C1466b7.m4801a(context);
        if (a == null) {
            return null;
        }
        try {
            if (!com.fossil.blesdk.obfuscated.C1466b7.m4807a(a, inputStream)) {
                return null;
            }
            android.graphics.Typeface createFromFile = android.graphics.Typeface.createFromFile(a.getPath());
            a.delete();
            return createFromFile;
        } catch (java.lang.RuntimeException unused) {
            return null;
        } finally {
            a.delete();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8577a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        java.io.InputStream inputStream;
        java.io.InputStream inputStream2 = null;
        if (fVarArr.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(mo8581a(fVarArr, i).mo16677c());
            try {
                android.graphics.Typeface a = mo8579a(context, inputStream);
                com.fossil.blesdk.obfuscated.C1466b7.m4805a((java.io.Closeable) inputStream);
                return a;
            } catch (java.io.IOException unused) {
                com.fossil.blesdk.obfuscated.C1466b7.m4805a((java.io.Closeable) inputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                com.fossil.blesdk.obfuscated.C1466b7.m4805a((java.io.Closeable) inputStream2);
                throw th;
            }
        } catch (java.io.IOException unused2) {
            inputStream = null;
            com.fossil.blesdk.obfuscated.C1466b7.m4805a((java.io.Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            com.fossil.blesdk.obfuscated.C1466b7.m4805a((java.io.Closeable) inputStream2);
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2528o6.C2531c mo8580a(com.fossil.blesdk.obfuscated.C2528o6.C2530b bVar, int i) {
        return (com.fossil.blesdk.obfuscated.C2528o6.C2531c) m4247a(bVar.mo14259a(), i, new com.fossil.blesdk.obfuscated.C1394a7.C1396b(this));
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8578a(android.content.Context context, com.fossil.blesdk.obfuscated.C2528o6.C2530b bVar, android.content.res.Resources resources, int i) {
        com.fossil.blesdk.obfuscated.C2528o6.C2531c a = mo8580a(bVar, i);
        if (a == null) {
            return null;
        }
        return com.fossil.blesdk.obfuscated.C3091v6.m15081a(context, resources, a.mo14261b(), a.mo14260a(), i);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8576a(android.content.Context context, android.content.res.Resources resources, int i, java.lang.String str, int i2) {
        java.io.File a = com.fossil.blesdk.obfuscated.C1466b7.m4801a(context);
        if (a == null) {
            return null;
        }
        try {
            if (!com.fossil.blesdk.obfuscated.C1466b7.m4806a(a, resources, i)) {
                return null;
            }
            android.graphics.Typeface createFromFile = android.graphics.Typeface.createFromFile(a.getPath());
            a.delete();
            return createFromFile;
        } catch (java.lang.RuntimeException unused) {
            return null;
        } finally {
            a.delete();
        }
    }
}
