package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface d51 extends IInterface {
    @DexIgnore
    sn0 zza(Bitmap bitmap) throws RemoteException;
}
