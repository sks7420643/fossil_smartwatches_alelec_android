package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gq */
public interface C1885gq {
    @DexIgnore
    /* renamed from: a */
    <T> T mo11282a(int i, java.lang.Class<T> cls);

    @DexIgnore
    /* renamed from: a */
    void mo11283a();

    @DexIgnore
    /* renamed from: a */
    void mo11284a(int i);

    @DexIgnore
    /* renamed from: b */
    <T> T mo11285b(int i, java.lang.Class<T> cls);

    @DexIgnore
    <T> void put(T t);
}
