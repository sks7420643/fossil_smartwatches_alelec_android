package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kz2 implements Factory<gz2> {
    @DexIgnore
    public static gz2 a(iz2 iz2) {
        gz2 c = iz2.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
