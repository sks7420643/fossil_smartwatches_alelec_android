package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j2 */
public interface C2101j2 {
    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C2110j9 mo8461a(int i, long j);

    @DexIgnore
    /* renamed from: a */
    void mo8462a(int i);

    @DexIgnore
    /* renamed from: a */
    void mo8464a(android.view.Menu menu, com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar);

    @DexIgnore
    /* renamed from: a */
    void mo8466a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar, com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar2);

    @DexIgnore
    /* renamed from: a */
    void mo8467a(com.fossil.blesdk.obfuscated.C2852s2 s2Var);

    @DexIgnore
    /* renamed from: a */
    void mo8469a(boolean z);

    @DexIgnore
    /* renamed from: a */
    boolean mo8470a();

    @DexIgnore
    /* renamed from: b */
    void mo8471b();

    @DexIgnore
    /* renamed from: b */
    void mo8472b(int i);

    @DexIgnore
    /* renamed from: b */
    void mo8475b(boolean z);

    @DexIgnore
    /* renamed from: c */
    boolean mo8478c();

    @DexIgnore
    void collapseActionView();

    @DexIgnore
    /* renamed from: d */
    boolean mo8481d();

    @DexIgnore
    /* renamed from: e */
    boolean mo8482e();

    @DexIgnore
    /* renamed from: f */
    boolean mo8483f();

    @DexIgnore
    /* renamed from: g */
    void mo8484g();

    @DexIgnore
    android.content.Context getContext();

    @DexIgnore
    java.lang.CharSequence getTitle();

    @DexIgnore
    /* renamed from: h */
    boolean mo8487h();

    @DexIgnore
    /* renamed from: i */
    android.view.Menu mo8488i();

    @DexIgnore
    /* renamed from: j */
    int mo8489j();

    @DexIgnore
    /* renamed from: k */
    android.view.ViewGroup mo8490k();

    @DexIgnore
    /* renamed from: l */
    int mo8491l();

    @DexIgnore
    /* renamed from: m */
    void mo8492m();

    @DexIgnore
    /* renamed from: n */
    void mo8493n();

    @DexIgnore
    void setIcon(int i);

    @DexIgnore
    void setIcon(android.graphics.drawable.Drawable drawable);

    @DexIgnore
    void setTitle(java.lang.CharSequence charSequence);

    @DexIgnore
    void setVisibility(int i);

    @DexIgnore
    void setWindowCallback(android.view.Window.Callback callback);

    @DexIgnore
    void setWindowTitle(java.lang.CharSequence charSequence);
}
