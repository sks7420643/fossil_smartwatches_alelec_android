package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qv<R> {
    @DexIgnore
    boolean a(GlideException glideException, Object obj, bw<R> bwVar, boolean z);

    @DexIgnore
    boolean a(R r, Object obj, bw<R> bwVar, DataSource dataSource, boolean z);
}
