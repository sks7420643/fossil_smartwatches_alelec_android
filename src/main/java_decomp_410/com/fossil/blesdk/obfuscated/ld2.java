package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ld2 extends kd2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ScrollView x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.cl_container, 1);
        A.put(R.id.rv_categories, 2);
        A.put(R.id.rv_micro_app, 3);
        A.put(R.id.tv_selected_micro_app, 4);
        A.put(R.id.tv_micro_app_setting, 5);
        A.put(R.id.tv_permission_order, 6);
        A.put(R.id.tv_micro_app_permission, 7);
        A.put(R.id.tv_micro_app_detail, 8);
    }
    */

    @DexIgnore
    public ld2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 9, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ld2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[1], objArr[2], objArr[3], objArr[8], objArr[7], objArr[5], objArr[6], objArr[4]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
