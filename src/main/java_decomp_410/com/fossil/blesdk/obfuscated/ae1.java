package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ae1 {
    @DexIgnore
    public static ie1 a;

    @DexIgnore
    public static ie1 a() {
        ie1 ie1 = a;
        bk0.a(ie1, (Object) "CameraUpdateFactory is not initialized");
        return ie1;
    }

    @DexIgnore
    public static void a(ie1 ie1) {
        bk0.a(ie1);
        a = ie1;
    }

    @DexIgnore
    public static zd1 a(float f) {
        try {
            return new zd1(a().a(f));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public static zd1 a(LatLng latLng, float f) {
        try {
            return new zd1(a().a(latLng, f));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
