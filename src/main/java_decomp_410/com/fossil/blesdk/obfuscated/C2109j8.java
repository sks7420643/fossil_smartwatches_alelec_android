package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j8 */
public class C2109j8 {
    @DexIgnore
    /* renamed from: a */
    public static <T> T m8871a(T t) {
        if (t != null) {
            return t;
        }
        throw new java.lang.NullPointerException();
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T m8872a(T t, java.lang.Object obj) {
        if (t != null) {
            return t;
        }
        throw new java.lang.NullPointerException(java.lang.String.valueOf(obj));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m8870a(int i) {
        if (i >= 0) {
            return i;
        }
        throw new java.lang.IllegalArgumentException();
    }
}
