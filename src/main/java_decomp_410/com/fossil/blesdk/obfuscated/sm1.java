package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sm1 extends jc1 implements rm1 {
    @DexIgnore
    public sm1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    public final sn0 a(sn0 sn0, int i, int i2, String str, int i3) throws RemoteException {
        Parcel o = o();
        kc1.a(o, sn0);
        o.writeInt(i);
        o.writeInt(i2);
        o.writeString(str);
        o.writeInt(i3);
        Parcel a = a(1, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
