package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x5 */
public class C3262x5 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.x5$a")
    /* renamed from: com.fossil.blesdk.obfuscated.x5$a */
    public static class C3263a extends com.fossil.blesdk.obfuscated.C3262x5 {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.app.ActivityOptions f10844a;

        @DexIgnore
        public C3263a(android.app.ActivityOptions activityOptions) {
            this.f10844a = activityOptions;
        }

        @DexIgnore
        /* renamed from: a */
        public android.os.Bundle mo17601a() {
            return this.f10844a.toBundle();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3262x5 m16150a(android.app.Activity activity, com.fossil.blesdk.obfuscated.C1775f8<android.view.View, java.lang.String>... f8VarArr) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return new com.fossil.blesdk.obfuscated.C3262x5();
        }
        android.util.Pair[] pairArr = null;
        if (f8VarArr != null) {
            pairArr = new android.util.Pair[f8VarArr.length];
            for (int i = 0; i < f8VarArr.length; i++) {
                pairArr[i] = android.util.Pair.create(f8VarArr[i].f5093a, f8VarArr[i].f5094b);
            }
        }
        return new com.fossil.blesdk.obfuscated.C3262x5.C3263a(android.app.ActivityOptions.makeSceneTransitionAnimation(activity, pairArr));
    }

    @DexIgnore
    /* renamed from: a */
    public android.os.Bundle mo17601a() {
        return null;
    }
}
