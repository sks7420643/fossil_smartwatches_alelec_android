package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kq0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kq0> CREATOR; // = new mq0();
    @DexIgnore
    public /* final */ zo0 e;
    @DexIgnore
    public /* final */ vp0 f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public kq0(zo0 zo0, IBinder iBinder, long j, long j2) {
        this.e = zo0;
        this.f = wp0.a(iBinder);
        this.g = j;
        this.h = j2;
    }

    @DexIgnore
    public zo0 H() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kq0)) {
            return false;
        }
        kq0 kq0 = (kq0) obj;
        return zj0.a(this.e, kq0.e) && this.g == kq0.g && this.h == kq0.h;
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(this.e, Long.valueOf(this.g), Long.valueOf(this.h));
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", new Object[]{this.e});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) H(), i, false);
        kk0.a(parcel, 2, this.f.asBinder(), false);
        kk0.a(parcel, 3, this.g);
        kk0.a(parcel, 4, this.h);
        kk0.a(parcel, a);
    }
}
