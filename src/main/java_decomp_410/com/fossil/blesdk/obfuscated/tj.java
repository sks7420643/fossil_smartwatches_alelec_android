package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.ExistingWorkPolicy;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.blesdk.obfuscated.dj;
import com.fossil.blesdk.obfuscated.xi;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tj extends jj {
    @DexIgnore
    public static tj j;
    @DexIgnore
    public static tj k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public Context a;
    @DexIgnore
    public xi b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public zl d;
    @DexIgnore
    public List<pj> e;
    @DexIgnore
    public oj f;
    @DexIgnore
    public sl g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public tj(Context context, xi xiVar, zl zlVar) {
        this(context, xiVar, zlVar, context.getResources().getBoolean(hj.workmanager_test_configuration));
    }

    @DexIgnore
    @Deprecated
    public static tj a() {
        synchronized (l) {
            if (j != null) {
                tj tjVar = j;
                return tjVar;
            }
            tj tjVar2 = k;
            return tjVar2;
        }
    }

    @DexIgnore
    public Context b() {
        return this.a;
    }

    @DexIgnore
    public xi c() {
        return this.b;
    }

    @DexIgnore
    public sl d() {
        return this.g;
    }

    @DexIgnore
    public oj e() {
        return this.f;
    }

    @DexIgnore
    public List<pj> f() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase g() {
        return this.c;
    }

    @DexIgnore
    public zl h() {
        return this.d;
    }

    @DexIgnore
    public void i() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void j() {
        if (Build.VERSION.SDK_INT >= 23) {
            ek.a(b());
        }
        g().d().d();
        qj.a(c(), g(), f());
    }

    @DexIgnore
    public void b(String str) {
        this.d.a(new vl(this, str));
    }

    @DexIgnore
    public tj(Context context, xi xiVar, zl zlVar, boolean z) {
        this(context, xiVar, zlVar, WorkDatabase.a(context.getApplicationContext(), zlVar.b(), z));
    }

    @DexIgnore
    public static tj a(Context context) {
        tj a2;
        synchronized (l) {
            a2 = a();
            if (a2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof xi.b) {
                    a(applicationContext, ((xi.b) applicationContext).a());
                    a2 = a(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return a2;
    }

    @DexIgnore
    public tj(Context context, xi xiVar, zl zlVar, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        dj.a((dj) new dj.a(xiVar.g()));
        Context context2 = context;
        xi xiVar2 = xiVar;
        zl zlVar2 = zlVar;
        WorkDatabase workDatabase2 = workDatabase;
        List<pj> a2 = a(applicationContext, zlVar);
        a(context2, xiVar2, zlVar2, workDatabase2, a2, new oj(context2, xiVar2, zlVar2, workDatabase2, a2));
    }

    @DexIgnore
    public static void a(Context context, xi xiVar) {
        synchronized (l) {
            if (j != null) {
                if (k != null) {
                    throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class levelJavadoc for more information.");
                }
            }
            if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new tj(applicationContext, xiVar, new am(xiVar.h()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    public fj a(String str, ExistingWorkPolicy existingWorkPolicy, List<ej> list) {
        return new rj(this, str, existingWorkPolicy, list).a();
    }

    @DexIgnore
    public void a(String str) {
        a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public void a(String str, WorkerParameters.a aVar) {
        this.d.a(new ul(this, str, aVar));
    }

    @DexIgnore
    public void a(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public final void a(Context context, xi xiVar, zl zlVar, WorkDatabase workDatabase, List<pj> list, oj ojVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = xiVar;
        this.d = zlVar;
        this.c = workDatabase;
        this.e = list;
        this.f = ojVar;
        this.g = new sl(this.a);
        this.h = false;
        this.d.a(new ForceStopRunnable(applicationContext, this));
    }

    @DexIgnore
    public List<pj> a(Context context, zl zlVar) {
        return Arrays.asList(new pj[]{qj.a(context, this), new vj(context, zlVar, this)});
    }
}
