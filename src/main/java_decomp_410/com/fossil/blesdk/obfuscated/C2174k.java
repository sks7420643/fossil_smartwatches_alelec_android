package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k */
public interface C2174k extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.k$a")
    /* renamed from: com.fossil.blesdk.obfuscated.k$a */
    public static abstract class C2175a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C2174k {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.k$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.k$a$a */
        public static class C2176a implements com.fossil.blesdk.obfuscated.C2174k {

            @DexIgnore
            /* renamed from: e */
            public android.os.IBinder f6641e;

            @DexIgnore
            public C2176a(android.os.IBinder iBinder) {
                this.f6641e = iBinder;
            }

            @DexIgnore
            /* renamed from: a */
            public boolean mo12529a(android.view.KeyEvent keyEvent) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    boolean z = true;
                    if (keyEvent != null) {
                        obtain.writeInt(1);
                        keyEvent.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f6641e.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z = false;
                    }
                    return z;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public android.os.IBinder asBinder() {
                return this.f6641e;
            }

            @DexIgnore
            /* renamed from: f */
            public android.support.p000v4.media.MediaMetadataCompat mo12530f() throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    this.f6641e.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? android.support.p000v4.media.MediaMetadataCompat.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            /* renamed from: g */
            public android.support.p000v4.media.session.PlaybackStateCompat mo12531g() throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    this.f6641e.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? android.support.p000v4.media.session.PlaybackStateCompat.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            /* renamed from: a */
            public void mo12528a(com.fossil.blesdk.obfuscated.C2083j jVar) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.media.session.IMediaSession");
                    obtain.writeStrongBinder(jVar != null ? jVar.asBinder() : null);
                    this.f6641e.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2174k m9254a(android.os.IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            android.os.IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.media.session.IMediaSession");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof com.fossil.blesdk.obfuscated.C2174k)) {
                return new com.fossil.blesdk.obfuscated.C2174k.C2175a.C2176a(iBinder);
            }
            return (com.fossil.blesdk.obfuscated.C2174k) queryLocalInterface;
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo12528a(com.fossil.blesdk.obfuscated.C2083j jVar) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    boolean mo12529a(android.view.KeyEvent keyEvent) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: f */
    android.support.p000v4.media.MediaMetadataCompat mo12530f() throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: g */
    android.support.p000v4.media.session.PlaybackStateCompat mo12531g() throws android.os.RemoteException;
}
