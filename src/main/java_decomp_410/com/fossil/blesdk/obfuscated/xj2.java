package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xj2 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public xj2(String str) {
        this.a = str.substring(0, 3);
    }

    @DexIgnore
    public List<ButtonMapping> a() {
        List<ButtonMapping> arrayList = new ArrayList<>();
        try {
            InputStream openRawResource = PortfolioApp.R.getResources().openRawResource(R.raw.mapping_set_default);
            arrayList = a(openRawResource);
            openRawResource.close();
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public final List b(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            String name = xmlPullParser.getName();
            if (eventType == 2 && name.equals(LegacyDeviceModel.COLUMN_DEVICE_MODEL)) {
                String attributeValue = xmlPullParser.getAttributeValue((String) null, "name");
                if (attributeValue != null && this.a.contains(attributeValue)) {
                    arrayList.addAll(a(xmlPullParser));
                    return arrayList;
                }
            }
            eventType = xmlPullParser.next();
        }
        return arrayList;
    }

    @DexIgnore
    public final List a(InputStream inputStream) throws XmlPullParserException, IOException {
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, (String) null);
            return b(newPullParser);
        } finally {
            inputStream.close();
        }
    }

    @DexIgnore
    public final List a(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            String name = xmlPullParser.getName();
            if (eventType != 2) {
                if (eventType == 3 && name.equals(LegacyDeviceModel.COLUMN_DEVICE_MODEL)) {
                    return arrayList;
                }
            } else if (name.equals("microappmapping")) {
                arrayList.add(new ButtonMapping(xmlPullParser.getAttributeValue((String) null, "button"), xmlPullParser.getAttributeValue((String) null, "appid")));
            }
            eventType = xmlPullParser.next();
        }
        return arrayList;
    }
}
