package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fs */
public class C1825fs implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f5263a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fs$a */
    public static class C1826a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f5264a;

        @DexIgnore
        public C1826a(android.content.Context context) {
            this.f5264a = context;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1825fs(this.f5264a);
        }
    }

    @DexIgnore
    public C1825fs(android.content.Context context) {
        this.f5263a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.InputStream> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        if (com.fossil.blesdk.obfuscated.C1740ep.m6535a(i, i2)) {
            return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(uri), com.fossil.blesdk.obfuscated.C1817fp.m7092a(this.f5263a, uri));
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return com.fossil.blesdk.obfuscated.C1740ep.m6536a(uri);
    }
}
