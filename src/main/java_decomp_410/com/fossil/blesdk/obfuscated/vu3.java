package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vu3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public vu3(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof vu3) {
            vu3 vu3 = (vu3) obj;
            return wv3.a((Object) this.a, (Object) vu3.a) && wv3.a((Object) this.b, (Object) vu3.b);
        }
    }

    @DexIgnore
    public int hashCode() {
        String str = this.b;
        int i = 0;
        int hashCode = (899 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.a;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return this.a + " realm=\"" + this.b + "\"";
    }
}
