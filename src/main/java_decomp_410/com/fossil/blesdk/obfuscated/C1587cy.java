package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cy */
public final class C1587cy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f4214a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f4215b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f4216c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.Boolean f4217d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f4218e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.String f4219f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.String f4220g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.lang.String f4221h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ java.lang.String f4222i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.lang.String f4223j;

    @DexIgnore
    /* renamed from: k */
    public java.lang.String f4224k;

    @DexIgnore
    public C1587cy(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.Boolean bool, java.lang.String str4, java.lang.String str5, java.lang.String str6, java.lang.String str7, java.lang.String str8, java.lang.String str9) {
        this.f4214a = str;
        this.f4215b = str2;
        this.f4216c = str3;
        this.f4217d = bool;
        this.f4218e = str4;
        this.f4219f = str5;
        this.f4220g = str6;
        this.f4221h = str7;
        this.f4222i = str8;
        this.f4223j = str9;
    }

    @DexIgnore
    public java.lang.String toString() {
        if (this.f4224k == null) {
            this.f4224k = "appBundleId=" + this.f4214a + ", executionId=" + this.f4215b + ", installationId=" + this.f4216c + ", limitAdTrackingEnabled=" + this.f4217d + ", betaDeviceToken=" + this.f4218e + ", buildId=" + this.f4219f + ", osVersion=" + this.f4220g + ", deviceModel=" + this.f4221h + ", appVersionCode=" + this.f4222i + ", appVersionName=" + this.f4223j;
        }
        return this.f4224k;
    }
}
