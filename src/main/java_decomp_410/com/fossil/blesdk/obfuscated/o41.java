package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.sc1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o41 extends sc1.a<vc1> {
    @DexIgnore
    public /* final */ /* synthetic */ tc1 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o41(n41 n41, ge0 ge0, tc1 tc1, String str) {
        super(ge0);
        this.s = tc1;
    }

    @DexIgnore
    public final /* synthetic */ me0 a(Status status) {
        return new vc1(status);
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        ((f41) bVar).a(this.s, (ue0<vc1>) this, this.t);
    }
}
