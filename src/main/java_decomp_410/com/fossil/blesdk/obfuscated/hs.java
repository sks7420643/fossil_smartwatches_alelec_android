package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hs implements sr<URL, InputStream> {
    @DexIgnore
    public /* final */ sr<lr, InputStream> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<URL, InputStream> {
        @DexIgnore
        public sr<URL, InputStream> a(wr wrVar) {
            return new hs(wrVar.a(lr.class, InputStream.class));
        }
    }

    @DexIgnore
    public hs(sr<lr, InputStream> srVar) {
        this.a = srVar;
    }

    @DexIgnore
    public boolean a(URL url) {
        return true;
    }

    @DexIgnore
    public sr.a<InputStream> a(URL url, int i, int i2, lo loVar) {
        return this.a.a(new lr(url), i, i2, loVar);
    }
}
