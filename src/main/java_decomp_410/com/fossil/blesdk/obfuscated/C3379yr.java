package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yr */
public class C3379yr implements com.fossil.blesdk.obfuscated.C1963ho<java.io.InputStream> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f11311a;

    @DexIgnore
    public C3379yr(com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f11311a = gqVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[Catch:{ all -> 0x002e }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003f A[SYNTHETIC, Splitter:B:23:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004a A[SYNTHETIC, Splitter:B:29:0x004a] */
    /* renamed from: a */
    public boolean mo11698a(java.io.InputStream inputStream, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        byte[] bArr = (byte[]) this.f11311a.mo11285b(65536, byte[].class);
        boolean z = false;
        java.io.FileOutputStream fileOutputStream = null;
        try {
            java.io.FileOutputStream fileOutputStream2 = new java.io.FileOutputStream(file);
            while (true) {
                try {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    fileOutputStream2.write(bArr, 0, read);
                } catch (java.io.IOException e) {
                    e = e;
                    fileOutputStream = fileOutputStream2;
                    try {
                        if (android.util.Log.isLoggable("StreamEncoder", 3)) {
                        }
                        if (fileOutputStream != null) {
                        }
                        this.f11311a.put(bArr);
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (java.io.IOException unused) {
                            }
                        }
                        this.f11311a.put(bArr);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileOutputStream = fileOutputStream2;
                    if (fileOutputStream != null) {
                    }
                    this.f11311a.put(bArr);
                    throw th;
                }
            }
            fileOutputStream2.close();
            z = true;
            try {
                fileOutputStream2.close();
            } catch (java.io.IOException unused2) {
            }
        } catch (java.io.IOException e2) {
            e = e2;
            if (android.util.Log.isLoggable("StreamEncoder", 3)) {
                android.util.Log.d("StreamEncoder", "Failed to encode data onto the OutputStream", e);
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            this.f11311a.put(bArr);
            return z;
        }
        this.f11311a.put(bArr);
        return z;
    }
}
