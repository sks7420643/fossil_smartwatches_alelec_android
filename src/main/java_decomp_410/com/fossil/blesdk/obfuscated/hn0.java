package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hn0 extends gn0 {
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public hn0(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f = bArr;
    }

    @DexIgnore
    public final byte[] o() {
        return this.f;
    }
}
