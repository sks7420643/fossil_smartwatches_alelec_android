package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t3 */
public class C2939t3 extends android.app.Service {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1521c.C1522a f9542e; // = new com.fossil.blesdk.obfuscated.C2939t3.C2940a(this);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.t3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.t3$a */
    public class C2940a extends com.fossil.blesdk.obfuscated.C1521c.C1522a {
        @DexIgnore
        public C2940a(com.fossil.blesdk.obfuscated.C2939t3 t3Var) {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9352a(com.fossil.blesdk.obfuscated.C1365a aVar, android.os.Bundle bundle) throws android.os.RemoteException {
            aVar.mo8355d(bundle);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9353a(com.fossil.blesdk.obfuscated.C1365a aVar, java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException {
            aVar.mo8354a(str, bundle);
        }
    }

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        return this.f9542e;
    }
}
