package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jg4 extends gi4<li4> implements ig4 {
    @DexIgnore
    public /* final */ kg4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jg4(li4 li4, kg4 kg4) {
        super(li4);
        kd4.b(li4, "parent");
        kd4.b(kg4, "childJob");
        this.i = kg4;
    }

    @DexIgnore
    public boolean a(Throwable th) {
        kd4.b(th, "cause");
        return ((li4) this.h).d(th);
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.a((ti4) this.h);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildHandle[" + this.i + ']';
    }
}
