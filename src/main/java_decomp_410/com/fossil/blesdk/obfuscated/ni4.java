package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.DeferredCoroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ni4<T> extends DeferredCoroutine<T> {
    @DexIgnore
    public yc4<? super zg4, ? super yb4<? super T>, ? extends Object> h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ni4(CoroutineContext coroutineContext, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4) {
        super(coroutineContext, false);
        kd4.b(coroutineContext, "parentContext");
        kd4.b(yc4, "block");
        this.h = yc4;
    }

    @DexIgnore
    public void l() {
        yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4 = this.h;
        if (yc4 != null) {
            this.h = null;
            kk4.a(yc4, this, this);
            return;
        }
        throw new IllegalStateException("Already started".toString());
    }
}
