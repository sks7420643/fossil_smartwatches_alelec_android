package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ox */
public class C2609ox implements com.fossil.blesdk.obfuscated.C1519by {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.v44 f8241a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.z64 f8242b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.content.Context f8243c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3395yx f8244d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.ScheduledExecutorService f8245e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledFuture<?>> f8246f; // = new java.util.concurrent.atomic.AtomicReference<>();

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C1587cy f8247g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2844rx f8248h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.t64 f8249i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.k54 f8250j; // = new com.fossil.blesdk.obfuscated.k54();

    @DexIgnore
    /* renamed from: k */
    public com.fossil.blesdk.obfuscated.C2683px f8251k; // = new com.fossil.blesdk.obfuscated.C3068ux();

    @DexIgnore
    /* renamed from: l */
    public boolean f8252l; // = true;

    @DexIgnore
    /* renamed from: m */
    public boolean f8253m; // = true;

    @DexIgnore
    /* renamed from: n */
    public volatile int f8254n; // = -1;

    @DexIgnore
    /* renamed from: o */
    public boolean f8255o; // = false;

    @DexIgnore
    /* renamed from: p */
    public boolean f8256p; // = false;

    @DexIgnore
    public C2609ox(com.fossil.blesdk.obfuscated.v44 v44, android.content.Context context, java.util.concurrent.ScheduledExecutorService scheduledExecutorService, com.fossil.blesdk.obfuscated.C3395yx yxVar, com.fossil.blesdk.obfuscated.z64 z64, com.fossil.blesdk.obfuscated.C1587cy cyVar, com.fossil.blesdk.obfuscated.C2844rx rxVar) {
        this.f8241a = v44;
        this.f8243c = context;
        this.f8245e = scheduledExecutorService;
        this.f8244d = yxVar;
        this.f8242b = z64;
        this.f8247g = cyVar;
        this.f8248h = rxVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9346a(com.fossil.blesdk.obfuscated.j74 j74, java.lang.String str) {
        java.lang.String str2;
        java.lang.String str3;
        com.fossil.blesdk.obfuscated.C3472zx zxVar = new com.fossil.blesdk.obfuscated.C3472zx(this.f8241a, str, j74.f15934a, this.f8242b, this.f8250j.mo28770d(this.f8243c));
        this.f8249i = com.fossil.blesdk.obfuscated.C2167jx.m9204a(zxVar);
        this.f8244d.mo18362a(j74);
        this.f8255o = j74.f15938e;
        this.f8256p = j74.f15939f;
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Firebase analytics forwarding ");
        java.lang.String str4 = "enabled";
        sb.append(this.f8255o ? str4 : "disabled");
        g.mo30060d("Answers", sb.toString());
        com.fossil.blesdk.obfuscated.y44 g2 = com.fossil.blesdk.obfuscated.q44.m26805g();
        java.lang.StringBuilder sb2 = new java.lang.StringBuilder();
        sb2.append("Firebase analytics including purchase events ");
        if (this.f8256p) {
            str2 = str4;
        } else {
            str2 = "disabled";
        }
        sb2.append(str2);
        g2.mo30060d("Answers", sb2.toString());
        this.f8252l = j74.f15940g;
        com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
        java.lang.StringBuilder sb3 = new java.lang.StringBuilder();
        sb3.append("Custom event tracking ");
        if (this.f8252l) {
            str3 = str4;
        } else {
            str3 = "disabled";
        }
        sb3.append(str3);
        g3.mo30060d("Answers", sb3.toString());
        this.f8253m = j74.f15941h;
        com.fossil.blesdk.obfuscated.y44 g4 = com.fossil.blesdk.obfuscated.q44.m26805g();
        java.lang.StringBuilder sb4 = new java.lang.StringBuilder();
        sb4.append("Predefined event tracking ");
        if (!this.f8253m) {
            str4 = "disabled";
        }
        sb4.append(str4);
        g4.mo30060d("Answers", sb4.toString());
        if (j74.f15943j > 1) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Event sampling enabled");
            this.f8251k = new com.crashlytics.android.answers.SamplingEventFilter(j74.f15943j);
        }
        this.f8254n = j74.f15935b;
        mo14548a(0, (long) this.f8254n);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo14142b() {
        try {
            return this.f8244d.mo30088g();
        } catch (java.io.IOException e) {
            p011io.fabric.sdk.android.services.common.CommonUtils.m36871a(this.f8243c, "Failed to roll file over.", (java.lang.Throwable) e);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14143c() {
        if (this.f8246f.get() != null) {
            p011io.fabric.sdk.android.services.common.CommonUtils.m36885c(this.f8243c, "Cancelling time-based rollover because no events are currently being generated.");
            this.f8246f.get().cancel(false);
            this.f8246f.set((java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo9347d() {
        this.f8244d.mo30080a();
    }

    @DexIgnore
    /* renamed from: e */
    public void mo14549e() {
        if (this.f8254n != -1) {
            mo14548a((long) this.f8254n, (long) this.f8254n);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9345a(com.crashlytics.android.answers.SessionEvent.C0393b bVar) {
        com.crashlytics.android.answers.SessionEvent a = bVar.mo4065a(this.f8247g);
        if (!this.f8252l && com.crashlytics.android.answers.SessionEvent.Type.CUSTOM.equals(a.f2166c)) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("Answers", "Custom events tracking disabled - skipping event: " + a);
        } else if (!this.f8253m && com.crashlytics.android.answers.SessionEvent.Type.PREDEFINED.equals(a.f2166c)) {
            com.fossil.blesdk.obfuscated.y44 g2 = com.fossil.blesdk.obfuscated.q44.m26805g();
            g2.mo30060d("Answers", "Predefined events tracking disabled - skipping event: " + a);
        } else if (this.f8251k.mo4061a(a)) {
            com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
            g3.mo30060d("Answers", "Skipping filtered event: " + a);
        } else {
            try {
                this.f8244d.mo30083a(a);
            } catch (java.io.IOException e) {
                com.fossil.blesdk.obfuscated.y44 g4 = com.fossil.blesdk.obfuscated.q44.m26805g();
                g4.mo30063e("Answers", "Failed to write event: " + a, e);
            }
            mo14549e();
            boolean z = com.crashlytics.android.answers.SessionEvent.Type.CUSTOM.equals(a.f2166c) || com.crashlytics.android.answers.SessionEvent.Type.PREDEFINED.equals(a.f2166c);
            boolean equals = "purchase".equals(a.f2170g);
            if (this.f8255o && z) {
                if (!equals || this.f8256p) {
                    try {
                        this.f8248h.mo15785a(a);
                    } catch (java.lang.Exception e2) {
                        com.fossil.blesdk.obfuscated.y44 g5 = com.fossil.blesdk.obfuscated.q44.m26805g();
                        g5.mo30063e("Answers", "Failed to map event to Firebase: " + a, e2);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9344a() {
        if (this.f8249i == null) {
            p011io.fabric.sdk.android.services.common.CommonUtils.m36885c(this.f8243c, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        p011io.fabric.sdk.android.services.common.CommonUtils.m36885c(this.f8243c, "Sending all files");
        java.util.List<java.io.File> d = this.f8244d.mo30087d();
        int i = 0;
        while (true) {
            try {
                if (d.size() <= 0) {
                    break;
                }
                p011io.fabric.sdk.android.services.common.CommonUtils.m36885c(this.f8243c, java.lang.String.format(java.util.Locale.US, "attempt to send batch of %d files", new java.lang.Object[]{java.lang.Integer.valueOf(d.size())}));
                boolean a = this.f8249i.mo12506a(d);
                if (a) {
                    i += d.size();
                    this.f8244d.mo30084a(d);
                }
                if (!a) {
                    break;
                }
                d = this.f8244d.mo30087d();
            } catch (java.lang.Exception e) {
                android.content.Context context = this.f8243c;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36871a(context, "Failed to send batch of analytics files to server: " + e.getMessage(), (java.lang.Throwable) e);
            }
        }
        if (i == 0) {
            this.f8244d.mo30085b();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14548a(long j, long j2) {
        if (this.f8246f.get() == null) {
            com.fossil.blesdk.obfuscated.w64 w64 = new com.fossil.blesdk.obfuscated.w64(this.f8243c, this);
            android.content.Context context = this.f8243c;
            p011io.fabric.sdk.android.services.common.CommonUtils.m36885c(context, "Scheduling time based file roll over every " + j2 + " seconds");
            try {
                this.f8246f.set(this.f8245e.scheduleAtFixedRate(w64, j, j2, java.util.concurrent.TimeUnit.SECONDS));
            } catch (java.util.concurrent.RejectedExecutionException e) {
                p011io.fabric.sdk.android.services.common.CommonUtils.m36871a(this.f8243c, "Failed to schedule time based file roll over", (java.lang.Throwable) e);
            }
        }
    }
}
