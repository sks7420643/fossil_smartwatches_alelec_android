package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import java.io.IOException;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yx extends p64<SessionEvent> {
    @DexIgnore
    public j74 g;

    @DexIgnore
    public yx(Context context, dy dyVar, n54 n54, q64 q64) throws IOException {
        super(context, dyVar, n54, q64, 100);
    }

    @DexIgnore
    public void a(j74 j74) {
        this.g = j74;
    }

    @DexIgnore
    public String c() {
        UUID randomUUID = UUID.randomUUID();
        return "sa" + "_" + randomUUID.toString() + "_" + this.c.a() + ".tap";
    }

    @DexIgnore
    public int e() {
        j74 j74 = this.g;
        return j74 == null ? super.e() : j74.c;
    }

    @DexIgnore
    public int f() {
        j74 j74 = this.g;
        return j74 == null ? super.f() : j74.d;
    }
}
