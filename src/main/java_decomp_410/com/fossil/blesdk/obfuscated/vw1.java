package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vw1<T> implements ez1<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Object a; // = c;
    @DexIgnore
    public volatile ez1<T> b;

    @DexIgnore
    public vw1(kw1<T> kw1, jw1 jw1) {
        this.b = ww1.a(kw1, jw1);
    }

    @DexIgnore
    public final T get() {
        T t = this.a;
        if (t == c) {
            synchronized (this) {
                t = this.a;
                if (t == c) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
