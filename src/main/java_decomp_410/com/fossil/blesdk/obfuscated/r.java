package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r {
    @DexIgnore
    public static /* final */ int actionBarDivider; // = 2130968645;
    @DexIgnore
    public static /* final */ int actionBarItemBackground; // = 2130968646;
    @DexIgnore
    public static /* final */ int actionBarPopupTheme; // = 2130968647;
    @DexIgnore
    public static /* final */ int actionBarSize; // = 2130968648;
    @DexIgnore
    public static /* final */ int actionBarSplitStyle; // = 2130968649;
    @DexIgnore
    public static /* final */ int actionBarStyle; // = 2130968650;
    @DexIgnore
    public static /* final */ int actionBarTabBarStyle; // = 2130968651;
    @DexIgnore
    public static /* final */ int actionBarTabStyle; // = 2130968652;
    @DexIgnore
    public static /* final */ int actionBarTabTextStyle; // = 2130968653;
    @DexIgnore
    public static /* final */ int actionBarTheme; // = 2130968654;
    @DexIgnore
    public static /* final */ int actionBarWidgetTheme; // = 2130968655;
    @DexIgnore
    public static /* final */ int actionButtonStyle; // = 2130968656;
    @DexIgnore
    public static /* final */ int actionDropDownStyle; // = 2130968657;
    @DexIgnore
    public static /* final */ int actionLayout; // = 2130968658;
    @DexIgnore
    public static /* final */ int actionMenuTextAppearance; // = 2130968659;
    @DexIgnore
    public static /* final */ int actionMenuTextColor; // = 2130968660;
    @DexIgnore
    public static /* final */ int actionModeBackground; // = 2130968661;
    @DexIgnore
    public static /* final */ int actionModeCloseButtonStyle; // = 2130968662;
    @DexIgnore
    public static /* final */ int actionModeCloseDrawable; // = 2130968663;
    @DexIgnore
    public static /* final */ int actionModeCopyDrawable; // = 2130968664;
    @DexIgnore
    public static /* final */ int actionModeCutDrawable; // = 2130968665;
    @DexIgnore
    public static /* final */ int actionModeFindDrawable; // = 2130968666;
    @DexIgnore
    public static /* final */ int actionModePasteDrawable; // = 2130968667;
    @DexIgnore
    public static /* final */ int actionModePopupWindowStyle; // = 2130968668;
    @DexIgnore
    public static /* final */ int actionModeSelectAllDrawable; // = 2130968669;
    @DexIgnore
    public static /* final */ int actionModeShareDrawable; // = 2130968670;
    @DexIgnore
    public static /* final */ int actionModeSplitBackground; // = 2130968671;
    @DexIgnore
    public static /* final */ int actionModeStyle; // = 2130968672;
    @DexIgnore
    public static /* final */ int actionModeWebSearchDrawable; // = 2130968673;
    @DexIgnore
    public static /* final */ int actionOverflowButtonStyle; // = 2130968674;
    @DexIgnore
    public static /* final */ int actionOverflowMenuStyle; // = 2130968675;
    @DexIgnore
    public static /* final */ int actionProviderClass; // = 2130968676;
    @DexIgnore
    public static /* final */ int actionViewClass; // = 2130968677;
    @DexIgnore
    public static /* final */ int activityChooserViewStyle; // = 2130968686;
    @DexIgnore
    public static /* final */ int alertDialogButtonGroupStyle; // = 2130968736;
    @DexIgnore
    public static /* final */ int alertDialogCenterButtons; // = 2130968737;
    @DexIgnore
    public static /* final */ int alertDialogStyle; // = 2130968738;
    @DexIgnore
    public static /* final */ int alertDialogTheme; // = 2130968739;
    @DexIgnore
    public static /* final */ int allowStacking; // = 2130968742;
    @DexIgnore
    public static /* final */ int alpha; // = 2130968743;
    @DexIgnore
    public static /* final */ int alphabeticModifiers; // = 2130968747;
    @DexIgnore
    public static /* final */ int arrowHeadLength; // = 2130968749;
    @DexIgnore
    public static /* final */ int arrowShaftLength; // = 2130968750;
    @DexIgnore
    public static /* final */ int autoCompleteTextViewStyle; // = 2130968751;
    @DexIgnore
    public static /* final */ int autoSizeMaxTextSize; // = 2130968753;
    @DexIgnore
    public static /* final */ int autoSizeMinTextSize; // = 2130968754;
    @DexIgnore
    public static /* final */ int autoSizePresetSizes; // = 2130968755;
    @DexIgnore
    public static /* final */ int autoSizeStepGranularity; // = 2130968756;
    @DexIgnore
    public static /* final */ int autoSizeTextType; // = 2130968757;
    @DexIgnore
    public static /* final */ int background; // = 2130968761;
    @DexIgnore
    public static /* final */ int backgroundSplit; // = 2130968763;
    @DexIgnore
    public static /* final */ int backgroundStacked; // = 2130968764;
    @DexIgnore
    public static /* final */ int backgroundTint; // = 2130968766;
    @DexIgnore
    public static /* final */ int backgroundTintMode; // = 2130968767;
    @DexIgnore
    public static /* final */ int barLength; // = 2130968768;
    @DexIgnore
    public static /* final */ int borderlessButtonStyle; // = 2130968843;
    @DexIgnore
    public static /* final */ int buttonBarButtonStyle; // = 2130968859;
    @DexIgnore
    public static /* final */ int buttonBarNegativeButtonStyle; // = 2130968860;
    @DexIgnore
    public static /* final */ int buttonBarNeutralButtonStyle; // = 2130968861;
    @DexIgnore
    public static /* final */ int buttonBarPositiveButtonStyle; // = 2130968862;
    @DexIgnore
    public static /* final */ int buttonBarStyle; // = 2130968863;
    @DexIgnore
    public static /* final */ int buttonGravity; // = 2130968864;
    @DexIgnore
    public static /* final */ int buttonIconDimen; // = 2130968865;
    @DexIgnore
    public static /* final */ int buttonPanelSideLayout; // = 2130968866;
    @DexIgnore
    public static /* final */ int buttonStyle; // = 2130968874;
    @DexIgnore
    public static /* final */ int buttonStyleSmall; // = 2130968875;
    @DexIgnore
    public static /* final */ int buttonTint; // = 2130968876;
    @DexIgnore
    public static /* final */ int buttonTintMode; // = 2130968877;
    @DexIgnore
    public static /* final */ int checkboxStyle; // = 2130968913;
    @DexIgnore
    public static /* final */ int checkedTextViewStyle; // = 2130968918;
    @DexIgnore
    public static /* final */ int closeIcon; // = 2130968953;
    @DexIgnore
    public static /* final */ int closeItemLayout; // = 2130968960;
    @DexIgnore
    public static /* final */ int collapseContentDescription; // = 2130968962;
    @DexIgnore
    public static /* final */ int collapseIcon; // = 2130968963;
    @DexIgnore
    public static /* final */ int color; // = 2130968968;
    @DexIgnore
    public static /* final */ int colorAccent; // = 2130968969;
    @DexIgnore
    public static /* final */ int colorBackgroundFloating; // = 2130968971;
    @DexIgnore
    public static /* final */ int colorButtonNormal; // = 2130968972;
    @DexIgnore
    public static /* final */ int colorControlActivated; // = 2130968973;
    @DexIgnore
    public static /* final */ int colorControlHighlight; // = 2130968974;
    @DexIgnore
    public static /* final */ int colorControlNormal; // = 2130968975;
    @DexIgnore
    public static /* final */ int colorError; // = 2130968976;
    @DexIgnore
    public static /* final */ int colorPrimary; // = 2130968977;
    @DexIgnore
    public static /* final */ int colorPrimaryDark; // = 2130968978;
    @DexIgnore
    public static /* final */ int colorSwitchThumbNormal; // = 2130968981;
    @DexIgnore
    public static /* final */ int commitIcon; // = 2130968994;
    @DexIgnore
    public static /* final */ int contentDescription; // = 2130969009;
    @DexIgnore
    public static /* final */ int contentInsetEnd; // = 2130969010;
    @DexIgnore
    public static /* final */ int contentInsetEndWithActions; // = 2130969011;
    @DexIgnore
    public static /* final */ int contentInsetLeft; // = 2130969012;
    @DexIgnore
    public static /* final */ int contentInsetRight; // = 2130969013;
    @DexIgnore
    public static /* final */ int contentInsetStart; // = 2130969014;
    @DexIgnore
    public static /* final */ int contentInsetStartWithNavigation; // = 2130969015;
    @DexIgnore
    public static /* final */ int controlBackground; // = 2130969022;
    @DexIgnore
    public static /* final */ int coordinatorLayoutStyle; // = 2130969039;
    @DexIgnore
    public static /* final */ int customNavigationLayout; // = 2130969047;
    @DexIgnore
    public static /* final */ int defaultQueryHint; // = 2130969089;
    @DexIgnore
    public static /* final */ int dialogCornerRadius; // = 2130969113;
    @DexIgnore
    public static /* final */ int dialogPreferredPadding; // = 2130969114;
    @DexIgnore
    public static /* final */ int dialogTheme; // = 2130969115;
    @DexIgnore
    public static /* final */ int displayOptions; // = 2130969116;
    @DexIgnore
    public static /* final */ int divider; // = 2130969118;
    @DexIgnore
    public static /* final */ int dividerHorizontal; // = 2130969119;
    @DexIgnore
    public static /* final */ int dividerPadding; // = 2130969120;
    @DexIgnore
    public static /* final */ int dividerVertical; // = 2130969121;
    @DexIgnore
    public static /* final */ int drawableSize; // = 2130969124;
    @DexIgnore
    public static /* final */ int drawerArrowStyle; // = 2130969125;
    @DexIgnore
    public static /* final */ int dropDownListViewStyle; // = 2130969126;
    @DexIgnore
    public static /* final */ int dropdownListPreferredItemHeight; // = 2130969127;
    @DexIgnore
    public static /* final */ int editTextBackground; // = 2130969128;
    @DexIgnore
    public static /* final */ int editTextColor; // = 2130969129;
    @DexIgnore
    public static /* final */ int editTextStyle; // = 2130969131;
    @DexIgnore
    public static /* final */ int elevation; // = 2130969139;
    @DexIgnore
    public static /* final */ int expandActivityOverflowButtonDrawable; // = 2130969156;
    @DexIgnore
    public static /* final */ int firstBaselineToTopHeight; // = 2130969180;
    @DexIgnore
    public static /* final */ int font; // = 2130969182;
    @DexIgnore
    public static /* final */ int fontFamily; // = 2130969183;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969187;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969188;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969189;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969190;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969191;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969192;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969193;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969194;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969195;
    @DexIgnore
    public static /* final */ int gapBetweenBars; // = 2130969202;
    @DexIgnore
    public static /* final */ int goIcon; // = 2130969204;
    @DexIgnore
    public static /* final */ int height; // = 2130969235;
    @DexIgnore
    public static /* final */ int hideOnContentScroll; // = 2130969242;
    @DexIgnore
    public static /* final */ int homeAsUpIndicator; // = 2130969248;
    @DexIgnore
    public static /* final */ int homeLayout; // = 2130969249;
    @DexIgnore
    public static /* final */ int icon; // = 2130969251;
    @DexIgnore
    public static /* final */ int iconTint; // = 2130969257;
    @DexIgnore
    public static /* final */ int iconTintMode; // = 2130969258;
    @DexIgnore
    public static /* final */ int iconifiedByDefault; // = 2130969264;
    @DexIgnore
    public static /* final */ int imageButtonStyle; // = 2130969268;
    @DexIgnore
    public static /* final */ int indeterminateProgressStyle; // = 2130969272;
    @DexIgnore
    public static /* final */ int initialActivityCount; // = 2130969283;
    @DexIgnore
    public static /* final */ int isLightTheme; // = 2130969291;
    @DexIgnore
    public static /* final */ int itemPadding; // = 2130969302;
    @DexIgnore
    public static /* final */ int keylines; // = 2130969308;
    @DexIgnore
    public static /* final */ int lastBaselineToBottomHeight; // = 2130969310;
    @DexIgnore
    public static /* final */ int layout; // = 2130969315;
    @DexIgnore
    public static /* final */ int layout_anchor; // = 2130969317;
    @DexIgnore
    public static /* final */ int layout_anchorGravity; // = 2130969318;
    @DexIgnore
    public static /* final */ int layout_behavior; // = 2130969320;
    @DexIgnore
    public static /* final */ int layout_dodgeInsetEdges; // = 2130969364;
    @DexIgnore
    public static /* final */ int layout_insetEdge; // = 2130969374;
    @DexIgnore
    public static /* final */ int layout_keyline; // = 2130969375;
    @DexIgnore
    public static /* final */ int lineHeight; // = 2130969391;
    @DexIgnore
    public static /* final */ int listChoiceBackgroundIndicator; // = 2130969394;
    @DexIgnore
    public static /* final */ int listDividerAlertDialog; // = 2130969395;
    @DexIgnore
    public static /* final */ int listItemLayout; // = 2130969396;
    @DexIgnore
    public static /* final */ int listLayout; // = 2130969397;
    @DexIgnore
    public static /* final */ int listMenuViewStyle; // = 2130969398;
    @DexIgnore
    public static /* final */ int listPopupWindowStyle; // = 2130969399;
    @DexIgnore
    public static /* final */ int listPreferredItemHeight; // = 2130969400;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightLarge; // = 2130969401;
    @DexIgnore
    public static /* final */ int listPreferredItemHeightSmall; // = 2130969402;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingLeft; // = 2130969403;
    @DexIgnore
    public static /* final */ int listPreferredItemPaddingRight; // = 2130969404;
    @DexIgnore
    public static /* final */ int logo; // = 2130969409;
    @DexIgnore
    public static /* final */ int logoDescription; // = 2130969410;
    @DexIgnore
    public static /* final */ int maxButtonHeight; // = 2130969426;
    @DexIgnore
    public static /* final */ int measureWithLargestChild; // = 2130969430;
    @DexIgnore
    public static /* final */ int multiChoiceItemLayout; // = 2130969443;
    @DexIgnore
    public static /* final */ int navigationContentDescription; // = 2130969448;
    @DexIgnore
    public static /* final */ int navigationIcon; // = 2130969449;
    @DexIgnore
    public static /* final */ int navigationMode; // = 2130969450;
    @DexIgnore
    public static /* final */ int numericModifiers; // = 2130969466;
    @DexIgnore
    public static /* final */ int overlapAnchor; // = 2130969467;
    @DexIgnore
    public static /* final */ int paddingBottomNoButtons; // = 2130969468;
    @DexIgnore
    public static /* final */ int paddingEnd; // = 2130969469;
    @DexIgnore
    public static /* final */ int paddingStart; // = 2130969471;
    @DexIgnore
    public static /* final */ int paddingTopNoTitle; // = 2130969472;
    @DexIgnore
    public static /* final */ int panelBackground; // = 2130969474;
    @DexIgnore
    public static /* final */ int panelMenuListTheme; // = 2130969475;
    @DexIgnore
    public static /* final */ int panelMenuListWidth; // = 2130969476;
    @DexIgnore
    public static /* final */ int popupMenuStyle; // = 2130969483;
    @DexIgnore
    public static /* final */ int popupTheme; // = 2130969484;
    @DexIgnore
    public static /* final */ int popupWindowStyle; // = 2130969485;
    @DexIgnore
    public static /* final */ int preserveIconSpacing; // = 2130969486;
    @DexIgnore
    public static /* final */ int progressBarPadding; // = 2130969496;
    @DexIgnore
    public static /* final */ int progressBarStyle; // = 2130969497;
    @DexIgnore
    public static /* final */ int queryBackground; // = 2130969510;
    @DexIgnore
    public static /* final */ int queryHint; // = 2130969511;
    @DexIgnore
    public static /* final */ int radioButtonStyle; // = 2130969512;
    @DexIgnore
    public static /* final */ int ratingBarStyle; // = 2130969514;
    @DexIgnore
    public static /* final */ int ratingBarStyleIndicator; // = 2130969515;
    @DexIgnore
    public static /* final */ int ratingBarStyleSmall; // = 2130969516;
    @DexIgnore
    public static /* final */ int searchHintIcon; // = 2130969555;
    @DexIgnore
    public static /* final */ int searchIcon; // = 2130969556;
    @DexIgnore
    public static /* final */ int searchViewStyle; // = 2130969558;
    @DexIgnore
    public static /* final */ int seekBarStyle; // = 2130969559;
    @DexIgnore
    public static /* final */ int selectableItemBackground; // = 2130969560;
    @DexIgnore
    public static /* final */ int selectableItemBackgroundBorderless; // = 2130969561;
    @DexIgnore
    public static /* final */ int showAsAction; // = 2130969583;
    @DexIgnore
    public static /* final */ int showDividers; // = 2130969584;
    @DexIgnore
    public static /* final */ int showText; // = 2130969586;
    @DexIgnore
    public static /* final */ int showTitle; // = 2130969587;
    @DexIgnore
    public static /* final */ int singleChoiceItemLayout; // = 2130969588;
    @DexIgnore
    public static /* final */ int spinBars; // = 2130969654;
    @DexIgnore
    public static /* final */ int spinnerDropDownItemStyle; // = 2130969655;
    @DexIgnore
    public static /* final */ int spinnerStyle; // = 2130969656;
    @DexIgnore
    public static /* final */ int splitTrack; // = 2130969657;
    @DexIgnore
    public static /* final */ int srcCompat; // = 2130969658;
    @DexIgnore
    public static /* final */ int state_above_anchor; // = 2130969662;
    @DexIgnore
    public static /* final */ int statusBarBackground; // = 2130969672;
    @DexIgnore
    public static /* final */ int subMenuArrow; // = 2130969682;
    @DexIgnore
    public static /* final */ int submitBackground; // = 2130969684;
    @DexIgnore
    public static /* final */ int subtitle; // = 2130969685;
    @DexIgnore
    public static /* final */ int subtitleTextAppearance; // = 2130969686;
    @DexIgnore
    public static /* final */ int subtitleTextColor; // = 2130969687;
    @DexIgnore
    public static /* final */ int subtitleTextStyle; // = 2130969688;
    @DexIgnore
    public static /* final */ int suggestionRowLayout; // = 2130969689;
    @DexIgnore
    public static /* final */ int switchMinWidth; // = 2130969693;
    @DexIgnore
    public static /* final */ int switchPadding; // = 2130969694;
    @DexIgnore
    public static /* final */ int switchStyle; // = 2130969695;
    @DexIgnore
    public static /* final */ int switchTextAppearance; // = 2130969696;
    @DexIgnore
    public static /* final */ int textAllCaps; // = 2130969733;
    @DexIgnore
    public static /* final */ int textAppearanceLargePopupMenu; // = 2130969744;
    @DexIgnore
    public static /* final */ int textAppearanceListItem; // = 2130969745;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSecondary; // = 2130969746;
    @DexIgnore
    public static /* final */ int textAppearanceListItemSmall; // = 2130969747;
    @DexIgnore
    public static /* final */ int textAppearancePopupMenuHeader; // = 2130969749;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultSubtitle; // = 2130969750;
    @DexIgnore
    public static /* final */ int textAppearanceSearchResultTitle; // = 2130969751;
    @DexIgnore
    public static /* final */ int textAppearanceSmallPopupMenu; // = 2130969752;
    @DexIgnore
    public static /* final */ int textColorAlertDialogListItem; // = 2130969764;
    @DexIgnore
    public static /* final */ int textColorSearchUrl; // = 2130969765;
    @DexIgnore
    public static /* final */ int theme; // = 2130969791;
    @DexIgnore
    public static /* final */ int thickness; // = 2130969792;
    @DexIgnore
    public static /* final */ int thumbTextPadding; // = 2130969793;
    @DexIgnore
    public static /* final */ int thumbTint; // = 2130969794;
    @DexIgnore
    public static /* final */ int thumbTintMode; // = 2130969795;
    @DexIgnore
    public static /* final */ int tickMark; // = 2130969800;
    @DexIgnore
    public static /* final */ int tickMarkTint; // = 2130969801;
    @DexIgnore
    public static /* final */ int tickMarkTintMode; // = 2130969802;
    @DexIgnore
    public static /* final */ int tint; // = 2130969803;
    @DexIgnore
    public static /* final */ int tintMode; // = 2130969804;
    @DexIgnore
    public static /* final */ int title; // = 2130969806;
    @DexIgnore
    public static /* final */ int titleMargin; // = 2130969808;
    @DexIgnore
    public static /* final */ int titleMarginBottom; // = 2130969809;
    @DexIgnore
    public static /* final */ int titleMarginEnd; // = 2130969810;
    @DexIgnore
    public static /* final */ int titleMarginStart; // = 2130969811;
    @DexIgnore
    public static /* final */ int titleMarginTop; // = 2130969812;
    @DexIgnore
    public static /* final */ int titleMargins; // = 2130969813;
    @DexIgnore
    public static /* final */ int titleTextAppearance; // = 2130969814;
    @DexIgnore
    public static /* final */ int titleTextColor; // = 2130969815;
    @DexIgnore
    public static /* final */ int titleTextStyle; // = 2130969816;
    @DexIgnore
    public static /* final */ int toolbarNavigationButtonStyle; // = 2130969824;
    @DexIgnore
    public static /* final */ int toolbarStyle; // = 2130969825;
    @DexIgnore
    public static /* final */ int tooltipForegroundColor; // = 2130969826;
    @DexIgnore
    public static /* final */ int tooltipFrameBackground; // = 2130969827;
    @DexIgnore
    public static /* final */ int tooltipText; // = 2130969828;
    @DexIgnore
    public static /* final */ int track; // = 2130969830;
    @DexIgnore
    public static /* final */ int trackTint; // = 2130969831;
    @DexIgnore
    public static /* final */ int trackTintMode; // = 2130969832;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969834;
    @DexIgnore
    public static /* final */ int viewInflaterClass; // = 2130969853;
    @DexIgnore
    public static /* final */ int voiceIcon; // = 2130969869;
    @DexIgnore
    public static /* final */ int windowActionBar; // = 2130969934;
    @DexIgnore
    public static /* final */ int windowActionBarOverlay; // = 2130969935;
    @DexIgnore
    public static /* final */ int windowActionModeOverlay; // = 2130969936;
    @DexIgnore
    public static /* final */ int windowFixedHeightMajor; // = 2130969937;
    @DexIgnore
    public static /* final */ int windowFixedHeightMinor; // = 2130969938;
    @DexIgnore
    public static /* final */ int windowFixedWidthMajor; // = 2130969939;
    @DexIgnore
    public static /* final */ int windowFixedWidthMinor; // = 2130969940;
    @DexIgnore
    public static /* final */ int windowMinWidthMajor; // = 2130969941;
    @DexIgnore
    public static /* final */ int windowMinWidthMinor; // = 2130969942;
    @DexIgnore
    public static /* final */ int windowNoTitle; // = 2130969943;
}
