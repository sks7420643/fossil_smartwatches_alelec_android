package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s73 implements Factory<zb3> {
    @DexIgnore
    public static zb3 a(n73 n73) {
        zb3 e = n73.e();
        n44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
