package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vh1<V> extends FutureTask<V> implements Comparable<vh1> {
    @DexIgnore
    public /* final */ long e; // = th1.l.getAndIncrement();
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ th1 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vh1(th1 th1, Callable<V> callable, boolean z, String str) {
        super(callable);
        this.h = th1;
        bk0.a(str);
        this.g = str;
        this.f = z;
        if (this.e == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
            th1.d().s().a("Tasks index overflow");
        }
    }

    @DexIgnore
    public final /* synthetic */ int compareTo(Object obj) {
        vh1 vh1 = (vh1) obj;
        boolean z = this.f;
        if (z != vh1.f) {
            return z ? -1 : 1;
        }
        long j = this.e;
        long j2 = vh1.e;
        if (j < j2) {
            return -1;
        }
        if (j > j2) {
            return 1;
        }
        this.h.d().t().a("Two tasks share the same index. index", Long.valueOf(this.e));
        return 0;
    }

    @DexIgnore
    public final void setException(Throwable th) {
        this.h.d().s().a(this.g, th);
        super.setException(th);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vh1(th1 th1, Runnable runnable, boolean z, String str) {
        super(runnable, (Object) null);
        this.h = th1;
        bk0.a(str);
        this.g = str;
        this.f = false;
        if (this.e == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
            th1.d().s().a("Tasks index overflow");
        }
    }
}
