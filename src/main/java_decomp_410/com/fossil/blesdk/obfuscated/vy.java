package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vy {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ e74 b;

    @DexIgnore
    public vy(String str, e74 e74) {
        this.a = str;
        this.b = e74;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            y44 g = q44.g();
            g.e("CrashlyticsCore", "Error creating marker: " + this.a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.a(), this.a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
