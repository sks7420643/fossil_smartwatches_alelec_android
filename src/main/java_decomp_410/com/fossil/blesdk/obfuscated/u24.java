package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ k04 f;

    @DexIgnore
    public u24(Context context, k04 k04) {
        this.e = context;
        this.f = k04;
    }

    @DexIgnore
    public final void run() {
        Context context = this.e;
        if (context == null) {
            j04.m.d("The Context of StatService.onPause() can not be null!");
        } else {
            j04.b(context, e24.k(context), this.f);
        }
    }
}
