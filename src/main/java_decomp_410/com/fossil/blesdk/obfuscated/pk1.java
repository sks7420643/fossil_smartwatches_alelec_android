package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pk1 extends tj1 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public pk1(xh1 xh1) {
        super(xh1);
        this.a.a(this);
    }

    @DexIgnore
    public final boolean u() {
        return this.b;
    }

    @DexIgnore
    public final void v() {
        if (!u()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void w() {
        if (!this.b) {
            y();
            this.a.G();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean x();

    @DexIgnore
    public void y() {
    }

    @DexIgnore
    public final void z() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!x()) {
            this.a.G();
            this.b = true;
        }
    }
}
