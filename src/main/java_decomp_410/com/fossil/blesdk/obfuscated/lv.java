package com.fossil.blesdk.obfuscated;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.lv;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lv<T extends lv<T>> implements Cloneable {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C; // = true;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f; // = 1.0f;
    @DexIgnore
    public pp g; // = pp.d;
    @DexIgnore
    public Priority h; // = Priority.NORMAL;
    @DexIgnore
    public Drawable i;
    @DexIgnore
    public int j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public int n; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public jo p; // = iw.a();
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public Drawable s;
    @DexIgnore
    public int t;
    @DexIgnore
    public lo u; // = new lo();
    @DexIgnore
    public Map<Class<?>, oo<?>> v; // = new lw();
    @DexIgnore
    public Class<?> w; // = Object.class;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public Resources.Theme y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public static boolean b(int i2, int i3) {
        return (i2 & i3) != 0;
    }

    @DexIgnore
    public final float A() {
        return this.f;
    }

    @DexIgnore
    public final Resources.Theme B() {
        return this.y;
    }

    @DexIgnore
    public final Map<Class<?>, oo<?>> C() {
        return this.v;
    }

    @DexIgnore
    public final boolean D() {
        return this.D;
    }

    @DexIgnore
    public final boolean E() {
        return this.A;
    }

    @DexIgnore
    public final boolean F() {
        return a(4);
    }

    @DexIgnore
    public final boolean G() {
        return this.m;
    }

    @DexIgnore
    public final boolean H() {
        return a(8);
    }

    @DexIgnore
    public boolean I() {
        return this.C;
    }

    @DexIgnore
    public final boolean J() {
        return a(256);
    }

    @DexIgnore
    public final boolean K() {
        return this.r;
    }

    @DexIgnore
    public final boolean L() {
        return this.q;
    }

    @DexIgnore
    public final boolean M() {
        return a(2048);
    }

    @DexIgnore
    public final boolean N() {
        return uw.b(this.o, this.n);
    }

    @DexIgnore
    public T O() {
        this.x = true;
        S();
        return this;
    }

    @DexIgnore
    public T P() {
        return b(DownsampleStrategy.c, (oo<Bitmap>) new ts());
    }

    @DexIgnore
    public T Q() {
        return a(DownsampleStrategy.b, (oo<Bitmap>) new us());
    }

    @DexIgnore
    public T R() {
        return a(DownsampleStrategy.a, (oo<Bitmap>) new at());
    }

    @DexIgnore
    public final T S() {
        return this;
    }

    @DexIgnore
    public final T T() {
        if (!this.x) {
            S();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    public T a(float f2) {
        if (this.z) {
            return clone().a(f2);
        }
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f2 > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.f = f2;
        this.e |= 2;
        T();
        return this;
    }

    @DexIgnore
    public T b(boolean z2) {
        if (this.z) {
            return clone().b(z2);
        }
        this.D = z2;
        this.e |= 1048576;
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T c(DownsampleStrategy downsampleStrategy, oo<Bitmap> r3) {
        return a(downsampleStrategy, (oo<Bitmap>) r3, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T d(DownsampleStrategy downsampleStrategy, oo<Bitmap> r3) {
        if (this.z) {
            return clone().d(downsampleStrategy, r3);
        }
        a(downsampleStrategy);
        return a((oo<Bitmap>) r3);
    }

    @DexIgnore
    public final int e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof lv)) {
            return false;
        }
        lv lvVar = (lv) obj;
        if (Float.compare(lvVar.f, this.f) == 0 && this.j == lvVar.j && uw.b((Object) this.i, (Object) lvVar.i) && this.l == lvVar.l && uw.b((Object) this.k, (Object) lvVar.k) && this.t == lvVar.t && uw.b((Object) this.s, (Object) lvVar.s) && this.m == lvVar.m && this.n == lvVar.n && this.o == lvVar.o && this.q == lvVar.q && this.r == lvVar.r && this.A == lvVar.A && this.B == lvVar.B && this.g.equals(lvVar.g) && this.h == lvVar.h && this.u.equals(lvVar.u) && this.v.equals(lvVar.v) && this.w.equals(lvVar.w) && uw.b((Object) this.p, (Object) lvVar.p) && uw.b((Object) this.y, (Object) lvVar.y)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final Drawable f() {
        return this.i;
    }

    @DexIgnore
    public final Drawable g() {
        return this.s;
    }

    @DexIgnore
    public final int h() {
        return this.t;
    }

    @DexIgnore
    public int hashCode() {
        return uw.a((Object) this.y, uw.a((Object) this.p, uw.a((Object) this.w, uw.a((Object) this.v, uw.a((Object) this.u, uw.a((Object) this.h, uw.a((Object) this.g, uw.a(this.B, uw.a(this.A, uw.a(this.r, uw.a(this.q, uw.a(this.o, uw.a(this.n, uw.a(this.m, uw.a((Object) this.s, uw.a(this.t, uw.a((Object) this.k, uw.a(this.l, uw.a((Object) this.i, uw.a(this.j, uw.a(this.f)))))))))))))))))))));
    }

    @DexIgnore
    public final boolean i() {
        return this.B;
    }

    @DexIgnore
    public final lo j() {
        return this.u;
    }

    @DexIgnore
    public final int k() {
        return this.n;
    }

    @DexIgnore
    public final int l() {
        return this.o;
    }

    @DexIgnore
    public final Drawable m() {
        return this.k;
    }

    @DexIgnore
    public final int w() {
        return this.l;
    }

    @DexIgnore
    public final Priority x() {
        return this.h;
    }

    @DexIgnore
    public final Class<?> y() {
        return this.w;
    }

    @DexIgnore
    public final jo z() {
        return this.p;
    }

    @DexIgnore
    public T c() {
        return a(au.b, true);
    }

    @DexIgnore
    public T clone() {
        try {
            T t2 = (lv) super.clone();
            t2.u = new lo();
            t2.u.a(this.u);
            t2.v = new lw();
            t2.v.putAll(this.v);
            t2.x = false;
            t2.z = false;
            return t2;
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    @DexIgnore
    public final pp d() {
        return this.g;
    }

    @DexIgnore
    public T b(int i2) {
        if (this.z) {
            return clone().b(i2);
        }
        this.l = i2;
        this.e |= 128;
        this.k = null;
        this.e &= -65;
        T();
        return this;
    }

    @DexIgnore
    public T a(pp ppVar) {
        if (this.z) {
            return clone().a(ppVar);
        }
        tw.a(ppVar);
        this.g = ppVar;
        this.e |= 4;
        T();
        return this;
    }

    @DexIgnore
    public T a(Priority priority) {
        if (this.z) {
            return clone().a(priority);
        }
        tw.a(priority);
        this.h = priority;
        this.e |= 8;
        T();
        return this;
    }

    @DexIgnore
    public T b() {
        return c(DownsampleStrategy.b, new us());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T b(DownsampleStrategy downsampleStrategy, oo<Bitmap> r3) {
        if (this.z) {
            return clone().b(downsampleStrategy, (oo<Bitmap>) r3);
        }
        a(downsampleStrategy);
        return a((oo<Bitmap>) r3, false);
    }

    @DexIgnore
    public T a(boolean z2) {
        if (this.z) {
            return clone().a(true);
        }
        this.m = !z2;
        this.e |= 256;
        T();
        return this;
    }

    @DexIgnore
    public T a(int i2, int i3) {
        if (this.z) {
            return clone().a(i2, i3);
        }
        this.o = i2;
        this.n = i3;
        this.e |= RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN;
        T();
        return this;
    }

    @DexIgnore
    public T a(jo joVar) {
        if (this.z) {
            return clone().a(joVar);
        }
        tw.a(joVar);
        this.p = joVar;
        this.e |= 1024;
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.ko<Y>, java.lang.Object, com.fossil.blesdk.obfuscated.ko] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public <Y> T a(ko<Y> r2, Y y2) {
        if (this.z) {
            return clone().a(r2, y2);
        }
        tw.a(r2);
        tw.a(y2);
        this.u.a(r2, y2);
        T();
        return this;
    }

    @DexIgnore
    public T a(Class<?> cls) {
        if (this.z) {
            return clone().a(cls);
        }
        tw.a(cls);
        this.w = cls;
        this.e |= 4096;
        T();
        return this;
    }

    @DexIgnore
    public T a(DownsampleStrategy downsampleStrategy) {
        ko koVar = DownsampleStrategy.f;
        tw.a(downsampleStrategy);
        return a(koVar, downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(DownsampleStrategy downsampleStrategy, oo<Bitmap> r3) {
        return a(downsampleStrategy, (oo<Bitmap>) r3, false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final T a(DownsampleStrategy downsampleStrategy, oo<Bitmap> r2, boolean z2) {
        T t2;
        if (z2) {
            t2 = d(downsampleStrategy, r2);
        } else {
            t2 = b(downsampleStrategy, (oo<Bitmap>) r2);
        }
        t2.C = true;
        return t2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(oo<Bitmap> r2) {
        return a((oo<Bitmap>) r2, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public T a(oo<Bitmap> r3, boolean z2) {
        if (this.z) {
            return clone().a((oo<Bitmap>) r3, z2);
        }
        ys ysVar = new ys(r3, z2);
        a(Bitmap.class, r3, z2);
        a(Drawable.class, ysVar, z2);
        ysVar.a();
        a(BitmapDrawable.class, ysVar, z2);
        a(ut.class, new xt(r3), z2);
        T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<Y>, java.lang.Object, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 2 */
    public <Y> T a(Class<Y> r2, oo<Y> r3, boolean z2) {
        if (this.z) {
            return clone().a(r2, r3, z2);
        }
        tw.a(r2);
        tw.a(r3);
        this.v.put(r2, r3);
        this.e |= 2048;
        this.r = true;
        this.e |= 65536;
        this.C = false;
        if (z2) {
            this.e |= 131072;
            this.q = true;
        }
        T();
        return this;
    }

    @DexIgnore
    public T a(lv<?> lvVar) {
        if (this.z) {
            return clone().a(lvVar);
        }
        if (b(lvVar.e, 2)) {
            this.f = lvVar.f;
        }
        if (b(lvVar.e, 262144)) {
            this.A = lvVar.A;
        }
        if (b(lvVar.e, 1048576)) {
            this.D = lvVar.D;
        }
        if (b(lvVar.e, 4)) {
            this.g = lvVar.g;
        }
        if (b(lvVar.e, 8)) {
            this.h = lvVar.h;
        }
        if (b(lvVar.e, 16)) {
            this.i = lvVar.i;
            this.j = 0;
            this.e &= -33;
        }
        if (b(lvVar.e, 32)) {
            this.j = lvVar.j;
            this.i = null;
            this.e &= -17;
        }
        if (b(lvVar.e, 64)) {
            this.k = lvVar.k;
            this.l = 0;
            this.e &= -129;
        }
        if (b(lvVar.e, 128)) {
            this.l = lvVar.l;
            this.k = null;
            this.e &= -65;
        }
        if (b(lvVar.e, 256)) {
            this.m = lvVar.m;
        }
        if (b(lvVar.e, (int) RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN)) {
            this.o = lvVar.o;
            this.n = lvVar.n;
        }
        if (b(lvVar.e, 1024)) {
            this.p = lvVar.p;
        }
        if (b(lvVar.e, 4096)) {
            this.w = lvVar.w;
        }
        if (b(lvVar.e, 8192)) {
            this.s = lvVar.s;
            this.t = 0;
            this.e &= -16385;
        }
        if (b(lvVar.e, (int) RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE)) {
            this.t = lvVar.t;
            this.s = null;
            this.e &= -8193;
        }
        if (b(lvVar.e, 32768)) {
            this.y = lvVar.y;
        }
        if (b(lvVar.e, 65536)) {
            this.r = lvVar.r;
        }
        if (b(lvVar.e, 131072)) {
            this.q = lvVar.q;
        }
        if (b(lvVar.e, 2048)) {
            this.v.putAll(lvVar.v);
            this.C = lvVar.C;
        }
        if (b(lvVar.e, 524288)) {
            this.B = lvVar.B;
        }
        if (!this.r) {
            this.v.clear();
            this.e &= -2049;
            this.q = false;
            this.e &= -131073;
            this.C = true;
        }
        this.e |= lvVar.e;
        this.u.a(lvVar.u);
        T();
        return this;
    }

    @DexIgnore
    public T a() {
        if (!this.x || this.z) {
            this.z = true;
            return O();
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    public final boolean a(int i2) {
        return b(this.e, i2);
    }
}
