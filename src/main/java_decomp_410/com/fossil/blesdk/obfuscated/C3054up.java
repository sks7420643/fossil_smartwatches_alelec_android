package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.up */
public class C3054up {
    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2980tp mo16841a(java.lang.Object obj, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, java.lang.Class<?> cls, java.lang.Class<?> cls2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        com.fossil.blesdk.obfuscated.C2980tp tpVar = new com.fossil.blesdk.obfuscated.C2980tp(obj, joVar, i, i2, map, cls, cls2, loVar);
        return tpVar;
    }
}
