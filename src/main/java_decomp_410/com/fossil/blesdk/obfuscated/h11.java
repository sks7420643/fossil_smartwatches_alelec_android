package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class h11 extends j01 implements g11 {
    @DexIgnore
    public h11() {
        super("com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public static g11 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IStatusCallback");
        if (queryLocalInterface instanceof g11) {
            return (g11) queryLocalInterface;
        }
        return new i11(iBinder);
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        c((Status) y01.a(parcel, Status.CREATOR));
        return true;
    }
}
