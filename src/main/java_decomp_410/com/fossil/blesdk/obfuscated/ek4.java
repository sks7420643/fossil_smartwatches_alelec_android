package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ek4 {
    @DexIgnore
    public static final int a() {
        return fk4.a();
    }

    @DexIgnore
    public static final int a(String str, int i, int i2, int i3) {
        return gk4.a(str, i, i2, i3);
    }

    @DexIgnore
    public static final long a(String str, long j, long j2, long j3) {
        return gk4.a(str, j, j2, j3);
    }

    @DexIgnore
    public static final String a(String str) {
        return fk4.a(str);
    }

    @DexIgnore
    public static final boolean a(String str, boolean z) {
        return gk4.a(str, z);
    }
}
