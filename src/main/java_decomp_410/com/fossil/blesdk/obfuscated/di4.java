package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class di4 extends gi4<fi4> {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater j; // = AtomicIntegerFieldUpdater.newUpdater(di4.class, "_invoked");
    @DexIgnore
    public volatile int _invoked; // = 0;
    @DexIgnore
    public /* final */ xc4<Throwable, qa4> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public di4(fi4 fi4, xc4<? super Throwable, qa4> xc4) {
        super(fi4);
        kd4.b(fi4, "job");
        kd4.b(xc4, "handler");
        this.i = xc4;
    }

    @DexIgnore
    public void b(Throwable th) {
        if (j.compareAndSet(this, 0, 1)) {
            this.i.invoke(th);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancelling[" + dh4.a((Object) this) + '@' + dh4.b(this) + ']';
    }
}
