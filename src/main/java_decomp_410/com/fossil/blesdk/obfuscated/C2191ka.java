package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ka */
public class C2191ka {

    @DexIgnore
    /* renamed from: w */
    public static /* final */ android.view.animation.Interpolator f6727w; // = new com.fossil.blesdk.obfuscated.C2191ka.C2192a();

    @DexIgnore
    /* renamed from: a */
    public int f6728a;

    @DexIgnore
    /* renamed from: b */
    public int f6729b;

    @DexIgnore
    /* renamed from: c */
    public int f6730c; // = -1;

    @DexIgnore
    /* renamed from: d */
    public float[] f6731d;

    @DexIgnore
    /* renamed from: e */
    public float[] f6732e;

    @DexIgnore
    /* renamed from: f */
    public float[] f6733f;

    @DexIgnore
    /* renamed from: g */
    public float[] f6734g;

    @DexIgnore
    /* renamed from: h */
    public int[] f6735h;

    @DexIgnore
    /* renamed from: i */
    public int[] f6736i;

    @DexIgnore
    /* renamed from: j */
    public int[] f6737j;

    @DexIgnore
    /* renamed from: k */
    public int f6738k;

    @DexIgnore
    /* renamed from: l */
    public android.view.VelocityTracker f6739l;

    @DexIgnore
    /* renamed from: m */
    public float f6740m;

    @DexIgnore
    /* renamed from: n */
    public float f6741n;

    @DexIgnore
    /* renamed from: o */
    public int f6742o;

    @DexIgnore
    /* renamed from: p */
    public int f6743p;

    @DexIgnore
    /* renamed from: q */
    public android.widget.OverScroller f6744q;

    @DexIgnore
    /* renamed from: r */
    public /* final */ com.fossil.blesdk.obfuscated.C2191ka.C2194c f6745r;

    @DexIgnore
    /* renamed from: s */
    public android.view.View f6746s;

    @DexIgnore
    /* renamed from: t */
    public boolean f6747t;

    @DexIgnore
    /* renamed from: u */
    public /* final */ android.view.ViewGroup f6748u;

    @DexIgnore
    /* renamed from: v */
    public /* final */ java.lang.Runnable f6749v; // = new com.fossil.blesdk.obfuscated.C2191ka.C2193b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ka$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ka$a */
    public static class C2192a implements android.view.animation.Interpolator {
        @DexIgnore
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ka$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ka$b */
    public class C2193b implements java.lang.Runnable {
        @DexIgnore
        public C2193b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2191ka.this.mo12678f(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ka$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ka$c */
    public static abstract class C2194c {
        @DexIgnore
        /* renamed from: a */
        public int mo12684a(int i) {
            return i;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo1808a(android.view.View view) {
            return 0;
        }

        @DexIgnore
        /* renamed from: a */
        public abstract int mo1809a(android.view.View view, int i, int i2);

        @DexIgnore
        /* renamed from: a */
        public void mo1811a(int i, int i2) {
        }

        @DexIgnore
        /* renamed from: a */
        public abstract void mo1812a(android.view.View view, float f, float f2);

        @DexIgnore
        /* renamed from: a */
        public void mo1813a(android.view.View view, int i) {
        }

        @DexIgnore
        /* renamed from: a */
        public abstract void mo1814a(android.view.View view, int i, int i2, int i3, int i4);

        @DexIgnore
        /* renamed from: b */
        public int mo12685b(android.view.View view) {
            return 0;
        }

        @DexIgnore
        /* renamed from: b */
        public abstract int mo1816b(android.view.View view, int i, int i2);

        @DexIgnore
        /* renamed from: b */
        public void mo1818b(int i, int i2) {
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo1819b(int i) {
            return false;
        }

        @DexIgnore
        /* renamed from: b */
        public abstract boolean mo1820b(android.view.View view, int i);

        @DexIgnore
        /* renamed from: c */
        public abstract void mo1822c(int i);
    }

    @DexIgnore
    public C2191ka(android.content.Context context, android.view.ViewGroup viewGroup, com.fossil.blesdk.obfuscated.C2191ka.C2194c cVar) {
        if (viewGroup == null) {
            throw new java.lang.IllegalArgumentException("Parent view may not be null");
        } else if (cVar != null) {
            this.f6748u = viewGroup;
            this.f6745r = cVar;
            android.view.ViewConfiguration viewConfiguration = android.view.ViewConfiguration.get(context);
            this.f6742o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.f6729b = viewConfiguration.getScaledTouchSlop();
            this.f6740m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.f6741n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.f6744q = new android.widget.OverScroller(context, f6727w);
        } else {
            throw new java.lang.IllegalArgumentException("Callback may not be null");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2191ka m9409a(android.view.ViewGroup viewGroup, com.fossil.blesdk.obfuscated.C2191ka.C2194c cVar) {
        return new com.fossil.blesdk.obfuscated.C2191ka(viewGroup.getContext(), viewGroup, cVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12660b(float f) {
        this.f6741n = f;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo12668c() {
        float[] fArr = this.f6731d;
        if (fArr != null) {
            java.util.Arrays.fill(fArr, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            java.util.Arrays.fill(this.f6732e, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            java.util.Arrays.fill(this.f6733f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            java.util.Arrays.fill(this.f6734g, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            java.util.Arrays.fill(this.f6735h, 0);
            java.util.Arrays.fill(this.f6736i, 0);
            java.util.Arrays.fill(this.f6737j, 0);
            this.f6738k = 0;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public android.view.View mo12671d() {
        return this.f6746s;
    }

    @DexIgnore
    /* renamed from: e */
    public int mo12674e() {
        return this.f6742o;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo12677f() {
        return this.f6729b;
    }

    @DexIgnore
    /* renamed from: g */
    public int mo12679g() {
        return this.f6728a;
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo12681h() {
        this.f6739l.computeCurrentVelocity(1000, this.f6740m);
        mo12646a(mo12642a(this.f6739l.getXVelocity(this.f6730c), this.f6741n, this.f6740m), mo12642a(this.f6739l.getYVelocity(this.f6730c), this.f6741n, this.f6740m));
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2191ka m9408a(android.view.ViewGroup viewGroup, float f, com.fossil.blesdk.obfuscated.C2191ka.C2194c cVar) {
        com.fossil.blesdk.obfuscated.C2191ka a = m9409a(viewGroup, cVar);
        a.f6729b = (int) (((float) a.f6729b) * (1.0f / f));
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12659b() {
        this.f6730c = -1;
        mo12668c();
        android.view.VelocityTracker velocityTracker = this.f6739l;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f6739l = null;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo12672d(int i) {
        return ((1 << i) & this.f6738k) != 0;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo12676e(int i, int i2) {
        if (this.f6747t) {
            return mo12664b(i, i2, (int) this.f6739l.getXVelocity(this.f6730c), (int) this.f6739l.getYVelocity(this.f6730c));
        }
        throw new java.lang.IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    @DexIgnore
    /* renamed from: f */
    public void mo12678f(int i) {
        this.f6748u.removeCallbacks(this.f6749v);
        if (this.f6728a != i) {
            this.f6728a = i;
            this.f6745r.mo1822c(i);
            if (this.f6728a == 0) {
                this.f6746s = null;
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo12680g(int i) {
        this.f6743p = i;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo12673d(int i, int i2) {
        return mo12655a(this.f6746s, i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12650a(android.view.View view, int i) {
        if (view.getParent() == this.f6748u) {
            this.f6746s = view;
            this.f6730c = i;
            this.f6745r.mo1813a(view, i);
            mo12678f(1);
            return;
        }
        throw new java.lang.IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.f6748u + ")");
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo12666b(android.view.View view, int i, int i2) {
        this.f6746s = view;
        this.f6730c = -1;
        boolean b = mo12664b(i, i2, 0, 0);
        if (!b && this.f6728a == 0 && this.f6746s != null) {
            this.f6746s = null;
        }
        return b;
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo12675e(int i) {
        if (mo12672d(i)) {
            return true;
        }
        android.util.Log.e("ViewDragHelper", "Ignoring pointerId=" + i + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12645a() {
        mo12659b();
        if (this.f6728a == 2) {
            int currX = this.f6744q.getCurrX();
            int currY = this.f6744q.getCurrY();
            this.f6744q.abortAnimation();
            int currX2 = this.f6744q.getCurrX();
            int currY2 = this.f6744q.getCurrY();
            this.f6745r.mo1814a(this.f6746s, currX2, currY2, currX2 - currX, currY2 - currY);
        }
        mo12678f(0);
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo12669c(int i) {
        float[] fArr = this.f6731d;
        if (fArr == null || fArr.length <= i) {
            int i2 = i + 1;
            float[] fArr2 = new float[i2];
            float[] fArr3 = new float[i2];
            float[] fArr4 = new float[i2];
            float[] fArr5 = new float[i2];
            int[] iArr = new int[i2];
            int[] iArr2 = new int[i2];
            int[] iArr3 = new int[i2];
            float[] fArr6 = this.f6731d;
            if (fArr6 != null) {
                java.lang.System.arraycopy(fArr6, 0, fArr2, 0, fArr6.length);
                float[] fArr7 = this.f6732e;
                java.lang.System.arraycopy(fArr7, 0, fArr3, 0, fArr7.length);
                float[] fArr8 = this.f6733f;
                java.lang.System.arraycopy(fArr8, 0, fArr4, 0, fArr8.length);
                float[] fArr9 = this.f6734g;
                java.lang.System.arraycopy(fArr9, 0, fArr5, 0, fArr9.length);
                int[] iArr4 = this.f6735h;
                java.lang.System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.f6736i;
                java.lang.System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.f6737j;
                java.lang.System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.f6731d = fArr2;
            this.f6732e = fArr3;
            this.f6733f = fArr4;
            this.f6734g = fArr5;
            this.f6735h = iArr;
            this.f6736i = iArr2;
            this.f6737j = iArr3;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo12664b(int i, int i2, int i3, int i4) {
        int left = this.f6746s.getLeft();
        int top = this.f6746s.getTop();
        int i5 = i - left;
        int i6 = i2 - top;
        if (i5 == 0 && i6 == 0) {
            this.f6744q.abortAnimation();
            mo12678f(0);
            return false;
        }
        this.f6744q.startScroll(left, top, i5, i6, mo12644a(this.f6746s, i5, i6, i3, i4));
        mo12678f(2);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo12644a(android.view.View view, int i, int i2, int i3, int i4) {
        float f;
        float f2;
        float f3;
        float f4;
        int a = mo12643a(i3, (int) this.f6741n, (int) this.f6740m);
        int a2 = mo12643a(i4, (int) this.f6741n, (int) this.f6740m);
        int abs = java.lang.Math.abs(i);
        int abs2 = java.lang.Math.abs(i2);
        int abs3 = java.lang.Math.abs(a);
        int abs4 = java.lang.Math.abs(a2);
        int i5 = abs3 + abs4;
        int i6 = abs + abs2;
        if (a != 0) {
            f2 = (float) abs3;
            f = (float) i5;
        } else {
            f2 = (float) abs;
            f = (float) i6;
        }
        float f5 = f2 / f;
        if (a2 != 0) {
            f4 = (float) abs4;
            f3 = (float) i5;
        } else {
            f4 = (float) abs2;
            f3 = (float) i6;
        }
        float f6 = f4 / f3;
        return (int) ((((float) mo12657b(i, a, this.f6745r.mo1808a(view))) * f5) + (((float) mo12657b(i2, a2, this.f6745r.mo12685b(view))) * f6));
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo12657b(int i, int i2, int i3) {
        int i4;
        if (i == 0) {
            return 0;
        }
        int width = this.f6748u.getWidth();
        float f = (float) (width / 2);
        float a = f + (mo12641a(java.lang.Math.min(1.0f, ((float) java.lang.Math.abs(i)) / ((float) width))) * f);
        int abs = java.lang.Math.abs(i2);
        if (abs > 0) {
            i4 = java.lang.Math.round(java.lang.Math.abs(a / ((float) abs)) * 1000.0f) * 4;
        } else {
            i4 = (int) (((((float) java.lang.Math.abs(i)) / ((float) i3)) + 1.0f) * 256.0f);
        }
        return java.lang.Math.min(i4, 600);
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo12643a(int i, int i2, int i3) {
        int abs = java.lang.Math.abs(i);
        if (abs < i2) {
            return 0;
        }
        if (abs > i3) {
            return i > 0 ? i3 : -i3;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12662b(int i) {
        if (this.f6731d != null && mo12672d(i)) {
            this.f6731d[i] = 0.0f;
            this.f6732e[i] = 0.0f;
            this.f6733f[i] = 0.0f;
            this.f6734g[i] = 0.0f;
            this.f6735h[i] = 0;
            this.f6736i[i] = 0;
            this.f6737j[i] = 0;
            this.f6738k = (~(1 << i)) & this.f6738k;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo12642a(float f, float f2, float f3) {
        float abs = java.lang.Math.abs(f);
        if (abs < f2) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (abs > f3) {
            return f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? f3 : -f3;
        }
        return f;
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo12641a(float f) {
        return (float) java.lang.Math.sin((double) ((f - 0.5f) * 0.47123894f));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12656a(boolean z) {
        if (this.f6728a == 2) {
            boolean computeScrollOffset = this.f6744q.computeScrollOffset();
            int currX = this.f6744q.getCurrX();
            int currY = this.f6744q.getCurrY();
            int left = currX - this.f6746s.getLeft();
            int top = currY - this.f6746s.getTop();
            if (left != 0) {
                com.fossil.blesdk.obfuscated.C1776f9.m6831c(this.f6746s, left);
            }
            if (top != 0) {
                com.fossil.blesdk.obfuscated.C1776f9.m6834d(this.f6746s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.f6745r.mo1814a(this.f6746s, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.f6744q.getFinalX() && currY == this.f6744q.getFinalY()) {
                this.f6744q.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z) {
                    this.f6748u.post(this.f6749v);
                } else {
                    mo12678f(0);
                }
            }
        }
        if (this.f6728a == 2) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00dd, code lost:
        if (r12 != r11) goto L_0x00e6;
     */
    @DexIgnore
    /* renamed from: c */
    public boolean mo12670c(android.view.MotionEvent motionEvent) {
        boolean z;
        android.view.MotionEvent motionEvent2 = motionEvent;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            mo12659b();
        }
        if (this.f6739l == null) {
            this.f6739l = android.view.VelocityTracker.obtain();
        }
        this.f6739l.addMovement(motionEvent2);
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        if (actionMasked == 5) {
                            int pointerId = motionEvent2.getPointerId(actionIndex);
                            float x = motionEvent2.getX(actionIndex);
                            float y = motionEvent2.getY(actionIndex);
                            mo12661b(x, y, pointerId);
                            int i = this.f6728a;
                            if (i == 0) {
                                int i2 = this.f6735h[pointerId];
                                int i3 = this.f6743p;
                                if ((i2 & i3) != 0) {
                                    this.f6745r.mo1818b(i2 & i3, pointerId);
                                }
                            } else if (i == 2) {
                                android.view.View b = mo12658b((int) x, (int) y);
                                if (b == this.f6746s) {
                                    mo12665b(b, pointerId);
                                }
                            }
                        } else if (actionMasked == 6) {
                            mo12662b(motionEvent2.getPointerId(actionIndex));
                        }
                    }
                } else if (!(this.f6731d == null || this.f6732e == null)) {
                    int pointerCount = motionEvent.getPointerCount();
                    for (int i4 = 0; i4 < pointerCount; i4++) {
                        int pointerId2 = motionEvent2.getPointerId(i4);
                        if (mo12675e(pointerId2)) {
                            float x2 = motionEvent2.getX(i4);
                            float y2 = motionEvent2.getY(i4);
                            float f = x2 - this.f6731d[pointerId2];
                            float f2 = y2 - this.f6732e[pointerId2];
                            android.view.View b2 = mo12658b((int) x2, (int) y2);
                            boolean z2 = b2 != null && mo12654a(b2, f, f2);
                            if (z2) {
                                int left = b2.getLeft();
                                int i5 = (int) f;
                                int a = this.f6745r.mo1809a(b2, left + i5, i5);
                                int top = b2.getTop();
                                int i6 = (int) f2;
                                int b3 = this.f6745r.mo1816b(b2, top + i6, i6);
                                int a2 = this.f6745r.mo1808a(b2);
                                int b4 = this.f6745r.mo12685b(b2);
                                if (a2 != 0) {
                                    if (a2 > 0) {
                                    }
                                }
                                if (b4 != 0) {
                                    if (b4 > 0 && b3 == top) {
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                            mo12647a(f, f2, pointerId2);
                            if (this.f6728a != 1) {
                                if (z2 && mo12665b(b2, pointerId2)) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                    mo12663b(motionEvent);
                }
                z = false;
            }
            mo12659b();
            z = false;
        } else {
            float x3 = motionEvent.getX();
            float y3 = motionEvent.getY();
            z = false;
            int pointerId3 = motionEvent2.getPointerId(0);
            mo12661b(x3, y3, pointerId3);
            android.view.View b5 = mo12658b((int) x3, (int) y3);
            if (b5 == this.f6746s && this.f6728a == 2) {
                mo12665b(b5, pointerId3);
            }
            int i7 = this.f6735h[pointerId3];
            int i8 = this.f6743p;
            if ((i7 & i8) != 0) {
                this.f6745r.mo1818b(i7 & i8, pointerId3);
            }
        }
        if (this.f6728a == 1) {
            return true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12661b(float f, float f2, int i) {
        mo12669c(i);
        float[] fArr = this.f6731d;
        this.f6733f[i] = f;
        fArr[i] = f;
        float[] fArr2 = this.f6732e;
        this.f6734g[i] = f2;
        fArr2[i] = f2;
        this.f6735h[i] = mo12667c((int) f, (int) f2);
        this.f6738k |= 1 << i;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12663b(android.view.MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            int pointerId = motionEvent.getPointerId(i);
            if (mo12675e(pointerId)) {
                float x = motionEvent.getX(i);
                float y = motionEvent.getY(i);
                this.f6733f[pointerId] = x;
                this.f6734g[pointerId] = y;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12646a(float f, float f2) {
        this.f6747t = true;
        this.f6745r.mo1812a(this.f6746s, f, f2);
        this.f6747t = false;
        if (this.f6728a == 1) {
            mo12678f(0);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo12665b(android.view.View view, int i) {
        if (view == this.f6746s && this.f6730c == i) {
            return true;
        }
        if (view == null || !this.f6745r.mo1820b(view, i)) {
            return false;
        }
        this.f6730c = i;
        mo12650a(view, i);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12649a(android.view.MotionEvent motionEvent) {
        int i;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            mo12659b();
        }
        if (this.f6739l == null) {
            this.f6739l = android.view.VelocityTracker.obtain();
        }
        this.f6739l.addMovement(motionEvent);
        int i2 = 0;
        if (actionMasked == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int pointerId = motionEvent.getPointerId(0);
            android.view.View b = mo12658b((int) x, (int) y);
            mo12661b(x, y, pointerId);
            mo12665b(b, pointerId);
            int i3 = this.f6735h[pointerId];
            int i4 = this.f6743p;
            if ((i3 & i4) != 0) {
                this.f6745r.mo1818b(i3 & i4, pointerId);
            }
        } else if (actionMasked == 1) {
            if (this.f6728a == 1) {
                mo12681h();
            }
            mo12659b();
        } else if (actionMasked != 2) {
            if (actionMasked == 3) {
                if (this.f6728a == 1) {
                    mo12646a((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                mo12659b();
            } else if (actionMasked == 5) {
                int pointerId2 = motionEvent.getPointerId(actionIndex);
                float x2 = motionEvent.getX(actionIndex);
                float y2 = motionEvent.getY(actionIndex);
                mo12661b(x2, y2, pointerId2);
                if (this.f6728a == 0) {
                    mo12665b(mo12658b((int) x2, (int) y2), pointerId2);
                    int i5 = this.f6735h[pointerId2];
                    int i6 = this.f6743p;
                    if ((i5 & i6) != 0) {
                        this.f6745r.mo1818b(i5 & i6, pointerId2);
                    }
                } else if (mo12673d((int) x2, (int) y2)) {
                    mo12665b(this.f6746s, pointerId2);
                }
            } else if (actionMasked == 6) {
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                if (this.f6728a == 1 && pointerId3 == this.f6730c) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (true) {
                        if (i2 >= pointerCount) {
                            i = -1;
                            break;
                        }
                        int pointerId4 = motionEvent.getPointerId(i2);
                        if (pointerId4 != this.f6730c) {
                            android.view.View b2 = mo12658b((int) motionEvent.getX(i2), (int) motionEvent.getY(i2));
                            android.view.View view = this.f6746s;
                            if (b2 == view && mo12665b(view, pointerId4)) {
                                i = this.f6730c;
                                break;
                            }
                        }
                        i2++;
                    }
                    if (i == -1) {
                        mo12681h();
                    }
                }
                mo12662b(pointerId3);
            }
        } else if (this.f6728a != 1) {
            int pointerCount2 = motionEvent.getPointerCount();
            while (i2 < pointerCount2) {
                int pointerId5 = motionEvent.getPointerId(i2);
                if (mo12675e(pointerId5)) {
                    float x3 = motionEvent.getX(i2);
                    float y3 = motionEvent.getY(i2);
                    float f = x3 - this.f6731d[pointerId5];
                    float f2 = y3 - this.f6732e[pointerId5];
                    mo12647a(f, f2, pointerId5);
                    if (this.f6728a != 1) {
                        android.view.View b3 = mo12658b((int) x3, (int) y3);
                        if (mo12654a(b3, f, f2) && mo12665b(b3, pointerId5)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i2++;
            }
            mo12663b(motionEvent);
        } else if (mo12675e(this.f6730c)) {
            int findPointerIndex = motionEvent.findPointerIndex(this.f6730c);
            float x4 = motionEvent.getX(findPointerIndex);
            float y4 = motionEvent.getY(findPointerIndex);
            float[] fArr = this.f6733f;
            int i7 = this.f6730c;
            int i8 = (int) (x4 - fArr[i7]);
            int i9 = (int) (y4 - this.f6734g[i7]);
            mo12648a(this.f6746s.getLeft() + i8, this.f6746s.getTop() + i9, i8, i9);
            mo12663b(motionEvent);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo12658b(int i, int i2) {
        for (int childCount = this.f6748u.getChildCount() - 1; childCount >= 0; childCount--) {
            android.view.ViewGroup viewGroup = this.f6748u;
            this.f6745r.mo12684a(childCount);
            android.view.View childAt = viewGroup.getChildAt(childCount);
            if (i >= childAt.getLeft() && i < childAt.getRight() && i2 >= childAt.getTop() && i2 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public final int mo12667c(int i, int i2) {
        int i3 = i < this.f6748u.getLeft() + this.f6742o ? 1 : 0;
        if (i2 < this.f6748u.getTop() + this.f6742o) {
            i3 |= 4;
        }
        if (i > this.f6748u.getRight() - this.f6742o) {
            i3 |= 2;
        }
        return i2 > this.f6748u.getBottom() - this.f6742o ? i3 | 8 : i3;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12647a(float f, float f2, int i) {
        int i2 = 1;
        if (!mo12651a(f, f2, i, 1)) {
            i2 = 0;
        }
        if (mo12651a(f2, f, i, 4)) {
            i2 |= 4;
        }
        if (mo12651a(f, f2, i, 2)) {
            i2 |= 2;
        }
        if (mo12651a(f2, f, i, 8)) {
            i2 |= 8;
        }
        if (i2 != 0) {
            int[] iArr = this.f6736i;
            iArr[i] = iArr[i] | i2;
            this.f6745r.mo1811a(i2, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo12651a(float f, float f2, int i, int i2) {
        float abs = java.lang.Math.abs(f);
        float abs2 = java.lang.Math.abs(f2);
        if ((this.f6735h[i] & i2) != i2 || (this.f6743p & i2) == 0 || (this.f6737j[i] & i2) == i2 || (this.f6736i[i] & i2) == i2) {
            return false;
        }
        int i3 = this.f6729b;
        if (abs <= ((float) i3) && abs2 <= ((float) i3)) {
            return false;
        }
        if (abs < abs2 * 0.5f && this.f6745r.mo1819b(i2)) {
            int[] iArr = this.f6737j;
            iArr[i] = iArr[i] | i2;
            return false;
        } else if ((this.f6736i[i] & i2) != 0 || abs <= ((float) this.f6729b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo12654a(android.view.View view, float f, float f2) {
        if (view == null) {
            return false;
        }
        boolean z = this.f6745r.mo1808a(view) > 0;
        boolean z2 = this.f6745r.mo12685b(view) > 0;
        if (z && z2) {
            int i = this.f6729b;
            if ((f * f) + (f2 * f2) > ((float) (i * i))) {
                return true;
            }
            return false;
        } else if (z) {
            if (java.lang.Math.abs(f) > ((float) this.f6729b)) {
                return true;
            }
            return false;
        } else if (!z2 || java.lang.Math.abs(f2) <= ((float) this.f6729b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12652a(int i) {
        int length = this.f6731d.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (mo12653a(i, i2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12653a(int i, int i2) {
        if (!mo12672d(i2)) {
            return false;
        }
        boolean z = (i & 1) == 1;
        boolean z2 = (i & 2) == 2;
        float f = this.f6733f[i2] - this.f6731d[i2];
        float f2 = this.f6734g[i2] - this.f6732e[i2];
        if (z && z2) {
            int i3 = this.f6729b;
            if ((f * f) + (f2 * f2) > ((float) (i3 * i3))) {
                return true;
            }
            return false;
        } else if (z) {
            if (java.lang.Math.abs(f) > ((float) this.f6729b)) {
                return true;
            }
            return false;
        } else if (!z2 || java.lang.Math.abs(f2) <= ((float) this.f6729b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12648a(int i, int i2, int i3, int i4) {
        int left = this.f6746s.getLeft();
        int top = this.f6746s.getTop();
        if (i3 != 0) {
            i = this.f6745r.mo1809a(this.f6746s, i, i3);
            com.fossil.blesdk.obfuscated.C1776f9.m6831c(this.f6746s, i - left);
        }
        int i5 = i;
        if (i4 != 0) {
            i2 = this.f6745r.mo1816b(this.f6746s, i2, i4);
            com.fossil.blesdk.obfuscated.C1776f9.m6834d(this.f6746s, i2 - top);
        }
        int i6 = i2;
        if (i3 != 0 || i4 != 0) {
            this.f6745r.mo1814a(this.f6746s, i5, i6, i5 - left, i6 - top);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12655a(android.view.View view, int i, int i2) {
        if (view != null && i >= view.getLeft() && i < view.getRight() && i2 >= view.getTop() && i2 < view.getBottom()) {
            return true;
        }
        return false;
    }
}
