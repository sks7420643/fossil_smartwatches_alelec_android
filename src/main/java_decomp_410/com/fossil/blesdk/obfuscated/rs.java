package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rs implements mo<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ ws a;

    @DexIgnore
    public rs(ws wsVar) {
        this.a = wsVar;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, lo loVar) {
        return this.a.a(byteBuffer);
    }

    @DexIgnore
    public aq<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, lo loVar) throws IOException {
        return this.a.a(kw.c(byteBuffer), i, i2, loVar);
    }
}
