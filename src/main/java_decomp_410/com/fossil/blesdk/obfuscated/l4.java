package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l4<K, V> {
    @DexIgnore
    public l4<K, V>.b a;
    @DexIgnore
    public l4<K, V>.c b;
    @DexIgnore
    public l4<K, V>.e c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public boolean h; // = false;

        @DexIgnore
        public a(int i2) {
            this.e = i2;
            this.f = l4.this.c();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.g < this.f;
        }

        @DexIgnore
        public T next() {
            if (hasNext()) {
                T a = l4.this.a(this.g, this.e);
                this.g++;
                this.h = true;
                return a;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            if (this.h) {
                this.g--;
                this.f--;
                this.h = false;
                l4.this.a(this.g);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean a(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            a((Map.Entry) obj);
            throw null;
        }

        @DexIgnore
        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int c = l4.this.c();
            for (Map.Entry entry : collection) {
                l4.this.a(entry.getKey(), entry.getValue());
            }
            return c != l4.this.c();
        }

        @DexIgnore
        public void clear() {
            l4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a = l4.this.a(entry.getKey());
            if (a < 0) {
                return false;
            }
            return i4.a(l4.this.a(a, 1), entry.getValue());
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return l4.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int c = l4.this.c() - 1; c >= 0; c--) {
                Object a = l4.this.a(c, 0);
                Object a2 = l4.this.a(c, 1);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                if (a2 == null) {
                    i2 = 0;
                } else {
                    i2 = a2.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }

        @DexIgnore
        public boolean isEmpty() {
            return l4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public int size() {
            return l4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Set<K> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            l4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return l4.this.a(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            return l4.a(l4.this.b(), collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return l4.a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2 = 0;
            for (int c = l4.this.c() - 1; c >= 0; c--) {
                Object a = l4.this.a(c, 0);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                i2 += i;
            }
            return i2;
        }

        @DexIgnore
        public boolean isEmpty() {
            return l4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return new a(0);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int a = l4.this.a(obj);
            if (a < 0) {
                return false;
            }
            l4.this.a(a);
            return true;
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            return l4.b(l4.this.b(), collection);
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            return l4.c(l4.this.b(), collection);
        }

        @DexIgnore
        public int size() {
            return l4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return l4.this.b(0);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return l4.this.a(tArr, 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g; // = false;

        @DexIgnore
        public d() {
            this.e = l4.this.c() - 1;
            this.f = -1;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!this.g) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!i4.a(entry.getKey(), l4.this.a(this.f, 0)) || !i4.a(entry.getValue(), l4.this.a(this.f, 1))) {
                    return false;
                }
                return true;
            }
        }

        @DexIgnore
        public K getKey() {
            if (this.g) {
                return l4.this.a(this.f, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public V getValue() {
            if (this.g) {
                return l4.this.a(this.f, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f < this.e;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.g) {
                int i2 = 0;
                Object a = l4.this.a(this.f, 0);
                Object a2 = l4.this.a(this.f, 1);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                if (a2 != null) {
                    i2 = a2.hashCode();
                }
                return i ^ i2;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public void remove() {
            if (this.g) {
                l4.this.a(this.f);
                this.f--;
                this.e--;
                this.g = false;
                return;
            }
            throw new IllegalStateException();
        }

        @DexIgnore
        public V setValue(V v) {
            if (this.g) {
                return l4.this.a(this.f, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public String toString() {
            return getKey() + SimpleComparison.EQUAL_TO_OPERATION + getValue();
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            if (hasNext()) {
                this.f++;
                this.g = true;
                return this;
            }
            throw new NoSuchElementException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Collection<V> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            l4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return l4.this.b(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean isEmpty() {
            return l4.this.c() == 0;
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return new a(1);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int b = l4.this.b(obj);
            if (b < 0) {
                return false;
            }
            l4.this.a(b);
            return true;
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            int c = l4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (collection.contains(l4.this.a(i, 1))) {
                    l4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            int c = l4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (!collection.contains(l4.this.a(i, 1))) {
                    l4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public int size() {
            return l4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return l4.this.b(1);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return l4.this.a(tArr, 1);
        }
    }

    @DexIgnore
    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        for (Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <K, V> boolean b(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        for (Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    @DexIgnore
    public static <K, V> boolean c(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    @DexIgnore
    public abstract int a(Object obj);

    @DexIgnore
    public abstract Object a(int i, int i2);

    @DexIgnore
    public abstract V a(int i, V v);

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(K k, V v);

    @DexIgnore
    public abstract int b(Object obj);

    @DexIgnore
    public abstract Map<K, V> b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public Set<Map.Entry<K, V>> d() {
        if (this.a == null) {
            this.a = new b();
        }
        return this.a;
    }

    @DexIgnore
    public Set<K> e() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }

    @DexIgnore
    public Collection<V> f() {
        if (this.c == null) {
            this.c = new e();
        }
        return this.c;
    }

    @DexIgnore
    public <T> T[] a(T[] tArr, int i) {
        int c2 = c();
        if (tArr.length < c2) {
            tArr = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), c2);
        }
        for (int i2 = 0; i2 < c2; i2++) {
            tArr[i2] = a(i2, i);
        }
        if (tArr.length > c2) {
            tArr[c2] = null;
        }
        return tArr;
    }

    @DexIgnore
    public Object[] b(int i) {
        int c2 = c();
        Object[] objArr = new Object[c2];
        for (int i2 = 0; i2 < c2; i2++) {
            objArr[i2] = a(i2, i);
        }
        return objArr;
    }

    @DexIgnore
    public static <T> boolean a(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
