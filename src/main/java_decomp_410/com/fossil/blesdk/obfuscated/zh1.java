package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zh1 extends lg1 {
    @DexIgnore
    public /* final */ dl1 e;
    @DexIgnore
    public Boolean f;
    @DexIgnore
    public String g;

    @DexIgnore
    public zh1(dl1 dl1) {
        this(dl1, (String) null);
    }

    @DexIgnore
    public final void a(hg1 hg1, rl1 rl1) {
        bk0.a(hg1);
        b(rl1, false);
        a((Runnable) new li1(this, hg1, rl1));
    }

    @DexIgnore
    public final hg1 b(hg1 hg1, rl1 rl1) {
        boolean z = false;
        if ("_cmp".equals(hg1.e)) {
            eg1 eg1 = hg1.f;
            if (!(eg1 == null || eg1.size() == 0)) {
                String g2 = hg1.f.g("_cis");
                if (!TextUtils.isEmpty(g2) && (("referrer broadcast".equals(g2) || "referrer API".equals(g2)) && this.e.i().m(rl1.e))) {
                    z = true;
                }
            }
        }
        if (!z) {
            return hg1;
        }
        this.e.d().y().a("Event has been filtered ", hg1.toString());
        return new hg1("_cmpx", hg1.f, hg1.g, hg1.h);
    }

    @DexIgnore
    public final void c(rl1 rl1) {
        b(rl1, false);
        a((Runnable) new ai1(this, rl1));
    }

    @DexIgnore
    public final String d(rl1 rl1) {
        b(rl1, false);
        return this.e.e(rl1);
    }

    @DexIgnore
    public zh1(dl1 dl1, String str) {
        bk0.a(dl1);
        this.e = dl1;
        this.g = null;
    }

    @DexIgnore
    public final void a(hg1 hg1, String str, String str2) {
        bk0.a(hg1);
        bk0.b(str);
        a(str, true);
        a((Runnable) new mi1(this, hg1, str));
    }

    @DexIgnore
    public final byte[] a(hg1 hg1, String str) {
        bk0.b(str);
        bk0.a(hg1);
        a(str, true);
        this.e.d().z().a("Log and bundle. event", this.e.g().a(hg1.e));
        long a = this.e.c().a() / 1000000;
        try {
            byte[] bArr = (byte[]) this.e.a().b(new ni1(this, hg1, str)).get();
            if (bArr == null) {
                this.e.d().s().a("Log and bundle returned null. appId", tg1.a(str));
                bArr = new byte[0];
            }
            this.e.d().z().a("Log and bundle processed. event, size, time_ms", this.e.g().a(hg1.e), Integer.valueOf(bArr.length), Long.valueOf((this.e.c().a() / 1000000) - a));
            return bArr;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to log and bundle. appId, event, error", tg1.a(str), this.e.g().a(hg1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final void b(rl1 rl1, boolean z) {
        bk0.a(rl1);
        a(rl1.e, false);
        this.e.h().d(rl1.f, rl1.v);
    }

    @DexIgnore
    public final void b(rl1 rl1) {
        a(rl1.e, false);
        a((Runnable) new ki1(this, rl1));
    }

    @DexIgnore
    public final void a(kl1 kl1, rl1 rl1) {
        bk0.a(kl1);
        b(rl1, false);
        if (kl1.H() == null) {
            a((Runnable) new oi1(this, kl1, rl1));
        } else {
            a((Runnable) new pi1(this, kl1, rl1));
        }
    }

    @DexIgnore
    public final List<kl1> a(rl1 rl1, boolean z) {
        b(rl1, false);
        try {
            List<ml1> list = (List) this.e.a().a(new qi1(this, rl1)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ml1 ml1 : list) {
                if (z || !nl1.h(ml1.c)) {
                    arrayList.add(new kl1(ml1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", tg1.a(rl1.e), e2);
            return null;
        }
    }

    @DexIgnore
    public final void a(rl1 rl1) {
        b(rl1, false);
        a((Runnable) new ri1(this, rl1));
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.f == null) {
                        if (!"com.google.android.gms".equals(this.g) && !sm0.a(this.e.getContext(), Binder.getCallingUid())) {
                            if (!ae0.a(this.e.getContext()).a(Binder.getCallingUid())) {
                                z2 = false;
                                this.f = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.f = Boolean.valueOf(z2);
                    }
                    if (this.f.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e2) {
                    this.e.d().s().a("Measurement Service called with invalid calling package. appId", tg1.a(str));
                    throw e2;
                }
            }
            if (this.g == null && zd0.uidHasPackageName(this.e.getContext(), Binder.getCallingUid(), str)) {
                this.g = str;
            }
            if (!str.equals(this.g)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", new Object[]{str}));
            }
            return;
        }
        this.e.d().s().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) {
        a((Runnable) new si1(this, str2, str3, str, j));
    }

    @DexIgnore
    public final void a(vl1 vl1, rl1 rl1) {
        bk0.a(vl1);
        bk0.a(vl1.g);
        b(rl1, false);
        vl1 vl12 = new vl1(vl1);
        vl12.e = rl1.e;
        if (vl1.g.H() == null) {
            a((Runnable) new ci1(this, vl12, rl1));
        } else {
            a((Runnable) new di1(this, vl12, rl1));
        }
    }

    @DexIgnore
    public final void a(vl1 vl1) {
        bk0.a(vl1);
        bk0.a(vl1.g);
        a(vl1.e, true);
        vl1 vl12 = new vl1(vl1);
        if (vl1.g.H() == null) {
            a((Runnable) new ei1(this, vl12));
        } else {
            a((Runnable) new fi1(this, vl12));
        }
    }

    @DexIgnore
    public final List<kl1> a(String str, String str2, boolean z, rl1 rl1) {
        b(rl1, false);
        try {
            List<ml1> list = (List) this.e.a().a(new gi1(this, rl1, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ml1 ml1 : list) {
                if (z || !nl1.h(ml1.c)) {
                    arrayList.add(new kl1(ml1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", tg1.a(rl1.e), e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<kl1> a(String str, String str2, String str3, boolean z) {
        a(str, true);
        try {
            List<ml1> list = (List) this.e.a().a(new hi1(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (ml1 ml1 : list) {
                if (z || !nl1.h(ml1.c)) {
                    arrayList.add(new kl1(ml1));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get user attributes. appId", tg1.a(str), e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<vl1> a(String str, String str2, rl1 rl1) {
        b(rl1, false);
        try {
            return (List) this.e.a().a(new ii1(this, rl1, str, str2)).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final List<vl1> a(String str, String str2, String str3) {
        a(str, true);
        try {
            return (List) this.e.a().a(new ji1(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e2) {
            this.e.d().s().a("Failed to get conditional user properties", e2);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        bk0.a(runnable);
        if (!jg1.Y.a().booleanValue() || !this.e.a().s()) {
            this.e.a().a(runnable);
        } else {
            runnable.run();
        }
    }
}
