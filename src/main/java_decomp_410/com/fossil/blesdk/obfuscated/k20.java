package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class k20<T> {
    @DexIgnore
    public /* final */ Version a;

    @DexIgnore
    public k20(Version version) {
        kd4.b(version, "signedVersion");
        this.a = version;
    }

    @DexIgnore
    public final Version a() {
        return this.a;
    }

    @DexIgnore
    public abstract byte[] a(T t);

    @DexIgnore
    public abstract byte[] a(short s, T t);
}
