package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ui4 extends bg4 {
    @DexIgnore
    public /* final */ sj4 e;

    @DexIgnore
    public ui4(sj4 sj4) {
        kd4.b(sj4, "node");
        this.e = sj4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.j();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "RemoveOnCancel[" + this.e + ']';
    }
}
