package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ym4 implements Interceptor {
    @DexIgnore
    public /* final */ boolean a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends no4 {
        @DexIgnore
        public long f;

        @DexIgnore
        public a(xo4 xo4) {
            super(xo4);
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            super.a(jo4, j);
            this.f += j;
        }
    }

    @DexIgnore
    public ym4(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response;
        dn4 dn4 = (dn4) chain;
        zm4 g = dn4.g();
        wm4 h = dn4.h();
        tm4 tm4 = (tm4) dn4.c();
        dm4 n = dn4.n();
        long currentTimeMillis = System.currentTimeMillis();
        dn4.f().d(dn4.e());
        g.a(n);
        dn4.f().a(dn4.e(), n);
        Response.a aVar = null;
        if (cn4.b(n.e()) && n.a() != null) {
            if ("100-continue".equalsIgnoreCase(n.a("Expect"))) {
                g.b();
                dn4.f().f(dn4.e());
                aVar = g.a(true);
            }
            if (aVar == null) {
                dn4.f().c(dn4.e());
                a aVar2 = new a(g.a(n, n.a().a()));
                ko4 a2 = so4.a((xo4) aVar2);
                n.a().a(a2);
                a2.close();
                dn4.f().a(dn4.e(), aVar2.f);
            } else if (!tm4.e()) {
                h.e();
            }
        }
        g.a();
        if (aVar == null) {
            dn4.f().f(dn4.e());
            aVar = g.a(false);
        }
        aVar.a(n);
        aVar.a(h.c().d());
        aVar.b(currentTimeMillis);
        aVar.a(System.currentTimeMillis());
        Response a3 = aVar.a();
        int B = a3.B();
        if (B == 100) {
            Response.a a4 = g.a(false);
            a4.a(n);
            a4.a(h.c().d());
            a4.b(currentTimeMillis);
            a4.a(System.currentTimeMillis());
            a3 = a4.a();
            B = a3.B();
        }
        dn4.f().a(dn4.e(), a3);
        if (!this.a || B != 101) {
            Response.a H = a3.H();
            H.a(g.a(a3));
            response = H.a();
        } else {
            Response.a H2 = a3.H();
            H2.a(jm4.c);
            response = H2.a();
        }
        if ("close".equalsIgnoreCase(response.L().a("Connection")) || "close".equalsIgnoreCase(response.e("Connection"))) {
            h.e();
        }
        if ((B != 204 && B != 205) || response.y().C() <= 0) {
            return response;
        }
        throw new ProtocolException("HTTP " + B + " had non-zero Content-Length: " + response.y().C());
    }
}
