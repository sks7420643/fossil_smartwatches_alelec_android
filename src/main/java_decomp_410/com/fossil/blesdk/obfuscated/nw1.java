package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class nw1 implements jw1 {
    @DexIgnore
    public <T> T a(Class<T> cls) {
        ez1<T> b = b(cls);
        if (b == null) {
            return null;
        }
        return b.get();
    }
}
