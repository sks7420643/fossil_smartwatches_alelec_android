package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gu2 implements MembersInjector<ConnectedAppsActivity> {
    @DexIgnore
    public static void a(ConnectedAppsActivity connectedAppsActivity, ConnectedAppsPresenter connectedAppsPresenter) {
        connectedAppsActivity.C = connectedAppsPresenter;
    }
}
