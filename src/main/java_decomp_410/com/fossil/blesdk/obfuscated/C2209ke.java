package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ke */
public class C2209ke extends com.fossil.blesdk.obfuscated.C1415af {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static android.animation.TimeInterpolator sDefaultInterpolator;
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mAddAnimations; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder>> mAdditionsList; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mChangeAnimations; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2218i>> mChangesList; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mMoveAnimations; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2219j>> mMovesList; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mPendingAdditions; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2218i> mPendingChanges; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2219j> mPendingMoves; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mPendingRemovals; // = new java.util.ArrayList<>();
    @DexIgnore
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView.ViewHolder> mRemoveAnimations; // = new java.util.ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$a */
    public class C2210a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f6796e;

        @DexIgnore
        public C2210a(java.util.ArrayList arrayList) {
            this.f6796e = arrayList;
        }

        @DexIgnore
        public void run() {
            java.util.Iterator it = this.f6796e.iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C2209ke.C2219j jVar = (com.fossil.blesdk.obfuscated.C2209ke.C2219j) it.next();
                com.fossil.blesdk.obfuscated.C2209ke.this.animateMoveImpl(jVar.f6830a, jVar.f6831b, jVar.f6832c, jVar.f6833d, jVar.f6834e);
            }
            this.f6796e.clear();
            com.fossil.blesdk.obfuscated.C2209ke.this.mMovesList.remove(this.f6796e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$b */
    public class C2211b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f6798e;

        @DexIgnore
        public C2211b(java.util.ArrayList arrayList) {
            this.f6798e = arrayList;
        }

        @DexIgnore
        public void run() {
            java.util.Iterator it = this.f6798e.iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C2209ke.this.animateChangeImpl((com.fossil.blesdk.obfuscated.C2209ke.C2218i) it.next());
            }
            this.f6798e.clear();
            com.fossil.blesdk.obfuscated.C2209ke.this.mChangesList.remove(this.f6798e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$c */
    public class C2212c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f6800e;

        @DexIgnore
        public C2212c(java.util.ArrayList arrayList) {
            this.f6800e = arrayList;
        }

        @DexIgnore
        public void run() {
            java.util.Iterator it = this.f6800e.iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C2209ke.this.animateAddImpl((androidx.recyclerview.widget.RecyclerView.ViewHolder) it.next());
            }
            this.f6800e.clear();
            com.fossil.blesdk.obfuscated.C2209ke.this.mAdditionsList.remove(this.f6800e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$d")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$d */
    public class C2213d extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ androidx.recyclerview.widget.RecyclerView.ViewHolder f6802a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.ViewPropertyAnimator f6803b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ android.view.View f6804c;

        @DexIgnore
        public C2213d(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, android.view.ViewPropertyAnimator viewPropertyAnimator, android.view.View view) {
            this.f6802a = viewHolder;
            this.f6803b = viewPropertyAnimator;
            this.f6804c = view;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6803b.setListener((android.animation.Animator.AnimatorListener) null);
            this.f6804c.setAlpha(1.0f);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchRemoveFinished(this.f6802a);
            com.fossil.blesdk.obfuscated.C2209ke.this.mRemoveAnimations.remove(this.f6802a);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchRemoveStarting(this.f6802a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$e")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$e */
    public class C2214e extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ androidx.recyclerview.widget.RecyclerView.ViewHolder f6806a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.View f6807b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ android.view.ViewPropertyAnimator f6808c;

        @DexIgnore
        public C2214e(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, android.view.View view, android.view.ViewPropertyAnimator viewPropertyAnimator) {
            this.f6806a = viewHolder;
            this.f6807b = view;
            this.f6808c = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            this.f6807b.setAlpha(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6808c.setListener((android.animation.Animator.AnimatorListener) null);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchAddFinished(this.f6806a);
            com.fossil.blesdk.obfuscated.C2209ke.this.mAddAnimations.remove(this.f6806a);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchAddStarting(this.f6806a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$f")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$f */
    public class C2215f extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ androidx.recyclerview.widget.RecyclerView.ViewHolder f6810a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ int f6811b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ android.view.View f6812c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ int f6813d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ android.view.ViewPropertyAnimator f6814e;

        @DexIgnore
        public C2215f(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, android.view.View view, int i2, android.view.ViewPropertyAnimator viewPropertyAnimator) {
            this.f6810a = viewHolder;
            this.f6811b = i;
            this.f6812c = view;
            this.f6813d = i2;
            this.f6814e = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            if (this.f6811b != 0) {
                this.f6812c.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            if (this.f6813d != 0) {
                this.f6812c.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6814e.setListener((android.animation.Animator.AnimatorListener) null);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchMoveFinished(this.f6810a);
            com.fossil.blesdk.obfuscated.C2209ke.this.mMoveAnimations.remove(this.f6810a);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchMoveStarting(this.f6810a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$g")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$g */
    public class C2216g extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2209ke.C2218i f6816a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.ViewPropertyAnimator f6817b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ android.view.View f6818c;

        @DexIgnore
        public C2216g(com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar, android.view.ViewPropertyAnimator viewPropertyAnimator, android.view.View view) {
            this.f6816a = iVar;
            this.f6817b = viewPropertyAnimator;
            this.f6818c = view;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6817b.setListener((android.animation.Animator.AnimatorListener) null);
            this.f6818c.setAlpha(1.0f);
            this.f6818c.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.f6818c.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchChangeFinished(this.f6816a.f6824a, true);
            com.fossil.blesdk.obfuscated.C2209ke.this.mChangeAnimations.remove(this.f6816a.f6824a);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchChangeStarting(this.f6816a.f6824a, true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$h")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$h */
    public class C2217h extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2209ke.C2218i f6820a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.ViewPropertyAnimator f6821b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ android.view.View f6822c;

        @DexIgnore
        public C2217h(com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar, android.view.ViewPropertyAnimator viewPropertyAnimator, android.view.View view) {
            this.f6820a = iVar;
            this.f6821b = viewPropertyAnimator;
            this.f6822c = view;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6821b.setListener((android.animation.Animator.AnimatorListener) null);
            this.f6822c.setAlpha(1.0f);
            this.f6822c.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.f6822c.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchChangeFinished(this.f6820a.f6825b, false);
            com.fossil.blesdk.obfuscated.C2209ke.this.mChangeAnimations.remove(this.f6820a.f6825b);
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2209ke.this.dispatchChangeStarting(this.f6820a.f6825b, false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$i")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$i */
    public static class C2218i {

        @DexIgnore
        /* renamed from: a */
        public androidx.recyclerview.widget.RecyclerView.ViewHolder f6824a;

        @DexIgnore
        /* renamed from: b */
        public androidx.recyclerview.widget.RecyclerView.ViewHolder f6825b;

        @DexIgnore
        /* renamed from: c */
        public int f6826c;

        @DexIgnore
        /* renamed from: d */
        public int f6827d;

        @DexIgnore
        /* renamed from: e */
        public int f6828e;

        @DexIgnore
        /* renamed from: f */
        public int f6829f;

        @DexIgnore
        public C2218i(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2) {
            this.f6824a = viewHolder;
            this.f6825b = viewHolder2;
        }

        @DexIgnore
        public java.lang.String toString() {
            return "ChangeInfo{oldHolder=" + this.f6824a + ", newHolder=" + this.f6825b + ", fromX=" + this.f6826c + ", fromY=" + this.f6827d + ", toX=" + this.f6828e + ", toY=" + this.f6829f + '}';
        }

        @DexIgnore
        public C2218i(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
            this(viewHolder, viewHolder2);
            this.f6826c = i;
            this.f6827d = i2;
            this.f6828e = i3;
            this.f6829f = i4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ke$j")
    /* renamed from: com.fossil.blesdk.obfuscated.ke$j */
    public static class C2219j {

        @DexIgnore
        /* renamed from: a */
        public androidx.recyclerview.widget.RecyclerView.ViewHolder f6830a;

        @DexIgnore
        /* renamed from: b */
        public int f6831b;

        @DexIgnore
        /* renamed from: c */
        public int f6832c;

        @DexIgnore
        /* renamed from: d */
        public int f6833d;

        @DexIgnore
        /* renamed from: e */
        public int f6834e;

        @DexIgnore
        public C2219j(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
            this.f6830a = viewHolder;
            this.f6831b = i;
            this.f6832c = i2;
            this.f6833d = i3;
            this.f6834e = i4;
        }
    }

    @DexIgnore
    private void animateRemoveImpl(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        android.view.View view = viewHolder.itemView;
        android.view.ViewPropertyAnimator animate = view.animate();
        this.mRemoveAnimations.add(viewHolder);
        animate.setDuration(getRemoveDuration()).alpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new com.fossil.blesdk.obfuscated.C2209ke.C2213d(viewHolder, animate, view)).start();
    }

    @DexIgnore
    private void endChangeAnimation(java.util.List<com.fossil.blesdk.obfuscated.C2209ke.C2218i> list, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        for (int size = list.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar = list.get(size);
            if (endChangeAnimationIfNecessary(iVar, viewHolder) && iVar.f6824a == null && iVar.f6825b == null) {
                list.remove(iVar);
            }
        }
    }

    @DexIgnore
    private void endChangeAnimationIfNecessary(com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar) {
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = iVar.f6824a;
        if (viewHolder != null) {
            endChangeAnimationIfNecessary(iVar, viewHolder);
        }
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = iVar.f6825b;
        if (viewHolder2 != null) {
            endChangeAnimationIfNecessary(iVar, viewHolder2);
        }
    }

    @DexIgnore
    private void resetAnimation(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        if (sDefaultInterpolator == null) {
            sDefaultInterpolator = new android.animation.ValueAnimator().getInterpolator();
        }
        viewHolder.itemView.animate().setInterpolator(sDefaultInterpolator);
        endAnimation(viewHolder);
    }

    @DexIgnore
    public boolean animateAdd(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        viewHolder.itemView.setAlpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.mPendingAdditions.add(viewHolder);
        return true;
    }

    @DexIgnore
    public void animateAddImpl(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        android.view.View view = viewHolder.itemView;
        android.view.ViewPropertyAnimator animate = view.animate();
        this.mAddAnimations.add(viewHolder);
        animate.alpha(1.0f).setDuration(getAddDuration()).setListener(new com.fossil.blesdk.obfuscated.C2209ke.C2214e(viewHolder, view, animate)).start();
    }

    @DexIgnore
    public boolean animateChange(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
        if (viewHolder == viewHolder2) {
            return animateMove(viewHolder, i, i2, i3, i4);
        }
        float translationX = viewHolder.itemView.getTranslationX();
        float translationY = viewHolder.itemView.getTranslationY();
        float alpha = viewHolder.itemView.getAlpha();
        resetAnimation(viewHolder);
        int i5 = (int) (((float) (i3 - i)) - translationX);
        int i6 = (int) (((float) (i4 - i2)) - translationY);
        viewHolder.itemView.setTranslationX(translationX);
        viewHolder.itemView.setTranslationY(translationY);
        viewHolder.itemView.setAlpha(alpha);
        if (viewHolder2 != null) {
            resetAnimation(viewHolder2);
            viewHolder2.itemView.setTranslationX((float) (-i5));
            viewHolder2.itemView.setTranslationY((float) (-i6));
            viewHolder2.itemView.setAlpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2218i> arrayList = this.mPendingChanges;
        com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar = new com.fossil.blesdk.obfuscated.C2209ke.C2218i(viewHolder, viewHolder2, i, i2, i3, i4);
        arrayList.add(iVar);
        return true;
    }

    @DexIgnore
    public void animateChangeImpl(com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar) {
        android.view.View view;
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = iVar.f6824a;
        android.view.View view2 = null;
        if (viewHolder == null) {
            view = null;
        } else {
            view = viewHolder.itemView;
        }
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = iVar.f6825b;
        if (viewHolder2 != null) {
            view2 = viewHolder2.itemView;
        }
        if (view != null) {
            android.view.ViewPropertyAnimator duration = view.animate().setDuration(getChangeDuration());
            this.mChangeAnimations.add(iVar.f6824a);
            duration.translationX((float) (iVar.f6828e - iVar.f6826c));
            duration.translationY((float) (iVar.f6829f - iVar.f6827d));
            duration.alpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new com.fossil.blesdk.obfuscated.C2209ke.C2216g(iVar, duration, view)).start();
        }
        if (view2 != null) {
            android.view.ViewPropertyAnimator animate = view2.animate();
            this.mChangeAnimations.add(iVar.f6825b);
            animate.translationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).translationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setDuration(getChangeDuration()).alpha(1.0f).setListener(new com.fossil.blesdk.obfuscated.C2209ke.C2217h(iVar, animate, view2)).start();
        }
    }

    @DexIgnore
    public boolean animateMove(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        android.view.View view = viewHolder.itemView;
        int translationX = i + ((int) view.getTranslationX());
        int translationY = i2 + ((int) viewHolder.itemView.getTranslationY());
        resetAnimation(viewHolder);
        int i5 = i3 - translationX;
        int i6 = i4 - translationY;
        if (i5 == 0 && i6 == 0) {
            dispatchMoveFinished(viewHolder);
            return false;
        }
        if (i5 != 0) {
            view.setTranslationX((float) (-i5));
        }
        if (i6 != 0) {
            view.setTranslationY((float) (-i6));
        }
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2209ke.C2219j> arrayList = this.mPendingMoves;
        com.fossil.blesdk.obfuscated.C2209ke.C2219j jVar = new com.fossil.blesdk.obfuscated.C2209ke.C2219j(viewHolder, translationX, translationY, i3, i4);
        arrayList.add(jVar);
        return true;
    }

    @DexIgnore
    public void animateMoveImpl(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        android.view.View view = viewHolder.itemView;
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 != 0) {
            view.animate().translationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        if (i6 != 0) {
            view.animate().translationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        android.view.ViewPropertyAnimator animate = view.animate();
        this.mMoveAnimations.add(viewHolder);
        android.view.ViewPropertyAnimator duration = animate.setDuration(getMoveDuration());
        com.fossil.blesdk.obfuscated.C2209ke.C2215f fVar = new com.fossil.blesdk.obfuscated.C2209ke.C2215f(viewHolder, i5, view, i6, animate);
        duration.setListener(fVar).start();
    }

    @DexIgnore
    public boolean animateRemove(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        this.mPendingRemovals.add(viewHolder);
        return true;
    }

    @DexIgnore
    public boolean canReuseUpdatedViewHolder(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, java.util.List<java.lang.Object> list) {
        return !list.isEmpty() || super.canReuseUpdatedViewHolder(viewHolder, list);
    }

    @DexIgnore
    public void cancelAll(java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).itemView.animate().cancel();
        }
    }

    @DexIgnore
    public void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    public void endAnimation(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        android.view.View view = viewHolder.itemView;
        view.animate().cancel();
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.mPendingMoves.get(size).f6830a == viewHolder) {
                view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                view.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                dispatchMoveFinished(viewHolder);
                this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, viewHolder);
        if (this.mPendingRemovals.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchRemoveFinished(viewHolder);
        }
        if (this.mPendingAdditions.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            java.util.ArrayList arrayList = this.mChangesList.get(size2);
            endChangeAnimation(arrayList, viewHolder);
            if (arrayList.isEmpty()) {
                this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            java.util.ArrayList arrayList2 = this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((com.fossil.blesdk.obfuscated.C2209ke.C2219j) arrayList2.get(size4)).f6830a == viewHolder) {
                    view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(viewHolder);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            java.util.ArrayList arrayList3 = this.mAdditionsList.get(size5);
            if (arrayList3.remove(viewHolder)) {
                view.setAlpha(1.0f);
                dispatchAddFinished(viewHolder);
                if (arrayList3.isEmpty()) {
                    this.mAdditionsList.remove(size5);
                }
            }
        }
        this.mRemoveAnimations.remove(viewHolder);
        this.mAddAnimations.remove(viewHolder);
        this.mChangeAnimations.remove(viewHolder);
        this.mMoveAnimations.remove(viewHolder);
        dispatchFinishedWhenDone();
    }

    @DexIgnore
    public void endAnimations() {
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            com.fossil.blesdk.obfuscated.C2209ke.C2219j jVar = this.mPendingMoves.get(size);
            android.view.View view = jVar.f6830a.itemView;
            view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            dispatchMoveFinished(jVar.f6830a);
            this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished(this.mPendingRemovals.get(size2));
            this.mPendingRemovals.remove(size2);
        }
        int size3 = this.mPendingAdditions.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = this.mPendingAdditions.get(size3);
            viewHolder.itemView.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
            this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary(this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                java.util.ArrayList arrayList = this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    com.fossil.blesdk.obfuscated.C2209ke.C2219j jVar2 = (com.fossil.blesdk.obfuscated.C2209ke.C2219j) arrayList.get(size6);
                    android.view.View view2 = jVar2.f6830a.itemView;
                    view2.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view2.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(jVar2.f6830a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                java.util.ArrayList arrayList2 = this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = (androidx.recyclerview.widget.RecyclerView.ViewHolder) arrayList2.get(size8);
                    viewHolder2.itemView.setAlpha(1.0f);
                    dispatchAddFinished(viewHolder2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                java.util.ArrayList arrayList3 = this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary((com.fossil.blesdk.obfuscated.C2209ke.C2218i) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    @DexIgnore
    public void runPendingAnimations() {
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            java.util.Iterator<androidx.recyclerview.widget.RecyclerView.ViewHolder> it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl(it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                java.util.ArrayList arrayList = new java.util.ArrayList();
                arrayList.addAll(this.mPendingMoves);
                this.mMovesList.add(arrayList);
                this.mPendingMoves.clear();
                com.fossil.blesdk.obfuscated.C2209ke.C2210a aVar = new com.fossil.blesdk.obfuscated.C2209ke.C2210a(arrayList);
                if (z) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6817a(((com.fossil.blesdk.obfuscated.C2209ke.C2219j) arrayList.get(0)).f6830a.itemView, (java.lang.Runnable) aVar, getRemoveDuration());
                } else {
                    aVar.run();
                }
            }
            if (z3) {
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                arrayList2.addAll(this.mPendingChanges);
                this.mChangesList.add(arrayList2);
                this.mPendingChanges.clear();
                com.fossil.blesdk.obfuscated.C2209ke.C2211b bVar = new com.fossil.blesdk.obfuscated.C2209ke.C2211b(arrayList2);
                if (z) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6817a(((com.fossil.blesdk.obfuscated.C2209ke.C2218i) arrayList2.get(0)).f6824a.itemView, (java.lang.Runnable) bVar, getRemoveDuration());
                } else {
                    bVar.run();
                }
            }
            if (z4) {
                java.util.ArrayList arrayList3 = new java.util.ArrayList();
                arrayList3.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(arrayList3);
                this.mPendingAdditions.clear();
                com.fossil.blesdk.obfuscated.C2209ke.C2212c cVar = new com.fossil.blesdk.obfuscated.C2209ke.C2212c(arrayList3);
                if (z || z2 || z3) {
                    long j = 0;
                    long removeDuration = z ? getRemoveDuration() : 0;
                    long moveDuration = z2 ? getMoveDuration() : 0;
                    if (z3) {
                        j = getChangeDuration();
                    }
                    com.fossil.blesdk.obfuscated.C1776f9.m6817a(((androidx.recyclerview.widget.RecyclerView.ViewHolder) arrayList3.get(0)).itemView, (java.lang.Runnable) cVar, removeDuration + java.lang.Math.max(moveDuration, j));
                    return;
                }
                cVar.run();
            }
        }
    }

    @DexIgnore
    private boolean endChangeAnimationIfNecessary(com.fossil.blesdk.obfuscated.C2209ke.C2218i iVar, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        boolean z = false;
        if (iVar.f6825b == viewHolder) {
            iVar.f6825b = null;
        } else if (iVar.f6824a != viewHolder) {
            return false;
        } else {
            iVar.f6824a = null;
            z = true;
        }
        viewHolder.itemView.setAlpha(1.0f);
        viewHolder.itemView.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        viewHolder.itemView.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        dispatchChangeFinished(viewHolder, z);
        return true;
    }
}
