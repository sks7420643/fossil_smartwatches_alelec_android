package com.fossil.blesdk.obfuscated;

import android.content.ContentUris;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.dt3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vr3 {
    @DexIgnore
    public static /* final */ String a; // = "vr3";
    @DexIgnore
    public static /* final */ Bitmap.Config b; // = Bitmap.Config.ARGB_8888;

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, int i) {
        Bitmap a2 = a(bitmap, i);
        Bitmap a3 = a(bitmap2, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (a2 != null) {
            canvas.drawBitmap(a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a2.recycle();
        }
        if (a3 != null) {
            canvas.drawBitmap(a3, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a3.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap b(Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            bitmap2 = Bitmap.createBitmap(bitmap, (width / 2) - (height / 2), 0, height, height);
        } else {
            bitmap2 = Bitmap.createBitmap(bitmap, 0, (height / 2) - (width / 2), width, width);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap2.getWidth() + 4, bitmap2.getHeight() + 4, bitmap2.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(bitmap2, 2.0f, 2.0f, (Paint) null);
        bitmap2.recycle();
        return Bitmap.createScaledBitmap(createBitmap, i, i, false);
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, int i) {
        Bitmap a2 = a(bitmap, i);
        Bitmap b2 = b(bitmap2, i);
        Bitmap b3 = b(bitmap3, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (a2 != null) {
            canvas.drawBitmap(a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a2.recycle();
        }
        if (b2 != null) {
            canvas.drawBitmap(b2, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b2.recycle();
        }
        if (b3 != null) {
            float f = (float) i;
            canvas.drawBitmap(b3, f, f, (Paint) null);
            b3.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap b(String str) {
        String[] split = str.split(" ");
        String str2 = split[0];
        String str3 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str3)) {
            str3 = str3.substring(0, 1);
        }
        int color = PortfolioApp.R.getResources().getColor(R.color.fossilCoolGray);
        int color2 = PortfolioApp.R.getResources().getColor(R.color.white);
        dt3.c b2 = dt3.a().b();
        b2.a(200);
        b2.c(200);
        b2.b(a(30));
        b2.d(color2);
        dt3.d a2 = b2.a();
        return a((Drawable) a2.a(str2 + str3, color));
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4, int i) {
        Bitmap b2 = b(bitmap, i);
        Bitmap b3 = b(bitmap2, i);
        Bitmap b4 = b(bitmap3, i);
        Bitmap b5 = b(bitmap4, i);
        int i2 = i * 2;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i2, b);
        Canvas canvas = new Canvas(createBitmap);
        if (b2 != null) {
            canvas.drawBitmap(b2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b2.recycle();
        }
        if (b3 != null) {
            canvas.drawBitmap(b3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i, (Paint) null);
            b3.recycle();
        }
        if (b4 != null) {
            canvas.drawBitmap(b4, (float) i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            b4.recycle();
        }
        if (b5 != null) {
            float f = (float) i;
            canvas.drawBitmap(b5, f, f, (Paint) null);
            b5.recycle();
        }
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap a(Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i2 = width * 2;
        if (height > i2) {
            bitmap2 = Bitmap.createBitmap(bitmap, 0, (height / 2) - width, width, i2);
        } else {
            bitmap2 = Bitmap.createBitmap(bitmap, (width / 2) - (height / 4), 0, height / 2, height);
        }
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap2, i, i * 2, false);
        bitmap2.recycle();
        Bitmap createBitmap = Bitmap.createBitmap(createScaledBitmap.getWidth() + 4, createScaledBitmap.getHeight() + 4, createScaledBitmap.getConfig());
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor(Color.parseColor("#C9C9C9"));
        canvas.drawBitmap(createScaledBitmap, 2.0f, 2.0f, (Paint) null);
        createScaledBitmap.recycle();
        return createBitmap;
    }

    @DexIgnore
    public static Bitmap a(Drawable drawable) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        try {
            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(2, 2, b);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), b);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public static Bitmap a(Contact contact) {
        String[] split = contact.getDisplayName().split(" ");
        String str = split[0];
        String str2 = split.length > 1 ? split[1] : "";
        if (!TextUtils.isEmpty(str)) {
            str = str.substring(0, 1);
        }
        if (!TextUtils.isEmpty(str2)) {
            str2 = str2.substring(0, 1);
        }
        int color = PortfolioApp.R.getResources().getColor(R.color.fossilCoolGray);
        int color2 = PortfolioApp.R.getResources().getColor(R.color.white);
        dt3.c b2 = dt3.a().b();
        b2.a(200);
        b2.c(200);
        b2.b(a(30));
        b2.d(color2);
        dt3.d a2 = b2.a();
        return a((Drawable) a2.a(str + str2, color));
    }

    @DexIgnore
    public static Bitmap a(String str) {
        int color = PortfolioApp.R.getResources().getColor(R.color.white_30);
        int color2 = PortfolioApp.R.getResources().getColor(R.color.hex7D7D7D);
        dt3.c b2 = dt3.a().b();
        b2.a(200);
        b2.c(200);
        b2.b(120);
        b2.d(color2);
        dt3 a2 = b2.a().a(str, color);
        a2.a(true);
        return a((Drawable) a2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v6, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: android.graphics.Bitmap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: java.io.InputStream} */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.graphics.Bitmap] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006c A[SYNTHETIC, Splitter:B:30:0x006c] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static Bitmap a(Long l) {
        InputStream inputStream;
        Bitmap bitmap;
        InputStream inputStream2 = null;
        if (!ns3.a.a((Context) PortfolioApp.R, "android.permission.READ_CONTACTS", "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.READ_SMS")) {
            FLogger.INSTANCE.getLocal().d(a, "getContactPhotoById does not enough permissions");
            return null;
        }
        try {
            InputStream openContactPhotoInputStream = ContactsContract.Contacts.openContactPhotoInputStream(PortfolioApp.R.getContentResolver(), ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, l.longValue()), true);
            if (openContactPhotoInputStream != null) {
                try {
                    Object decodeStream = BitmapFactory.decodeStream(openContactPhotoInputStream);
                    openContactPhotoInputStream.close();
                    inputStream2 = decodeStream;
                    inputStream2 = decodeStream;
                } catch (Exception e) {
                    e = e;
                    InputStream inputStream3 = inputStream2;
                    inputStream2 = openContactPhotoInputStream;
                    bitmap = inputStream3;
                } catch (Throwable th) {
                    th = th;
                    inputStream = openContactPhotoInputStream;
                    if (inputStream != null) {
                    }
                    throw th;
                }
            }
            if (openContactPhotoInputStream != null) {
                try {
                    openContactPhotoInputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            return inputStream2;
        } catch (Exception e3) {
            e = e3;
            bitmap = null;
            try {
                e.printStackTrace();
                if (inputStream2 == null) {
                    return bitmap;
                }
                try {
                    inputStream2.close();
                    return bitmap;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    return bitmap;
                }
            } catch (Throwable th2) {
                th = th2;
                inputStream = inputStream2;
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
                throw th;
            }
        }
    }

    @DexIgnore
    public static int a(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, PortfolioApp.R.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public static String a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        try {
            String str = new String(Base64.encode(byteArrayOutputStream.toByteArray(), 2), "UTF-8");
            try {
                byteArrayOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                byteArrayOutputStream.close();
                return "";
            } catch (Exception e3) {
                e3.printStackTrace();
                return "";
            }
        } catch (Throwable th) {
            try {
                byteArrayOutputStream.close();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            throw th;
        }
    }

    @DexIgnore
    public static Bitmap a(Resources resources, Drawable drawable) {
        Bitmap bitmap;
        int i;
        if (drawable instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        } else {
            Bitmap createBitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            bitmap = createBitmap;
        }
        int dimensionPixelSize = resources.getDimensionPixelSize(R.dimen.dp48);
        if (bitmap.getHeight() <= dimensionPixelSize && bitmap.getWidth() <= dimensionPixelSize) {
            return bitmap;
        }
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > width) {
            int i2 = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
            i = dimensionPixelSize;
            dimensionPixelSize = i2;
        } else {
            i = (int) ((((float) width) * ((float) dimensionPixelSize)) / ((float) height));
        }
        return Bitmap.createScaledBitmap(bitmap, dimensionPixelSize, i, false);
    }
}
