package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i0 */
public class C1987i0 extends androidx.appcompat.app.ActionBar {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2101j2 f5900a;

    @DexIgnore
    /* renamed from: b */
    public boolean f5901b;

    @DexIgnore
    /* renamed from: c */
    public android.view.Window.Callback f5902c;

    @DexIgnore
    /* renamed from: d */
    public boolean f5903d;

    @DexIgnore
    /* renamed from: e */
    public boolean f5904e;

    @DexIgnore
    /* renamed from: f */
    public java.util.ArrayList<androidx.appcompat.app.ActionBar.C0044a> f5905f; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.Runnable f5906g; // = new com.fossil.blesdk.obfuscated.C1987i0.C1988a();

    @DexIgnore
    /* renamed from: h */
    public /* final */ androidx.appcompat.widget.Toolbar.C0119e f5907h; // = new com.fossil.blesdk.obfuscated.C1987i0.C1989b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.i0$a */
    public class C1988a implements java.lang.Runnable {
        @DexIgnore
        public C1988a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1987i0.this.mo11804n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.i0$b */
    public class C1989b implements androidx.appcompat.widget.Toolbar.C0119e {
        @DexIgnore
        public C1989b() {
        }

        @DexIgnore
        public boolean onMenuItemClick(android.view.MenuItem menuItem) {
            return com.fossil.blesdk.obfuscated.C1987i0.this.f5902c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i0$c")
    /* renamed from: com.fossil.blesdk.obfuscated.i0$c */
    public final class C1990c implements com.fossil.blesdk.obfuscated.C2618p1.C2619a {

        @DexIgnore
        /* renamed from: e */
        public boolean f5910e;

        @DexIgnore
        public C1990c() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            android.view.Window.Callback callback = com.fossil.blesdk.obfuscated.C1987i0.this.f5902c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, h1Var);
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
            if (!this.f5910e) {
                this.f5910e = true;
                com.fossil.blesdk.obfuscated.C1987i0.this.f5900a.mo8484g();
                android.view.Window.Callback callback = com.fossil.blesdk.obfuscated.C1987i0.this.f5902c;
                if (callback != null) {
                    callback.onPanelClosed(108, h1Var);
                }
                this.f5910e = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i0$d")
    /* renamed from: com.fossil.blesdk.obfuscated.i0$d */
    public final class C1991d implements com.fossil.blesdk.obfuscated.C1915h1.C1916a {
        @DexIgnore
        public C1991d() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo535a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            com.fossil.blesdk.obfuscated.C1987i0 i0Var = com.fossil.blesdk.obfuscated.C1987i0.this;
            if (i0Var.f5902c == null) {
                return;
            }
            if (i0Var.f5900a.mo8470a()) {
                com.fossil.blesdk.obfuscated.C1987i0.this.f5902c.onPanelClosed(108, h1Var);
            } else if (com.fossil.blesdk.obfuscated.C1987i0.this.f5902c.onPreparePanel(0, (android.view.View) null, h1Var)) {
                com.fossil.blesdk.obfuscated.C1987i0.this.f5902c.onMenuOpened(108, h1Var);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo536a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i0$e")
    /* renamed from: com.fossil.blesdk.obfuscated.i0$e */
    public class C1992e extends com.fossil.blesdk.obfuscated.C3399z0 {
        @DexIgnore
        public C1992e(android.view.Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public android.view.View onCreatePanelView(int i) {
            if (i == 0) {
                return new android.view.View(com.fossil.blesdk.obfuscated.C1987i0.this.f5900a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, android.view.View view, android.view.Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                com.fossil.blesdk.obfuscated.C1987i0 i0Var = com.fossil.blesdk.obfuscated.C1987i0.this;
                if (!i0Var.f5901b) {
                    i0Var.f5900a.mo8471b();
                    com.fossil.blesdk.obfuscated.C1987i0.this.f5901b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public C1987i0(androidx.appcompat.widget.Toolbar toolbar, java.lang.CharSequence charSequence, android.view.Window.Callback callback) {
        this.f5900a = new com.fossil.blesdk.obfuscated.C1378a3(toolbar, false);
        this.f5902c = new com.fossil.blesdk.obfuscated.C1987i0.C1992e(callback);
        this.f5900a.setWindowCallback(this.f5902c);
        toolbar.setOnMenuItemClickListener(this.f5907h);
        this.f5900a.setWindowTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo177a(float f) {
        com.fossil.blesdk.obfuscated.C1776f9.m6824b((android.view.View) this.f5900a.mo8490k(), f);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo182b(java.lang.CharSequence charSequence) {
        this.f5900a.setWindowTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo184c(boolean z) {
    }

    @DexIgnore
    /* renamed from: d */
    public void mo185d(boolean z) {
        mo11801a(z ? 4 : 0, 4);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo186e(boolean z) {
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo187e() {
        return this.f5900a.mo8482e();
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo188f() {
        if (!this.f5900a.mo8487h()) {
            return false;
        }
        this.f5900a.collapseActionView();
        return true;
    }

    @DexIgnore
    /* renamed from: g */
    public int mo189g() {
        return this.f5900a.mo8491l();
    }

    @DexIgnore
    /* renamed from: h */
    public android.content.Context mo190h() {
        return this.f5900a.getContext();
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo191i() {
        this.f5900a.mo8490k().removeCallbacks(this.f5906g);
        com.fossil.blesdk.obfuscated.C1776f9.m6816a((android.view.View) this.f5900a.mo8490k(), this.f5906g);
        return true;
    }

    @DexIgnore
    /* renamed from: j */
    public void mo192j() {
        this.f5900a.mo8490k().removeCallbacks(this.f5906g);
    }

    @DexIgnore
    /* renamed from: k */
    public boolean mo193k() {
        return this.f5900a.mo8483f();
    }

    @DexIgnore
    /* renamed from: l */
    public final android.view.Menu mo11802l() {
        if (!this.f5903d) {
            this.f5900a.mo8466a((com.fossil.blesdk.obfuscated.C2618p1.C2619a) new com.fossil.blesdk.obfuscated.C1987i0.C1990c(), (com.fossil.blesdk.obfuscated.C1915h1.C1916a) new com.fossil.blesdk.obfuscated.C1987i0.C1991d());
            this.f5903d = true;
        }
        return this.f5900a.mo8488i();
    }

    @DexIgnore
    /* renamed from: m */
    public android.view.Window.Callback mo11803m() {
        return this.f5902c;
    }

    @DexIgnore
    /* renamed from: n */
    public void mo11804n() {
        android.view.Menu l = mo11802l();
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = l instanceof com.fossil.blesdk.obfuscated.C1915h1 ? (com.fossil.blesdk.obfuscated.C1915h1) l : null;
        if (h1Var != null) {
            h1Var.mo11524s();
        }
        try {
            l.clear();
            if (!this.f5902c.onCreatePanelMenu(0, l) || !this.f5902c.onPreparePanel(0, (android.view.View) null, l)) {
                l.clear();
            }
        } finally {
            if (h1Var != null) {
                h1Var.mo11521r();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo178a(android.content.res.Configuration configuration) {
        super.mo178a(configuration);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo183b(boolean z) {
        if (z != this.f5904e) {
            this.f5904e = z;
            int size = this.f5905f.size();
            for (int i = 0; i < size; i++) {
                this.f5905f.get(i).mo194a(z);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo179a(java.lang.CharSequence charSequence) {
        this.f5900a.setTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11801a(int i, int i2) {
        this.f5900a.mo8462a((i & i2) | ((~i2) & this.f5900a.mo8491l()));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo181a(android.view.KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            mo193k();
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo180a(int i, android.view.KeyEvent keyEvent) {
        android.view.Menu l = mo11802l();
        if (l == null) {
            return false;
        }
        boolean z = true;
        if (android.view.KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        l.setQwertyMode(z);
        return l.performShortcut(i, keyEvent, 0);
    }
}
