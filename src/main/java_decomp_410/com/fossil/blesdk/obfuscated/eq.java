package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.mp;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eq implements mp, so.a<Object>, mp.a {
    @DexIgnore
    public /* final */ np<?> e;
    @DexIgnore
    public /* final */ mp.a f;
    @DexIgnore
    public int g;
    @DexIgnore
    public jp h;
    @DexIgnore
    public Object i;
    @DexIgnore
    public volatile sr.a<?> j;
    @DexIgnore
    public kp k;

    @DexIgnore
    public eq(np<?> npVar, mp.a aVar) {
        this.e = npVar;
        this.f = aVar;
    }

    @DexIgnore
    public boolean a() {
        Object obj = this.i;
        if (obj != null) {
            this.i = null;
            b(obj);
        }
        jp jpVar = this.h;
        if (jpVar != null && jpVar.a()) {
            return true;
        }
        this.h = null;
        this.j = null;
        boolean z = false;
        while (!z && b()) {
            List<sr.a<?>> g2 = this.e.g();
            int i2 = this.g;
            this.g = i2 + 1;
            this.j = g2.get(i2);
            if (this.j != null && (this.e.e().a(this.j.c.b()) || this.e.c(this.j.c.getDataClass()))) {
                this.j.c.a(this.e.j(), this);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public final boolean b() {
        return this.g < this.e.g().size();
    }

    @DexIgnore
    public void cancel() {
        sr.a<?> aVar = this.j;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore
    public void j() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void b(Object obj) {
        long a = pw.a();
        try {
            ho<X> a2 = this.e.a(obj);
            lp lpVar = new lp(a2, obj, this.e.i());
            this.k = new kp(this.j.a, this.e.l());
            this.e.d().a(this.k, lpVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.k + ", data: " + obj + ", encoder: " + a2 + ", duration: " + pw.a(a));
            }
            this.j.c.a();
            this.h = new jp(Collections.singletonList(this.j.a), this.e, this);
        } catch (Throwable th) {
            this.j.c.a();
            throw th;
        }
    }

    @DexIgnore
    public void a(Object obj) {
        pp e2 = this.e.e();
        if (obj == null || !e2.a(this.j.c.b())) {
            this.f.a(this.j.a, obj, this.j.c, this.j.c.b(), this.k);
            return;
        }
        this.i = obj;
        this.f.j();
    }

    @DexIgnore
    public void a(Exception exc) {
        this.f.a(this.k, exc, this.j.c, this.j.c.b());
    }

    @DexIgnore
    public void a(jo joVar, Object obj, so<?> soVar, DataSource dataSource, jo joVar2) {
        this.f.a(joVar, obj, soVar, this.j.c.b(), joVar);
    }

    @DexIgnore
    public void a(jo joVar, Exception exc, so<?> soVar, DataSource dataSource) {
        this.f.a(joVar, exc, soVar, this.j.c.b());
    }
}
