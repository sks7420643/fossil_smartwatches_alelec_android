package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t0 */
public interface C2935t0 {
    @DexIgnore
    void onActionViewCollapsed();

    @DexIgnore
    void onActionViewExpanded();
}
