package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b3 */
public class C1460b3 {
    @DexIgnore
    /* renamed from: a */
    public static void m4756a(android.view.View view, java.lang.CharSequence charSequence) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            view.setTooltipText(charSequence);
        } else {
            com.fossil.blesdk.obfuscated.C1532c3.m5261a(view, charSequence);
        }
    }
}
