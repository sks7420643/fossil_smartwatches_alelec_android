package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bd3 implements Factory<DashboardSleepPresenter> {
    @DexIgnore
    public static DashboardSleepPresenter a(zc3 zc3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, UserRepository userRepository, h42 h42) {
        return new DashboardSleepPresenter(zc3, sleepSummariesRepository, sleepSessionsRepository, fitnessDataRepository, sleepDao, sleepDatabase, userRepository, h42);
    }
}
