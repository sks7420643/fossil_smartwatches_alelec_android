package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gs */
public class C1893gs implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f5504a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.gs$a */
    public static class C1894a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f5505a;

        @DexIgnore
        public C1894a(android.content.Context context) {
            this.f5505a = context;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1893gs(this.f5505a);
        }
    }

    @DexIgnore
    public C1893gs(android.content.Context context) {
        this.f5504a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.InputStream> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        if (!com.fossil.blesdk.obfuscated.C1740ep.m6535a(i, i2) || !mo11299a(loVar)) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(uri), com.fossil.blesdk.obfuscated.C1817fp.m7094b(this.f5504a, uri));
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11299a(com.fossil.blesdk.obfuscated.C2337lo loVar) {
        java.lang.Long l = (java.lang.Long) loVar.mo13319a(com.fossil.blesdk.obfuscated.C2065it.f6189d);
        return l != null && l.longValue() == -1;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return com.fossil.blesdk.obfuscated.C1740ep.m6538c(uri);
    }
}
