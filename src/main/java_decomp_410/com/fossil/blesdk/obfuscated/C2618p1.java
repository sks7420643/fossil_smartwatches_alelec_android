package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p1 */
public interface C2618p1 {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.p1$a */
    public interface C2619a {
        @DexIgnore
        /* renamed from: a */
        void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z);

        @DexIgnore
        /* renamed from: a */
        boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var);
    }

    @DexIgnore
    /* renamed from: a */
    void mo1158a(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var);

    @DexIgnore
    /* renamed from: a */
    void mo1159a(android.os.Parcelable parcelable);

    @DexIgnore
    /* renamed from: a */
    void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z);

    @DexIgnore
    /* renamed from: a */
    void mo9013a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar);

    @DexIgnore
    /* renamed from: a */
    void mo1161a(boolean z);

    @DexIgnore
    /* renamed from: a */
    boolean mo1162a();

    @DexIgnore
    /* renamed from: a */
    boolean mo1163a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var);

    @DexIgnore
    /* renamed from: a */
    boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var);

    @DexIgnore
    /* renamed from: b */
    android.os.Parcelable mo1165b();

    @DexIgnore
    /* renamed from: b */
    boolean mo1166b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var);

    @DexIgnore
    int getId();
}
