package com.fossil.blesdk.obfuscated;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class du1<E> implements Iterable<E> {
    @DexIgnore
    public /* final */ Optional<Iterable<E>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends du1<E> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Iterable iterable, Iterable iterable2) {
            super(iterable);
            this.f = iterable2;
        }

        @DexIgnore
        public Iterator<E> iterator() {
            return this.f.iterator();
        }
    }

    @DexIgnore
    public du1() {
        this.e = Optional.absent();
    }

    @DexIgnore
    public final Iterable<E> a() {
        return this.e.or(this);
    }

    @DexIgnore
    public final ImmutableSet<E> b() {
        return ImmutableSet.copyOf(a());
    }

    @DexIgnore
    public String toString() {
        return ku1.d(a());
    }

    @DexIgnore
    public static <E> du1<E> a(Iterable<E> iterable) {
        return iterable instanceof du1 ? (du1) iterable : new a(iterable, iterable);
    }

    @DexIgnore
    public du1(Iterable<E> iterable) {
        st1.a(iterable);
        this.e = Optional.fromNullable(this == iterable ? null : iterable);
    }

    @DexIgnore
    public final du1<E> a(tt1<? super E> tt1) {
        return a(ku1.b(a(), tt1));
    }
}
