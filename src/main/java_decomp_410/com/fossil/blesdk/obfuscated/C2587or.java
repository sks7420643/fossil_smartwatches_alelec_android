package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.or */
public final class C2587or implements com.fossil.blesdk.obfuscated.C2432mr {

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C2501nr>> f8195b;

    @DexIgnore
    /* renamed from: c */
    public volatile java.util.Map<java.lang.String, java.lang.String> f8196c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.or$a")
    /* renamed from: com.fossil.blesdk.obfuscated.or$a */
    public static final class C2588a {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ java.lang.String f8197b; // = m11890b();

        @DexIgnore
        /* renamed from: c */
        public static /* final */ java.util.Map<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C2501nr>> f8198c;

        @DexIgnore
        /* renamed from: a */
        public java.util.Map<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C2501nr>> f8199a; // = f8198c;

        /*
        static {
            java.util.HashMap hashMap = new java.util.HashMap(2);
            if (!android.text.TextUtils.isEmpty(f8197b)) {
                hashMap.put("User-Agent", java.util.Collections.singletonList(new com.fossil.blesdk.obfuscated.C2587or.C2589b(f8197b)));
            }
            f8198c = java.util.Collections.unmodifiableMap(hashMap);
        }
        */

        @DexIgnore
        /* renamed from: b */
        public static java.lang.String m11890b() {
            java.lang.String property = java.lang.System.getProperty("http.agent");
            if (android.text.TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            java.lang.StringBuilder sb = new java.lang.StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == 9) && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2587or mo14495a() {
            return new com.fossil.blesdk.obfuscated.C2587or(this.f8199a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.or$b")
    /* renamed from: com.fossil.blesdk.obfuscated.or$b */
    public static final class C2589b implements com.fossil.blesdk.obfuscated.C2501nr {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f8200a;

        @DexIgnore
        public C2589b(java.lang.String str) {
            this.f8200a = str;
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.String mo14081a() {
            return this.f8200a;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (obj instanceof com.fossil.blesdk.obfuscated.C2587or.C2589b) {
                return this.f8200a.equals(((com.fossil.blesdk.obfuscated.C2587or.C2589b) obj).f8200a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.f8200a.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "StringHeaderFactory{value='" + this.f8200a + '\'' + '}';
        }
    }

    @DexIgnore
    public C2587or(java.util.Map<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C2501nr>> map) {
        this.f8195b = java.util.Collections.unmodifiableMap(map);
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.String> mo13704a() {
        if (this.f8196c == null) {
            synchronized (this) {
                if (this.f8196c == null) {
                    this.f8196c = java.util.Collections.unmodifiableMap(mo14491b());
                }
            }
        }
        return this.f8196c;
    }

    @DexIgnore
    /* renamed from: b */
    public final java.util.Map<java.lang.String, java.lang.String> mo14491b() {
        java.util.HashMap hashMap = new java.util.HashMap();
        for (java.util.Map.Entry next : this.f8195b.entrySet()) {
            java.lang.String a = mo14490a((java.util.List) next.getValue());
            if (!android.text.TextUtils.isEmpty(a)) {
                hashMap.put(next.getKey(), a);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C2587or) {
            return this.f8195b.equals(((com.fossil.blesdk.obfuscated.C2587or) obj).f8195b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f8195b.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "LazyHeaders{headers=" + this.f8195b + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo14490a(java.util.List<com.fossil.blesdk.obfuscated.C2501nr> list) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            java.lang.String a = list.get(i).mo14081a();
            if (!android.text.TextUtils.isEmpty(a)) {
                sb.append(a);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
}
