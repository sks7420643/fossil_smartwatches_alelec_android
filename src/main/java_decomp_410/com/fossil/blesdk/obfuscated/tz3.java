package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.uz3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tz3 extends hz3 {
    @DexIgnore
    public uz3 c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public tz3(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        this.d = bundle.getString("_wxapi_showmessage_req_lang");
        this.e = bundle.getString("_wxapi_showmessage_req_country");
        this.c = uz3.a.a(bundle);
    }

    @DexIgnore
    public boolean a() {
        uz3 uz3 = this.c;
        if (uz3 == null) {
            return false;
        }
        return uz3.a();
    }

    @DexIgnore
    public int b() {
        return 4;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        Bundle a = uz3.a.a(this.c);
        super.b(a);
        bundle.putString("_wxapi_showmessage_req_lang", this.d);
        bundle.putString("_wxapi_showmessage_req_country", this.e);
        bundle.putAll(a);
    }
}
