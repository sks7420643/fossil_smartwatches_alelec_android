package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mq */
public class C2430mq<K extends com.fossil.blesdk.obfuscated.C2825rq, V> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> f7560a; // = new com.fossil.blesdk.obfuscated.C2430mq.C2431a<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<K, com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V>> f7561b; // = new java.util.HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mq$a */
    public static class C2431a<K, V> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ K f7562a;

        @DexIgnore
        /* renamed from: b */
        public java.util.List<V> f7563b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> f7564c;

        @DexIgnore
        /* renamed from: d */
        public com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> f7565d;

        @DexIgnore
        public C2431a() {
            this((java.lang.Object) null);
        }

        @DexIgnore
        /* renamed from: a */
        public V mo13698a() {
            int b = mo13700b();
            if (b > 0) {
                return this.f7563b.remove(b - 1);
            }
            return null;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo13700b() {
            java.util.List<V> list = this.f7563b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        @DexIgnore
        public C2431a(K k) {
            this.f7565d = this;
            this.f7564c = this;
            this.f7562a = k;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13699a(V v) {
            if (this.f7563b == null) {
                this.f7563b = new java.util.ArrayList();
            }
            this.f7563b.add(v);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static <K, V> void m10819c(com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar) {
        com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar2 = aVar.f7565d;
        aVar2.f7564c = aVar.f7564c;
        aVar.f7564c.f7565d = aVar2;
    }

    @DexIgnore
    /* renamed from: d */
    public static <K, V> void m10820d(com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar) {
        aVar.f7564c.f7565d = aVar;
        aVar.f7565d.f7564c = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13695a(K k, V v) {
        com.fossil.blesdk.obfuscated.C2430mq.C2431a aVar = this.f7561b.get(k);
        if (aVar == null) {
            aVar = new com.fossil.blesdk.obfuscated.C2430mq.C2431a(k);
            mo13696b(aVar);
            this.f7561b.put(k, aVar);
        } else {
            k.mo11713a();
        }
        aVar.mo13699a(v);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13696b(com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar) {
        m10819c(aVar);
        com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar2 = this.f7560a;
        aVar.f7565d = aVar2.f7565d;
        aVar.f7564c = aVar2;
        m10820d(aVar);
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar = this.f7560a.f7564c; !aVar.equals(this.f7560a); aVar = aVar.f7564c) {
            z = true;
            sb.append('{');
            sb.append(aVar.f7562a);
            sb.append(':');
            sb.append(aVar.mo13700b());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public V mo13693a(K k) {
        com.fossil.blesdk.obfuscated.C2430mq.C2431a aVar = this.f7561b.get(k);
        if (aVar == null) {
            aVar = new com.fossil.blesdk.obfuscated.C2430mq.C2431a(k);
            this.f7561b.put(k, aVar);
        } else {
            k.mo11713a();
        }
        mo13694a(aVar);
        return aVar.mo13698a();
    }

    @DexIgnore
    /* renamed from: a */
    public V mo13692a() {
        for (com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar = this.f7560a.f7565d; !aVar.equals(this.f7560a); aVar = aVar.f7565d) {
            V a = aVar.mo13698a();
            if (a != null) {
                return a;
            }
            m10819c(aVar);
            this.f7561b.remove(aVar.f7562a);
            ((com.fossil.blesdk.obfuscated.C2825rq) aVar.f7562a).mo11713a();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13694a(com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar) {
        m10819c(aVar);
        com.fossil.blesdk.obfuscated.C2430mq.C2431a<K, V> aVar2 = this.f7560a;
        aVar.f7565d = aVar2;
        aVar.f7564c = aVar2.f7564c;
        m10820d(aVar);
    }
}
