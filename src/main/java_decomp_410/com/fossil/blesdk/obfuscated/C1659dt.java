package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dt */
public final class C1659dt implements com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.BitmapDrawable>, com.fossil.blesdk.obfuscated.C3233wp {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.res.Resources f4527e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> f4528f;

    @DexIgnore
    public C1659dt(android.content.res.Resources resources, com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(resources);
        this.f4527e = resources;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(aqVar);
        this.f4528f = aqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.BitmapDrawable> m6045a(android.content.res.Resources resources, com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar) {
        if (aqVar == null) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C1659dt(resources, aqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return this.f4528f.mo8888b();
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<android.graphics.drawable.BitmapDrawable> mo8889c() {
        return android.graphics.drawable.BitmapDrawable.class;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10116d() {
        com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar = this.f4528f;
        if (aqVar instanceof com.fossil.blesdk.obfuscated.C3233wp) {
            ((com.fossil.blesdk.obfuscated.C3233wp) aqVar).mo10116d();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
        this.f4528f.mo8887a();
    }

    @DexIgnore
    public android.graphics.drawable.BitmapDrawable get() {
        return new android.graphics.drawable.BitmapDrawable(this.f4527e, this.f4528f.get());
    }
}
