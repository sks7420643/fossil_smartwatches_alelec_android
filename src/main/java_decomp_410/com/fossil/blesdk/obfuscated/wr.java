package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.sr;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wr {
    @DexIgnore
    public static /* final */ c e; // = new c();
    @DexIgnore
    public static /* final */ sr<Object, Object> f; // = new a();
    @DexIgnore
    public /* final */ List<b<?, ?>> a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Set<b<?, ?>> c;
    @DexIgnore
    public /* final */ g8<List<Throwable>> d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements sr<Object, Object> {
        @DexIgnore
        public sr.a<Object> a(Object obj, int i, int i2, lo loVar) {
            return null;
        }

        @DexIgnore
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model, Data> {
        @DexIgnore
        public /* final */ Class<Model> a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ tr<? extends Model, ? extends Data> c;

        @DexIgnore
        public b(Class<Model> cls, Class<Data> cls2, tr<? extends Model, ? extends Data> trVar) {
            this.a = cls;
            this.b = cls2;
            this.c = trVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <Model, Data> vr<Model, Data> a(List<sr<Model, Data>> list, g8<List<Throwable>> g8Var) {
            return new vr<>(list, g8Var);
        }
    }

    @DexIgnore
    public wr(g8<List<Throwable>> g8Var) {
        this(g8Var, e);
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, tr<? extends Model, ? extends Data> trVar) {
        a(cls, cls2, trVar, true);
    }

    @DexIgnore
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b next : this.a) {
            if (!arrayList.contains(next.b) && next.a(cls)) {
                arrayList.add(next.b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public wr(g8<List<Throwable>> g8Var, c cVar) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = g8Var;
        this.b = cVar;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, tr<? extends Model, ? extends Data> trVar, boolean z) {
        b bVar = new b(cls, cls2, trVar);
        List<b<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    public synchronized <Model> List<sr<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b next : this.a) {
                if (!this.c.contains(next)) {
                    if (next.a(cls)) {
                        this.c.add(next);
                        arrayList.add(a((b<?, ?>) next));
                        this.c.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Model, Data> sr<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b next : this.a) {
                if (this.c.contains(next)) {
                    z = true;
                } else if (next.a(cls, cls2)) {
                    this.c.add(next);
                    arrayList.add(a((b<?, ?>) next));
                    this.c.remove(next);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (sr) arrayList.get(0);
            } else if (z) {
                return a();
            } else {
                throw new Registry.NoModelLoaderAvailableException(cls, cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    @DexIgnore
    public final <Model, Data> sr<Model, Data> a(b<?, ?> bVar) {
        sr<? extends Model, ? extends Data> a2 = bVar.c.a(this);
        tw.a(a2);
        return a2;
    }

    @DexIgnore
    public static <Model, Data> sr<Model, Data> a() {
        return f;
    }
}
