package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wy2 implements Factory<vy2> {
    @DexIgnore
    public /* final */ Provider<Context> a;

    @DexIgnore
    public wy2(Provider<Context> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static wy2 a(Provider<Context> provider) {
        return new wy2(provider);
    }

    @DexIgnore
    public static vy2 b(Provider<Context> provider) {
        return new vy2(provider.get());
    }

    @DexIgnore
    public vy2 get() {
        return b(this.a);
    }
}
