package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gx1 {
    @DexIgnore
    public static gx1 e;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public ix1 c; // = new ix1(this);
    @DexIgnore
    public int d; // = 1;

    @DexIgnore
    public gx1(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static synchronized gx1 a(Context context) {
        gx1 gx1;
        synchronized (gx1.class) {
            if (e == null) {
                e = new gx1(context, dz0.a().a(1, new um0("MessengerIpcClient"), 9));
            }
            gx1 = e;
        }
        return gx1;
    }

    @DexIgnore
    public final wn1<Bundle> b(int i, Bundle bundle) {
        return a(new qx1(a(), 1, bundle));
    }

    @DexIgnore
    public final wn1<Void> a(int i, Bundle bundle) {
        return a(new ox1(a(), 2, bundle));
    }

    @DexIgnore
    public final synchronized <T> wn1<T> a(px1<T> px1) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(px1);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.c.a((px1) px1)) {
            this.c = new ix1(this);
            this.c.a((px1) px1);
        }
        return px1.b.a();
    }

    @DexIgnore
    public final synchronized int a() {
        int i;
        i = this.d;
        this.d = i + 1;
        return i;
    }
}
