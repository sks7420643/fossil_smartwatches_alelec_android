package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z10 extends GattOperationResult {
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z10(GattOperationResult.GattResult gattResult, int i) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        this.b = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }
}
