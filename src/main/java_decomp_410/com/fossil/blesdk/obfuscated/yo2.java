package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yo2 implements Factory<xo2> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<dn2> b;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> c;
    @DexIgnore
    public /* final */ Provider<en2> d;

    @DexIgnore
    public yo2(Provider<PortfolioApp> provider, Provider<dn2> provider2, Provider<AuthApiGuestService> provider3, Provider<en2> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static yo2 a(Provider<PortfolioApp> provider, Provider<dn2> provider2, Provider<AuthApiGuestService> provider3, Provider<en2> provider4) {
        return new yo2(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static xo2 b(Provider<PortfolioApp> provider, Provider<dn2> provider2, Provider<AuthApiGuestService> provider3, Provider<en2> provider4) {
        return new xo2(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public xo2 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
