package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hp0 implements Parcelable.Creator<ep0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        jp0 jp0 = null;
        Long l = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 2:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 3:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 5:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 7:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 8:
                    jp0 = SafeParcelReader.a(parcel2, a, jp0.CREATOR);
                    break;
                case 9:
                    l = SafeParcelReader.t(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new ep0(j, j2, str, str2, str3, i, jp0, l);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ep0[i];
    }
}
