package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qp */
public class C2755qp implements com.fossil.blesdk.obfuscated.C2905sp, com.fossil.blesdk.obfuscated.C1440ar.C1441a, com.fossil.blesdk.obfuscated.C3128vp.C3129a {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ boolean f8751i; // = android.util.Log.isLoggable("Engine", 2);

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3303xp f8752a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3054up f8753b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1440ar f8754c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2755qp.C2758b f8755d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C1648dq f8756e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2755qp.C2760c f8757f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2755qp.C2756a f8758g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2050ip f8759h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qp$a */
    public static class C2756a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.bumptech.glide.load.engine.DecodeJob.C0381e f8760a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1862g8<com.bumptech.glide.load.engine.DecodeJob<?>> f8761b; // = com.fossil.blesdk.obfuscated.C3145vw.m15491a(150, new com.fossil.blesdk.obfuscated.C2755qp.C2756a.C2757a());

        @DexIgnore
        /* renamed from: c */
        public int f8762c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qp$a$a */
        public class C2757a implements com.fossil.blesdk.obfuscated.C3145vw.C3149d<com.bumptech.glide.load.engine.DecodeJob<?>> {
            @DexIgnore
            public C2757a() {
            }

            @DexIgnore
            /* renamed from: a */
            public com.bumptech.glide.load.engine.DecodeJob<?> m12936a() {
                com.fossil.blesdk.obfuscated.C2755qp.C2756a aVar = com.fossil.blesdk.obfuscated.C2755qp.C2756a.this;
                return new com.bumptech.glide.load.engine.DecodeJob<>(aVar.f8760a, aVar.f8761b);
            }
        }

        @DexIgnore
        public C2756a(com.bumptech.glide.load.engine.DecodeJob.C0381e eVar) {
            this.f8760a = eVar;
        }

        @DexIgnore
        /* renamed from: a */
        public <R> com.bumptech.glide.load.engine.DecodeJob<R> mo15342a(com.fossil.blesdk.obfuscated.C2977tn tnVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2980tp tpVar, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, java.lang.Class<?> cls, java.lang.Class<R> cls2, com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2663pp ppVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, boolean z, boolean z2, boolean z3, com.fossil.blesdk.obfuscated.C2337lo loVar, com.bumptech.glide.load.engine.DecodeJob.C0378b<R> bVar) {
            com.bumptech.glide.load.engine.DecodeJob<R> a = this.f8761b.mo11162a();
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
            com.bumptech.glide.load.engine.DecodeJob<R> decodeJob = a;
            int i3 = this.f8762c;
            int i4 = i3;
            this.f8762c = i3 + 1;
            decodeJob.mo3944a(tnVar, obj, tpVar, joVar, i, i2, cls, cls2, priority, ppVar, map, z, z2, z3, loVar, bVar, i4);
            return decodeJob;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$b")
    /* renamed from: com.fossil.blesdk.obfuscated.qp$b */
    public static class C2758b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1650dr f8764a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1650dr f8765b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C1650dr f8766c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ com.fossil.blesdk.obfuscated.C1650dr f8767d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C2905sp f8768e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C3128vp.C3129a f8769f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C2819rp<?>> f8770g; // = com.fossil.blesdk.obfuscated.C3145vw.m15491a(150, new com.fossil.blesdk.obfuscated.C2755qp.C2758b.C2759a());

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qp$b$a */
        public class C2759a implements com.fossil.blesdk.obfuscated.C3145vw.C3149d<com.fossil.blesdk.obfuscated.C2819rp<?>> {
            @DexIgnore
            public C2759a() {
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C2819rp<?> m12939a() {
                com.fossil.blesdk.obfuscated.C2755qp.C2758b bVar = com.fossil.blesdk.obfuscated.C2755qp.C2758b.this;
                com.fossil.blesdk.obfuscated.C2819rp rpVar = new com.fossil.blesdk.obfuscated.C2819rp(bVar.f8764a, bVar.f8765b, bVar.f8766c, bVar.f8767d, bVar.f8768e, bVar.f8769f, bVar.f8770g);
                return rpVar;
            }
        }

        @DexIgnore
        public C2758b(com.fossil.blesdk.obfuscated.C1650dr drVar, com.fossil.blesdk.obfuscated.C1650dr drVar2, com.fossil.blesdk.obfuscated.C1650dr drVar3, com.fossil.blesdk.obfuscated.C1650dr drVar4, com.fossil.blesdk.obfuscated.C2905sp spVar, com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar) {
            this.f8764a = drVar;
            this.f8765b = drVar2;
            this.f8766c = drVar3;
            this.f8767d = drVar4;
            this.f8768e = spVar;
            this.f8769f = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public <R> com.fossil.blesdk.obfuscated.C2819rp<R> mo15343a(com.fossil.blesdk.obfuscated.C2143jo joVar, boolean z, boolean z2, boolean z3, boolean z4) {
            com.fossil.blesdk.obfuscated.C2819rp<R> a = this.f8770g.mo11162a();
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
            com.fossil.blesdk.obfuscated.C2819rp<R> rpVar = a;
            rpVar.mo15681a(joVar, z, z2, z3, z4);
            return rpVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$c")
    /* renamed from: com.fossil.blesdk.obfuscated.qp$c */
    public static class C2760c implements com.bumptech.glide.load.engine.DecodeJob.C0381e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2981tq.C2982a f8772a;

        @DexIgnore
        /* renamed from: b */
        public volatile com.fossil.blesdk.obfuscated.C2981tq f8773b;

        @DexIgnore
        public C2760c(com.fossil.blesdk.obfuscated.C2981tq.C2982a aVar) {
            this.f8772a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2981tq mo3981a() {
            if (this.f8773b == null) {
                synchronized (this) {
                    if (this.f8773b == null) {
                        this.f8773b = this.f8772a.build();
                    }
                    if (this.f8773b == null) {
                        this.f8773b = new com.fossil.blesdk.obfuscated.C3055uq();
                    }
                }
            }
            return this.f8773b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qp$d")
    /* renamed from: com.fossil.blesdk.obfuscated.qp$d */
    public class C2761d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2819rp<?> f8774a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2924sv f8775b;

        @DexIgnore
        public C2761d(com.fossil.blesdk.obfuscated.C2924sv svVar, com.fossil.blesdk.obfuscated.C2819rp<?> rpVar) {
            this.f8775b = svVar;
            this.f8774a = rpVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15344a() {
            synchronized (com.fossil.blesdk.obfuscated.C2755qp.this) {
                this.f8774a.mo15690c(this.f8775b);
            }
        }
    }

    @DexIgnore
    public C2755qp(com.fossil.blesdk.obfuscated.C1440ar arVar, com.fossil.blesdk.obfuscated.C2981tq.C2982a aVar, com.fossil.blesdk.obfuscated.C1650dr drVar, com.fossil.blesdk.obfuscated.C1650dr drVar2, com.fossil.blesdk.obfuscated.C1650dr drVar3, com.fossil.blesdk.obfuscated.C1650dr drVar4, boolean z) {
        this(arVar, aVar, drVar, drVar2, drVar3, drVar4, (com.fossil.blesdk.obfuscated.C3303xp) null, (com.fossil.blesdk.obfuscated.C3054up) null, (com.fossil.blesdk.obfuscated.C2050ip) null, (com.fossil.blesdk.obfuscated.C2755qp.C2758b) null, (com.fossil.blesdk.obfuscated.C2755qp.C2756a) null, (com.fossil.blesdk.obfuscated.C1648dq) null, z);
    }

    @DexIgnore
    /* renamed from: a */
    public <R> com.fossil.blesdk.obfuscated.C2755qp.C2761d mo15332a(com.fossil.blesdk.obfuscated.C2977tn tnVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, java.lang.Class<?> cls, java.lang.Class<R> cls2, com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2663pp ppVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, boolean z, boolean z2, com.fossil.blesdk.obfuscated.C2337lo loVar, boolean z3, boolean z4, boolean z5, boolean z6, com.fossil.blesdk.obfuscated.C2924sv svVar, java.util.concurrent.Executor executor) {
        long a = f8751i ? com.fossil.blesdk.obfuscated.C2682pw.m12452a() : 0;
        com.fossil.blesdk.obfuscated.C2980tp a2 = this.f8753b.mo16841a(obj, joVar, i, i2, map, cls, cls2, loVar);
        synchronized (this) {
            com.fossil.blesdk.obfuscated.C3128vp<?> a3 = mo15335a(a2, z3, a);
            if (a3 == null) {
                com.fossil.blesdk.obfuscated.C2755qp.C2761d a4 = mo15333a(tnVar, obj, joVar, i, i2, cls, cls2, priority, ppVar, map, z, z2, loVar, z3, z4, z5, z6, svVar, executor, a2, a);
                return a4;
            }
            svVar.mo4030a(a3, com.bumptech.glide.load.DataSource.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C3128vp<?> mo15339b(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C3128vp<?> b = this.f8759h.mo12060b(joVar);
        if (b != null) {
            b.mo17153d();
        }
        return b;
    }

    @DexIgnore
    /* renamed from: c */
    public final com.fossil.blesdk.obfuscated.C3128vp<?> mo15341c(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C3128vp<?> a = mo15334a(joVar);
        if (a != null) {
            a.mo17153d();
            this.f8759h.mo12058a(joVar, a);
        }
        return a;
    }

    @DexIgnore
    public C2755qp(com.fossil.blesdk.obfuscated.C1440ar arVar, com.fossil.blesdk.obfuscated.C2981tq.C2982a aVar, com.fossil.blesdk.obfuscated.C1650dr drVar, com.fossil.blesdk.obfuscated.C1650dr drVar2, com.fossil.blesdk.obfuscated.C1650dr drVar3, com.fossil.blesdk.obfuscated.C1650dr drVar4, com.fossil.blesdk.obfuscated.C3303xp xpVar, com.fossil.blesdk.obfuscated.C3054up upVar, com.fossil.blesdk.obfuscated.C2050ip ipVar, com.fossil.blesdk.obfuscated.C2755qp.C2758b bVar, com.fossil.blesdk.obfuscated.C2755qp.C2756a aVar2, com.fossil.blesdk.obfuscated.C1648dq dqVar, boolean z) {
        this.f8754c = arVar;
        com.fossil.blesdk.obfuscated.C2981tq.C2982a aVar3 = aVar;
        this.f8757f = new com.fossil.blesdk.obfuscated.C2755qp.C2760c(aVar);
        com.fossil.blesdk.obfuscated.C2050ip ipVar2 = ipVar == null ? new com.fossil.blesdk.obfuscated.C2050ip(z) : ipVar;
        this.f8759h = ipVar2;
        ipVar2.mo12059a((com.fossil.blesdk.obfuscated.C3128vp.C3129a) this);
        this.f8753b = upVar == null ? new com.fossil.blesdk.obfuscated.C3054up() : upVar;
        this.f8752a = xpVar == null ? new com.fossil.blesdk.obfuscated.C3303xp() : xpVar;
        this.f8755d = bVar == null ? new com.fossil.blesdk.obfuscated.C2755qp.C2758b(drVar, drVar2, drVar3, drVar4, this, this) : bVar;
        this.f8758g = aVar2 == null ? new com.fossil.blesdk.obfuscated.C2755qp.C2756a(this.f8757f) : aVar2;
        this.f8756e = dqVar == null ? new com.fossil.blesdk.obfuscated.C1648dq() : dqVar;
        arVar.mo8908a((com.fossil.blesdk.obfuscated.C1440ar.C1441a) this);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15340b(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        if (aqVar instanceof com.fossil.blesdk.obfuscated.C3128vp) {
            ((com.fossil.blesdk.obfuscated.C3128vp) aqVar).mo17156g();
            return;
        }
        throw new java.lang.IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    /* renamed from: a */
    public final <R> com.fossil.blesdk.obfuscated.C2755qp.C2761d mo15333a(com.fossil.blesdk.obfuscated.C2977tn tnVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, java.lang.Class<?> cls, java.lang.Class<R> cls2, com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2663pp ppVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, boolean z, boolean z2, com.fossil.blesdk.obfuscated.C2337lo loVar, boolean z3, boolean z4, boolean z5, boolean z6, com.fossil.blesdk.obfuscated.C2924sv svVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2980tp tpVar, long j) {
        com.fossil.blesdk.obfuscated.C2924sv svVar2 = svVar;
        java.util.concurrent.Executor executor2 = executor;
        com.fossil.blesdk.obfuscated.C2980tp tpVar2 = tpVar;
        long j2 = j;
        com.fossil.blesdk.obfuscated.C2819rp<?> a = this.f8752a.mo17842a((com.fossil.blesdk.obfuscated.C2143jo) tpVar2, z6);
        if (a != null) {
            a.mo15685a(svVar2, executor2);
            if (f8751i) {
                m12922a("Added to existing load", j2, (com.fossil.blesdk.obfuscated.C2143jo) tpVar2);
            }
            return new com.fossil.blesdk.obfuscated.C2755qp.C2761d(svVar2, a);
        }
        com.fossil.blesdk.obfuscated.C2819rp a2 = this.f8755d.mo15343a(tpVar, z3, z4, z5, z6);
        com.fossil.blesdk.obfuscated.C2819rp rpVar = a2;
        com.fossil.blesdk.obfuscated.C2980tp tpVar3 = tpVar2;
        com.bumptech.glide.load.engine.DecodeJob<R> a3 = this.f8758g.mo15342a(tnVar, obj, tpVar, joVar, i, i2, cls, cls2, priority, ppVar, map, z, z2, z6, loVar, a2);
        this.f8752a.mo17844a((com.fossil.blesdk.obfuscated.C2143jo) tpVar3, (com.fossil.blesdk.obfuscated.C2819rp<?>) rpVar);
        com.fossil.blesdk.obfuscated.C2819rp rpVar2 = rpVar;
        com.fossil.blesdk.obfuscated.C2980tp tpVar4 = tpVar3;
        com.fossil.blesdk.obfuscated.C2924sv svVar3 = svVar;
        rpVar2.mo15685a(svVar3, executor);
        rpVar2.mo15687b(a3);
        if (f8751i) {
            m12922a("Started new load", j, (com.fossil.blesdk.obfuscated.C2143jo) tpVar4);
        }
        return new com.fossil.blesdk.obfuscated.C2755qp.C2761d(svVar3, rpVar2);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3128vp<?> mo15335a(com.fossil.blesdk.obfuscated.C2980tp tpVar, boolean z, long j) {
        if (!z) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C3128vp<?> b = mo15339b((com.fossil.blesdk.obfuscated.C2143jo) tpVar);
        if (b != null) {
            if (f8751i) {
                m12922a("Loaded resource from active resources", j, (com.fossil.blesdk.obfuscated.C2143jo) tpVar);
            }
            return b;
        }
        com.fossil.blesdk.obfuscated.C3128vp<?> c = mo15341c(tpVar);
        if (c == null) {
            return null;
        }
        if (f8751i) {
            m12922a("Loaded resource from cache", j, (com.fossil.blesdk.obfuscated.C2143jo) tpVar);
        }
        return c;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m12922a(java.lang.String str, long j, com.fossil.blesdk.obfuscated.C2143jo joVar) {
        android.util.Log.v("Engine", str + " in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(j) + "ms, key: " + joVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3128vp<?> mo15334a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C1438aq<?> a = this.f8754c.mo8904a(joVar);
        if (a == null) {
            return null;
        }
        if (a instanceof com.fossil.blesdk.obfuscated.C3128vp) {
            return (com.fossil.blesdk.obfuscated.C3128vp) a;
        }
        com.fossil.blesdk.obfuscated.C3128vp<?> vpVar = new com.fossil.blesdk.obfuscated.C3128vp<>(a, true, true, joVar, this);
        return vpVar;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo15338a(com.fossil.blesdk.obfuscated.C2819rp<?> rpVar, com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar) {
        if (vpVar != null) {
            if (vpVar.mo17155f()) {
                this.f8759h.mo12058a(joVar, vpVar);
            }
        }
        this.f8752a.mo17845b(joVar, rpVar);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo15337a(com.fossil.blesdk.obfuscated.C2819rp<?> rpVar, com.fossil.blesdk.obfuscated.C2143jo joVar) {
        this.f8752a.mo17845b(joVar, rpVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8909a(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        this.f8756e.mo10092a(aqVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15336a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar) {
        this.f8759h.mo12057a(joVar);
        if (vpVar.mo17155f()) {
            this.f8754c.mo8905a(joVar, vpVar);
        } else {
            this.f8756e.mo10092a(vpVar);
        }
    }
}
