package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 f;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 g;

    @DexIgnore
    public xj1(vj1 vj1, AtomicReference atomicReference, rl1 rl1) {
        this.g = vj1;
        this.e = atomicReference;
        this.f = rl1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.e) {
            try {
                kg1 d = this.g.d;
                if (d == null) {
                    this.g.d().s().a("Failed to get app instance id");
                    this.e.notify();
                    return;
                }
                this.e.set(d.d(this.f));
                String str = (String) this.e.get();
                if (str != null) {
                    this.g.o().a(str);
                    this.g.k().l.a(str);
                }
                this.g.C();
                this.e.notify();
            } catch (RemoteException e2) {
                try {
                    this.g.d().s().a("Failed to get app instance id", e2);
                    this.e.notify();
                } catch (Throwable th) {
                    this.e.notify();
                    throw th;
                }
            }
        }
    }
}
