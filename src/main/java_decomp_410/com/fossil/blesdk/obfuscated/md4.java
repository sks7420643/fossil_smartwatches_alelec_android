package com.fossil.blesdk.obfuscated;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class md4 {
    @DexIgnore
    public static /* final */ nd4 a;

    /*
    static {
        nd4 nd4 = null;
        try {
            nd4 = (nd4) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (nd4 == null) {
            nd4 = new nd4();
        }
        a = nd4;
    }
    */

    @DexIgnore
    public static je4 a(Class cls, String str) {
        return a.a(cls, str);
    }

    @DexIgnore
    public static he4 a(Class cls) {
        return a.a(cls);
    }

    @DexIgnore
    public static String a(Lambda lambda) {
        return a.a(lambda);
    }

    @DexIgnore
    public static String a(hd4 hd4) {
        return a.a(hd4);
    }

    @DexIgnore
    public static ke4 a(FunctionReference functionReference) {
        a.a(functionReference);
        return functionReference;
    }

    @DexIgnore
    public static me4 a(PropertyReference1 propertyReference1) {
        a.a(propertyReference1);
        return propertyReference1;
    }
}
