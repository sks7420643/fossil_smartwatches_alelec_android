package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d8 extends Writer {
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public StringBuilder f; // = new StringBuilder(128);

    @DexIgnore
    public d8(String str) {
        this.e = str;
    }

    @DexIgnore
    public void close() {
        y();
    }

    @DexIgnore
    public void flush() {
        y();
    }

    @DexIgnore
    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                y();
            } else {
                this.f.append(c);
            }
        }
    }

    @DexIgnore
    public final void y() {
        if (this.f.length() > 0) {
            Log.d(this.e, this.f.toString());
            StringBuilder sb = this.f;
            sb.delete(0, sb.length());
        }
    }
}
