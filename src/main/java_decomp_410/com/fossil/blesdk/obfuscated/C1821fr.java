package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fr */
public class C1821fr<Data> implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int f5256c; // = 22;

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.res.AssetManager f5257a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1821fr.C1822a<Data> f5258b;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.fr$a */
    public interface C1822a<Data> {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C2902so<Data> mo11022a(android.content.res.AssetManager assetManager, java.lang.String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fr$b */
    public static class C1823b implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, android.os.ParcelFileDescriptor>, com.fossil.blesdk.obfuscated.C1821fr.C1822a<android.os.ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.AssetManager f5259a;

        @DexIgnore
        public C1823b(android.content.res.AssetManager assetManager) {
            this.f5259a = assetManager;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, android.os.ParcelFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1821fr(this.f5259a, this);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2902so<android.os.ParcelFileDescriptor> mo11022a(android.content.res.AssetManager assetManager, java.lang.String str) {
            return new com.fossil.blesdk.obfuscated.C3232wo(assetManager, str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.fr$c */
    public static class C1824c implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream>, com.fossil.blesdk.obfuscated.C1821fr.C1822a<java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.AssetManager f5260a;

        @DexIgnore
        public C1824c(android.content.res.AssetManager assetManager) {
            this.f5260a = assetManager;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1821fr(this.f5260a, this);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2902so<java.io.InputStream> mo11022a(android.content.res.AssetManager assetManager, java.lang.String str) {
            return new com.fossil.blesdk.obfuscated.C1497bp(assetManager, str);
        }
    }

    @DexIgnore
    public C1821fr(android.content.res.AssetManager assetManager, com.fossil.blesdk.obfuscated.C1821fr.C1822a<Data> aVar) {
        this.f5257a = assetManager;
        this.f5258b = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(uri), this.f5258b.mo11022a(this.f5257a, uri.toString().substring(f5256c)));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
