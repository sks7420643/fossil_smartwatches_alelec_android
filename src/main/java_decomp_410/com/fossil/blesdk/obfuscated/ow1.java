package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ow1 implements kw1 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public ow1(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static kw1 a(Object obj) {
        return new ow1(obj);
    }

    @DexIgnore
    public final Object a(jw1 jw1) {
        Object obj = this.a;
        iw1.a(obj);
        return obj;
    }
}
