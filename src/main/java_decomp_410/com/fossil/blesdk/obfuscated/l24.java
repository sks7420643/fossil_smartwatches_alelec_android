package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l24 {
    @DexIgnore
    public static volatile l24 c;
    @DexIgnore
    public Timer a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public l24(Context context) {
        this.b = context.getApplicationContext();
        this.a = new Timer(false);
    }

    @DexIgnore
    public static l24 a(Context context) {
        if (c == null) {
            synchronized (l24.class) {
                if (c == null) {
                    c = new l24(context);
                }
            }
        }
        return c;
    }

    @DexIgnore
    public void a() {
        if (h04.o() == StatReportStrategy.PERIOD) {
            long l = (long) (h04.l() * 60 * 1000);
            if (h04.q()) {
                t14 b2 = e24.b();
                b2.e("setupPeriodTimer delay:" + l);
            }
            a(new m24(this), l);
        }
    }

    @DexIgnore
    public void a(TimerTask timerTask, long j) {
        if (this.a != null) {
            if (h04.q()) {
                t14 b2 = e24.b();
                b2.e("setupPeriodTimer schedule delay:" + j);
            }
            this.a.schedule(timerTask, j);
        } else if (h04.q()) {
            e24.b().g("setupPeriodTimer schedule timer == null");
        }
    }
}
