package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rl */
public class C2813rl {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f9012a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("PackageManagerHelper");

    @DexIgnore
    /* renamed from: a */
    public static void m13254a(android.content.Context context, java.lang.Class<?> cls, boolean z) {
        java.lang.String str;
        java.lang.String str2 = "enabled";
        try {
            context.getPackageManager().setComponentEnabledSetting(new android.content.ComponentName(context, cls.getName()), z ? 1 : 2, 1);
            com.fossil.blesdk.obfuscated.C1635dj a = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
            java.lang.String str3 = f9012a;
            java.lang.Object[] objArr = new java.lang.Object[2];
            objArr[0] = cls.getName();
            if (z) {
                str = str2;
            } else {
                str = "disabled";
            }
            objArr[1] = str;
            a.mo9962a(str3, java.lang.String.format("%s %s", objArr), new java.lang.Throwable[0]);
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.C1635dj a2 = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
            java.lang.String str4 = f9012a;
            java.lang.Object[] objArr2 = new java.lang.Object[2];
            objArr2[0] = cls.getName();
            if (!z) {
                str2 = "disabled";
            }
            objArr2[1] = str2;
            a2.mo9962a(str4, java.lang.String.format("%s could not be %s", objArr2), e);
        }
    }
}
