package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wk0 extends ls0 implements vk0 {
    @DexIgnore
    public wk0() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    @DexIgnore
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        e(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
