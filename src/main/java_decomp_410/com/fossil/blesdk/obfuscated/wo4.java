package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wo4 {
    @DexIgnore
    public static vo4 a;
    @DexIgnore
    public static long b;

    @DexIgnore
    public static vo4 a() {
        synchronized (wo4.class) {
            if (a == null) {
                return new vo4();
            }
            vo4 vo4 = a;
            a = vo4.f;
            vo4.f = null;
            b -= 8192;
            return vo4;
        }
    }

    @DexIgnore
    public static void a(vo4 vo4) {
        if (vo4.f != null || vo4.g != null) {
            throw new IllegalArgumentException();
        } else if (!vo4.d) {
            synchronized (wo4.class) {
                if (b + 8192 <= 65536) {
                    b += 8192;
                    vo4.f = a;
                    vo4.c = 0;
                    vo4.b = 0;
                    a = vo4;
                }
            }
        }
    }
}
