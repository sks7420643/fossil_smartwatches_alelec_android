package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class zs0 implements gt0 {
    @DexIgnore
    public /* final */ ys0 a;
    @DexIgnore
    public /* final */ vs0 b;

    @DexIgnore
    public zs0(ys0 ys0, vs0 vs0) {
        this.a = ys0;
        this.b = vs0;
    }

    @DexIgnore
    public final Object a() {
        return this.b.a().get(this.a.b);
    }
}
