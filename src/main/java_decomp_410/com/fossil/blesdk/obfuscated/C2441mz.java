package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mz */
public class C2441mz implements com.fossil.blesdk.obfuscated.c00 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f7604a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.c00[] f7605b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2512nz f7606c;

    @DexIgnore
    public C2441mz(int i, com.fossil.blesdk.obfuscated.c00... c00Arr) {
        this.f7604a = i;
        this.f7605b = c00Arr;
        this.f7606c = new com.fossil.blesdk.obfuscated.C2512nz(i);
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.StackTraceElement[] mo9374a(java.lang.StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.f7604a) {
            return stackTraceElementArr;
        }
        java.lang.StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (com.fossil.blesdk.obfuscated.c00 c00 : this.f7605b) {
            if (stackTraceElementArr2.length <= this.f7604a) {
                break;
            }
            stackTraceElementArr2 = c00.mo9374a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.f7604a ? this.f7606c.mo9374a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
