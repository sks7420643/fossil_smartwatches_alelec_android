package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vy */
public class C3154vy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f10426a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.e74 f10427b;

    @DexIgnore
    public C3154vy(java.lang.String str, com.fossil.blesdk.obfuscated.e74 e74) {
        this.f10426a = str;
        this.f10427b = e74;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17229a() {
        try {
            return mo17230b().createNewFile();
        } catch (java.io.IOException e) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30063e("CrashlyticsCore", "Error creating marker: " + this.f10426a, e);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.io.File mo17230b() {
        return new java.io.File(this.f10427b.mo26861a(), this.f10426a);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo17231c() {
        return mo17230b().exists();
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo17232d() {
        return mo17230b().delete();
    }
}
