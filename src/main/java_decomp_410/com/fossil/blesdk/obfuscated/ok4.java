package com.fossil.blesdk.obfuscated;

import java.util.concurrent.RejectedExecutionException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.scheduling.CoroutineScheduler;
import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ok4 extends xh4 {
    @DexIgnore
    public CoroutineScheduler e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore
    public ok4(int i2, int i3, long j, String str) {
        kd4.b(str, "schedulerName");
        this.f = i2;
        this.g = i3;
        this.h = j;
        this.i = str;
        this.e = C();
    }

    @DexIgnore
    public final CoroutineScheduler C() {
        return new CoroutineScheduler(this.f, this.g, this.h, this.i);
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        try {
            CoroutineScheduler.a(this.e, runnable, (uk4) null, false, 6, (Object) null);
        } catch (RejectedExecutionException unused) {
            eh4.k.a(coroutineContext, runnable);
        }
    }

    @DexIgnore
    public void b(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        try {
            CoroutineScheduler.a(this.e, runnable, (uk4) null, true, 2, (Object) null);
        } catch (RejectedExecutionException unused) {
            eh4.k.b(coroutineContext, runnable);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ok4(int i2, int i3, String str, int i4, fd4 fd4) {
        this((i4 & 1) != 0 ? wk4.c : i2, (i4 & 2) != 0 ? wk4.d : i3, (i4 & 4) != 0 ? "DefaultDispatcher" : str);
    }

    @DexIgnore
    public final void a(Runnable runnable, uk4 uk4, boolean z) {
        kd4.b(runnable, "block");
        kd4.b(uk4, "context");
        try {
            this.e.a(runnable, uk4, z);
        } catch (RejectedExecutionException unused) {
            eh4.k.a((Runnable) this.e.a(runnable, uk4));
        }
    }

    @DexIgnore
    public final ug4 b(int i2) {
        if (i2 > 0) {
            return new qk4(this, i2, TaskMode.PROBABLY_BLOCKING);
        }
        throw new IllegalArgumentException(("Expected positive parallelism level, but have " + i2).toString());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ok4(int i2, int i3, String str) {
        this(i2, i3, wk4.e, str);
        kd4.b(str, "schedulerName");
    }
}
