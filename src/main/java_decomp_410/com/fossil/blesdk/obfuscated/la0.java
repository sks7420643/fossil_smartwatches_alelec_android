package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.model.microapp.enumerate.InstructionId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class la0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[InstructionId.values().length];

    /*
    static {
        a[InstructionId.ANIMATION.ordinal()] = 1;
        a[InstructionId.CLOSE.ordinal()] = 2;
        a[InstructionId.SWITCH_ACTIVITY.ordinal()] = 3;
        a[InstructionId.START_CRITICAL.ordinal()] = 4;
        a[InstructionId.END_CRITICAL.ordinal()] = 5;
        a[InstructionId.REMAP.ordinal()] = 6;
        a[InstructionId.START_REPEAT.ordinal()] = 7;
        a[InstructionId.END_REPEAT.ordinal()] = 8;
        a[InstructionId.DELAY.ordinal()] = 9;
        a[InstructionId.VIBE.ordinal()] = 10;
        a[InstructionId.STREAM.ordinal()] = 11;
        a[InstructionId.HID.ordinal()] = 12;
    }
    */
}
