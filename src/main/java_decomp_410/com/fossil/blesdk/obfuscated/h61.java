package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h61 extends vb1<h61> {
    @DexIgnore
    public static volatile h61[] e;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public long[] d; // = dc1.b;

    @DexIgnore
    public h61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static h61[] e() {
        if (e == null) {
            synchronized (zb1.b) {
                if (e == null) {
                    e = new h61[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        long[] jArr = this.d;
        if (jArr != null && jArr.length > 0) {
            int i = 0;
            while (true) {
                long[] jArr2 = this.d;
                if (i >= jArr2.length) {
                    break;
                }
                ub1.b(2, jArr2[i]);
                i++;
            }
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h61)) {
            return false;
        }
        h61 h61 = (h61) obj;
        Integer num = this.c;
        if (num == null) {
            if (h61.c != null) {
                return false;
            }
        } else if (!num.equals(h61.c)) {
            return false;
        }
        if (!zb1.a(this.d, h61.d)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(h61.b);
        }
        xb1 xb12 = h61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (h61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (((hashCode + (num == null ? 0 : num.hashCode())) * 31) + zb1.a(this.d)) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        long[] jArr = this.d;
        if (jArr == null || jArr.length <= 0) {
            return a;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            long[] jArr2 = this.d;
            if (i >= jArr2.length) {
                return a + i2 + (jArr2.length * 1);
            }
            i2 += ub1.b(jArr2[i]);
            i++;
        }
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 16) {
                int a = dc1.a(tb1, 16);
                long[] jArr = this.d;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.d, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = tb1.f();
                    tb1.c();
                    length++;
                }
                jArr2[length] = tb1.f();
                this.d = jArr2;
            } else if (c2 == 18) {
                int c3 = tb1.c(tb1.e());
                int a2 = tb1.a();
                int i = 0;
                while (tb1.l() > 0) {
                    tb1.f();
                    i++;
                }
                tb1.f(a2);
                long[] jArr3 = this.d;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.d, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = tb1.f();
                    length2++;
                }
                this.d = jArr4;
                tb1.d(c3);
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
