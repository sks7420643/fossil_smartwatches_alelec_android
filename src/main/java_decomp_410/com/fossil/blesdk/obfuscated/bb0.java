package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bb0 implements Runnable {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ wc4<qa4> f;

    @DexIgnore
    public bb0(wc4<qa4> wc4) {
        kd4.b(wc4, "task");
        this.f = wc4;
    }

    @DexIgnore
    public final void a() {
        this.e = true;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public void run() {
        if (!this.e) {
            this.f.invoke();
        }
    }
}
