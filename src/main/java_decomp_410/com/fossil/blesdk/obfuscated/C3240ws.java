package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ws */
public final class C3240ws {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<com.bumptech.glide.load.DecodeFormat> f10692f; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", com.bumptech.glide.load.DecodeFormat.DEFAULT);

    @DexIgnore
    /* renamed from: g */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<com.bumptech.glide.load.PreferredColorSpace> f10693g; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace", com.bumptech.glide.load.PreferredColorSpace.SRGB);

    @DexIgnore
    /* renamed from: h */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Boolean> f10694h; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", false);

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Boolean> f10695i; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", false);

    @DexIgnore
    /* renamed from: j */
    public static /* final */ java.util.Set<java.lang.String> f10696j; // = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"image/vnd.wap.wbmp", "image/x-ico"})));

    @DexIgnore
    /* renamed from: k */
    public static /* final */ com.fossil.blesdk.obfuscated.C3240ws.C3242b f10697k; // = new com.fossil.blesdk.obfuscated.C3240ws.C3241a();

    @DexIgnore
    /* renamed from: l */
    public static /* final */ java.util.Set<com.bumptech.glide.load.ImageHeaderParser.ImageType> f10698l; // = java.util.Collections.unmodifiableSet(java.util.EnumSet.of(com.bumptech.glide.load.ImageHeaderParser.ImageType.JPEG, com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG_A, com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG));

    @DexIgnore
    /* renamed from: m */
    public static /* final */ java.util.Queue<android.graphics.BitmapFactory.Options> f10699m; // = com.fossil.blesdk.obfuscated.C3066uw.m14928a(0);

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f10700a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.util.DisplayMetrics f10701b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f10702c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.List<com.bumptech.glide.load.ImageHeaderParser> f10703d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C1514bt f10704e; // = com.fossil.blesdk.obfuscated.C1514bt.m5110b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ws$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ws$a */
    public class C3241a implements com.fossil.blesdk.obfuscated.C3240ws.C3242b {
        @DexIgnore
        /* renamed from: a */
        public void mo11037a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11038a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap) {
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ws$b */
    public interface C3242b {
        @DexIgnore
        /* renamed from: a */
        void mo11037a();

        @DexIgnore
        /* renamed from: a */
        void mo11038a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap) throws java.io.IOException;
    }

    /*
    static {
        com.fossil.blesdk.obfuscated.C2232ko<com.bumptech.glide.load.resource.bitmap.DownsampleStrategy> koVar = com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2121f;
    }
    */

    @DexIgnore
    public C3240ws(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, android.util.DisplayMetrics displayMetrics, com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f10703d = list;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(displayMetrics);
        this.f10701b = displayMetrics;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(jqVar);
        this.f10700a = jqVar;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(gqVar);
        this.f10702c = gqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15953a(int i) {
        return i == 90 || i == 270;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m15954b(double d) {
        if (d > 1.0d) {
            d = 1.0d / d;
        }
        return (int) java.lang.Math.round(d * 2.147483647E9d);
    }

    @DexIgnore
    /* renamed from: c */
    public static int m15957c(double d) {
        return (int) (d + 0.5d);
    }

    @DexIgnore
    /* renamed from: c */
    public static void m15958c(android.graphics.BitmapFactory.Options options) {
        m15959d(options);
        synchronized (f10699m) {
            f10699m.offer(options);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m15959d(android.graphics.BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo17518a(java.io.InputStream inputStream, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return mo17519a(inputStream, i, i2, loVar, f10697k);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17522a(java.io.InputStream inputStream) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17523a(java.nio.ByteBuffer byteBuffer) {
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public static int[] m15956b(java.io.InputStream inputStream, android.graphics.BitmapFactory.Options options, com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar, com.fossil.blesdk.obfuscated.C2149jq jqVar) throws java.io.IOException {
        options.inJustDecodeBounds = true;
        m15945a(inputStream, options, bVar, jqVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo17519a(java.io.InputStream inputStream, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar, com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C2337lo loVar2 = loVar;
        com.fossil.blesdk.obfuscated.C2992tw.m14461a(inputStream.markSupported(), "You must provide an InputStream that supports mark()");
        byte[] bArr = (byte[]) this.f10702c.mo11285b(65536, byte[].class);
        android.graphics.BitmapFactory.Options a = m15946a();
        a.inTempStorage = bArr;
        com.bumptech.glide.load.DecodeFormat decodeFormat = (com.bumptech.glide.load.DecodeFormat) loVar2.mo13319a(f10692f);
        com.bumptech.glide.load.PreferredColorSpace preferredColorSpace = (com.bumptech.glide.load.PreferredColorSpace) loVar2.mo13319a(f10693g);
        try {
            return com.fossil.blesdk.obfuscated.C2675ps.m12395a(mo17517a(inputStream, a, (com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) loVar2.mo13319a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2121f), decodeFormat, preferredColorSpace, loVar2.mo13319a(f10695i) != null && ((java.lang.Boolean) loVar2.mo13319a(f10695i)).booleanValue(), i, i2, ((java.lang.Boolean) loVar2.mo13319a(f10694h)).booleanValue(), bVar), this.f10700a);
        } finally {
            m15958c(a);
            this.f10702c.put(bArr);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m15955b(android.graphics.BitmapFactory.Options options) {
        int i = options.inTargetDensity;
        if (i > 0) {
            int i2 = options.inDensity;
            return i2 > 0 && i != i2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.Bitmap mo17517a(java.io.InputStream inputStream, android.graphics.BitmapFactory.Options options, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.bumptech.glide.load.DecodeFormat decodeFormat, com.bumptech.glide.load.PreferredColorSpace preferredColorSpace, boolean z, int i, int i2, boolean z2, com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar) throws java.io.IOException {
        int i3;
        int i4;
        com.fossil.blesdk.obfuscated.C3240ws wsVar;
        int i5;
        int i6;
        int i7;
        java.io.InputStream inputStream2 = inputStream;
        android.graphics.BitmapFactory.Options options2 = options;
        com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar2 = bVar;
        long a = com.fossil.blesdk.obfuscated.C2682pw.m12452a();
        int[] b = m15956b(inputStream2, options2, bVar2, this.f10700a);
        boolean z3 = false;
        int i8 = b[0];
        int i9 = b[1];
        java.lang.String str = options2.outMimeType;
        boolean z4 = (i8 == -1 || i9 == -1) ? false : z;
        int a2 = com.fossil.blesdk.obfuscated.C2049io.m8571a(this.f10703d, inputStream2, this.f10702c);
        int a3 = com.fossil.blesdk.obfuscated.C1904gt.m7576a(a2);
        boolean b2 = com.fossil.blesdk.obfuscated.C1904gt.m7586b(a2);
        int i10 = i;
        if (i10 == Integer.MIN_VALUE) {
            i4 = i2;
            i3 = m15953a(a3) ? i9 : i8;
        } else {
            i4 = i2;
            i3 = i10;
        }
        int i11 = i4 == Integer.MIN_VALUE ? m15953a(a3) ? i8 : i9 : i4;
        com.bumptech.glide.load.ImageHeaderParser.ImageType b3 = com.fossil.blesdk.obfuscated.C2049io.m8573b(this.f10703d, inputStream2, this.f10702c);
        com.fossil.blesdk.obfuscated.C2149jq jqVar = this.f10700a;
        com.bumptech.glide.load.ImageHeaderParser.ImageType imageType = b3;
        m15952a(b3, inputStream, bVar, jqVar, downsampleStrategy, a3, i8, i9, i3, i11, options);
        int i12 = a2;
        java.lang.String str2 = str;
        int i13 = i9;
        int i14 = i8;
        com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar3 = bVar2;
        android.graphics.BitmapFactory.Options options3 = options2;
        mo17520a(inputStream, decodeFormat, z4, b2, options, i3, i11);
        boolean z5 = android.os.Build.VERSION.SDK_INT >= 19;
        if (options3.inSampleSize == 1 || z5) {
            wsVar = this;
            if (wsVar.mo17521a(imageType)) {
                if (i14 < 0 || i13 < 0 || !z2 || !z5) {
                    float f = m15955b(options) ? ((float) options3.inTargetDensity) / ((float) options3.inDensity) : 1.0f;
                    int i15 = options3.inSampleSize;
                    float f2 = (float) i15;
                    i7 = java.lang.Math.round(((float) ((int) java.lang.Math.ceil((double) (((float) i14) / f2)))) * f);
                    i6 = java.lang.Math.round(((float) ((int) java.lang.Math.ceil((double) (((float) i13) / f2)))) * f);
                    if (android.util.Log.isLoggable("Downsampler", 2)) {
                        android.util.Log.v("Downsampler", "Calculated target [" + i7 + "x" + i6 + "] for source [" + i14 + "x" + i13 + "], sampleSize: " + i15 + ", targetDensity: " + options3.inTargetDensity + ", density: " + options3.inDensity + ", density multiplier: " + f);
                    }
                } else {
                    i7 = i3;
                    i6 = i11;
                }
                if (i7 > 0 && i6 > 0) {
                    m15951a(options3, wsVar.f10700a, i7, i6);
                }
            }
        } else {
            wsVar = this;
        }
        int i16 = android.os.Build.VERSION.SDK_INT;
        if (i16 >= 28) {
            if (preferredColorSpace == com.bumptech.glide.load.PreferredColorSpace.DISPLAY_P3) {
                android.graphics.ColorSpace colorSpace = options3.outColorSpace;
                if (colorSpace != null && colorSpace.isWideGamut()) {
                    z3 = true;
                }
            }
            options3.inPreferredColorSpace = android.graphics.ColorSpace.get(z3 ? android.graphics.ColorSpace.Named.DISPLAY_P3 : android.graphics.ColorSpace.Named.SRGB);
        } else if (i16 >= 26) {
            options3.inPreferredColorSpace = android.graphics.ColorSpace.get(android.graphics.ColorSpace.Named.SRGB);
        }
        android.graphics.Bitmap a4 = m15945a(inputStream, options3, bVar3, wsVar.f10700a);
        bVar3.mo11038a(wsVar.f10700a, a4);
        if (android.util.Log.isLoggable("Downsampler", 2)) {
            i5 = i12;
            m15950a(i14, i13, str2, options, a4, i, i2, a);
        } else {
            i5 = i12;
        }
        android.graphics.Bitmap bitmap = null;
        if (a4 != null) {
            a4.setDensity(wsVar.f10701b.densityDpi);
            bitmap = com.fossil.blesdk.obfuscated.C1904gt.m7578a(wsVar.f10700a, a4, i5);
            if (!a4.equals(bitmap)) {
                wsVar.f10700a.mo12446a(a4);
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15952a(com.bumptech.glide.load.ImageHeaderParser.ImageType imageType, java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar, com.fossil.blesdk.obfuscated.C2149jq jqVar, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, int i, int i2, int i3, int i4, int i5, android.graphics.BitmapFactory.Options options) throws java.io.IOException {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        double d;
        com.bumptech.glide.load.ImageHeaderParser.ImageType imageType2 = imageType;
        com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
        int i12 = i2;
        int i13 = i3;
        int i14 = i4;
        int i15 = i5;
        android.graphics.BitmapFactory.Options options2 = options;
        if (i12 <= 0 || i13 <= 0) {
            java.lang.String str = "x";
            if (android.util.Log.isLoggable("Downsampler", 3)) {
                android.util.Log.d("Downsampler", "Unable to determine dimensions for: " + imageType2 + " with target [" + i14 + str + i15 + "]");
                return;
            }
            return;
        }
        if (m15953a(i)) {
            i6 = i12;
            i7 = i13;
        } else {
            i7 = i12;
            i6 = i13;
        }
        float b = downsampleStrategy2.mo4006b(i7, i6, i14, i15);
        if (b > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.SampleSizeRounding a = downsampleStrategy2.mo4005a(i7, i6, i14, i15);
            if (a != null) {
                float f = (float) i7;
                float f2 = (float) i6;
                int c = i7 / m15957c((double) (b * f));
                int c2 = i6 / m15957c((double) (b * f2));
                if (a == com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.SampleSizeRounding.MEMORY) {
                    i8 = java.lang.Math.max(c, c2);
                } else {
                    i8 = java.lang.Math.min(c, c2);
                }
                java.lang.String str2 = "x";
                if (android.os.Build.VERSION.SDK_INT > 23 || !f10696j.contains(options2.outMimeType)) {
                    i9 = java.lang.Math.max(1, java.lang.Integer.highestOneBit(i8));
                    if (a == com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.SampleSizeRounding.MEMORY && ((float) i9) < 1.0f / b) {
                        i9 <<= 1;
                    }
                } else {
                    i9 = 1;
                }
                options2.inSampleSize = i9;
                if (imageType2 == com.bumptech.glide.load.ImageHeaderParser.ImageType.JPEG) {
                    float min = (float) java.lang.Math.min(i9, 8);
                    i10 = (int) java.lang.Math.ceil((double) (f / min));
                    i11 = (int) java.lang.Math.ceil((double) (f2 / min));
                    int i16 = i9 / 8;
                    if (i16 > 0) {
                        i10 /= i16;
                        i11 /= i16;
                    }
                } else {
                    if (imageType2 == com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG || imageType2 == com.bumptech.glide.load.ImageHeaderParser.ImageType.PNG_A) {
                        float f3 = (float) i9;
                        i10 = (int) java.lang.Math.floor((double) (f / f3));
                        d = java.lang.Math.floor((double) (f2 / f3));
                    } else if (imageType2 == com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP || imageType2 == com.bumptech.glide.load.ImageHeaderParser.ImageType.WEBP_A) {
                        if (android.os.Build.VERSION.SDK_INT >= 24) {
                            float f4 = (float) i9;
                            i10 = java.lang.Math.round(f / f4);
                            i11 = java.lang.Math.round(f2 / f4);
                        } else {
                            float f5 = (float) i9;
                            i10 = (int) java.lang.Math.floor((double) (f / f5));
                            d = java.lang.Math.floor((double) (f2 / f5));
                        }
                    } else if (i7 % i9 == 0 && i6 % i9 == 0) {
                        i10 = i7 / i9;
                        i11 = i6 / i9;
                    } else {
                        int[] b2 = m15956b(inputStream, options2, bVar, jqVar);
                        i10 = b2[0];
                        i11 = b2[1];
                    }
                    i11 = (int) d;
                }
                double b3 = (double) downsampleStrategy2.mo4006b(i10, i11, i14, i15);
                if (android.os.Build.VERSION.SDK_INT >= 19) {
                    options2.inTargetDensity = m15944a(b3);
                    options2.inDensity = m15954b(b3);
                }
                if (m15955b(options)) {
                    options2.inScaled = true;
                } else {
                    options2.inTargetDensity = 0;
                    options2.inDensity = 0;
                }
                if (android.util.Log.isLoggable("Downsampler", 2)) {
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    sb.append("Calculate scaling, source: [");
                    sb.append(i2);
                    java.lang.String str3 = str2;
                    sb.append(str3);
                    sb.append(i3);
                    sb.append("], degreesToRotate: ");
                    sb.append(i);
                    sb.append(", target: [");
                    sb.append(i14);
                    sb.append(str3);
                    sb.append(i15);
                    sb.append("], power of two scaled: [");
                    sb.append(i10);
                    sb.append(str3);
                    sb.append(i11);
                    sb.append("], exact scale factor: ");
                    sb.append(b);
                    sb.append(", power of 2 sample size: ");
                    sb.append(i9);
                    sb.append(", adjusted scale factor: ");
                    sb.append(b3);
                    sb.append(", target density: ");
                    sb.append(options2.inTargetDensity);
                    sb.append(", density: ");
                    sb.append(options2.inDensity);
                    android.util.Log.v("Downsampler", sb.toString());
                    return;
                }
                return;
            }
            throw new java.lang.IllegalArgumentException("Cannot round with null rounding");
        }
        java.lang.String str4 = "x";
        int i17 = i12;
        throw new java.lang.IllegalArgumentException("Cannot scale with factor: " + b + " from: " + downsampleStrategy2 + ", source: [" + i17 + str4 + i13 + "], target: [" + i14 + str4 + i15 + "]");
    }

    @DexIgnore
    /* renamed from: a */
    public static int m15944a(double d) {
        int b = m15954b(d);
        int c = m15957c(((double) b) * d);
        return m15957c((d / ((double) (((float) c) / ((float) b)))) * ((double) c));
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo17521a(com.bumptech.glide.load.ImageHeaderParser.ImageType imageType) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return f10698l.contains(imageType);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17520a(java.io.InputStream inputStream, com.bumptech.glide.load.DecodeFormat decodeFormat, boolean z, boolean z2, android.graphics.BitmapFactory.Options options, int i, int i2) {
        if (!this.f10704e.mo9292a(i, i2, options, z, z2)) {
            if (decodeFormat == com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888 || android.os.Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = android.graphics.Bitmap.Config.ARGB_8888;
                return;
            }
            boolean z3 = false;
            try {
                z3 = com.fossil.blesdk.obfuscated.C2049io.m8573b(this.f10703d, inputStream, this.f10702c).hasAlpha();
            } catch (java.io.IOException e) {
                if (android.util.Log.isLoggable("Downsampler", 3)) {
                    android.util.Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + decodeFormat, e);
                }
            }
            options.inPreferredConfig = z3 ? android.graphics.Bitmap.Config.ARGB_8888 : android.graphics.Bitmap.Config.RGB_565;
            if (options.inPreferredConfig == android.graphics.Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r1;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005d */
    /* renamed from: a */
    public static android.graphics.Bitmap m15945a(java.io.InputStream inputStream, android.graphics.BitmapFactory.Options options, com.fossil.blesdk.obfuscated.C3240ws.C3242b bVar, com.fossil.blesdk.obfuscated.C2149jq jqVar) throws java.io.IOException {
        if (options.inJustDecodeBounds) {
            inputStream.mark(com.zendesk.sdk.network.impl.CachingAuthorizingOkHttp3Downloader.MIN_DISK_CACHE_SIZE);
        } else {
            bVar.mo11037a();
        }
        int i = options.outWidth;
        int i2 = options.outHeight;
        java.lang.String str = options.outMimeType;
        com.fossil.blesdk.obfuscated.C1904gt.m7580a().lock();
        try {
            android.graphics.Bitmap decodeStream = android.graphics.BitmapFactory.decodeStream(inputStream, (android.graphics.Rect) null, options);
            com.fossil.blesdk.obfuscated.C1904gt.m7580a().unlock();
            if (options.inJustDecodeBounds) {
                inputStream.reset();
            }
            return decodeStream;
        } catch (java.lang.IllegalArgumentException e) {
            java.io.IOException a = m15947a(e, i, i2, str, options);
            if (android.util.Log.isLoggable("Downsampler", 3)) {
                android.util.Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", a);
            }
            if (options.inBitmap != null) {
                inputStream.reset();
                jqVar.mo12446a(options.inBitmap);
                options.inBitmap = null;
                android.graphics.Bitmap a2 = m15945a(inputStream, options, bVar, jqVar);
                com.fossil.blesdk.obfuscated.C1904gt.m7580a().unlock();
                return a2;
            }
            throw a;
        } catch (Throwable th) {
            com.fossil.blesdk.obfuscated.C1904gt.m7580a().unlock();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15950a(int i, int i2, java.lang.String str, android.graphics.BitmapFactory.Options options, android.graphics.Bitmap bitmap, int i3, int i4, long j) {
        android.util.Log.v("Downsampler", "Decoded " + m15948a(bitmap) + " from [" + i + "x" + i2 + "] " + str + " with inBitmap " + m15949a(options) + " for [" + i3 + "x" + i4 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + java.lang.Thread.currentThread().getName() + ", duration: " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(j));
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m15949a(android.graphics.BitmapFactory.Options options) {
        return m15948a(options.inBitmap);
    }

    @DexIgnore
    @android.annotation.TargetApi(19)
    /* renamed from: a */
    public static java.lang.String m15948a(android.graphics.Bitmap bitmap) {
        java.lang.String str;
        if (bitmap == null) {
            return null;
        }
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.io.IOException m15947a(java.lang.IllegalArgumentException illegalArgumentException, int i, int i2, java.lang.String str, android.graphics.BitmapFactory.Options options) {
        return new java.io.IOException("Exception decoding bitmap, outWidth: " + i + ", outHeight: " + i2 + ", outMimeType: " + str + ", inBitmap: " + m15949a(options), illegalArgumentException);
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: a */
    public static void m15951a(android.graphics.BitmapFactory.Options options, com.fossil.blesdk.obfuscated.C2149jq jqVar, int i, int i2) {
        android.graphics.Bitmap.Config config;
        if (android.os.Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != android.graphics.Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = jqVar.mo12447b(i, i2, config);
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized android.graphics.BitmapFactory.Options m15946a() {
        android.graphics.BitmapFactory.Options poll;
        synchronized (com.fossil.blesdk.obfuscated.C3240ws.class) {
            synchronized (f10699m) {
                poll = f10699m.poll();
            }
            if (poll == null) {
                poll = new android.graphics.BitmapFactory.Options();
                m15959d(poll);
            }
        }
        return poll;
    }
}
