package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tm2 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public void a(xz1 xz1) {
        if (xz1.d("platform")) {
            xz1.a("platform").f();
        }
        if (xz1.d("data")) {
            this.a = xz1.a("data").d().a("url").f();
        }
        if (xz1.d("metadata")) {
            this.b = xz1.a("metadata").d().a("checksum").f();
        }
        if (xz1.d("metadata")) {
            this.c = xz1.a("metadata").d().a("appVersion").f();
        }
        if (xz1.d("createdAt")) {
            xz1.a("createdAt").f();
        }
        if (xz1.d("updatedAt")) {
            this.d = xz1.a("updatedAt").f();
        }
        if (xz1.d("objectId")) {
            xz1.a("objectId").f();
        }
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
