package com.fossil.blesdk.obfuscated;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vv1<T> {
    @DexIgnore
    public final Type capture() {
        Type genericSuperclass = getClass().getGenericSuperclass();
        st1.a(genericSuperclass instanceof ParameterizedType, "%s isn't parameterized", (Object) genericSuperclass);
        return ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
    }
}
