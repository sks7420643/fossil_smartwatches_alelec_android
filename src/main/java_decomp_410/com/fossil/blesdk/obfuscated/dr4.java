package com.fossil.blesdk.obfuscated;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dr4<R, T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public static Type a(int i, ParameterizedType parameterizedType) {
            return ur4.b(i, parameterizedType);
        }

        @DexIgnore
        public abstract dr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3);

        @DexIgnore
        public static Class<?> a(Type type) {
            return ur4.b(type);
        }
    }

    @DexIgnore
    T a(Call<R> call);

    @DexIgnore
    Type a();
}
