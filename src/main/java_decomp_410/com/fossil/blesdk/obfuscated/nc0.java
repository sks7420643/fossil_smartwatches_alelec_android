package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nc0 extends ec0 {
    @DexIgnore
    public /* final */ /* synthetic */ mc0 e;

    @DexIgnore
    public nc0(mc0 mc0) {
        this.e = mc0;
    }

    @DexIgnore
    public final void b(Status status) throws RemoteException {
        this.e.a(status);
    }
}
