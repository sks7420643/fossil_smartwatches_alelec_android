package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x1 */
public abstract class C3254x1 extends android.view.ViewGroup {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3254x1.C3255a f10803e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.Context f10804f;

    @DexIgnore
    /* renamed from: g */
    public androidx.appcompat.widget.ActionMenuView f10805g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3400z1 f10806h;

    @DexIgnore
    /* renamed from: i */
    public int f10807i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2110j9 f10808j;

    @DexIgnore
    /* renamed from: k */
    public boolean f10809k;

    @DexIgnore
    /* renamed from: l */
    public boolean f10810l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.x1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.x1$a */
    public class C3255a implements com.fossil.blesdk.obfuscated.C2190k9 {

        @DexIgnore
        /* renamed from: a */
        public boolean f10811a; // = false;

        @DexIgnore
        /* renamed from: b */
        public int f10812b;

        @DexIgnore
        public C3255a() {
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3254x1.C3255a mo17589a(com.fossil.blesdk.obfuscated.C2110j9 j9Var, int i) {
            com.fossil.blesdk.obfuscated.C3254x1.this.f10808j = j9Var;
            this.f10812b = i;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            if (!this.f10811a) {
                com.fossil.blesdk.obfuscated.C3254x1 x1Var = com.fossil.blesdk.obfuscated.C3254x1.this;
                x1Var.f10808j = null;
                com.fossil.blesdk.obfuscated.C3254x1.super.setVisibility(this.f10812b);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8507c(android.view.View view) {
            com.fossil.blesdk.obfuscated.C3254x1.super.setVisibility(0);
            this.f10811a = false;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8505a(android.view.View view) {
            this.f10811a = true;
        }
    }

    @DexIgnore
    public C3254x1(android.content.Context context) {
        this(context, (android.util.AttributeSet) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m16096a(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    @DexIgnore
    public int getAnimatedVisibility() {
        if (this.f10808j != null) {
            return this.f10803e.f10812b;
        }
        return getVisibility();
    }

    @DexIgnore
    public int getContentHeight() {
        return this.f10807i;
    }

    @DexIgnore
    public void onConfigurationChanged(android.content.res.Configuration configuration) {
        super.onConfigurationChanged(configuration);
        android.content.res.TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes((android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C1368a0.ActionBar, com.fossil.blesdk.obfuscated.C2777r.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        com.fossil.blesdk.obfuscated.C3400z1 z1Var = this.f10806h;
        if (z1Var != null) {
            z1Var.mo18389a(configuration);
        }
    }

    @DexIgnore
    public boolean onHoverEvent(android.view.MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.f10810l = false;
        }
        if (!this.f10810l) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.f10810l = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.f10810l = false;
        }
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(android.view.MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.f10809k = false;
        }
        if (!this.f10809k) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.f10809k = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.f10809k = false;
        }
        return true;
    }

    @DexIgnore
    public abstract void setContentHeight(int i);

    @DexIgnore
    public void setVisibility(int i) {
        if (i != getVisibility()) {
            com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f10808j;
            if (j9Var != null) {
                j9Var.mo12280a();
            }
            super.setVisibility(i);
        }
    }

    @DexIgnore
    public C3254x1(android.content.Context context, android.util.AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo17585a(int i, long j) {
        com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f10808j;
        if (j9Var != null) {
            j9Var.mo12280a();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                setAlpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this);
            a.mo12275a(1.0f);
            a.mo12276a(j);
            com.fossil.blesdk.obfuscated.C3254x1.C3255a aVar = this.f10803e;
            aVar.mo17589a(a, i);
            a.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) aVar);
            return a;
        }
        com.fossil.blesdk.obfuscated.C2110j9 a2 = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this);
        a2.mo12275a((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a2.mo12276a(j);
        com.fossil.blesdk.obfuscated.C3254x1.C3255a aVar2 = this.f10803e;
        aVar2.mo17589a(a2, i);
        a2.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) aVar2);
        return a2;
    }

    @DexIgnore
    public C3254x1(android.content.Context context, android.util.AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f10803e = new com.fossil.blesdk.obfuscated.C3254x1.C3255a();
        android.util.TypedValue typedValue = new android.util.TypedValue();
        if (context.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarPopupTheme, typedValue, true)) {
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.f10804f = new android.view.ContextThemeWrapper(context, i2);
                return;
            }
        }
        this.f10804f = context;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo17583a(android.view.View view, int i, int i2, int i3) {
        view.measure(android.view.View.MeasureSpec.makeMeasureSpec(i, Integer.MIN_VALUE), i2);
        return java.lang.Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo17584a(android.view.View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) / 2);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
