package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import com.google.firebase.components.DependencyCycleException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iw1<T> {
    @DexIgnore
    public /* final */ Set<Class<? super T>> a;
    @DexIgnore
    public /* final */ Set<mw1> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ kw1<T> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a<T> {
        @DexIgnore
        public /* final */ T a;
        @DexIgnore
        public /* final */ qw1<T> b;

        @DexIgnore
        public a(T t, qw1<T> qw1) {
            this.a = t;
            this.b = qw1;
        }

        @DexIgnore
        public static a<Context> a(Context context) {
            return new a<>(context, new pw1((byte) 0));
        }

        @DexIgnore
        public static List<lw1> b(List<String> list) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                try {
                    Class<?> cls = Class.forName(next);
                    if (!lw1.class.isAssignableFrom(cls)) {
                        Log.w("ComponentDiscovery", String.format("Class %s is not an instance of %s", new Object[]{next, "com.google.firebase.components.ComponentRegistrar"}));
                    } else {
                        arrayList.add((lw1) cls.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
                    }
                } catch (ClassNotFoundException e) {
                    Log.w("ComponentDiscovery", String.format("Class %s is not an found.", new Object[]{next}), e);
                } catch (IllegalAccessException e2) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{next}), e2);
                } catch (InstantiationException e3) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s.", new Object[]{next}), e3);
                } catch (NoSuchMethodException e4) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s", new Object[]{next}), e4);
                } catch (InvocationTargetException e5) {
                    Log.w("ComponentDiscovery", String.format("Could not instantiate %s", new Object[]{next}), e5);
                }
            }
            return arrayList;
        }

        @DexIgnore
        public List<lw1> a() {
            return b(this.b.zza(this.a));
        }

        @DexIgnore
        public static List<iw1<?>> a(List<iw1<?>> list) {
            HashMap hashMap = new HashMap(list.size());
            for (iw1 next : list) {
                sw1 sw1 = new sw1(next);
                Iterator it = next.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Class cls = (Class) it.next();
                        if (hashMap.put(cls, sw1) != null) {
                            throw new IllegalArgumentException(String.format("Multiple components provide %s.", new Object[]{cls}));
                        }
                    }
                }
            }
            for (sw1 sw12 : hashMap.values()) {
                for (mw1 next2 : sw12.b().b()) {
                    if (next2.c()) {
                        sw1 sw13 = (sw1) hashMap.get(next2.a());
                        if (sw13 != null) {
                            sw12.a(sw13);
                            sw13.b(sw12);
                        }
                    }
                }
            }
            HashSet<sw1> hashSet = new HashSet<>(hashMap.values());
            Set<sw1> a2 = a((Set<sw1>) hashSet);
            ArrayList arrayList = new ArrayList();
            while (!a2.isEmpty()) {
                sw1 next3 = a2.iterator().next();
                a2.remove(next3);
                arrayList.add(next3.b());
                for (sw1 next4 : next3.a()) {
                    next4.c(next3);
                    if (next4.c()) {
                        a2.add(next4);
                    }
                }
            }
            if (arrayList.size() == list.size()) {
                Collections.reverse(arrayList);
                return arrayList;
            }
            ArrayList arrayList2 = new ArrayList();
            for (sw1 sw14 : hashSet) {
                if (!sw14.c() && !sw14.d()) {
                    arrayList2.add(sw14.b());
                }
            }
            throw new DependencyCycleException(arrayList2);
        }

        @DexIgnore
        public static Set<sw1> a(Set<sw1> set) {
            HashSet hashSet = new HashSet();
            for (sw1 next : set) {
                if (next.c()) {
                    hashSet.add(next);
                }
            }
            return hashSet;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> {
        @DexIgnore
        public /* final */ Set<Class<? super T>> a;
        @DexIgnore
        public /* final */ Set<mw1> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public kw1<T> d;
        @DexIgnore
        public Set<Class<?>> e;

        @DexIgnore
        public /* synthetic */ b(Class cls, Class[] clsArr, byte b2) {
            this(cls, clsArr);
        }

        @DexIgnore
        public b<T> a(mw1 mw1) {
            bk0.a(mw1, (Object) "Null dependency");
            bk0.a(!this.a.contains(mw1.a()), (Object) "Components are not allowed to depend on interfaces they themselves provide.");
            this.b.add(mw1);
            return this;
        }

        @DexIgnore
        public iw1<T> b() {
            bk0.b(this.d != null, "Missing required property: factory.");
            return new iw1(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, (byte) 0);
        }

        @DexIgnore
        public b<T> c() {
            a(2);
            return this;
        }

        @DexIgnore
        public b(Class<T> cls, Class<? super T>... clsArr) {
            this.a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.e = new HashSet();
            bk0.a(cls, (Object) "Null interface");
            this.a.add(cls);
            for (Class<? super T> a2 : clsArr) {
                bk0.a(a2, (Object) "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        @DexIgnore
        public b<T> a() {
            a(1);
            return this;
        }

        @DexIgnore
        public final b<T> a(int i) {
            bk0.b(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public b<T> a(kw1<T> kw1) {
            bk0.a(kw1, (Object) "Null factory");
            this.d = kw1;
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ iw1(Set set, Set set2, int i, kw1 kw1, Set set3, byte b2) {
        this(set, set2, i, kw1, set3);
    }

    @DexIgnore
    public static /* synthetic */ Object a(Object obj) {
        return obj;
    }

    @DexIgnore
    public final Set<Class<? super T>> a() {
        return this.a;
    }

    @DexIgnore
    public final Set<mw1> b() {
        return this.b;
    }

    @DexIgnore
    public final kw1<T> c() {
        return this.d;
    }

    @DexIgnore
    public final Set<Class<?>> d() {
        return this.e;
    }

    @DexIgnore
    public final boolean e() {
        return this.c == 1;
    }

    @DexIgnore
    public final boolean f() {
        return this.c == 2;
    }

    @DexIgnore
    public final String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }

    @DexIgnore
    public iw1(Set<Class<? super T>> set, Set<mw1> set2, int i, kw1<T> kw1, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = kw1;
        this.e = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0], (byte) 0);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr, (byte) 0);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> iw1<T> a(T t, Class<T> cls, Class<? super T>... clsArr) {
        b<T> a2 = a(cls, clsArr);
        a2.a((kw1<T>) ow1.a((Object) t));
        return a2.b();
    }
}
