package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.th */
public class C2966th implements com.fossil.blesdk.obfuscated.C3038uh {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.ViewGroupOverlay f9669a;

    @DexIgnore
    public C2966th(android.view.ViewGroup viewGroup) {
        this.f9669a = viewGroup.getOverlay();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8741a(android.graphics.drawable.Drawable drawable) {
        this.f9669a.add(drawable);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8742b(android.graphics.drawable.Drawable drawable) {
        this.f9669a.remove(drawable);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15987a(android.view.View view) {
        this.f9669a.add(view);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15988b(android.view.View view) {
        this.f9669a.remove(view);
    }
}
