package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p8 */
public final class C2634p8 {
    @DexIgnore
    /* renamed from: a */
    public static void m12136a(int i, int i2, int i3, android.graphics.Rect rect, android.graphics.Rect rect2, int i4) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            android.view.Gravity.apply(i, i2, i3, rect, rect2, i4);
        } else {
            android.view.Gravity.apply(i, i2, i3, rect, rect2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m12135a(int i, int i2) {
        return android.os.Build.VERSION.SDK_INT >= 17 ? android.view.Gravity.getAbsoluteGravity(i, i2) : i & -8388609;
    }
}
