package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ea2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ AppCompatAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ LinearLayout w;
    @DexIgnore
    public /* final */ RecyclerView x;

    @DexIgnore
    public ea2(Object obj, View view, int i, AppCompatAutoCompleteTextView appCompatAutoCompleteTextView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, View view2, LinearLayout linearLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = appCompatAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = rTLImageView;
        this.v = view2;
        this.w = linearLayout;
        this.x = recyclerView;
    }
}
