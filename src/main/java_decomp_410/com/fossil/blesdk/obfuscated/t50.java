package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t50 extends SingleRequestPhase {
    @DexIgnore
    public int B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t50(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.READ_RSSI, new w60(peripheral));
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
    }

    @DexIgnore
    public void c(Request request) {
        kd4.b(request, "request");
        if (request instanceof w60) {
            this.B = ((w60) request).B();
        }
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(super.x(), JSONKey.RSSI, Integer.valueOf(this.B));
    }

    @DexIgnore
    public Integer i() {
        return Integer.valueOf(this.B);
    }
}
