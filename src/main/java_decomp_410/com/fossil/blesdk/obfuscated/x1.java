package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class x1 extends ViewGroup {
    @DexIgnore
    public /* final */ a e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public ActionMenuView g;
    @DexIgnore
    public z1 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public j9 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;

    @DexIgnore
    public x1(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public static int a(int i2, int i3, boolean z) {
        return z ? i2 - i3 : i2 + i3;
    }

    @DexIgnore
    public int getAnimatedVisibility() {
        if (this.j != null) {
            return this.e.b;
        }
        return getVisibility();
    }

    @DexIgnore
    public int getContentHeight() {
        return this.i;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes((AttributeSet) null, a0.ActionBar, r.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(a0.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        z1 z1Var = this.h;
        if (z1Var != null) {
            z1Var.a(configuration);
        }
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.l = false;
        }
        if (!this.l) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.l = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.l = false;
        }
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.k = false;
        }
        if (!this.k) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.k = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.k = false;
        }
        return true;
    }

    @DexIgnore
    public abstract void setContentHeight(int i2);

    @DexIgnore
    public void setVisibility(int i2) {
        if (i2 != getVisibility()) {
            j9 j9Var = this.j;
            if (j9Var != null) {
                j9Var.a();
            }
            super.setVisibility(i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements k9 {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a a(j9 j9Var, int i) {
            x1.this.j = j9Var;
            this.b = i;
            return this;
        }

        @DexIgnore
        public void b(View view) {
            if (!this.a) {
                x1 x1Var = x1.this;
                x1Var.j = null;
                x1.super.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void c(View view) {
            x1.super.setVisibility(0);
            this.a = false;
        }

        @DexIgnore
        public void a(View view) {
            this.a = true;
        }
    }

    @DexIgnore
    public x1(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public j9 a(int i2, long j2) {
        j9 j9Var = this.j;
        if (j9Var != null) {
            j9Var.a();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            j9 a2 = f9.a(this);
            a2.a(1.0f);
            a2.a(j2);
            a aVar = this.e;
            aVar.a(a2, i2);
            a2.a((k9) aVar);
            return a2;
        }
        j9 a3 = f9.a(this);
        a3.a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a3.a(j2);
        a aVar2 = this.e;
        aVar2.a(a3, i2);
        a3.a((k9) aVar2);
        return a3;
    }

    @DexIgnore
    public x1(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.e = new a();
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(r.actionBarPopupTheme, typedValue, true)) {
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                this.f = new ContextThemeWrapper(context, i3);
                return;
            }
        }
        this.f = context;
    }

    @DexIgnore
    public int a(View view, int i2, int i3, int i4) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) - i4);
    }

    @DexIgnore
    public int a(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = i3 + ((i4 - measuredHeight) / 2);
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
