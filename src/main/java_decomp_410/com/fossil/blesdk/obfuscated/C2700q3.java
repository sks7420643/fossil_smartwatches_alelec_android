package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q3 */
public abstract class C2700q3 extends android.app.Service {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.Map<android.os.IBinder, android.os.IBinder.DeathRecipient> f8516e; // = new com.fossil.blesdk.obfuscated.C1855g4();

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1455b.C1456a f8517f; // = new com.fossil.blesdk.obfuscated.C2700q3.C2701a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.q3$a */
    public class C2701a extends com.fossil.blesdk.obfuscated.C1455b.C1456a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.q3$a$a */
        public class C2702a implements android.os.IBinder.DeathRecipient {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2859s3 f8519a;

            @DexIgnore
            public C2702a(com.fossil.blesdk.obfuscated.C2859s3 s3Var) {
                this.f8519a = s3Var;
            }

            @DexIgnore
            public void binderDied() {
                com.fossil.blesdk.obfuscated.C2700q3.this.mo14990a(this.f8519a);
            }
        }

        @DexIgnore
        public C2701a() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8995a(long j) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14989a(j);
        }

        @DexIgnore
        /* renamed from: b */
        public android.os.Bundle mo9001b(java.lang.String str, android.os.Bundle bundle) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14988a(str, bundle);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8996a(com.fossil.blesdk.obfuscated.C1365a aVar) {
            com.fossil.blesdk.obfuscated.C2859s3 s3Var = new com.fossil.blesdk.obfuscated.C2859s3(aVar);
            try {
                com.fossil.blesdk.obfuscated.C2700q3.C2701a.C2702a aVar2 = new com.fossil.blesdk.obfuscated.C2700q3.C2701a.C2702a(s3Var);
                synchronized (com.fossil.blesdk.obfuscated.C2700q3.this.f8516e) {
                    aVar.asBinder().linkToDeath(aVar2, 0);
                    com.fossil.blesdk.obfuscated.C2700q3.this.f8516e.put(aVar.asBinder(), aVar2);
                }
                return com.fossil.blesdk.obfuscated.C2700q3.this.mo14995b(s3Var);
            } catch (android.os.RemoteException unused) {
                return false;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo9002b(com.fossil.blesdk.obfuscated.C1365a aVar, android.os.Bundle bundle) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14994a(new com.fossil.blesdk.obfuscated.C2859s3(aVar), bundle);
        }

        @DexIgnore
        /* renamed from: b */
        public int mo9000b(com.fossil.blesdk.obfuscated.C1365a aVar, java.lang.String str, android.os.Bundle bundle) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14987a(new com.fossil.blesdk.obfuscated.C2859s3(aVar), str, bundle);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8999a(com.fossil.blesdk.obfuscated.C1365a aVar, android.net.Uri uri, android.os.Bundle bundle, java.util.List<android.os.Bundle> list) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14993a(new com.fossil.blesdk.obfuscated.C2859s3(aVar), uri, bundle, list);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8998a(com.fossil.blesdk.obfuscated.C1365a aVar, android.net.Uri uri) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14992a(new com.fossil.blesdk.obfuscated.C2859s3(aVar), uri);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8997a(com.fossil.blesdk.obfuscated.C1365a aVar, int i, android.net.Uri uri, android.os.Bundle bundle) {
            return com.fossil.blesdk.obfuscated.C2700q3.this.mo14991a(new com.fossil.blesdk.obfuscated.C2859s3(aVar), i, uri, bundle);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int mo14987a(com.fossil.blesdk.obfuscated.C2859s3 s3Var, java.lang.String str, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: a */
    public abstract android.os.Bundle mo14988a(java.lang.String str, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14989a(long j);

    @DexIgnore
    /* renamed from: a */
    public boolean mo14990a(com.fossil.blesdk.obfuscated.C2859s3 s3Var) {
        try {
            synchronized (this.f8516e) {
                android.os.IBinder a = s3Var.mo15890a();
                a.unlinkToDeath(this.f8516e.get(a), 0);
                this.f8516e.remove(a);
            }
            return true;
        } catch (java.util.NoSuchElementException unused) {
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14991a(com.fossil.blesdk.obfuscated.C2859s3 s3Var, int i, android.net.Uri uri, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14992a(com.fossil.blesdk.obfuscated.C2859s3 s3Var, android.net.Uri uri);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14993a(com.fossil.blesdk.obfuscated.C2859s3 s3Var, android.net.Uri uri, android.os.Bundle bundle, java.util.List<android.os.Bundle> list);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14994a(com.fossil.blesdk.obfuscated.C2859s3 s3Var, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: b */
    public abstract boolean mo14995b(com.fossil.blesdk.obfuscated.C2859s3 s3Var);

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        return this.f8517f;
    }
}
