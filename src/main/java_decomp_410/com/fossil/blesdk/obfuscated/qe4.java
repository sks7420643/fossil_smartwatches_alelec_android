package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qe4<T> implements re4<T> {
    @DexIgnore
    public /* final */ wc4<T> a;
    @DexIgnore
    public /* final */ xc4<T, T> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, rd4 {
        @DexIgnore
        public T e;
        @DexIgnore
        public int f; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ qe4 g;

        @DexIgnore
        public a(qe4 qe4) {
            this.g = qe4;
        }

        @DexIgnore
        public final void a() {
            T t;
            if (this.f == -2) {
                t = this.g.a.invoke();
            } else {
                xc4 b = this.g.b;
                T t2 = this.e;
                if (t2 != null) {
                    t = b.invoke(t2);
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.e = t;
            this.f = this.e == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.f < 0) {
                a();
            }
            return this.f == 1;
        }

        @DexIgnore
        public T next() {
            if (this.f < 0) {
                a();
            }
            if (this.f != 0) {
                T t = this.e;
                if (t != null) {
                    this.f = -1;
                    return t;
                }
                throw new TypeCastException("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public qe4(wc4<? extends T> wc4, xc4<? super T, ? extends T> xc4) {
        kd4.b(wc4, "getInitialValue");
        kd4.b(xc4, "getNextValue");
        this.a = wc4;
        this.b = xc4;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
