package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.frontlight.FrontLightConfig;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a60 extends i60 {
    @DexIgnore
    public /* final */ boolean R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ a60(Peripheral peripheral, Phase.a aVar, boolean z, String str, int i, fd4 fd4) {
        this(peripheral, aVar, z, str);
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.ENABLE, Boolean.valueOf(this.R));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a60(Peripheral peripheral, Phase.a aVar, boolean z, String str) {
        super(peripheral, aVar, PhaseId.SET_FRONT_LIGHT_ENABLE, new FrontLightConfig(z).getFrontLightSettingsJSON$blesdk_productionRelease(), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 48, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(str, "phaseUuid");
        this.R = z;
    }
}
