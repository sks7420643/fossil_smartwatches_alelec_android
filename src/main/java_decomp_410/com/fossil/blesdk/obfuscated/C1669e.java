package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e */
public class C1669e {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.e$a */
    public interface C1670a {
        @DexIgnore
        /* renamed from: d */
        void mo20d();

        @DexIgnore
        /* renamed from: e */
        void mo21e();

        @DexIgnore
        /* renamed from: f */
        void mo22f();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e$b")
    /* renamed from: com.fossil.blesdk.obfuscated.e$b */
    public static class C1671b<T extends com.fossil.blesdk.obfuscated.C1669e.C1670a> extends android.media.browse.MediaBrowser.ConnectionCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ T f4549a;

        @DexIgnore
        public C1671b(T t) {
            this.f4549a = t;
        }

        @DexIgnore
        public void onConnected() {
            this.f4549a.mo21e();
        }

        @DexIgnore
        public void onConnectionFailed() {
            this.f4549a.mo22f();
        }

        @DexIgnore
        public void onConnectionSuspended() {
            this.f4549a.mo20d();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e$c")
    /* renamed from: com.fossil.blesdk.obfuscated.e$c */
    public static class C1672c {
        @DexIgnore
        /* renamed from: a */
        public static java.lang.Object m6119a(java.lang.Object obj) {
            return ((android.media.browse.MediaBrowser.MediaItem) obj).getDescription();
        }

        @DexIgnore
        /* renamed from: b */
        public static int m6120b(java.lang.Object obj) {
            return ((android.media.browse.MediaBrowser.MediaItem) obj).getFlags();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.e$d */
    public interface C1673d {
        @DexIgnore
        /* renamed from: a */
        void mo62a(java.lang.String str);

        @DexIgnore
        /* renamed from: a */
        void mo63a(java.lang.String str, java.util.List<?> list);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e$e")
    /* renamed from: com.fossil.blesdk.obfuscated.e$e */
    public static class C1674e<T extends com.fossil.blesdk.obfuscated.C1669e.C1673d> extends android.media.browse.MediaBrowser.SubscriptionCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ T f4550a;

        @DexIgnore
        public C1674e(T t) {
            this.f4550a = t;
        }

        @DexIgnore
        public void onChildrenLoaded(java.lang.String str, java.util.List<android.media.browse.MediaBrowser.MediaItem> list) {
            this.f4550a.mo63a(str, list);
        }

        @DexIgnore
        public void onError(java.lang.String str) {
            this.f4550a.mo62a(str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m6109a(com.fossil.blesdk.obfuscated.C1669e.C1670a aVar) {
        return new com.fossil.blesdk.obfuscated.C1669e.C1671b(aVar);
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6112b(java.lang.Object obj) {
        ((android.media.browse.MediaBrowser) obj).disconnect();
    }

    @DexIgnore
    /* renamed from: c */
    public static android.os.Bundle m6113c(java.lang.Object obj) {
        return ((android.media.browse.MediaBrowser) obj).getExtras();
    }

    @DexIgnore
    /* renamed from: d */
    public static java.lang.Object m6114d(java.lang.Object obj) {
        return ((android.media.browse.MediaBrowser) obj).getSessionToken();
    }

    @DexIgnore
    /* renamed from: e */
    public static boolean m6115e(java.lang.Object obj) {
        return ((android.media.browse.MediaBrowser) obj).isConnected();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m6108a(android.content.Context context, android.content.ComponentName componentName, java.lang.Object obj, android.os.Bundle bundle) {
        return new android.media.browse.MediaBrowser(context, componentName, (android.media.browse.MediaBrowser.ConnectionCallback) obj, bundle);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6111a(java.lang.Object obj) {
        ((android.media.browse.MediaBrowser) obj).connect();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m6110a(com.fossil.blesdk.obfuscated.C1669e.C1673d dVar) {
        return new com.fossil.blesdk.obfuscated.C1669e.C1674e(dVar);
    }
}
