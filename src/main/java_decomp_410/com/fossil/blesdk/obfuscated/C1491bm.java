package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bm */
public final class C1491bm {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C1491bm f3776b; // = new com.fossil.blesdk.obfuscated.C1491bm();

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int f3777c; // = java.lang.Runtime.getRuntime().availableProcessors();

    @DexIgnore
    /* renamed from: d */
    public static /* final */ int f3778d;

    @DexIgnore
    /* renamed from: e */
    public static /* final */ int f3779e;

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.Executor f3780a; // = new com.fossil.blesdk.obfuscated.C1491bm.C1493b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bm$b")
    /* renamed from: com.fossil.blesdk.obfuscated.bm$b */
    public static class C1493b implements java.util.concurrent.Executor {
        @DexIgnore
        public C1493b() {
        }

        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            new android.os.Handler(android.os.Looper.getMainLooper()).post(runnable);
        }
    }

    /*
    static {
        int i = f3777c;
        f3778d = i + 1;
        f3779e = (i * 2) + 1;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static java.util.concurrent.ExecutorService m5037a() {
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(f3778d, f3779e, 1, java.util.concurrent.TimeUnit.SECONDS, new java.util.concurrent.LinkedBlockingQueue());
        m5038a(threadPoolExecutor, true);
        return threadPoolExecutor;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.util.concurrent.Executor m5039b() {
        return f3776b.f3780a;
    }

    @DexIgnore
    @android.annotation.SuppressLint({"NewApi"})
    /* renamed from: a */
    public static void m5038a(java.util.concurrent.ThreadPoolExecutor threadPoolExecutor, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 9) {
            threadPoolExecutor.allowCoreThreadTimeOut(z);
        }
    }
}
