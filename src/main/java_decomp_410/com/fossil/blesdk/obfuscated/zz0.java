package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zz0 extends k21<a11> {
    @DexIgnore
    public static /* final */ de0.g<zz0> E; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0<Object> F; // = new de0<>("Fitness.RECORDING_API", new b01(), E);

    /*
    static {
        new de0("Fitness.RECORDING_CLIENT", new c01(), E);
    }
    */

    @DexIgnore
    public zz0(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 56, bVar, cVar, kj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitRecordingApi");
        if (queryLocalInterface instanceof a11) {
            return (a11) queryLocalInterface;
        }
        return new b11(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitRecordingApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.RecordingApi";
    }
}
