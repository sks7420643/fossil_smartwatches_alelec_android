package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.SystemClock;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o0 extends Drawable implements Drawable.Callback {
    @DexIgnore
    public c e;
    @DexIgnore
    public Rect f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public Drawable h;
    @DexIgnore
    public int i; // = 255;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public Runnable m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public b p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            o0.this.a(true);
            o0.this.invalidateSelf();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Drawable.Callback {
        @DexIgnore
        public Drawable.Callback e;

        @DexIgnore
        public b a(Drawable.Callback callback) {
            this.e = callback;
            return this;
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            Drawable.Callback callback = this.e;
            if (callback != null) {
                callback.scheduleDrawable(drawable, runnable, j);
            }
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            Drawable.Callback callback = this.e;
            if (callback != null) {
                callback.unscheduleDrawable(drawable, runnable);
            }
        }

        @DexIgnore
        public Drawable.Callback a() {
            Drawable.Callback callback = this.e;
            this.e = null;
            return callback;
        }
    }

    @DexIgnore
    public c a() {
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0073  */
    public boolean a(int i2) {
        Runnable runnable;
        if (i2 == this.k) {
            return false;
        }
        long uptimeMillis = SystemClock.uptimeMillis();
        if (this.e.B > 0) {
            Drawable drawable = this.h;
            if (drawable != null) {
                drawable.setVisible(false, false);
            }
            Drawable drawable2 = this.g;
            if (drawable2 != null) {
                this.h = drawable2;
                this.o = ((long) this.e.B) + uptimeMillis;
            } else {
                this.h = null;
                this.o = 0;
            }
        } else {
            Drawable drawable3 = this.g;
            if (drawable3 != null) {
                drawable3.setVisible(false, false);
            }
        }
        if (i2 >= 0) {
            c cVar = this.e;
            if (i2 < cVar.h) {
                Drawable a2 = cVar.a(i2);
                this.g = a2;
                this.k = i2;
                if (a2 != null) {
                    int i3 = this.e.A;
                    if (i3 > 0) {
                        this.n = uptimeMillis + ((long) i3);
                    }
                    a(a2);
                }
                if (!(this.n == 0 && this.o == 0)) {
                    runnable = this.m;
                    if (runnable != null) {
                        this.m = new a();
                    } else {
                        unscheduleSelf(runnable);
                    }
                    a(true);
                }
                invalidateSelf();
                return true;
            }
        }
        this.g = null;
        this.k = -1;
        runnable = this.m;
        if (runnable != null) {
        }
        a(true);
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void applyTheme(Resources.Theme theme) {
        this.e.a(theme);
    }

    @DexIgnore
    public int b() {
        return this.k;
    }

    @DexIgnore
    @SuppressLint({"WrongConstant"})
    @TargetApi(23)
    public final boolean c() {
        return isAutoMirrored() && getLayoutDirection() == 1;
    }

    @DexIgnore
    public boolean canApplyTheme() {
        return this.e.canApplyTheme();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.g;
        if (drawable != null) {
            drawable.draw(canvas);
        }
        Drawable drawable2 = this.h;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
    }

    @DexIgnore
    public int getAlpha() {
        return this.i;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.e.getChangingConfigurations();
    }

    @DexIgnore
    public final Drawable.ConstantState getConstantState() {
        if (!this.e.a()) {
            return null;
        }
        this.e.d = getChangingConfigurations();
        return this.e;
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.g;
    }

    @DexIgnore
    public void getHotspotBounds(Rect rect) {
        Rect rect2 = this.f;
        if (rect2 != null) {
            rect.set(rect2);
        } else {
            super.getHotspotBounds(rect);
        }
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        if (this.e.m()) {
            return this.e.f();
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return -1;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        if (this.e.m()) {
            return this.e.j();
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return -1;
    }

    @DexIgnore
    public int getMinimumHeight() {
        if (this.e.m()) {
            return this.e.g();
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return 0;
    }

    @DexIgnore
    public int getMinimumWidth() {
        if (this.e.m()) {
            return this.e.h();
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return 0;
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.g;
        if (drawable == null || !drawable.isVisible()) {
            return -2;
        }
        return this.e.k();
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        Drawable drawable = this.g;
        if (drawable != null) {
            drawable.getOutline(outline);
        }
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        boolean z;
        Rect i2 = this.e.i();
        if (i2 != null) {
            rect.set(i2);
            z = (i2.right | ((i2.left | i2.top) | i2.bottom)) != 0;
        } else {
            Drawable drawable = this.g;
            if (drawable != null) {
                z = drawable.getPadding(rect);
            } else {
                z = super.getPadding(rect);
            }
        }
        if (c()) {
            int i3 = rect.left;
            rect.left = rect.right;
            rect.right = i3;
        }
        return z;
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        c cVar = this.e;
        if (cVar != null) {
            cVar.l();
        }
        if (drawable == this.g && getCallback() != null) {
            getCallback().invalidateDrawable(this);
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.e.C;
    }

    @DexIgnore
    public void jumpToCurrentState() {
        boolean z;
        Drawable drawable = this.h;
        if (drawable != null) {
            drawable.jumpToCurrentState();
            this.h = null;
            z = true;
        } else {
            z = false;
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
            if (this.j) {
                this.g.setAlpha(this.i);
            }
        }
        if (this.o != 0) {
            this.o = 0;
            z = true;
        }
        if (this.n != 0) {
            this.n = 0;
            z = true;
        }
        if (z) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.l && super.mutate() == this) {
            c a2 = a();
            a2.n();
            a(a2);
            this.l = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.h;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLayoutDirectionChanged(int i2) {
        return this.e.b(i2, b());
    }

    @DexIgnore
    public boolean onLevelChange(int i2) {
        Drawable drawable = this.h;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            return drawable2.setLevel(i2);
        }
        return false;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.h;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            return drawable2.setState(iArr);
        }
        return false;
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        if (drawable == this.g && getCallback() != null) {
            getCallback().scheduleDrawable(this, runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        if (!this.j || this.i != i2) {
            this.j = true;
            this.i = i2;
            Drawable drawable = this.g;
            if (drawable == null) {
                return;
            }
            if (this.n == 0) {
                drawable.setAlpha(i2);
            } else {
                a(false);
            }
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        c cVar = this.e;
        if (cVar.C != z) {
            cVar.C = z;
            Drawable drawable = this.g;
            if (drawable != null) {
                c7.a(drawable, cVar.C);
            }
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        c cVar = this.e;
        cVar.E = true;
        if (cVar.D != colorFilter) {
            cVar.D = colorFilter;
            Drawable drawable = this.g;
            if (drawable != null) {
                drawable.setColorFilter(colorFilter);
            }
        }
    }

    @DexIgnore
    public void setDither(boolean z) {
        c cVar = this.e;
        if (cVar.x != z) {
            cVar.x = z;
            Drawable drawable = this.g;
            if (drawable != null) {
                drawable.setDither(cVar.x);
            }
        }
    }

    @DexIgnore
    public void setHotspot(float f2, float f3) {
        Drawable drawable = this.g;
        if (drawable != null) {
            c7.a(drawable, f2, f3);
        }
    }

    @DexIgnore
    public void setHotspotBounds(int i2, int i3, int i4, int i5) {
        Rect rect = this.f;
        if (rect == null) {
            this.f = new Rect(i2, i3, i4, i5);
        } else {
            rect.set(i2, i3, i4, i5);
        }
        Drawable drawable = this.g;
        if (drawable != null) {
            c7.a(drawable, i2, i3, i4, i5);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        c cVar = this.e;
        cVar.H = true;
        if (cVar.F != colorStateList) {
            cVar.F = colorStateList;
            c7.a(this.g, colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        c cVar = this.e;
        cVar.I = true;
        if (cVar.G != mode) {
            cVar.G = mode;
            c7.a(this.g, mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        Drawable drawable = this.h;
        if (drawable != null) {
            drawable.setVisible(z, z2);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.setVisible(z, z2);
        }
        return visible;
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        if (drawable == this.g && getCallback() != null) {
            getCallback().unscheduleDrawable(this, runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c extends Drawable.ConstantState {
        @DexIgnore
        public int A;
        @DexIgnore
        public int B;
        @DexIgnore
        public boolean C;
        @DexIgnore
        public ColorFilter D;
        @DexIgnore
        public boolean E;
        @DexIgnore
        public ColorStateList F;
        @DexIgnore
        public PorterDuff.Mode G;
        @DexIgnore
        public boolean H;
        @DexIgnore
        public boolean I;
        @DexIgnore
        public /* final */ o0 a;
        @DexIgnore
        public Resources b;
        @DexIgnore
        public int c; // = 160;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public SparseArray<Drawable.ConstantState> f;
        @DexIgnore
        public Drawable[] g;
        @DexIgnore
        public int h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public Rect k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public int n;
        @DexIgnore
        public int o;
        @DexIgnore
        public int p;
        @DexIgnore
        public int q;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public int s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public boolean u;
        @DexIgnore
        public boolean v;
        @DexIgnore
        public boolean w;
        @DexIgnore
        public boolean x;
        @DexIgnore
        public boolean y;
        @DexIgnore
        public int z;

        @DexIgnore
        public c(c cVar, o0 o0Var, Resources resources) {
            Resources resources2;
            this.i = false;
            this.l = false;
            this.x = true;
            this.A = 0;
            this.B = 0;
            this.a = o0Var;
            if (resources != null) {
                resources2 = resources;
            } else {
                resources2 = cVar != null ? cVar.b : null;
            }
            this.b = resources2;
            this.c = o0.a(resources, cVar != null ? cVar.c : 0);
            if (cVar != null) {
                this.d = cVar.d;
                this.e = cVar.e;
                this.v = true;
                this.w = true;
                this.i = cVar.i;
                this.l = cVar.l;
                this.x = cVar.x;
                this.y = cVar.y;
                this.z = cVar.z;
                this.A = cVar.A;
                this.B = cVar.B;
                this.C = cVar.C;
                this.D = cVar.D;
                this.E = cVar.E;
                this.F = cVar.F;
                this.G = cVar.G;
                this.H = cVar.H;
                this.I = cVar.I;
                if (cVar.c == this.c) {
                    if (cVar.j) {
                        this.k = new Rect(cVar.k);
                        this.j = true;
                    }
                    if (cVar.m) {
                        this.n = cVar.n;
                        this.o = cVar.o;
                        this.p = cVar.p;
                        this.q = cVar.q;
                        this.m = true;
                    }
                }
                if (cVar.r) {
                    this.s = cVar.s;
                    this.r = true;
                }
                if (cVar.t) {
                    this.u = cVar.u;
                    this.t = true;
                }
                Drawable[] drawableArr = cVar.g;
                this.g = new Drawable[drawableArr.length];
                this.h = cVar.h;
                SparseArray<Drawable.ConstantState> sparseArray = cVar.f;
                if (sparseArray != null) {
                    this.f = sparseArray.clone();
                } else {
                    this.f = new SparseArray<>(this.h);
                }
                int i2 = this.h;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null) {
                        Drawable.ConstantState constantState = drawableArr[i3].getConstantState();
                        if (constantState != null) {
                            this.f.put(i3, constantState);
                        } else {
                            this.g[i3] = drawableArr[i3];
                        }
                    }
                }
                return;
            }
            this.g = new Drawable[10];
            this.h = 0;
        }

        @DexIgnore
        public final int a(Drawable drawable) {
            int i2 = this.h;
            if (i2 >= this.g.length) {
                a(i2, i2 + 10);
            }
            drawable.mutate();
            drawable.setVisible(false, true);
            drawable.setCallback(this.a);
            this.g[i2] = drawable;
            this.h++;
            this.e = drawable.getChangingConfigurations() | this.e;
            l();
            this.k = null;
            this.j = false;
            this.m = false;
            this.v = false;
            return i2;
        }

        @DexIgnore
        public final Drawable b(Drawable drawable) {
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(this.z);
            }
            Drawable mutate = drawable.mutate();
            mutate.setCallback(this.a);
            return mutate;
        }

        @DexIgnore
        public final void c() {
            SparseArray<Drawable.ConstantState> sparseArray = this.f;
            if (sparseArray != null) {
                int size = sparseArray.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g[this.f.keyAt(i2)] = b(this.f.valueAt(i2).newDrawable(this.b));
                }
                this.f = null;
            }
        }

        @DexIgnore
        public boolean canApplyTheme() {
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                if (drawable == null) {
                    Drawable.ConstantState constantState = this.f.get(i3);
                    if (constantState != null && constantState.canApplyTheme()) {
                        return true;
                    }
                } else if (drawable.canApplyTheme()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public final int d() {
            return this.g.length;
        }

        @DexIgnore
        public final int e() {
            return this.h;
        }

        @DexIgnore
        public final int f() {
            if (!this.m) {
                b();
            }
            return this.o;
        }

        @DexIgnore
        public final int g() {
            if (!this.m) {
                b();
            }
            return this.q;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.d | this.e;
        }

        @DexIgnore
        public final int h() {
            if (!this.m) {
                b();
            }
            return this.p;
        }

        @DexIgnore
        public final Rect i() {
            if (this.i) {
                return null;
            }
            if (this.k != null || this.j) {
                return this.k;
            }
            c();
            Rect rect = new Rect();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            Rect rect2 = null;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getPadding(rect)) {
                    if (rect2 == null) {
                        rect2 = new Rect(0, 0, 0, 0);
                    }
                    int i4 = rect.left;
                    if (i4 > rect2.left) {
                        rect2.left = i4;
                    }
                    int i5 = rect.top;
                    if (i5 > rect2.top) {
                        rect2.top = i5;
                    }
                    int i6 = rect.right;
                    if (i6 > rect2.right) {
                        rect2.right = i6;
                    }
                    int i7 = rect.bottom;
                    if (i7 > rect2.bottom) {
                        rect2.bottom = i7;
                    }
                }
            }
            this.j = true;
            this.k = rect2;
            return rect2;
        }

        @DexIgnore
        public final int j() {
            if (!this.m) {
                b();
            }
            return this.n;
        }

        @DexIgnore
        public final int k() {
            if (this.r) {
                return this.s;
            }
            c();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            int opacity = i2 > 0 ? drawableArr[0].getOpacity() : -2;
            for (int i3 = 1; i3 < i2; i3++) {
                opacity = Drawable.resolveOpacity(opacity, drawableArr[i3].getOpacity());
            }
            this.s = opacity;
            this.r = true;
            return opacity;
        }

        @DexIgnore
        public void l() {
            this.r = false;
            this.t = false;
        }

        @DexIgnore
        public final boolean m() {
            return this.l;
        }

        @DexIgnore
        public abstract void n();

        @DexIgnore
        public final boolean b(int i2, int i3) {
            int i4 = this.h;
            Drawable[] drawableArr = this.g;
            boolean z2 = false;
            for (int i5 = 0; i5 < i4; i5++) {
                if (drawableArr[i5] != null) {
                    boolean layoutDirection = Build.VERSION.SDK_INT >= 23 ? drawableArr[i5].setLayoutDirection(i2) : false;
                    if (i5 == i3) {
                        z2 = layoutDirection;
                    }
                }
            }
            this.z = i2;
            return z2;
        }

        @DexIgnore
        public final void c(int i2) {
            this.B = i2;
        }

        @DexIgnore
        public final void b(boolean z2) {
            this.i = z2;
        }

        @DexIgnore
        public void b() {
            this.m = true;
            c();
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            this.o = -1;
            this.n = -1;
            this.q = 0;
            this.p = 0;
            for (int i3 = 0; i3 < i2; i3++) {
                Drawable drawable = drawableArr[i3];
                int intrinsicWidth = drawable.getIntrinsicWidth();
                if (intrinsicWidth > this.n) {
                    this.n = intrinsicWidth;
                }
                int intrinsicHeight = drawable.getIntrinsicHeight();
                if (intrinsicHeight > this.o) {
                    this.o = intrinsicHeight;
                }
                int minimumWidth = drawable.getMinimumWidth();
                if (minimumWidth > this.p) {
                    this.p = minimumWidth;
                }
                int minimumHeight = drawable.getMinimumHeight();
                if (minimumHeight > this.q) {
                    this.q = minimumHeight;
                }
            }
        }

        @DexIgnore
        public final Drawable a(int i2) {
            Drawable drawable = this.g[i2];
            if (drawable != null) {
                return drawable;
            }
            SparseArray<Drawable.ConstantState> sparseArray = this.f;
            if (sparseArray != null) {
                int indexOfKey = sparseArray.indexOfKey(i2);
                if (indexOfKey >= 0) {
                    Drawable b2 = b(this.f.valueAt(indexOfKey).newDrawable(this.b));
                    this.g[i2] = b2;
                    this.f.removeAt(indexOfKey);
                    if (this.f.size() == 0) {
                        this.f = null;
                    }
                    return b2;
                }
            }
            return null;
        }

        @DexIgnore
        public final void a(Resources resources) {
            if (resources != null) {
                this.b = resources;
                int a2 = o0.a(resources, this.c);
                int i2 = this.c;
                this.c = a2;
                if (i2 != a2) {
                    this.m = false;
                    this.j = false;
                }
            }
        }

        @DexIgnore
        public final void b(int i2) {
            this.A = i2;
        }

        @DexIgnore
        public final void a(Resources.Theme theme) {
            if (theme != null) {
                c();
                int i2 = this.h;
                Drawable[] drawableArr = this.g;
                for (int i3 = 0; i3 < i2; i3++) {
                    if (drawableArr[i3] != null && drawableArr[i3].canApplyTheme()) {
                        drawableArr[i3].applyTheme(theme);
                        this.e |= drawableArr[i3].getChangingConfigurations();
                    }
                }
                a(theme.getResources());
            }
        }

        @DexIgnore
        public final void a(boolean z2) {
            this.l = z2;
        }

        @DexIgnore
        public void a(int i2, int i3) {
            Drawable[] drawableArr = new Drawable[i3];
            System.arraycopy(this.g, 0, drawableArr, 0, i2);
            this.g = drawableArr;
        }

        @DexIgnore
        public synchronized boolean a() {
            if (this.v) {
                return this.w;
            }
            c();
            this.v = true;
            int i2 = this.h;
            Drawable[] drawableArr = this.g;
            for (int i3 = 0; i3 < i2; i3++) {
                if (drawableArr[i3].getConstantState() == null) {
                    this.w = false;
                    return false;
                }
            }
            this.w = true;
            return true;
        }
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        if (this.p == null) {
            this.p = new b();
        }
        b bVar = this.p;
        bVar.a(drawable.getCallback());
        drawable.setCallback(bVar);
        try {
            if (this.e.A <= 0 && this.j) {
                drawable.setAlpha(this.i);
            }
            if (this.e.E) {
                drawable.setColorFilter(this.e.D);
            } else {
                if (this.e.H) {
                    c7.a(drawable, this.e.F);
                }
                if (this.e.I) {
                    c7.a(drawable, this.e.G);
                }
            }
            drawable.setVisible(isVisible(), true);
            drawable.setDither(this.e.x);
            drawable.setState(getState());
            drawable.setLevel(getLevel());
            drawable.setBounds(getBounds());
            if (Build.VERSION.SDK_INT >= 23) {
                drawable.setLayoutDirection(getLayoutDirection());
            }
            if (Build.VERSION.SDK_INT >= 19) {
                drawable.setAutoMirrored(this.e.C);
            }
            Rect rect = this.f;
            if (Build.VERSION.SDK_INT >= 21 && rect != null) {
                drawable.setHotspotBounds(rect.left, rect.top, rect.right, rect.bottom);
            }
        } finally {
            drawable.setCallback(this.p.a());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public void a(boolean z) {
        boolean z2;
        Drawable drawable;
        boolean z3 = true;
        this.j = true;
        long uptimeMillis = SystemClock.uptimeMillis();
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            long j2 = this.n;
            if (j2 != 0) {
                if (j2 <= uptimeMillis) {
                    drawable2.setAlpha(this.i);
                    this.n = 0;
                } else {
                    drawable2.setAlpha(((255 - (((int) ((j2 - uptimeMillis) * 255)) / this.e.A)) * this.i) / 255);
                    z2 = true;
                    drawable = this.h;
                    if (drawable == null) {
                        long j3 = this.o;
                        if (j3 != 0) {
                            if (j3 <= uptimeMillis) {
                                drawable.setVisible(false, false);
                                this.h = null;
                                this.o = 0;
                            } else {
                                drawable.setAlpha(((((int) ((j3 - uptimeMillis) * 255)) / this.e.B) * this.i) / 255);
                                if (!z && z3) {
                                    scheduleSelf(this.m, uptimeMillis + 16);
                                    return;
                                }
                                return;
                            }
                        }
                    } else {
                        this.o = 0;
                    }
                    z3 = z2;
                    if (!z) {
                        return;
                    }
                    return;
                }
            }
        } else {
            this.n = 0;
        }
        z2 = false;
        drawable = this.h;
        if (drawable == null) {
        }
        z3 = z2;
        if (!z) {
        }
    }

    @DexIgnore
    public final void a(Resources resources) {
        this.e.a(resources);
    }

    @DexIgnore
    public void a(c cVar) {
        this.e = cVar;
        int i2 = this.k;
        if (i2 >= 0) {
            this.g = cVar.a(i2);
            Drawable drawable = this.g;
            if (drawable != null) {
                a(drawable);
            }
        }
        this.h = null;
    }

    @DexIgnore
    public static int a(Resources resources, int i2) {
        if (resources != null) {
            i2 = resources.getDisplayMetrics().densityDpi;
        }
        if (i2 == 0) {
            return 160;
        }
        return i2;
    }
}
