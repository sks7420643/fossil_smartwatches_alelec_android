package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ie3 implements MembersInjector<ActivityDetailActivity> {
    @DexIgnore
    public static void a(ActivityDetailActivity activityDetailActivity, ActivityDetailPresenter activityDetailPresenter) {
        activityDetailActivity.B = activityDetailPresenter;
    }
}
