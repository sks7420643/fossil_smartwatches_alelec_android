package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class u4 {
    @DexIgnore
    public static void a(y4 y4Var) {
        if ((y4Var.N() & 32) != 32) {
            b(y4Var);
            return;
        }
        y4Var.D0 = true;
        y4Var.x0 = false;
        y4Var.y0 = false;
        y4Var.z0 = false;
        ArrayList<ConstraintWidget> arrayList = y4Var.k0;
        List<z4> list = y4Var.w0;
        boolean z = y4Var.k() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z2 = y4Var.r() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (ConstraintWidget next : arrayList) {
            next.p = null;
            next.d0 = false;
            next.G();
        }
        for (ConstraintWidget next2 : arrayList) {
            if (next2.p == null && !a(next2, list, z3)) {
                b(y4Var);
                y4Var.D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (z4 next3 : list) {
            i = Math.max(i, a(next3, 0));
            i2 = Math.max(i2, a(next3, 1));
        }
        if (z) {
            y4Var.a(ConstraintWidget.DimensionBehaviour.FIXED);
            y4Var.p(i);
            y4Var.x0 = true;
            y4Var.y0 = true;
            y4Var.A0 = i;
        }
        if (z2) {
            y4Var.b(ConstraintWidget.DimensionBehaviour.FIXED);
            y4Var.h(i2);
            y4Var.x0 = true;
            y4Var.z0 = true;
            y4Var.B0 = i2;
        }
        a(list, 0, y4Var.t());
        a(list, 1, y4Var.j());
    }

    @DexIgnore
    public static void b(y4 y4Var) {
        y4Var.w0.clear();
        y4Var.w0.add(0, new z4(y4Var.k0));
    }

    @DexIgnore
    public static boolean a(ConstraintWidget constraintWidget, List<z4> list, boolean z) {
        z4 z4Var = new z4(new ArrayList(), true);
        list.add(z4Var);
        return a(constraintWidget, z4Var, list, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0181, code lost:
        if (r3.b == r4) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0128, code lost:
        if (r3.b == r4) goto L_0x012a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01c2  */
    public static boolean a(ConstraintWidget constraintWidget, z4 z4Var, List<z4> list, boolean z) {
        ConstraintAnchor constraintAnchor;
        if (constraintWidget == null) {
            return true;
        }
        constraintWidget.c0 = false;
        y4 y4Var = (y4) constraintWidget.l();
        z4 z4Var2 = constraintWidget.p;
        if (z4Var2 == null) {
            constraintWidget.b0 = true;
            z4Var.a.add(constraintWidget);
            constraintWidget.p = z4Var;
            if (constraintWidget.s.d == null && constraintWidget.u.d == null && constraintWidget.t.d == null && constraintWidget.v.d == null && constraintWidget.w.d == null && constraintWidget.z.d == null) {
                a(y4Var, constraintWidget, z4Var);
                if (z) {
                    return false;
                }
            }
            if (!(constraintWidget.t.d == null || constraintWidget.v.d == null)) {
                ConstraintWidget.DimensionBehaviour r = y4Var.r();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    a(y4Var, constraintWidget, z4Var);
                    return false;
                } else if (!(constraintWidget.t.d.b == constraintWidget.l() && constraintWidget.v.d.b == constraintWidget.l())) {
                    a(y4Var, constraintWidget, z4Var);
                }
            }
            if (!(constraintWidget.s.d == null || constraintWidget.u.d == null)) {
                ConstraintWidget.DimensionBehaviour k = y4Var.k();
                ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    a(y4Var, constraintWidget, z4Var);
                    return false;
                } else if (!(constraintWidget.s.d.b == constraintWidget.l() && constraintWidget.u.d.b == constraintWidget.l())) {
                    a(y4Var, constraintWidget, z4Var);
                }
            }
            if (((constraintWidget.k() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) ^ (constraintWidget.r() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT)) && constraintWidget.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                a(constraintWidget);
            } else if (constraintWidget.k() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.r() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                a(y4Var, constraintWidget, z4Var);
                if (z) {
                    return false;
                }
            }
            if (!(constraintWidget.s.d == null && constraintWidget.u.d == null)) {
                ConstraintAnchor constraintAnchor2 = constraintWidget.s.d;
                if (!(constraintAnchor2 != null && constraintAnchor2.b == constraintWidget.D && constraintWidget.u.d == null)) {
                    ConstraintAnchor constraintAnchor3 = constraintWidget.u.d;
                    if (!(constraintAnchor3 != null && constraintAnchor3.b == constraintWidget.D && constraintWidget.s.d == null)) {
                        ConstraintAnchor constraintAnchor4 = constraintWidget.s.d;
                        if (constraintAnchor4 != null) {
                            ConstraintWidget constraintWidget2 = constraintAnchor4.b;
                            ConstraintWidget constraintWidget3 = constraintWidget.D;
                            if (constraintWidget2 == constraintWidget3) {
                                ConstraintAnchor constraintAnchor5 = constraintWidget.u.d;
                                if (constraintAnchor5 != null) {
                                }
                            }
                        }
                        if (!(constraintWidget.t.d == null && constraintWidget.v.d == null)) {
                            ConstraintAnchor constraintAnchor6 = constraintWidget.t.d;
                            if (!(constraintAnchor6 != null && constraintAnchor6.b == constraintWidget.D && constraintWidget.v.d == null)) {
                                ConstraintAnchor constraintAnchor7 = constraintWidget.v.d;
                                if (!(constraintAnchor7 != null && constraintAnchor7.b == constraintWidget.D && constraintWidget.t.d == null)) {
                                    constraintAnchor = constraintWidget.t.d;
                                    if (constraintAnchor != null) {
                                        ConstraintWidget constraintWidget4 = constraintAnchor.b;
                                        ConstraintWidget constraintWidget5 = constraintWidget.D;
                                        if (constraintWidget4 == constraintWidget5) {
                                            ConstraintAnchor constraintAnchor8 = constraintWidget.v.d;
                                            if (constraintAnchor8 != null) {
                                            }
                                        }
                                    }
                                    if (constraintWidget instanceof b5) {
                                        a(y4Var, constraintWidget, z4Var);
                                        if (z) {
                                            return false;
                                        }
                                        b5 b5Var = (b5) constraintWidget;
                                        for (int i = 0; i < b5Var.l0; i++) {
                                            if (!a(b5Var.k0[i], z4Var, list, z)) {
                                                return false;
                                            }
                                        }
                                    }
                                    for (ConstraintAnchor constraintAnchor9 : constraintWidget.A) {
                                        ConstraintAnchor constraintAnchor10 = constraintAnchor9.d;
                                        if (!(constraintAnchor10 == null || constraintAnchor10.b == constraintWidget.l())) {
                                            if (constraintAnchor9.c == ConstraintAnchor.Type.CENTER) {
                                                a(y4Var, constraintWidget, z4Var);
                                                if (z) {
                                                    return false;
                                                }
                                            } else {
                                                a(constraintAnchor9);
                                            }
                                            if (!a(constraintAnchor9.d.b, z4Var, list, z)) {
                                                return false;
                                            }
                                        }
                                    }
                                    return true;
                                }
                            }
                        }
                        if (constraintWidget.z.d == null && constraintWidget.w.d == null && !(constraintWidget instanceof a5) && !(constraintWidget instanceof b5)) {
                            z4Var.g.add(constraintWidget);
                        }
                        if (constraintWidget instanceof b5) {
                        }
                        while (r4 < r3) {
                        }
                        return true;
                    }
                }
            }
            if (constraintWidget.z.d == null && !(constraintWidget instanceof a5) && !(constraintWidget instanceof b5)) {
                z4Var.f.add(constraintWidget);
            }
            ConstraintAnchor constraintAnchor62 = constraintWidget.t.d;
            ConstraintAnchor constraintAnchor72 = constraintWidget.v.d;
            constraintAnchor = constraintWidget.t.d;
            if (constraintAnchor != null) {
            }
            if (constraintWidget instanceof b5) {
            }
            while (r4 < r3) {
            }
            return true;
        }
        if (z4Var2 != z4Var) {
            z4Var.a.addAll(z4Var2.a);
            z4Var.f.addAll(constraintWidget.p.f);
            z4Var.g.addAll(constraintWidget.p.g);
            if (!constraintWidget.p.d) {
                z4Var.d = false;
            }
            list.remove(constraintWidget.p);
            for (ConstraintWidget constraintWidget6 : constraintWidget.p.a) {
                constraintWidget6.p = z4Var;
            }
        }
        return true;
    }

    @DexIgnore
    public static void a(y4 y4Var, ConstraintWidget constraintWidget, z4 z4Var) {
        z4Var.d = false;
        y4Var.D0 = false;
        constraintWidget.b0 = false;
    }

    @DexIgnore
    public static int a(z4 z4Var, int i) {
        int i2 = i * 2;
        List<ConstraintWidget> a = z4Var.a(i);
        int size = a.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            ConstraintWidget constraintWidget = a.get(i4);
            ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
            int i5 = i2 + 1;
            i3 = Math.max(i3, a(constraintWidget, i, constraintAnchorArr[i5].d == null || !(constraintAnchorArr[i2].d == null || constraintAnchorArr[i5].d == null), 0));
        }
        z4Var.e[i] = i3;
        return i3;
    }

    @DexIgnore
    public static int a(ConstraintWidget constraintWidget, int i, boolean z, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int t;
        int i9;
        int i10;
        int i11;
        ConstraintWidget constraintWidget2 = constraintWidget;
        int i12 = i;
        boolean z2 = z;
        int i13 = 0;
        if (!constraintWidget2.b0) {
            return 0;
        }
        boolean z3 = constraintWidget2.w.d != null && i12 == 1;
        if (z2) {
            i6 = constraintWidget.d();
            i5 = constraintWidget.j() - constraintWidget.d();
            i4 = i12 * 2;
            i3 = i4 + 1;
        } else {
            i6 = constraintWidget.j() - constraintWidget.d();
            i5 = constraintWidget.d();
            i3 = i12 * 2;
            i4 = i3 + 1;
        }
        ConstraintAnchor[] constraintAnchorArr = constraintWidget2.A;
        if (constraintAnchorArr[i3].d == null || constraintAnchorArr[i4].d != null) {
            i7 = i3;
            i8 = 1;
        } else {
            i7 = i4;
            i4 = i3;
            i8 = -1;
        }
        int i14 = z3 ? i2 - i6 : i2;
        int b = (constraintWidget2.A[i4].b() * i8) + a(constraintWidget, i);
        int i15 = i14 + b;
        int t2 = (i12 == 0 ? constraintWidget.t() : constraintWidget.j()) * i8;
        Iterator<g5> it = constraintWidget2.A[i4].d().a.iterator();
        while (it.hasNext()) {
            i13 = Math.max(i13, a(((e5) it.next()).c.b, i12, z2, i15));
        }
        int i16 = 0;
        for (Iterator<g5> it2 = constraintWidget2.A[i7].d().a.iterator(); it2.hasNext(); it2 = it2) {
            i16 = Math.max(i16, a(((e5) it2.next()).c.b, i12, z2, t2 + i15));
        }
        if (z3) {
            i13 -= i6;
            t = i16 + i5;
        } else {
            t = i16 + ((i12 == 0 ? constraintWidget.t() : constraintWidget.j()) * i8);
        }
        int i17 = 1;
        if (i12 == 1) {
            Iterator<g5> it3 = constraintWidget2.w.d().a.iterator();
            int i18 = 0;
            while (it3.hasNext()) {
                Iterator<g5> it4 = it3;
                e5 e5Var = (e5) it3.next();
                if (i8 == i17) {
                    i18 = Math.max(i18, a(e5Var.c.b, i12, z2, i6 + i15));
                    i11 = i7;
                } else {
                    i11 = i7;
                    i18 = Math.max(i18, a(e5Var.c.b, i12, z2, (i5 * i8) + i15));
                }
                it3 = it4;
                i7 = i11;
                i17 = 1;
            }
            i9 = i7;
            int i19 = i18;
            i10 = (constraintWidget2.w.d().a.size() <= 0 || z3) ? i19 : i8 == 1 ? i19 + i6 : i19 - i5;
        } else {
            i9 = i7;
            i10 = 0;
        }
        int max = b + Math.max(i13, Math.max(t, i10));
        int i20 = i15 + t2;
        if (i8 != -1) {
            int i21 = i15;
            i15 = i20;
            i20 = i21;
        }
        if (z2) {
            c5.a(constraintWidget2, i12, i20);
            constraintWidget2.a(i20, i15, i12);
        } else {
            constraintWidget2.p.a(constraintWidget2, i12);
            constraintWidget2.d(i20, i12);
        }
        if (constraintWidget.c(i) == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget2.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            constraintWidget2.p.a(constraintWidget2, i12);
        }
        ConstraintAnchor[] constraintAnchorArr2 = constraintWidget2.A;
        if (!(constraintAnchorArr2[i4].d == null || constraintAnchorArr2[i9].d == null)) {
            ConstraintWidget l = constraintWidget.l();
            ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.A;
            if (constraintAnchorArr3[i4].d.b == l && constraintAnchorArr3[i9].d.b == l) {
                constraintWidget2.p.a(constraintWidget2, i12);
            }
        }
        return max;
    }

    @DexIgnore
    public static void a(ConstraintAnchor constraintAnchor) {
        e5 d = constraintAnchor.d();
        ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
        if (constraintAnchor2 != null && constraintAnchor2.d != constraintAnchor) {
            constraintAnchor2.d().a(d);
        }
    }

    @DexIgnore
    public static void a(List<z4> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (ConstraintWidget next : list.get(i3).b(i)) {
                if (next.b0) {
                    a(next, i, i2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
        ConstraintAnchor constraintAnchor = constraintAnchorArr[i3];
        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i3 + 1];
        if ((constraintAnchor.d == null || constraintAnchor2.d == null) ? false : true) {
            c5.a(constraintWidget, i, a(constraintWidget, i) + constraintAnchor.b());
        } else if (constraintWidget.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || constraintWidget.c(i) != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int e = i2 - constraintWidget.e(i);
            int d = e - constraintWidget.d(i);
            constraintWidget.a(d, e, i);
            c5.a(constraintWidget, i, d);
        } else {
            int a = a(constraintWidget);
            int i4 = (int) constraintWidget.A[i3].d().g;
            constraintAnchor2.d().f = constraintAnchor.d();
            constraintAnchor2.d().g = (float) a;
            constraintAnchor2.d().b = 1;
            constraintWidget.a(i4, i4 + a, i);
        }
    }

    @DexIgnore
    public static int a(ConstraintWidget constraintWidget, int i) {
        int i2 = i * 2;
        ConstraintAnchor[] constraintAnchorArr = constraintWidget.A;
        ConstraintAnchor constraintAnchor = constraintAnchorArr[i2];
        ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i2 + 1];
        ConstraintAnchor constraintAnchor3 = constraintAnchor.d;
        if (constraintAnchor3 == null) {
            return 0;
        }
        ConstraintWidget constraintWidget2 = constraintAnchor3.b;
        ConstraintWidget constraintWidget3 = constraintWidget.D;
        if (constraintWidget2 != constraintWidget3) {
            return 0;
        }
        ConstraintAnchor constraintAnchor4 = constraintAnchor2.d;
        if (constraintAnchor4 == null || constraintAnchor4.b != constraintWidget3) {
            return 0;
        }
        return (int) (((float) (((constraintWidget3.d(i) - constraintAnchor.b()) - constraintAnchor2.b()) - constraintWidget.d(i))) * (i == 0 ? constraintWidget.V : constraintWidget.W));
    }

    @DexIgnore
    public static int a(ConstraintWidget constraintWidget) {
        float f;
        float f2;
        if (constraintWidget.k() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            if (constraintWidget.H == 0) {
                f2 = ((float) constraintWidget.j()) * constraintWidget.G;
            } else {
                f2 = ((float) constraintWidget.j()) / constraintWidget.G;
            }
            int i = (int) f2;
            constraintWidget.p(i);
            return i;
        } else if (constraintWidget.r() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (constraintWidget.H == 1) {
                f = ((float) constraintWidget.t()) * constraintWidget.G;
            } else {
                f = ((float) constraintWidget.t()) / constraintWidget.G;
            }
            int i2 = (int) f;
            constraintWidget.h(i2);
            return i2;
        }
    }
}
