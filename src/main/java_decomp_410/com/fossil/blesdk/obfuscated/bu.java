package com.fossil.blesdk.obfuscated;

import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bu implements mo<InputStream, ut> {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a;
    @DexIgnore
    public /* final */ mo<ByteBuffer, ut> b;
    @DexIgnore
    public /* final */ gq c;

    @DexIgnore
    public bu(List<ImageHeaderParser> list, mo<ByteBuffer, ut> moVar, gq gqVar) {
        this.a = list;
        this.b = moVar;
        this.c = gqVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, lo loVar) throws IOException {
        return !((Boolean) loVar.a(au.b)).booleanValue() && io.b(this.a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public aq<ut> a(InputStream inputStream, int i, int i2, lo loVar) throws IOException {
        byte[] a2 = a(inputStream);
        if (a2 == null) {
            return null;
        }
        return this.b.a(ByteBuffer.wrap(a2), i, i2, loVar);
    }

    @DexIgnore
    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        try {
            byte[] bArr = new byte[RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (!Log.isLoggable("StreamGifDecoder", 5)) {
                return null;
            }
            Log.w("StreamGifDecoder", "Error reading data from stream", e);
            return null;
        }
    }
}
