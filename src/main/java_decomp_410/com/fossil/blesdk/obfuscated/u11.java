package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u11 extends h11 {
    @DexIgnore
    public /* final */ ue0<Status> e;

    @DexIgnore
    public u11(ue0<Status> ue0) {
        this.e = ue0;
    }

    @DexIgnore
    public final void c(Status status) {
        this.e.a(status);
    }
}
