package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wm */
public interface C3229wm {
    @DexIgnore
    /* renamed from: a */
    int mo14040a();

    @DexIgnore
    /* renamed from: a */
    void mo14041a(com.android.volley.VolleyError volleyError) throws com.android.volley.VolleyError;

    @DexIgnore
    /* renamed from: b */
    int mo14042b();
}
