package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bl {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public bl(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || bl.class != obj.getClass()) {
            return false;
        }
        bl blVar = (bl) obj;
        if (this.b != blVar.b) {
            return false;
        }
        return this.a.equals(blVar.a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }
}
