package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.wq;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yq extends wq {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wq.a {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(Context context, String str) {
            this.a = context;
            this.b = str;
        }

        @DexIgnore
        public File a() {
            File cacheDir = this.a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            String str = this.b;
            return str != null ? new File(cacheDir, str) : cacheDir;
        }
    }

    @DexIgnore
    public yq(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public yq(Context context, String str, long j) {
        super(new a(context, str), j);
    }
}
