package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fe2 extends ee2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.iv_back, 1);
        y.put(R.id.ftv_title, 2);
        y.put(R.id.cl_calls_from_every_one, 3);
        y.put(R.id.accb_select_calls, 4);
        y.put(R.id.ll_text_container_calls, 5);
        y.put(R.id.pick_contact_title_calls, 6);
        y.put(R.id.flv_assigned_calls, 7);
        y.put(R.id.cl_texts_from_every_one, 8);
        y.put(R.id.accb_select_messages, 9);
        y.put(R.id.ll_text_container, 10);
        y.put(R.id.pick_contact_title, 11);
        y.put(R.id.flv_assigned_texts, 12);
    }
    */

    @DexIgnore
    public fe2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 13, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public fe2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[4], objArr[9], objArr[3], objArr[8], objArr[7], objArr[12], objArr[2], objArr[1], objArr[10], objArr[5], objArr[11], objArr[6]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
