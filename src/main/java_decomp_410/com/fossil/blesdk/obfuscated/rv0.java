package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rv0 {
    @DexIgnore
    boolean zza(Class<?> cls);

    @DexIgnore
    qv0 zzb(Class<?> cls);
}
