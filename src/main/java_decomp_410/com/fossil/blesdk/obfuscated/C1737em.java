package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.em */
public final class C1737em {
    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m6510a(android.content.Intent intent) {
        return intent.getBundleExtra(com.facebook.applinks.AppLinkData.BUNDLE_AL_APPLINK_DATA_KEY);
    }

    @DexIgnore
    /* renamed from: b */
    public static android.os.Bundle m6511b(android.content.Intent intent) {
        android.os.Bundle a = m6510a(intent);
        if (a == null) {
            return null;
        }
        return a.getBundle(com.facebook.applinks.AppLinkData.ARGUMENTS_EXTRAS_KEY);
    }
}
