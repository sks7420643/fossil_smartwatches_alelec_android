package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q */
public class C2687q implements android.os.Parcelable {
    @DexIgnore
    public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C2687q> CREATOR; // = new com.fossil.blesdk.obfuscated.C2687q.C2688a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ boolean f8486e; // = false;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.os.Handler f8487f; // = null;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2612p f8488g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q$a")
    /* renamed from: com.fossil.blesdk.obfuscated.q$a */
    public static class C2688a implements android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C2687q> {
        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2687q createFromParcel(android.os.Parcel parcel) {
            return new com.fossil.blesdk.obfuscated.C2687q(parcel);
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2687q[] newArray(int i) {
            return new com.fossil.blesdk.obfuscated.C2687q[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q$b")
    /* renamed from: com.fossil.blesdk.obfuscated.q$b */
    public class C2689b extends com.fossil.blesdk.obfuscated.C2612p.C2613a {
        @DexIgnore
        public C2689b() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14570a(int i, android.os.Bundle bundle) {
            com.fossil.blesdk.obfuscated.C2687q qVar = com.fossil.blesdk.obfuscated.C2687q.this;
            android.os.Handler handler = qVar.f8487f;
            if (handler != null) {
                handler.post(new com.fossil.blesdk.obfuscated.C2687q.C2690c(i, bundle));
            } else {
                qVar.mo5a(i, bundle);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q$c")
    /* renamed from: com.fossil.blesdk.obfuscated.q$c */
    public class C2690c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f8490e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ android.os.Bundle f8491f;

        @DexIgnore
        public C2690c(int i, android.os.Bundle bundle) {
            this.f8490e = i;
            this.f8491f = bundle;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2687q.this.mo5a(this.f8490e, this.f8491f);
        }
    }

    @DexIgnore
    public C2687q(android.os.Parcel parcel) {
        this.f8488g = com.fossil.blesdk.obfuscated.C2612p.C2613a.m12030a(parcel.readStrongBinder());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo5a(int i, android.os.Bundle bundle) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14923b(int i, android.os.Bundle bundle) {
        if (this.f8486e) {
            android.os.Handler handler = this.f8487f;
            if (handler != null) {
                handler.post(new com.fossil.blesdk.obfuscated.C2687q.C2690c(i, bundle));
            } else {
                mo5a(i, bundle);
            }
        } else {
            com.fossil.blesdk.obfuscated.C2612p pVar = this.f8488g;
            if (pVar != null) {
                try {
                    pVar.mo14570a(i, bundle);
                } catch (android.os.RemoteException unused) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(android.os.Parcel parcel, int i) {
        synchronized (this) {
            if (this.f8488g == null) {
                this.f8488g = new com.fossil.blesdk.obfuscated.C2687q.C2689b();
            }
            parcel.writeStrongBinder(this.f8488g.asBinder());
        }
    }
}
