package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k1 */
public final class C2179k1 implements com.fossil.blesdk.obfuscated.C2001i7 {

    @DexIgnore
    /* renamed from: A */
    public android.view.View f6653A;

    @DexIgnore
    /* renamed from: B */
    public com.fossil.blesdk.obfuscated.C2382m8 f6654B;

    @DexIgnore
    /* renamed from: C */
    public android.view.MenuItem.OnActionExpandListener f6655C;

    @DexIgnore
    /* renamed from: D */
    public boolean f6656D; // = false;

    @DexIgnore
    /* renamed from: E */
    public android.view.ContextMenu.ContextMenuInfo f6657E;

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f6658a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f6659b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f6660c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f6661d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.CharSequence f6662e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.CharSequence f6663f;

    @DexIgnore
    /* renamed from: g */
    public android.content.Intent f6664g;

    @DexIgnore
    /* renamed from: h */
    public char f6665h;

    @DexIgnore
    /* renamed from: i */
    public int f6666i; // = 4096;

    @DexIgnore
    /* renamed from: j */
    public char f6667j;

    @DexIgnore
    /* renamed from: k */
    public int f6668k; // = 4096;

    @DexIgnore
    /* renamed from: l */
    public android.graphics.drawable.Drawable f6669l;

    @DexIgnore
    /* renamed from: m */
    public int f6670m; // = 0;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C1915h1 f6671n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C3078v1 f6672o;

    @DexIgnore
    /* renamed from: p */
    public java.lang.Runnable f6673p;

    @DexIgnore
    /* renamed from: q */
    public android.view.MenuItem.OnMenuItemClickListener f6674q;

    @DexIgnore
    /* renamed from: r */
    public java.lang.CharSequence f6675r;

    @DexIgnore
    /* renamed from: s */
    public java.lang.CharSequence f6676s;

    @DexIgnore
    /* renamed from: t */
    public android.content.res.ColorStateList f6677t; // = null;

    @DexIgnore
    /* renamed from: u */
    public android.graphics.PorterDuff.Mode f6678u; // = null;

    @DexIgnore
    /* renamed from: v */
    public boolean f6679v; // = false;

    @DexIgnore
    /* renamed from: w */
    public boolean f6680w; // = false;

    @DexIgnore
    /* renamed from: x */
    public boolean f6681x; // = false;

    @DexIgnore
    /* renamed from: y */
    public int f6682y; // = 16;

    @DexIgnore
    /* renamed from: z */
    public int f6683z; // = 0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.k1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.k1$a */
    public class C2180a implements com.fossil.blesdk.obfuscated.C2382m8.C2384b {
        @DexIgnore
        public C2180a() {
        }

        @DexIgnore
        public void onActionProviderVisibilityChanged(boolean z) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = com.fossil.blesdk.obfuscated.C2179k1.this;
            k1Var.f6671n.mo11496d(k1Var);
        }
    }

    @DexIgnore
    public C2179k1(com.fossil.blesdk.obfuscated.C1915h1 h1Var, int i, int i2, int i3, int i4, java.lang.CharSequence charSequence, int i5) {
        this.f6671n = h1Var;
        this.f6658a = i2;
        this.f6659b = i;
        this.f6660c = i3;
        this.f6661d = i4;
        this.f6662e = charSequence;
        this.f6683z = i5;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m9279a(java.lang.StringBuilder sb, int i, int i2, java.lang.String str) {
        if ((i & i2) == i2) {
            sb.append(str);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12551b(boolean z) {
        int i = this.f6682y;
        this.f6682y = (z ? 2 : 0) | (i & -3);
        if (i != this.f6682y) {
            this.f6671n.mo11489c(false);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public int mo12552c() {
        return this.f6661d;
    }

    @DexIgnore
    public boolean collapseActionView() {
        if ((this.f6683z & 8) == 0) {
            return false;
        }
        if (this.f6653A == null) {
            return true;
        }
        android.view.MenuItem.OnActionExpandListener onActionExpandListener = this.f6655C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionCollapse(this)) {
            return this.f6671n.mo11468a(this);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: d */
    public char mo12554d() {
        return this.f6671n.mo11517p() ? this.f6667j : this.f6665h;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.String mo12556e() {
        char d = mo12554d();
        if (d == 0) {
            return "";
        }
        android.content.res.Resources resources = this.f6671n.mo11498e().getResources();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        if (android.view.ViewConfiguration.get(this.f6671n.mo11498e()).hasPermanentMenuKey()) {
            sb.append(resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_prepend_shortcut_label));
        }
        int i = this.f6671n.mo11517p() ? this.f6668k : this.f6666i;
        m9279a(sb, i, 65536, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_meta_shortcut_label));
        m9279a(sb, i, 4096, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_ctrl_shortcut_label));
        m9279a(sb, i, 2, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_alt_shortcut_label));
        m9279a(sb, i, 1, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_shift_shortcut_label));
        m9279a(sb, i, 4, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_sym_shortcut_label));
        m9279a(sb, i, 8, resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_function_shortcut_label));
        if (d == 8) {
            sb.append(resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_delete_shortcut_label));
        } else if (d == 10) {
            sb.append(resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_enter_shortcut_label));
        } else if (d != ' ') {
            sb.append(d);
        } else {
            sb.append(resources.getString(com.fossil.blesdk.obfuscated.C3336y.abc_menu_space_shortcut_label));
        }
        return sb.toString();
    }

    @DexIgnore
    public boolean expandActionView() {
        if (!mo12558f()) {
            return false;
        }
        android.view.MenuItem.OnActionExpandListener onActionExpandListener = this.f6655C;
        if (onActionExpandListener == null || onActionExpandListener.onMenuItemActionExpand(this)) {
            return this.f6671n.mo11484b(this);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo12558f() {
        if ((this.f6683z & 8) == 0) {
            return false;
        }
        if (this.f6653A == null) {
            com.fossil.blesdk.obfuscated.C2382m8 m8Var = this.f6654B;
            if (m8Var != null) {
                this.f6653A = m8Var.onCreateActionView(this);
            }
        }
        if (this.f6653A != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo12559g() {
        android.view.MenuItem.OnMenuItemClickListener onMenuItemClickListener = this.f6674q;
        if (onMenuItemClickListener != null && onMenuItemClickListener.onMenuItemClick(this)) {
            return true;
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f6671n;
        if (h1Var.mo11467a(h1Var, (android.view.MenuItem) this)) {
            return true;
        }
        java.lang.Runnable runnable = this.f6673p;
        if (runnable != null) {
            runnable.run();
            return true;
        }
        if (this.f6664g != null) {
            try {
                this.f6671n.mo11498e().startActivity(this.f6664g);
                return true;
            } catch (android.content.ActivityNotFoundException e) {
                android.util.Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e);
            }
        }
        com.fossil.blesdk.obfuscated.C2382m8 m8Var = this.f6654B;
        if (m8Var == null || !m8Var.onPerformDefaultAction()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public android.view.ActionProvider getActionProvider() {
        throw new java.lang.UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    @DexIgnore
    public android.view.View getActionView() {
        android.view.View view = this.f6653A;
        if (view != null) {
            return view;
        }
        com.fossil.blesdk.obfuscated.C2382m8 m8Var = this.f6654B;
        if (m8Var == null) {
            return null;
        }
        this.f6653A = m8Var.onCreateActionView(this);
        return this.f6653A;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return this.f6668k;
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.f6667j;
    }

    @DexIgnore
    public java.lang.CharSequence getContentDescription() {
        return this.f6675r;
    }

    @DexIgnore
    public int getGroupId() {
        return this.f6659b;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getIcon() {
        android.graphics.drawable.Drawable drawable = this.f6669l;
        if (drawable != null) {
            return mo12545a(drawable);
        }
        if (this.f6670m == 0) {
            return null;
        }
        android.graphics.drawable.Drawable c = com.fossil.blesdk.obfuscated.C2364m0.m10497c(this.f6671n.mo11498e(), this.f6670m);
        this.f6670m = 0;
        this.f6669l = c;
        return mo12545a(c);
    }

    @DexIgnore
    public android.content.res.ColorStateList getIconTintList() {
        return this.f6677t;
    }

    @DexIgnore
    public android.graphics.PorterDuff.Mode getIconTintMode() {
        return this.f6678u;
    }

    @DexIgnore
    public android.content.Intent getIntent() {
        return this.f6664g;
    }

    @DexIgnore
    @android.view.ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f6658a;
    }

    @DexIgnore
    public android.view.ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.f6657E;
    }

    @DexIgnore
    public int getNumericModifiers() {
        return this.f6666i;
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.f6665h;
    }

    @DexIgnore
    public int getOrder() {
        return this.f6660c;
    }

    @DexIgnore
    public android.view.SubMenu getSubMenu() {
        return this.f6672o;
    }

    @DexIgnore
    @android.view.ViewDebug.CapturedViewProperty
    public java.lang.CharSequence getTitle() {
        return this.f6662e;
    }

    @DexIgnore
    public java.lang.CharSequence getTitleCondensed() {
        java.lang.CharSequence charSequence = this.f6663f;
        if (charSequence == null) {
            charSequence = this.f6662e;
        }
        return (android.os.Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof java.lang.String)) ? charSequence : charSequence.toString();
    }

    @DexIgnore
    public java.lang.CharSequence getTooltipText() {
        return this.f6676s;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo12572h() {
        return (this.f6682y & 32) == 32;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return this.f6672o != null;
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo12574i() {
        return (this.f6682y & 4) != 0;
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return this.f6656D;
    }

    @DexIgnore
    public boolean isCheckable() {
        return (this.f6682y & 1) == 1;
    }

    @DexIgnore
    public boolean isChecked() {
        return (this.f6682y & 2) == 2;
    }

    @DexIgnore
    public boolean isEnabled() {
        return (this.f6682y & 16) != 0;
    }

    @DexIgnore
    public boolean isVisible() {
        com.fossil.blesdk.obfuscated.C2382m8 m8Var = this.f6654B;
        if (m8Var == null || !m8Var.overridesItemVisibility()) {
            if ((this.f6682y & 8) == 0) {
                return true;
            }
            return false;
        } else if ((this.f6682y & 8) != 0 || !this.f6654B.isVisible()) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public boolean mo12579j() {
        return (this.f6683z & 1) == 1;
    }

    @DexIgnore
    /* renamed from: k */
    public boolean mo12580k() {
        return (this.f6683z & 2) == 2;
    }

    @DexIgnore
    /* renamed from: l */
    public boolean mo12581l() {
        return this.f6671n.mo11512k();
    }

    @DexIgnore
    /* renamed from: m */
    public boolean mo12582m() {
        return this.f6671n.mo11520q() && mo12554d() != 0;
    }

    @DexIgnore
    /* renamed from: n */
    public boolean mo12583n() {
        return (this.f6683z & 4) == 4;
    }

    @DexIgnore
    public android.view.MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new java.lang.UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c) {
        if (this.f6667j == c) {
            return this;
        }
        this.f6667j = java.lang.Character.toLowerCase(c);
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setCheckable(boolean z) {
        int i = this.f6682y;
        this.f6682y = z | (i & true) ? 1 : 0;
        if (i != this.f6682y) {
            this.f6671n.mo11489c(false);
        }
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setChecked(boolean z) {
        if ((this.f6682y & 4) != 0) {
            this.f6671n.mo11459a((android.view.MenuItem) this);
        } else {
            mo12551b(z);
        }
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setEnabled(boolean z) {
        if (z) {
            this.f6682y |= 16;
        } else {
            this.f6682y &= -17;
        }
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIcon(android.graphics.drawable.Drawable drawable) {
        this.f6670m = 0;
        this.f6669l = drawable;
        this.f6681x = true;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintList(android.content.res.ColorStateList colorStateList) {
        this.f6677t = colorStateList;
        this.f6679v = true;
        this.f6681x = true;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintMode(android.graphics.PorterDuff.Mode mode) {
        this.f6678u = mode;
        this.f6680w = true;
        this.f6681x = true;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIntent(android.content.Intent intent) {
        this.f6664g = intent;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c) {
        if (this.f6665h == c) {
            return this;
        }
        this.f6665h = c;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem.OnActionExpandListener onActionExpandListener) {
        this.f6655C = onActionExpandListener;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.f6674q = onMenuItemClickListener;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2) {
        this.f6665h = c;
        this.f6667j = java.lang.Character.toLowerCase(c2);
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
        int i2 = i & 3;
        if (i2 == 0 || i2 == 1 || i2 == 2) {
            this.f6683z = i;
            this.f6671n.mo11488c(this);
            return;
        }
        throw new java.lang.IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
    }

    @DexIgnore
    public android.view.MenuItem setTitle(java.lang.CharSequence charSequence) {
        this.f6662e = charSequence;
        this.f6671n.mo11489c(false);
        com.fossil.blesdk.obfuscated.C3078v1 v1Var = this.f6672o;
        if (v1Var != null) {
            v1Var.setHeaderTitle(charSequence);
        }
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitleCondensed(java.lang.CharSequence charSequence) {
        this.f6663f = charSequence;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setVisible(boolean z) {
        if (mo12557e(z)) {
            this.f6671n.mo11496d(this);
        }
        return this;
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.CharSequence charSequence = this.f6662e;
        if (charSequence != null) {
            return charSequence.toString();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12548a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        this.f6672o = v1Var;
        v1Var.setHeaderTitle(getTitle());
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12553c(boolean z) {
        this.f6682y = (z ? 4 : 0) | (this.f6682y & -5);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo12555d(boolean z) {
        if (z) {
            this.f6682y |= 32;
        } else {
            this.f6682y &= -33;
        }
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setContentDescription(java.lang.CharSequence charSequence) {
        this.f6675r = charSequence;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setTooltipText(java.lang.CharSequence charSequence) {
        this.f6676s = charSequence;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setActionView(android.view.View view) {
        this.f6653A = view;
        this.f6654B = null;
        if (view != null && view.getId() == -1) {
            int i = this.f6658a;
            if (i > 0) {
                view.setId(i);
            }
        }
        this.f6671n.mo11488c(this);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.CharSequence mo12546a(com.fossil.blesdk.obfuscated.C2697q1.C2698a aVar) {
        if (aVar == null || !aVar.mo351a()) {
            return getTitle();
        }
        return getTitleCondensed();
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c, int i) {
        if (this.f6667j == c && this.f6668k == i) {
            return this;
        }
        this.f6667j = java.lang.Character.toLowerCase(c);
        this.f6668k = android.view.KeyEvent.normalizeMetaState(i);
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c, int i) {
        if (this.f6665h == c && this.f6666i == i) {
            return this;
        }
        this.f6665h = c;
        this.f6666i = android.view.KeyEvent.normalizeMetaState(i);
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f6665h = c;
        this.f6666i = android.view.KeyEvent.normalizeMetaState(i);
        this.f6667j = java.lang.Character.toLowerCase(c2);
        this.f6668k = android.view.KeyEvent.normalizeMetaState(i2);
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12550b() {
        this.f6671n.mo11488c(this);
    }

    @DexIgnore
    public android.view.MenuItem setIcon(int i) {
        this.f6669l = null;
        this.f6670m = i;
        this.f6681x = true;
        this.f6671n.mo11489c(false);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitle(int i) {
        setTitle((java.lang.CharSequence) this.f6671n.mo11498e().getString(i));
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo12545a(android.graphics.drawable.Drawable drawable) {
        if (drawable != null && this.f6681x && (this.f6679v || this.f6680w)) {
            drawable = com.fossil.blesdk.obfuscated.C1538c7.m5314i(drawable).mutate();
            if (this.f6679v) {
                com.fossil.blesdk.obfuscated.C1538c7.m5299a(drawable, this.f6677t);
            }
            if (this.f6680w) {
                com.fossil.blesdk.obfuscated.C1538c7.m5302a(drawable, this.f6678u);
            }
            this.f6681x = false;
        }
        return drawable;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setActionView(int i) {
        android.content.Context e = this.f6671n.mo11498e();
        setActionView(android.view.LayoutInflater.from(e).inflate(i, new android.widget.LinearLayout(e), false));
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12547a(android.view.ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.f6657E = contextMenuInfo;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2382m8 mo8358a() {
        return this.f6654B;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2001i7 mo8357a(com.fossil.blesdk.obfuscated.C2382m8 m8Var) {
        com.fossil.blesdk.obfuscated.C2382m8 m8Var2 = this.f6654B;
        if (m8Var2 != null) {
            m8Var2.reset();
        }
        this.f6653A = null;
        this.f6654B = m8Var;
        this.f6671n.mo11489c(true);
        com.fossil.blesdk.obfuscated.C2382m8 m8Var3 = this.f6654B;
        if (m8Var3 != null) {
            m8Var3.setVisibilityListener(new com.fossil.blesdk.obfuscated.C2179k1.C2180a());
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12549a(boolean z) {
        this.f6656D = z;
        this.f6671n.mo11489c(false);
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo12557e(boolean z) {
        int i = this.f6682y;
        this.f6682y = (z ? 0 : 8) | (i & -9);
        if (i != this.f6682y) {
            return true;
        }
        return false;
    }
}
