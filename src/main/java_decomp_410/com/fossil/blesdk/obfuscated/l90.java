package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.JSONKey;
import com.fossil.fitness.ActiveMinute;
import com.fossil.fitness.Calorie;
import com.fossil.fitness.Distance;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GoalTracking;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.Resting;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.Step;
import com.fossil.fitness.WorkoutSession;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l90 {
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f8  */
    public static final JSONObject a(FitnessData fitnessData) {
        Object obj;
        kd4.b(fitnessData, "$this$toJSONObject");
        JSONObject jSONObject = new JSONObject();
        JSONKey jSONKey = JSONKey.WORKOUT_SESSION;
        ArrayList<WorkoutSession> workouts = fitnessData.getWorkouts();
        wa0.a(jSONObject, jSONKey, workouts != null ? Integer.valueOf(workouts.size()) : JSONObject.NULL);
        JSONKey jSONKey2 = JSONKey.SLEEP_SESSION;
        ArrayList<SleepSession> sleeps = fitnessData.getSleeps();
        wa0.a(jSONObject, jSONKey2, sleeps != null ? Integer.valueOf(sleeps.size()) : JSONObject.NULL);
        wa0.a(jSONObject, JSONKey.TIMEZONE_OFFSET_IN_SECOND, Integer.valueOf(fitnessData.getTimezoneOffsetInSecond()));
        JSONKey jSONKey3 = JSONKey.TOTAL_ACTIVE_MINUTES;
        ActiveMinute activeMinute = fitnessData.getActiveMinute();
        wa0.a(jSONObject, jSONKey3, activeMinute != null ? Integer.valueOf(activeMinute.getTotal()) : JSONObject.NULL);
        JSONKey jSONKey4 = JSONKey.TOTAL_DISTANCE;
        Distance distance = fitnessData.getDistance();
        wa0.a(jSONObject, jSONKey4, distance != null ? Double.valueOf(distance.getTotal()) : JSONObject.NULL);
        wa0.a(jSONObject, JSONKey.END_TIME, Integer.valueOf(fitnessData.getEndTime()));
        wa0.a(jSONObject, JSONKey.START_TIME, Integer.valueOf(fitnessData.getStartTime()));
        JSONKey jSONKey5 = JSONKey.TOTAL_STEPS;
        Step step = fitnessData.getStep();
        wa0.a(jSONObject, jSONKey5, step != null ? Integer.valueOf(step.getTotal()) : JSONObject.NULL);
        JSONKey jSONKey6 = JSONKey.TOTAL_CALORIES;
        Calorie calorie = fitnessData.getCalorie();
        wa0.a(jSONObject, jSONKey6, calorie != null ? Integer.valueOf(calorie.getTotal()) : JSONObject.NULL);
        JSONKey jSONKey7 = JSONKey.HEART_RATE_RECORD;
        HeartRate heartrate = fitnessData.getHeartrate();
        if (heartrate != null) {
            ArrayList<Short> values = heartrate.getValues();
            if (values != null) {
                obj = Integer.valueOf(values.size());
                wa0.a(jSONObject, jSONKey7, obj);
                JSONKey jSONKey8 = JSONKey.RESTING;
                ArrayList<Resting> resting = fitnessData.getResting();
                wa0.a(jSONObject, jSONKey8, resting == null ? Integer.valueOf(resting.size()) : JSONObject.NULL);
                JSONKey jSONKey9 = JSONKey.GOAL_TRACKING;
                ArrayList<GoalTracking> goals = fitnessData.getGoals();
                wa0.a(jSONObject, jSONKey9, goals == null ? Integer.valueOf(goals.size()) : JSONObject.NULL);
                return jSONObject;
            }
        }
        obj = JSONObject.NULL;
        wa0.a(jSONObject, jSONKey7, obj);
        JSONKey jSONKey82 = JSONKey.RESTING;
        ArrayList<Resting> resting2 = fitnessData.getResting();
        wa0.a(jSONObject, jSONKey82, resting2 == null ? Integer.valueOf(resting2.size()) : JSONObject.NULL);
        JSONKey jSONKey92 = JSONKey.GOAL_TRACKING;
        ArrayList<GoalTracking> goals2 = fitnessData.getGoals();
        wa0.a(jSONObject, jSONKey92, goals2 == null ? Integer.valueOf(goals2.size()) : JSONObject.NULL);
        return jSONObject;
    }

    @DexIgnore
    public static final JSONArray a(FitnessData[] fitnessDataArr) {
        kd4.b(fitnessDataArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (FitnessData a : fitnessDataArr) {
            jSONArray.put(a(a));
        }
        return jSONArray;
    }
}
