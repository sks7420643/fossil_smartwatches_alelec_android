package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m */
public class C2362m {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.m$a")
    /* renamed from: com.fossil.blesdk.obfuscated.m$a */
    public static class C2363a {
        @DexIgnore
        /* renamed from: a */
        public static java.lang.Object m10491a(java.lang.Object obj) {
            return ((android.media.session.MediaSession.QueueItem) obj).getDescription();
        }

        @DexIgnore
        /* renamed from: b */
        public static long m10492b(java.lang.Object obj) {
            return ((android.media.session.MediaSession.QueueItem) obj).getQueueId();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m10490a(java.lang.Object obj) {
        if (obj instanceof android.media.session.MediaSession.Token) {
            return obj;
        }
        throw new java.lang.IllegalArgumentException("token is not a valid MediaSession.Token object");
    }
}
