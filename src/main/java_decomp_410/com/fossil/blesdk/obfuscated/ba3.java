package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ba3 implements Factory<DashboardCaloriesPresenter> {
    @DexIgnore
    public static DashboardCaloriesPresenter a(z93 z93, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, ActivitySummaryDao activitySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, h42 h42, xk2 xk2) {
        return new DashboardCaloriesPresenter(z93, summariesRepository, fitnessDataRepository, activitySummaryDao, fitnessDatabase, userRepository, h42, xk2);
    }
}
