package com.fossil.blesdk.obfuscated;

import io.reactivex.BackpressureStrategy;
import java.lang.reflect.Type;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class as4<R> implements dr4<R, Object> {
    @DexIgnore
    public /* final */ Type a;
    @DexIgnore
    public /* final */ r84 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public as4(Type type, r84 r84, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        this.a = type;
        this.b = r84;
        this.c = z;
        this.d = z2;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = z6;
        this.i = z7;
    }

    @DexIgnore
    public Type a() {
        return this.a;
    }

    @DexIgnore
    public Object a(Call<R> call) {
        o84 o84;
        o84 o842;
        if (this.c) {
            o84 = new wr4(call);
        } else {
            o84 = new xr4(call);
        }
        if (this.d) {
            o842 = new zr4(o84);
        } else {
            o842 = this.e ? new vr4(o84) : o84;
        }
        r84 r84 = this.b;
        if (r84 != null) {
            o842 = o842.b(r84);
        }
        if (this.f) {
            return o842.a(BackpressureStrategy.LATEST);
        }
        if (this.g) {
            return o842.c();
        }
        if (this.h) {
            return o842.b();
        }
        if (this.i) {
            return o842.a();
        }
        return ia4.a(o842);
    }
}
