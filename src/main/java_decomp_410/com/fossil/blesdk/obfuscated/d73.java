package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d73 {
    @DexIgnore
    public /* final */ b73 a;

    @DexIgnore
    public d73(b73 b73) {
        kd4.b(b73, "mView");
        this.a = b73;
    }

    @DexIgnore
    public final b73 a() {
        return this.a;
    }
}
