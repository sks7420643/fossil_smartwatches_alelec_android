package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zd4 implements Iterable<Long>, rd4 {
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public zd4(long j, long j2, long j3) {
        if (j3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (j3 != Long.MIN_VALUE) {
            this.e = j;
            this.f = nc4.b(j, j2, j3);
            this.g = j3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
        }
    }

    @DexIgnore
    public final long a() {
        return this.e;
    }

    @DexIgnore
    public final long b() {
        return this.f;
    }

    @DexIgnore
    public nb4 iterator() {
        return new ae4(this.e, this.f, this.g);
    }
}
