package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.contract.JSONAbleObject;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j00 {
    @DexIgnore
    public static final JSONArray a(JSONAbleObject[] jSONAbleObjectArr) {
        kd4.b(jSONAbleObjectArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (JSONAbleObject jSONObject : jSONAbleObjectArr) {
            jSONArray.put(jSONObject.toJSONObject());
        }
        return jSONArray;
    }
}
