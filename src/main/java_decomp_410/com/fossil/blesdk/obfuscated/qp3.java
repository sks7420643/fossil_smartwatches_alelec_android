package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.qm2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qp3 extends eq2 implements bq3, ws3.g, qm2.b, ws3.h {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((fd4) null);
    @DexIgnore
    public aq3 m;
    @DexIgnore
    public tr3<q92> n;
    @DexIgnore
    public fk2 o;
    @DexIgnore
    public List<DeviceHelper.ImageStyle> p; // = new ArrayList();
    @DexIgnore
    public HashMap q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final qp3 a() {
            return new qp3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qp3 e;

        @DexIgnore
        public b(qp3 qp3) {
            this.e = qp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qp3.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qp3 e;

        @DexIgnore
        public c(qp3 qp3) {
            this.e = qp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            qp3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ q92 a;
        @DexIgnore
        public /* final */ /* synthetic */ qp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements qv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = qp3.r;
                local.d(j1, "showHour onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
                kd4.b(drawable, "resource");
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                kd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(qp3.r, "showHour onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public d(q92 q92, qp3 qp3, String str) {
            this.a = q92;
            this.b = qp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = qp3.r;
            local.d(j1, "showHour onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                fk2 a2 = this.b.o;
                if (a2 != null) {
                    ek2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        ek2<Drawable> b2 = a3.b((qv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ q92 a;
        @DexIgnore
        public /* final */ /* synthetic */ qp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements qv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = qp3.r;
                local.d(j1, "showMinute onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
                kd4.b(drawable, "resource");
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                kd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(qp3.r, "showMinute onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public e(q92 q92, qp3 qp3, String str) {
            this.a = q92;
            this.b = qp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = qp3.r;
            local.d(j1, "showMinute onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                fk2 a2 = this.b.o;
                if (a2 != null) {
                    ek2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        ek2<Drawable> b2 = a3.b((qv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ q92 a;
        @DexIgnore
        public /* final */ /* synthetic */ qp3 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements qv<Drawable> {
            @DexIgnore
            public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String j1 = qp3.r;
                local.d(j1, "showSubeye onLoadFail e=" + glideException);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
                kd4.b(drawable, "resource");
                kd4.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                kd4.b(bwVar, "target");
                kd4.b(dataSource, "dataSource");
                FLogger.INSTANCE.getLocal().d(qp3.r, "showSubeye onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public f(q92 q92, qp3 qp3, String str) {
            this.a = q92;
            this.b = qp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String j1 = qp3.r;
            local.d(j1, "showSubeye onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive()) {
                fk2 a2 = this.b.o;
                if (a2 != null) {
                    ek2<Drawable> a3 = a2.a(str2);
                    if (a3 != null) {
                        ek2<Drawable> b2 = a3.b((qv<Drawable>) new a());
                        if (b2 != null) {
                            b2.a((ImageView) this.a.r);
                        }
                    }
                }
            }
        }
    }

    /*
    static {
        String simpleName = qp3.class.getSimpleName();
        kd4.a((Object) simpleName, "CalibrationFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ aq3 b(qp3 qp3) {
        aq3 aq3 = qp3.m;
        if (aq3 != null) {
            return aq3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B() {
        FLogger.INSTANCE.getLocal().d(r, "showBluetoothError");
        if (isActive()) {
            aq3 aq3 = this.m;
            if (aq3 != null) {
                aq3.a(true);
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.e(childFragmentManager);
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void Q(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            aq3 aq3 = this.m;
            if (aq3 != null) {
                aq3.a(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public boolean S0() {
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.h();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(View view, Boolean bool) {
    }

    @DexIgnore
    public void a1() {
        n();
    }

    @DexIgnore
    public void b1() {
        n();
    }

    @DexIgnore
    public void c(View view) {
        kd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    public void c1() {
        n();
    }

    @DexIgnore
    public void d(View view) {
        kd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }

    @DexIgnore
    public void d1() {
    }

    @DexIgnore
    public void e1() {
    }

    @DexIgnore
    public void j(String str) {
        kd4.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.p.size() > 2) {
                tr3<q92> tr3 = this.n;
                if (tr3 != null) {
                    q92 a2 = tr3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.w;
                        kd4.a((Object) flexibleTextView, "binding.ftvTitle");
                        flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchSubeyeHybrid_Title__AlignTheWatchsSubeyeHandTo));
                        FlexibleTextView flexibleTextView2 = a2.u;
                        kd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                        flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchSubeyeHybrid_Text__FinallyUseTheArrowsToMatch));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        kd4.a((Object) appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(2))).setImageCallback(new f(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void k(String str) {
        kd4.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            tr3<q92> tr3 = this.n;
            if (tr3 != null) {
                q92 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    kd4.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_Title__AlignTheWatchsHourHandTo));
                    FlexibleTextView flexibleTextView2 = a2.u;
                    kd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_Text__UseTheArrowsToMoveThe));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    kd4.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(0))).setImageCallback(new d(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void n() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void o(String str) {
        kd4.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showMinute: deviceId = " + str);
            tr3<q92> tr3 = this.n;
            if (tr3 != null) {
                q92 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    kd4.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHand_Title__AlignTheWatchsMinuteHandTo));
                    FlexibleTextView flexibleTextView2 = a2.u;
                    kd4.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHand_Text__NowUseTheArrowsToMatch));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    kd4.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(1))).setImageCallback(new e(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onClick(View view) {
        kd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onClick");
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.b(view.getId() == R.id.aciv_right);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        aq3 aq3 = this.m;
        if (aq3 != null) {
            this.p = aq3.i();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        q92 q92 = (q92) qa.a(layoutInflater, R.layout.fragment_calibration, viewGroup, false, O0());
        q92.q.setOnClickListener(new b(this));
        q92.v.setOnClickListener(new c(this));
        new qm2().a(q92.s, this);
        new qm2().a(q92.t, this);
        this.n = new tr3<>(this, q92);
        this.o = ck2.a((Fragment) this);
        kd4.a((Object) q92, "binding");
        return q92.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(View view) {
        kd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEnded");
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.k();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(aq3 aq3) {
        kd4.b(aq3, "presenter");
        this.m = aq3;
    }

    @DexIgnore
    public void a(int i, int i2) {
        String str;
        if (isActive()) {
            tr3<q92> tr3 = this.n;
            if (tr3 != null) {
                q92 a2 = tr3.a();
                if (a2 != null) {
                    a2.x.setLength(i2);
                    int i3 = i + 1;
                    a2.x.setProgress((int) ((((float) i3) / ((float) i2)) * ((float) 100)));
                    if (i3 < i2) {
                        str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchHourHand_CTA__Next);
                    } else {
                        str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchCalibrate_CalibrateWatchMinuteHandDiana_CTA__Done);
                    }
                    FlexibleButton flexibleButton = a2.v;
                    kd4.a((Object) flexibleButton, "it.ftvNext");
                    flexibleButton.setText(str);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
        kd4.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEvent");
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.c(view.getId() == R.id.aciv_right);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        super.a(str, i, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        n();
    }

    @DexIgnore
    public void o() {
        FLogger.INSTANCE.getLocal().d(r, "showGeneralError");
        aq3 aq3 = this.m;
        if (aq3 != null) {
            aq3.a(true);
            if (isActive()) {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.w(childFragmentManager);
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }
}
