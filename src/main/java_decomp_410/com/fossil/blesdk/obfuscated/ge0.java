package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.kj0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ge0 {
    @DexIgnore
    public static /* final */ Set<ge0> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public interface b {
        @DexIgnore
        void e(Bundle bundle);

        @DexIgnore
        void f(int i);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ud0 ud0);
    }

    @DexIgnore
    public static Set<ge0> i() {
        Set<ge0> set;
        synchronized (a) {
            set = a;
        }
        return set;
    }

    @DexIgnore
    public <A extends de0.b, R extends me0, T extends te0<R, A>> T a(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract ud0 a();

    @DexIgnore
    public abstract void a(c cVar);

    @DexIgnore
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    public abstract he0<Status> b();

    @DexIgnore
    public <A extends de0.b, T extends te0<? extends me0, A>> T b(T t) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract void b(c cVar);

    @DexIgnore
    public abstract void c();

    @DexIgnore
    public abstract void d();

    @DexIgnore
    public Context e() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public Looper f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    public void h() {
        throw new UnsupportedOperationException();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public /* final */ Set<Scope> b; // = new HashSet();
        @DexIgnore
        public /* final */ Set<Scope> c; // = new HashSet();
        @DexIgnore
        public int d;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public /* final */ Map<de0<?>, kj0.b> h; // = new g4();
        @DexIgnore
        public /* final */ Context i;
        @DexIgnore
        public /* final */ Map<de0<?>, de0.d> j; // = new g4();
        @DexIgnore
        public xe0 k;
        @DexIgnore
        public int l; // = -1;
        @DexIgnore
        public c m;
        @DexIgnore
        public Looper n;
        @DexIgnore
        public xd0 o; // = xd0.a();
        @DexIgnore
        public de0.a<? extends ln1, vm1> p; // = in1.c;
        @DexIgnore
        public /* final */ ArrayList<b> q; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<c> r; // = new ArrayList<>();

        @DexIgnore
        public a(Context context) {
            this.i = context;
            this.n = context.getMainLooper();
            this.f = context.getPackageName();
            this.g = context.getClass().getName();
        }

        @DexIgnore
        public final a a(Handler handler) {
            bk0.a(handler, (Object) "Handler must not be null");
            this.n = handler.getLooper();
            return this;
        }

        @DexIgnore
        public final kj0 b() {
            vm1 vm1 = vm1.m;
            if (this.j.containsKey(in1.e)) {
                vm1 = (vm1) this.j.get(in1.e);
            }
            return new kj0(this.a, this.b, this.h, this.d, this.e, this.f, this.g, vm1, false);
        }

        @DexIgnore
        public final a a(b bVar) {
            bk0.a(bVar, (Object) "Listener must not be null");
            this.q.add(bVar);
            return this;
        }

        @DexIgnore
        public final a a(c cVar) {
            bk0.a(cVar, (Object) "Listener must not be null");
            this.r.add(cVar);
            return this;
        }

        @DexIgnore
        public final a a(Scope scope) {
            bk0.a(scope, (Object) "Scope must not be null");
            this.b.add(scope);
            return this;
        }

        @DexIgnore
        public final a a(String[] strArr) {
            for (String scope : strArr) {
                this.b.add(new Scope(scope));
            }
            return this;
        }

        @DexIgnore
        public final a a(de0<? extends de0.d.C0010d> de0) {
            bk0.a(de0, (Object) "Api must not be null");
            this.j.put(de0, (Object) null);
            List<Scope> a2 = de0.c().a(null);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final a a(de0<? extends de0.d.C0010d> de0, Scope... scopeArr) {
            bk0.a(de0, (Object) "Api must not be null");
            this.j.put(de0, (Object) null);
            a(de0, (de0.d) null, scopeArr);
            return this;
        }

        @DexIgnore
        public final <O extends de0.d.c> a a(de0<O> de0, O o2) {
            bk0.a(de0, (Object) "Api must not be null");
            bk0.a(o2, (Object) "Null options are not permitted for this Api");
            this.j.put(de0, o2);
            List<Scope> a2 = de0.c().a(o2);
            this.c.addAll(a2);
            this.b.addAll(a2);
            return this;
        }

        @DexIgnore
        public final ge0 a() {
            bk0.a(!this.j.isEmpty(), (Object) "must call addApi() to add at least one API");
            kj0 b2 = b();
            de0 de0 = null;
            Map<de0<?>, kj0.b> f2 = b2.f();
            g4 g4Var = new g4();
            g4 g4Var2 = new g4();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (de0 next : this.j.keySet()) {
                de0.d dVar = this.j.get(next);
                boolean z2 = f2.get(next) != null;
                g4Var.put(next, Boolean.valueOf(z2));
                gi0 gi0 = new gi0(next, z2);
                arrayList.add(gi0);
                de0.a d2 = next.d();
                de0 de02 = next;
                de0.f a2 = d2.a(this.i, this.n, b2, dVar, gi0, gi0);
                g4Var2.put(de02.a(), a2);
                if (d2.a() == 1) {
                    z = dVar != null;
                }
                if (a2.d()) {
                    if (de0 == null) {
                        de0 = de02;
                    } else {
                        String b3 = de02.b();
                        String b4 = de0.b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b3).length() + 21 + String.valueOf(b4).length());
                        sb.append(b3);
                        sb.append(" cannot be used with ");
                        sb.append(b4);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (de0 != null) {
                if (!z) {
                    bk0.b(this.a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", de0.b());
                    bk0.b(this.b.equals(this.c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", de0.b());
                } else {
                    String b5 = de0.b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b5).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b5);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            eg0 eg0 = new eg0(this.i, new ReentrantLock(), this.n, b2, this.o, this.p, g4Var, this.q, this.r, g4Var2, this.l, eg0.a((Iterable<de0.f>) g4Var2.values(), true), arrayList, false);
            synchronized (ge0.a) {
                ge0.a.add(eg0);
            }
            if (this.l >= 0) {
                zh0.b(this.k).a(this.l, eg0, this.m);
            }
            return eg0;
        }

        @DexIgnore
        public final <O extends de0.d> void a(de0<O> de0, O o2, Scope... scopeArr) {
            HashSet hashSet = new HashSet(de0.c().a(o2));
            for (Scope add : scopeArr) {
                hashSet.add(add);
            }
            this.h.put(de0, new kj0.b(hashSet));
        }
    }

    @DexIgnore
    public boolean a(cf0 cf0) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public void a(mh0 mh0) {
        throw new UnsupportedOperationException();
    }
}
