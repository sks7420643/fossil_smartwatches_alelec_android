package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.ev3;
import com.fossil.blesdk.obfuscated.hv3;
import com.squareup.okhttp.internal.http.RequestException;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.net.ProtocolException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tu3 {
    @DexIgnore
    public /* final */ gv3 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public hv3 d;
    @DexIgnore
    public uw3 e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ev3.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(int i, hv3 hv3, boolean z) {
            this.a = i;
            this.b = z;
        }

        @DexIgnore
        public jv3 a(hv3 hv3) throws IOException {
            if (this.a >= tu3.this.a.D().size()) {
                return tu3.this.a(hv3, this.b);
            }
            return tu3.this.a.D().get(this.a).a(new a(this.a + 1, hv3, this.b));
        }
    }

    @DexIgnore
    public tu3(gv3 gv3, hv3 hv3) {
        this.a = gv3.a();
        this.d = hv3;
    }

    @DexIgnore
    public jv3 a() throws IOException {
        synchronized (this) {
            if (!this.b) {
                this.b = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        try {
            this.a.i().a(this);
            jv3 a2 = a(false);
            if (a2 != null) {
                return a2;
            }
            throw new IOException("Canceled");
        } finally {
            this.a.i().b(this);
        }
    }

    @DexIgnore
    public final jv3 a(boolean z) throws IOException {
        return new a(0, this.d, z).a(this.d);
    }

    @DexIgnore
    public jv3 a(hv3 hv3, boolean z) throws IOException {
        iv3 a2 = hv3.a();
        if (a2 != null) {
            hv3.b g = hv3.g();
            fv3 contentType = a2.contentType();
            if (contentType != null) {
                g.b(GraphRequest.CONTENT_TYPE_HEADER, contentType.toString());
            }
            long contentLength = a2.contentLength();
            if (contentLength != -1) {
                g.b("Content-Length", Long.toString(contentLength));
                g.a("Transfer-Encoding");
            } else {
                g.b("Transfer-Encoding", "chunked");
                g.a("Content-Length");
            }
            hv3 = g.a();
        }
        this.e = new uw3(this.a, hv3, false, false, z, (wu3) null, (bx3) null, (ax3) null, (jv3) null);
        int i = 0;
        while (!this.c) {
            try {
                this.e.n();
                this.e.l();
                jv3 g2 = this.e.g();
                hv3 d2 = this.e.d();
                if (d2 == null) {
                    if (!z) {
                        this.e.m();
                    }
                    return g2;
                }
                i++;
                if (i <= 20) {
                    if (!this.e.a(d2.d())) {
                        this.e.m();
                    }
                    this.e = new uw3(this.a, d2, false, false, z, this.e.a(), (bx3) null, (ax3) null, g2);
                } else {
                    throw new ProtocolException("Too many follow-up requests: " + i);
                }
            } catch (RequestException e2) {
                throw e2.getCause();
            } catch (RouteException e3) {
                uw3 b2 = this.e.b(e3);
                if (b2 != null) {
                    this.e = b2;
                } else {
                    throw e3.getLastConnectException();
                }
            } catch (IOException e4) {
                uw3 a3 = this.e.a(e4, (xo4) null);
                if (a3 != null) {
                    this.e = a3;
                } else {
                    throw e4;
                }
            }
        }
        this.e.m();
        throw new IOException("Canceled");
    }
}
