package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rh */
public class C2808rh {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rh$a */
    public static class C2809a extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.View f8975a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.view.View f8976b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f8977c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ int f8978d;

        @DexIgnore
        /* renamed from: e */
        public int[] f8979e; // = ((int[]) this.f8975a.getTag(com.fossil.blesdk.obfuscated.C1875gh.transition_position));

        @DexIgnore
        /* renamed from: f */
        public float f8980f;

        @DexIgnore
        /* renamed from: g */
        public float f8981g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ float f8982h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ float f8983i;

        @DexIgnore
        public C2809a(android.view.View view, android.view.View view2, int i, int i2, float f, float f2) {
            this.f8976b = view;
            this.f8975a = view2;
            this.f8977c = i - java.lang.Math.round(this.f8976b.getTranslationX());
            this.f8978d = i2 - java.lang.Math.round(this.f8976b.getTranslationY());
            this.f8982h = f;
            this.f8983i = f2;
            if (this.f8979e != null) {
                this.f8975a.setTag(com.fossil.blesdk.obfuscated.C1875gh.transition_position, (java.lang.Object) null);
            }
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            if (this.f8979e == null) {
                this.f8979e = new int[2];
            }
            this.f8979e[0] = java.lang.Math.round(((float) this.f8977c) + this.f8976b.getTranslationX());
            this.f8979e[1] = java.lang.Math.round(((float) this.f8978d) + this.f8976b.getTranslationY());
            this.f8975a.setTag(com.fossil.blesdk.obfuscated.C1875gh.transition_position, this.f8979e);
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f8976b.setTranslationX(this.f8982h);
            this.f8976b.setTranslationY(this.f8983i);
        }

        @DexIgnore
        public void onAnimationPause(android.animation.Animator animator) {
            this.f8980f = this.f8976b.getTranslationX();
            this.f8981g = this.f8976b.getTranslationY();
            this.f8976b.setTranslationX(this.f8982h);
            this.f8976b.setTranslationY(this.f8983i);
        }

        @DexIgnore
        public void onAnimationResume(android.animation.Animator animator) {
            this.f8976b.setTranslationX(this.f8980f);
            this.f8976b.setTranslationY(this.f8981g);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m13215a(android.view.View view, com.fossil.blesdk.obfuscated.C2654ph phVar, int i, int i2, float f, float f2, float f3, float f4, android.animation.TimeInterpolator timeInterpolator) {
        float f5;
        float f6;
        android.view.View view2 = view;
        com.fossil.blesdk.obfuscated.C2654ph phVar2 = phVar;
        float translationX = view.getTranslationX();
        float translationY = view.getTranslationY();
        int[] iArr = (int[]) phVar2.f8383b.getTag(com.fossil.blesdk.obfuscated.C1875gh.transition_position);
        if (iArr != null) {
            f5 = ((float) (iArr[0] - i)) + translationX;
            f6 = ((float) (iArr[1] - i2)) + translationY;
        } else {
            f5 = f;
            f6 = f2;
        }
        int round = i + java.lang.Math.round(f5 - translationX);
        int round2 = i2 + java.lang.Math.round(f6 - translationY);
        view.setTranslationX(f5);
        view.setTranslationY(f6);
        if (f5 == f3 && f6 == f4) {
            return null;
        }
        android.animation.ObjectAnimator ofPropertyValuesHolder = android.animation.ObjectAnimator.ofPropertyValuesHolder(view, new android.animation.PropertyValuesHolder[]{android.animation.PropertyValuesHolder.ofFloat(android.view.View.TRANSLATION_X, new float[]{f5, f3}), android.animation.PropertyValuesHolder.ofFloat(android.view.View.TRANSLATION_Y, new float[]{f6, f4})});
        com.fossil.blesdk.obfuscated.C2808rh.C2809a aVar = new com.fossil.blesdk.obfuscated.C2808rh.C2809a(view, phVar2.f8383b, round, round2, translationX, translationY);
        ofPropertyValuesHolder.addListener(aVar);
        com.fossil.blesdk.obfuscated.C2964tg.m14194a(ofPropertyValuesHolder, aVar);
        ofPropertyValuesHolder.setInterpolator(timeInterpolator);
        return ofPropertyValuesHolder;
    }
}
