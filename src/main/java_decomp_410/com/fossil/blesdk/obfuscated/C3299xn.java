package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xn */
public class C3299xn implements android.content.ComponentCallbacks2, com.fossil.blesdk.obfuscated.C2835ru, com.fossil.blesdk.obfuscated.C3050un<com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable>> {

    @DexIgnore
    /* renamed from: q */
    public static /* final */ com.fossil.blesdk.obfuscated.C2842rv f11018q; // = ((com.fossil.blesdk.obfuscated.C2842rv) com.fossil.blesdk.obfuscated.C2842rv.m13424b((java.lang.Class<?>) android.graphics.Bitmap.class).mo13423O());

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2815rn f11019e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.Context f11020f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2770qu f11021g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3244wu f11022h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C3141vu f11023i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C3392yu f11024j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ java.lang.Runnable f11025k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ android.os.Handler f11026l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.fossil.blesdk.obfuscated.C2344lu f11027m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ java.util.concurrent.CopyOnWriteArrayList<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> f11028n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C2842rv f11029o;

    @DexIgnore
    /* renamed from: p */
    public boolean f11030p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xn$a */
    public class C3300a implements java.lang.Runnable {
        @DexIgnore
        public C3300a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C3299xn xnVar = com.fossil.blesdk.obfuscated.C3299xn.this;
            xnVar.f11021g.mo12479a(xnVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xn$b")
    /* renamed from: com.fossil.blesdk.obfuscated.xn$b */
    public class C3301b implements com.fossil.blesdk.obfuscated.C2344lu.C2345a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3244wu f11032a;

        @DexIgnore
        public C3301b(com.fossil.blesdk.obfuscated.C3244wu wuVar) {
            this.f11032a = wuVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13389a(boolean z) {
            if (z) {
                synchronized (com.fossil.blesdk.obfuscated.C3299xn.this) {
                    this.f11032a.mo17538d();
                }
            }
        }
    }

    /*
    static {
        com.fossil.blesdk.obfuscated.C2842rv rvVar = (com.fossil.blesdk.obfuscated.C2842rv) com.fossil.blesdk.obfuscated.C2842rv.m13424b((java.lang.Class<?>) com.fossil.blesdk.obfuscated.C3060ut.class).mo13423O();
        com.fossil.blesdk.obfuscated.C2842rv rvVar2 = (com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) com.fossil.blesdk.obfuscated.C2842rv.m13423b(com.fossil.blesdk.obfuscated.C2663pp.f8416b).mo13432a(com.bumptech.glide.Priority.LOW)).mo13444a(true);
    }
    */

    @DexIgnore
    public C3299xn(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C2770qu quVar, com.fossil.blesdk.obfuscated.C3141vu vuVar, android.content.Context context) {
        this(rnVar, quVar, vuVar, new com.fossil.blesdk.obfuscated.C3244wu(), rnVar.mo15646d(), context);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo17819a(com.fossil.blesdk.obfuscated.C2842rv rvVar) {
        this.f11029o = (com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) rvVar.clone()).mo13429a();
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized void mo14096b() {
        this.f11024j.mo14096b();
        for (com.fossil.blesdk.obfuscated.C1517bw<?> a : this.f11024j.mo18349f()) {
            mo17817a(a);
        }
        this.f11024j.mo18348e();
        this.f11022h.mo17533a();
        this.f11021g.mo12481b(this);
        this.f11021g.mo12481b(this.f11027m);
        this.f11026l.removeCallbacks(this.f11025k);
        this.f11019e.mo15644b(this);
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized void mo14097c() {
        mo17829k();
        this.f11024j.mo14097c();
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.Bitmap> mo17823e() {
        return mo17812a(android.graphics.Bitmap.class).mo13438a((com.fossil.blesdk.obfuscated.C2354lv) f11018q);
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17824f() {
        return mo17812a(android.graphics.drawable.Drawable.class);
    }

    @DexIgnore
    /* renamed from: g */
    public java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> mo17825g() {
        return this.f11028n;
    }

    @DexIgnore
    /* renamed from: h */
    public synchronized com.fossil.blesdk.obfuscated.C2842rv mo17826h() {
        return this.f11029o;
    }

    @DexIgnore
    /* renamed from: i */
    public synchronized void mo17827i() {
        this.f11022h.mo17535b();
    }

    @DexIgnore
    /* renamed from: j */
    public synchronized void mo17828j() {
        mo17827i();
        for (com.fossil.blesdk.obfuscated.C3299xn i : this.f11023i.mo14860a()) {
            i.mo17827i();
        }
    }

    @DexIgnore
    /* renamed from: k */
    public synchronized void mo17829k() {
        this.f11022h.mo17537c();
    }

    @DexIgnore
    /* renamed from: l */
    public synchronized void mo17830l() {
        this.f11022h.mo17539e();
    }

    @DexIgnore
    public void onConfigurationChanged(android.content.res.Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    public void onTrimMemory(int i) {
        if (i == 60 && this.f11030p) {
            mo17828j();
        }
    }

    @DexIgnore
    public synchronized java.lang.String toString() {
        return super.toString() + "{tracker=" + this.f11022h + ", treeNode=" + this.f11023i + "}";
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo14094a() {
        mo17830l();
        this.f11024j.mo14094a();
    }

    @DexIgnore
    public C3299xn(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C2770qu quVar, com.fossil.blesdk.obfuscated.C3141vu vuVar, com.fossil.blesdk.obfuscated.C3244wu wuVar, com.fossil.blesdk.obfuscated.C2436mu muVar, android.content.Context context) {
        this.f11024j = new com.fossil.blesdk.obfuscated.C3392yu();
        this.f11025k = new com.fossil.blesdk.obfuscated.C3299xn.C3300a();
        this.f11026l = new android.os.Handler(android.os.Looper.getMainLooper());
        this.f11019e = rnVar;
        this.f11021g = quVar;
        this.f11023i = vuVar;
        this.f11022h = wuVar;
        this.f11020f = context;
        this.f11027m = muVar.mo13714a(context.getApplicationContext(), new com.fossil.blesdk.obfuscated.C3299xn.C3301b(wuVar));
        if (com.fossil.blesdk.obfuscated.C3066uw.m14935c()) {
            this.f11026l.post(this.f11025k);
        } else {
            quVar.mo12479a(this);
        }
        quVar.mo12479a(this.f11027m);
        this.f11028n = new java.util.concurrent.CopyOnWriteArrayList<>(rnVar.mo15648f().mo16513b());
        mo17819a(rnVar.mo15648f().mo16514c());
        rnVar.mo15641a(this);
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo17822c(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        boolean b = mo17821b(bwVar);
        com.fossil.blesdk.obfuscated.C2604ov d = bwVar.mo9319d();
        if (!b && !this.f11019e.mo15642a(bwVar) && d != null) {
            bwVar.mo9314a((com.fossil.blesdk.obfuscated.C2604ov) null);
            d.clear();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17815a(java.lang.String str) {
        return mo17824f().mo17495a(str);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17811a(android.net.Uri uri) {
        return mo17824f().mo17490a(uri);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17813a(java.lang.Integer num) {
        return mo17824f().mo17493a(num);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17816a(byte[] bArr) {
        return mo17824f().mo17496a(bArr);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.drawable.Drawable> mo17814a(java.lang.Object obj) {
        return mo17824f().mo17494a(obj);
    }

    @DexIgnore
    /* renamed from: a */
    public <ResourceType> com.fossil.blesdk.obfuscated.C3230wn<ResourceType> mo17812a(java.lang.Class<ResourceType> cls) {
        return new com.fossil.blesdk.obfuscated.C3230wn<>(this.f11019e, this, cls, this.f11020f);
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized boolean mo17821b(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        com.fossil.blesdk.obfuscated.C2604ov d = bwVar.mo9319d();
        if (d == null) {
            return true;
        }
        if (!this.f11022h.mo17534a(d)) {
            return false;
        }
        this.f11024j.mo18347b(bwVar);
        bwVar.mo9314a((com.fossil.blesdk.obfuscated.C2604ov) null);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17817a(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        if (bwVar != null) {
            mo17822c(bwVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo17818a(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar, com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        this.f11024j.mo18346a(bwVar);
        this.f11022h.mo17536b(ovVar);
    }

    @DexIgnore
    /* renamed from: b */
    public <T> com.fossil.blesdk.obfuscated.C3371yn<?, T> mo17820b(java.lang.Class<T> cls) {
        return this.f11019e.mo15648f().mo16512a(cls);
    }
}
