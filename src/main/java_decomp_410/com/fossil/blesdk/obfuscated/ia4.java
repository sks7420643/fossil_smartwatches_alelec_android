package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.internal.util.ExceptionHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ia4 {
    @DexIgnore
    public static volatile e94<? super Throwable> a;
    @DexIgnore
    public static volatile f94<? super Runnable, ? extends Runnable> b;
    @DexIgnore
    public static volatile f94<? super k84, ? extends k84> c;
    @DexIgnore
    public static volatile f94<? super o84, ? extends o84> d;
    @DexIgnore
    public static volatile f94<? super l84, ? extends l84> e;
    @DexIgnore
    public static volatile f94<? super s84, ? extends s84> f;
    @DexIgnore
    public static volatile f94<? super h84, ? extends h84> g;
    @DexIgnore
    public static volatile c94<? super o84, ? super q84, ? extends q84> h;

    @DexIgnore
    public static boolean a(Throwable th) {
        if (!(th instanceof OnErrorNotImplementedException) && !(th instanceof MissingBackpressureException) && !(th instanceof IllegalStateException) && !(th instanceof NullPointerException) && !(th instanceof IllegalArgumentException) && !(th instanceof CompositeException)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static void b(Throwable th) {
        e94<? super Throwable> e94 = a;
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        } else if (!a(th)) {
            th = new UndeliverableException(th);
        }
        if (e94 != null) {
            try {
                e94.a(th);
                return;
            } catch (Throwable th2) {
                th2.printStackTrace();
                c(th2);
            }
        }
        th.printStackTrace();
        c(th);
    }

    @DexIgnore
    public static void c(Throwable th) {
        Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
    }

    @DexIgnore
    public static Runnable a(Runnable runnable) {
        f94<? super Runnable, ? extends Runnable> f94 = b;
        if (f94 == null) {
            return runnable;
        }
        a(f94, runnable);
        return runnable;
    }

    @DexIgnore
    public static <T> q84<? super T> a(o84<T> o84, q84<? super T> q84) {
        c94<? super o84, ? super q84, ? extends q84> c94 = h;
        return c94 != null ? (q84) a(c94, o84, q84) : q84;
    }

    @DexIgnore
    public static <T> l84<T> a(l84<T> l84) {
        f94<? super l84, ? extends l84> f94 = e;
        if (f94 == null) {
            return l84;
        }
        a(f94, l84);
        return l84;
    }

    @DexIgnore
    public static <T> k84<T> a(k84<T> k84) {
        f94<? super k84, ? extends k84> f94 = c;
        if (f94 == null) {
            return k84;
        }
        a(f94, k84);
        return k84;
    }

    @DexIgnore
    public static <T> o84<T> a(o84<T> o84) {
        f94<? super o84, ? extends o84> f94 = d;
        if (f94 == null) {
            return o84;
        }
        a(f94, o84);
        return o84;
    }

    @DexIgnore
    public static <T> s84<T> a(s84<T> s84) {
        f94<? super s84, ? extends s84> f94 = f;
        if (f94 == null) {
            return s84;
        }
        a(f94, s84);
        return s84;
    }

    @DexIgnore
    public static h84 a(h84 h84) {
        f94<? super h84, ? extends h84> f94 = g;
        if (f94 == null) {
            return h84;
        }
        a(f94, h84);
        return h84;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [R, T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <T, R> R a(f94<T, R> f94, T r1) {
        try {
            f94.apply(r1);
            return r1;
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    @DexIgnore
    public static <T, U, R> R a(c94<T, U, R> c94, T t, U u) {
        try {
            return c94.a(t, u);
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }
}
