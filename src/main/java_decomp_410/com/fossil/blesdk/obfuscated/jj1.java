package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ String h;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 i;

    @DexIgnore
    public jj1(dj1 dj1, AtomicReference atomicReference, String str, String str2, String str3) {
        this.i = dj1;
        this.e = atomicReference;
        this.f = str;
        this.g = str2;
        this.h = str3;
    }

    @DexIgnore
    public final void run() {
        this.i.a.m().a(this.e, this.f, this.g, this.h);
    }
}
