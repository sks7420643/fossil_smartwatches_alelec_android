package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f0 */
public class C1761f0 extends android.app.Dialog implements com.fossil.blesdk.obfuscated.C1591d0 {

    @DexIgnore
    /* renamed from: e */
    public androidx.appcompat.app.AppCompatDelegate f4976e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2709q8.C2710a f4977f; // = new com.fossil.blesdk.obfuscated.C1761f0.C1762a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.f0$a */
    public class C1762a implements com.fossil.blesdk.obfuscated.C2709q8.C2710a {
        @DexIgnore
        public C1762a() {
        }

        @DexIgnore
        public boolean superDispatchKeyEvent(android.view.KeyEvent keyEvent) {
            return com.fossil.blesdk.obfuscated.C1761f0.this.mo10669a(keyEvent);
        }
    }

    @DexIgnore
    public C1761f0(android.content.Context context, int i) {
        super(context, m6658a(context, i));
        mo10667a().mo291a((android.os.Bundle) null);
        mo10667a().mo296a();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10668a(int i) {
        return mo10667a().mo300b(i);
    }

    @DexIgnore
    public void addContentView(android.view.View view, android.view.ViewGroup.LayoutParams layoutParams) {
        mo10667a().mo293a(view, layoutParams);
    }

    @DexIgnore
    public boolean dispatchKeyEvent(android.view.KeyEvent keyEvent) {
        return com.fossil.blesdk.obfuscated.C2709q8.m12639a(this.f4977f, getWindow().getDecorView(), this, keyEvent);
    }

    @DexIgnore
    public <T extends android.view.View> T findViewById(int i) {
        return mo10667a().mo288a(i);
    }

    @DexIgnore
    public void invalidateOptionsMenu() {
        mo10667a().mo306f();
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        mo10667a().mo305e();
        super.onCreate(bundle);
        mo10667a().mo291a(bundle);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        mo10667a().mo310j();
    }

    @DexIgnore
    public void onSupportActionModeFinished(androidx.appcompat.view.ActionMode actionMode) {
    }

    @DexIgnore
    public void onSupportActionModeStarted(androidx.appcompat.view.ActionMode actionMode) {
    }

    @DexIgnore
    public androidx.appcompat.view.ActionMode onWindowStartingSupportActionMode(androidx.appcompat.view.ActionMode.Callback callback) {
        return null;
    }

    @DexIgnore
    public void setContentView(int i) {
        mo10667a().mo302c(i);
    }

    @DexIgnore
    public void setTitle(java.lang.CharSequence charSequence) {
        super.setTitle(charSequence);
        mo10667a().mo295a(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.appcompat.app.AppCompatDelegate mo10667a() {
        if (this.f4976e == null) {
            this.f4976e = androidx.appcompat.app.AppCompatDelegate.m217a((android.app.Dialog) this, (com.fossil.blesdk.obfuscated.C1591d0) this);
        }
        return this.f4976e;
    }

    @DexIgnore
    public void setContentView(android.view.View view) {
        mo10667a().mo292a(view);
    }

    @DexIgnore
    public void setContentView(android.view.View view, android.view.ViewGroup.LayoutParams layoutParams) {
        mo10667a().mo299b(view, layoutParams);
    }

    @DexIgnore
    public void setTitle(int i) {
        super.setTitle(i);
        mo10667a().mo295a((java.lang.CharSequence) getContext().getString(i));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m6658a(android.content.Context context, int i) {
        if (i != 0) {
            return i;
        }
        android.util.TypedValue typedValue = new android.util.TypedValue();
        context.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10669a(android.view.KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
}
