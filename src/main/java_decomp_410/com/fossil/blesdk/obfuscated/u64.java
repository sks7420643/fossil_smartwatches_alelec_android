package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class u64 extends v64 {
    @DexIgnore
    public u64(Context context, File file, String str, String str2) throws IOException {
        super(context, file, str, str2);
    }

    @DexIgnore
    public OutputStream a(File file) throws IOException {
        return new GZIPOutputStream(new FileOutputStream(file));
    }
}
