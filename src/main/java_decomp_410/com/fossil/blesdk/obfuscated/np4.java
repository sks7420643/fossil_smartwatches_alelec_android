package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class np4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public np4(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a + ": " + this.b;
    }
}
