package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ef */
public class C1725ef {

    @DexIgnore
    /* renamed from: a */
    public long f4780a;

    @DexIgnore
    /* renamed from: b */
    public boolean f4781b; // = false;

    @DexIgnore
    /* renamed from: c */
    public androidx.renderscript.RenderScript f4782c;

    @DexIgnore
    public C1725ef(long j, androidx.renderscript.RenderScript renderScript) {
        renderScript.mo3204j();
        this.f4782c = renderScript;
        this.f4780a = j;
    }

    @DexIgnore
    /* renamed from: a */
    public long mo10463a(androidx.renderscript.RenderScript renderScript) {
        this.f4782c.mo3204j();
        if (this.f4781b) {
            throw new androidx.renderscript.RSInvalidStateException("using a destroyed object.");
        } else if (this.f4780a == 0) {
            throw new androidx.renderscript.RSRuntimeException("Internal error: Object id 0.");
        } else if (renderScript == null || renderScript == this.f4782c) {
            return this.f4780a;
        } else {
            throw new androidx.renderscript.RSInvalidStateException("using object with mismatched context.");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo3165b() {
        if (!this.f4781b) {
            mo10466d();
            return;
        }
        throw new androidx.renderscript.RSInvalidStateException("Object already destroyed.");
    }

    @DexIgnore
    /* renamed from: c */
    public android.renderscript.BaseObj mo10465c() {
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo10466d() {
        boolean z;
        synchronized (this) {
            z = true;
            if (!this.f4781b) {
                this.f4781b = true;
            } else {
                z = false;
            }
        }
        if (z) {
            java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock readLock = this.f4782c.f1536k.readLock();
            readLock.lock();
            if (this.f4782c.mo3196c()) {
                this.f4782c.mo3194b(this.f4780a);
            }
            readLock.unlock();
            this.f4782c = null;
            this.f4780a = 0;
        }
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass() && this.f4780a == ((com.fossil.blesdk.obfuscated.C1725ef) obj).f4780a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void finalize() throws java.lang.Throwable {
        mo10466d();
        super.finalize();
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f4780a;
        return (int) ((j >> 32) ^ (268435455 & j));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10464a() {
        if (this.f4780a == 0 && mo10465c() == null) {
            throw new androidx.renderscript.RSIllegalArgumentException("Invalid object.");
        }
    }
}
