package com.fossil.blesdk.obfuscated;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ad {
    @DexIgnore
    public bd a;

    @DexIgnore
    public ad(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a = new cd(str, i, i2);
        } else {
            this.a = new dd(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ad)) {
            return false;
        }
        return this.a.equals(((ad) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
