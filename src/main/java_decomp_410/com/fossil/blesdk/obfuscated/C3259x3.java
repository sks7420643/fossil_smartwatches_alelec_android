package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x3 */
public final class C3259x3 {
    @DexIgnore
    public static /* final */ int Base_CardView; // = 2131886225;
    @DexIgnore
    public static /* final */ int CardView; // = 2131886423;
    @DexIgnore
    public static /* final */ int CardView_Dark; // = 2131886424;
    @DexIgnore
    public static /* final */ int CardView_Light; // = 2131886425;
}
