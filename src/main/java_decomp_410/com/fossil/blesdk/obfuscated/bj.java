package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bj {
    @DexIgnore
    public static /* final */ String a; // = dj.a("InputMerger");

    @DexIgnore
    public static bj a(String str) {
        try {
            return (bj) Class.forName(str).newInstance();
        } catch (Exception e) {
            dj a2 = dj.a();
            String str2 = a;
            a2.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract aj a(List<aj> list);
}
