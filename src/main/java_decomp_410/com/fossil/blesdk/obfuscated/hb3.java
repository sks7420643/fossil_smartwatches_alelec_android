package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hb3 implements Factory<GoalTrackingOverviewDayPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewDayPresenter a(fb3 fb3, en2 en2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewDayPresenter(fb3, en2, goalTrackingRepository);
    }
}
