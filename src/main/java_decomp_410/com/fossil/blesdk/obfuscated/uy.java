package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import com.crashlytics.android.core.CrashlyticsController;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@b64({yy.class})
public class uy extends v44<Void> {
    @DexIgnore
    public /* final */ long k;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, String> l;
    @DexIgnore
    public vy m;
    @DexIgnore
    public vy n;
    @DexIgnore
    public wy o;
    @DexIgnore
    public CrashlyticsController p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public float t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ rz v;
    @DexIgnore
    public z64 w;
    @DexIgnore
    public ty x;
    @DexIgnore
    public yy y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends d64<Void> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Priority getPriority() {
            return Priority.IMMEDIATE;
        }

        @DexIgnore
        public Void call() throws Exception {
            return uy.this.k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public Void call() throws Exception {
            uy.this.m.a();
            q44.g().d("CrashlyticsCore", "Initialization marker file created.");
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Boolean> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public Boolean call() throws Exception {
            try {
                boolean d = uy.this.m.d();
                y44 g = q44.g();
                g.d("CrashlyticsCore", "Initialization marker file removed: " + d);
                return Boolean.valueOf(d);
            } catch (Exception e2) {
                q44.g().e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", e2);
                return false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Callable<Boolean> {
        @DexIgnore
        public /* final */ vy e;

        @DexIgnore
        public d(vy vyVar) {
            this.e = vyVar;
        }

        @DexIgnore
        public Boolean call() throws Exception {
            if (!this.e.c()) {
                return Boolean.FALSE;
            }
            q44.g().d("CrashlyticsCore", "Found previous crash marker.");
            this.e.d();
            return Boolean.TRUE;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements wy {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public /* synthetic */ e(a aVar) {
            this();
        }
    }

    @DexIgnore
    public uy() {
        this(1.0f, (wy) null, (rz) null, false);
    }

    @DexIgnore
    public static uy G() {
        return (uy) q44.a(uy.class);
    }

    @DexIgnore
    public static boolean d(String str) {
        uy G = G();
        if (G != null && G.p != null) {
            return true;
        }
        y44 g = q44.g();
        g.e("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + str, (Throwable) null);
        return false;
    }

    @DexIgnore
    public static String e(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @DexIgnore
    public xy A() {
        yy yyVar = this.y;
        if (yyVar != null) {
            return yyVar.a();
        }
        return null;
    }

    @DexIgnore
    public String B() {
        if (o().a()) {
            return this.r;
        }
        return null;
    }

    @DexIgnore
    public String C() {
        if (o().a()) {
            return this.q;
        }
        return null;
    }

    @DexIgnore
    public String D() {
        if (o().a()) {
            return this.s;
        }
        return null;
    }

    @DexIgnore
    public void E() {
        this.x.a(new c());
    }

    @DexIgnore
    public void F() {
        this.x.b(new b());
    }

    @DexIgnore
    public void b(int i, String str, String str2) {
        a(i, str, str2);
        q44.g().a(i, "" + str, "" + str2, true);
    }

    @DexIgnore
    public void c(String str) {
        if (!this.u && d("prior to setting user data.")) {
            this.q = e(str);
            this.p.a(this.q, this.s, this.r);
        }
    }

    @DexIgnore
    public String p() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    @DexIgnore
    public String r() {
        return "2.7.0.33";
    }

    @DexIgnore
    public boolean u() {
        return a(super.l());
    }

    @DexIgnore
    public final void v() {
        if (Boolean.TRUE.equals((Boolean) this.x.b(new d(this.n)))) {
            try {
                this.o.a();
            } catch (Exception e2) {
                q44.g().e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", e2);
            }
        }
    }

    @DexIgnore
    public void w() {
        this.n.a();
    }

    @DexIgnore
    public boolean x() {
        return this.m.c();
    }

    @DexIgnore
    public final void y() {
        a aVar = new a();
        for (i64 a2 : m()) {
            aVar.a(a2);
        }
        Future submit = n().b().submit(aVar);
        q44.g().d("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            q44.g().e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            q44.g().e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            q44.g().e("CrashlyticsCore", "Crashlytics timed out during initialization.", e4);
        }
    }

    @DexIgnore
    public Map<String, String> z() {
        return Collections.unmodifiableMap(this.l);
    }

    @DexIgnore
    public uy(float f, wy wyVar, rz rzVar, boolean z) {
        this(f, wyVar, rzVar, z, q54.a("Crashlytics Exception Handler"));
    }

    @DexIgnore
    public boolean a(Context context) {
        Context context2 = context;
        if (!o54.a(context).a()) {
            q44.g().d("CrashlyticsCore", "Crashlytics is disabled, because data collection is disabled by Firebase.");
            this.u = true;
        }
        if (this.u) {
            return false;
        }
        String d2 = new k54().d(context2);
        if (d2 == null) {
            return false;
        }
        String n2 = CommonUtils.n(context);
        if (a(n2, CommonUtils.a(context2, "com.crashlytics.RequireBuildId", true))) {
            try {
                y44 g = q44.g();
                g.i("CrashlyticsCore", "Initializing Crashlytics Core " + r());
                f74 f74 = new f74(this);
                this.n = new vy("crash_marker", f74);
                this.m = new vy("initialization_marker", f74);
                sz a2 = sz.a(new h74(l(), "com.crashlytics.android.core.CrashlyticsCore"), this);
                zy zyVar = this.v != null ? new zy(this.v) : null;
                this.w = new y64(q44.g());
                this.w.a(zyVar);
                IdManager o2 = o();
                ly a3 = ly.a(context2, o2, d2, n2);
                CrashlyticsController crashlyticsController = r1;
                CrashlyticsController crashlyticsController2 = new CrashlyticsController(this, this.x, this.w, o2, a2, f74, a3, new yz(context2, new kz(context2, a3.d)), new dz(this), kx.b(context));
                this.p = crashlyticsController;
                boolean x2 = x();
                v();
                this.p.a(Thread.getDefaultUncaughtExceptionHandler(), new t54().e(context2));
                if (!x2 || !CommonUtils.b(context)) {
                    q44.g().d("CrashlyticsCore", "Exception handling initialization successful");
                    return true;
                }
                q44.g().d("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
                y();
                return false;
            } catch (Exception e2) {
                q44.g().e("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", e2);
                this.p = null;
                return false;
            }
        } else {
            throw new UnmetDependencyException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    public Void k() {
        F();
        this.p.a();
        try {
            this.p.p();
            b84 a2 = z74.d().a();
            if (a2 == null) {
                q44.g().w("CrashlyticsCore", "Received null settings, skipping report submission!");
                E();
                return null;
            }
            this.p.a(a2);
            if (!a2.d.b) {
                q44.g().d("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
                E();
                return null;
            } else if (!o54.a(l()).a()) {
                q44.g().d("CrashlyticsCore", "Automatic collection of crash reports disabled by Firebase settings.");
                E();
                return null;
            } else {
                xy A = A();
                if (A != null && !this.p.a(A)) {
                    q44.g().d("CrashlyticsCore", "Could not finalize previous NDK sessions.");
                }
                if (!this.p.b(a2.b)) {
                    q44.g().d("CrashlyticsCore", "Could not finalize previous sessions.");
                }
                this.p.a(this.t, a2);
                E();
                return null;
            }
        } catch (Exception e2) {
            q44.g().e("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", e2);
        } catch (Throwable th) {
            E();
            throw th;
        }
    }

    @DexIgnore
    public void b(String str) {
        if (!this.u && d("prior to setting user data.")) {
            this.r = e(str);
            this.p.a(this.q, this.s, this.r);
        }
    }

    @DexIgnore
    public uy(float f, wy wyVar, rz rzVar, boolean z, ExecutorService executorService) {
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = f;
        this.o = wyVar == null ? new e((a) null) : wyVar;
        this.v = rzVar;
        this.u = z;
        this.x = new ty(executorService);
        this.l = new ConcurrentHashMap<>();
        this.k = System.currentTimeMillis();
    }

    @DexIgnore
    public static String c(int i, String str, String str2) {
        return CommonUtils.a(i) + ZendeskConfig.SLASH + str + " " + str2;
    }

    @DexIgnore
    public void a(String str) {
        a(3, "CrashlyticsCore", str);
    }

    @DexIgnore
    public final void a(int i, String str, String str2) {
        if (!this.u && d("prior to logging messages.")) {
            this.p.a(System.currentTimeMillis() - this.k, c(i, str, str2));
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        String str3;
        if (this.u || !d("prior to setting keys.")) {
            return;
        }
        if (str == null) {
            Context l2 = l();
            if (l2 == null || !CommonUtils.j(l2)) {
                q44.g().e("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", (Throwable) null);
                return;
            }
            throw new IllegalArgumentException("Custom attribute key must not be null.");
        }
        String e2 = e(str);
        if (this.l.size() < 64 || this.l.containsKey(e2)) {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = e(str2);
            }
            this.l.put(e2, str3);
            this.p.a((Map<String, String>) this.l);
            return;
        }
        q44.g().d("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
    }

    @DexIgnore
    public static boolean a(String str, boolean z) {
        if (!z) {
            q44.g().d("CrashlyticsCore", "Configured not to require a build ID.");
            return true;
        } else if (!CommonUtils.b(str)) {
            return true;
        } else {
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", ".     |  | ");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".   \\ |  | /");
            Log.e("CrashlyticsCore", ".    \\    /");
            Log.e("CrashlyticsCore", ".     \\  /");
            Log.e("CrashlyticsCore", ".      \\/");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("CrashlyticsCore", ".      /\\");
            Log.e("CrashlyticsCore", ".     /  \\");
            Log.e("CrashlyticsCore", ".    /    \\");
            Log.e("CrashlyticsCore", ".   / |  | \\");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", ".     |  |");
            Log.e("CrashlyticsCore", CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }
}
