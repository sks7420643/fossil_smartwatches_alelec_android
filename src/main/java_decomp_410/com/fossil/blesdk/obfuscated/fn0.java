package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class fn0 implements Callable {
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ gn0 g;

    @DexIgnore
    public fn0(boolean z, String str, gn0 gn0) {
        this.e = z;
        this.f = str;
        this.g = gn0;
    }

    @DexIgnore
    public final Object call() {
        return en0.a(this.e, this.f, this.g);
    }
}
