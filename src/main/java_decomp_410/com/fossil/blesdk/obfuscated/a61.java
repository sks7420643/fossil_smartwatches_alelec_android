package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a61 extends vb1<a61> {
    @DexIgnore
    public static volatile a61[] g;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public g61 d; // = null;
    @DexIgnore
    public g61 e; // = null;
    @DexIgnore
    public Boolean f; // = null;

    @DexIgnore
    public a61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static a61[] e() {
        if (g == null) {
            synchronized (zb1.b) {
                if (g == null) {
                    g = new a61[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        g61 g61 = this.d;
        if (g61 != null) {
            ub1.a(2, (ac1) g61);
        }
        g61 g612 = this.e;
        if (g612 != null) {
            ub1.a(3, (ac1) g612);
        }
        Boolean bool = this.f;
        if (bool != null) {
            ub1.a(4, bool.booleanValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof a61)) {
            return false;
        }
        a61 a61 = (a61) obj;
        Integer num = this.c;
        if (num == null) {
            if (a61.c != null) {
                return false;
            }
        } else if (!num.equals(a61.c)) {
            return false;
        }
        g61 g61 = this.d;
        if (g61 == null) {
            if (a61.d != null) {
                return false;
            }
        } else if (!g61.equals(a61.d)) {
            return false;
        }
        g61 g612 = this.e;
        if (g612 == null) {
            if (a61.e != null) {
                return false;
            }
        } else if (!g612.equals(a61.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (a61.f != null) {
                return false;
            }
        } else if (!bool.equals(a61.f)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(a61.b);
        }
        xb1 xb12 = a61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i;
        int i2;
        int hashCode = (a61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i3 = 0;
        int hashCode2 = hashCode + (num == null ? 0 : num.hashCode());
        g61 g61 = this.d;
        int i4 = hashCode2 * 31;
        if (g61 == null) {
            i = 0;
        } else {
            i = g61.hashCode();
        }
        int i5 = i4 + i;
        g61 g612 = this.e;
        int i6 = i5 * 31;
        if (g612 == null) {
            i2 = 0;
        } else {
            i2 = g612.hashCode();
        }
        int i7 = (i6 + i2) * 31;
        Boolean bool = this.f;
        int hashCode3 = (i7 + (bool == null ? 0 : bool.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode3 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        g61 g61 = this.d;
        if (g61 != null) {
            a += ub1.b(2, (ac1) g61);
        }
        g61 g612 = this.e;
        if (g612 != null) {
            a += ub1.b(3, (ac1) g612);
        }
        Boolean bool = this.f;
        if (bool == null) {
            return a;
        }
        bool.booleanValue();
        return a + ub1.c(4) + 1;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 18) {
                if (this.d == null) {
                    this.d = new g61();
                }
                tb1.a((ac1) this.d);
            } else if (c2 == 26) {
                if (this.e == null) {
                    this.e = new g61();
                }
                tb1.a((ac1) this.e);
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(tb1.d());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
