package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kv0 implements rv0 {
    @DexIgnore
    public final boolean zza(Class<?> cls) {
        return false;
    }

    @DexIgnore
    public final qv0 zzb(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
