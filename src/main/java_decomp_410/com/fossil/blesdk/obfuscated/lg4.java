package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lg4 extends xh4 {
    @DexIgnore
    public static /* final */ int e;
    @DexIgnore
    public static boolean f;
    @DexIgnore
    public static /* final */ lg4 g; // = new lg4();
    @DexIgnore
    public static volatile Executor pool;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicInteger a;

        @DexIgnore
        public a(AtomicInteger atomicInteger) {
            this.a = atomicInteger;
        }

        @DexIgnore
        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "CommonPool-worker-" + this.a.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void run() {
        }
    }

    /*
    static {
        String str;
        int i;
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable unused) {
            str = null;
        }
        if (str != null) {
            Integer b2 = pf4.b(str);
            if (b2 == null || b2.intValue() < 1) {
                throw new IllegalStateException(("Expected positive number in kotlinx.coroutines.default.parallelism, but has " + str).toString());
            }
            i = b2.intValue();
        } else {
            i = -1;
        }
        e = i;
    }
    */

    @DexIgnore
    public final ExecutorService C() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(F(), new a(new AtomicInteger()));
        kd4.a((Object) newFixedThreadPool, "Executors.newFixedThread\u2026Daemon = true }\n        }");
        return newFixedThreadPool;
    }

    @DexIgnore
    public final ExecutorService D() {
        Class<?> cls;
        ExecutorService executorService;
        ExecutorService executorService2;
        if (System.getSecurityManager() != null) {
            return C();
        }
        try {
            cls = Class.forName("java.util.concurrent.ForkJoinPool");
        } catch (Throwable unused) {
            cls = null;
        }
        if (cls == null) {
            return C();
        }
        if (!f && e < 0) {
            try {
                Method method = cls.getMethod("commonPool", new Class[0]);
                Object invoke = method != null ? method.invoke((Object) null, new Object[0]) : null;
                if (!(invoke instanceof ExecutorService)) {
                    invoke = null;
                }
                executorService2 = (ExecutorService) invoke;
            } catch (Throwable unused2) {
                executorService2 = null;
            }
            if (executorService2 != null) {
                if (!g.a(cls, executorService2)) {
                    executorService2 = null;
                }
                if (executorService2 != null) {
                    return executorService2;
                }
            }
        }
        try {
            Object newInstance = cls.getConstructor(new Class[]{Integer.TYPE}).newInstance(new Object[]{Integer.valueOf(g.F())});
            if (!(newInstance instanceof ExecutorService)) {
                newInstance = null;
            }
            executorService = (ExecutorService) newInstance;
        } catch (Throwable unused3) {
            executorService = null;
        }
        if (executorService != null) {
            return executorService;
        }
        return C();
    }

    @DexIgnore
    public final synchronized Executor E() {
        Executor executor;
        executor = pool;
        if (executor == null) {
            executor = D();
            pool = executor;
        }
        return executor;
    }

    @DexIgnore
    public final int F() {
        Integer valueOf = Integer.valueOf(e);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return ee4.a(Runtime.getRuntime().availableProcessors() - 1, 1);
    }

    @DexIgnore
    public final boolean a(Class<?> cls, ExecutorService executorService) {
        Integer num;
        kd4.b(cls, "fjpClass");
        kd4.b(executorService, "executor");
        executorService.submit(b.e);
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (!(invoke instanceof Integer)) {
                invoke = null;
            }
            num = (Integer) invoke;
        } catch (Throwable unused) {
            num = null;
        }
        return num != null && num.intValue() >= 1;
    }

    @DexIgnore
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }

    @DexIgnore
    public String toString() {
        return "CommonPool";
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        Runnable runnable2;
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        try {
            Executor executor = pool;
            if (executor == null) {
                executor = E();
            }
            cj4 a2 = dj4.a();
            if (a2 != null) {
                runnable2 = a2.a(runnable);
                if (runnable2 != null) {
                    executor.execute(runnable2);
                }
            }
            runnable2 = runnable;
            executor.execute(runnable2);
        } catch (RejectedExecutionException unused) {
            cj4 a3 = dj4.a();
            if (a3 != null) {
                a3.c();
            }
            eh4.k.a(runnable);
        }
    }
}
