package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.nm4;
import com.fossil.blesdk.obfuscated.yl4;
import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lm4 implements Interceptor {
    @DexIgnore
    public /* final */ qm4 a;

    @DexIgnore
    public lm4(qm4 qm4) {
        this.a = qm4;
    }

    @DexIgnore
    public static Response a(Response response) {
        if (response == null || response.y() == null) {
            return response;
        }
        Response.a H = response.H();
        H.a((em4) null);
        return H.a();
    }

    @DexIgnore
    public static boolean b(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        qm4 qm4 = this.a;
        Response b = qm4 != null ? qm4.b(chain.n()) : null;
        nm4 c = new nm4.a(System.currentTimeMillis(), chain.n(), b).c();
        dm4 dm4 = c.a;
        Response response = c.b;
        qm4 qm42 = this.a;
        if (qm42 != null) {
            qm42.a(c);
        }
        if (b != null && response == null) {
            jm4.a((Closeable) b.y());
        }
        if (dm4 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.a(chain.n());
            aVar.a(Protocol.HTTP_1_1);
            aVar.a(504);
            aVar.a("Unsatisfiable Request (only-if-cached)");
            aVar.a(jm4.c);
            aVar.b(-1);
            aVar.a(System.currentTimeMillis());
            return aVar.a();
        } else if (dm4 == null) {
            Response.a H = response.H();
            H.a(a(response));
            return H.a();
        } else {
            try {
                Response a2 = chain.a(dm4);
                if (a2 == null && b != null) {
                }
                if (response != null) {
                    if (a2.B() == 304) {
                        Response.a H2 = response.H();
                        H2.a(a(response.D(), a2.D()));
                        H2.b(a2.M());
                        H2.a(a2.K());
                        H2.a(a(response));
                        H2.c(a(a2));
                        Response a3 = H2.a();
                        a2.y().close();
                        this.a.a();
                        this.a.a(response, a3);
                        return a3;
                    }
                    jm4.a((Closeable) response.y());
                }
                Response.a H3 = a2.H();
                H3.a(a(response));
                H3.c(a(a2));
                Response a4 = H3.a();
                if (this.a != null) {
                    if (bn4.b(a4) && nm4.a(a4, dm4)) {
                        return a(this.a.a(a4), a4);
                    }
                    if (cn4.a(dm4.e())) {
                        try {
                            this.a.a(dm4);
                        } catch (IOException unused) {
                        }
                    }
                }
                return a4;
            } finally {
                if (b != null) {
                    jm4.a((Closeable) b.y());
                }
            }
        }
    }

    @DexIgnore
    public final Response a(mm4 mm4, Response response) throws IOException {
        if (mm4 == null) {
            return response;
        }
        xo4 a2 = mm4.a();
        if (a2 == null) {
            return response;
        }
        a aVar = new a(this, response.y().E(), mm4, so4.a(a2));
        String e = response.e(GraphRequest.CONTENT_TYPE_HEADER);
        long C = response.y().C();
        Response.a H = response.H();
        H.a((em4) new en4(e, C, so4.a((yo4) aVar)));
        return H.a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements yo4 {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ lo4 f;
        @DexIgnore
        public /* final */ /* synthetic */ mm4 g;
        @DexIgnore
        public /* final */ /* synthetic */ ko4 h;

        @DexIgnore
        public a(lm4 lm4, lo4 lo4, mm4 mm4, ko4 ko4) {
            this.f = lo4;
            this.g = mm4;
            this.h = ko4;
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            try {
                long b = this.f.b(jo4, j);
                if (b == -1) {
                    if (!this.e) {
                        this.e = true;
                        this.h.close();
                    }
                    return -1;
                }
                jo4.a(this.h.a(), jo4.B() - b, b);
                this.h.d();
                return b;
            } catch (IOException e2) {
                if (!this.e) {
                    this.e = true;
                    this.g.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.e && !jm4.a((yo4) this, 100, TimeUnit.MILLISECONDS)) {
                this.e = true;
                this.g.abort();
            }
            this.f.close();
        }

        @DexIgnore
        public zo4 b() {
            return this.f.b();
        }
    }

    @DexIgnore
    public static yl4 a(yl4 yl4, yl4 yl42) {
        yl4.a aVar = new yl4.a();
        int b = yl4.b();
        for (int i = 0; i < b; i++) {
            String a2 = yl4.a(i);
            String b2 = yl4.b(i);
            if ((!"Warning".equalsIgnoreCase(a2) || !b2.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_YES)) && (a(a2) || !b(a2) || yl42.a(a2) == null)) {
                hm4.a.a(aVar, a2, b2);
            }
        }
        int b3 = yl42.b();
        for (int i2 = 0; i2 < b3; i2++) {
            String a3 = yl42.a(i2);
            if (!a(a3) && b(a3)) {
                hm4.a.a(aVar, a3, yl42.b(i2));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return "Content-Length".equalsIgnoreCase(str) || GraphRequest.CONTENT_ENCODING_HEADER.equalsIgnoreCase(str) || GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(str);
    }
}
