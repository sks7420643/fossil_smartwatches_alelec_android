package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sz2 {
    @DexIgnore
    public /* final */ qz2 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public sz2(qz2 qz2, LoaderManager loaderManager) {
        kd4.b(qz2, "mView");
        kd4.b(loaderManager, "mLoaderManager");
        this.a = qz2;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final qz2 b() {
        return this.a;
    }
}
