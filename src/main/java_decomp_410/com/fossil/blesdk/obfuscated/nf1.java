package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nf1 implements Parcelable.Creator<CameraPosition> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        LatLng latLng = null;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                latLng = (LatLng) SafeParcelReader.a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 3) {
                f = SafeParcelReader.n(parcel, a);
            } else if (a2 == 4) {
                f2 = SafeParcelReader.n(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                f3 = SafeParcelReader.n(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new CameraPosition(latLng, f, f2, f3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new CameraPosition[i];
    }
}
