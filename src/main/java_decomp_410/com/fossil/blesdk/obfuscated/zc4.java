package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zc4<T> implements Iterator<T>, rd4 {
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ T[] f;

    @DexIgnore
    public zc4(T[] tArr) {
        kd4.b(tArr, "array");
        this.f = tArr;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.e < this.f.length;
    }

    @DexIgnore
    public T next() {
        try {
            T[] tArr = this.f;
            int i = this.e;
            this.e = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e2) {
            this.e--;
            throw new NoSuchElementException(e2.getMessage());
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
