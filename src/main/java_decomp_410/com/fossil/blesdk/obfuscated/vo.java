package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vo extends FilterInputStream {
    @DexIgnore
    public static /* final */ byte[] g; // = {-1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, DateTimeFieldType.MINUTE_OF_DAY, 0, 2, 0, 0, 0, 1, 0};
    @DexIgnore
    public static /* final */ int h; // = g.length;
    @DexIgnore
    public static /* final */ int i; // = (h + 2);
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public int f;

    @DexIgnore
    public vo(InputStream inputStream, int i2) {
        super(inputStream);
        if (i2 < -1 || i2 > 8) {
            throw new IllegalArgumentException("Cannot add invalid orientation: " + i2);
        }
        this.e = (byte) i2;
    }

    @DexIgnore
    public void mark(int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean markSupported() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    public int read() throws IOException {
        int i2;
        int i3 = this.f;
        if (i3 >= 2) {
            int i4 = i;
            if (i3 <= i4) {
                if (i3 == i4) {
                    i2 = this.e;
                } else {
                    i2 = g[i3 - 2] & FileType.MASKED_INDEX;
                }
                if (i2 != -1) {
                    this.f++;
                }
                return i2;
            }
        }
        i2 = super.read();
        if (i2 != -1) {
        }
        return i2;
    }

    @DexIgnore
    public void reset() throws IOException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public long skip(long j) throws IOException {
        long skip = super.skip(j);
        if (skip > 0) {
            this.f = (int) (((long) this.f) + skip);
        }
        return skip;
    }

    @DexIgnore
    public int read(byte[] bArr, int i2, int i3) throws IOException {
        int i4;
        int i5 = this.f;
        int i6 = i;
        if (i5 > i6) {
            i4 = super.read(bArr, i2, i3);
        } else if (i5 == i6) {
            bArr[i2] = this.e;
            i4 = 1;
        } else if (i5 < 2) {
            i4 = super.read(bArr, i2, 2 - i5);
        } else {
            int min = Math.min(i6 - i5, i3);
            System.arraycopy(g, this.f - 2, bArr, i2, min);
            i4 = min;
        }
        if (i4 > 0) {
            this.f += i4;
        }
        return i4;
    }
}
