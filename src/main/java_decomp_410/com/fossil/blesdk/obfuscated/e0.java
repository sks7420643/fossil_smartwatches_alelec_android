package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle$Delegate;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.AppCompatViewInflater;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ContentFrameLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h1;
import com.fossil.blesdk.obfuscated.m2;
import com.fossil.blesdk.obfuscated.p1;
import com.fossil.blesdk.obfuscated.q8;
import com.fossil.blesdk.obfuscated.w0;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.lang.Thread;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e0 extends AppCompatDelegate implements h1.a, LayoutInflater.Factory2 {
    @DexIgnore
    public static /* final */ boolean W; // = (Build.VERSION.SDK_INT < 21);
    @DexIgnore
    public static /* final */ int[] X; // = {16842836};
    @DexIgnore
    public static boolean Y; // = true;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public n[] I;
    @DexIgnore
    public n J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public int M; // = -100;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public l O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public int Q;
    @DexIgnore
    public /* final */ Runnable R; // = new b();
    @DexIgnore
    public boolean S;
    @DexIgnore
    public Rect T;
    @DexIgnore
    public Rect U;
    @DexIgnore
    public AppCompatViewInflater V;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ Window g;
    @DexIgnore
    public /* final */ Window.Callback h;
    @DexIgnore
    public /* final */ Window.Callback i;
    @DexIgnore
    public /* final */ d0 j;
    @DexIgnore
    public ActionBar k;
    @DexIgnore
    public MenuInflater l;
    @DexIgnore
    public CharSequence m;
    @DexIgnore
    public i2 n;
    @DexIgnore
    public i o;
    @DexIgnore
    public o p;
    @DexIgnore
    public ActionMode q;
    @DexIgnore
    public ActionBarContextView r;
    @DexIgnore
    public PopupWindow s;
    @DexIgnore
    public Runnable t;
    @DexIgnore
    public j9 u; // = null;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public ViewGroup x;
    @DexIgnore
    public TextView y;
    @DexIgnore
    public View z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ Thread.UncaughtExceptionHandler a;

        @DexIgnore
        public a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.a = uncaughtExceptionHandler;
        }

        @DexIgnore
        public final boolean a(Throwable th) {
            if (!(th instanceof Resources.NotFoundException)) {
                return false;
            }
            String message = th.getMessage();
            if (message == null) {
                return false;
            }
            if (message.contains(ResourceManager.DRAWABLE) || message.contains("Drawable")) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void uncaughtException(Thread thread, Throwable th) {
            if (a(th)) {
                Resources.NotFoundException notFoundException = new Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.a.uncaughtException(thread, notFoundException);
                return;
            }
            this.a.uncaughtException(thread, th);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            e0 e0Var = e0.this;
            if ((e0Var.Q & 1) != 0) {
                e0Var.e(0);
            }
            e0 e0Var2 = e0.this;
            if ((e0Var2.Q & 4096) != 0) {
                e0Var2.e(108);
            }
            e0 e0Var3 = e0.this;
            e0Var3.P = false;
            e0Var3.Q = 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements b9 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public n9 a(View view, n9 n9Var) {
            int e = n9Var.e();
            int l = e0.this.l(e);
            if (e != l) {
                n9Var = n9Var.a(n9Var.c(), l, n9Var.d(), n9Var.b());
            }
            return f9.b(view, n9Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements m2.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(Rect rect) {
            rect.top = e0.this.l(rect.top);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements ContentFrameLayout.a {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void onDetachedFromWindow() {
            e0.this.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends l9 {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void b(View view) {
                e0.this.r.setAlpha(1.0f);
                e0.this.u.a((k9) null);
                e0.this.u = null;
            }

            @DexIgnore
            public void c(View view) {
                e0.this.r.setVisibility(0);
            }
        }

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void run() {
            e0 e0Var = e0.this;
            e0Var.s.showAtLocation(e0Var.r, 55, 0, 0);
            e0.this.o();
            if (e0.this.z()) {
                e0.this.r.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                e0 e0Var2 = e0.this;
                j9 a2 = f9.a(e0Var2.r);
                a2.a(1.0f);
                e0Var2.u = a2;
                e0.this.u.a((k9) new a());
                return;
            }
            e0.this.r.setAlpha(1.0f);
            e0.this.r.setVisibility(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends l9 {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void b(View view) {
            e0.this.r.setAlpha(1.0f);
            e0.this.u.a((k9) null);
            e0.this.u = null;
        }

        @DexIgnore
        public void c(View view) {
            e0.this.r.setVisibility(0);
            e0.this.r.sendAccessibilityEvent(32);
            if (e0.this.r.getParent() instanceof View) {
                f9.D((View) e0.this.r.getParent());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements ActionBarDrawerToggle$Delegate {
        @DexIgnore
        public h(e0 e0Var) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements ActionMode.Callback {
        @DexIgnore
        public ActionMode.Callback a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends l9 {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void b(View view) {
                e0.this.r.setVisibility(8);
                e0 e0Var = e0.this;
                PopupWindow popupWindow = e0Var.s;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (e0Var.r.getParent() instanceof View) {
                    f9.D((View) e0.this.r.getParent());
                }
                e0.this.r.removeAllViews();
                e0.this.u.a((k9) null);
                e0.this.u = null;
            }
        }

        @DexIgnore
        public j(ActionMode.Callback callback) {
            this.a = callback;
        }

        @DexIgnore
        public boolean a(ActionMode actionMode, Menu menu) {
            return this.a.a(actionMode, menu);
        }

        @DexIgnore
        public boolean b(ActionMode actionMode, Menu menu) {
            return this.a.b(actionMode, menu);
        }

        @DexIgnore
        public boolean a(ActionMode actionMode, MenuItem menuItem) {
            return this.a.a(actionMode, menuItem);
        }

        @DexIgnore
        public void a(ActionMode actionMode) {
            this.a.a(actionMode);
            e0 e0Var = e0.this;
            if (e0Var.s != null) {
                e0Var.g.getDecorView().removeCallbacks(e0.this.t);
            }
            e0 e0Var2 = e0.this;
            if (e0Var2.r != null) {
                e0Var2.o();
                e0 e0Var3 = e0.this;
                j9 a2 = f9.a(e0Var3.r);
                a2.a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                e0Var3.u = a2;
                e0.this.u.a((k9) new a());
            }
            e0 e0Var4 = e0.this;
            d0 d0Var = e0Var4.j;
            if (d0Var != null) {
                d0Var.onSupportActionModeFinished(e0Var4.q);
            }
            e0.this.q = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l {
        @DexIgnore
        public k0 a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public BroadcastReceiver c;
        @DexIgnore
        public IntentFilter d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends BroadcastReceiver {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onReceive(Context context, Intent intent) {
                l.this.b();
            }
        }

        @DexIgnore
        public l(k0 k0Var) {
            this.a = k0Var;
            this.b = k0Var.b();
        }

        @DexIgnore
        public void a() {
            BroadcastReceiver broadcastReceiver = this.c;
            if (broadcastReceiver != null) {
                e0.this.f.unregisterReceiver(broadcastReceiver);
                this.c = null;
            }
        }

        @DexIgnore
        public void b() {
            boolean b2 = this.a.b();
            if (b2 != this.b) {
                this.b = b2;
                e0.this.a();
            }
        }

        @DexIgnore
        public int c() {
            this.b = this.a.b();
            return this.b ? 2 : 1;
        }

        @DexIgnore
        public void d() {
            a();
            if (this.c == null) {
                this.c = new a();
            }
            if (this.d == null) {
                this.d = new IntentFilter();
                this.d.addAction("android.intent.action.TIME_SET");
                this.d.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.d.addAction("android.intent.action.TIME_TICK");
            }
            e0.this.f.registerReceiver(this.c, this.d);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class m extends ContentFrameLayout {
        @DexIgnore
        public m(Context context) {
            super(context);
        }

        @DexIgnore
        public final boolean a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return e0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            e0.this.d(0);
            return true;
        }

        @DexIgnore
        public void setBackgroundResource(int i) {
            setBackgroundDrawable(m0.c(getContext(), i));
        }
    }

    /*
    static {
        if (W && !Y) {
            Thread.setDefaultUncaughtExceptionHandler(new a(Thread.getDefaultUncaughtExceptionHandler()));
        }
    }
    */

    @DexIgnore
    public e0(Context context, Window window, d0 d0Var) {
        this.f = context;
        this.g = window;
        this.j = d0Var;
        this.h = this.g.getCallback();
        Window.Callback callback = this.h;
        if (!(callback instanceof k)) {
            this.i = new k(callback);
            this.g.setCallback(this.i);
            z2 a2 = z2.a(context, (AttributeSet) null, X);
            Drawable c2 = a2.c(0);
            if (c2 != null) {
                this.g.setBackgroundDrawable(c2);
            }
            a2.a();
            return;
        }
        throw new IllegalStateException("AppCompat has already installed itself into the Window");
    }

    @DexIgnore
    public final boolean A() {
        if (this.N) {
            Context context = this.f;
            if (context instanceof Activity) {
                try {
                    if ((context.getPackageManager().getActivityInfo(new ComponentName(this.f, this.f.getClass()), 0).configChanges & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0) {
                        return true;
                    }
                    return false;
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e2);
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void B() {
        if (this.w) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        Window.Callback callback = this.h;
        if (callback instanceof Activity) {
            String str = null;
            try {
                str = b6.b((Activity) callback);
            } catch (IllegalArgumentException unused) {
            }
            if (str != null) {
                ActionBar y2 = y();
                if (y2 == null) {
                    this.S = true;
                } else {
                    y2.c(true);
                }
            }
        }
        if (bundle != null && this.M == -100) {
            this.M = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    @DexIgnore
    public void a(ViewGroup viewGroup) {
    }

    @DexIgnore
    public void b(Bundle bundle) {
        q();
    }

    @DexIgnore
    public MenuInflater c() {
        if (this.l == null) {
            v();
            ActionBar actionBar = this.k;
            this.l = new x0(actionBar != null ? actionBar.h() : this.f);
        }
        return this.l;
    }

    @DexIgnore
    public ActionBar d() {
        v();
        return this.k;
    }

    @DexIgnore
    public void e() {
        LayoutInflater from = LayoutInflater.from(this.f);
        if (from.getFactory() == null) {
            r8.b(from, this);
        } else if (!(from.getFactory2() instanceof e0)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @DexIgnore
    public void f() {
        ActionBar d2 = d();
        if (d2 == null || !d2.i()) {
            f(0);
        }
    }

    @DexIgnore
    public void g() {
        if (this.P) {
            this.g.getDecorView().removeCallbacks(this.R);
        }
        this.L = true;
        ActionBar actionBar = this.k;
        if (actionBar != null) {
            actionBar.j();
        }
        l lVar = this.O;
        if (lVar != null) {
            lVar.a();
        }
    }

    @DexIgnore
    public void h() {
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(true);
        }
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void j() {
        ActionBar d2 = d();
        if (d2 != null) {
            d2.e(false);
        }
        l lVar = this.O;
        if (lVar != null) {
            lVar.a();
        }
    }

    @DexIgnore
    public final boolean k(int i2) {
        Resources resources = this.f.getResources();
        Configuration configuration = resources.getConfiguration();
        int i3 = configuration.uiMode & 48;
        int i4 = i2 == 2 ? 32 : 16;
        if (i3 == i4) {
            return false;
        }
        if (A()) {
            ((Activity) this.f).recreate();
            return true;
        }
        Configuration configuration2 = new Configuration(configuration);
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration2.uiMode = i4 | (configuration2.uiMode & -49);
        resources.updateConfiguration(configuration2, displayMetrics);
        if (Build.VERSION.SDK_INT >= 26) {
            return true;
        }
        h0.a(resources);
        return true;
    }

    @DexIgnore
    public final void l() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.x.findViewById(16908290);
        View decorView = this.g.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f.obtainStyledAttributes(a0.AppCompatTheme);
        obtainStyledAttributes.getValue(a0.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(a0.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(a0.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(a0.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(a0.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(a0.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(a0.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(a0.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(a0.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(a0.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @DexIgnore
    public final ViewGroup m() {
        ViewGroup viewGroup;
        Context context;
        TypedArray obtainStyledAttributes = this.f.obtainStyledAttributes(a0.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(a0.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(a0.AppCompatTheme_windowNoTitle, false)) {
                b(1);
            } else if (obtainStyledAttributes.getBoolean(a0.AppCompatTheme_windowActionBar, false)) {
                b(108);
            }
            if (obtainStyledAttributes.getBoolean(a0.AppCompatTheme_windowActionBarOverlay, false)) {
                b(109);
            }
            if (obtainStyledAttributes.getBoolean(a0.AppCompatTheme_windowActionModeOverlay, false)) {
                b(10);
            }
            this.F = obtainStyledAttributes.getBoolean(a0.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            this.g.getDecorView();
            LayoutInflater from = LayoutInflater.from(this.f);
            if (this.G) {
                if (this.E) {
                    viewGroup = (ViewGroup) from.inflate(x.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    viewGroup = (ViewGroup) from.inflate(x.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    f9.a((View) viewGroup, (b9) new c());
                } else {
                    ((m2) viewGroup).setOnFitSystemWindowsListener(new d());
                }
            } else if (this.F) {
                viewGroup = (ViewGroup) from.inflate(x.abc_dialog_title_material, (ViewGroup) null);
                this.D = false;
                this.C = false;
            } else if (this.C) {
                TypedValue typedValue = new TypedValue();
                this.f.getTheme().resolveAttribute(r.actionBarTheme, typedValue, true);
                int i2 = typedValue.resourceId;
                if (i2 != 0) {
                    context = new u0(this.f, i2);
                } else {
                    context = this.f;
                }
                viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(x.abc_screen_toolbar, (ViewGroup) null);
                this.n = (i2) viewGroup.findViewById(w.decor_content_parent);
                this.n.setWindowCallback(u());
                if (this.D) {
                    this.n.a(109);
                }
                if (this.A) {
                    this.n.a(2);
                }
                if (this.B) {
                    this.n.a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.n == null) {
                    this.y = (TextView) viewGroup.findViewById(w.title);
                }
                f3.b(viewGroup);
                ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(w.action_bar_activity_content);
                ViewGroup viewGroup2 = (ViewGroup) this.g.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof FrameLayout) {
                        ((FrameLayout) viewGroup2).setForeground((Drawable) null);
                    }
                }
                this.g.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new e());
                return viewGroup;
            }
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.C + ", windowActionBarOverlay: " + this.D + ", android:windowIsFloating: " + this.F + ", windowActionModeOverlay: " + this.E + ", windowNoTitle: " + this.G + " }");
        }
        obtainStyledAttributes.recycle();
        throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    @DexIgnore
    public void n() {
        i2 i2Var = this.n;
        if (i2Var != null) {
            i2Var.g();
        }
        if (this.s != null) {
            this.g.getDecorView().removeCallbacks(this.t);
            if (this.s.isShowing()) {
                try {
                    this.s.dismiss();
                } catch (IllegalArgumentException unused) {
                }
            }
            this.s = null;
        }
        o();
        n a2 = a(0, false);
        if (a2 != null) {
            h1 h1Var = a2.j;
            if (h1Var != null) {
                h1Var.close();
            }
        }
    }

    @DexIgnore
    public void o() {
        j9 j9Var = this.u;
        if (j9Var != null) {
            j9Var.a();
        }
    }

    @DexIgnore
    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        return a(view, str, context, attributeSet);
    }

    @DexIgnore
    public final void p() {
        if (this.O == null) {
            this.O = new l(k0.a(this.f));
        }
    }

    @DexIgnore
    public final void q() {
        if (!this.w) {
            this.x = m();
            CharSequence t2 = t();
            if (!TextUtils.isEmpty(t2)) {
                i2 i2Var = this.n;
                if (i2Var != null) {
                    i2Var.setWindowTitle(t2);
                } else if (y() != null) {
                    y().b(t2);
                } else {
                    TextView textView = this.y;
                    if (textView != null) {
                        textView.setText(t2);
                    }
                }
            }
            l();
            a(this.x);
            this.w = true;
            n a2 = a(0, false);
            if (this.L) {
                return;
            }
            if (a2 == null || a2.j == null) {
                f(108);
            }
        }
    }

    @DexIgnore
    public final Context r() {
        ActionBar d2 = d();
        Context h2 = d2 != null ? d2.h() : null;
        return h2 == null ? this.f : h2;
    }

    @DexIgnore
    public final int s() {
        int i2 = this.M;
        return i2 != -100 ? i2 : AppCompatDelegate.k();
    }

    @DexIgnore
    public final CharSequence t() {
        Window.Callback callback = this.h;
        if (callback instanceof Activity) {
            return ((Activity) callback).getTitle();
        }
        return this.m;
    }

    @DexIgnore
    public final Window.Callback u() {
        return this.g.getCallback();
    }

    @DexIgnore
    public final void v() {
        q();
        if (this.C && this.k == null) {
            Window.Callback callback = this.h;
            if (callback instanceof Activity) {
                this.k = new l0((Activity) callback, this.D);
            } else if (callback instanceof Dialog) {
                this.k = new l0((Dialog) callback);
            }
            ActionBar actionBar = this.k;
            if (actionBar != null) {
                actionBar.c(this.S);
            }
        }
    }

    @DexIgnore
    public boolean w() {
        return this.v;
    }

    @DexIgnore
    public boolean x() {
        ActionMode actionMode = this.q;
        if (actionMode != null) {
            actionMode.a();
            return true;
        }
        ActionBar d2 = d();
        if (d2 == null || !d2.f()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ActionBar y() {
        return this.k;
    }

    @DexIgnore
    public final boolean z() {
        if (this.w) {
            ViewGroup viewGroup = this.x;
            return viewGroup != null && f9.z(viewGroup);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i implements p1.a {
        @DexIgnore
        public i() {
        }

        @DexIgnore
        public boolean a(h1 h1Var) {
            Window.Callback u = e0.this.u();
            if (u == null) {
                return true;
            }
            u.onMenuOpened(108, h1Var);
            return true;
        }

        @DexIgnore
        public void a(h1 h1Var, boolean z) {
            e0.this.b(h1Var);
        }
    }

    @DexIgnore
    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.h.onContentChanged();
    }

    @DexIgnore
    public void i(int i2) {
        if (i2 == 108) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.b(false);
            }
        } else if (i2 == 0) {
            n a2 = a(i2, true);
            if (a2.o) {
                a(a2, false);
            }
        }
    }

    @DexIgnore
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        return onCreateView((View) null, str, context, attributeSet);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public ViewGroup g;
        @DexIgnore
        public View h;
        @DexIgnore
        public View i;
        @DexIgnore
        public h1 j;
        @DexIgnore
        public f1 k;
        @DexIgnore
        public Context l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q; // = false;
        @DexIgnore
        public boolean r;
        @DexIgnore
        public Bundle s;

        @DexIgnore
        public n(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public boolean a() {
            if (this.h == null) {
                return false;
            }
            if (this.i == null && this.k.c().getCount() <= 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(r.actionBarPopupTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            }
            newTheme.resolveAttribute(r.panelMenuListTheme, typedValue, true);
            int i3 = typedValue.resourceId;
            if (i3 != 0) {
                newTheme.applyStyle(i3, true);
            } else {
                newTheme.applyStyle(z.Theme_AppCompat_CompactMenu, true);
            }
            u0 u0Var = new u0(context, 0);
            u0Var.getTheme().setTo(newTheme);
            this.l = u0Var;
            TypedArray obtainStyledAttributes = u0Var.obtainStyledAttributes(a0.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(a0.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(a0.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        public void a(h1 h1Var) {
            h1 h1Var2 = this.j;
            if (h1Var != h1Var2) {
                if (h1Var2 != null) {
                    h1Var2.b((p1) this.k);
                }
                this.j = h1Var;
                if (h1Var != null) {
                    f1 f1Var = this.k;
                    if (f1Var != null) {
                        h1Var.a((p1) f1Var);
                    }
                }
            }
        }

        @DexIgnore
        public q1 a(p1.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new f1(this.l, x.abc_list_menu_item_layout);
                this.k.a(aVar);
                this.j.a((p1) this.k);
            }
            return this.k.a(this.g);
        }
    }

    @DexIgnore
    public boolean d(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            boolean z2 = this.K;
            this.K = false;
            n a2 = a(0, false);
            if (a2 != null && a2.o) {
                if (!z2) {
                    a(a2, true);
                }
                return true;
            } else if (x()) {
                return true;
            }
        } else if (i2 == 82) {
            e(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public void h(int i2) {
        if (i2 == 108) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.b(true);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends z0 {
        @DexIgnore
        public k(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public final android.view.ActionMode a(ActionMode.Callback callback) {
            w0.a aVar = new w0.a(e0.this.f, callback);
            androidx.appcompat.view.ActionMode a = e0.this.a((ActionMode.Callback) aVar);
            if (a != null) {
                return aVar.b(a);
            }
            return null;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return e0.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || e0.this.c(keyEvent.getKeyCode(), keyEvent);
        }

        @DexIgnore
        public void onContentChanged() {
        }

        @DexIgnore
        public boolean onCreatePanelMenu(int i, Menu menu) {
            if (i != 0 || (menu instanceof h1)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @DexIgnore
        public boolean onMenuOpened(int i, Menu menu) {
            super.onMenuOpened(i, menu);
            e0.this.h(i);
            return true;
        }

        @DexIgnore
        public void onPanelClosed(int i, Menu menu) {
            super.onPanelClosed(i, menu);
            e0.this.i(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, View view, Menu menu) {
            h1 h1Var = menu instanceof h1 ? (h1) menu : null;
            if (i == 0 && h1Var == null) {
                return false;
            }
            if (h1Var != null) {
                h1Var.d(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (h1Var != null) {
                h1Var.d(false);
            }
            return onPreparePanel;
        }

        @DexIgnore
        public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
            n a = e0.this.a(0, true);
            if (a != null) {
                h1 h1Var = a.j;
                if (h1Var != null) {
                    super.onProvideKeyboardShortcuts(list, h1Var, i);
                    return;
                }
            }
            super.onProvideKeyboardShortcuts(list, menu, i);
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (e0.this.w()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            if (!e0.this.w() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return a(callback);
        }
    }

    @DexIgnore
    public final void f(int i2) {
        this.Q = (1 << i2) | this.Q;
        if (!this.P) {
            f9.a(this.g.getDecorView(), this.R);
            this.P = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements p1.a {
        @DexIgnore
        public o() {
        }

        @DexIgnore
        public void a(h1 h1Var, boolean z) {
            h1 m = h1Var.m();
            boolean z2 = m != h1Var;
            e0 e0Var = e0.this;
            if (z2) {
                h1Var = m;
            }
            n a = e0Var.a((Menu) h1Var);
            if (a == null) {
                return;
            }
            if (z2) {
                e0.this.a(a.a, a, m);
                e0.this.a(a, true);
                return;
            }
            e0.this.a(a, z);
        }

        @DexIgnore
        public boolean a(h1 h1Var) {
            if (h1Var != null) {
                return true;
            }
            e0 e0Var = e0.this;
            if (!e0Var.C) {
                return true;
            }
            Window.Callback u = e0Var.u();
            if (u == null || e0.this.L) {
                return true;
            }
            u.onMenuOpened(108, h1Var);
            return true;
        }
    }

    @DexIgnore
    public final int j(int i2) {
        if (i2 == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    @DexIgnore
    public void c(int i2) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f).inflate(i2, viewGroup);
        this.h.onContentChanged();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    public final boolean e(int i2, KeyEvent keyEvent) {
        boolean z2;
        boolean z3;
        if (this.q != null) {
            return false;
        }
        n a2 = a(i2, true);
        if (i2 == 0) {
            i2 i2Var = this.n;
            if (i2Var != null && i2Var.c() && !ViewConfiguration.get(this.f).hasPermanentMenuKey()) {
                if (!this.n.a()) {
                    if (!this.L && b(a2, keyEvent)) {
                        z2 = this.n.f();
                    }
                    z2 = false;
                } else {
                    z2 = this.n.e();
                }
                if (z2) {
                    AudioManager audioManager = (AudioManager) this.f.getSystemService("audio");
                    if (audioManager != null) {
                        audioManager.playSoundEffect(0);
                    } else {
                        Log.w("AppCompatDelegate", "Couldn't get audio manager");
                    }
                }
                return z2;
            }
        }
        if (a2.o || a2.n) {
            z2 = a2.o;
            a(a2, true);
            if (z2) {
            }
            return z2;
        }
        if (a2.m) {
            if (a2.r) {
                a2.m = false;
                z3 = b(a2, keyEvent);
            } else {
                z3 = true;
            }
            if (z3) {
                a(a2, keyEvent);
                z2 = true;
                if (z2) {
                }
                return z2;
            }
        }
        z2 = false;
        if (z2) {
        }
        return z2;
    }

    @DexIgnore
    public boolean b(int i2) {
        int j2 = j(i2);
        if (this.G && j2 == 108) {
            return false;
        }
        if (this.C && j2 == 1) {
            this.C = false;
        }
        if (j2 == 1) {
            B();
            this.G = true;
            return true;
        } else if (j2 == 2) {
            B();
            this.A = true;
            return true;
        } else if (j2 == 5) {
            B();
            this.B = true;
            return true;
        } else if (j2 == 10) {
            B();
            this.E = true;
            return true;
        } else if (j2 == 108) {
            B();
            this.C = true;
            return true;
        } else if (j2 != 109) {
            return this.g.requestFeature(j2);
        } else {
            B();
            this.D = true;
            return true;
        }
    }

    @DexIgnore
    public void a(Toolbar toolbar) {
        if (this.h instanceof Activity) {
            ActionBar d2 = d();
            if (!(d2 instanceof l0)) {
                this.l = null;
                if (d2 != null) {
                    d2.j();
                }
                if (toolbar != null) {
                    i0 i0Var = new i0(toolbar, ((Activity) this.h).getTitle(), this.i);
                    this.k = i0Var;
                    this.g.setCallback(i0Var.m());
                } else {
                    this.k = null;
                    this.g.setCallback(this.i);
                }
                f();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    @DexIgnore
    public int g(int i2) {
        if (i2 == -100) {
            return -1;
        }
        if (i2 != 0) {
            return i2;
        }
        if (Build.VERSION.SDK_INT >= 23 && ((UiModeManager) this.f.getSystemService(UiModeManager.class)).getNightMode() == 0) {
            return -1;
        }
        p();
        return this.O.c();
    }

    @DexIgnore
    public void d(int i2) {
        a(a(i2, true), true);
    }

    @DexIgnore
    public void c(Bundle bundle) {
        int i2 = this.M;
        if (i2 != -100) {
            bundle.putInt("appcompat:local_night_mode", i2);
        }
    }

    @DexIgnore
    public boolean c(int i2, KeyEvent keyEvent) {
        ActionBar d2 = d();
        if (d2 != null && d2.a(i2, keyEvent)) {
            return true;
        }
        n nVar = this.J;
        if (nVar == null || !a(nVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.J == null) {
                n a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.m = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        }
        n nVar2 = this.J;
        if (nVar2 != null) {
            nVar2.n = true;
        }
        return true;
    }

    @DexIgnore
    public <T extends View> T a(int i2) {
        q();
        return this.g.findViewById(i2);
    }

    @DexIgnore
    public void a(Configuration configuration) {
        if (this.C && this.w) {
            ActionBar d2 = d();
            if (d2 != null) {
                d2.a(configuration);
            }
        }
        c2.a().f(this.f);
        a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0169  */
    public androidx.appcompat.view.ActionMode b(ActionMode.Callback callback) {
        androidx.appcompat.view.ActionMode actionMode;
        androidx.appcompat.view.ActionMode actionMode2;
        u0 u0Var;
        o();
        androidx.appcompat.view.ActionMode actionMode3 = this.q;
        if (actionMode3 != null) {
            actionMode3.a();
        }
        if (!(callback instanceof j)) {
            callback = new j(callback);
        }
        d0 d0Var = this.j;
        if (d0Var != null && !this.L) {
            try {
                actionMode = d0Var.onWindowStartingSupportActionMode(callback);
            } catch (AbstractMethodError unused) {
            }
            if (actionMode == null) {
                this.q = actionMode;
            } else {
                boolean z2 = true;
                if (this.r == null) {
                    if (this.F) {
                        TypedValue typedValue = new TypedValue();
                        Resources.Theme theme = this.f.getTheme();
                        theme.resolveAttribute(r.actionBarTheme, typedValue, true);
                        if (typedValue.resourceId != 0) {
                            Resources.Theme newTheme = this.f.getResources().newTheme();
                            newTheme.setTo(theme);
                            newTheme.applyStyle(typedValue.resourceId, true);
                            u0 u0Var2 = new u0(this.f, 0);
                            u0Var2.getTheme().setTo(newTheme);
                            u0Var = u0Var2;
                        } else {
                            u0Var = this.f;
                        }
                        this.r = new ActionBarContextView(u0Var);
                        this.s = new PopupWindow(u0Var, (AttributeSet) null, r.actionModePopupWindowStyle);
                        aa.a(this.s, 2);
                        this.s.setContentView(this.r);
                        this.s.setWidth(-1);
                        u0Var.getTheme().resolveAttribute(r.actionBarSize, typedValue, true);
                        this.r.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, u0Var.getResources().getDisplayMetrics()));
                        this.s.setHeight(-2);
                        this.t = new f();
                    } else {
                        ViewStubCompat viewStubCompat = (ViewStubCompat) this.x.findViewById(w.action_mode_bar_stub);
                        if (viewStubCompat != null) {
                            viewStubCompat.setLayoutInflater(LayoutInflater.from(r()));
                            this.r = (ActionBarContextView) viewStubCompat.a();
                        }
                    }
                }
                if (this.r != null) {
                    o();
                    this.r.d();
                    Context context = this.r.getContext();
                    ActionBarContextView actionBarContextView = this.r;
                    if (this.s != null) {
                        z2 = false;
                    }
                    v0 v0Var = new v0(context, actionBarContextView, callback, z2);
                    if (callback.a((androidx.appcompat.view.ActionMode) v0Var, v0Var.c())) {
                        v0Var.i();
                        this.r.a(v0Var);
                        this.q = v0Var;
                        if (z()) {
                            this.r.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            j9 a2 = f9.a(this.r);
                            a2.a(1.0f);
                            this.u = a2;
                            this.u.a((k9) new g());
                        } else {
                            this.r.setAlpha(1.0f);
                            this.r.setVisibility(0);
                            this.r.sendAccessibilityEvent(32);
                            if (this.r.getParent() instanceof View) {
                                f9.D((View) this.r.getParent());
                            }
                        }
                        if (this.s != null) {
                            this.g.getDecorView().post(this.t);
                        }
                    } else {
                        this.q = null;
                    }
                }
            }
            actionMode2 = this.q;
            if (actionMode2 != null) {
                d0 d0Var2 = this.j;
                if (d0Var2 != null) {
                    d0Var2.onSupportActionModeStarted(actionMode2);
                }
            }
            return this.q;
        }
        actionMode = null;
        if (actionMode == null) {
        }
        actionMode2 = this.q;
        if (actionMode2 != null) {
        }
        return this.q;
    }

    @DexIgnore
    public final boolean c(n nVar) {
        Context context = this.f;
        int i2 = nVar.a;
        if ((i2 == 0 || i2 == 108) && this.n != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(r.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(r.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(r.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                u0 u0Var = new u0(context, 0);
                u0Var.getTheme().setTo(theme2);
                context = u0Var;
            }
        }
        h1 h1Var = new h1(context);
        h1Var.a((h1.a) this);
        nVar.a(h1Var);
        return true;
    }

    @DexIgnore
    public void e(int i2) {
        n a2 = a(i2, true);
        if (a2.j != null) {
            Bundle bundle = new Bundle();
            a2.j.e(bundle);
            if (bundle.size() > 0) {
                a2.s = bundle;
            }
            a2.j.s();
            a2.j.clear();
        }
        a2.r = true;
        a2.q = true;
        if ((i2 == 108 || i2 == 0) && this.n != null) {
            n a3 = a(0, false);
            if (a3 != null) {
                a3.m = false;
                b(a3, (KeyEvent) null);
            }
        }
    }

    @DexIgnore
    public void a(View view) {
        q();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.h.onContentChanged();
    }

    @DexIgnore
    public int l(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        ActionBarContextView actionBarContextView = this.r;
        int i3 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.r.getLayoutParams();
            z2 = true;
            if (this.r.isShown()) {
                if (this.T == null) {
                    this.T = new Rect();
                    this.U = new Rect();
                }
                Rect rect = this.T;
                Rect rect2 = this.U;
                rect.set(0, i2, 0, 0);
                f3.a(this.x, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    View view = this.z;
                    if (view == null) {
                        this.z = new View(this.f);
                        this.z.setBackgroundColor(this.f.getResources().getColor(t.abc_input_method_navigation_guard));
                        this.x.addView(this.z, -1, new ViewGroup.LayoutParams(-1, i2));
                    } else {
                        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.z.setLayoutParams(layoutParams);
                        }
                    }
                    z3 = true;
                } else {
                    z3 = false;
                }
                if (this.z == null) {
                    z2 = false;
                }
                if (!this.E && z2) {
                    i2 = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z4 = true;
                } else {
                    z4 = false;
                }
                z2 = false;
            }
            if (z3) {
                this.r.setLayoutParams(marginLayoutParams);
            }
        }
        View view2 = this.z;
        if (view2 != null) {
            if (!z2) {
                i3 = 8;
            }
            view2.setVisibility(i3);
        }
        return i2;
    }

    @DexIgnore
    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        q();
        ((ViewGroup) this.x.findViewById(16908290)).addView(view, layoutParams);
        this.h.onContentChanged();
    }

    @DexIgnore
    public final void a(CharSequence charSequence) {
        this.m = charSequence;
        i2 i2Var = this.n;
        if (i2Var != null) {
            i2Var.setWindowTitle(charSequence);
        } else if (y() != null) {
            y().b(charSequence);
        } else {
            TextView textView = this.y;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    @DexIgnore
    public boolean a(h1 h1Var, MenuItem menuItem) {
        Window.Callback u2 = u();
        if (u2 == null || this.L) {
            return false;
        }
        n a2 = a((Menu) h1Var.m());
        if (a2 != null) {
            return u2.onMenuItemSelected(a2.a, menuItem);
        }
        return false;
    }

    @DexIgnore
    public void a(h1 h1Var) {
        a(h1Var, true);
    }

    @DexIgnore
    public androidx.appcompat.view.ActionMode a(ActionMode.Callback callback) {
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.q;
            if (actionMode != null) {
                actionMode.a();
            }
            j jVar = new j(callback);
            ActionBar d2 = d();
            if (d2 != null) {
                this.q = d2.a((ActionMode.Callback) jVar);
                androidx.appcompat.view.ActionMode actionMode2 = this.q;
                if (actionMode2 != null) {
                    d0 d0Var = this.j;
                    if (d0Var != null) {
                        d0Var.onSupportActionModeStarted(actionMode2);
                    }
                }
            }
            if (this.q == null) {
                this.q = b((ActionMode.Callback) jVar);
            }
            return this.q;
        }
        throw new IllegalArgumentException("ActionMode callback can not be null.");
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        Window.Callback callback = this.h;
        boolean z2 = true;
        if ((callback instanceof q8.a) || (callback instanceof f0)) {
            View decorView = this.g.getDecorView();
            if (decorView != null && q8.a(decorView, keyEvent)) {
                return true;
            }
        }
        if (keyEvent.getKeyCode() == 82 && this.h.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? a(keyCode, keyEvent) : d(keyCode, keyEvent);
    }

    @DexIgnore
    public boolean a(int i2, KeyEvent keyEvent) {
        boolean z2 = true;
        if (i2 == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z2 = false;
            }
            this.K = z2;
        } else if (i2 == 82) {
            b(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = false;
        if (this.V == null) {
            String string = this.f.obtainStyledAttributes(a0.AppCompatTheme).getString(a0.AppCompatTheme_viewInflaterClass);
            if (string == null || AppCompatViewInflater.class.getName().equals(string)) {
                this.V = new AppCompatViewInflater();
            } else {
                try {
                    this.V = (AppCompatViewInflater) Class.forName(string).getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
                } catch (Throwable th) {
                    Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.V = new AppCompatViewInflater();
                }
            }
        }
        if (W) {
            if (!(attributeSet instanceof XmlPullParser)) {
                z3 = a((ViewParent) view);
            } else if (((XmlPullParser) attributeSet).getDepth() > 1) {
                z3 = true;
            }
            z2 = z3;
        } else {
            z2 = false;
        }
        return this.V.createView(view, str, context, attributeSet, z2, W, true, e3.b());
    }

    @DexIgnore
    public final boolean b(n nVar) {
        nVar.a(r());
        nVar.g = new m(nVar.l);
        nVar.c = 81;
        return true;
    }

    @DexIgnore
    public final boolean b(n nVar, KeyEvent keyEvent) {
        if (this.L) {
            return false;
        }
        if (nVar.m) {
            return true;
        }
        n nVar2 = this.J;
        if (!(nVar2 == null || nVar2 == nVar)) {
            a(nVar2, false);
        }
        Window.Callback u2 = u();
        if (u2 != null) {
            nVar.i = u2.onCreatePanelView(nVar.a);
        }
        int i2 = nVar.a;
        boolean z2 = i2 == 0 || i2 == 108;
        if (z2) {
            i2 i2Var = this.n;
            if (i2Var != null) {
                i2Var.b();
            }
        }
        if (nVar.i == null && (!z2 || !(y() instanceof i0))) {
            if (nVar.j == null || nVar.r) {
                if (nVar.j == null && (!c(nVar) || nVar.j == null)) {
                    return false;
                }
                if (z2 && this.n != null) {
                    if (this.o == null) {
                        this.o = new i();
                    }
                    this.n.a(nVar.j, this.o);
                }
                nVar.j.s();
                if (!u2.onCreatePanelMenu(nVar.a, nVar.j)) {
                    nVar.a((h1) null);
                    if (z2) {
                        i2 i2Var2 = this.n;
                        if (i2Var2 != null) {
                            i2Var2.a((Menu) null, this.o);
                        }
                    }
                    return false;
                }
                nVar.r = false;
            }
            nVar.j.s();
            Bundle bundle = nVar.s;
            if (bundle != null) {
                nVar.j.c(bundle);
                nVar.s = null;
            }
            if (!u2.onPreparePanel(0, nVar.i, nVar.j)) {
                if (z2) {
                    i2 i2Var3 = this.n;
                    if (i2Var3 != null) {
                        i2Var3.a((Menu) null, this.o);
                    }
                }
                nVar.j.r();
                return false;
            }
            nVar.p = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            nVar.j.setQwertyMode(nVar.p);
            nVar.j.r();
        }
        nVar.m = true;
        nVar.n = false;
        this.J = nVar;
        return true;
    }

    @DexIgnore
    public final boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.g.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof View) || f9.y((View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    @DexIgnore
    public final void a(n nVar, KeyEvent keyEvent) {
        int i2;
        if (!nVar.o && !this.L) {
            if (nVar.a == 0) {
                if ((this.f.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            Window.Callback u2 = u();
            if (u2 == null || u2.onMenuOpened(nVar.a, nVar.j)) {
                WindowManager windowManager = (WindowManager) this.f.getSystemService("window");
                if (windowManager != null && b(nVar, keyEvent)) {
                    if (nVar.g == null || nVar.q) {
                        ViewGroup viewGroup = nVar.g;
                        if (viewGroup == null) {
                            if (!b(nVar) || nVar.g == null) {
                                return;
                            }
                        } else if (nVar.q && viewGroup.getChildCount() > 0) {
                            nVar.g.removeAllViews();
                        }
                        if (a(nVar) && nVar.a()) {
                            ViewGroup.LayoutParams layoutParams = nVar.h.getLayoutParams();
                            if (layoutParams == null) {
                                layoutParams = new ViewGroup.LayoutParams(-2, -2);
                            }
                            nVar.g.setBackgroundResource(nVar.b);
                            ViewParent parent = nVar.h.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(nVar.h);
                            }
                            nVar.g.addView(nVar.h, layoutParams);
                            if (!nVar.h.hasFocus()) {
                                nVar.h.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else {
                        View view = nVar.i;
                        if (view != null) {
                            ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
                            if (layoutParams2 != null && layoutParams2.width == -1) {
                                i2 = -1;
                                nVar.n = false;
                                WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams(i2, -2, nVar.d, nVar.e, 1002, 8519680, -3);
                                layoutParams3.gravity = nVar.c;
                                layoutParams3.windowAnimations = nVar.f;
                                windowManager.addView(nVar.g, layoutParams3);
                                nVar.o = true;
                                return;
                            }
                        }
                    }
                    i2 = -2;
                    nVar.n = false;
                    WindowManager.LayoutParams layoutParams32 = new WindowManager.LayoutParams(i2, -2, nVar.d, nVar.e, 1002, 8519680, -3);
                    layoutParams32.gravity = nVar.c;
                    layoutParams32.windowAnimations = nVar.f;
                    windowManager.addView(nVar.g, layoutParams32);
                    nVar.o = true;
                    return;
                }
                return;
            }
            a(nVar, true);
        }
    }

    @DexIgnore
    public void b(h1 h1Var) {
        if (!this.H) {
            this.H = true;
            this.n.g();
            Window.Callback u2 = u();
            if (u2 != null && !this.L) {
                u2.onPanelClosed(108, h1Var);
            }
            this.H = false;
        }
    }

    @DexIgnore
    public final void a(h1 h1Var, boolean z2) {
        i2 i2Var = this.n;
        if (i2Var == null || !i2Var.c() || (ViewConfiguration.get(this.f).hasPermanentMenuKey() && !this.n.d())) {
            n a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback u2 = u();
        if (this.n.a() && z2) {
            this.n.e();
            if (!this.L) {
                u2.onPanelClosed(108, a(0, true).j);
            }
        } else if (u2 != null && !this.L) {
            if (this.P && (this.Q & 1) != 0) {
                this.g.getDecorView().removeCallbacks(this.R);
                this.R.run();
            }
            n a3 = a(0, true);
            h1 h1Var2 = a3.j;
            if (h1Var2 != null && !a3.r && u2.onPreparePanel(0, a3.i, h1Var2)) {
                u2.onMenuOpened(108, a3.j);
                this.n.f();
            }
        }
    }

    @DexIgnore
    public final boolean b(int i2, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        n a2 = a(i2, true);
        if (!a2.o) {
            return b(a2, keyEvent);
        }
        return false;
    }

    @DexIgnore
    public final ActionBarDrawerToggle$Delegate b() {
        return new h(this);
    }

    @DexIgnore
    public final boolean a(n nVar) {
        View view = nVar.i;
        if (view != null) {
            nVar.h = view;
            return true;
        } else if (nVar.j == null) {
            return false;
        } else {
            if (this.p == null) {
                this.p = new o();
            }
            nVar.h = (View) nVar.a((p1.a) this.p);
            if (nVar.h != null) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void a(n nVar, boolean z2) {
        if (z2 && nVar.a == 0) {
            i2 i2Var = this.n;
            if (i2Var != null && i2Var.a()) {
                b(nVar.j);
                return;
            }
        }
        WindowManager windowManager = (WindowManager) this.f.getSystemService("window");
        if (windowManager != null && nVar.o) {
            ViewGroup viewGroup = nVar.g;
            if (viewGroup != null) {
                windowManager.removeView(viewGroup);
                if (z2) {
                    a(nVar.a, nVar, (Menu) null);
                }
            }
        }
        nVar.m = false;
        nVar.n = false;
        nVar.o = false;
        nVar.h = null;
        nVar.q = true;
        if (this.J == nVar) {
            this.J = null;
        }
    }

    @DexIgnore
    public void a(int i2, n nVar, Menu menu) {
        if (menu == null) {
            if (nVar == null && i2 >= 0) {
                n[] nVarArr = this.I;
                if (i2 < nVarArr.length) {
                    nVar = nVarArr[i2];
                }
            }
            if (nVar != null) {
                menu = nVar.j;
            }
        }
        if ((nVar == null || nVar.o) && !this.L) {
            this.h.onPanelClosed(i2, menu);
        }
    }

    @DexIgnore
    public n a(Menu menu) {
        n[] nVarArr = this.I;
        int length = nVarArr != null ? nVarArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            n nVar = nVarArr[i2];
            if (nVar != null && nVar.j == menu) {
                return nVar;
            }
        }
        return null;
    }

    @DexIgnore
    public n a(int i2, boolean z2) {
        n[] nVarArr = this.I;
        if (nVarArr == null || nVarArr.length <= i2) {
            n[] nVarArr2 = new n[(i2 + 1)];
            if (nVarArr != null) {
                System.arraycopy(nVarArr, 0, nVarArr2, 0, nVarArr.length);
            }
            this.I = nVarArr2;
            nVarArr = nVarArr2;
        }
        n nVar = nVarArr[i2];
        if (nVar != null) {
            return nVar;
        }
        n nVar2 = new n(i2);
        nVarArr[i2] = nVar2;
        return nVar2;
    }

    @DexIgnore
    public final boolean a(n nVar, int i2, KeyEvent keyEvent, int i3) {
        boolean z2 = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if (nVar.m || b(nVar, keyEvent)) {
            h1 h1Var = nVar.j;
            if (h1Var != null) {
                z2 = h1Var.performShortcut(i2, keyEvent, i3);
            }
        }
        if (z2 && (i3 & 1) == 0 && this.n == null) {
            a(nVar, true);
        }
        return z2;
    }

    @DexIgnore
    public boolean a() {
        int s2 = s();
        int g2 = g(s2);
        boolean k2 = g2 != -1 ? k(g2) : false;
        if (s2 == 0) {
            p();
            this.O.d();
        }
        this.N = true;
        return k2;
    }
}
