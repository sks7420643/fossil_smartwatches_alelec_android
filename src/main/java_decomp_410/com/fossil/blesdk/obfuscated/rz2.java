package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rz2 extends zr2 implements qz2 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public pz2 j;
    @DexIgnore
    public tr3<yd2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rz2.m;
        }

        @DexIgnore
        public final rz2 b() {
            return new rz2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rz2 e;

        @DexIgnore
        public b(rz2 rz2) {
            this.e = rz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationSummaryDialView.b {
        @DexIgnore
        public /* final */ /* synthetic */ rz2 a;

        @DexIgnore
        public c(rz2 rz2) {
            this.a = rz2;
        }

        @DexIgnore
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.C.a(this.a, i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ yd2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public d(yd2 yd2, Ref$ObjectRef ref$ObjectRef) {
            this.e = yd2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.e.r.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.e.r;
            kd4.a((Object) notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
        }
    }

    /*
    static {
        String simpleName = rz2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        yd2 yd2 = (yd2) qa.a(layoutInflater, R.layout.fragment_notification_dial_landing, viewGroup, false, O0());
        yd2.q.setOnClickListener(new b(this));
        yd2.r.setOnItemClickListener(new c(this));
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        ref$ObjectRef.element = new d(yd2, ref$ObjectRef);
        NotificationSummaryDialView notificationSummaryDialView = yd2.r;
        kd4.a((Object) notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
        this.k = new tr3<>(this, yd2);
        kd4.a((Object) yd2, "binding");
        return yd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        pz2 pz2 = this.j;
        if (pz2 != null) {
            pz2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        pz2 pz2 = this.j;
        if (pz2 != null) {
            pz2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(pz2 pz2) {
        kd4.b(pz2, "presenter");
        this.j = pz2;
    }

    @DexIgnore
    public void a(SparseArray<List<BaseFeatureModel>> sparseArray) {
        kd4.b(sparseArray, "data");
        tr3<yd2> tr3 = this.k;
        if (tr3 != null) {
            yd2 a2 = tr3.a();
            if (a2 != null) {
                NotificationSummaryDialView notificationSummaryDialView = a2.r;
                if (notificationSummaryDialView != null) {
                    notificationSummaryDialView.setDataAsync(sparseArray);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
