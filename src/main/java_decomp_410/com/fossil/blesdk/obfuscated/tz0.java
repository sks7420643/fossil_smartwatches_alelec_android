package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tz0 extends k21<x01> {
    @DexIgnore
    public static /* final */ de0.g<tz0> E; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0<Object> F; // = new de0<>("Fitness.API", new vz0(), E);

    /*
    static {
        new de0("Fitness.CLIENT", new xz0(), E);
    }
    */

    @DexIgnore
    public tz0(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 57, bVar, cVar, kj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
        if (queryLocalInterface instanceof x01) {
            return (x01) queryLocalInterface;
        }
        return new z01(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.HistoryApi";
    }
}
