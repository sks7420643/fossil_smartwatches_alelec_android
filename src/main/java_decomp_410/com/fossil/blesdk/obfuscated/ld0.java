package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ld0 {
    @DexIgnore
    public static /* final */ int[] LoadingImageView; // = {R.attr.circleCrop, R.attr.imageAspectRatio, R.attr.imageAspectRatioAdjust};
    @DexIgnore
    public static /* final */ int LoadingImageView_circleCrop; // = 0;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatio; // = 1;
    @DexIgnore
    public static /* final */ int LoadingImageView_imageAspectRatioAdjust; // = 2;
    @DexIgnore
    public static /* final */ int[] SignInButton; // = {R.attr.buttonSize, R.attr.colorScheme, R.attr.scopeUris};
    @DexIgnore
    public static /* final */ int SignInButton_buttonSize; // = 0;
    @DexIgnore
    public static /* final */ int SignInButton_colorScheme; // = 1;
    @DexIgnore
    public static /* final */ int SignInButton_scopeUris; // = 2;
}
