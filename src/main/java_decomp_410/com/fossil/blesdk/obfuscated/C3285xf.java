package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xf */
public class C3285xf implements java.util.concurrent.Executor {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.Executor f10915e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.ArrayDeque<java.lang.Runnable> f10916f; // = new java.util.ArrayDeque<>();

    @DexIgnore
    /* renamed from: g */
    public java.lang.Runnable f10917g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xf$a */
    public class C3286a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.Runnable f10918e;

        @DexIgnore
        public C3286a(java.lang.Runnable runnable) {
            this.f10918e = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.f10918e.run();
            } finally {
                com.fossil.blesdk.obfuscated.C3285xf.this.mo17699a();
            }
        }
    }

    @DexIgnore
    public C3285xf(java.util.concurrent.Executor executor) {
        this.f10915e = executor;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo17699a() {
        java.lang.Runnable poll = this.f10916f.poll();
        this.f10917g = poll;
        if (poll != null) {
            this.f10915e.execute(this.f10917g);
        }
    }

    @DexIgnore
    public synchronized void execute(java.lang.Runnable runnable) {
        this.f10916f.offer(new com.fossil.blesdk.obfuscated.C3285xf.C3286a(runnable));
        if (this.f10917g == null) {
            mo17699a();
        }
    }
}
