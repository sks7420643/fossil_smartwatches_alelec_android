package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g22 implements m22 {
    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    public void a(n22 n22) {
        if (p22.a((CharSequence) n22.d(), n22.f) >= 2) {
            n22.a(a(n22.d().charAt(n22.f), n22.d().charAt(n22.f + 1)));
            n22.f += 2;
            return;
        }
        char c = n22.c();
        int a = p22.a(n22.d(), n22.f, a());
        if (a != a()) {
            if (a == 1) {
                n22.a(230);
                n22.b(1);
            } else if (a == 2) {
                n22.a(239);
                n22.b(2);
            } else if (a == 3) {
                n22.a(238);
                n22.b(3);
            } else if (a == 4) {
                n22.a(240);
                n22.b(4);
            } else if (a == 5) {
                n22.a(231);
                n22.b(5);
            } else {
                throw new IllegalStateException("Illegal mode: " + a);
            }
        } else if (p22.c(c)) {
            n22.a(235);
            n22.a((char) ((c - 128) + 1));
            n22.f++;
        } else {
            n22.a((char) (c + 1));
            n22.f++;
        }
    }

    @DexIgnore
    public static char a(char c, char c2) {
        if (p22.b(c) && p22.b(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + 130);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }
}
