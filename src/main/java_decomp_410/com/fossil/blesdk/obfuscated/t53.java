package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.at2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t53 extends zr2 implements s53 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<gg2> j;
    @DexIgnore
    public at2 k;
    @DexIgnore
    public r53 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return t53.n;
        }

        @DexIgnore
        public final t53 b() {
            return new t53();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t53 e;

        @DexIgnore
        public b(t53 t53) {
            this.e = t53;
        }

        @DexIgnore
        public final void onClick(View view) {
            gg2 a = this.e.U0().a();
            if (a != null) {
                a.s.setText("");
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ t53 e;

        @DexIgnore
        public c(t53 t53) {
            this.e = t53;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (TextUtils.isEmpty(charSequence)) {
                gg2 a = this.e.U0().a();
                if (a != null) {
                    ImageView imageView = a.r;
                    if (imageView != null) {
                        imageView.setVisibility(8);
                    }
                }
                this.e.a("");
                this.e.V0().h();
                return;
            }
            gg2 a2 = this.e.U0().a();
            if (a2 != null) {
                ImageView imageView2 = a2.r;
                if (imageView2 != null) {
                    imageView2.setVisibility(0);
                }
            }
            this.e.a(String.valueOf(charSequence));
            this.e.V0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t53 e;

        @DexIgnore
        public d(t53 t53) {
            this.e = t53;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements at2.d {
        @DexIgnore
        public /* final */ /* synthetic */ t53 a;

        @DexIgnore
        public e(t53 t53) {
            this.a = t53;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            gg2 a2 = this.a.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                kd4.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.u;
                kd4.a((Object) flexibleTextView2, "it.tvNotFound");
                pd4 pd4 = pd4.a;
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements at2.e {
        @DexIgnore
        public /* final */ /* synthetic */ t53 a;

        @DexIgnore
        public f(t53 t53) {
            this.a = t53;
        }

        @DexIgnore
        public void a(WatchApp watchApp) {
            kd4.b(watchApp, "item");
            this.a.V0().a(watchApp);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ gg2 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(gg2 gg2, long j) {
            this.a = gg2;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.a.q;
            kd4.a((Object) flexibleTextView, "binding.btnCancel");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = t53.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchAppSearchFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void T0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            cl2 cl2 = cl2.a;
            tr3<gg2> tr3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (tr3 != null) {
                gg2 a2 = tr3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    kd4.a((Object) activity, "it");
                    cl2.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final tr3<gg2> U0() {
        tr3<gg2> tr3 = this.j;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final r53 V0() {
        r53 r53 = this.l;
        if (r53 != null) {
            return r53;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(List<Pair<WatchApp, String>> list) {
        kd4.b(list, "results");
        at2 at2 = this.k;
        if (at2 != null) {
            at2.b(list);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void e(List<Pair<WatchApp, String>> list) {
        kd4.b(list, "recentSearchResult");
        at2 at2 = this.k;
        if (at2 != null) {
            at2.a(list);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (gg2) qa.a(layoutInflater, R.layout.fragment_watch_app_search, viewGroup, false, O0()));
        tr3<gg2> tr3 = this.j;
        if (tr3 != null) {
            gg2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        r53 r53 = this.l;
        if (r53 != null) {
            r53.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        r53 r53 = this.l;
        if (r53 != null) {
            r53.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "it");
            a(activity, 550);
        }
        this.k = new at2();
        tr3<gg2> tr3 = this.j;
        if (tr3 != null) {
            gg2 a2 = tr3.a();
            if (a2 != null) {
                gg2 gg2 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = gg2.t;
                kd4.a((Object) recyclerViewEmptySupport, "this.rvResults");
                at2 at2 = this.k;
                if (at2 != null) {
                    recyclerViewEmptySupport.setAdapter(at2);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = gg2.t;
                    kd4.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = gg2.t;
                    FlexibleTextView flexibleTextView = gg2.u;
                    kd4.a((Object) flexibleTextView, "this.tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = gg2.r;
                    kd4.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    gg2.r.setOnClickListener(new b(this));
                    gg2.s.addTextChangedListener(new c(this));
                    gg2.q.setOnClickListener(new d(this));
                    at2 at22 = this.k;
                    if (at22 != null) {
                        at22.a((at2.d) new e(this));
                        at2 at23 = this.k;
                        if (at23 != null) {
                            at23.a((at2.e) new f(this));
                        } else {
                            kd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        kd4.d("mAdapter");
                        throw null;
                    }
                } else {
                    kd4.d("mAdapter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        at2 at2 = this.k;
        if (at2 != null) {
            at2.b((List<Pair<WatchApp, String>>) null);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = aq2.a.a(j2);
        Window window = fragmentActivity.getWindow();
        kd4.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        tr3<gg2> tr3 = this.j;
        if (tr3 != null) {
            gg2 a3 = tr3.a();
            if (a3 != null) {
                kd4.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, gg2 gg2) {
        FlexibleTextView flexibleTextView = gg2.q;
        kd4.a((Object) flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener(new g(gg2, j2));
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        at2 at2 = this.k;
        if (at2 != null) {
            at2.a(str);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(r53 r53) {
        kd4.b(r53, "presenter");
        this.l = r53;
    }

    @DexIgnore
    public void a(WatchApp watchApp) {
        kd4.b(watchApp, "selectedWatchApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            cl2 cl2 = cl2.a;
            tr3<gg2> tr3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (tr3 != null) {
                gg2 a2 = tr3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    kd4.a((Object) activity, "it");
                    cl2.a(flexibleTextView, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_WATCH_APP_RESULT_ID", watchApp.getWatchappId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            kd4.d("mBinding");
            throw null;
        }
    }
}
