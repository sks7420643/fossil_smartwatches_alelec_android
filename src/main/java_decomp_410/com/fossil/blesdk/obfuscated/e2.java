package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e2 {
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public x2 b;
    @DexIgnore
    public x2 c;
    @DexIgnore
    public x2 d;

    @DexIgnore
    public e2(ImageView imageView) {
        this.a = imageView;
    }

    @DexIgnore
    public void a(AttributeSet attributeSet, int i) {
        z2 a2 = z2.a(this.a.getContext(), attributeSet, a0.AppCompatImageView, i, 0);
        try {
            Drawable drawable = this.a.getDrawable();
            if (drawable == null) {
                int g = a2.g(a0.AppCompatImageView_srcCompat, -1);
                if (g != -1) {
                    drawable = m0.c(this.a.getContext(), g);
                    if (drawable != null) {
                        this.a.setImageDrawable(drawable);
                    }
                }
            }
            if (drawable != null) {
                k2.b(drawable);
            }
            if (a2.g(a0.AppCompatImageView_tint)) {
                x9.a(this.a, a2.a(a0.AppCompatImageView_tint));
            }
            if (a2.g(a0.AppCompatImageView_tintMode)) {
                x9.a(this.a, k2.a(a2.d(a0.AppCompatImageView_tintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            a2.a();
        }
    }

    @DexIgnore
    public ColorStateList b() {
        x2 x2Var = this.c;
        if (x2Var != null) {
            return x2Var.a;
        }
        return null;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        x2 x2Var = this.c;
        if (x2Var != null) {
            return x2Var.b;
        }
        return null;
    }

    @DexIgnore
    public boolean d() {
        return Build.VERSION.SDK_INT < 21 || !(this.a.getBackground() instanceof RippleDrawable);
    }

    @DexIgnore
    public final boolean e() {
        int i = Build.VERSION.SDK_INT;
        if (i <= 21) {
            return i == 21;
        }
        if (this.b != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(int i) {
        if (i != 0) {
            Drawable c2 = m0.c(this.a.getContext(), i);
            if (c2 != null) {
                k2.b(c2);
            }
            this.a.setImageDrawable(c2);
        } else {
            this.a.setImageDrawable((Drawable) null);
        }
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new x2();
        }
        x2 x2Var = this.c;
        x2Var.a = colorStateList;
        x2Var.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new x2();
        }
        x2 x2Var = this.c;
        x2Var.b = mode;
        x2Var.c = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            k2.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!e() || !a(drawable)) {
            x2 x2Var = this.c;
            if (x2Var != null) {
                c2.a(drawable, x2Var, this.a.getDrawableState());
                return;
            }
            x2 x2Var2 = this.b;
            if (x2Var2 != null) {
                c2.a(drawable, x2Var2, this.a.getDrawableState());
            }
        }
    }

    @DexIgnore
    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new x2();
        }
        x2 x2Var = this.d;
        x2Var.a();
        ColorStateList a2 = x9.a(this.a);
        if (a2 != null) {
            x2Var.d = true;
            x2Var.a = a2;
        }
        PorterDuff.Mode b2 = x9.b(this.a);
        if (b2 != null) {
            x2Var.c = true;
            x2Var.b = b2;
        }
        if (!x2Var.d && !x2Var.c) {
            return false;
        }
        c2.a(drawable, x2Var, this.a.getDrawableState());
        return true;
    }
}
