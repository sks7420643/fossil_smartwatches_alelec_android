package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a90 {
    @DexIgnore
    public static /* final */ String c; // = a90.class.getSimpleName();
    @DexIgnore
    public /* final */ Hashtable<ResourceType, LinkedHashSet<Phase>> a;
    @DexIgnore
    public /* final */ Hashtable<ResourceType, Integer> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public /* synthetic */ a90(Hashtable hashtable, fd4 fd4) {
        this(hashtable);
    }

    @DexIgnore
    public final boolean a(Phase phase) {
        Object obj;
        kd4.b(phase, "targetPhase");
        synchronized (this.a) {
            synchronized (this.b) {
                t90 t90 = t90.c;
                String str = c;
                kd4.a((Object) str, "TAG");
                t90.a(str, "Before allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                Iterator<ResourceType> it = phase.n().iterator();
                while (it.hasNext()) {
                    ResourceType next = it.next();
                    LinkedHashSet linkedHashSet = this.a.get(next);
                    if (linkedHashSet == null) {
                        linkedHashSet = new LinkedHashSet();
                    }
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    kd4.a((Object) num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator it2 = linkedHashSet.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it2.next();
                        if (((Phase) obj).b(phase)) {
                            break;
                        }
                    }
                    if (obj == null) {
                        if (linkedHashSet.size() < intValue) {
                            linkedHashSet.add(phase);
                            this.a.put(next, linkedHashSet);
                        } else {
                            Set<Map.Entry<ResourceType, LinkedHashSet<Phase>>> entrySet = this.a.entrySet();
                            kd4.a((Object) entrySet, "resourceHolders.entries");
                            for (Map.Entry value : entrySet) {
                                ((LinkedHashSet) value.getValue()).remove(phase);
                            }
                            t90 t902 = t90.c;
                            String str2 = c;
                            kd4.a((Object) str2, "TAG");
                            t902.a(str2, "After allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                            return false;
                        }
                    }
                }
                t90 t903 = t90.c;
                String str3 = c;
                kd4.a((Object) str3, "TAG");
                t903.a(str3, "After allocateResource for " + phase + ", " + "current resourceHolders=" + this.a);
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(Phase phase) {
        kd4.b(phase, "targetPhase");
        synchronized (this.a) {
            synchronized (this.b) {
                t90 t90 = t90.c;
                String str = c;
                kd4.a((Object) str, "TAG");
                t90.a(str, "Before releaseResource for " + phase + ", " + "current resourceHolders=" + this.a);
                Iterator<ResourceType> it = phase.n().iterator();
                while (it.hasNext()) {
                    LinkedHashSet linkedHashSet = this.a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(phase);
                    }
                }
                t90 t902 = t90.c;
                String str2 = c;
                kd4.a((Object) str2, "TAG");
                t902.a(str2, "After releaseResource for " + phase + ", " + "current resourceHolders=" + this.a);
                qa4 qa4 = qa4.a;
            }
            qa4 qa42 = qa4.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Hashtable<ResourceType, Integer> a; // = new Hashtable<>();

        @DexIgnore
        public final b a(ResourceType resourceType, int i) {
            kd4.b(resourceType, "resourceType");
            Integer num = this.a.get(resourceType);
            if (num == null) {
                num = 0;
            }
            kd4.a((Object) num, "resourceQuotas[resourceType] ?: 0");
            this.a.put(resourceType, Integer.valueOf(num.intValue() + i));
            return this;
        }

        @DexIgnore
        public final b b(ResourceType resourceType, int i) {
            kd4.b(resourceType, "resourceType");
            this.a.put(resourceType, Integer.valueOf(i));
            return this;
        }

        @DexIgnore
        public final a90 a() {
            return new a90(this.a, (fd4) null);
        }
    }

    @DexIgnore
    public a90(Hashtable<ResourceType, Integer> hashtable) {
        this.b = hashtable;
        this.a = new Hashtable<>();
        Set<ResourceType> keySet = this.b.keySet();
        kd4.a((Object) keySet, "resourceQuotas.keys");
        for (ResourceType put : keySet) {
            this.a.put(put, new LinkedHashSet());
        }
    }
}
