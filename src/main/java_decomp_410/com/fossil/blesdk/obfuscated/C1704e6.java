package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e6 */
public class C1704e6 implements com.fossil.blesdk.obfuscated.C1537c6 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.app.Notification.Builder f4712a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1597d6.C1600c f4713b;

    @DexIgnore
    /* renamed from: c */
    public android.widget.RemoteViews f4714c;

    @DexIgnore
    /* renamed from: d */
    public android.widget.RemoteViews f4715d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<android.os.Bundle> f4716e; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.os.Bundle f4717f; // = new android.os.Bundle();

    @DexIgnore
    /* renamed from: g */
    public int f4718g;

    @DexIgnore
    /* renamed from: h */
    public android.widget.RemoteViews f4719h;

    @DexIgnore
    public C1704e6(com.fossil.blesdk.obfuscated.C1597d6.C1600c cVar) {
        this.f4713b = cVar;
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            this.f4712a = new android.app.Notification.Builder(cVar.f4286a, cVar.f4279I);
        } else {
            this.f4712a = new android.app.Notification.Builder(cVar.f4286a);
        }
        android.app.Notification notification = cVar.f4284N;
        this.f4712a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, cVar.f4293h).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(cVar.f4289d).setContentText(cVar.f4290e).setContentInfo(cVar.f4295j).setContentIntent(cVar.f4291f).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(cVar.f4292g, (notification.flags & 128) != 0).setLargeIcon(cVar.f4294i).setNumber(cVar.f4296k).setProgress(cVar.f4303r, cVar.f4304s, cVar.f4305t);
        if (android.os.Build.VERSION.SDK_INT < 21) {
            this.f4712a.setSound(notification.sound, notification.audioStreamType);
        }
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            this.f4712a.setSubText(cVar.f4301p).setUsesChronometer(cVar.f4299n).setPriority(cVar.f4297l);
            java.util.Iterator<com.fossil.blesdk.obfuscated.C1597d6.C1598a> it = cVar.f4287b.iterator();
            while (it.hasNext()) {
                mo10338a(it.next());
            }
            android.os.Bundle bundle = cVar.f4272B;
            if (bundle != null) {
                this.f4717f.putAll(bundle);
            }
            if (android.os.Build.VERSION.SDK_INT < 20) {
                if (cVar.f4309x) {
                    this.f4717f.putBoolean("android.support.localOnly", true);
                }
                java.lang.String str = cVar.f4306u;
                if (str != null) {
                    this.f4717f.putString("android.support.groupKey", str);
                    if (cVar.f4307v) {
                        this.f4717f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f4717f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                java.lang.String str2 = cVar.f4308w;
                if (str2 != null) {
                    this.f4717f.putString("android.support.sortKey", str2);
                }
            }
            this.f4714c = cVar.f4276F;
            this.f4715d = cVar.f4277G;
        }
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            this.f4712a.setShowWhen(cVar.f4298m);
            if (android.os.Build.VERSION.SDK_INT < 21) {
                java.util.ArrayList<java.lang.String> arrayList = cVar.f4285O;
                if (arrayList != null && !arrayList.isEmpty()) {
                    android.os.Bundle bundle2 = this.f4717f;
                    java.util.ArrayList<java.lang.String> arrayList2 = cVar.f4285O;
                    bundle2.putStringArray("android.people", (java.lang.String[]) arrayList2.toArray(new java.lang.String[arrayList2.size()]));
                }
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            this.f4712a.setLocalOnly(cVar.f4309x).setGroup(cVar.f4306u).setGroupSummary(cVar.f4307v).setSortKey(cVar.f4308w);
            this.f4718g = cVar.f4283M;
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            this.f4712a.setCategory(cVar.f4271A).setColor(cVar.f4273C).setVisibility(cVar.f4274D).setPublicVersion(cVar.f4275E).setSound(notification.sound, notification.audioAttributes);
            java.util.Iterator<java.lang.String> it2 = cVar.f4285O.iterator();
            while (it2.hasNext()) {
                this.f4712a.addPerson(it2.next());
            }
            this.f4719h = cVar.f4278H;
            if (cVar.f4288c.size() > 0) {
                android.os.Bundle bundle3 = cVar.mo9784b().getBundle("android.car.EXTENSIONS");
                bundle3 = bundle3 == null ? new android.os.Bundle() : bundle3;
                android.os.Bundle bundle4 = new android.os.Bundle();
                for (int i = 0; i < cVar.f4288c.size(); i++) {
                    bundle4.putBundle(java.lang.Integer.toString(i), com.fossil.blesdk.obfuscated.C1771f6.m6745a(cVar.f4288c.get(i)));
                }
                bundle3.putBundle("invisible_actions", bundle4);
                cVar.mo9784b().putBundle("android.car.EXTENSIONS", bundle3);
                this.f4717f.putBundle("android.car.EXTENSIONS", bundle3);
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            this.f4712a.setExtras(cVar.f4272B).setRemoteInputHistory(cVar.f4302q);
            android.widget.RemoteViews remoteViews = cVar.f4276F;
            if (remoteViews != null) {
                this.f4712a.setCustomContentView(remoteViews);
            }
            android.widget.RemoteViews remoteViews2 = cVar.f4277G;
            if (remoteViews2 != null) {
                this.f4712a.setCustomBigContentView(remoteViews2);
            }
            android.widget.RemoteViews remoteViews3 = cVar.f4278H;
            if (remoteViews3 != null) {
                this.f4712a.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            this.f4712a.setBadgeIconType(cVar.f4280J).setShortcutId(cVar.f4281K).setTimeoutAfter(cVar.f4282L).setGroupAlertBehavior(cVar.f4283M);
            if (cVar.f4311z) {
                this.f4712a.setColorized(cVar.f4310y);
            }
            if (!android.text.TextUtils.isEmpty(cVar.f4279I)) {
                this.f4712a.setSound((android.net.Uri) null).setDefaults(0).setLights(0, 0, 0).setVibrate((long[]) null);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.app.Notification.Builder mo9428a() {
        return this.f4712a;
    }

    @DexIgnore
    /* renamed from: b */
    public android.app.Notification mo10339b() {
        com.fossil.blesdk.obfuscated.C1597d6.C1601d dVar = this.f4713b.f4300o;
        if (dVar != null) {
            dVar.mo9769a((com.fossil.blesdk.obfuscated.C1537c6) this);
        }
        android.widget.RemoteViews c = dVar != null ? dVar.mo9798c(this) : null;
        android.app.Notification c2 = mo10340c();
        if (c != null) {
            c2.contentView = c;
        } else {
            android.widget.RemoteViews remoteViews = this.f4713b.f4276F;
            if (remoteViews != null) {
                c2.contentView = remoteViews;
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 16 && dVar != null) {
            android.widget.RemoteViews b = dVar.mo9797b(this);
            if (b != null) {
                c2.bigContentView = b;
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 21 && dVar != null) {
            android.widget.RemoteViews d = this.f4713b.f4300o.mo9799d(this);
            if (d != null) {
                c2.headsUpContentView = d;
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= 16 && dVar != null) {
            android.os.Bundle a = com.fossil.blesdk.obfuscated.C1597d6.m5690a(c2);
            if (a != null) {
                dVar.mo9795a(a);
            }
        }
        return c2;
    }

    @DexIgnore
    /* renamed from: c */
    public android.app.Notification mo10340c() {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 26) {
            return this.f4712a.build();
        }
        if (i >= 24) {
            android.app.Notification build = this.f4712a.build();
            if (this.f4718g != 0) {
                if (!(build.getGroup() == null || (build.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 || this.f4718g != 2)) {
                    mo10337a(build);
                }
                if (build.getGroup() != null && (build.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.f4718g == 1) {
                    mo10337a(build);
                }
            }
            return build;
        } else if (i >= 21) {
            this.f4712a.setExtras(this.f4717f);
            android.app.Notification build2 = this.f4712a.build();
            android.widget.RemoteViews remoteViews = this.f4714c;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            android.widget.RemoteViews remoteViews2 = this.f4715d;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            android.widget.RemoteViews remoteViews3 = this.f4719h;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.f4718g != 0) {
                if (!(build2.getGroup() == null || (build2.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 || this.f4718g != 2)) {
                    mo10337a(build2);
                }
                if (build2.getGroup() != null && (build2.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.f4718g == 1) {
                    mo10337a(build2);
                }
            }
            return build2;
        } else if (i >= 20) {
            this.f4712a.setExtras(this.f4717f);
            android.app.Notification build3 = this.f4712a.build();
            android.widget.RemoteViews remoteViews4 = this.f4714c;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            android.widget.RemoteViews remoteViews5 = this.f4715d;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.f4718g != 0) {
                if (!(build3.getGroup() == null || (build3.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 || this.f4718g != 2)) {
                    mo10337a(build3);
                }
                if (build3.getGroup() != null && (build3.flags & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0 && this.f4718g == 1) {
                    mo10337a(build3);
                }
            }
            return build3;
        } else if (i >= 19) {
            android.util.SparseArray<android.os.Bundle> a = com.fossil.blesdk.obfuscated.C1771f6.m6747a(this.f4716e);
            if (a != null) {
                this.f4717f.putSparseParcelableArray("android.support.actionExtras", a);
            }
            this.f4712a.setExtras(this.f4717f);
            android.app.Notification build4 = this.f4712a.build();
            android.widget.RemoteViews remoteViews6 = this.f4714c;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            android.widget.RemoteViews remoteViews7 = this.f4715d;
            if (remoteViews7 != null) {
                build4.bigContentView = remoteViews7;
            }
            return build4;
        } else if (i < 16) {
            return this.f4712a.getNotification();
        } else {
            android.app.Notification build5 = this.f4712a.build();
            android.os.Bundle a2 = com.fossil.blesdk.obfuscated.C1597d6.m5690a(build5);
            android.os.Bundle bundle = new android.os.Bundle(this.f4717f);
            for (java.lang.String str : this.f4717f.keySet()) {
                if (a2.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a2.putAll(bundle);
            android.util.SparseArray<android.os.Bundle> a3 = com.fossil.blesdk.obfuscated.C1771f6.m6747a(this.f4716e);
            if (a3 != null) {
                com.fossil.blesdk.obfuscated.C1597d6.m5690a(build5).putSparseParcelableArray("android.support.actionExtras", a3);
            }
            android.widget.RemoteViews remoteViews8 = this.f4714c;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            android.widget.RemoteViews remoteViews9 = this.f4715d;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10338a(com.fossil.blesdk.obfuscated.C1597d6.C1598a aVar) {
        android.os.Bundle bundle;
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 20) {
            android.app.Notification.Action.Builder builder = new android.app.Notification.Action.Builder(aVar.mo9763e(), aVar.mo9767i(), aVar.mo9759a());
            if (aVar.mo9764f() != null) {
                for (android.app.RemoteInput addRemoteInput : com.fossil.blesdk.obfuscated.C1926h6.m7802a(aVar.mo9764f())) {
                    builder.addRemoteInput(addRemoteInput);
                }
            }
            if (aVar.mo9762d() != null) {
                bundle = new android.os.Bundle(aVar.mo9762d());
            } else {
                bundle = new android.os.Bundle();
            }
            bundle.putBoolean("android.support.allowGeneratedReplies", aVar.mo9760b());
            if (android.os.Build.VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(aVar.mo9760b());
            }
            bundle.putInt("android.support.action.semanticAction", aVar.mo9765g());
            if (android.os.Build.VERSION.SDK_INT >= 28) {
                builder.setSemanticAction(aVar.mo9765g());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", aVar.mo9766h());
            builder.addExtras(bundle);
            this.f4712a.addAction(builder.build());
        } else if (i >= 16) {
            this.f4716e.add(com.fossil.blesdk.obfuscated.C1771f6.m6743a(this.f4712a, aVar));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10337a(android.app.Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        notification.defaults &= -2;
        notification.defaults &= -3;
    }
}
