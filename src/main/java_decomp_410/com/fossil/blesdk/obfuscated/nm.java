package com.fossil.blesdk.obfuscated;

import com.android.volley.VolleyError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nm implements wm {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public nm() {
        this(2500, 1, 1.0f);
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public int b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.b <= this.c;
    }

    @DexIgnore
    public nm(int i, int i2, float f) {
        this.a = i;
        this.c = i2;
        this.d = f;
    }

    @DexIgnore
    public void a(VolleyError volleyError) throws VolleyError {
        this.b++;
        int i = this.a;
        this.a = i + ((int) (((float) i) * this.d));
        if (!c()) {
            throw volleyError;
        }
    }
}
