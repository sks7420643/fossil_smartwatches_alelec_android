package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n90 {
    @DexIgnore
    public static final double a(long j) {
        return ((double) j) / ((double) 1000);
    }

    @DexIgnore
    public static final int a(od4 od4) {
        kd4.b(od4, "$this$MAX_UNSIGNED_VALUE");
        return 65535;
    }

    @DexIgnore
    public static final long a(jd4 jd4) {
        kd4.b(jd4, "$this$MAX_UNSIGNED_VALUE");
        return 4294967295L;
    }

    @DexIgnore
    public static final String a(int i) {
        ze4.a(16);
        String l = Long.toString(((long) i) & 4294967295L, 16);
        kd4.a((Object) l, "java.lang.Long.toString(this, checkRadix(radix))");
        if (l != null) {
            String upperCase = l.toUpperCase();
            kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            return StringsKt__StringsKt.a(upperCase, 8, '0');
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final short a(bd4 bd4) {
        kd4.b(bd4, "$this$MAX_UNSIGNED_VALUE");
        return 255;
    }

    @DexIgnore
    public static final int b(short s) {
        return s < 0 ? s + 65536 : s;
    }

    @DexIgnore
    public static final long b(int i) {
        return i < 0 ? ((long) i) + 4294967296L : (long) i;
    }

    @DexIgnore
    public static final short b(byte b) {
        return b < 0 ? (short) (b + 256) : (short) b;
    }

    @DexIgnore
    public static final short b(bd4 bd4) {
        kd4.b(bd4, "$this$MIN_UNSIGNED_VALUE");
        return 0;
    }

    @DexIgnore
    public static final String a(short s) {
        short s2 = s & NotificationHandMovingConfig.HAND_DEGREE_DEVICE_DEFAULT_POSITION;
        ze4.a(16);
        String num = Integer.toString(s2, 16);
        kd4.a((Object) num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        if (num != null) {
            String upperCase = num.toUpperCase();
            kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            return StringsKt__StringsKt.a(upperCase, 4, '0');
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(byte b) {
        byte b2 = b & FileType.MASKED_INDEX;
        ze4.a(16);
        String num = Integer.toString(b2, 16);
        kd4.a((Object) num, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        if (num != null) {
            String upperCase = num.toUpperCase();
            kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            return StringsKt__StringsKt.a(upperCase, 2, '0');
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(long j, boolean z) {
        if (z) {
            return String.valueOf(((double) j) / ((double) 1000));
        }
        return String.valueOf(j / ((long) 1000));
    }

    @DexIgnore
    public static final float a(float f, int i) {
        float pow = (float) Math.pow((double) 10.0f, (double) i);
        return ((float) td4.b(f * pow)) / pow;
    }
}
