package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ol0 implements Parcelable.Creator<nl0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Bundle bundle = null;
        wd0[] wd0Arr = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                bundle = SafeParcelReader.a(parcel, a);
            } else if (a2 != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                wd0Arr = (wd0[]) SafeParcelReader.b(parcel, a, wd0.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new nl0(bundle, wd0Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new nl0[i];
    }
}
