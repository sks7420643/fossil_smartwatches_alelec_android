package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v23 implements MembersInjector<CommuteTimeSettingsDefaultAddressActivity> {
    @DexIgnore
    public static void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
        commuteTimeSettingsDefaultAddressActivity.B = commuteTimeSettingsDefaultAddressPresenter;
    }
}
