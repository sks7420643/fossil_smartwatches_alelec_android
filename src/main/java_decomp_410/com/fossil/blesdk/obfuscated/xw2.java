package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.rt2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xw2 extends zr2 implements ww2 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<wd2> j;
    @DexIgnore
    public vw2 k;
    @DexIgnore
    public rt2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xw2.n;
        }

        @DexIgnore
        public final xw2 b() {
            return new xw2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements rt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ xw2 a;

        @DexIgnore
        public b(xw2 xw2) {
            this.a = xw2;
        }

        @DexIgnore
        public void a() {
            xw2.b(this.a).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xw2 e;

        @DexIgnore
        public c(xw2 xw2) {
            this.e = xw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            xw2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ xw2 e;

        @DexIgnore
        public d(xw2 xw2) {
            this.e = xw2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            rt2 a = this.e.l;
            if (a != null) {
                a.a(String.valueOf(charSequence));
            }
            this.e.O(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xw2 e;

        @DexIgnore
        public e(xw2 xw2) {
            this.e = xw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            xw2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wd2 e;

        @DexIgnore
        public f(wd2 wd2) {
            this.e = wd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.r.setText("");
        }
    }

    /*
    static {
        String simpleName = xw2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ vw2 b(xw2 xw2) {
        vw2 vw2 = xw2.k;
        if (vw2 != null) {
            return vw2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void H() {
    }

    @DexIgnore
    public void K(boolean z) {
        int i;
        tr3<wd2> tr3 = this.j;
        if (tr3 != null) {
            wd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
            }
            tr3<wd2> tr32 = this.j;
            if (tr32 != null) {
                wd2 a3 = tr32.a();
                if (a3 != null) {
                    FlexibleButton flexibleButton2 = a3.q;
                    if (flexibleButton2 != null) {
                        if (z) {
                            i = k6.a((Context) PortfolioApp.W.c(), (int) R.color.colorPrimary);
                        } else {
                            i = k6.a((Context) PortfolioApp.W.c(), (int) R.color.fossilCoolGray);
                        }
                        flexibleButton2.setBackgroundColor(i);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        tr3<wd2> tr3 = this.j;
        if (tr3 != null) {
            wd2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.s;
                kd4.a((Object) imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.s;
            kd4.a((Object) imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        vw2 vw2 = this.k;
        if (vw2 != null) {
            vw2.h();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Typeface typeface;
        kd4.b(layoutInflater, "inflater");
        wd2 wd2 = (wd2) qa.a(layoutInflater, R.layout.fragment_notification_contacts, viewGroup, false, O0());
        wd2.t.setOnClickListener(new c(this));
        wd2.r.addTextChangedListener(new d(this));
        wd2.q.setOnClickListener(new e(this));
        wd2.s.setOnClickListener(new f(wd2));
        wd2.u.setIndexBarVisibility(true);
        wd2.u.setIndexBarHighLateTextVisibility(true);
        wd2.u.setIndexbarHighLateTextColor((int) R.color.greyishBrown);
        wd2.u.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        wd2.u.setIndexTextSize(9);
        wd2.u.setIndexBarTextColor((int) R.color.black);
        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_bold);
            kd4.a((Object) typeface, "resources.getFont(R.font.font_bold)");
        } else {
            typeface = Typeface.DEFAULT_BOLD;
            kd4.a((Object) typeface, "Typeface.DEFAULT_BOLD");
        }
        wd2.u.setTypeface(typeface);
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = wd2.u;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext(), 1, false));
        this.j = new tr3<>(this, wd2);
        kd4.a((Object) wd2, "binding");
        return wd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        vw2 vw2 = this.k;
        if (vw2 != null) {
            vw2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vw2 vw2 = this.k;
        if (vw2 != null) {
            vw2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(vw2 vw2) {
        kd4.b(vw2, "presenter");
        this.k = vw2;
    }

    @DexIgnore
    public void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider) {
        kd4.b(list, "listContactWrapper");
        kd4.b(filterQueryProvider, "filterQueryProvider");
        this.l = new rt2((Cursor) null, list);
        rt2 rt2 = this.l;
        if (rt2 != null) {
            rt2.a((rt2.b) new b(this));
            rt2 rt22 = this.l;
            if (rt22 != null) {
                rt22.a(filterQueryProvider);
                tr3<wd2> tr3 = this.j;
                if (tr3 != null) {
                    wd2 a2 = tr3.a();
                    if (a2 != null) {
                        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = a2.u;
                        if (alphabetFastScrollRecyclerView != null) {
                            alphabetFastScrollRecyclerView.setAdapter(this.l);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        rt2 rt2 = this.l;
        if (rt2 != null) {
            rt2.c(cursor);
        }
    }
}
