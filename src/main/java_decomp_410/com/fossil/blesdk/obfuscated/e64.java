package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e64<V> extends FutureTask<V> implements a64<i64>, f64, i64, z54 {
    @DexIgnore
    public /* final */ Object e;

    @DexIgnore
    public e64(Callable<V> callable) {
        super(callable);
        this.e = b(callable);
    }

    @DexIgnore
    public boolean b() {
        return ((a64) ((f64) d())).b();
    }

    @DexIgnore
    public Collection<i64> c() {
        return ((a64) ((f64) d())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return ((f64) d()).compareTo(obj);
    }

    @DexIgnore
    public <T extends a64<i64> & f64 & i64> T d() {
        return (a64) this.e;
    }

    @DexIgnore
    public Priority getPriority() {
        return ((f64) d()).getPriority();
    }

    @DexIgnore
    public void a(i64 i64) {
        ((a64) ((f64) d())).a(i64);
    }

    @DexIgnore
    public <T extends a64<i64> & f64 & i64> T b(Object obj) {
        if (g64.b(obj)) {
            return (a64) obj;
        }
        return new g64();
    }

    @DexIgnore
    public e64(Runnable runnable, V v) {
        super(runnable, v);
        this.e = b(runnable);
    }

    @DexIgnore
    public void a(boolean z) {
        ((i64) ((f64) d())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((i64) ((f64) d())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((i64) ((f64) d())).a(th);
    }
}
