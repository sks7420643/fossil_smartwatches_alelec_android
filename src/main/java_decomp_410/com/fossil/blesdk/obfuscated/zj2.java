package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.InstalledApp;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zj2 implements jo {
    @DexIgnore
    public /* final */ InstalledApp b;

    @DexIgnore
    public zj2(InstalledApp installedApp) {
        kd4.b(installedApp, "installedApp");
        this.b = installedApp;
    }

    @DexIgnore
    public final InstalledApp a() {
        return this.b;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        kd4.b(messageDigest, "messageDigest");
        String identifier = this.b.getIdentifier();
        kd4.a((Object) identifier, "installedApp.identifier");
        Charset charset = jo.a;
        kd4.a((Object) charset, "Key.CHARSET");
        if (identifier != null) {
            byte[] bytes = identifier.getBytes(charset);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
