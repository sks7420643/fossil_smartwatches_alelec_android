package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bb3 implements Factory<DashboardGoalTrackingPresenter> {
    @DexIgnore
    public static DashboardGoalTrackingPresenter a(za3 za3, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, h42 h42) {
        return new DashboardGoalTrackingPresenter(za3, goalTrackingRepository, userRepository, goalTrackingDao, goalTrackingDatabase, h42);
    }
}
