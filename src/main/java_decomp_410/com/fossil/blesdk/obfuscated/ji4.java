package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ji4 {
    @DexIgnore
    public static final mg4 a(fi4 fi4) {
        return new hi4(fi4);
    }

    @DexIgnore
    public static final oh4 a(fi4 fi4, oh4 oh4) {
        kd4.b(fi4, "$this$disposeOnCompletion");
        kd4.b(oh4, "handle");
        return fi4.a((xc4<? super Throwable, qa4>) new qh4(fi4, oh4));
    }

    @DexIgnore
    public static /* synthetic */ void a(CoroutineContext coroutineContext, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        ii4.a(coroutineContext, cancellationException);
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, CancellationException cancellationException) {
        kd4.b(coroutineContext, "$this$cancel");
        fi4 fi4 = (fi4) coroutineContext.get(fi4.d);
        if (fi4 != null) {
            fi4.a(cancellationException);
        }
    }
}
