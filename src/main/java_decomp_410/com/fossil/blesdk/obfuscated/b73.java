package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b73 extends v52<a73> {
    @DexIgnore
    void a(MicroApp microApp);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void b(List<Pair<MicroApp, String>> list);

    @DexIgnore
    void e(List<Pair<MicroApp, String>> list);

    @DexIgnore
    void u();
}
