package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@android.annotation.SuppressLint({"ViewConstructor"})
/* renamed from: com.fossil.blesdk.obfuscated.xg */
public class C3287xg extends android.view.View implements com.fossil.blesdk.obfuscated.C3436zg {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.view.View f10922e;

    @DexIgnore
    /* renamed from: f */
    public android.view.ViewGroup f10923f;

    @DexIgnore
    /* renamed from: g */
    public android.view.View f10924g;

    @DexIgnore
    /* renamed from: h */
    public int f10925h;

    @DexIgnore
    /* renamed from: i */
    public int f10926i;

    @DexIgnore
    /* renamed from: j */
    public int f10927j;

    @DexIgnore
    /* renamed from: k */
    public android.graphics.Matrix f10928k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ android.graphics.Matrix f10929l; // = new android.graphics.Matrix();

    @DexIgnore
    /* renamed from: m */
    public /* final */ android.view.ViewTreeObserver.OnPreDrawListener f10930m; // = new com.fossil.blesdk.obfuscated.C3287xg.C3288a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xg$a */
    public class C3288a implements android.view.ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public C3288a() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            com.fossil.blesdk.obfuscated.C3287xg xgVar = com.fossil.blesdk.obfuscated.C3287xg.this;
            xgVar.f10928k = xgVar.f10922e.getMatrix();
            com.fossil.blesdk.obfuscated.C1776f9.m6796C(com.fossil.blesdk.obfuscated.C3287xg.this);
            com.fossil.blesdk.obfuscated.C3287xg xgVar2 = com.fossil.blesdk.obfuscated.C3287xg.this;
            android.view.ViewGroup viewGroup = xgVar2.f10923f;
            if (viewGroup == null) {
                return true;
            }
            android.view.View view = xgVar2.f10924g;
            if (view == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            com.fossil.blesdk.obfuscated.C1776f9.m6796C(com.fossil.blesdk.obfuscated.C3287xg.this.f10923f);
            com.fossil.blesdk.obfuscated.C3287xg xgVar3 = com.fossil.blesdk.obfuscated.C3287xg.this;
            xgVar3.f10923f = null;
            xgVar3.f10924g = null;
            return true;
        }
    }

    @DexIgnore
    public C3287xg(android.view.View view) {
        super(view.getContext());
        this.f10922e = view;
        setLayerType(2, (android.graphics.Paint) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3436zg m16288a(android.view.View view, android.view.ViewGroup viewGroup) {
        com.fossil.blesdk.obfuscated.C3287xg a = m16287a(view);
        if (a == null) {
            android.widget.FrameLayout a2 = m16286a(viewGroup);
            if (a2 == null) {
                return null;
            }
            a = new com.fossil.blesdk.obfuscated.C3287xg(view);
            a2.addView(a);
        }
        a.f10925h++;
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m16290b(android.view.View view) {
        com.fossil.blesdk.obfuscated.C3287xg a = m16287a(view);
        if (a != null) {
            a.f10925h--;
            if (a.f10925h <= 0) {
                android.view.ViewParent parent = a.getParent();
                if (parent instanceof android.view.ViewGroup) {
                    android.view.ViewGroup viewGroup = (android.view.ViewGroup) parent;
                    viewGroup.endViewTransition(a);
                    viewGroup.removeView(a);
                }
            }
        }
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        m16289a(this.f10922e, this);
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        getLocationOnScreen(iArr);
        this.f10922e.getLocationOnScreen(iArr2);
        iArr2[0] = (int) (((float) iArr2[0]) - this.f10922e.getTranslationX());
        iArr2[1] = (int) (((float) iArr2[1]) - this.f10922e.getTranslationY());
        this.f10926i = iArr2[0] - iArr[0];
        this.f10927j = iArr2[1] - iArr[1];
        this.f10922e.getViewTreeObserver().addOnPreDrawListener(this.f10930m);
        this.f10922e.setVisibility(4);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.f10922e.getViewTreeObserver().removeOnPreDrawListener(this.f10930m);
        this.f10922e.setVisibility(0);
        m16289a(this.f10922e, (com.fossil.blesdk.obfuscated.C3287xg) null);
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(android.graphics.Canvas canvas) {
        this.f10929l.set(this.f10928k);
        this.f10929l.postTranslate((float) this.f10926i, (float) this.f10927j);
        canvas.setMatrix(this.f10929l);
        this.f10922e.draw(canvas);
    }

    @DexIgnore
    public void setVisibility(int i) {
        super.setVisibility(i);
        this.f10922e.setVisibility(i == 0 ? 4 : 0);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.widget.FrameLayout m16286a(android.view.ViewGroup viewGroup) {
        while (!(viewGroup instanceof android.widget.FrameLayout)) {
            android.view.ViewParent parent = viewGroup.getParent();
            if (!(parent instanceof android.view.ViewGroup)) {
                return null;
            }
            viewGroup = (android.view.ViewGroup) parent;
        }
        return (android.widget.FrameLayout) viewGroup;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17702a(android.view.ViewGroup viewGroup, android.view.View view) {
        this.f10923f = viewGroup;
        this.f10924g = view;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16289a(android.view.View view, com.fossil.blesdk.obfuscated.C3287xg xgVar) {
        view.setTag(com.fossil.blesdk.obfuscated.C1875gh.ghost_view, xgVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3287xg m16287a(android.view.View view) {
        return (com.fossil.blesdk.obfuscated.C3287xg) view.getTag(com.fossil.blesdk.obfuscated.C1875gh.ghost_view);
    }
}
