package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rd3 implements Factory<SleepOverviewMonthPresenter> {
    @DexIgnore
    public static SleepOverviewMonthPresenter a(pd3 pd3, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository) {
        return new SleepOverviewMonthPresenter(pd3, userRepository, sleepSummariesRepository);
    }
}
