package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mj2 extends ix3 {
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        public a(Object obj) {
            this.e = obj;
        }

        @DexIgnore
        public void run() {
            mj2.super.a(this.e);
        }
    }

    @DexIgnore
    public mj2(px3 px3) {
        super(px3);
    }

    @DexIgnore
    public void a(Object obj) {
        if (Objects.equals(Looper.myLooper(), Looper.getMainLooper())) {
            super.a(obj);
        } else {
            this.i.post(new a(obj));
        }
    }
}
