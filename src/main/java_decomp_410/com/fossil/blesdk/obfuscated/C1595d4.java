package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d4 */
public interface C1595d4 {
    @DexIgnore
    /* renamed from: a */
    float mo8510a(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: a */
    void mo8511a();

    @DexIgnore
    /* renamed from: a */
    void mo8512a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f);

    @DexIgnore
    /* renamed from: a */
    void mo8513a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.Context context, android.content.res.ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    /* renamed from: a */
    void mo8514a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.res.ColorStateList colorStateList);

    @DexIgnore
    /* renamed from: b */
    float mo8515b(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: b */
    void mo8516b(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f);

    @DexIgnore
    /* renamed from: c */
    void mo8517c(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: c */
    void mo8518c(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f);

    @DexIgnore
    /* renamed from: d */
    float mo8519d(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: e */
    android.content.res.ColorStateList mo8520e(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: f */
    void mo8521f(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: g */
    float mo8522g(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: h */
    float mo8523h(com.fossil.blesdk.obfuscated.C1535c4 c4Var);

    @DexIgnore
    /* renamed from: i */
    void mo8524i(com.fossil.blesdk.obfuscated.C1535c4 c4Var);
}
