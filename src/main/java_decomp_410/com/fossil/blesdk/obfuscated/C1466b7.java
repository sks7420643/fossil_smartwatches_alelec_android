package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b7 */
public class C1466b7 {
    @DexIgnore
    /* renamed from: a */
    public static java.io.File m4801a(android.content.Context context) {
        java.lang.String str = ".font" + android.os.Process.myPid() + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + android.os.Process.myTid() + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        int i = 0;
        while (i < 100) {
            java.io.File file = new java.io.File(context.getCacheDir(), str + i);
            try {
                if (file.createNewFile()) {
                    return file;
                }
                i++;
            } catch (java.io.IOException unused) {
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        throw r1;
     */
    @DexIgnore
    /* renamed from: a */
    public static java.nio.ByteBuffer m4804a(java.io.File file) {
        try {
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(file);
            java.nio.channels.FileChannel channel = fileInputStream.getChannel();
            java.nio.MappedByteBuffer map = channel.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0, channel.size());
            fileInputStream.close();
            return map;
        } catch (java.io.IOException unused) {
            return null;
        } catch (Throwable th) {
            r7.addSuppressed(th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0037, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r9.addSuppressed(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0040, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0043, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0044, code lost:
        if (r7 != null) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x004e, code lost:
        throw r9;
     */
    @DexIgnore
    /* renamed from: a */
    public static java.nio.ByteBuffer m4803a(android.content.Context context, android.os.CancellationSignal cancellationSignal, android.net.Uri uri) {
        try {
            android.os.ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r", cancellationSignal);
            if (openFileDescriptor == null) {
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                return null;
            }
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(openFileDescriptor.getFileDescriptor());
            java.nio.channels.FileChannel channel = fileInputStream.getChannel();
            java.nio.MappedByteBuffer map = channel.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0, channel.size());
            fileInputStream.close();
            if (openFileDescriptor != null) {
                openFileDescriptor.close();
            }
            return map;
        } catch (java.io.IOException unused) {
            return null;
        } catch (Throwable th) {
            r8.addSuppressed(th);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.nio.ByteBuffer m4802a(android.content.Context context, android.content.res.Resources resources, int i) {
        java.io.File a = m4801a(context);
        if (a == null) {
            return null;
        }
        try {
            if (!m4806a(a, resources, i)) {
                return null;
            }
            java.nio.ByteBuffer a2 = m4804a(a);
            a.delete();
            return a2;
        } finally {
            a.delete();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m4807a(java.io.File file, java.io.InputStream inputStream) {
        android.os.StrictMode.ThreadPolicy allowThreadDiskWrites = android.os.StrictMode.allowThreadDiskWrites();
        java.io.FileOutputStream fileOutputStream = null;
        try {
            java.io.FileOutputStream fileOutputStream2 = new java.io.FileOutputStream(file, false);
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read != -1) {
                        fileOutputStream2.write(bArr, 0, read);
                    } else {
                        m4805a((java.io.Closeable) fileOutputStream2);
                        android.os.StrictMode.setThreadPolicy(allowThreadDiskWrites);
                        return true;
                    }
                }
            } catch (java.io.IOException e) {
                e = e;
                fileOutputStream = fileOutputStream2;
                try {
                    android.util.Log.e("TypefaceCompatUtil", "Error copying resource contents to temp file: " + e.getMessage());
                    m4805a((java.io.Closeable) fileOutputStream);
                    android.os.StrictMode.setThreadPolicy(allowThreadDiskWrites);
                    return false;
                } catch (Throwable th) {
                    th = th;
                    m4805a((java.io.Closeable) fileOutputStream);
                    android.os.StrictMode.setThreadPolicy(allowThreadDiskWrites);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                m4805a((java.io.Closeable) fileOutputStream);
                android.os.StrictMode.setThreadPolicy(allowThreadDiskWrites);
                throw th;
            }
        } catch (java.io.IOException e2) {
            e = e2;
            android.util.Log.e("TypefaceCompatUtil", "Error copying resource contents to temp file: " + e.getMessage());
            m4805a((java.io.Closeable) fileOutputStream);
            android.os.StrictMode.setThreadPolicy(allowThreadDiskWrites);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m4806a(java.io.File file, android.content.res.Resources resources, int i) {
        java.io.InputStream inputStream;
        try {
            inputStream = resources.openRawResource(i);
            try {
                boolean a = m4807a(file, inputStream);
                m4805a((java.io.Closeable) inputStream);
                return a;
            } catch (Throwable th) {
                th = th;
                m4805a((java.io.Closeable) inputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            m4805a((java.io.Closeable) inputStream);
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4805a(java.io.Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (java.io.IOException unused) {
            }
        }
    }
}
