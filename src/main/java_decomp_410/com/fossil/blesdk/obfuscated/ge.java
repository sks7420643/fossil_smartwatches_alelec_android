package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ge implements ue {
    @DexIgnore
    public /* final */ RecyclerView.g a;

    @DexIgnore
    public ge(RecyclerView.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.a.notifyItemMoved(i, i2);
    }

    @DexIgnore
    public void b(int i, int i2) {
        this.a.notifyItemRangeInserted(i, i2);
    }

    @DexIgnore
    public void c(int i, int i2) {
        this.a.notifyItemRangeRemoved(i, i2);
    }

    @DexIgnore
    public void a(int i, int i2, Object obj) {
        this.a.notifyItemRangeChanged(i, i2, obj);
    }
}
