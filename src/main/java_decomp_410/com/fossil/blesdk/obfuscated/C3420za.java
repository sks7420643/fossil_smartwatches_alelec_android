package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.za */
public final class C3420za implements android.os.Parcelable {
    @DexIgnore
    public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3420za> CREATOR; // = new com.fossil.blesdk.obfuscated.C3420za.C3421a();

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1409ab[] f11520e;

    @DexIgnore
    /* renamed from: f */
    public int[] f11521f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C3177wa[] f11522g;

    @DexIgnore
    /* renamed from: h */
    public int f11523h; // = -1;

    @DexIgnore
    /* renamed from: i */
    public int f11524i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.za$a")
    /* renamed from: com.fossil.blesdk.obfuscated.za$a */
    public static class C3421a implements android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3420za> {
        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3420za createFromParcel(android.os.Parcel parcel) {
            return new com.fossil.blesdk.obfuscated.C3420za(parcel);
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3420za[] newArray(int i) {
            return new com.fossil.blesdk.obfuscated.C3420za[i];
        }
    }

    @DexIgnore
    public C3420za() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(android.os.Parcel parcel, int i) {
        parcel.writeTypedArray(this.f11520e, i);
        parcel.writeIntArray(this.f11521f);
        parcel.writeTypedArray(this.f11522g, i);
        parcel.writeInt(this.f11523h);
        parcel.writeInt(this.f11524i);
    }

    @DexIgnore
    public C3420za(android.os.Parcel parcel) {
        this.f11520e = (com.fossil.blesdk.obfuscated.C1409ab[]) parcel.createTypedArray(com.fossil.blesdk.obfuscated.C1409ab.CREATOR);
        this.f11521f = parcel.createIntArray();
        this.f11522g = (com.fossil.blesdk.obfuscated.C3177wa[]) parcel.createTypedArray(com.fossil.blesdk.obfuscated.C3177wa.CREATOR);
        this.f11523h = parcel.readInt();
        this.f11524i = parcel.readInt();
    }
}
