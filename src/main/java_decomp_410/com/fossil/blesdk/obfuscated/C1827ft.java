package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ft */
public class C1827ft implements com.fossil.blesdk.obfuscated.C2427mo<java.io.InputStream, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3240ws f5269a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f5270b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ft$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ft$a */
    public static class C1828a implements com.fossil.blesdk.obfuscated.C3240ws.C3242b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream f5271a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2508nw f5272b;

        @DexIgnore
        public C1828a(com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream recyclableBufferedInputStream, com.fossil.blesdk.obfuscated.C2508nw nwVar) {
            this.f5271a = recyclableBufferedInputStream;
            this.f5272b = nwVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11037a() {
            this.f5271a.mo4016y();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11038a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap) throws java.io.IOException {
            java.io.IOException y = this.f5272b.mo14140y();
            if (y != null) {
                if (bitmap != null) {
                    jqVar.mo12446a(bitmap);
                }
                throw y;
            }
        }
    }

    @DexIgnore
    public C1827ft(com.fossil.blesdk.obfuscated.C3240ws wsVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f5269a = wsVar;
        this.f5270b = gqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f5269a.mo17522a(inputStream);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(java.io.InputStream inputStream, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream recyclableBufferedInputStream;
        boolean z;
        if (inputStream instanceof com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            recyclableBufferedInputStream = new com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream(inputStream, this.f5270b);
            z = true;
        }
        com.fossil.blesdk.obfuscated.C2508nw b = com.fossil.blesdk.obfuscated.C2508nw.m11476b(recyclableBufferedInputStream);
        try {
            return this.f5269a.mo17519a((java.io.InputStream) new com.fossil.blesdk.obfuscated.C2843rw(b), i, i2, loVar, (com.fossil.blesdk.obfuscated.C3240ws.C3242b) new com.fossil.blesdk.obfuscated.C1827ft.C1828a(recyclableBufferedInputStream, b));
        } finally {
            b.mo14141z();
            if (z) {
                recyclableBufferedInputStream.mo4017z();
            }
        }
    }
}
