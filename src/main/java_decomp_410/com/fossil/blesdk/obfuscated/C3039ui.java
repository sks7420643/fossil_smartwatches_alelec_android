package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ui */
public class C3039ui extends com.fossil.blesdk.obfuscated.C2967ti {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.util.SparseIntArray f9973a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Parcel f9974b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f9975c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f9976d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f9977e;

    @DexIgnore
    /* renamed from: f */
    public int f9978f;

    @DexIgnore
    /* renamed from: g */
    public int f9979g;

    @DexIgnore
    public C3039ui(android.os.Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "");
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16427a(int i) {
        int d = mo16775d(i);
        if (d == -1) {
            return false;
        }
        this.f9974b.setDataPosition(d);
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16430b(int i) {
        mo16421a();
        this.f9978f = i;
        this.f9973a.put(i, this.f9974b.dataPosition());
        mo16437c(0);
        mo16437c(i);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16437c(int i) {
        this.f9974b.writeInt(i);
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo16775d(int i) {
        int readInt;
        do {
            int i2 = this.f9979g;
            if (i2 >= this.f9976d) {
                return -1;
            }
            this.f9974b.setDataPosition(i2);
            int readInt2 = this.f9974b.readInt();
            readInt = this.f9974b.readInt();
            this.f9979g += readInt2;
        } while (readInt != i);
        return this.f9974b.dataPosition();
    }

    @DexIgnore
    /* renamed from: e */
    public int mo16440e() {
        return this.f9974b.readInt();
    }

    @DexIgnore
    /* renamed from: f */
    public <T extends android.os.Parcelable> T mo16441f() {
        return this.f9974b.readParcelable(com.fossil.blesdk.obfuscated.C3039ui.class.getClassLoader());
    }

    @DexIgnore
    /* renamed from: g */
    public java.lang.String mo16442g() {
        return this.f9974b.readString();
    }

    @DexIgnore
    public C3039ui(android.os.Parcel parcel, int i, int i2, java.lang.String str) {
        this.f9973a = new android.util.SparseIntArray();
        this.f9978f = -1;
        this.f9979g = 0;
        this.f9974b = parcel;
        this.f9975c = i;
        this.f9976d = i2;
        this.f9979g = this.f9975c;
        this.f9977e = str;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16421a() {
        int i = this.f9978f;
        if (i >= 0) {
            int i2 = this.f9973a.get(i);
            int dataPosition = this.f9974b.dataPosition();
            this.f9974b.setDataPosition(i2);
            this.f9974b.writeInt(dataPosition - i2);
            this.f9974b.setDataPosition(dataPosition);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2967ti mo16429b() {
        android.os.Parcel parcel = this.f9974b;
        int dataPosition = parcel.dataPosition();
        int i = this.f9979g;
        if (i == this.f9975c) {
            i = this.f9976d;
        }
        return new com.fossil.blesdk.obfuscated.C3039ui(parcel, dataPosition, i, this.f9977e + "  ");
    }

    @DexIgnore
    /* renamed from: d */
    public byte[] mo16439d() {
        int readInt = this.f9974b.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.f9974b.readByteArray(bArr);
        return bArr;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16426a(byte[] bArr) {
        if (bArr != null) {
            this.f9974b.writeInt(bArr.length);
            this.f9974b.writeByteArray(bArr);
            return;
        }
        this.f9974b.writeInt(-1);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16424a(java.lang.String str) {
        this.f9974b.writeString(str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16422a(android.os.Parcelable parcelable) {
        this.f9974b.writeParcelable(parcelable, 0);
    }
}
