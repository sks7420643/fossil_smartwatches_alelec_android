package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jv1 implements Closeable {
    @DexIgnore
    public static /* final */ c h; // = (b.b() ? b.a : a.a);
    @DexIgnore
    public /* final */ c e;
    @DexIgnore
    public /* final */ Deque<Closeable> f; // = new ArrayDeque(4);
    @DexIgnore
    public Throwable g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements c {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            Logger logger = iv1.a;
            Level level = Level.WARNING;
            logger.log(level, "Suppressing exception thrown when closing " + closeable, th2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {
        @DexIgnore
        public static /* final */ b a; // = new b();
        @DexIgnore
        public static /* final */ Method b; // = a();

        @DexIgnore
        public static Method a() {
            try {
                return Throwable.class.getMethod("addSuppressed", new Class[]{Throwable.class});
            } catch (Throwable unused) {
                return null;
            }
        }

        @DexIgnore
        public static boolean b() {
            return b != null;
        }

        @DexIgnore
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            if (th != th2) {
                try {
                    b.invoke(th, new Object[]{th2});
                } catch (Throwable unused) {
                    a.a.a(closeable, th, th2);
                }
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Closeable closeable, Throwable th, Throwable th2);
    }

    @DexIgnore
    public jv1(c cVar) {
        st1.a(cVar);
        this.e = cVar;
    }

    @DexIgnore
    public static jv1 y() {
        return new jv1(h);
    }

    @DexIgnore
    public <C extends Closeable> C a(C c2) {
        if (c2 != null) {
            this.f.addFirst(c2);
        }
        return c2;
    }

    @DexIgnore
    public void close() throws IOException {
        Throwable th = this.g;
        while (!this.f.isEmpty()) {
            Closeable removeFirst = this.f.removeFirst();
            try {
                removeFirst.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.e.a(removeFirst, th, th2);
                }
            }
        }
        if (this.g == null && th != null) {
            vt1.b(th, IOException.class);
            throw new AssertionError(th);
        }
    }

    @DexIgnore
    public RuntimeException a(Throwable th) throws IOException {
        st1.a(th);
        this.g = th;
        vt1.b(th, IOException.class);
        throw new RuntimeException(th);
    }
}
