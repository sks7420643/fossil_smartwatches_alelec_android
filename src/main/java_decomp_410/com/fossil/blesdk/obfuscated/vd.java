package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.qd;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vd<T> extends qd<T> {
    @DexIgnore
    public /* final */ boolean s;
    @DexIgnore
    public /* final */ Object t;
    @DexIgnore
    public /* final */ ld<?, T> u;

    @DexIgnore
    public vd(qd<T> qdVar) {
        super(qdVar.i.n(), qdVar.e, qdVar.f, (qd.c) null, qdVar.h);
        this.u = qdVar.d();
        this.s = qdVar.g();
        this.j = qdVar.j;
        this.t = qdVar.e();
    }

    @DexIgnore
    public void a(qd<T> qdVar, qd.e eVar) {
    }

    @DexIgnore
    public ld<?, T> d() {
        return this.u;
    }

    @DexIgnore
    public Object e() {
        return this.t;
    }

    @DexIgnore
    public boolean g() {
        return this.s;
    }

    @DexIgnore
    public void h(int i) {
    }

    @DexIgnore
    public boolean h() {
        return true;
    }

    @DexIgnore
    public boolean i() {
        return true;
    }
}
