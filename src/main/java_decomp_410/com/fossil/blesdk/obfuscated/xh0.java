package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ve0;
import com.fossil.blesdk.obfuscated.ze0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xh0 extends th0<Boolean> {
    @DexIgnore
    public /* final */ ze0.a<?> b;

    @DexIgnore
    public xh0(ze0.a<?> aVar, xn1<Boolean> xn1) {
        super(4, xn1);
        this.b = aVar;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(jf0 jf0, boolean z) {
    }

    @DexIgnore
    public final wd0[] b(ve0.a<?> aVar) {
        eh0 eh0 = aVar.l().get(this.b);
        if (eh0 == null) {
            return null;
        }
        return eh0.a.c();
    }

    @DexIgnore
    public final boolean c(ve0.a<?> aVar) {
        eh0 eh0 = aVar.l().get(this.b);
        return eh0 != null && eh0.a.d();
    }

    @DexIgnore
    public final void d(ve0.a<?> aVar) throws RemoteException {
        eh0 remove = aVar.l().remove(this.b);
        if (remove != null) {
            remove.b.a(aVar.f(), this.a);
            remove.a.a();
            return;
        }
        this.a.b(false);
    }
}
