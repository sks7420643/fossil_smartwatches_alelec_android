package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vs0 {
    @DexIgnore
    public static /* final */ ConcurrentHashMap<Uri, vs0> h; // = new ConcurrentHashMap<>();
    @DexIgnore
    public static /* final */ String[] i; // = {"key", "value"};
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ ContentObserver c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public volatile Map<String, String> e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public /* final */ List<xs0> g; // = new ArrayList();

    @DexIgnore
    public vs0(ContentResolver contentResolver, Uri uri) {
        this.a = contentResolver;
        this.b = uri;
        this.c = new ws0(this, (Handler) null);
    }

    @DexIgnore
    public static vs0 a(ContentResolver contentResolver, Uri uri) {
        vs0 vs0 = h.get(uri);
        if (vs0 != null) {
            return vs0;
        }
        vs0 vs02 = new vs0(contentResolver, uri);
        vs0 putIfAbsent = h.putIfAbsent(uri, vs02);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        vs02.a.registerContentObserver(vs02.b, false, vs02.c);
        return vs02;
    }

    @DexIgnore
    public final Map<String, String> a() {
        Map<String, String> c2 = ys0.a("gms:phenotype:phenotype_flag:debug_disable_caching", false) ? c() : this.e;
        if (c2 == null) {
            synchronized (this.d) {
                Map<String, String> map = this.e;
                if (map == null) {
                    map = c();
                    this.e = map;
                }
            }
        }
        return c2 != null ? c2 : Collections.emptyMap();
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d) {
            this.e = null;
        }
    }

    @DexIgnore
    public final Map<String, String> c() {
        Cursor query;
        try {
            HashMap hashMap = new HashMap();
            query = this.a.query(this.b, i, (String) null, (String[]) null, (String) null);
            if (query != null) {
                while (query.moveToNext()) {
                    hashMap.put(query.getString(0), query.getString(1));
                }
                query.close();
            }
            return hashMap;
        } catch (SQLiteException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            return null;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    @DexIgnore
    public final void d() {
        synchronized (this.f) {
            for (xs0 l : this.g) {
                l.l();
            }
        }
    }
}
