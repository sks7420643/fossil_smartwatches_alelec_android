package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class aj0<T> implements Iterator<T> {
    @DexIgnore
    public /* final */ zi0<T> e;
    @DexIgnore
    public int f; // = -1;

    @DexIgnore
    public aj0(zi0<T> zi0) {
        bk0.a(zi0);
        this.e = zi0;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.f < this.e.getCount() - 1;
    }

    @DexIgnore
    public T next() {
        if (hasNext()) {
            zi0<T> zi0 = this.e;
            int i = this.f + 1;
            this.f = i;
            return zi0.get(i);
        }
        int i2 = this.f;
        StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
