package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gs0 extends oj0<hs0> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public gs0(Context context, Looper looper, kj0 kj0, sb0 sb0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 16, kj0, bVar, cVar);
        if (sb0 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof hs0) {
            return (hs0) queryLocalInterface;
        }
        return new is0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final boolean l() {
        kj0 F = F();
        return !TextUtils.isEmpty(F.b()) && !F.a((de0<?>) rb0.c).isEmpty();
    }

    @DexIgnore
    public final Bundle u() {
        return this.E;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.service.START";
    }
}
