package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.command.BluetoothCommand;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class a70 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[BluetoothCommand.Result.ResultCode.values().length];

    /*
    static {
        a[BluetoothCommand.Result.ResultCode.SUCCESS.ordinal()] = 1;
        a[BluetoothCommand.Result.ResultCode.INTERRUPTED.ordinal()] = 2;
        a[BluetoothCommand.Result.ResultCode.INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT.ordinal()] = 3;
        a[BluetoothCommand.Result.ResultCode.CONNECTION_DROPPED.ordinal()] = 4;
        a[BluetoothCommand.Result.ResultCode.BLUETOOTH_OFF.ordinal()] = 5;
        a[BluetoothCommand.Result.ResultCode.HID_PROXY_NOT_CONNECTED.ordinal()] = 6;
        a[BluetoothCommand.Result.ResultCode.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.ordinal()] = 7;
        a[BluetoothCommand.Result.ResultCode.HID_INPUT_DEVICE_DISABLED.ordinal()] = 8;
        a[BluetoothCommand.Result.ResultCode.HID_UNKNOWN_ERROR.ordinal()] = 9;
        a[BluetoothCommand.Result.ResultCode.GATT_ERROR.ordinal()] = 10;
    }
    */
}
