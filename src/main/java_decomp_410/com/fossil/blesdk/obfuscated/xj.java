package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import com.fossil.blesdk.obfuscated.ak;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xj implements mj {
    @DexIgnore
    public static /* final */ String h; // = dj.a("CommandHandler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Map<String, mj> f; // = new HashMap();
    @DexIgnore
    public /* final */ Object g; // = new Object();

    @DexIgnore
    public xj(Context context) {
        this.e = context;
    }

    @DexIgnore
    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent c(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public final void d(Intent intent, int i, ak akVar) {
        dj.a().a(h, String.format("Handling reschedule %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        akVar.e().j();
    }

    @DexIgnore
    public final void e(Intent intent, int i, ak akVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        dj.a().a(h, String.format("Handling schedule work for %s", new Object[]{string}), new Throwable[0]);
        WorkDatabase g2 = akVar.e().g();
        g2.beginTransaction();
        try {
            hl e2 = g2.d().e(string);
            if (e2 == null) {
                dj a = dj.a();
                String str = h;
                a.e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (e2.b.isFinished()) {
                dj a2 = dj.a();
                String str2 = h;
                a2.e(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                g2.endTransaction();
            } else {
                long a3 = e2.a();
                if (!e2.b()) {
                    dj.a().a(h, String.format("Setting up Alarms for %s at %s", new Object[]{string, Long.valueOf(a3)}), new Throwable[0]);
                    wj.a(this.e, akVar.e(), string, a3);
                } else {
                    dj.a().a(h, String.format("Opportunistically setting an alarm for %s at %s", new Object[]{string, Long.valueOf(a3)}), new Throwable[0]);
                    wj.a(this.e, akVar.e(), string, a3);
                    akVar.a((Runnable) new ak.b(akVar, a(this.e), i));
                }
                g2.setTransactionSuccessful();
                g2.endTransaction();
            }
        } finally {
            g2.endTransaction();
        }
    }

    @DexIgnore
    public final void f(Intent intent, int i, ak akVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        dj.a().a(h, String.format("Handing stopWork work for %s", new Object[]{string}), new Throwable[0]);
        akVar.e().b(string);
        wj.a(this.e, akVar.e(), string);
        akVar.a(string, false);
    }

    @DexIgnore
    public void g(Intent intent, int i, ak akVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            a(intent, i, akVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            d(intent, i, akVar);
        } else if (!a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            dj.a().b(h, String.format("Invalid request for %s, requires %s.", new Object[]{action, "KEY_WORKSPEC_ID"}), new Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            e(intent, i, akVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            b(intent, i, akVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            f(intent, i, akVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            c(intent, i, akVar);
        } else {
            dj.a().e(h, String.format("Ignoring intent %s", new Object[]{intent}), new Throwable[0]);
        }
    }

    @DexIgnore
    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    public final void c(Intent intent, int i, ak akVar) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        dj.a().a(h, String.format("Handling onExecutionCompleted %s, %s", new Object[]{intent, Integer.valueOf(i)}), new Throwable[0]);
        a(string, z);
    }

    @DexIgnore
    public static Intent a(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    public final void b(Intent intent, int i, ak akVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.g) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            dj.a().a(h, String.format("Handing delay met for %s", new Object[]{string}), new Throwable[0]);
            if (!this.f.containsKey(string)) {
                zj zjVar = new zj(this.e, i, string, akVar);
                this.f.put(string, zjVar);
                zjVar.b();
            } else {
                dj.a().a(h, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", new Object[]{string}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.g) {
            mj remove = this.f.remove(str);
            if (remove != null) {
                remove.a(str, z);
            }
        }
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.g) {
            z = !this.f.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public final void a(Intent intent, int i, ak akVar) {
        dj.a().a(h, String.format("Handling constraints changed %s", new Object[]{intent}), new Throwable[0]);
        new yj(this.e, i, akVar).a();
    }

    @DexIgnore
    public static boolean a(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
