package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wl */
public class C3228wl {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f10656a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WakeLocks");

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.WeakHashMap<android.os.PowerManager.WakeLock, java.lang.String> f10657b; // = new java.util.WeakHashMap<>();

    @DexIgnore
    /* renamed from: a */
    public static android.os.PowerManager.WakeLock m15862a(android.content.Context context, java.lang.String str) {
        java.lang.String str2 = "WorkManager: " + str;
        android.os.PowerManager.WakeLock newWakeLock = ((android.os.PowerManager) context.getApplicationContext().getSystemService("power")).newWakeLock(1, str2);
        synchronized (f10657b) {
            f10657b.put(newWakeLock, str2);
        }
        return newWakeLock;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15863a() {
        java.util.HashMap hashMap = new java.util.HashMap();
        synchronized (f10657b) {
            hashMap.putAll(f10657b);
        }
        for (android.os.PowerManager.WakeLock wakeLock : hashMap.keySet()) {
            if (wakeLock != null && wakeLock.isHeld()) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9966e(f10656a, java.lang.String.format("WakeLock held for %s", new java.lang.Object[]{hashMap.get(wakeLock)}), new java.lang.Throwable[0]);
            }
        }
    }
}
