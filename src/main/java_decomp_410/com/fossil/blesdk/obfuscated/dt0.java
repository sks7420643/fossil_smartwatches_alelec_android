package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dt0 extends ys0<Boolean> {
    @DexIgnore
    public dt0(it0 it0, String str, Boolean bool) {
        super(it0, str, bool, (ct0) null);
    }

    @DexIgnore
    public final /* synthetic */ Object a(String str) {
        if (wy0.c.matcher(str).matches()) {
            return true;
        }
        if (wy0.d.matcher(str).matches()) {
            return false;
        }
        String str2 = this.b;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 28 + String.valueOf(str).length());
        sb.append("Invalid boolean value for ");
        sb.append(str2);
        sb.append(": ");
        sb.append(str);
        Log.e("PhenotypeFlag", sb.toString());
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public final Boolean a(SharedPreferences sharedPreferences) {
        try {
            return Boolean.valueOf(sharedPreferences.getBoolean(this.b, false));
        } catch (ClassCastException e) {
            String valueOf = String.valueOf(this.b);
            Log.e("PhenotypeFlag", valueOf.length() != 0 ? "Invalid boolean value in SharedPreferences for ".concat(valueOf) : new String("Invalid boolean value in SharedPreferences for "), e);
            return null;
        }
    }
}
