package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fm2 {
    @DexIgnore
    public Vector<jm2> a;
    @DexIgnore
    public Map<String, jm2> b;
    @DexIgnore
    public Map<String, jm2> c;
    @DexIgnore
    public Map<String, jm2> d;
    @DexIgnore
    public Map<String, jm2> e;

    @DexIgnore
    public fm2() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public boolean a(jm2 jm2) {
        if (jm2 != null) {
            b(jm2);
            return this.a.add(jm2);
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(jm2 jm2) {
        byte[] a2 = jm2.a();
        if (a2 != null) {
            this.b.put(new String(a2), jm2);
        }
        byte[] b2 = jm2.b();
        if (b2 != null) {
            this.c.put(new String(b2), jm2);
        }
        byte[] f = jm2.f();
        if (f != null) {
            this.d.put(new String(f), jm2);
        }
        byte[] e2 = jm2.e();
        if (e2 != null) {
            this.e.put(new String(e2), jm2);
        }
    }

    @DexIgnore
    public void a(int i, jm2 jm2) {
        if (jm2 != null) {
            b(jm2);
            this.a.add(i, jm2);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }

    @DexIgnore
    public jm2 a(int i) {
        return this.a.get(i);
    }
}
