package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tn */
public class C2977tn extends android.content.ContextWrapper {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ com.fossil.blesdk.obfuscated.C3371yn<?, ?> f9726k; // = new com.fossil.blesdk.obfuscated.C2753qn();

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f9727a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.bumptech.glide.Registry f9728b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C3470zv f9729c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2815rn.C2816a f9730d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> f9731e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C3371yn<?, ?>> f9732f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2755qp f9733g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ boolean f9734h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f9735i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2842rv f9736j;

    @DexIgnore
    public C2977tn(android.content.Context context, com.fossil.blesdk.obfuscated.C1885gq gqVar, com.bumptech.glide.Registry registry, com.fossil.blesdk.obfuscated.C3470zv zvVar, com.fossil.blesdk.obfuscated.C2815rn.C2816a aVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C3371yn<?, ?>> map, java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> list, com.fossil.blesdk.obfuscated.C2755qp qpVar, boolean z, int i) {
        super(context.getApplicationContext());
        this.f9727a = gqVar;
        this.f9728b = registry;
        this.f9729c = zvVar;
        this.f9730d = aVar;
        this.f9731e = list;
        this.f9732f = map;
        this.f9733g = qpVar;
        this.f9734h = z;
        this.f9735i = i;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> com.fossil.blesdk.obfuscated.C3371yn<?, T> mo16512a(java.lang.Class<T> cls) {
        com.fossil.blesdk.obfuscated.C3371yn<?, T> ynVar = this.f9732f.get(cls);
        if (ynVar == null) {
            for (java.util.Map.Entry next : this.f9732f.entrySet()) {
                if (((java.lang.Class) next.getKey()).isAssignableFrom(cls)) {
                    ynVar = (com.fossil.blesdk.obfuscated.C3371yn) next.getValue();
                }
            }
        }
        return ynVar == null ? f9726k : ynVar;
    }

    @DexIgnore
    /* renamed from: b */
    public java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> mo16513b() {
        return this.f9731e;
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized com.fossil.blesdk.obfuscated.C2842rv mo16514c() {
        if (this.f9736j == null) {
            this.f9736j = (com.fossil.blesdk.obfuscated.C2842rv) this.f9730d.build().mo13423O();
        }
        return this.f9736j;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2755qp mo16515d() {
        return this.f9733g;
    }

    @DexIgnore
    /* renamed from: e */
    public int mo16516e() {
        return this.f9735i;
    }

    @DexIgnore
    /* renamed from: f */
    public com.bumptech.glide.Registry mo16517f() {
        return this.f9728b;
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo16518g() {
        return this.f9734h;
    }

    @DexIgnore
    /* renamed from: a */
    public <X> com.fossil.blesdk.obfuscated.C1583cw<android.widget.ImageView, X> mo16510a(android.widget.ImageView imageView, java.lang.Class<X> cls) {
        return this.f9729c.mo18615a(imageView, cls);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1885gq mo16511a() {
        return this.f9727a;
    }
}
