package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kf3 implements MembersInjector<HeartRateDetailActivity> {
    @DexIgnore
    public static void a(HeartRateDetailActivity heartRateDetailActivity, HeartRateDetailPresenter heartRateDetailPresenter) {
        heartRateDetailActivity.B = heartRateDetailPresenter;
    }
}
