package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.er */
public final class C1743er {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.er$a")
    /* renamed from: com.fossil.blesdk.obfuscated.er$a */
    public class C1744a implements java.io.FilenameFilter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ java.util.regex.Pattern f4922a;

        @DexIgnore
        public C1744a(java.util.regex.Pattern pattern) {
            this.f4922a = pattern;
        }

        @DexIgnore
        public boolean accept(java.io.File file, java.lang.String str) {
            return this.f4922a.matcher(str).matches();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m6567a() {
        int availableProcessors = java.lang.Runtime.getRuntime().availableProcessors();
        return android.os.Build.VERSION.SDK_INT < 17 ? java.lang.Math.max(m6568b(), availableProcessors) : availableProcessors;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public static int m6568b() {
        java.io.File[] fileArr;
        android.os.StrictMode.ThreadPolicy allowThreadDiskReads = android.os.StrictMode.allowThreadDiskReads();
        try {
            fileArr = new java.io.File("/sys/devices/system/cpu/").listFiles(new com.fossil.blesdk.obfuscated.C1743er.C1744a(java.util.regex.Pattern.compile("cpu[0-9]+")));
            android.os.StrictMode.setThreadPolicy(allowThreadDiskReads);
        } catch (Throwable th) {
            android.os.StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        return java.lang.Math.max(1, fileArr != null ? fileArr.length : 0);
    }
}
