package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.error.Error;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f90<V> extends g90<V> {
    @DexIgnore
    public f90<V> f(xc4<? super qa4, qa4> xc4) {
        kd4.b(xc4, "actionOnFinal");
        g90<V> f = super.f(xc4);
        if (f != null) {
            return (f90) f;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public f90<V> i(xc4<? super Float, qa4> xc4) {
        kd4.b(xc4, "actionOnProgressChange");
        g90<V> d = super.d(xc4);
        if (d != null) {
            return (f90) d;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public f90<V> g(xc4<? super Error, qa4> xc4) {
        kd4.b(xc4, "actionOnError");
        g90<V> g = super.c(xc4);
        if (g != null) {
            return (f90) g;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public f90<V> h(xc4<? super V, qa4> xc4) {
        kd4.b(xc4, "actionOnSuccess");
        g90<V> h = super.e(xc4);
        if (h != null) {
            return (f90) h;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    public f90<V> b(xc4<? super Error, qa4> xc4) {
        kd4.b(xc4, "actionOnCancel");
        g90<V> b = super.b(xc4);
        if (b != null) {
            return (f90) b;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }
}
