package com.fossil.blesdk.obfuscated;

import android.util.Base64;
import java.security.KeyPair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dz1 {
    @DexIgnore
    public /* final */ KeyPair a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public dz1(KeyPair keyPair, long j) {
        this.a = keyPair;
        this.b = j;
    }

    @DexIgnore
    public final KeyPair a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return Base64.encodeToString(this.a.getPublic().getEncoded(), 11);
    }

    @DexIgnore
    public final String c() {
        return Base64.encodeToString(this.a.getPrivate().getEncoded(), 11);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof dz1)) {
            return false;
        }
        dz1 dz1 = (dz1) obj;
        if (this.b != dz1.b || !this.a.getPublic().equals(dz1.a.getPublic()) || !this.a.getPrivate().equals(dz1.a.getPrivate())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.a.getPublic(), this.a.getPrivate(), Long.valueOf(this.b));
    }
}
