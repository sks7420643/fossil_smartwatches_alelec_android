package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.te */
public class C2961te extends com.fossil.blesdk.obfuscated.C1479bf {

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3214we f9647d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3214we f9648e;

    @DexIgnore
    /* renamed from: a */
    public int[] mo9164a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, android.view.View view) {
        int[] iArr = new int[2];
        if (mVar.mo2426a()) {
            iArr[0] = mo16375a(mVar, view, mo16378d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.mo2437b()) {
            iArr[1] = mo16375a(mVar, view, mo16379e(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    /* renamed from: b */
    public final android.view.View mo16377b(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int i;
        int e = mVar.mo2941e();
        android.view.View view = null;
        if (e == 0) {
            return null;
        }
        if (mVar.mo2948f()) {
            i = weVar.mo17440f() + (weVar.mo17442g() / 2);
        } else {
            i = weVar.mo17429a() / 2;
        }
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < e; i3++) {
            android.view.View d = mVar.mo2936d(i3);
            int abs = java.lang.Math.abs((weVar.mo17437d(d) + (weVar.mo17433b(d) / 2)) - i);
            if (abs < i2) {
                view = d;
                i2 = abs;
            }
        }
        return view;
    }

    @DexIgnore
    /* renamed from: c */
    public android.view.View mo9169c(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        if (mVar.mo2437b()) {
            return mo16377b(mVar, mo16379e(mVar));
        }
        if (mVar.mo2426a()) {
            return mo16377b(mVar, mo16378d(mVar));
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.C3214we mo16378d(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        com.fossil.blesdk.obfuscated.C3214we weVar = this.f9648e;
        if (weVar == null || weVar.f10613a != mVar) {
            this.f9648e = com.fossil.blesdk.obfuscated.C3214we.m15739a(mVar);
        }
        return this.f9648e;
    }

    @DexIgnore
    /* renamed from: e */
    public final com.fossil.blesdk.obfuscated.C3214we mo16379e(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        com.fossil.blesdk.obfuscated.C3214we weVar = this.f9647d;
        if (weVar == null || weVar.f10613a != mVar) {
            this.f9647d = com.fossil.blesdk.obfuscated.C3214we.m15741b(mVar);
        }
        return this.f9647d;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo9160a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, int i, int i2) {
        int i3;
        int i4;
        if (!(mVar instanceof androidx.recyclerview.widget.RecyclerView.C0251v.C0253b)) {
            return -1;
        }
        int j = mVar.mo2957j();
        if (j == 0) {
            return -1;
        }
        android.view.View c = mo9169c(mVar);
        if (c == null) {
            return -1;
        }
        int l = mVar.mo2962l(c);
        if (l == -1) {
            return -1;
        }
        int i5 = j - 1;
        android.graphics.PointF a = ((androidx.recyclerview.widget.RecyclerView.C0251v.C0253b) mVar).mo2410a(i5);
        if (a == null) {
            return -1;
        }
        if (mVar.mo2426a()) {
            i3 = mo16376a(mVar, mo16378d(mVar), i, 0);
            if (a.x < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i3 = -i3;
            }
        } else {
            i3 = 0;
        }
        if (mVar.mo2437b()) {
            i4 = mo16376a(mVar, mo16379e(mVar), 0, i2);
            if (a.y < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i4 = -i4;
            }
        } else {
            i4 = 0;
        }
        if (mVar.mo2437b()) {
            i3 = i4;
        }
        if (i3 == 0) {
            return -1;
        }
        int i6 = l + i3;
        if (i6 < 0) {
            i6 = 0;
        }
        return i6 >= j ? i5 : i6;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo16375a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, android.view.View view, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int i;
        int d = weVar.mo17437d(view) + (weVar.mo17433b(view) / 2);
        if (mVar.mo2948f()) {
            i = weVar.mo17440f() + (weVar.mo17442g() / 2);
        } else {
            i = weVar.mo17429a() / 2;
        }
        return d - i;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo16376a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we weVar, int i, int i2) {
        int[] b = mo9168b(i, i2);
        float a = mo16374a(mVar, weVar);
        if (a <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return 0;
        }
        return java.lang.Math.round(((float) (java.lang.Math.abs(b[0]) > java.lang.Math.abs(b[1]) ? b[0] : b[1])) / a);
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo16374a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int e = mVar.mo2941e();
        if (e == 0) {
            return 1.0f;
        }
        android.view.View view = null;
        android.view.View view2 = null;
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < e; i3++) {
            android.view.View d = mVar.mo2936d(i3);
            int l = mVar.mo2962l(d);
            if (l != -1) {
                if (l < i) {
                    view = d;
                    i = l;
                }
                if (l > i2) {
                    view2 = d;
                    i2 = l;
                }
            }
        }
        if (view == null || view2 == null) {
            return 1.0f;
        }
        int max = java.lang.Math.max(weVar.mo17430a(view), weVar.mo17430a(view2)) - java.lang.Math.min(weVar.mo17437d(view), weVar.mo17437d(view2));
        if (max == 0) {
            return 1.0f;
        }
        return (((float) max) * 1.0f) / ((float) ((i2 - i) + 1));
    }
}
