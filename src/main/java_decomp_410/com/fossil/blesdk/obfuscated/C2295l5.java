package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l5 */
public final class C2295l5 {
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_Layout; // = {16842948, 16843039, 16843040, 16843071, 16843072, com.fossil.wearables.fossil.R.attr.barrierAllowsGoneWidgets, com.fossil.wearables.fossil.R.attr.barrierDirection, com.fossil.wearables.fossil.R.attr.chainUseRtl, com.fossil.wearables.fossil.R.attr.constraintSet, com.fossil.wearables.fossil.R.attr.constraint_referenced_ids, com.fossil.wearables.fossil.R.attr.layout_constrainedHeight, com.fossil.wearables.fossil.R.attr.layout_constrainedWidth, com.fossil.wearables.fossil.R.attr.layout_constraintBaseline_creator, com.fossil.wearables.fossil.R.attr.layout_constraintBaseline_toBaselineOf, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_creator, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_toBottomOf, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_toTopOf, com.fossil.wearables.fossil.R.attr.layout_constraintCircle, com.fossil.wearables.fossil.R.attr.layout_constraintCircleAngle, com.fossil.wearables.fossil.R.attr.layout_constraintCircleRadius, com.fossil.wearables.fossil.R.attr.layout_constraintDimensionRatio, com.fossil.wearables.fossil.R.attr.layout_constraintEnd_toEndOf, com.fossil.wearables.fossil.R.attr.layout_constraintEnd_toStartOf, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_begin, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_end, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_percent, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_default, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_max, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_min, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_percent, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_bias, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_chainStyle, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_weight, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_creator, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_toLeftOf, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_toRightOf, com.fossil.wearables.fossil.R.attr.layout_constraintRight_creator, com.fossil.wearables.fossil.R.attr.layout_constraintRight_toLeftOf, com.fossil.wearables.fossil.R.attr.layout_constraintRight_toRightOf, com.fossil.wearables.fossil.R.attr.layout_constraintStart_toEndOf, com.fossil.wearables.fossil.R.attr.layout_constraintStart_toStartOf, com.fossil.wearables.fossil.R.attr.layout_constraintTop_creator, com.fossil.wearables.fossil.R.attr.layout_constraintTop_toBottomOf, com.fossil.wearables.fossil.R.attr.layout_constraintTop_toTopOf, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_bias, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_chainStyle, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_weight, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_default, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_max, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_min, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_percent, com.fossil.wearables.fossil.R.attr.layout_editor_absoluteX, com.fossil.wearables.fossil.R.attr.layout_editor_absoluteY, com.fossil.wearables.fossil.R.attr.layout_goneMarginBottom, com.fossil.wearables.fossil.R.attr.layout_goneMarginEnd, com.fossil.wearables.fossil.R.attr.layout_goneMarginLeft, com.fossil.wearables.fossil.R.attr.layout_goneMarginRight, com.fossil.wearables.fossil.R.attr.layout_goneMarginStart, com.fossil.wearables.fossil.R.attr.layout_goneMarginTop, com.fossil.wearables.fossil.R.attr.layout_optimizationLevel};
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxHeight; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minHeight; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_minWidth; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierAllowsGoneWidgets; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_barrierDirection; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_chainUseRtl; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraintSet; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_constraint_referenced_ids; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constrainedWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_creator; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_creator; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintBottom_toTopOf; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircle; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleAngle; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintCircleRadius; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintDimensionRatio; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toEndOf; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintEnd_toStartOf; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_begin; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_end; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintGuide_percent; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_default; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_max; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_min; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHeight_percent; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_bias; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintHorizontal_weight; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintLeft_toRightOf; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_creator; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toLeftOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintRight_toRightOf; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toEndOf; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintStart_toStartOf; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_creator; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toBottomOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintTop_toTopOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_bias; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_chainStyle; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintVertical_weight; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_constraintWidth_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteX; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_editor_absoluteY; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginBottom; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginEnd; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginLeft; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginRight; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginStart; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_goneMarginTop; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintLayout_Layout_layout_optimizationLevel; // = 59;
    @DexIgnore
    public static /* final */ int[] ConstraintLayout_placeholder; // = {com.fossil.wearables.fossil.R.attr.content, com.fossil.wearables.fossil.R.attr.emptyVisibility};
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_content; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintLayout_placeholder_emptyVisibility; // = 1;
    @DexIgnore
    public static /* final */ int[] ConstraintSet; // = {16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, com.fossil.wearables.fossil.R.attr.barrierAllowsGoneWidgets, com.fossil.wearables.fossil.R.attr.barrierDirection, com.fossil.wearables.fossil.R.attr.chainUseRtl, com.fossil.wearables.fossil.R.attr.constraint_referenced_ids, com.fossil.wearables.fossil.R.attr.layout_constrainedHeight, com.fossil.wearables.fossil.R.attr.layout_constrainedWidth, com.fossil.wearables.fossil.R.attr.layout_constraintBaseline_creator, com.fossil.wearables.fossil.R.attr.layout_constraintBaseline_toBaselineOf, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_creator, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_toBottomOf, com.fossil.wearables.fossil.R.attr.layout_constraintBottom_toTopOf, com.fossil.wearables.fossil.R.attr.layout_constraintCircle, com.fossil.wearables.fossil.R.attr.layout_constraintCircleAngle, com.fossil.wearables.fossil.R.attr.layout_constraintCircleRadius, com.fossil.wearables.fossil.R.attr.layout_constraintDimensionRatio, com.fossil.wearables.fossil.R.attr.layout_constraintEnd_toEndOf, com.fossil.wearables.fossil.R.attr.layout_constraintEnd_toStartOf, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_begin, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_end, com.fossil.wearables.fossil.R.attr.layout_constraintGuide_percent, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_default, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_max, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_min, com.fossil.wearables.fossil.R.attr.layout_constraintHeight_percent, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_bias, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_chainStyle, com.fossil.wearables.fossil.R.attr.layout_constraintHorizontal_weight, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_creator, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_toLeftOf, com.fossil.wearables.fossil.R.attr.layout_constraintLeft_toRightOf, com.fossil.wearables.fossil.R.attr.layout_constraintRight_creator, com.fossil.wearables.fossil.R.attr.layout_constraintRight_toLeftOf, com.fossil.wearables.fossil.R.attr.layout_constraintRight_toRightOf, com.fossil.wearables.fossil.R.attr.layout_constraintStart_toEndOf, com.fossil.wearables.fossil.R.attr.layout_constraintStart_toStartOf, com.fossil.wearables.fossil.R.attr.layout_constraintTop_creator, com.fossil.wearables.fossil.R.attr.layout_constraintTop_toBottomOf, com.fossil.wearables.fossil.R.attr.layout_constraintTop_toTopOf, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_bias, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_chainStyle, com.fossil.wearables.fossil.R.attr.layout_constraintVertical_weight, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_default, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_max, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_min, com.fossil.wearables.fossil.R.attr.layout_constraintWidth_percent, com.fossil.wearables.fossil.R.attr.layout_editor_absoluteX, com.fossil.wearables.fossil.R.attr.layout_editor_absoluteY, com.fossil.wearables.fossil.R.attr.layout_goneMarginBottom, com.fossil.wearables.fossil.R.attr.layout_goneMarginEnd, com.fossil.wearables.fossil.R.attr.layout_goneMarginLeft, com.fossil.wearables.fossil.R.attr.layout_goneMarginRight, com.fossil.wearables.fossil.R.attr.layout_goneMarginStart, com.fossil.wearables.fossil.R.attr.layout_goneMarginTop};
    @DexIgnore
    public static /* final */ int ConstraintSet_android_alpha; // = 13;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_elevation; // = 26;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_id; // = 1;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_height; // = 4;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginBottom; // = 8;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginEnd; // = 24;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginLeft; // = 5;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginRight; // = 7;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginStart; // = 23;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_marginTop; // = 6;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_layout_width; // = 3;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxHeight; // = 10;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_maxWidth; // = 9;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minHeight; // = 12;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_minWidth; // = 11;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_orientation; // = 0;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotation; // = 20;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationX; // = 21;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_rotationY; // = 22;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleX; // = 18;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_scaleY; // = 19;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotX; // = 14;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_transformPivotY; // = 15;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationX; // = 16;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationY; // = 17;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_translationZ; // = 25;
    @DexIgnore
    public static /* final */ int ConstraintSet_android_visibility; // = 2;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierAllowsGoneWidgets; // = 27;
    @DexIgnore
    public static /* final */ int ConstraintSet_barrierDirection; // = 28;
    @DexIgnore
    public static /* final */ int ConstraintSet_chainUseRtl; // = 29;
    @DexIgnore
    public static /* final */ int ConstraintSet_constraint_referenced_ids; // = 30;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedHeight; // = 31;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constrainedWidth; // = 32;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_creator; // = 33;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBaseline_toBaselineOf; // = 34;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_creator; // = 35;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toBottomOf; // = 36;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintBottom_toTopOf; // = 37;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircle; // = 38;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleAngle; // = 39;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintCircleRadius; // = 40;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintDimensionRatio; // = 41;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toEndOf; // = 42;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintEnd_toStartOf; // = 43;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_begin; // = 44;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_end; // = 45;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintGuide_percent; // = 46;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_default; // = 47;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_max; // = 48;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_min; // = 49;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHeight_percent; // = 50;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_bias; // = 51;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_chainStyle; // = 52;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintHorizontal_weight; // = 53;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_creator; // = 54;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toLeftOf; // = 55;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintLeft_toRightOf; // = 56;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_creator; // = 57;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toLeftOf; // = 58;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintRight_toRightOf; // = 59;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toEndOf; // = 60;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintStart_toStartOf; // = 61;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_creator; // = 62;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toBottomOf; // = 63;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintTop_toTopOf; // = 64;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_bias; // = 65;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_chainStyle; // = 66;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintVertical_weight; // = 67;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_default; // = 68;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_max; // = 69;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_min; // = 70;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_constraintWidth_percent; // = 71;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteX; // = 72;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_editor_absoluteY; // = 73;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginBottom; // = 74;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginEnd; // = 75;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginLeft; // = 76;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginRight; // = 77;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginStart; // = 78;
    @DexIgnore
    public static /* final */ int ConstraintSet_layout_goneMarginTop; // = 79;
    @DexIgnore
    public static /* final */ int[] LinearConstraintLayout; // = {16842948};
    @DexIgnore
    public static /* final */ int LinearConstraintLayout_android_orientation; // = 0;
}
