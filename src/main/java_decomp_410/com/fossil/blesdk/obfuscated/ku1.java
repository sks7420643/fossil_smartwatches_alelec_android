package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ku1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends du1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;
        @DexIgnore
        public /* final */ /* synthetic */ tt1 g;

        @DexIgnore
        public a(Iterable iterable, tt1 tt1) {
            this.f = iterable;
            this.g = tt1;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return lu1.c(this.f.iterator(), this.g);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends du1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable f;
        @DexIgnore
        public /* final */ /* synthetic */ ot1 g;

        @DexIgnore
        public b(Iterable iterable, ot1 ot1) {
            this.f = iterable;
            this.g = ot1;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return lu1.a(this.f.iterator(), this.g);
        }
    }

    @DexIgnore
    public static <T> T[] a(Iterable<? extends T> iterable, T[] tArr) {
        return a(iterable).toArray(tArr);
    }

    @DexIgnore
    public static <T> T b(Iterable<T> iterable) {
        return lu1.b(iterable.iterator());
    }

    @DexIgnore
    public static Object[] c(Iterable<?> iterable) {
        return a(iterable).toArray();
    }

    @DexIgnore
    public static String d(Iterable<?> iterable) {
        return lu1.d(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> b(Iterable<T> iterable, tt1<? super T> tt1) {
        st1.a(iterable);
        st1.a(tt1);
        return new a(iterable, tt1);
    }

    @DexIgnore
    public static <E> Collection<E> a(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : Lists.a(iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(cu1.a(iterable));
        }
        st1.a(iterable);
        return lu1.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Iterable<T> iterable, tt1<? super T> tt1) {
        return lu1.a(iterable.iterator(), tt1);
    }

    @DexIgnore
    public static <F, T> Iterable<T> a(Iterable<F> iterable, ot1<? super F, ? extends T> ot1) {
        st1.a(iterable);
        st1.a(ot1);
        return new b(iterable, ot1);
    }

    @DexIgnore
    public static <T> T a(Iterable<? extends T> iterable, T t) {
        return lu1.b(iterable.iterator(), t);
    }
}
