package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s01 extends nz0 implements r01 {
    @DexIgnore
    public s01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitBleApi");
    }
}
