package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wa3 implements Factory<CaloriesOverviewWeekPresenter> {
    @DexIgnore
    public static CaloriesOverviewWeekPresenter a(ua3 ua3, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new CaloriesOverviewWeekPresenter(ua3, userRepository, summariesRepository);
    }
}
