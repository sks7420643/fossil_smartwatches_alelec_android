package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface aq<Z> {
    @DexIgnore
    void a();

    @DexIgnore
    int b();

    @DexIgnore
    Class<Z> c();

    @DexIgnore
    Z get();
}
