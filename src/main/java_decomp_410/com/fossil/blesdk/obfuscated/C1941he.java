package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.he */
public final class C1941he<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.Executor f5723a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2314le.C2318d<T> f5724b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.he$a")
    /* renamed from: com.fossil.blesdk.obfuscated.he$a */
    public static final class C1942a<T> {

        @DexIgnore
        /* renamed from: d */
        public static /* final */ java.lang.Object f5725d; // = new java.lang.Object();

        @DexIgnore
        /* renamed from: e */
        public static java.util.concurrent.Executor f5726e;

        @DexIgnore
        /* renamed from: a */
        public java.util.concurrent.Executor f5727a;

        @DexIgnore
        /* renamed from: b */
        public java.util.concurrent.Executor f5728b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2314le.C2318d<T> f5729c;

        @DexIgnore
        public C1942a(com.fossil.blesdk.obfuscated.C2314le.C2318d<T> dVar) {
            this.f5729c = dVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1941he<T> mo11625a() {
            if (this.f5728b == null) {
                synchronized (f5725d) {
                    if (f5726e == null) {
                        f5726e = java.util.concurrent.Executors.newFixedThreadPool(2);
                    }
                }
                this.f5728b = f5726e;
            }
            return new com.fossil.blesdk.obfuscated.C1941he<>(this.f5727a, this.f5728b, this.f5729c);
        }
    }

    @DexIgnore
    public C1941he(java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, com.fossil.blesdk.obfuscated.C2314le.C2318d<T> dVar) {
        this.f5723a = executor2;
        this.f5724b = dVar;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.concurrent.Executor mo11623a() {
        return this.f5723a;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2314le.C2318d<T> mo11624b() {
        return this.f5724b;
    }
}
