package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wx */
public class C3247wx implements com.fossil.blesdk.obfuscated.j64 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.j64 f10737a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Random f10738b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ double f10739c;

    @DexIgnore
    public C3247wx(com.fossil.blesdk.obfuscated.j64 j64, double d) {
        this(j64, d, new java.util.Random());
    }

    @DexIgnore
    /* renamed from: a */
    public long mo17563a(int i) {
        return (long) (mo17562a() * ((double) this.f10737a.mo17563a(i)));
    }

    @DexIgnore
    public C3247wx(com.fossil.blesdk.obfuscated.j64 j64, double d, java.util.Random random) {
        if (d < 0.0d || d > 1.0d) {
            throw new java.lang.IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (j64 == null) {
            throw new java.lang.NullPointerException("backoff must not be null");
        } else if (random != null) {
            this.f10737a = j64;
            this.f10739c = d;
            this.f10738b = random;
        } else {
            throw new java.lang.NullPointerException("random must not be null");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public double mo17562a() {
        double d = this.f10739c;
        double d2 = 1.0d - d;
        return d2 + (((d + 1.0d) - d2) * this.f10738b.nextDouble());
    }
}
