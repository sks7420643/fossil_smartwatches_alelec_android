package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dl implements cl {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ lf b;
    @DexIgnore
    public /* final */ wf c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lf<bl> {
        @DexIgnore
        public a(dl dlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(kg kgVar, bl blVar) {
            String str = blVar.a;
            if (str == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, str);
            }
            kgVar.b(2, (long) blVar.b);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo`(`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends wf {
        @DexIgnore
        public b(dl dlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public dl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
    }

    @DexIgnore
    public void a(bl blVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(blVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.n();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public bl a(String str) {
        uf b2 = uf.b("SELECT * FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? new bl(a2.getString(ag.b(a2, "work_spec_id")), a2.getInt(ag.b(a2, "system_id"))) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
