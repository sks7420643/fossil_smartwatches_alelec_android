package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ze1 extends a51 implements oe1 {
    @DexIgnore
    public ze1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IUiSettingsDelegate");
    }

    @DexIgnore
    public final void d(boolean z) throws RemoteException {
        Parcel o = o();
        c51.a(o, z);
        b(18, o);
    }
}
