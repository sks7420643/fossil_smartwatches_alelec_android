package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ew */
public interface C1750ew<R> {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ew$a */
    public interface C1751a {
    }

    @DexIgnore
    /* renamed from: a */
    boolean mo10141a(R r, com.fossil.blesdk.obfuscated.C1750ew.C1751a aVar);
}
