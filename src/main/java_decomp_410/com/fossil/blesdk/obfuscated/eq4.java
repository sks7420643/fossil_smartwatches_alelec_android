package com.fossil.blesdk.obfuscated;

import java.util.Queue;
import org.slf4j.Marker;
import org.slf4j.event.Level;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class eq4 implements cq4 {
    @DexIgnore
    public String e;
    @DexIgnore
    public iq4 f;
    @DexIgnore
    public Queue<gq4> g;

    @DexIgnore
    public eq4(iq4 iq4, Queue<gq4> queue) {
        this.f = iq4;
        this.e = iq4.c();
        this.g = queue;
    }

    @DexIgnore
    public final void a(Level level, String str, Object[] objArr, Throwable th) {
        a(level, (Marker) null, str, objArr, th);
    }

    @DexIgnore
    public void debug(String str) {
        a(Level.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void error(String str) {
        a(Level.ERROR, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void info(String str) {
        a(Level.INFO, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    public void trace(String str) {
        a(Level.TRACE, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public void warn(String str) {
        a(Level.WARN, str, (Object[]) null, (Throwable) null);
    }

    @DexIgnore
    public final void a(Level level, Marker marker, String str, Object[] objArr, Throwable th) {
        gq4 gq4 = new gq4();
        gq4.a(System.currentTimeMillis());
        gq4.a(level);
        gq4.a(this.f);
        gq4.a(this.e);
        gq4.a(marker);
        gq4.b(str);
        gq4.a(objArr);
        gq4.a(th);
        gq4.c(Thread.currentThread().getName());
        this.g.add(gq4);
    }

    @DexIgnore
    public void debug(String str, Throwable th) {
        a(Level.DEBUG, str, (Object[]) null, th);
    }

    @DexIgnore
    public void error(String str, Object... objArr) {
        a(Level.ERROR, str, objArr, (Throwable) null);
    }

    @DexIgnore
    public void info(String str, Object obj) {
        a(Level.INFO, str, new Object[]{obj}, (Throwable) null);
    }

    @DexIgnore
    public void trace(String str, Throwable th) {
        a(Level.TRACE, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Object obj, Object obj2) {
        a(Level.WARN, str, new Object[]{obj, obj2}, (Throwable) null);
    }

    @DexIgnore
    public void error(String str, Throwable th) {
        a(Level.ERROR, str, (Object[]) null, th);
    }

    @DexIgnore
    public void info(String str, Throwable th) {
        a(Level.INFO, str, (Object[]) null, th);
    }

    @DexIgnore
    public void warn(String str, Throwable th) {
        a(Level.WARN, str, (Object[]) null, th);
    }
}
