package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.cv3;
import com.fossil.blesdk.obfuscated.hv3;
import com.fossil.blesdk.obfuscated.jv3;
import com.fossil.blesdk.obfuscated.nv3;
import com.squareup.okhttp.Protocol;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ru3 {
    @DexIgnore
    public /* final */ qv3 a;
    @DexIgnore
    public /* final */ nv3 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements qv3 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public jv3 a(hv3 hv3) throws IOException {
            return ru3.this.a(hv3);
        }

        @DexIgnore
        public void b(hv3 hv3) throws IOException {
            ru3.this.b(hv3);
        }

        @DexIgnore
        public ow3 a(jv3 jv3) throws IOException {
            return ru3.this.a(jv3);
        }

        @DexIgnore
        public void a(jv3 jv3, jv3 jv32) throws IOException {
            ru3.this.a(jv3, jv32);
        }

        @DexIgnore
        public void a() {
            ru3.this.a();
        }

        @DexIgnore
        public void a(pw3 pw3) {
            ru3.this.a(pw3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends kv3 {
        @DexIgnore
        public /* final */ nv3.f e;
        @DexIgnore
        public /* final */ lo4 f;
        @DexIgnore
        public /* final */ String g;
        @DexIgnore
        public /* final */ String h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends oo4 {
            @DexIgnore
            public /* final */ /* synthetic */ nv3.f f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, yo4 yo4, nv3.f fVar) {
                super(yo4);
                this.f = fVar;
            }

            @DexIgnore
            public void close() throws IOException {
                this.f.close();
                super.close();
            }
        }

        @DexIgnore
        public c(nv3.f fVar, String str, String str2) {
            this.e = fVar;
            this.g = str;
            this.h = str2;
            this.f = so4.a((yo4) new a(this, fVar.b(1), fVar));
        }

        @DexIgnore
        public fv3 A() {
            String str = this.g;
            if (str != null) {
                return fv3.a(str);
            }
            return null;
        }

        @DexIgnore
        public lo4 B() {
            return this.f;
        }

        @DexIgnore
        public long z() {
            try {
                if (this.h != null) {
                    return Long.parseLong(this.h);
                }
                return -1;
            } catch (NumberFormatException unused) {
                return -1;
            }
        }
    }

    @DexIgnore
    public ru3(File file, long j) {
        this(file, j, ex3.a);
    }

    @DexIgnore
    public static /* synthetic */ int b(ru3 ru3) {
        int i = ru3.c;
        ru3.c = i + 1;
        return i;
    }

    @DexIgnore
    public static /* synthetic */ int c(ru3 ru3) {
        int i = ru3.d;
        ru3.d = i + 1;
        return i;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements ow3 {
        @DexIgnore
        public /* final */ nv3.d a;
        @DexIgnore
        public xo4 b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public xo4 d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends no4 {
            @DexIgnore
            public /* final */ /* synthetic */ nv3.d f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(xo4 xo4, ru3 ru3, nv3.d dVar) {
                super(xo4);
                this.f = dVar;
            }

            @DexIgnore
            public void close() throws IOException {
                synchronized (ru3.this) {
                    if (!b.this.c) {
                        boolean unused = b.this.c = true;
                        ru3.b(ru3.this);
                        super.close();
                        this.f.b();
                    }
                }
            }
        }

        @DexIgnore
        public b(nv3.d dVar) throws IOException {
            this.a = dVar;
            this.b = dVar.a(1);
            this.d = new a(this.b, ru3.this, dVar);
        }

        @DexIgnore
        public void abort() {
            synchronized (ru3.this) {
                if (!this.c) {
                    this.c = true;
                    ru3.c(ru3.this);
                    wv3.a((Closeable) this.b);
                    try {
                        this.a.a();
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexIgnore
        public xo4 a() {
            return this.d;
        }
    }

    @DexIgnore
    public ru3(File file, long j, ex3 ex3) {
        this.a = new a();
        this.b = nv3.a(ex3, file, 201105, 2, j);
    }

    @DexIgnore
    public static String c(hv3 hv3) {
        return wv3.a(hv3.i());
    }

    @DexIgnore
    public final void b(hv3 hv3) throws IOException {
        this.b.h(c(hv3));
    }

    @DexIgnore
    public static int b(lo4 lo4) throws IOException {
        try {
            long h = lo4.h();
            String i = lo4.i();
            if (h >= 0 && h <= 2147483647L && i.isEmpty()) {
                return (int) h;
            }
            throw new IOException("expected an int but was \"" + h + i + "\"");
        } catch (NumberFormatException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    @DexIgnore
    public jv3 a(hv3 hv3) {
        try {
            nv3.f f2 = this.b.f(c(hv3));
            if (f2 == null) {
                return null;
            }
            try {
                d dVar = new d(f2.b(0));
                jv3 a2 = dVar.a(hv3, f2);
                if (dVar.a(hv3, a2)) {
                    return a2;
                }
                wv3.a((Closeable) a2.a());
                return null;
            } catch (IOException unused) {
                wv3.a((Closeable) f2);
                return null;
            }
        } catch (IOException unused2) {
            return null;
        }
    }

    @DexIgnore
    public final ow3 a(jv3 jv3) throws IOException {
        nv3.d dVar;
        String f2 = jv3.l().f();
        if (vw3.a(jv3.l().f())) {
            try {
                b(jv3.l());
            } catch (IOException unused) {
            }
            return null;
        } else if (!f2.equals("GET") || xw3.b(jv3)) {
            return null;
        } else {
            d dVar2 = new d(jv3);
            try {
                dVar = this.b.e(c(jv3.l()));
                if (dVar == null) {
                    return null;
                }
                try {
                    dVar2.a(dVar);
                    return new b(dVar);
                } catch (IOException unused2) {
                    a(dVar);
                    return null;
                }
            } catch (IOException unused3) {
                dVar = null;
                a(dVar);
                return null;
            }
        }
    }

    @DexIgnore
    public final void a(jv3 jv3, jv3 jv32) {
        nv3.d dVar;
        d dVar2 = new d(jv32);
        try {
            dVar = ((c) jv3.a()).e.y();
            if (dVar != null) {
                try {
                    dVar2.a(dVar);
                    dVar.b();
                } catch (IOException unused) {
                }
            }
        } catch (IOException unused2) {
            dVar = null;
            a(dVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ cv3 b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ Protocol d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ cv3 g;
        @DexIgnore
        public /* final */ bv3 h;

        @DexIgnore
        public d(yo4 yo4) throws IOException {
            try {
                lo4 a2 = so4.a(yo4);
                this.a = a2.i();
                this.c = a2.i();
                cv3.b bVar = new cv3.b();
                int a3 = ru3.b(a2);
                for (int i = 0; i < a3; i++) {
                    bVar.a(a2.i());
                }
                this.b = bVar.a();
                cx3 a4 = cx3.a(a2.i());
                this.d = a4.a;
                this.e = a4.b;
                this.f = a4.c;
                cv3.b bVar2 = new cv3.b();
                int a5 = ru3.b(a2);
                for (int i2 = 0; i2 < a5; i2++) {
                    bVar2.a(a2.i());
                }
                this.g = bVar2.a();
                if (a()) {
                    String i3 = a2.i();
                    if (i3.length() <= 0) {
                        this.h = bv3.a(a2.i(), a(a2), a(a2));
                    } else {
                        throw new IOException("expected \"\" but was \"" + i3 + "\"");
                    }
                } else {
                    this.h = null;
                }
            } finally {
                yo4.close();
            }
        }

        @DexIgnore
        public void a(nv3.d dVar) throws IOException {
            ko4 a2 = so4.a(dVar.a(0));
            a2.a(this.a);
            a2.writeByte(10);
            a2.a(this.c);
            a2.writeByte(10);
            a2.b((long) this.b.b());
            a2.writeByte(10);
            int b2 = this.b.b();
            for (int i = 0; i < b2; i++) {
                a2.a(this.b.a(i));
                a2.a(": ");
                a2.a(this.b.b(i));
                a2.writeByte(10);
            }
            a2.a(new cx3(this.d, this.e, this.f).toString());
            a2.writeByte(10);
            a2.b((long) this.g.b());
            a2.writeByte(10);
            int b3 = this.g.b();
            for (int i2 = 0; i2 < b3; i2++) {
                a2.a(this.g.a(i2));
                a2.a(": ");
                a2.a(this.g.b(i2));
                a2.writeByte(10);
            }
            if (a()) {
                a2.writeByte(10);
                a2.a(this.h.a());
                a2.writeByte(10);
                a(a2, this.h.c());
                a(a2, this.h.b());
            }
            a2.close();
        }

        @DexIgnore
        public d(jv3 jv3) {
            this.a = jv3.l().i();
            this.b = xw3.d(jv3);
            this.c = jv3.l().f();
            this.d = jv3.k();
            this.e = jv3.e();
            this.f = jv3.h();
            this.g = jv3.g();
            this.h = jv3.f();
        }

        @DexIgnore
        public final boolean a() {
            return this.a.startsWith("https://");
        }

        @DexIgnore
        public final List<Certificate> a(lo4 lo4) throws IOException {
            int a2 = ru3.b(lo4);
            if (a2 == -1) {
                return Collections.emptyList();
            }
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                ArrayList arrayList = new ArrayList(a2);
                for (int i = 0; i < a2; i++) {
                    String i2 = lo4.i();
                    jo4 jo4 = new jo4();
                    jo4.a(ByteString.decodeBase64(i2));
                    arrayList.add(instance.generateCertificate(jo4.m()));
                }
                return arrayList;
            } catch (CertificateException e2) {
                throw new IOException(e2.getMessage());
            }
        }

        @DexIgnore
        public final void a(ko4 ko4, List<Certificate> list) throws IOException {
            try {
                ko4.b((long) list.size());
                ko4.writeByte(10);
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    ko4.a(ByteString.of(list.get(i).getEncoded()).base64());
                    ko4.writeByte(10);
                }
            } catch (CertificateEncodingException e2) {
                throw new IOException(e2.getMessage());
            }
        }

        @DexIgnore
        public boolean a(hv3 hv3, jv3 jv3) {
            return this.a.equals(hv3.i()) && this.c.equals(hv3.f()) && xw3.a(jv3, this.b, hv3);
        }

        @DexIgnore
        public jv3 a(hv3 hv3, nv3.f fVar) {
            String a2 = this.g.a(GraphRequest.CONTENT_TYPE_HEADER);
            String a3 = this.g.a("Content-Length");
            hv3.b bVar = new hv3.b();
            bVar.b(this.a);
            bVar.a(this.c, (iv3) null);
            bVar.a(this.b);
            hv3 a4 = bVar.a();
            jv3.b bVar2 = new jv3.b();
            bVar2.a(a4);
            bVar2.a(this.d);
            bVar2.a(this.e);
            bVar2.a(this.f);
            bVar2.a(this.g);
            bVar2.a((kv3) new c(fVar, a2, a3));
            bVar2.a(this.h);
            return bVar2.a();
        }
    }

    @DexIgnore
    public final void a(nv3.d dVar) {
        if (dVar != null) {
            try {
                dVar.a();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public final synchronized void a(pw3 pw3) {
        this.g++;
        if (pw3.a != null) {
            this.e++;
        } else if (pw3.b != null) {
            this.f++;
        }
    }

    @DexIgnore
    public final synchronized void a() {
        this.f++;
    }
}
