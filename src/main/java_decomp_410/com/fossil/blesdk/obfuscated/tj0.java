package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tj0 extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends zy0 implements tj0 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tj0$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.tj0$a$a  reason: collision with other inner class name */
        public static class C0035a extends yy0 implements tj0 {
            @DexIgnore
            public C0035a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            public final Account h() throws RemoteException {
                Parcel a = a(2, o());
                Account account = (Account) az0.a(a, Account.CREATOR);
                a.recycle();
                return account;
            }
        }

        @DexIgnore
        public static tj0 a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            if (queryLocalInterface instanceof tj0) {
                return (tj0) queryLocalInterface;
            }
            return new C0035a(iBinder);
        }
    }

    @DexIgnore
    Account h() throws RemoteException;
}
