package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r9 */
public class C2791r9 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f8882a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.r9$a")
    /* renamed from: com.fossil.blesdk.obfuscated.r9$a */
    public static class C2792a extends android.view.accessibility.AccessibilityNodeProvider {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2791r9 f8883a;

        @DexIgnore
        public C2792a(com.fossil.blesdk.obfuscated.C2791r9 r9Var) {
            this.f8883a = r9Var;
        }

        @DexIgnore
        public android.view.accessibility.AccessibilityNodeInfo createAccessibilityNodeInfo(int i) {
            com.fossil.blesdk.obfuscated.C2711q9 a = this.f8883a.mo11884a(i);
            if (a == null) {
                return null;
            }
            return a.mo15121w();
        }

        @DexIgnore
        public java.util.List<android.view.accessibility.AccessibilityNodeInfo> findAccessibilityNodeInfosByText(java.lang.String str, int i) {
            java.util.List<com.fossil.blesdk.obfuscated.C2711q9> a = this.f8883a.mo15455a(str, i);
            if (a == null) {
                return null;
            }
            java.util.ArrayList arrayList = new java.util.ArrayList();
            int size = a.size();
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(a.get(i2).mo15121w());
            }
            return arrayList;
        }

        @DexIgnore
        public boolean performAction(int i, int i2, android.os.Bundle bundle) {
            return this.f8883a.mo11885a(i, i2, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.r9$b")
    /* renamed from: com.fossil.blesdk.obfuscated.r9$b */
    public static class C2793b extends com.fossil.blesdk.obfuscated.C2791r9.C2792a {
        @DexIgnore
        public C2793b(com.fossil.blesdk.obfuscated.C2791r9 r9Var) {
            super(r9Var);
        }

        @DexIgnore
        public android.view.accessibility.AccessibilityNodeInfo findFocus(int i) {
            com.fossil.blesdk.obfuscated.C2711q9 b = this.f8883a.mo11886b(i);
            if (b == null) {
                return null;
            }
            return b.mo15121w();
        }
    }

    @DexIgnore
    public C2791r9() {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 19) {
            this.f8882a = new com.fossil.blesdk.obfuscated.C2791r9.C2793b(this);
        } else if (i >= 16) {
            this.f8882a = new com.fossil.blesdk.obfuscated.C2791r9.C2792a(this);
        } else {
            this.f8882a = null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2711q9 mo11884a(int i) {
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo15454a() {
        return this.f8882a;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C2711q9> mo15455a(java.lang.String str, int i) {
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11885a(int i, int i2, android.os.Bundle bundle) {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2711q9 mo11886b(int i) {
        return null;
    }

    @DexIgnore
    public C2791r9(java.lang.Object obj) {
        this.f8882a = obj;
    }
}
