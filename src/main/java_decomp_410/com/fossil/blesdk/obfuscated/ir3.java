package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ir3 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public /* final */ DianaPresetRepository d;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ir3(DianaPresetRepository dianaPresetRepository, CustomizeRealDataRepository customizeRealDataRepository) {
        kd4.b(dianaPresetRepository, "mDianaPresetRepository");
        kd4.b(customizeRealDataRepository, "mRealDataRepository");
        this.d = dianaPresetRepository;
        this.e = customizeRealDataRepository;
    }

    @DexIgnore
    public String c() {
        return "DSTChangeUseCase";
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        String str;
        String e2 = PortfolioApp.W.c().e();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(e2);
        if (deviceBySerial != null && jr3.a[deviceBySerial.ordinal()] == 1) {
            DianaPreset activePresetBySerial = this.d.getActivePresetBySerial(e2);
            if (activePresetBySerial != null) {
                ComplicationAppMappingSettings a2 = sj2.a(activePresetBySerial.getComplications(), new Gson());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DSTChangeUseCase", "DST change, reset complications " + a2);
                PortfolioApp.W.c().a(a2, e2);
                for (DianaPresetComplicationSetting dianaPresetComplicationSetting : activePresetBySerial.getComplications()) {
                    if (kd4.a((Object) dianaPresetComplicationSetting.getId(), (Object) "second-timezone")) {
                        try {
                            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().a(dianaPresetComplicationSetting.getSettings(), SecondTimezoneSetting.class);
                            if (secondTimezoneSetting != null) {
                                double timezoneRawOffsetById = (((double) ConversionUtils.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())) / 60.0d) - ((double) (((float) ml2.a()) / ((float) 3600)));
                                if (timezoneRawOffsetById > ((double) 0)) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append('+');
                                    sb.append(timezoneRawOffsetById);
                                    sb.append('h');
                                    str = sb.toString();
                                } else {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(timezoneRawOffsetById);
                                    sb2.append('h');
                                    str = sb2.toString();
                                }
                                this.e.upsertCustomizeRealData(new CustomizeRealData("second-timezone", str));
                            }
                        } catch (Exception e3) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("DSTChangeUseCase", "exception " + e3);
                        }
                    }
                }
            }
        }
        return new d();
    }
}
