package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zv */
public class C3470zv {
    @DexIgnore
    /* renamed from: a */
    public <Z> com.fossil.blesdk.obfuscated.C1583cw<android.widget.ImageView, Z> mo18615a(android.widget.ImageView imageView, java.lang.Class<Z> cls) {
        if (android.graphics.Bitmap.class.equals(cls)) {
            return new com.fossil.blesdk.obfuscated.C3144vv(imageView);
        }
        if (android.graphics.drawable.Drawable.class.isAssignableFrom(cls)) {
            return new com.fossil.blesdk.obfuscated.C3317xv(imageView);
        }
        throw new java.lang.IllegalArgumentException("Unhandled class: " + cls + ", try .as*(Class).transcode(ResourceTranscoder)");
    }
}
