package com.fossil.blesdk.obfuscated;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ae4 extends nb4 {
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public ae4(long j, long j2, long j3) {
        this.h = j3;
        this.e = j2;
        boolean z = true;
        int i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        if (this.h <= 0 ? i < 0 : i > 0) {
            z = false;
        }
        this.f = z;
        this.g = !this.f ? this.e : j;
    }

    @DexIgnore
    public long a() {
        long j = this.g;
        if (j != this.e) {
            this.g = this.h + j;
        } else if (this.f) {
            this.f = false;
        } else {
            throw new NoSuchElementException();
        }
        return j;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.f;
    }
}
