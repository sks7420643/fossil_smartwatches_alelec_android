package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ga extends ea {
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public LayoutInflater o;

    @DexIgnore
    @Deprecated
    public ga(Context context, int i, Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.n = i;
        this.m = i;
        this.o = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    @DexIgnore
    public View a(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o.inflate(this.n, viewGroup, false);
    }

    @DexIgnore
    public View b(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.o.inflate(this.m, viewGroup, false);
    }
}
