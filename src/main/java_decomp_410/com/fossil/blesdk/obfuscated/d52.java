package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d52 implements Factory<b62> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<en2> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetDao> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<DeviceDao> f;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> g;

    @DexIgnore
    public d52(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
    }

    @DexIgnore
    public static d52 a(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return new d52(n42, provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static b62 b(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<HybridPresetDao> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceDao> provider5, Provider<PortfolioApp> provider6) {
        return a(n42, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public static b62 a(n42 n42, en2 en2, UserRepository userRepository, HybridPresetDao hybridPresetDao, NotificationsRepository notificationsRepository, DeviceDao deviceDao, PortfolioApp portfolioApp) {
        b62 a2 = n42.a(en2, userRepository, hybridPresetDao, notificationsRepository, deviceDao, portfolioApp);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public b62 get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
    }
}
