package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tw */
public final class C2992tw {
    @DexIgnore
    /* renamed from: a */
    public static void m14461a(boolean z, java.lang.String str) {
        if (!z) {
            throw new java.lang.IllegalArgumentException(str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T m14457a(T t) {
        m14458a(t, "Argument must not be null");
        return t;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T m14458a(T t, java.lang.String str) {
        if (t != null) {
            return t;
        }
        throw new java.lang.NullPointerException(str);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m14459a(java.lang.String str) {
        if (!android.text.TextUtils.isEmpty(str)) {
            return str;
        }
        throw new java.lang.IllegalArgumentException("Must not be null or empty");
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends java.util.Collection<Y>, Y> T m14460a(T t) {
        if (!t.isEmpty()) {
            return t;
        }
        throw new java.lang.IllegalArgumentException("Must not be empty.");
    }
}
