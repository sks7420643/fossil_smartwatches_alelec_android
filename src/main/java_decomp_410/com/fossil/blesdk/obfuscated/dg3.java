package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDaySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dg3 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<b> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ OverviewSleepDayChart a;
        @DexIgnore
        public /* final */ OverviewSleepDaySummary b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ FlexibleTextView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ FlexibleTextView h;
        @DexIgnore
        public /* final */ FlexibleTextView i;
        @DexIgnore
        public /* final */ FlexibleTextView j;
        @DexIgnore
        public /* final */ FlexibleTextView k;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            kd4.b(view, "item");
            View findViewById = view.findViewById(R.id.sleep_chart);
            kd4.a((Object) findViewById, "item.findViewById(R.id.sleep_chart)");
            this.a = (OverviewSleepDayChart) findViewById;
            View findViewById2 = view.findViewById(R.id.v_summary_chart);
            kd4.a((Object) findViewById2, "item.findViewById(R.id.v_summary_chart)");
            this.b = (OverviewSleepDaySummary) findViewById2;
            View findViewById3 = view.findViewById(R.id.tv_awake);
            kd4.a((Object) findViewById3, "item.findViewById(R.id.tv_awake)");
            this.c = (FlexibleTextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.tv_light);
            kd4.a((Object) findViewById4, "item.findViewById(R.id.tv_light)");
            this.d = (FlexibleTextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.tv_deep);
            kd4.a((Object) findViewById5, "item.findViewById(R.id.tv_deep)");
            this.e = (FlexibleTextView) findViewById5;
            View findViewById6 = view.findViewById(R.id.tv_awake_min);
            kd4.a((Object) findViewById6, "item.findViewById(R.id.tv_awake_min)");
            this.f = (FlexibleTextView) findViewById6;
            View findViewById7 = view.findViewById(R.id.tv_light_min);
            kd4.a((Object) findViewById7, "item.findViewById(R.id.tv_light_min)");
            this.g = (FlexibleTextView) findViewById7;
            View findViewById8 = view.findViewById(R.id.tv_deep_min);
            kd4.a((Object) findViewById8, "item.findViewById(R.id.tv_deep_min)");
            this.h = (FlexibleTextView) findViewById8;
            View findViewById9 = view.findViewById(R.id.tv_awake_hour);
            kd4.a((Object) findViewById9, "item.findViewById(R.id.tv_awake_hour)");
            this.i = (FlexibleTextView) findViewById9;
            View findViewById10 = view.findViewById(R.id.tv_light_hour);
            kd4.a((Object) findViewById10, "item.findViewById(R.id.tv_light_hour)");
            this.j = (FlexibleTextView) findViewById10;
            View findViewById11 = view.findViewById(R.id.tv_deep_hour);
            kd4.a((Object) findViewById11, "item.findViewById(R.id.tv_deep_hour)");
            this.k = (FlexibleTextView) findViewById11;
        }

        @DexIgnore
        public final OverviewSleepDayChart a() {
            return this.a;
        }

        @DexIgnore
        public final OverviewSleepDaySummary b() {
            return this.b;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView d() {
            return this.i;
        }

        @DexIgnore
        public final FlexibleTextView e() {
            return this.f;
        }

        @DexIgnore
        public final FlexibleTextView f() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView g() {
            return this.k;
        }

        @DexIgnore
        public final FlexibleTextView h() {
            return this.h;
        }

        @DexIgnore
        public final FlexibleTextView i() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView j() {
            return this.j;
        }

        @DexIgnore
        public final FlexibleTextView k() {
            return this.g;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public wr2 a;
        @DexIgnore
        public /* final */ float b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public b(wr2 wr2, float f2, float f3, float f4, int i, int i2, int i3, int i4) {
            kd4.b(wr2, "sleepChartModel");
            this.a = wr2;
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = i;
            this.f = i2;
            this.g = i3;
            this.h = i4;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final float b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.g;
        }

        @DexIgnore
        public final float d() {
            return this.d;
        }

        @DexIgnore
        public final int e() {
            return this.f;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (kd4.a((Object) this.a, (Object) bVar.a) && Float.compare(this.b, bVar.b) == 0 && Float.compare(this.c, bVar.c) == 0 && Float.compare(this.d, bVar.d) == 0) {
                        if (this.e == bVar.e) {
                            if (this.f == bVar.f) {
                                if (this.g == bVar.g) {
                                    if (this.h == bVar.h) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final float f() {
            return this.c;
        }

        @DexIgnore
        public final wr2 g() {
            return this.a;
        }

        @DexIgnore
        public final int h() {
            return this.h;
        }

        @DexIgnore
        public int hashCode() {
            wr2 wr2 = this.a;
            return ((((((((((((((wr2 != null ? wr2.hashCode() : 0) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c)) * 31) + Float.floatToIntBits(this.d)) * 31) + this.e) * 31) + this.f) * 31) + this.g) * 31) + this.h;
        }

        @DexIgnore
        public String toString() {
            return "SleepUIData(sleepChartModel=" + this.a + ", awakePer=" + this.b + ", lightPer=" + this.c + ", deepPer=" + this.d + ", awakeMin=" + this.e + ", lightMin=" + this.f + ", deepMin=" + this.g + ", timeZoneOffsetInSecond=" + this.h + ")";
        }
    }

    @DexIgnore
    public dg3(ArrayList<b> arrayList) {
        kd4.b(arrayList, "data");
        this.a = arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        b bVar = this.a.get(i);
        kd4.a((Object) bVar, "data[position]");
        b bVar2 = bVar;
        aVar.b().a(bVar2.b(), bVar2.f(), bVar2.d());
        aVar.c().setText(ll2.b.a(R.string.DashboardDiana_Sleep_DetailPage_Label__AwakeNumber, bVar2.b()));
        aVar.i().setText(ll2.b.a(R.string.DashboardDiana_Sleep_DetailPage_Label__LightNumber, bVar2.f()));
        aVar.f().setText(ll2.b.a(R.string.DashboardDiana_Sleep_DetailPage_Label__DeepNumber, bVar2.d()));
        aVar.e().setText(String.valueOf(nl2.b(bVar2.a())));
        aVar.k().setText(String.valueOf(nl2.b(bVar2.e())));
        aVar.h().setText(String.valueOf(nl2.b(bVar2.c())));
        aVar.d().setText(String.valueOf(nl2.a(bVar2.a())));
        aVar.j().setText(String.valueOf(nl2.a(bVar2.e())));
        aVar.g().setText(String.valueOf(nl2.a(bVar2.c())));
        OverviewSleepDayChart a2 = aVar.a();
        BarChart.a(a2, 0, k6.a((Context) PortfolioApp.W.c(), (int) R.color.awakeSleep), k6.a((Context) PortfolioApp.W.c(), (int) R.color.lightSleep), k6.a((Context) PortfolioApp.W.c(), (int) R.color.deepSleep), (String) null, (GoalType) null, 49, (Object) null);
        a2.setTimeZoneOffsetInSecond(bVar2.h());
        a2.a(bVar2.g());
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout_sleep_session, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026p_session, parent, false)");
        return new a(inflate);
    }

    @DexIgnore
    public final void a(List<b> list) {
        kd4.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }
}
