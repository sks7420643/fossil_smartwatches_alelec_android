package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.ap1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qp1 extends jk0 implements yo1, ap1.a {
    @DexIgnore
    public static /* final */ Parcelable.Creator<qp1> CREATOR; // = new rp1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public qp1(String str, String str2, String str3) {
        bk0.a(str);
        this.e = str;
        bk0.a(str2);
        this.f = str2;
        bk0.a(str3);
        this.g = str3;
    }

    @DexIgnore
    public final String H() {
        return this.f;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof qp1)) {
            return false;
        }
        qp1 qp1 = (qp1) obj;
        return this.e.equals(qp1.e) && zj0.a(qp1.f, this.f) && zj0.a(qp1.g, this.g);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final String toString() {
        int i = 0;
        for (char c : this.e.toCharArray()) {
            i += c;
        }
        String trim = this.e.trim();
        int length = trim.length();
        if (length > 25) {
            String substring = trim.substring(0, 10);
            String substring2 = trim.substring(length - 10, length);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 16 + String.valueOf(substring2).length());
            sb.append(substring);
            sb.append("...");
            sb.append(substring2);
            sb.append("::");
            sb.append(i);
            trim = sb.toString();
        }
        String str = this.f;
        String str2 = this.g;
        StringBuilder sb2 = new StringBuilder(String.valueOf(trim).length() + 31 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb2.append("Channel{token=");
        sb2.append(trim);
        sb2.append(", nodeId=");
        sb2.append(str);
        sb2.append(", path=");
        sb2.append(str2);
        sb2.append("}");
        return sb2.toString();
    }

    @DexIgnore
    public final String w() {
        return this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e, false);
        kk0.a(parcel, 3, H(), false);
        kk0.a(parcel, 4, w(), false);
        kk0.a(parcel, a);
    }
}
