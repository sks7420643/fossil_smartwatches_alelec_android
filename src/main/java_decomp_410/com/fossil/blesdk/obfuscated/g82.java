package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class g82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon20 e;
    @DexIgnore
    private /* final */ /* synthetic */ SavedPreset f;

    @DexIgnore
    public /* synthetic */ g82(PresetRepository.Anon20 anon20, SavedPreset savedPreset) {
        this.e = anon20;
        this.f = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f);
    }
}
