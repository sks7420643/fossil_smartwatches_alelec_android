package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.yl4;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dm4 {
    @DexIgnore
    public /* final */ zl4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ yl4 c;
    @DexIgnore
    public /* final */ RequestBody d;
    @DexIgnore
    public /* final */ Map<Class<?>, Object> e;
    @DexIgnore
    public volatile il4 f;

    @DexIgnore
    public dm4(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c.a();
        this.d = aVar.d;
        this.e = jm4.a(aVar.e);
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }

    @DexIgnore
    public List<String> b(String str) {
        return this.c.b(str);
    }

    @DexIgnore
    public yl4 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.h();
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public a f() {
        return new a(this);
    }

    @DexIgnore
    public zl4 g() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Request{method=" + this.b + ", url=" + this.a + ", tags=" + this.e + '}';
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public zl4 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public yl4.a c;
        @DexIgnore
        public RequestBody d;
        @DexIgnore
        public Map<Class<?>, Object> e;

        @DexIgnore
        public a() {
            this.e = Collections.emptyMap();
            this.b = "GET";
            this.c = new yl4.a();
        }

        @DexIgnore
        public a a(zl4 zl4) {
            if (zl4 != null) {
                this.a = zl4;
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                a(zl4.d(str));
                return this;
            }
            throw new NullPointerException("url == null");
        }

        @DexIgnore
        public a a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.c.c(str);
            return this;
        }

        @DexIgnore
        public a(dm4 dm4) {
            Map<Class<?>, Object> map;
            this.e = Collections.emptyMap();
            this.a = dm4.a;
            this.b = dm4.b;
            this.d = dm4.d;
            if (dm4.e.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = new LinkedHashMap<>(dm4.e);
            }
            this.e = map;
            this.c = dm4.c.a();
        }

        @DexIgnore
        public a a(yl4 yl4) {
            this.c = yl4.a();
            return this;
        }

        @DexIgnore
        public a a(RequestBody requestBody) {
            a("POST", requestBody);
            return this;
        }

        @DexIgnore
        public a a(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !cn4.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !cn4.e(str)) {
                this.b = str;
                this.d = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            this.c.c(str, str2);
            return this;
        }

        @DexIgnore
        public a b() {
            a("GET", (RequestBody) null);
            return this;
        }

        @DexIgnore
        public <T> a a(Class<? super T> cls, T t) {
            if (cls != null) {
                if (t == null) {
                    this.e.remove(cls);
                } else {
                    if (this.e.isEmpty()) {
                        this.e = new LinkedHashMap();
                    }
                    this.e.put(cls, cls.cast(t));
                }
                return this;
            }
            throw new NullPointerException("type == null");
        }

        @DexIgnore
        public dm4 a() {
            if (this.a != null) {
                return new dm4(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    @DexIgnore
    public RequestBody a() {
        return this.d;
    }

    @DexIgnore
    public il4 b() {
        il4 il4 = this.f;
        if (il4 != null) {
            return il4;
        }
        il4 a2 = il4.a(this.c);
        this.f = a2;
        return a2;
    }

    @DexIgnore
    public <T> T a(Class<? extends T> cls) {
        return cls.cast(this.e.get(cls));
    }
}
