package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ hk1 e;

    @DexIgnore
    public lk1(hk1 hk1) {
        this.e = hk1;
    }

    @DexIgnore
    public final void run() {
        vj1 vj1 = this.e.c;
        Context context = vj1.getContext();
        this.e.c.b();
        vj1.a(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
