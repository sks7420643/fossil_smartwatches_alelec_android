package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r70 extends p70 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.RELEASE_HANDS, RequestId.RELEASE_HANDS, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        c(true);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(0).array();
        kd4.a((Object) array, "ByteBuffer.allocate(2).o\u2026\n                .array()");
        return array;
    }
}
