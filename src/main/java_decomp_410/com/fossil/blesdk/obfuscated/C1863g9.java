package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g9 */
public final class C1863g9 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f5399a;

    /*
    static {
        if (android.os.Build.VERSION.SDK_INT == 25) {
            try {
                f5399a = android.view.ViewConfiguration.class.getDeclaredMethod("getScaledScrollFactor", new java.lang.Class[0]);
            } catch (java.lang.Exception unused) {
                android.util.Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
            }
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static float m7309a(android.view.ViewConfiguration viewConfiguration, android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 25) {
            java.lang.reflect.Method method = f5399a;
            if (method != null) {
                try {
                    return (float) ((java.lang.Integer) method.invoke(viewConfiguration, new java.lang.Object[0])).intValue();
                } catch (java.lang.Exception unused) {
                    android.util.Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
                }
            }
        }
        android.util.TypedValue typedValue = new android.util.TypedValue();
        return context.getTheme().resolveAttribute(16842829, typedValue, true) ? typedValue.getDimension(context.getResources().getDisplayMetrics()) : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: b */
    public static float m7311b(android.view.ViewConfiguration viewConfiguration, android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            return viewConfiguration.getScaledHorizontalScrollFactor();
        }
        return m7309a(viewConfiguration, context);
    }

    @DexIgnore
    /* renamed from: c */
    public static float m7312c(android.view.ViewConfiguration viewConfiguration, android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            return viewConfiguration.getScaledVerticalScrollFactor();
        }
        return m7309a(viewConfiguration, context);
    }

    @DexIgnore
    /* renamed from: d */
    public static boolean m7313d(android.view.ViewConfiguration viewConfiguration, android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return viewConfiguration.shouldShowMenuShortcutsWhenKeyboardPresent();
        }
        android.content.res.Resources resources = context.getResources();
        int identifier = resources.getIdentifier("config_showMenuShortcutsWhenKeyboardPresent", com.facebook.LegacyTokenHelper.TYPE_BOOLEAN, "android");
        return identifier != 0 && resources.getBoolean(identifier);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m7310a(android.view.ViewConfiguration viewConfiguration) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return viewConfiguration.getScaledHoverSlop();
        }
        return viewConfiguration.getScaledTouchSlop() / 2;
    }
}
