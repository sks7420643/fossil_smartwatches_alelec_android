package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n51 extends t81<n51, a> implements y91 {
    @DexIgnore
    public static /* final */ n51 zzauy; // = new n51();
    @DexIgnore
    public static volatile ha1<n51> zznw;
    @DexIgnore
    public String zzauw; // = "";
    @DexIgnore
    public long zzaux;
    @DexIgnore
    public int zznr;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends t81.a<n51, a> implements y91 {
        @DexIgnore
        public a() {
            super(n51.zzauy);
        }

        @DexIgnore
        public /* synthetic */ a(o51 o51) {
            this();
        }
    }

    /*
    static {
        t81.a(n51.class, zzauy);
    }
    */

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (o51.a[i - 1]) {
            case 1:
                return new n51();
            case 2:
                return new a((o51) null);
            case 3:
                return t81.a((w91) zzauy, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zznr", "zzauw", "zzaux"});
            case 4:
                return zzauy;
            case 5:
                ha1<n51> ha1 = zznw;
                if (ha1 == null) {
                    synchronized (n51.class) {
                        ha1 = zznw;
                        if (ha1 == null) {
                            ha1 = new t81.b<>(zzauy);
                            zznw = ha1;
                        }
                    }
                }
                return ha1;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
