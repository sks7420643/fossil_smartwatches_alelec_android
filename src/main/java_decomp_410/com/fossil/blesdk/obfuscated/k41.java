package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k41 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<k41> CREATOR; // = new l41();
    @DexIgnore
    public int e;
    @DexIgnore
    public i41 f;
    @DexIgnore
    public wd1 g;
    @DexIgnore
    public PendingIntent h;
    @DexIgnore
    public td1 i;
    @DexIgnore
    public r31 j;

    @DexIgnore
    public k41(int i2, i41 i41, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        this.e = i2;
        this.f = i41;
        r31 r31 = null;
        this.g = iBinder == null ? null : xd1.a(iBinder);
        this.h = pendingIntent;
        this.i = iBinder2 == null ? null : ud1.a(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            r31 = queryLocalInterface instanceof r31 ? (r31) queryLocalInterface : new t31(iBinder3);
        }
        this.j = r31;
    }

    @DexIgnore
    public static k41 a(td1 td1, r31 r31) {
        return new k41(2, (i41) null, (IBinder) null, (PendingIntent) null, td1.asBinder(), r31 != null ? r31.asBinder() : null);
    }

    @DexIgnore
    public static k41 a(wd1 wd1, r31 r31) {
        return new k41(2, (i41) null, wd1.asBinder(), (PendingIntent) null, (IBinder) null, r31 != null ? r31.asBinder() : null);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, (Parcelable) this.f, i2, false);
        wd1 wd1 = this.g;
        IBinder iBinder = null;
        kk0.a(parcel, 3, wd1 == null ? null : wd1.asBinder(), false);
        kk0.a(parcel, 4, (Parcelable) this.h, i2, false);
        td1 td1 = this.i;
        kk0.a(parcel, 5, td1 == null ? null : td1.asBinder(), false);
        r31 r31 = this.j;
        if (r31 != null) {
            iBinder = r31.asBinder();
        }
        kk0.a(parcel, 6, iBinder, false);
        kk0.a(parcel, a);
    }
}
