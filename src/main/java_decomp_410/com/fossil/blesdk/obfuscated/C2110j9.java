package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j9 */
public final class C2110j9 {

    @DexIgnore
    /* renamed from: a */
    public java.lang.ref.WeakReference<android.view.View> f6378a;

    @DexIgnore
    /* renamed from: b */
    public java.lang.Runnable f6379b; // = null;

    @DexIgnore
    /* renamed from: c */
    public java.lang.Runnable f6380c; // = null;

    @DexIgnore
    /* renamed from: d */
    public int f6381d; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.j9$a")
    /* renamed from: com.fossil.blesdk.obfuscated.j9$a */
    public class C2111a extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2190k9 f6382a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.View f6383b;

        @DexIgnore
        public C2111a(com.fossil.blesdk.obfuscated.C2110j9 j9Var, com.fossil.blesdk.obfuscated.C2190k9 k9Var, android.view.View view) {
            this.f6382a = k9Var;
            this.f6383b = view;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            this.f6382a.mo8505a(this.f6383b);
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            this.f6382a.mo8506b(this.f6383b);
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            this.f6382a.mo8507c(this.f6383b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.j9$b")
    /* renamed from: com.fossil.blesdk.obfuscated.j9$b */
    public class C2112b implements android.animation.ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2385m9 f6384a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.view.View f6385b;

        @DexIgnore
        public C2112b(com.fossil.blesdk.obfuscated.C2110j9 j9Var, com.fossil.blesdk.obfuscated.C2385m9 m9Var, android.view.View view) {
            this.f6384a = m9Var;
            this.f6385b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            this.f6384a.mo13014a(this.f6385b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.j9$c")
    /* renamed from: com.fossil.blesdk.obfuscated.j9$c */
    public static class C2113c implements com.fossil.blesdk.obfuscated.C2190k9 {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2110j9 f6386a;

        @DexIgnore
        /* renamed from: b */
        public boolean f6387b;

        @DexIgnore
        public C2113c(com.fossil.blesdk.obfuscated.C2110j9 j9Var) {
            this.f6386a = j9Var;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8505a(android.view.View view) {
            java.lang.Object tag = view.getTag(2113929216);
            com.fossil.blesdk.obfuscated.C2190k9 k9Var = tag instanceof com.fossil.blesdk.obfuscated.C2190k9 ? (com.fossil.blesdk.obfuscated.C2190k9) tag : null;
            if (k9Var != null) {
                k9Var.mo8505a(view);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.fossil.blesdk.obfuscated.k9} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            int i = this.f6386a.f6381d;
            com.fossil.blesdk.obfuscated.C2190k9 k9Var = null;
            if (i > -1) {
                view.setLayerType(i, (android.graphics.Paint) null);
                this.f6386a.f6381d = -1;
            }
            if (android.os.Build.VERSION.SDK_INT >= 16 || !this.f6387b) {
                com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f6386a;
                java.lang.Runnable runnable = j9Var.f6380c;
                if (runnable != null) {
                    j9Var.f6380c = null;
                    runnable.run();
                }
                java.lang.Object tag = view.getTag(2113929216);
                if (tag instanceof com.fossil.blesdk.obfuscated.C2190k9) {
                    k9Var = tag;
                }
                if (k9Var != null) {
                    k9Var.mo8506b(view);
                }
                this.f6387b = true;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: com.fossil.blesdk.obfuscated.k9} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: c */
        public void mo8507c(android.view.View view) {
            this.f6387b = false;
            com.fossil.blesdk.obfuscated.C2190k9 k9Var = null;
            if (this.f6386a.f6381d > -1) {
                view.setLayerType(2, (android.graphics.Paint) null);
            }
            com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f6386a;
            java.lang.Runnable runnable = j9Var.f6379b;
            if (runnable != null) {
                j9Var.f6379b = null;
                runnable.run();
            }
            java.lang.Object tag = view.getTag(2113929216);
            if (tag instanceof com.fossil.blesdk.obfuscated.C2190k9) {
                k9Var = tag;
            }
            if (k9Var != null) {
                k9Var.mo8507c(view);
            }
        }
    }

    @DexIgnore
    public C2110j9(android.view.View view) {
        this.f6378a = new java.lang.ref.WeakReference<>(view);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12276a(long j) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12283b(float f) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12285c() {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12275a(float f) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public long mo12282b() {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12277a(android.view.animation.Interpolator interpolator) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12284b(long j) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12280a() {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12278a(com.fossil.blesdk.obfuscated.C2190k9 k9Var) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null) {
            if (android.os.Build.VERSION.SDK_INT >= 16) {
                mo12281a(view, k9Var);
            } else {
                view.setTag(2113929216, k9Var);
                mo12281a(view, new com.fossil.blesdk.obfuscated.C2110j9.C2113c(this));
            }
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12281a(android.view.View view, com.fossil.blesdk.obfuscated.C2190k9 k9Var) {
        if (k9Var != null) {
            view.animate().setListener(new com.fossil.blesdk.obfuscated.C2110j9.C2111a(this, k9Var, view));
        } else {
            view.animate().setListener((android.animation.Animator.AnimatorListener) null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2110j9 mo12279a(com.fossil.blesdk.obfuscated.C2385m9 m9Var) {
        android.view.View view = (android.view.View) this.f6378a.get();
        if (view != null && android.os.Build.VERSION.SDK_INT >= 19) {
            com.fossil.blesdk.obfuscated.C2110j9.C2112b bVar = null;
            if (m9Var != null) {
                bVar = new com.fossil.blesdk.obfuscated.C2110j9.C2112b(this, m9Var, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
