package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ int f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ g14 i;

    @DexIgnore
    public h14(g14 g14, List list, int i2, boolean z, boolean z2) {
        this.i = g14;
        this.e = list;
        this.f = i2;
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public void run() {
        this.i.a((List<q14>) this.e, this.f, this.g);
        if (this.h) {
            this.e.clear();
        }
    }
}
