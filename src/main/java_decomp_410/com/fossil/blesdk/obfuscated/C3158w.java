package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w */
public final class C3158w {
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361817;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361818;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361819;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361820;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361821;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361822;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361823;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361824;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361825;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361826;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361827;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361828;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361829;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361830;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361831;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361832;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361833;
    @DexIgnore
    public static /* final */ int actions; // = 2131361834;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361835;
    @DexIgnore
    public static /* final */ int add; // = 2131361841;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361844;
    @DexIgnore
    public static /* final */ int async; // = 2131361854;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361872;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361873;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361917;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131361944;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361945;
    @DexIgnore
    public static /* final */ int content; // = 2131362035;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362036;
    @DexIgnore
    public static /* final */ int custom; // = 2131362041;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362042;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362062;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362063;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362078;
    @DexIgnore
    public static /* final */ int end; // = 2131362079;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362101;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362102;
    @DexIgnore
    public static /* final */ int forever; // = 2131362141;
    @DexIgnore
    public static /* final */ int group_divider; // = 2131362326;
    @DexIgnore
    public static /* final */ int home; // = 2131362335;
    @DexIgnore
    public static /* final */ int icon; // = 2131362359;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362360;
    @DexIgnore
    public static /* final */ int image; // = 2131362367;
    @DexIgnore
    public static /* final */ int info; // = 2131362371;
    @DexIgnore
    public static /* final */ int italic; // = 2131362382;
    @DexIgnore
    public static /* final */ int left; // = 2131362479;
    @DexIgnore
    public static /* final */ int line1; // = 2131362482;
    @DexIgnore
    public static /* final */ int line3; // = 2131362483;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362488;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362489;
    @DexIgnore
    public static /* final */ int message; // = 2131362527;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362534;
    @DexIgnore
    public static /* final */ int none; // = 2131362539;
    @DexIgnore
    public static /* final */ int normal; // = 2131362540;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362541;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362542;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362543;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362570;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362608;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362609;
    @DexIgnore
    public static /* final */ int radio; // = 2131362611;
    @DexIgnore
    public static /* final */ int right; // = 2131362617;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362618;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362619;
    @DexIgnore
    public static /* final */ int screen; // = 2131362681;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131362683;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131362684;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131362685;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131362689;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131362690;
    @DexIgnore
    public static /* final */ int search_button; // = 2131362691;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131362692;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131362693;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131362694;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131362695;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131362696;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131362697;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131362699;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131362703;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131362712;
    @DexIgnore
    public static /* final */ int spacer; // = 2131362727;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131362729;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131362732;
    @DexIgnore
    public static /* final */ int src_in; // = 2131362733;
    @DexIgnore
    public static /* final */ int src_over; // = 2131362734;
    @DexIgnore
    public static /* final */ int start; // = 2131362737;
    @DexIgnore
    public static /* final */ int submenuarrow; // = 2131362740;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131362741;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131362758;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131362759;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131362760;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131362761;
    @DexIgnore
    public static /* final */ int text; // = 2131362765;
    @DexIgnore
    public static /* final */ int text2; // = 2131362766;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131362767;
    @DexIgnore
    public static /* final */ int textSpacerNoTitle; // = 2131362768;
    @DexIgnore
    public static /* final */ int time; // = 2131362777;
    @DexIgnore
    public static /* final */ int title; // = 2131362779;
    @DexIgnore
    public static /* final */ int titleDividerNoCustom; // = 2131362780;
    @DexIgnore
    public static /* final */ int title_template; // = 2131362782;
    @DexIgnore
    public static /* final */ int top; // = 2131362787;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131362788;
    @DexIgnore
    public static /* final */ int uniform; // = 2131362964;

    @DexIgnore
    /* renamed from: up */
    public static /* final */ int f10439up; // = 2131362967;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363047;
}
