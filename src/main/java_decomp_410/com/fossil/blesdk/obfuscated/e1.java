package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.fossil.blesdk.obfuscated.p1;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e1 extends n1 implements p1, View.OnKeyListener, PopupWindow.OnDismissListener {
    @DexIgnore
    public static /* final */ int F; // = x.abc_cascading_menu_item_layout;
    @DexIgnore
    public boolean A;
    @DexIgnore
    public p1.a B;
    @DexIgnore
    public ViewTreeObserver C;
    @DexIgnore
    public PopupWindow.OnDismissListener D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ Handler k;
    @DexIgnore
    public /* final */ List<h1> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<d> m; // = new ArrayList();
    @DexIgnore
    public /* final */ ViewTreeObserver.OnGlobalLayoutListener n; // = new a();
    @DexIgnore
    public /* final */ View.OnAttachStateChangeListener o; // = new b();
    @DexIgnore
    public /* final */ o2 p; // = new c();
    @DexIgnore
    public int q; // = 0;
    @DexIgnore
    public int r; // = 0;
    @DexIgnore
    public View s;
    @DexIgnore
    public View t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public boolean z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (e1.this.d() && e1.this.m.size() > 0 && !e1.this.m.get(0).a.l()) {
                View view = e1.this.t;
                if (view == null || !view.isShown()) {
                    e1.this.dismiss();
                    return;
                }
                for (d dVar : e1.this.m) {
                    dVar.a.c();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = e1.this.C;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    e1.this.C = view.getViewTreeObserver();
                }
                e1 e1Var = e1.this;
                e1Var.C.removeGlobalOnLayoutListener(e1Var.n);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements o2 {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ d e;
            @DexIgnore
            public /* final */ /* synthetic */ MenuItem f;
            @DexIgnore
            public /* final */ /* synthetic */ h1 g;

            @DexIgnore
            public a(d dVar, MenuItem menuItem, h1 h1Var) {
                this.e = dVar;
                this.f = menuItem;
                this.g = h1Var;
            }

            @DexIgnore
            public void run() {
                d dVar = this.e;
                if (dVar != null) {
                    e1.this.E = true;
                    dVar.b.a(false);
                    e1.this.E = false;
                }
                if (this.f.isEnabled() && this.f.hasSubMenu()) {
                    this.g.a(this.f, 4);
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(h1 h1Var, MenuItem menuItem) {
            d dVar = null;
            e1.this.k.removeCallbacksAndMessages((Object) null);
            int size = e1.this.m.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (h1Var == e1.this.m.get(i).b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < e1.this.m.size()) {
                    dVar = e1.this.m.get(i2);
                }
                e1.this.k.postAtTime(new a(dVar, menuItem, h1Var), h1Var, SystemClock.uptimeMillis() + 200);
            }
        }

        @DexIgnore
        public void b(h1 h1Var, MenuItem menuItem) {
            e1.this.k.removeCallbacksAndMessages(h1Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ p2 a;
        @DexIgnore
        public /* final */ h1 b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public d(p2 p2Var, h1 h1Var, int i) {
            this.a = p2Var;
            this.b = h1Var;
            this.c = i;
        }

        @DexIgnore
        public ListView a() {
            return this.a.e();
        }
    }

    @DexIgnore
    public e1(Context context, View view, int i2, int i3, boolean z2) {
        this.f = context;
        this.s = view;
        this.h = i2;
        this.i = i3;
        this.j = z2;
        this.z = false;
        this.u = i();
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(u.abc_config_prefDialogWidth));
        this.k = new Handler();
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
    }

    @DexIgnore
    public void a(h1 h1Var) {
        h1Var.a((p1) this, this.f);
        if (d()) {
            d(h1Var);
        } else {
            this.l.add(h1Var);
        }
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public void c() {
        if (!d()) {
            for (h1 d2 : this.l) {
                d(d2);
            }
            this.l.clear();
            this.t = this.s;
            if (this.t != null) {
                boolean z2 = this.C == null;
                this.C = this.t.getViewTreeObserver();
                if (z2) {
                    this.C.addOnGlobalLayoutListener(this.n);
                }
                this.t.addOnAttachStateChangeListener(this.o);
            }
        }
    }

    @DexIgnore
    public final int d(int i2) {
        List<d> list = this.m;
        ListView a2 = list.get(list.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.t.getWindowVisibleDisplayFrame(rect);
        if (this.u == 1) {
            if (iArr[0] + a2.getWidth() + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @DexIgnore
    public void dismiss() {
        int size = this.m.size();
        if (size > 0) {
            d[] dVarArr = (d[]) this.m.toArray(new d[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                d dVar = dVarArr[i2];
                if (dVar.a.d()) {
                    dVar.a.dismiss();
                }
            }
        }
    }

    @DexIgnore
    public ListView e() {
        if (this.m.isEmpty()) {
            return null;
        }
        List<d> list = this.m;
        return list.get(list.size() - 1).a();
    }

    @DexIgnore
    public boolean f() {
        return false;
    }

    @DexIgnore
    public final p2 h() {
        p2 p2Var = new p2(this.f, (AttributeSet) null, this.h, this.i);
        p2Var.a(this.p);
        p2Var.a((AdapterView.OnItemClickListener) this);
        p2Var.a((PopupWindow.OnDismissListener) this);
        p2Var.a(this.s);
        p2Var.c(this.r);
        p2Var.a(true);
        p2Var.e(2);
        return p2Var;
    }

    @DexIgnore
    public final int i() {
        return f9.k(this.s) == 1 ? 0 : 1;
    }

    @DexIgnore
    public void onDismiss() {
        d dVar;
        int size = this.m.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                dVar = null;
                break;
            }
            dVar = this.m.get(i2);
            if (!dVar.a.d()) {
                break;
            }
            i2++;
        }
        if (dVar != null) {
            dVar.b.a(false);
        }
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    public void b(int i2) {
        this.v = true;
        this.x = i2;
    }

    @DexIgnore
    public final MenuItem a(h1 h1Var, h1 h1Var2) {
        int size = h1Var.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = h1Var.getItem(i2);
            if (item.hasSubMenu() && h1Var2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @DexIgnore
    public final View a(d dVar, h1 h1Var) {
        int i2;
        g1 g1Var;
        MenuItem a2 = a(dVar.b, h1Var);
        if (a2 == null) {
            return null;
        }
        ListView a3 = dVar.a();
        ListAdapter adapter = a3.getAdapter();
        int i3 = 0;
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            g1Var = (g1) headerViewListAdapter.getWrappedAdapter();
        } else {
            g1Var = (g1) adapter;
            i2 = 0;
        }
        int count = g1Var.getCount();
        while (true) {
            if (i3 >= count) {
                i3 = -1;
                break;
            } else if (a2 == g1Var.getItem(i3)) {
                break;
            } else {
                i3++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a3.getChildCount()) {
            return null;
        }
        return a3.getChildAt(firstVisiblePosition);
    }

    @DexIgnore
    public final void d(h1 h1Var) {
        View view;
        d dVar;
        int i2;
        int i3;
        int i4;
        LayoutInflater from = LayoutInflater.from(this.f);
        g1 g1Var = new g1(h1Var, from, this.j, F);
        if (!d() && this.z) {
            g1Var.a(true);
        } else if (d()) {
            g1Var.a(n1.b(h1Var));
        }
        int a2 = n1.a(g1Var, (ViewGroup) null, this.f, this.g);
        p2 h2 = h();
        h2.a((ListAdapter) g1Var);
        h2.b(a2);
        h2.c(this.r);
        if (this.m.size() > 0) {
            List<d> list = this.m;
            dVar = list.get(list.size() - 1);
            view = a(dVar, h1Var);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            h2.d(false);
            h2.a((Object) null);
            int d2 = d(a2);
            boolean z2 = d2 == 1;
            this.u = d2;
            if (Build.VERSION.SDK_INT >= 26) {
                h2.a(view);
                i3 = 0;
                i2 = 0;
            } else {
                int[] iArr = new int[2];
                this.s.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.r & 7) == 5) {
                    iArr[0] = iArr[0] + this.s.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i2 = iArr2[0] - iArr[0];
                i3 = iArr2[1] - iArr[1];
            }
            if ((this.r & 5) != 5) {
                if (z2) {
                    a2 = view.getWidth();
                }
                i4 = i2 - a2;
                h2.d(i4);
                h2.b(true);
                h2.h(i3);
            } else if (!z2) {
                a2 = view.getWidth();
                i4 = i2 - a2;
                h2.d(i4);
                h2.b(true);
                h2.h(i3);
            }
            i4 = i2 + a2;
            h2.d(i4);
            h2.b(true);
            h2.h(i3);
        } else {
            if (this.v) {
                h2.d(this.x);
            }
            if (this.w) {
                h2.h(this.y);
            }
            h2.a(g());
        }
        this.m.add(new d(h2, h1Var, this.u));
        h2.c();
        ListView e = h2.e();
        e.setOnKeyListener(this);
        if (dVar == null && this.A && h1Var.h() != null) {
            FrameLayout frameLayout = (FrameLayout) from.inflate(x.abc_popup_menu_header_item_layout, e, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(h1Var.h());
            e.addHeaderView(frameLayout, (Object) null, false);
            h2.c();
        }
    }

    @DexIgnore
    public final int c(h1 h1Var) {
        int size = this.m.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (h1Var == this.m.get(i2).b) {
                return i2;
            }
        }
        return -1;
    }

    @DexIgnore
    public void c(int i2) {
        this.w = true;
        this.y = i2;
    }

    @DexIgnore
    public void c(boolean z2) {
        this.A = z2;
    }

    @DexIgnore
    public void a(boolean z2) {
        for (d a2 : this.m) {
            n1.a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(p1.a aVar) {
        this.B = aVar;
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        for (d next : this.m) {
            if (v1Var == next.b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        a((h1) v1Var);
        p1.a aVar = this.B;
        if (aVar != null) {
            aVar.a(v1Var);
        }
        return true;
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z2) {
        int c2 = c(h1Var);
        if (c2 >= 0) {
            int i2 = c2 + 1;
            if (i2 < this.m.size()) {
                this.m.get(i2).b.a(false);
            }
            d remove = this.m.remove(c2);
            remove.b.b((p1) this);
            if (this.E) {
                remove.a.b((Object) null);
                remove.a.a(0);
            }
            remove.a.dismiss();
            int size = this.m.size();
            if (size > 0) {
                this.u = this.m.get(size - 1).c;
            } else {
                this.u = i();
            }
            if (size == 0) {
                dismiss();
                p1.a aVar = this.B;
                if (aVar != null) {
                    aVar.a(h1Var, true);
                }
                ViewTreeObserver viewTreeObserver = this.C;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.C.removeGlobalOnLayoutListener(this.n);
                    }
                    this.C = null;
                }
                this.t.removeOnAttachStateChangeListener(this.o);
                this.D.onDismiss();
            } else if (z2) {
                this.m.get(0).b.a(false);
            }
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (this.q != i2) {
            this.q = i2;
            this.r = p8.a(i2, f9.k(this.s));
        }
    }

    @DexIgnore
    public void a(View view) {
        if (this.s != view) {
            this.s = view;
            this.r = p8.a(this.q, f9.k(this.s));
        }
    }

    @DexIgnore
    public boolean d() {
        return this.m.size() > 0 && this.m.get(0).a.d();
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.D = onDismissListener;
    }
}
