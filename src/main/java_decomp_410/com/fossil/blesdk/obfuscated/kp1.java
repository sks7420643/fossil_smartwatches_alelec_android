package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kp1 extends jk0 implements xo1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kp1> CREATOR; // = new lp1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ List<zp1> f;

    @DexIgnore
    public kp1(String str, List<zp1> list) {
        this.e = str;
        this.f = list;
        bk0.a(this.e);
        bk0.a(this.f);
    }

    @DexIgnore
    public final String H() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || kp1.class != obj.getClass()) {
            return false;
        }
        kp1 kp1 = (kp1) obj;
        String str = this.e;
        if (str == null ? kp1.e != null : !str.equals(kp1.e)) {
            return false;
        }
        List<zp1> list = this.f;
        List<zp1> list2 = kp1.f;
        return list == null ? list2 == null : list.equals(list2);
    }

    @DexIgnore
    public final int hashCode() {
        String str = this.e;
        int i = 0;
        int hashCode = ((str != null ? str.hashCode() : 0) + 31) * 31;
        List<zp1> list = this.f;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final String toString() {
        String str = this.e;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, H(), false);
        kk0.b(parcel, 3, this.f, false);
        kk0.a(parcel, a);
    }
}
