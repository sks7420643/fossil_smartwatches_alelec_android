package com.fossil.blesdk.obfuscated;

import androidx.appcompat.view.ActionMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface d0 {
    @DexIgnore
    void onSupportActionModeFinished(ActionMode actionMode);

    @DexIgnore
    void onSupportActionModeStarted(ActionMode actionMode);

    @DexIgnore
    ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback);
}
