package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ng */
public final class C2489ng implements com.fossil.blesdk.obfuscated.C1945hg.C1949c {
    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1945hg mo11648a(com.fossil.blesdk.obfuscated.C1945hg.C1947b bVar) {
        return new com.fossil.blesdk.obfuscated.C2409mg(bVar.f5734a, bVar.f5735b, bVar.f5736c);
    }
}
