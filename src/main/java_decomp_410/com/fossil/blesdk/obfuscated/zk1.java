package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zk1 extends cl1 {
    @DexIgnore
    public /* final */ AlarmManager d; // = ((AlarmManager) getContext().getSystemService(Alarm.TABLE_NAME));
    @DexIgnore
    public /* final */ fm1 e;
    @DexIgnore
    public Integer f;

    @DexIgnore
    public zk1(dl1 dl1) {
        super(dl1);
        this.e = new al1(this, dl1.B(), dl1);
    }

    @DexIgnore
    public final void a(long j) {
        q();
        b();
        Context context = getContext();
        if (!oh1.a(context)) {
            d().z().a("Receiver not registered/enabled");
        }
        if (!nl1.a(context, false)) {
            d().z().a("Service not registered/enabled");
        }
        t();
        long c = c().c() + j;
        if (j < Math.max(0, jg1.H.a().longValue()) && !this.e.d()) {
            d().A().a("Scheduling upload with DelayedRunnable");
            this.e.a(j);
        }
        b();
        if (Build.VERSION.SDK_INT >= 24) {
            d().A().a("Scheduling upload with JobScheduler");
            Context context2 = getContext();
            ComponentName componentName = new ComponentName(context2, "com.google.android.gms.measurement.AppMeasurementJobService");
            int u = u();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
            JobInfo build = new JobInfo.Builder(u, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build();
            d().A().a("Scheduling job. JobID", Integer.valueOf(u));
            j51.a(context2, build, "com.google.android.gms", "UploadAlarm");
            return;
        }
        d().A().a("Scheduling upload with AlarmManager");
        this.d.setInexactRepeating(2, c, Math.max(jg1.C.a().longValue(), j), v());
    }

    @DexIgnore
    public final boolean r() {
        this.d.cancel(v());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        w();
        return false;
    }

    @DexIgnore
    public final void t() {
        q();
        this.d.cancel(v());
        this.e.a();
        if (Build.VERSION.SDK_INT >= 24) {
            w();
        }
    }

    @DexIgnore
    public final int u() {
        if (this.f == null) {
            String valueOf = String.valueOf(getContext().getPackageName());
            this.f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final PendingIntent v() {
        Context context = getContext();
        return PendingIntent.getBroadcast(context, 0, new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }

    @DexIgnore
    @TargetApi(24)
    public final void w() {
        int u = u();
        d().A().a("Cancelling job. JobID", Integer.valueOf(u));
        ((JobScheduler) getContext().getSystemService("jobscheduler")).cancel(u);
    }
}
