package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.network.HttpMethod;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g84 extends i74 {
    @DexIgnore
    public g84(v44 v44, String str, String str2, z64 z64) {
        super(v44, str, str2, z64, HttpMethod.PUT);
    }
}
