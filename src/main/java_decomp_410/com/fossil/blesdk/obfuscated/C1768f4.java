package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f4 */
public class C1768f4 extends android.graphics.drawable.Drawable {

    @DexIgnore
    /* renamed from: q */
    public static /* final */ double f5009q; // = java.lang.Math.cos(java.lang.Math.toRadians(45.0d));

    @DexIgnore
    /* renamed from: r */
    public static com.fossil.blesdk.obfuscated.C1768f4.C1769a f5010r;

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f5011a;

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Paint f5012b;

    @DexIgnore
    /* renamed from: c */
    public android.graphics.Paint f5013c;

    @DexIgnore
    /* renamed from: d */
    public android.graphics.Paint f5014d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.RectF f5015e;

    @DexIgnore
    /* renamed from: f */
    public float f5016f;

    @DexIgnore
    /* renamed from: g */
    public android.graphics.Path f5017g;

    @DexIgnore
    /* renamed from: h */
    public float f5018h;

    @DexIgnore
    /* renamed from: i */
    public float f5019i;

    @DexIgnore
    /* renamed from: j */
    public float f5020j;

    @DexIgnore
    /* renamed from: k */
    public android.content.res.ColorStateList f5021k;

    @DexIgnore
    /* renamed from: l */
    public boolean f5022l; // = true;

    @DexIgnore
    /* renamed from: m */
    public /* final */ int f5023m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ int f5024n;

    @DexIgnore
    /* renamed from: o */
    public boolean f5025o; // = true;

    @DexIgnore
    /* renamed from: p */
    public boolean f5026p; // = false;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.f4$a */
    public interface C1769a {
        @DexIgnore
        /* renamed from: a */
        void mo9043a(android.graphics.Canvas canvas, android.graphics.RectF rectF, float f, android.graphics.Paint paint);
    }

    @DexIgnore
    public C1768f4(android.content.res.Resources resources, android.content.res.ColorStateList colorStateList, float f, float f2, float f3) {
        this.f5023m = resources.getColor(com.fossil.blesdk.obfuscated.C3082v3.cardview_shadow_start_color);
        this.f5024n = resources.getColor(com.fossil.blesdk.obfuscated.C3082v3.cardview_shadow_end_color);
        this.f5011a = resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C3166w3.cardview_compat_inset_shadow);
        this.f5012b = new android.graphics.Paint(5);
        mo10710a(colorStateList);
        this.f5013c = new android.graphics.Paint(5);
        this.f5013c.setStyle(android.graphics.Paint.Style.FILL);
        this.f5016f = (float) ((int) (f + 0.5f));
        this.f5015e = new android.graphics.RectF();
        this.f5014d = new android.graphics.Paint(this.f5013c);
        this.f5014d.setAntiAlias(false);
        mo10709a(f2, f3);
    }

    @DexIgnore
    /* renamed from: b */
    public static float m6709b(float f, float f2, boolean z) {
        return z ? (float) (((double) (f * 1.5f)) + ((1.0d - f5009q) * ((double) f2))) : f * 1.5f;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10710a(android.content.res.ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = android.content.res.ColorStateList.valueOf(0);
        }
        this.f5021k = colorStateList;
        this.f5012b.setColor(this.f5021k.getColorForState(getState(), this.f5021k.getDefaultColor()));
    }

    @DexIgnore
    /* renamed from: c */
    public float mo10718c() {
        return this.f5016f;
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo10721d(float f) {
        int i = (int) (f + 0.5f);
        return i % 2 == 1 ? i - 1 : i;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        if (this.f5022l) {
            mo10712a(getBounds());
            this.f5022l = false;
        }
        canvas.translate(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f5020j / 2.0f);
        mo10711a(canvas);
        canvas.translate(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (-this.f5020j) / 2.0f);
        f5010r.mo9043a(canvas, this.f5015e, this.f5016f, this.f5012b);
    }

    @DexIgnore
    /* renamed from: e */
    public float mo10723e() {
        float f = this.f5018h;
        return (java.lang.Math.max(f, this.f5016f + ((float) this.f5011a) + ((f * 1.5f) / 2.0f)) * 2.0f) + (((this.f5018h * 1.5f) + ((float) this.f5011a)) * 2.0f);
    }

    @DexIgnore
    /* renamed from: f */
    public float mo10724f() {
        float f = this.f5018h;
        return (java.lang.Math.max(f, this.f5016f + ((float) this.f5011a) + (f / 2.0f)) * 2.0f) + ((this.f5018h + ((float) this.f5011a)) * 2.0f);
    }

    @DexIgnore
    /* renamed from: g */
    public float mo10725g() {
        return this.f5020j;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean getPadding(android.graphics.Rect rect) {
        int ceil = (int) java.lang.Math.ceil((double) m6709b(this.f5018h, this.f5016f, this.f5025o));
        int ceil2 = (int) java.lang.Math.ceil((double) m6708a(this.f5018h, this.f5016f, this.f5025o));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    @DexIgnore
    public boolean isStateful() {
        android.content.res.ColorStateList colorStateList = this.f5021k;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        super.onBoundsChange(rect);
        this.f5022l = true;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        android.content.res.ColorStateList colorStateList = this.f5021k;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.f5012b.getColor() == colorForState) {
            return false;
        }
        this.f5012b.setColor(colorForState);
        this.f5022l = true;
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f5012b.setAlpha(i);
        this.f5013c.setAlpha(i);
        this.f5014d.setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        this.f5012b.setColorFilter(colorFilter);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10717b(android.graphics.Rect rect) {
        getPadding(rect);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10719c(float f) {
        mo10709a(f, this.f5018h);
    }

    @DexIgnore
    /* renamed from: d */
    public float mo10720d() {
        return this.f5018h;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10713a(boolean z) {
        this.f5025o = z;
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10715b(float f) {
        mo10709a(this.f5020j, f);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10716b(android.content.res.ColorStateList colorStateList) {
        mo10710a(colorStateList);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10709a(float f, float f2) {
        if (f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new java.lang.IllegalArgumentException("Invalid shadow size " + f + ". Must be >= 0");
        } else if (f2 >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float d = (float) mo10721d(f);
            float d2 = (float) mo10721d(f2);
            if (d > d2) {
                if (!this.f5026p) {
                    this.f5026p = true;
                }
                d = d2;
            }
            if (this.f5020j != d || this.f5018h != d2) {
                this.f5020j = d;
                this.f5018h = d2;
                this.f5019i = (float) ((int) ((d * 1.5f) + ((float) this.f5011a) + 0.5f));
                this.f5022l = true;
                invalidateSelf();
            }
        } else {
            throw new java.lang.IllegalArgumentException("Invalid max shadow size " + f2 + ". Must be >= 0");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.res.ColorStateList mo10714b() {
        return this.f5021k;
    }

    @DexIgnore
    /* renamed from: a */
    public static float m6708a(float f, float f2, boolean z) {
        return z ? (float) (((double) f) + ((1.0d - f5009q) * ((double) f2))) : f;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10708a(float f) {
        if (f >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f2 = (float) ((int) (f + 0.5f));
            if (this.f5016f != f2) {
                this.f5016f = f2;
                this.f5022l = true;
                invalidateSelf();
                return;
            }
            return;
        }
        throw new java.lang.IllegalArgumentException("Invalid radius " + f + ". Must be >= 0");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10711a(android.graphics.Canvas canvas) {
        float f = this.f5016f;
        float f2 = (-f) - this.f5019i;
        float f3 = f + ((float) this.f5011a) + (this.f5020j / 2.0f);
        float f4 = f3 * 2.0f;
        boolean z = this.f5015e.width() - f4 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = this.f5015e.height() - f4 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int save = canvas.save();
        android.graphics.RectF rectF = this.f5015e;
        canvas.translate(rectF.left + f3, rectF.top + f3);
        canvas.drawPath(this.f5017g, this.f5013c);
        if (z) {
            canvas.drawRect(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, this.f5015e.width() - f4, -this.f5016f, this.f5014d);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        android.graphics.RectF rectF2 = this.f5015e;
        canvas.translate(rectF2.right - f3, rectF2.bottom - f3);
        canvas.rotate(180.0f);
        canvas.drawPath(this.f5017g, this.f5013c);
        if (z) {
            canvas.drawRect(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, this.f5015e.width() - f4, (-this.f5016f) + this.f5019i, this.f5014d);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        android.graphics.RectF rectF3 = this.f5015e;
        canvas.translate(rectF3.left + f3, rectF3.bottom - f3);
        canvas.rotate(270.0f);
        canvas.drawPath(this.f5017g, this.f5013c);
        if (z2) {
            canvas.drawRect(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, this.f5015e.height() - f4, -this.f5016f, this.f5014d);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        android.graphics.RectF rectF4 = this.f5015e;
        canvas.translate(rectF4.right - f3, rectF4.top + f3);
        canvas.rotate(90.0f);
        canvas.drawPath(this.f5017g, this.f5013c);
        if (z2) {
            canvas.drawRect(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f2, this.f5015e.height() - f4, -this.f5016f, this.f5014d);
        }
        canvas.restoreToCount(save4);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10707a() {
        float f = this.f5016f;
        android.graphics.RectF rectF = new android.graphics.RectF(-f, -f, f, f);
        android.graphics.RectF rectF2 = new android.graphics.RectF(rectF);
        float f2 = this.f5019i;
        rectF2.inset(-f2, -f2);
        android.graphics.Path path = this.f5017g;
        if (path == null) {
            this.f5017g = new android.graphics.Path();
        } else {
            path.reset();
        }
        this.f5017g.setFillType(android.graphics.Path.FillType.EVEN_ODD);
        this.f5017g.moveTo(-this.f5016f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f5017g.rLineTo(-this.f5019i, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f5017g.arcTo(rectF2, 180.0f, 90.0f, false);
        this.f5017g.arcTo(rectF, 270.0f, -90.0f, false);
        this.f5017g.close();
        float f3 = this.f5016f;
        float f4 = this.f5019i;
        android.graphics.Paint paint = this.f5013c;
        float f5 = f3 + f4;
        int i = this.f5023m;
        android.graphics.RadialGradient radialGradient = new android.graphics.RadialGradient(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5, new int[]{i, i, this.f5024n}, new float[]{0.0f, f3 / (f3 + f4), 1.0f}, android.graphics.Shader.TileMode.CLAMP);
        paint.setShader(radialGradient);
        android.graphics.Paint paint2 = this.f5014d;
        float f6 = this.f5016f;
        float f7 = this.f5019i;
        float f8 = (-f6) + f7;
        float f9 = (-f6) - f7;
        int i2 = this.f5023m;
        int[] iArr = {i2, i2, this.f5024n};
        android.graphics.LinearGradient linearGradient = new android.graphics.LinearGradient(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f8, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, iArr, new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f}, android.graphics.Shader.TileMode.CLAMP);
        paint2.setShader(linearGradient);
        this.f5014d.setAntiAlias(false);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10712a(android.graphics.Rect rect) {
        float f = this.f5018h;
        float f2 = 1.5f * f;
        this.f5015e.set(((float) rect.left) + f, ((float) rect.top) + f2, ((float) rect.right) - f, ((float) rect.bottom) - f2);
        mo10707a();
    }
}
