package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kq */
public class C2236kq implements com.fossil.blesdk.obfuscated.C2149jq {
    @DexIgnore
    /* renamed from: a */
    public void mo12444a() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12445a(int i) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12446a(android.graphics.Bitmap bitmap) {
        bitmap.recycle();
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Bitmap mo12447b(int i, int i2, android.graphics.Bitmap.Config config) {
        return mo12443a(i, i2, config);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo12443a(int i, int i2, android.graphics.Bitmap.Config config) {
        return android.graphics.Bitmap.createBitmap(i, i2, config);
    }
}
