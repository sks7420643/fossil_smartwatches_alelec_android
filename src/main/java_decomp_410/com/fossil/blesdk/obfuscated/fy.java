package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fy implements iy {
    @DexIgnore
    public /* final */ bx a;

    @DexIgnore
    public fy(bx bxVar) {
        this.a = bxVar;
    }

    @DexIgnore
    public static fy a() throws NoClassDefFoundError, IllegalStateException {
        return a(bx.w());
    }

    @DexIgnore
    public static fy a(bx bxVar) throws IllegalStateException {
        if (bxVar != null) {
            return new fy(bxVar);
        }
        throw new IllegalStateException("Answers must be initialized before logging kit events");
    }

    @DexIgnore
    public void a(hy hyVar) {
        try {
            this.a.a(hyVar.a());
        } catch (Throwable th) {
            Log.w("AnswersKitEventLogger", "Unexpected error sending Answers event", th);
        }
    }
}
