package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.workoutsession.ActivityType;
import com.fossil.blesdk.device.data.workoutsession.SessionState;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d50 extends SingleRequestPhase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> B; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.DEVICE_CONFIG}));
    @DexIgnore
    public WorkoutSession C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d50(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.GET_CURRENT_WORKOUT_SESSION, new v70(peripheral));
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
    }

    @DexIgnore
    public void c(Request request) {
        kd4.b(request, "request");
        this.C = ((v70) request).I();
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.B;
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.WORKOUT_SESSION;
        WorkoutSession workoutSession = this.C;
        return wa0.a(x, jSONKey, workoutSession != null ? workoutSession.toJSONObject() : null);
    }

    @DexIgnore
    public WorkoutSession i() {
        WorkoutSession workoutSession = this.C;
        return workoutSession != null ? workoutSession : new WorkoutSession(0, ActivityType.IDLE, SessionState.COMPLETED, 0, 0, 0, 0, 0, 0, 0, 0);
    }
}
