package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.je */
public class C2121je {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2121je.C2123b f6405a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2121je.C2122a f6406b; // = new com.fossil.blesdk.obfuscated.C2121je.C2122a();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.List<android.view.View> f6407c; // = new java.util.ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.je$a")
    /* renamed from: com.fossil.blesdk.obfuscated.je$a */
    public static class C2122a {

        @DexIgnore
        /* renamed from: a */
        public long f6408a; // = 0;

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2121je.C2122a f6409b;

        @DexIgnore
        /* renamed from: a */
        public final void mo12330a() {
            if (this.f6409b == null) {
                this.f6409b = new com.fossil.blesdk.obfuscated.C2121je.C2122a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo12334b() {
            this.f6408a = 0;
            com.fossil.blesdk.obfuscated.C2121je.C2122a aVar = this.f6409b;
            if (aVar != null) {
                aVar.mo12334b();
            }
        }

        @DexIgnore
        /* renamed from: c */
        public boolean mo12335c(int i) {
            if (i < 64) {
                return (this.f6408a & (1 << i)) != 0;
            }
            mo12330a();
            return this.f6409b.mo12335c(i - 64);
        }

        @DexIgnore
        /* renamed from: d */
        public boolean mo12336d(int i) {
            if (i >= 64) {
                mo12330a();
                return this.f6409b.mo12336d(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.f6408a & j) != 0;
            this.f6408a &= ~j;
            long j2 = j - 1;
            long j3 = this.f6408a;
            this.f6408a = java.lang.Long.rotateRight(j3 & (~j2), 1) | (j3 & j2);
            com.fossil.blesdk.obfuscated.C2121je.C2122a aVar = this.f6409b;
            if (aVar != null) {
                if (aVar.mo12335c(0)) {
                    mo12337e(63);
                }
                this.f6409b.mo12336d(0);
            }
            return z;
        }

        @DexIgnore
        /* renamed from: e */
        public void mo12337e(int i) {
            if (i >= 64) {
                mo12330a();
                this.f6409b.mo12337e(i - 64);
                return;
            }
            this.f6408a |= 1 << i;
        }

        @DexIgnore
        public java.lang.String toString() {
            if (this.f6409b == null) {
                return java.lang.Long.toBinaryString(this.f6408a);
            }
            return this.f6409b.toString() + "xx" + java.lang.Long.toBinaryString(this.f6408a);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12331a(int i) {
            if (i >= 64) {
                com.fossil.blesdk.obfuscated.C2121je.C2122a aVar = this.f6409b;
                if (aVar != null) {
                    aVar.mo12331a(i - 64);
                    return;
                }
                return;
            }
            this.f6408a &= ~(1 << i);
        }

        @DexIgnore
        /* renamed from: b */
        public int mo12333b(int i) {
            com.fossil.blesdk.obfuscated.C2121je.C2122a aVar = this.f6409b;
            if (aVar == null) {
                if (i >= 64) {
                    return java.lang.Long.bitCount(this.f6408a);
                }
                return java.lang.Long.bitCount(this.f6408a & ((1 << i) - 1));
            } else if (i < 64) {
                return java.lang.Long.bitCount(this.f6408a & ((1 << i) - 1));
            } else {
                return aVar.mo12333b(i - 64) + java.lang.Long.bitCount(this.f6408a);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12332a(int i, boolean z) {
            if (i >= 64) {
                mo12330a();
                this.f6409b.mo12332a(i - 64, z);
                return;
            }
            boolean z2 = (this.f6408a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            long j2 = this.f6408a;
            this.f6408a = ((j2 & (~j)) << 1) | (j2 & j);
            if (z) {
                mo12337e(i);
            } else {
                mo12331a(i);
            }
            if (z2 || this.f6409b != null) {
                mo12330a();
                this.f6409b.mo12332a(0, z2);
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.je$b */
    public interface C2123b {
        @DexIgnore
        /* renamed from: a */
        int mo2791a();

        @DexIgnore
        /* renamed from: a */
        android.view.View mo2792a(int i);

        @DexIgnore
        /* renamed from: a */
        void mo2793a(android.view.View view);

        @DexIgnore
        /* renamed from: a */
        void mo2794a(android.view.View view, int i);

        @DexIgnore
        /* renamed from: a */
        void mo2795a(android.view.View view, int i, android.view.ViewGroup.LayoutParams layoutParams);

        @DexIgnore
        /* renamed from: b */
        int mo2796b(android.view.View view);

        @DexIgnore
        /* renamed from: b */
        void mo2797b();

        @DexIgnore
        /* renamed from: b */
        void mo2798b(int i);

        @DexIgnore
        /* renamed from: c */
        androidx.recyclerview.widget.RecyclerView.ViewHolder mo2799c(android.view.View view);

        @DexIgnore
        /* renamed from: c */
        void mo2800c(int i);

        @DexIgnore
        /* renamed from: d */
        void mo2801d(android.view.View view);
    }

    @DexIgnore
    public C2121je(com.fossil.blesdk.obfuscated.C2121je.C2123b bVar) {
        this.f6405a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12314a(android.view.View view, boolean z) {
        mo12313a(view, -1, z);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12317b(android.view.View view) {
        this.f6407c.add(view);
        this.f6405a.mo2793a(view);
    }

    @DexIgnore
    /* renamed from: c */
    public android.view.View mo12319c(int i) {
        return this.f6405a.mo2792a(mo12321d(i));
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo12321d(int i) {
        if (i < 0) {
            return -1;
        }
        int a = this.f6405a.mo2791a();
        int i2 = i;
        while (i2 < a) {
            int b = i - (i2 - this.f6406b.mo12333b(i2));
            if (b == 0) {
                while (this.f6406b.mo12335c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += b;
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo12324e(android.view.View view) {
        int b = this.f6405a.mo2796b(view);
        if (b >= 0) {
            if (this.f6406b.mo12336d(b)) {
                mo12328h(view);
            }
            this.f6405a.mo2800c(b);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo12325f(int i) {
        int d = mo12321d(i);
        android.view.View a = this.f6405a.mo2792a(d);
        if (a != null) {
            if (this.f6406b.mo12336d(d)) {
                mo12328h(a);
            }
            this.f6405a.mo2800c(d);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo12327g(android.view.View view) {
        int b = this.f6405a.mo2796b(view);
        if (b < 0) {
            throw new java.lang.IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.f6406b.mo12335c(b)) {
            this.f6406b.mo12331a(b);
            mo12328h(view);
        } else {
            throw new java.lang.RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    @DexIgnore
    /* renamed from: h */
    public final boolean mo12328h(android.view.View view) {
        if (!this.f6407c.remove(view)) {
            return false;
        }
        this.f6405a.mo2801d(view);
        return true;
    }

    @DexIgnore
    public java.lang.String toString() {
        return this.f6406b.toString() + ", hidden list:" + this.f6407c.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12313a(android.view.View view, int i, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f6405a.mo2791a();
        } else {
            i2 = mo12321d(i);
        }
        this.f6406b.mo12332a(i2, z);
        if (z) {
            mo12317b(view);
        }
        this.f6405a.mo2794a(view, i2);
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo12316b(int i) {
        int size = this.f6407c.size();
        for (int i2 = 0; i2 < size; i2++) {
            android.view.View view = this.f6407c.get(i2);
            androidx.recyclerview.widget.RecyclerView.ViewHolder c = this.f6405a.mo2799c(view);
            if (c.getLayoutPosition() == i && !c.isInvalid() && !c.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12320c() {
        this.f6406b.mo12334b();
        for (int size = this.f6407c.size() - 1; size >= 0; size--) {
            this.f6405a.mo2801d(this.f6407c.get(size));
            this.f6407c.remove(size);
        }
        this.f6405a.mo2797b();
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo12322d(android.view.View view) {
        return this.f6407c.contains(view);
    }

    @DexIgnore
    /* renamed from: e */
    public android.view.View mo12323e(int i) {
        return this.f6405a.mo2792a(i);
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo12326f(android.view.View view) {
        int b = this.f6405a.mo2796b(view);
        if (b == -1) {
            mo12328h(view);
            return true;
        } else if (!this.f6406b.mo12335c(b)) {
            return false;
        } else {
            this.f6406b.mo12336d(b);
            mo12328h(view);
            this.f6405a.mo2800c(b);
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12312a(android.view.View view, int i, android.view.ViewGroup.LayoutParams layoutParams, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.f6405a.mo2791a();
        } else {
            i2 = mo12321d(i);
        }
        this.f6406b.mo12332a(i2, z);
        if (z) {
            mo12317b(view);
        }
        this.f6405a.mo2795a(view, i2, layoutParams);
    }

    @DexIgnore
    /* renamed from: c */
    public int mo12318c(android.view.View view) {
        int b = this.f6405a.mo2796b(view);
        if (b != -1 && !this.f6406b.mo12335c(b)) {
            return b - this.f6406b.mo12333b(b);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo12315b() {
        return this.f6405a.mo2791a();
    }

    @DexIgnore
    /* renamed from: a */
    public int mo12309a() {
        return this.f6405a.mo2791a() - this.f6407c.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12310a(int i) {
        int d = mo12321d(i);
        this.f6406b.mo12336d(d);
        this.f6405a.mo2798b(d);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12311a(android.view.View view) {
        int b = this.f6405a.mo2796b(view);
        if (b >= 0) {
            this.f6406b.mo12337e(b);
            mo12317b(view);
            return;
        }
        throw new java.lang.IllegalArgumentException("view is not a child, cannot hide " + view);
    }
}
