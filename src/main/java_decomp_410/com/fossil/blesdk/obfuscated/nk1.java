package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.fossil.blesdk.obfuscated.sk1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nk1<T extends Context & sk1> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public nk1(T t) {
        bk0.a(t);
        this.a = t;
    }

    @DexIgnore
    public final void a() {
        xh1 a2 = xh1.a((Context) this.a, (og1) null);
        tg1 d = a2.d();
        a2.b();
        d.A().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final void b() {
        xh1 a2 = xh1.a((Context) this.a, (og1) null);
        tg1 d = a2.d();
        a2.b();
        d.A().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final boolean c(Intent intent) {
        if (intent == null) {
            c().s().a("onUnbind called with null intent");
            return true;
        }
        c().A().a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    @DexIgnore
    public final tg1 c() {
        return xh1.a((Context) this.a, (og1) null).d();
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        xh1 a2 = xh1.a((Context) this.a, (og1) null);
        tg1 d = a2.d();
        if (intent == null) {
            d.v().a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a2.b();
        d.A().a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            a((Runnable) new ok1(this, i2, d, intent));
        }
        return 2;
    }

    @DexIgnore
    public final void b(Intent intent) {
        if (intent == null) {
            c().s().a("onRebind called with null intent");
            return;
        }
        c().A().a("onRebind called. action", intent.getAction());
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        dl1 a2 = dl1.a((Context) this.a);
        a2.a().a((Runnable) new rk1(this, a2, runnable));
    }

    @DexIgnore
    public final IBinder a(Intent intent) {
        if (intent == null) {
            c().s().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new zh1(dl1.a((Context) this.a));
        }
        c().v().a("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean a(JobParameters jobParameters) {
        xh1 a2 = xh1.a((Context) this.a, (og1) null);
        tg1 d = a2.d();
        String string = jobParameters.getExtras().getString("action");
        a2.b();
        d.A().a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a((Runnable) new qk1(this, d, jobParameters));
        return true;
    }

    @DexIgnore
    public final /* synthetic */ void a(tg1 tg1, JobParameters jobParameters) {
        tg1.A().a("AppMeasurementJobService processed last upload request.");
        ((sk1) this.a).a(jobParameters, false);
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, tg1 tg1, Intent intent) {
        if (((sk1) this.a).a(i)) {
            tg1.A().a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            c().A().a("Completed wakeful intent.");
            ((sk1) this.a).a(intent);
        }
    }
}
