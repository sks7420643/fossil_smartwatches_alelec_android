package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wb3 implements Factory<GoalTrackingOverviewWeekPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewWeekPresenter a(ub3 ub3, UserRepository userRepository, en2 en2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewWeekPresenter(ub3, userRepository, en2, goalTrackingRepository);
    }
}
