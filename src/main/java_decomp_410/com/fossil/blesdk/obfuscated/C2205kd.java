package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kd */
public class C2205kd<K, V> extends com.fossil.blesdk.obfuscated.C2723qd<V> implements com.fossil.blesdk.obfuscated.C2871sd.C2872a {

    @DexIgnore
    /* renamed from: s */
    public /* final */ com.fossil.blesdk.obfuscated.C2120jd<K, V> f6776s;

    @DexIgnore
    /* renamed from: t */
    public int f6777t; // = 0;

    @DexIgnore
    /* renamed from: u */
    public int f6778u; // = 0;

    @DexIgnore
    /* renamed from: v */
    public int f6779v; // = 0;

    @DexIgnore
    /* renamed from: w */
    public int f6780w; // = 0;

    @DexIgnore
    /* renamed from: x */
    public boolean f6781x; // = false;

    @DexIgnore
    /* renamed from: y */
    public /* final */ boolean f6782y;

    @DexIgnore
    /* renamed from: z */
    public com.fossil.blesdk.obfuscated.C2644pd.C2645a<V> f6783z; // = new com.fossil.blesdk.obfuscated.C2205kd.C2206a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kd$a */
    public class C2206a extends com.fossil.blesdk.obfuscated.C2644pd.C2645a<V> {
        @DexIgnore
        public C2206a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12730a(int i, com.fossil.blesdk.obfuscated.C2644pd<V> pdVar) {
            if (pdVar.mo14665a()) {
                com.fossil.blesdk.obfuscated.C2205kd.this.mo15185c();
            } else if (!com.fossil.blesdk.obfuscated.C2205kd.this.mo15192h()) {
                java.util.List<T> list = pdVar.f8343a;
                boolean z = true;
                if (i == 0) {
                    com.fossil.blesdk.obfuscated.C2205kd kdVar = com.fossil.blesdk.obfuscated.C2205kd.this;
                    kdVar.f8608i.mo15915a(pdVar.f8344b, list, pdVar.f8345c, pdVar.f8346d, kdVar);
                    com.fossil.blesdk.obfuscated.C2205kd kdVar2 = com.fossil.blesdk.obfuscated.C2205kd.this;
                    if (kdVar2.f8609j == -1) {
                        kdVar2.f8609j = pdVar.f8344b + pdVar.f8346d + (list.size() / 2);
                    }
                } else {
                    com.fossil.blesdk.obfuscated.C2205kd kdVar3 = com.fossil.blesdk.obfuscated.C2205kd.this;
                    boolean z2 = kdVar3.f8609j > kdVar3.f8608i.mo15932f();
                    com.fossil.blesdk.obfuscated.C2205kd kdVar4 = com.fossil.blesdk.obfuscated.C2205kd.this;
                    boolean z3 = kdVar4.f6782y && kdVar4.f8608i.mo15925b(kdVar4.f8607h.f8634d, kdVar4.f8611l, list.size());
                    if (i == 1) {
                        if (!z3 || z2) {
                            com.fossil.blesdk.obfuscated.C2205kd kdVar5 = com.fossil.blesdk.obfuscated.C2205kd.this;
                            kdVar5.f8608i.mo15917a(list, (com.fossil.blesdk.obfuscated.C2871sd.C2872a) kdVar5);
                        } else {
                            com.fossil.blesdk.obfuscated.C2205kd kdVar6 = com.fossil.blesdk.obfuscated.C2205kd.this;
                            kdVar6.f6780w = 0;
                            kdVar6.f6778u = 0;
                        }
                    } else if (i != 2) {
                        throw new java.lang.IllegalArgumentException("unexpected resultType " + i);
                    } else if (!z3 || !z2) {
                        com.fossil.blesdk.obfuscated.C2205kd kdVar7 = com.fossil.blesdk.obfuscated.C2205kd.this;
                        kdVar7.f8608i.mo15923b(list, (com.fossil.blesdk.obfuscated.C2871sd.C2872a) kdVar7);
                    } else {
                        com.fossil.blesdk.obfuscated.C2205kd kdVar8 = com.fossil.blesdk.obfuscated.C2205kd.this;
                        kdVar8.f6779v = 0;
                        kdVar8.f6777t = 0;
                    }
                    com.fossil.blesdk.obfuscated.C2205kd kdVar9 = com.fossil.blesdk.obfuscated.C2205kd.this;
                    if (kdVar9.f6782y) {
                        if (z2) {
                            if (kdVar9.f6777t != 1 && kdVar9.f8608i.mo15926b(kdVar9.f6781x, kdVar9.f8607h.f8634d, kdVar9.f8611l, kdVar9)) {
                                com.fossil.blesdk.obfuscated.C2205kd.this.f6777t = 0;
                            }
                        } else if (kdVar9.f6778u != 1 && kdVar9.f8608i.mo15920a(kdVar9.f6781x, kdVar9.f8607h.f8634d, kdVar9.f8611l, (com.fossil.blesdk.obfuscated.C2871sd.C2872a) kdVar9)) {
                            com.fossil.blesdk.obfuscated.C2205kd.this.f6778u = 0;
                        }
                    }
                }
                com.fossil.blesdk.obfuscated.C2205kd kdVar10 = com.fossil.blesdk.obfuscated.C2205kd.this;
                if (kdVar10.f8606g != null) {
                    boolean z4 = kdVar10.f8608i.size() == 0;
                    boolean z5 = !z4 && i == 2 && pdVar.f8343a.size() == 0;
                    if (!(!z4 && i == 1 && pdVar.f8343a.size() == 0)) {
                        z = false;
                    }
                    com.fossil.blesdk.obfuscated.C2205kd.this.mo15184a(z4, z5, z);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.kd$b */
    public class C2207b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ int f6785e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f6786f;

        @DexIgnore
        public C2207b(int i, java.lang.Object obj) {
            this.f6785e = i;
            this.f6786f = obj;
        }

        @DexIgnore
        public void run() {
            if (!com.fossil.blesdk.obfuscated.C2205kd.this.mo15192h()) {
                if (com.fossil.blesdk.obfuscated.C2205kd.this.f6776s.isInvalid()) {
                    com.fossil.blesdk.obfuscated.C2205kd.this.mo15185c();
                    return;
                }
                com.fossil.blesdk.obfuscated.C2205kd kdVar = com.fossil.blesdk.obfuscated.C2205kd.this;
                kdVar.f6776s.dispatchLoadBefore(this.f6785e, this.f6786f, kdVar.f8607h.f8631a, kdVar.f8604e, kdVar.f6783z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kd$c")
    /* renamed from: com.fossil.blesdk.obfuscated.kd$c */
    public class C2208c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ int f6788e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f6789f;

        @DexIgnore
        public C2208c(int i, java.lang.Object obj) {
            this.f6788e = i;
            this.f6789f = obj;
        }

        @DexIgnore
        public void run() {
            if (!com.fossil.blesdk.obfuscated.C2205kd.this.mo15192h()) {
                if (com.fossil.blesdk.obfuscated.C2205kd.this.f6776s.isInvalid()) {
                    com.fossil.blesdk.obfuscated.C2205kd.this.mo15185c();
                    return;
                }
                com.fossil.blesdk.obfuscated.C2205kd kdVar = com.fossil.blesdk.obfuscated.C2205kd.this;
                kdVar.f6776s.dispatchLoadAfter(this.f6788e, this.f6789f, kdVar.f8607h.f8631a, kdVar.f8604e, kdVar.f6783z);
            }
        }
    }

    @DexIgnore
    public C2205kd(com.fossil.blesdk.obfuscated.C2120jd<K, V> jdVar, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, com.fossil.blesdk.obfuscated.C2723qd.C2726c<V> cVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar, K k, int i) {
        super(new com.fossil.blesdk.obfuscated.C2871sd(), executor, executor2, cVar, fVar);
        boolean z = false;
        this.f6776s = jdVar;
        this.f8609j = i;
        if (this.f6776s.isInvalid()) {
            mo15185c();
        } else {
            com.fossil.blesdk.obfuscated.C2120jd<K, V> jdVar2 = this.f6776s;
            com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar2 = this.f8607h;
            jdVar2.dispatchLoadInitial(k, fVar2.f8635e, fVar2.f8631a, fVar2.f8633c, this.f8604e, this.f6783z);
        }
        if (this.f6776s.supportsPageDropping() && this.f8607h.f8634d != Integer.MAX_VALUE) {
            z = true;
        }
        this.f6782y = z;
    }

    @DexIgnore
    /* renamed from: c */
    public static int m9571c(int i, int i2, int i3) {
        return ((i2 + i) + 1) - i3;
    }

    @DexIgnore
    /* renamed from: d */
    public static int m9572d(int i, int i2, int i3) {
        return i - (i2 - i3);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12718a(com.fossil.blesdk.obfuscated.C2723qd<V> qdVar, com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar) {
        com.fossil.blesdk.obfuscated.C2871sd<T> sdVar = qdVar.f8608i;
        int g = this.f8608i.mo15933g() - sdVar.mo15933g();
        int h = this.f8608i.mo15935h() - sdVar.mo15935h();
        int l = sdVar.mo15939l();
        int e = sdVar.mo15931e();
        if (sdVar.isEmpty() || g < 0 || h < 0 || this.f8608i.mo15939l() != java.lang.Math.max(l - g, 0) || this.f8608i.mo15931e() != java.lang.Math.max(e - h, 0) || this.f8608i.mo15938k() != sdVar.mo15938k() + g + h) {
            throw new java.lang.IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        if (g != 0) {
            int min = java.lang.Math.min(l, g);
            int i = g - min;
            int e2 = sdVar.mo15931e() + sdVar.mo15938k();
            if (min != 0) {
                eVar.mo11912a(e2, min);
            }
            if (i != 0) {
                eVar.mo11913b(e2 + min, i);
            }
        }
        if (h != 0) {
            int min2 = java.lang.Math.min(e, h);
            int i2 = h - min2;
            if (min2 != 0) {
                eVar.mo11912a(e, min2);
            }
            if (i2 != 0) {
                eVar.mo11913b(0, i2);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12719b() {
        this.f6777t = 2;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12722c(int i, int i2) {
        throw new java.lang.IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2307ld<?, V> mo12723d() {
        return this.f6776s;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.Object mo12724e() {
        return this.f6776s.getKey(this.f8609j, this.f8610k);
    }

    @DexIgnore
    /* renamed from: f */
    public void mo12725f(int i) {
        throw new java.lang.IllegalStateException("Tiled callback on ContiguousPagedList");
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo12726g() {
        return true;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo12727h(int i) {
        int d = m9572d(this.f8607h.f8632b, i, this.f8608i.mo15931e());
        int c = m9571c(this.f8607h.f8632b, i, this.f8608i.mo15931e() + this.f8608i.mo15938k());
        this.f6779v = java.lang.Math.max(d, this.f6779v);
        if (this.f6779v > 0) {
            mo12729l();
        }
        this.f6780w = java.lang.Math.max(c, this.f6780w);
        if (this.f6780w > 0) {
            mo12728k();
        }
    }

    @DexIgnore
    /* renamed from: k */
    public final void mo12728k() {
        if (this.f6778u == 0) {
            this.f6778u = 1;
            this.f8605f.execute(new com.fossil.blesdk.obfuscated.C2205kd.C2208c(((this.f8608i.mo15931e() + this.f8608i.mo15938k()) - 1) + this.f8608i.mo15937j(), this.f8608i.mo15929d()));
        }
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo12729l() {
        if (this.f6777t == 0) {
            this.f6777t = 1;
            this.f8605f.execute(new com.fossil.blesdk.obfuscated.C2205kd.C2207b(this.f8608i.mo15931e() + this.f8608i.mo15937j(), this.f8608i.mo15927c()));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12721b(int i, int i2, int i3) {
        this.f6780w = (this.f6780w - i2) - i3;
        this.f6778u = 0;
        if (this.f6780w > 0) {
            mo12728k();
        }
        mo15186d(i, i2);
        mo15187e(i + i2, i3);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12720b(int i, int i2) {
        mo15189f(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12715a(int i) {
        boolean z = false;
        mo15187e(0, i);
        if (this.f8608i.mo15931e() > 0 || this.f8608i.mo15939l() > 0) {
            z = true;
        }
        this.f6781x = z;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12717a(int i, int i2, int i3) {
        this.f6779v = (this.f6779v - i2) - i3;
        this.f6777t = 0;
        if (this.f6779v > 0) {
            mo12729l();
        }
        mo15186d(i, i2);
        mo15187e(0, i3);
        mo15193i(i3);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12714a() {
        this.f6778u = 2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12716a(int i, int i2) {
        mo15186d(i, i2);
    }
}
