package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qg4 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ xc4<Throwable, qa4> b;

    @DexIgnore
    public String toString() {
        return "CompletedWithCancellation[" + this.a + ']';
    }
}
