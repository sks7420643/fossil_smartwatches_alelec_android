package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bv1<F, T> implements Iterator<T> {
    @DexIgnore
    public /* final */ Iterator<? extends F> e;

    @DexIgnore
    public bv1(Iterator<? extends F> it) {
        st1.a(it);
        this.e = it;
    }

    @DexIgnore
    public abstract T a(F f);

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final T next() {
        return a(this.e.next());
    }

    @DexIgnore
    public final void remove() {
        this.e.remove();
    }
}
