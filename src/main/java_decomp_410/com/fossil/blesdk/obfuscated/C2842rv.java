package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rv */
public class C2842rv extends com.fossil.blesdk.obfuscated.C2354lv<com.fossil.blesdk.obfuscated.C2842rv> {

    @DexIgnore
    /* renamed from: E */
    public static com.fossil.blesdk.obfuscated.C2842rv f9140E;

    @DexIgnore
    /* renamed from: F */
    public static com.fossil.blesdk.obfuscated.C2842rv f9141F;

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2842rv m13423b(com.fossil.blesdk.obfuscated.C2663pp ppVar) {
        return (com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13441a(ppVar);
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C2842rv m13425c(boolean z) {
        if (z) {
            if (f9140E == null) {
                f9140E = (com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13444a(true)).mo13429a();
            }
            return f9140E;
        }
        if (f9141F == null) {
            f9141F = (com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13444a(false)).mo13429a();
        }
        return f9141F;
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2842rv m13422b(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        return (com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13436a(joVar);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2842rv m13424b(java.lang.Class<?> cls) {
        return (com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13442a(cls);
    }
}
