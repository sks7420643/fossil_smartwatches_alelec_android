package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.tencent.wxop.stat.StatReportStrategy;
import java.net.URI;
import java.util.Iterator;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h04 {
    @DexIgnore
    public static boolean A; // = true;
    @DexIgnore
    public static volatile String B; // = "pingma.qq.com:80";
    @DexIgnore
    public static volatile String C; // = "http://pingma.qq.com:80/mstat/report";
    @DexIgnore
    public static int D; // = 20;
    @DexIgnore
    public static int E; // = 0;
    @DexIgnore
    public static boolean F; // = false;
    @DexIgnore
    public static String G; // = null;
    @DexIgnore
    public static o24 H; // = null;
    @DexIgnore
    public static boolean I; // = true;
    @DexIgnore
    public static int J; // = 0;
    @DexIgnore
    public static long K; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int L; // = RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN;
    @DexIgnore
    public static t14 a; // = e24.b();
    @DexIgnore
    public static n24 b; // = new n24(2);
    @DexIgnore
    public static n24 c; // = new n24(1);
    @DexIgnore
    public static StatReportStrategy d; // = StatReportStrategy.APP_LAUNCH;
    @DexIgnore
    public static boolean e; // = false;
    @DexIgnore
    public static boolean f; // = true;
    @DexIgnore
    public static int g; // = 30000;
    @DexIgnore
    public static int h; // = 100000;
    @DexIgnore
    public static int i; // = 30;
    @DexIgnore
    public static int j; // = 10;
    @DexIgnore
    public static int k; // = 100;
    @DexIgnore
    public static int l; // = 30;
    @DexIgnore
    public static int m; // = 1;
    @DexIgnore
    public static String n; // = "__HIBERNATE__";
    @DexIgnore
    public static String o; // = "__HIBERNATE__TIME";
    @DexIgnore
    public static String p; // = "__MTA_KILL__";
    @DexIgnore
    public static String q;
    @DexIgnore
    public static String r;
    @DexIgnore
    public static String s; // = "mta_channel";
    @DexIgnore
    public static String t; // = "";
    @DexIgnore
    public static int u; // = BackgroundImageConfig.BOTTOM_BACKGROUND_ANGLE;
    @DexIgnore
    public static boolean v; // = false;
    @DexIgnore
    public static int w; // = 100;
    @DexIgnore
    public static long x; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public static int y; // = 1024;
    @DexIgnore
    public static boolean z; // = true;

    @DexIgnore
    public static int a() {
        return i;
    }

    @DexIgnore
    public static String a(Context context) {
        return j24.a(i24.a(context, "_mta_ky_tag_", (String) null));
    }

    @DexIgnore
    public static String a(String str, String str2) {
        try {
            String string = c.b.getString(str);
            return string != null ? string : str2;
        } catch (Throwable unused) {
            t14 t14 = a;
            t14.g("can't find custom key:" + str);
        }
    }

    @DexIgnore
    public static synchronized void a(int i2) {
        synchronized (h04.class) {
        }
    }

    @DexIgnore
    public static void a(long j2) {
        i24.b(q24.a(), n, j2);
        b(false);
        a.h("MTA is disable for current SDK version");
    }

    @DexIgnore
    public static void a(Context context, n24 n24) {
        int i2 = n24.a;
        if (i2 == c.a) {
            c = n24;
            a(n24.b);
            if (!c.b.isNull("iplist")) {
                t04.a(context).a(c.b.getString("iplist"));
            }
        } else if (i2 == b.a) {
            b = n24;
        }
    }

    @DexIgnore
    public static void a(Context context, n24 n24, JSONObject jSONObject) {
        boolean z2 = false;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    int i2 = jSONObject.getInt(next);
                    if (n24.d != i2) {
                        z2 = true;
                    }
                    n24.d = i2;
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        n24.b = new JSONObject(string);
                    }
                } else if (next.equalsIgnoreCase("m")) {
                    n24.c = jSONObject.getString("m");
                }
            }
            if (z2) {
                g14 b2 = g14.b(q24.a());
                if (b2 != null) {
                    b2.a(n24);
                }
                if (n24.a == c.a) {
                    a(n24.b);
                    b(n24.b);
                }
            }
            a(context, n24);
        } catch (JSONException e2) {
            a.a((Throwable) e2);
        }
    }

    @DexIgnore
    public static void a(Context context, String str) {
        if (str != null) {
            i24.b(context, "_mta_ky_tag_", j24.b(str));
        }
    }

    @DexIgnore
    public static void a(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2;
        n24 n24;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(c.a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    n24 = c;
                } else if (next.equalsIgnoreCase(Integer.toString(b.a))) {
                    jSONObject2 = jSONObject.getJSONObject(next);
                    n24 = b;
                } else if (next.equalsIgnoreCase("rs")) {
                    StatReportStrategy statReportStrategy = StatReportStrategy.getStatReportStrategy(jSONObject.getInt(next));
                    if (statReportStrategy != null) {
                        d = statReportStrategy;
                        if (q()) {
                            t14 t14 = a;
                            t14.a((Object) "Change to ReportStrategy:" + statReportStrategy.name());
                        }
                    }
                } else {
                    return;
                }
                a(context, n24, jSONObject2);
            }
        } catch (JSONException e2) {
            a.a((Throwable) e2);
        }
    }

    @DexIgnore
    public static void a(StatReportStrategy statReportStrategy) {
        d = statReportStrategy;
        if (statReportStrategy != StatReportStrategy.PERIOD) {
            j04.s = 0;
        }
        if (q()) {
            t14 t14 = a;
            t14.a((Object) "Change to statSendStrategy: " + statReportStrategy);
        }
    }

    @DexIgnore
    public static void a(JSONObject jSONObject) {
        try {
            StatReportStrategy statReportStrategy = StatReportStrategy.getStatReportStrategy(jSONObject.getInt("rs"));
            if (statReportStrategy != null) {
                a(statReportStrategy);
            }
        } catch (JSONException unused) {
            if (q()) {
                a.e("rs not found.");
            }
        }
    }

    @DexIgnore
    public static void a(boolean z2) {
        z = z2;
    }

    @DexIgnore
    public static boolean a(int i2, int i3, int i4) {
        return i2 >= i3 && i2 <= i4;
    }

    @DexIgnore
    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        String str2 = q;
        if (str2 == null) {
            q = str;
            return true;
        } else if (str2.contains(str)) {
            return false;
        } else {
            q += "|" + str;
            return true;
        }
    }

    @DexIgnore
    public static boolean a(JSONObject jSONObject, String str, String str2) {
        if (jSONObject.isNull(str)) {
            return false;
        }
        String optString = jSONObject.optString(str);
        return e24.c(str2) && e24.c(optString) && str2.equalsIgnoreCase(optString);
    }

    @DexIgnore
    public static synchronized String b(Context context) {
        synchronized (h04.class) {
            if (q != null) {
                String str = q;
                return str;
            }
            if (context != null) {
                if (q == null) {
                    q = e24.i(context);
                }
            }
            if (q == null || q.trim().length() == 0) {
                a.d("AppKey can not be null or empty, please read Developer's Guide first!");
            }
            String str2 = q;
            return str2;
        }
    }

    @DexIgnore
    public static void b() {
        E++;
    }

    @DexIgnore
    public static void b(int i2) {
        if (i2 >= 0) {
            E = i2;
        }
    }

    @DexIgnore
    public static void b(Context context, String str) {
        t14 t14;
        String str2;
        if (context == null) {
            t14 = a;
            str2 = "ctx in StatConfig.setAppKey() is null";
        } else if (str == null || str.length() > 256) {
            t14 = a;
            str2 = "appkey in StatConfig.setAppKey() is null or exceed 256 bytes";
        } else {
            if (q == null) {
                q = a(context);
            }
            if (a(str) || a(e24.i(context))) {
                a(context, q);
                return;
            }
            return;
        }
        t14.d(str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0040 A[Catch:{ Exception -> 0x01b1 }] */
    public static void b(Context context, JSONObject jSONObject) {
        int i2;
        Integer valueOf;
        try {
            String optString = jSONObject.optString(p);
            if (e24.c(optString)) {
                JSONObject jSONObject2 = new JSONObject(optString);
                if (jSONObject2.length() != 0) {
                    boolean z2 = false;
                    if (!jSONObject2.isNull("sm")) {
                        Object obj = jSONObject2.get("sm");
                        if (obj instanceof Integer) {
                            valueOf = (Integer) obj;
                        } else if (obj instanceof String) {
                            valueOf = Integer.valueOf((String) obj);
                        } else {
                            i2 = 0;
                            if (i2 > 0) {
                                if (q()) {
                                    t14 t14 = a;
                                    t14.e("match sleepTime:" + i2 + " minutes");
                                }
                                i24.b(context, o, System.currentTimeMillis() + ((long) (i2 * 60 * 1000)));
                                b(false);
                                a.h("MTA is disable for current SDK version");
                            }
                        }
                        i2 = valueOf.intValue();
                        if (i2 > 0) {
                        }
                    }
                    boolean z3 = true;
                    if (a(jSONObject2, "sv", "2.0.3")) {
                        a.e("match sdk version:2.0.3");
                        z2 = true;
                    }
                    if (a(jSONObject2, "md", Build.MODEL)) {
                        t14 t142 = a;
                        t142.e("match MODEL:" + Build.MODEL);
                        z2 = true;
                    }
                    if (a(jSONObject2, "av", e24.m(context))) {
                        t14 t143 = a;
                        t143.e("match app version:" + e24.m(context));
                        z2 = true;
                    }
                    if (a(jSONObject2, "mf", Build.MANUFACTURER)) {
                        t14 t144 = a;
                        t144.e("match MANUFACTURER:" + Build.MANUFACTURER);
                        z2 = true;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(Build.VERSION.SDK_INT);
                    if (a(jSONObject2, "osv", sb.toString())) {
                        t14 t145 = a;
                        t145.e("match android SDK version:" + Build.VERSION.SDK_INT);
                        z2 = true;
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(Build.VERSION.SDK_INT);
                    if (a(jSONObject2, "ov", sb2.toString())) {
                        t14 t146 = a;
                        t146.e("match android SDK version:" + Build.VERSION.SDK_INT);
                        z2 = true;
                    }
                    if (a(jSONObject2, "ui", g14.b(context).a(context).b())) {
                        t14 t147 = a;
                        t147.e("match imei:" + g14.b(context).a(context).b());
                        z2 = true;
                    }
                    if (a(jSONObject2, "mid", e(context))) {
                        t14 t148 = a;
                        t148.e("match mid:" + e(context));
                    } else {
                        z3 = z2;
                    }
                    if (z3) {
                        a(e24.b("2.0.3"));
                    }
                }
            }
        } catch (Exception e2) {
            a.a((Throwable) e2);
        }
    }

    @DexIgnore
    public static void b(String str) {
        if (str.length() > 128) {
            a.d("the length of installChannel can not exceed the range of 128 bytes.");
        } else {
            r = str;
        }
    }

    @DexIgnore
    public static void b(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.length() != 0) {
            try {
                b(q24.a(), jSONObject);
                String string = jSONObject.getString(n);
                if (q()) {
                    t14 t14 = a;
                    t14.a((Object) "hibernateVer:" + string + ", current version:2.0.3");
                }
                long b2 = e24.b(string);
                if (e24.b("2.0.3") <= b2) {
                    a(b2);
                }
            } catch (JSONException unused) {
                a.a((Object) "__HIBERNATE__ not found.");
            }
        }
    }

    @DexIgnore
    public static void b(boolean z2) {
        f = z2;
        if (!z2) {
            a.h("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    @DexIgnore
    public static int c() {
        return E;
    }

    @DexIgnore
    public static String c(Context context) {
        if (context == null) {
            a.d("Context for getCustomUid is null.");
            return null;
        }
        if (G == null) {
            G = i24.a(context, "MTA_CUSTOM_UID", "");
        }
        return G;
    }

    @DexIgnore
    public static void c(int i2) {
        if (!a(i2, 1, (int) DateTimeConstants.MINUTES_PER_WEEK)) {
            a.d("setSendPeriodMinutes can not exceed the range of [1, 7*24*60] minutes.");
        } else {
            u = i2;
        }
    }

    @DexIgnore
    public static void c(Context context, String str) {
        if (str.length() > 128) {
            a.d("the length of installChannel can not exceed the range of 128 bytes.");
            return;
        }
        r = str;
        i24.b(context, s, str);
    }

    @DexIgnore
    public static void c(String str) {
        if (str == null || str.length() == 0) {
            a.d("statReportUrl cannot be null or empty.");
            return;
        }
        C = str;
        try {
            B = new URI(C).getHost();
        } catch (Exception e2) {
            a.g(e2);
        }
        if (q()) {
            t14 t14 = a;
            t14.e("url:" + C + ", domain:" + B);
        }
    }

    @DexIgnore
    public static o24 d() {
        return H;
    }

    @DexIgnore
    public static synchronized String d(Context context) {
        synchronized (h04.class) {
            if (r != null) {
                String str = r;
                return str;
            }
            String a2 = i24.a(context, s, "");
            r = a2;
            if (a2 == null || r.trim().length() == 0) {
                r = e24.j(context);
            }
            if (r == null || r.trim().length() == 0) {
                a.g("installChannel can not be null or empty, please read Developer's Guide first!");
            }
            String str2 = r;
            return str2;
        }
    }

    @DexIgnore
    public static int e() {
        return l;
    }

    @DexIgnore
    public static String e(Context context) {
        return context != null ? vy3.a(context).a().a() : "0";
    }

    @DexIgnore
    public static int f() {
        return D;
    }

    @DexIgnore
    public static String f(Context context) {
        return i24.a(context, "mta.acc.qq", t);
    }

    @DexIgnore
    public static int g() {
        return k;
    }

    @DexIgnore
    public static int h() {
        return y;
    }

    @DexIgnore
    public static int i() {
        return j;
    }

    @DexIgnore
    public static int j() {
        return h;
    }

    @DexIgnore
    public static int k() {
        return m;
    }

    @DexIgnore
    public static int l() {
        return u;
    }

    @DexIgnore
    public static int m() {
        return g;
    }

    @DexIgnore
    public static String n() {
        return C;
    }

    @DexIgnore
    public static StatReportStrategy o() {
        return d;
    }

    @DexIgnore
    public static boolean p() {
        return A;
    }

    @DexIgnore
    public static boolean q() {
        return e;
    }

    @DexIgnore
    public static boolean r() {
        return F;
    }

    @DexIgnore
    public static boolean s() {
        return f;
    }
}
