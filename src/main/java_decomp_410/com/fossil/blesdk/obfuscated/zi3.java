package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zi3 extends zr2 implements yi3 {
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public tr3<me2> j;
    @DexIgnore
    public xi3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final zi3 a() {
            return new zi3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zi3 e;

        @DexIgnore
        public b(zi3 zi3) {
            this.e = zi3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zi3 e;
        @DexIgnore
        public /* final */ /* synthetic */ me2 f;

        @DexIgnore
        public c(zi3 zi3, me2 me2) {
            this.e = zi3;
            this.f = me2;
        }

        @DexIgnore
        public final void onClick(View view) {
            zi3 zi3 = this.e;
            SwitchCompat switchCompat = this.f.q;
            kd4.a((Object) switchCompat, "binding.anonymousSwitch");
            zi3.c("Usage_Data", switchCompat.isChecked());
            AnalyticsHelper a = this.e.P0();
            SwitchCompat switchCompat2 = this.f.q;
            kd4.a((Object) switchCompat2, "binding.anonymousSwitch");
            a.c(switchCompat2.isChecked());
            xi3 b = zi3.b(this.e);
            SwitchCompat switchCompat3 = this.f.q;
            kd4.a((Object) switchCompat3, "binding.anonymousSwitch");
            b.a(switchCompat3.isChecked());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zi3 e;
        @DexIgnore
        public /* final */ /* synthetic */ me2 f;

        @DexIgnore
        public d(zi3 zi3, me2 me2) {
            this.e = zi3;
            this.f = me2;
        }

        @DexIgnore
        public final void onClick(View view) {
            zi3 zi3 = this.e;
            SwitchCompat switchCompat = this.f.t;
            kd4.a((Object) switchCompat, "binding.scSubcribeEmail");
            zi3.c("Emails", switchCompat.isChecked());
            AnalyticsHelper a = this.e.P0();
            SwitchCompat switchCompat2 = this.f.t;
            kd4.a((Object) switchCompat2, "binding.scSubcribeEmail");
            a.b(switchCompat2.isChecked());
            xi3 b = zi3.b(this.e);
            SwitchCompat switchCompat3 = this.f.t;
            kd4.a((Object) switchCompat3, "binding.scSubcribeEmail");
            b.b(switchCompat3.isChecked());
        }
    }

    /*
    static {
        kd4.a((Object) zi3.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ xi3 b(zi3 zi3) {
        xi3 xi3 = zi3.k;
        if (xi3 != null) {
            return xi3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void E(boolean z) {
        tr3<me2> tr3 = this.j;
        if (tr3 != null) {
            me2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.q;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void E0() {
        a();
    }

    @DexIgnore
    public void H(boolean z) {
        tr3<me2> tr3 = this.j;
        if (tr3 != null) {
            me2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.t;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void Z() {
        String string = getString(R.string.Onboarding_Login_LoggingIn_Text__PleaseWait);
        kd4.a((Object) string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public final void c(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        a("profile_optin", (Map<String, String>) hashMap);
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        me2 me2 = (me2) qa.a(LayoutInflater.from(getContext()), (int) R.layout.fragment_opt_in, (ViewGroup) null, false);
        me2.s.setOnClickListener(new b(this));
        FlexibleTextView flexibleTextView = me2.r;
        kd4.a((Object) flexibleTextView, "binding.ftvDescriptionSubcribeEmail");
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Select_Optin_List__GetTipsAboutBrandsTools);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        Object[] objArr = {PortfolioApp.W.c().i()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        me2.q.setOnClickListener(new c(this, me2));
        me2.t.setOnClickListener(new d(this, me2));
        this.j = new tr3<>(this, me2);
        kd4.a((Object) me2, "binding");
        return me2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        xi3 xi3 = this.k;
        if (xi3 != null) {
            xi3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        xi3 xi3 = this.k;
        if (xi3 != null) {
            xi3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, String str) {
        kd4.b(str, "message");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(xi3 xi3) {
        kd4.b(xi3, "presenter");
        st1.a(xi3);
        kd4.a((Object) xi3, "checkNotNull(presenter)");
        this.k = xi3;
    }
}
