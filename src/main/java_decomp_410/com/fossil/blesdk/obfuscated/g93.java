package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g93 extends zr2 implements f93 {
    @DexIgnore
    public tr3<e92> j;
    @DexIgnore
    public e93 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.D;
            Date date = new Date();
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void b(wr2 wr2, ArrayList<String> arrayList) {
        kd4.b(wr2, "baseModel");
        kd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewDayFragment", "showDayDetails - baseModel=" + wr2);
        tr3<e92> tr3 = this.j;
        if (tr3 != null) {
            e92 a2 = tr3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.q;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ll2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onCreateView");
        e92 e92 = (e92) qa.a(layoutInflater, R.layout.fragment_activity_overview_day, viewGroup, false, O0());
        e92.r.setOnClickListener(b.e);
        this.j = new tr3<>(this, e92);
        tr3<e92> tr3 = this.j;
        if (tr3 != null) {
            e92 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onResume");
        e93 e93 = this.k;
        if (e93 != null) {
            e93.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onStop");
        e93 e93 = this.k;
        if (e93 != null) {
            e93.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        kd4.b(list, "workoutSessions");
        tr3<e92> tr3 = this.j;
        if (tr3 != null) {
            e92 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.u;
                kd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    yg2 yg2 = a2.t;
                    if (yg2 != null) {
                        View d = yg2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                yg2 yg22 = a2.t;
                if (yg22 != null) {
                    View d2 = yg22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            kd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(e93 e93) {
        kd4.b(e93, "presenter");
        this.k = e93;
    }

    @DexIgnore
    public final void a(yg2 yg2, WorkoutSession workoutSession) {
        if (yg2 != null) {
            View d = yg2.d();
            kd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = sm2.a(context, a2.getSecond().intValue());
            yg2.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = yg2.r;
            kd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = yg2.s;
            kd4.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            pd4 pd4 = pd4.a;
            String a4 = sm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Text__NumberSteps);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
            Object[] objArr = new Object[1];
            Integer totalSteps = workoutSession.getTotalSteps();
            objArr[0] = il2.b(totalSteps != null ? (float) totalSteps.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1);
            String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = yg2.q;
            kd4.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(rk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
