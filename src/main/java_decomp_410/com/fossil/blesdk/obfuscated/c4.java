package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface c4 {
    @DexIgnore
    void a(int i, int i2);

    @DexIgnore
    void a(int i, int i2, int i3, int i4);

    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    boolean a();

    @DexIgnore
    boolean b();

    @DexIgnore
    Drawable c();

    @DexIgnore
    View d();
}
