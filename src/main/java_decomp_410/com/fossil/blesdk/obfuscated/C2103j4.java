package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j4 */
public class C2103j4<E> implements java.lang.Cloneable {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.Object f6275i; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: e */
    public boolean f6276e;

    @DexIgnore
    /* renamed from: f */
    public long[] f6277f;

    @DexIgnore
    /* renamed from: g */
    public java.lang.Object[] f6278g;

    @DexIgnore
    /* renamed from: h */
    public int f6279h;

    @DexIgnore
    public C2103j4() {
        this(10);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12214a(long j) {
        int a = com.fossil.blesdk.obfuscated.C1997i4.m8224a(this.f6277f, this.f6279h, j);
        if (a >= 0) {
            java.lang.Object[] objArr = this.f6278g;
            java.lang.Object obj = objArr[a];
            java.lang.Object obj2 = f6275i;
            if (obj != obj2) {
                objArr[a] = obj2;
                this.f6276e = true;
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public E mo12216b(long j) {
        return mo12217b(j, (java.lang.Object) null);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12223c(long j, E e) {
        int a = com.fossil.blesdk.obfuscated.C1997i4.m8224a(this.f6277f, this.f6279h, j);
        if (a >= 0) {
            this.f6278g[a] = e;
            return;
        }
        int i = ~a;
        if (i < this.f6279h) {
            java.lang.Object[] objArr = this.f6278g;
            if (objArr[i] == f6275i) {
                this.f6277f[i] = j;
                objArr[i] = e;
                return;
            }
        }
        if (this.f6276e && this.f6279h >= this.f6277f.length) {
            mo12218b();
            i = ~com.fossil.blesdk.obfuscated.C1997i4.m8224a(this.f6277f, this.f6279h, j);
        }
        int i2 = this.f6279h;
        if (i2 >= this.f6277f.length) {
            int c = com.fossil.blesdk.obfuscated.C1997i4.m8227c(i2 + 1);
            long[] jArr = new long[c];
            java.lang.Object[] objArr2 = new java.lang.Object[c];
            long[] jArr2 = this.f6277f;
            java.lang.System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            java.lang.Object[] objArr3 = this.f6278g;
            java.lang.System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f6277f = jArr;
            this.f6278g = objArr2;
        }
        int i3 = this.f6279h;
        if (i3 - i != 0) {
            long[] jArr3 = this.f6277f;
            int i4 = i + 1;
            java.lang.System.arraycopy(jArr3, i, jArr3, i4, i3 - i);
            java.lang.Object[] objArr4 = this.f6278g;
            java.lang.System.arraycopy(objArr4, i, objArr4, i4, this.f6279h - i);
        }
        this.f6277f[i] = j;
        this.f6278g[i] = e;
        this.f6279h++;
    }

    @DexIgnore
    public java.lang.String toString() {
        if (mo12220c() <= 0) {
            return "{}";
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder(this.f6279h * 28);
        sb.append('{');
        for (int i = 0; i < this.f6279h; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(mo12212a(i));
            sb.append('=');
            java.lang.Object c = mo12222c(i);
            if (c != this) {
                sb.append(c);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public C2103j4(int i) {
        this.f6276e = false;
        if (i == 0) {
            this.f6277f = com.fossil.blesdk.obfuscated.C1997i4.f5934b;
            this.f6278g = com.fossil.blesdk.obfuscated.C1997i4.f5935c;
        } else {
            int c = com.fossil.blesdk.obfuscated.C1997i4.m8227c(i);
            this.f6277f = new long[c];
            this.f6278g = new java.lang.Object[c];
        }
        this.f6279h = 0;
    }

    @DexIgnore
    /* renamed from: b */
    public E mo12217b(long j, E e) {
        int a = com.fossil.blesdk.obfuscated.C1997i4.m8224a(this.f6277f, this.f6279h, j);
        if (a >= 0) {
            E[] eArr = this.f6278g;
            if (eArr[a] != f6275i) {
                return eArr[a];
            }
        }
        return e;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2103j4<E> clone() {
        try {
            com.fossil.blesdk.obfuscated.C2103j4<E> j4Var = (com.fossil.blesdk.obfuscated.C2103j4) super.clone();
            j4Var.f6277f = (long[]) this.f6277f.clone();
            j4Var.f6278g = (java.lang.Object[]) this.f6278g.clone();
            return j4Var;
        } catch (java.lang.CloneNotSupportedException e) {
            throw new java.lang.AssertionError(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public long mo12212a(int i) {
        if (this.f6276e) {
            mo12218b();
        }
        return this.f6277f[i];
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12219b(int i) {
        java.lang.Object[] objArr = this.f6278g;
        java.lang.Object obj = objArr[i];
        java.lang.Object obj2 = f6275i;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.f6276e = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12213a() {
        int i = this.f6279h;
        java.lang.Object[] objArr = this.f6278g;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.f6279h = 0;
        this.f6276e = false;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12218b() {
        int i = this.f6279h;
        long[] jArr = this.f6277f;
        java.lang.Object[] objArr = this.f6278g;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            java.lang.Object obj = objArr[i3];
            if (obj != f6275i) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.f6276e = false;
        this.f6279h = i2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12215a(long j, E e) {
        int i = this.f6279h;
        if (i == 0 || j > this.f6277f[i - 1]) {
            if (this.f6276e && this.f6279h >= this.f6277f.length) {
                mo12218b();
            }
            int i2 = this.f6279h;
            if (i2 >= this.f6277f.length) {
                int c = com.fossil.blesdk.obfuscated.C1997i4.m8227c(i2 + 1);
                long[] jArr = new long[c];
                java.lang.Object[] objArr = new java.lang.Object[c];
                long[] jArr2 = this.f6277f;
                java.lang.System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                java.lang.Object[] objArr2 = this.f6278g;
                java.lang.System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.f6277f = jArr;
                this.f6278g = objArr;
            }
            this.f6277f[i2] = j;
            this.f6278g[i2] = e;
            this.f6279h = i2 + 1;
            return;
        }
        mo12223c(j, e);
    }

    @DexIgnore
    /* renamed from: c */
    public int mo12220c() {
        if (this.f6276e) {
            mo12218b();
        }
        return this.f6279h;
    }

    @DexIgnore
    /* renamed from: c */
    public E mo12222c(int i) {
        if (this.f6276e) {
            mo12218b();
        }
        return this.f6278g[i];
    }

    @DexIgnore
    /* renamed from: c */
    public int mo12221c(long j) {
        if (this.f6276e) {
            mo12218b();
        }
        return com.fossil.blesdk.obfuscated.C1997i4.m8224a(this.f6277f, this.f6279h, j);
    }
}
