package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ia */
public abstract class C2005ia extends com.fossil.blesdk.obfuscated.C2300l8 {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ android.graphics.Rect f5974m; // = new android.graphics.Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);

    @DexIgnore
    /* renamed from: n */
    public static /* final */ com.fossil.blesdk.obfuscated.C2114ja.C2115a<com.fossil.blesdk.obfuscated.C2711q9> f5975n; // = new com.fossil.blesdk.obfuscated.C2005ia.C2006a();

    @DexIgnore
    /* renamed from: o */
    public static /* final */ com.fossil.blesdk.obfuscated.C2114ja.C2116b<androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9>, com.fossil.blesdk.obfuscated.C2711q9> f5976o; // = new com.fossil.blesdk.obfuscated.C2005ia.C2007b();

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.graphics.Rect f5977c; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.graphics.Rect f5978d; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.Rect f5979e; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: f */
    public /* final */ int[] f5980f; // = new int[2];

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.view.accessibility.AccessibilityManager f5981g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.view.View f5982h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2005ia.C2008c f5983i;

    @DexIgnore
    /* renamed from: j */
    public int f5984j; // = Integer.MIN_VALUE;

    @DexIgnore
    /* renamed from: k */
    public int f5985k; // = Integer.MIN_VALUE;

    @DexIgnore
    /* renamed from: l */
    public int f5986l; // = Integer.MIN_VALUE;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ia$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ia$a */
    public static class C2006a implements com.fossil.blesdk.obfuscated.C2114ja.C2115a<com.fossil.blesdk.obfuscated.C2711q9> {
        @DexIgnore
        /* renamed from: a */
        public void mo11879a(com.fossil.blesdk.obfuscated.C2711q9 q9Var, android.graphics.Rect rect) {
            q9Var.mo15061a(rect);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ia$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ia$b */
    public static class C2007b implements com.fossil.blesdk.obfuscated.C2114ja.C2116b<androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9>, com.fossil.blesdk.obfuscated.C2711q9> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2711q9 mo11883a(androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9> sparseArrayCompat, int i) {
            return sparseArrayCompat.mo1251f(i);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo11881a(androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9> sparseArrayCompat) {
            return sparseArrayCompat.mo1245c();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ia$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ia$c */
    public class C2008c extends com.fossil.blesdk.obfuscated.C2791r9 {
        @DexIgnore
        public C2008c() {
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2711q9 mo11884a(int i) {
            return com.fossil.blesdk.obfuscated.C2711q9.m12654a(com.fossil.blesdk.obfuscated.C2005ia.this.mo11874e(i));
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2711q9 mo11886b(int i) {
            int i2 = i == 2 ? com.fossil.blesdk.obfuscated.C2005ia.this.f5984j : com.fossil.blesdk.obfuscated.C2005ia.this.f5985k;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return mo11884a(i2);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo11885a(int i, int i2, android.os.Bundle bundle) {
            return com.fossil.blesdk.obfuscated.C2005ia.this.mo11866b(i, i2, bundle);
        }
    }

    @DexIgnore
    public C2005ia(android.view.View view) {
        if (view != null) {
            this.f5982h = view;
            this.f5981g = (android.view.accessibility.AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (com.fossil.blesdk.obfuscated.C1776f9.m6843i(view) == 0) {
                com.fossil.blesdk.obfuscated.C1776f9.m6838f(view, 1);
                return;
            }
            return;
        }
        throw new java.lang.IllegalArgumentException("View may not be null");
    }

    @DexIgnore
    /* renamed from: i */
    public static int m8286i(int i) {
        if (i == 19) {
            return 33;
        }
        if (i != 21) {
            return i != 22 ? 130 : 66;
        }
        return 17;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int mo11846a(float f, float f2);

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2791r9 mo11848a(android.view.View view) {
        if (this.f5983i == null) {
            this.f5983i = new com.fossil.blesdk.obfuscated.C2005ia.C2008c();
        }
        return this.f5983i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11850a(int i, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11851a(int i, com.fossil.blesdk.obfuscated.C2711q9 q9Var);

    @DexIgnore
    /* renamed from: a */
    public void mo11852a(int i, boolean z) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11853a(android.view.accessibility.AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11854a(com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11855a(java.util.List<java.lang.Integer> list);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo11858a(int i, int i2, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: b */
    public final boolean mo11867b(int i, android.graphics.Rect rect) {
        com.fossil.blesdk.obfuscated.C2711q9 q9Var;
        com.fossil.blesdk.obfuscated.C2711q9 q9Var2;
        androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9> d = mo11872d();
        int i2 = this.f5985k;
        int i3 = Integer.MIN_VALUE;
        if (i2 == Integer.MIN_VALUE) {
            q9Var = null;
        } else {
            q9Var = d.mo1242b(i2);
        }
        com.fossil.blesdk.obfuscated.C2711q9 q9Var3 = q9Var;
        if (i == 1 || i == 2) {
            q9Var2 = (com.fossil.blesdk.obfuscated.C2711q9) com.fossil.blesdk.obfuscated.C2114ja.m8904a(d, f5976o, f5975n, q9Var3, i, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f5982h) == 1, false);
        } else if (i == 17 || i == 33 || i == 66 || i == 130) {
            android.graphics.Rect rect2 = new android.graphics.Rect();
            int i4 = this.f5985k;
            if (i4 != Integer.MIN_VALUE) {
                mo11849a(i4, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                m8285a(this.f5982h, i, rect2);
            }
            q9Var2 = (com.fossil.blesdk.obfuscated.C2711q9) com.fossil.blesdk.obfuscated.C2114ja.m8905a(d, f5976o, f5975n, q9Var3, rect2, i);
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        if (q9Var2 != null) {
            i3 = d.mo1249d(d.mo1238a(q9Var2));
        }
        return mo11876g(i3);
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo11870c(int i, int i2) {
        if (i == Integer.MIN_VALUE || !this.f5981g.isEnabled()) {
            return false;
        }
        android.view.ViewParent parent = this.f5982h.getParent();
        if (parent == null) {
            return false;
        }
        return com.fossil.blesdk.obfuscated.C2004i9.m8277a(parent, this.f5982h, mo11847a(i, i2));
    }

    @DexIgnore
    /* renamed from: d */
    public final androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9> mo11872d() {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        mo11855a((java.util.List<java.lang.Integer>) arrayList);
        androidx.collection.SparseArrayCompat<com.fossil.blesdk.obfuscated.C2711q9> sparseArrayCompat = new androidx.collection.SparseArrayCompat<>();
        for (int i = 0; i < arrayList.size(); i++) {
            sparseArrayCompat.mo1247c(i, mo11873d(i));
        }
        return sparseArrayCompat;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2711q9 mo11874e(int i) {
        if (i == -1) {
            return mo11869c();
        }
        return mo11873d(i);
    }

    @DexIgnore
    /* renamed from: f */
    public final boolean mo11875f(int i) {
        if (this.f5981g.isEnabled() && this.f5981g.isTouchExplorationEnabled()) {
            int i2 = this.f5984j;
            if (i2 != i) {
                if (i2 != Integer.MIN_VALUE) {
                    mo11857a(i2);
                }
                this.f5984j = i;
                this.f5982h.invalidate();
                mo11870c(i, 32768);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: g */
    public final boolean mo11876g(int i) {
        if (!this.f5982h.isFocused() && !this.f5982h.requestFocus()) {
            return false;
        }
        int i2 = this.f5985k;
        if (i2 == i) {
            return false;
        }
        if (i2 != Integer.MIN_VALUE) {
            mo11865b(i2);
        }
        this.f5985k = i;
        mo11852a(i, true);
        mo11870c(i, 8);
        return true;
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo11877h(int i) {
        int i2 = this.f5986l;
        if (i2 != i) {
            this.f5986l = i;
            mo11870c(i, 128);
            mo11870c(i2, 256);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11862a(android.view.MotionEvent motionEvent) {
        if (!this.f5981g.isEnabled() || !this.f5981g.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int a = mo11846a(motionEvent.getX(), motionEvent.getY());
            mo11877h(a);
            if (a != Integer.MIN_VALUE) {
                return true;
            }
            return false;
        } else if (action != 10 || this.f5986l == Integer.MIN_VALUE) {
            return false;
        } else {
            mo11877h(Integer.MIN_VALUE);
            return true;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final android.view.accessibility.AccessibilityEvent mo11868c(int i) {
        android.view.accessibility.AccessibilityEvent obtain = android.view.accessibility.AccessibilityEvent.obtain(i);
        this.f5982h.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    @DexIgnore
    /* renamed from: c */
    public final com.fossil.blesdk.obfuscated.C2711q9 mo11869c() {
        com.fossil.blesdk.obfuscated.C2711q9 d = com.fossil.blesdk.obfuscated.C2711q9.m12656d(this.f5982h);
        com.fossil.blesdk.obfuscated.C1776f9.m6815a(this.f5982h, d);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        mo11855a((java.util.List<java.lang.Integer>) arrayList);
        if (d.mo15068b() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                d.mo15063a(this.f5982h, ((java.lang.Integer) arrayList.get(i)).intValue());
            }
            return d;
        }
        throw new java.lang.RuntimeException("Views cannot have both real and virtual children");
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.C2711q9 mo11873d(int i) {
        com.fossil.blesdk.obfuscated.C2711q9 x = com.fossil.blesdk.obfuscated.C2711q9.m12657x();
        x.mo15097h(true);
        x.mo15100i(true);
        x.mo15065a((java.lang.CharSequence) "android.view.View");
        x.mo15078c(f5974m);
        x.mo15084d(f5974m);
        x.mo15071b(this.f5982h);
        mo11851a(i, x);
        if (x.mo15096h() == null && x.mo15083d() == null) {
            throw new java.lang.RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        x.mo15061a(this.f5978d);
        if (!this.f5978d.equals(f5974m)) {
            int a = x.mo15058a();
            if ((a & 64) != 0) {
                throw new java.lang.RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((a & 128) == 0) {
                x.mo15088e((java.lang.CharSequence) this.f5982h.getContext().getPackageName());
                x.mo15080c(this.f5982h, i);
                if (this.f5984j == i) {
                    x.mo15067a(true);
                    x.mo15059a(128);
                } else {
                    x.mo15067a(false);
                    x.mo15059a(64);
                }
                boolean z = this.f5985k == i;
                if (z) {
                    x.mo15059a(2);
                } else if (x.mo15112o()) {
                    x.mo15059a(1);
                }
                x.mo15101j(z);
                this.f5982h.getLocationOnScreen(this.f5980f);
                x.mo15070b(this.f5977c);
                if (this.f5977c.equals(f5974m)) {
                    x.mo15061a(this.f5977c);
                    if (x.f8573b != -1) {
                        com.fossil.blesdk.obfuscated.C2711q9 x2 = com.fossil.blesdk.obfuscated.C2711q9.m12657x();
                        for (int i2 = x.f8573b; i2 != -1; i2 = x2.f8573b) {
                            x2.mo15072b(this.f5982h, -1);
                            x2.mo15078c(f5974m);
                            mo11851a(i2, x2);
                            x2.mo15061a(this.f5978d);
                            android.graphics.Rect rect = this.f5977c;
                            android.graphics.Rect rect2 = this.f5978d;
                            rect.offset(rect2.left, rect2.top);
                        }
                        x2.mo15120v();
                    }
                    this.f5977c.offset(this.f5980f[0] - this.f5982h.getScrollX(), this.f5980f[1] - this.f5982h.getScrollY());
                }
                if (this.f5982h.getLocalVisibleRect(this.f5979e)) {
                    this.f5979e.offset(this.f5980f[0] - this.f5982h.getScrollX(), this.f5980f[1] - this.f5982h.getScrollY());
                    if (this.f5977c.intersect(this.f5979e)) {
                        x.mo15084d(this.f5977c);
                        if (mo11860a(this.f5977c)) {
                            x.mo15111o(true);
                        }
                    }
                }
                return x;
            } else {
                throw new java.lang.RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new java.lang.RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11861a(android.view.KeyEvent keyEvent) {
        int i = 0;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 61) {
            if (keyCode != 66) {
                switch (keyCode) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        if (!keyEvent.hasNoModifiers()) {
                            return false;
                        }
                        int i2 = m8286i(keyCode);
                        int repeatCount = keyEvent.getRepeatCount() + 1;
                        boolean z = false;
                        while (i < repeatCount && mo11867b(i2, (android.graphics.Rect) null)) {
                            i++;
                            z = true;
                        }
                        return z;
                    case 23:
                        break;
                    default:
                        return false;
                }
            }
            if (!keyEvent.hasNoModifiers() || keyEvent.getRepeatCount() != 0) {
                return false;
            }
            mo11864b();
            return true;
        } else if (keyEvent.hasNoModifiers()) {
            return mo11867b(2, (android.graphics.Rect) null);
        } else {
            if (keyEvent.hasModifiers(1)) {
                return mo11867b(1, (android.graphics.Rect) null);
            }
            return false;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo11871c(int i, int i2, android.os.Bundle bundle) {
        if (i2 == 1) {
            return mo11876g(i);
        }
        if (i2 == 2) {
            return mo11865b(i);
        }
        if (i2 == 64) {
            return mo11875f(i);
        }
        if (i2 != 128) {
            return mo11858a(i, i2, bundle);
        }
        return mo11857a(i);
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo11864b() {
        int i = this.f5985k;
        return i != Integer.MIN_VALUE && mo11858a(i, 16, (android.os.Bundle) null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo1698b(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        super.mo1698b(view, accessibilityEvent);
        mo11853a(accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: b */
    public final android.view.accessibility.AccessibilityEvent mo11863b(int i, int i2) {
        android.view.accessibility.AccessibilityEvent obtain = android.view.accessibility.AccessibilityEvent.obtain(i2);
        com.fossil.blesdk.obfuscated.C2711q9 e = mo11874e(i);
        obtain.getText().add(e.mo15096h());
        obtain.setContentDescription(e.mo15083d());
        obtain.setScrollable(e.mo15116s());
        obtain.setPassword(e.mo15115r());
        obtain.setEnabled(e.mo15110n());
        obtain.setChecked(e.mo15106l());
        mo11850a(i, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setClassName(e.mo15077c());
            com.fossil.blesdk.obfuscated.C2866s9.m13573a(obtain, this.f5982h, i);
            obtain.setPackageName(this.f5982h.getContext().getPackageName());
            return obtain;
        }
        throw new java.lang.RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11856a(boolean z, int i, android.graphics.Rect rect) {
        int i2 = this.f5985k;
        if (i2 != Integer.MIN_VALUE) {
            mo11865b(i2);
        }
        if (z) {
            mo11867b(i, rect);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11849a(int i, android.graphics.Rect rect) {
        mo11874e(i).mo15061a(rect);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Rect m8285a(android.view.View view, int i, android.graphics.Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i == 17) {
            rect.set(width, 0, width, height);
        } else if (i == 33) {
            rect.set(0, height, width, height);
        } else if (i == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo11866b(int i, int i2, android.os.Bundle bundle) {
        if (i != -1) {
            return mo11871c(i, i2, bundle);
        }
        return mo11859a(i2, bundle);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.accessibility.AccessibilityEvent mo11847a(int i, int i2) {
        if (i != -1) {
            return mo11863b(i, i2);
        }
        return mo11868c(i2);
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo11865b(int i) {
        if (this.f5985k != i) {
            return false;
        }
        this.f5985k = Integer.MIN_VALUE;
        mo11852a(i, false);
        mo11870c(i, 8);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1696a(android.view.View view, com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
        super.mo1696a(view, q9Var);
        mo11854a(q9Var);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11859a(int i, android.os.Bundle bundle) {
        return com.fossil.blesdk.obfuscated.C1776f9.m6820a(this.f5982h, i, bundle);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11860a(android.graphics.Rect rect) {
        if (rect == null || rect.isEmpty() || this.f5982h.getWindowVisibility() != 0) {
            return false;
        }
        android.view.ViewParent parent = this.f5982h.getParent();
        while (parent instanceof android.view.View) {
            android.view.View view = (android.view.View) parent;
            if (view.getAlpha() <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        if (parent != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11857a(int i) {
        if (this.f5984j != i) {
            return false;
        }
        this.f5984j = Integer.MIN_VALUE;
        this.f5982h.invalidate();
        mo11870c(i, 65536);
        return true;
    }
}
