package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pd */
public class C2644pd<T> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ com.fossil.blesdk.obfuscated.C2644pd f8341e; // = new com.fossil.blesdk.obfuscated.C2644pd(java.util.Collections.emptyList(), 0);

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C2644pd f8342f; // = new com.fossil.blesdk.obfuscated.C2644pd(java.util.Collections.emptyList(), 0);

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<T> f8343a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f8344b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f8345c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f8346d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pd$a */
    public static abstract class C2645a<T> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo12730a(int i, com.fossil.blesdk.obfuscated.C2644pd<T> pdVar);
    }

    @DexIgnore
    public C2644pd(java.util.List<T> list, int i, int i2, int i3) {
        this.f8343a = list;
        this.f8344b = i;
        this.f8345c = i2;
        this.f8346d = i3;
    }

    @DexIgnore
    /* renamed from: b */
    public static <T> com.fossil.blesdk.obfuscated.C2644pd<T> m12217b() {
        return f8341e;
    }

    @DexIgnore
    /* renamed from: c */
    public static <T> com.fossil.blesdk.obfuscated.C2644pd<T> m12218c() {
        return f8342f;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14665a() {
        return this == f8342f;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Result " + this.f8344b + ", " + this.f8343a + ", " + this.f8345c + ", offset " + this.f8346d;
    }

    @DexIgnore
    public C2644pd(java.util.List<T> list, int i) {
        this.f8343a = list;
        this.f8344b = 0;
        this.f8345c = 0;
        this.f8346d = i;
    }
}
