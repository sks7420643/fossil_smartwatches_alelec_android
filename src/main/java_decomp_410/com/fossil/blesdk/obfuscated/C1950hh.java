package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hh */
public class C1950hh implements android.animation.TypeEvaluator<android.graphics.Rect> {

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Rect f5745a;

    @DexIgnore
    public C1950hh() {
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Rect evaluate(float f, android.graphics.Rect rect, android.graphics.Rect rect2) {
        int i = rect.left;
        int i2 = i + ((int) (((float) (rect2.left - i)) * f));
        int i3 = rect.top;
        int i4 = i3 + ((int) (((float) (rect2.top - i3)) * f));
        int i5 = rect.right;
        int i6 = i5 + ((int) (((float) (rect2.right - i5)) * f));
        int i7 = rect.bottom;
        int i8 = i7 + ((int) (((float) (rect2.bottom - i7)) * f));
        android.graphics.Rect rect3 = this.f5745a;
        if (rect3 == null) {
            return new android.graphics.Rect(i2, i4, i6, i8);
        }
        rect3.set(i2, i4, i6, i8);
        return this.f5745a;
    }

    @DexIgnore
    public C1950hh(android.graphics.Rect rect) {
        this.f5745a = rect;
    }
}
