package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eh */
public class C1731eh<T> extends android.util.Property<T, java.lang.Float> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.util.Property<T, android.graphics.PointF> f4830a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.graphics.PathMeasure f4831b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ float f4832c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ float[] f4833d; // = new float[2];

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.PointF f4834e; // = new android.graphics.PointF();

    @DexIgnore
    /* renamed from: f */
    public float f4835f;

    @DexIgnore
    public C1731eh(android.util.Property<T, android.graphics.PointF> property, android.graphics.Path path) {
        super(java.lang.Float.class, property.getName());
        this.f4830a = property;
        this.f4831b = new android.graphics.PathMeasure(path, false);
        this.f4832c = this.f4831b.getLength();
    }

    @DexIgnore
    /* renamed from: a */
    public void set(T t, java.lang.Float f) {
        this.f4835f = f.floatValue();
        this.f4831b.getPosTan(this.f4832c * f.floatValue(), this.f4833d, (float[]) null);
        android.graphics.PointF pointF = this.f4834e;
        float[] fArr = this.f4833d;
        pointF.x = fArr[0];
        pointF.y = fArr[1];
        this.f4830a.set(t, pointF);
    }

    @DexIgnore
    public java.lang.Float get(T t) {
        return java.lang.Float.valueOf(this.f4835f);
    }
}
