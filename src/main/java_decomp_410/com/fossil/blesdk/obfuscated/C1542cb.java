package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cb */
public class C1542cb {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ int[] f4030a; // = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8};

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C1713eb f4031b; // = (android.os.Build.VERSION.SDK_INT >= 21 ? new com.fossil.blesdk.obfuscated.C1607db() : null);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C1713eb f4032c; // = m5351a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cb$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cb$a */
    public static class C1543a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f4033e;

        @DexIgnore
        public C1543a(java.util.ArrayList arrayList) {
            this.f4033e = arrayList;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1542cb.m5370a((java.util.ArrayList<android.view.View>) this.f4033e, 4);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cb$b")
    /* renamed from: com.fossil.blesdk.obfuscated.cb$b */
    public static class C1544b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.Object f4034e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1713eb f4035f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ android.view.View f4036g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ androidx.fragment.app.Fragment f4037h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ java.util.ArrayList f4038i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ /* synthetic */ java.util.ArrayList f4039j;

        @DexIgnore
        /* renamed from: k */
        public /* final */ /* synthetic */ java.util.ArrayList f4040k;

        @DexIgnore
        /* renamed from: l */
        public /* final */ /* synthetic */ java.lang.Object f4041l;

        @DexIgnore
        public C1544b(java.lang.Object obj, com.fossil.blesdk.obfuscated.C1713eb ebVar, android.view.View view, androidx.fragment.app.Fragment fragment, java.util.ArrayList arrayList, java.util.ArrayList arrayList2, java.util.ArrayList arrayList3, java.lang.Object obj2) {
            this.f4034e = obj;
            this.f4035f = ebVar;
            this.f4036g = view;
            this.f4037h = fragment;
            this.f4038i = arrayList;
            this.f4039j = arrayList2;
            this.f4040k = arrayList3;
            this.f4041l = obj2;
        }

        @DexIgnore
        public void run() {
            java.lang.Object obj = this.f4034e;
            if (obj != null) {
                this.f4035f.mo9870b(obj, this.f4036g);
                this.f4039j.addAll(com.fossil.blesdk.obfuscated.C1542cb.m5360a(this.f4035f, this.f4034e, this.f4037h, (java.util.ArrayList<android.view.View>) this.f4038i, this.f4036g));
            }
            if (this.f4040k != null) {
                if (this.f4041l != null) {
                    java.util.ArrayList arrayList = new java.util.ArrayList();
                    arrayList.add(this.f4036g);
                    this.f4035f.mo9866a(this.f4041l, (java.util.ArrayList<android.view.View>) this.f4040k, (java.util.ArrayList<android.view.View>) arrayList);
                }
                this.f4040k.clear();
                this.f4040k.add(this.f4036g);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cb$c")
    /* renamed from: com.fossil.blesdk.obfuscated.cb$c */
    public static class C1545c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ androidx.fragment.app.Fragment f4042e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ androidx.fragment.app.Fragment f4043f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ boolean f4044g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1855g4 f4045h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ android.view.View f4046i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1713eb f4047j;

        @DexIgnore
        /* renamed from: k */
        public /* final */ /* synthetic */ android.graphics.Rect f4048k;

        @DexIgnore
        public C1545c(androidx.fragment.app.Fragment fragment, androidx.fragment.app.Fragment fragment2, boolean z, com.fossil.blesdk.obfuscated.C1855g4 g4Var, android.view.View view, com.fossil.blesdk.obfuscated.C1713eb ebVar, android.graphics.Rect rect) {
            this.f4042e = fragment;
            this.f4043f = fragment2;
            this.f4044g = z;
            this.f4045h = g4Var;
            this.f4046i = view;
            this.f4047j = ebVar;
            this.f4048k = rect;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1542cb.m5361a(this.f4042e, this.f4043f, this.f4044g, (com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View>) this.f4045h, false);
            android.view.View view = this.f4046i;
            if (view != null) {
                this.f4047j.mo10405a(view, this.f4048k);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cb$d")
    /* renamed from: com.fossil.blesdk.obfuscated.cb$d */
    public static class C1546d implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1713eb f4049e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1855g4 f4050f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ java.lang.Object f4051g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1542cb.C1547e f4052h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ java.util.ArrayList f4053i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ /* synthetic */ android.view.View f4054j;

        @DexIgnore
        /* renamed from: k */
        public /* final */ /* synthetic */ androidx.fragment.app.Fragment f4055k;

        @DexIgnore
        /* renamed from: l */
        public /* final */ /* synthetic */ androidx.fragment.app.Fragment f4056l;

        @DexIgnore
        /* renamed from: m */
        public /* final */ /* synthetic */ boolean f4057m;

        @DexIgnore
        /* renamed from: n */
        public /* final */ /* synthetic */ java.util.ArrayList f4058n;

        @DexIgnore
        /* renamed from: o */
        public /* final */ /* synthetic */ java.lang.Object f4059o;

        @DexIgnore
        /* renamed from: p */
        public /* final */ /* synthetic */ android.graphics.Rect f4060p;

        @DexIgnore
        public C1546d(com.fossil.blesdk.obfuscated.C1713eb ebVar, com.fossil.blesdk.obfuscated.C1855g4 g4Var, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, java.util.ArrayList arrayList, android.view.View view, androidx.fragment.app.Fragment fragment, androidx.fragment.app.Fragment fragment2, boolean z, java.util.ArrayList arrayList2, java.lang.Object obj2, android.graphics.Rect rect) {
            this.f4049e = ebVar;
            this.f4050f = g4Var;
            this.f4051g = obj;
            this.f4052h = eVar;
            this.f4053i = arrayList;
            this.f4054j = view;
            this.f4055k = fragment;
            this.f4056l = fragment2;
            this.f4057m = z;
            this.f4058n = arrayList2;
            this.f4059o = obj2;
            this.f4060p = rect;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> a = com.fossil.blesdk.obfuscated.C1542cb.m5354a(this.f4049e, (com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String>) this.f4050f, this.f4051g, this.f4052h);
            if (a != null) {
                this.f4053i.addAll(a.values());
                this.f4053i.add(this.f4054j);
            }
            com.fossil.blesdk.obfuscated.C1542cb.m5361a(this.f4055k, this.f4056l, this.f4057m, a, false);
            java.lang.Object obj = this.f4051g;
            if (obj != null) {
                this.f4049e.mo9872b(obj, (java.util.ArrayList<android.view.View>) this.f4058n, (java.util.ArrayList<android.view.View>) this.f4053i);
                android.view.View a2 = com.fossil.blesdk.obfuscated.C1542cb.m5349a(a, this.f4052h, this.f4059o, this.f4057m);
                if (a2 != null) {
                    this.f4049e.mo10405a(a2, this.f4060p);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cb$e")
    /* renamed from: com.fossil.blesdk.obfuscated.cb$e */
    public static class C1547e {

        @DexIgnore
        /* renamed from: a */
        public androidx.fragment.app.Fragment f4061a;

        @DexIgnore
        /* renamed from: b */
        public boolean f4062b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C3101va f4063c;

        @DexIgnore
        /* renamed from: d */
        public androidx.fragment.app.Fragment f4064d;

        @DexIgnore
        /* renamed from: e */
        public boolean f4065e;

        @DexIgnore
        /* renamed from: f */
        public com.fossil.blesdk.obfuscated.C3101va f4066f;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1713eb m5351a() {
        try {
            return (com.fossil.blesdk.obfuscated.C1713eb) java.lang.Class.forName("com.fossil.blesdk.obfuscated.wg").getDeclaredConstructor(new java.lang.Class[0]).newInstance(new java.lang.Object[0]);
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m5376b(androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl, int i, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, android.view.View view, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var) {
        java.lang.Object obj;
        androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl2 = fragmentManagerImpl;
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = eVar;
        android.view.View view2 = view;
        android.view.ViewGroup viewGroup = fragmentManagerImpl2.f1071r.mo1995a() ? (android.view.ViewGroup) fragmentManagerImpl2.f1071r.mo1993a(i) : null;
        if (viewGroup != null) {
            androidx.fragment.app.Fragment fragment = eVar2.f4061a;
            androidx.fragment.app.Fragment fragment2 = eVar2.f4064d;
            com.fossil.blesdk.obfuscated.C1713eb a = m5352a(fragment2, fragment);
            if (a != null) {
                boolean z = eVar2.f4062b;
                boolean z2 = eVar2.f4065e;
                java.util.ArrayList arrayList = new java.util.ArrayList();
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                java.lang.Object a2 = m5357a(a, fragment, z);
                java.lang.Object b = m5375b(a, fragment2, z2);
                android.view.ViewGroup viewGroup2 = viewGroup;
                java.util.ArrayList arrayList3 = arrayList2;
                java.lang.Object b2 = m5374b(a, viewGroup, view, g4Var, eVar, arrayList2, arrayList, a2, b);
                java.lang.Object obj2 = a2;
                if (obj2 == null && b2 == null) {
                    obj = b;
                    if (obj == null) {
                        return;
                    }
                } else {
                    obj = b;
                }
                java.util.ArrayList<android.view.View> a3 = m5360a(a, obj, fragment2, (java.util.ArrayList<android.view.View>) arrayList3, view2);
                java.util.ArrayList<android.view.View> a4 = m5360a(a, obj2, fragment, (java.util.ArrayList<android.view.View>) arrayList, view2);
                m5370a(a4, 4);
                androidx.fragment.app.Fragment fragment3 = fragment;
                java.util.ArrayList<android.view.View> arrayList4 = a3;
                java.lang.Object a5 = m5358a(a, obj2, obj, b2, fragment3, z);
                if (a5 != null) {
                    m5365a(a, obj, fragment2, arrayList4);
                    java.util.ArrayList<java.lang.String> a6 = a.mo10404a((java.util.ArrayList<android.view.View>) arrayList);
                    a.mo9864a(a5, obj2, a4, obj, arrayList4, b2, arrayList);
                    android.view.ViewGroup viewGroup3 = viewGroup2;
                    a.mo9860a(viewGroup3, a5);
                    a.mo10406a(viewGroup3, arrayList3, arrayList, a6, g4Var);
                    m5370a(a4, 0);
                    a.mo9872b(b2, (java.util.ArrayList<android.view.View>) arrayList3, (java.util.ArrayList<android.view.View>) arrayList);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5363a(androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl, java.util.ArrayList<com.fossil.blesdk.obfuscated.C3101va> arrayList, java.util.ArrayList<java.lang.Boolean> arrayList2, int i, int i2, boolean z) {
        if (fragmentManagerImpl.f1069p >= 1) {
            android.util.SparseArray sparseArray = new android.util.SparseArray();
            for (int i3 = i; i3 < i2; i3++) {
                com.fossil.blesdk.obfuscated.C3101va vaVar = arrayList.get(i3);
                if (arrayList2.get(i3).booleanValue()) {
                    m5377b(vaVar, (android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e>) sparseArray, z);
                } else {
                    m5368a(vaVar, (android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e>) sparseArray, z);
                }
            }
            if (sparseArray.size() != 0) {
                android.view.View view = new android.view.View(fragmentManagerImpl.f1070q.mo2073c());
                int size = sparseArray.size();
                for (int i4 = 0; i4 < size; i4++) {
                    int keyAt = sparseArray.keyAt(i4);
                    com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> a = m5353a(keyAt, arrayList, arrayList2, i, i2);
                    com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar = (com.fossil.blesdk.obfuscated.C1542cb.C1547e) sparseArray.valueAt(i4);
                    if (z) {
                        m5376b(fragmentManagerImpl, keyAt, eVar, view, a);
                    } else {
                        m5362a(fragmentManagerImpl, keyAt, eVar, view, a);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> m5353a(int i, java.util.ArrayList<com.fossil.blesdk.obfuscated.C3101va> arrayList, java.util.ArrayList<java.lang.Boolean> arrayList2, int i2, int i3) {
        java.util.ArrayList<java.lang.String> arrayList3;
        java.util.ArrayList<java.lang.String> arrayList4;
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var = new com.fossil.blesdk.obfuscated.C1855g4<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            com.fossil.blesdk.obfuscated.C3101va vaVar = arrayList.get(i4);
            if (vaVar.mo17011b(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                java.util.ArrayList<java.lang.String> arrayList5 = vaVar.f10207r;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = vaVar.f10207r;
                        arrayList4 = vaVar.f10208s;
                    } else {
                        java.util.ArrayList<java.lang.String> arrayList6 = vaVar.f10207r;
                        arrayList3 = vaVar.f10208s;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        java.lang.String str = arrayList4.get(i5);
                        java.lang.String str2 = arrayList3.get(i5);
                        java.lang.String remove = g4Var.remove(str2);
                        if (remove != null) {
                            g4Var.put(str, remove);
                        } else {
                            g4Var.put(str, str2);
                        }
                    }
                }
            }
        }
        return g4Var;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.Object m5375b(com.fossil.blesdk.obfuscated.C1713eb ebVar, androidx.fragment.app.Fragment fragment, boolean z) {
        java.lang.Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return ebVar.mo9868b(obj);
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.Object m5374b(com.fossil.blesdk.obfuscated.C1713eb ebVar, android.view.ViewGroup viewGroup, android.view.View view, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj, java.lang.Object obj2) {
        java.lang.Object obj3;
        java.lang.Object obj4;
        android.graphics.Rect rect;
        android.view.View view2;
        com.fossil.blesdk.obfuscated.C1713eb ebVar2 = ebVar;
        android.view.View view3 = view;
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var2 = g4Var;
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = eVar;
        java.util.ArrayList<android.view.View> arrayList3 = arrayList;
        java.util.ArrayList<android.view.View> arrayList4 = arrayList2;
        java.lang.Object obj5 = obj;
        androidx.fragment.app.Fragment fragment = eVar2.f4061a;
        androidx.fragment.app.Fragment fragment2 = eVar2.f4064d;
        if (fragment != null) {
            fragment.getView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.f4062b;
        if (g4Var.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = m5356a(ebVar, fragment, fragment2, z);
        }
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> b = m5373b(ebVar, g4Var2, obj3, eVar2);
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> a = m5354a(ebVar, g4Var2, obj3, eVar2);
        if (g4Var.isEmpty()) {
            if (b != null) {
                b.clear();
            }
            if (a != null) {
                a.clear();
            }
            obj4 = null;
        } else {
            m5371a(arrayList3, b, (java.util.Collection<java.lang.String>) g4Var.keySet());
            m5371a(arrayList4, a, g4Var.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        m5361a(fragment, fragment2, z, b, true);
        if (obj4 != null) {
            arrayList4.add(view3);
            ebVar.mo9871b(obj4, view3, arrayList3);
            m5366a(ebVar, obj4, obj2, b, eVar2.f4065e, eVar2.f4066f);
            android.graphics.Rect rect2 = new android.graphics.Rect();
            android.view.View a2 = m5349a(a, eVar2, obj5, z);
            if (a2 != null) {
                ebVar.mo9861a(obj5, rect2);
            }
            rect = rect2;
            view2 = a2;
        } else {
            view2 = null;
            rect = null;
        }
        com.fossil.blesdk.obfuscated.C1542cb.C1545c cVar = new com.fossil.blesdk.obfuscated.C1542cb.C1545c(fragment, fragment2, z, a, view2, ebVar, rect);
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(viewGroup, cVar);
        return obj4;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5365a(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.lang.Object obj, androidx.fragment.app.Fragment fragment, java.util.ArrayList<android.view.View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            ebVar.mo9863a(obj, fragment.getView(), arrayList);
            com.fossil.blesdk.obfuscated.C1865gb.m7335a(fragment.mContainer, new com.fossil.blesdk.obfuscated.C1542cb.C1543a(arrayList));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5362a(androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl, int i, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, android.view.View view, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var) {
        java.lang.Object obj;
        androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl2 = fragmentManagerImpl;
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = eVar;
        android.view.View view2 = view;
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var2 = g4Var;
        android.view.ViewGroup viewGroup = fragmentManagerImpl2.f1071r.mo1995a() ? (android.view.ViewGroup) fragmentManagerImpl2.f1071r.mo1993a(i) : null;
        if (viewGroup != null) {
            androidx.fragment.app.Fragment fragment = eVar2.f4061a;
            androidx.fragment.app.Fragment fragment2 = eVar2.f4064d;
            com.fossil.blesdk.obfuscated.C1713eb a = m5352a(fragment2, fragment);
            if (a != null) {
                boolean z = eVar2.f4062b;
                boolean z2 = eVar2.f4065e;
                java.lang.Object a2 = m5357a(a, fragment, z);
                java.lang.Object b = m5375b(a, fragment2, z2);
                java.util.ArrayList arrayList = new java.util.ArrayList();
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                java.util.ArrayList arrayList3 = arrayList;
                java.lang.Object obj2 = b;
                com.fossil.blesdk.obfuscated.C1713eb ebVar = a;
                java.lang.Object a3 = m5355a(a, viewGroup, view, g4Var, eVar, (java.util.ArrayList<android.view.View>) arrayList, (java.util.ArrayList<android.view.View>) arrayList2, a2, obj2);
                java.lang.Object obj3 = a2;
                if (obj3 == null && a3 == null) {
                    obj = obj2;
                    if (obj == null) {
                        return;
                    }
                } else {
                    obj = obj2;
                }
                java.util.ArrayList<android.view.View> a4 = m5360a(ebVar, obj, fragment2, (java.util.ArrayList<android.view.View>) arrayList3, view2);
                java.lang.Object obj4 = (a4 == null || a4.isEmpty()) ? null : obj;
                ebVar.mo9862a(obj3, view2);
                java.lang.Object a5 = m5358a(ebVar, obj3, obj4, a3, fragment, eVar2.f4062b);
                if (a5 != null) {
                    java.util.ArrayList arrayList4 = new java.util.ArrayList();
                    com.fossil.blesdk.obfuscated.C1713eb ebVar2 = ebVar;
                    ebVar2.mo9864a(a5, obj3, arrayList4, obj4, a4, a3, arrayList2);
                    m5364a(ebVar2, viewGroup, fragment, view, (java.util.ArrayList<android.view.View>) arrayList2, obj3, (java.util.ArrayList<android.view.View>) arrayList4, obj4, a4);
                    java.util.ArrayList arrayList5 = arrayList2;
                    ebVar.mo10407a((android.view.View) viewGroup, (java.util.ArrayList<android.view.View>) arrayList5, (java.util.Map<java.lang.String, java.lang.String>) g4Var2);
                    ebVar.mo9860a(viewGroup, a5);
                    ebVar.mo10408a(viewGroup, (java.util.ArrayList<android.view.View>) arrayList5, (java.util.Map<java.lang.String, java.lang.String>) g4Var2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> m5373b(com.fossil.blesdk.obfuscated.C1713eb ebVar, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar) {
        androidx.core.app.SharedElementCallback sharedElementCallback;
        java.util.ArrayList<java.lang.String> arrayList;
        if (g4Var.isEmpty() || obj == null) {
            g4Var.clear();
            return null;
        }
        androidx.fragment.app.Fragment fragment = eVar.f4064d;
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var2 = new com.fossil.blesdk.obfuscated.C1855g4<>();
        ebVar.mo10410a((java.util.Map<java.lang.String, android.view.View>) g4Var2, fragment.getView());
        com.fossil.blesdk.obfuscated.C3101va vaVar = eVar.f4066f;
        if (eVar.f4065e) {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = vaVar.f10208s;
        } else {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = vaVar.f10207r;
        }
        g4Var2.mo11119a(arrayList);
        if (sharedElementCallback != null) {
            sharedElementCallback.mo1581a((java.util.List<java.lang.String>) arrayList, (java.util.Map<java.lang.String, android.view.View>) g4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                java.lang.String str = arrayList.get(size);
                android.view.View view = g4Var2.get(str);
                if (view == null) {
                    g4Var.remove(str);
                } else if (!str.equals(com.fossil.blesdk.obfuscated.C1776f9.m6851q(view))) {
                    g4Var.put(com.fossil.blesdk.obfuscated.C1776f9.m6851q(view), g4Var.remove(str));
                }
            }
        } else {
            g4Var.mo11119a(g4Var2.keySet());
        }
        return g4Var2;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5364a(com.fossil.blesdk.obfuscated.C1713eb ebVar, android.view.ViewGroup viewGroup, androidx.fragment.app.Fragment fragment, android.view.View view, java.util.ArrayList<android.view.View> arrayList, java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj2, java.util.ArrayList<android.view.View> arrayList3) {
        com.fossil.blesdk.obfuscated.C1542cb.C1544b bVar = new com.fossil.blesdk.obfuscated.C1542cb.C1544b(obj, ebVar, view, fragment, arrayList, arrayList2, arrayList3, obj2);
        android.view.ViewGroup viewGroup2 = viewGroup;
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(viewGroup, bVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1713eb m5352a(androidx.fragment.app.Fragment fragment, androidx.fragment.app.Fragment fragment2) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        if (fragment != null) {
            java.lang.Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            java.lang.Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            java.lang.Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            java.lang.Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            java.lang.Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            java.lang.Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C1713eb ebVar = f4031b;
        if (ebVar != null && m5372a(ebVar, (java.util.List<java.lang.Object>) arrayList)) {
            return f4031b;
        }
        com.fossil.blesdk.obfuscated.C1713eb ebVar2 = f4032c;
        if (ebVar2 != null && m5372a(ebVar2, (java.util.List<java.lang.Object>) arrayList)) {
            return f4032c;
        }
        if (f4031b == null && f4032c == null) {
            return null;
        }
        throw new java.lang.IllegalArgumentException("Invalid Transition types");
    }

    @DexIgnore
    /* renamed from: b */
    public static void m5377b(com.fossil.blesdk.obfuscated.C3101va vaVar, android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e> sparseArray, boolean z) {
        if (vaVar.f10190a.f1071r.mo1995a()) {
            for (int size = vaVar.f10191b.size() - 1; size >= 0; size--) {
                m5369a(vaVar, vaVar.f10191b.get(size), sparseArray, true, z);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5372a(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.util.List<java.lang.Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!ebVar.mo9867a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m5356a(com.fossil.blesdk.obfuscated.C1713eb ebVar, androidx.fragment.app.Fragment fragment, androidx.fragment.app.Fragment fragment2, boolean z) {
        java.lang.Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return ebVar.mo9873c(ebVar.mo9868b(obj));
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m5357a(com.fossil.blesdk.obfuscated.C1713eb ebVar, androidx.fragment.app.Fragment fragment, boolean z) {
        java.lang.Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return ebVar.mo9868b(obj);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5371a(java.util.ArrayList<android.view.View> arrayList, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var, java.util.Collection<java.lang.String> collection) {
        for (int size = g4Var.size() - 1; size >= 0; size--) {
            android.view.View e = g4Var.mo1229e(size);
            if (collection.contains(com.fossil.blesdk.obfuscated.C1776f9.m6851q(e))) {
                arrayList.add(e);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m5355a(com.fossil.blesdk.obfuscated.C1713eb ebVar, android.view.ViewGroup viewGroup, android.view.View view, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj, java.lang.Object obj2) {
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var2;
        java.lang.Object obj3;
        java.lang.Object obj4;
        android.graphics.Rect rect;
        com.fossil.blesdk.obfuscated.C1713eb ebVar2 = ebVar;
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = eVar;
        java.util.ArrayList<android.view.View> arrayList3 = arrayList;
        java.lang.Object obj5 = obj;
        androidx.fragment.app.Fragment fragment = eVar2.f4061a;
        androidx.fragment.app.Fragment fragment2 = eVar2.f4064d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = eVar2.f4062b;
        if (g4Var.isEmpty()) {
            g4Var2 = g4Var;
            obj3 = null;
        } else {
            obj3 = m5356a(ebVar2, fragment, fragment2, z);
            g4Var2 = g4Var;
        }
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> b = m5373b(ebVar2, g4Var2, obj3, eVar2);
        if (g4Var.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(b.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        m5361a(fragment, fragment2, z, b, true);
        if (obj4 != null) {
            rect = new android.graphics.Rect();
            ebVar2.mo9871b(obj4, view, arrayList3);
            m5366a(ebVar, obj4, obj2, b, eVar2.f4065e, eVar2.f4066f);
            if (obj5 != null) {
                ebVar2.mo9861a(obj5, rect);
            }
        } else {
            rect = null;
        }
        com.fossil.blesdk.obfuscated.C1542cb.C1546d dVar = r0;
        com.fossil.blesdk.obfuscated.C1542cb.C1546d dVar2 = new com.fossil.blesdk.obfuscated.C1542cb.C1546d(ebVar, g4Var, obj4, eVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect);
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(viewGroup, dVar);
        return obj4;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> m5354a(com.fossil.blesdk.obfuscated.C1713eb ebVar, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar) {
        androidx.core.app.SharedElementCallback sharedElementCallback;
        java.util.ArrayList<java.lang.String> arrayList;
        androidx.fragment.app.Fragment fragment = eVar.f4061a;
        android.view.View view = fragment.getView();
        if (g4Var.isEmpty() || obj == null || view == null) {
            g4Var.clear();
            return null;
        }
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var2 = new com.fossil.blesdk.obfuscated.C1855g4<>();
        ebVar.mo10410a((java.util.Map<java.lang.String, android.view.View>) g4Var2, view);
        com.fossil.blesdk.obfuscated.C3101va vaVar = eVar.f4063c;
        if (eVar.f4062b) {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = vaVar.f10207r;
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = vaVar.f10208s;
        }
        if (arrayList != null) {
            g4Var2.mo11119a(arrayList);
            g4Var2.mo11119a(g4Var.values());
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.mo1581a((java.util.List<java.lang.String>) arrayList, (java.util.Map<java.lang.String, android.view.View>) g4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                java.lang.String str = arrayList.get(size);
                android.view.View view2 = g4Var2.get(str);
                if (view2 == null) {
                    java.lang.String a = m5359a(g4Var, str);
                    if (a != null) {
                        g4Var.remove(a);
                    }
                } else if (!str.equals(com.fossil.blesdk.obfuscated.C1776f9.m6851q(view2))) {
                    java.lang.String a2 = m5359a(g4Var, str);
                    if (a2 != null) {
                        g4Var.put(a2, com.fossil.blesdk.obfuscated.C1776f9.m6851q(view2));
                    }
                }
            }
        } else {
            m5367a(g4Var, g4Var2);
        }
        return g4Var2;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m5359a(com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, java.lang.String str) {
        int size = g4Var.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(g4Var.mo1229e(i))) {
                return g4Var.mo1224c(i);
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.View m5349a(com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var, com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, java.lang.Object obj, boolean z) {
        java.lang.String str;
        com.fossil.blesdk.obfuscated.C3101va vaVar = eVar.f4063c;
        if (obj == null || g4Var == null) {
            return null;
        }
        java.util.ArrayList<java.lang.String> arrayList = vaVar.f10207r;
        if (arrayList == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = vaVar.f10207r.get(0);
        } else {
            str = vaVar.f10208s.get(0);
        }
        return g4Var.get(str);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5366a(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.lang.Object obj, java.lang.Object obj2, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var, boolean z, com.fossil.blesdk.obfuscated.C3101va vaVar) {
        java.lang.String str;
        java.util.ArrayList<java.lang.String> arrayList = vaVar.f10207r;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = vaVar.f10208s.get(0);
            } else {
                str = vaVar.f10207r.get(0);
            }
            android.view.View view = g4Var.get(str);
            ebVar.mo9874c(obj, view);
            if (obj2 != null) {
                ebVar.mo9874c(obj2, view);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5367a(com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.String> g4Var, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var2) {
        for (int size = g4Var.size() - 1; size >= 0; size--) {
            if (!g4Var2.containsKey(g4Var.mo1229e(size))) {
                g4Var.mo1228d(size);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5361a(androidx.fragment.app.Fragment fragment, androidx.fragment.app.Fragment fragment2, boolean z, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> g4Var, boolean z2) {
        androidx.core.app.SharedElementCallback sharedElementCallback;
        int i;
        if (z) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            java.util.ArrayList arrayList = new java.util.ArrayList();
            java.util.ArrayList arrayList2 = new java.util.ArrayList();
            if (g4Var == null) {
                i = 0;
            } else {
                i = g4Var.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(g4Var.mo1224c(i2));
                arrayList.add(g4Var.mo1229e(i2));
            }
            if (z2) {
                sharedElementCallback.mo1582b(arrayList2, arrayList, (java.util.List<android.view.View>) null);
            } else {
                sharedElementCallback.mo1580a((java.util.List<java.lang.String>) arrayList2, (java.util.List<android.view.View>) arrayList, (java.util.List<android.view.View>) null);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.ArrayList<android.view.View> m5360a(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.lang.Object obj, androidx.fragment.app.Fragment fragment, java.util.ArrayList<android.view.View> arrayList, android.view.View view) {
        if (obj == null) {
            return null;
        }
        java.util.ArrayList<android.view.View> arrayList2 = new java.util.ArrayList<>();
        android.view.View view2 = fragment.getView();
        if (view2 != null) {
            ebVar.mo10409a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        ebVar.mo9865a(obj, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5370a(java.util.ArrayList<android.view.View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m5358a(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3, androidx.fragment.app.Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return ebVar.mo9869b(obj2, obj, obj3);
        }
        return ebVar.mo9859a(obj2, obj, obj3);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5368a(com.fossil.blesdk.obfuscated.C3101va vaVar, android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e> sparseArray, boolean z) {
        int size = vaVar.f10191b.size();
        for (int i = 0; i < size; i++) {
            m5369a(vaVar, vaVar.f10191b.get(i), sparseArray, false, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0041, code lost:
        if (r10.mAdded != false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0074, code lost:
        if (r10.mPostponedAlpha >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0076, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0080, code lost:
        if (r10.mHidden == false) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0092, code lost:
        if (r10.mHidden == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0094, code lost:
        r1 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00e7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* renamed from: a */
    public static void m5369a(com.fossil.blesdk.obfuscated.C3101va vaVar, com.fossil.blesdk.obfuscated.C3101va.C3102a aVar, android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e> sparseArray, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5;
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar;
        androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl;
        boolean z6;
        com.fossil.blesdk.obfuscated.C3101va vaVar2 = vaVar;
        com.fossil.blesdk.obfuscated.C3101va.C3102a aVar2 = aVar;
        android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e> sparseArray2 = sparseArray;
        boolean z7 = z;
        androidx.fragment.app.Fragment fragment = aVar2.f10212b;
        if (fragment != null) {
            int i = fragment.mContainerId;
            if (i != 0) {
                int i2 = z7 ? f4030a[aVar2.f10211a] : aVar2.f10211a;
                boolean z8 = false;
                if (i2 != 1) {
                    if (i2 != 3) {
                        if (i2 != 4) {
                            if (i2 != 5) {
                                if (i2 != 6) {
                                    if (i2 != 7) {
                                        z5 = false;
                                        z4 = false;
                                        z3 = false;
                                        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = sparseArray2.get(i);
                                        if (z8) {
                                            eVar2 = m5350a(eVar2, sparseArray2, i);
                                            eVar2.f4061a = fragment;
                                            eVar2.f4062b = z7;
                                            eVar2.f4063c = vaVar2;
                                        }
                                        eVar = eVar2;
                                        if (!z2 && z5) {
                                            if (eVar != null && eVar.f4064d == fragment) {
                                                eVar.f4064d = null;
                                            }
                                            fragmentManagerImpl = vaVar2.f10190a;
                                            if (fragment.mState < 1 && fragmentManagerImpl.f1069p >= 1 && !vaVar2.f10209t) {
                                                fragmentManagerImpl.mo2167g(fragment);
                                                fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                                            }
                                        }
                                        if (z3 && (eVar == null || eVar.f4064d == null)) {
                                            eVar = m5350a(eVar, sparseArray2, i);
                                            eVar.f4064d = fragment;
                                            eVar.f4065e = z7;
                                            eVar.f4066f = vaVar2;
                                        }
                                        if (z2 && z4 && eVar != null && eVar.f4061a == fragment) {
                                            eVar.f4061a = null;
                                            return;
                                        }
                                        return;
                                    }
                                }
                            } else if (z2) {
                                if (fragment.mHiddenChanged) {
                                    if (!fragment.mHidden) {
                                    }
                                }
                                z6 = false;
                                z8 = z6;
                                z5 = true;
                                z4 = false;
                                z3 = false;
                                com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar22 = sparseArray2.get(i);
                                if (z8) {
                                }
                                eVar = eVar22;
                                eVar.f4064d = null;
                                fragmentManagerImpl = vaVar2.f10190a;
                                fragmentManagerImpl.mo2167g(fragment);
                                fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                                eVar = m5350a(eVar, sparseArray2, i);
                                eVar.f4064d = fragment;
                                eVar.f4065e = z7;
                                eVar.f4066f = vaVar2;
                                if (z2) {
                                    return;
                                }
                                return;
                            } else {
                                z6 = fragment.mHidden;
                                z8 = z6;
                                z5 = true;
                                z4 = false;
                                z3 = false;
                                com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar222 = sparseArray2.get(i);
                                if (z8) {
                                }
                                eVar = eVar222;
                                eVar.f4064d = null;
                                fragmentManagerImpl = vaVar2.f10190a;
                                fragmentManagerImpl.mo2167g(fragment);
                                fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                                eVar = m5350a(eVar, sparseArray2, i);
                                eVar.f4064d = fragment;
                                eVar.f4065e = z7;
                                eVar.f4066f = vaVar2;
                                if (z2) {
                                }
                            }
                        } else if (!z2) {
                            boolean z9 = false;
                            z3 = z9;
                            z5 = false;
                            z4 = true;
                            com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2222 = sparseArray2.get(i);
                            if (z8) {
                            }
                            eVar = eVar2222;
                            eVar.f4064d = null;
                            fragmentManagerImpl = vaVar2.f10190a;
                            fragmentManagerImpl.mo2167g(fragment);
                            fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                            eVar = m5350a(eVar, sparseArray2, i);
                            eVar.f4064d = fragment;
                            eVar.f4065e = z7;
                            eVar.f4066f = vaVar2;
                            if (z2) {
                            }
                        } else {
                            boolean z92 = false;
                            z3 = z92;
                            z5 = false;
                            z4 = true;
                            com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar22222 = sparseArray2.get(i);
                            if (z8) {
                            }
                            eVar = eVar22222;
                            eVar.f4064d = null;
                            fragmentManagerImpl = vaVar2.f10190a;
                            fragmentManagerImpl.mo2167g(fragment);
                            fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                            eVar = m5350a(eVar, sparseArray2, i);
                            eVar.f4064d = fragment;
                            eVar.f4065e = z7;
                            eVar.f4066f = vaVar2;
                            if (z2) {
                            }
                        }
                    }
                    if (z2) {
                        if (!fragment.mAdded) {
                            android.view.View view = fragment.mView;
                            if (view != null) {
                                if (view.getVisibility() == 0) {
                                }
                            }
                        }
                        boolean z922 = false;
                        z3 = z922;
                        z5 = false;
                        z4 = true;
                        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar222222 = sparseArray2.get(i);
                        if (z8) {
                        }
                        eVar = eVar222222;
                        eVar.f4064d = null;
                        fragmentManagerImpl = vaVar2.f10190a;
                        fragmentManagerImpl.mo2167g(fragment);
                        fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                        eVar = m5350a(eVar, sparseArray2, i);
                        eVar.f4064d = fragment;
                        eVar.f4065e = z7;
                        eVar.f4066f = vaVar2;
                        if (z2) {
                        }
                    } else {
                        if (fragment.mAdded) {
                        }
                        boolean z9222 = false;
                        z3 = z9222;
                        z5 = false;
                        z4 = true;
                        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2222222 = sparseArray2.get(i);
                        if (z8) {
                        }
                        eVar = eVar2222222;
                        eVar.f4064d = null;
                        fragmentManagerImpl = vaVar2.f10190a;
                        fragmentManagerImpl.mo2167g(fragment);
                        fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                        eVar = m5350a(eVar, sparseArray2, i);
                        eVar.f4064d = fragment;
                        eVar.f4065e = z7;
                        eVar.f4066f = vaVar2;
                        if (z2) {
                        }
                    }
                }
                if (z2) {
                    z6 = fragment.mIsNewlyAdded;
                    z8 = z6;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar22222222 = sparseArray2.get(i);
                    if (z8) {
                    }
                    eVar = eVar22222222;
                    eVar.f4064d = null;
                    fragmentManagerImpl = vaVar2.f10190a;
                    fragmentManagerImpl.mo2167g(fragment);
                    fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                    eVar = m5350a(eVar, sparseArray2, i);
                    eVar.f4064d = fragment;
                    eVar.f4065e = z7;
                    eVar.f4066f = vaVar2;
                    if (z2) {
                    }
                } else {
                    if (!fragment.mAdded) {
                    }
                    z6 = false;
                    z8 = z6;
                    z5 = true;
                    z4 = false;
                    z3 = false;
                    com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar222222222 = sparseArray2.get(i);
                    if (z8) {
                    }
                    eVar = eVar222222222;
                    eVar.f4064d = null;
                    fragmentManagerImpl = vaVar2.f10190a;
                    fragmentManagerImpl.mo2167g(fragment);
                    fragmentManagerImpl.mo2121a(fragment, 1, 0, 0, false);
                    eVar = m5350a(eVar, sparseArray2, i);
                    eVar.f4064d = fragment;
                    eVar.f4065e = z7;
                    eVar.f4066f = vaVar2;
                    if (z2) {
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1542cb.C1547e m5350a(com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar, android.util.SparseArray<com.fossil.blesdk.obfuscated.C1542cb.C1547e> sparseArray, int i) {
        if (eVar != null) {
            return eVar;
        }
        com.fossil.blesdk.obfuscated.C1542cb.C1547e eVar2 = new com.fossil.blesdk.obfuscated.C1542cb.C1547e();
        sparseArray.put(i, eVar2);
        return eVar2;
    }
}
