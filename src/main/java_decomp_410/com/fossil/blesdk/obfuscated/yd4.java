package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yd4 extends wd4 implements vd4<Integer> {
    @DexIgnore
    public static /* final */ yd4 i; // = new yd4(1, 0);
    @DexIgnore
    public static /* final */ a j; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final yd4 a() {
            return yd4.i;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public yd4(int i2, int i3) {
        super(i2, i3, 1);
    }

    @DexIgnore
    public Integer d() {
        return Integer.valueOf(b());
    }

    @DexIgnore
    public Integer e() {
        return Integer.valueOf(a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof yd4) {
            if (!isEmpty() || !((yd4) obj).isEmpty()) {
                yd4 yd4 = (yd4) obj;
                if (!(a() == yd4.a() && b() == yd4.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
