package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class zs3 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[MicroAppInstruction.MicroAppID.values().length];

    /*
    static {
        a[MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE.ordinal()] = 1;
        a[MicroAppInstruction.MicroAppID.UAPP_SELFIE.ordinal()] = 2;
        a[MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID.ordinal()] = 3;
        a[MicroAppInstruction.MicroAppID.UAPP_ALARM_ID.ordinal()] = 4;
        a[MicroAppInstruction.MicroAppID.UAPP_ALERT_ID.ordinal()] = 5;
        a[MicroAppInstruction.MicroAppID.UAPP_DATE_ID.ordinal()] = 6;
        a[MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.ordinal()] = 7;
        a[MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.ordinal()] = 8;
        a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.ordinal()] = 9;
        a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID.ordinal()] = 10;
        a[MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID.ordinal()] = 11;
        a[MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.ordinal()] = 12;
        a[MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD.ordinal()] = 13;
        a[MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.ordinal()] = 14;
        a[MicroAppInstruction.MicroAppID.UAPP_STOPWATCH.ordinal()] = 15;
    }
    */
}
