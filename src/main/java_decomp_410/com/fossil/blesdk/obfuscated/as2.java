package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ws3;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class as2 extends zr2 implements ws3.g {
    @DexIgnore
    public HashMap j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        kd4.a((Object) as2.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a(PermissionCodes... permissionCodesArr) {
        kd4.b(permissionCodesArr, "permissionCodes");
        ArrayList arrayList = new ArrayList();
        for (PermissionCodes add : permissionCodesArr) {
            arrayList.add(add);
        }
        bn2.d.a(getContext(), (ArrayList<PermissionCodes>) arrayList);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (getActivity() instanceof BaseActivity) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
