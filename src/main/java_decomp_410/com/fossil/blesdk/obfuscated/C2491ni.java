package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ni */
public class C2491ni {
    @DexIgnore
    /* renamed from: a */
    public static android.view.animation.Interpolator m11269a(android.content.Context context, int i) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.view.animation.AnimationUtils.loadInterpolator(context, i);
        }
        android.content.res.XmlResourceParser xmlResourceParser = null;
        if (i == 17563663) {
            try {
                return new com.fossil.blesdk.obfuscated.C1934hb();
            } catch (org.xmlpull.v1.XmlPullParserException e) {
                android.content.res.Resources.NotFoundException notFoundException = new android.content.res.Resources.NotFoundException("Can't load animation resource ID #0x" + java.lang.Integer.toHexString(i));
                notFoundException.initCause(e);
                throw notFoundException;
            } catch (java.io.IOException e2) {
                android.content.res.Resources.NotFoundException notFoundException2 = new android.content.res.Resources.NotFoundException("Can't load animation resource ID #0x" + java.lang.Integer.toHexString(i));
                notFoundException2.initCause(e2);
                throw notFoundException2;
            } catch (Throwable th) {
                if (xmlResourceParser != null) {
                    xmlResourceParser.close();
                }
                throw th;
            }
        } else if (i == 17563661) {
            return new com.fossil.blesdk.obfuscated.C2009ib();
        } else {
            if (i == 17563662) {
                return new com.fossil.blesdk.obfuscated.C2118jb();
            }
            android.content.res.XmlResourceParser animation = context.getResources().getAnimation(i);
            android.view.animation.Interpolator a = m11270a(context, context.getResources(), context.getTheme(), animation);
            if (animation != null) {
                animation.close();
            }
            return a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.animation.Interpolator m11270a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.view.animation.Interpolator qiVar;
        int depth = xmlPullParser.getDepth();
        android.view.animation.Interpolator interpolator = null;
        while (true) {
            int next = xmlPullParser.next();
            if ((next != 3 || xmlPullParser.getDepth() > depth) && next != 1) {
                if (next == 2) {
                    android.util.AttributeSet asAttributeSet = android.util.Xml.asAttributeSet(xmlPullParser);
                    java.lang.String name = xmlPullParser.getName();
                    if (name.equals("linearInterpolator")) {
                        interpolator = new android.view.animation.LinearInterpolator();
                    } else {
                        if (name.equals("accelerateInterpolator")) {
                            qiVar = new android.view.animation.AccelerateInterpolator(context, asAttributeSet);
                        } else if (name.equals("decelerateInterpolator")) {
                            qiVar = new android.view.animation.DecelerateInterpolator(context, asAttributeSet);
                        } else if (name.equals("accelerateDecelerateInterpolator")) {
                            interpolator = new android.view.animation.AccelerateDecelerateInterpolator();
                        } else if (name.equals("cycleInterpolator")) {
                            qiVar = new android.view.animation.CycleInterpolator(context, asAttributeSet);
                        } else if (name.equals("anticipateInterpolator")) {
                            qiVar = new android.view.animation.AnticipateInterpolator(context, asAttributeSet);
                        } else if (name.equals("overshootInterpolator")) {
                            qiVar = new android.view.animation.OvershootInterpolator(context, asAttributeSet);
                        } else if (name.equals("anticipateOvershootInterpolator")) {
                            qiVar = new android.view.animation.AnticipateOvershootInterpolator(context, asAttributeSet);
                        } else if (name.equals("bounceInterpolator")) {
                            interpolator = new android.view.animation.BounceInterpolator();
                        } else if (name.equals("pathInterpolator")) {
                            qiVar = new com.fossil.blesdk.obfuscated.C2743qi(context, asAttributeSet, xmlPullParser);
                        } else {
                            throw new java.lang.RuntimeException("Unknown interpolator name: " + xmlPullParser.getName());
                        }
                        interpolator = qiVar;
                    }
                }
            }
        }
        return interpolator;
    }
}
