package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yo */
public class C3373yo implements com.fossil.blesdk.obfuscated.C2902so<java.io.InputStream> {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ com.fossil.blesdk.obfuscated.C3373yo.C3375b f11298k; // = new com.fossil.blesdk.obfuscated.C3373yo.C3374a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2340lr f11299e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f11300f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C3373yo.C3375b f11301g;

    @DexIgnore
    /* renamed from: h */
    public java.net.HttpURLConnection f11302h;

    @DexIgnore
    /* renamed from: i */
    public java.io.InputStream f11303i;

    @DexIgnore
    /* renamed from: j */
    public volatile boolean f11304j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yo$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yo$a */
    public static class C3374a implements com.fossil.blesdk.obfuscated.C3373yo.C3375b {
        @DexIgnore
        /* renamed from: a */
        public java.net.HttpURLConnection mo18136a(java.net.URL url) throws java.io.IOException {
            return (java.net.HttpURLConnection) url.openConnection();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.yo$b */
    public interface C3375b {
        @DexIgnore
        /* renamed from: a */
        java.net.HttpURLConnection mo18136a(java.net.URL url) throws java.io.IOException;
    }

    @DexIgnore
    public C3373yo(com.fossil.blesdk.obfuscated.C2340lr lrVar, int i) {
        this(lrVar, i, f11298k);
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m16901b(int i) {
        return i / 100 == 3;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super java.io.InputStream> aVar) {
        java.lang.StringBuilder sb;
        long a = com.fossil.blesdk.obfuscated.C2682pw.m12452a();
        try {
            aVar.mo9252a(mo18135a(this.f11299e.mo13338f(), 0, (java.net.URL) null, this.f11299e.mo13335c()));
            if (android.util.Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new java.lang.StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
                android.util.Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (java.io.IOException e) {
            if (android.util.Log.isLoggable("HttpUrlFetcher", 3)) {
                android.util.Log.d("HttpUrlFetcher", "Failed to load data for url", e);
            }
            aVar.mo9251a((java.lang.Exception) e);
            if (android.util.Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new java.lang.StringBuilder();
            }
        } catch (Throwable th) {
            if (android.util.Log.isLoggable("HttpUrlFetcher", 2)) {
                android.util.Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
            }
            throw th;
        }
    }

    @DexIgnore
    public void cancel() {
        this.f11304j = true;
    }

    @DexIgnore
    public java.lang.Class<java.io.InputStream> getDataClass() {
        return java.io.InputStream.class;
    }

    @DexIgnore
    public C3373yo(com.fossil.blesdk.obfuscated.C2340lr lrVar, int i, com.fossil.blesdk.obfuscated.C3373yo.C3375b bVar) {
        this.f11299e = lrVar;
        this.f11300f = i;
        this.f11301g = bVar;
    }

    @DexIgnore
    /* renamed from: b */
    public com.bumptech.glide.load.DataSource mo8872b() {
        return com.bumptech.glide.load.DataSource.REMOTE;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.io.InputStream mo18135a(java.net.URL url, int i, java.net.URL url2, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new com.bumptech.glide.load.HttpException("In re-direct loop");
                    }
                } catch (java.net.URISyntaxException unused) {
                }
            }
            this.f11302h = this.f11301g.mo18136a(url);
            for (java.util.Map.Entry next : map.entrySet()) {
                this.f11302h.addRequestProperty((java.lang.String) next.getKey(), (java.lang.String) next.getValue());
            }
            this.f11302h.setConnectTimeout(this.f11300f);
            this.f11302h.setReadTimeout(this.f11300f);
            this.f11302h.setUseCaches(false);
            this.f11302h.setDoInput(true);
            this.f11302h.setInstanceFollowRedirects(false);
            this.f11302h.connect();
            this.f11303i = this.f11302h.getInputStream();
            if (this.f11304j) {
                return null;
            }
            int responseCode = this.f11302h.getResponseCode();
            if (m16900a(responseCode)) {
                return mo18134a(this.f11302h);
            }
            if (m16901b(responseCode)) {
                java.lang.String headerField = this.f11302h.getHeaderField("Location");
                if (!android.text.TextUtils.isEmpty(headerField)) {
                    java.net.URL url3 = new java.net.URL(url, headerField);
                    mo8869a();
                    return mo18135a(url3, i + 1, url, map);
                }
                throw new com.bumptech.glide.load.HttpException("Received empty or null redirect url");
            } else if (responseCode == -1) {
                throw new com.bumptech.glide.load.HttpException(responseCode);
            } else {
                throw new com.bumptech.glide.load.HttpException(this.f11302h.getResponseMessage(), responseCode);
            }
        } else {
            throw new com.bumptech.glide.load.HttpException("Too many (> 5) redirects!");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m16900a(int i) {
        return i / 100 == 2;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.io.InputStream mo18134a(java.net.HttpURLConnection httpURLConnection) throws java.io.IOException {
        if (android.text.TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f11303i = com.fossil.blesdk.obfuscated.C2438mw.m10869a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (android.util.Log.isLoggable("HttpUrlFetcher", 3)) {
                android.util.Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f11303i = httpURLConnection.getInputStream();
        }
        return this.f11303i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8869a() {
        java.io.InputStream inputStream = this.f11303i;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (java.io.IOException unused) {
            }
        }
        java.net.HttpURLConnection httpURLConnection = this.f11302h;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.f11302h = null;
    }
}
