package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.ev */
public interface C1749ev {
    @DexIgnore
    /* renamed from: a */
    void mo3915a(android.content.Context context, com.fossil.blesdk.obfuscated.C2815rn rnVar, com.bumptech.glide.Registry registry);
}
