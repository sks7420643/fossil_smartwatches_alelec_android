package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.n13;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m63 extends as2 implements l63, CustomizeWidget.c, ws3.g, View.OnClickListener {
    @DexIgnore
    public k63 k;
    @DexIgnore
    public tr3<id2> l;
    @DexIgnore
    public /* final */ ArrayList<Fragment> m; // = new ArrayList<>();
    @DexIgnore
    public g63 n;
    @DexIgnore
    public MicroAppPresenter o;
    @DexIgnore
    public j42 p;
    @DexIgnore
    public HybridCustomizeViewModel q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ m63 a;

        @DexIgnore
        public b(m63 m63) {
            this.a = m63;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                kd4.a((Object) activity, "it");
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.E;
                kd4.a((Object) stringExtra, "presetId");
                kd4.a((Object) stringExtra2, "microAppPos");
                aVar.a(activity, stringExtra, stringExtra2);
            }
            id2 a2 = this.a.T0().a();
            if (a2 != null) {
                TextView textView = a2.w;
                kd4.a((Object) textView, "binding.tvPresetName");
                textView.setEllipsize(TextUtils.TruncateAt.END);
                a2.w.setSingleLine(true);
                TextView textView2 = a2.w;
                kd4.a((Object) textView2, "binding.tvPresetName");
                textView2.setMaxLines(1);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            id2 a2 = this.a.T0().a();
            if (a2 != null) {
                aq2 aq2 = aq2.a;
                id2 a3 = this.a.T0().a();
                if (a3 != null) {
                    CardView cardView = a3.r;
                    kd4.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    aq2.a((View) cardView);
                    ViewPropertyAnimator animate = a2.z.animate();
                    if (animate != null) {
                        ViewPropertyAnimator duration = animate.setDuration(500);
                        if (duration != null) {
                            duration.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate2 = a2.y.animate();
                    if (animate2 != null) {
                        ViewPropertyAnimator duration2 = animate2.setDuration(500);
                        if (duration2 != null) {
                            duration2.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate3 = a2.x.animate();
                    if (animate3 != null) {
                        ViewPropertyAnimator duration3 = animate3.setDuration(500);
                        if (duration3 != null) {
                            duration3.alpha(1.0f);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ m63 a;

        @DexIgnore
        public d(m63 m63) {
            this.a = m63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.e(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.g(str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.f(ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.U(ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ m63 a;

        @DexIgnore
        public e(m63 m63) {
            this.a = m63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a("middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.e("middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.g(str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.f("middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.U("middle");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ m63 a;

        @DexIgnore
        public f(m63 m63) {
            this.a = m63;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a("bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.e("bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.g(str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.f("bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.U("bottom");
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void D() {
        if (isActive()) {
            tr3<id2> tr3 = this.l;
            if (tr3 != null) {
                id2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.t;
                    if (imageView != null) {
                        imageView.setImageResource(R.drawable.hybrid_default_watch_background);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean S0() {
        k63 k63 = this.k;
        if (k63 != null) {
            k63.h();
            return false;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final tr3<id2> T0() {
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U(String str) {
        kd4.b(str, "position");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.e();
                }
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                a2.A.setDragMode(false);
                g63 g63 = this.n;
                if (g63 != null) {
                    g63.T0();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.n = (g63) getChildFragmentManager().a("MicroAppsFragment");
        if (this.n == null) {
            this.n = new g63();
        }
        g63 g63 = this.n;
        if (g63 != null) {
            this.m.add(g63);
        }
        l42 g = PortfolioApp.W.c().g();
        g63 g632 = this.n;
        if (g632 != null) {
            g.a(new d63(g632)).a(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppContract.View");
    }

    @DexIgnore
    public final void V(String str) {
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.setSelectedWc(true);
                            a2.B.setSelectedWc(false);
                            a2.A.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.C.setSelectedWc(false);
                        a2.B.setSelectedWc(true);
                        a2.A.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.C.setSelectedWc(false);
                    a2.B.setSelectedWc(false);
                    a2.A.setSelectedWc(true);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V0() {
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.C;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                kd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_WATCH_APP", putExtra, new n13(new d(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.B;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", "middle");
                kd4.a((Object) putExtra2, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_WATCH_APP", putExtra2, new n13(new e(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.A;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "bottom");
                kd4.a((Object) putExtra3, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_WATCH_APP", putExtra3, new n13(new f(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2, String str3) {
        kd4.b(str, "message");
        kd4.b(str2, "microAppId");
        kd4.b(str3, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        kd4.b(str, "position");
        kd4.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.d();
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void f(String str, String str2) {
        kd4.b(str, "position");
        kd4.b(str2, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.A.e();
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void g(String str, String str2) {
        kd4.b(str, "fromPosition");
        kd4.b(str2, "toPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.C.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.B.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.A.e();
                }
                a2.C.setDragMode(false);
                a2.B.setDragMode(false);
                a2.A.setDragMode(false);
                k63 k63 = this.k;
                if (k63 != null) {
                    k63.b(str, str2);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void l() {
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        S(a2);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void n(String str) {
        kd4.b(str, "buttonsPosition");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
        V(str);
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HybridCustomizeEditActivity hybridCustomizeEditActivity = (HybridCustomizeEditActivity) activity;
            j42 j42 = this.p;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) hybridCustomizeEditActivity, (kc.b) j42).a(HybridCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.q = (HybridCustomizeViewModel) a2;
                k63 k63 = this.k;
                if (k63 != null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.q;
                    if (hybridCustomizeViewModel != null) {
                        k63.a(hybridCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.ftv_set_to_watch) {
                k63 k63 = this.k;
                if (k63 != null) {
                    k63.i();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else if (id != R.id.tv_cancel) {
                switch (id) {
                    case R.id.wa_bottom /*2131363027*/:
                        k63 k632 = this.k;
                        if (k632 != null) {
                            k632.a("bottom");
                            return;
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    case R.id.wa_middle /*2131363028*/:
                        k63 k633 = this.k;
                        if (k633 != null) {
                            k633.a("middle");
                            return;
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    case R.id.wa_top /*2131363029*/:
                        k63 k634 = this.k;
                        if (k634 != null) {
                            k634.a(ViewHierarchy.DIMENSION_TOP_KEY);
                            return;
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    default:
                        return;
                }
            } else {
                k63 k635 = this.k;
                if (k635 != null) {
                    k635.h();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        id2 id2 = (id2) qa.a(layoutInflater, R.layout.fragment_hybrid_customize_edit, viewGroup, false, O0());
        U0();
        this.l = new tr3<>(this, id2);
        kd4.a((Object) id2, "binding");
        return id2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        k63 k63 = this.k;
        if (k63 != null) {
            k63.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        k63 k63 = this.k;
        if (k63 != null) {
            k63.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            kd4.a((Object) window, "activity.window");
            window.setEnterTransition(aq2.a.a());
            Window window2 = activity.getWindow();
            kd4.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(aq2.a.a(PortfolioApp.W.c()));
            Intent intent = activity.getIntent();
            kd4.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new zp2(intent, PortfolioApp.W.c()));
            a(activity);
        }
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                a2.C.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.A.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                a2.s.setOnClickListener(this);
                RecyclerViewPager recyclerViewPager = a2.u;
                kd4.a((Object) recyclerViewPager, "it.rvPreset");
                recyclerViewPager.setAdapter(new cu3(getChildFragmentManager(), this.m));
                a2.u.setItemViewCacheSize(2);
                a2.u.a((RecyclerView.p) new c());
            }
            V0();
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p() {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_Text__SaveChangesToThePresetBefore));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Discard));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public void q() {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        kd4.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public final boolean a(String str, View view, String str2) {
        kd4.b(str, "position");
        kd4.b(view, "view");
        kd4.b(str2, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 == null) {
                return true;
            }
            int hashCode = str.hashCode();
            if (hashCode != -1383228885) {
                if (hashCode != -1074341483) {
                    if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.C.e();
                    }
                } else if (str.equals("middle")) {
                    a2.B.e();
                }
            } else if (str.equals("bottom")) {
                a2.A.e();
            }
            a2.C.setDragMode(false);
            a2.B.setDragMode(false);
            a2.A.setDragMode(false);
            g63 g63 = this.n;
            if (g63 != null) {
                g63.T0();
            }
            k63 k63 = this.k;
            if (k63 != null) {
                k63.a(str2, str);
                return true;
            }
            kd4.d("mPresenter");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2) {
        kd4.b(str, "microAppId");
        kd4.b(str2, "microAppPos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str);
            bundle.putString("TO_POS", str2);
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
            pd4 pd4 = pd4.a;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_to_watch_fail_app_permission);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026atch_fail_app_permission)");
            Object[] objArr = {str, str2};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a((int) R.id.tv_description, format);
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_PERMISSION", bundle);
        }
    }

    @DexIgnore
    public void f(boolean z) {
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.s;
                kd4.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.s;
                kd4.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.s.setBackgroundResource(R.drawable.preset_saved);
                return;
            }
            ImageButton imageButton3 = a2.s;
            kd4.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.s;
            kd4.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.s.setBackgroundResource(R.drawable.preset_unsaved);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g(boolean z) {
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                TextView textView = a2.w;
                if (textView != null) {
                    textView.setSingleLine(false);
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                if (z) {
                    activity.setResult(-1);
                } else {
                    activity.setResult(0);
                }
                activity.supportFinishAfterTransition();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(g13 g13) {
        kd4.b(g13, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "showCurrentPreset  microApps=" + g13.a());
        tr3<id2> tr3 = this.l;
        if (tr3 != null) {
            id2 a2 = tr3.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(g13.a());
                TextView textView = a2.w;
                kd4.a((Object) textView, "it.tvPresetName");
                textView.setText(g13.c());
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    h13 h13 = (h13) it.next();
                    String a3 = h13.a();
                    String b2 = h13.b();
                    String c2 = h13.c();
                    if (c2 != null) {
                        int hashCode = c2.hashCode();
                        if (hashCode != -1383228885) {
                            if (hashCode != -1074341483) {
                                if (hashCode == 115029 && c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    FlexibleTextView flexibleTextView = a2.z;
                                    kd4.a((Object) flexibleTextView, "it.tvWaTop");
                                    flexibleTextView.setText(b2);
                                    a2.C.c(a3);
                                    a2.C.h();
                                    CustomizeWidget customizeWidget = a2.C;
                                    kd4.a((Object) customizeWidget, "it.waTop");
                                    a(customizeWidget, a3);
                                }
                            } else if (c2.equals("middle")) {
                                FlexibleTextView flexibleTextView2 = a2.y;
                                kd4.a((Object) flexibleTextView2, "it.tvWaMiddle");
                                flexibleTextView2.setText(b2);
                                a2.B.c(a3);
                                a2.B.h();
                                CustomizeWidget customizeWidget2 = a2.B;
                                kd4.a((Object) customizeWidget2, "it.waMiddle");
                                a(customizeWidget2, a3);
                            }
                        } else if (c2.equals("bottom")) {
                            FlexibleTextView flexibleTextView3 = a2.x;
                            kd4.a((Object) flexibleTextView3, "it.tvWaBottom");
                            flexibleTextView3.setText(b2);
                            a2.A.c(a3);
                            a2.A.h();
                            CustomizeWidget customizeWidget3 = a2.A;
                            kd4.a((Object) customizeWidget3, "it.waBottom");
                            a(customizeWidget3, a3);
                        }
                    }
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(k63 k63) {
        kd4.b(k63, "presenter");
        this.k = k63;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode != 291193711 || !str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING")) {
                    return;
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
                return;
            } else {
                if (i == R.id.tv_cancel) {
                    g(false);
                    return;
                } else if (i == R.id.tv_ok) {
                    k63 k63 = this.k;
                    if (k63 != null) {
                        k63.i();
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
        } else if (!str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION")) {
            return;
        }
        if (i == R.id.tv_ok && intent != null) {
            String stringExtra = intent.getStringExtra("TO_POS");
            String stringExtra2 = intent.getStringExtra("TO_ID");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
            if (kd4.a((Object) stringExtra2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                bn2 bn2 = bn2.d;
                Context context = getContext();
                if (context != null) {
                    bn2.b(context, stringExtra2);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                k63 k632 = this.k;
                if (k632 != null) {
                    kd4.a((Object) stringExtra, "toPos");
                    k632.a(stringExtra);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
        }
    }
}
