package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk2 implements jo {
    @DexIgnore
    public /* final */ List<BaseFeatureModel> b;

    @DexIgnore
    public hk2(List<? extends BaseFeatureModel> list) {
        this.b = list;
    }

    @DexIgnore
    public final List<BaseFeatureModel> a() {
        return this.b;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        kd4.b(messageDigest, "messageDigest");
        List<BaseFeatureModel> list = this.b;
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (BaseFeatureModel next : list) {
                if (next instanceof AppFilter) {
                    sb.append(((AppFilter) next).getType());
                    kd4.a((Object) sb, "id.append(item.type)");
                } else if (next != null) {
                    List<Contact> contacts = ((ContactGroup) next).getContacts();
                    if (contacts != null && (!contacts.isEmpty())) {
                        Contact contact = contacts.get(0);
                        kd4.a((Object) contact, "contactList[0]");
                        sb.append(contact.getFirstName());
                        Contact contact2 = contacts.get(0);
                        kd4.a((Object) contact2, "contactList[0]");
                        sb.append(contact2.getLastName());
                        Contact contact3 = contacts.get(0);
                        kd4.a((Object) contact3, "contactList[0]");
                        sb.append(contact3.getPhotoThumbUri());
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type com.fossil.wearables.fsl.contact.ContactGroup");
                }
            }
        }
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "id.toString()");
        Charset charset = jo.a;
        kd4.a((Object) charset, "Key.CHARSET");
        if (sb2 != null) {
            byte[] bytes = sb2.getBytes(charset);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
