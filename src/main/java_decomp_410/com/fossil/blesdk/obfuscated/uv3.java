package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.Protocol;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uv3 {
    @DexIgnore
    public static /* final */ uv3 a; // = b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements InvocationHandler {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public String c;

        @DexIgnore
        public c(List<String> list) {
            this.a = list;
        }

        @DexIgnore
        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Class<?> returnType = method.getReturnType();
            if (objArr == null) {
                objArr = wv3.b;
            }
            if (name.equals("supports") && Boolean.TYPE == returnType) {
                return true;
            }
            if (name.equals("unsupported") && Void.TYPE == returnType) {
                this.b = true;
                return null;
            } else if (name.equals("protocols") && objArr.length == 0) {
                return this.a;
            } else {
                if ((name.equals("selectProtocol") || name.equals("select")) && String.class == returnType && objArr.length == 1 && (objArr[0] instanceof List)) {
                    List list = (List) objArr[0];
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        if (this.a.contains(list.get(i))) {
                            String str = (String) list.get(i);
                            this.c = str;
                            return str;
                        }
                    }
                    String str2 = this.a.get(0);
                    this.c = str2;
                    return str2;
                } else if ((!name.equals("protocolSelected") && !name.equals("selected")) || objArr.length != 1) {
                    return method.invoke(this, objArr);
                } else {
                    this.c = (String) objArr[0];
                    return null;
                }
            }
        }
    }

    @DexIgnore
    public static uv3 b() {
        tv3 tv3;
        Method method;
        Method method2;
        Class<byte[]> cls = byte[].class;
        try {
            Class.forName("com.android.org.conscrypt.OpenSSLSocketImpl");
        } catch (ClassNotFoundException unused) {
            try {
                Class.forName("org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl");
            } catch (ClassNotFoundException unused2) {
                try {
                    Class<?> cls2 = Class.forName("org.eclipse.jetty.alpn.ALPN");
                    Class<?> cls3 = Class.forName("org.eclipse.jetty.alpn.ALPN" + "$Provider");
                    return new b(cls2.getMethod("put", new Class[]{SSLSocket.class, cls3}), cls2.getMethod("get", new Class[]{SSLSocket.class}), cls2.getMethod("remove", new Class[]{SSLSocket.class}), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ClientProvider"), Class.forName("org.eclipse.jetty.alpn.ALPN" + "$ServerProvider"));
                } catch (ClassNotFoundException | NoSuchMethodException unused3) {
                    return new uv3();
                }
            }
        }
        tv3 tv32 = null;
        tv3 tv33 = new tv3((Class<?>) null, "setUseSessionTickets", Boolean.TYPE);
        tv3 tv34 = new tv3((Class<?>) null, "setHostname", String.class);
        try {
            Class<?> cls4 = Class.forName("android.net.TrafficStats");
            method = cls4.getMethod("tagSocket", new Class[]{Socket.class});
            try {
                method2 = cls4.getMethod("untagSocket", new Class[]{Socket.class});
                try {
                    Class.forName("android.net.Network");
                    tv3 = new tv3(cls, "getAlpnSelectedProtocol", new Class[0]);
                    try {
                        tv32 = new tv3((Class<?>) null, "setAlpnProtocols", cls);
                    } catch (ClassNotFoundException | NoSuchMethodException unused4) {
                    }
                } catch (ClassNotFoundException | NoSuchMethodException unused5) {
                    tv3 = null;
                }
            } catch (ClassNotFoundException | NoSuchMethodException unused6) {
                method2 = null;
                tv3 = null;
            }
        } catch (ClassNotFoundException | NoSuchMethodException unused7) {
            method2 = null;
            method = null;
            tv3 = null;
        }
        return new a(tv33, tv34, method, method2, tv3, tv32);
    }

    @DexIgnore
    public static uv3 c() {
        return a;
    }

    @DexIgnore
    public String a() {
        return "OkHttp";
    }

    @DexIgnore
    public void a(String str) {
        System.out.println(str);
    }

    @DexIgnore
    public void a(Socket socket) throws SocketException {
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket) {
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
    }

    @DexIgnore
    public String b(SSLSocket sSLSocket) {
        return null;
    }

    @DexIgnore
    public void b(Socket socket) throws SocketException {
    }

    @DexIgnore
    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    @DexIgnore
    public static byte[] a(List<Protocol> list) {
        jo4 jo4 = new jo4();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Protocol protocol = list.get(i);
            if (protocol != Protocol.HTTP_1_0) {
                jo4.writeByte(protocol.toString().length());
                jo4.a(protocol.toString());
            }
        }
        return jo4.f();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends uv3 {
        @DexIgnore
        public /* final */ tv3<Socket> b;
        @DexIgnore
        public /* final */ tv3<Socket> c;
        @DexIgnore
        public /* final */ Method d;
        @DexIgnore
        public /* final */ Method e;
        @DexIgnore
        public /* final */ tv3<Socket> f;
        @DexIgnore
        public /* final */ tv3<Socket> g;

        @DexIgnore
        public a(tv3<Socket> tv3, tv3<Socket> tv32, Method method, Method method2, tv3<Socket> tv33, tv3<Socket> tv34) {
            this.b = tv3;
            this.c = tv32;
            this.d = method;
            this.e = method2;
            this.f = tv33;
            this.g = tv34;
        }

        @DexIgnore
        public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
            try {
                socket.connect(inetSocketAddress, i);
            } catch (SecurityException e2) {
                IOException iOException = new IOException("Exception in connect");
                iOException.initCause(e2);
                throw iOException;
            }
        }

        @DexIgnore
        public String b(SSLSocket sSLSocket) {
            tv3<Socket> tv3 = this.f;
            if (tv3 == null || !tv3.a(sSLSocket)) {
                return null;
            }
            byte[] bArr = (byte[]) this.f.d(sSLSocket, new Object[0]);
            if (bArr != null) {
                return new String(bArr, wv3.c);
            }
            return null;
        }

        @DexIgnore
        public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
            if (str != null) {
                this.b.c(sSLSocket, true);
                this.c.c(sSLSocket, str);
            }
            tv3<Socket> tv3 = this.g;
            if (tv3 != null && tv3.a(sSLSocket)) {
                this.g.d(sSLSocket, uv3.a(list));
            }
        }

        @DexIgnore
        public void b(Socket socket) throws SocketException {
            Method method = this.e;
            if (method != null) {
                try {
                    method.invoke((Object) null, new Object[]{socket});
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                } catch (InvocationTargetException e3) {
                    throw new RuntimeException(e3.getCause());
                }
            }
        }

        @DexIgnore
        public void a(Socket socket) throws SocketException {
            Method method = this.d;
            if (method != null) {
                try {
                    method.invoke((Object) null, new Object[]{socket});
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                } catch (InvocationTargetException e3) {
                    throw new RuntimeException(e3.getCause());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends uv3 {
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;
        @DexIgnore
        public /* final */ Method d;
        @DexIgnore
        public /* final */ Class<?> e;
        @DexIgnore
        public /* final */ Class<?> f;

        @DexIgnore
        public b(Method method, Method method2, Method method3, Class<?> cls, Class<?> cls2) {
            this.b = method;
            this.c = method2;
            this.d = method3;
            this.e = cls;
            this.f = cls2;
        }

        @DexIgnore
        public void a(SSLSocket sSLSocket, String str, List<Protocol> list) {
            ArrayList arrayList = new ArrayList(list.size());
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Protocol protocol = list.get(i);
                if (protocol != Protocol.HTTP_1_0) {
                    arrayList.add(protocol.toString());
                }
            }
            try {
                Object newProxyInstance = Proxy.newProxyInstance(uv3.class.getClassLoader(), new Class[]{this.e, this.f}, new c(arrayList));
                this.b.invoke((Object) null, new Object[]{sSLSocket, newProxyInstance});
            } catch (IllegalAccessException | InvocationTargetException e2) {
                throw new AssertionError(e2);
            }
        }

        @DexIgnore
        public String b(SSLSocket sSLSocket) {
            try {
                c cVar = (c) Proxy.getInvocationHandler(this.c.invoke((Object) null, new Object[]{sSLSocket}));
                if (!cVar.b && cVar.c == null) {
                    pv3.a.log(Level.INFO, "ALPN callback dropped: SPDY and HTTP/2 are disabled. Is alpn-boot on the boot class path?");
                    return null;
                } else if (cVar.b) {
                    return null;
                } else {
                    return cVar.c;
                }
            } catch (IllegalAccessException | InvocationTargetException unused) {
                throw new AssertionError();
            }
        }

        @DexIgnore
        public void a(SSLSocket sSLSocket) {
            try {
                this.d.invoke((Object) null, new Object[]{sSLSocket});
            } catch (IllegalAccessException | InvocationTargetException unused) {
                throw new AssertionError();
            }
        }
    }
}
