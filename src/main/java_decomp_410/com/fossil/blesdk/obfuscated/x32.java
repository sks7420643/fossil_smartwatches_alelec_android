package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.u32;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.CharacterSetECI;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.decoder.Mode;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x32 {
    @DexIgnore
    public static /* final */ int[] a; // = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Mode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[Mode.NUMERIC.ordinal()] = 1;
            a[Mode.ALPHANUMERIC.ordinal()] = 2;
            a[Mode.BYTE.ordinal()] = 3;
            try {
                a[Mode.KANJI.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public static int a(w32 w32) {
        return y32.a(w32) + y32.b(w32) + y32.c(w32) + y32.d(w32);
    }

    @DexIgnore
    public static void b(CharSequence charSequence, a22 a22) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - '0';
            int i2 = i + 2;
            if (i2 < length) {
                a22.a((charAt * 100) + ((charSequence.charAt(i + 1) - '0') * 10) + (charSequence.charAt(i2) - '0'), 10);
                i += 3;
            } else {
                i++;
                if (i < length) {
                    a22.a((charAt * 10) + (charSequence.charAt(i) - '0'), 7);
                    i = i2;
                } else {
                    a22.a(charAt, 4);
                }
            }
        }
    }

    @DexIgnore
    public static a42 a(String str, ErrorCorrectionLevel errorCorrectionLevel, Map<EncodeHintType, ?> map) throws WriterException {
        String str2;
        u32 u32;
        if (map == null || !map.containsKey(EncodeHintType.CHARACTER_SET)) {
            str2 = "ISO-8859-1";
        } else {
            str2 = map.get(EncodeHintType.CHARACTER_SET).toString();
        }
        Mode a2 = a(str, str2);
        a22 a22 = new a22();
        if (a2 == Mode.BYTE && !"ISO-8859-1".equals(str2)) {
            CharacterSetECI characterSetECIByName = CharacterSetECI.getCharacterSetECIByName(str2);
            if (characterSetECIByName != null) {
                a(characterSetECIByName, a22);
            }
        }
        a(a2, a22);
        a22 a222 = new a22();
        a(str, a2, a222, str2);
        if (map == null || !map.containsKey(EncodeHintType.QR_VERSION)) {
            u32 = a(errorCorrectionLevel, a2, a22, a222);
        } else {
            u32 = u32.a(Integer.parseInt(map.get(EncodeHintType.QR_VERSION).toString()));
            if (!a(a(a2, a22, a222, u32), u32, errorCorrectionLevel)) {
                throw new WriterException("Data too big for requested version");
            }
        }
        a22 a223 = new a22();
        a223.a(a22);
        a(a2 == Mode.BYTE ? a222.b() : str.length(), u32, a2, a223);
        a223.a(a222);
        u32.b a3 = u32.a(errorCorrectionLevel);
        int b = u32.b() - a3.d();
        a(b, a223);
        a22 a4 = a(a223, u32.b(), b, a3.c());
        a42 a42 = new a42();
        a42.a(errorCorrectionLevel);
        a42.a(a2);
        a42.a(u32);
        int a5 = u32.a();
        w32 w32 = new w32(a5, a5);
        int a6 = a(a4, errorCorrectionLevel, u32, w32);
        a42.a(a6);
        z32.a(a4, errorCorrectionLevel, u32, a6, w32);
        a42.a(w32);
        return a42;
    }

    @DexIgnore
    public static u32 a(ErrorCorrectionLevel errorCorrectionLevel, Mode mode, a22 a22, a22 a222) throws WriterException {
        return a(a(mode, a22, a222, a(a(mode, a22, a222, u32.a(1)), errorCorrectionLevel)), errorCorrectionLevel);
    }

    @DexIgnore
    public static int a(Mode mode, a22 a22, a22 a222, u32 u32) {
        return a22.a() + mode.getCharacterCountBits(u32) + a222.a();
    }

    @DexIgnore
    public static int a(int i) {
        int[] iArr = a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    @DexIgnore
    public static Mode a(String str, String str2) {
        if ("Shift_JIS".equals(str2) && a(str)) {
            return Mode.KANJI;
        }
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                z2 = true;
            } else if (a((int) charAt) == -1) {
                return Mode.BYTE;
            } else {
                z = true;
            }
        }
        if (z) {
            return Mode.ALPHANUMERIC;
        }
        if (z2) {
            return Mode.NUMERIC;
        }
        return Mode.BYTE;
    }

    @DexIgnore
    public static boolean a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                byte b = bytes[i] & FileType.MASKED_INDEX;
                if ((b < 129 || b > 159) && (b < 224 || b > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    @DexIgnore
    public static int a(a22 a22, ErrorCorrectionLevel errorCorrectionLevel, u32 u32, w32 w32) throws WriterException {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        for (int i3 = 0; i3 < 8; i3++) {
            z32.a(a22, errorCorrectionLevel, u32, i3, w32);
            int a2 = a(w32);
            if (a2 < i) {
                i2 = i3;
                i = a2;
            }
        }
        return i2;
    }

    @DexIgnore
    public static u32 a(int i, ErrorCorrectionLevel errorCorrectionLevel) throws WriterException {
        for (int i2 = 1; i2 <= 40; i2++) {
            u32 a2 = u32.a(i2);
            if (a(i, a2, errorCorrectionLevel)) {
                return a2;
            }
        }
        throw new WriterException("Data too big");
    }

    @DexIgnore
    public static boolean a(int i, u32 u32, ErrorCorrectionLevel errorCorrectionLevel) {
        return u32.b() - u32.a(errorCorrectionLevel).d() >= (i + 7) / 8;
    }

    @DexIgnore
    public static void a(int i, a22 a22) throws WriterException {
        int i2 = i << 3;
        if (a22.a() <= i2) {
            for (int i3 = 0; i3 < 4 && a22.a() < i2; i3++) {
                a22.a(false);
            }
            int a2 = a22.a() & 7;
            if (a2 > 0) {
                while (a2 < 8) {
                    a22.a(false);
                    a2++;
                }
            }
            int b = i - a22.b();
            for (int i4 = 0; i4 < b; i4++) {
                a22.a((i4 & 1) == 0 ? 236 : 17, 8);
            }
            if (a22.a() != i2) {
                throw new WriterException("Bits size does not equal capacity");
            }
            return;
        }
        throw new WriterException("data bits cannot fit in the QR Code" + a22.a() + " > " + i2);
    }

    @DexIgnore
    public static void a(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) throws WriterException {
        if (i4 < i3) {
            int i5 = i % i3;
            int i6 = i3 - i5;
            int i7 = i / i3;
            int i8 = i7 + 1;
            int i9 = i2 / i3;
            int i10 = i9 + 1;
            int i11 = i7 - i9;
            int i12 = i8 - i10;
            if (i11 != i12) {
                throw new WriterException("EC bytes mismatch");
            } else if (i3 != i6 + i5) {
                throw new WriterException("RS blocks mismatch");
            } else if (i != ((i9 + i11) * i6) + ((i10 + i12) * i5)) {
                throw new WriterException("Total bytes mismatch");
            } else if (i4 < i6) {
                iArr[0] = i9;
                iArr2[0] = i11;
            } else {
                iArr[0] = i10;
                iArr2[0] = i12;
            }
        } else {
            throw new WriterException("Block ID too large");
        }
    }

    @DexIgnore
    public static a22 a(a22 a22, int i, int i2, int i3) throws WriterException {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (a22.b() == i5) {
            ArrayList<v32> arrayList = new ArrayList<>(i6);
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            for (int i10 = 0; i10 < i6; i10++) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                a(i, i2, i3, i10, iArr, iArr2);
                int i11 = iArr[0];
                byte[] bArr = new byte[i11];
                a22.a(i7 << 3, bArr, 0, i11);
                byte[] a2 = a(bArr, iArr2[0]);
                arrayList.add(new v32(bArr, a2));
                i8 = Math.max(i8, i11);
                i9 = Math.max(i9, a2.length);
                i7 += iArr[0];
            }
            if (i5 == i7) {
                a22 a222 = new a22();
                for (int i12 = 0; i12 < i8; i12++) {
                    for (v32 a3 : arrayList) {
                        byte[] a4 = a3.a();
                        if (i12 < a4.length) {
                            a222.a(a4[i12], 8);
                        }
                    }
                }
                for (int i13 = 0; i13 < i9; i13++) {
                    for (v32 b : arrayList) {
                        byte[] b2 = b.b();
                        if (i13 < b2.length) {
                            a222.a(b2[i13], 8);
                        }
                    }
                }
                if (i4 == a222.b()) {
                    return a222;
                }
                throw new WriterException("Interleaving error: " + i4 + " and " + a222.b() + " differ.");
            }
            throw new WriterException("Data bytes does not match offset");
        }
        throw new WriterException("Number of bits and data bytes does not match");
    }

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & FileType.MASKED_INDEX;
        }
        new e22(c22.k).a(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) iArr[length + i3];
        }
        return bArr2;
    }

    @DexIgnore
    public static void a(Mode mode, a22 a22) {
        a22.a(mode.getBits(), 4);
    }

    @DexIgnore
    public static void a(int i, u32 u32, Mode mode, a22 a22) throws WriterException {
        int characterCountBits = mode.getCharacterCountBits(u32);
        int i2 = 1 << characterCountBits;
        if (i < i2) {
            a22.a(i, characterCountBits);
            return;
        }
        throw new WriterException(i + " is bigger than " + (i2 - 1));
    }

    @DexIgnore
    public static void a(String str, Mode mode, a22 a22, String str2) throws WriterException {
        int i = a.a[mode.ordinal()];
        if (i == 1) {
            b(str, a22);
        } else if (i == 2) {
            a((CharSequence) str, a22);
        } else if (i == 3) {
            a(str, a22, str2);
        } else if (i == 4) {
            a(str, a22);
        } else {
            throw new WriterException("Invalid mode: " + mode);
        }
    }

    @DexIgnore
    public static void a(CharSequence charSequence, a22 a22) throws WriterException {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int a2 = a((int) charSequence.charAt(i));
            if (a2 != -1) {
                int i2 = i + 1;
                if (i2 < length) {
                    int a3 = a((int) charSequence.charAt(i2));
                    if (a3 != -1) {
                        a22.a((a2 * 45) + a3, 11);
                        i += 2;
                    } else {
                        throw new WriterException();
                    }
                } else {
                    a22.a(a2, 6);
                    i = i2;
                }
            } else {
                throw new WriterException();
            }
        }
    }

    @DexIgnore
    public static void a(String str, a22 a22, String str2) throws WriterException {
        try {
            for (byte a2 : str.getBytes(str2)) {
                a22.a(a2, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new WriterException((Throwable) e);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[LOOP:0: B:4:0x0008->B:17:0x0035, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC] */
    public static void a(String str, a22 a22) throws WriterException {
        int i;
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            int i2 = 0;
            while (i2 < length) {
                byte b = ((bytes[i2] & FileType.MASKED_INDEX) << 8) | (bytes[i2 + 1] & FileType.MASKED_INDEX);
                byte b2 = 33088;
                if (b < 33088 || b > 40956) {
                    if (b < 57408 || b > 60351) {
                        i = -1;
                        if (i == -1) {
                            a22.a(((i >> 8) * 192) + (i & 255), 13);
                            i2 += 2;
                        } else {
                            throw new WriterException("Invalid byte sequence");
                        }
                    } else {
                        b2 = 49472;
                    }
                }
                i = b - b2;
                if (i == -1) {
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new WriterException((Throwable) e);
        }
    }

    @DexIgnore
    public static void a(CharacterSetECI characterSetECI, a22 a22) {
        a22.a(Mode.ECI.getBits(), 4);
        a22.a(characterSetECI.getValue(), 8);
    }
}
