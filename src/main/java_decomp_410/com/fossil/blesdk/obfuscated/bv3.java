package com.fossil.blesdk.obfuscated;

import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bv3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ List<Certificate> b;
    @DexIgnore
    public /* final */ List<Certificate> c;

    @DexIgnore
    public bv3(String str, List<Certificate> list, List<Certificate> list2) {
        this.a = str;
        this.b = list;
        this.c = list2;
    }

    @DexIgnore
    public static bv3 a(SSLSession sSLSession) {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite != null) {
            try {
                certificateArr = sSLSession.getPeerCertificates();
            } catch (SSLPeerUnverifiedException unused) {
                certificateArr = null;
            }
            if (certificateArr != null) {
                list = wv3.a((T[]) certificateArr);
            } else {
                list = Collections.emptyList();
            }
            Certificate[] localCertificates = sSLSession.getLocalCertificates();
            if (localCertificates != null) {
                list2 = wv3.a((T[]) localCertificates);
            } else {
                list2 = Collections.emptyList();
            }
            return new bv3(cipherSuite, list, list2);
        }
        throw new IllegalStateException("cipherSuite == null");
    }

    @DexIgnore
    public List<Certificate> b() {
        return this.c;
    }

    @DexIgnore
    public List<Certificate> c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof bv3)) {
            return false;
        }
        bv3 bv3 = (bv3) obj;
        if (!this.a.equals(bv3.a) || !this.b.equals(bv3.b) || !this.c.equals(bv3.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public static bv3 a(String str, List<Certificate> list, List<Certificate> list2) {
        if (str != null) {
            return new bv3(str, wv3.a(list), wv3.a(list2));
        }
        throw new IllegalArgumentException("cipherSuite == null");
    }

    @DexIgnore
    public String a() {
        return this.a;
    }
}
