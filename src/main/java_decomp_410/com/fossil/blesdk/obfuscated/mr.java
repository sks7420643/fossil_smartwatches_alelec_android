package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.or;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mr {
    @DexIgnore
    public static final mr a = new or.a().a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mr {
        @DexIgnore
        public Map<String, String> a() {
            return Collections.emptyMap();
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    Map<String, String> a();
}
