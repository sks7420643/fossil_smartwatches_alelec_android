package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class si1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ long h;
    @DexIgnore
    public /* final */ /* synthetic */ zh1 i;

    @DexIgnore
    public si1(zh1 zh1, String str, String str2, String str3, long j) {
        this.i = zh1;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = j;
    }

    @DexIgnore
    public final void run() {
        String str = this.e;
        if (str == null) {
            this.i.e.B().n().a(this.f, (qj1) null);
            return;
        }
        this.i.e.B().n().a(this.f, new qj1(this.g, str, this.h));
    }
}
