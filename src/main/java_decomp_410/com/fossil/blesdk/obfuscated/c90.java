package com.fossil.blesdk.obfuscated;

import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c90 {
    @DexIgnore
    public static /* final */ Hashtable<String, b90> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ c90 b; // = new c90();

    @DexIgnore
    public final void a(String str, byte[] bArr) {
        kd4.b(str, "macAddress");
        synchronized (a) {
            b.a(str).a(bArr);
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public final void a(String str, byte[] bArr, byte[] bArr2) {
        kd4.b(str, "macAddress");
        kd4.b(bArr, "phoneRandomNumber");
        kd4.b(bArr2, "deviceRandomNumber");
        synchronized (a) {
            b.a(str).a(bArr, bArr2);
            qa4 qa4 = qa4.a;
        }
    }

    @DexIgnore
    public final b90 a(String str) {
        b90 b90;
        kd4.b(str, "macAddress");
        synchronized (a) {
            b90 = a.get(str);
            if (b90 == null) {
                b90 = new b90();
            }
            a.put(str, b90);
        }
        return b90;
    }
}
