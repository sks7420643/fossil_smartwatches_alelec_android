package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n11 implements uo0 {
    @DexIgnore
    public final he0<Status> a(ge0 ge0, DataSet dataSet) {
        bk0.a(dataSet, (Object) "Must set the data set");
        bk0.b(!dataSet.I().isEmpty(), "Cannot use an empty data set");
        bk0.a(dataSet.J().Q(), (Object) "Must set the app package name for the data source");
        return ge0.a(new o11(this, ge0, dataSet, false));
    }
}
