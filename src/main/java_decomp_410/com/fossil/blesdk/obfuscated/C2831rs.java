package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rs */
public class C2831rs implements com.fossil.blesdk.obfuscated.C2427mo<java.nio.ByteBuffer, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3240ws f9115a;

    @DexIgnore
    public C2831rs(com.fossil.blesdk.obfuscated.C3240ws wsVar) {
        this.f9115a = wsVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.nio.ByteBuffer byteBuffer, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f9115a.mo17523a(byteBuffer);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(java.nio.ByteBuffer byteBuffer, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return this.f9115a.mo17518a(com.fossil.blesdk.obfuscated.C2255kw.m9874c(byteBuffer), i, i2, loVar);
    }
}
