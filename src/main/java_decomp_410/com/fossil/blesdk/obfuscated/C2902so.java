package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.so */
public interface C2902so<T> {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.so$a */
    public interface C2903a<T> {
        @DexIgnore
        /* renamed from: a */
        void mo9251a(java.lang.Exception exc);

        @DexIgnore
        /* renamed from: a */
        void mo9252a(T t);
    }

    @DexIgnore
    /* renamed from: a */
    void mo8869a();

    @DexIgnore
    /* renamed from: a */
    void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super T> aVar);

    @DexIgnore
    /* renamed from: b */
    com.bumptech.glide.load.DataSource mo8872b();

    @DexIgnore
    void cancel();

    @DexIgnore
    java.lang.Class<T> getDataClass();
}
