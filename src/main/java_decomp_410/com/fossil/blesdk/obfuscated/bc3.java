package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bc3 implements Factory<DashboardHeartRatePresenter> {
    @DexIgnore
    public static DashboardHeartRatePresenter a(zb3 zb3, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, h42 h42) {
        return new DashboardHeartRatePresenter(zb3, heartRateSummaryRepository, fitnessDataRepository, heartRateDailySummaryDao, fitnessDatabase, userRepository, h42);
    }
}
