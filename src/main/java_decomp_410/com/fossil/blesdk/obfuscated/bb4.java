package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bb4 {
    @DexIgnore
    public static final <T> List<T> a(T t) {
        List<T> singletonList = Collections.singletonList(t);
        kd4.a((Object) singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }

    @DexIgnore
    public static final <T> Object[] a(T[] tArr, boolean z) {
        Class<Object[]> cls = Object[].class;
        kd4.b(tArr, "$this$copyToArrayOfAny");
        if (z && kd4.a((Object) tArr.getClass(), (Object) cls)) {
            return tArr;
        }
        Object[] copyOf = Arrays.copyOf(tArr, tArr.length, cls);
        kd4.a((Object) copyOf, "java.util.Arrays.copyOf(\u2026 Array<Any?>::class.java)");
        return copyOf;
    }
}
