package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.device.data.file.FileType;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fo {
    @DexIgnore
    public /* final */ byte[] a; // = new byte[256];
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public eo c;
    @DexIgnore
    public int d; // = 0;

    @DexIgnore
    public fo a(ByteBuffer byteBuffer) {
        m();
        this.b = byteBuffer.asReadOnlyBuffer();
        this.b.position(0);
        this.b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    @DexIgnore
    public final void b(int i) {
        boolean z = false;
        while (!z && !b() && this.c.c <= i) {
            int d2 = d();
            if (d2 == 33) {
                int d3 = d();
                if (d3 == 1) {
                    n();
                } else if (d3 == 249) {
                    this.c.d = new Cdo();
                    h();
                } else if (d3 == 254) {
                    n();
                } else if (d3 != 255) {
                    n();
                } else {
                    f();
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        k();
                    } else {
                        n();
                    }
                }
            } else if (d2 == 44) {
                eo eoVar = this.c;
                if (eoVar.d == null) {
                    eoVar.d = new Cdo();
                }
                e();
            } else if (d2 != 59) {
                this.c.b = 1;
            } else {
                z = true;
            }
        }
    }

    @DexIgnore
    public eo c() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (b()) {
            return this.c;
        } else {
            i();
            if (!b()) {
                g();
                eo eoVar = this.c;
                if (eoVar.c < 0) {
                    eoVar.b = 1;
                }
            }
            return this.c;
        }
    }

    @DexIgnore
    public final int d() {
        try {
            return this.b.get() & FileType.MASKED_INDEX;
        } catch (Exception unused) {
            this.c.b = 1;
            return 0;
        }
    }

    @DexIgnore
    public final void e() {
        this.c.d.a = l();
        this.c.d.b = l();
        this.c.d.c = l();
        this.c.d.d = l();
        int d2 = d();
        boolean z = false;
        boolean z2 = (d2 & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        Cdo doVar = this.c.d;
        if ((d2 & 64) != 0) {
            z = true;
        }
        doVar.e = z;
        if (z2) {
            this.c.d.k = a(pow);
        } else {
            this.c.d.k = null;
        }
        this.c.d.j = this.b.position();
        o();
        if (!b()) {
            eo eoVar = this.c;
            eoVar.c++;
            eoVar.e.add(eoVar.d);
        }
    }

    @DexIgnore
    public final void f() {
        this.d = d();
        if (this.d > 0) {
            int i = 0;
            int i2 = 0;
            while (i < this.d) {
                try {
                    i2 = this.d - i;
                    this.b.get(this.a, i, i2);
                    i += i2;
                } catch (Exception e) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.d, e);
                    }
                    this.c.b = 1;
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void g() {
        b(Integer.MAX_VALUE);
    }

    @DexIgnore
    public final void h() {
        d();
        int d2 = d();
        Cdo doVar = this.c.d;
        doVar.g = (d2 & 28) >> 2;
        boolean z = true;
        if (doVar.g == 0) {
            doVar.g = 1;
        }
        Cdo doVar2 = this.c.d;
        if ((d2 & 1) == 0) {
            z = false;
        }
        doVar2.f = z;
        int l = l();
        if (l < 2) {
            l = 10;
        }
        Cdo doVar3 = this.c.d;
        doVar3.i = l * 10;
        doVar3.h = d();
        d();
    }

    @DexIgnore
    public final void i() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        j();
        if (this.c.h && !b()) {
            eo eoVar = this.c;
            eoVar.a = a(eoVar.i);
            eo eoVar2 = this.c;
            eoVar2.l = eoVar2.a[eoVar2.j];
        }
    }

    @DexIgnore
    public final void j() {
        this.c.f = l();
        this.c.g = l();
        int d2 = d();
        this.c.h = (d2 & 128) != 0;
        this.c.i = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        this.c.j = d();
        this.c.k = d();
    }

    @DexIgnore
    public final void k() {
        do {
            f();
            byte[] bArr = this.a;
            if (bArr[0] == 1) {
                this.c.m = ((bArr[2] & FileType.MASKED_INDEX) << 8) | (bArr[1] & FileType.MASKED_INDEX);
            }
            if (this.d <= 0) {
                return;
            }
        } while (!b());
    }

    @DexIgnore
    public final int l() {
        return this.b.getShort();
    }

    @DexIgnore
    public final void m() {
        this.b = null;
        Arrays.fill(this.a, (byte) 0);
        this.c = new eo();
        this.d = 0;
    }

    @DexIgnore
    public final void n() {
        int d2;
        do {
            d2 = d();
            this.b.position(Math.min(this.b.position() + d2, this.b.limit()));
        } while (d2 > 0);
    }

    @DexIgnore
    public final void o() {
        d();
        n();
    }

    @DexIgnore
    public void a() {
        this.b = null;
        this.c = null;
    }

    @DexIgnore
    public final int[] a(int i) {
        byte[] bArr = new byte[(i * 3)];
        int[] iArr = null;
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                byte b2 = bArr[i3] & FileType.MASKED_INDEX;
                int i5 = i4 + 1;
                byte b3 = bArr[i4] & FileType.MASKED_INDEX;
                int i6 = i5 + 1;
                int i7 = i2 + 1;
                iArr[i2] = (b2 << DateTimeFieldType.CLOCKHOUR_OF_DAY) | -16777216 | (b3 << 8) | (bArr[i5] & FileType.MASKED_INDEX);
                i3 = i6;
                i2 = i7;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.c.b = 1;
        }
        return iArr;
    }

    @DexIgnore
    public final boolean b() {
        return this.c.b != 0;
    }
}
