package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p62 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public ArrayList<g13> a;
    @DexIgnore
    public d b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public View e;
        @DexIgnore
        public TextView f;
        @DexIgnore
        public /* final */ /* synthetic */ p62 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(p62 p62, View view) {
            super(view);
            kd4.b(view, "view");
            this.g = p62;
            View findViewById = view.findViewById(R.id.btn_add);
            kd4.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.e = findViewById;
            View findViewById2 = view.findViewById(R.id.tv_create_new_preset);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.f = (TextView) findViewById2;
            this.e.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_grey));
                this.e.setClickable(false);
                this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_MaximumPresets_Actions_Text__YouveSavedMaximumNumberOfPresets));
                return;
            }
            this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_black));
            this.e.setClickable(true);
            this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwipedCreate_Text__CreateANewPreset));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == R.id.btn_add) {
                this.g.b.z0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(g13 g13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void c(String str, String str2);

        @DexIgnore
        void y0();

        @DexIgnore
        void z0();
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public p62(ArrayList<g13> arrayList, d dVar) {
        kd4.b(arrayList, "mData");
        kd4.b(dVar, "mListener");
        this.a = arrayList;
        this.b = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.a.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.a.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            g13 g13 = this.a.get(i);
            kd4.a((Object) g13, "mData[position]");
            g13 g132 = g13;
            if (i != 0) {
                z = false;
            }
            bVar.a(g132, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.a.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hybrid_customize_preset_detail, viewGroup, false);
            kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_customize_preset_add, viewGroup, false);
        kd4.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(ArrayList<g13> arrayList) {
        kd4.b(arrayList, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + arrayList);
        this.a.clear();
        this.a.addAll(arrayList);
        this.a.add(new g13("", "", new ArrayList(), false));
        notifyDataSetChanged();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView e;
        @DexIgnore
        public CustomizeWidget f;
        @DexIgnore
        public CustomizeWidget g;
        @DexIgnore
        public CustomizeWidget h;
        @DexIgnore
        public TextView i;
        @DexIgnore
        public View j;
        @DexIgnore
        public TextView k;
        @DexIgnore
        public ImageView l;
        @DexIgnore
        public g13 m;
        @DexIgnore
        public View n;
        @DexIgnore
        public View o;
        @DexIgnore
        public View p;
        @DexIgnore
        public View q;
        @DexIgnore
        public /* final */ /* synthetic */ p62 r;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(p62 p62, View view) {
            super(view);
            kd4.b(view, "view");
            this.r = p62;
            View findViewById = view.findViewById(R.id.tv_preset_name);
            kd4.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.e = (TextView) findViewById;
            this.e.setOnClickListener(this);
            View findViewById2 = view.findViewById(R.id.wa_top);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.wa_top)");
            this.f = (CustomizeWidget) findViewById2;
            View findViewById3 = view.findViewById(R.id.wa_middle);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.wa_middle)");
            this.g = (CustomizeWidget) findViewById3;
            View findViewById4 = view.findViewById(R.id.wa_bottom);
            kd4.a((Object) findViewById4, "view.findViewById(R.id.wa_bottom)");
            this.h = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(R.id.iv_watch_theme_background);
            kd4.a((Object) findViewById5, "view.findViewById(R.id.iv_watch_theme_background)");
            this.l = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(R.id.v_underline);
            kd4.a((Object) findViewById6, "view.findViewById(R.id.v_underline)");
            this.q = findViewById6;
            View findViewById7 = view.findViewById(R.id.line_bottom);
            kd4.a((Object) findViewById7, "view.findViewById(R.id.line_bottom)");
            this.p = findViewById7;
            View findViewById8 = view.findViewById(R.id.line_center);
            kd4.a((Object) findViewById8, "view.findViewById(R.id.line_center)");
            this.o = findViewById8;
            View findViewById9 = view.findViewById(R.id.line_top);
            kd4.a((Object) findViewById9, "view.findViewById(R.id.line_top)");
            this.n = findViewById9;
            View findViewById10 = view.findViewById(R.id.layout_right);
            kd4.a((Object) findViewById10, "view.findViewById(R.id.layout_right)");
            this.j = findViewById10;
            View findViewById11 = view.findViewById(R.id.tv_right);
            kd4.a((Object) findViewById11, "view.findViewById(R.id.tv_right)");
            this.k = (TextView) findViewById11;
            View findViewById12 = view.findViewById(R.id.tv_left);
            kd4.a((Object) findViewById12, "view.findViewById(R.id.tv_left)");
            this.i = (TextView) findViewById12;
            this.j.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.f.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.h.setOnClickListener(this);
            if (FossilDeviceSerialPatternUtil.isSamDevice(PortfolioApp.W.c().e())) {
                this.l.setImageResource(R.drawable.hybrid_default_watch_background);
            }
        }

        @DexIgnore
        public final List<f8<View, String>> a() {
            f8[] f8VarArr = new f8[6];
            TextView textView = this.e;
            f8VarArr[0] = new f8(textView, textView.getTransitionName());
            View view = this.q;
            f8VarArr[1] = new f8(view, view.getTransitionName());
            ImageView imageView = this.l;
            if (imageView != null) {
                f8VarArr[2] = new f8(imageView, imageView.getTransitionName());
                View view2 = this.p;
                f8VarArr[3] = new f8(view2, view2.getTransitionName());
                View view3 = this.o;
                f8VarArr[4] = new f8(view3, view3.getTransitionName());
                View view4 = this.n;
                f8VarArr[5] = new f8(view4, view4.getTransitionName());
                return cb4.c(f8VarArr);
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<f8<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.f;
            CustomizeWidget customizeWidget2 = this.g;
            CustomizeWidget customizeWidget3 = this.h;
            return cb4.c(new f8(customizeWidget, customizeWidget.getTransitionName()), new f8(customizeWidget2, customizeWidget2.getTransitionName()), new f8(customizeWidget3, customizeWidget3.getTransitionName()));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0098, code lost:
            if (r0 != null) goto L_0x009c;
         */
        @DexIgnore
        public void onClick(View view) {
            String str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.m == null) && view != null) {
                switch (view.getId()) {
                    case R.id.layout_right /*2131362478*/:
                        this.r.b.y0();
                        return;
                    case R.id.tv_left /*2131362897*/:
                        if (this.r.a.size() > 2) {
                            Object obj = this.r.a.get(1);
                            kd4.a(obj, "mData[1]");
                            g13 g13 = (g13) obj;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("current ");
                            g13 g132 = this.m;
                            sb.append(g132 != null ? g132.c() : null);
                            sb.append(" nextPreset ");
                            sb.append(g13.c());
                            local2.d("CustomizeAdapter", sb.toString());
                            d b = this.r.b;
                            g13 g133 = this.m;
                            if (g133 != null) {
                                boolean d = g133.d();
                                g13 g134 = this.m;
                                if (g134 != null) {
                                    b.b(d, g134.c(), g13.c(), g13.b());
                                    return;
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case R.id.tv_preset_name /*2131362920*/:
                        d b2 = this.r.b;
                        g13 g135 = this.m;
                        String str2 = "";
                        if (g135 != null) {
                            str = g135.c();
                            break;
                        }
                        str = str2;
                        g13 g136 = this.m;
                        if (g136 != null) {
                            String b3 = g136.b();
                            if (b3 != null) {
                                str2 = b3;
                            }
                        }
                        b2.c(str, str2);
                        return;
                    case R.id.wa_bottom /*2131363027*/:
                        this.r.b.a(this.m, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wa_middle /*2131363028*/:
                        this.r.b.a(this.m, a(), b(), "middle", getAdapterPosition());
                        return;
                    case R.id.wa_top /*2131363029*/:
                        this.r.b.a(this.m, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        public final void a(g13 g13, boolean z) {
            kd4.b(g13, "preset");
            if (z) {
                this.j.setSelected(true);
                this.k.setText(PortfolioApp.W.c().getString(R.string.Customization_Delete_PresetDeleted_Label__Applied));
                this.k.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_tint, 0, 0, 0);
                this.k.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.white));
                this.j.setClickable(false);
                if (this.r.a.size() == 2) {
                    this.i.setVisibility(4);
                }
            } else {
                this.j.setSelected(false);
                this.k.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwiped_CTA__Apply));
                this.k.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.k.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor));
                this.j.setClickable(true);
                this.i.setVisibility(0);
            }
            this.m = g13;
            this.e.setText(g13.c());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(g13.a());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with microApps=" + arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                h13 h13 = (h13) it.next();
                String f2 = h13.f();
                if (f2 != null) {
                    int hashCode = f2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && f2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                this.f.c(h13.d());
                                this.f.h();
                            }
                        } else if (f2.equals("middle")) {
                            this.g.c(h13.d());
                            this.g.h();
                        }
                    } else if (f2.equals("bottom")) {
                        this.h.c(h13.d());
                        this.h.h();
                    }
                }
            }
        }
    }
}
