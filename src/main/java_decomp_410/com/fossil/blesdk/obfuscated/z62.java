package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z62 {
    @DexIgnore
    public final Date a(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = rk2.c.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.parse(str);
            }
            return null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateLongStringConverter", "toOffsetDateTime - e=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final String a(Date date) {
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = rk2.c.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.format(date);
            }
            return null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateLongStringConverter", "fromOffsetDateTime - e=" + e);
            return null;
        }
    }
}
