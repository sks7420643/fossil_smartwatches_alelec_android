package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Handler;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w61 implements t61 {
    @DexIgnore
    public static w61 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public w61(Context context) {
        this.a = context;
        this.a.getContentResolver().registerContentObserver(l61.a, true, new y61(this, (Handler) null));
    }

    @DexIgnore
    public static w61 a(Context context) {
        w61 w61;
        synchronized (w61.class) {
            if (b == null) {
                b = l6.b(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new w61(context) : new w61();
            }
            w61 = b;
        }
        return w61;
    }

    @DexIgnore
    /* renamed from: b */
    public final String a(String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) u61.a(new x61(this, str));
        } catch (SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    @DexIgnore
    public final /* synthetic */ String c(String str) {
        return l61.a(this.a.getContentResolver(), str, (String) null);
    }

    @DexIgnore
    public w61() {
        this.a = null;
    }
}
