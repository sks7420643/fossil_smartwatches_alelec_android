package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yf */
public abstract class C3361yf {
    @DexIgnore
    public /* final */ int endVersion;
    @DexIgnore
    public /* final */ int startVersion;

    @DexIgnore
    public C3361yf(int i, int i2) {
        this.startVersion = i;
        this.endVersion = i2;
    }

    @DexIgnore
    public abstract void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar);
}
