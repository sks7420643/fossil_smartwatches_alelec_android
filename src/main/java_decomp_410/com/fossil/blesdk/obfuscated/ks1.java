package com.fossil.blesdk.obfuscated;

import android.animation.TimeInterpolator;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ks1 {
    @DexIgnore
    public static /* final */ boolean T; // = (Build.VERSION.SDK_INT < 18);
    @DexIgnore
    public static /* final */ Paint U; // = null;
    @DexIgnore
    public Paint A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public int[] F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public /* final */ TextPaint H;
    @DexIgnore
    public /* final */ TextPaint I;
    @DexIgnore
    public TimeInterpolator J;
    @DexIgnore
    public TimeInterpolator K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public int O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public float R;
    @DexIgnore
    public int S;
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Rect d;
    @DexIgnore
    public /* final */ Rect e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public int g; // = 16;
    @DexIgnore
    public int h; // = 16;
    @DexIgnore
    public float i; // = 15.0f;
    @DexIgnore
    public float j; // = 15.0f;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public Typeface s;
    @DexIgnore
    public Typeface t;
    @DexIgnore
    public Typeface u;
    @DexIgnore
    public CharSequence v;
    @DexIgnore
    public CharSequence w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public Bitmap z;

    /*
    static {
        Paint paint = U;
        if (paint != null) {
            paint.setAntiAlias(true);
            U.setColor(-65281);
        }
    }
    */

    @DexIgnore
    public ks1(View view) {
        this.a = view;
        this.H = new TextPaint(129);
        this.I = new TextPaint(this.H);
        this.e = new Rect();
        this.d = new Rect();
        this.f = new RectF();
    }

    @DexIgnore
    public void a(TimeInterpolator timeInterpolator) {
        this.J = timeInterpolator;
        r();
    }

    @DexIgnore
    public void b(TimeInterpolator timeInterpolator) {
        this.K = timeInterpolator;
        r();
    }

    @DexIgnore
    public void c(int i2) {
        if (this.h != i2) {
            this.h = i2;
            r();
        }
    }

    @DexIgnore
    public void d(float f2) {
        if (this.i != f2) {
            this.i = f2;
            r();
        }
    }

    @DexIgnore
    public void e(int i2) {
        if (this.g != i2) {
            this.g = i2;
            r();
        }
    }

    @DexIgnore
    public final void f(float f2) {
        b(f2);
        this.y = T && this.D != 1.0f;
        if (this.y) {
            e();
        }
        f9.C(this.a);
    }

    @DexIgnore
    public int g() {
        return this.h;
    }

    @DexIgnore
    public float h() {
        a(this.I);
        return -this.I.ascent();
    }

    @DexIgnore
    public Typeface i() {
        Typeface typeface = this.s;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public int j() {
        int[] iArr = this.F;
        if (iArr != null) {
            return this.l.getColorForState(iArr, 0);
        }
        return this.l.getDefaultColor();
    }

    @DexIgnore
    public final int k() {
        int[] iArr = this.F;
        if (iArr != null) {
            return this.k.getColorForState(iArr, 0);
        }
        return this.k.getDefaultColor();
    }

    @DexIgnore
    public int l() {
        return this.g;
    }

    @DexIgnore
    public Typeface m() {
        Typeface typeface = this.t;
        return typeface != null ? typeface : Typeface.DEFAULT;
    }

    @DexIgnore
    public float n() {
        return this.c;
    }

    @DexIgnore
    public CharSequence o() {
        return this.v;
    }

    @DexIgnore
    public final boolean p() {
        ColorStateList colorStateList = this.l;
        if (colorStateList == null || !colorStateList.isStateful()) {
            ColorStateList colorStateList2 = this.k;
            if (colorStateList2 == null || !colorStateList2.isStateful()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public void q() {
        this.b = this.e.width() > 0 && this.e.height() > 0 && this.d.width() > 0 && this.d.height() > 0;
    }

    @DexIgnore
    public void r() {
        if (this.a.getHeight() > 0 && this.a.getWidth() > 0) {
            a();
            c();
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            r();
        }
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            r();
        }
    }

    @DexIgnore
    public void c(Typeface typeface) {
        this.t = typeface;
        this.s = typeface;
        r();
    }

    @DexIgnore
    public void d(int i2) {
        z2 a2 = z2.a(this.a.getContext(), i2, a0.TextAppearance);
        if (a2.g(a0.TextAppearance_android_textColor)) {
            this.k = a2.a(a0.TextAppearance_android_textColor);
        }
        if (a2.g(a0.TextAppearance_android_textSize)) {
            this.i = (float) a2.c(a0.TextAppearance_android_textSize, (int) this.i);
        }
        this.S = a2.d(a0.TextAppearance_android_shadowColor, 0);
        this.Q = a2.b(a0.TextAppearance_android_shadowDx, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.R = a2.b(a0.TextAppearance_android_shadowDy, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.P = a2.b(a0.TextAppearance_android_shadowRadius, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a2.a();
        if (Build.VERSION.SDK_INT >= 16) {
            this.t = a(i2);
        }
        r();
    }

    @DexIgnore
    public void e(float f2) {
        float a2 = k7.a(f2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        if (a2 != this.c) {
            this.c = a2;
            c();
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        if (!a(this.e, i2, i3, i4, i5)) {
            this.e.set(i2, i3, i4, i5);
            this.G = true;
            q();
        }
    }

    @DexIgnore
    public void b(int i2, int i3, int i4, int i5) {
        if (!a(this.d, i2, i3, i4, i5)) {
            this.d.set(i2, i3, i4, i5);
            this.G = true;
            q();
        }
    }

    @DexIgnore
    public final void c() {
        a(this.c);
    }

    @DexIgnore
    public ColorStateList f() {
        return this.l;
    }

    @DexIgnore
    public final void c(float f2) {
        this.f.left = a((float) this.d.left, (float) this.e.left, f2, this.J);
        this.f.top = a(this.m, this.n, f2, this.J);
        this.f.right = a((float) this.d.right, (float) this.e.right, f2, this.J);
        this.f.bottom = a((float) this.d.bottom, (float) this.e.bottom, f2, this.J);
    }

    @DexIgnore
    public final void e() {
        if (this.z == null && !this.d.isEmpty() && !TextUtils.isEmpty(this.w)) {
            a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.B = this.H.ascent();
            this.C = this.H.descent();
            TextPaint textPaint = this.H;
            CharSequence charSequence = this.w;
            int round = Math.round(textPaint.measureText(charSequence, 0, charSequence.length()));
            int round2 = Math.round(this.C - this.B);
            if (round > 0 && round2 > 0) {
                this.z = Bitmap.createBitmap(round, round2, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(this.z);
                CharSequence charSequence2 = this.w;
                canvas.drawText(charSequence2, 0, charSequence2.length(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ((float) round2) - this.H.descent(), this.H);
                if (this.A == null) {
                    this.A = new Paint(3);
                }
            }
        }
    }

    @DexIgnore
    public void a(RectF rectF) {
        float f2;
        boolean a2 = a(this.v);
        Rect rect = this.e;
        if (!a2) {
            f2 = (float) rect.left;
        } else {
            f2 = ((float) rect.right) - b();
        }
        rectF.left = f2;
        Rect rect2 = this.e;
        rectF.top = (float) rect2.top;
        rectF.right = !a2 ? rectF.left + b() : (float) rect2.right;
        rectF.bottom = ((float) this.e.top) + h();
    }

    @DexIgnore
    public float b() {
        if (this.v == null) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        a(this.I);
        TextPaint textPaint = this.I;
        CharSequence charSequence = this.v;
        return textPaint.measureText(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public void b(int i2) {
        z2 a2 = z2.a(this.a.getContext(), i2, a0.TextAppearance);
        if (a2.g(a0.TextAppearance_android_textColor)) {
            this.l = a2.a(a0.TextAppearance_android_textColor);
        }
        if (a2.g(a0.TextAppearance_android_textSize)) {
            this.j = (float) a2.c(a0.TextAppearance_android_textSize, (int) this.j);
        }
        this.O = a2.d(a0.TextAppearance_android_shadowColor, 0);
        this.M = a2.b(a0.TextAppearance_android_shadowDx, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.N = a2.b(a0.TextAppearance_android_shadowDy, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.L = a2.b(a0.TextAppearance_android_shadowRadius, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a2.a();
        if (Build.VERSION.SDK_INT >= 16) {
            this.s = a(i2);
        }
        r();
    }

    @DexIgnore
    public final void a(TextPaint textPaint) {
        textPaint.setTextSize(this.j);
        textPaint.setTypeface(this.s);
    }

    @DexIgnore
    public final Typeface a(int i2) {
        TypedArray obtainStyledAttributes = this.a.getContext().obtainStyledAttributes(i2, new int[]{16843692});
        try {
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                return Typeface.create(string, 0);
            }
            obtainStyledAttributes.recycle();
            return null;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void a(Typeface typeface) {
        if (this.s != typeface) {
            this.s = typeface;
            r();
        }
    }

    @DexIgnore
    public final void d() {
        Bitmap bitmap = this.z;
        if (bitmap != null) {
            bitmap.recycle();
            this.z = null;
        }
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        this.F = iArr;
        if (!p()) {
            return false;
        }
        r();
        return true;
    }

    @DexIgnore
    public final void a(float f2) {
        c(f2);
        this.q = a(this.o, this.p, f2, this.J);
        this.r = a(this.m, this.n, f2, this.J);
        f(a(this.i, this.j, f2, this.K));
        if (this.l != this.k) {
            this.H.setColor(a(k(), j(), f2));
        } else {
            this.H.setColor(j());
        }
        this.H.setShadowLayer(a(this.P, this.L, f2, (TimeInterpolator) null), a(this.Q, this.M, f2, (TimeInterpolator) null), a(this.R, this.N, f2, (TimeInterpolator) null), a(this.S, this.O, f2));
        f9.C(this.a);
    }

    @DexIgnore
    public void b(Typeface typeface) {
        if (this.t != typeface) {
            this.t = typeface;
            r();
        }
    }

    @DexIgnore
    public final void b(float f2) {
        boolean z2;
        float f3;
        boolean z3;
        if (this.v != null) {
            float width = (float) this.e.width();
            float width2 = (float) this.d.width();
            boolean z4 = true;
            if (a(f2, this.j)) {
                float f4 = this.j;
                this.D = 1.0f;
                Typeface typeface = this.u;
                Typeface typeface2 = this.s;
                if (typeface != typeface2) {
                    this.u = typeface2;
                    z3 = true;
                } else {
                    z3 = false;
                }
                f3 = f4;
                z2 = z3;
            } else {
                f3 = this.i;
                Typeface typeface3 = this.u;
                Typeface typeface4 = this.t;
                if (typeface3 != typeface4) {
                    this.u = typeface4;
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (a(f2, this.i)) {
                    this.D = 1.0f;
                } else {
                    this.D = f2 / this.i;
                }
                float f5 = this.j / this.i;
                width = width2 * f5 > width ? Math.min(width / f5, width2) : width2;
            }
            if (width > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z2 = this.E != f3 || this.G || z2;
                this.E = f3;
                this.G = false;
            }
            if (this.w == null || z2) {
                this.H.setTextSize(this.E);
                this.H.setTypeface(this.u);
                TextPaint textPaint = this.H;
                if (this.D == 1.0f) {
                    z4 = false;
                }
                textPaint.setLinearText(z4);
                CharSequence ellipsize = TextUtils.ellipsize(this.v, this.H, width, TextUtils.TruncateAt.END);
                if (!TextUtils.equals(ellipsize, this.w)) {
                    this.w = ellipsize;
                    this.x = a(this.w);
                }
            }
        }
    }

    @DexIgnore
    public final void a() {
        float f2 = this.E;
        b(this.j);
        CharSequence charSequence = this.w;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float measureText = charSequence != null ? this.H.measureText(charSequence, 0, charSequence.length()) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int a2 = p8.a(this.h, this.x ? 1 : 0);
        int i2 = a2 & 112;
        if (i2 == 48) {
            this.n = ((float) this.e.top) - this.H.ascent();
        } else if (i2 != 80) {
            this.n = ((float) this.e.centerY()) + (((this.H.descent() - this.H.ascent()) / 2.0f) - this.H.descent());
        } else {
            this.n = (float) this.e.bottom;
        }
        int i3 = a2 & 8388615;
        if (i3 == 1) {
            this.p = ((float) this.e.centerX()) - (measureText / 2.0f);
        } else if (i3 != 5) {
            this.p = (float) this.e.left;
        } else {
            this.p = ((float) this.e.right) - measureText;
        }
        b(this.i);
        CharSequence charSequence2 = this.w;
        if (charSequence2 != null) {
            f3 = this.H.measureText(charSequence2, 0, charSequence2.length());
        }
        int a3 = p8.a(this.g, this.x ? 1 : 0);
        int i4 = a3 & 112;
        if (i4 == 48) {
            this.m = ((float) this.d.top) - this.H.ascent();
        } else if (i4 != 80) {
            this.m = ((float) this.d.centerY()) + (((this.H.descent() - this.H.ascent()) / 2.0f) - this.H.descent());
        } else {
            this.m = (float) this.d.bottom;
        }
        int i5 = a3 & 8388615;
        if (i5 == 1) {
            this.o = ((float) this.d.centerX()) - (f3 / 2.0f);
        } else if (i5 != 5) {
            this.o = (float) this.d.left;
        } else {
            this.o = ((float) this.d.right) - f3;
        }
        d();
        f(f2);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        if (charSequence == null || !charSequence.equals(this.v)) {
            this.v = charSequence;
            this.w = null;
            d();
            r();
        }
    }

    @DexIgnore
    public void a(Canvas canvas) {
        float f2;
        int save = canvas.save();
        if (this.w != null && this.b) {
            float f3 = this.q;
            float f4 = this.r;
            boolean z2 = this.y && this.z != null;
            if (z2) {
                f2 = this.B * this.D;
            } else {
                f2 = this.H.ascent() * this.D;
                this.H.descent();
            }
            if (z2) {
                f4 += f2;
            }
            float f5 = f4;
            float f6 = this.D;
            if (f6 != 1.0f) {
                canvas.scale(f6, f6, f3, f5);
            }
            if (z2) {
                canvas.drawBitmap(this.z, f3, f5, this.A);
            } else {
                CharSequence charSequence = this.w;
                canvas.drawText(charSequence, 0, charSequence.length(), f3, f5, this.H);
            }
        }
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public final boolean a(CharSequence charSequence) {
        boolean z2 = true;
        if (f9.k(this.a) != 1) {
            z2 = false;
        }
        return (z2 ? a8.d : a8.c).a(charSequence, 0, charSequence.length());
    }

    @DexIgnore
    public static boolean a(float f2, float f3) {
        return Math.abs(f2 - f3) < 0.001f;
    }

    @DexIgnore
    public static int a(int i2, int i3, float f2) {
        float f3 = 1.0f - f2;
        return Color.argb((int) ((((float) Color.alpha(i2)) * f3) + (((float) Color.alpha(i3)) * f2)), (int) ((((float) Color.red(i2)) * f3) + (((float) Color.red(i3)) * f2)), (int) ((((float) Color.green(i2)) * f3) + (((float) Color.green(i3)) * f2)), (int) ((((float) Color.blue(i2)) * f3) + (((float) Color.blue(i3)) * f2)));
    }

    @DexIgnore
    public static float a(float f2, float f3, float f4, TimeInterpolator timeInterpolator) {
        if (timeInterpolator != null) {
            f4 = timeInterpolator.getInterpolation(f4);
        }
        return dr1.a(f2, f3, f4);
    }

    @DexIgnore
    public static boolean a(Rect rect, int i2, int i3, int i4, int i5) {
        return rect.left == i2 && rect.top == i3 && rect.right == i4 && rect.bottom == i5;
    }
}
