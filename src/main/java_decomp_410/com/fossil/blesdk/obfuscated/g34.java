package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g34 implements d34 {
    @DexIgnore
    public boolean a; // = false;

    @DexIgnore
    public void d(String str, String str2) {
        if (this.a) {
            Log.d(str, str2);
        }
    }

    @DexIgnore
    public void e(String str, String str2) {
        if (this.a) {
            Log.e(str, str2);
        }
    }

    @DexIgnore
    public void setLoggable(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public void w(String str, String str2) {
        if (this.a) {
            Log.w(str, str2);
        }
    }

    @DexIgnore
    public void e(String str, String str2, Throwable th) {
        if (this.a) {
            Log.e(str, str2, th);
        }
    }
}
