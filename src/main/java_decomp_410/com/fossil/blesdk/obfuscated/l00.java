package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.database.entity.DeviceFile;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l00 implements k00 {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ lf b;
    @DexIgnore
    public /* final */ wf c;
    @DexIgnore
    public /* final */ wf d;
    @DexIgnore
    public /* final */ wf e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lf<DeviceFile> {
        @DexIgnore
        public a(l00 l00, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(kg kgVar, DeviceFile deviceFile) {
            kgVar.b(1, (long) deviceFile.getId());
            if (deviceFile.getDeviceMacAddress() == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, deviceFile.getDeviceMacAddress());
            }
            kgVar.b(3, (long) deviceFile.getFileType());
            kgVar.b(4, (long) deviceFile.getFileIndex());
            if (deviceFile.getRawData() == null) {
                kgVar.a(5);
            } else {
                kgVar.a(5, deviceFile.getRawData());
            }
            kgVar.b(6, deviceFile.getFileLength());
            kgVar.b(7, deviceFile.getFileCrc());
            kgVar.b(8, deviceFile.getCreatedTimeStamp());
            kgVar.b(9, deviceFile.isCompleted() ? 1 : 0);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DeviceFile`(`id`,`deviceMacAddress`,`fileType`,`fileIndex`,`rawData`,`fileLength`,`fileCrc`,`createdTimeStamp`,`isCompleted`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends wf {
        @DexIgnore
        public b(l00 l00, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "delete from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends wf {
        @DexIgnore
        public c(l00 l00, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "delete from DeviceFile where deviceMacAddress = ? and fileType = ?";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends wf {
        @DexIgnore
        public d(l00 l00, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public String createQuery() {
            return "delete from DeviceFile where deviceMacAddress = ?";
        }
    }

    @DexIgnore
    public l00(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
        this.c = new b(this, roomDatabase);
        this.d = new c(this, roomDatabase);
        this.e = new d(this, roomDatabase);
    }

    @DexIgnore
    public long a(DeviceFile deviceFile) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(deviceFile);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public int b(String str, byte b2, byte b3) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        acquire.b(2, (long) b2);
        acquire.b(3, (long) b3);
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public int c(String str, byte b2) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.d.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        acquire.b(2, (long) b2);
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    public int a(String str) {
        this.a.assertNotSuspendingTransaction();
        kg acquire = this.e.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int n = acquire.n();
            this.a.setTransactionSuccessful();
            return n;
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<DeviceFile> b(String str, byte b2) {
        String str2 = str;
        uf b3 = uf.b("select * from DeviceFile where deviceMacAddress = ? and fileType = ?", 2);
        if (str2 == null) {
            b3.a(1);
        } else {
            b3.a(1, str2);
        }
        b3.b(2, (long) b2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b3, false);
        try {
            int b4 = ag.b(a2, "id");
            int b5 = ag.b(a2, "deviceMacAddress");
            int b6 = ag.b(a2, "fileType");
            int b7 = ag.b(a2, "fileIndex");
            int b8 = ag.b(a2, "rawData");
            int b9 = ag.b(a2, "fileLength");
            int b10 = ag.b(a2, "fileCrc");
            int b11 = ag.b(a2, "createdTimeStamp");
            int b12 = ag.b(a2, "isCompleted");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                DeviceFile deviceFile = new DeviceFile(a2.getString(b5), (byte) a2.getShort(b6), (byte) a2.getShort(b7), a2.getBlob(b8), a2.getLong(b9), a2.getLong(b10), a2.getLong(b11), a2.getInt(b12) != 0);
                deviceFile.setId(a2.getInt(b4));
                arrayList.add(deviceFile);
            }
            return arrayList;
        } finally {
            a2.close();
            b3.c();
        }
    }

    @DexIgnore
    public List<DeviceFile> a(String str, byte b2) {
        String str2 = str;
        uf b3 = uf.b("select * from DeviceFile where deviceMacAddress = ? and fileType = ? and isCompleted = 0", 2);
        if (str2 == null) {
            b3.a(1);
        } else {
            b3.a(1, str2);
        }
        b3.b(2, (long) b2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b3, false);
        try {
            int b4 = ag.b(a2, "id");
            int b5 = ag.b(a2, "deviceMacAddress");
            int b6 = ag.b(a2, "fileType");
            int b7 = ag.b(a2, "fileIndex");
            int b8 = ag.b(a2, "rawData");
            int b9 = ag.b(a2, "fileLength");
            int b10 = ag.b(a2, "fileCrc");
            int b11 = ag.b(a2, "createdTimeStamp");
            int b12 = ag.b(a2, "isCompleted");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                DeviceFile deviceFile = new DeviceFile(a2.getString(b5), (byte) a2.getShort(b6), (byte) a2.getShort(b7), a2.getBlob(b8), a2.getLong(b9), a2.getLong(b10), a2.getLong(b11), a2.getInt(b12) != 0);
                deviceFile.setId(a2.getInt(b4));
                arrayList.add(deviceFile);
            }
            return arrayList;
        } finally {
            a2.close();
            b3.c();
        }
    }

    @DexIgnore
    public List<DeviceFile> a(String str, byte b2, byte b3) {
        String str2 = str;
        uf b4 = uf.b("select * from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0", 3);
        if (str2 == null) {
            b4.a(1);
        } else {
            b4.a(1, str2);
        }
        b4.b(2, (long) b2);
        b4.b(3, (long) b3);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b4, false);
        try {
            int b5 = ag.b(a2, "id");
            int b6 = ag.b(a2, "deviceMacAddress");
            int b7 = ag.b(a2, "fileType");
            int b8 = ag.b(a2, "fileIndex");
            int b9 = ag.b(a2, "rawData");
            int b10 = ag.b(a2, "fileLength");
            int b11 = ag.b(a2, "fileCrc");
            int b12 = ag.b(a2, "createdTimeStamp");
            int b13 = ag.b(a2, "isCompleted");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                DeviceFile deviceFile = new DeviceFile(a2.getString(b6), (byte) a2.getShort(b7), (byte) a2.getShort(b8), a2.getBlob(b9), a2.getLong(b10), a2.getLong(b11), a2.getLong(b12), a2.getInt(b13) != 0);
                deviceFile.setId(a2.getInt(b5));
                arrayList.add(deviceFile);
            }
            return arrayList;
        } finally {
            a2.close();
            b4.c();
        }
    }
}
