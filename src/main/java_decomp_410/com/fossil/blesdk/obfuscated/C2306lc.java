package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lc */
public class C2306lc {
    @DexIgnore
    /* renamed from: a */
    public static android.app.Application m10128a(android.app.Activity activity) {
        android.app.Application application = activity.getApplication();
        if (application != null) {
            return application;
        }
        throw new java.lang.IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
    }

    @DexIgnore
    /* renamed from: a */
    public static android.app.Activity m10127a(androidx.fragment.app.Fragment fragment) {
        androidx.fragment.app.FragmentActivity activity = fragment.getActivity();
        if (activity != null) {
            return activity;
        }
        throw new java.lang.IllegalStateException("Can't create ViewModelProvider for detached fragment");
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2200kc m10130a(androidx.fragment.app.FragmentActivity fragmentActivity) {
        return m10131a(fragmentActivity, (com.fossil.blesdk.obfuscated.C2200kc.C2202b) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2200kc m10129a(androidx.fragment.app.Fragment fragment, com.fossil.blesdk.obfuscated.C2200kc.C2202b bVar) {
        android.app.Application a = m10128a(m10127a(fragment));
        if (bVar == null) {
            bVar = com.fossil.blesdk.obfuscated.C2200kc.C2201a.m9562a(a);
        }
        return new com.fossil.blesdk.obfuscated.C2200kc(fragment.getViewModelStore(), bVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2200kc m10131a(androidx.fragment.app.FragmentActivity fragmentActivity, com.fossil.blesdk.obfuscated.C2200kc.C2202b bVar) {
        android.app.Application a = m10128a((android.app.Activity) fragmentActivity);
        if (bVar == null) {
            bVar = com.fossil.blesdk.obfuscated.C2200kc.C2201a.m9562a(a);
        }
        return new com.fossil.blesdk.obfuscated.C2200kc(fragmentActivity.getViewModelStore(), bVar);
    }
}
