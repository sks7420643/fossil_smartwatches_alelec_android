package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.sina.weibo.sdk.constant.WBConstants;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hr3 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = hr3.class.getSimpleName();
        kd4.a((Object) simpleName, "UAUrlBuilder::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public hr3(String str) {
        kd4.b(str, "baseWebUrl");
        this.a = str;
    }

    @DexIgnore
    public final URL a(String str, String str2) {
        kd4.b(str, "clientId");
        kd4.b(str2, WBConstants.SSO_REDIRECT_URL);
        return a(this.a, "oauth2/authorize/?client_id=%s&response_type=code&redirect_uri=%s", str, str2);
    }

    @DexIgnore
    public final URL a(String str, String str2, Object... objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        pd4 pd4 = pd4.a;
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
        String format = String.format(locale, str2, Arrays.copyOf(copyOf, copyOf.length));
        kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
        sb.append(format);
        try {
            return new URL(sb.toString());
        } catch (MalformedURLException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = b;
            String message = e.getMessage();
            if (message == null) {
                kd4.a();
                throw null;
            }
            local.e(str3, message);
            throw new RuntimeException(e);
        }
    }
}
