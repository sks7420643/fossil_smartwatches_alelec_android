package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ o04 e;
    @DexIgnore
    public /* final */ /* synthetic */ p24 f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ boolean h;
    @DexIgnore
    public /* final */ /* synthetic */ g14 i;

    @DexIgnore
    public k14(g14 g14, o04 o04, p24 p24, boolean z, boolean z2) {
        this.i = g14;
        this.e = o04;
        this.f = p24;
        this.g = z;
        this.h = z2;
    }

    @DexIgnore
    public void run() {
        this.i.b(this.e, this.f, this.g, this.h);
    }
}
