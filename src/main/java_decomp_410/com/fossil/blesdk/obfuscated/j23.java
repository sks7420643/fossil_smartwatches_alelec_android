package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.ps2;
import com.fossil.blesdk.obfuscated.rs2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j23 extends zr2 implements i23, ws3.g {
    @DexIgnore
    public tr3<ma2> j;
    @DexIgnore
    public h23 k;
    @DexIgnore
    public ps2 l;
    @DexIgnore
    public rs2 m;
    @DexIgnore
    public j42 n;
    @DexIgnore
    public DianaCustomizeViewModel o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements rs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ j23 a;

        @DexIgnore
        public b(j23 j23) {
            this.a = j23;
        }

        @DexIgnore
        public void a(Complication complication) {
            kd4.b(complication, "complication");
            j23.a(this.a).b(complication.getComplicationId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ps2.c {
        @DexIgnore
        public /* final */ /* synthetic */ j23 a;

        @DexIgnore
        public c(j23 j23) {
            this.a = j23;
        }

        @DexIgnore
        public void a() {
            j23.a(this.a).h();
        }

        @DexIgnore
        public void a(Category category) {
            kd4.b(category, "category");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "onItemClicked category=" + category);
            j23.a(this.a).a(category.getId());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ j23 e;

        @DexIgnore
        public d(j23 j23) {
            this.e = j23;
        }

        @DexIgnore
        public final void onClick(View view) {
            j23.a(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ j23 e;

        @DexIgnore
        public e(j23 j23) {
            this.e = j23;
        }

        @DexIgnore
        public final void onClick(View view) {
            j23.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ma2 e;
        @DexIgnore
        public /* final */ /* synthetic */ j23 f;

        @DexIgnore
        public f(ma2 ma2, j23 j23) {
            this.e = ma2;
            this.f = j23;
        }

        @DexIgnore
        public final void onClick(View view) {
            h23 a = j23.a(this.f);
            Switch switchR = this.e.u;
            kd4.a((Object) switchR, "binding.switchRing");
            a.a(switchR.isChecked());
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ h23 a(j23 j23) {
        h23 h23 = j23.k;
        if (h23 != null) {
            return h23;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void A(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ds3 ds3 = ds3.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        kd4.a((Object) childFragmentManager, "childFragmentManager");
                        ds3.B(childFragmentManager);
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    ds3 ds32 = ds3.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    kd4.a((Object) childFragmentManager2, "childFragmentManager");
                    ds32.A(childFragmentManager2);
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                ds3 ds33 = ds3.c;
                FragmentManager childFragmentManager3 = getChildFragmentManager();
                kd4.a((Object) childFragmentManager3, "childFragmentManager");
                ds33.u(childFragmentManager3);
            }
        }
    }

    @DexIgnore
    public void C(String str) {
        kd4.b(str, "content");
        tr3<ma2> tr3 = this.j;
        if (tr3 != null) {
            ma2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                kd4.a((Object) flexibleTextView, "it.tvComplicationDetail");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ComplicationsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        if (isActive()) {
            rs2 rs2 = this.m;
            if (rs2 != null) {
                rs2.b();
            } else {
                kd4.d("mComplicationAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(Complication complication) {
        if (complication != null) {
            rs2 rs2 = this.m;
            if (rs2 != null) {
                rs2.b(complication.getComplicationId());
                c(complication);
                return;
            }
            kd4.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void c(Complication complication) {
        tr3<ma2> tr3 = this.j;
        if (tr3 != null) {
            ma2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.z;
                kd4.a((Object) flexibleTextView, "binding.tvSelectedComplication");
                flexibleTextView.setText(sm2.a(PortfolioApp.W.c(), complication.getNameKey(), complication.getName()));
                FlexibleTextView flexibleTextView2 = a2.v;
                kd4.a((Object) flexibleTextView2, "binding.tvComplicationDetail");
                flexibleTextView2.setText(sm2.a(PortfolioApp.W.c(), complication.getDescriptionKey(), complication.getDescription()));
                rs2 rs2 = this.m;
                if (rs2 != null) {
                    int a3 = rs2.a(complication.getComplicationId());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + a3);
                    if (a3 >= 0) {
                        a2.t.i(a3);
                        return;
                    }
                    return;
                }
                kd4.d("mComplicationAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(String str) {
        kd4.b(str, "category");
        tr3<ma2> tr3 = this.j;
        if (tr3 != null) {
            ma2 a2 = tr3.a();
            if (a2 != null) {
                ps2 ps2 = this.l;
                if (ps2 != null) {
                    int a3 = ps2.a(str);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ComplicationsFragment", "scrollToCategory category=" + str + " scrollTo " + a3);
                    if (a3 >= 0) {
                        ps2 ps22 = this.l;
                        if (ps22 != null) {
                            ps22.a(a3);
                            a2.s.j(a3);
                            return;
                        }
                        kd4.d("mCategoriesAdapter");
                        throw null;
                    }
                    return;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(String str) {
        kd4.b(str, "permission");
        if (isActive()) {
            int hashCode = str.hashCode();
            if (hashCode != 385352715) {
                if (hashCode != 564039755) {
                    if (hashCode == 766697727 && str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        ns3.a.a((Fragment) this, 100, "android.permission.ACCESS_FINE_LOCATION");
                    }
                } else if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                    ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
                }
            } else if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                ns3.a aVar = ns3.a;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void g(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        SearchSecondTimezoneActivity.C.a(this, str);
    }

    @DexIgnore
    public void m(List<Complication> list) {
        kd4.b(list, "complications");
        rs2 rs2 = this.m;
        if (rs2 != null) {
            rs2.a(list);
        } else {
            kd4.d("mComplicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            j42 j42 = this.n;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) dianaCustomizeEditActivity, (kc.b) j42).a(DianaCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.o = (DianaCustomizeViewModel) a2;
                h23 h23 = this.k;
                if (h23 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.o;
                    if (dianaCustomizeViewModel != null) {
                        h23.a(dianaCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onActivityResult requestCode " + i);
        if (i != 100) {
            if (i != 102) {
                if (i == 106 && i2 == -1 && intent != null) {
                    CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING");
                    if (commuteTimeSetting != null) {
                        h23 h23 = this.k;
                        if (h23 != null) {
                            h23.a("commute-time", commuteTimeSetting);
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    }
                }
            } else if (intent != null) {
                String stringExtra = intent.getStringExtra("SEARCH_COMPLICATION_RESULT_ID");
                if (!TextUtils.isEmpty(stringExtra)) {
                    h23 h232 = this.k;
                    if (h232 != null) {
                        kd4.a((Object) stringExtra, "selectedComplicationId");
                        h232.b(stringExtra);
                        return;
                    }
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (i2 == -1 && intent != null) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE");
            if (secondTimezoneSetting != null) {
                h23 h233 = this.k;
                if (h233 != null) {
                    h233.a("second-timezone", secondTimezoneSetting);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ma2 ma2 = (ma2) qa.a(layoutInflater, R.layout.fragment_complications, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new l23(this)).a(this);
        this.j = new tr3<>(this, ma2);
        kd4.a((Object) ma2, "binding");
        return ma2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        h23 h23 = this.k;
        if (h23 != null) {
            h23.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        h23 h23 = this.k;
        if (h23 != null) {
            h23.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        rs2 rs2 = new rs2((ArrayList) null, (rs2.c) null, 3, (fd4) null);
        rs2.a((rs2.c) new b(this));
        this.m = rs2;
        ps2 ps2 = new ps2((ArrayList) null, (ps2.c) null, 3, (fd4) null);
        ps2.a((ps2.c) new c(this));
        this.l = ps2;
        tr3<ma2> tr3 = this.j;
        if (tr3 != null) {
            ma2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.s;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                ps2 ps22 = this.l;
                if (ps22 != null) {
                    recyclerView.setAdapter(ps22);
                    RecyclerView recyclerView2 = a2.t;
                    recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                    rs2 rs22 = this.m;
                    if (rs22 != null) {
                        recyclerView2.setAdapter(rs22);
                        a2.w.setOnClickListener(new d(this));
                        a2.x.setOnClickListener(new e(this));
                        a2.u.setOnClickListener(new f(a2, this));
                        return;
                    }
                    kd4.d("mComplicationAdapter");
                    throw null;
                }
                kd4.d("mCategoriesAdapter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(h23 h23) {
        kd4.b(h23, "presenter");
        this.k = h23;
    }

    @DexIgnore
    public void a(List<Category> list) {
        kd4.b(list, "categories");
        ps2 ps2 = this.l;
        if (ps2 != null) {
            ps2.a(list);
        } else {
            kd4.d("mCategoriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void a(boolean z, String str, String str2, Parcelable parcelable) {
        kd4.b(str, "complicationId");
        kd4.b(str2, "emptySettingRequestContent");
        tr3<ma2> tr3 = this.j;
        if (tr3 != null) {
            ma2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.y;
                kd4.a((Object) flexibleTextView, "it.tvPermissionOrder");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.w;
                kd4.a((Object) flexibleTextView2, "it.tvComplicationPermission");
                flexibleTextView2.setVisibility(8);
                if (z) {
                    ConstraintLayout constraintLayout = a2.q;
                    kd4.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(0);
                    a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    FlexibleTextView flexibleTextView3 = a2.x;
                    kd4.a((Object) flexibleTextView3, "it.tvComplicationSetting");
                    FragmentActivity activity = getActivity();
                    Resources resources = activity != null ? activity.getResources() : null;
                    if (resources != null) {
                        flexibleTextView3.setCompoundDrawablePadding((int) resources.getDimension(R.dimen.dp5));
                        int hashCode = str.hashCode();
                        if (hashCode != -829740640) {
                            if (hashCode == 134170930 && str.equals("second-timezone")) {
                                Switch switchR = a2.u;
                                kd4.a((Object) switchR, "it.switchRing");
                                switchR.setVisibility(8);
                                ImageView imageView = a2.r;
                                kd4.a((Object) imageView, "it.ivComplicationSetting");
                                imageView.setVisibility(8);
                                a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_caret_right, 0);
                                if (parcelable != null) {
                                    FlexibleTextView flexibleTextView4 = a2.x;
                                    kd4.a((Object) flexibleTextView4, "it.tvComplicationSetting");
                                    flexibleTextView4.setText(((SecondTimezoneSetting) parcelable).getTimeZoneName());
                                    return;
                                }
                                FlexibleTextView flexibleTextView5 = a2.x;
                                kd4.a((Object) flexibleTextView5, "it.tvComplicationSetting");
                                flexibleTextView5.setText(str2);
                            }
                        } else if (str.equals("commute-time")) {
                            Switch switchR2 = a2.u;
                            kd4.a((Object) switchR2, "it.switchRing");
                            switchR2.setVisibility(8);
                            ImageView imageView2 = a2.r;
                            kd4.a((Object) imageView2, "it.ivComplicationSetting");
                            imageView2.setVisibility(8);
                            a2.x.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_caret_right, 0);
                            if (parcelable != null) {
                                FlexibleTextView flexibleTextView6 = a2.x;
                                kd4.a((Object) flexibleTextView6, "it.tvComplicationSetting");
                                flexibleTextView6.setText(((CommuteTimeSetting) parcelable).getAddress());
                                return;
                            }
                            FlexibleTextView flexibleTextView7 = a2.x;
                            kd4.a((Object) flexibleTextView7, "it.tvComplicationSetting");
                            flexibleTextView7.setText(str2);
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    ConstraintLayout constraintLayout2 = a2.q;
                    kd4.a((Object) constraintLayout2, "it.clComplicationSetting");
                    constraintLayout2.setVisibility(8);
                    Switch switchR3 = a2.u;
                    kd4.a((Object) switchR3, "it.switchRing");
                    switchR3.setVisibility(8);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str) {
        kd4.b(str, MicroAppSetting.SETTING);
        CommuteTimeSettingsActivity.C.a(this, str);
    }

    @DexIgnore
    public void a(int i, int i2, String str, String str2) {
        kd4.b(str, "title");
        kd4.b(str2, "content");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ComplicationsFragment", "showPermissionRequired current " + i + " total " + i2 + " title " + str + " content " + str2);
            tr3<ma2> tr3 = this.j;
            if (tr3 != null) {
                ma2 a2 = tr3.a();
                if (a2 != null) {
                    Switch switchR = a2.u;
                    kd4.a((Object) switchR, "it.switchRing");
                    switchR.setVisibility(8);
                    ConstraintLayout constraintLayout = a2.q;
                    kd4.a((Object) constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(8);
                    if (i2 == 0 || i == i2) {
                        FlexibleTextView flexibleTextView = a2.w;
                        kd4.a((Object) flexibleTextView, "it.tvComplicationPermission");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = a2.y;
                        kd4.a((Object) flexibleTextView2, "it.tvPermissionOrder");
                        flexibleTextView2.setVisibility(8);
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "it.tvComplicationPermission");
                    flexibleTextView3.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = a2.w;
                    kd4.a((Object) flexibleTextView4, "it.tvComplicationPermission");
                    flexibleTextView4.setText(str);
                    if (str2.length() > 0) {
                        FlexibleTextView flexibleTextView5 = a2.v;
                        kd4.a((Object) flexibleTextView5, "it.tvComplicationDetail");
                        flexibleTextView5.setText(str2);
                    }
                    if (i2 > 1) {
                        FlexibleTextView flexibleTextView6 = a2.y;
                        kd4.a((Object) flexibleTextView6, "it.tvPermissionOrder");
                        flexibleTextView6.setVisibility(0);
                        FlexibleTextView flexibleTextView7 = a2.y;
                        kd4.a((Object) flexibleTextView7, "it.tvPermissionOrder");
                        pd4 pd4 = pd4.a;
                        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.permission_of);
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026  R.string.permission_of)");
                        Object[] objArr = {Integer.valueOf(i), Integer.valueOf(i2)};
                        String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView7.setText(format);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationsFragment", "onPermissionsDenied:" + i + ':' + list.size());
        if (pq4.a((Fragment) this, list)) {
            for (String next : list) {
                int hashCode = next.hashCode();
                if (hashCode != -1888586689) {
                    if (hashCode == 2024715147 && next.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
                        A(InAppPermission.ACCESS_BACKGROUND_LOCATION);
                    }
                } else if (next.equals("android.permission.ACCESS_FINE_LOCATION")) {
                    A(InAppPermission.ACCESS_FINE_LOCATION);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (kd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == R.id.tv_ok) {
                FragmentActivity activity = getActivity();
                if (activity == null) {
                    return;
                }
                if (activity != null) {
                    ((BaseActivity) activity).l();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        } else if (kd4.a((Object) str, (Object) ds3.c.a()) && i == R.id.tv_ok) {
            FragmentActivity activity2 = getActivity();
            if (activity2 == null) {
                return;
            }
            if (!pq4.a((Fragment) this, "android.permission.ACCESS_BACKGROUND_LOCATION")) {
                ns3.a.a((Fragment) this, 200, "android.permission.ACCESS_BACKGROUND_LOCATION");
            } else if (activity2 != null) {
                ((BaseActivity) activity2).l();
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, String str3, String str4) {
        kd4.b(str, "topComplication");
        kd4.b(str2, "bottomComplication");
        kd4.b(str3, "rightComlication");
        kd4.b(str4, "leftComplication");
        ComplicationSearchActivity.C.a(this, str, str2, str4, str3);
    }
}
