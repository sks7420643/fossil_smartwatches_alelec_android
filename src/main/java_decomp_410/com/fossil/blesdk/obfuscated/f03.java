package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f03 implements Factory<NotificationHybridAppPresenter> {
    @DexIgnore
    public static NotificationHybridAppPresenter a(a03 a03, int i, ArrayList<String> arrayList, j62 j62, h03 h03) {
        return new NotificationHybridAppPresenter(a03, i, arrayList, j62, h03);
    }
}
