package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r7 */
public interface C2789r7 {
    @DexIgnore
    /* renamed from: a */
    java.lang.Object mo14640a();

    @DexIgnore
    /* renamed from: a */
    void mo14641a(java.util.Locale... localeArr);

    @DexIgnore
    boolean equals(java.lang.Object obj);

    @DexIgnore
    java.util.Locale get(int i);

    @DexIgnore
    int hashCode();

    @DexIgnore
    java.lang.String toString();
}
