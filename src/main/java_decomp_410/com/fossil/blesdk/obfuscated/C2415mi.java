package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mi */
public class C2415mi extends com.fossil.blesdk.obfuscated.C2810ri implements com.fossil.blesdk.obfuscated.C2327li {

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2415mi.C2417b f7511f;

    @DexIgnore
    /* renamed from: g */
    public android.content.Context f7512g;

    @DexIgnore
    /* renamed from: h */
    public android.animation.ArgbEvaluator f7513h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.graphics.drawable.Drawable.Callback f7514i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mi$a */
    public class C2416a implements android.graphics.drawable.Drawable.Callback {
        @DexIgnore
        public C2416a() {
        }

        @DexIgnore
        public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
            com.fossil.blesdk.obfuscated.C2415mi.this.invalidateSelf();
        }

        @DexIgnore
        public void scheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable, long j) {
            com.fossil.blesdk.obfuscated.C2415mi.this.scheduleSelf(runnable, j);
        }

        @DexIgnore
        public void unscheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable) {
            com.fossil.blesdk.obfuscated.C2415mi.this.unscheduleSelf(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mi$b")
    /* renamed from: com.fossil.blesdk.obfuscated.mi$b */
    public static class C2417b extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public int f7516a;

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2879si f7517b;

        @DexIgnore
        /* renamed from: c */
        public android.animation.AnimatorSet f7518c;

        @DexIgnore
        /* renamed from: d */
        public java.util.ArrayList<android.animation.Animator> f7519d;

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C1855g4<android.animation.Animator, java.lang.String> f7520e;

        @DexIgnore
        public C2417b(android.content.Context context, com.fossil.blesdk.obfuscated.C2415mi.C2417b bVar, android.graphics.drawable.Drawable.Callback callback, android.content.res.Resources resources) {
            if (bVar != null) {
                this.f7516a = bVar.f7516a;
                com.fossil.blesdk.obfuscated.C2879si siVar = bVar.f7517b;
                if (siVar != null) {
                    android.graphics.drawable.Drawable.ConstantState constantState = siVar.getConstantState();
                    if (resources != null) {
                        this.f7517b = (com.fossil.blesdk.obfuscated.C2879si) constantState.newDrawable(resources);
                    } else {
                        this.f7517b = (com.fossil.blesdk.obfuscated.C2879si) constantState.newDrawable();
                    }
                    com.fossil.blesdk.obfuscated.C2879si siVar2 = this.f7517b;
                    siVar2.mutate();
                    this.f7517b = siVar2;
                    this.f7517b.setCallback(callback);
                    this.f7517b.setBounds(bVar.f7517b.getBounds());
                    this.f7517b.mo16008a(false);
                }
                java.util.ArrayList<android.animation.Animator> arrayList = bVar.f7519d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.f7519d = new java.util.ArrayList<>(size);
                    this.f7520e = new com.fossil.blesdk.obfuscated.C1855g4<>(size);
                    for (int i = 0; i < size; i++) {
                        android.animation.Animator animator = bVar.f7519d.get(i);
                        android.animation.Animator clone = animator.clone();
                        java.lang.String str = bVar.f7520e.get(animator);
                        clone.setTarget(this.f7517b.mo16005a(str));
                        this.f7519d.add(clone);
                        this.f7520e.put(clone, str);
                    }
                    mo13662a();
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13662a() {
            if (this.f7518c == null) {
                this.f7518c = new android.animation.AnimatorSet();
            }
            this.f7518c.playTogether(this.f7519d);
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f7516a;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            throw new java.lang.IllegalStateException("No constant state support for SDK < 24.");
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            throw new java.lang.IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mi$c")
    /* renamed from: com.fossil.blesdk.obfuscated.mi$c */
    public static class C2418c extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.drawable.Drawable.ConstantState f7521a;

        @DexIgnore
        public C2418c(android.graphics.drawable.Drawable.ConstantState constantState) {
            this.f7521a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.f7521a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f7521a.getChangingConfigurations();
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            com.fossil.blesdk.obfuscated.C2415mi miVar = new com.fossil.blesdk.obfuscated.C2415mi();
            miVar.f8987e = this.f7521a.newDrawable();
            miVar.f8987e.setCallback(miVar.f7514i);
            return miVar;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            com.fossil.blesdk.obfuscated.C2415mi miVar = new com.fossil.blesdk.obfuscated.C2415mi();
            miVar.f8987e = this.f7521a.newDrawable(resources);
            miVar.f8987e.setCallback(miVar.f7514i);
            return miVar;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources, android.content.res.Resources.Theme theme) {
            com.fossil.blesdk.obfuscated.C2415mi miVar = new com.fossil.blesdk.obfuscated.C2415mi();
            miVar.f8987e = this.f7521a.newDrawable(resources, theme);
            miVar.f8987e.setCallback(miVar.f7514i);
            return miVar;
        }
    }

    @DexIgnore
    public C2415mi() {
        this((android.content.Context) null, (com.fossil.blesdk.obfuscated.C2415mi.C2417b) null, (android.content.res.Resources) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2415mi m10769a(android.content.Context context, android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        com.fossil.blesdk.obfuscated.C2415mi miVar = new com.fossil.blesdk.obfuscated.C2415mi(context);
        miVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return miVar;
    }

    @DexIgnore
    public void applyTheme(android.content.res.Resources.Theme theme) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5300a(drawable, theme);
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5304a(drawable);
        }
        return false;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.f7511f.f7517b.draw(canvas);
        if (this.f7511f.f7518c.isStarted()) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public int getAlpha() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5308c(drawable);
        }
        return this.f7511f.f7517b.getAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f7511f.f7516a;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable.ConstantState getConstantState() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable == null || android.os.Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2415mi.C2418c(drawable.getConstantState());
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.f7511f.f7517b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.f7511f.f7517b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getOpacity() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.f7511f.f7517b.getOpacity();
    }

    @DexIgnore
    public void inflate(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5301a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                java.lang.String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6871e);
                    int resourceId = a.getResourceId(0, 0);
                    if (resourceId != 0) {
                        com.fossil.blesdk.obfuscated.C2879si a2 = com.fossil.blesdk.obfuscated.C2879si.m13737a(resources, resourceId, theme);
                        a2.mo16008a(false);
                        a2.setCallback(this.f7514i);
                        com.fossil.blesdk.obfuscated.C2879si siVar = this.f7511f.f7517b;
                        if (siVar != null) {
                            siVar.setCallback((android.graphics.drawable.Drawable.Callback) null);
                        }
                        this.f7511f.f7517b = a2;
                    }
                    a.recycle();
                } else if ("target".equals(name)) {
                    android.content.res.TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6872f);
                    java.lang.String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        android.content.Context context = this.f7512g;
                        if (context != null) {
                            mo13634a(string, com.fossil.blesdk.obfuscated.C2570oi.m11792a(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new java.lang.IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f7511f.mo13662a();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5311f(drawable);
        }
        return this.f7511f.f7517b.isAutoMirrored();
    }

    @DexIgnore
    public boolean isRunning() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return ((android.graphics.drawable.AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.f7511f.f7518c.isRunning();
    }

    @DexIgnore
    public boolean isStateful() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.f7511f.f7517b.isStateful();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.f7511f.f7517b.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setLevel(i);
        }
        return this.f7511f.f7517b.setLevel(i);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.f7511f.f7517b.setState(iArr);
    }

    @DexIgnore
    public void setAlpha(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else {
            this.f7511f.f7517b.setAlpha(i);
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5303a(drawable, z);
        } else {
            this.f7511f.f7517b.setAutoMirrored(z);
        }
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.f7511f.f7517b.setColorFilter(colorFilter);
        }
    }

    @DexIgnore
    public void setTint(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5307b(drawable, i);
        } else {
            this.f7511f.f7517b.setTint(i);
        }
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5299a(drawable, colorStateList);
        } else {
            this.f7511f.f7517b.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5302a(drawable, mode);
        } else {
            this.f7511f.f7517b.setTintMode(mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.f7511f.f7517b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            ((android.graphics.drawable.AnimatedVectorDrawable) drawable).start();
        } else if (!this.f7511f.f7518c.isStarted()) {
            this.f7511f.f7518c.start();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void stop() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            ((android.graphics.drawable.AnimatedVectorDrawable) drawable).stop();
        } else {
            this.f7511f.f7518c.end();
        }
    }

    @DexIgnore
    public C2415mi(android.content.Context context) {
        this(context, (com.fossil.blesdk.obfuscated.C2415mi.C2417b) null, (android.content.res.Resources) null);
    }

    @DexIgnore
    public C2415mi(android.content.Context context, com.fossil.blesdk.obfuscated.C2415mi.C2417b bVar, android.content.res.Resources resources) {
        this.f7513h = null;
        this.f7514i = new com.fossil.blesdk.obfuscated.C2415mi.C2416a();
        this.f7512g = context;
        if (bVar != null) {
            this.f7511f = bVar;
        } else {
            this.f7511f = new com.fossil.blesdk.obfuscated.C2415mi.C2417b(context, bVar, this.f7514i, resources);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13633a(android.animation.Animator animator) {
        if (animator instanceof android.animation.AnimatorSet) {
            java.util.ArrayList<android.animation.Animator> childAnimations = ((android.animation.AnimatorSet) animator).getChildAnimations();
            if (childAnimations != null) {
                for (int i = 0; i < childAnimations.size(); i++) {
                    mo13633a(childAnimations.get(i));
                }
            }
        }
        if (animator instanceof android.animation.ObjectAnimator) {
            android.animation.ObjectAnimator objectAnimator = (android.animation.ObjectAnimator) animator;
            java.lang.String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.f7513h == null) {
                    this.f7513h = new android.animation.ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.f7513h);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13634a(java.lang.String str, android.animation.Animator animator) {
        animator.setTarget(this.f7511f.f7517b.mo16005a(str));
        if (android.os.Build.VERSION.SDK_INT < 21) {
            mo13633a(animator);
        }
        com.fossil.blesdk.obfuscated.C2415mi.C2417b bVar = this.f7511f;
        if (bVar.f7519d == null) {
            bVar.f7519d = new java.util.ArrayList<>();
            this.f7511f.f7520e = new com.fossil.blesdk.obfuscated.C1855g4<>();
        }
        this.f7511f.f7519d.add(animator);
        this.f7511f.f7520e.put(animator, str);
    }

    @DexIgnore
    public void inflate(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        inflate(resources, xmlPullParser, attributeSet, (android.content.res.Resources.Theme) null);
    }
}
