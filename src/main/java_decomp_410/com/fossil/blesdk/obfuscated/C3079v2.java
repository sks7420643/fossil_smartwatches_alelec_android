package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v2 */
public interface C3079v2 extends android.widget.SpinnerAdapter {
    @DexIgnore
    android.content.res.Resources.Theme getDropDownViewTheme();

    @DexIgnore
    void setDropDownViewTheme(android.content.res.Resources.Theme theme);
}
