package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jn */
public class C2142jn extends com.android.volley.Request<android.graphics.Bitmap> {
    @DexIgnore
    public static /* final */ float DEFAULT_IMAGE_BACKOFF_MULT; // = 2.0f;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_MAX_RETRIES; // = 2;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_TIMEOUT_MS; // = 1000;
    @DexIgnore
    public static /* final */ java.lang.Object sDecodeLock; // = new java.lang.Object();
    @DexIgnore
    public /* final */ android.graphics.Bitmap.Config mDecodeConfig;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C3047um.C3049b<android.graphics.Bitmap> mListener;
    @DexIgnore
    public /* final */ java.lang.Object mLock;
    @DexIgnore
    public /* final */ int mMaxHeight;
    @DexIgnore
    public /* final */ int mMaxWidth;
    @DexIgnore
    public /* final */ android.widget.ImageView.ScaleType mScaleType;

    @DexIgnore
    public C2142jn(java.lang.String str, com.fossil.blesdk.obfuscated.C3047um.C3049b<android.graphics.Bitmap> bVar, int i, int i2, android.widget.ImageView.ScaleType scaleType, android.graphics.Bitmap.Config config, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        super(0, str, aVar);
        this.mLock = new java.lang.Object();
        setRetryPolicy(new com.fossil.blesdk.obfuscated.C2496nm(1000, 2, 2.0f));
        this.mListener = bVar;
        this.mDecodeConfig = config;
        this.mMaxWidth = i;
        this.mMaxHeight = i2;
        this.mScaleType = scaleType;
    }

    @DexIgnore
    private com.fossil.blesdk.obfuscated.C3047um<android.graphics.Bitmap> doParse(com.fossil.blesdk.obfuscated.C2897sm smVar) {
        android.graphics.Bitmap bitmap;
        byte[] bArr = smVar.f9391b;
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        if (this.mMaxWidth == 0 && this.mMaxHeight == 0) {
            options.inPreferredConfig = this.mDecodeConfig;
            bitmap = android.graphics.BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            android.graphics.BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int resizedDimension = getResizedDimension(this.mMaxWidth, this.mMaxHeight, i, i2, this.mScaleType);
            int resizedDimension2 = getResizedDimension(this.mMaxHeight, this.mMaxWidth, i2, i, this.mScaleType);
            options.inJustDecodeBounds = false;
            options.inSampleSize = findBestSampleSize(i, i2, resizedDimension, resizedDimension2);
            bitmap = android.graphics.BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (bitmap != null && (bitmap.getWidth() > resizedDimension || bitmap.getHeight() > resizedDimension2)) {
                android.graphics.Bitmap createScaledBitmap = android.graphics.Bitmap.createScaledBitmap(bitmap, resizedDimension, resizedDimension2, true);
                bitmap.recycle();
                bitmap = createScaledBitmap;
            }
        }
        if (bitmap == null) {
            return com.fossil.blesdk.obfuscated.C3047um.m14805a(new com.android.volley.ParseError(smVar));
        }
        return com.fossil.blesdk.obfuscated.C3047um.m14806a(bitmap, com.fossil.blesdk.obfuscated.C1738en.m6518a(smVar));
    }

    @DexIgnore
    public static int findBestSampleSize(int i, int i2, int i3, int i4) {
        double min = java.lang.Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4));
        float f = 1.0f;
        while (true) {
            float f2 = 2.0f * f;
            if (((double) f2) > min) {
                return (int) f;
            }
            f = f2;
        }
    }

    @DexIgnore
    public static int getResizedDimension(int i, int i2, int i3, int i4, android.widget.ImageView.ScaleType scaleType) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (scaleType == android.widget.ImageView.ScaleType.FIT_XY) {
            return i == 0 ? i3 : i;
        }
        if (i == 0) {
            return (int) (((double) i3) * (((double) i2) / ((double) i4)));
        } else if (i2 == 0) {
            return i;
        } else {
            double d = ((double) i4) / ((double) i3);
            if (scaleType == android.widget.ImageView.ScaleType.CENTER_CROP) {
                double d2 = (double) i2;
                return ((double) i) * d < d2 ? (int) (d2 / d) : i;
            }
            double d3 = (double) i2;
            return ((double) i) * d > d3 ? (int) (d3 / d) : i;
        }
    }

    @DexIgnore
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public com.android.volley.Request.Priority getPriority() {
        return com.android.volley.Request.Priority.LOW;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C3047um<android.graphics.Bitmap> parseNetworkResponse(com.fossil.blesdk.obfuscated.C2897sm smVar) {
        com.fossil.blesdk.obfuscated.C3047um<android.graphics.Bitmap> doParse;
        synchronized (sDecodeLock) {
            try {
                doParse = doParse(smVar);
            } catch (java.lang.OutOfMemoryError e) {
                com.fossil.blesdk.obfuscated.C3296xm.m16422c("Caught OOM for %d byte image, url=%s", java.lang.Integer.valueOf(smVar.f9391b.length), getUrl());
                return com.fossil.blesdk.obfuscated.C3047um.m14805a(new com.android.volley.ParseError((java.lang.Throwable) e));
            } catch (Throwable th) {
                throw th;
            }
        }
        return doParse;
    }

    @DexIgnore
    public void deliverResponse(android.graphics.Bitmap bitmap) {
        com.fossil.blesdk.obfuscated.C3047um.C3049b<android.graphics.Bitmap> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(bitmap);
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    public C2142jn(java.lang.String str, com.fossil.blesdk.obfuscated.C3047um.C3049b<android.graphics.Bitmap> bVar, int i, int i2, android.graphics.Bitmap.Config config, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        this(str, bVar, i, i2, android.widget.ImageView.ScaleType.CENTER_INSIDE, config, aVar);
    }
}
