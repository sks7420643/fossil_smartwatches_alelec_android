package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cf */
public class C1551cf {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1551cf.C1553b f4079a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1551cf.C1552a f4080b; // = new com.fossil.blesdk.obfuscated.C1551cf.C1552a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cf$a */
    public static class C1552a {

        @DexIgnore
        /* renamed from: a */
        public int f4081a; // = 0;

        @DexIgnore
        /* renamed from: b */
        public int f4082b;

        @DexIgnore
        /* renamed from: c */
        public int f4083c;

        @DexIgnore
        /* renamed from: d */
        public int f4084d;

        @DexIgnore
        /* renamed from: e */
        public int f4085e;

        @DexIgnore
        /* renamed from: a */
        public int mo9474a(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9476a(int i, int i2, int i3, int i4) {
            this.f4082b = i;
            this.f4083c = i2;
            this.f4084d = i3;
            this.f4085e = i4;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo9478b() {
            this.f4081a = 0;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9475a(int i) {
            this.f4081a = i | this.f4081a;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo9477a() {
            int i = this.f4081a;
            if ((i & 7) != 0 && (i & (mo9474a(this.f4084d, this.f4082b) << 0)) == 0) {
                return false;
            }
            int i2 = this.f4081a;
            if ((i2 & 112) != 0 && (i2 & (mo9474a(this.f4084d, this.f4083c) << 4)) == 0) {
                return false;
            }
            int i3 = this.f4081a;
            if ((i3 & 1792) != 0 && (i3 & (mo9474a(this.f4085e, this.f4082b) << 8)) == 0) {
                return false;
            }
            int i4 = this.f4081a;
            if ((i4 & 28672) == 0 || (i4 & (mo9474a(this.f4085e, this.f4083c) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.cf$b */
    public interface C1553b {
        @DexIgnore
        /* renamed from: a */
        int mo2978a();

        @DexIgnore
        /* renamed from: a */
        int mo2979a(android.view.View view);

        @DexIgnore
        /* renamed from: a */
        android.view.View mo2980a(int i);

        @DexIgnore
        /* renamed from: b */
        int mo2981b();

        @DexIgnore
        /* renamed from: b */
        int mo2982b(android.view.View view);
    }

    @DexIgnore
    public C1551cf(com.fossil.blesdk.obfuscated.C1551cf.C1553b bVar) {
        this.f4079a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo9472a(int i, int i2, int i3, int i4) {
        int a = this.f4079a.mo2978a();
        int b = this.f4079a.mo2981b();
        int i5 = i2 > i ? 1 : -1;
        android.view.View view = null;
        while (i != i2) {
            android.view.View a2 = this.f4079a.mo2980a(i);
            this.f4080b.mo9476a(a, b, this.f4079a.mo2979a(a2), this.f4079a.mo2982b(a2));
            if (i3 != 0) {
                this.f4080b.mo9478b();
                this.f4080b.mo9475a(i3);
                if (this.f4080b.mo9477a()) {
                    return a2;
                }
            }
            if (i4 != 0) {
                this.f4080b.mo9478b();
                this.f4080b.mo9475a(i4);
                if (this.f4080b.mo9477a()) {
                    view = a2;
                }
            }
            i += i5;
        }
        return view;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9473a(android.view.View view, int i) {
        this.f4080b.mo9476a(this.f4079a.mo2978a(), this.f4079a.mo2981b(), this.f4079a.mo2979a(view), this.f4079a.mo2982b(view));
        if (i == 0) {
            return false;
        }
        this.f4080b.mo9478b();
        this.f4080b.mo9475a(i);
        return this.f4080b.mo9477a();
    }
}
