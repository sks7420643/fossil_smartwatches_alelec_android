package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kp */
public final class C2235kp implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f6935b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f6936c;

    @DexIgnore
    public C2235kp(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2143jo joVar2) {
        this.f6935b = joVar;
        this.f6936c = joVar2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        this.f6935b.mo8934a(messageDigest);
        this.f6936c.mo8934a(messageDigest);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2235kp)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2235kp kpVar = (com.fossil.blesdk.obfuscated.C2235kp) obj;
        if (!this.f6935b.equals(kpVar.f6935b) || !this.f6936c.equals(kpVar.f6936c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f6935b.hashCode() * 31) + this.f6936c.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "DataCacheKey{sourceKey=" + this.f6935b + ", signature=" + this.f6936c + '}';
    }
}
