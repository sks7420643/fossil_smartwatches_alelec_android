package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.de0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yf0 extends cg0 {
    @DexIgnore
    public /* final */ ArrayList<de0.f> f;
    @DexIgnore
    public /* final */ /* synthetic */ sf0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yf0(sf0 sf0, ArrayList<de0.f> arrayList) {
        super(sf0, (tf0) null);
        this.g = sf0;
        this.f = arrayList;
    }

    @DexIgnore
    public final void a() {
        this.g.a.r.q = this.g.i();
        ArrayList<de0.f> arrayList = this.f;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            de0.f fVar = arrayList.get(i);
            i++;
            fVar.a(this.g.o, this.g.a.r.q);
        }
    }
}
