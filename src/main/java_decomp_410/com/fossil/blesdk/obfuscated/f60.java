package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class f60 extends Phase {
    @DexIgnore
    public /* final */ short A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f60(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, short s, String str) {
        super(peripheral, aVar, phaseId, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(phaseId, "id");
        kd4.b(str, "phaseUuid");
        this.A = s;
    }

    @DexIgnore
    public final short A() {
        return this.A;
    }

    @DexIgnore
    public boolean a(Phase phase) {
        kd4.b(phase, "otherPhase");
        return !(phase instanceof f60) || ((f60) phase).A != this.A;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.FILE_HANDLE, n90.a(this.A)), JSONKey.FILE_HANDLE_DESCRIPTION, FileHandle.Companion.a(this.A));
    }

    @DexIgnore
    public boolean a(Request request) {
        RequestId f = request != null ? request.f() : null;
        return f != null && e60.a[f.ordinal()] == 1;
    }
}
