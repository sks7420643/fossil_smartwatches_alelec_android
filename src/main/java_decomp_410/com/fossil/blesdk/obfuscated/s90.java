package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s90 extends GattOperationResult {
    @DexIgnore
    public /* final */ BluetoothDevice b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s90(GattOperationResult.GattResult gattResult, BluetoothDevice bluetoothDevice) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        kd4.b(bluetoothDevice, "device");
        this.b = bluetoothDevice;
    }

    @DexIgnore
    public final BluetoothDevice b() {
        return this.b;
    }
}
