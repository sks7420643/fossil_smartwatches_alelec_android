package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jp */
public class C2144jp implements com.fossil.blesdk.obfuscated.C2428mp, com.fossil.blesdk.obfuscated.C2902so.C2903a<java.lang.Object> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2143jo> f6541e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2499np<?> f6542f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2428mp.C2429a f6543g;

    @DexIgnore
    /* renamed from: h */
    public int f6544h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2143jo f6545i;

    @DexIgnore
    /* renamed from: j */
    public java.util.List<com.fossil.blesdk.obfuscated.C2912sr<java.io.File, ?>> f6546j;

    @DexIgnore
    /* renamed from: k */
    public int f6547k;

    @DexIgnore
    /* renamed from: l */
    public volatile com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> f6548l;

    @DexIgnore
    /* renamed from: m */
    public java.io.File f6549m;

    @DexIgnore
    public C2144jp(com.fossil.blesdk.obfuscated.C2499np<?> npVar, com.fossil.blesdk.obfuscated.C2428mp.C2429a aVar) {
        this(npVar.mo14061c(), npVar, aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9253a() {
        while (true) {
            boolean z = false;
            if (this.f6546j == null || !mo12401b()) {
                this.f6544h++;
                if (this.f6544h >= this.f6541e.size()) {
                    return false;
                }
                com.fossil.blesdk.obfuscated.C2143jo joVar = this.f6541e.get(this.f6544h);
                this.f6549m = this.f6542f.mo14063d().mo16535a(new com.fossil.blesdk.obfuscated.C2235kp(joVar, this.f6542f.mo14071l()));
                java.io.File file = this.f6549m;
                if (file != null) {
                    this.f6545i = joVar;
                    this.f6546j = this.f6542f.mo14054a(file);
                    this.f6547k = 0;
                }
            } else {
                this.f6548l = null;
                while (!z && mo12401b()) {
                    java.util.List<com.fossil.blesdk.obfuscated.C2912sr<java.io.File, ?>> list = this.f6546j;
                    int i = this.f6547k;
                    this.f6547k = i + 1;
                    this.f6548l = list.get(i).mo8911a(this.f6549m, this.f6542f.mo14073n(), this.f6542f.mo14065f(), this.f6542f.mo14068i());
                    if (this.f6548l != null && this.f6542f.mo14062c(this.f6548l.f9451c.getDataClass())) {
                        this.f6548l.f9451c.mo8870a(this.f6542f.mo14069j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo12401b() {
        return this.f6547k < this.f6546j.size();
    }

    @DexIgnore
    public void cancel() {
        com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> aVar = this.f6548l;
        if (aVar != null) {
            aVar.f9451c.cancel();
        }
    }

    @DexIgnore
    public C2144jp(java.util.List<com.fossil.blesdk.obfuscated.C2143jo> list, com.fossil.blesdk.obfuscated.C2499np<?> npVar, com.fossil.blesdk.obfuscated.C2428mp.C2429a aVar) {
        this.f6544h = -1;
        this.f6541e = list;
        this.f6542f = npVar;
        this.f6543g = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9252a(java.lang.Object obj) {
        this.f6543g.mo3952a(this.f6545i, obj, this.f6548l.f9451c, com.bumptech.glide.load.DataSource.DATA_DISK_CACHE, this.f6545i);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9251a(java.lang.Exception exc) {
        this.f6543g.mo3951a(this.f6545i, exc, this.f6548l.f9451c, com.bumptech.glide.load.DataSource.DATA_DISK_CACHE);
    }
}
