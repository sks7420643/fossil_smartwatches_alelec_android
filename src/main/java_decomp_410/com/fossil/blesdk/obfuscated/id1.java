package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class id1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<id1> CREATOR; // = new jd1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public id1(int i, int i2, long j, long j2) {
        this.e = i;
        this.f = i2;
        this.g = j;
        this.h = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && id1.class == obj.getClass()) {
            id1 id1 = (id1) obj;
            return this.e == id1.e && this.f == id1.f && this.g == id1.g && this.h == id1.h;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(Integer.valueOf(this.f), Integer.valueOf(this.e), Long.valueOf(this.h), Long.valueOf(this.g));
    }

    @DexIgnore
    public final String toString() {
        return "NetworkLocationStatus:" + " Wifi status: " + this.e + " Cell status: " + this.f + " elapsed time NS: " + this.h + " system time ms: " + this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f);
        kk0.a(parcel, 3, this.g);
        kk0.a(parcel, 4, this.h);
        kk0.a(parcel, a);
    }
}
