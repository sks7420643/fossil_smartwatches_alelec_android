package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hu1 {
    @DexIgnore
    public abstract Object delegate();

    @DexIgnore
    public String toString() {
        return delegate().toString();
    }
}
