package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mp */
public interface C2428mp {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.mp$a */
    public interface C2429a {
        @DexIgnore
        /* renamed from: a */
        void mo3951a(com.fossil.blesdk.obfuscated.C2143jo joVar, java.lang.Exception exc, com.fossil.blesdk.obfuscated.C2902so<?> soVar, com.bumptech.glide.load.DataSource dataSource);

        @DexIgnore
        /* renamed from: a */
        void mo3952a(com.fossil.blesdk.obfuscated.C2143jo joVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2902so<?> soVar, com.bumptech.glide.load.DataSource dataSource, com.fossil.blesdk.obfuscated.C2143jo joVar2);

        @DexIgnore
        /* renamed from: j */
        void mo3960j();
    }

    @DexIgnore
    /* renamed from: a */
    boolean mo9253a();

    @DexIgnore
    void cancel();
}
