package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wr2 {
    @DexIgnore
    public static /* final */ a a; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            int i2 = (int) (((double) i) * 1.15d);
            if (i2 >= 0 && 9 >= i2) {
                if (i2 % 2 != 0) {
                    return ((i2 / 2) * 2) + 2;
                }
                return i2;
            } else if (10 <= i2 && 99 >= i2) {
                if (i2 % 10 != 0) {
                    return ((i2 / 10) * 10) + 10;
                }
                return i2;
            } else if (100 > i2 || 999 < i2) {
                return i2 % 200 != 0 ? ((i2 / 200) * 200) + 200 : i2;
            } else {
                if (i2 % 20 != 0) {
                    return ((i2 / 20) * 20) + 20;
                }
                return i2;
            }
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final ArrayList<String> a(Context context, BarChart.c cVar) {
            kd4.b(context, "context");
            kd4.b(cVar, "chartModel");
            ArrayList<String> arrayList = new ArrayList<>();
            Calendar instance = Calendar.getInstance();
            for (BarChart.a aVar : cVar.a()) {
                if (aVar.b() > 0) {
                    kd4.a((Object) instance, "calendar");
                    instance.setTimeInMillis(aVar.b());
                    Boolean s = rk2.s(instance.getTime());
                    kd4.a((Object) s, "DateHelper.isToday(calendar.time)");
                    if (!s.booleanValue()) {
                        switch (instance.get(7)) {
                            case 1:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__S));
                                break;
                            case 2:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__M));
                                break;
                            case 3:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__T));
                                break;
                            case 4:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__W));
                                break;
                            case 5:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1));
                                break;
                            case 6:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__F));
                                break;
                            case 7:
                                arrayList.add(sm2.a(context, (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1));
                                break;
                        }
                    } else {
                        arrayList.add(sm2.a(context, (int) R.string.DashboardDiana_Main_Sleep7days_Label__Today));
                    }
                } else {
                    arrayList.add("");
                }
            }
            return arrayList;
        }
    }
}
