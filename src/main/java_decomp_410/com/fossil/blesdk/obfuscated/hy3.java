package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.gy3;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hy3 {
    @DexIgnore
    public static /* final */ AtomicInteger m; // = new AtomicInteger();
    @DexIgnore
    public /* final */ Picasso a;
    @DexIgnore
    public /* final */ gy3.b b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Drawable j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public Object l;

    @DexIgnore
    public hy3(Picasso picasso, Uri uri, int i2) {
        if (!picasso.o) {
            this.a = picasso;
            this.b = new gy3.b(uri, i2, picasso.l);
            return;
        }
        throw new IllegalStateException("Picasso instance already shut down. Cannot submit new requests.");
    }

    @DexIgnore
    public hy3 a(int i2) {
        if (i2 == 0) {
            throw new IllegalArgumentException("Error image resource invalid.");
        } else if (this.k == null) {
            this.g = i2;
            return this;
        } else {
            throw new IllegalStateException("Error image already set.");
        }
    }

    @DexIgnore
    public hy3 b(int i2) {
        if (!this.e) {
            throw new IllegalStateException("Already explicitly declared as no placeholder.");
        } else if (i2 == 0) {
            throw new IllegalArgumentException("Placeholder image resource invalid.");
        } else if (this.j == null) {
            this.f = i2;
            return this;
        } else {
            throw new IllegalStateException("Placeholder image already set.");
        }
    }

    @DexIgnore
    public hy3 c() {
        this.d = false;
        return this;
    }

    @DexIgnore
    public hy3 a(int i2, int i3) {
        this.b.a(i2, i3);
        return this;
    }

    @DexIgnore
    public hy3 a(Transformation transformation) {
        this.b.a(transformation);
        return this;
    }

    @DexIgnore
    public void a(Target target) {
        long nanoTime = System.nanoTime();
        oy3.a();
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.d) {
            Drawable drawable = null;
            if (!this.b.b()) {
                this.a.a(target);
                if (this.e) {
                    drawable = a();
                }
                target.onPrepareLoad(drawable);
                return;
            }
            gy3 a2 = a(nanoTime);
            String a3 = oy3.a(a2);
            if (MemoryPolicy.shouldReadFromMemoryCache(this.h)) {
                Bitmap b2 = this.a.b(a3);
                if (b2 != null) {
                    this.a.a(target);
                    target.onBitmapLoaded(b2, Picasso.LoadedFrom.MEMORY);
                    return;
                }
            }
            if (this.e) {
                drawable = a();
            }
            target.onPrepareLoad(drawable);
            this.a.a((qx3) new my3(this.a, target, a2, this.h, this.i, this.k, a3, this.l, this.g));
        } else {
            throw new IllegalStateException("Fit cannot be used with a Target.");
        }
    }

    @DexIgnore
    public hy3 b() {
        this.c = true;
        return this;
    }

    @DexIgnore
    public void a(ImageView imageView) {
        a(imageView, (ux3) null);
    }

    @DexIgnore
    public void a(ImageView imageView, ux3 ux3) {
        ImageView imageView2 = imageView;
        ux3 ux32 = ux3;
        long nanoTime = System.nanoTime();
        oy3.a();
        if (imageView2 == null) {
            throw new IllegalArgumentException("Target must not be null.");
        } else if (!this.b.b()) {
            this.a.a(imageView2);
            if (this.e) {
                ey3.a(imageView2, a());
            }
        } else {
            if (this.d) {
                if (!this.b.c()) {
                    int width = imageView.getWidth();
                    int height = imageView.getHeight();
                    if (width == 0 || height == 0) {
                        if (this.e) {
                            ey3.a(imageView2, a());
                        }
                        this.a.a(imageView2, new xx3(this, imageView2, ux32));
                        return;
                    }
                    this.b.a(width, height);
                } else {
                    throw new IllegalStateException("Fit cannot be used with resize.");
                }
            }
            gy3 a2 = a(nanoTime);
            String a3 = oy3.a(a2);
            if (MemoryPolicy.shouldReadFromMemoryCache(this.h)) {
                Bitmap b2 = this.a.b(a3);
                if (b2 != null) {
                    this.a.a(imageView2);
                    Picasso picasso = this.a;
                    ey3.a(imageView, picasso.e, b2, Picasso.LoadedFrom.MEMORY, this.c, picasso.m);
                    if (this.a.n) {
                        String g2 = a2.g();
                        oy3.a("Main", "completed", g2, "from " + Picasso.LoadedFrom.MEMORY);
                    }
                    if (ux32 != null) {
                        ux3.onSuccess();
                        return;
                    }
                    return;
                }
            }
            if (this.e) {
                ey3.a(imageView2, a());
            }
            this.a.a((qx3) new ay3(this.a, imageView, a2, this.h, this.i, this.g, this.k, a3, this.l, ux3, this.c));
        }
    }

    @DexIgnore
    public final Drawable a() {
        if (this.f != 0) {
            return this.a.e.getResources().getDrawable(this.f);
        }
        return this.j;
    }

    @DexIgnore
    public final gy3 a(long j2) {
        int andIncrement = m.getAndIncrement();
        gy3 a2 = this.b.a();
        a2.a = andIncrement;
        a2.b = j2;
        boolean z = this.a.n;
        if (z) {
            oy3.a("Main", "created", a2.g(), a2.toString());
        }
        gy3 a3 = this.a.a(a2);
        if (a3 != a2) {
            a3.a = andIncrement;
            a3.b = j2;
            if (z) {
                String d2 = a3.d();
                oy3.a("Main", "changed", d2, "into " + a3);
            }
        }
        return a3;
    }
}
