package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.FeatureError;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.error.Error;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g90<V> extends v00<V, Error> {
    @DexIgnore
    public g90<V> d(xc4<? super Float, qa4> xc4) {
        kd4.b(xc4, "actionOnProgressChange");
        super.d(xc4);
        return this;
    }

    @DexIgnore
    public g90<V> f(xc4<? super qa4, qa4> xc4) {
        kd4.b(xc4, "actionOnFinal");
        super.a(xc4);
        return this;
    }

    @DexIgnore
    /* renamed from: g */
    public g90<V> c(xc4<? super Error, qa4> xc4) {
        kd4.b(xc4, "actionOnError");
        super.c(xc4);
        return this;
    }

    @DexIgnore
    /* renamed from: h */
    public g90<V> e(xc4<? super V, qa4> xc4) {
        kd4.b(xc4, "actionOnSuccess");
        super.e(xc4);
        return this;
    }

    @DexIgnore
    public g90<V> b(xc4<? super Error, qa4> xc4) {
        kd4.b(xc4, "actionOnCancel");
        super.b(xc4);
        return this;
    }

    @DexIgnore
    public final void e() {
        super.a(new FeatureError(FeatureErrorCode.INTERRUPTED, (Phase.Result) null, 2, (fd4) null));
    }
}
