package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ac0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ac0> CREATOR; // = new dc0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Bundle g;

    @DexIgnore
    public ac0(int i, int i2, Bundle bundle) {
        this.e = i;
        this.f = i2;
        this.g = bundle;
    }

    @DexIgnore
    public int H() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, H());
        kk0.a(parcel, 3, this.g, false);
        kk0.a(parcel, a);
    }
}
