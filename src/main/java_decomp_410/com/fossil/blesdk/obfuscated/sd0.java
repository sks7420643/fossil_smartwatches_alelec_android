package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sd0 implements Parcelable.Creator<rd0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        uy0 uy0 = null;
        byte[] bArr = null;
        int[] iArr = null;
        String[] strArr = null;
        int[] iArr2 = null;
        byte[][] bArr2 = null;
        hm1[] hm1Arr = null;
        boolean z = true;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    uy0 = SafeParcelReader.a(parcel, a, uy0.CREATOR);
                    break;
                case 3:
                    bArr = SafeParcelReader.b(parcel, a);
                    break;
                case 4:
                    iArr = SafeParcelReader.e(parcel, a);
                    break;
                case 5:
                    strArr = SafeParcelReader.g(parcel, a);
                    break;
                case 6:
                    iArr2 = SafeParcelReader.e(parcel, a);
                    break;
                case 7:
                    bArr2 = SafeParcelReader.c(parcel, a);
                    break;
                case 8:
                    z = SafeParcelReader.i(parcel, a);
                    break;
                case 9:
                    hm1Arr = SafeParcelReader.b(parcel, a, hm1.CREATOR);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new rd0(uy0, bArr, iArr, strArr, iArr2, bArr2, z, hm1Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new rd0[i];
    }
}
