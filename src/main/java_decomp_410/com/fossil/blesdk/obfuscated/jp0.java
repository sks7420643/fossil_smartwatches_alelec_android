package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jp0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<jp0> CREATOR; // = new kp0();
    @DexIgnore
    public static /* final */ jp0 g; // = new jp0("com.google.android.gms", (String) null);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public jp0(String str, String str2) {
        bk0.a(str);
        this.e = str;
        this.f = str2;
    }

    @DexIgnore
    public static jp0 e(String str) {
        if ("com.google.android.gms".equals(str)) {
            return g;
        }
        return new jp0(str, (String) null);
    }

    @DexIgnore
    public final String H() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jp0)) {
            return false;
        }
        jp0 jp0 = (jp0) obj;
        return this.e.equals(jp0.e) && zj0.a(this.f, jp0.f);
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.e, this.f);
    }

    @DexIgnore
    public final String toString() {
        return String.format("Application{%s:%s}", new Object[]{this.e, this.f});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e, false);
        kk0.a(parcel, 3, this.f, false);
        kk0.a(parcel, a);
    }
}
