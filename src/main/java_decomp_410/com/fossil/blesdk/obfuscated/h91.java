package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface h91 extends List {
    @DexIgnore
    h91 C();

    @DexIgnore
    List<?> E();

    @DexIgnore
    void a(zzte zzte);

    @DexIgnore
    Object d(int i);
}
