package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s42 implements Factory<ApplicationEventListener> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> b;
    @DexIgnore
    public /* final */ Provider<en2> c;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> f;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> g;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> h;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> i;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> j;
    @DexIgnore
    public /* final */ Provider<UserRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<vj2> m;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> n;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> o;

    @DexIgnore
    public s42(n42 n42, Provider<PortfolioApp> provider, Provider<en2> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<vj2> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
        this.n = provider13;
        this.o = provider14;
    }

    @DexIgnore
    public static s42 a(n42 n42, Provider<PortfolioApp> provider, Provider<en2> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<vj2> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14) {
        return new s42(n42, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14);
    }

    @DexIgnore
    public static ApplicationEventListener b(n42 n42, Provider<PortfolioApp> provider, Provider<en2> provider2, Provider<HybridPresetRepository> provider3, Provider<CategoryRepository> provider4, Provider<WatchAppRepository> provider5, Provider<ComplicationRepository> provider6, Provider<MicroAppRepository> provider7, Provider<DianaPresetRepository> provider8, Provider<DeviceRepository> provider9, Provider<UserRepository> provider10, Provider<AlarmsRepository> provider11, Provider<vj2> provider12, Provider<WatchFaceRepository> provider13, Provider<WatchLocalizationRepository> provider14) {
        return a(n42, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get(), provider12.get(), provider13.get(), provider14.get());
    }

    @DexIgnore
    public static ApplicationEventListener a(n42 n42, PortfolioApp portfolioApp, en2 en2, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, DeviceRepository deviceRepository, UserRepository userRepository, AlarmsRepository alarmsRepository, vj2 vj2, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository) {
        ApplicationEventListener a2 = n42.a(portfolioApp, en2, hybridPresetRepository, categoryRepository, watchAppRepository, complicationRepository, microAppRepository, dianaPresetRepository, deviceRepository, userRepository, alarmsRepository, vj2, watchFaceRepository, watchLocalizationRepository);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ApplicationEventListener get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o);
    }
}
