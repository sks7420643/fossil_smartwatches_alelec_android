package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ld */
public abstract class C2307ld<Key, Value> {
    @DexIgnore
    public java.util.concurrent.atomic.AtomicBoolean mInvalid; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    public java.util.concurrent.CopyOnWriteArrayList<com.fossil.blesdk.obfuscated.C2307ld.C2311c> mOnInvalidatedCallbacks; // = new java.util.concurrent.CopyOnWriteArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ld$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ld$a */
    public static class C2308a implements com.fossil.blesdk.obfuscated.C2374m3<java.util.List<X>, java.util.List<Y>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2374m3 f7180a;

        @DexIgnore
        public C2308a(com.fossil.blesdk.obfuscated.C2374m3 m3Var) {
            this.f7180a = m3Var;
        }

        @DexIgnore
        /* renamed from: a */
        public java.util.List<Y> apply(java.util.List<X> list) {
            java.util.ArrayList arrayList = new java.util.ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.f7180a.apply(list.get(i)));
            }
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ld$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ld$b */
    public static abstract class C2309b<Key, Value> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ld$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ld$b$a */
        public class C2310a extends com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, ToValue> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2374m3 f7181a;

            @DexIgnore
            public C2310a(com.fossil.blesdk.obfuscated.C2374m3 m3Var) {
                this.f7181a = m3Var;
            }

            @DexIgnore
            public com.fossil.blesdk.obfuscated.C2307ld<Key, ToValue> create() {
                return com.fossil.blesdk.obfuscated.C2307ld.C2309b.this.create().mapByPage(this.f7181a);
            }
        }

        @DexIgnore
        public abstract com.fossil.blesdk.obfuscated.C2307ld<Key, Value> create();

        @DexIgnore
        public <ToValue> com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, ToValue> map(com.fossil.blesdk.obfuscated.C2374m3<Value, ToValue> m3Var) {
            return mapByPage(com.fossil.blesdk.obfuscated.C2307ld.createListFunction(m3Var));
        }

        @DexIgnore
        public <ToValue> com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, ToValue> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<Value>, java.util.List<ToValue>> m3Var) {
            return new com.fossil.blesdk.obfuscated.C2307ld.C2309b.C2310a(m3Var);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ld$c */
    public interface C2311c {
        @DexIgnore
        /* renamed from: a */
        void mo13244a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ld$d")
    /* renamed from: com.fossil.blesdk.obfuscated.ld$d */
    public static class C2312d<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f7183a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld f7184b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> f7185c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ java.lang.Object f7186d; // = new java.lang.Object();

        @DexIgnore
        /* renamed from: e */
        public java.util.concurrent.Executor f7187e; // = null;

        @DexIgnore
        /* renamed from: f */
        public boolean f7188f; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ld$d$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ld$d$a */
        public class C2313a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2644pd f7189e;

            @DexIgnore
            public C2313a(com.fossil.blesdk.obfuscated.C2644pd pdVar) {
                this.f7189e = pdVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C2307ld.C2312d dVar = com.fossil.blesdk.obfuscated.C2307ld.C2312d.this;
                dVar.f7185c.mo12730a(dVar.f7183a, this.f7189e);
            }
        }

        @DexIgnore
        public C2312d(com.fossil.blesdk.obfuscated.C2307ld ldVar, int i, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> aVar) {
            this.f7184b = ldVar;
            this.f7183a = i;
            this.f7187e = executor;
            this.f7185c = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public static void m10136a(java.util.List<?> list, int i, int i2) {
            if (i < 0) {
                throw new java.lang.IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new java.lang.IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new java.lang.IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13246a(java.util.concurrent.Executor executor) {
            synchronized (this.f7186d) {
                this.f7187e = executor;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13247a() {
            if (!this.f7184b.isInvalid()) {
                return false;
            }
            mo13245a(com.fossil.blesdk.obfuscated.C2644pd.m12218c());
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13245a(com.fossil.blesdk.obfuscated.C2644pd<T> pdVar) {
            java.util.concurrent.Executor executor;
            synchronized (this.f7186d) {
                if (!this.f7188f) {
                    this.f7188f = true;
                    executor = this.f7187e;
                } else {
                    throw new java.lang.IllegalStateException("callback.onResult already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new com.fossil.blesdk.obfuscated.C2307ld.C2312d.C2313a(pdVar));
            } else {
                this.f7185c.mo12730a(this.f7183a, pdVar);
            }
        }
    }

    @DexIgnore
    public static <A, B> java.util.List<B> convert(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> m3Var, java.util.List<A> list) {
        java.util.List<B> apply = m3Var.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new java.lang.IllegalStateException("Invalid Function " + m3Var + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> com.fossil.blesdk.obfuscated.C2374m3<java.util.List<X>, java.util.List<Y>> createListFunction(com.fossil.blesdk.obfuscated.C2374m3<X, Y> m3Var) {
        return new com.fossil.blesdk.obfuscated.C2307ld.C2308a(m3Var);
    }

    @DexIgnore
    public void addInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.mOnInvalidatedCallbacks.add(cVar);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2307ld.C2311c> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().mo13244a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> com.fossil.blesdk.obfuscated.C2307ld<Key, ToValue> map(com.fossil.blesdk.obfuscated.C2374m3<Value, ToValue> m3Var);

    @DexIgnore
    public abstract <ToValue> com.fossil.blesdk.obfuscated.C2307ld<Key, ToValue> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<Value>, java.util.List<ToValue>> m3Var);

    @DexIgnore
    public void removeInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.mOnInvalidatedCallbacks.remove(cVar);
    }
}
