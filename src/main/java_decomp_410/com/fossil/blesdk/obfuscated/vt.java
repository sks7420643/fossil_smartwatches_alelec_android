package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vt implements no<ut> {
    @DexIgnore
    public EncodeStrategy a(lo loVar) {
        return EncodeStrategy.SOURCE;
    }

    @DexIgnore
    public boolean a(aq<ut> aqVar, File file, lo loVar) {
        try {
            kw.a(aqVar.get().c(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
