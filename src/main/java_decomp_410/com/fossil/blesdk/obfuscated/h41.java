package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h41 extends x31 {
    @DexIgnore
    public ue0<vc1> e;

    @DexIgnore
    public h41(ue0<vc1> ue0) {
        bk0.a(ue0 != null, (Object) "listener can't be null.");
        this.e = ue0;
    }

    @DexIgnore
    public final void a(vc1 vc1) throws RemoteException {
        this.e.a(vc1);
        this.e = null;
    }
}
