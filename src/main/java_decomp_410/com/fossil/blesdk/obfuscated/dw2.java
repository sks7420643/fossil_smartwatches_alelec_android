package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ot2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dw2 extends as2 implements cw2, ws3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public bw2 k;
    @DexIgnore
    public tr3<qd2> l;
    @DexIgnore
    public ix2 m;
    @DexIgnore
    public ot2 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return dw2.p;
        }

        @DexIgnore
        public final dw2 b() {
            return new dw2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ot2.b {
        @DexIgnore
        public /* final */ /* synthetic */ dw2 a;

        @DexIgnore
        public b(dw2 dw2) {
            this.a = dw2;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            kd4.b(appWrapper, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = dw2.q.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            dw2.b(this.a).a(appWrapper, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dw2 e;

        @DexIgnore
        public c(dw2 dw2) {
            this.e = dw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(dw2.q.a(), "press on close button");
            dw2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ dw2 a;

        @DexIgnore
        public d(dw2 dw2) {
            this.a = dw2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            dw2.b(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ dw2 e;
        @DexIgnore
        public /* final */ /* synthetic */ qd2 f;

        @DexIgnore
        public e(dw2 dw2, qd2 qd2) {
            this.e = dw2;
            this.f = qd2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.r;
            kd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            dw2.a(this.e).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qd2 e;

        @DexIgnore
        public f(qd2 qd2) {
            this.e = qd2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = dw2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationAppsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ot2 a(dw2 dw2) {
        ot2 ot2 = dw2.n;
        if (ot2 != null) {
            return ot2;
        }
        kd4.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ bw2 b(dw2 dw2) {
        bw2 bw2 = dw2.k;
        if (bw2 != null) {
            return bw2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d(p, "onActivityBackPressed -");
        bw2 bw2 = this.k;
        if (bw2 != null) {
            bw2.h();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void g(List<AppWrapper> list) {
        kd4.b(list, "listAppWrapper");
        ot2 ot2 = this.n;
        if (ot2 != null) {
            ot2.a(list);
        } else {
            kd4.d("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void h(boolean z) {
        tr3<qd2> tr3 = this.l;
        if (tr3 != null) {
            qd2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.u;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                aVar.a(context, PortfolioApp.W.c().e());
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        qd2 qd2 = (qd2) qa.a(layoutInflater, R.layout.fragment_notification_apps, viewGroup, false, O0());
        qd2.s.setOnClickListener(new c(this));
        qd2.u.setOnCheckedChangeListener(new d(this));
        qd2.q.addTextChangedListener(new e(this, qd2));
        qd2.r.setOnClickListener(new f(qd2));
        ot2 ot2 = new ot2();
        ot2.a((ot2.b) new b(this));
        this.n = ot2;
        this.m = (ix2) getChildFragmentManager().a(ix2.t.a());
        if (this.m == null) {
            this.m = ix2.t.b();
        }
        RecyclerView recyclerView = qd2.t;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        ot2 ot22 = this.n;
        if (ot22 != null) {
            recyclerView.setAdapter(ot22);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((ke) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                l42 g = PortfolioApp.W.c().g();
                ix2 ix2 = this.m;
                if (ix2 != null) {
                    g.a(new kx2(ix2)).a(this);
                    this.l = new tr3<>(this, qd2);
                    R("app_notification_view");
                    kd4.a((Object) qd2, "binding");
                    return qd2.d();
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        kd4.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        bw2 bw2 = this.k;
        if (bw2 != null) {
            bw2.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        bw2 bw2 = this.k;
        if (bw2 != null) {
            bw2.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(bw2 bw2) {
        kd4.b(bw2, "presenter");
        this.k = bw2;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i == R.id.tv_ok) {
                bw2 bw2 = this.k;
                if (bw2 != null) {
                    bw2.i();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i == R.id.fb_try_again) {
                bw2 bw22 = this.k;
                if (bw22 != null) {
                    bw22.h();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else if (i != R.id.ftv_contact_cs) {
                if (i == R.id.iv_close) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.C;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }
}
