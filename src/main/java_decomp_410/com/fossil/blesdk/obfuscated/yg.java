package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yg implements zg {
    @DexIgnore
    public static Class<?> f;
    @DexIgnore
    public static boolean g;
    @DexIgnore
    public static Method h;
    @DexIgnore
    public static boolean i;
    @DexIgnore
    public static Method j;
    @DexIgnore
    public static boolean k;
    @DexIgnore
    public /* final */ View e;

    @DexIgnore
    public yg(View view) {
        this.e = view;
    }

    @DexIgnore
    public static zg a(View view, ViewGroup viewGroup, Matrix matrix) {
        a();
        Method method = h;
        if (method != null) {
            try {
                return new yg((View) method.invoke((Object) null, new Object[]{view, viewGroup, matrix}));
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return null;
    }

    @DexIgnore
    public static void b() {
        if (!g) {
            try {
                f = Class.forName("android.view.GhostView");
            } catch (ClassNotFoundException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve GhostView class", e2);
            }
            g = true;
        }
    }

    @DexIgnore
    public static void c() {
        if (!k) {
            try {
                b();
                j = f.getDeclaredMethod("removeGhost", new Class[]{View.class});
                j.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", e2);
            }
            k = true;
        }
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, View view) {
    }

    @DexIgnore
    public void setVisibility(int i2) {
        this.e.setVisibility(i2);
    }

    @DexIgnore
    public static void a(View view) {
        c();
        Method method = j;
        if (method != null) {
            try {
                method.invoke((Object) null, new Object[]{view});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    @DexIgnore
    public static void a() {
        if (!i) {
            try {
                b();
                h = f.getDeclaredMethod("addGhost", new Class[]{View.class, ViewGroup.class, Matrix.class});
                h.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve addGhost method", e2);
            }
            i = true;
        }
    }
}
