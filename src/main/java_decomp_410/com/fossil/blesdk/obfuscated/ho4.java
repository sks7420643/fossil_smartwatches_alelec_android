package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ho4 extends zo4 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static ho4 j;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ho4 f;
    @DexIgnore
    public long g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements xo4 {
        @DexIgnore
        public /* final */ /* synthetic */ xo4 e;

        @DexIgnore
        public a(xo4 xo4) {
            this.e = xo4;
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            ap4.a(jo4.f, 0, j);
            while (true) {
                long j2 = 0;
                if (j > 0) {
                    vo4 vo4 = jo4.e;
                    while (true) {
                        if (j2 >= 65536) {
                            break;
                        }
                        j2 += (long) (vo4.c - vo4.b);
                        if (j2 >= j) {
                            j2 = j;
                            break;
                        }
                        vo4 = vo4.f;
                    }
                    ho4.this.g();
                    try {
                        this.e.a(jo4, j2);
                        j -= j2;
                        ho4.this.a(true);
                    } catch (IOException e2) {
                        throw ho4.this.a(e2);
                    } catch (Throwable th) {
                        ho4.this.a(false);
                        throw th;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public zo4 b() {
            return ho4.this;
        }

        @DexIgnore
        public void close() throws IOException {
            ho4.this.g();
            try {
                this.e.close();
                ho4.this.a(true);
            } catch (IOException e2) {
                throw ho4.this.a(e2);
            } catch (Throwable th) {
                ho4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            ho4.this.g();
            try {
                this.e.flush();
                ho4.this.a(true);
            } catch (IOException e2) {
                throw ho4.this.a(e2);
            } catch (Throwable th) {
                ho4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.e + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Thread {
        @DexIgnore
        public c() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r1.i();
         */
        @DexIgnore
        public void run() {
            while (true) {
                try {
                    synchronized (ho4.class) {
                        ho4 j = ho4.j();
                        if (j != null) {
                            if (j == ho4.j) {
                                ho4.j = null;
                                return;
                            }
                        }
                    }
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    @DexIgnore
    public static synchronized void a(ho4 ho4, long j2, boolean z) {
        Class<ho4> cls = ho4.class;
        synchronized (cls) {
            if (j == null) {
                j = new ho4();
                new c().start();
            }
            long nanoTime = System.nanoTime();
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (i2 != 0 && z) {
                ho4.g = Math.min(j2, ho4.c() - nanoTime) + nanoTime;
            } else if (i2 != 0) {
                ho4.g = j2 + nanoTime;
            } else if (z) {
                ho4.g = ho4.c();
            } else {
                throw new AssertionError();
            }
            long b2 = ho4.b(nanoTime);
            ho4 ho42 = j;
            while (true) {
                if (ho42.f == null) {
                    break;
                } else if (b2 < ho42.f.b(nanoTime)) {
                    break;
                } else {
                    ho42 = ho42.f;
                }
            }
            ho4.f = ho42.f;
            ho42.f = ho4;
            if (ho42 == j) {
                cls.notify();
            }
        }
    }

    @DexIgnore
    public static ho4 j() throws InterruptedException {
        Class<ho4> cls = ho4.class;
        ho4 ho4 = j.f;
        if (ho4 == null) {
            long nanoTime = System.nanoTime();
            cls.wait(h);
            if (j.f != null || System.nanoTime() - nanoTime < i) {
                return null;
            }
            return j;
        }
        long b2 = ho4.b(System.nanoTime());
        if (b2 > 0) {
            long j2 = b2 / 1000000;
            cls.wait(j2, (int) (b2 - (1000000 * j2)));
            return null;
        }
        j.f = ho4.f;
        ho4.f = null;
        return ho4;
    }

    @DexIgnore
    public final long b(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final void g() {
        if (!this.e) {
            long f2 = f();
            boolean d = d();
            if (f2 != 0 || d) {
                this.e = true;
                a(this, f2, d);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit");
    }

    @DexIgnore
    public final boolean h() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return a(this);
    }

    @DexIgnore
    public void i() {
    }

    @DexIgnore
    public IOException b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements yo4 {
        @DexIgnore
        public /* final */ /* synthetic */ yo4 e;

        @DexIgnore
        public b(yo4 yo4) {
            this.e = yo4;
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            ho4.this.g();
            try {
                long b = this.e.b(jo4, j);
                ho4.this.a(true);
                return b;
            } catch (IOException e2) {
                throw ho4.this.a(e2);
            } catch (Throwable th) {
                ho4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            try {
                this.e.close();
                ho4.this.a(true);
            } catch (IOException e2) {
                throw ho4.this.a(e2);
            } catch (Throwable th) {
                ho4.this.a(false);
                throw th;
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.e + ")";
        }

        @DexIgnore
        public zo4 b() {
            return ho4.this;
        }
    }

    @DexIgnore
    public static synchronized boolean a(ho4 ho4) {
        synchronized (ho4.class) {
            for (ho4 ho42 = j; ho42 != null; ho42 = ho42.f) {
                if (ho42.f == ho4) {
                    ho42.f = ho4.f;
                    ho4.f = null;
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final xo4 a(xo4 xo4) {
        return new a(xo4);
    }

    @DexIgnore
    public final yo4 a(yo4 yo4) {
        return new b(yo4);
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        if (h() && z) {
            throw b((IOException) null);
        }
    }

    @DexIgnore
    public final IOException a(IOException iOException) throws IOException {
        if (!h()) {
            return iOException;
        }
        return b(iOException);
    }
}
