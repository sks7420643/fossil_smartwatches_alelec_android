package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class js<T> implements aq<T> {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public js(T t) {
        tw.a(t);
        this.e = t;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final int b() {
        return 1;
    }

    @DexIgnore
    public Class<T> c() {
        return this.e.getClass();
    }

    @DexIgnore
    public final T get() {
        return this.e;
    }
}
