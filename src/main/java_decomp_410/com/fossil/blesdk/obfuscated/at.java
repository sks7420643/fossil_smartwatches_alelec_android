package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class at extends qs {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(jo.a);

    @DexIgnore
    public Bitmap a(jq jqVar, Bitmap bitmap, int i, int i2) {
        return gt.c(jqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof at;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.FitCenter".hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
