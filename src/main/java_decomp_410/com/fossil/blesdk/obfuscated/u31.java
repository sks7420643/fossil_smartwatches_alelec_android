package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface u31 extends IInterface {
    @DexIgnore
    void a(k41 k41) throws RemoteException;

    @DexIgnore
    void a(tc1 tc1, w31 w31, String str) throws RemoteException;

    @DexIgnore
    void a(v41 v41) throws RemoteException;

    @DexIgnore
    Location b(String str) throws RemoteException;

    @DexIgnore
    void e(boolean z) throws RemoteException;
}
