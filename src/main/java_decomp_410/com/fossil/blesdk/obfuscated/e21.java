package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface e21<DP, DT> {
    @DexIgnore
    double a(DP dp, int i);

    @DexIgnore
    long a(DP dp, TimeUnit timeUnit);

    @DexIgnore
    int b(DP dp, int i);

    @DexIgnore
    boolean c(DP dp, int i);

    @DexIgnore
    DT zza(DP dp);

    @DexIgnore
    f21<DT> zzb();

    @DexIgnore
    String zzb(DP dp);
}
