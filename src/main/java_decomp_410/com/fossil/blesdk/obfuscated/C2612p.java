package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p */
public interface C2612p extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p$a")
    /* renamed from: com.fossil.blesdk.obfuscated.p$a */
    public static abstract class C2613a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C2612p {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.p$a$a */
        public static class C2614a implements com.fossil.blesdk.obfuscated.C2612p {

            @DexIgnore
            /* renamed from: e */
            public android.os.IBinder f8260e;

            @DexIgnore
            public C2614a(android.os.IBinder iBinder) {
                this.f8260e = iBinder;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo14570a(int i, android.os.Bundle bundle) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.os.IResultReceiver");
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f8260e.transact(1, obtain, (android.os.Parcel) null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            @DexIgnore
            public android.os.IBinder asBinder() {
                return this.f8260e;
            }
        }

        @DexIgnore
        public C2613a() {
            attachInterface(this, "android.support.v4.os.IResultReceiver");
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2612p m12030a(android.os.IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            android.os.IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof com.fossil.blesdk.obfuscated.C2612p)) {
                return new com.fossil.blesdk.obfuscated.C2612p.C2613a.C2614a(iBinder);
            }
            return (com.fossil.blesdk.obfuscated.C2612p) queryLocalInterface;
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
                mo14570a(parcel.readInt(), parcel.readInt() != 0 ? (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo14570a(int i, android.os.Bundle bundle) throws android.os.RemoteException;
}
