package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.iw1;
import com.google.firebase.components.MissingDependencyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rw1 extends nw1 {
    @DexIgnore
    public /* final */ List<iw1<?>> a;
    @DexIgnore
    public /* final */ Map<Class<?>, vw1<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ tw1 c;

    @DexIgnore
    public rw1(Executor executor, Iterable<lw1> iterable, iw1<?>... iw1Arr) {
        this.c = new tw1(executor);
        ArrayList arrayList = new ArrayList();
        arrayList.add(iw1.a(this.c, tw1.class, bx1.class, ax1.class));
        for (lw1 components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, iw1Arr);
        this.a = Collections.unmodifiableList(iw1.a.a((List<iw1<?>>) arrayList));
        for (iw1<?> a2 : this.a) {
            a(a2);
        }
        a();
    }

    @DexIgnore
    public final void a(boolean z) {
        for (iw1 next : this.a) {
            if (next.e() || (next.f() && z)) {
                a((Class) next.a().iterator().next());
            }
        }
        this.c.a();
    }

    @DexIgnore
    public final <T> ez1<T> b(Class<T> cls) {
        bk0.a(cls, (Object) "Null interface requested.");
        return this.b.get(cls);
    }

    @DexIgnore
    public final <T> void a(iw1<T> iw1) {
        vw1 vw1 = new vw1(iw1.c(), new xw1(iw1, this));
        for (Class<? super T> put : iw1.a()) {
            this.b.put(put, vw1);
        }
    }

    @DexIgnore
    public final void a() {
        for (iw1 next : this.a) {
            Iterator<mw1> it = next.b().iterator();
            while (true) {
                if (it.hasNext()) {
                    mw1 next2 = it.next();
                    if (next2.b() && !this.b.containsKey(next2.a())) {
                        throw new MissingDependencyException(String.format("Unsatisfied dependency for component %s: %s", new Object[]{next, next2.a()}));
                    }
                }
            }
        }
    }
}
