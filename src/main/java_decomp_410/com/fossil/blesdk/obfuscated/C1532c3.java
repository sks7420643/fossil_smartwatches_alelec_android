package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c3 */
public class C1532c3 implements android.view.View.OnLongClickListener, android.view.View.OnHoverListener, android.view.View.OnAttachStateChangeListener {

    @DexIgnore
    /* renamed from: n */
    public static com.fossil.blesdk.obfuscated.C1532c3 f3987n;

    @DexIgnore
    /* renamed from: o */
    public static com.fossil.blesdk.obfuscated.C1532c3 f3988o;

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.view.View f3989e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.CharSequence f3990f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f3991g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.lang.Runnable f3992h; // = new com.fossil.blesdk.obfuscated.C1532c3.C1533a();

    @DexIgnore
    /* renamed from: i */
    public /* final */ java.lang.Runnable f3993i; // = new com.fossil.blesdk.obfuscated.C1532c3.C1534b();

    @DexIgnore
    /* renamed from: j */
    public int f3994j;

    @DexIgnore
    /* renamed from: k */
    public int f3995k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1594d3 f3996l;

    @DexIgnore
    /* renamed from: m */
    public boolean f3997m;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.c3$a */
    public class C1533a implements java.lang.Runnable {
        @DexIgnore
        public C1533a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1532c3.this.mo9417a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c3$b")
    /* renamed from: com.fossil.blesdk.obfuscated.c3$b */
    public class C1534b implements java.lang.Runnable {
        @DexIgnore
        public C1534b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1532c3.this.mo9420c();
        }
    }

    @DexIgnore
    public C1532c3(android.view.View view, java.lang.CharSequence charSequence) {
        this.f3989e = view;
        this.f3990f = charSequence;
        this.f3991g = com.fossil.blesdk.obfuscated.C1863g9.m7310a(android.view.ViewConfiguration.get(this.f3989e.getContext()));
        mo9419b();
        this.f3989e.setOnLongClickListener(this);
        this.f3989e.setOnHoverListener(this);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5261a(android.view.View view, java.lang.CharSequence charSequence) {
        com.fossil.blesdk.obfuscated.C1532c3 c3Var = f3987n;
        if (c3Var != null && c3Var.f3989e == view) {
            m5262a((com.fossil.blesdk.obfuscated.C1532c3) null);
        }
        if (android.text.TextUtils.isEmpty(charSequence)) {
            com.fossil.blesdk.obfuscated.C1532c3 c3Var2 = f3988o;
            if (c3Var2 != null && c3Var2.f3989e == view) {
                c3Var2.mo9420c();
            }
            view.setOnLongClickListener((android.view.View.OnLongClickListener) null);
            view.setLongClickable(false);
            view.setOnHoverListener((android.view.View.OnHoverListener) null);
            return;
        }
        new com.fossil.blesdk.obfuscated.C1532c3(view, charSequence);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo9419b() {
        this.f3994j = Integer.MAX_VALUE;
        this.f3995k = Integer.MAX_VALUE;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9420c() {
        if (f3988o == this) {
            f3988o = null;
            com.fossil.blesdk.obfuscated.C1594d3 d3Var = this.f3996l;
            if (d3Var != null) {
                d3Var.mo9743a();
                this.f3996l = null;
                mo9419b();
                this.f3989e.removeOnAttachStateChangeListener(this);
            } else {
                android.util.Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (f3987n == this) {
            m5262a((com.fossil.blesdk.obfuscated.C1532c3) null);
        }
        this.f3989e.removeCallbacks(this.f3993i);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo9421d() {
        this.f3989e.postDelayed(this.f3992h, (long) android.view.ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public boolean onHover(android.view.View view, android.view.MotionEvent motionEvent) {
        if (this.f3996l != null && this.f3997m) {
            return false;
        }
        android.view.accessibility.AccessibilityManager accessibilityManager = (android.view.accessibility.AccessibilityManager) this.f3989e.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                mo9419b();
                mo9420c();
            }
        } else if (this.f3989e.isEnabled() && this.f3996l == null && mo9418a(motionEvent)) {
            m5262a(this);
        }
        return false;
    }

    @DexIgnore
    public boolean onLongClick(android.view.View view) {
        this.f3994j = view.getWidth() / 2;
        this.f3995k = view.getHeight() / 2;
        mo9417a(true);
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(android.view.View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(android.view.View view) {
        mo9420c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9417a(boolean z) {
        long j;
        int i;
        long j2;
        if (com.fossil.blesdk.obfuscated.C1776f9.m6859y(this.f3989e)) {
            m5262a((com.fossil.blesdk.obfuscated.C1532c3) null);
            com.fossil.blesdk.obfuscated.C1532c3 c3Var = f3988o;
            if (c3Var != null) {
                c3Var.mo9420c();
            }
            f3988o = this;
            this.f3997m = z;
            this.f3996l = new com.fossil.blesdk.obfuscated.C1594d3(this.f3989e.getContext());
            this.f3996l.mo9745a(this.f3989e, this.f3994j, this.f3995k, this.f3997m, this.f3990f);
            this.f3989e.addOnAttachStateChangeListener(this);
            if (this.f3997m) {
                j = 2500;
            } else {
                if ((com.fossil.blesdk.obfuscated.C1776f9.m6853s(this.f3989e) & 1) == 1) {
                    j2 = 3000;
                    i = android.view.ViewConfiguration.getLongPressTimeout();
                } else {
                    j2 = 15000;
                    i = android.view.ViewConfiguration.getLongPressTimeout();
                }
                j = j2 - ((long) i);
            }
            this.f3989e.removeCallbacks(this.f3993i);
            this.f3989e.postDelayed(this.f3993i, j);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5262a(com.fossil.blesdk.obfuscated.C1532c3 c3Var) {
        com.fossil.blesdk.obfuscated.C1532c3 c3Var2 = f3987n;
        if (c3Var2 != null) {
            c3Var2.mo9416a();
        }
        f3987n = c3Var;
        com.fossil.blesdk.obfuscated.C1532c3 c3Var3 = f3987n;
        if (c3Var3 != null) {
            c3Var3.mo9421d();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9416a() {
        this.f3989e.removeCallbacks(this.f3992h);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo9418a(android.view.MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (java.lang.Math.abs(x - this.f3994j) <= this.f3991g && java.lang.Math.abs(y - this.f3995k) <= this.f3991g) {
            return false;
        }
        this.f3994j = x;
        this.f3995k = y;
        return true;
    }
}
