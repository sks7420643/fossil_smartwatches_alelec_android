package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ei */
public class C1732ei extends com.fossil.blesdk.obfuscated.C1634di {

    @DexIgnore
    /* renamed from: l */
    public static java.lang.reflect.Method f4840l;

    @DexIgnore
    /* renamed from: m */
    public static boolean f4841m;

    @DexIgnore
    /* renamed from: a */
    public void mo10528a(android.view.View view, int i, int i2, int i3, int i4) {
        mo10529f();
        java.lang.reflect.Method method = f4840l;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(i2), java.lang.Integer.valueOf(i3), java.lang.Integer.valueOf(i4)});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    @android.annotation.SuppressLint({"PrivateApi"})
    /* renamed from: f */
    public final void mo10529f() {
        if (!f4841m) {
            try {
                f4840l = android.view.View.class.getDeclaredMethod("setLeftTopRightBottom", new java.lang.Class[]{java.lang.Integer.TYPE, java.lang.Integer.TYPE, java.lang.Integer.TYPE, java.lang.Integer.TYPE});
                f4840l.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi22", "Failed to retrieve setLeftTopRightBottom method", e);
            }
            f4841m = true;
        }
    }
}
