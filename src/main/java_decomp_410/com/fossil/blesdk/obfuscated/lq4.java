package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lq4 implements mq4 {
    @DexIgnore
    public static /* final */ lq4 b; // = new lq4();
    @DexIgnore
    public static String c; // = "1.6.99";
    @DexIgnore
    public static /* final */ String d; // = hq4.class.getName();
    @DexIgnore
    public /* final */ bq4 a; // = new hq4();

    @DexIgnore
    public static final lq4 c() {
        return b;
    }

    @DexIgnore
    public bq4 a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return d;
    }
}
