package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kj */
public abstract class C2224kj {

    @DexIgnore
    /* renamed from: a */
    public java.util.UUID f6882a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1954hl f6883b;

    @DexIgnore
    /* renamed from: c */
    public java.util.Set<java.lang.String> f6884c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kj$a */
    public static abstract class C2225a<B extends com.fossil.blesdk.obfuscated.C2224kj.C2225a, W extends com.fossil.blesdk.obfuscated.C2224kj> {

        @DexIgnore
        /* renamed from: a */
        public boolean f6885a; // = false;

        @DexIgnore
        /* renamed from: b */
        public java.util.UUID f6886b; // = java.util.UUID.randomUUID();

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1954hl f6887c;

        @DexIgnore
        /* renamed from: d */
        public java.util.Set<java.lang.String> f6888d; // = new java.util.HashSet();

        @DexIgnore
        public C2225a(java.lang.Class<? extends androidx.work.ListenableWorker> cls) {
            this.f6887c = new com.fossil.blesdk.obfuscated.C1954hl(this.f6886b.toString(), cls.getName());
            mo12810a(cls.getName());
        }

        @DexIgnore
        /* renamed from: a */
        public final B mo12809a(com.fossil.blesdk.obfuscated.C3365yi yiVar) {
            this.f6887c.f5780j = yiVar;
            mo10532c();
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public abstract W mo10531b();

        @DexIgnore
        /* renamed from: c */
        public abstract B mo10532c();

        @DexIgnore
        /* renamed from: a */
        public final B mo12810a(java.lang.String str) {
            this.f6888d.add(str);
            mo10532c();
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public final W mo12811a() {
            W b = mo10531b();
            this.f6886b = java.util.UUID.randomUUID();
            this.f6887c = new com.fossil.blesdk.obfuscated.C1954hl(this.f6887c);
            this.f6887c.f5771a = this.f6886b.toString();
            return b;
        }
    }

    @DexIgnore
    public C2224kj(java.util.UUID uuid, com.fossil.blesdk.obfuscated.C1954hl hlVar, java.util.Set<java.lang.String> set) {
        this.f6882a = uuid;
        this.f6883b = hlVar;
        this.f6884c = set;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.UUID mo12805a() {
        return this.f6882a;
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo12806b() {
        return this.f6882a.toString();
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.Set<java.lang.String> mo12807c() {
        return this.f6884c;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1954hl mo12808d() {
        return this.f6883b;
    }
}
