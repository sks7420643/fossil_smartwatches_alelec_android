package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class te2 extends se2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v; // = new SparseIntArray();
    @DexIgnore
    public long t;

    /*
    static {
        v.put(R.id.iv_close, 1);
        v.put(R.id.progressBar, 2);
        v.put(R.id.ftv_title, 3);
        v.put(R.id.iv_device_image, 4);
        v.put(R.id.ftv_desc, 5);
    }
    */

    @DexIgnore
    public te2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 6, u, v));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public te2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[0], objArr[5], objArr[3], objArr[1], objArr[4], objArr[2]);
        this.t = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
