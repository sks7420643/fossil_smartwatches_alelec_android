package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface p1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(h1 h1Var, boolean z);

        @DexIgnore
        boolean a(h1 h1Var);
    }

    @DexIgnore
    void a(Context context, h1 h1Var);

    @DexIgnore
    void a(Parcelable parcelable);

    @DexIgnore
    void a(h1 h1Var, boolean z);

    @DexIgnore
    void a(a aVar);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(h1 h1Var, k1 k1Var);

    @DexIgnore
    boolean a(v1 v1Var);

    @DexIgnore
    Parcelable b();

    @DexIgnore
    boolean b(h1 h1Var, k1 k1Var);

    @DexIgnore
    int getId();
}
