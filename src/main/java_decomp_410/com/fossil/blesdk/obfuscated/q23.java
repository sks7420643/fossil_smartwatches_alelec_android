package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.adapter.SecondTimeZoneSearchAdapter;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q23 extends zr2 implements t33 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<kf2> j;
    @DexIgnore
    public s33 k;
    @DexIgnore
    public SecondTimeZoneSearchAdapter l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final q23 a() {
            return new q23();
        }

        @DexIgnore
        public final String b() {
            return q23.n;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements SecondTimeZoneSearchAdapter.b {
        @DexIgnore
        public /* final */ /* synthetic */ q23 a;

        @DexIgnore
        public b(q23 q23) {
            this.a = q23;
        }

        @DexIgnore
        public void a(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = q23.o.b();
            local.d(b, "onItemClick position=" + i);
            SecondTimeZoneSearchAdapter a2 = this.a.l;
            if (a2 != null) {
                SecondTimeZoneSearchAdapter.SecondTimeZoneSearchModel secondTimeZoneSearchModel = a2.b().get(i);
                if (secondTimeZoneSearchModel.b() == SecondTimeZoneSearchAdapter.SecondTimeZoneSearchModel.TYPE.TYPE_VALUE) {
                    SecondTimezoneSetting c = secondTimeZoneSearchModel.c();
                    if (c != null) {
                        c.setTimezoneOffset(ConversionUtils.getTimezoneRawOffsetById(c.getTimeZoneId()));
                        Intent intent = new Intent();
                        intent.putExtra("SECOND_TIMEZONE", c);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            activity.setResult(-1, intent);
                        }
                        FragmentActivity activity2 = this.a.getActivity();
                        if (activity2 != null) {
                            activity2.finish();
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewAlphabetIndex.b {
        @DexIgnore
        public /* final */ /* synthetic */ q23 a;
        @DexIgnore
        public /* final */ /* synthetic */ kf2 b;

        @DexIgnore
        public c(q23 q23, kf2 kf2) {
            this.a = q23;
            this.b = kf2;
        }

        @DexIgnore
        public void a(View view, int i, String str) {
            kd4.b(view, "view");
            kd4.b(str, "character");
            SecondTimeZoneSearchAdapter a2 = this.a.l;
            Integer valueOf = a2 != null ? Integer.valueOf(a2.b(str)) : null;
            if (valueOf != null && valueOf.intValue() != -1) {
                RecyclerView recyclerView = this.b.v;
                kd4.a((Object) recyclerView, "binding.timezoneRecyclerView");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    ((LinearLayoutManager) layoutManager).f(valueOf.intValue(), 0);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;
        @DexIgnore
        public /* final */ /* synthetic */ kf2 f;

        @DexIgnore
        public d(q23 q23, kf2 kf2) {
            this.e = q23;
            this.f = kf2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FLogger.INSTANCE.getLocal().d(q23.o.b(), "afterTextChanged s=" + editable);
            SecondTimeZoneSearchAdapter a = this.e.l;
            if (a != null) {
                String valueOf = String.valueOf(editable);
                int length = valueOf.length() - 1;
                int i = 0;
                boolean z = false;
                while (i <= length) {
                    boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                a.a(valueOf.subSequence(i, length + 1).toString());
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = q23.o.b();
            local.d(b, "beforeTextChanged s=" + charSequence + " start=" + i + " count=" + i2 + " after=" + i3);
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = q23.o.b();
            local.d(b, "onTextChanged s=" + charSequence + " start=" + i + " before=" + i2 + " count=" + i3);
            ImageView imageView = this.f.r;
            kd4.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(!TextUtils.isEmpty(charSequence) ? 0 : 4);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ q23 e;

        @DexIgnore
        public e(q23 q23) {
            this.e = q23;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kf2 e;

        @DexIgnore
        public f(kf2 kf2) {
            this.e = kf2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.u.setText("");
        }
    }

    /*
    static {
        String simpleName = q23.class.getSimpleName();
        kd4.a((Object) simpleName, "SearchSecondTimezoneFrag\u2026nt::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void I(String str) {
        kd4.b(str, "cityName");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "cityName=" + str);
        tr3<kf2> tr3 = this.j;
        if (tr3 != null) {
            kf2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        kf2 kf2 = (kf2) qa.a(layoutInflater, R.layout.fragment_search_second_timezone, viewGroup, false, O0());
        SecondTimeZoneSearchAdapter secondTimeZoneSearchAdapter = new SecondTimeZoneSearchAdapter();
        secondTimeZoneSearchAdapter.a((SecondTimeZoneSearchAdapter.b) new b(this));
        this.l = secondTimeZoneSearchAdapter;
        RecyclerView recyclerView = kf2.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        RecyclerViewAlphabetIndex recyclerViewAlphabetIndex = kf2.t;
        recyclerViewAlphabetIndex.P();
        recyclerViewAlphabetIndex.setOnSectionIndexClickListener(new c(this, kf2));
        kf2.u.addTextChangedListener(new d(this, kf2));
        kf2.q.setOnClickListener(new e(this));
        kf2.r.setOnClickListener(new f(kf2));
        this.j = new tr3<>(this, kf2);
        kd4.a((Object) kf2, "binding");
        return kf2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        s33 s33 = this.k;
        if (s33 != null) {
            s33.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        s33 s33 = this.k;
        if (s33 != null) {
            s33.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void q(List<SecondTimezoneSetting> list) {
        kd4.b(list, "secondTimezones");
        SecondTimeZoneSearchAdapter secondTimeZoneSearchAdapter = this.l;
        if (secondTimeZoneSearchAdapter != null) {
            secondTimeZoneSearchAdapter.a(list);
        }
    }

    @DexIgnore
    public void a(s33 s33) {
        kd4.b(s33, "presenter");
        this.k = s33;
    }
}
