package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class io3 extends zr2 implements ho3 {
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public go3 j;
    @DexIgnore
    public tr3<of2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final io3 a() {
            return new io3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ io3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ of2 e;

            @DexIgnore
            public a(of2 of2) {
                this.e = of2;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.e.F;
                kd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.G;
                kd4.a((Object) flexibleTextView2, "it.tvLogin");
                flexibleTextView2.setVisibility(0);
            }
        }

        @DexIgnore
        public b(View view, io3 io3) {
            this.e = view;
            this.f = io3;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                of2 of2 = (of2) io3.a(this.f).a();
                if (of2 != null) {
                    try {
                        FlexibleTextView flexibleTextView = of2.F;
                        kd4.a((Object) flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleTextView flexibleTextView2 = of2.G;
                        kd4.a((Object) flexibleTextView2, "it.tvLogin");
                        flexibleTextView2.setVisibility(8);
                        qa4 qa4 = qa4.a;
                    } catch (Exception e2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e2);
                        qa4 qa42 = qa4.a;
                    }
                }
            } else {
                of2 of22 = (of2) io3.a(this.f).a();
                if (of22 != null) {
                    try {
                        kd4.a((Object) of22, "it");
                        Boolean.valueOf(of22.d().postDelayed(new a(of22), 100));
                    } catch (Exception e3) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e3);
                        qa4 qa43 = qa4.a;
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public c(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            io3.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public d(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            io3.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public e(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.G;
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ of2 e;
        @DexIgnore
        public /* final */ /* synthetic */ io3 f;

        @DexIgnore
        public f(of2 of2, io3 io3) {
            this.e = of2;
            this.f = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            TextInputLayout textInputLayout = this.e.u;
            kd4.a((Object) textInputLayout, "binding.inputPassword");
            textInputLayout.setErrorEnabled(false);
            TextInputLayout textInputLayout2 = this.e.t;
            kd4.a((Object) textInputLayout2, "binding.inputEmail");
            textInputLayout2.setErrorEnabled(false);
            this.f.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public g(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            io3.b(this.e).a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public h(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            io3.b(this.e).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public i(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            io3.b(this.e).b(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 a;

        @DexIgnore
        public j(io3 io3) {
            this.a = io3;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.a.T0();
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public k(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public l(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            io3.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public m(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            io3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ io3 e;

        @DexIgnore
        public n(io3 io3) {
            this.e = io3;
        }

        @DexIgnore
        public final void onClick(View view) {
            io3.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ of2 e;

        @DexIgnore
        public o(of2 of2) {
            this.e = of2;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.e.D;
                kd4.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.e.E;
                kd4.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
                flexibleTextView2.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView3 = this.e.D;
            kd4.a((Object) flexibleTextView3, "binding.tvErrorCheckCharacter");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = this.e.E;
            kd4.a((Object) flexibleTextView4, "binding.tvErrorCheckCombine");
            flexibleTextView4.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ tr3 a(io3 io3) {
        tr3<of2> tr3 = io3.k;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ go3 b(io3 io3) {
        go3 go3 = io3.j;
        if (go3 != null) {
            return go3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B0() {
        if (isActive()) {
            tr3<of2> tr3 = this.k;
            if (tr3 != null) {
                of2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.q;
                    kd4.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.q;
                    kd4.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.q;
                    kd4.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(false);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void F() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.F;
            kd4.a((Object) activity, "it");
            aVar.a(activity, "file:///android_asset/signinwithapple.html");
        }
    }

    @DexIgnore
    public void N(String str) {
        kd4.b(str, "errorMessage");
        tr3<of2> tr3 = this.k;
        if (tr3 != null) {
            of2 a2 = tr3.a();
            if (a2 != null) {
                TextInputLayout textInputLayout = a2.t;
                kd4.a((Object) textInputLayout, "it.inputEmail");
                textInputLayout.setErrorEnabled(true);
                TextInputLayout textInputLayout2 = a2.t;
                kd4.a((Object) textInputLayout2, "it.inputEmail");
                textInputLayout2.setError(str);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SignUpFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        tr3<of2> tr3 = this.k;
        if (tr3 != null) {
            of2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.q;
                kd4.a((Object) flexibleButton, "this.btContinue");
                if (flexibleButton.isEnabled()) {
                    go3 go3 = this.j;
                    if (go3 != null) {
                        TextInputEditText textInputEditText = a2.r;
                        kd4.a((Object) textInputEditText, "etEmail");
                        String valueOf = String.valueOf(textInputEditText.getText());
                        TextInputEditText textInputEditText2 = a2.s;
                        kd4.a((Object) textInputEditText2, "etPassword");
                        go3.a(valueOf, String.valueOf(textInputEditText2.getText()));
                        return;
                    }
                    kd4.d("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(int i2, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    public void g() {
        tr3<of2> tr3 = this.k;
        if (tr3 != null) {
            of2 a2 = tr3.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.C, "progress", new int[]{0, 10});
                kd4.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(500);
                ofInt.start();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                aVar.a(activity);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void k() {
        String string = getString(R.string.Onboarding_SignUp_CreatingAccount_Text__PleaseWait);
        kd4.a((Object) string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        S(string);
    }

    @DexIgnore
    public void k0() {
        if (isActive()) {
            tr3<of2> tr3 = this.k;
            if (tr3 != null) {
                of2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.q;
                    kd4.a((Object) flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.q;
                    kd4.a((Object) flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.q;
                    kd4.a((Object) flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(true);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            go3 go3 = this.j;
            if (go3 != null) {
                kd4.a((Object) signUpSocialAuth, "authCode");
                go3.a(signUpSocialAuth);
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        of2 of2 = (of2) qa.a(layoutInflater, R.layout.fragment_signup, viewGroup, false, O0());
        kd4.a((Object) of2, "bindingLocal");
        View d2 = of2.d();
        kd4.a((Object) d2, "bindingLocal.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, this));
        this.k = new tr3<>(this, of2);
        tr3<of2> tr3 = this.k;
        if (tr3 != null) {
            of2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        go3 go3 = this.j;
        if (go3 != null) {
            go3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        go3 go3 = this.j;
        if (go3 != null) {
            go3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<of2> tr3 = this.k;
        if (tr3 != null) {
            of2 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new f(a2, this));
                a2.r.addTextChangedListener(new g(this));
                a2.r.setOnFocusChangeListener(new h(this));
                a2.s.addTextChangedListener(new i(this));
                a2.s.setOnFocusChangeListener(new o(a2));
                a2.s.setOnEditorActionListener(new j(this));
                a2.x.setOnClickListener(new k(this));
                a2.y.setOnClickListener(new l(this));
                a2.z.setOnClickListener(new m(this));
                a2.v.setOnClickListener(new n(this));
                a2.A.setOnClickListener(new c(this));
                a2.B.setOnClickListener(new d(this));
                a2.G.setOnClickListener(new e(this));
            }
            R("sign_up_view");
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void z(boolean z) {
        if (isActive()) {
            tr3<of2> tr3 = this.k;
            if (tr3 != null) {
                of2 a2 = tr3.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    FloatingActionButton floatingActionButton = a2.B;
                    kd4.a((Object) floatingActionButton, "it.ivWeibo");
                    floatingActionButton.setVisibility(0);
                    FloatingActionButton floatingActionButton2 = a2.A;
                    kd4.a((Object) floatingActionButton2, "it.ivWechat");
                    floatingActionButton2.setVisibility(0);
                    ImageView imageView = a2.z;
                    kd4.a((Object) imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.y;
                    kd4.a((Object) imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                FloatingActionButton floatingActionButton3 = a2.B;
                kd4.a((Object) floatingActionButton3, "it.ivWeibo");
                floatingActionButton3.setVisibility(8);
                FloatingActionButton floatingActionButton4 = a2.A;
                kd4.a((Object) floatingActionButton4, "it.ivWechat");
                floatingActionButton4.setVisibility(8);
                ImageView imageView3 = a2.z;
                kd4.a((Object) imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.y;
                kd4.a((Object) imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(int i2, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void c(boolean z, boolean z2) {
        if (isActive()) {
            tr3<of2> tr3 = this.k;
            if (tr3 != null) {
                of2 a2 = tr3.a();
                if (a2 != null) {
                    if (z) {
                        a2.D.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_ok_button), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.D.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_ok_button), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.E.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(go3 go3) {
        kd4.b(go3, "presenter");
        this.j = go3;
    }

    @DexIgnore
    public void a(boolean z, boolean z2, String str) {
        kd4.b(str, "errorMessage");
        if (isActive()) {
            tr3<of2> tr3 = this.k;
            if (tr3 != null) {
                of2 a2 = tr3.a();
                if (a2 != null) {
                    TextInputLayout textInputLayout = a2.t;
                    kd4.a((Object) textInputLayout, "it.inputEmail");
                    textInputLayout.setErrorEnabled(z2);
                    TextInputLayout textInputLayout2 = a2.t;
                    kd4.a((Object) textInputLayout2, "it.inputEmail");
                    textInputLayout2.setError(str);
                }
                tr3<of2> tr32 = this.k;
                if (tr32 != null) {
                    of2 a3 = tr32.a();
                    if (a3 != null) {
                        ImageView imageView = a3.w;
                        if (imageView != null) {
                            kd4.a((Object) imageView, "it");
                            imageView.setVisibility(z ? 0 : 8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void b(SignUpEmailAuth signUpEmailAuth) {
        kd4.b(signUpEmailAuth, "emailAuth");
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity, signUpEmailAuth);
            }
        }
    }

    @DexIgnore
    public void b(SignUpSocialAuth signUpSocialAuth) {
        kd4.b(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a((Context) activity, signUpSocialAuth);
            activity.finish();
        }
    }
}
