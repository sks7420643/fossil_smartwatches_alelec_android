package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c1 */
public abstract class C1525c1<T> extends com.fossil.blesdk.obfuscated.C1592d1<T> {

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.Context f3952b;

    @DexIgnore
    /* renamed from: c */
    public java.util.Map<com.fossil.blesdk.obfuscated.C2001i7, android.view.MenuItem> f3953c;

    @DexIgnore
    /* renamed from: d */
    public java.util.Map<com.fossil.blesdk.obfuscated.C2108j7, android.view.SubMenu> f3954d;

    @DexIgnore
    public C1525c1(android.content.Context context, T t) {
        super(t);
        this.f3952b = context;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.MenuItem mo9376a(android.view.MenuItem menuItem) {
        if (!(menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7)) {
            return menuItem;
        }
        com.fossil.blesdk.obfuscated.C2001i7 i7Var = (com.fossil.blesdk.obfuscated.C2001i7) menuItem;
        if (this.f3953c == null) {
            this.f3953c = new com.fossil.blesdk.obfuscated.C1855g4();
        }
        android.view.MenuItem menuItem2 = this.f3953c.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        android.view.MenuItem a = com.fossil.blesdk.obfuscated.C2780r1.m13039a(this.f3952b, i7Var);
        this.f3953c.put(i7Var, a);
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo9379b() {
        java.util.Map<com.fossil.blesdk.obfuscated.C2001i7, android.view.MenuItem> map = this.f3953c;
        if (map != null) {
            map.clear();
        }
        java.util.Map<com.fossil.blesdk.obfuscated.C2108j7, android.view.SubMenu> map2 = this.f3954d;
        if (map2 != null) {
            map2.clear();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo9380b(int i) {
        java.util.Map<com.fossil.blesdk.obfuscated.C2001i7, android.view.MenuItem> map = this.f3953c;
        if (map != null) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2001i7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.SubMenu mo9377a(android.view.SubMenu subMenu) {
        if (!(subMenu instanceof com.fossil.blesdk.obfuscated.C2108j7)) {
            return subMenu;
        }
        com.fossil.blesdk.obfuscated.C2108j7 j7Var = (com.fossil.blesdk.obfuscated.C2108j7) subMenu;
        if (this.f3954d == null) {
            this.f3954d = new com.fossil.blesdk.obfuscated.C1855g4();
        }
        android.view.SubMenu subMenu2 = this.f3954d.get(j7Var);
        if (subMenu2 != null) {
            return subMenu2;
        }
        android.view.SubMenu a = com.fossil.blesdk.obfuscated.C2780r1.m13040a(this.f3952b, j7Var);
        this.f3954d.put(j7Var, a);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9378a(int i) {
        java.util.Map<com.fossil.blesdk.obfuscated.C2001i7, android.view.MenuItem> map = this.f3953c;
        if (map != null) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2001i7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }
}
