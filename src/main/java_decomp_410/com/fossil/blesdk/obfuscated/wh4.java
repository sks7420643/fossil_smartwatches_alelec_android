package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wh4 {
    @DexIgnore
    public static /* final */ dk4 a; // = new dk4("REMOVED_TASK");
    @DexIgnore
    public static /* final */ dk4 b; // = new dk4("CLOSED_EMPTY");

    @DexIgnore
    public static final long a(long j) {
        if (j <= 0) {
            return 0;
        }
        return j >= 9223372036854L ? ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD : 1000000 * j;
    }
}
