package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x4 */
public class C3261x4 {

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10827a;

    @DexIgnore
    /* renamed from: b */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10828b;

    @DexIgnore
    /* renamed from: c */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10829c;

    @DexIgnore
    /* renamed from: d */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10830d;

    @DexIgnore
    /* renamed from: e */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10831e;

    @DexIgnore
    /* renamed from: f */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10832f;

    @DexIgnore
    /* renamed from: g */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget f10833g;

    @DexIgnore
    /* renamed from: h */
    public java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> f10834h;

    @DexIgnore
    /* renamed from: i */
    public int f10835i;

    @DexIgnore
    /* renamed from: j */
    public int f10836j;

    @DexIgnore
    /* renamed from: k */
    public float f10837k; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    /* renamed from: l */
    public int f10838l;

    @DexIgnore
    /* renamed from: m */
    public boolean f10839m; // = false;

    @DexIgnore
    /* renamed from: n */
    public boolean f10840n;

    @DexIgnore
    /* renamed from: o */
    public boolean f10841o;

    @DexIgnore
    /* renamed from: p */
    public boolean f10842p;

    @DexIgnore
    /* renamed from: q */
    public boolean f10843q;

    @DexIgnore
    public C3261x4(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i, boolean z) {
        this.f10827a = constraintWidget;
        this.f10838l = i;
        this.f10839m = z;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m16145a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i) {
        if (constraintWidget.mo1350s() != 8 && constraintWidget.f665C[i] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int[] iArr = constraintWidget.f701g;
            if (iArr[i] == 0 || iArr[i] == 3) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17600b() {
        int i = this.f10838l * 2;
        boolean z = false;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f10827a;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintWidget;
        boolean z2 = false;
        while (!z2) {
            this.f10835i++;
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] constraintWidgetArr = constraintWidget.f706i0;
            int i2 = this.f10838l;
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = null;
            constraintWidgetArr[i2] = null;
            constraintWidget.f704h0[i2] = null;
            if (constraintWidget.mo1350s() != 8) {
                if (this.f10828b == null) {
                    this.f10828b = constraintWidget;
                }
                this.f10830d = constraintWidget;
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.f665C;
                int i3 = this.f10838l;
                if (dimensionBehaviourArr[i3] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    int[] iArr = constraintWidget.f701g;
                    if (iArr[i3] == 0 || iArr[i3] == 3 || iArr[i3] == 2) {
                        this.f10836j++;
                        float[] fArr = constraintWidget.f702g0;
                        int i4 = this.f10838l;
                        float f = fArr[i4];
                        if (f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            this.f10837k += fArr[i4];
                        }
                        if (m16145a(constraintWidget, this.f10838l)) {
                            if (f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                this.f10840n = true;
                            } else {
                                this.f10841o = true;
                            }
                            if (this.f10834h == null) {
                                this.f10834h = new java.util.ArrayList<>();
                            }
                            this.f10834h.add(constraintWidget);
                        }
                        if (this.f10832f == null) {
                            this.f10832f = constraintWidget;
                        }
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4 = this.f10833g;
                        if (constraintWidget4 != null) {
                            constraintWidget4.f704h0[this.f10838l] = constraintWidget;
                        }
                        this.f10833g = constraintWidget;
                    }
                }
            }
            if (constraintWidget2 != constraintWidget) {
                constraintWidget2.f706i0[this.f10838l] = constraintWidget;
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintWidget.f663A[i + 1].f652d;
            if (constraintAnchor != null) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget5 = constraintAnchor.f650b;
                androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget5.f663A;
                if (constraintAnchorArr[i].f652d != null && constraintAnchorArr[i].f652d.f650b == constraintWidget) {
                    constraintWidget3 = constraintWidget5;
                }
            }
            if (constraintWidget3 == null) {
                constraintWidget3 = constraintWidget;
                z2 = true;
            }
            constraintWidget2 = constraintWidget;
            constraintWidget = constraintWidget3;
        }
        this.f10829c = constraintWidget;
        if (this.f10838l != 0 || !this.f10839m) {
            this.f10831e = this.f10827a;
        } else {
            this.f10831e = this.f10829c;
        }
        if (this.f10841o && this.f10840n) {
            z = true;
        }
        this.f10842p = z;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17599a() {
        if (!this.f10843q) {
            mo17600b();
        }
        this.f10843q = true;
    }
}
