package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e23 implements Factory<HomeDianaCustomizePresenter> {
    @DexIgnore
    public static HomeDianaCustomizePresenter a(d23 d23, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaPresetRepository dianaPresetRepository, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, wm2 wm2, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository, WatchFaceRepository watchFaceRepository, PortfolioApp portfolioApp) {
        return new HomeDianaCustomizePresenter(d23, watchAppRepository, complicationRepository, dianaPresetRepository, setDianaPresetToWatchUseCase, wm2, customizeRealDataRepository, userRepository, watchFaceRepository, portfolioApp);
    }
}
