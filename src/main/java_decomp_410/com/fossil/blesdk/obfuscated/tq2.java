package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tq2 extends CoroutineUseCase<c, e, d> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((fd4) null);
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ b f; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return tq2.g;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = tq2.h.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + tq2.this.d() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.FORCE_CONNECT && tq2.this.d()) {
                boolean z = false;
                tq2.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    tq2.this.a(new e());
                    return;
                }
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                tq2.this.a(new d(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            kd4.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public d(int i, int i2, ArrayList<Integer> arrayList) {
            kd4.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CoroutineUseCase.d {
    }

    /*
    static {
        String simpleName = tq2.class.getSimpleName();
        kd4.a((Object) simpleName, "ReconnectDeviceUseCase::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    public final boolean d() {
        return this.e;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.f, CommunicateMode.FORCE_CONNECT);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public Object a(c cVar, yb4<Object> yb4) {
        this.e = true;
        this.d = cVar != null ? cVar.a() : null;
        try {
            PortfolioApp c2 = PortfolioApp.W.c();
            String str = this.d;
            if (str != null) {
                c2.d(str);
                return new Object();
            }
            kd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.e(str2, "Error inside " + g + ".connectDevice - e=" + e2);
        }
    }
}
