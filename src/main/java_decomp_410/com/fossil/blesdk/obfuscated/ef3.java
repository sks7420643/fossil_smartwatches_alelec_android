package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ef3 extends v52<df3> {
    @DexIgnore
    void a(wr2 wr2, ArrayList<String> arrayList);

    @DexIgnore
    void a(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c(qd<GoalTrackingData> qdVar);

    @DexIgnore
    void f();
}
