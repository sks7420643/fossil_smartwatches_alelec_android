package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.AppEventsConstants;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ur3 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ ur3 b; // = new ur3();

    /*
    static {
        String simpleName = ur3.class.getSimpleName();
        kd4.a((Object) simpleName, "BackendURLUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final String a(int i) {
        return a(PortfolioApp.W.c(), i);
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i) {
        kd4.b(portfolioApp, "app");
        int j = portfolioApp.u().j();
        boolean z = false;
        if (j == 0 ? !qf4.b("release", "release", true) : j == 1) {
            z = true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "getBackendUrl - type=" + i + ", isUsingStagingUrl= " + z);
        if (i != 0) {
            if (i != 1) {
                if (i != 2) {
                    if (i != 4) {
                        return "";
                    }
                    if (z) {
                        return f62.x.e();
                    }
                    return f62.x.d();
                } else if (z) {
                    return f62.x.p();
                } else {
                    return f62.x.o();
                }
            } else if (z) {
                return f62.x.n();
            } else {
                return f62.x.m();
            }
        } else if (z) {
            return f62.x.j();
        } else {
            return f62.x.g();
        }
    }

    @DexIgnore
    public final String a(PortfolioApp portfolioApp, int i, String str) {
        kd4.b(portfolioApp, "app");
        kd4.b(str, "version");
        int j = portfolioApp.u().j();
        boolean z = false;
        if (j == 0 ? !qf4.b("release", "release", true) : j == 1) {
            z = true;
        }
        if (i != 0) {
            return a(portfolioApp, i);
        }
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != 49) {
                if (hashCode == 50 && str.equals("2")) {
                    return f62.x.k();
                }
            } else if (str.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
                return f62.x.j();
            }
            return f62.x.l();
        }
        int hashCode2 = str.hashCode();
        if (hashCode2 != 49) {
            if (hashCode2 == 50 && str.equals("2")) {
                return f62.x.h();
            }
        } else if (str.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES)) {
            return f62.x.g();
        }
        return f62.x.i();
    }
}
