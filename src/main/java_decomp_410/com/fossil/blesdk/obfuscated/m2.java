package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface m2 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Rect rect);
    }

    @DexIgnore
    void setOnFitSystemWindowsListener(a aVar);
}
