package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vc1 extends jk0 implements me0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vc1> CREATOR; // = new gd1();
    @DexIgnore
    public /* final */ Status e;
    @DexIgnore
    public /* final */ wc1 f;

    @DexIgnore
    public vc1(Status status) {
        this(status, (wc1) null);
    }

    @DexIgnore
    public vc1(Status status, wc1 wc1) {
        this.e = status;
        this.f = wc1;
    }

    @DexIgnore
    public final Status G() {
        return this.e;
    }

    @DexIgnore
    public final wc1 H() {
        return this.f;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) G(), i, false);
        kk0.a(parcel, 2, (Parcelable) H(), i, false);
        kk0.a(parcel, a);
    }
}
