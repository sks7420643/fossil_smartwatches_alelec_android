package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mv3 {
    @DexIgnore
    public /* final */ List<yu3> a;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public mv3(List<yu3> list) {
        this.a = list;
    }

    @DexIgnore
    public yu3 a(SSLSocket sSLSocket) throws IOException {
        yu3 yu3;
        int i = this.b;
        int size = this.a.size();
        while (true) {
            if (i >= size) {
                yu3 = null;
                break;
            }
            yu3 = this.a.get(i);
            if (yu3.a(sSLSocket)) {
                this.b = i + 1;
                break;
            }
            i++;
        }
        if (yu3 != null) {
            this.c = b(sSLSocket);
            pv3.b.a(yu3, sSLSocket, this.d);
            return yu3;
        }
        throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
    }

    @DexIgnore
    public final boolean b(SSLSocket sSLSocket) {
        for (int i = this.b; i < this.a.size(); i++) {
            if (this.a.get(i).a(sSLSocket)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(IOException iOException) {
        this.d = true;
        if ((iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        boolean z = iOException instanceof SSLHandshakeException;
        if ((z && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if ((z || (iOException instanceof SSLProtocolException)) && this.c) {
            return true;
        }
        return false;
    }
}
