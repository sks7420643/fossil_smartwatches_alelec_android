package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xr0 extends sr0 implements vr0 {
    @DexIgnore
    public xr0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    @DexIgnore
    public final boolean c(boolean z) throws RemoteException {
        Parcel o = o();
        ur0.a(o, true);
        Parcel a = a(2, o);
        boolean a2 = ur0.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final String getId() throws RemoteException {
        Parcel a = a(1, o());
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    public final boolean zzc() throws RemoteException {
        Parcel a = a(6, o());
        boolean a2 = ur0.a(a);
        a.recycle();
        return a2;
    }
}
