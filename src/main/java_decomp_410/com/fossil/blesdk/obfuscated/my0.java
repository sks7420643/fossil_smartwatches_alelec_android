package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class my0 extends oj0<qy0> {
    @DexIgnore
    public my0(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 40, kj0, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.clearcut.internal.IClearcutLoggerService");
        return queryLocalInterface instanceof qy0 ? (qy0) queryLocalInterface : new ry0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return 11925000;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.clearcut.internal.IClearcutLoggerService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.clearcut.service.START";
    }
}
