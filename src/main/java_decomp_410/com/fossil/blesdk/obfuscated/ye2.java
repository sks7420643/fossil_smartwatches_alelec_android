package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ye2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TextInputEditText q;
    @DexIgnore
    public /* final */ TextInputEditText r;
    @DexIgnore
    public /* final */ TextInputEditText s;
    @DexIgnore
    public /* final */ TextInputLayout t;
    @DexIgnore
    public /* final */ TextInputLayout u;
    @DexIgnore
    public /* final */ TextInputLayout v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ ProgressButton x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public ye2(Object obj, View view, int i, ConstraintLayout constraintLayout, TextInputEditText textInputEditText, TextInputEditText textInputEditText2, TextInputEditText textInputEditText3, TextInputLayout textInputLayout, TextInputLayout textInputLayout2, TextInputLayout textInputLayout3, RTLImageView rTLImageView, ProgressButton progressButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = textInputEditText;
        this.r = textInputEditText2;
        this.s = textInputEditText3;
        this.t = textInputLayout;
        this.u = textInputLayout2;
        this.v = textInputLayout3;
        this.w = rTLImageView;
        this.x = progressButton;
        this.y = flexibleTextView;
        this.z = flexibleTextView2;
    }
}
