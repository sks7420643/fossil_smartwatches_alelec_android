package com.fossil.blesdk.obfuscated;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sp0 {
    /*
    static {
        DataType[] dataTypeArr = {DataType.x, DataType.Q, DataType.R, DataType.v, DataType.w, DataType.p, DataType.U, cp0.e, cp0.o, cp0.b, cp0.l, cp0.a, cp0.k, DataType.L, DataType.d0, DataType.N, DataType.e0, cp0.d, cp0.n, DataType.M, DataType.f0, DataType.t, DataType.W, DataType.r, DataType.s, cp0.f, cp0.g, DataType.I, DataType.H, DataType.F, DataType.G, DataType.TYPE_DISTANCE_CUMULATIVE, DataType.D, DataType.q, DataType.V, DataType.l, DataType.m, DataType.n, DataType.X, DataType.Y, DataType.A, DataType.Z, DataType.J, DataType.h0, DataType.a0, DataType.B, DataType.C, cp0.h, DataType.O, DataType.P, DataType.i0, cp0.i, cp0.c, cp0.m, DataType.u, DataType.b0, DataType.z, DataType.y, DataType.E, DataType.c0, DataType.k, DataType.o, DataType.TYPE_STEP_COUNT_CUMULATIVE, DataType.i, DataType.j, cp0.j, DataType.K, DataType.g0, DataType.S, DataType.T};
        DataType[] dataTypeArr2 = {cp0.e, cp0.o, cp0.b, cp0.l, cp0.a, cp0.k, cp0.d, cp0.n, cp0.f, cp0.g, cp0.h, cp0.i, cp0.c, cp0.m, cp0.j};
    }
    */

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static DataType a(String str) {
        char c;
        switch (str.hashCode()) {
            case -2060095039:
                if (str.equals("com.google.cycling.wheel_revolution.rpm")) {
                    c = 30;
                    break;
                }
            case -2027664088:
                if (str.equals("com.google.calories.consumed")) {
                    c = 23;
                    break;
                }
            case -2023954015:
                if (str.equals("com.google.location.bounding_box")) {
                    c = ',';
                    break;
                }
            case -2001464928:
                if (str.equals("com.google.internal.symptom")) {
                    c = '%';
                    break;
                }
            case -1999891138:
                if (str.equals("com.google.heart_minutes")) {
                    c = '&';
                    break;
                }
            case -1939429191:
                if (str.equals("com.google.blood_glucose.summary")) {
                    c = 11;
                    break;
                }
            case -1783842905:
                if (str.equals("com.google.accelerometer")) {
                    c = 0;
                    break;
                }
            case -1757812901:
                if (str.equals("com.google.location.sample")) {
                    c = '-';
                    break;
                }
            case -1738991010:
                if (str.equals("com.google.internal.prescription_event")) {
                    c = '$';
                    break;
                }
            case -1659958877:
                if (str.equals("com.google.menstruation")) {
                    c = '/';
                    break;
                }
            case -1487055015:
                if (str.equals("com.google.body.temperature.basal.summary")) {
                    c = 8;
                    break;
                }
            case -1466904157:
                if (str.equals("com.google.floor_change.summary")) {
                    c = '\"';
                    break;
                }
            case -1465729060:
                if (str.equals("com.google.internal.primary_device")) {
                    c = 'G';
                    break;
                }
            case -1431431801:
                if (str.equals("com.google.height.summary")) {
                    c = '+';
                    break;
                }
            case -1248818137:
                if (str.equals("com.google.distance.delta")) {
                    c = ' ';
                    break;
                }
            case -1196687875:
                if (str.equals("com.google.internal.session.v2")) {
                    c = '<';
                    break;
                }
            case -1103712522:
                if (str.equals("com.google.heart_minutes.summary")) {
                    c = '\'';
                    break;
                }
            case -1102520626:
                if (str.equals("com.google.step_count.delta")) {
                    c = '@';
                    break;
                }
            case -1099695423:
                if (str.equals("com.google.activity.sample")) {
                    c = 3;
                    break;
                }
            case -1091068721:
                if (str.equals("com.google.height")) {
                    c = '*';
                    break;
                }
            case -1063046895:
                if (str.equals("com.google.step_length")) {
                    c = 'A';
                    break;
                }
            case -922976890:
                if (str.equals("com.google.cycling.pedaling.cumulative")) {
                    c = 28;
                    break;
                }
            case -900592674:
                if (str.equals("com.google.cycling.pedaling.cadence")) {
                    c = 27;
                    break;
                }
            case -886569606:
                if (str.equals("com.google.location.track")) {
                    c = '.';
                    break;
                }
            case -777285735:
                if (str.equals("com.google.heart_rate.summary")) {
                    c = ')';
                    break;
                }
            case -700668164:
                if (str.equals("com.google.internal.goal")) {
                    c = '#';
                    break;
                }
            case -661631456:
                if (str.equals("com.google.weight")) {
                    c = 'D';
                    break;
                }
            case -424876584:
                if (str.equals("com.google.weight.summary")) {
                    c = 'E';
                    break;
                }
            case -362418992:
                if (str.equals("com.google.body.temperature")) {
                    c = 17;
                    break;
                }
            case -217611775:
                if (str.equals("com.google.blood_glucose")) {
                    c = 10;
                    break;
                }
            case -185830635:
                if (str.equals("com.google.power.summary")) {
                    c = '7';
                    break;
                }
            case -177293656:
                if (str.equals("com.google.nutrition.summary")) {
                    c = '2';
                    break;
                }
            case -164586193:
                if (str.equals("com.google.activity.exercise")) {
                    c = 2;
                    break;
                }
            case -98150574:
                if (str.equals("com.google.heart_rate.bpm")) {
                    c = '(';
                    break;
                }
            case -56824761:
                if (str.equals("com.google.calories.bmr")) {
                    c = 21;
                    break;
                }
            case -43729332:
                if (str.equals("com.google.body.hip.circumference")) {
                    c = 15;
                    break;
                }
            case 2484093:
                if (str.equals("com.google.body.waist.circumference")) {
                    c = 19;
                    break;
                }
            case 53773386:
                if (str.equals("com.google.blood_pressure.summary")) {
                    c = 13;
                    break;
                }
            case 269180370:
                if (str.equals("com.google.activity.samples")) {
                    c = 4;
                    break;
                }
            case 295793957:
                if (str.equals("com.google.sensor.events")) {
                    c = '9';
                    break;
                }
            case 296250623:
                if (str.equals("com.google.calories.bmr.summary")) {
                    c = 22;
                    break;
                }
            case 324760871:
                if (str.equals("com.google.step_count.cadence")) {
                    c = '>';
                    break;
                }
            case 378060028:
                if (str.equals("com.google.activity.segment")) {
                    c = 5;
                    break;
                }
            case 529727579:
                if (str.equals("com.google.power.sample")) {
                    c = '6';
                    break;
                }
            case 634821360:
                if (str.equals("com.google.sensor.const_rate_events")) {
                    c = '8';
                    break;
                }
            case 657433501:
                if (str.equals("com.google.step_count.cumulative")) {
                    c = '?';
                    break;
                }
            case 682891187:
                if (str.equals("com.google.body.fat.percentage")) {
                    c = 9;
                    break;
                }
            case 841663855:
                if (str.equals("com.google.activity.summary")) {
                    c = 6;
                    break;
                }
            case 877955159:
                if (str.equals("com.google.speed.summary")) {
                    c = '=';
                    break;
                }
            case 899666941:
                if (str.equals("com.google.calories.expended")) {
                    c = 24;
                    break;
                }
            case 936279698:
                if (str.equals("com.google.blood_pressure")) {
                    c = 12;
                    break;
                }
            case 946706510:
                if (str.equals("com.google.hydration")) {
                    c = '1';
                    break;
                }
            case 946938859:
                if (str.equals("com.google.stride_model")) {
                    c = 'B';
                    break;
                }
            case 1029221057:
                if (str.equals("com.google.device_on_body")) {
                    c = 'F';
                    break;
                }
            case 1098265835:
                if (str.equals("com.google.floor_change")) {
                    c = '!';
                    break;
                }
            case 1111714923:
                if (str.equals("com.google.body.fat.percentage.summary")) {
                    c = 14;
                    break;
                }
            case 1214093899:
                if (str.equals("com.google.vaginal_spotting")) {
                    c = 'C';
                    break;
                }
            case 1404118825:
                if (str.equals("com.google.oxygen_saturation")) {
                    c = '4';
                    break;
                }
            case 1439932546:
                if (str.equals("com.google.ovulation_test")) {
                    c = '3';
                    break;
                }
            case 1483133089:
                if (str.equals("com.google.body.temperature.basal")) {
                    c = 7;
                    break;
                }
            case 1524007137:
                if (str.equals("com.google.cycling.wheel_revolution.cumulative")) {
                    c = 29;
                    break;
                }
            case 1532018766:
                if (str.equals("com.google.active_minutes")) {
                    c = 1;
                    break;
                }
            case 1633152752:
                if (str.equals("com.google.nutrition")) {
                    c = '0';
                    break;
                }
            case 1674865156:
                if (str.equals("com.google.body.hip.circumference.summary")) {
                    c = 16;
                    break;
                }
            case 1819660853:
                if (str.equals("com.google.body.waist.circumference.summary")) {
                    c = 20;
                    break;
                }
            case 1921738212:
                if (str.equals("com.google.distance.cumulative")) {
                    c = 31;
                    break;
                }
            case 1925848149:
                if (str.equals("com.google.cervical_position")) {
                    c = 26;
                    break;
                }
            case 1975902189:
                if (str.equals("com.google.cervical_mucus")) {
                    c = 25;
                    break;
                }
            case 1980033842:
                if (str.equals("com.google.internal.session.debug")) {
                    c = ';';
                    break;
                }
            case 2051843553:
                if (str.equals("com.google.oxygen_saturation.summary")) {
                    c = '5';
                    break;
                }
            case 2053496735:
                if (str.equals("com.google.speed")) {
                    c = ':';
                    break;
                }
            case 2131809416:
                if (str.equals("com.google.body.temperature.summary")) {
                    c = 18;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                return DataType.x;
            case 1:
                return DataType.R;
            case 2:
                return DataType.Q;
            case 3:
                return DataType.v;
            case 4:
                return DataType.w;
            case 5:
                return DataType.p;
            case 6:
                return DataType.U;
            case 7:
                return cp0.e;
            case 8:
                return cp0.o;
            case 9:
                return DataType.L;
            case 10:
                return cp0.b;
            case 11:
                return cp0.l;
            case 12:
                return cp0.a;
            case 13:
                return cp0.k;
            case 14:
                return DataType.d0;
            case 15:
                return DataType.N;
            case 16:
                return DataType.e0;
            case 17:
                return cp0.d;
            case 18:
                return cp0.n;
            case 19:
                return DataType.M;
            case 20:
                return DataType.f0;
            case 21:
                return DataType.t;
            case 22:
                return DataType.W;
            case 23:
                return DataType.r;
            case 24:
                return DataType.s;
            case 25:
                return cp0.f;
            case 26:
                return cp0.g;
            case 27:
                return DataType.I;
            case 28:
                return DataType.H;
            case 29:
                return DataType.F;
            case 30:
                return DataType.G;
            case 31:
                return DataType.TYPE_DISTANCE_CUMULATIVE;
            case ' ':
                return DataType.D;
            case '!':
                return DataType.q;
            case '\"':
                return DataType.V;
            case '#':
                return DataType.l;
            case '$':
                return DataType.m;
            case '%':
                return DataType.n;
            case '&':
                return DataType.X;
            case '\'':
                return DataType.Y;
            case '(':
                return DataType.A;
            case ')':
                return DataType.Z;
            case '*':
                return DataType.J;
            case '+':
                return DataType.h0;
            case ',':
                return DataType.a0;
            case '-':
                return DataType.B;
            case '.':
                return DataType.C;
            case '/':
                return cp0.h;
            case '0':
                return DataType.O;
            case '1':
                return DataType.P;
            case '2':
                return DataType.i0;
            case '3':
                return cp0.i;
            case '4':
                return cp0.c;
            case '5':
                return cp0.m;
            case '6':
                return DataType.u;
            case '7':
                return DataType.b0;
            case '8':
                return DataType.z;
            case '9':
                return DataType.y;
            case ':':
                return DataType.E;
            case ';':
                return DataType.a.a;
            case '<':
                return DataType.a.b;
            case '=':
                return DataType.c0;
            case '>':
                return DataType.k;
            case '?':
                return DataType.TYPE_STEP_COUNT_CUMULATIVE;
            case '@':
                return DataType.i;
            case 'A':
                return DataType.j;
            case 'B':
                return DataType.o;
            case 'C':
                return cp0.j;
            case 'D':
                return DataType.K;
            case 'E':
                return DataType.g0;
            case 'F':
                return DataType.S;
            case 'G':
                return DataType.T;
            default:
                return null;
        }
    }
}
