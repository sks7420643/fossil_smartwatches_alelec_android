package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jt */
public class C2157jt implements com.fossil.blesdk.obfuscated.C2978to<java.nio.ByteBuffer> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.nio.ByteBuffer f6583a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jt$a")
    /* renamed from: com.fossil.blesdk.obfuscated.jt$a */
    public static class C2158a implements com.fossil.blesdk.obfuscated.C2978to.C2979a<java.nio.ByteBuffer> {
        @DexIgnore
        public java.lang.Class<java.nio.ByteBuffer> getDataClass() {
            return java.nio.ByteBuffer.class;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2978to<java.nio.ByteBuffer> mo12468a(java.nio.ByteBuffer byteBuffer) {
            return new com.fossil.blesdk.obfuscated.C2157jt(byteBuffer);
        }
    }

    @DexIgnore
    public C2157jt(java.nio.ByteBuffer byteBuffer) {
        this.f6583a = byteBuffer;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12466a() {
    }

    @DexIgnore
    /* renamed from: b */
    public java.nio.ByteBuffer m9159b() {
        this.f6583a.position(0);
        return this.f6583a;
    }
}
