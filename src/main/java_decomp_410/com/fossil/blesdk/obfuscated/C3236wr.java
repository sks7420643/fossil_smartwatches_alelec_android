package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wr */
public class C3236wr {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ com.fossil.blesdk.obfuscated.C3236wr.C3239c f10683e; // = new com.fossil.blesdk.obfuscated.C3236wr.C3239c();

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C2912sr<java.lang.Object, java.lang.Object> f10684f; // = new com.fossil.blesdk.obfuscated.C3236wr.C3237a();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?>> f10685a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3236wr.C3239c f10686b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?>> f10687c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> f10688d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wr$a */
    public static class C3237a implements com.fossil.blesdk.obfuscated.C2912sr<java.lang.Object, java.lang.Object> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.lang.Object> mo8911a(java.lang.Object obj, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
            return null;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8912a(java.lang.Object obj) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.wr$b */
    public static class C3238b<Model, Data> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Class<Model> f10689a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.Class<Data> f10690b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2984tr<? extends Model, ? extends Data> f10691c;

        @DexIgnore
        public C3238b(java.lang.Class<Model> cls, java.lang.Class<Data> cls2, com.fossil.blesdk.obfuscated.C2984tr<? extends Model, ? extends Data> trVar) {
            this.f10689a = cls;
            this.f10690b = cls2;
            this.f10691c = trVar;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo17515a(java.lang.Class<?> cls, java.lang.Class<?> cls2) {
            return mo17514a(cls) && this.f10690b.isAssignableFrom(cls2);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo17514a(java.lang.Class<?> cls) {
            return this.f10689a.isAssignableFrom(cls);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.wr$c */
    public static class C3239c {
        @DexIgnore
        /* renamed from: a */
        public <Model, Data> com.fossil.blesdk.obfuscated.C3133vr<Model, Data> mo17516a(java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, Data>> list, com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
            return new com.fossil.blesdk.obfuscated.C3133vr<>(list, g8Var);
        }
    }

    @DexIgnore
    public C3236wr(com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
        this(g8Var, f10683e);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Model, Data> void mo17511a(java.lang.Class<Model> cls, java.lang.Class<Data> cls2, com.fossil.blesdk.obfuscated.C2984tr<? extends Model, ? extends Data> trVar) {
        mo17512a(cls, cls2, trVar, true);
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized java.util.List<java.lang.Class<?>> mo17513b(java.lang.Class<?> cls) {
        java.util.ArrayList arrayList;
        arrayList = new java.util.ArrayList();
        for (com.fossil.blesdk.obfuscated.C3236wr.C3238b next : this.f10685a) {
            if (!arrayList.contains(next.f10690b) && next.mo17514a(cls)) {
                arrayList.add(next.f10690b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public C3236wr(com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var, com.fossil.blesdk.obfuscated.C3236wr.C3239c cVar) {
        this.f10685a = new java.util.ArrayList();
        this.f10687c = new java.util.HashSet();
        this.f10688d = g8Var;
        this.f10686b = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final <Model, Data> void mo17512a(java.lang.Class<Model> cls, java.lang.Class<Data> cls2, com.fossil.blesdk.obfuscated.C2984tr<? extends Model, ? extends Data> trVar, boolean z) {
        com.fossil.blesdk.obfuscated.C3236wr.C3238b bVar = new com.fossil.blesdk.obfuscated.C3236wr.C3238b(cls, cls2, trVar);
        java.util.List<com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?>> list = this.f10685a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Model> java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, ?>> mo17510a(java.lang.Class<Model> cls) {
        java.util.ArrayList arrayList;
        try {
            arrayList = new java.util.ArrayList();
            for (com.fossil.blesdk.obfuscated.C3236wr.C3238b next : this.f10685a) {
                if (!this.f10687c.contains(next)) {
                    if (next.mo17514a(cls)) {
                        this.f10687c.add(next);
                        arrayList.add(mo17508a((com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?>) next));
                        this.f10687c.remove(next);
                    }
                }
            }
        } catch (Throwable th) {
            this.f10687c.clear();
            throw th;
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Model, Data> com.fossil.blesdk.obfuscated.C2912sr<Model, Data> mo17509a(java.lang.Class<Model> cls, java.lang.Class<Data> cls2) {
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList();
            boolean z = false;
            for (com.fossil.blesdk.obfuscated.C3236wr.C3238b next : this.f10685a) {
                if (this.f10687c.contains(next)) {
                    z = true;
                } else if (next.mo17515a(cls, cls2)) {
                    this.f10687c.add(next);
                    arrayList.add(mo17508a((com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?>) next));
                    this.f10687c.remove(next);
                }
            }
            if (arrayList.size() > 1) {
                return this.f10686b.mo17516a(arrayList, this.f10688d);
            } else if (arrayList.size() == 1) {
                return (com.fossil.blesdk.obfuscated.C2912sr) arrayList.get(0);
            } else if (z) {
                return m15930a();
            } else {
                throw new com.bumptech.glide.Registry.NoModelLoaderAvailableException(cls, cls2);
            }
        } catch (Throwable th) {
            this.f10687c.clear();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final <Model, Data> com.fossil.blesdk.obfuscated.C2912sr<Model, Data> mo17508a(com.fossil.blesdk.obfuscated.C3236wr.C3238b<?, ?> bVar) {
        com.fossil.blesdk.obfuscated.C2912sr<? extends Model, ? extends Data> a = bVar.f10691c.mo8913a(this);
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static <Model, Data> com.fossil.blesdk.obfuscated.C2912sr<Model, Data> m15930a() {
        return f10684f;
    }
}
