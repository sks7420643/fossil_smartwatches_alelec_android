package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gl */
public final class C1879gl implements com.fossil.blesdk.obfuscated.C1811fl {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.room.RoomDatabase f5467a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf f5468b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.gl$a */
    public class C1880a extends com.fossil.blesdk.obfuscated.C2322lf<com.fossil.blesdk.obfuscated.C1736el> {
        @DexIgnore
        public C1880a(com.fossil.blesdk.obfuscated.C1879gl glVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.fossil.blesdk.obfuscated.C1736el elVar) {
            java.lang.String str = elVar.f4862a;
            if (str == null) {
                kgVar.mo11930a(1);
            } else {
                kgVar.mo11932a(1, str);
            }
            java.lang.String str2 = elVar.f4863b;
            if (str2 == null) {
                kgVar.mo11930a(2);
            } else {
                kgVar.mo11932a(2, str2);
            }
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR IGNORE INTO `WorkName`(`name`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public C1879gl(androidx.room.RoomDatabase roomDatabase) {
        this.f5467a = roomDatabase;
        this.f5468b = new com.fossil.blesdk.obfuscated.C1879gl.C1880a(this, roomDatabase);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10960a(com.fossil.blesdk.obfuscated.C1736el elVar) {
        this.f5467a.assertNotSuspendingTransaction();
        this.f5467a.beginTransaction();
        try {
            this.f5468b.insert(elVar);
            this.f5467a.setTransactionSuccessful();
        } finally {
            this.f5467a.endTransaction();
        }
    }
}
