package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.te0;
import com.fossil.blesdk.obfuscated.ve0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uh0<A extends te0<? extends me0, de0.b>> extends ig0 {
    @DexIgnore
    public /* final */ A a;

    @DexIgnore
    public uh0(int i, A a2) {
        super(i);
        this.a = a2;
    }

    @DexIgnore
    public final void a(ve0.a<?> aVar) throws DeadObjectException {
        try {
            this.a.b(aVar.f());
        } catch (RuntimeException e) {
            a(e);
        }
    }

    @DexIgnore
    public final void a(Status status) {
        this.a.c(status);
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        String simpleName = runtimeException.getClass().getSimpleName();
        String localizedMessage = runtimeException.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.a.c(new Status(10, sb.toString()));
    }

    @DexIgnore
    public final void a(jf0 jf0, boolean z) {
        jf0.a((BasePendingResult<? extends me0>) this.a, z);
    }
}
