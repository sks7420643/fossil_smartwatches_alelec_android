package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s2 */
public class C2852s2 extends android.widget.HorizontalScrollView implements android.widget.AdapterView.OnItemSelectedListener {

    @DexIgnore
    /* renamed from: e */
    public java.lang.Runnable f9188e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2852s2.C2855c f9189f;

    @DexIgnore
    /* renamed from: g */
    public androidx.appcompat.widget.LinearLayoutCompat f9190g; // = mo15857b();

    @DexIgnore
    /* renamed from: h */
    public android.widget.Spinner f9191h;

    @DexIgnore
    /* renamed from: i */
    public boolean f9192i;

    @DexIgnore
    /* renamed from: j */
    public int f9193j;

    @DexIgnore
    /* renamed from: k */
    public int f9194k;

    @DexIgnore
    /* renamed from: l */
    public int f9195l;

    @DexIgnore
    /* renamed from: m */
    public int f9196m;

    @DexIgnore
    /* renamed from: n */
    public android.view.ViewPropertyAnimator f9197n;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.s2$a */
    public class C2853a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ android.view.View f9198e;

        @DexIgnore
        public C2853a(android.view.View view) {
            this.f9198e = view;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2852s2.this.smoothScrollTo(this.f9198e.getLeft() - ((com.fossil.blesdk.obfuscated.C2852s2.this.getWidth() - this.f9198e.getWidth()) / 2), 0);
            com.fossil.blesdk.obfuscated.C2852s2.this.f9188e = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s2$b")
    /* renamed from: com.fossil.blesdk.obfuscated.s2$b */
    public class C2854b extends android.widget.BaseAdapter {
        @DexIgnore
        public C2854b() {
        }

        @DexIgnore
        public int getCount() {
            return com.fossil.blesdk.obfuscated.C2852s2.this.f9190g.getChildCount();
        }

        @DexIgnore
        public java.lang.Object getItem(int i) {
            return ((com.fossil.blesdk.obfuscated.C2852s2.C2856d) com.fossil.blesdk.obfuscated.C2852s2.this.f9190g.getChildAt(i)).mo15876a();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
            if (view == null) {
                return com.fossil.blesdk.obfuscated.C2852s2.this.mo15855a((androidx.appcompat.app.ActionBar.C0045b) getItem(i), true);
            }
            ((com.fossil.blesdk.obfuscated.C2852s2.C2856d) view).mo15877a((androidx.appcompat.app.ActionBar.C0045b) getItem(i));
            return view;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s2$c")
    /* renamed from: com.fossil.blesdk.obfuscated.s2$c */
    public class C2855c implements android.view.View.OnClickListener {
        @DexIgnore
        public C2855c() {
        }

        @DexIgnore
        public void onClick(android.view.View view) {
            ((com.fossil.blesdk.obfuscated.C2852s2.C2856d) view).mo15876a().mo199e();
            int childCount = com.fossil.blesdk.obfuscated.C2852s2.this.f9190g.getChildCount();
            for (int i = 0; i < childCount; i++) {
                android.view.View childAt = com.fossil.blesdk.obfuscated.C2852s2.this.f9190g.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s2$d")
    /* renamed from: com.fossil.blesdk.obfuscated.s2$d */
    public class C2856d extends android.widget.LinearLayout {

        @DexIgnore
        /* renamed from: e */
        public /* final */ int[] f9202e; // = {16842964};

        @DexIgnore
        /* renamed from: f */
        public androidx.appcompat.app.ActionBar.C0045b f9203f;

        @DexIgnore
        /* renamed from: g */
        public android.widget.TextView f9204g;

        @DexIgnore
        /* renamed from: h */
        public android.widget.ImageView f9205h;

        @DexIgnore
        /* renamed from: i */
        public android.view.View f9206i;

        @DexIgnore
        public C2856d(android.content.Context context, androidx.appcompat.app.ActionBar.C0045b bVar, boolean z) {
            super(context, (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionBarTabStyle);
            this.f9203f = bVar;
            com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17205a(context, (android.util.AttributeSet) null, this.f9202e, com.fossil.blesdk.obfuscated.C2777r.actionBarTabStyle, 0);
            if (a.mo18432g(0)) {
                setBackgroundDrawable(a.mo18422b(0));
            }
            a.mo18418a();
            if (z) {
                setGravity(8388627);
            }
            mo15878b();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15877a(androidx.appcompat.app.ActionBar.C0045b bVar) {
            this.f9203f = bVar;
            mo15878b();
        }

        @DexIgnore
        /* renamed from: b */
        public void mo15878b() {
            androidx.appcompat.app.ActionBar.C0045b bVar = this.f9203f;
            android.view.View b = bVar.mo196b();
            java.lang.CharSequence charSequence = null;
            if (b != null) {
                android.view.ViewParent parent = b.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((android.view.ViewGroup) parent).removeView(b);
                    }
                    addView(b);
                }
                this.f9206i = b;
                android.widget.TextView textView = this.f9204g;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                android.widget.ImageView imageView = this.f9205h;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.f9205h.setImageDrawable((android.graphics.drawable.Drawable) null);
                    return;
                }
                return;
            }
            android.view.View view = this.f9206i;
            if (view != null) {
                removeView(view);
                this.f9206i = null;
            }
            android.graphics.drawable.Drawable c = bVar.mo197c();
            java.lang.CharSequence d = bVar.mo198d();
            if (c != null) {
                if (this.f9205h == null) {
                    androidx.appcompat.widget.AppCompatImageView appCompatImageView = new androidx.appcompat.widget.AppCompatImageView(getContext());
                    android.widget.LinearLayout.LayoutParams layoutParams = new android.widget.LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.f9205h = appCompatImageView;
                }
                this.f9205h.setImageDrawable(c);
                this.f9205h.setVisibility(0);
            } else {
                android.widget.ImageView imageView2 = this.f9205h;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.f9205h.setImageDrawable((android.graphics.drawable.Drawable) null);
                }
            }
            boolean z = !android.text.TextUtils.isEmpty(d);
            if (z) {
                if (this.f9204g == null) {
                    androidx.appcompat.widget.AppCompatTextView appCompatTextView = new androidx.appcompat.widget.AppCompatTextView(getContext(), (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(android.text.TextUtils.TruncateAt.END);
                    android.widget.LinearLayout.LayoutParams layoutParams2 = new android.widget.LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.f9204g = appCompatTextView;
                }
                this.f9204g.setText(d);
                this.f9204g.setVisibility(0);
            } else {
                android.widget.TextView textView2 = this.f9204g;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.f9204g.setText((java.lang.CharSequence) null);
                }
            }
            android.widget.ImageView imageView3 = this.f9205h;
            if (imageView3 != null) {
                imageView3.setContentDescription(bVar.mo195a());
            }
            if (!z) {
                charSequence = bVar.mo195a();
            }
            com.fossil.blesdk.obfuscated.C1460b3.m4756a(this, charSequence);
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(androidx.appcompat.app.ActionBar.C0045b.class.getName());
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName(androidx.appcompat.app.ActionBar.C0045b.class.getName());
        }

        @DexIgnore
        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (com.fossil.blesdk.obfuscated.C2852s2.this.f9193j > 0) {
                int measuredWidth = getMeasuredWidth();
                int i3 = com.fossil.blesdk.obfuscated.C2852s2.this.f9193j;
                if (measuredWidth > i3) {
                    super.onMeasure(android.view.View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
                }
            }
        }

        @DexIgnore
        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public androidx.appcompat.app.ActionBar.C0045b mo15876a() {
            return this.f9203f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.s2$e")
    /* renamed from: com.fossil.blesdk.obfuscated.s2$e */
    public class C2857e extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public boolean f9208a; // = false;

        @DexIgnore
        /* renamed from: b */
        public int f9209b;

        @DexIgnore
        public C2857e() {
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            this.f9208a = true;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            if (!this.f9208a) {
                com.fossil.blesdk.obfuscated.C2852s2 s2Var = com.fossil.blesdk.obfuscated.C2852s2.this;
                s2Var.f9197n = null;
                s2Var.setVisibility(this.f9209b);
            }
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2852s2.this.setVisibility(0);
            this.f9208a = false;
        }
    }

    /*
    static {
        new android.view.animation.DecelerateInterpolator();
    }
    */

    @DexIgnore
    public C2852s2(android.content.Context context) {
        super(context);
        new com.fossil.blesdk.obfuscated.C2852s2.C2857e();
        setHorizontalScrollBarEnabled(false);
        com.fossil.blesdk.obfuscated.C2849s0 a = com.fossil.blesdk.obfuscated.C2849s0.m13459a(context);
        setContentHeight(a.mo15806e());
        this.f9194k = a.mo15805d();
        addView(this.f9190g, new android.view.ViewGroup.LayoutParams(-2, -1));
    }

    @DexIgnore
    /* renamed from: a */
    public final android.widget.Spinner mo15854a() {
        androidx.appcompat.widget.AppCompatSpinner appCompatSpinner = new androidx.appcompat.widget.AppCompatSpinner(getContext(), (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new androidx.appcompat.widget.LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    @DexIgnore
    /* renamed from: b */
    public final androidx.appcompat.widget.LinearLayoutCompat mo15857b() {
        androidx.appcompat.widget.LinearLayoutCompat linearLayoutCompat = new androidx.appcompat.widget.LinearLayoutCompat(getContext(), (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        linearLayoutCompat.setLayoutParams(new androidx.appcompat.widget.LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo15858c() {
        android.widget.Spinner spinner = this.f9191h;
        return spinner != null && spinner.getParent() == this;
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo15859d() {
        if (!mo15858c()) {
            if (this.f9191h == null) {
                this.f9191h = mo15854a();
            }
            removeView(this.f9190g);
            addView(this.f9191h, new android.view.ViewGroup.LayoutParams(-2, -1));
            if (this.f9191h.getAdapter() == null) {
                this.f9191h.setAdapter(new com.fossil.blesdk.obfuscated.C2852s2.C2854b());
            }
            java.lang.Runnable runnable = this.f9188e;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.f9188e = null;
            }
            this.f9191h.setSelection(this.f9196m);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo15860e() {
        if (!mo15858c()) {
            return false;
        }
        removeView(this.f9191h);
        addView(this.f9190g, new android.view.ViewGroup.LayoutParams(-2, -1));
        setTabSelected(this.f9191h.getSelectedItemPosition());
        return false;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        java.lang.Runnable runnable = this.f9188e;
        if (runnable != null) {
            post(runnable);
        }
    }

    @DexIgnore
    public void onConfigurationChanged(android.content.res.Configuration configuration) {
        super.onConfigurationChanged(configuration);
        com.fossil.blesdk.obfuscated.C2849s0 a = com.fossil.blesdk.obfuscated.C2849s0.m13459a(getContext());
        setContentHeight(a.mo15806e());
        this.f9194k = a.mo15805d();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        java.lang.Runnable runnable = this.f9188e;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    @DexIgnore
    public void onItemSelected(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long j) {
        ((com.fossil.blesdk.obfuscated.C2852s2.C2856d) view).mo15876a().mo199e();
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        int mode = android.view.View.MeasureSpec.getMode(i);
        boolean z = true;
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f9190g.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.f9193j = -1;
        } else {
            if (childCount > 2) {
                this.f9193j = (int) (((float) android.view.View.MeasureSpec.getSize(i)) * 0.4f);
            } else {
                this.f9193j = android.view.View.MeasureSpec.getSize(i) / 2;
            }
            this.f9193j = java.lang.Math.min(this.f9193j, this.f9194k);
        }
        int makeMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(this.f9195l, 1073741824);
        if (z2 || !this.f9192i) {
            z = false;
        }
        if (z) {
            this.f9190g.measure(0, makeMeasureSpec);
            if (this.f9190g.getMeasuredWidth() > android.view.View.MeasureSpec.getSize(i)) {
                mo15859d();
            } else {
                mo15860e();
            }
        } else {
            mo15860e();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.f9196m);
        }
    }

    @DexIgnore
    public void onNothingSelected(android.widget.AdapterView<?> adapterView) {
    }

    @DexIgnore
    public void setAllowCollapse(boolean z) {
        this.f9192i = z;
    }

    @DexIgnore
    public void setContentHeight(int i) {
        this.f9195l = i;
        requestLayout();
    }

    @DexIgnore
    public void setTabSelected(int i) {
        this.f9196m = i;
        int childCount = this.f9190g.getChildCount();
        int i2 = 0;
        while (i2 < childCount) {
            android.view.View childAt = this.f9190g.getChildAt(i2);
            boolean z = i2 == i;
            childAt.setSelected(z);
            if (z) {
                mo15856a(i);
            }
            i2++;
        }
        android.widget.Spinner spinner = this.f9191h;
        if (spinner != null && i >= 0) {
            spinner.setSelection(i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15856a(int i) {
        android.view.View childAt = this.f9190g.getChildAt(i);
        java.lang.Runnable runnable = this.f9188e;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        this.f9188e = new com.fossil.blesdk.obfuscated.C2852s2.C2853a(childAt);
        post(this.f9188e);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2852s2.C2856d mo15855a(androidx.appcompat.app.ActionBar.C0045b bVar, boolean z) {
        com.fossil.blesdk.obfuscated.C2852s2.C2856d dVar = new com.fossil.blesdk.obfuscated.C2852s2.C2856d(getContext(), bVar, z);
        if (z) {
            dVar.setBackgroundDrawable((android.graphics.drawable.Drawable) null);
            dVar.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, this.f9195l));
        } else {
            dVar.setFocusable(true);
            if (this.f9189f == null) {
                this.f9189f = new com.fossil.blesdk.obfuscated.C2852s2.C2855c();
            }
            dVar.setOnClickListener(this.f9189f);
        }
        return dVar;
    }
}
