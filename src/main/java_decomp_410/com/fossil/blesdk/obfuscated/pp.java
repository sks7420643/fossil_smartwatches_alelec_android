package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pp {
    @DexIgnore
    public static /* final */ pp a; // = new b();
    @DexIgnore
    public static /* final */ pp b; // = new c();
    @DexIgnore
    public static /* final */ pp c; // = new d();
    @DexIgnore
    public static /* final */ pp d; // = new e();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pp {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(DataSource dataSource) {
            return dataSource == DataSource.REMOTE;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return (dataSource == DataSource.RESOURCE_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends pp {
        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(DataSource dataSource) {
            return false;
        }

        @DexIgnore
        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }

        @DexIgnore
        public boolean b() {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends pp {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(DataSource dataSource) {
            return (dataSource == DataSource.DATA_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return false;
        }

        @DexIgnore
        public boolean b() {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends pp {
        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(DataSource dataSource) {
            return false;
        }

        @DexIgnore
        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return (dataSource == DataSource.RESOURCE_DISK_CACHE || dataSource == DataSource.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends pp {
        @DexIgnore
        public boolean a() {
            return true;
        }

        @DexIgnore
        public boolean a(DataSource dataSource) {
            return dataSource == DataSource.REMOTE;
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy) {
            return ((z && dataSource == DataSource.DATA_DISK_CACHE) || dataSource == DataSource.LOCAL) && encodeStrategy == EncodeStrategy.TRANSFORMED;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract boolean a(DataSource dataSource);

    @DexIgnore
    public abstract boolean a(boolean z, DataSource dataSource, EncodeStrategy encodeStrategy);

    @DexIgnore
    public abstract boolean b();
}
