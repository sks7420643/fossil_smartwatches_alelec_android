package com.fossil.blesdk.obfuscated;

import android.os.Process;
import com.fossil.blesdk.obfuscated.vp;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ip {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Map<jo, d> b;
    @DexIgnore
    public /* final */ ReferenceQueue<vp<?>> c;
    @DexIgnore
    public vp.a d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile c f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ip$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ip$a$a  reason: collision with other inner class name */
        public class C0018a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable e;

            @DexIgnore
            public C0018a(a aVar, Runnable runnable) {
                this.e = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.e.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new C0018a(this, runnable), "glide-active-resources");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            ip.this.a();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends WeakReference<vp<?>> {
        @DexIgnore
        public /* final */ jo a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public aq<?> c;

        @DexIgnore
        public d(jo joVar, vp<?> vpVar, ReferenceQueue<? super vp<?>> referenceQueue, boolean z) {
            super(vpVar, referenceQueue);
            aq<?> aqVar;
            tw.a(joVar);
            this.a = joVar;
            if (!vpVar.f() || !z) {
                aqVar = null;
            } else {
                aq<?> e = vpVar.e();
                tw.a(e);
                aqVar = e;
            }
            this.c = aqVar;
            this.b = vpVar.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public ip(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new a()));
    }

    @DexIgnore
    public void a(vp.a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.d = aVar;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    @DexIgnore
    public synchronized vp<?> b(jo joVar) {
        d dVar = this.b.get(joVar);
        if (dVar == null) {
            return null;
        }
        vp<?> vpVar = (vp) dVar.get();
        if (vpVar == null) {
            a(dVar);
        }
    }

    @DexIgnore
    public ip(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.a = z;
        executor.execute(new b());
    }

    @DexIgnore
    public synchronized void a(jo joVar, vp<?> vpVar) {
        d put = this.b.put(joVar, new d(joVar, vpVar, this.c, this.a));
        if (put != null) {
            put.a();
        }
    }

    @DexIgnore
    public synchronized void a(jo joVar) {
        d remove = this.b.remove(joVar);
        if (remove != null) {
            remove.a();
        }
    }

    @DexIgnore
    public void a(d dVar) {
        synchronized (this) {
            this.b.remove(dVar.a);
            if (dVar.b) {
                if (dVar.c != null) {
                    this.d.a(dVar.a, new vp(dVar.c, true, false, dVar.a, this.d));
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        while (!this.e) {
            try {
                a((d) this.c.remove());
                c cVar = this.f;
                if (cVar != null) {
                    cVar.a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
