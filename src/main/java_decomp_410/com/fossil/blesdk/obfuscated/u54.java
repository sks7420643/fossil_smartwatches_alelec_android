package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class u54 {
    @DexIgnore
    public /* final */ d54<String> a; // = new a(this);
    @DexIgnore
    public /* final */ b54<String> b; // = new b54<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements d54<String> {
        @DexIgnore
        public a(u54 u54) {
        }

        @DexIgnore
        public String a(Context context) throws Exception {
            String installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName());
            return installerPackageName == null ? "" : installerPackageName;
        }
    }

    @DexIgnore
    public String a(Context context) {
        try {
            String a2 = this.b.a(context, this.a);
            if ("".equals(a2)) {
                return null;
            }
            return a2;
        } catch (Exception e) {
            q44.g().e("Fabric", "Failed to determine installer package name", e);
            return null;
        }
    }
}
