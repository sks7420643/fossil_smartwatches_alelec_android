package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ur */
public class C3056ur {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3236wr f10044a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3056ur.C3057a f10045b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ur$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ur$a */
    public static class C3057a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C3056ur.C3057a.C3058a<?>> f10046a; // = new java.util.HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ur$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ur$a$a */
        public static class C3058a<Model> {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, ?>> f10047a;

            @DexIgnore
            public C3058a(java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, ?>> list) {
                this.f10047a = list;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16853a() {
            this.f10046a.clear();
        }

        @DexIgnore
        /* renamed from: a */
        public <Model> void mo16854a(java.lang.Class<Model> cls, java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, ?>> list) {
            if (this.f10046a.put(cls, new com.fossil.blesdk.obfuscated.C3056ur.C3057a.C3058a(list)) != null) {
                throw new java.lang.IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public <Model> java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, ?>> mo16852a(java.lang.Class<Model> cls) {
            com.fossil.blesdk.obfuscated.C3056ur.C3057a.C3058a aVar = this.f10046a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.f10047a;
        }
    }

    @DexIgnore
    public C3056ur(com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
        this(new com.fossil.blesdk.obfuscated.C3236wr(g8Var));
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Model, Data> void mo16850a(java.lang.Class<Model> cls, java.lang.Class<Data> cls2, com.fossil.blesdk.obfuscated.C2984tr<? extends Model, ? extends Data> trVar) {
        this.f10044a.mo17511a(cls, cls2, trVar);
        this.f10045b.mo16853a();
    }

    @DexIgnore
    /* renamed from: b */
    public final synchronized <A> java.util.List<com.fossil.blesdk.obfuscated.C2912sr<A, ?>> mo16851b(java.lang.Class<A> cls) {
        java.util.List<com.fossil.blesdk.obfuscated.C2912sr<A, ?>> a;
        a = this.f10045b.mo16852a(cls);
        if (a == null) {
            a = java.util.Collections.unmodifiableList(this.f10044a.mo17510a(cls));
            this.f10045b.mo16854a(cls, a);
        }
        return a;
    }

    @DexIgnore
    public C3056ur(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
        this.f10045b = new com.fossil.blesdk.obfuscated.C3056ur.C3057a();
        this.f10044a = wrVar;
    }

    @DexIgnore
    /* renamed from: a */
    public <A> java.util.List<com.fossil.blesdk.obfuscated.C2912sr<A, ?>> mo16849a(A a) {
        java.util.List b = mo16851b(m14850b(a));
        int size = b.size();
        java.util.List<com.fossil.blesdk.obfuscated.C2912sr<A, ?>> emptyList = java.util.Collections.emptyList();
        boolean z = true;
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C2912sr srVar = (com.fossil.blesdk.obfuscated.C2912sr) b.get(i);
            if (srVar.mo8912a(a)) {
                if (z) {
                    emptyList = new java.util.ArrayList<>(size - i);
                    z = false;
                }
                emptyList.add(srVar);
            }
        }
        return emptyList;
    }

    @DexIgnore
    /* renamed from: b */
    public static <A> java.lang.Class<A> m14850b(A a) {
        return a.getClass();
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized java.util.List<java.lang.Class<?>> mo16848a(java.lang.Class<?> cls) {
        return this.f10044a.mo17513b(cls);
    }
}
