package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ky0 extends qx0<ky0> implements Cloneable {
    @DexIgnore
    public static volatile ky0[] i;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";

    @DexIgnore
    public ky0() {
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public static ky0[] f() {
        if (i == null) {
            synchronized (ux0.a) {
                if (i == null) {
                    i = new ky0[0];
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        String str = this.g;
        if (str != null && !str.equals("")) {
            px0.a(1, this.g);
        }
        String str2 = this.h;
        if (str2 != null && !str2.equals("")) {
            px0.a(2, this.h);
        }
        super.a(px0);
    }

    @DexIgnore
    public final int b() {
        int b = super.b();
        String str = this.g;
        if (str != null && !str.equals("")) {
            b += px0.b(1, this.g);
        }
        String str2 = this.h;
        return (str2 == null || str2.equals("")) ? b : b + px0.b(2, this.h);
    }

    @DexIgnore
    public final /* synthetic */ vx0 c() throws CloneNotSupportedException {
        return (ky0) clone();
    }

    @DexIgnore
    public final /* synthetic */ qx0 d() throws CloneNotSupportedException {
        return (ky0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final ky0 clone() {
        try {
            return (ky0) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ky0)) {
            return false;
        }
        ky0 ky0 = (ky0) obj;
        String str = this.g;
        if (str == null) {
            if (ky0.g != null) {
                return false;
            }
        } else if (!str.equals(ky0.g)) {
            return false;
        }
        String str2 = this.h;
        if (str2 == null) {
            if (ky0.h != null) {
                return false;
            }
        } else if (!str2.equals(ky0.h)) {
            return false;
        }
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            return this.f.equals(ky0.f);
        }
        sx0 sx02 = ky0.f;
        return sx02 == null || sx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (ky0.class.getName().hashCode() + 527) * 31;
        String str = this.g;
        int i2 = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.h;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            i2 = this.f.hashCode();
        }
        return hashCode3 + i2;
    }
}
