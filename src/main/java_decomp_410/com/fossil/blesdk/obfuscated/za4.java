package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class za4 extends ya4 {
    @DexIgnore
    public static final char a(char[] cArr) {
        kd4.b(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    @DexIgnore
    public static final <T> boolean b(T[] tArr, T t) {
        kd4.b(tArr, "$this$contains");
        return c(tArr, t) >= 0;
    }

    @DexIgnore
    public static final <T> int c(T[] tArr, T t) {
        kd4.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (kd4.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static final <T> T d(T[] tArr) {
        kd4.b(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    @DexIgnore
    public static final <T> List<T> e(T[] tArr) {
        kd4.b(tArr, "$this$toList");
        int length = tArr.length;
        if (length == 0) {
            return cb4.a();
        }
        if (length != 1) {
            return f(tArr);
        }
        return bb4.a(tArr[0]);
    }

    @DexIgnore
    public static final <T> List<T> f(T[] tArr) {
        kd4.b(tArr, "$this$toMutableList");
        return new ArrayList(cb4.b(tArr));
    }

    @DexIgnore
    public static final <T> Set<T> g(T[] tArr) {
        kd4.b(tArr, "$this$toMutableSet");
        LinkedHashSet linkedHashSet = new LinkedHashSet(rb4.a(tArr.length));
        for (T add : tArr) {
            linkedHashSet.add(add);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> T[] b(T[] tArr, Comparator<? super T> comparator) {
        kd4.b(tArr, "$this$sortedArrayWith");
        kd4.b(comparator, "comparator");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        kd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        ya4.a(copyOf, comparator);
        return copyOf;
    }

    @DexIgnore
    public static final int b(byte[] bArr) {
        kd4.b(bArr, "$this$lastIndex");
        return bArr.length - 1;
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T[] a(T[] tArr) {
        kd4.b(tArr, "$this$sortedArray");
        if (tArr.length == 0) {
            return tArr;
        }
        T[] copyOf = Arrays.copyOf(tArr, tArr.length);
        kd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
        T[] tArr2 = (Comparable[]) copyOf;
        if (tArr2 != null) {
            ya4.b(tArr2);
            return tArr2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
    }

    @DexIgnore
    public static final byte[] c(byte[] bArr) {
        kd4.b(bArr, "$this$reversedArray");
        int i = 0;
        if (bArr.length == 0) {
            return bArr;
        }
        byte[] bArr2 = new byte[bArr.length];
        int b = b(bArr);
        if (b >= 0) {
            while (true) {
                bArr2[b - i] = bArr[i];
                if (i == b) {
                    break;
                }
                i++;
            }
        }
        return bArr2;
    }

    @DexIgnore
    public static final yd4 a(byte[] bArr) {
        kd4.b(bArr, "$this$indices");
        return new yd4(0, b(bArr));
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(T[] tArr, C c) {
        kd4.b(tArr, "$this$toCollection");
        kd4.b(c, ShareConstants.DESTINATION);
        for (T add : tArr) {
            c.add(add);
        }
        return c;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr, Comparator<? super T> comparator) {
        kd4.b(tArr, "$this$sortedWith");
        kd4.b(comparator, "comparator");
        return ya4.a((T[]) b(tArr, comparator));
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(T[] tArr, A a, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4<? super T, ? extends CharSequence> xc4) {
        kd4.b(tArr, "$this$joinTo");
        kd4.b(a, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        kd4.b(charSequence, "separator");
        kd4.b(charSequence2, "prefix");
        kd4.b(charSequence3, "postfix");
        kd4.b(charSequence4, "truncated");
        a.append(charSequence2);
        int i2 = 0;
        for (T t : tArr) {
            i2++;
            if (i2 > 1) {
                a.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            nf4.a(a, t, xc4);
        }
        if (i >= 0 && i2 > i) {
            a.append(charSequence4);
        }
        a.append(charSequence3);
        return a;
    }

    @DexIgnore
    public static final <T> List<T> c(T[] tArr) {
        kd4.b(tArr, "$this$distinct");
        return kb4.k(g(tArr));
    }

    @DexIgnore
    public static /* synthetic */ String a(Object[] objArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4 xc4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            xc4 = null;
        }
        return a(objArr, charSequence, charSequence6, charSequence5, i3, charSequence7, xc4);
    }

    @DexIgnore
    public static final <T> String a(T[] tArr, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4<? super T, ? extends CharSequence> xc4) {
        kd4.b(tArr, "$this$joinToString");
        kd4.b(charSequence, "separator");
        kd4.b(charSequence2, "prefix");
        kd4.b(charSequence3, "postfix");
        kd4.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(tArr, sb, charSequence, charSequence2, charSequence3, i, charSequence4, xc4);
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
