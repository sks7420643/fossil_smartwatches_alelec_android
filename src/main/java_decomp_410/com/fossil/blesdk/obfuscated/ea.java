package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.fossil.blesdk.obfuscated.fa;
import com.j256.ormlite.field.FieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ea extends BaseAdapter implements Filterable, fa.a {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Cursor g;
    @DexIgnore
    public Context h;
    @DexIgnore
    public int i;
    @DexIgnore
    public a j;
    @DexIgnore
    public DataSetObserver k;
    @DexIgnore
    public fa l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            ea.this.b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends DataSetObserver {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onChanged() {
            ea eaVar = ea.this;
            eaVar.e = true;
            eaVar.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            ea eaVar = ea.this;
            eaVar.e = false;
            eaVar.notifyDataSetInvalidated();
        }
    }

    @DexIgnore
    public ea(Context context, Cursor cursor, boolean z) {
        a(context, cursor, z ? 1 : 2);
    }

    @DexIgnore
    public abstract View a(Context context, Cursor cursor, ViewGroup viewGroup);

    @DexIgnore
    public void a(Context context, Cursor cursor, int i2) {
        boolean z = false;
        if ((i2 & 1) == 1) {
            i2 |= 2;
            this.f = true;
        } else {
            this.f = false;
        }
        if (cursor != null) {
            z = true;
        }
        this.g = cursor;
        this.e = z;
        this.h = context;
        this.i = z ? cursor.getColumnIndexOrThrow(FieldType.FOREIGN_ID_FIELD_SUFFIX) : -1;
        if ((i2 & 2) == 2) {
            this.j = new a();
            this.k = new b();
        } else {
            this.j = null;
            this.k = null;
        }
        if (z) {
            a aVar = this.j;
            if (aVar != null) {
                cursor.registerContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.k;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    @DexIgnore
    public abstract void a(View view, Context context, Cursor cursor);

    @DexIgnore
    public abstract View b(Context context, Cursor cursor, ViewGroup viewGroup);

    @DexIgnore
    public abstract CharSequence b(Cursor cursor);

    @DexIgnore
    public void b() {
        if (this.f) {
            Cursor cursor = this.g;
            if (cursor != null && !cursor.isClosed()) {
                this.e = this.g.requery();
            }
        }
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        Cursor cursor2 = this.g;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            a aVar = this.j;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.k;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.g = cursor;
        if (cursor != null) {
            a aVar2 = this.j;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.k;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.i = cursor.getColumnIndexOrThrow(FieldType.FOREIGN_ID_FIELD_SUFFIX);
            this.e = true;
            notifyDataSetChanged();
        } else {
            this.i = -1;
            this.e = false;
            notifyDataSetInvalidated();
        }
        return cursor2;
    }

    @DexIgnore
    public int getCount() {
        if (!this.e) {
            return 0;
        }
        Cursor cursor = this.g;
        if (cursor != null) {
            return cursor.getCount();
        }
        return 0;
    }

    @DexIgnore
    public View getDropDownView(int i2, View view, ViewGroup viewGroup) {
        if (!this.e) {
            return null;
        }
        this.g.moveToPosition(i2);
        if (view == null) {
            view = a(this.h, this.g, viewGroup);
        }
        a(view, this.h, this.g);
        return view;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.l == null) {
            this.l = new fa(this);
        }
        return this.l;
    }

    @DexIgnore
    public Object getItem(int i2) {
        if (!this.e) {
            return null;
        }
        Cursor cursor = this.g;
        if (cursor == null) {
            return null;
        }
        cursor.moveToPosition(i2);
        return this.g;
    }

    @DexIgnore
    public long getItemId(int i2) {
        if (this.e) {
            Cursor cursor = this.g;
            if (cursor != null && cursor.moveToPosition(i2)) {
                return this.g.getLong(this.i);
            }
        }
        return 0;
    }

    @DexIgnore
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (!this.e) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.g.moveToPosition(i2)) {
            if (view == null) {
                view = b(this.h, this.g, viewGroup);
            }
            a(view, this.h, this.g);
            return view;
        } else {
            throw new IllegalStateException("couldn't move cursor to position " + i2);
        }
    }

    @DexIgnore
    public Cursor a() {
        return this.g;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        Cursor c = c(cursor);
        if (c != null) {
            c.close();
        }
    }
}
