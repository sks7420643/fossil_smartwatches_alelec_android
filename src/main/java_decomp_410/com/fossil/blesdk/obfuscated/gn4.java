package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.dm4;
import com.misfit.frameworks.common.enums.Action;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.SocketTimeoutException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ConnectionShutdownException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gn4 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public volatile wm4 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public volatile boolean d;

    @DexIgnore
    public gn4(OkHttpClient okHttpClient, boolean z) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public void a() {
        this.d = true;
        wm4 wm4 = this.b;
        if (wm4 != null) {
            wm4.a();
        }
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        dm4 n = chain.n();
        dn4 dn4 = (dn4) chain;
        jl4 e = dn4.e();
        vl4 f = dn4.f();
        wm4 wm4 = new wm4(this.a.e(), a(n.g()), e, f, this.c);
        this.b = wm4;
        Response response = null;
        int i = 0;
        while (!this.d) {
            try {
                Response a2 = dn4.a(n, wm4, (zm4) null, (tm4) null);
                if (response != null) {
                    Response.a H = a2.H();
                    Response.a H2 = response.H();
                    H2.a((em4) null);
                    H.d(H2.a());
                    a2 = H.a();
                }
                try {
                    dm4 a3 = a(a2, wm4.h());
                    if (a3 == null) {
                        wm4.f();
                        return a2;
                    }
                    jm4.a((Closeable) a2.y());
                    int i2 = i + 1;
                    if (i2 <= 20) {
                        a3.a();
                        if (!a(a2, a3.g())) {
                            wm4.f();
                            wm4 = new wm4(this.a.e(), a(a3.g()), e, f, this.c);
                            this.b = wm4;
                        } else if (wm4.b() != null) {
                            throw new IllegalStateException("Closing the body of " + a2 + " didn't close its backing stream. Bad interceptor?");
                        }
                        response = a2;
                        n = a3;
                        i = i2;
                    } else {
                        wm4.f();
                        throw new ProtocolException("Too many follow-up requests: " + i2);
                    }
                } catch (IOException e2) {
                    wm4.f();
                    throw e2;
                }
            } catch (RouteException e3) {
                if (!a(e3.getLastConnectException(), wm4, false, n)) {
                    throw e3.getFirstConnectException();
                }
            } catch (IOException e4) {
                if (!a(e4, wm4, !(e4 instanceof ConnectionShutdownException), n)) {
                    throw e4;
                }
            } catch (Throwable th) {
                wm4.a((IOException) null);
                wm4.f();
                throw th;
            }
        }
        wm4.f();
        throw new IOException("Canceled");
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj;
    }

    @DexIgnore
    public final gl4 a(zl4 zl4) {
        ll4 ll4;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (zl4.h()) {
            SSLSocketFactory H = this.a.H();
            hostnameVerifier = this.a.m();
            sSLSocketFactory = H;
            ll4 = this.a.c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            ll4 = null;
        }
        return new gl4(zl4.g(), zl4.k(), this.a.i(), this.a.G(), sSLSocketFactory, hostnameVerifier, ll4, this.a.C(), this.a.B(), this.a.A(), this.a.f(), this.a.D());
    }

    @DexIgnore
    public final boolean a(IOException iOException, wm4 wm4, boolean z, dm4 dm4) {
        wm4.a(iOException);
        if (!this.a.F()) {
            return false;
        }
        if (z) {
            dm4.a();
        }
        if (a(iOException, z) && wm4.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(IOException iOException, boolean z) {
        if (iOException instanceof ProtocolException) {
            return false;
        }
        if (iOException instanceof InterruptedIOException) {
            if (!(iOException instanceof SocketTimeoutException) || z) {
                return false;
            }
            return true;
        } else if ((!(iOException instanceof SSLHandshakeException) || !(iOException.getCause() instanceof CertificateException)) && !(iOException instanceof SSLPeerUnverifiedException)) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final dm4 a(Response response, fm4 fm4) throws IOException {
        Proxy proxy;
        if (response != null) {
            int B = response.B();
            String e = response.L().e();
            RequestBody requestBody = null;
            if (B == 307 || B == 308) {
                if (!e.equals("GET") && !e.equals("HEAD")) {
                    return null;
                }
            } else if (B == 401) {
                return this.a.a().authenticate(fm4, response);
            } else {
                if (B != 503) {
                    if (B == 407) {
                        if (fm4 != null) {
                            proxy = fm4.b();
                        } else {
                            proxy = this.a.B();
                        }
                        if (proxy.type() == Proxy.Type.HTTP) {
                            return this.a.C().authenticate(fm4, response);
                        }
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    } else if (B != 408) {
                        switch (B) {
                            case 300:
                            case Action.Presenter.NEXT /*301*/:
                            case Action.Presenter.PREVIOUS /*302*/:
                            case Action.Presenter.BLACKOUT /*303*/:
                                break;
                            default:
                                return null;
                        }
                    } else if (!this.a.F()) {
                        return null;
                    } else {
                        response.L().a();
                        if ((response.I() == null || response.I().B() != 408) && a(response, 0) <= 0) {
                            return response.L();
                        }
                        return null;
                    }
                } else if ((response.I() == null || response.I().B() != 503) && a(response, Integer.MAX_VALUE) == 0) {
                    return response.L();
                } else {
                    return null;
                }
            }
            if (!this.a.k()) {
                return null;
            }
            String e2 = response.e("Location");
            if (e2 == null) {
                return null;
            }
            zl4 b2 = response.L().g().b(e2);
            if (b2 == null) {
                return null;
            }
            if (!b2.n().equals(response.L().g().n()) && !this.a.l()) {
                return null;
            }
            dm4.a f = response.L().f();
            if (cn4.b(e)) {
                boolean d2 = cn4.d(e);
                if (cn4.c(e)) {
                    f.a("GET", (RequestBody) null);
                } else {
                    if (d2) {
                        requestBody = response.L().a();
                    }
                    f.a(e, requestBody);
                }
                if (!d2) {
                    f.a("Transfer-Encoding");
                    f.a("Content-Length");
                    f.a(GraphRequest.CONTENT_TYPE_HEADER);
                }
            }
            if (!a(response, b2)) {
                f.a("Authorization");
            }
            f.a(b2);
            return f.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final int a(Response response, int i) {
        String e = response.e("Retry-After");
        if (e == null) {
            return i;
        }
        if (e.matches("\\d+")) {
            return Integer.valueOf(e).intValue();
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public final boolean a(Response response, zl4 zl4) {
        zl4 g = response.L().g();
        return g.g().equals(zl4.g()) && g.k() == zl4.k() && g.n().equals(zl4.n());
    }
}
