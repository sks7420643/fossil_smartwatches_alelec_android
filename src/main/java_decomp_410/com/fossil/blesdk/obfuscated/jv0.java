package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jv0 implements kw0 {
    @DexIgnore
    public static /* final */ rv0 b; // = new kv0();
    @DexIgnore
    public /* final */ rv0 a;

    @DexIgnore
    public jv0() {
        this(new lv0(qu0.a(), a()));
    }

    @DexIgnore
    public jv0(rv0 rv0) {
        tu0.a(rv0, "messageInfoFactory");
        this.a = rv0;
    }

    @DexIgnore
    public static rv0 a() {
        try {
            return (rv0) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    @DexIgnore
    public static boolean a(qv0 qv0) {
        return qv0.a() == ru0.e.i;
    }

    @DexIgnore
    public final <T> jw0<T> a(Class<T> cls) {
        Class<ru0> cls2 = ru0.class;
        lw0.a((Class<?>) cls);
        qv0 zzb = this.a.zzb(cls);
        if (zzb.b()) {
            return cls2.isAssignableFrom(cls) ? xv0.a(lw0.c(), ju0.b(), zzb.c()) : xv0.a(lw0.a(), ju0.c(), zzb.c());
        }
        if (cls2.isAssignableFrom(cls)) {
            boolean a2 = a(zzb);
            zv0 b2 = bw0.b();
            ev0 b3 = ev0.b();
            ax0<?, ?> c = lw0.c();
            if (a2) {
                return wv0.a(cls, zzb, b2, b3, c, ju0.b(), pv0.b());
            }
            return wv0.a(cls, zzb, b2, b3, c, (gu0<?>) null, pv0.b());
        }
        boolean a3 = a(zzb);
        zv0 a4 = bw0.a();
        ev0 a5 = ev0.a();
        if (a3) {
            return wv0.a(cls, zzb, a4, a5, lw0.a(), ju0.c(), pv0.a());
        }
        return wv0.a(cls, zzb, a4, a5, lw0.b(), (gu0<?>) null, pv0.a());
    }
}
