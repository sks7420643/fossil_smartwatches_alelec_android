package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.fossil.blesdk.obfuscated.ij0;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uf0 implements ij0.c {
    @DexIgnore
    public /* final */ WeakReference<sf0> a;
    @DexIgnore
    public /* final */ de0<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public uf0(sf0 sf0, de0<?> de0, boolean z) {
        this.a = new WeakReference<>(sf0);
        this.b = de0;
        this.c = z;
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        sf0 sf0 = (sf0) this.a.get();
        if (sf0 != null) {
            bk0.b(Looper.myLooper() == sf0.a.r.f(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            sf0.b.lock();
            try {
                if (sf0.a(0)) {
                    if (!ud0.L()) {
                        sf0.b(ud0, this.b, this.c);
                    }
                    if (sf0.d()) {
                        sf0.e();
                    }
                    sf0.b.unlock();
                }
            } finally {
                sf0.b.unlock();
            }
        }
    }
}
