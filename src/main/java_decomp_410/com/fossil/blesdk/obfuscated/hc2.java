package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hc2 extends gc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public long F;

    /*
    static {
        H.put(R.id.cl_top, 1);
        H.put(R.id.iv_back, 2);
        H.put(R.id.tv_title, 3);
        H.put(R.id.cl_overview_day, 4);
        H.put(R.id.iv_back_date, 5);
        H.put(R.id.ftv_day_of_week, 6);
        H.put(R.id.ftv_day_of_month, 7);
        H.put(R.id.line, 8);
        H.put(R.id.cl_container, 9);
        H.put(R.id.guideline1, 10);
        H.put(R.id.ftv_resting_value, 11);
        H.put(R.id.ftv_resting_unit, 12);
        H.put(R.id.separatedLine, 13);
        H.put(R.id.ftv_max_value, 14);
        H.put(R.id.ftv_max_unit, 15);
        H.put(R.id.ftv_no_record, 16);
        H.put(R.id.iv_next_date, 17);
        H.put(R.id.appBarLayout, 18);
        H.put(R.id.dayChart, 19);
        H.put(R.id.llWorkout, 20);
        H.put(R.id.v_elevation, 21);
        H.put(R.id.ftv_no_workout_recorded, 22);
        H.put(R.id.rvWorkout, 23);
    }
    */

    @DexIgnore
    public hc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 24, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public hc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[18], objArr[9], objArr[4], objArr[1], objArr[19], objArr[7], objArr[6], objArr[15], objArr[14], objArr[16], objArr[22], objArr[12], objArr[11], objArr[10], objArr[2], objArr[5], objArr[17], objArr[8], objArr[20], objArr[23], objArr[13], objArr[3], objArr[21]);
        this.F = -1;
        this.E = objArr[0];
        this.E.setTag((Object) null);
        a(view);
        f();
    }
}
