package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.bumptech.glide.load.EncodeStrategy;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ms implements no<BitmapDrawable> {
    @DexIgnore
    public /* final */ jq a;
    @DexIgnore
    public /* final */ no<Bitmap> b;

    @DexIgnore
    public ms(jq jqVar, no<Bitmap> noVar) {
        this.a = jqVar;
        this.b = noVar;
    }

    @DexIgnore
    public boolean a(aq<BitmapDrawable> aqVar, File file, lo loVar) {
        return this.b.a(new ps(aqVar.get().getBitmap(), this.a), file, loVar);
    }

    @DexIgnore
    public EncodeStrategy a(lo loVar) {
        return this.b.a(loVar);
    }
}
