package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xe extends bf {
    @DexIgnore
    public we d;
    @DexIgnore
    public we e;

    @DexIgnore
    public int[] a(RecyclerView.m mVar, View view) {
        int[] iArr = new int[2];
        if (mVar.a()) {
            iArr[0] = a(mVar, view, d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.b()) {
            iArr[1] = a(mVar, view, e(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    public se b(RecyclerView.m mVar) {
        if (!(mVar instanceof RecyclerView.v.b)) {
            return null;
        }
        return new a(this.a.getContext());
    }

    @DexIgnore
    public View c(RecyclerView.m mVar) {
        if (mVar.b()) {
            return a(mVar, e(mVar));
        }
        if (mVar.a()) {
            return a(mVar, d(mVar));
        }
        return null;
    }

    @DexIgnore
    public final we d(RecyclerView.m mVar) {
        we weVar = this.e;
        if (weVar == null || weVar.a != mVar) {
            this.e = we.a(mVar);
        }
        return this.e;
    }

    @DexIgnore
    public final we e(RecyclerView.m mVar) {
        we weVar = this.d;
        if (weVar == null || weVar.a != mVar) {
            this.d = we.b(mVar);
        }
        return this.d;
    }

    @DexIgnore
    public final View b(RecyclerView.m mVar, we weVar) {
        int e2 = mVar.e();
        View view = null;
        if (e2 == 0) {
            return null;
        }
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < e2; i2++) {
            View d2 = mVar.d(i2);
            int d3 = weVar.d(d2);
            if (d3 < i) {
                view = d2;
                i = d3;
            }
        }
        return view;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends se {
        @DexIgnore
        public a(Context context) {
            super(context);
        }

        @DexIgnore
        public void a(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
            xe xeVar = xe.this;
            int[] a = xeVar.a(xeVar.a.getLayoutManager(), view);
            int i = a[0];
            int i2 = a[1];
            int d = d(Math.max(Math.abs(i), Math.abs(i2)));
            if (d > 0) {
                aVar.a(i, i2, d, this.j);
            }
        }

        @DexIgnore
        public int e(int i) {
            return Math.min(100, super.e(i));
        }

        @DexIgnore
        public float a(DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    public int a(RecyclerView.m mVar, int i, int i2) {
        int j = mVar.j();
        if (j == 0) {
            return -1;
        }
        View view = null;
        if (mVar.b()) {
            view = b(mVar, e(mVar));
        } else if (mVar.a()) {
            view = b(mVar, d(mVar));
        }
        if (view == null) {
            return -1;
        }
        int l = mVar.l(view);
        if (l == -1) {
            return -1;
        }
        boolean z = false;
        boolean z2 = !mVar.a() ? i2 > 0 : i > 0;
        if (mVar instanceof RecyclerView.v.b) {
            PointF a2 = ((RecyclerView.v.b) mVar).a(j - 1);
            if (a2 != null && (a2.x < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || a2.y < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                z = true;
            }
        }
        return z ? z2 ? l - 1 : l : z2 ? l + 1 : l;
    }

    @DexIgnore
    public final int a(RecyclerView.m mVar, View view, we weVar) {
        int i;
        int d2 = weVar.d(view) + (weVar.b(view) / 2);
        if (mVar.f()) {
            i = weVar.f() + (weVar.g() / 2);
        } else {
            i = weVar.a() / 2;
        }
        return d2 - i;
    }

    @DexIgnore
    public final View a(RecyclerView.m mVar, we weVar) {
        int i;
        int e2 = mVar.e();
        View view = null;
        if (e2 == 0) {
            return null;
        }
        if (mVar.f()) {
            i = weVar.f() + (weVar.g() / 2);
        } else {
            i = weVar.a() / 2;
        }
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < e2; i3++) {
            View d2 = mVar.d(i3);
            int abs = Math.abs((weVar.d(d2) + (weVar.b(d2) / 2)) - i);
            if (abs < i2) {
                view = d2;
                i2 = abs;
            }
        }
        return view;
    }
}
