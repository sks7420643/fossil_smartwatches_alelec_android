package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xx */
public class C3328xx {

    @DexIgnore
    /* renamed from: a */
    public long f11103a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.n64 f11104b;

    @DexIgnore
    public C3328xx(com.fossil.blesdk.obfuscated.n64 n64) {
        if (n64 != null) {
            this.f11104b = n64;
            return;
        }
        throw new java.lang.NullPointerException("retryState must not be null");
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17922a(long j) {
        return j - this.f11103a >= this.f11104b.mo29588a() * 1000000;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17923b(long j) {
        this.f11103a = j;
        this.f11104b = this.f11104b.mo29590c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17921a() {
        this.f11103a = 0;
        this.f11104b = this.f11104b.mo29589b();
    }
}
