package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lr */
public class C2340lr implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2432mr f7259b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.net.URL f7260c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.String f7261d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.String f7262e;

    @DexIgnore
    /* renamed from: f */
    public java.net.URL f7263f;

    @DexIgnore
    /* renamed from: g */
    public volatile byte[] f7264g;

    @DexIgnore
    /* renamed from: h */
    public int f7265h;

    @DexIgnore
    public C2340lr(java.net.URL url) {
        this(url, com.fossil.blesdk.obfuscated.C2432mr.f7568a);
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo13333a() {
        java.lang.String str = this.f7261d;
        if (str != null) {
            return str;
        }
        java.net.URL url = this.f7260c;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(url);
        return url.toString();
    }

    @DexIgnore
    /* renamed from: b */
    public final byte[] mo13334b() {
        if (this.f7264g == null) {
            this.f7264g = mo13333a().getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a);
        }
        return this.f7264g;
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.Map<java.lang.String, java.lang.String> mo13335c() {
        return this.f7259b.mo13704a();
    }

    @DexIgnore
    /* renamed from: d */
    public final java.lang.String mo13336d() {
        if (android.text.TextUtils.isEmpty(this.f7262e)) {
            java.lang.String str = this.f7261d;
            if (android.text.TextUtils.isEmpty(str)) {
                java.net.URL url = this.f7260c;
                com.fossil.blesdk.obfuscated.C2992tw.m14457a(url);
                str = url.toString();
            }
            this.f7262e = android.net.Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.f7262e;
    }

    @DexIgnore
    /* renamed from: e */
    public final java.net.URL mo13337e() throws java.net.MalformedURLException {
        if (this.f7263f == null) {
            this.f7263f = new java.net.URL(mo13336d());
        }
        return this.f7263f;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2340lr)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2340lr lrVar = (com.fossil.blesdk.obfuscated.C2340lr) obj;
        if (!mo13333a().equals(lrVar.mo13333a()) || !this.f7259b.equals(lrVar.f7259b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: f */
    public java.net.URL mo13338f() throws java.net.MalformedURLException {
        return mo13337e();
    }

    @DexIgnore
    public int hashCode() {
        if (this.f7265h == 0) {
            this.f7265h = mo13333a().hashCode();
            this.f7265h = (this.f7265h * 31) + this.f7259b.hashCode();
        }
        return this.f7265h;
    }

    @DexIgnore
    public java.lang.String toString() {
        return mo13333a();
    }

    @DexIgnore
    public C2340lr(java.lang.String str) {
        this(str, com.fossil.blesdk.obfuscated.C2432mr.f7568a);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        messageDigest.update(mo13334b());
    }

    @DexIgnore
    public C2340lr(java.net.URL url, com.fossil.blesdk.obfuscated.C2432mr mrVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(url);
        this.f7260c = url;
        this.f7261d = null;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(mrVar);
        this.f7259b = mrVar;
    }

    @DexIgnore
    public C2340lr(java.lang.String str, com.fossil.blesdk.obfuscated.C2432mr mrVar) {
        this.f7260c = null;
        com.fossil.blesdk.obfuscated.C2992tw.m14459a(str);
        this.f7261d = str;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(mrVar);
        this.f7259b = mrVar;
    }
}
