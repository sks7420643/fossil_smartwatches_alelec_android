package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l10 extends BluetoothCommand {
    @DexIgnore
    public int k; // = 23;
    @DexIgnore
    public /* final */ int l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l10(int i, Peripheral.c cVar) {
        super(BluetoothCommandId.REQUEST_MTU, cVar);
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.l = i;
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        kd4.b(peripheral, "peripheral");
        peripheral.a(this.l);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        return gattOperationResult instanceof z10;
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        this.k = ((z10) gattOperationResult).b();
    }

    @DexIgnore
    public ra0<GattOperationResult> f() {
        return b().j();
    }

    @DexIgnore
    public final int i() {
        return this.k;
    }
}
