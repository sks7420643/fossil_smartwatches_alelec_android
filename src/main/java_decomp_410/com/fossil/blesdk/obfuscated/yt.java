package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yt {
    @DexIgnore
    public /* final */ co a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public /* final */ xn d;
    @DexIgnore
    public /* final */ jq e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public wn<Bitmap> i;
    @DexIgnore
    public a j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public a l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public a n;
    @DexIgnore
    public d o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends wv<Bitmap> {
        @DexIgnore
        public /* final */ Handler h;
        @DexIgnore
        public /* final */ int i;
        @DexIgnore
        public /* final */ long j;
        @DexIgnore
        public Bitmap k;

        @DexIgnore
        public a(Handler handler, int i2, long j2) {
            this.h = handler;
            this.i = i2;
            this.j = j2;
        }

        @DexIgnore
        public void c(Drawable drawable) {
            this.k = null;
        }

        @DexIgnore
        public Bitmap e() {
            return this.k;
        }

        @DexIgnore
        public void a(Bitmap bitmap, ew<? super Bitmap> ewVar) {
            this.k = bitmap;
            this.h.sendMessageAtTime(this.h.obtainMessage(1, this), this.j);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Handler.Callback {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                yt.this.a((a) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                yt.this.d.a((bw<?>) (a) message.obj);
                return false;
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public yt(rn rnVar, co coVar, int i2, int i3, oo<Bitmap> ooVar, Bitmap bitmap) {
        this(rnVar.c(), rn.d(rnVar.e()), coVar, (Handler) null, a(rn.d(rnVar.e()), i2, i3), ooVar, bitmap);
    }

    @DexIgnore
    public static jo n() {
        return new jw(Double.valueOf(Math.random()));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [java.lang.Object, com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public void a(oo<Bitmap> r3, Bitmap bitmap) {
        tw.a(r3);
        oo ooVar = r3;
        tw.a(bitmap);
        this.m = bitmap;
        this.i = this.i.a(new rv().a((oo<Bitmap>) r3));
        this.p = uw.a(bitmap);
        this.q = bitmap.getWidth();
        this.r = bitmap.getHeight();
    }

    @DexIgnore
    public void b(b bVar) {
        this.c.remove(bVar);
        if (this.c.isEmpty()) {
            m();
        }
    }

    @DexIgnore
    public Bitmap c() {
        a aVar = this.j;
        return aVar != null ? aVar.e() : this.m;
    }

    @DexIgnore
    public int d() {
        a aVar = this.j;
        if (aVar != null) {
            return aVar.i;
        }
        return -1;
    }

    @DexIgnore
    public Bitmap e() {
        return this.m;
    }

    @DexIgnore
    public int f() {
        return this.a.c();
    }

    @DexIgnore
    public int g() {
        return this.r;
    }

    @DexIgnore
    public int h() {
        return this.a.h() + this.p;
    }

    @DexIgnore
    public int i() {
        return this.q;
    }

    @DexIgnore
    public final void j() {
        if (this.f && !this.g) {
            if (this.h) {
                tw.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.a.f();
                this.h = false;
            }
            a aVar = this.n;
            if (aVar != null) {
                this.n = null;
                a(aVar);
                return;
            }
            this.g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.a.d());
            this.a.b();
            this.l = new a(this.b, this.a.g(), uptimeMillis);
            this.i.a((lv) rv.b(n())).a((Object) this.a).a(this.l);
        }
    }

    @DexIgnore
    public final void k() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.a(bitmap);
            this.m = null;
        }
    }

    @DexIgnore
    public final void l() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            j();
        }
    }

    @DexIgnore
    public final void m() {
        this.f = false;
    }

    @DexIgnore
    public ByteBuffer b() {
        return this.a.e().asReadOnlyBuffer();
    }

    @DexIgnore
    public yt(jq jqVar, xn xnVar, co coVar, Handler handler, wn<Bitmap> wnVar, oo<Bitmap> ooVar, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = xnVar;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = jqVar;
        this.b = handler;
        this.i = wnVar;
        this.a = coVar;
        a(ooVar, bitmap);
    }

    @DexIgnore
    public void a(b bVar) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(bVar)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(bVar);
            if (isEmpty) {
                l();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    public void a() {
        this.c.clear();
        k();
        m();
        a aVar = this.j;
        if (aVar != null) {
            this.d.a((bw<?>) aVar);
            this.j = null;
        }
        a aVar2 = this.l;
        if (aVar2 != null) {
            this.d.a((bw<?>) aVar2);
            this.l = null;
        }
        a aVar3 = this.n;
        if (aVar3 != null) {
            this.d.a((bw<?>) aVar3);
            this.n = null;
        }
        this.a.clear();
        this.k = true;
    }

    @DexIgnore
    public void a(a aVar) {
        d dVar = this.o;
        if (dVar != null) {
            dVar.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f) {
            this.n = aVar;
        } else {
            if (aVar.e() != null) {
                k();
                a aVar2 = this.j;
                this.j = aVar;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            j();
        }
    }

    @DexIgnore
    public static wn<Bitmap> a(xn xnVar, int i2, int i3) {
        return xnVar.e().a(((rv) ((rv) rv.b(pp.a).b(true)).a(true)).a(i2, i3));
    }
}
