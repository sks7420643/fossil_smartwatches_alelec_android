package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gi1 implements Callable<List<ml1>> {
    @DexIgnore
    public /* final */ /* synthetic */ rl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ zh1 h;

    @DexIgnore
    public gi1(zh1 zh1, rl1 rl1, String str, String str2) {
        this.h = zh1;
        this.e = rl1;
        this.f = str;
        this.g = str2;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        this.h.e.y();
        return this.h.e.l().a(this.e.e, this.f, this.g);
    }
}
