package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m14 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ t04 a;

    @DexIgnore
    public m14(t04 t04) {
        this.a = t04;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        if (this.a.e != null) {
            this.a.e.a(new r14(this));
        }
    }
}
