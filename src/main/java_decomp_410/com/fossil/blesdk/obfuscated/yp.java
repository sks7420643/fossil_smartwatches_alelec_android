package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.op;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yp<Data, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ g8<List<Throwable>> a;
    @DexIgnore
    public /* final */ List<? extends op<Data, ResourceType, Transcode>> b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public yp(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<op<Data, ResourceType, Transcode>> list, g8<List<Throwable>> g8Var) {
        this.a = g8Var;
        tw.a(list);
        this.b = list;
        this.c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public aq<Transcode> a(to<Data> toVar, lo loVar, int i, int i2, op.a<ResourceType> aVar) throws GlideException {
        List<Throwable> a2 = this.a.a();
        tw.a(a2);
        List list = a2;
        try {
            return a(toVar, loVar, i, i2, aVar, list);
        } finally {
            this.a.a(list);
        }
    }

    @DexIgnore
    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.b.toArray()) + '}';
    }

    @DexIgnore
    public final aq<Transcode> a(to<Data> toVar, lo loVar, int i, int i2, op.a<ResourceType> aVar, List<Throwable> list) throws GlideException {
        List<Throwable> list2 = list;
        int size = this.b.size();
        aq<Transcode> aqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                aqVar = ((op) this.b.get(i3)).a(toVar, i, i2, loVar, aVar);
            } catch (GlideException e) {
                list2.add(e);
            }
            if (aqVar != null) {
                break;
            }
        }
        if (aqVar != null) {
            return aqVar;
        }
        throw new GlideException(this.c, (List<Throwable>) new ArrayList(list2));
    }
}
