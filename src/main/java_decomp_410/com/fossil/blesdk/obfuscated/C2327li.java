package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.li */
public interface C2327li extends android.graphics.drawable.Animatable {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.li$a")
    /* renamed from: com.fossil.blesdk.obfuscated.li$a */
    public static abstract class C2328a {
        @DexIgnore
        /* renamed from: a */
        public void mo13295a(android.graphics.drawable.Drawable drawable) {
        }
    }
}
