package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pz {
    @DexIgnore
    public static byte[] a(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    @DexIgnore
    public static byte[] b(File file) {
        File a = a(file, ".dmp");
        if (a == null) {
            return new byte[0];
        }
        return c(a);
    }

    @DexIgnore
    public static byte[] c(File file) {
        return d(file);
    }

    @DexIgnore
    public static byte[] d(File file) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                byte[] a = a((InputStream) fileInputStream);
                CommonUtils.a((Closeable) fileInputStream);
                return a;
            } catch (FileNotFoundException unused) {
                CommonUtils.a((Closeable) fileInputStream);
                return null;
            } catch (IOException unused2) {
                CommonUtils.a((Closeable) fileInputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = fileInputStream;
                CommonUtils.a((Closeable) fileInputStream2);
                throw th;
            }
        } catch (FileNotFoundException unused3) {
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream);
            return null;
        } catch (IOException unused4) {
            fileInputStream = null;
            CommonUtils.a((Closeable) fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            CommonUtils.a((Closeable) fileInputStream2);
            throw th;
        }
    }

    @DexIgnore
    public static byte[] c(File file, Context context) throws IOException {
        BufferedReader bufferedReader;
        if (!file.exists()) {
            return null;
        }
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                byte[] a = new ny(context, new b00()).a(bufferedReader);
                CommonUtils.a((Closeable) bufferedReader);
                return a;
            } catch (Throwable th) {
                th = th;
                CommonUtils.a((Closeable) bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            CommonUtils.a((Closeable) bufferedReader);
            throw th;
        }
    }

    @DexIgnore
    public static byte[] b(File file, Context context) throws IOException {
        File a = a(file, ".maps");
        if (a != null) {
            return c(a, context);
        }
        File a2 = a(file, ".binary_libs");
        if (a2 != null) {
            return a(a2, context);
        }
        return null;
    }

    @DexIgnore
    public static File a(File file, String str) {
        for (File file2 : file.listFiles()) {
            if (file2.getName().endsWith(str)) {
                return file2;
            }
        }
        return null;
    }

    @DexIgnore
    public static byte[] a(File file, Context context) throws IOException {
        byte[] d = d(file);
        if (d == null || d.length == 0) {
            return null;
        }
        return a(context, new String(d));
    }

    @DexIgnore
    public static byte[] a(File file) {
        File a = a(file, ".device_info");
        if (a == null) {
            return null;
        }
        return d(a);
    }

    @DexIgnore
    public static byte[] a(Context context, String str) throws IOException {
        return new ny(context, new b00()).a(str);
    }
}
