package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cj3 implements Factory<ProfileOptInPresenter> {
    @DexIgnore
    public static ProfileOptInPresenter a(yi3 yi3, UpdateUser updateUser, UserRepository userRepository) {
        return new ProfileOptInPresenter(yi3, updateUser, userRepository);
    }
}
