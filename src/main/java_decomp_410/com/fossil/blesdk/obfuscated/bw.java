package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bw<R> extends ru {
    @DexIgnore
    void a(Drawable drawable);

    @DexIgnore
    void a(aw awVar);

    @DexIgnore
    void a(ov ovVar);

    @DexIgnore
    void a(R r, ew<? super R> ewVar);

    @DexIgnore
    void b(Drawable drawable);

    @DexIgnore
    void b(aw awVar);

    @DexIgnore
    void c(Drawable drawable);

    @DexIgnore
    ov d();
}
