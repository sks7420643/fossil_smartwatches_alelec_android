package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dp0 {
    @DexIgnore
    public static /* final */ bp0 A; // = bp0.f("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ bp0 B; // = bp0.f("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ bp0 C; // = bp0.f("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ bp0 D; // = bp0.f("cervical_position");
    @DexIgnore
    public static /* final */ bp0 E; // = bp0.f("cervical_dilation");
    @DexIgnore
    public static /* final */ bp0 F; // = bp0.f("cervical_firmness");
    @DexIgnore
    public static /* final */ bp0 G; // = bp0.f("menstrual_flow");
    @DexIgnore
    public static /* final */ bp0 H; // = bp0.f("ovulation_test_result");
    @DexIgnore
    public static /* final */ bp0 a; // = bp0.g("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ bp0 b; // = bp0.g("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ bp0 c; // = bp0.g("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ bp0 d; // = bp0.g("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ bp0 e; // = bp0.g("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ bp0 f; // = bp0.g("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ bp0 g; // = bp0.g("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ bp0 h; // = bp0.g("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ bp0 i; // = bp0.f("body_position");
    @DexIgnore
    public static /* final */ bp0 j; // = bp0.f("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ bp0 k; // = bp0.g("blood_glucose_level");
    @DexIgnore
    public static /* final */ bp0 l; // = bp0.f("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ bp0 m; // = bp0.f("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ bp0 n; // = bp0.f("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ bp0 o; // = bp0.g("oxygen_saturation");
    @DexIgnore
    public static /* final */ bp0 p; // = bp0.g("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ bp0 q; // = bp0.g("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ bp0 r; // = bp0.g("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ bp0 s; // = bp0.g("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ bp0 t; // = bp0.g("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ bp0 u; // = bp0.g("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ bp0 v; // = bp0.g("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ bp0 w; // = bp0.f("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ bp0 x; // = bp0.f("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ bp0 y; // = bp0.f("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ bp0 z; // = bp0.g("body_temperature");
}
