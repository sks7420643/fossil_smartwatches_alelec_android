package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f2 */
public class C1765f2 {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int[] f5003c; // = {16843067, 16843068};

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.widget.ProgressBar f5004a;

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Bitmap f5005b;

    @DexIgnore
    public C1765f2(android.widget.ProgressBar progressBar) {
        this.f5004a = progressBar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10698a(android.util.AttributeSet attributeSet, int i) {
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17205a(this.f5004a.getContext(), attributeSet, f5003c, i, 0);
        android.graphics.drawable.Drawable c = a.mo18424c(0);
        if (c != null) {
            this.f5004a.setIndeterminateDrawable(mo10695a(c));
        }
        android.graphics.drawable.Drawable c2 = a.mo18424c(1);
        if (c2 != null) {
            this.f5004a.setProgressDrawable(mo10696a(c2, false));
        }
        a.mo18418a();
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Bitmap mo10699b() {
        return this.f5005b;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo10696a(android.graphics.drawable.Drawable drawable, boolean z) {
        if (drawable instanceof com.fossil.blesdk.obfuscated.C1705e7) {
            com.fossil.blesdk.obfuscated.C1705e7 e7Var = (com.fossil.blesdk.obfuscated.C1705e7) drawable;
            android.graphics.drawable.Drawable a = e7Var.mo10343a();
            if (a == null) {
                return drawable;
            }
            e7Var.mo10344a(mo10696a(a, z));
            return drawable;
        } else if (drawable instanceof android.graphics.drawable.LayerDrawable) {
            android.graphics.drawable.LayerDrawable layerDrawable = (android.graphics.drawable.LayerDrawable) drawable;
            int numberOfLayers = layerDrawable.getNumberOfLayers();
            android.graphics.drawable.Drawable[] drawableArr = new android.graphics.drawable.Drawable[numberOfLayers];
            for (int i = 0; i < numberOfLayers; i++) {
                int id = layerDrawable.getId(i);
                drawableArr[i] = mo10696a(layerDrawable.getDrawable(i), id == 16908301 || id == 16908303);
            }
            android.graphics.drawable.LayerDrawable layerDrawable2 = new android.graphics.drawable.LayerDrawable(drawableArr);
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                layerDrawable2.setId(i2, layerDrawable.getId(i2));
            }
            return layerDrawable2;
        } else if (!(drawable instanceof android.graphics.drawable.BitmapDrawable)) {
            return drawable;
        } else {
            android.graphics.drawable.BitmapDrawable bitmapDrawable = (android.graphics.drawable.BitmapDrawable) drawable;
            android.graphics.Bitmap bitmap = bitmapDrawable.getBitmap();
            if (this.f5005b == null) {
                this.f5005b = bitmap;
            }
            android.graphics.drawable.ShapeDrawable shapeDrawable = new android.graphics.drawable.ShapeDrawable(mo10697a());
            shapeDrawable.getPaint().setShader(new android.graphics.BitmapShader(bitmap, android.graphics.Shader.TileMode.REPEAT, android.graphics.Shader.TileMode.CLAMP));
            shapeDrawable.getPaint().setColorFilter(bitmapDrawable.getPaint().getColorFilter());
            return z ? new android.graphics.drawable.ClipDrawable(shapeDrawable, 3, 1) : shapeDrawable;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo10695a(android.graphics.drawable.Drawable drawable) {
        if (!(drawable instanceof android.graphics.drawable.AnimationDrawable)) {
            return drawable;
        }
        android.graphics.drawable.AnimationDrawable animationDrawable = (android.graphics.drawable.AnimationDrawable) drawable;
        int numberOfFrames = animationDrawable.getNumberOfFrames();
        android.graphics.drawable.AnimationDrawable animationDrawable2 = new android.graphics.drawable.AnimationDrawable();
        animationDrawable2.setOneShot(animationDrawable.isOneShot());
        for (int i = 0; i < numberOfFrames; i++) {
            android.graphics.drawable.Drawable a = mo10696a(animationDrawable.getFrame(i), true);
            a.setLevel(10000);
            animationDrawable2.addFrame(a, animationDrawable.getDuration(i));
        }
        animationDrawable2.setLevel(10000);
        return animationDrawable2;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.shapes.Shape mo10697a() {
        return new android.graphics.drawable.shapes.RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, (android.graphics.RectF) null, (float[]) null);
    }
}
