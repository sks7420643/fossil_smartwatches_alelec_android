package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.qt2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b03 extends zr2 implements a03, ws3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public zz2 j;
    @DexIgnore
    public qt2 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return b03.m;
        }

        @DexIgnore
        public final b03 b() {
            return new b03();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements qt2.a {
        @DexIgnore
        public /* final */ /* synthetic */ b03 a;

        @DexIgnore
        public b(b03 b03) {
            this.a = b03;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            kd4.b(appWrapper, "appWrapper");
            if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == b03.b(this.a).h()) {
                b03.b(this.a).a(appWrapper, z);
                return;
            }
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                ds3.a(childFragmentManager, title, appWrapper.getCurrentHandGroup(), b03.b(this.a).h(), appWrapper);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ b03 e;

        @DexIgnore
        public c(b03 b03) {
            this.e = b03;
        }

        @DexIgnore
        public final void onClick(View view) {
            b03.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ b03 e;
        @DexIgnore
        public /* final */ /* synthetic */ ae2 f;

        @DexIgnore
        public d(b03 b03, ae2 ae2) {
            this.e = b03;
            this.f = ae2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.s;
            kd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            b03.a(this.e).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ae2 e;

        @DexIgnore
        public e(ae2 ae2) {
            this.e = ae2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = b03.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qt2 a(b03 b03) {
        qt2 qt2 = b03.k;
        if (qt2 != null) {
            return qt2;
        }
        kd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zz2 b(b03 b03) {
        zz2 zz2 = b03.j;
        if (zz2 != null) {
            return zz2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        zz2 zz2 = this.j;
        if (zz2 != null) {
            zz2.i();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ae2 ae2 = (ae2) qa.a(layoutInflater, R.layout.fragment_notification_hybrid_app, viewGroup, false, O0());
        ae2.r.setOnClickListener(new c(this));
        ae2.q.addTextChangedListener(new d(this, ae2));
        ae2.s.setOnClickListener(new e(ae2));
        qt2 qt2 = new qt2();
        qt2.a((qt2.a) new b(this));
        this.k = qt2;
        RecyclerView recyclerView = ae2.t;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        qt2 qt22 = this.k;
        if (qt22 != null) {
            recyclerView.setAdapter(qt22);
            new tr3(this, ae2);
            kd4.a((Object) ae2, "binding");
            return ae2.d();
        }
        kd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        zz2 zz2 = this.j;
        if (zz2 != null) {
            zz2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        zz2 zz2 = this.j;
        if (zz2 != null) {
            zz2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(zz2 zz2) {
        kd4.b(zz2, "presenter");
        this.j = zz2;
    }

    @DexIgnore
    public void a(List<AppWrapper> list, int i) {
        kd4.b(list, "listAppWrapper");
        qt2 qt2 = this.k;
        if (qt2 != null) {
            qt2.a(list, i);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<String> arrayList) {
        kd4.b(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i != R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                AppWrapper appWrapper = (AppWrapper) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER");
                if (appWrapper != null) {
                    zz2 zz2 = this.j;
                    if (zz2 != null) {
                        zz2.a(appWrapper, false);
                        qt2 qt2 = this.k;
                        if (qt2 != null) {
                            qt2.notifyDataSetChanged();
                        } else {
                            kd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null) {
                AppWrapper appWrapper2 = (AppWrapper) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER");
                if (appWrapper2 != null) {
                    zz2 zz22 = this.j;
                    if (zz22 != null) {
                        zz22.a(appWrapper2, true);
                        qt2 qt22 = this.k;
                        if (qt22 != null) {
                            qt22.notifyDataSetChanged();
                        } else {
                            kd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
