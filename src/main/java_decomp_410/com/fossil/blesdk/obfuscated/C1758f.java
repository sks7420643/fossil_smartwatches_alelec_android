package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f */
public class C1758f {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.f$a */
    public interface C1759a extends com.fossil.blesdk.obfuscated.C1669e.C1673d {
        @DexIgnore
        /* renamed from: a */
        void mo64a(java.lang.String str, android.os.Bundle bundle);

        @DexIgnore
        /* renamed from: a */
        void mo65a(java.lang.String str, java.util.List<?> list, android.os.Bundle bundle);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f$b")
    /* renamed from: com.fossil.blesdk.obfuscated.f$b */
    public static class C1760b<T extends com.fossil.blesdk.obfuscated.C1758f.C1759a> extends com.fossil.blesdk.obfuscated.C1669e.C1674e<T> {
        @DexIgnore
        public C1760b(T t) {
            super(t);
        }

        @DexIgnore
        public void onChildrenLoaded(java.lang.String str, java.util.List<android.media.browse.MediaBrowser.MediaItem> list, android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            ((com.fossil.blesdk.obfuscated.C1758f.C1759a) this.f4550a).mo65a(str, list, bundle);
        }

        @DexIgnore
        public void onError(java.lang.String str, android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            ((com.fossil.blesdk.obfuscated.C1758f.C1759a) this.f4550a).mo64a(str, bundle);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m6655a(com.fossil.blesdk.obfuscated.C1758f.C1759a aVar) {
        return new com.fossil.blesdk.obfuscated.C1758f.C1760b(aVar);
    }
}
