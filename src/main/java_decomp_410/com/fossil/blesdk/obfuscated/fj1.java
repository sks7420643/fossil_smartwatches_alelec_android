package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ long g;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle h;
    @DexIgnore
    public /* final */ /* synthetic */ boolean i;
    @DexIgnore
    public /* final */ /* synthetic */ boolean j;
    @DexIgnore
    public /* final */ /* synthetic */ boolean k;
    @DexIgnore
    public /* final */ /* synthetic */ String l;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 m;

    @DexIgnore
    public fj1(dj1 dj1, String str, String str2, long j2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        this.m = dj1;
        this.e = str;
        this.f = str2;
        this.g = j2;
        this.h = bundle;
        this.i = z;
        this.j = z2;
        this.k = z3;
        this.l = str3;
    }

    @DexIgnore
    public final void run() {
        this.m.a(this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l);
    }
}
