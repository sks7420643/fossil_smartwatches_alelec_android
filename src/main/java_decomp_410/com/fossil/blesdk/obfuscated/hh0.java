package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hh0 extends zm1 implements ge0.b, ge0.c {
    @DexIgnore
    public static de0.a<? extends ln1, vm1> l; // = in1.c;
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ Handler f;
    @DexIgnore
    public /* final */ de0.a<? extends ln1, vm1> g;
    @DexIgnore
    public Set<Scope> h;
    @DexIgnore
    public kj0 i;
    @DexIgnore
    public ln1 j;
    @DexIgnore
    public kh0 k;

    @DexIgnore
    public hh0(Context context, Handler handler, kj0 kj0) {
        this(context, handler, kj0, l);
    }

    @DexIgnore
    public final void a(kh0 kh0) {
        ln1 ln1 = this.j;
        if (ln1 != null) {
            ln1.a();
        }
        this.i.a(Integer.valueOf(System.identityHashCode(this)));
        de0.a<? extends ln1, vm1> aVar = this.g;
        Context context = this.e;
        Looper looper = this.f.getLooper();
        kj0 kj0 = this.i;
        this.j = (ln1) aVar.a(context, looper, kj0, kj0.j(), this, this);
        this.k = kh0;
        Set<Scope> set = this.h;
        if (set == null || set.isEmpty()) {
            this.f.post(new ih0(this));
        } else {
            this.j.b();
        }
    }

    @DexIgnore
    public final void b(gn1 gn1) {
        ud0 H = gn1.H();
        if (H.L()) {
            dk0 I = gn1.I();
            ud0 I2 = I.I();
            if (!I2.L()) {
                String valueOf = String.valueOf(I2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                sb.append("Sign-in succeeded with resolve account failure: ");
                sb.append(valueOf);
                Log.wtf("SignInCoordinator", sb.toString(), new Exception());
                this.k.b(I2);
                this.j.a();
                return;
            }
            this.k.a(I.H(), this.h);
        } else {
            this.k.b(H);
        }
        this.j.a();
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.j.a(this);
    }

    @DexIgnore
    public final void f(int i2) {
        this.j.a();
    }

    @DexIgnore
    public final ln1 o() {
        return this.j;
    }

    @DexIgnore
    public final void p() {
        ln1 ln1 = this.j;
        if (ln1 != null) {
            ln1.a();
        }
    }

    @DexIgnore
    public hh0(Context context, Handler handler, kj0 kj0, de0.a<? extends ln1, vm1> aVar) {
        this.e = context;
        this.f = handler;
        bk0.a(kj0, (Object) "ClientSettings must not be null");
        this.i = kj0;
        this.h = kj0.i();
        this.g = aVar;
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        this.k.b(ud0);
    }

    @DexIgnore
    public final void a(gn1 gn1) {
        this.f.post(new jh0(this, gn1));
    }
}
