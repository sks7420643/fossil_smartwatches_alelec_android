package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xa extends Fragment implements DialogInterface.OnCancelListener, DialogInterface.OnDismissListener {
    @DexIgnore
    public static /* final */ String SAVED_BACK_STACK_ID; // = "android:backStackId";
    @DexIgnore
    public static /* final */ String SAVED_CANCELABLE; // = "android:cancelable";
    @DexIgnore
    public static /* final */ String SAVED_DIALOG_STATE_TAG; // = "android:savedDialogState";
    @DexIgnore
    public static /* final */ String SAVED_SHOWS_DIALOG; // = "android:showsDialog";
    @DexIgnore
    public static /* final */ String SAVED_STYLE; // = "android:style";
    @DexIgnore
    public static /* final */ String SAVED_THEME; // = "android:theme";
    @DexIgnore
    public static /* final */ int STYLE_NORMAL; // = 0;
    @DexIgnore
    public static /* final */ int STYLE_NO_FRAME; // = 2;
    @DexIgnore
    public static /* final */ int STYLE_NO_INPUT; // = 3;
    @DexIgnore
    public static /* final */ int STYLE_NO_TITLE; // = 1;
    @DexIgnore
    public int mBackStackId; // = -1;
    @DexIgnore
    public boolean mCancelable; // = true;
    @DexIgnore
    public Dialog mDialog;
    @DexIgnore
    public boolean mDismissed;
    @DexIgnore
    public boolean mShownByMe;
    @DexIgnore
    public boolean mShowsDialog; // = true;
    @DexIgnore
    public int mStyle; // = 0;
    @DexIgnore
    public int mTheme; // = 0;
    @DexIgnore
    public boolean mViewDestroyed;

    @DexIgnore
    public void dismiss() {
        dismissInternal(false);
    }

    @DexIgnore
    public void dismissAllowingStateLoss() {
        dismissInternal(true);
    }

    @DexIgnore
    public void dismissInternal(boolean z) {
        if (!this.mDismissed) {
            this.mDismissed = true;
            this.mShownByMe = false;
            Dialog dialog = this.mDialog;
            if (dialog != null) {
                dialog.dismiss();
            }
            this.mViewDestroyed = true;
            if (this.mBackStackId >= 0) {
                getFragmentManager().a(this.mBackStackId, 1);
                this.mBackStackId = -1;
                return;
            }
            bb a = getFragmentManager().a();
            a.d(this);
            if (z) {
                a.b();
            } else {
                a.a();
            }
        }
    }

    @DexIgnore
    public Dialog getDialog() {
        return this.mDialog;
    }

    @DexIgnore
    public boolean getShowsDialog() {
        return this.mShowsDialog;
    }

    @DexIgnore
    public int getTheme() {
        return this.mTheme;
    }

    @DexIgnore
    public boolean isCancelable() {
        return this.mCancelable;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.mShowsDialog) {
            View view = getView();
            if (view != null) {
                if (view.getParent() == null) {
                    this.mDialog.setContentView(view);
                } else {
                    throw new IllegalStateException("DialogFragment can not be attached to a container view");
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                this.mDialog.setOwnerActivity(activity);
            }
            this.mDialog.setCancelable(this.mCancelable);
            this.mDialog.setOnCancelListener(this);
            this.mDialog.setOnDismissListener(this);
            if (bundle != null) {
                Bundle bundle2 = bundle.getBundle(SAVED_DIALOG_STATE_TAG);
                if (bundle2 != null) {
                    this.mDialog.onRestoreInstanceState(bundle2);
                }
            }
        }
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!this.mShownByMe) {
            this.mDismissed = false;
        }
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mShowsDialog = this.mContainerId == 0;
        if (bundle != null) {
            this.mStyle = bundle.getInt(SAVED_STYLE, 0);
            this.mTheme = bundle.getInt(SAVED_THEME, 0);
            this.mCancelable = bundle.getBoolean(SAVED_CANCELABLE, true);
            this.mShowsDialog = bundle.getBoolean(SAVED_SHOWS_DIALOG, this.mShowsDialog);
            this.mBackStackId = bundle.getInt(SAVED_BACK_STACK_ID, -1);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        return new Dialog(getActivity(), getTheme());
    }

    @DexIgnore
    public void onDestroyView() {
        super.onDestroyView();
        Dialog dialog = this.mDialog;
        if (dialog != null) {
            this.mViewDestroyed = true;
            dialog.dismiss();
            this.mDialog = null;
        }
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        if (!this.mShownByMe && !this.mDismissed) {
            this.mDismissed = true;
        }
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        if (!this.mViewDestroyed) {
            dismissInternal(true);
        }
    }

    @DexIgnore
    public LayoutInflater onGetLayoutInflater(Bundle bundle) {
        if (!this.mShowsDialog) {
            return super.onGetLayoutInflater(bundle);
        }
        this.mDialog = onCreateDialog(bundle);
        Dialog dialog = this.mDialog;
        if (dialog == null) {
            return (LayoutInflater) this.mHost.c().getSystemService("layout_inflater");
        }
        setupDialog(dialog, this.mStyle);
        return (LayoutInflater) this.mDialog.getContext().getSystemService("layout_inflater");
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Dialog dialog = this.mDialog;
        if (dialog != null) {
            Bundle onSaveInstanceState = dialog.onSaveInstanceState();
            if (onSaveInstanceState != null) {
                bundle.putBundle(SAVED_DIALOG_STATE_TAG, onSaveInstanceState);
            }
        }
        int i = this.mStyle;
        if (i != 0) {
            bundle.putInt(SAVED_STYLE, i);
        }
        int i2 = this.mTheme;
        if (i2 != 0) {
            bundle.putInt(SAVED_THEME, i2);
        }
        boolean z = this.mCancelable;
        if (!z) {
            bundle.putBoolean(SAVED_CANCELABLE, z);
        }
        boolean z2 = this.mShowsDialog;
        if (!z2) {
            bundle.putBoolean(SAVED_SHOWS_DIALOG, z2);
        }
        int i3 = this.mBackStackId;
        if (i3 != -1) {
            bundle.putInt(SAVED_BACK_STACK_ID, i3);
        }
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        Dialog dialog = this.mDialog;
        if (dialog != null) {
            this.mViewDestroyed = false;
            dialog.show();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        Dialog dialog = this.mDialog;
        if (dialog != null) {
            dialog.hide();
        }
    }

    @DexIgnore
    public void setCancelable(boolean z) {
        this.mCancelable = z;
        Dialog dialog = this.mDialog;
        if (dialog != null) {
            dialog.setCancelable(z);
        }
    }

    @DexIgnore
    public void setShowsDialog(boolean z) {
        this.mShowsDialog = z;
    }

    @DexIgnore
    public void setStyle(int i, int i2) {
        this.mStyle = i;
        int i3 = this.mStyle;
        if (i3 == 2 || i3 == 3) {
            this.mTheme = 16973913;
        }
        if (i2 != 0) {
            this.mTheme = i2;
        }
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i) {
        if (!(i == 1 || i == 2)) {
            if (i == 3) {
                dialog.getWindow().addFlags(24);
            } else {
                return;
            }
        }
        dialog.requestWindowFeature(1);
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        this.mDismissed = false;
        this.mShownByMe = true;
        bb a = fragmentManager.a();
        a.a((Fragment) this, str);
        a.a();
    }

    @DexIgnore
    public void showNow(FragmentManager fragmentManager, String str) {
        this.mDismissed = false;
        this.mShownByMe = true;
        bb a = fragmentManager.a();
        a.a((Fragment) this, str);
        a.c();
    }

    @DexIgnore
    public int show(bb bbVar, String str) {
        this.mDismissed = false;
        this.mShownByMe = true;
        bbVar.a((Fragment) this, str);
        this.mViewDestroyed = false;
        this.mBackStackId = bbVar.a();
        return this.mBackStackId;
    }
}
