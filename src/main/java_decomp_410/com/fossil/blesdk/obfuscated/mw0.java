package com.fossil.blesdk.obfuscated;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mw0<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public List<tw0> f;
    @DexIgnore
    public Map<K, V> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile vw0 i;
    @DexIgnore
    public Map<K, V> j;
    @DexIgnore
    public volatile pw0 k;

    @DexIgnore
    public mw0(int i2) {
        this.e = i2;
        this.f = Collections.emptyList();
        this.g = Collections.emptyMap();
        this.j = Collections.emptyMap();
    }

    @DexIgnore
    public /* synthetic */ mw0(int i2, nw0 nw0) {
        this(i2);
    }

    @DexIgnore
    public static <FieldDescriptorType extends nu0<FieldDescriptorType>> mw0<FieldDescriptorType, Object> c(int i2) {
        return new nw0(i2);
    }

    @DexIgnore
    public final int a(K k2) {
        int size = this.f.size() - 1;
        if (size >= 0) {
            int compareTo = k2.compareTo((Comparable) this.f.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i2 = 0;
        while (i2 <= size) {
            int i3 = (i2 + size) / 2;
            int compareTo2 = k2.compareTo((Comparable) this.f.get(i3).getKey());
            if (compareTo2 < 0) {
                size = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i2 = i3 + 1;
            }
        }
        return -(i2 + 1);
    }

    @DexIgnore
    /* renamed from: a */
    public final V put(K k2, V v) {
        e();
        int a = a(k2);
        if (a >= 0) {
            return this.f.get(a).setValue(v);
        }
        e();
        if (this.f.isEmpty() && !(this.f instanceof ArrayList)) {
            this.f = new ArrayList(this.e);
        }
        int i2 = -(a + 1);
        if (i2 >= this.e) {
            return f().put(k2, v);
        }
        int size = this.f.size();
        int i3 = this.e;
        if (size == i3) {
            tw0 remove = this.f.remove(i3 - 1);
            f().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.f.add(i2, new tw0(this, k2, v));
        return null;
    }

    @DexIgnore
    public final Map.Entry<K, V> a(int i2) {
        return this.f.get(i2);
    }

    @DexIgnore
    public final boolean a() {
        return this.h;
    }

    @DexIgnore
    public final int b() {
        return this.f.size();
    }

    @DexIgnore
    public final V b(int i2) {
        e();
        V value = this.f.remove(i2).getValue();
        if (!this.g.isEmpty()) {
            Iterator it = f().entrySet().iterator();
            this.f.add(new tw0(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    @DexIgnore
    public final Iterable<Map.Entry<K, V>> c() {
        return this.g.isEmpty() ? qw0.a() : this.g.entrySet();
    }

    @DexIgnore
    public void clear() {
        e();
        if (!this.f.isEmpty()) {
            this.f.clear();
        }
        if (!this.g.isEmpty()) {
            this.g.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.g.containsKey(comparable);
    }

    @DexIgnore
    public final Set<Map.Entry<K, V>> d() {
        if (this.k == null) {
            this.k = new pw0(this, (nw0) null);
        }
        return this.k;
    }

    @DexIgnore
    public final void e() {
        if (this.h) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.i == null) {
            this.i = new vw0(this, (nw0) null);
        }
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof mw0)) {
            return super.equals(obj);
        }
        mw0 mw0 = (mw0) obj;
        int size = size();
        if (size != mw0.size()) {
            return false;
        }
        int b = b();
        if (b != mw0.b()) {
            return entrySet().equals(mw0.entrySet());
        }
        for (int i2 = 0; i2 < b; i2++) {
            if (!a(i2).equals(mw0.a(i2))) {
                return false;
            }
        }
        if (b != size) {
            return this.g.equals(mw0.g);
        }
        return true;
    }

    @DexIgnore
    public final SortedMap<K, V> f() {
        e();
        if (this.g.isEmpty() && !(this.g instanceof TreeMap)) {
            this.g = new TreeMap();
            this.j = ((TreeMap) this.g).descendingMap();
        }
        return (SortedMap) this.g;
    }

    @DexIgnore
    public void g() {
        if (!this.h) {
            this.g = this.g.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.g);
            this.j = this.j.isEmpty() ? Collections.emptyMap() : Collections.unmodifiableMap(this.j);
            this.h = true;
        }
    }

    @DexIgnore
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        return a >= 0 ? this.f.get(a).getValue() : this.g.get(comparable);
    }

    @DexIgnore
    public int hashCode() {
        int b = b();
        int i2 = 0;
        for (int i3 = 0; i3 < b; i3++) {
            i2 += this.f.get(i3).hashCode();
        }
        return this.g.size() > 0 ? i2 + this.g.hashCode() : i2;
    }

    @DexIgnore
    public V remove(Object obj) {
        e();
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return b(a);
        }
        if (this.g.isEmpty()) {
            return null;
        }
        return this.g.remove(comparable);
    }

    @DexIgnore
    public int size() {
        return this.f.size() + this.g.size();
    }
}
