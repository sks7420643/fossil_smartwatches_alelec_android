package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ac1 {
    @DexIgnore
    public volatile int a; // = -1;

    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    public abstract ac1 a(tb1 tb1) throws IOException;

    @DexIgnore
    public void a(ub1 ub1) throws IOException {
    }

    @DexIgnore
    public final int b() {
        int a2 = a();
        this.a = a2;
        return a2;
    }

    @DexIgnore
    /* renamed from: c */
    public ac1 clone() throws CloneNotSupportedException {
        return (ac1) super.clone();
    }

    @DexIgnore
    public final int d() {
        if (this.a < 0) {
            b();
        }
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return bc1.a(this);
    }
}
