package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l80 extends e80 {
    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ l80(short s, RequestId requestId, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, requestId, peripheral, (i2 & 8) != 0 ? 3 : i);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l80(short s, RequestId requestId, Peripheral peripheral, int i) {
        super(FileControlOperationCode.VERIFY_FILE, s, requestId, peripheral, i);
        kd4.b(requestId, "requestId");
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public l80(short s, Peripheral peripheral) {
        this(s, RequestId.VERIFY_FILE, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
    }
}
