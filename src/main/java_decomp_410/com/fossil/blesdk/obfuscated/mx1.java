package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class mx1 implements Runnable {
    @DexIgnore
    public /* final */ ix1 e;
    @DexIgnore
    public /* final */ px1 f;

    @DexIgnore
    public mx1(ix1 ix1, px1 px1) {
        this.e = ix1;
        this.f = px1;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f.a);
    }
}
