package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lj2 extends cj2 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public lj2(String str, int i) {
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }
}
