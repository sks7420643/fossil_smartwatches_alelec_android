package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gw0 implements qv0 {
    @DexIgnore
    public /* final */ sv0 a;
    @DexIgnore
    public /* final */ hw0 b;

    @DexIgnore
    public gw0(sv0 sv0, String str, Object[] objArr) {
        this.a = sv0;
        this.b = new hw0(sv0.getClass(), str, objArr);
    }

    @DexIgnore
    public final int a() {
        return (this.b.d & 1) == 1 ? ru0.e.i : ru0.e.j;
    }

    @DexIgnore
    public final boolean b() {
        return (this.b.d & 2) == 2;
    }

    @DexIgnore
    public final sv0 c() {
        return this.a;
    }

    @DexIgnore
    public final int d() {
        return this.b.e;
    }

    @DexIgnore
    public final hw0 e() {
        return this.b;
    }

    @DexIgnore
    public final int f() {
        return this.b.h;
    }

    @DexIgnore
    public final int g() {
        return this.b.i;
    }

    @DexIgnore
    public final int h() {
        return this.b.j;
    }

    @DexIgnore
    public final int i() {
        return this.b.m;
    }

    @DexIgnore
    public final int[] j() {
        return this.b.n;
    }

    @DexIgnore
    public final int k() {
        return this.b.l;
    }

    @DexIgnore
    public final int l() {
        return this.b.k;
    }
}
