package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b80 extends p70 {
    @DexIgnore
    public /* final */ byte[] L; // = {DateTimeFieldType.CLOCKHOUR_OF_DAY, 1};

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b80(Peripheral peripheral) {
        super(DeviceConfigOperationCode.STOP_CURRENT_WORKOUT_SESSION, RequestId.STOP_CURRENT_WORKOUT_SESSION, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        c(true);
    }

    @DexIgnore
    public byte[] C() {
        return this.L;
    }
}
