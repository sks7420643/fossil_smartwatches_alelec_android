package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import com.fossil.blesdk.obfuscated.gr3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.data.model.ua.UADataSource;
import com.portfolio.platform.data.model.ua.UALink;
import com.portfolio.platform.data.model.ua.UALinks;
import com.portfolio.platform.underamour.UASharePref;
import com.portfolio.platform.underamour.UAValues;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dr3 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public cr3 a;
    @DexIgnore
    public er3 b;
    @DexIgnore
    public String c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements e94<xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ dr3 a;
        @DexIgnore
        public /* final */ /* synthetic */ UAActivityTimeSeries b;
        @DexIgnore
        public /* final */ /* synthetic */ gr3.d c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements e94<qr4<xz1>> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(List list, b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void a(qr4<xz1> qr4) {
                if (qr4 != null) {
                    FLogger.INSTANCE.getLocal().d(dr3.d + "_postActivityTimeSeries", "onResponse: " + String.valueOf(qr4.a()));
                    gr3.d dVar = this.a.c;
                    if (dVar != null) {
                        dVar.onSuccess();
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr3$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.dr3$b$b  reason: collision with other inner class name */
        public static final class C0084b<T> implements e94<Throwable> {
            @DexIgnore
            public static /* final */ C0084b a; // = new C0084b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(dr3.d + "_postActivityTimeSeries", "onFailure: " + th.getMessage());
                }
            }
        }

        @DexIgnore
        public b(dr3 dr3, UAActivityTimeSeries uAActivityTimeSeries, gr3.d dVar) {
            this.a = dr3;
            this.b = uAActivityTimeSeries;
            this.c = dVar;
        }

        @DexIgnore
        public final void a(xz1 xz1) {
            if (xz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) xz1, UAAccessToken.class);
                List<UALink> list = null;
                if (!qf4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                UADataSource c2 = UASharePref.c.a().c();
                if (c2 != null) {
                    UALinks link = c2.getLink();
                    if (link != null) {
                        list = link.getSelf();
                    }
                }
                if (list != null && (!list.isEmpty())) {
                    this.b.setExtraJsonObject((xz1) new Gson().a(new Gson().a((Object) list.get(0), (Type) UALink.class), xz1.class));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(dr3.d + "_postActivityTimeSeries", this.b.getJsonData().toString());
                    cr3 b2 = this.a.a;
                    xz1 jsonData = this.b.getJsonData();
                    String a2 = this.a.c;
                    pd4 pd4 = pd4.a;
                    String value = UAValues.Authorization.BEARER.getValue();
                    Object[] objArr = {UASharePref.c.a().a()};
                    String format = String.format(value, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                    b2.a(jsonData, a2, format).a(3).a(w84.a()).a(new a(list, this), (e94<? super Throwable>) C0084b.a);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements e94<Throwable> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(dr3.d + "_postActivityTimeSeries", th.getMessage());
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = dr3.class.getSimpleName();
        kd4.a((Object) simpleName, "UAActivityManager::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public dr3(cr3 cr3, er3 er3, String str) {
        kd4.b(cr3, "uaApi");
        kd4.b(er3, "uaAuthorizationManager");
        kd4.b(str, "clientId");
        this.a = cr3;
        this.b = er3;
        this.c = str;
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void a(UAActivityTimeSeries uAActivityTimeSeries, gr3.d dVar) {
        kd4.b(uAActivityTimeSeries, "uaActivityTimeSeries");
        this.b.a().a(new b(this, uAActivityTimeSeries, dVar), (e94<? super Throwable>) c.a);
    }
}
