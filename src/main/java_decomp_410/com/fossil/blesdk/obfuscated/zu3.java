package com.fossil.blesdk.obfuscated;

import java.io.UnsupportedEncodingException;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zu3 {
    @DexIgnore
    public static String a(String str, String str2) {
        try {
            String base64 = ByteString.of((str + ":" + str2).getBytes("ISO-8859-1")).base64();
            return "Basic " + base64;
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError();
        }
    }
}
