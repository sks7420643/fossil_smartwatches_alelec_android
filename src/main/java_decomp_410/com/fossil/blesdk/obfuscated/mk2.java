package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.VideoUploader;
import com.portfolio.platform.enums.Gender;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mk2 {
    @DexIgnore
    public static /* final */ mk2 a; // = new mk2();

    @DexIgnore
    public final int a() {
        return 30;
    }

    @DexIgnore
    public final int a(int i) {
        if (i < 18) {
            return 540;
        }
        return (18 <= i && 65 >= i) ? 480 : 450;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r9 < 35) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r0 = 70;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        if (r9 < 35) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002b, code lost:
        if (r9 < 35) goto L_0x002d;
     */
    @DexIgnore
    public final Pair<Integer, Integer> a(Gender gender, int i) {
        int i2;
        int i3;
        kd4.b(gender, "gender");
        int i4 = lk2.a[gender.ordinal()];
        if (i4 != 1) {
            if (i4 == 2) {
                i2 = 175;
                i3 = 80;
                if (i >= 18) {
                }
            } else if (i4 == 3) {
                i2 = 170;
                i3 = 75;
                if (i >= 18) {
                }
            } else {
                throw new NoWhenBranchMatchedException();
            }
            i3 = 65;
            return new Pair<>(Integer.valueOf(i2), Integer.valueOf(i3));
        }
        i2 = 165;
        if (i < 18) {
            i3 = 55;
            return new Pair<>(Integer.valueOf(i2), Integer.valueOf(i3));
        }
        i3 = 60;
        return new Pair<>(Integer.valueOf(i2), Integer.valueOf(i3));
    }

    @DexIgnore
    public final int b() {
        return VideoUploader.RETRY_DELAY_UNIT_MS;
    }

    @DexIgnore
    public final int a(int i, int i2, int i3, Gender gender) {
        double d;
        kd4.b(gender, "gender");
        double d2 = i < 18 ? 0.0d : (18 <= i && 30 >= i) ? 21.5650154d : (30 <= i && 50 >= i) ? 25.291792d : 33.3115727d;
        int i4 = lk2.b[gender.ordinal()];
        if (i4 == 1) {
            d = -171.5010333d;
        } else if (i4 == 2) {
            d = -159.3516885d;
        } else if (i4 == 3) {
            d = -64.4165128d;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        return td4.a(d2 + 147.7007883d + (((double) i2) * 2.3639882d) + (((double) i3) * 0.2297702d) + d);
    }
}
