package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ck0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ck0> CREATOR; // = new kl0();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ Account f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ GoogleSignInAccount h;

    @DexIgnore
    public ck0(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.e = i;
        this.f = account;
        this.g = i2;
        this.h = googleSignInAccount;
    }

    @DexIgnore
    public Account H() {
        return this.f;
    }

    @DexIgnore
    public int I() {
        return this.g;
    }

    @DexIgnore
    public GoogleSignInAccount J() {
        return this.h;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, (Parcelable) H(), i, false);
        kk0.a(parcel, 3, I());
        kk0.a(parcel, 4, (Parcelable) J(), i, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public ck0(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
