package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vf */
public class C3111vf<T> extends androidx.lifecycle.LiveData<T> {

    @DexIgnore
    /* renamed from: k */
    public /* final */ androidx.room.RoomDatabase f10270k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ boolean f10271l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.util.concurrent.Callable<T> f10272m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ com.fossil.blesdk.obfuscated.C2566of f10273n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ com.fossil.blesdk.obfuscated.C2647pf.C2650c f10274o;

    @DexIgnore
    /* renamed from: p */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f10275p; // = new java.util.concurrent.atomic.AtomicBoolean(true);

    @DexIgnore
    /* renamed from: q */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f10276q; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: r */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f10277r; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: s */
    public /* final */ java.lang.Runnable f10278s; // = new com.fossil.blesdk.obfuscated.C3111vf.C3112a();

    @DexIgnore
    /* renamed from: t */
    public /* final */ java.lang.Runnable f10279t; // = new com.fossil.blesdk.obfuscated.C3111vf.C3113b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vf$a */
    public class C3112a implements java.lang.Runnable {
        @DexIgnore
        public C3112a() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (com.fossil.blesdk.obfuscated.C3111vf.this.f10277r.compareAndSet(false, true)) {
                com.fossil.blesdk.obfuscated.C3111vf.this.f10270k.getInvalidationTracker().mo14687b(com.fossil.blesdk.obfuscated.C3111vf.this.f10274o);
            }
            do {
                if (com.fossil.blesdk.obfuscated.C3111vf.this.f10276q.compareAndSet(false, true)) {
                    java.lang.Object obj = null;
                    z = false;
                    while (com.fossil.blesdk.obfuscated.C3111vf.this.f10275p.compareAndSet(true, false)) {
                        try {
                            obj = com.fossil.blesdk.obfuscated.C3111vf.this.f10272m.call();
                            z = true;
                        } catch (java.lang.Exception e) {
                            throw new java.lang.RuntimeException("Exception while computing database live data.", e);
                        } catch (Throwable th) {
                            com.fossil.blesdk.obfuscated.C3111vf.this.f10276q.set(false);
                            throw th;
                        }
                    }
                    if (z) {
                        com.fossil.blesdk.obfuscated.C3111vf.this.mo2280a(obj);
                    }
                    com.fossil.blesdk.obfuscated.C3111vf.this.f10276q.set(false);
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (com.fossil.blesdk.obfuscated.C3111vf.this.f10275p.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vf$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vf$b */
    public class C3113b implements java.lang.Runnable {
        @DexIgnore
        public C3113b() {
        }

        @DexIgnore
        public void run() {
            boolean c = com.fossil.blesdk.obfuscated.C3111vf.this.mo2285c();
            if (com.fossil.blesdk.obfuscated.C3111vf.this.f10275p.compareAndSet(false, true) && c) {
                com.fossil.blesdk.obfuscated.C3111vf.this.mo17092f().execute(com.fossil.blesdk.obfuscated.C3111vf.this.f10278s);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vf$c")
    /* renamed from: com.fossil.blesdk.obfuscated.vf$c */
    public class C3114c extends com.fossil.blesdk.obfuscated.C2647pf.C2650c {
        @DexIgnore
        public C3114c(java.lang.String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(java.util.Set<java.lang.String> set) {
            com.fossil.blesdk.obfuscated.C1919h3.m7768c().mo12211b(com.fossil.blesdk.obfuscated.C3111vf.this.f10279t);
        }
    }

    @DexIgnore
    @android.annotation.SuppressLint({"RestrictedApi"})
    public C3111vf(androidx.room.RoomDatabase roomDatabase, com.fossil.blesdk.obfuscated.C2566of ofVar, boolean z, java.util.concurrent.Callable<T> callable, java.lang.String[] strArr) {
        this.f10270k = roomDatabase;
        this.f10271l = z;
        this.f10272m = callable;
        this.f10273n = ofVar;
        this.f10274o = new com.fossil.blesdk.obfuscated.C3111vf.C3114c(strArr);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo2286d() {
        super.mo2286d();
        this.f10273n.mo14412a(this);
        mo17092f().execute(this.f10278s);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo2287e() {
        super.mo2287e();
        this.f10273n.mo14413b(this);
    }

    @DexIgnore
    /* renamed from: f */
    public java.util.concurrent.Executor mo17092f() {
        if (this.f10271l) {
            return this.f10270k.getTransactionExecutor();
        }
        return this.f10270k.getQueryExecutor();
    }
}
