package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mg2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ AppCompatAutoCompleteTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ RecyclerView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;

    @DexIgnore
    public mg2(Object obj, View view, int i, AppCompatAutoCompleteTextView appCompatAutoCompleteTextView, RTLImageView rTLImageView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, RecyclerView recyclerView, FlexibleTextView flexibleTextView3) {
        super(obj, view, i);
        this.q = appCompatAutoCompleteTextView;
        this.r = rTLImageView;
        this.s = imageView;
        this.t = flexibleTextView;
        this.u = view2;
        this.v = recyclerView;
        this.w = flexibleTextView3;
    }
}
