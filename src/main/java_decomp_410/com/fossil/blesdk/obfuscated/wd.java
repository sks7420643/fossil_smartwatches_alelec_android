package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pd;
import com.fossil.blesdk.obfuscated.qd;
import com.fossil.blesdk.obfuscated.sd;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wd<T> extends qd<T> implements sd.a {
    @DexIgnore
    public /* final */ ud<T> s;
    @DexIgnore
    public pd.a<T> t; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pd.a<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, pd<T> pdVar) {
            if (pdVar.a()) {
                wd.this.c();
            } else if (!wd.this.h()) {
                if (i == 0 || i == 3) {
                    List<T> list = pdVar.a;
                    if (wd.this.i.i() == 0) {
                        wd wdVar = wd.this;
                        wdVar.i.a(pdVar.b, list, pdVar.c, pdVar.d, wdVar.h.a, wdVar);
                    } else {
                        wd wdVar2 = wd.this;
                        wdVar2.i.b(pdVar.d, list, wdVar2.j, wdVar2.h.d, wdVar2.l, wdVar2);
                    }
                    wd wdVar3 = wd.this;
                    if (wdVar3.g != null) {
                        boolean z = true;
                        boolean z2 = wdVar3.i.size() == 0;
                        boolean z3 = !z2 && pdVar.b == 0 && pdVar.d == 0;
                        int size = wd.this.size();
                        if (z2 || (!(i == 0 && pdVar.c == 0) && (i != 3 || pdVar.d + wd.this.h.a < size))) {
                            z = false;
                        }
                        wd.this.a(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore
        public b(int i) {
            this.e = i;
        }

        @DexIgnore
        public void run() {
            if (!wd.this.h()) {
                wd wdVar = wd.this;
                int i = wdVar.h.a;
                if (wdVar.s.isInvalid()) {
                    wd.this.c();
                    return;
                }
                int i2 = this.e * i;
                int min = Math.min(i, wd.this.i.size() - i2);
                wd wdVar2 = wd.this;
                wdVar2.s.dispatchLoadRange(3, i2, min, wdVar2.e, wdVar2.t);
            }
        }
    }

    @DexIgnore
    public wd(ud<T> udVar, Executor executor, Executor executor2, qd.c<T> cVar, qd.f fVar, int i) {
        super(new sd(), executor, executor2, cVar, fVar);
        this.s = udVar;
        int i2 = this.h.a;
        this.j = i;
        if (this.s.isInvalid()) {
            c();
            return;
        }
        int max = Math.max(this.h.e / i2, 2) * i2;
        this.s.dispatchLoadInitial(true, Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.e, this.t);
    }

    @DexIgnore
    public void a(qd<T> qdVar, qd.e eVar) {
        sd<T> sdVar = qdVar.i;
        if (sdVar.isEmpty() || this.i.size() != sdVar.size()) {
            throw new IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i = this.h.a;
        int e = this.i.e() / i;
        int i2 = this.i.i();
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + e;
            int i5 = 0;
            while (i5 < this.i.i()) {
                int i6 = i4 + i5;
                if (!this.i.b(i, i6) || sdVar.b(i, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                eVar.a(i4 * i, i * i5);
                i3 += i5 - 1;
            }
            i3++;
        }
    }

    @DexIgnore
    public void b(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void c(int i, int i2) {
        d(i, i2);
    }

    @DexIgnore
    public ld<?, T> d() {
        return this.s;
    }

    @DexIgnore
    public Object e() {
        return Integer.valueOf(this.j);
    }

    @DexIgnore
    public void f(int i) {
        this.f.execute(new b(i));
    }

    @DexIgnore
    public boolean g() {
        return false;
    }

    @DexIgnore
    public void h(int i) {
        sd<T> sdVar = this.i;
        qd.f fVar = this.h;
        sdVar.a(i, fVar.b, fVar.a, (sd.a) this);
    }

    @DexIgnore
    public void b() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void b(int i, int i2) {
        f(i, i2);
    }

    @DexIgnore
    public void a(int i) {
        e(0, i);
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void a() {
        throw new IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    public void a(int i, int i2) {
        d(i, i2);
    }
}
