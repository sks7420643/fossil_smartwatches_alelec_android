package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kw */
public final class C2255kw {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.concurrent.atomic.AtomicReference<byte[]> f7023a; // = new java.util.concurrent.atomic.AtomicReference<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kw$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kw$a */
    public static class C2256a extends java.io.InputStream {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.nio.ByteBuffer f7024e;

        @DexIgnore
        /* renamed from: f */
        public int f7025f; // = -1;

        @DexIgnore
        public C2256a(java.nio.ByteBuffer byteBuffer) {
            this.f7024e = byteBuffer;
        }

        @DexIgnore
        public int available() {
            return this.f7024e.remaining();
        }

        @DexIgnore
        public synchronized void mark(int i) {
            this.f7025f = this.f7024e.position();
        }

        @DexIgnore
        public boolean markSupported() {
            return true;
        }

        @DexIgnore
        public int read() {
            if (!this.f7024e.hasRemaining()) {
                return -1;
            }
            return this.f7024e.get() & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
        }

        @DexIgnore
        public synchronized void reset() throws java.io.IOException {
            if (this.f7025f != -1) {
                this.f7024e.position(this.f7025f);
            } else {
                throw new java.io.IOException("Cannot reset to unset mark position");
            }
        }

        @DexIgnore
        public long skip(long j) throws java.io.IOException {
            if (!this.f7024e.hasRemaining()) {
                return -1;
            }
            long min = java.lang.Math.min(j, (long) available());
            java.nio.ByteBuffer byteBuffer = this.f7024e;
            byteBuffer.position((int) (((long) byteBuffer.position()) + min));
            return min;
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws java.io.IOException {
            if (!this.f7024e.hasRemaining()) {
                return -1;
            }
            int min = java.lang.Math.min(i2, available());
            this.f7024e.get(bArr, i, min);
            return min;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kw$b")
    /* renamed from: com.fossil.blesdk.obfuscated.kw$b */
    public static final class C2257b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f7026a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f7027b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ byte[] f7028c;

        @DexIgnore
        public C2257b(byte[] bArr, int i, int i2) {
            this.f7028c = bArr;
            this.f7026a = i;
            this.f7027b = i2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:9|10|(2:12|13)|14|15|16) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x002f */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004e A[SYNTHETIC, Splitter:B:29:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0055 A[SYNTHETIC, Splitter:B:33:0x0055] */
    /* renamed from: a */
    public static java.nio.ByteBuffer m9870a(java.io.File file) throws java.io.IOException {
        java.io.RandomAccessFile randomAccessFile;
        java.nio.channels.FileChannel fileChannel = null;
        try {
            long length = file.length();
            if (length > 2147483647L) {
                throw new java.io.IOException("File too large to map into memory");
            } else if (length != 0) {
                randomAccessFile = new java.io.RandomAccessFile(file, "r");
                try {
                    java.nio.channels.FileChannel channel = randomAccessFile.getChannel();
                    try {
                        java.nio.MappedByteBuffer load = channel.map(java.nio.channels.FileChannel.MapMode.READ_ONLY, 0, length).load();
                        if (channel != null) {
                            channel.close();
                        }
                        randomAccessFile.close();
                        return load;
                    } catch (Throwable th) {
                        java.lang.Throwable th2 = th;
                        fileChannel = channel;
                        th = th2;
                        if (fileChannel != null) {
                            try {
                                fileChannel.close();
                            } catch (java.io.IOException unused) {
                            }
                        }
                        if (randomAccessFile != null) {
                            try {
                                randomAccessFile.close();
                            } catch (java.io.IOException unused2) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    if (fileChannel != null) {
                    }
                    if (randomAccessFile != null) {
                    }
                    throw th;
                }
            } else {
                throw new java.io.IOException("File unsuitable for memory mapping");
            }
        } catch (Throwable th4) {
            th = th4;
            randomAccessFile = null;
            if (fileChannel != null) {
            }
            if (randomAccessFile != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static byte[] m9873b(java.nio.ByteBuffer byteBuffer) {
        com.fossil.blesdk.obfuscated.C2255kw.C2257b a = m9869a(byteBuffer);
        if (a != null && a.f7026a == 0 && a.f7027b == a.f7028c.length) {
            return byteBuffer.array();
        }
        java.nio.ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        byte[] bArr = new byte[asReadOnlyBuffer.limit()];
        asReadOnlyBuffer.position(0);
        asReadOnlyBuffer.get(bArr);
        return bArr;
    }

    @DexIgnore
    /* renamed from: c */
    public static java.io.InputStream m9874c(java.nio.ByteBuffer byteBuffer) {
        return new com.fossil.blesdk.obfuscated.C2255kw.C2256a(byteBuffer);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:0|1|2|3|4|(2:6|7)|8|9|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0021 */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x002b A[SYNTHETIC, Splitter:B:15:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0032 A[SYNTHETIC, Splitter:B:19:0x0032] */
    /* renamed from: a */
    public static void m9872a(java.nio.ByteBuffer byteBuffer, java.io.File file) throws java.io.IOException {
        java.io.RandomAccessFile randomAccessFile;
        byteBuffer.position(0);
        java.nio.channels.FileChannel fileChannel = null;
        try {
            randomAccessFile = new java.io.RandomAccessFile(file, "rw");
            try {
                fileChannel = randomAccessFile.getChannel();
                fileChannel.write(byteBuffer);
                fileChannel.force(false);
                fileChannel.close();
                randomAccessFile.close();
                if (fileChannel != null) {
                    fileChannel.close();
                }
                randomAccessFile.close();
            } catch (Throwable th) {
                th = th;
                if (fileChannel != null) {
                    try {
                        fileChannel.close();
                    } catch (java.io.IOException unused) {
                    }
                }
                if (randomAccessFile != null) {
                    try {
                        randomAccessFile.close();
                    } catch (java.io.IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            randomAccessFile = null;
            if (fileChannel != null) {
            }
            if (randomAccessFile != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.nio.ByteBuffer m9871a(java.io.InputStream inputStream) throws java.io.IOException {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream(androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        byte[] andSet = f7023a.getAndSet((java.lang.Object) null);
        if (andSet == null) {
            andSet = new byte[androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE];
        }
        while (true) {
            int read = inputStream.read(andSet);
            if (read >= 0) {
                byteArrayOutputStream.write(andSet, 0, read);
            } else {
                f7023a.set(andSet);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return (java.nio.ByteBuffer) java.nio.ByteBuffer.allocateDirect(byteArray.length).put(byteArray).position(0);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2255kw.C2257b m9869a(java.nio.ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly() || !byteBuffer.hasArray()) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2255kw.C2257b(byteBuffer.array(), byteBuffer.arrayOffset(), byteBuffer.limit());
    }
}
