package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class to4 implements ko4 {
    @DexIgnore
    public /* final */ jo4 e; // = new jo4();
    @DexIgnore
    public /* final */ xo4 f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public to4(xo4 xo4) {
        if (xo4 != null) {
            this.f = xo4;
            return;
        }
        throw new NullPointerException("sink == null");
    }

    @DexIgnore
    public jo4 a() {
        return this.e;
    }

    @DexIgnore
    public ko4 b(long j) throws IOException {
        if (!this.g) {
            this.e.b(j);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 c() throws IOException {
        if (!this.g) {
            long B = this.e.B();
            if (B > 0) {
                this.f.a(this.e, B);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.g) {
            try {
                if (this.e.f > 0) {
                    this.f.a(this.e, this.e.f);
                }
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.f.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            this.g = true;
            if (th != null) {
                ap4.a(th);
                throw null;
            }
        }
    }

    @DexIgnore
    public ko4 d() throws IOException {
        if (!this.g) {
            long x = this.e.x();
            if (x > 0) {
                this.f.a(this.e, x);
            }
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public OutputStream e() {
        return new a();
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.g) {
            jo4 jo4 = this.e;
            long j = jo4.f;
            if (j > 0) {
                this.f.a(jo4, j);
            }
            this.f.flush();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public boolean isOpen() {
        return !this.g;
    }

    @DexIgnore
    public String toString() {
        return "buffer(" + this.f + ")";
    }

    @DexIgnore
    public ko4 write(byte[] bArr) throws IOException {
        if (!this.g) {
            this.e.write(bArr);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 writeByte(int i) throws IOException {
        if (!this.g) {
            this.e.writeByte(i);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 writeInt(int i) throws IOException {
        if (!this.g) {
            this.e.writeInt(i);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 writeShort(int i) throws IOException {
        if (!this.g) {
            this.e.writeShort(i);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public void a(jo4 jo4, long j) throws IOException {
        if (!this.g) {
            this.e.a(jo4, j);
            d();
            return;
        }
        throw new IllegalStateException("closed");
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OutputStream {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void close() throws IOException {
            to4.this.close();
        }

        @DexIgnore
        public void flush() throws IOException {
            to4 to4 = to4.this;
            if (!to4.g) {
                to4.flush();
            }
        }

        @DexIgnore
        public String toString() {
            return to4.this + ".outputStream()";
        }

        @DexIgnore
        public void write(int i) throws IOException {
            to4 to4 = to4.this;
            if (!to4.g) {
                to4.e.writeByte((int) (byte) i);
                to4.this.d();
                return;
            }
            throw new IOException("closed");
        }

        @DexIgnore
        public void write(byte[] bArr, int i, int i2) throws IOException {
            to4 to4 = to4.this;
            if (!to4.g) {
                to4.e.write(bArr, i, i2);
                to4.this.d();
                return;
            }
            throw new IOException("closed");
        }
    }

    @DexIgnore
    public zo4 b() {
        return this.f.b();
    }

    @DexIgnore
    public ko4 write(byte[] bArr, int i, int i2) throws IOException {
        if (!this.g) {
            this.e.write(bArr, i, i2);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 a(ByteString byteString) throws IOException {
        if (!this.g) {
            this.e.a(byteString);
            d();
            return this;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public int write(ByteBuffer byteBuffer) throws IOException {
        if (!this.g) {
            int write = this.e.write(byteBuffer);
            d();
            return write;
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public ko4 a(String str) throws IOException {
        if (!this.g) {
            this.e.a(str);
            return d();
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public long a(yo4 yo4) throws IOException {
        if (yo4 != null) {
            long j = 0;
            while (true) {
                long b = yo4.b(this.e, 8192);
                if (b == -1) {
                    return j;
                }
                j += b;
                d();
            }
        } else {
            throw new IllegalArgumentException("source == null");
        }
    }

    @DexIgnore
    public ko4 a(long j) throws IOException {
        if (!this.g) {
            this.e.a(j);
            return d();
        }
        throw new IllegalStateException("closed");
    }
}
