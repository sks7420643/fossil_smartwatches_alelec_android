package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cl4 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public cl4(Object obj) {
        kd4.b(obj, "locked");
        this.a = obj;
    }

    @DexIgnore
    public String toString() {
        return "Empty[" + this.a + ']';
    }
}
