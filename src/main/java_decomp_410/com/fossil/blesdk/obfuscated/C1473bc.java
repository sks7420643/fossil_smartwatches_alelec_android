package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bc */
public class C1473bc {

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.Integer> f3703a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: a */
    public boolean mo9140a(java.lang.String str, int i) {
        java.lang.Integer num = this.f3703a.get(str);
        boolean z = false;
        int intValue = num != null ? num.intValue() : 0;
        if ((intValue & i) != 0) {
            z = true;
        }
        this.f3703a.put(str, java.lang.Integer.valueOf(i | intValue));
        return !z;
    }
}
