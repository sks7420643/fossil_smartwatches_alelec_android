package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ew0 {
    @DexIgnore
    public static /* final */ ew0 c; // = new ew0();
    @DexIgnore
    public /* final */ kw0 a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, jw0<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public ew0() {
        String[] strArr = {"com.google.protobuf.AndroidProto3SchemaFactory"};
        kw0 kw0 = null;
        for (int i = 0; i <= 0; i++) {
            kw0 = a(strArr[0]);
            if (kw0 != null) {
                break;
            }
        }
        this.a = kw0 == null ? new jv0() : kw0;
    }

    @DexIgnore
    public static ew0 a() {
        return c;
    }

    @DexIgnore
    public static kw0 a(String str) {
        try {
            return (kw0) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public final <T> jw0<T> a(Class<T> cls) {
        tu0.a(cls, "messageType");
        jw0<T> jw0 = (jw0) this.b.get(cls);
        if (jw0 != null) {
            return jw0;
        }
        jw0<T> a2 = this.a.a(cls);
        tu0.a(cls, "messageType");
        tu0.a(a2, "schema");
        jw0<T> putIfAbsent = this.b.putIfAbsent(cls, a2);
        return putIfAbsent != null ? putIfAbsent : a2;
    }

    @DexIgnore
    public final <T> jw0<T> a(T t) {
        return a(t.getClass());
    }
}
