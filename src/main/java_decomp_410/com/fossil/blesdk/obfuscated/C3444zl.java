package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zl */
public interface C3444zl {
    @DexIgnore
    /* renamed from: a */
    java.util.concurrent.Executor mo8787a();

    @DexIgnore
    /* renamed from: a */
    void mo8788a(java.lang.Runnable runnable);

    @DexIgnore
    /* renamed from: b */
    com.fossil.blesdk.obfuscated.C2973tl mo8789b();
}
