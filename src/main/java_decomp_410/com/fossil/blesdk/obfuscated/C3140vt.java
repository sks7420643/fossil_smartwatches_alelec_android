package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vt */
public class C3140vt implements com.fossil.blesdk.obfuscated.C2498no<com.fossil.blesdk.obfuscated.C3060ut> {
    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.EncodeStrategy mo13706a(com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return com.bumptech.glide.load.EncodeStrategy.SOURCE;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11698a(com.fossil.blesdk.obfuscated.C1438aq<com.fossil.blesdk.obfuscated.C3060ut> aqVar, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        try {
            com.fossil.blesdk.obfuscated.C2255kw.m9872a(aqVar.get().mo16859c(), file);
            return true;
        } catch (java.io.IOException e) {
            if (android.util.Log.isLoggable("GifEncoder", 5)) {
                android.util.Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
