package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.zj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bw1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public bw1(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        bk0.b(!rm0.a(str), "ApplicationId must be set.");
        this.b = str;
        this.a = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
        this.g = str7;
    }

    @DexIgnore
    public static bw1 a(Context context) {
        hk0 hk0 = new hk0(context);
        String a2 = hk0.a("google_app_id");
        if (TextUtils.isEmpty(a2)) {
            return null;
        }
        return new bw1(a2, hk0.a("google_api_key"), hk0.a("firebase_database_url"), hk0.a("ga_trackingId"), hk0.a("gcm_defaultSenderId"), hk0.a("google_storage_bucket"), hk0.a("project_id"));
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof bw1)) {
            return false;
        }
        bw1 bw1 = (bw1) obj;
        if (!zj0.a(this.b, bw1.b) || !zj0.a(this.a, bw1.a) || !zj0.a(this.c, bw1.c) || !zj0.a(this.d, bw1.d) || !zj0.a(this.e, bw1.e) || !zj0.a(this.f, bw1.f) || !zj0.a(this.g, bw1.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.b, this.a, this.c, this.d, this.e, this.f, this.g);
    }

    @DexIgnore
    public final String toString() {
        zj0.a a2 = zj0.a((Object) this);
        a2.a("applicationId", this.b);
        a2.a("apiKey", this.a);
        a2.a("databaseUrl", this.c);
        a2.a("gcmSenderId", this.e);
        a2.a("storageBucket", this.f);
        a2.a("projectId", this.g);
        return a2.toString();
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
