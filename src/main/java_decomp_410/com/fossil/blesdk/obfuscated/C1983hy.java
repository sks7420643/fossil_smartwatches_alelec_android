package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hy */
public class C1983hy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f5890a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, java.lang.Object> f5891b; // = new java.util.HashMap();

    @DexIgnore
    public C1983hy(java.lang.String str) {
        this.f5890a = str;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1983hy mo11789a(java.lang.String str, java.lang.String str2) {
        this.f5891b.put(str, str2);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2439mx mo11790a() {
        com.fossil.blesdk.obfuscated.C2439mx mxVar = new com.fossil.blesdk.obfuscated.C2439mx(this.f5890a);
        for (java.lang.String next : this.f5891b.keySet()) {
            java.lang.Object obj = this.f5891b.get(next);
            if (obj instanceof java.lang.String) {
                mxVar.mo10145a(next, (java.lang.String) obj);
            } else if (obj instanceof java.lang.Number) {
                mxVar.mo10144a(next, (java.lang.Number) obj);
            }
        }
        return mxVar;
    }
}
