package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jr2 extends i62<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            kd4.b(str, "oldPass");
            kd4.b(str2, "newPass");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements i62.c {
    }

    /*
    static {
        String simpleName = jr2.class.getSimpleName();
        kd4.a((Object) simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public jr2(AuthApiUserService authApiUserService) {
        kd4.b(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends zo2<xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ jr2 e;

        @DexIgnore
        public e(jr2 jr2) {
            this.e = jr2;
        }

        @DexIgnore
        public void a(Call<xz1> call, qr4<xz1> qr4) {
            kd4.b(call, "call");
            kd4.b(qr4, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = jr2.f.a();
            local.d(a, "changePassword onSuccessResponse response=" + qr4);
            this.e.a().onSuccess(new d());
        }

        @DexIgnore
        public void a(Call<xz1> call, ServerError serverError) {
            kd4.b(call, "call");
            kd4.b(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = jr2.f.a();
            local.d(a, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            i62.d a2 = this.e.a();
            Integer code = serverError.getCode();
            kd4.a((Object) code, "response.code");
            a2.a(new c(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        public void a(Call<xz1> call, Throwable th) {
            kd4.b(call, "call");
            kd4.b(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = jr2.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(qa4.a);
            local.d(a, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.e.a().a(new c(MFNetworkReturnCode.CLIENT_TIMEOUT, (String) null));
            } else {
                this.e.a().a(new c(601, (String) null));
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        kd4.b(bVar, "requestValues");
        if (!as3.b(PortfolioApp.W.c())) {
            a().a(new c(601, ""));
            return;
        }
        xz1 xz1 = new xz1();
        try {
            xz1.a("oldPassword", bVar.b());
            xz1.a("newPassword", bVar.a());
        } catch (Exception unused) {
        }
        this.d.changePassword(xz1).a(new e(this));
    }
}
