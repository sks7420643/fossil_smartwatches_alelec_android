package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class di3 extends zr2 implements qi3, View.OnClickListener, ws3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<gb2> j;
    @DexIgnore
    public pi3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final di3 a() {
            return new di3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ di3 e;

        @DexIgnore
        public b(di3 di3) {
            this.e = di3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ di3 e;

        @DexIgnore
        public c(di3 di3) {
            this.e = di3;
        }

        @DexIgnore
        public final void onClick(View view) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.e.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.f(childFragmentManager);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ di3 e;

        @DexIgnore
        public d(di3 di3) {
            this.e = di3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().i();
            this.e.T0().a("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = di3.class.getSimpleName();
        kd4.a((Object) simpleName, "DeleteAccountFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void G0() {
        P0().c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(m, "deleteUser - successfully");
                WelcomeActivity.C.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final pi3 T0() {
        pi3 pi3 = this.k;
        if (pi3 != null) {
            return pi3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        kd4.b(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 1000) {
            super.onActivityResult(i, i2, intent);
        } else if (i2 == -1) {
            pi3 pi3 = this.k;
            if (pi3 != null) {
                pi3.h();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        kd4.b(view, "v");
        if (view.getId() == R.id.aciv_back) {
            n();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        gb2 gb2 = (gb2) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_delete_account, (ViewGroup) null, false, O0());
        this.j = new tr3<>(this, gb2);
        kd4.a((Object) gb2, "binding");
        return gb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        pi3 pi3 = this.k;
        if (pi3 != null) {
            pi3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        pi3 pi3 = this.k;
        if (pi3 != null) {
            pi3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<gb2> tr3 = this.j;
        if (tr3 != null) {
            gb2 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                String a3 = qf4.a(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_Text__IfYouWantToExportYour).toString(), "contact_our_support_team", "", false, 4, (Object) null);
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "binding.tvDescription");
                flexibleTextView.setText(Html.fromHtml(a3));
                a2.s.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, String str) {
        kd4.b(str, "message");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(pi3 pi3) {
        kd4.b(pi3, "presenter");
        st1.a(pi3);
        kd4.a((Object) pi3, "Preconditions.checkNotNull(presenter)");
        this.k = pi3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((BaseActivity) activity).a(str, i, intent);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i == R.id.tv_ok) {
                pi3 pi3 = this.k;
                if (pi3 != null) {
                    MFUser b2 = dn2.p.a().n().b();
                    kd4.a((Object) b2, "ProviderManager.getInsta\u2026serProvider().currentUser");
                    pi3.a(b2);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
        }
    }
}
