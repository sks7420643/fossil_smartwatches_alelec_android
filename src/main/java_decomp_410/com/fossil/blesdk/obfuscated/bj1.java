package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bj1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public Boolean f;
    @DexIgnore
    public og1 g;

    @DexIgnore
    public bj1(Context context, og1 og1) {
        bk0.a(context);
        Context applicationContext = context.getApplicationContext();
        bk0.a(applicationContext);
        this.a = applicationContext;
        if (og1 != null) {
            this.g = og1;
            this.b = og1.f;
            this.c = og1.e;
            this.d = og1.d;
            this.e = og1.c;
            Bundle bundle = og1.g;
            if (bundle != null) {
                this.f = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
