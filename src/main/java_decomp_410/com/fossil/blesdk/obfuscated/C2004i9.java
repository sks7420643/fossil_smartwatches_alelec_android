package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i9 */
public final class C2004i9 {
    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static boolean m8277a(android.view.ViewParent viewParent, android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        return viewParent.requestSendAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m8278b(android.view.ViewParent viewParent, android.view.View view, android.view.View view2, int i, int i2) {
        if (viewParent instanceof com.fossil.blesdk.obfuscated.C3348y8) {
            return ((com.fossil.blesdk.obfuscated.C3348y8) viewParent).mo1454a(view, view2, i, i2);
        }
        if (i2 != 0) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                return viewParent.onStartNestedScroll(view, view2, i);
            } catch (java.lang.AbstractMethodError e) {
                android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onStartNestedScroll", e);
                return false;
            }
        } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
            return ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onStartNestedScroll(view, view2, i);
        } else {
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8274a(android.view.ViewParent viewParent, android.view.View view, android.view.View view2, int i, int i2) {
        if (viewParent instanceof com.fossil.blesdk.obfuscated.C3348y8) {
            ((com.fossil.blesdk.obfuscated.C3348y8) viewParent).mo1461b(view, view2, i, i2);
        } else if (i2 != 0) {
        } else {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                try {
                    viewParent.onNestedScrollAccepted(view, view2, i);
                } catch (java.lang.AbstractMethodError e) {
                    android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedScrollAccepted", e);
                }
            } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
                ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onNestedScrollAccepted(view, view2, i);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8271a(android.view.ViewParent viewParent, android.view.View view, int i) {
        if (viewParent instanceof com.fossil.blesdk.obfuscated.C3348y8) {
            ((com.fossil.blesdk.obfuscated.C3348y8) viewParent).mo1439a(view, i);
        } else if (i != 0) {
        } else {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                try {
                    viewParent.onStopNestedScroll(view);
                } catch (java.lang.AbstractMethodError e) {
                    android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onStopNestedScroll", e);
                }
            } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
                ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onStopNestedScroll(view);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8272a(android.view.ViewParent viewParent, android.view.View view, int i, int i2, int i3, int i4, int i5) {
        if (viewParent instanceof com.fossil.blesdk.obfuscated.C3348y8) {
            ((com.fossil.blesdk.obfuscated.C3348y8) viewParent).mo1441a(view, i, i2, i3, i4, i5);
        } else if (i5 != 0) {
        } else {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                try {
                    viewParent.onNestedScroll(view, i, i2, i3, i4);
                } catch (java.lang.AbstractMethodError e) {
                    android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedScroll", e);
                }
            } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
                ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onNestedScroll(view, i, i2, i3, i4);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8273a(android.view.ViewParent viewParent, android.view.View view, int i, int i2, int[] iArr, int i3) {
        if (viewParent instanceof com.fossil.blesdk.obfuscated.C3348y8) {
            ((com.fossil.blesdk.obfuscated.C3348y8) viewParent).mo1442a(view, i, i2, iArr, i3);
        } else if (i3 != 0) {
        } else {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                try {
                    viewParent.onNestedPreScroll(view, i, i2, iArr);
                } catch (java.lang.AbstractMethodError e) {
                    android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedPreScroll", e);
                }
            } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
                ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onNestedPreScroll(view, i, i2, iArr);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8276a(android.view.ViewParent viewParent, android.view.View view, float f, float f2, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                return viewParent.onNestedFling(view, f, f2, z);
            } catch (java.lang.AbstractMethodError e) {
                android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedFling", e);
                return false;
            }
        } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
            return ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onNestedFling(view, f, f2, z);
        } else {
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8275a(android.view.ViewParent viewParent, android.view.View view, float f, float f2) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                return viewParent.onNestedPreFling(view, f, f2);
            } catch (java.lang.AbstractMethodError e) {
                android.util.Log.e("ViewParentCompat", "ViewParent " + viewParent + " does not implement interface " + "method onNestedPreFling", e);
                return false;
            }
        } else if (viewParent instanceof com.fossil.blesdk.obfuscated.C3418z8) {
            return ((com.fossil.blesdk.obfuscated.C3418z8) viewParent).onNestedPreFling(view, f, f2);
        } else {
            return false;
        }
    }
}
