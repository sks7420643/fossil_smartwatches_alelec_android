package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.fossil.blesdk.obfuscated.ws;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ft implements mo<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ ws a;
    @DexIgnore
    public /* final */ gq b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ws.b {
        @DexIgnore
        public /* final */ RecyclableBufferedInputStream a;
        @DexIgnore
        public /* final */ nw b;

        @DexIgnore
        public a(RecyclableBufferedInputStream recyclableBufferedInputStream, nw nwVar) {
            this.a = recyclableBufferedInputStream;
            this.b = nwVar;
        }

        @DexIgnore
        public void a() {
            this.a.y();
        }

        @DexIgnore
        public void a(jq jqVar, Bitmap bitmap) throws IOException {
            IOException y = this.b.y();
            if (y != null) {
                if (bitmap != null) {
                    jqVar.a(bitmap);
                }
                throw y;
            }
        }
    }

    @DexIgnore
    public ft(ws wsVar, gq gqVar) {
        this.a = wsVar;
        this.b = gqVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, lo loVar) {
        return this.a.a(inputStream);
    }

    @DexIgnore
    public aq<Bitmap> a(InputStream inputStream, int i, int i2, lo loVar) throws IOException {
        RecyclableBufferedInputStream recyclableBufferedInputStream;
        boolean z;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            recyclableBufferedInputStream = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            recyclableBufferedInputStream = new RecyclableBufferedInputStream(inputStream, this.b);
            z = true;
        }
        nw b2 = nw.b(recyclableBufferedInputStream);
        try {
            return this.a.a((InputStream) new rw(b2), i, i2, loVar, (ws.b) new a(recyclableBufferedInputStream, b2));
        } finally {
            b2.z();
            if (z) {
                recyclableBufferedInputStream.z();
            }
        }
    }
}
