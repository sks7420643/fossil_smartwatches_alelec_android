package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class we4 extends ve4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements re4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public a(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    @DexIgnore
    public static final <T> re4<T> a(Iterator<? extends T> it) {
        kd4.b(it, "$this$asSequence");
        return a(new a(it));
    }

    @DexIgnore
    public static final <T> re4<T> a(re4<? extends T> re4) {
        kd4.b(re4, "$this$constrainOnce");
        return re4 instanceof oe4 ? re4 : new oe4(re4);
    }

    @DexIgnore
    public static final <T> re4<T> a(wc4<? extends T> wc4, xc4<? super T, ? extends T> xc4) {
        kd4.b(wc4, "seedFunction");
        kd4.b(xc4, "nextFunction");
        return new qe4(wc4, xc4);
    }
}
