package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.firebase.FirebaseApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ew1 implements kw1 {
    @DexIgnore
    public static /* final */ kw1 a; // = new ew1();

    @DexIgnore
    public final Object a(jw1 jw1) {
        return dw1.a((FirebaseApp) jw1.a(FirebaseApp.class), (Context) jw1.a(Context.class), (bx1) jw1.a(bx1.class));
    }
}
