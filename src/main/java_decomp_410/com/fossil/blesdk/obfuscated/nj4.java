package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nj4 {
    /*
    static {
        try {
            ScheduledThreadPoolExecutor.class.getMethod("setRemoveOnCancelPolicy", new Class[]{Boolean.TYPE});
        } catch (Throwable unused) {
        }
    }
    */

    @DexIgnore
    public static final <E> Set<E> a(int i) {
        Set<E> newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(i));
        kd4.a((Object) newSetFromMap, "Collections.newSetFromMa\u2026ityHashMap(expectedSize))");
        return newSetFromMap;
    }
}
