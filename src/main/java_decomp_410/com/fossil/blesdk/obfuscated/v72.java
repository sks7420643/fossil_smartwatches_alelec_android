package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.util.ArrayList;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v72 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Background> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Background> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore
    public final String a(ArrayList<RingStyleItem> arrayList) {
        if (im0.a((Collection<?>) arrayList)) {
            return "";
        }
        String a2 = new Gson().a((Object) arrayList, new d().getType());
        kd4.a((Object) a2, "Gson().toJson(ringStyles, type)");
        return a2;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> b(String str) {
        kd4.b(str, "json");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new c().getType());
        kd4.a(a2, "Gson().fromJson(json, type)");
        return (ArrayList) a2;
    }

    @DexIgnore
    public final String a(Background background) {
        if (background == null) {
            return "";
        }
        String a2 = new Gson().a((Object) background, new a().getType());
        kd4.a((Object) a2, "Gson().toJson(background, type)");
        return a2;
    }

    @DexIgnore
    public final Background a(String str) {
        kd4.b(str, "json");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Background) new Gson().a(str, new b().getType());
    }
}
