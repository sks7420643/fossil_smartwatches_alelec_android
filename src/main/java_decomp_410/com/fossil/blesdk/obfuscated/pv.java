package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pv<R> implements nv<R>, qv<R> {
    @DexIgnore
    public static /* final */ a o; // = new a();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ a h;
    @DexIgnore
    public R i;
    @DexIgnore
    public ov j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public GlideException n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public void a(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }

        @DexIgnore
        public void a(Object obj) {
            obj.notifyAll();
        }
    }

    @DexIgnore
    public pv(int i2, int i3) {
        this(i2, i3, true, o);
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(aw awVar) {
    }

    @DexIgnore
    public synchronized void a(ov ovVar) {
        this.j = ovVar;
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Drawable drawable) {
    }

    @DexIgnore
    public void b(aw awVar) {
        awVar.a(this.e, this.f);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void c(Drawable drawable) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r3 == null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        r3.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        return true;
     */
    @DexIgnore
    public boolean cancel(boolean z) {
        ov ovVar;
        synchronized (this) {
            if (isDone()) {
                return false;
            }
            this.k = true;
            this.h.a(this);
            if (z) {
                ovVar = this.j;
                this.j = null;
            } else {
                ovVar = null;
            }
        }
    }

    @DexIgnore
    public synchronized ov d() {
        return this.j;
    }

    @DexIgnore
    public R get() throws InterruptedException, ExecutionException {
        try {
            return a((Long) null);
        } catch (TimeoutException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public synchronized boolean isCancelled() {
        return this.k;
    }

    @DexIgnore
    public synchronized boolean isDone() {
        return this.k || this.l || this.m;
    }

    @DexIgnore
    public pv(int i2, int i3, boolean z, a aVar) {
        this.e = i2;
        this.f = i3;
        this.g = z;
        this.h = aVar;
    }

    @DexIgnore
    public synchronized void a(Drawable drawable) {
    }

    @DexIgnore
    public R get(long j2, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return a(Long.valueOf(timeUnit.toMillis(j2)));
    }

    @DexIgnore
    public synchronized void a(R r, ew<? super R> ewVar) {
    }

    @DexIgnore
    public final synchronized R a(Long l2) throws ExecutionException, InterruptedException, TimeoutException {
        if (this.g && !isDone()) {
            uw.a();
        }
        if (this.k) {
            throw new CancellationException();
        } else if (this.m) {
            throw new ExecutionException(this.n);
        } else if (this.l) {
            return this.i;
        } else {
            if (l2 == null) {
                this.h.a(this, 0);
            } else if (l2.longValue() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                long longValue = l2.longValue() + currentTimeMillis;
                while (!isDone() && currentTimeMillis < longValue) {
                    this.h.a(this, longValue - currentTimeMillis);
                    currentTimeMillis = System.currentTimeMillis();
                }
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            } else if (this.m) {
                throw new ExecutionException(this.n);
            } else if (this.k) {
                throw new CancellationException();
            } else if (this.l) {
                return this.i;
            } else {
                throw new TimeoutException();
            }
        }
    }

    @DexIgnore
    public synchronized boolean a(GlideException glideException, Object obj, bw<R> bwVar, boolean z) {
        this.m = true;
        this.n = glideException;
        this.h.a(this);
        return false;
    }

    @DexIgnore
    public synchronized boolean a(R r, Object obj, bw<R> bwVar, DataSource dataSource, boolean z) {
        this.l = true;
        this.i = r;
        this.h.a(this);
        return false;
    }
}
