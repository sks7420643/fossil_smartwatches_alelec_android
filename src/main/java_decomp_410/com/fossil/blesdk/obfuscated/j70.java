package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AuthenticationOperationCode;
import com.fossil.blesdk.device.logic.request.code.AuthenticationResponseStatusCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j70 extends f70 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId I;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId J;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ j70(Peripheral peripheral, AuthenticationOperationCode authenticationOperationCode, RequestId requestId, int i, int i2, fd4 fd4) {
        this(peripheral, authenticationOperationCode, requestId, (i2 & 8) != 0 ? 3 : i);
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId B() {
        return this.J;
    }

    @DexIgnore
    public final byte[] D() {
        return this.G;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId E() {
        return this.I;
    }

    @DexIgnore
    public final byte[] G() {
        return this.H;
    }

    @DexIgnore
    public final long a(c20 c20) {
        kd4.b(c20, "notification");
        return super.a(c20);
    }

    @DexIgnore
    public final o70 b(byte b) {
        return AuthenticationResponseStatusCode.Companion.a(b);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j70(Peripheral peripheral, AuthenticationOperationCode authenticationOperationCode, RequestId requestId, int i) {
        super(requestId, peripheral, i);
        kd4.b(peripheral, "peripheral");
        kd4.b(authenticationOperationCode, "operation");
        kd4.b(requestId, "requestId");
        this.G = authenticationOperationCode.getOperationCode();
        this.H = authenticationOperationCode.getResponseOperationCode();
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.AUTHENTICATION;
        this.I = characteristicId;
        this.J = characteristicId;
    }
}
