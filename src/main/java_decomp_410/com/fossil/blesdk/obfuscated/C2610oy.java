package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oy */
public final class C2610oy {

    @DexIgnore
    /* renamed from: a */
    public /* final */ byte[] f8257a;

    @DexIgnore
    /* renamed from: b */
    public volatile int f8258b; // = 0;

    /*
    static {
        new com.fossil.blesdk.obfuscated.C2610oy(new byte[0]);
    }
    */

    @DexIgnore
    public C2610oy(byte[] bArr) {
        this.f8257a = bArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2610oy m12010a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        java.lang.System.arraycopy(bArr, i, bArr2, 0, i2);
        return new com.fossil.blesdk.obfuscated.C2610oy(bArr2);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo14554b() {
        return this.f8257a.length;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2610oy)) {
            return false;
        }
        byte[] bArr = this.f8257a;
        int length = bArr.length;
        byte[] bArr2 = ((com.fossil.blesdk.obfuscated.C2610oy) obj).f8257a;
        if (length != bArr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.f8258b;
        if (i == 0) {
            int i2 = r1;
            for (byte b : this.f8257a) {
                i2 = (i2 * 31) + b;
            }
            i = i2 == 0 ? 1 : i2;
            this.f8258b = i;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2610oy m12009a(java.lang.String str) {
        try {
            return new com.fossil.blesdk.obfuscated.C2610oy(str.getBytes("UTF-8"));
        } catch (java.io.UnsupportedEncodingException e) {
            throw new java.lang.RuntimeException("UTF-8 not supported.", e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14553a(byte[] bArr, int i, int i2, int i3) {
        java.lang.System.arraycopy(this.f8257a, i, bArr, i2, i3);
    }

    @DexIgnore
    /* renamed from: a */
    public java.io.InputStream mo14552a() {
        return new java.io.ByteArrayInputStream(this.f8257a);
    }
}
