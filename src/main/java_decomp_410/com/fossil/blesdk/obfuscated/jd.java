package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pd;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jd<Key, Value> extends ld<Key, Value> {
    @DexIgnore
    public abstract void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pd.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pd.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, pd.a<Value> aVar);

    @DexIgnore
    public abstract Key getKey(int i, Value value);

    @DexIgnore
    public boolean isContiguous() {
        return true;
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return true;
    }
}
