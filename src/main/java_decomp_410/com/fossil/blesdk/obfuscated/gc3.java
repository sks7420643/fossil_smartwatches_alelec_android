package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.enums.WorkoutType;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gc3 extends zr2 implements fc3 {
    @DexIgnore
    public tr3<kc2> j;
    @DexIgnore
    public ec3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            Date date = new Date();
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewDayFragment";
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewDayFragment", "onCreateView");
        kc2 kc2 = (kc2) qa.a(layoutInflater, R.layout.fragment_heartrate_overview_day, viewGroup, false, O0());
        kc2.s.setOnClickListener(b.e);
        this.j = new tr3<>(this, kc2);
        tr3<kc2> tr3 = this.j;
        if (tr3 != null) {
            kc2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        ec3 ec3 = this.k;
        if (ec3 != null) {
            ec3.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ec3 ec3 = this.k;
        if (ec3 != null) {
            ec3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ec3 ec3) {
        kd4.b(ec3, "presenter");
        this.k = ec3;
    }

    @DexIgnore
    public void a(int i, List<rt3> list, List<Triple<Integer, Pair<Integer, Float>, String>> list2) {
        kd4.b(list, "listTodayHeartRateModel");
        kd4.b(list2, "listTimeZoneChange");
        tr3<kc2> tr3 = this.j;
        if (tr3 != null) {
            kc2 a2 = tr3.a();
            if (a2 != null) {
                TodayHeartRateChart todayHeartRateChart = a2.u;
                if (todayHeartRateChart != null) {
                    todayHeartRateChart.setDayInMinuteWithTimeZone(i);
                    todayHeartRateChart.setListTimeZoneChange(list2);
                    todayHeartRateChart.a(list);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        kd4.b(list, "workoutSessions");
        tr3<kc2> tr3 = this.j;
        if (tr3 != null) {
            kc2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                LinearLayout linearLayout = a2.t;
                kd4.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.q, list.get(0));
                if (size == 1) {
                    ei2 ei2 = a2.r;
                    if (ei2 != null) {
                        View d = ei2.d();
                        if (d != null) {
                            d.setVisibility(8);
                            return;
                        }
                        return;
                    }
                    return;
                }
                ei2 ei22 = a2.r;
                if (ei22 != null) {
                    View d2 = ei22.d();
                    if (d2 != null) {
                        d2.setVisibility(0);
                    }
                }
                a(a2.r, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.t;
            kd4.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(ei2 ei2, WorkoutSession workoutSession) {
        if (ei2 != null) {
            View d = ei2.d();
            kd4.a((Object) d, "binding.root");
            Context context = d.getContext();
            Pair<Integer, Integer> a2 = WorkoutType.Companion.a(workoutSession.getWorkoutType());
            String a3 = sm2.a(context, a2.getSecond().intValue());
            ei2.u.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = ei2.t;
            kd4.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            FlexibleTextView flexibleTextView2 = ei2.r;
            kd4.a((Object) flexibleTextView2, "it.ftvRestingValue");
            WorkoutHeartRate heartRate = workoutSession.getHeartRate();
            int i = 0;
            flexibleTextView2.setText(String.valueOf(heartRate != null ? td4.a(heartRate.getAverage()) : 0));
            FlexibleTextView flexibleTextView3 = ei2.q;
            kd4.a((Object) flexibleTextView3, "it.ftvMaxValue");
            WorkoutHeartRate heartRate2 = workoutSession.getHeartRate();
            if (heartRate2 != null) {
                i = heartRate2.getMax();
            }
            flexibleTextView3.setText(String.valueOf(i));
            FlexibleTextView flexibleTextView4 = ei2.s;
            kd4.a((Object) flexibleTextView4, "it.ftvWorkoutTime");
            flexibleTextView4.setText(rk2.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
