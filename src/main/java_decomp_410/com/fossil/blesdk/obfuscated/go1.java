package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class go1<TResult> implements qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public qn1 c;

    @DexIgnore
    public go1(Executor executor, qn1 qn1) {
        this.a = executor;
        this.c = qn1;
    }

    @DexIgnore
    public final void onComplete(wn1 wn1) {
        if (wn1.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new ho1(this));
                }
            }
        }
    }
}
