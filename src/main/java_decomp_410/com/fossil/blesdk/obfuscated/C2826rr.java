package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rr */
public class C2826rr<A, B> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2772qw<com.fossil.blesdk.obfuscated.C2826rr.C2828b<A>, B> f9105a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rr$a */
    public class C2827a extends com.fossil.blesdk.obfuscated.C2772qw<com.fossil.blesdk.obfuscated.C2826rr.C2828b<A>, B> {
        @DexIgnore
        public C2827a(com.fossil.blesdk.obfuscated.C2826rr rrVar, long j) {
            super(j);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15391a(com.fossil.blesdk.obfuscated.C2826rr.C2828b<A> bVar, B b) {
            bVar.mo15720a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.rr$b */
    public static final class C2828b<A> {

        @DexIgnore
        /* renamed from: d */
        public static /* final */ java.util.Queue<com.fossil.blesdk.obfuscated.C2826rr.C2828b<?>> f9106d; // = com.fossil.blesdk.obfuscated.C3066uw.m14928a(0);

        @DexIgnore
        /* renamed from: a */
        public int f9107a;

        @DexIgnore
        /* renamed from: b */
        public int f9108b;

        @DexIgnore
        /* renamed from: c */
        public A f9109c;

        @DexIgnore
        /* renamed from: b */
        public static <A> com.fossil.blesdk.obfuscated.C2826rr.C2828b<A> m13355b(A a, int i, int i2) {
            com.fossil.blesdk.obfuscated.C2826rr.C2828b<A> poll;
            synchronized (f9106d) {
                poll = f9106d.poll();
            }
            if (poll == null) {
                poll = new com.fossil.blesdk.obfuscated.C2826rr.C2828b<>();
            }
            poll.mo15721a(a, i, i2);
            return poll;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo15721a(A a, int i, int i2) {
            this.f9109c = a;
            this.f9108b = i;
            this.f9107a = i2;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C2826rr.C2828b)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2826rr.C2828b bVar = (com.fossil.blesdk.obfuscated.C2826rr.C2828b) obj;
            if (this.f9108b == bVar.f9108b && this.f9107a == bVar.f9107a && this.f9109c.equals(bVar.f9109c)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.f9107a * 31) + this.f9108b) * 31) + this.f9109c.hashCode();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15720a() {
            synchronized (f9106d) {
                f9106d.offer(this);
            }
        }
    }

    @DexIgnore
    public C2826rr(long j) {
        this.f9105a = new com.fossil.blesdk.obfuscated.C2826rr.C2827a(this, j);
    }

    @DexIgnore
    /* renamed from: a */
    public B mo15717a(A a, int i, int i2) {
        com.fossil.blesdk.obfuscated.C2826rr.C2828b b = com.fossil.blesdk.obfuscated.C2826rr.C2828b.m13355b(a, i, i2);
        B a2 = this.f9105a.mo15388a(b);
        b.mo15720a();
        return a2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15718a(A a, int i, int i2, B b) {
        this.f9105a.mo15393b(com.fossil.blesdk.obfuscated.C2826rr.C2828b.m13355b(a, i, i2), b);
    }
}
