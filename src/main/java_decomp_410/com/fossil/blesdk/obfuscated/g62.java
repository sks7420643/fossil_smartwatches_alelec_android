package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g62 {
    @DexIgnore
    public static /* final */ int ALT; // = 2131361792;
    @DexIgnore
    public static /* final */ int BOLD; // = 2131361793;
    @DexIgnore
    public static /* final */ int BOLD_ITALIC; // = 2131361794;
    @DexIgnore
    public static /* final */ int CTRL; // = 2131361795;
    @DexIgnore
    public static /* final */ int DAY; // = 2131361796;
    @DexIgnore
    public static /* final */ int FUNCTION; // = 2131361797;
    @DexIgnore
    public static /* final */ int GENERAL; // = 2131361798;
    @DexIgnore
    public static /* final */ int ITALIC; // = 2131361799;
    @DexIgnore
    public static /* final */ int META; // = 2131361800;
    @DexIgnore
    public static /* final */ int MONTH; // = 2131361801;
    @DexIgnore
    public static /* final */ int NONE; // = 2131361802;
    @DexIgnore
    public static /* final */ int NORMAL; // = 2131361803;
    @DexIgnore
    public static /* final */ int SHIFT; // = 2131361804;
    @DexIgnore
    public static /* final */ int SYM; // = 2131361805;
    @DexIgnore
    public static /* final */ int WEEK; // = 2131361806;
    @DexIgnore
    public static /* final */ int abl_home_fragment; // = 2131361807;
    @DexIgnore
    public static /* final */ int accb_select; // = 2131361808;
    @DexIgnore
    public static /* final */ int accb_select_calls; // = 2131361809;
    @DexIgnore
    public static /* final */ int accb_select_messages; // = 2131361810;
    @DexIgnore
    public static /* final */ int aciv_back; // = 2131361811;
    @DexIgnore
    public static /* final */ int aciv_device; // = 2131361812;
    @DexIgnore
    public static /* final */ int aciv_icon; // = 2131361813;
    @DexIgnore
    public static /* final */ int aciv_left; // = 2131361814;
    @DexIgnore
    public static /* final */ int aciv_right; // = 2131361815;
    @DexIgnore
    public static /* final */ int action0; // = 2131361816;
    @DexIgnore
    public static /* final */ int action_bar; // = 2131361817;
    @DexIgnore
    public static /* final */ int action_bar_activity_content; // = 2131361818;
    @DexIgnore
    public static /* final */ int action_bar_container; // = 2131361819;
    @DexIgnore
    public static /* final */ int action_bar_root; // = 2131361820;
    @DexIgnore
    public static /* final */ int action_bar_spinner; // = 2131361821;
    @DexIgnore
    public static /* final */ int action_bar_subtitle; // = 2131361822;
    @DexIgnore
    public static /* final */ int action_bar_title; // = 2131361823;
    @DexIgnore
    public static /* final */ int action_container; // = 2131361824;
    @DexIgnore
    public static /* final */ int action_context_bar; // = 2131361825;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361826;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361827;
    @DexIgnore
    public static /* final */ int action_menu_divider; // = 2131361828;
    @DexIgnore
    public static /* final */ int action_menu_presenter; // = 2131361829;
    @DexIgnore
    public static /* final */ int action_mode_bar; // = 2131361830;
    @DexIgnore
    public static /* final */ int action_mode_bar_stub; // = 2131361831;
    @DexIgnore
    public static /* final */ int action_mode_close_button; // = 2131361832;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361833;
    @DexIgnore
    public static /* final */ int actions; // = 2131361834;
    @DexIgnore
    public static /* final */ int activity_chooser_view_content; // = 2131361835;
    @DexIgnore
    public static /* final */ int activity_contact_zendesk_root; // = 2131361836;
    @DexIgnore
    public static /* final */ int activity_network_no_connectivity; // = 2131361837;
    @DexIgnore
    public static /* final */ int activity_request_fragment; // = 2131361838;
    @DexIgnore
    public static /* final */ int activity_request_list_add_icon; // = 2131361839;
    @DexIgnore
    public static /* final */ int activity_request_view_root; // = 2131361840;
    @DexIgnore
    public static /* final */ int add; // = 2131361841;
    @DexIgnore
    public static /* final */ int adjust_height; // = 2131361842;
    @DexIgnore
    public static /* final */ int adjust_width; // = 2131361843;
    @DexIgnore
    public static /* final */ int alertTitle; // = 2131361844;
    @DexIgnore
    public static /* final */ int alertsFragment; // = 2131361845;
    @DexIgnore
    public static /* final */ int all; // = 2131361846;
    @DexIgnore
    public static /* final */ int always; // = 2131361847;
    @DexIgnore
    public static /* final */ int anonymous_switch; // = 2131361848;
    @DexIgnore
    public static /* final */ int appBarLayout; // = 2131361849;
    @DexIgnore
    public static /* final */ int article_attachment_row_filename_text; // = 2131361850;
    @DexIgnore
    public static /* final */ int article_attachment_row_filesize_text; // = 2131361851;
    @DexIgnore
    public static /* final */ int article_title; // = 2131361852;
    @DexIgnore
    public static /* final */ int article_voting_container; // = 2131361853;
    @DexIgnore
    public static /* final */ int async; // = 2131361854;
    @DexIgnore
    public static /* final */ int attachment_delete; // = 2131361855;
    @DexIgnore
    public static /* final */ int attachment_image; // = 2131361856;
    @DexIgnore
    public static /* final */ int attachment_inline_container; // = 2131361857;
    @DexIgnore
    public static /* final */ int attachment_inline_image; // = 2131361858;
    @DexIgnore
    public static /* final */ int attachment_inline_progressbar; // = 2131361859;
    @DexIgnore
    public static /* final */ int attachment_progress; // = 2131361860;
    @DexIgnore
    public static /* final */ int auto; // = 2131361861;
    @DexIgnore
    public static /* final */ int autocomplete_places; // = 2131361862;
    @DexIgnore
    public static /* final */ int automatic; // = 2131361863;
    @DexIgnore
    public static /* final */ int avatar; // = 2131361864;
    @DexIgnore
    public static /* final */ int back_iv; // = 2131361865;
    @DexIgnore
    public static /* final */ int barrier; // = 2131361866;
    @DexIgnore
    public static /* final */ int barrier_edit_goal_bottom; // = 2131361867;
    @DexIgnore
    public static /* final */ int beginning; // = 2131361868;
    @DexIgnore
    public static /* final */ int belvedere_dialog_listview; // = 2131361869;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_image; // = 2131361870;
    @DexIgnore
    public static /* final */ int belvedere_dialog_row_text; // = 2131361871;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361872;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361873;
    @DexIgnore
    public static /* final */ int bottom_line; // = 2131361874;
    @DexIgnore
    public static /* final */ int bottom_navigation; // = 2131361875;
    @DexIgnore
    public static /* final */ int box_count; // = 2131361876;
    @DexIgnore
    public static /* final */ int br_bottom; // = 2131361877;
    @DexIgnore
    public static /* final */ int br_device; // = 2131361878;
    @DexIgnore
    public static /* final */ int br_top; // = 2131361879;
    @DexIgnore
    public static /* final */ int browser_actions_header_text; // = 2131361880;
    @DexIgnore
    public static /* final */ int browser_actions_menu_item_icon; // = 2131361881;
    @DexIgnore
    public static /* final */ int browser_actions_menu_item_text; // = 2131361882;
    @DexIgnore
    public static /* final */ int browser_actions_menu_items; // = 2131361883;
    @DexIgnore
    public static /* final */ int browser_actions_menu_view; // = 2131361884;
    @DexIgnore
    public static /* final */ int bt_about; // = 2131361885;
    @DexIgnore
    public static /* final */ int bt_active; // = 2131361886;
    @DexIgnore
    public static /* final */ int bt_back; // = 2131361887;
    @DexIgnore
    public static /* final */ int bt_change_password; // = 2131361888;
    @DexIgnore
    public static /* final */ int bt_connect; // = 2131361889;
    @DexIgnore
    public static /* final */ int bt_connectedapps; // = 2131361890;
    @DexIgnore
    public static /* final */ int bt_continue; // = 2131361891;
    @DexIgnore
    public static /* final */ int bt_continue_sign_up; // = 2131361892;
    @DexIgnore
    public static /* final */ int bt_faq; // = 2131361893;
    @DexIgnore
    public static /* final */ int bt_help; // = 2131361894;
    @DexIgnore
    public static /* final */ int bt_install_now; // = 2131361895;
    @DexIgnore
    public static /* final */ int bt_kill_app_feedback; // = 2131361896;
    @DexIgnore
    public static /* final */ int bt_need_help; // = 2131361897;
    @DexIgnore
    public static /* final */ int bt_on_off_bluetooth_feedback; // = 2131361898;
    @DexIgnore
    public static /* final */ int bt_open_source_license; // = 2131361899;
    @DexIgnore
    public static /* final */ int bt_opt_in; // = 2131361900;
    @DexIgnore
    public static /* final */ int bt_privacy_policy; // = 2131361901;
    @DexIgnore
    public static /* final */ int bt_purchase; // = 2131361902;
    @DexIgnore
    public static /* final */ int bt_remind; // = 2131361903;
    @DexIgnore
    public static /* final */ int bt_repair_center; // = 2131361904;
    @DexIgnore
    public static /* final */ int bt_replace_battery; // = 2131361905;
    @DexIgnore
    public static /* final */ int bt_set_goals; // = 2131361906;
    @DexIgnore
    public static /* final */ int bt_terms_of_use; // = 2131361907;
    @DexIgnore
    public static /* final */ int bt_tutorial; // = 2131361908;
    @DexIgnore
    public static /* final */ int bt_units; // = 2131361909;
    @DexIgnore
    public static /* final */ int btn_add; // = 2131361910;
    @DexIgnore
    public static /* final */ int btn_cancel; // = 2131361911;
    @DexIgnore
    public static /* final */ int btn_debug_child_with_spinner; // = 2131361912;
    @DexIgnore
    public static /* final */ int btn_done; // = 2131361913;
    @DexIgnore
    public static /* final */ int btn_got_it; // = 2131361914;
    @DexIgnore
    public static /* final */ int btn_search_clear; // = 2131361915;
    @DexIgnore
    public static /* final */ int button; // = 2131361916;
    @DexIgnore
    public static /* final */ int buttonPanel; // = 2131361917;
    @DexIgnore
    public static /* final */ int buttons; // = 2131361918;
    @DexIgnore
    public static /* final */ int c_search; // = 2131361919;
    @DexIgnore
    public static /* final */ int calendarMonth; // = 2131361920;
    @DexIgnore
    public static /* final */ int calligraphy_tag_id; // = 2131361921;
    @DexIgnore
    public static /* final */ int cancel_action; // = 2131361922;
    @DexIgnore
    public static /* final */ int cancel_button; // = 2131361923;
    @DexIgnore
    public static /* final */ int card_frame_content_container; // = 2131361924;
    @DexIgnore
    public static /* final */ int card_frame_overlay_container; // = 2131361925;
    @DexIgnore
    public static /* final */ int card_profile_info; // = 2131361926;
    @DexIgnore
    public static /* final */ int card_view; // = 2131361927;
    @DexIgnore
    public static /* final */ int category_title; // = 2131361928;
    @DexIgnore
    public static /* final */ int cb_collect_data; // = 2131361929;
    @DexIgnore
    public static /* final */ int cb_googlefit; // = 2131361930;
    @DexIgnore
    public static /* final */ int cb_jawbone; // = 2131361931;
    @DexIgnore
    public static /* final */ int cb_location; // = 2131361932;
    @DexIgnore
    public static /* final */ int cb_terms_service; // = 2131361933;
    @DexIgnore
    public static /* final */ int cb_under_armour; // = 2131361934;
    @DexIgnore
    public static /* final */ int cdl_home_fragment; // = 2131361935;
    @DexIgnore
    public static /* final */ int cegv_bottom_left; // = 2131361936;
    @DexIgnore
    public static /* final */ int cegv_bottom_right; // = 2131361937;
    @DexIgnore
    public static /* final */ int cegv_top_left; // = 2131361938;
    @DexIgnore
    public static /* final */ int cegv_top_right; // = 2131361939;
    @DexIgnore
    public static /* final */ int center; // = 2131361940;
    @DexIgnore
    public static /* final */ int center_horizontal; // = 2131361941;
    @DexIgnore
    public static /* final */ int center_vertical; // = 2131361942;
    @DexIgnore
    public static /* final */ int chains; // = 2131361943;
    @DexIgnore
    public static /* final */ int checkbox; // = 2131361944;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361945;
    @DexIgnore
    public static /* final */ int circle; // = 2131361946;
    @DexIgnore
    public static /* final */ int clBottom; // = 2131361947;
    @DexIgnore
    public static /* final */ int clHRSleepSession; // = 2131361948;
    @DexIgnore
    public static /* final */ int clTracking; // = 2131361949;
    @DexIgnore
    public static /* final */ int cl_active_time; // = 2131361950;
    @DexIgnore
    public static /* final */ int cl_address_commute_time_container; // = 2131361951;
    @DexIgnore
    public static /* final */ int cl_all_apps; // = 2131361952;
    @DexIgnore
    public static /* final */ int cl_avg_sleep; // = 2131361953;
    @DexIgnore
    public static /* final */ int cl_avg_step; // = 2131361954;
    @DexIgnore
    public static /* final */ int cl_awake; // = 2131361955;
    @DexIgnore
    public static /* final */ int cl_bedtime_reminder_container; // = 2131361956;
    @DexIgnore
    public static /* final */ int cl_birthday_picker; // = 2131361957;
    @DexIgnore
    public static /* final */ int cl_calls_from_every_one; // = 2131361958;
    @DexIgnore
    public static /* final */ int cl_calories; // = 2131361959;
    @DexIgnore
    public static /* final */ int cl_commute_time; // = 2131361960;
    @DexIgnore
    public static /* final */ int cl_complication_setting; // = 2131361961;
    @DexIgnore
    public static /* final */ int cl_complications; // = 2131361962;
    @DexIgnore
    public static /* final */ int cl_container; // = 2131361963;
    @DexIgnore
    public static /* final */ int cl_content; // = 2131361964;
    @DexIgnore
    public static /* final */ int cl_daily_value; // = 2131361965;
    @DexIgnore
    public static /* final */ int cl_days_repeat; // = 2131361966;
    @DexIgnore
    public static /* final */ int cl_debug_parent; // = 2131361967;
    @DexIgnore
    public static /* final */ int cl_deep; // = 2131361968;
    @DexIgnore
    public static /* final */ int cl_default_place_commute_time_container; // = 2131361969;
    @DexIgnore
    public static /* final */ int cl_detail_day; // = 2131361970;
    @DexIgnore
    public static /* final */ int cl_fw_available; // = 2131361971;
    @DexIgnore
    public static /* final */ int cl_googlefit; // = 2131361972;
    @DexIgnore
    public static /* final */ int cl_inactivity_nudge_container; // = 2131361973;
    @DexIgnore
    public static /* final */ int cl_jawbone; // = 2131361974;
    @DexIgnore
    public static /* final */ int cl_keep_going_container; // = 2131361975;
    @DexIgnore
    public static /* final */ int cl_left; // = 2131361976;
    @DexIgnore
    public static /* final */ int cl_light; // = 2131361977;
    @DexIgnore
    public static /* final */ int cl_location; // = 2131361978;
    @DexIgnore
    public static /* final */ int cl_low_battery; // = 2131361979;
    @DexIgnore
    public static /* final */ int cl_main_container; // = 2131361980;
    @DexIgnore
    public static /* final */ int cl_micro_apps; // = 2131361981;
    @DexIgnore
    public static /* final */ int cl_model; // = 2131361982;
    @DexIgnore
    public static /* final */ int cl_no_device; // = 2131361983;
    @DexIgnore
    public static /* final */ int cl_no_device_container; // = 2131361984;
    @DexIgnore
    public static /* final */ int cl_overview_day; // = 2131361985;
    @DexIgnore
    public static /* final */ int cl_pb_goal; // = 2131361986;
    @DexIgnore
    public static /* final */ int cl_preset_buttons; // = 2131361987;
    @DexIgnore
    public static /* final */ int cl_recorded_goal_tracking; // = 2131361988;
    @DexIgnore
    public static /* final */ int cl_reminders_container; // = 2131361989;
    @DexIgnore
    public static /* final */ int cl_reset_pw; // = 2131361990;
    @DexIgnore
    public static /* final */ int cl_reset_pw_success; // = 2131361991;
    @DexIgnore
    public static /* final */ int cl_right; // = 2131361992;
    @DexIgnore
    public static /* final */ int cl_root; // = 2131361993;
    @DexIgnore
    public static /* final */ int cl_scheduled_time_container; // = 2131361994;
    @DexIgnore
    public static /* final */ int cl_sections_container; // = 2131361995;
    @DexIgnore
    public static /* final */ int cl_share_data; // = 2131361996;
    @DexIgnore
    public static /* final */ int cl_sleep_summary_container; // = 2131361997;
    @DexIgnore
    public static /* final */ int cl_subcribe_email; // = 2131361998;
    @DexIgnore
    public static /* final */ int cl_texts_from_every_one; // = 2131361999;
    @DexIgnore
    public static /* final */ int cl_time_picker; // = 2131362000;
    @DexIgnore
    public static /* final */ int cl_toolbar; // = 2131362001;
    @DexIgnore
    public static /* final */ int cl_top; // = 2131362002;
    @DexIgnore
    public static /* final */ int cl_under_amour; // = 2131362003;
    @DexIgnore
    public static /* final */ int cl_update_fw; // = 2131362004;
    @DexIgnore
    public static /* final */ int cl_update_fw_fail; // = 2131362005;
    @DexIgnore
    public static /* final */ int cl_updating_fw; // = 2131362006;
    @DexIgnore
    public static /* final */ int cl_verification_code; // = 2131362007;
    @DexIgnore
    public static /* final */ int cl_visualization; // = 2131362008;
    @DexIgnore
    public static /* final */ int cl_watch_apps; // = 2131362009;
    @DexIgnore
    public static /* final */ int cl_weekly; // = 2131362010;
    @DexIgnore
    public static /* final */ int clear_iv; // = 2131362011;
    @DexIgnore
    public static /* final */ int clip_horizontal; // = 2131362012;
    @DexIgnore
    public static /* final */ int clip_vertical; // = 2131362013;
    @DexIgnore
    public static /* final */ int close_iv; // = 2131362014;
    @DexIgnore
    public static /* final */ int collapseActionView; // = 2131362015;
    @DexIgnore
    public static /* final */ int collapsing_toolbar; // = 2131362016;
    @DexIgnore
    public static /* final */ int com_facebook_body_frame; // = 2131362017;
    @DexIgnore
    public static /* final */ int com_facebook_button_xout; // = 2131362018;
    @DexIgnore
    public static /* final */ int com_facebook_device_auth_instructions; // = 2131362019;
    @DexIgnore
    public static /* final */ int com_facebook_fragment_container; // = 2131362020;
    @DexIgnore
    public static /* final */ int com_facebook_login_fragment_progress_bar; // = 2131362021;
    @DexIgnore
    public static /* final */ int com_facebook_smart_instructions_0; // = 2131362022;
    @DexIgnore
    public static /* final */ int com_facebook_smart_instructions_or; // = 2131362023;
    @DexIgnore
    public static /* final */ int com_facebook_tooltip_bubble_view_bottom_pointer; // = 2131362024;
    @DexIgnore
    public static /* final */ int com_facebook_tooltip_bubble_view_text_body; // = 2131362025;
    @DexIgnore
    public static /* final */ int com_facebook_tooltip_bubble_view_top_pointer; // = 2131362026;
    @DexIgnore
    public static /* final */ int confirmation_code; // = 2131362027;
    @DexIgnore
    public static /* final */ int contact_fragment_attachments; // = 2131362028;
    @DexIgnore
    public static /* final */ int contact_fragment_container; // = 2131362029;
    @DexIgnore
    public static /* final */ int contact_fragment_description; // = 2131362030;
    @DexIgnore
    public static /* final */ int contact_fragment_email; // = 2131362031;
    @DexIgnore
    public static /* final */ int contact_fragment_progress; // = 2131362032;
    @DexIgnore
    public static /* final */ int contact_us_button; // = 2131362033;
    @DexIgnore
    public static /* final */ int container; // = 2131362034;
    @DexIgnore
    public static /* final */ int content; // = 2131362035;
    @DexIgnore
    public static /* final */ int contentPanel; // = 2131362036;
    @DexIgnore
    public static /* final */ int coordinator; // = 2131362037;
    @DexIgnore
    public static /* final */ int coordinatorLayout; // = 2131362038;
    @DexIgnore
    public static /* final */ int cpi_preset; // = 2131362039;
    @DexIgnore
    public static /* final */ int cpi_sleep; // = 2131362040;
    @DexIgnore
    public static /* final */ int custom; // = 2131362041;
    @DexIgnore
    public static /* final */ int customPanel; // = 2131362042;
    @DexIgnore
    public static /* final */ int customToolbar; // = 2131362043;
    @DexIgnore
    public static /* final */ int customizeFragment; // = 2131362044;
    @DexIgnore
    public static /* final */ int cv_detail; // = 2131362045;
    @DexIgnore
    public static /* final */ int cv_group; // = 2131362046;
    @DexIgnore
    public static /* final */ int cv_pair_first_watch; // = 2131362047;
    @DexIgnore
    public static /* final */ int cv_root; // = 2131362048;
    @DexIgnore
    public static /* final */ int daily_item; // = 2131362049;
    @DexIgnore
    public static /* final */ int dark; // = 2131362050;
    @DexIgnore
    public static /* final */ int dashboardFragment; // = 2131362051;
    @DexIgnore
    public static /* final */ int dataBinding; // = 2131362052;
    @DexIgnore
    public static /* final */ int dayChart; // = 2131362053;
    @DexIgnore
    public static /* final */ int day_friday; // = 2131362054;
    @DexIgnore
    public static /* final */ int day_monday; // = 2131362055;
    @DexIgnore
    public static /* final */ int day_saturday; // = 2131362056;
    @DexIgnore
    public static /* final */ int day_sunday; // = 2131362057;
    @DexIgnore
    public static /* final */ int day_thursday; // = 2131362058;
    @DexIgnore
    public static /* final */ int day_tuesday; // = 2131362059;
    @DexIgnore
    public static /* final */ int day_wednesday; // = 2131362060;
    @DexIgnore
    public static /* final */ int days; // = 2131362061;
    @DexIgnore
    public static /* final */ int decor_content_parent; // = 2131362062;
    @DexIgnore
    public static /* final */ int default_activity_button; // = 2131362063;
    @DexIgnore
    public static /* final */ int design_bottom_sheet; // = 2131362064;
    @DexIgnore
    public static /* final */ int design_menu_item_action_area; // = 2131362065;
    @DexIgnore
    public static /* final */ int design_menu_item_action_area_stub; // = 2131362066;
    @DexIgnore
    public static /* final */ int design_menu_item_text; // = 2131362067;
    @DexIgnore
    public static /* final */ int design_navigation_view; // = 2131362068;
    @DexIgnore
    public static /* final */ int dimensions; // = 2131362069;
    @DexIgnore
    public static /* final */ int direct; // = 2131362070;
    @DexIgnore
    public static /* final */ int disableHome; // = 2131362071;
    @DexIgnore
    public static /* final */ int dismiss_btn; // = 2131362072;
    @DexIgnore
    public static /* final */ int display_always; // = 2131362073;
    @DexIgnore
    public static /* final */ int distance_container; // = 2131362074;
    @DexIgnore
    public static /* final */ int divider; // = 2131362075;
    @DexIgnore
    public static /* final */ int downvote_button; // = 2131362076;
    @DexIgnore
    public static /* final */ int downvote_button_frame; // = 2131362077;
    @DexIgnore
    public static /* final */ int edit_query; // = 2131362078;
    @DexIgnore
    public static /* final */ int end; // = 2131362079;
    @DexIgnore
    public static /* final */ int end_padder; // = 2131362080;
    @DexIgnore
    public static /* final */ int enterAlways; // = 2131362081;
    @DexIgnore
    public static /* final */ int enterAlwaysCollapsed; // = 2131362082;
    @DexIgnore
    public static /* final */ int et_birthday; // = 2131362083;
    @DexIgnore
    public static /* final */ int et_delay; // = 2131362084;
    @DexIgnore
    public static /* final */ int et_delay_each_time; // = 2131362085;
    @DexIgnore
    public static /* final */ int et_duration; // = 2131362086;
    @DexIgnore
    public static /* final */ int et_email; // = 2131362087;
    @DexIgnore
    public static /* final */ int et_first_code; // = 2131362088;
    @DexIgnore
    public static /* final */ int et_first_name; // = 2131362089;
    @DexIgnore
    public static /* final */ int et_fourth_code; // = 2131362090;
    @DexIgnore
    public static /* final */ int et_last_name; // = 2131362091;
    @DexIgnore
    public static /* final */ int et_new_pass; // = 2131362092;
    @DexIgnore
    public static /* final */ int et_old_pass; // = 2131362093;
    @DexIgnore
    public static /* final */ int et_password; // = 2131362094;
    @DexIgnore
    public static /* final */ int et_repeat; // = 2131362095;
    @DexIgnore
    public static /* final */ int et_repeat_pass; // = 2131362096;
    @DexIgnore
    public static /* final */ int et_search; // = 2131362097;
    @DexIgnore
    public static /* final */ int et_second_code; // = 2131362098;
    @DexIgnore
    public static /* final */ int et_third_code; // = 2131362099;
    @DexIgnore
    public static /* final */ int exitUntilCollapsed; // = 2131362100;
    @DexIgnore
    public static /* final */ int expand_activities_button; // = 2131362101;
    @DexIgnore
    public static /* final */ int expanded_menu; // = 2131362102;
    @DexIgnore
    public static /* final */ int f_map; // = 2131362103;
    @DexIgnore
    public static /* final */ int fb_add; // = 2131362104;
    @DexIgnore
    public static /* final */ int fb_add_to_favorites; // = 2131362105;
    @DexIgnore
    public static /* final */ int fb_close; // = 2131362106;
    @DexIgnore
    public static /* final */ int fb_continue; // = 2131362107;
    @DexIgnore
    public static /* final */ int fb_create_account; // = 2131362108;
    @DexIgnore
    public static /* final */ int fb_delete_account; // = 2131362109;
    @DexIgnore
    public static /* final */ int fb_female; // = 2131362110;
    @DexIgnore
    public static /* final */ int fb_find_watch; // = 2131362111;
    @DexIgnore
    public static /* final */ int fb_get_started; // = 2131362112;
    @DexIgnore
    public static /* final */ int fb_grant_permission; // = 2131362113;
    @DexIgnore
    public static /* final */ int fb_login; // = 2131362114;
    @DexIgnore
    public static /* final */ int fb_male; // = 2131362115;
    @DexIgnore
    public static /* final */ int fb_ok; // = 2131362116;
    @DexIgnore
    public static /* final */ int fb_other; // = 2131362117;
    @DexIgnore
    public static /* final */ int fb_save; // = 2131362118;
    @DexIgnore
    public static /* final */ int fb_send_link; // = 2131362119;
    @DexIgnore
    public static /* final */ int fb_skip; // = 2131362120;
    @DexIgnore
    public static /* final */ int fb_start_pairing; // = 2131362121;
    @DexIgnore
    public static /* final */ int fb_try_again; // = 2131362122;
    @DexIgnore
    public static /* final */ int fb_vibration_high; // = 2131362123;
    @DexIgnore
    public static /* final */ int fb_vibration_low; // = 2131362124;
    @DexIgnore
    public static /* final */ int fb_vibration_medium; // = 2131362125;
    @DexIgnore
    public static /* final */ int fet_address_name; // = 2131362126;
    @DexIgnore
    public static /* final */ int fet_goals_value; // = 2131362127;
    @DexIgnore
    public static /* final */ int fet_rename; // = 2131362128;
    @DexIgnore
    public static /* final */ int fet_search_apps; // = 2131362129;
    @DexIgnore
    public static /* final */ int fet_search_contact; // = 2131362130;
    @DexIgnore
    public static /* final */ int fet_search_contacts; // = 2131362131;
    @DexIgnore
    public static /* final */ int fet_sleep_hour_value; // = 2131362132;
    @DexIgnore
    public static /* final */ int fet_sleep_minute_value; // = 2131362133;
    @DexIgnore
    public static /* final */ int fill; // = 2131362134;
    @DexIgnore
    public static /* final */ int fill_horizontal; // = 2131362135;
    @DexIgnore
    public static /* final */ int fill_vertical; // = 2131362136;
    @DexIgnore
    public static /* final */ int filled; // = 2131362137;
    @DexIgnore
    public static /* final */ int fixed; // = 2131362138;
    @DexIgnore
    public static /* final */ int flv_assigned_calls; // = 2131362139;
    @DexIgnore
    public static /* final */ int flv_assigned_texts; // = 2131362140;
    @DexIgnore
    public static /* final */ int forever; // = 2131362141;
    @DexIgnore
    public static /* final */ int fossil_button_container; // = 2131362142;
    @DexIgnore
    public static /* final */ int fossil_button_icon; // = 2131362143;
    @DexIgnore
    public static /* final */ int fossil_button_title; // = 2131362144;
    @DexIgnore
    public static /* final */ int fragment_contact_zendesk_attachment; // = 2131362145;
    @DexIgnore
    public static /* final */ int fragment_contact_zendesk_menu_done; // = 2131362146;
    @DexIgnore
    public static /* final */ int fragment_container; // = 2131362147;
    @DexIgnore
    public static /* final */ int fragment_help_menu_contact; // = 2131362148;
    @DexIgnore
    public static /* final */ int fragment_help_menu_search; // = 2131362149;
    @DexIgnore
    public static /* final */ int freedom; // = 2131362150;
    @DexIgnore
    public static /* final */ int freedom_no_bottom; // = 2131362151;
    @DexIgnore
    public static /* final */ int ftv7Days; // = 2131362152;
    @DexIgnore
    public static /* final */ int ftvDay; // = 2131362153;
    @DexIgnore
    public static /* final */ int ftvDeviceName; // = 2131362154;
    @DexIgnore
    public static /* final */ int ftvDistance; // = 2131362155;
    @DexIgnore
    public static /* final */ int ftvHeight; // = 2131362156;
    @DexIgnore
    public static /* final */ int ftvLastSynced; // = 2131362157;
    @DexIgnore
    public static /* final */ int ftvMonth; // = 2131362158;
    @DexIgnore
    public static /* final */ int ftvNoDevice; // = 2131362159;
    @DexIgnore
    public static /* final */ int ftvPopupContent; // = 2131362160;
    @DexIgnore
    public static /* final */ int ftvStatus; // = 2131362161;
    @DexIgnore
    public static /* final */ int ftvTemperature; // = 2131362162;
    @DexIgnore
    public static /* final */ int ftvTitle; // = 2131362163;
    @DexIgnore
    public static /* final */ int ftvToday; // = 2131362164;
    @DexIgnore
    public static /* final */ int ftvViewMore; // = 2131362165;
    @DexIgnore
    public static /* final */ int ftvWeight; // = 2131362166;
    @DexIgnore
    public static /* final */ int ftv_active_time; // = 2131362167;
    @DexIgnore
    public static /* final */ int ftv_add; // = 2131362168;
    @DexIgnore
    public static /* final */ int ftv_address_error; // = 2131362169;
    @DexIgnore
    public static /* final */ int ftv_alarms_section; // = 2131362170;
    @DexIgnore
    public static /* final */ int ftv_alerts; // = 2131362171;
    @DexIgnore
    public static /* final */ int ftv_all_apps; // = 2131362172;
    @DexIgnore
    public static /* final */ int ftv_allow_calls_from_value; // = 2131362173;
    @DexIgnore
    public static /* final */ int ftv_allow_gather_data; // = 2131362174;
    @DexIgnore
    public static /* final */ int ftv_allow_messages_from_value; // = 2131362175;
    @DexIgnore
    public static /* final */ int ftv_alphabet; // = 2131362176;
    @DexIgnore
    public static /* final */ int ftv_app_name; // = 2131362177;
    @DexIgnore
    public static /* final */ int ftv_apps_overview; // = 2131362178;
    @DexIgnore
    public static /* final */ int ftv_assign_notifications_overview; // = 2131362179;
    @DexIgnore
    public static /* final */ int ftv_assign_section; // = 2131362180;
    @DexIgnore
    public static /* final */ int ftv_assigned; // = 2131362181;
    @DexIgnore
    public static /* final */ int ftv_bedtime_reminder_label; // = 2131362182;
    @DexIgnore
    public static /* final */ int ftv_calls; // = 2131362183;
    @DexIgnore
    public static /* final */ int ftv_calls_and_messages; // = 2131362184;
    @DexIgnore
    public static /* final */ int ftv_calories; // = 2131362185;
    @DexIgnore
    public static /* final */ int ftv_cancel; // = 2131362186;
    @DexIgnore
    public static /* final */ int ftv_category; // = 2131362187;
    @DexIgnore
    public static /* final */ int ftv_confirm_desc; // = 2131362188;
    @DexIgnore
    public static /* final */ int ftv_confirm_title; // = 2131362189;
    @DexIgnore
    public static /* final */ int ftv_contact_cs; // = 2131362190;
    @DexIgnore
    public static /* final */ int ftv_content; // = 2131362191;
    @DexIgnore
    public static /* final */ int ftv_countdown_time; // = 2131362192;
    @DexIgnore
    public static /* final */ int ftv_current_timezone; // = 2131362193;
    @DexIgnore
    public static /* final */ int ftv_customize; // = 2131362194;
    @DexIgnore
    public static /* final */ int ftv_daily_unit; // = 2131362195;
    @DexIgnore
    public static /* final */ int ftv_daily_unit2; // = 2131362196;
    @DexIgnore
    public static /* final */ int ftv_daily_value; // = 2131362197;
    @DexIgnore
    public static /* final */ int ftv_daily_value2; // = 2131362198;
    @DexIgnore
    public static /* final */ int ftv_day_of_month; // = 2131362199;
    @DexIgnore
    public static /* final */ int ftv_day_of_week; // = 2131362200;
    @DexIgnore
    public static /* final */ int ftv_delete; // = 2131362201;
    @DexIgnore
    public static /* final */ int ftv_desc; // = 2131362202;
    @DexIgnore
    public static /* final */ int ftv_description; // = 2131362203;
    @DexIgnore
    public static /* final */ int ftv_description_1; // = 2131362204;
    @DexIgnore
    public static /* final */ int ftv_description_2; // = 2131362205;
    @DexIgnore
    public static /* final */ int ftv_description_3; // = 2131362206;
    @DexIgnore
    public static /* final */ int ftv_description_low_battery; // = 2131362207;
    @DexIgnore
    public static /* final */ int ftv_description_subcribe_email; // = 2131362208;
    @DexIgnore
    public static /* final */ int ftv_device_name; // = 2131362209;
    @DexIgnore
    public static /* final */ int ftv_device_serial; // = 2131362210;
    @DexIgnore
    public static /* final */ int ftv_distance; // = 2131362211;
    @DexIgnore
    public static /* final */ int ftv_do_not_disturb_section; // = 2131362212;
    @DexIgnore
    public static /* final */ int ftv_dont_see_device; // = 2131362213;
    @DexIgnore
    public static /* final */ int ftv_email; // = 2131362214;
    @DexIgnore
    public static /* final */ int ftv_email_notification; // = 2131362215;
    @DexIgnore
    public static /* final */ int ftv_enter_codes_guide; // = 2131362216;
    @DexIgnore
    public static /* final */ int ftv_error; // = 2131362217;
    @DexIgnore
    public static /* final */ int ftv_est; // = 2131362218;
    @DexIgnore
    public static /* final */ int ftv_everyone; // = 2131362219;
    @DexIgnore
    public static /* final */ int ftv_everyone_overview; // = 2131362220;
    @DexIgnore
    public static /* final */ int ftv_failed_desc; // = 2131362221;
    @DexIgnore
    public static /* final */ int ftv_favorite_contact_name; // = 2131362222;
    @DexIgnore
    public static /* final */ int ftv_favorite_contacts; // = 2131362223;
    @DexIgnore
    public static /* final */ int ftv_favorite_contacts_section; // = 2131362224;
    @DexIgnore
    public static /* final */ int ftv_gender; // = 2131362225;
    @DexIgnore
    public static /* final */ int ftv_get_notified; // = 2131362226;
    @DexIgnore
    public static /* final */ int ftv_get_notified_description; // = 2131362227;
    @DexIgnore
    public static /* final */ int ftv_go_to_setting; // = 2131362228;
    @DexIgnore
    public static /* final */ int ftv_goal_title; // = 2131362229;
    @DexIgnore
    public static /* final */ int ftv_goal_type; // = 2131362230;
    @DexIgnore
    public static /* final */ int ftv_goal_value; // = 2131362231;
    @DexIgnore
    public static /* final */ int ftv_goals_unit; // = 2131362232;
    @DexIgnore
    public static /* final */ int ftv_heart_rate_avg; // = 2131362233;
    @DexIgnore
    public static /* final */ int ftv_heart_rate_max; // = 2131362234;
    @DexIgnore
    public static /* final */ int ftv_height; // = 2131362235;
    @DexIgnore
    public static /* final */ int ftv_height_unit; // = 2131362236;
    @DexIgnore
    public static /* final */ int ftv_help; // = 2131362237;
    @DexIgnore
    public static /* final */ int ftv_hrSleepSessionDesc; // = 2131362238;
    @DexIgnore
    public static /* final */ int ftv_hrSleepSessionTitle; // = 2131362239;
    @DexIgnore
    public static /* final */ int ftv_hybrid_feature; // = 2131362240;
    @DexIgnore
    public static /* final */ int ftv_hybrid_name; // = 2131362241;
    @DexIgnore
    public static /* final */ int ftv_hybrid_watch_label; // = 2131362242;
    @DexIgnore
    public static /* final */ int ftv_inactivity_nudge_label; // = 2131362243;
    @DexIgnore
    public static /* final */ int ftv_inactivity_nudge_time_end_label; // = 2131362244;
    @DexIgnore
    public static /* final */ int ftv_inactivity_nudge_time_end_value; // = 2131362245;
    @DexIgnore
    public static /* final */ int ftv_inactivity_nudge_time_start_label; // = 2131362246;
    @DexIgnore
    public static /* final */ int ftv_inactivity_nudge_time_start_value; // = 2131362247;
    @DexIgnore
    public static /* final */ int ftv_indicator; // = 2131362248;
    @DexIgnore
    public static /* final */ int ftv_invalid_code; // = 2131362249;
    @DexIgnore
    public static /* final */ int ftv_keep_going_label; // = 2131362250;
    @DexIgnore
    public static /* final */ int ftv_label_current; // = 2131362251;
    @DexIgnore
    public static /* final */ int ftv_last_location_address; // = 2131362252;
    @DexIgnore
    public static /* final */ int ftv_last_location_city; // = 2131362253;
    @DexIgnore
    public static /* final */ int ftv_last_location_tile; // = 2131362254;
    @DexIgnore
    public static /* final */ int ftv_last_location_time; // = 2131362255;
    @DexIgnore
    public static /* final */ int ftv_locate_on_map; // = 2131362256;
    @DexIgnore
    public static /* final */ int ftv_location_error; // = 2131362257;
    @DexIgnore
    public static /* final */ int ftv_low_battery; // = 2131362258;
    @DexIgnore
    public static /* final */ int ftv_max_unit; // = 2131362259;
    @DexIgnore
    public static /* final */ int ftv_max_value; // = 2131362260;
    @DexIgnore
    public static /* final */ int ftv_messages; // = 2131362261;
    @DexIgnore
    public static /* final */ int ftv_my_apps_overview; // = 2131362262;
    @DexIgnore
    public static /* final */ int ftv_my_contacts_overview; // = 2131362263;
    @DexIgnore
    public static /* final */ int ftv_next; // = 2131362264;
    @DexIgnore
    public static /* final */ int ftv_no_device_title; // = 2131362265;
    @DexIgnore
    public static /* final */ int ftv_no_one; // = 2131362266;
    @DexIgnore
    public static /* final */ int ftv_no_record; // = 2131362267;
    @DexIgnore
    public static /* final */ int ftv_no_time; // = 2131362268;
    @DexIgnore
    public static /* final */ int ftv_no_workout_recorded; // = 2131362269;
    @DexIgnore
    public static /* final */ int ftv_not_receive_email; // = 2131362270;
    @DexIgnore
    public static /* final */ int ftv_note; // = 2131362271;
    @DexIgnore
    public static /* final */ int ftv_notifications_section; // = 2131362272;
    @DexIgnore
    public static /* final */ int ftv_pair_watch; // = 2131362273;
    @DexIgnore
    public static /* final */ int ftv_pair_your_watch; // = 2131362274;
    @DexIgnore
    public static /* final */ int ftv_progress_value; // = 2131362275;
    @DexIgnore
    public static /* final */ int ftv_receive_a_reminder_label; // = 2131362276;
    @DexIgnore
    public static /* final */ int ftv_receive_summary_label; // = 2131362277;
    @DexIgnore
    public static /* final */ int ftv_remind_if_inactive_label; // = 2131362278;
    @DexIgnore
    public static /* final */ int ftv_remind_if_inactive_time_value; // = 2131362279;
    @DexIgnore
    public static /* final */ int ftv_rename; // = 2131362280;
    @DexIgnore
    public static /* final */ int ftv_repeat_label; // = 2131362281;
    @DexIgnore
    public static /* final */ int ftv_repeated_days; // = 2131362282;
    @DexIgnore
    public static /* final */ int ftv_resend_email; // = 2131362283;
    @DexIgnore
    public static /* final */ int ftv_resting_unit; // = 2131362284;
    @DexIgnore
    public static /* final */ int ftv_resting_value; // = 2131362285;
    @DexIgnore
    public static /* final */ int ftv_scheduled; // = 2131362286;
    @DexIgnore
    public static /* final */ int ftv_scheduled_end_label; // = 2131362287;
    @DexIgnore
    public static /* final */ int ftv_scheduled_end_value; // = 2131362288;
    @DexIgnore
    public static /* final */ int ftv_scheduled_start_label; // = 2131362289;
    @DexIgnore
    public static /* final */ int ftv_scheduled_start_value; // = 2131362290;
    @DexIgnore
    public static /* final */ int ftv_select_section; // = 2131362291;
    @DexIgnore
    public static /* final */ int ftv_sent_email; // = 2131362292;
    @DexIgnore
    public static /* final */ int ftv_set_to_watch; // = 2131362293;
    @DexIgnore
    public static /* final */ int ftv_set_up_a_time_label; // = 2131362294;
    @DexIgnore
    public static /* final */ int ftv_sign_up; // = 2131362295;
    @DexIgnore
    public static /* final */ int ftv_sleep_hour_unit; // = 2131362296;
    @DexIgnore
    public static /* final */ int ftv_sleep_minute_unit; // = 2131362297;
    @DexIgnore
    public static /* final */ int ftv_sleep_summary_label; // = 2131362298;
    @DexIgnore
    public static /* final */ int ftv_steps; // = 2131362299;
    @DexIgnore
    public static /* final */ int ftv_subcribe_email; // = 2131362300;
    @DexIgnore
    public static /* final */ int ftv_tab_unit; // = 2131362301;
    @DexIgnore
    public static /* final */ int ftv_tab_value; // = 2131362302;
    @DexIgnore
    public static /* final */ int ftv_tap_to_assign; // = 2131362303;
    @DexIgnore
    public static /* final */ int ftv_terms_service; // = 2131362304;
    @DexIgnore
    public static /* final */ int ftv_text; // = 2131362305;
    @DexIgnore
    public static /* final */ int ftv_time; // = 2131362306;
    @DexIgnore
    public static /* final */ int ftv_title; // = 2131362307;
    @DexIgnore
    public static /* final */ int ftv_title_fail; // = 2131362308;
    @DexIgnore
    public static /* final */ int ftv_title_low_battery; // = 2131362309;
    @DexIgnore
    public static /* final */ int ftv_update; // = 2131362310;
    @DexIgnore
    public static /* final */ int ftv_update_warning; // = 2131362311;
    @DexIgnore
    public static /* final */ int ftv_watch_reminders_overview; // = 2131362312;
    @DexIgnore
    public static /* final */ int ftv_wear_os; // = 2131362313;
    @DexIgnore
    public static /* final */ int ftv_weekly; // = 2131362314;
    @DexIgnore
    public static /* final */ int ftv_weekly_value; // = 2131362315;
    @DexIgnore
    public static /* final */ int ftv_weight; // = 2131362316;
    @DexIgnore
    public static /* final */ int ftv_weight_unit; // = 2131362317;
    @DexIgnore
    public static /* final */ int ftv_workout_time; // = 2131362318;
    @DexIgnore
    public static /* final */ int ftv_workout_title; // = 2131362319;
    @DexIgnore
    public static /* final */ int ftv_workout_value; // = 2131362320;
    @DexIgnore
    public static /* final */ int full_tv; // = 2131362321;
    @DexIgnore
    public static /* final */ int ghost_view; // = 2131362322;
    @DexIgnore
    public static /* final */ int gl_device_info; // = 2131362323;
    @DexIgnore
    public static /* final */ int glide_custom_view_target_tag; // = 2131362324;
    @DexIgnore
    public static /* final */ int gone; // = 2131362325;
    @DexIgnore
    public static /* final */ int group_divider; // = 2131362326;
    @DexIgnore
    public static /* final */ int groups; // = 2131362327;
    @DexIgnore
    public static /* final */ int guideline; // = 2131362328;
    @DexIgnore
    public static /* final */ int guideline1; // = 2131362329;
    @DexIgnore
    public static /* final */ int guideline_complications_holder; // = 2131362330;
    @DexIgnore
    public static /* final */ int height_container; // = 2131362331;
    @DexIgnore
    public static /* final */ int help_section_action_button; // = 2131362332;
    @DexIgnore
    public static /* final */ int help_section_loading_progress; // = 2131362333;
    @DexIgnore
    public static /* final */ int holder; // = 2131362334;
    @DexIgnore
    public static /* final */ int home; // = 2131362335;
    @DexIgnore
    public static /* final */ int homeAsUp; // = 2131362336;
    @DexIgnore
    public static /* final */ int horizontal; // = 2131362337;
    @DexIgnore
    public static /* final */ int horizontal_guide; // = 2131362338;
    @DexIgnore
    public static /* final */ int horizontal_guideline; // = 2131362339;
    @DexIgnore
    public static /* final */ int hrssc; // = 2131362340;
    @DexIgnore
    public static /* final */ int hybrid; // = 2131362341;
    @DexIgnore
    public static /* final */ int ib_back; // = 2131362342;
    @DexIgnore
    public static /* final */ int ib_close; // = 2131362343;
    @DexIgnore
    public static /* final */ int ib_edit_themes; // = 2131362344;
    @DexIgnore
    public static /* final */ int ib_info; // = 2131362345;
    @DexIgnore
    public static /* final */ int ib_remove; // = 2131362346;
    @DexIgnore
    public static /* final */ int icWorkoutOne; // = 2131362347;
    @DexIgnore
    public static /* final */ int icWorkoutTwo; // = 2131362348;
    @DexIgnore
    public static /* final */ int ic_active_time; // = 2131362349;
    @DexIgnore
    public static /* final */ int ic_activity; // = 2131362350;
    @DexIgnore
    public static /* final */ int ic_calorie; // = 2131362351;
    @DexIgnore
    public static /* final */ int ic_goal_tracking; // = 2131362352;
    @DexIgnore
    public static /* final */ int ic_heart_rate; // = 2131362353;
    @DexIgnore
    public static /* final */ int ic_home; // = 2131362354;
    @DexIgnore
    public static /* final */ int ic_launcher_transparent; // = 2131362355;
    @DexIgnore
    public static /* final */ int ic_sleep; // = 2131362356;
    @DexIgnore
    public static /* final */ int ic_view_no_device; // = 2131362357;
    @DexIgnore
    public static /* final */ int ic_work; // = 2131362358;
    @DexIgnore
    public static /* final */ int icon; // = 2131362359;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362360;
    @DexIgnore
    public static /* final */ int icon_only; // = 2131362361;
    @DexIgnore
    public static /* final */ int id_active_time; // = 2131362362;
    @DexIgnore
    public static /* final */ int id_avg_sleep; // = 2131362363;
    @DexIgnore
    public static /* final */ int id_avg_step; // = 2131362364;
    @DexIgnore
    public static /* final */ int id_calories; // = 2131362365;
    @DexIgnore
    public static /* final */ int ifRoom; // = 2131362366;
    @DexIgnore
    public static /* final */ int image; // = 2131362367;
    @DexIgnore
    public static /* final */ int include_layout_troubleshoot; // = 2131362368;
    @DexIgnore
    public static /* final */ int index; // = 2131362369;
    @DexIgnore
    public static /* final */ int indicator; // = 2131362370;
    @DexIgnore
    public static /* final */ int info; // = 2131362371;
    @DexIgnore
    public static /* final */ int inline; // = 2131362372;
    @DexIgnore
    public static /* final */ int input_birthday; // = 2131362373;
    @DexIgnore
    public static /* final */ int input_email; // = 2131362374;
    @DexIgnore
    public static /* final */ int input_first_name; // = 2131362375;
    @DexIgnore
    public static /* final */ int input_last_name; // = 2131362376;
    @DexIgnore
    public static /* final */ int input_new_pass; // = 2131362377;
    @DexIgnore
    public static /* final */ int input_old_pass; // = 2131362378;
    @DexIgnore
    public static /* final */ int input_password; // = 2131362379;
    @DexIgnore
    public static /* final */ int input_repeat_pass; // = 2131362380;
    @DexIgnore
    public static /* final */ int invisible; // = 2131362381;
    @DexIgnore
    public static /* final */ int italic; // = 2131362382;
    @DexIgnore
    public static /* final */ int item_touch_helper_previous_elevation; // = 2131362383;
    @DexIgnore
    public static /* final */ int iv; // = 2131362384;
    @DexIgnore
    public static /* final */ int ivBackground; // = 2131362385;
    @DexIgnore
    public static /* final */ int ivDevice; // = 2131362386;
    @DexIgnore
    public static /* final */ int ivRightButton; // = 2131362387;
    @DexIgnore
    public static /* final */ int ivViewMore; // = 2131362388;
    @DexIgnore
    public static /* final */ int iv_active_time; // = 2131362389;
    @DexIgnore
    public static /* final */ int iv_add; // = 2131362390;
    @DexIgnore
    public static /* final */ int iv_add_device; // = 2131362391;
    @DexIgnore
    public static /* final */ int iv_app_icon; // = 2131362392;
    @DexIgnore
    public static /* final */ int iv_apple; // = 2131362393;
    @DexIgnore
    public static /* final */ int iv_arrival_time; // = 2131362394;
    @DexIgnore
    public static /* final */ int iv_arrow; // = 2131362395;
    @DexIgnore
    public static /* final */ int iv_artwork; // = 2131362396;
    @DexIgnore
    public static /* final */ int iv_avatar; // = 2131362397;
    @DexIgnore
    public static /* final */ int iv_back; // = 2131362398;
    @DexIgnore
    public static /* final */ int iv_back_date; // = 2131362399;
    @DexIgnore
    public static /* final */ int iv_back_to_login; // = 2131362400;
    @DexIgnore
    public static /* final */ int iv_back_to_reset_password; // = 2131362401;
    @DexIgnore
    public static /* final */ int iv_background; // = 2131362402;
    @DexIgnore
    public static /* final */ int iv_background_preview; // = 2131362403;
    @DexIgnore
    public static /* final */ int iv_calls_check; // = 2131362404;
    @DexIgnore
    public static /* final */ int iv_calls_message_check; // = 2131362405;
    @DexIgnore
    public static /* final */ int iv_calories; // = 2131362406;
    @DexIgnore
    public static /* final */ int iv_check; // = 2131362407;
    @DexIgnore
    public static /* final */ int iv_checked_birthday; // = 2131362408;
    @DexIgnore
    public static /* final */ int iv_checked_email; // = 2131362409;
    @DexIgnore
    public static /* final */ int iv_checked_first_name; // = 2131362410;
    @DexIgnore
    public static /* final */ int iv_checked_last_name; // = 2131362411;
    @DexIgnore
    public static /* final */ int iv_checkmark; // = 2131362412;
    @DexIgnore
    public static /* final */ int iv_clear; // = 2131362413;
    @DexIgnore
    public static /* final */ int iv_clear_name; // = 2131362414;
    @DexIgnore
    public static /* final */ int iv_clear_search; // = 2131362415;
    @DexIgnore
    public static /* final */ int iv_close; // = 2131362416;
    @DexIgnore
    public static /* final */ int iv_complication_setting; // = 2131362417;
    @DexIgnore
    public static /* final */ int iv_complications_holder; // = 2131362418;
    @DexIgnore
    public static /* final */ int iv_delete; // = 2131362419;
    @DexIgnore
    public static /* final */ int iv_device; // = 2131362420;
    @DexIgnore
    public static /* final */ int iv_device_image; // = 2131362421;
    @DexIgnore
    public static /* final */ int iv_distance; // = 2131362422;
    @DexIgnore
    public static /* final */ int iv_done; // = 2131362423;
    @DexIgnore
    public static /* final */ int iv_edit_button; // = 2131362424;
    @DexIgnore
    public static /* final */ int iv_edit_profile; // = 2131362425;
    @DexIgnore
    public static /* final */ int iv_email; // = 2131362426;
    @DexIgnore
    public static /* final */ int iv_everyone_check; // = 2131362427;
    @DexIgnore
    public static /* final */ int iv_facebook; // = 2131362428;
    @DexIgnore
    public static /* final */ int iv_failed; // = 2131362429;
    @DexIgnore
    public static /* final */ int iv_favorite_contact_icon; // = 2131362430;
    @DexIgnore
    public static /* final */ int iv_favorite_contacts_check; // = 2131362431;
    @DexIgnore
    public static /* final */ int iv_goal; // = 2131362432;
    @DexIgnore
    public static /* final */ int iv_google; // = 2131362433;
    @DexIgnore
    public static /* final */ int iv_googlefit; // = 2131362434;
    @DexIgnore
    public static /* final */ int iv_header; // = 2131362435;
    @DexIgnore
    public static /* final */ int iv_heart_rate_avg; // = 2131362436;
    @DexIgnore
    public static /* final */ int iv_heart_rate_max; // = 2131362437;
    @DexIgnore
    public static /* final */ int iv_help; // = 2131362438;
    @DexIgnore
    public static /* final */ int iv_hrSleepSessionIcon; // = 2131362439;
    @DexIgnore
    public static /* final */ int iv_hybrid_icon; // = 2131362440;
    @DexIgnore
    public static /* final */ int iv_icon; // = 2131362441;
    @DexIgnore
    public static /* final */ int iv_indicator; // = 2131362442;
    @DexIgnore
    public static /* final */ int iv_jawbone; // = 2131362443;
    @DexIgnore
    public static /* final */ int iv_left; // = 2131362444;
    @DexIgnore
    public static /* final */ int iv_messages_check; // = 2131362445;
    @DexIgnore
    public static /* final */ int iv_network; // = 2131362446;
    @DexIgnore
    public static /* final */ int iv_next_date; // = 2131362447;
    @DexIgnore
    public static /* final */ int iv_no_one_check; // = 2131362448;
    @DexIgnore
    public static /* final */ int iv_no_watch_found; // = 2131362449;
    @DexIgnore
    public static /* final */ int iv_note; // = 2131362450;
    @DexIgnore
    public static /* final */ int iv_permission; // = 2131362451;
    @DexIgnore
    public static /* final */ int iv_remove; // = 2131362452;
    @DexIgnore
    public static /* final */ int iv_repeat; // = 2131362453;
    @DexIgnore
    public static /* final */ int iv_ringphone; // = 2131362454;
    @DexIgnore
    public static /* final */ int iv_ringphone_selected; // = 2131362455;
    @DexIgnore
    public static /* final */ int iv_selected; // = 2131362456;
    @DexIgnore
    public static /* final */ int iv_setting; // = 2131362457;
    @DexIgnore
    public static /* final */ int iv_smartwatch; // = 2131362458;
    @DexIgnore
    public static /* final */ int iv_sport_smartwatch; // = 2131362459;
    @DexIgnore
    public static /* final */ int iv_steps; // = 2131362460;
    @DexIgnore
    public static /* final */ int iv_tab_icon; // = 2131362461;
    @DexIgnore
    public static /* final */ int iv_title; // = 2131362462;
    @DexIgnore
    public static /* final */ int iv_top; // = 2131362463;
    @DexIgnore
    public static /* final */ int iv_travel_time; // = 2131362464;
    @DexIgnore
    public static /* final */ int iv_tutorial; // = 2131362465;
    @DexIgnore
    public static /* final */ int iv_under_amour; // = 2131362466;
    @DexIgnore
    public static /* final */ int iv_user_avatar; // = 2131362467;
    @DexIgnore
    public static /* final */ int iv_user_avatar_edit; // = 2131362468;
    @DexIgnore
    public static /* final */ int iv_watch_hands_background; // = 2131362469;
    @DexIgnore
    public static /* final */ int iv_watch_theme_background; // = 2131362470;
    @DexIgnore
    public static /* final */ int iv_wechat; // = 2131362471;
    @DexIgnore
    public static /* final */ int iv_weibo; // = 2131362472;
    @DexIgnore
    public static /* final */ int iv_workout; // = 2131362473;
    @DexIgnore
    public static /* final */ int iv_x; // = 2131362474;
    @DexIgnore
    public static /* final */ int labeled; // = 2131362475;
    @DexIgnore
    public static /* final */ int large; // = 2131362476;
    @DexIgnore
    public static /* final */ int largeLabel; // = 2131362477;
    @DexIgnore
    public static /* final */ int layout_right; // = 2131362478;
    @DexIgnore
    public static /* final */ int left; // = 2131362479;
    @DexIgnore
    public static /* final */ int light; // = 2131362480;
    @DexIgnore
    public static /* final */ int line; // = 2131362481;
    @DexIgnore
    public static /* final */ int line1; // = 2131362482;
    @DexIgnore
    public static /* final */ int line3; // = 2131362483;
    @DexIgnore
    public static /* final */ int line_bottom; // = 2131362484;
    @DexIgnore
    public static /* final */ int line_center; // = 2131362485;
    @DexIgnore
    public static /* final */ int line_text; // = 2131362486;
    @DexIgnore
    public static /* final */ int line_top; // = 2131362487;
    @DexIgnore
    public static /* final */ int listMode; // = 2131362488;
    @DexIgnore
    public static /* final */ int list_item; // = 2131362489;
    @DexIgnore
    public static /* final */ int llContainer; // = 2131362490;
    @DexIgnore
    public static /* final */ int llWorkout; // = 2131362491;
    @DexIgnore
    public static /* final */ int ll_allow_calls_from; // = 2131362492;
    @DexIgnore
    public static /* final */ int ll_allow_messages_from; // = 2131362493;
    @DexIgnore
    public static /* final */ int ll_apps; // = 2131362494;
    @DexIgnore
    public static /* final */ int ll_assign_notifications; // = 2131362495;
    @DexIgnore
    public static /* final */ int ll_avoid_tolls_container; // = 2131362496;
    @DexIgnore
    public static /* final */ int ll_battery_reinstall; // = 2131362497;
    @DexIgnore
    public static /* final */ int ll_calls_and_messages; // = 2131362498;
    @DexIgnore
    public static /* final */ int ll_cancel; // = 2131362499;
    @DexIgnore
    public static /* final */ int ll_everyone; // = 2131362500;
    @DexIgnore
    public static /* final */ int ll_goals_value; // = 2131362501;
    @DexIgnore
    public static /* final */ int ll_icClose; // = 2131362502;
    @DexIgnore
    public static /* final */ int ll_inactivity_nudge_time_end; // = 2131362503;
    @DexIgnore
    public static /* final */ int ll_inactivity_nudge_time_start; // = 2131362504;
    @DexIgnore
    public static /* final */ int ll_my_apps; // = 2131362505;
    @DexIgnore
    public static /* final */ int ll_my_contacts; // = 2131362506;
    @DexIgnore
    public static /* final */ int ll_ok; // = 2131362507;
    @DexIgnore
    public static /* final */ int ll_open_debug_screen; // = 2131362508;
    @DexIgnore
    public static /* final */ int ll_recent_container; // = 2131362509;
    @DexIgnore
    public static /* final */ int ll_remind_if_inactive_time; // = 2131362510;
    @DexIgnore
    public static /* final */ int ll_scheduled_time_end; // = 2131362511;
    @DexIgnore
    public static /* final */ int ll_scheduled_time_start; // = 2131362512;
    @DexIgnore
    public static /* final */ int ll_settings; // = 2131362513;
    @DexIgnore
    public static /* final */ int ll_sleep_goal_value; // = 2131362514;
    @DexIgnore
    public static /* final */ int ll_text_container; // = 2131362515;
    @DexIgnore
    public static /* final */ int ll_text_container_calls; // = 2131362516;
    @DexIgnore
    public static /* final */ int ll_time_container; // = 2131362517;
    @DexIgnore
    public static /* final */ int ll_view_more; // = 2131362518;
    @DexIgnore
    public static /* final */ int ll_watch_reminders; // = 2131362519;
    @DexIgnore
    public static /* final */ int ln_get_supports; // = 2131362520;
    @DexIgnore
    public static /* final */ int loading_view; // = 2131362521;
    @DexIgnore
    public static /* final */ int logcat_text; // = 2131362522;
    @DexIgnore
    public static /* final */ int lower_case_all; // = 2131362523;
    @DexIgnore
    public static /* final */ int map_pin; // = 2131362524;
    @DexIgnore
    public static /* final */ int masked; // = 2131362525;
    @DexIgnore
    public static /* final */ int media_actions; // = 2131362526;
    @DexIgnore
    public static /* final */ int message; // = 2131362527;
    @DexIgnore
    public static /* final */ int messenger_send_button; // = 2131362528;
    @DexIgnore
    public static /* final */ int middle; // = 2131362529;
    @DexIgnore
    public static /* final */ int mini; // = 2131362530;
    @DexIgnore
    public static /* final */ int month; // = 2131362531;
    @DexIgnore
    public static /* final */ int mtrl_child_content_container; // = 2131362532;
    @DexIgnore
    public static /* final */ int mtrl_internal_children_alpha_tag; // = 2131362533;
    @DexIgnore
    public static /* final */ int multiply; // = 2131362534;
    @DexIgnore
    public static /* final */ int navigation_header_container; // = 2131362535;
    @DexIgnore
    public static /* final */ int never; // = 2131362536;
    @DexIgnore
    public static /* final */ int never_display; // = 2131362537;
    @DexIgnore
    public static /* final */ int next; // = 2131362538;
    @DexIgnore
    public static /* final */ int none; // = 2131362539;
    @DexIgnore
    public static /* final */ int normal; // = 2131362540;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362541;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362542;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362543;
    @DexIgnore
    public static /* final */ int np__decrement; // = 2131362544;
    @DexIgnore
    public static /* final */ int np__increment; // = 2131362545;
    @DexIgnore
    public static /* final */ int np__numberpicker_input; // = 2131362546;
    @DexIgnore
    public static /* final */ int np_day; // = 2131362547;
    @DexIgnore
    public static /* final */ int np_hour; // = 2131362548;
    @DexIgnore
    public static /* final */ int np_minute; // = 2131362549;
    @DexIgnore
    public static /* final */ int np_month; // = 2131362550;
    @DexIgnore
    public static /* final */ int np_suffix; // = 2131362551;
    @DexIgnore
    public static /* final */ int np_year; // = 2131362552;
    @DexIgnore
    public static /* final */ int nsdv; // = 2131362553;
    @DexIgnore
    public static /* final */ int nsv_low_battery; // = 2131362554;
    @DexIgnore
    public static /* final */ int numberPicker; // = 2131362555;
    @DexIgnore
    public static /* final */ int numberPickerOne; // = 2131362556;
    @DexIgnore
    public static /* final */ int numberPickerThree; // = 2131362557;
    @DexIgnore
    public static /* final */ int numberPickerTwo; // = 2131362558;
    @DexIgnore
    public static /* final */ int onAttachStateChangeListener; // = 2131362559;
    @DexIgnore
    public static /* final */ int onDateChanged; // = 2131362560;
    @DexIgnore
    public static /* final */ int one_circle; // = 2131362561;
    @DexIgnore
    public static /* final */ int open_debug_screen; // = 2131362562;
    @DexIgnore
    public static /* final */ int open_graph; // = 2131362563;
    @DexIgnore
    public static /* final */ int outline; // = 2131362564;
    @DexIgnore
    public static /* final */ int packed; // = 2131362565;
    @DexIgnore
    public static /* final */ int padding; // = 2131362566;
    @DexIgnore
    public static /* final */ int page; // = 2131362567;
    @DexIgnore
    public static /* final */ int parallax; // = 2131362568;
    @DexIgnore
    public static /* final */ int parent; // = 2131362569;
    @DexIgnore
    public static /* final */ int parentPanel; // = 2131362570;
    @DexIgnore
    public static /* final */ int parent_matrix; // = 2131362571;
    @DexIgnore
    public static /* final */ int pbProgress; // = 2131362572;
    @DexIgnore
    public static /* final */ int pbSleep; // = 2131362573;
    @DexIgnore
    public static /* final */ int pbSteps; // = 2131362574;
    @DexIgnore
    public static /* final */ int pb_goal; // = 2131362575;
    @DexIgnore
    public static /* final */ int pb_ota; // = 2131362576;
    @DexIgnore
    public static /* final */ int pb_progress; // = 2131362577;
    @DexIgnore
    public static /* final */ int percent; // = 2131362578;
    @DexIgnore
    public static /* final */ int pick_contact_phone; // = 2131362579;
    @DexIgnore
    public static /* final */ int pick_contact_title; // = 2131362580;
    @DexIgnore
    public static /* final */ int pick_contact_title_calls; // = 2131362581;
    @DexIgnore
    public static /* final */ int pin; // = 2131362582;
    @DexIgnore
    public static /* final */ int places_autocomplete_action_bar; // = 2131362583;
    @DexIgnore
    public static /* final */ int places_autocomplete_back_button; // = 2131362584;
    @DexIgnore
    public static /* final */ int places_autocomplete_clear_button; // = 2131362585;
    @DexIgnore
    public static /* final */ int places_autocomplete_edit_text; // = 2131362586;
    @DexIgnore
    public static /* final */ int places_autocomplete_error; // = 2131362587;
    @DexIgnore
    public static /* final */ int places_autocomplete_error_message; // = 2131362588;
    @DexIgnore
    public static /* final */ int places_autocomplete_error_progress; // = 2131362589;
    @DexIgnore
    public static /* final */ int places_autocomplete_list; // = 2131362590;
    @DexIgnore
    public static /* final */ int places_autocomplete_overlay_content; // = 2131362591;
    @DexIgnore
    public static /* final */ int places_autocomplete_overlay_root; // = 2131362592;
    @DexIgnore
    public static /* final */ int places_autocomplete_powered_by_google; // = 2131362593;
    @DexIgnore
    public static /* final */ int places_autocomplete_prediction_primary_text; // = 2131362594;
    @DexIgnore
    public static /* final */ int places_autocomplete_prediction_secondary_text; // = 2131362595;
    @DexIgnore
    public static /* final */ int places_autocomplete_progress; // = 2131362596;
    @DexIgnore
    public static /* final */ int places_autocomplete_search_button; // = 2131362597;
    @DexIgnore
    public static /* final */ int places_autocomplete_search_input; // = 2131362598;
    @DexIgnore
    public static /* final */ int places_autocomplete_separator; // = 2131362599;
    @DexIgnore
    public static /* final */ int places_autocomplete_try_again; // = 2131362600;
    @DexIgnore
    public static /* final */ int popupAction; // = 2131362601;
    @DexIgnore
    public static /* final */ int popup_root; // = 2131362602;
    @DexIgnore
    public static /* final */ int prev; // = 2131362603;
    @DexIgnore
    public static /* final */ int primary_tv; // = 2131362604;
    @DexIgnore
    public static /* final */ int profileFragment; // = 2131362605;
    @DexIgnore
    public static /* final */ int progressBar; // = 2131362606;
    @DexIgnore
    public static /* final */ int progress_bar; // = 2131362607;
    @DexIgnore
    public static /* final */ int progress_circular; // = 2131362608;
    @DexIgnore
    public static /* final */ int progress_horizontal; // = 2131362609;
    @DexIgnore
    public static /* final */ int progress_update; // = 2131362610;
    @DexIgnore
    public static /* final */ int radio; // = 2131362611;
    @DexIgnore
    public static /* final */ int recycler_view; // = 2131362612;
    @DexIgnore
    public static /* final */ int request_list_fragment_progress; // = 2131362613;
    @DexIgnore
    public static /* final */ int retry_view_button; // = 2131362614;
    @DexIgnore
    public static /* final */ int retry_view_container; // = 2131362615;
    @DexIgnore
    public static /* final */ int retry_view_text; // = 2131362616;
    @DexIgnore
    public static /* final */ int right; // = 2131362617;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362618;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362619;
    @DexIgnore
    public static /* final */ int rlContent; // = 2131362620;
    @DexIgnore
    public static /* final */ int rlSleep; // = 2131362621;
    @DexIgnore
    public static /* final */ int rlSteps; // = 2131362622;
    @DexIgnore
    public static /* final */ int rl_actions; // = 2131362623;
    @DexIgnore
    public static /* final */ int rl_wear_os_group; // = 2131362624;
    @DexIgnore
    public static /* final */ int rma_dialog_root; // = 2131362625;
    @DexIgnore
    public static /* final */ int rma_dont_ask_again; // = 2131362626;
    @DexIgnore
    public static /* final */ int rma_feedback_button; // = 2131362627;
    @DexIgnore
    public static /* final */ int rma_store_button; // = 2131362628;
    @DexIgnore
    public static /* final */ int root; // = 2131362629;
    @DexIgnore
    public static /* final */ int row_request_container; // = 2131362630;
    @DexIgnore
    public static /* final */ int row_request_date; // = 2131362631;
    @DexIgnore
    public static /* final */ int row_request_description; // = 2131362632;
    @DexIgnore
    public static /* final */ int row_request_unread_indicator; // = 2131362633;
    @DexIgnore
    public static /* final */ int rpb_big; // = 2131362634;
    @DexIgnore
    public static /* final */ int rpb_biggest; // = 2131362635;
    @DexIgnore
    public static /* final */ int rpb_medium; // = 2131362636;
    @DexIgnore
    public static /* final */ int rpb_smallest; // = 2131362637;
    @DexIgnore
    public static /* final */ int rvAddresses; // = 2131362638;
    @DexIgnore
    public static /* final */ int rvOverview; // = 2131362639;
    @DexIgnore
    public static /* final */ int rvWorkout; // = 2131362640;
    @DexIgnore
    public static /* final */ int rv_activities; // = 2131362641;
    @DexIgnore
    public static /* final */ int rv_alarms; // = 2131362642;
    @DexIgnore
    public static /* final */ int rv_apps; // = 2131362643;
    @DexIgnore
    public static /* final */ int rv_assign; // = 2131362644;
    @DexIgnore
    public static /* final */ int rv_background; // = 2131362645;
    @DexIgnore
    public static /* final */ int rv_categories; // = 2131362646;
    @DexIgnore
    public static /* final */ int rv_complications; // = 2131362647;
    @DexIgnore
    public static /* final */ int rv_contacts; // = 2131362648;
    @DexIgnore
    public static /* final */ int rv_debug_parent; // = 2131362649;
    @DexIgnore
    public static /* final */ int rv_devices; // = 2131362650;
    @DexIgnore
    public static /* final */ int rv_favorite_contacts; // = 2131362651;
    @DexIgnore
    public static /* final */ int rv_goal_tracking; // = 2131362652;
    @DexIgnore
    public static /* final */ int rv_heart_rate_sleep; // = 2131362653;
    @DexIgnore
    public static /* final */ int rv_heart_rates; // = 2131362654;
    @DexIgnore
    public static /* final */ int rv_list_device; // = 2131362655;
    @DexIgnore
    public static /* final */ int rv_micro_app; // = 2131362656;
    @DexIgnore
    public static /* final */ int rv_notification_apps; // = 2131362657;
    @DexIgnore
    public static /* final */ int rv_permissions; // = 2131362658;
    @DexIgnore
    public static /* final */ int rv_preset; // = 2131362659;
    @DexIgnore
    public static /* final */ int rv_recent_addresses; // = 2131362660;
    @DexIgnore
    public static /* final */ int rv_recorded_goal_tracking; // = 2131362661;
    @DexIgnore
    public static /* final */ int rv_results; // = 2131362662;
    @DexIgnore
    public static /* final */ int rv_ringphones; // = 2131362663;
    @DexIgnore
    public static /* final */ int rv_sleeps; // = 2131362664;
    @DexIgnore
    public static /* final */ int rv_tabs; // = 2131362665;
    @DexIgnore
    public static /* final */ int rv_watch_apps; // = 2131362666;
    @DexIgnore
    public static /* final */ int rvai_timezone; // = 2131362667;
    @DexIgnore
    public static /* final */ int rvp_height; // = 2131362668;
    @DexIgnore
    public static /* final */ int rvp_tutorial; // = 2131362669;
    @DexIgnore
    public static /* final */ int rvp_weight; // = 2131362670;
    @DexIgnore
    public static /* final */ int satellite; // = 2131362671;
    @DexIgnore
    public static /* final */ int save; // = 2131362672;
    @DexIgnore
    public static /* final */ int save_image_matrix; // = 2131362673;
    @DexIgnore
    public static /* final */ int save_non_transition_alpha; // = 2131362674;
    @DexIgnore
    public static /* final */ int save_scale_type; // = 2131362675;
    @DexIgnore
    public static /* final */ int sc_avoid_tolls; // = 2131362676;
    @DexIgnore
    public static /* final */ int sc_content; // = 2131362677;
    @DexIgnore
    public static /* final */ int sc_debug_child_with_switch; // = 2131362678;
    @DexIgnore
    public static /* final */ int sc_subcribe_email; // = 2131362679;
    @DexIgnore
    public static /* final */ int scan_device_container; // = 2131362680;
    @DexIgnore
    public static /* final */ int screen; // = 2131362681;
    @DexIgnore
    public static /* final */ int scroll; // = 2131362682;
    @DexIgnore
    public static /* final */ int scrollIndicatorDown; // = 2131362683;
    @DexIgnore
    public static /* final */ int scrollIndicatorUp; // = 2131362684;
    @DexIgnore
    public static /* final */ int scrollView; // = 2131362685;
    @DexIgnore
    public static /* final */ int scroll_layout; // = 2131362686;
    @DexIgnore
    public static /* final */ int scroll_unit_layout; // = 2131362687;
    @DexIgnore
    public static /* final */ int scrollable; // = 2131362688;
    @DexIgnore
    public static /* final */ int search_badge; // = 2131362689;
    @DexIgnore
    public static /* final */ int search_bar; // = 2131362690;
    @DexIgnore
    public static /* final */ int search_button; // = 2131362691;
    @DexIgnore
    public static /* final */ int search_close_btn; // = 2131362692;
    @DexIgnore
    public static /* final */ int search_edit_frame; // = 2131362693;
    @DexIgnore
    public static /* final */ int search_go_btn; // = 2131362694;
    @DexIgnore
    public static /* final */ int search_mag_icon; // = 2131362695;
    @DexIgnore
    public static /* final */ int search_plate; // = 2131362696;
    @DexIgnore
    public static /* final */ int search_src_text; // = 2131362697;
    @DexIgnore
    public static /* final */ int search_time_zone; // = 2131362698;
    @DexIgnore
    public static /* final */ int search_voice_btn; // = 2131362699;
    @DexIgnore
    public static /* final */ int secondary_tv; // = 2131362700;
    @DexIgnore
    public static /* final */ int section_group; // = 2131362701;
    @DexIgnore
    public static /* final */ int section_title; // = 2131362702;
    @DexIgnore
    public static /* final */ int select_dialog_listview; // = 2131362703;
    @DexIgnore
    public static /* final */ int selected; // = 2131362704;
    @DexIgnore
    public static /* final */ int send_beta_feedback; // = 2131362705;
    @DexIgnore
    public static /* final */ int send_fw_log; // = 2131362706;
    @DexIgnore
    public static /* final */ int send_general_feedback; // = 2131362707;
    @DexIgnore
    public static /* final */ int send_sw_feedback; // = 2131362708;
    @DexIgnore
    public static /* final */ int send_sw_question_feedback; // = 2131362709;
    @DexIgnore
    public static /* final */ int send_uat_feedback; // = 2131362710;
    @DexIgnore
    public static /* final */ int separatedLine; // = 2131362711;
    @DexIgnore
    public static /* final */ int shortcut; // = 2131362712;
    @DexIgnore
    public static /* final */ int showCustom; // = 2131362713;
    @DexIgnore
    public static /* final */ int showHome; // = 2131362714;
    @DexIgnore
    public static /* final */ int showTitle; // = 2131362715;
    @DexIgnore
    public static /* final */ int sleep_chart; // = 2131362716;
    @DexIgnore
    public static /* final */ int small; // = 2131362717;
    @DexIgnore
    public static /* final */ int smallLabel; // = 2131362718;
    @DexIgnore
    public static /* final */ int snackbar_action; // = 2131362719;
    @DexIgnore
    public static /* final */ int snackbar_text; // = 2131362720;
    @DexIgnore
    public static /* final */ int snap; // = 2131362721;
    @DexIgnore
    public static /* final */ int snapMargins; // = 2131362722;
    @DexIgnore
    public static /* final */ int sp_debug_child_with_spinner; // = 2131362723;
    @DexIgnore
    public static /* final */ int space_1; // = 2131362724;
    @DexIgnore
    public static /* final */ int space_2; // = 2131362725;
    @DexIgnore
    public static /* final */ int space_3; // = 2131362726;
    @DexIgnore
    public static /* final */ int spacer; // = 2131362727;
    @DexIgnore
    public static /* final */ int spinner_filter; // = 2131362728;
    @DexIgnore
    public static /* final */ int split_action_bar; // = 2131362729;
    @DexIgnore
    public static /* final */ int spread; // = 2131362730;
    @DexIgnore
    public static /* final */ int spread_inside; // = 2131362731;
    @DexIgnore
    public static /* final */ int src_atop; // = 2131362732;
    @DexIgnore
    public static /* final */ int src_in; // = 2131362733;
    @DexIgnore
    public static /* final */ int src_over; // = 2131362734;
    @DexIgnore
    public static /* final */ int srlPullToSync; // = 2131362735;
    @DexIgnore
    public static /* final */ int standard; // = 2131362736;
    @DexIgnore
    public static /* final */ int start; // = 2131362737;
    @DexIgnore
    public static /* final */ int status_bar_latest_event_content; // = 2131362738;
    @DexIgnore
    public static /* final */ int stretch; // = 2131362739;
    @DexIgnore
    public static /* final */ int submenuarrow; // = 2131362740;
    @DexIgnore
    public static /* final */ int submit_area; // = 2131362741;
    @DexIgnore
    public static /* final */ int subtitle; // = 2131362742;
    @DexIgnore
    public static /* final */ int svLogin; // = 2131362743;
    @DexIgnore
    public static /* final */ int sv_container; // = 2131362744;
    @DexIgnore
    public static /* final */ int sv_content; // = 2131362745;
    @DexIgnore
    public static /* final */ int sv_goals_edit; // = 2131362746;
    @DexIgnore
    public static /* final */ int sw_all_apps_enabled; // = 2131362747;
    @DexIgnore
    public static /* final */ int sw_bedtime_reminder; // = 2131362748;
    @DexIgnore
    public static /* final */ int sw_enabled; // = 2131362749;
    @DexIgnore
    public static /* final */ int sw_inactivity_nudge; // = 2131362750;
    @DexIgnore
    public static /* final */ int sw_keep_going; // = 2131362751;
    @DexIgnore
    public static /* final */ int sw_locate; // = 2131362752;
    @DexIgnore
    public static /* final */ int sw_repeat; // = 2131362753;
    @DexIgnore
    public static /* final */ int sw_scheduled; // = 2131362754;
    @DexIgnore
    public static /* final */ int sw_sleep_summary; // = 2131362755;
    @DexIgnore
    public static /* final */ int switch_ring; // = 2131362756;
    @DexIgnore
    public static /* final */ int sync_progress; // = 2131362757;
    @DexIgnore
    public static /* final */ int tabMode; // = 2131362758;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131362759;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131362760;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131362761;
    @DexIgnore
    public static /* final */ int tag_widget_control_transition_properties; // = 2131362762;
    @DexIgnore
    public static /* final */ int temperature_container; // = 2131362763;
    @DexIgnore
    public static /* final */ int terrain; // = 2131362764;
    @DexIgnore
    public static /* final */ int text; // = 2131362765;
    @DexIgnore
    public static /* final */ int text2; // = 2131362766;
    @DexIgnore
    public static /* final */ int textSpacerNoButtons; // = 2131362767;
    @DexIgnore
    public static /* final */ int textSpacerNoTitle; // = 2131362768;
    @DexIgnore
    public static /* final */ int textStart; // = 2131362769;
    @DexIgnore
    public static /* final */ int textWatcher; // = 2131362770;
    @DexIgnore
    public static /* final */ int text_input_password_toggle; // = 2131362771;
    @DexIgnore
    public static /* final */ int textinput_counter; // = 2131362772;
    @DexIgnore
    public static /* final */ int textinput_error; // = 2131362773;
    @DexIgnore
    public static /* final */ int textinput_helper_text; // = 2131362774;
    @DexIgnore
    public static /* final */ int thrc; // = 2131362775;
    @DexIgnore
    public static /* final */ int three_circle; // = 2131362776;
    @DexIgnore
    public static /* final */ int time; // = 2131362777;
    @DexIgnore
    public static /* final */ int timezone_recycler_view; // = 2131362778;
    @DexIgnore
    public static /* final */ int title; // = 2131362779;
    @DexIgnore
    public static /* final */ int titleDividerNoCustom; // = 2131362780;
    @DexIgnore
    public static /* final */ int title_share_data; // = 2131362781;
    @DexIgnore
    public static /* final */ int title_template; // = 2131362782;
    @DexIgnore
    public static /* final */ int tl_distance_unit; // = 2131362783;
    @DexIgnore
    public static /* final */ int tl_height_unit; // = 2131362784;
    @DexIgnore
    public static /* final */ int tl_temperature_unit; // = 2131362785;
    @DexIgnore
    public static /* final */ int tl_weight_unit; // = 2131362786;
    @DexIgnore
    public static /* final */ int top; // = 2131362787;
    @DexIgnore
    public static /* final */ int topPanel; // = 2131362788;
    @DexIgnore
    public static /* final */ int touch_outside; // = 2131362789;
    @DexIgnore
    public static /* final */ int transition_current_scene; // = 2131362790;
    @DexIgnore
    public static /* final */ int transition_layout_save; // = 2131362791;
    @DexIgnore
    public static /* final */ int transition_position; // = 2131362792;
    @DexIgnore
    public static /* final */ int transition_scene_layoutid_cache; // = 2131362793;
    @DexIgnore
    public static /* final */ int transition_transform; // = 2131362794;
    @DexIgnore
    public static /* final */ int tvAsleep; // = 2131362795;
    @DexIgnore
    public static /* final */ int tvAsleepUnit; // = 2131362796;
    @DexIgnore
    public static /* final */ int tvAwake; // = 2131362797;
    @DexIgnore
    public static /* final */ int tvAwakeUnit; // = 2131362798;
    @DexIgnore
    public static /* final */ int tvCalories; // = 2131362799;
    @DexIgnore
    public static /* final */ int tvCaloriesUnit; // = 2131362800;
    @DexIgnore
    public static /* final */ int tvContent; // = 2131362801;
    @DexIgnore
    public static /* final */ int tvDistance; // = 2131362802;
    @DexIgnore
    public static /* final */ int tvDistanceUnit; // = 2131362803;
    @DexIgnore
    public static /* final */ int tvGoToCustomize; // = 2131362804;
    @DexIgnore
    public static /* final */ int tvLetter; // = 2131362805;
    @DexIgnore
    public static /* final */ int tvSleep; // = 2131362806;
    @DexIgnore
    public static /* final */ int tvSleepGoal; // = 2131362807;
    @DexIgnore
    public static /* final */ int tvSteps; // = 2131362808;
    @DexIgnore
    public static /* final */ int tvStepsGoal; // = 2131362809;
    @DexIgnore
    public static /* final */ int tvTracking1; // = 2131362810;
    @DexIgnore
    public static /* final */ int tvTracking2; // = 2131362811;
    @DexIgnore
    public static /* final */ int tvTrackingTitle; // = 2131362812;
    @DexIgnore
    public static /* final */ int tv_access_token_expired_at; // = 2131362813;
    @DexIgnore
    public static /* final */ int tv_access_token_expired_at_label; // = 2131362814;
    @DexIgnore
    public static /* final */ int tv_added; // = 2131362815;
    @DexIgnore
    public static /* final */ int tv_allow; // = 2131362816;
    @DexIgnore
    public static /* final */ int tv_app_version; // = 2131362817;
    @DexIgnore
    public static /* final */ int tv_assigned_to; // = 2131362818;
    @DexIgnore
    public static /* final */ int tv_avg_active_time; // = 2131362819;
    @DexIgnore
    public static /* final */ int tv_avg_active_time_date; // = 2131362820;
    @DexIgnore
    public static /* final */ int tv_avg_active_time_desc; // = 2131362821;
    @DexIgnore
    public static /* final */ int tv_avg_activity; // = 2131362822;
    @DexIgnore
    public static /* final */ int tv_avg_activity_date; // = 2131362823;
    @DexIgnore
    public static /* final */ int tv_avg_activity_desc; // = 2131362824;
    @DexIgnore
    public static /* final */ int tv_avg_calories; // = 2131362825;
    @DexIgnore
    public static /* final */ int tv_avg_calories_date; // = 2131362826;
    @DexIgnore
    public static /* final */ int tv_avg_calories_desc; // = 2131362827;
    @DexIgnore
    public static /* final */ int tv_avg_sleep; // = 2131362828;
    @DexIgnore
    public static /* final */ int tv_avg_sleep_date; // = 2131362829;
    @DexIgnore
    public static /* final */ int tv_avg_sleep_desc; // = 2131362830;
    @DexIgnore
    public static /* final */ int tv_awake; // = 2131362831;
    @DexIgnore
    public static /* final */ int tv_awake_hour; // = 2131362832;
    @DexIgnore
    public static /* final */ int tv_awake_hour_unit; // = 2131362833;
    @DexIgnore
    public static /* final */ int tv_awake_min; // = 2131362834;
    @DexIgnore
    public static /* final */ int tv_awake_min_unit; // = 2131362835;
    @DexIgnore
    public static /* final */ int tv_bottom; // = 2131362836;
    @DexIgnore
    public static /* final */ int tv_calibration; // = 2131362837;
    @DexIgnore
    public static /* final */ int tv_calibration_disconnected; // = 2131362838;
    @DexIgnore
    public static /* final */ int tv_call_us; // = 2131362839;
    @DexIgnore
    public static /* final */ int tv_cancel; // = 2131362840;
    @DexIgnore
    public static /* final */ int tv_change_watch_face; // = 2131362841;
    @DexIgnore
    public static /* final */ int tv_complication_detail; // = 2131362842;
    @DexIgnore
    public static /* final */ int tv_complication_name; // = 2131362843;
    @DexIgnore
    public static /* final */ int tv_complication_permission; // = 2131362844;
    @DexIgnore
    public static /* final */ int tv_complication_setting; // = 2131362845;
    @DexIgnore
    public static /* final */ int tv_connection_status; // = 2131362846;
    @DexIgnore
    public static /* final */ int tv_contacts; // = 2131362847;
    @DexIgnore
    public static /* final */ int tv_create_new_preset; // = 2131362848;
    @DexIgnore
    public static /* final */ int tv_debug_child; // = 2131362849;
    @DexIgnore
    public static /* final */ int tv_debug_child_with_switch; // = 2131362850;
    @DexIgnore
    public static /* final */ int tv_debug_child_with_text; // = 2131362851;
    @DexIgnore
    public static /* final */ int tv_debug_child_with_text_text; // = 2131362852;
    @DexIgnore
    public static /* final */ int tv_debug_parent; // = 2131362853;
    @DexIgnore
    public static /* final */ int tv_debug_title; // = 2131362854;
    @DexIgnore
    public static /* final */ int tv_deep; // = 2131362855;
    @DexIgnore
    public static /* final */ int tv_deep_hour; // = 2131362856;
    @DexIgnore
    public static /* final */ int tv_deep_hour_unit; // = 2131362857;
    @DexIgnore
    public static /* final */ int tv_deep_min; // = 2131362858;
    @DexIgnore
    public static /* final */ int tv_deep_min_unit; // = 2131362859;
    @DexIgnore
    public static /* final */ int tv_delete_account; // = 2131362860;
    @DexIgnore
    public static /* final */ int tv_desc; // = 2131362861;
    @DexIgnore
    public static /* final */ int tv_description; // = 2131362862;
    @DexIgnore
    public static /* final */ int tv_device; // = 2131362863;
    @DexIgnore
    public static /* final */ int tv_device_battery; // = 2131362864;
    @DexIgnore
    public static /* final */ int tv_device_battery_label; // = 2131362865;
    @DexIgnore
    public static /* final */ int tv_device_bottom_button; // = 2131362866;
    @DexIgnore
    public static /* final */ int tv_device_bottom_button_label; // = 2131362867;
    @DexIgnore
    public static /* final */ int tv_device_firmware; // = 2131362868;
    @DexIgnore
    public static /* final */ int tv_device_firmware_label; // = 2131362869;
    @DexIgnore
    public static /* final */ int tv_device_middle_button; // = 2131362870;
    @DexIgnore
    public static /* final */ int tv_device_middle_button_label; // = 2131362871;
    @DexIgnore
    public static /* final */ int tv_device_name; // = 2131362872;
    @DexIgnore
    public static /* final */ int tv_device_rssi; // = 2131362873;
    @DexIgnore
    public static /* final */ int tv_device_rssi_label; // = 2131362874;
    @DexIgnore
    public static /* final */ int tv_device_serial; // = 2131362875;
    @DexIgnore
    public static /* final */ int tv_device_serial_label; // = 2131362876;
    @DexIgnore
    public static /* final */ int tv_device_top_button; // = 2131362877;
    @DexIgnore
    public static /* final */ int tv_device_top_button_label; // = 2131362878;
    @DexIgnore
    public static /* final */ int tv_dob; // = 2131362879;
    @DexIgnore
    public static /* final */ int tv_email; // = 2131362880;
    @DexIgnore
    public static /* final */ int tv_end; // = 2131362881;
    @DexIgnore
    public static /* final */ int tv_error_check_character; // = 2131362882;
    @DexIgnore
    public static /* final */ int tv_error_check_combine; // = 2131362883;
    @DexIgnore
    public static /* final */ int tv_find_device; // = 2131362884;
    @DexIgnore
    public static /* final */ int tv_forgot; // = 2131362885;
    @DexIgnore
    public static /* final */ int tv_fw_version; // = 2131362886;
    @DexIgnore
    public static /* final */ int tv_fw_version_value; // = 2131362887;
    @DexIgnore
    public static /* final */ int tv_googlefit; // = 2131362888;
    @DexIgnore
    public static /* final */ int tv_have_account; // = 2131362889;
    @DexIgnore
    public static /* final */ int tv_height_weight; // = 2131362890;
    @DexIgnore
    public static /* final */ int tv_hour; // = 2131362891;
    @DexIgnore
    public static /* final */ int tv_hour_unit; // = 2131362892;
    @DexIgnore
    public static /* final */ int tv_jawbone; // = 2131362893;
    @DexIgnore
    public static /* final */ int tv_last_sync; // = 2131362894;
    @DexIgnore
    public static /* final */ int tv_last_sync_value; // = 2131362895;
    @DexIgnore
    public static /* final */ int tv_last_synced_time; // = 2131362896;
    @DexIgnore
    public static /* final */ int tv_left; // = 2131362897;
    @DexIgnore
    public static /* final */ int tv_light; // = 2131362898;
    @DexIgnore
    public static /* final */ int tv_light_hour; // = 2131362899;
    @DexIgnore
    public static /* final */ int tv_light_hour_unit; // = 2131362900;
    @DexIgnore
    public static /* final */ int tv_light_min; // = 2131362901;
    @DexIgnore
    public static /* final */ int tv_light_min_unit; // = 2131362902;
    @DexIgnore
    public static /* final */ int tv_live_chat; // = 2131362903;
    @DexIgnore
    public static /* final */ int tv_location_status; // = 2131362904;
    @DexIgnore
    public static /* final */ int tv_logcat_item; // = 2131362905;
    @DexIgnore
    public static /* final */ int tv_login; // = 2131362906;
    @DexIgnore
    public static /* final */ int tv_logout; // = 2131362907;
    @DexIgnore
    public static /* final */ int tv_member_since; // = 2131362908;
    @DexIgnore
    public static /* final */ int tv_micro_app_detail; // = 2131362909;
    @DexIgnore
    public static /* final */ int tv_micro_app_permission; // = 2131362910;
    @DexIgnore
    public static /* final */ int tv_micro_app_setting; // = 2131362911;
    @DexIgnore
    public static /* final */ int tv_min; // = 2131362912;
    @DexIgnore
    public static /* final */ int tv_min_unit; // = 2131362913;
    @DexIgnore
    public static /* final */ int tv_name; // = 2131362914;
    @DexIgnore
    public static /* final */ int tv_no_record; // = 2131362915;
    @DexIgnore
    public static /* final */ int tv_not_found; // = 2131362916;
    @DexIgnore
    public static /* final */ int tv_not_now; // = 2131362917;
    @DexIgnore
    public static /* final */ int tv_ok; // = 2131362918;
    @DexIgnore
    public static /* final */ int tv_permission_order; // = 2131362919;
    @DexIgnore
    public static /* final */ int tv_preset_name; // = 2131362920;
    @DexIgnore
    public static /* final */ int tv_range; // = 2131362921;
    @DexIgnore
    public static /* final */ int tv_recent; // = 2131362922;
    @DexIgnore
    public static /* final */ int tv_record_breaker; // = 2131362923;
    @DexIgnore
    public static /* final */ int tv_remove_device; // = 2131362924;
    @DexIgnore
    public static /* final */ int tv_right; // = 2131362925;
    @DexIgnore
    public static /* final */ int tv_ring_phone; // = 2131362926;
    @DexIgnore
    public static /* final */ int tv_second_timezone; // = 2131362927;
    @DexIgnore
    public static /* final */ int tv_selected_complication; // = 2131362928;
    @DexIgnore
    public static /* final */ int tv_selected_micro_app; // = 2131362929;
    @DexIgnore
    public static /* final */ int tv_selected_watchapps; // = 2131362930;
    @DexIgnore
    public static /* final */ int tv_serial; // = 2131362931;
    @DexIgnore
    public static /* final */ int tv_serial_value; // = 2131362932;
    @DexIgnore
    public static /* final */ int tv_setting; // = 2131362933;
    @DexIgnore
    public static /* final */ int tv_signup; // = 2131362934;
    @DexIgnore
    public static /* final */ int tv_signup_via; // = 2131362935;
    @DexIgnore
    public static /* final */ int tv_skip; // = 2131362936;
    @DexIgnore
    public static /* final */ int tv_status; // = 2131362937;
    @DexIgnore
    public static /* final */ int tv_tap_icon_to_customize; // = 2131362938;
    @DexIgnore
    public static /* final */ int tv_text; // = 2131362939;
    @DexIgnore
    public static /* final */ int tv_title; // = 2131362940;
    @DexIgnore
    public static /* final */ int tv_title_debug_child_with_spinner; // = 2131362941;
    @DexIgnore
    public static /* final */ int tv_today; // = 2131362942;
    @DexIgnore
    public static /* final */ int tv_top; // = 2131362943;
    @DexIgnore
    public static /* final */ int tv_under_amour; // = 2131362944;
    @DexIgnore
    public static /* final */ int tv_update_fw_detail; // = 2131362945;
    @DexIgnore
    public static /* final */ int tv_update_fw_title; // = 2131362946;
    @DexIgnore
    public static /* final */ int tv_user_name; // = 2131362947;
    @DexIgnore
    public static /* final */ int tv_value; // = 2131362948;
    @DexIgnore
    public static /* final */ int tv_version_name; // = 2131362949;
    @DexIgnore
    public static /* final */ int tv_vibration; // = 2131362950;
    @DexIgnore
    public static /* final */ int tv_wa_bottom; // = 2131362951;
    @DexIgnore
    public static /* final */ int tv_wa_middle; // = 2131362952;
    @DexIgnore
    public static /* final */ int tv_wa_top; // = 2131362953;
    @DexIgnore
    public static /* final */ int tv_watch_app_name; // = 2131362954;
    @DexIgnore
    public static /* final */ int tv_watchapps_detail; // = 2131362955;
    @DexIgnore
    public static /* final */ int tv_watchapps_permission; // = 2131362956;
    @DexIgnore
    public static /* final */ int tv_watchapps_setting; // = 2131362957;
    @DexIgnore
    public static /* final */ int two_circle; // = 2131362958;
    @DexIgnore
    public static /* final */ int type_chaps; // = 2131362959;
    @DexIgnore
    public static /* final */ int type_ea; // = 2131362960;
    @DexIgnore
    public static /* final */ int type_kate_spade; // = 2131362961;
    @DexIgnore
    public static /* final */ int type_mk; // = 2131362962;
    @DexIgnore
    public static /* final */ int type_white_label; // = 2131362963;
    @DexIgnore
    public static /* final */ int uniform; // = 2131362964;
    @DexIgnore
    public static /* final */ int unknown; // = 2131362965;
    @DexIgnore
    public static /* final */ int unlabeled; // = 2131362966;
    @DexIgnore
    public static /* final */ int up; // = 2131362967;
    @DexIgnore
    public static /* final */ int upper_case_all; // = 2131362968;
    @DexIgnore
    public static /* final */ int upper_case_first_alphabetic_letter; // = 2131362969;
    @DexIgnore
    public static /* final */ int upper_case_first_letter; // = 2131362970;
    @DexIgnore
    public static /* final */ int upper_case_first_letter_sentence; // = 2131362971;
    @DexIgnore
    public static /* final */ int upvote_button; // = 2131362972;
    @DexIgnore
    public static /* final */ int upvote_button_frame; // = 2131362973;
    @DexIgnore
    public static /* final */ int useLogo; // = 2131362974;
    @DexIgnore
    public static /* final */ int vOne; // = 2131362975;
    @DexIgnore
    public static /* final */ int vTwo; // = 2131362976;
    @DexIgnore
    public static /* final */ int v_align; // = 2131362977;
    @DexIgnore
    public static /* final */ int v_background_selected; // = 2131362978;
    @DexIgnore
    public static /* final */ int v_border_bottom; // = 2131362979;
    @DexIgnore
    public static /* final */ int v_center; // = 2131362980;
    @DexIgnore
    public static /* final */ int v_center_horizontal; // = 2131362981;
    @DexIgnore
    public static /* final */ int v_change_password_separator_line; // = 2131362982;
    @DexIgnore
    public static /* final */ int v_elevation; // = 2131362983;
    @DexIgnore
    public static /* final */ int v_line; // = 2131362984;
    @DexIgnore
    public static /* final */ int v_line0; // = 2131362985;
    @DexIgnore
    public static /* final */ int v_line1; // = 2131362986;
    @DexIgnore
    public static /* final */ int v_line2; // = 2131362987;
    @DexIgnore
    public static /* final */ int v_line3; // = 2131362988;
    @DexIgnore
    public static /* final */ int v_line_bottom; // = 2131362989;
    @DexIgnore
    public static /* final */ int v_line_separation; // = 2131362990;
    @DexIgnore
    public static /* final */ int v_separator; // = 2131362991;
    @DexIgnore
    public static /* final */ int v_shadow; // = 2131362992;
    @DexIgnore
    public static /* final */ int v_summary_chart; // = 2131362993;
    @DexIgnore
    public static /* final */ int v_underline; // = 2131362994;
    @DexIgnore
    public static /* final */ int vertical; // = 2131362995;
    @DexIgnore
    public static /* final */ int vertical_guideline; // = 2131362996;
    @DexIgnore
    public static /* final */ int view_article_attachment_list; // = 2131362997;
    @DexIgnore
    public static /* final */ int view_article_content_webview; // = 2131362998;
    @DexIgnore
    public static /* final */ int view_article_progress; // = 2131362999;
    @DexIgnore
    public static /* final */ int view_article_toolbar_holder; // = 2131363000;
    @DexIgnore
    public static /* final */ int view_btn; // = 2131363001;
    @DexIgnore
    public static /* final */ int view_offset_helper; // = 2131363002;
    @DexIgnore
    public static /* final */ int view_request_agent_avatar_imageview; // = 2131363003;
    @DexIgnore
    public static /* final */ int view_request_agent_comment_date; // = 2131363004;
    @DexIgnore
    public static /* final */ int view_request_agent_name_textview; // = 2131363005;
    @DexIgnore
    public static /* final */ int view_request_agent_response_attachment_container; // = 2131363006;
    @DexIgnore
    public static /* final */ int view_request_agent_response_textview; // = 2131363007;
    @DexIgnore
    public static /* final */ int view_request_attachment_container; // = 2131363008;
    @DexIgnore
    public static /* final */ int view_request_comment_attachment_bth; // = 2131363009;
    @DexIgnore
    public static /* final */ int view_request_comment_attachment_container; // = 2131363010;
    @DexIgnore
    public static /* final */ int view_request_comment_container; // = 2131363011;
    @DexIgnore
    public static /* final */ int view_request_comment_edittext; // = 2131363012;
    @DexIgnore
    public static /* final */ int view_request_comment_list; // = 2131363013;
    @DexIgnore
    public static /* final */ int view_request_comment_send_bth; // = 2131363014;
    @DexIgnore
    public static /* final */ int view_request_end_user_avatar_imageview; // = 2131363015;
    @DexIgnore
    public static /* final */ int view_request_end_user_comment_date; // = 2131363016;
    @DexIgnore
    public static /* final */ int view_request_end_user_name_textview; // = 2131363017;
    @DexIgnore
    public static /* final */ int view_request_end_user_response_attachment_container; // = 2131363018;
    @DexIgnore
    public static /* final */ int view_request_end_user_response_textview; // = 2131363019;
    @DexIgnore
    public static /* final */ int view_request_fragment_progress; // = 2131363020;
    @DexIgnore
    public static /* final */ int view_request_separator_1; // = 2131363021;
    @DexIgnore
    public static /* final */ int view_request_separator_2; // = 2131363022;
    @DexIgnore
    public static /* final */ int view_tabs; // = 2131363023;
    @DexIgnore
    public static /* final */ int visible; // = 2131363024;
    @DexIgnore
    public static /* final */ int vote_was_this_helpful; // = 2131363025;
    @DexIgnore
    public static /* final */ int vp_explore; // = 2131363026;
    @DexIgnore
    public static /* final */ int wa_bottom; // = 2131363027;
    @DexIgnore
    public static /* final */ int wa_middle; // = 2131363028;
    @DexIgnore
    public static /* final */ int wa_top; // = 2131363029;
    @DexIgnore
    public static /* final */ int wc_bottom; // = 2131363030;
    @DexIgnore
    public static /* final */ int wc_call_us; // = 2131363031;
    @DexIgnore
    public static /* final */ int wc_complication; // = 2131363032;
    @DexIgnore
    public static /* final */ int wc_email; // = 2131363033;
    @DexIgnore
    public static /* final */ int wc_end; // = 2131363034;
    @DexIgnore
    public static /* final */ int wc_icon; // = 2131363035;
    @DexIgnore
    public static /* final */ int wc_live_chat; // = 2131363036;
    @DexIgnore
    public static /* final */ int wc_start; // = 2131363037;
    @DexIgnore
    public static /* final */ int wc_top; // = 2131363038;
    @DexIgnore
    public static /* final */ int wc_watch_app; // = 2131363039;
    @DexIgnore
    public static /* final */ int webView; // = 2131363040;
    @DexIgnore
    public static /* final */ int weekChart; // = 2131363041;
    @DexIgnore
    public static /* final */ int weight_container; // = 2131363042;
    @DexIgnore
    public static /* final */ int whrc; // = 2131363043;
    @DexIgnore
    public static /* final */ int wide; // = 2131363044;
    @DexIgnore
    public static /* final */ int withText; // = 2131363045;
    @DexIgnore
    public static /* final */ int wrap; // = 2131363046;
    @DexIgnore
    public static /* final */ int wrap_content; // = 2131363047;
    @DexIgnore
    public static /* final */ int zd_toolbar; // = 2131363048;
    @DexIgnore
    public static /* final */ int zd_toolbar_container; // = 2131363049;
    @DexIgnore
    public static /* final */ int zd_toolbar_shadow; // = 2131363050;
}
