package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface d4 {
    @DexIgnore
    float a(c4 c4Var);

    @DexIgnore
    void a();

    @DexIgnore
    void a(c4 c4Var, float f);

    @DexIgnore
    void a(c4 c4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    void a(c4 c4Var, ColorStateList colorStateList);

    @DexIgnore
    float b(c4 c4Var);

    @DexIgnore
    void b(c4 c4Var, float f);

    @DexIgnore
    void c(c4 c4Var);

    @DexIgnore
    void c(c4 c4Var, float f);

    @DexIgnore
    float d(c4 c4Var);

    @DexIgnore
    ColorStateList e(c4 c4Var);

    @DexIgnore
    void f(c4 c4Var);

    @DexIgnore
    float g(c4 c4Var);

    @DexIgnore
    float h(c4 c4Var);

    @DexIgnore
    void i(c4 c4Var);
}
