package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uu */
public class C3062uu implements android.os.Handler.Callback {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.fossil.blesdk.obfuscated.C3062uu.C3064b f10063i; // = new com.fossil.blesdk.obfuscated.C3062uu.C3063a();

    @DexIgnore
    /* renamed from: a */
    public volatile com.fossil.blesdk.obfuscated.C3299xn f10064a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<android.app.FragmentManager, com.fossil.blesdk.obfuscated.C2987tu> f10065b; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Map<androidx.fragment.app.FragmentManager, com.fossil.blesdk.obfuscated.C3314xu> f10066c; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.os.Handler f10067d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3062uu.C3064b f10068e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<android.view.View, androidx.fragment.app.Fragment> f10069f; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<android.view.View, android.app.Fragment> f10070g; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.os.Bundle f10071h; // = new android.os.Bundle();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uu$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uu$a */
    public class C3063a implements com.fossil.blesdk.obfuscated.C3062uu.C3064b {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3299xn mo14809a(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C2770qu quVar, com.fossil.blesdk.obfuscated.C3141vu vuVar, android.content.Context context) {
            return new com.fossil.blesdk.obfuscated.C3299xn(rnVar, quVar, vuVar, context);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.uu$b */
    public interface C3064b {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C3299xn mo14809a(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C2770qu quVar, com.fossil.blesdk.obfuscated.C3141vu vuVar, android.content.Context context);
    }

    @DexIgnore
    public C3062uu(com.fossil.blesdk.obfuscated.C3062uu.C3064b bVar) {
        this.f10068e = bVar == null ? f10063i : bVar;
        this.f10067d = new android.os.Handler(android.os.Looper.getMainLooper(), this);
    }

    @DexIgnore
    /* renamed from: c */
    public static android.app.Activity m14884c(android.content.Context context) {
        if (context instanceof android.app.Activity) {
            return (android.app.Activity) context;
        }
        if (context instanceof android.content.ContextWrapper) {
            return m14884c(((android.content.ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public static boolean m14886d(android.content.Context context) {
        android.app.Activity c = m14884c(context);
        return c == null || !c.isFinishing();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16893a(android.content.Context context) {
        if (context != null) {
            if (com.fossil.blesdk.obfuscated.C3066uw.m14936d() && !(context instanceof android.app.Application)) {
                if (context instanceof androidx.fragment.app.FragmentActivity) {
                    return mo16898a((androidx.fragment.app.FragmentActivity) context);
                }
                if (context instanceof android.app.Activity) {
                    return mo16891a((android.app.Activity) context);
                }
                if (context instanceof android.content.ContextWrapper) {
                    android.content.ContextWrapper contextWrapper = (android.content.ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return mo16893a(contextWrapper.getBaseContext());
                    }
                }
            }
            return mo16903b(context);
        }
        throw new java.lang.IllegalArgumentException("You cannot start a load on a null Context");
    }

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C3299xn mo16903b(android.content.Context context) {
        if (this.f10064a == null) {
            synchronized (this) {
                if (this.f10064a == null) {
                    this.f10064a = this.f10068e.mo14809a(com.fossil.blesdk.obfuscated.C2815rn.m13272a(context.getApplicationContext()), new com.fossil.blesdk.obfuscated.C2247ku(), new com.fossil.blesdk.obfuscated.C2679pu(), context.getApplicationContext());
                }
            }
        }
        return this.f10064a;
    }

    @DexIgnore
    public boolean handleMessage(android.os.Message message) {
        java.lang.Object obj;
        int i = message.what;
        java.lang.Object obj2 = null;
        boolean z = true;
        if (i == 1) {
            obj2 = (android.app.FragmentManager) message.obj;
            obj = this.f10065b.remove(obj2);
        } else if (i != 2) {
            z = false;
            obj = null;
        } else {
            obj2 = (androidx.fragment.app.FragmentManager) message.obj;
            obj = this.f10066c.remove(obj2);
        }
        if (z && obj == null && android.util.Log.isLoggable("RMRetriever", 5)) {
            android.util.Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj2);
        }
        return z;
    }

    @DexIgnore
    @android.annotation.TargetApi(17)
    /* renamed from: c */
    public static void m14885c(android.app.Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new java.lang.IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: b */
    public final void mo16904b(android.app.FragmentManager fragmentManager, com.fossil.blesdk.obfuscated.C1855g4<android.view.View, android.app.Fragment> g4Var) {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            this.f10071h.putInt("key", i);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.f10071h, "key");
            } catch (java.lang.Exception unused) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    g4Var.put(fragment.getView(), fragment);
                    if (android.os.Build.VERSION.SDK_INT >= 17) {
                        mo16901a(fragment.getChildFragmentManager(), g4Var);
                    }
                }
                i = i2;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16898a(androidx.fragment.app.FragmentActivity fragmentActivity) {
        if (com.fossil.blesdk.obfuscated.C3066uw.m14935c()) {
            return mo16893a(fragmentActivity.getApplicationContext());
        }
        m14885c((android.app.Activity) fragmentActivity);
        return mo16895a((android.content.Context) fragmentActivity, fragmentActivity.getSupportFragmentManager(), (androidx.fragment.app.Fragment) null, m14886d(fragmentActivity));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16897a(androidx.fragment.app.Fragment fragment) {
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (com.fossil.blesdk.obfuscated.C3066uw.m14935c()) {
            return mo16893a(fragment.getContext().getApplicationContext());
        }
        return mo16895a(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2987tu mo16902b(android.app.Activity activity) {
        return mo16890a(activity.getFragmentManager(), (android.app.Fragment) null, m14886d(activity));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16891a(android.app.Activity activity) {
        if (com.fossil.blesdk.obfuscated.C3066uw.m14935c()) {
            return mo16893a(activity.getApplicationContext());
        }
        m14885c(activity);
        return mo16894a((android.content.Context) activity, activity.getFragmentManager(), (android.app.Fragment) null, m14886d(activity));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16896a(android.view.View view) {
        if (com.fossil.blesdk.obfuscated.C3066uw.m14935c()) {
            return mo16893a(view.getContext().getApplicationContext());
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(view);
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        android.app.Activity c = m14884c(view.getContext());
        if (c == null) {
            return mo16893a(view.getContext().getApplicationContext());
        }
        if (c instanceof androidx.fragment.app.FragmentActivity) {
            androidx.fragment.app.FragmentActivity fragmentActivity = (androidx.fragment.app.FragmentActivity) c;
            androidx.fragment.app.Fragment a = mo16889a(view, fragmentActivity);
            return a != null ? mo16897a(a) : mo16898a(fragmentActivity);
        }
        android.app.Fragment a2 = mo16888a(view, c);
        if (a2 == null) {
            return mo16891a(c);
        }
        return mo16892a(a2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14883a(java.util.Collection<androidx.fragment.app.Fragment> collection, java.util.Map<android.view.View, androidx.fragment.app.Fragment> map) {
        if (collection != null) {
            for (androidx.fragment.app.Fragment next : collection) {
                if (!(next == null || next.getView() == null)) {
                    map.put(next.getView(), next);
                    m14883a((java.util.Collection<androidx.fragment.app.Fragment>) next.getChildFragmentManager().mo2086d(), map);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.fragment.app.Fragment mo16889a(android.view.View view, androidx.fragment.app.FragmentActivity fragmentActivity) {
        this.f10069f.clear();
        m14883a((java.util.Collection<androidx.fragment.app.Fragment>) fragmentActivity.getSupportFragmentManager().mo2086d(), (java.util.Map<android.view.View, androidx.fragment.app.Fragment>) this.f10069f);
        android.view.View findViewById = fragmentActivity.findViewById(16908290);
        androidx.fragment.app.Fragment fragment = null;
        while (!view.equals(findViewById)) {
            fragment = this.f10069f.get(view);
            if (fragment != null || !(view.getParent() instanceof android.view.View)) {
                break;
            }
            view = (android.view.View) view.getParent();
        }
        this.f10069f.clear();
        return fragment;
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public final android.app.Fragment mo16888a(android.view.View view, android.app.Activity activity) {
        this.f10070g.clear();
        mo16901a(activity.getFragmentManager(), this.f10070g);
        android.view.View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById)) {
            fragment = this.f10070g.get(view);
            if (fragment != null || !(view.getParent() instanceof android.view.View)) {
                break;
            }
            view = (android.view.View) view.getParent();
        }
        this.f10070g.clear();
        return fragment;
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    @java.lang.Deprecated
    /* renamed from: a */
    public final void mo16901a(android.app.FragmentManager fragmentManager, com.fossil.blesdk.obfuscated.C1855g4<android.view.View, android.app.Fragment> g4Var) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment next : fragmentManager.getFragments()) {
                if (next.getView() != null) {
                    g4Var.put(next.getView(), next);
                    mo16901a(next.getChildFragmentManager(), g4Var);
                }
            }
            return;
        }
        mo16904b(fragmentManager, g4Var);
    }

    @DexIgnore
    @android.annotation.TargetApi(17)
    @java.lang.Deprecated
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3299xn mo16892a(android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new java.lang.IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (com.fossil.blesdk.obfuscated.C3066uw.m14935c() || android.os.Build.VERSION.SDK_INT < 17) {
            return mo16893a(fragment.getActivity().getApplicationContext());
        } else {
            return mo16894a((android.content.Context) fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2987tu mo16890a(android.app.FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        com.fossil.blesdk.obfuscated.C2987tu tuVar = (com.fossil.blesdk.obfuscated.C2987tu) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (tuVar == null) {
            tuVar = this.f10065b.get(fragmentManager);
            if (tuVar == null) {
                tuVar = new com.fossil.blesdk.obfuscated.C2987tu();
                tuVar.mo16553b(fragment);
                if (z) {
                    tuVar.mo16552b().mo12480b();
                }
                this.f10065b.put(fragmentManager, tuVar);
                fragmentManager.beginTransaction().add(tuVar, "com.bumptech.glide.manager").commitAllowingStateLoss();
                this.f10067d.obtainMessage(1, fragmentManager).sendToTarget();
            }
        }
        return tuVar;
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3299xn mo16894a(android.content.Context context, android.app.FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        com.fossil.blesdk.obfuscated.C2987tu a = mo16890a(fragmentManager, fragment, z);
        com.fossil.blesdk.obfuscated.C3299xn d = a.mo16556d();
        if (d != null) {
            return d;
        }
        com.fossil.blesdk.obfuscated.C3299xn a2 = this.f10068e.mo14809a(com.fossil.blesdk.obfuscated.C2815rn.m13272a(context), a.mo16552b(), a.mo16557e(), context);
        a.mo16550a(a2);
        return a2;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3314xu mo16899a(android.content.Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        return mo16900a(fragmentManager, (androidx.fragment.app.Fragment) null, m14886d(context));
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3314xu mo16900a(androidx.fragment.app.FragmentManager fragmentManager, androidx.fragment.app.Fragment fragment, boolean z) {
        com.fossil.blesdk.obfuscated.C3314xu xuVar = (com.fossil.blesdk.obfuscated.C3314xu) fragmentManager.mo2077a("com.bumptech.glide.manager");
        if (xuVar == null) {
            xuVar = this.f10066c.get(fragmentManager);
            if (xuVar == null) {
                xuVar = new com.fossil.blesdk.obfuscated.C3314xu();
                xuVar.mo17888b(fragment);
                if (z) {
                    xuVar.mo17879O0().mo12480b();
                }
                this.f10066c.put(fragmentManager, xuVar);
                com.fossil.blesdk.obfuscated.C1472bb a = fragmentManager.mo2078a();
                a.mo9127a((androidx.fragment.app.Fragment) xuVar, "com.bumptech.glide.manager");
                a.mo9129b();
                this.f10067d.obtainMessage(2, fragmentManager).sendToTarget();
            }
        }
        return xuVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3299xn mo16895a(android.content.Context context, androidx.fragment.app.FragmentManager fragmentManager, androidx.fragment.app.Fragment fragment, boolean z) {
        com.fossil.blesdk.obfuscated.C3314xu a = mo16900a(fragmentManager, fragment, z);
        com.fossil.blesdk.obfuscated.C3299xn Q0 = a.mo17881Q0();
        if (Q0 != null) {
            return Q0;
        }
        com.fossil.blesdk.obfuscated.C3299xn a2 = this.f10068e.mo14809a(com.fossil.blesdk.obfuscated.C2815rn.m13272a(context), a.mo17879O0(), a.mo17882R0(), context);
        a.mo17885a(a2);
        return a2;
    }
}
