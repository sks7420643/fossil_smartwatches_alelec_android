package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qy1 implements Parcelable.Creator<py1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder != null) {
            return new py1(readStrongBinder);
        }
        return null;
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new py1[i];
    }
}
