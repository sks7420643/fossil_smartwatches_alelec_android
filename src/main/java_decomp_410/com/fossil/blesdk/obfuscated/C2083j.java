package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j */
public interface C2083j extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.j$a")
    /* renamed from: com.fossil.blesdk.obfuscated.j$a */
    public static abstract class C2084a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C2083j {
        @DexIgnore
        public C2084a() {
            attachInterface(this, "android.support.v4.media.session.IMediaControllerCallback");
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: android.support.v4.media.session.PlaybackStateCompat} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: android.support.v4.media.MediaMetadataCompat} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v13, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v16, resolved type: android.support.v4.media.session.ParcelableVolumeInfo} */
        /* JADX WARNING: type inference failed for: r3v0 */
        /* JADX WARNING: type inference failed for: r3v10, types: [java.lang.CharSequence] */
        /* JADX WARNING: type inference failed for: r3v19 */
        /* JADX WARNING: type inference failed for: r3v20 */
        /* JADX WARNING: type inference failed for: r3v21 */
        /* JADX WARNING: type inference failed for: r3v22 */
        /* JADX WARNING: type inference failed for: r3v23 */
        /* JADX WARNING: type inference failed for: r3v24 */
        /* JADX WARNING: Multi-variable type inference failed */
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i != 1598968902) {
                boolean z = false;
                Object r3 = 0;
                switch (i) {
                    case 1:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        java.lang.String readString = parcel.readString();
                        if (parcel.readInt() != 0) {
                            r3 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        mo140c(readString, r3);
                        return true;
                    case 2:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        mo110i();
                        return true;
                    case 3:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            r3 = android.support.p000v4.media.session.PlaybackStateCompat.CREATOR.createFromParcel(parcel);
                        }
                        mo136a(r3);
                        return true;
                    case 4:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            r3 = android.support.p000v4.media.MediaMetadataCompat.CREATOR.createFromParcel(parcel);
                        }
                        mo105a(r3);
                        return true;
                    case 5:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        mo108a((java.util.List<android.support.p000v4.media.session.MediaSessionCompat.QueueItem>) parcel.createTypedArrayList(android.support.p000v4.media.session.MediaSessionCompat.QueueItem.CREATOR));
                        return true;
                    case 6:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            r3 = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
                        }
                        mo107a((java.lang.CharSequence) r3);
                        return true;
                    case 7:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            r3 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        mo109c(r3);
                        return true;
                    case 8:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            r3 = android.support.p000v4.media.session.ParcelableVolumeInfo.CREATOR.createFromParcel(parcel);
                        }
                        mo106a(r3);
                        return true;
                    case 9:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        mo135a(parcel.readInt());
                        return true;
                    case 10:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        mo139b(z);
                        return true;
                    case 11:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        if (parcel.readInt() != 0) {
                            z = true;
                        }
                        mo137a(z);
                        return true;
                    case 12:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        mo138b(parcel.readInt());
                        return true;
                    case 13:
                        parcel.enforceInterface("android.support.v4.media.session.IMediaControllerCallback");
                        mo141j();
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("android.support.v4.media.session.IMediaControllerCallback");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo135a(int i) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo105a(android.support.p000v4.media.MediaMetadataCompat mediaMetadataCompat) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo106a(android.support.p000v4.media.session.ParcelableVolumeInfo parcelableVolumeInfo) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo136a(android.support.p000v4.media.session.PlaybackStateCompat playbackStateCompat) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo107a(java.lang.CharSequence charSequence) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo108a(java.util.List<android.support.p000v4.media.session.MediaSessionCompat.QueueItem> list) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo137a(boolean z) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: b */
    void mo138b(int i) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: b */
    void mo139b(boolean z) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: c */
    void mo109c(android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: c */
    void mo140c(java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: i */
    void mo110i() throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: j */
    void mo141j() throws android.os.RemoteException;
}
