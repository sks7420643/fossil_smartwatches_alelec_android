package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g4 */
public class C1855g4<K, V> extends androidx.collection.SimpleArrayMap<K, V> implements java.util.Map<K, V> {

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2289l4<K, V> f5381l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.g4$a")
    /* renamed from: com.fossil.blesdk.obfuscated.g4$a */
    public class C1856a extends com.fossil.blesdk.obfuscated.C2289l4<K, V> {
        @DexIgnore
        public C1856a() {
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Object mo11126a(int i, int i2) {
            return com.fossil.blesdk.obfuscated.C1855g4.this.f630f[(i << 1) + i2];
        }

        @DexIgnore
        /* renamed from: b */
        public int mo11131b(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C1855g4.this.mo1223c(obj);
        }

        @DexIgnore
        /* renamed from: c */
        public int mo11133c() {
            return com.fossil.blesdk.obfuscated.C1855g4.this.f631g;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo11125a(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C1855g4.this.mo1221b(obj);
        }

        @DexIgnore
        /* renamed from: b */
        public java.util.Map<K, V> mo11132b() {
            return com.fossil.blesdk.obfuscated.C1855g4.this;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11130a(K k, V v) {
            com.fossil.blesdk.obfuscated.C1855g4.this.put(k, v);
        }

        @DexIgnore
        /* renamed from: a */
        public V mo11127a(int i, V v) {
            return com.fossil.blesdk.obfuscated.C1855g4.this.mo1218a(i, v);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11129a(int i) {
            com.fossil.blesdk.obfuscated.C1855g4.this.mo1228d(i);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11128a() {
            com.fossil.blesdk.obfuscated.C1855g4.this.clear();
        }
    }

    @DexIgnore
    public C1855g4() {
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11119a(java.util.Collection<?> collection) {
        return com.fossil.blesdk.obfuscated.C2289l4.m10054c(this, collection);
    }

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C2289l4<K, V> mo11120b() {
        if (this.f5381l == null) {
            this.f5381l = new com.fossil.blesdk.obfuscated.C1855g4.C1856a();
        }
        return this.f5381l;
    }

    @DexIgnore
    public java.util.Set<java.util.Map.Entry<K, V>> entrySet() {
        return mo11120b().mo13145d();
    }

    @DexIgnore
    public java.util.Set<K> keySet() {
        return mo11120b().mo13146e();
    }

    @DexIgnore
    public void putAll(java.util.Map<? extends K, ? extends V> map) {
        mo1222b(this.f631g + map.size());
        for (java.util.Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    @DexIgnore
    public java.util.Collection<V> values() {
        return mo11120b().mo13147f();
    }

    @DexIgnore
    public C1855g4(int i) {
        super(i);
    }

    @DexIgnore
    public C1855g4(androidx.collection.SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
}
