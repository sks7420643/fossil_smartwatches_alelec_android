package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class i71 implements SharedPreferences.OnSharedPreferenceChangeListener {
    @DexIgnore
    public /* final */ h71 a;

    @DexIgnore
    public i71(h71 h71) {
        this.a = h71;
    }

    @DexIgnore
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.a.a(sharedPreferences, str);
    }
}
