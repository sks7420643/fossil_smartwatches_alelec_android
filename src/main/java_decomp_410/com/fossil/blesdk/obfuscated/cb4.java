package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.collections.EmptyList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cb4 extends bb4 {
    @DexIgnore
    public static final <T> List<T> a() {
        return EmptyList.INSTANCE;
    }

    @DexIgnore
    public static final <T> Collection<T> b(T[] tArr) {
        kd4.b(tArr, "$this$asCollection");
        return new va4(tArr, false);
    }

    @DexIgnore
    public static final <T> List<T> c(T... tArr) {
        kd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length > 0 ? ya4.a(tArr) : a();
    }

    @DexIgnore
    public static final <T> List<T> d(T... tArr) {
        kd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new va4(tArr, true));
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(T... tArr) {
        kd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new va4(tArr, true));
    }

    @DexIgnore
    public static final <T> List<T> b(List<? extends T> list) {
        kd4.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return list;
        }
        return bb4.a(list.get(0));
    }

    @DexIgnore
    public static final void c() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    @DexIgnore
    public static final yd4 a(Collection<?> collection) {
        kd4.b(collection, "$this$indices");
        return new yd4(0, collection.size() - 1);
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list) {
        kd4.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    @DexIgnore
    public static final void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }
}
