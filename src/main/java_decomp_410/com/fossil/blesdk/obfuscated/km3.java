package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.blesdk.obfuscated.ys2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import kotlin.Pair;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class km3 extends ym3 implements cn3, ys2.b {
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<oe2> k;
    @DexIgnore
    public dn3 l;
    @DexIgnore
    public ys2 m;
    @DexIgnore
    public os2 n;
    @DexIgnore
    public List<Pair<ShineDevice, String>> o; // = new ArrayList();
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = km3.class.getSimpleName();
            kd4.a((Object) simpleName, "PairingDeviceFoundFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final km3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            km3 km3 = new km3();
            km3.setArguments(bundle);
            return km3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ oe2 a;
        @DexIgnore
        public /* final */ /* synthetic */ km3 b;

        @DexIgnore
        public b(oe2 oe2, km3 km3, Ref$ObjectRef ref$ObjectRef) {
            this.a = oe2;
            this.b = km3;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b.getActivity(), R.anim.move_in);
            this.a.s.startAnimation(loadAnimation);
            this.a.v.startAnimation(loadAnimation);
            this.a.q.startAnimation(loadAnimation);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ km3 e;

        @DexIgnore
        public c(km3 km3) {
            this.e = km3;
        }

        @DexIgnore
        public final void onClick(View view) {
            km3.b(this.e).a(2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ km3 e;

        @DexIgnore
        public d(km3 km3) {
            this.e = km3;
        }

        @DexIgnore
        public final void onClick(View view) {
            km3.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ km3 e;

        @DexIgnore
        public e(km3 km3) {
            this.e = km3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (km3.c(this.e).d() < this.e.o.size()) {
                km3.b(this.e).b((ShineDevice) ((Pair) this.e.o.get(km3.c(this.e).d())).getFirst());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ dn3 b(km3 km3) {
        dn3 dn3 = km3.l;
        if (dn3 != null) {
            return dn3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ os2 c(km3 km3) {
        os2 os2 = km3.n;
        if (os2 != null) {
            return os2;
        }
        kd4.d("mSnapHelper");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void h(List<Pair<ShineDevice, String>> list) {
        kd4.b(list, "shineDeviceList");
        this.o = list;
        ys2 ys2 = this.m;
        if (ys2 != null) {
            ys2.a(list);
            int size = this.o.size();
            if (size <= 1) {
                tr3<oe2> tr3 = this.k;
                if (tr3 != null) {
                    oe2 a2 = tr3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.s;
                        if (flexibleTextView != null) {
                            pd4 pd4 = pd4.a;
                            Locale locale = Locale.US;
                            kd4.a((Object) locale, "Locale.US");
                            String a3 = sm2.a((Context) getActivity(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                            kd4.a((Object) a3, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                            Object[] objArr = {Integer.valueOf(size)};
                            String format = String.format(locale, a3, Arrays.copyOf(objArr, objArr.length));
                            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                            flexibleTextView.setText(format);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            tr3<oe2> tr32 = this.k;
            if (tr32 != null) {
                oe2 a4 = tr32.a();
                if (a4 != null) {
                    FlexibleTextView flexibleTextView2 = a4.s;
                    if (flexibleTextView2 != null) {
                        pd4 pd42 = pd4.a;
                        Locale locale2 = Locale.US;
                        kd4.a((Object) locale2, "Locale.US");
                        String a5 = sm2.a((Context) getActivity(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                        kd4.a((Object) a5, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        Object[] objArr2 = {Integer.valueOf(size)};
                        String format2 = String.format(locale2, a5, Arrays.copyOf(objArr2, objArr2.length));
                        kd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView2.setText(format2);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i, boolean z, int i2) {
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        ref$ObjectRef.element = null;
        if (z) {
            tr3<oe2> tr3 = this.k;
            if (tr3 != null) {
                oe2 a2 = tr3.a();
                if (!(a2 == null || getActivity() == null)) {
                    ref$ObjectRef.element = AnimationUtils.loadAnimation(getActivity(), R.anim.fragment_enter);
                    Animation animation = (Animation) ref$ObjectRef.element;
                    if (animation != null) {
                        animation.setAnimationListener(new b(a2, this, ref$ObjectRef));
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
        return (Animation) ref$ObjectRef.element;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        oe2 oe2 = (oe2) qa.a(layoutInflater, R.layout.fragment_pairing_device_found, viewGroup, false, O0());
        this.k = new tr3<>(this, oe2);
        kd4.a((Object) oe2, "binding");
        return oe2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<oe2> tr3 = this.k;
        if (tr3 != null) {
            oe2 a2 = tr3.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.t;
                if (rTLImageView != null) {
                    rTLImageView.setOnClickListener(new c(this));
                }
            }
            tr3<oe2> tr32 = this.k;
            if (tr32 != null) {
                oe2 a3 = tr32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView = a3.s;
                    if (flexibleTextView != null) {
                        pd4 pd4 = pd4.a;
                        Locale locale = Locale.US;
                        kd4.a((Object) locale, "Locale.US");
                        String a4 = sm2.a(getContext(), (int) R.string.Onboarding_Pairing_DeviceFound_Title__NumberDevicesFound);
                        kd4.a((Object) a4, "LanguageHelper.getString\u2026itle__NumberDevicesFound)");
                        Object[] objArr = {Integer.valueOf(this.o.size())};
                        String format = String.format(locale, a4, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        flexibleTextView.setText(format);
                    }
                }
                xn a5 = rn.a((Fragment) this);
                kd4.a((Object) a5, "Glide.with(this)");
                this.m = new ys2(a5, this);
                ys2 ys2 = this.m;
                if (ys2 != null) {
                    ys2.a(this.o);
                }
                tr3<oe2> tr33 = this.k;
                if (tr33 != null) {
                    oe2 a6 = tr33.a();
                    if (a6 != null) {
                        RecyclerView recyclerView = a6.v;
                        if (recyclerView != null) {
                            kd4.a((Object) recyclerView, "it");
                            recyclerView.setLayoutManager(new LinearLayoutManager(PortfolioApp.W.c().getApplicationContext(), 0, false));
                            recyclerView.a((RecyclerView.l) new du3());
                            recyclerView.setAdapter(this.m);
                            this.n = new os2();
                            os2 os2 = this.n;
                            if (os2 != null) {
                                os2.a(recyclerView);
                            } else {
                                kd4.d("mSnapHelper");
                                throw null;
                            }
                        }
                    }
                    tr3<oe2> tr34 = this.k;
                    if (tr34 != null) {
                        oe2 a7 = tr34.a();
                        if (a7 != null) {
                            FlexibleTextView flexibleTextView2 = a7.r;
                            if (flexibleTextView2 != null) {
                                flexibleTextView2.setOnClickListener(new d(this));
                            }
                        }
                        tr3<oe2> tr35 = this.k;
                        if (tr35 != null) {
                            oe2 a8 = tr35.a();
                            if (a8 != null) {
                                FlexibleButton flexibleButton = a8.q;
                                if (flexibleButton != null) {
                                    flexibleButton.setOnClickListener(new e(this));
                                }
                            }
                            Bundle arguments = getArguments();
                            if (arguments != null) {
                                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                                tr3<oe2> tr36 = this.k;
                                if (tr36 != null) {
                                    oe2 a9 = tr36.a();
                                    if (a9 != null) {
                                        DashBar dashBar = a9.u;
                                        if (dashBar != null) {
                                            gs3.a aVar = gs3.a;
                                            kd4.a((Object) dashBar, "this");
                                            aVar.b(dashBar, z, 500);
                                            return;
                                        }
                                        return;
                                    }
                                    return;
                                }
                                kd4.d("mBinding");
                                throw null;
                            }
                            return;
                        }
                        kd4.d("mBinding");
                        throw null;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(dn3 dn3) {
        kd4.b(dn3, "presenter");
        this.l = dn3;
    }

    @DexIgnore
    public void a(View view, ys2.c cVar, int i) {
        kd4.b(view, "view");
        kd4.b(cVar, "viewHolder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = q.a();
        StringBuilder sb = new StringBuilder();
        sb.append("onItemClick - position=");
        sb.append(i);
        sb.append(", mSnappedPos=");
        os2 os2 = this.n;
        if (os2 != null) {
            sb.append(os2.d());
            local.d(a2, sb.toString());
            os2 os22 = this.n;
            if (os22 == null) {
                kd4.d("mSnapHelper");
                throw null;
            } else if (os22.d() != i) {
                tr3<oe2> tr3 = this.k;
                if (tr3 != null) {
                    oe2 a3 = tr3.a();
                    if (a3 != null) {
                        RecyclerView recyclerView = a3.v;
                        if (recyclerView != null) {
                            os2 os23 = this.n;
                            if (os23 != null) {
                                recyclerView.j((i - os23.d()) * view.getWidth(), view.getHeight());
                            } else {
                                kd4.d("mSnapHelper");
                                throw null;
                            }
                        }
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else {
                dn3 dn3 = this.l;
                if (dn3 != null) {
                    List<Pair<ShineDevice, String>> list = this.o;
                    os2 os24 = this.n;
                    if (os24 != null) {
                        dn3.b((ShineDevice) list.get(os24.d()).getFirst());
                    } else {
                        kd4.d("mSnapHelper");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            kd4.d("mSnapHelper");
            throw null;
        }
    }
}
