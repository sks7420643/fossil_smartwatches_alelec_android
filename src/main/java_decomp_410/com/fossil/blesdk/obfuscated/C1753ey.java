package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ey */
public class C1753ey {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f4967a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ p011io.fabric.sdk.android.services.common.IdManager f4968b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f4969c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.String f4970d;

    @DexIgnore
    public C1753ey(android.content.Context context, p011io.fabric.sdk.android.services.common.IdManager idManager, java.lang.String str, java.lang.String str2) {
        this.f4967a = context;
        this.f4968b = idManager;
        this.f4969c = str;
        this.f4970d = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1587cy mo10642a() {
        java.util.Map<p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType, java.lang.String> f = this.f4968b.mo44550f();
        java.lang.String n = p011io.fabric.sdk.android.services.common.CommonUtils.m36898n(this.f4967a);
        java.lang.String k = this.f4968b.mo44555k();
        java.lang.String h = this.f4968b.mo44552h();
        com.fossil.blesdk.obfuscated.C1587cy cyVar = new com.fossil.blesdk.obfuscated.C1587cy(this.f4968b.mo44548d(), java.util.UUID.randomUUID().toString(), this.f4968b.mo44549e(), this.f4968b.mo44556l(), f.get(p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType.FONT_TOKEN), n, k, h, this.f4969c, this.f4970d);
        return cyVar;
    }
}
