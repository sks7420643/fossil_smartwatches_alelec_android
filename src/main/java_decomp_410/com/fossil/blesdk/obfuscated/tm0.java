package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tm0 implements Executor {
    @DexIgnore
    public /* final */ Handler e;

    @DexIgnore
    public tm0(Looper looper) {
        this.e = new bz0(looper);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.e.post(runnable);
    }
}
