package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kr0 extends Service {
    @DexIgnore
    public /* final */ ExecutorService e; // = w21.a().a(new um0("EnhancedIntentService"), 9);
    @DexIgnore
    public Binder f;
    @DexIgnore
    public /* final */ Object g; // = new Object();
    @DexIgnore
    public int h;
    @DexIgnore
    public int i; // = 0;

    @DexIgnore
    public final void a(Intent intent) {
        if (intent != null) {
            lb.a(intent);
        }
        synchronized (this.g) {
            this.i--;
            if (this.i == 0) {
                stopSelfResult(this.h);
            }
        }
    }

    @DexIgnore
    public abstract void handleIntent(Intent intent);

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.f == null) {
            this.f = new or0(this);
        }
        return this.f;
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i2, int i3) {
        synchronized (this.g) {
            this.h = i3;
            this.i++;
        }
        if (intent == null) {
            a(intent);
            return 2;
        }
        this.e.execute(new lr0(this, intent, intent));
        return 3;
    }
}
