package com.fossil.blesdk.obfuscated;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zh0 extends bi0 {
    @DexIgnore
    public /* final */ SparseArray<a> j; // = new SparseArray<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ge0.c {
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ ge0 f;
        @DexIgnore
        public /* final */ ge0.c g;

        @DexIgnore
        public a(int i, ge0 ge0, ge0.c cVar) {
            this.e = i;
            this.f = ge0;
            this.g = cVar;
            ge0.a((ge0.c) this);
        }

        @DexIgnore
        public final void a(ud0 ud0) {
            String valueOf = String.valueOf(ud0);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            zh0.this.b(ud0, this.e);
        }
    }

    @DexIgnore
    public zh0(ye0 ye0) {
        super(ye0);
        this.e.a("AutoManageHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static zh0 b(xe0 xe0) {
        ye0 a2 = LifecycleCallback.a(xe0);
        zh0 zh0 = (zh0) a2.a("AutoManageHelper", zh0.class);
        if (zh0 != null) {
            return zh0;
        }
        return new zh0(a2);
    }

    @DexIgnore
    public final void a(int i, ge0 ge0, ge0.c cVar) {
        bk0.a(ge0, (Object) "GoogleApiClient instance cannot be null");
        boolean z = this.j.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        bk0.b(z, sb.toString());
        ci0 ci0 = this.g.get();
        boolean z2 = this.f;
        String valueOf = String.valueOf(ci0);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.j.put(i, new a(i, ge0, cVar));
        if (this.f && ci0 == null) {
            String valueOf2 = String.valueOf(ge0);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            ge0.c();
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        boolean z = this.f;
        String valueOf = String.valueOf(this.j);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.g.get() == null) {
            for (int i = 0; i < this.j.size(); i++) {
                a b = b(i);
                if (b != null) {
                    b.f.c();
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        super.e();
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.f.d();
            }
        }
    }

    @DexIgnore
    public final void f() {
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.f.c();
            }
        }
    }

    @DexIgnore
    public final a b(int i) {
        if (this.j.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.j;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    @DexIgnore
    public final void a(int i) {
        a aVar = this.j.get(i);
        this.j.remove(i);
        if (aVar != null) {
            aVar.f.b((ge0.c) aVar);
            aVar.f.d();
        }
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.j.size(); i++) {
            a b = b(i);
            if (b != null) {
                printWriter.append(str).append("GoogleApiClient #").print(b.e);
                printWriter.println(":");
                b.f.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    public final void a(ud0 ud0, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.j.get(i);
        if (aVar != null) {
            a(i);
            ge0.c cVar = aVar.g;
            if (cVar != null) {
                cVar.a(ud0);
            }
        }
    }
}
