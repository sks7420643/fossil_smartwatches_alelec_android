package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class po4 extends zo4 {
    @DexIgnore
    public zo4 e;

    @DexIgnore
    public po4(zo4 zo4) {
        if (zo4 != null) {
            this.e = zo4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public final po4 a(zo4 zo4) {
        if (zo4 != null) {
            this.e = zo4;
            return this;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public zo4 b() {
        return this.e.b();
    }

    @DexIgnore
    public long c() {
        return this.e.c();
    }

    @DexIgnore
    public boolean d() {
        return this.e.d();
    }

    @DexIgnore
    public void e() throws IOException {
        this.e.e();
    }

    @DexIgnore
    public final zo4 g() {
        return this.e;
    }

    @DexIgnore
    public zo4 a(long j, TimeUnit timeUnit) {
        return this.e.a(j, timeUnit);
    }

    @DexIgnore
    public zo4 a(long j) {
        return this.e.a(j);
    }

    @DexIgnore
    public zo4 a() {
        return this.e.a();
    }
}
