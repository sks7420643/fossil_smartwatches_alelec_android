package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzco;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class au0 extends yt0 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public au0(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.e = Integer.MAX_VALUE;
        this.a = i2 + i;
        this.c = i;
        this.d = this.c;
    }

    @DexIgnore
    public final int a() {
        return this.c - this.d;
    }

    @DexIgnore
    public final int b(int i) throws zzco {
        if (i >= 0) {
            int a2 = i + a();
            int i2 = this.e;
            if (a2 <= i2) {
                this.e = a2;
                this.a += this.b;
                int i3 = this.a;
                int i4 = i3 - this.d;
                int i5 = this.e;
                if (i4 > i5) {
                    this.b = i4 - i5;
                    this.a = i3 - this.b;
                } else {
                    this.b = 0;
                }
                return i2;
            }
            throw zzco.zzbl();
        }
        throw new zzco("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }
}
