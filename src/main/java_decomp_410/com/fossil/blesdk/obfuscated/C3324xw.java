package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xw */
public abstract class C3324xw {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xw$b")
    /* renamed from: com.fossil.blesdk.obfuscated.xw$b */
    public static class C3326b extends com.fossil.blesdk.obfuscated.C3324xw {

        @DexIgnore
        /* renamed from: a */
        public volatile boolean f11098a;

        @DexIgnore
        public C3326b() {
            super();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17919a() {
            if (this.f11098a) {
                throw new java.lang.IllegalStateException("Already released");
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17920a(boolean z) {
            this.f11098a = z;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C3324xw m16582b() {
        return new com.fossil.blesdk.obfuscated.C3324xw.C3326b();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo17919a();

    @DexIgnore
    /* renamed from: a */
    public abstract void mo17920a(boolean z);

    @DexIgnore
    public C3324xw() {
    }
}
