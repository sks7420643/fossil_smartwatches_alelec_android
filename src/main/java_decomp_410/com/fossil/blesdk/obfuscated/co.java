package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface co {

    @DexIgnore
    public interface a {
        @DexIgnore
        Bitmap a(int i, int i2, Bitmap.Config config);

        @DexIgnore
        void a(Bitmap bitmap);

        @DexIgnore
        void a(byte[] bArr);

        @DexIgnore
        void a(int[] iArr);

        @DexIgnore
        int[] a(int i);

        @DexIgnore
        byte[] b(int i);
    }

    @DexIgnore
    Bitmap a();

    @DexIgnore
    void a(Bitmap.Config config);

    @DexIgnore
    void b();

    @DexIgnore
    int c();

    @DexIgnore
    void clear();

    @DexIgnore
    int d();

    @DexIgnore
    ByteBuffer e();

    @DexIgnore
    void f();

    @DexIgnore
    int g();

    @DexIgnore
    int h();
}
