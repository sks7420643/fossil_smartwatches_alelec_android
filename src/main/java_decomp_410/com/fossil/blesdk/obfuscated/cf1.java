package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cf1 extends IInterface {
    @DexIgnore
    le1 a(sn0 sn0, GoogleMapOptions googleMapOptions) throws RemoteException;

    @DexIgnore
    ne1 a(sn0 sn0, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException;

    @DexIgnore
    void a(sn0 sn0, int i) throws RemoteException;

    @DexIgnore
    ke1 c(sn0 sn0) throws RemoteException;

    @DexIgnore
    ie1 zze() throws RemoteException;

    @DexIgnore
    d51 zzf() throws RemoteException;
}
