package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.AppEventsConstants;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nv3 implements Closeable {
    @DexIgnore
    public static /* final */ Pattern w; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public static /* final */ xo4 x; // = new c();
    @DexIgnore
    public /* final */ ex3 e;
    @DexIgnore
    public /* final */ File f;
    @DexIgnore
    public /* final */ File g;
    @DexIgnore
    public /* final */ File h;
    @DexIgnore
    public /* final */ File i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public long k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public ko4 n;
    @DexIgnore
    public /* final */ LinkedHashMap<String, e> o; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public long t; // = 0;
    @DexIgnore
    public /* final */ Executor u;
    @DexIgnore
    public /* final */ Runnable v; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            synchronized (nv3.this) {
                if (!(!nv3.this.r) && !nv3.this.s) {
                    try {
                        nv3.this.G();
                        if (nv3.this.B()) {
                            nv3.this.F();
                            int unused = nv3.this.p = 0;
                        }
                    } catch (IOException e2) {
                        throw new RuntimeException(e2);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ov3 {
        /*
        static {
            Class<nv3> cls = nv3.class;
        }
        */

        @DexIgnore
        public b(xo4 xo4) {
            super(xo4);
        }

        @DexIgnore
        public void a(IOException iOException) {
            boolean unused = nv3.this.q = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements xo4 {
        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            jo4.skip(j);
        }

        @DexIgnore
        public zo4 b() {
            return zo4.d;
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public void flush() throws IOException {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ e a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ov3 {
            @DexIgnore
            public a(xo4 xo4) {
                super(xo4);
            }

            @DexIgnore
            public void a(IOException iOException) {
                synchronized (nv3.this) {
                    boolean unused = d.this.c = true;
                }
            }
        }

        @DexIgnore
        public /* synthetic */ d(nv3 nv3, e eVar, a aVar) {
            this(eVar);
        }

        @DexIgnore
        public d(e eVar) {
            this.a = eVar;
            this.b = eVar.e ? null : new boolean[nv3.this.l];
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (nv3.this) {
                if (this.c) {
                    nv3.this.a(this, false);
                    boolean unused = nv3.this.a(this.a);
                } else {
                    nv3.this.a(this, true);
                }
            }
        }

        @DexIgnore
        public xo4 a(int i) throws IOException {
            a aVar;
            synchronized (nv3.this) {
                if (this.a.f == this) {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    try {
                        aVar = new a(nv3.this.e.b(this.a.d[i]));
                    } catch (FileNotFoundException unused) {
                        return nv3.x;
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return aVar;
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (nv3.this) {
                nv3.this.a(this, false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public d f;
        @DexIgnore
        public long g;

        @DexIgnore
        public /* synthetic */ e(nv3 nv3, String str, a aVar) {
            this(str);
        }

        @DexIgnore
        public e(String str) {
            this.a = str;
            this.b = new long[nv3.this.l];
            this.c = new File[nv3.this.l];
            this.d = new File[nv3.this.l];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < nv3.this.l; i++) {
                sb.append(i);
                this.c[i] = new File(nv3.this.f, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(nv3.this.f, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public final void b(String[] strArr) throws IOException {
            if (strArr.length == nv3.this.l) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public void a(ko4 ko4) throws IOException {
            for (long b2 : this.b) {
                ko4.writeByte(32).b(b2);
            }
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public f a() {
            if (Thread.holdsLock(nv3.this)) {
                yo4[] yo4Arr = new yo4[nv3.this.l];
                long[] jArr = (long[]) this.b.clone();
                int i = 0;
                int i2 = 0;
                while (i2 < nv3.this.l) {
                    try {
                        yo4Arr[i2] = nv3.this.e.a(this.c[i2]);
                        i2++;
                    } catch (FileNotFoundException unused) {
                        while (i < nv3.this.l && yo4Arr[i] != null) {
                            wv3.a((Closeable) yo4Arr[i]);
                            i++;
                        }
                        return null;
                    }
                }
                return new f(nv3.this, this.a, this.g, yo4Arr, jArr, (a) null);
            }
            throw new AssertionError();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements Closeable {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ yo4[] g;

        @DexIgnore
        public /* synthetic */ f(nv3 nv3, String str, long j, yo4[] yo4Arr, long[] jArr, a aVar) {
            this(str, j, yo4Arr, jArr);
        }

        @DexIgnore
        public yo4 b(int i) {
            return this.g[i];
        }

        @DexIgnore
        public void close() {
            for (yo4 a : this.g) {
                wv3.a((Closeable) a);
            }
        }

        @DexIgnore
        public d y() throws IOException {
            return nv3.this.a(this.e, this.f);
        }

        @DexIgnore
        public f(String str, long j, yo4[] yo4Arr, long[] jArr) {
            this.e = str;
            this.f = j;
            this.g = yo4Arr;
        }
    }

    /*
    static {
        Class<nv3> cls = nv3.class;
    }
    */

    @DexIgnore
    public nv3(ex3 ex3, File file, int i2, int i3, long j2, Executor executor) {
        this.e = ex3;
        this.f = file;
        this.j = i2;
        this.g = new File(file, "journal");
        this.h = new File(file, "journal.tmp");
        this.i = new File(file, "journal.bkp");
        this.l = i3;
        this.k = j2;
        this.u = executor;
    }

    @DexIgnore
    public synchronized void A() throws IOException {
        if (!this.r) {
            if (this.e.d(this.i)) {
                if (this.e.d(this.g)) {
                    this.e.e(this.i);
                } else {
                    this.e.a(this.i, this.g);
                }
            }
            if (this.e.d(this.g)) {
                try {
                    E();
                    D();
                    this.r = true;
                    return;
                } catch (IOException e2) {
                    uv3 c2 = uv3.c();
                    c2.a("DiskLruCache " + this.f + " is corrupt: " + e2.getMessage() + ", removing");
                    z();
                    this.s = false;
                }
            }
            F();
            this.r = true;
        }
    }

    @DexIgnore
    public final boolean B() {
        int i2 = this.p;
        return i2 >= 2000 && i2 >= this.o.size();
    }

    @DexIgnore
    public final ko4 C() throws FileNotFoundException {
        return so4.a((xo4) new b(this.e.f(this.g)));
    }

    @DexIgnore
    public final void D() throws IOException {
        this.e.e(this.h);
        Iterator<e> it = this.o.values().iterator();
        while (it.hasNext()) {
            e next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.l) {
                    this.m += next.b[i2];
                    i2++;
                }
            } else {
                d unused = next.f = null;
                while (i2 < this.l) {
                    this.e.e(next.c[i2]);
                    this.e.e(next.d[i2]);
                    i2++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.p = r0 - r9.o.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        if (r1.g() == false) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006c, code lost:
        F();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0070, code lost:
        r9.n = C();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0079, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005d */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x007a=Splitter:B:23:0x007a, B:16:0x005d=Splitter:B:16:0x005d} */
    public final void E() throws IOException {
        lo4 a2 = so4.a(this.e.a(this.g));
        try {
            String i2 = a2.i();
            String i3 = a2.i();
            String i4 = a2.i();
            String i5 = a2.i();
            String i6 = a2.i();
            if (!"libcore.io.DiskLruCache".equals(i2) || !AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(i3) || !Integer.toString(this.j).equals(i4) || !Integer.toString(this.l).equals(i5) || !"".equals(i6)) {
                throw new IOException("unexpected journal header: [" + i2 + ", " + i3 + ", " + i5 + ", " + i6 + "]");
            }
            int i7 = 0;
            while (true) {
                g(a2.i());
                i7++;
            }
        } finally {
            wv3.a((Closeable) a2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final synchronized void F() throws IOException {
        if (this.n != null) {
            this.n.close();
        }
        ko4 a2 = so4.a(this.e.b(this.h));
        try {
            a2.a("libcore.io.DiskLruCache").writeByte(10);
            a2.a(AppEventsConstants.EVENT_PARAM_VALUE_YES).writeByte(10);
            a2.b((long) this.j).writeByte(10);
            a2.b((long) this.l).writeByte(10);
            a2.writeByte(10);
            for (e next : this.o.values()) {
                if (next.f != null) {
                    a2.a("DIRTY").writeByte(32);
                    a2.a(next.a);
                    a2.writeByte(10);
                } else {
                    a2.a("CLEAN").writeByte(32);
                    a2.a(next.a);
                    next.a(a2);
                    a2.writeByte(10);
                }
            }
            a2.close();
            if (this.e.d(this.g)) {
                this.e.a(this.g, this.i);
            }
            this.e.a(this.h, this.g);
            this.e.e(this.i);
            this.n = C();
            this.q = false;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    @DexIgnore
    public final void G() throws IOException {
        while (this.m > this.k) {
            a(this.o.values().iterator().next());
        }
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        if (this.r) {
            if (!this.s) {
                for (e eVar : (e[]) this.o.values().toArray(new e[this.o.size()])) {
                    if (eVar.f != null) {
                        eVar.f.a();
                    }
                }
                G();
                this.n.close();
                this.n = null;
                this.s = true;
                return;
            }
        }
        this.s = true;
    }

    @DexIgnore
    public final void i(String str) {
        if (!w.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    public synchronized boolean isClosed() {
        return this.s;
    }

    @DexIgnore
    public final synchronized void y() {
        if (isClosed()) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    public void z() throws IOException {
        close();
        this.e.c(this.f);
    }

    @DexIgnore
    public d e(String str) throws IOException {
        return a(str, -1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0051, code lost:
        return null;
     */
    @DexIgnore
    public synchronized f f(String str) throws IOException {
        A();
        y();
        i(str);
        e eVar = this.o.get(str);
        if (eVar != null) {
            if (eVar.e) {
                f a2 = eVar.a();
                if (a2 == null) {
                    return null;
                }
                this.p++;
                this.n.a("READ").writeByte(32).a(str).writeByte(10);
                if (B()) {
                    this.u.execute(this.v);
                }
            }
        }
    }

    @DexIgnore
    public final void g(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.o.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            e eVar = this.o.get(str2);
            if (eVar == null) {
                eVar = new e(this, str2, (a) null);
                this.o.put(str2, eVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = eVar.e = true;
                d unused2 = eVar.f = null;
                eVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                d unused3 = eVar.f = new d(this, eVar, (a) null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    public synchronized boolean h(String str) throws IOException {
        A();
        y();
        i(str);
        e eVar = this.o.get(str);
        if (eVar == null) {
            return false;
        }
        return a(eVar);
    }

    @DexIgnore
    public static nv3 a(ex3 ex3, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new nv3(ex3, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), wv3.a("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        return null;
     */
    @DexIgnore
    public final synchronized d a(String str, long j2) throws IOException {
        A();
        y();
        i(str);
        e eVar = this.o.get(str);
        if (j2 == -1 || (eVar != null && eVar.g == j2)) {
            if (eVar != null) {
                if (eVar.f != null) {
                    return null;
                }
            }
            this.n.a("DIRTY").writeByte(32).a(str).writeByte(10);
            this.n.flush();
            if (this.q) {
                return null;
            }
            if (eVar == null) {
                eVar = new e(this, str, (a) null);
                this.o.put(str, eVar);
            }
            d dVar = new d(this, eVar, (a) null);
            d unused = eVar.f = dVar;
            return dVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0111, code lost:
        return;
     */
    @DexIgnore
    public final synchronized void a(d dVar, boolean z) throws IOException {
        e a2 = dVar.a;
        if (a2.f == dVar) {
            if (z && !a2.e) {
                int i2 = 0;
                while (i2 < this.l) {
                    if (!dVar.b[i2]) {
                        dVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!this.e.d(a2.d[i2])) {
                        dVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.l; i3++) {
                File file = a2.d[i3];
                if (!z) {
                    this.e.e(file);
                } else if (this.e.d(file)) {
                    File file2 = a2.c[i3];
                    this.e.a(file, file2);
                    long j2 = a2.b[i3];
                    long g2 = this.e.g(file2);
                    a2.b[i3] = g2;
                    this.m = (this.m - j2) + g2;
                }
            }
            this.p++;
            d unused = a2.f = null;
            if (a2.e || z) {
                boolean unused2 = a2.e = true;
                this.n.a("CLEAN").writeByte(32);
                this.n.a(a2.a);
                a2.a(this.n);
                this.n.writeByte(10);
                if (z) {
                    long j3 = this.t;
                    this.t = 1 + j3;
                    long unused3 = a2.g = j3;
                }
            } else {
                this.o.remove(a2.a);
                this.n.a("REMOVE").writeByte(32);
                this.n.a(a2.a);
                this.n.writeByte(10);
            }
            this.n.flush();
            if (this.m > this.k || B()) {
                this.u.execute(this.v);
            }
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public final boolean a(e eVar) throws IOException {
        if (eVar.f != null) {
            boolean unused = eVar.f.c = true;
        }
        for (int i2 = 0; i2 < this.l; i2++) {
            this.e.e(eVar.c[i2]);
            this.m -= eVar.b[i2];
            eVar.b[i2] = 0;
        }
        this.p++;
        this.n.a("REMOVE").writeByte(32).a(eVar.a).writeByte(10);
        this.o.remove(eVar.a);
        if (B()) {
            this.u.execute(this.v);
        }
        return true;
    }
}
