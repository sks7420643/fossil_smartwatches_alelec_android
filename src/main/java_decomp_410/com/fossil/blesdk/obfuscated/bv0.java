package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bv0 {
    @DexIgnore
    public volatile sv0 a;
    @DexIgnore
    public volatile zzbb b;

    /*
    static {
        fu0.b();
    }
    */

    @DexIgnore
    public final int a() {
        if (this.b != null) {
            return this.b.size();
        }
        if (this.a != null) {
            return this.a.f();
        }
        return 0;
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(4:7|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    public final sv0 a(sv0 sv0) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = sv0;
                    this.b = zzbb.zzfi;
                    this.a = sv0;
                    this.b = zzbb.zzfi;
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public final sv0 b(sv0 sv0) {
        sv0 sv02 = this.a;
        this.b = null;
        this.a = sv0;
        return sv02;
    }

    @DexIgnore
    public final zzbb b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                zzbb zzbb = this.b;
                return zzbb;
            }
            this.b = this.a == null ? zzbb.zzfi : this.a.d();
            zzbb zzbb2 = this.b;
            return zzbb2;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bv0)) {
            return false;
        }
        bv0 bv0 = (bv0) obj;
        sv0 sv0 = this.a;
        sv0 sv02 = bv0.a;
        return (sv0 == null && sv02 == null) ? b().equals(bv0.b()) : (sv0 == null || sv02 == null) ? sv0 != null ? sv0.equals(bv0.a(sv0.b())) : a(sv02.b()).equals(sv02) : sv0.equals(sv02);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }
}
