package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.provider.HourNotificationProviderImp;
import com.portfolio.platform.provider.MicroAppSettingProviderImp;
import com.portfolio.platform.provider.UserProviderImp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn2 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static dn2 o;
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public ContactProvider a;
    @DexIgnore
    public LocationProvider b;
    @DexIgnore
    public AppFilterProvider c;
    @DexIgnore
    public DeviceProvider d;
    @DexIgnore
    public wn2 e;
    @DexIgnore
    public MFSleepSessionProvider f;
    @DexIgnore
    public tn2 g;
    @DexIgnore
    public zn2 h;
    @DexIgnore
    public un2 i;
    @DexIgnore
    public do2 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public xn2 l;
    @DexIgnore
    public bo2 m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(dn2 dn2) {
            dn2.o = dn2;
        }

        @DexIgnore
        public final dn2 b() {
            return dn2.o;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final dn2 a() {
            if (b() == null) {
                a(new dn2((fd4) null));
            }
            dn2 b = b();
            if (b != null) {
                return b;
            }
            kd4.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = dn2.class.getSimpleName();
        kd4.a((Object) simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public dn2() {
    }

    @DexIgnore
    public final synchronized ContactProvider b() {
        ContactProvider contactProvider;
        String c2 = c();
        if (this.a == null) {
            this.a = new rn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + ContactProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + ContactProviderImpl.DB_NAME;
            ContactProvider contactProvider2 = this.a;
            if (contactProvider2 != null) {
                String dbPath = contactProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.a = new rn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        contactProvider = this.a;
        if (contactProvider == null) {
            kd4.a();
            throw null;
        }
        return contactProvider;
    }

    @DexIgnore
    public final String c() {
        do2 do2 = this.j;
        if (do2 == null) {
            return "Anonymous";
        }
        if (do2 != null) {
            MFUser b2 = do2.b();
            if (b2 == null || TextUtils.isEmpty(b2.getUserId())) {
                return "Anonymous";
            }
            String userId = b2.getUserId();
            kd4.a((Object) userId, "user.userId");
            return userId;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized DeviceProvider d() {
        DeviceProvider deviceProvider;
        DeviceProviderImp deviceProviderImp;
        String c2 = c();
        FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + c2);
        if (this.d == null) {
            this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + DeviceProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + DeviceProviderImp.DB_NAME;
            try {
                DeviceProvider deviceProvider2 = this.d;
                if (deviceProvider2 != null) {
                    String dbPath = deviceProvider2.getDbPath();
                    if (TextUtils.isEmpty(dbPath) || (!kd4.a((Object) str, (Object) dbPath))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                        this.d = deviceProviderImp;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                    if (TextUtils.isEmpty((CharSequence) null) || (!kd4.a((Object) str, (Object) null))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (TextUtils.isEmpty((CharSequence) null) || (!kd4.a((Object) str, (Object) null))) {
                        this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            kd4.a();
            throw null;
        }
        return deviceProvider;
    }

    @DexIgnore
    public final synchronized un2 e() {
        un2 un2;
        String c2 = c();
        if (this.i == null) {
            this.i = new vn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "firmwares.db");
        } else {
            String str = c2 + "_" + "firmwares.db";
            un2 un22 = this.i;
            if (un22 != null) {
                String dbPath = un22.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.i = new vn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        un2 = this.i;
        if (un2 == null) {
            kd4.a();
            throw null;
        }
        return un2;
    }

    @DexIgnore
    public final synchronized tn2 f() {
        tn2 tn2;
        if (this.g == null) {
            String c2 = c();
            this.g = new HourNotificationProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "hourNotification.db");
        }
        tn2 = this.g;
        if (tn2 == null) {
            kd4.a();
            throw null;
        }
        return tn2;
    }

    @DexIgnore
    public final synchronized LocationProvider g() {
        LocationProvider locationProvider;
        String c2 = c();
        if (this.b == null) {
            this.b = new LocationProviderImpl(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + LocationProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + LocationProviderImpl.DB_NAME;
            LocationProvider locationProvider2 = this.b;
            if (locationProvider2 != null) {
                String dbPath = locationProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.b = new LocationProviderImpl(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        locationProvider = this.b;
        if (locationProvider == null) {
            kd4.a();
            throw null;
        }
        return locationProvider;
    }

    @DexIgnore
    public final synchronized wn2 h() {
        wn2 wn2;
        String c2 = c();
        if (this.e == null) {
            this.e = new MicroAppSettingProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "microAppSetting.db");
        } else {
            String str = c2 + "_" + "microAppSetting.db";
            wn2 wn22 = this.e;
            if (wn22 != null) {
                String dbPath = wn22.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.e = new MicroAppSettingProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        wn2 = this.e;
        if (wn2 == null) {
            kd4.a();
            throw null;
        }
        return wn2;
    }

    @DexIgnore
    public final xn2 i() {
        if (this.l == null) {
            this.l = new yn2(PortfolioApp.W.c().getApplicationContext(), "phone_favorites_contact.db");
        }
        xn2 xn2 = this.l;
        if (xn2 != null) {
            return xn2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized zn2 j() {
        zn2 zn2;
        ao2 ao2;
        String c2 = c();
        if (this.h == null) {
            this.h = new ao2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + "pin.db");
        } else {
            String str = c2 + "_" + "pin.db";
            try {
                zn2 zn22 = this.h;
                if (zn22 != null) {
                    String dbPath = zn22.getDbPath();
                    if (yp4.a(dbPath) || (!kd4.a((Object) str, (Object) dbPath))) {
                        ao2 = new ao2(PortfolioApp.W.c().getApplicationContext(), str);
                        this.h = ao2;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                try {
                    FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                    if (yp4.a((String) null) || (!kd4.a((Object) str, (Object) null))) {
                        ao2 = new ao2(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                } catch (Throwable th) {
                    if (yp4.a((String) null) || (!kd4.a((Object) str, (Object) null))) {
                        this.h = new ao2(PortfolioApp.W.c().getApplicationContext(), str);
                    }
                    throw th;
                }
            }
        }
        zn2 = this.h;
        if (zn2 == null) {
            kd4.a();
            throw null;
        }
        return zn2;
    }

    @DexIgnore
    public final synchronized SecondTimezoneProvider k() {
        SecondTimezoneProvider secondTimezoneProvider;
        String c2 = c();
        if (this.k == null) {
            this.k = new SecondTimezoneProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + SecondTimezoneProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + SecondTimezoneProviderImp.DB_NAME;
            SecondTimezoneProvider secondTimezoneProvider2 = this.k;
            if (secondTimezoneProvider2 != null) {
                String dbPath = secondTimezoneProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.k = new SecondTimezoneProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        secondTimezoneProvider = this.k;
        if (secondTimezoneProvider == null) {
            kd4.a();
            throw null;
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    public final bo2 l() {
        if (this.m == null) {
            synchronized (dn2.class) {
                if (this.m == null) {
                    String str = c() + "_" + "serverSetting.db";
                    FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                    Context applicationContext = PortfolioApp.W.c().getApplicationContext();
                    kd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    this.m = new co2(applicationContext, str);
                }
                qa4 qa4 = qa4.a;
            }
        }
        bo2 bo2 = this.m;
        if (bo2 != null) {
            return bo2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final synchronized MFSleepSessionProvider m() {
        MFSleepSessionProvider mFSleepSessionProvider;
        String c2 = c();
        if (this.f == null) {
            this.f = new MFSleepSessionProviderImp(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + MFSleepSessionProviderImp.DB_NAME);
        } else {
            String str = c2 + "_" + MFSleepSessionProviderImp.DB_NAME;
            MFSleepSessionProvider mFSleepSessionProvider2 = this.f;
            if (mFSleepSessionProvider2 != null) {
                String dbPath = mFSleepSessionProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.f = new MFSleepSessionProviderImp(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        mFSleepSessionProvider = this.f;
        if (mFSleepSessionProvider == null) {
            kd4.a();
            throw null;
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    public final do2 n() {
        if (this.j == null) {
            Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            kd4.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            this.j = new UserProviderImp(applicationContext, "user.db");
        }
        do2 do2 = this.j;
        if (do2 != null) {
            return do2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void o() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.f = null;
        this.h = null;
        this.d = null;
        this.g = null;
        this.j = null;
        this.e = null;
        this.m = null;
    }

    @DexIgnore
    public /* synthetic */ dn2(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final synchronized AppFilterProvider a() {
        AppFilterProvider appFilterProvider;
        String c2 = c();
        if (this.c == null) {
            this.c = new qn2(PortfolioApp.W.c().getApplicationContext(), c2 + "_" + AppFilterProviderImpl.DB_NAME);
        } else {
            String str = c2 + "_" + AppFilterProviderImpl.DB_NAME;
            AppFilterProvider appFilterProvider2 = this.c;
            if (appFilterProvider2 != null) {
                String dbPath = appFilterProvider2.getDbPath();
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str);
                if (!TextUtils.isEmpty(dbPath) && (!kd4.a((Object) str, (Object) dbPath))) {
                    this.c = new qn2(PortfolioApp.W.c().getApplicationContext(), str);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        appFilterProvider = this.c;
        if (appFilterProvider == null) {
            kd4.a();
            throw null;
        }
        return appFilterProvider;
    }

    @DexIgnore
    public final synchronized DeviceProvider a(String str) {
        DeviceProvider deviceProvider;
        kd4.b(str, ButtonService.USER_ID);
        if (this.d == null) {
            String str2 = str + "_" + DeviceProviderImp.DB_NAME;
            FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
            this.d = new DeviceProviderImp(PortfolioApp.W.c().getApplicationContext(), str2);
        }
        deviceProvider = this.d;
        if (deviceProvider == null) {
            kd4.a();
            throw null;
        }
        return deviceProvider;
    }
}
