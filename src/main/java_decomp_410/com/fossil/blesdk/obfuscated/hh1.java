package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hh1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ fh1 e;

    @DexIgnore
    public hh1(fh1 fh1, String str, boolean z) {
        this.e = fh1;
        bk0.b(str);
        this.a = str;
        this.b = z;
    }

    @DexIgnore
    public final boolean a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.s().getBoolean(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(boolean z) {
        SharedPreferences.Editor edit = this.e.s().edit();
        edit.putBoolean(this.a, z);
        edit.apply();
        this.d = z;
    }
}
