package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dj */
public abstract class C1635dj {

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1635dj f4399a; // = null;

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int f4400b; // = 20;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dj$a */
    public static class C1636a extends com.fossil.blesdk.obfuscated.C1635dj {

        @DexIgnore
        /* renamed from: c */
        public int f4401c;

        @DexIgnore
        public C1636a(int i) {
            super(i);
            this.f4401c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9962a(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr) {
            if (this.f4401c > 3) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                android.util.Log.d(str, str2);
            } else {
                android.util.Log.d(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo9963b(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr) {
            if (this.f4401c > 6) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                android.util.Log.e(str, str2);
            } else {
                android.util.Log.e(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo9964c(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr) {
            if (this.f4401c > 4) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                android.util.Log.i(str, str2);
            } else {
                android.util.Log.i(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        /* renamed from: d */
        public void mo9965d(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr) {
            if (this.f4401c > 2) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                android.util.Log.v(str, str2);
            } else {
                android.util.Log.v(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        /* renamed from: e */
        public void mo9966e(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr) {
            if (this.f4401c > 5) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                android.util.Log.w(str, str2);
            } else {
                android.util.Log.w(str, str2, thArr[0]);
            }
        }
    }

    @DexIgnore
    public C1635dj(int i) {
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized void m5872a(com.fossil.blesdk.obfuscated.C1635dj djVar) {
        synchronized (com.fossil.blesdk.obfuscated.C1635dj.class) {
            f4399a = djVar;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9962a(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr);

    @DexIgnore
    /* renamed from: b */
    public abstract void mo9963b(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr);

    @DexIgnore
    /* renamed from: c */
    public abstract void mo9964c(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr);

    @DexIgnore
    /* renamed from: d */
    public abstract void mo9965d(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr);

    @DexIgnore
    /* renamed from: e */
    public abstract void mo9966e(java.lang.String str, java.lang.String str2, java.lang.Throwable... thArr);

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m5871a(java.lang.String str) {
        int length = str.length();
        java.lang.StringBuilder sb = new java.lang.StringBuilder(23);
        sb.append("WM-");
        int i = f4400b;
        if (length >= i) {
            sb.append(str.substring(0, i));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized com.fossil.blesdk.obfuscated.C1635dj m5870a() {
        com.fossil.blesdk.obfuscated.C1635dj djVar;
        synchronized (com.fossil.blesdk.obfuscated.C1635dj.class) {
            if (f4399a == null) {
                f4399a = new com.fossil.blesdk.obfuscated.C1635dj.C1636a(3);
            }
            djVar = f4399a;
        }
        return djVar;
    }
}
