package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseIntArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ui extends ti {
    @DexIgnore
    public /* final */ SparseIntArray a;
    @DexIgnore
    public /* final */ Parcel b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexIgnore
    public ui(Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "");
    }

    @DexIgnore
    public boolean a(int i) {
        int d2 = d(i);
        if (d2 == -1) {
            return false;
        }
        this.b.setDataPosition(d2);
        return true;
    }

    @DexIgnore
    public void b(int i) {
        a();
        this.f = i;
        this.a.put(i, this.b.dataPosition());
        c(0);
        c(i);
    }

    @DexIgnore
    public void c(int i) {
        this.b.writeInt(i);
    }

    @DexIgnore
    public final int d(int i) {
        int readInt;
        do {
            int i2 = this.g;
            if (i2 >= this.d) {
                return -1;
            }
            this.b.setDataPosition(i2);
            int readInt2 = this.b.readInt();
            readInt = this.b.readInt();
            this.g += readInt2;
        } while (readInt != i);
        return this.b.dataPosition();
    }

    @DexIgnore
    public int e() {
        return this.b.readInt();
    }

    @DexIgnore
    public <T extends Parcelable> T f() {
        return this.b.readParcelable(ui.class.getClassLoader());
    }

    @DexIgnore
    public String g() {
        return this.b.readString();
    }

    @DexIgnore
    public ui(Parcel parcel, int i, int i2, String str) {
        this.a = new SparseIntArray();
        this.f = -1;
        this.g = 0;
        this.b = parcel;
        this.c = i;
        this.d = i2;
        this.g = this.c;
        this.e = str;
    }

    @DexIgnore
    public void a() {
        int i = this.f;
        if (i >= 0) {
            int i2 = this.a.get(i);
            int dataPosition = this.b.dataPosition();
            this.b.setDataPosition(i2);
            this.b.writeInt(dataPosition - i2);
            this.b.setDataPosition(dataPosition);
        }
    }

    @DexIgnore
    public ti b() {
        Parcel parcel = this.b;
        int dataPosition = parcel.dataPosition();
        int i = this.g;
        if (i == this.c) {
            i = this.d;
        }
        return new ui(parcel, dataPosition, i, this.e + "  ");
    }

    @DexIgnore
    public byte[] d() {
        int readInt = this.b.readInt();
        if (readInt < 0) {
            return null;
        }
        byte[] bArr = new byte[readInt];
        this.b.readByteArray(bArr);
        return bArr;
    }

    @DexIgnore
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.b.writeInt(bArr.length);
            this.b.writeByteArray(bArr);
            return;
        }
        this.b.writeInt(-1);
    }

    @DexIgnore
    public void a(String str) {
        this.b.writeString(str);
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        this.b.writeParcelable(parcelable, 0);
    }
}
