package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yg */
public class C3362yg implements com.fossil.blesdk.obfuscated.C3436zg {

    @DexIgnore
    /* renamed from: f */
    public static java.lang.Class<?> f11240f;

    @DexIgnore
    /* renamed from: g */
    public static boolean f11241g;

    @DexIgnore
    /* renamed from: h */
    public static java.lang.reflect.Method f11242h;

    @DexIgnore
    /* renamed from: i */
    public static boolean f11243i;

    @DexIgnore
    /* renamed from: j */
    public static java.lang.reflect.Method f11244j;

    @DexIgnore
    /* renamed from: k */
    public static boolean f11245k;

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.view.View f11246e;

    @DexIgnore
    public C3362yg(android.view.View view) {
        this.f11246e = view;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3436zg m16839a(android.view.View view, android.view.ViewGroup viewGroup, android.graphics.Matrix matrix) {
        m16840a();
        java.lang.reflect.Method method = f11242h;
        if (method != null) {
            try {
                return new com.fossil.blesdk.obfuscated.C3362yg((android.view.View) method.invoke((java.lang.Object) null, new java.lang.Object[]{view, viewGroup, matrix}));
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m16842b() {
        if (!f11241g) {
            try {
                f11240f = java.lang.Class.forName("android.view.GhostView");
            } catch (java.lang.ClassNotFoundException e) {
                android.util.Log.i("GhostViewApi21", "Failed to retrieve GhostView class", e);
            }
            f11241g = true;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m16843c() {
        if (!f11245k) {
            try {
                m16842b();
                f11244j = f11240f.getDeclaredMethod("removeGhost", new java.lang.Class[]{android.view.View.class});
                f11244j.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", e);
            }
            f11245k = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17702a(android.view.ViewGroup viewGroup, android.view.View view) {
    }

    @DexIgnore
    public void setVisibility(int i) {
        this.f11246e.setVisibility(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16841a(android.view.View view) {
        m16843c();
        java.lang.reflect.Method method = f11244j;
        if (method != null) {
            try {
                method.invoke((java.lang.Object) null, new java.lang.Object[]{view});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16840a() {
        if (!f11243i) {
            try {
                m16842b();
                f11242h = f11240f.getDeclaredMethod("addGhost", new java.lang.Class[]{android.view.View.class, android.view.ViewGroup.class, android.graphics.Matrix.class});
                f11242h.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("GhostViewApi21", "Failed to retrieve addGhost method", e);
            }
            f11243i = true;
        }
    }
}
