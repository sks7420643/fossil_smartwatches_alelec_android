package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.td */
public class C2958td {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.td$a")
    /* renamed from: com.fossil.blesdk.obfuscated.td$a */
    public static class C2959a extends com.fossil.blesdk.obfuscated.C2314le.C2316b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2871sd f9637a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ int f9638b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2871sd f9639c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2314le.C2318d f9640d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ int f9641e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ int f9642f;

        @DexIgnore
        public C2959a(com.fossil.blesdk.obfuscated.C2871sd sdVar, int i, com.fossil.blesdk.obfuscated.C2871sd sdVar2, com.fossil.blesdk.obfuscated.C2314le.C2318d dVar, int i2, int i3) {
            this.f9637a = sdVar;
            this.f9638b = i;
            this.f9639c = sdVar2;
            this.f9640d = dVar;
            this.f9641e = i2;
            this.f9642f = i3;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13253a() {
            return this.f9642f;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo13255b() {
            return this.f9641e;
        }

        @DexIgnore
        /* renamed from: c */
        public java.lang.Object mo13257c(int i, int i2) {
            java.lang.Object obj = this.f9637a.get(i + this.f9638b);
            com.fossil.blesdk.obfuscated.C2871sd sdVar = this.f9639c;
            java.lang.Object obj2 = sdVar.get(i2 + sdVar.mo15931e());
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.f9640d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13254a(int i, int i2) {
            java.lang.Object obj = this.f9637a.get(i + this.f9638b);
            com.fossil.blesdk.obfuscated.C2871sd sdVar = this.f9639c;
            java.lang.Object obj2 = sdVar.get(i2 + sdVar.mo15931e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.f9640d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo13256b(int i, int i2) {
            java.lang.Object obj = this.f9637a.get(i + this.f9638b);
            com.fossil.blesdk.obfuscated.C2871sd sdVar = this.f9639c;
            java.lang.Object obj2 = sdVar.get(i2 + sdVar.mo15931e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.f9640d.areItemsTheSame(obj, obj2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.td$b")
    /* renamed from: com.fossil.blesdk.obfuscated.td$b */
    public static class C2960b implements com.fossil.blesdk.obfuscated.C3034ue {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f9643a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C3034ue f9644b;

        @DexIgnore
        public C2960b(int i, com.fossil.blesdk.obfuscated.C3034ue ueVar) {
            this.f9643a = i;
            this.f9644b = ueVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11199a(int i, int i2) {
            com.fossil.blesdk.obfuscated.C3034ue ueVar = this.f9644b;
            int i3 = this.f9643a;
            ueVar.mo11199a(i + i3, i2 + i3);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo11201b(int i, int i2) {
            this.f9644b.mo11201b(i + this.f9643a, i2);
        }

        @DexIgnore
        /* renamed from: c */
        public void mo11202c(int i, int i2) {
            this.f9644b.mo11202c(i + this.f9643a, i2);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11200a(int i, int i2, java.lang.Object obj) {
            this.f9644b.mo11200a(i + this.f9643a, i2, obj);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C2314le.C2317c m14152a(com.fossil.blesdk.obfuscated.C2871sd<T> sdVar, com.fossil.blesdk.obfuscated.C2871sd<T> sdVar2, com.fossil.blesdk.obfuscated.C2314le.C2318d<T> dVar) {
        int a = sdVar.mo15910a();
        int a2 = sdVar2.mo15910a();
        com.fossil.blesdk.obfuscated.C2958td.C2959a aVar = new com.fossil.blesdk.obfuscated.C2958td.C2959a(sdVar, a, sdVar2, dVar, (sdVar.size() - a) - sdVar.mo15921b(), (sdVar2.size() - a2) - sdVar2.mo15921b());
        return com.fossil.blesdk.obfuscated.C2314le.m10140a(aVar, true);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> void m14153a(com.fossil.blesdk.obfuscated.C3034ue ueVar, com.fossil.blesdk.obfuscated.C2871sd<T> sdVar, com.fossil.blesdk.obfuscated.C2871sd<T> sdVar2, com.fossil.blesdk.obfuscated.C2314le.C2317c cVar) {
        int b = sdVar.mo15921b();
        int b2 = sdVar2.mo15921b();
        int a = sdVar.mo15910a();
        int a2 = sdVar2.mo15910a();
        if (b == 0 && b2 == 0 && a == 0 && a2 == 0) {
            cVar.mo13261a(ueVar);
            return;
        }
        if (b > b2) {
            int i = b - b2;
            ueVar.mo11202c(sdVar.size() - i, i);
        } else if (b < b2) {
            ueVar.mo11201b(sdVar.size(), b2 - b);
        }
        if (a > a2) {
            ueVar.mo11202c(0, a - a2);
        } else if (a < a2) {
            ueVar.mo11201b(0, a2 - a);
        }
        if (a2 != 0) {
            cVar.mo13261a((com.fossil.blesdk.obfuscated.C3034ue) new com.fossil.blesdk.obfuscated.C2958td.C2960b(a2, ueVar));
        } else {
            cVar.mo13261a(ueVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14151a(com.fossil.blesdk.obfuscated.C2314le.C2317c cVar, com.fossil.blesdk.obfuscated.C2871sd sdVar, com.fossil.blesdk.obfuscated.C2871sd sdVar2, int i) {
        int a = sdVar.mo15910a();
        int i2 = i - a;
        int size = (sdVar.size() - a) - sdVar.mo15921b();
        if (i2 >= 0 && i2 < size) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 / 2) * (i3 % 2 == 1 ? -1 : 1)) + i2;
                if (i4 >= 0 && i4 < sdVar.mo15938k()) {
                    int a2 = cVar.mo13258a(i4);
                    if (a2 != -1) {
                        return a2 + sdVar2.mo15931e();
                    }
                }
            }
        }
        return java.lang.Math.max(0, java.lang.Math.min(i, sdVar2.size() - 1));
    }
}
