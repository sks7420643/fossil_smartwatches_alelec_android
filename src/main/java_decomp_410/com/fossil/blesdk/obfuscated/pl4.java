package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.net.ssl.SSLSocket;
import okhttp3.TlsVersion;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pl4 {
    @DexIgnore
    public static /* final */ ml4[] e; // = {ml4.q, ml4.r, ml4.s, ml4.t, ml4.u, ml4.k, ml4.m, ml4.l, ml4.n, ml4.p, ml4.o};
    @DexIgnore
    public static /* final */ ml4[] f; // = {ml4.q, ml4.r, ml4.s, ml4.t, ml4.u, ml4.k, ml4.m, ml4.l, ml4.n, ml4.p, ml4.o, ml4.i, ml4.j, ml4.g, ml4.h, ml4.e, ml4.f, ml4.d};
    @DexIgnore
    public static /* final */ pl4 g;
    @DexIgnore
    public static /* final */ pl4 h; // = new a(false).a();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ String[] d;

    /*
    static {
        a aVar = new a(true);
        aVar.a(e);
        aVar.a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2);
        aVar.a(true);
        aVar.a();
        a aVar2 = new a(true);
        aVar2.a(f);
        aVar2.a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0);
        aVar2.a(true);
        g = aVar2.a();
        a aVar3 = new a(true);
        aVar3.a(f);
        aVar3.a(TlsVersion.TLS_1_0);
        aVar3.a(true);
        aVar3.a();
    }
    */

    @DexIgnore
    public pl4(a aVar) {
        this.a = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.b = aVar.d;
    }

    @DexIgnore
    public List<ml4> a() {
        String[] strArr = this.c;
        if (strArr != null) {
            return ml4.a(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }

    @DexIgnore
    public boolean c() {
        return this.b;
    }

    @DexIgnore
    public List<TlsVersion> d() {
        String[] strArr = this.d;
        if (strArr != null) {
            return TlsVersion.forJavaNames(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof pl4)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        pl4 pl4 = (pl4) obj;
        boolean z = this.a;
        if (z != pl4.a) {
            return false;
        }
        return !z || (Arrays.equals(this.c, pl4.c) && Arrays.equals(this.d, pl4.d) && this.b == pl4.b);
    }

    @DexIgnore
    public int hashCode() {
        if (this.a) {
            return ((((527 + Arrays.hashCode(this.c)) * 31) + Arrays.hashCode(this.d)) * 31) + (this.b ^ true ? 1 : 0);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        if (!this.a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.c != null ? a().toString() : str;
        if (this.d != null) {
            str = d().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.b + ")";
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public a a(ml4... ml4Arr) {
            if (this.a) {
                String[] strArr = new String[ml4Arr.length];
                for (int i = 0; i < ml4Arr.length; i++) {
                    strArr[i] = ml4Arr[i].a;
                }
                a(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public a b(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        @DexIgnore
        public a(pl4 pl4) {
            this.a = pl4.a;
            this.b = pl4.c;
            this.c = pl4.d;
            this.d = pl4.b;
        }

        @DexIgnore
        public a a(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        @DexIgnore
        public a a(TlsVersion... tlsVersionArr) {
            if (this.a) {
                String[] strArr = new String[tlsVersionArr.length];
                for (int i = 0; i < tlsVersionArr.length; i++) {
                    strArr[i] = tlsVersionArr[i].javaName;
                }
                b(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        @DexIgnore
        public a a(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public pl4 a() {
            return new pl4(this);
        }
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        pl4 b2 = b(sSLSocket, z);
        String[] strArr = b2.d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = b2.c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    @DexIgnore
    public final pl4 b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2;
        if (this.c != null) {
            strArr = jm4.a((Comparator<? super String>) ml4.b, sSLSocket.getEnabledCipherSuites(), this.c);
        } else {
            strArr = sSLSocket.getEnabledCipherSuites();
        }
        if (this.d != null) {
            strArr2 = jm4.a((Comparator<? super String>) jm4.p, sSLSocket.getEnabledProtocols(), this.d);
        } else {
            strArr2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int a2 = jm4.a(ml4.b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && a2 != -1) {
            strArr = jm4.a(strArr, supportedCipherSuites[a2]);
        }
        a aVar = new a(this);
        aVar.a(strArr);
        aVar.b(strArr2);
        return aVar.a();
    }

    @DexIgnore
    public boolean a(SSLSocket sSLSocket) {
        if (!this.a) {
            return false;
        }
        String[] strArr = this.d;
        if (strArr != null && !jm4.b(jm4.p, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.c;
        if (strArr2 == null || jm4.b(ml4.b, strArr2, sSLSocket.getEnabledCipherSuites())) {
            return true;
        }
        return false;
    }
}
