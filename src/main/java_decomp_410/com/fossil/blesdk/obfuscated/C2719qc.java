package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qc */
public class C2719qc<D> {
    @DexIgnore
    public boolean mAbandoned; // = false;
    @DexIgnore
    public boolean mContentChanged; // = false;
    @DexIgnore
    public android.content.Context mContext;
    @DexIgnore
    public int mId;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> mListener;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2719qc.C2721b<D> mOnLoadCanceledListener;
    @DexIgnore
    public boolean mProcessingChange; // = false;
    @DexIgnore
    public boolean mReset; // = true;
    @DexIgnore
    public boolean mStarted; // = false;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qc$a */
    public final class C2720a extends android.database.ContentObserver {
        @DexIgnore
        public C2720a() {
            super(new android.os.Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            com.fossil.blesdk.obfuscated.C2719qc.this.onContentChanged();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.qc$b */
    public interface C2721b<D> {
        @DexIgnore
        /* renamed from: a */
        void mo15175a(com.fossil.blesdk.obfuscated.C2719qc<D> qcVar);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.qc$c */
    public interface C2722c<D> {
        @DexIgnore
        /* renamed from: a */
        void mo13909a(com.fossil.blesdk.obfuscated.C2719qc<D> qcVar, D d);
    }

    @DexIgnore
    public C2719qc(android.content.Context context) {
        this.mContext = context.getApplicationContext();
    }

    @DexIgnore
    public void abandon() {
        this.mAbandoned = true;
        onAbandon();
    }

    @DexIgnore
    public boolean cancelLoad() {
        return onCancelLoad();
    }

    @DexIgnore
    public void commitContentChanged() {
        this.mProcessingChange = false;
    }

    @DexIgnore
    public java.lang.String dataToString(D d) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(64);
        com.fossil.blesdk.obfuscated.C1539c8.m5321a(d, sb);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void deliverCancellation() {
        com.fossil.blesdk.obfuscated.C2719qc.C2721b<D> bVar = this.mOnLoadCanceledListener;
        if (bVar != null) {
            bVar.mo15175a(this);
        }
    }

    @DexIgnore
    public void deliverResult(D d) {
        com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> cVar = this.mListener;
        if (cVar != null) {
            cVar.mo13909a(this, d);
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    public void dump(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.mId);
        printWriter.print(" mListener=");
        printWriter.println(this.mListener);
        if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.mStarted);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.mContentChanged);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.mProcessingChange);
        }
        if (this.mAbandoned || this.mReset) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.mAbandoned);
            printWriter.print(" mReset=");
            printWriter.println(this.mReset);
        }
    }

    @DexIgnore
    public void forceLoad() {
        onForceLoad();
    }

    @DexIgnore
    public android.content.Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public boolean isAbandoned() {
        return this.mAbandoned;
    }

    @DexIgnore
    public boolean isReset() {
        return this.mReset;
    }

    @DexIgnore
    public boolean isStarted() {
        return this.mStarted;
    }

    @DexIgnore
    public void onAbandon() {
    }

    @DexIgnore
    public boolean onCancelLoad() {
        throw null;
    }

    @DexIgnore
    public void onContentChanged() {
        if (this.mStarted) {
            forceLoad();
        } else {
            this.mContentChanged = true;
        }
    }

    @DexIgnore
    public void onForceLoad() {
    }

    @DexIgnore
    public void onReset() {
    }

    @DexIgnore
    public void onStartLoading() {
    }

    @DexIgnore
    public void onStopLoading() {
    }

    @DexIgnore
    public void registerListener(int i, com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> cVar) {
        if (this.mListener == null) {
            this.mListener = cVar;
            this.mId = i;
            return;
        }
        throw new java.lang.IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void registerOnLoadCanceledListener(com.fossil.blesdk.obfuscated.C2719qc.C2721b<D> bVar) {
        if (this.mOnLoadCanceledListener == null) {
            this.mOnLoadCanceledListener = bVar;
            return;
        }
        throw new java.lang.IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void reset() {
        onReset();
        this.mReset = true;
        this.mStarted = false;
        this.mAbandoned = false;
        this.mContentChanged = false;
        this.mProcessingChange = false;
    }

    @DexIgnore
    public void rollbackContentChanged() {
        if (this.mProcessingChange) {
            onContentChanged();
        }
    }

    @DexIgnore
    public final void startLoading() {
        this.mStarted = true;
        this.mReset = false;
        this.mAbandoned = false;
        onStartLoading();
    }

    @DexIgnore
    public void stopLoading() {
        this.mStarted = false;
        onStopLoading();
    }

    @DexIgnore
    public boolean takeContentChanged() {
        boolean z = this.mContentChanged;
        this.mContentChanged = false;
        this.mProcessingChange |= z;
        return z;
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder(64);
        com.fossil.blesdk.obfuscated.C1539c8.m5321a(this, sb);
        sb.append(" id=");
        sb.append(this.mId);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void unregisterListener(com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> cVar) {
        com.fossil.blesdk.obfuscated.C2719qc.C2722c<D> cVar2 = this.mListener;
        if (cVar2 == null) {
            throw new java.lang.IllegalStateException("No listener register");
        } else if (cVar2 == cVar) {
            this.mListener = null;
        } else {
            throw new java.lang.IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    @DexIgnore
    public void unregisterOnLoadCanceledListener(com.fossil.blesdk.obfuscated.C2719qc.C2721b<D> bVar) {
        com.fossil.blesdk.obfuscated.C2719qc.C2721b<D> bVar2 = this.mOnLoadCanceledListener;
        if (bVar2 == null) {
            throw new java.lang.IllegalStateException("No listener register");
        } else if (bVar2 == bVar) {
            this.mOnLoadCanceledListener = null;
        } else {
            throw new java.lang.IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }
}
