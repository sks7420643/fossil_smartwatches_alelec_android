package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lc4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Method a;

        /* JADX WARNING: Removed duplicated region for block: B:13:0x0047 A[EDGE_INSN: B:13:0x0047->B:11:0x0047 ?: BREAK  , SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0043 A[LOOP:0: B:1:0x0013->B:9:0x0043, LOOP_END] */
        /*
        static {
            Method method;
            boolean z;
            new a();
            Class<Throwable> cls = Throwable.class;
            Method[] methods = cls.getMethods();
            kd4.a((Object) methods, "throwableClass.methods");
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    method = null;
                    break;
                }
                method = methods[i];
                kd4.a((Object) method, "it");
                if (kd4.a((Object) method.getName(), (Object) "addSuppressed")) {
                    Class[] parameterTypes = method.getParameterTypes();
                    kd4.a((Object) parameterTypes, "it.parameterTypes");
                    if (kd4.a((Object) (Class) za4.d(parameterTypes), (Object) cls)) {
                        z = true;
                        if (!z) {
                            break;
                        }
                        i++;
                    }
                }
                z = false;
                if (!z) {
                }
            }
            a = method;
        }
        */
    }

    @DexIgnore
    public void a(Throwable th, Throwable th2) {
        kd4.b(th, "cause");
        kd4.b(th2, "exception");
        Method method = a.a;
        if (method != null) {
            method.invoke(th, new Object[]{th2});
        }
    }
}
