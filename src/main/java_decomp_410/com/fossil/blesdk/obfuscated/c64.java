package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class c64<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements a64<i64>, f64, i64, z54 {
    @DexIgnore
    public /* final */ g64 r; // = new g64();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Result> implements Executor {
        @DexIgnore
        public /* final */ Executor e;
        @DexIgnore
        public /* final */ c64 f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c64$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.c64$a$a  reason: collision with other inner class name */
        public class C0083a extends e64<Result> {
            @DexIgnore
            public C0083a(Runnable runnable, Object obj) {
                super(runnable, obj);
            }

            @DexIgnore
            public <T extends a64<i64> & f64 & i64> T d() {
                return a.this.f;
            }
        }

        @DexIgnore
        public a(Executor executor, c64 c64) {
            this.e = executor;
            this.f = c64;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.e.execute(new C0083a(runnable, (Object) null));
        }
    }

    @DexIgnore
    public boolean b() {
        return ((a64) ((f64) g())).b();
    }

    @DexIgnore
    public Collection<i64> c() {
        return ((a64) ((f64) g())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return Priority.compareTo(this, obj);
    }

    @DexIgnore
    public <T extends a64<i64> & f64 & i64> T g() {
        return this.r;
    }

    @DexIgnore
    public final void a(ExecutorService executorService, Params... paramsArr) {
        super.a((Executor) new a(executorService, this), paramsArr);
    }

    @DexIgnore
    public void a(i64 i64) {
        if (d() == AsyncTask.Status.PENDING) {
            ((a64) ((f64) g())).a(i64);
            return;
        }
        throw new IllegalStateException("Must not add Dependency after task is running");
    }

    @DexIgnore
    public void a(boolean z) {
        ((i64) ((f64) g())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((i64) ((f64) g())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((i64) ((f64) g())).a(th);
    }
}
