package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tz */
public class C2997tz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ long f9793a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ long f9794b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f9795c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.String f9796d;

    @DexIgnore
    public C2997tz(long j, long j2, java.lang.String str, java.lang.String str2) {
        this.f9793a = j;
        this.f9794b = j2;
        this.f9795c = str;
        this.f9796d = str2;
    }
}
