package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class gw1 implements Executor {
    @DexIgnore
    public static /* final */ Executor e; // = new gw1();

    @DexIgnore
    public final void execute(Runnable runnable) {
        runnable.run();
    }
}
