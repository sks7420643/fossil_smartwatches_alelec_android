package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ws2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sn3 extends zr2 implements wn3 {
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public tr3<m92> j;
    @DexIgnore
    public vn3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final sn3 a() {
            return new sn3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sn3 e;

        @DexIgnore
        public b(sn3 sn3) {
            this.e = sn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            sn3.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sn3 e;

        @DexIgnore
        public c(sn3 sn3) {
            this.e = sn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        }
    }

    @DexIgnore
    public static final /* synthetic */ vn3 a(sn3 sn3) {
        vn3 vn3 = sn3.k;
        if (vn3 != null) {
            return vn3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        vn3 vn3 = this.k;
        if (vn3 != null) {
            vn3.h();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        m92 m92 = (m92) qa.a(layoutInflater, R.layout.fragment_allow_notification_service, viewGroup, false, O0());
        FlexibleTextView flexibleTextView = m92.q;
        kd4.a((Object) flexibleTextView, "binding.ftvDescription");
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidInAppNotication_Text__BrandNeedsAccessToYourNotification);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026AccessToYourNotification)");
        Object[] objArr = {PortfolioApp.W.c().i()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        m92.s.setOnClickListener(new b(this));
        m92.r.setOnClickListener(new c(this));
        this.j = new tr3<>(this, m92);
        tr3<m92> tr3 = this.j;
        if (tr3 != null) {
            m92 a3 = tr3.a();
            if (a3 != null) {
                kd4.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        vn3 vn3 = this.k;
        if (vn3 != null) {
            vn3.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vn3 vn3 = this.k;
        if (vn3 != null) {
            vn3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void t(List<ws2.c> list) {
        kd4.b(list, "listPermissionModel");
    }

    @DexIgnore
    public void a(vn3 vn3) {
        kd4.b(vn3, "presenter");
        this.k = vn3;
    }
}
