package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fh */
public class C1802fh {
    @DexIgnore
    /* renamed from: a */
    public static android.animation.PropertyValuesHolder m7014a(android.util.Property<?, android.graphics.PointF> property, android.graphics.Path path) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.animation.PropertyValuesHolder.ofObject(property, (android.animation.TypeConverter) null, path);
        }
        return android.animation.PropertyValuesHolder.ofFloat(new com.fossil.blesdk.obfuscated.C1731eh(property, path), new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
    }
}
