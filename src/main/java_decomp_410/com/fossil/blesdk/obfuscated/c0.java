package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import androidx.appcompat.app.AlertController;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c0 extends f0 implements DialogInterface {
    @DexIgnore
    public /* final */ AlertController g; // = new AlertController(getContext(), this, getWindow());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ AlertController.f a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Context context) {
            this(context, c0.b(context, 0));
        }

        @DexIgnore
        public a a(View view) {
            this.a.g = view;
            return this;
        }

        @DexIgnore
        public Context b() {
            return this.a.a;
        }

        @DexIgnore
        public c0 c() {
            c0 a2 = a();
            a2.show();
            return a2;
        }

        @DexIgnore
        public a(Context context, int i) {
            this.a = new AlertController.f(new ContextThemeWrapper(context, c0.b(context, i)));
            this.b = i;
        }

        @DexIgnore
        public a a(CharSequence charSequence) {
            this.a.h = charSequence;
            return this;
        }

        @DexIgnore
        public a b(CharSequence charSequence) {
            this.a.f = charSequence;
            return this;
        }

        @DexIgnore
        public a a(Drawable drawable) {
            this.a.d = drawable;
            return this;
        }

        @DexIgnore
        public a b(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.i = charSequence;
            fVar.k = onClickListener;
            return this;
        }

        @DexIgnore
        public a a(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.l = charSequence;
            fVar.n = onClickListener;
            return this;
        }

        @DexIgnore
        public a b(View view) {
            AlertController.f fVar = this.a;
            fVar.z = view;
            fVar.y = 0;
            fVar.E = false;
            return this;
        }

        @DexIgnore
        public a a(boolean z) {
            this.a.r = z;
            return this;
        }

        @DexIgnore
        public a a(DialogInterface.OnKeyListener onKeyListener) {
            this.a.u = onKeyListener;
            return this;
        }

        @DexIgnore
        public a a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            AlertController.f fVar = this.a;
            fVar.w = listAdapter;
            fVar.x = onClickListener;
            return this;
        }

        @DexIgnore
        public c0 a() {
            c0 c0Var = new c0(this.a.a, this.b);
            this.a.a(c0Var.g);
            c0Var.setCancelable(this.a.r);
            if (this.a.r) {
                c0Var.setCanceledOnTouchOutside(true);
            }
            c0Var.setOnCancelListener(this.a.s);
            c0Var.setOnDismissListener(this.a.t);
            DialogInterface.OnKeyListener onKeyListener = this.a.u;
            if (onKeyListener != null) {
                c0Var.setOnKeyListener(onKeyListener);
            }
            return c0Var;
        }
    }

    @DexIgnore
    public c0(Context context, int i) {
        super(context, b(context, i));
    }

    @DexIgnore
    public static int b(Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(r.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g.a();
    }

    @DexIgnore
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.g.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @DexIgnore
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.g.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @DexIgnore
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.g.b(charSequence);
    }

    @DexIgnore
    public Button b(int i) {
        return this.g.a(i);
    }
}
