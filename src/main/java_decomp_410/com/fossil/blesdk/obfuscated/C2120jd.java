package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jd */
public abstract class C2120jd<Key, Value> extends com.fossil.blesdk.obfuscated.C2307ld<Key, Value> {
    @DexIgnore
    public abstract void dispatchLoadAfter(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadBefore(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadInitial(Key key, int i, int i2, boolean z, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar);

    @DexIgnore
    public abstract Key getKey(int i, Value value);

    @DexIgnore
    public boolean isContiguous() {
        return true;
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return true;
    }
}
