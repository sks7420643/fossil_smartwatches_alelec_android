package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pe4<T> implements re4<T> {
    @DexIgnore
    public /* final */ re4<T> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ xc4<T, Boolean> c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, rd4 {
        @DexIgnore
        public /* final */ Iterator<T> e;
        @DexIgnore
        public int f; // = -1;
        @DexIgnore
        public T g;
        @DexIgnore
        public /* final */ /* synthetic */ pe4 h;

        @DexIgnore
        public a(pe4 pe4) {
            this.h = pe4;
            this.e = pe4.a.iterator();
        }

        @DexIgnore
        public final void a() {
            while (this.e.hasNext()) {
                T next = this.e.next();
                if (((Boolean) this.h.c.invoke(next)).booleanValue() == this.h.b) {
                    this.g = next;
                    this.f = 1;
                    return;
                }
            }
            this.f = 0;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.f == -1) {
                a();
            }
            return this.f == 1;
        }

        @DexIgnore
        public T next() {
            if (this.f == -1) {
                a();
            }
            if (this.f != 0) {
                T t = this.g;
                this.g = null;
                this.f = -1;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public pe4(re4<? extends T> re4, boolean z, xc4<? super T, Boolean> xc4) {
        kd4.b(re4, "sequence");
        kd4.b(xc4, "predicate");
        this.a = re4;
        this.b = z;
        this.c = xc4;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }
}
