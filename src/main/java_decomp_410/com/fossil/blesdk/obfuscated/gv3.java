package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cv3;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.Proxy;
import java.net.ProxySelector;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gv3 implements Cloneable {
    @DexIgnore
    public static /* final */ List<Protocol> C; // = wv3.a((T[]) new Protocol[]{Protocol.HTTP_2, Protocol.SPDY_3, Protocol.HTTP_1_1});
    @DexIgnore
    public static /* final */ List<yu3> D; // = wv3.a((T[]) new yu3[]{yu3.f, yu3.g, yu3.h});
    @DexIgnore
    public static SSLSocketFactory E;
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public /* final */ vv3 e;
    @DexIgnore
    public av3 f;
    @DexIgnore
    public Proxy g;
    @DexIgnore
    public List<Protocol> h;
    @DexIgnore
    public List<yu3> i;
    @DexIgnore
    public /* final */ List<ev3> j;
    @DexIgnore
    public /* final */ List<ev3> k;
    @DexIgnore
    public ProxySelector l;
    @DexIgnore
    public CookieHandler m;
    @DexIgnore
    public qv3 n;
    @DexIgnore
    public ru3 o;
    @DexIgnore
    public SocketFactory p;
    @DexIgnore
    public SSLSocketFactory q;
    @DexIgnore
    public HostnameVerifier r;
    @DexIgnore
    public uu3 s;
    @DexIgnore
    public qu3 t;
    @DexIgnore
    public xu3 u;
    @DexIgnore
    public sv3 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pv3 {
        @DexIgnore
        public dx3 a(wu3 wu3, uw3 uw3) throws IOException {
            return wu3.a(uw3);
        }

        @DexIgnore
        public void b(wu3 wu3, uw3 uw3) {
            wu3.a((Object) uw3);
        }

        @DexIgnore
        public int c(wu3 wu3) {
            return wu3.m();
        }

        @DexIgnore
        public boolean a(wu3 wu3) {
            return wu3.a();
        }

        @DexIgnore
        public boolean b(wu3 wu3) {
            return wu3.l();
        }

        @DexIgnore
        public vv3 c(gv3 gv3) {
            return gv3.G();
        }

        @DexIgnore
        public void a(wu3 wu3, Protocol protocol) {
            wu3.a(protocol);
        }

        @DexIgnore
        public sv3 b(gv3 gv3) {
            return gv3.v;
        }

        @DexIgnore
        public void a(cv3.b bVar, String str) {
            bVar.a(str);
        }

        @DexIgnore
        public qv3 a(gv3 gv3) {
            return gv3.E();
        }

        @DexIgnore
        public void a(xu3 xu3, wu3 wu3) {
            xu3.b(wu3);
        }

        @DexIgnore
        public void a(gv3 gv3, wu3 wu3, uw3 uw3, hv3 hv3) throws RouteException {
            wu3.a(gv3, (Object) uw3, hv3);
        }

        @DexIgnore
        public void a(yu3 yu3, SSLSocket sSLSocket, boolean z) {
            yu3.a(sSLSocket, z);
        }
    }

    /*
    static {
        pv3.b = new a();
    }
    */

    @DexIgnore
    public gv3() {
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.w = true;
        this.x = true;
        this.y = true;
        this.z = 10000;
        this.A = 10000;
        this.B = 10000;
        this.e = new vv3();
        this.f = new av3();
    }

    @DexIgnore
    public SocketFactory A() {
        return this.p;
    }

    @DexIgnore
    public SSLSocketFactory B() {
        return this.q;
    }

    @DexIgnore
    public int C() {
        return this.B;
    }

    @DexIgnore
    public List<ev3> D() {
        return this.j;
    }

    @DexIgnore
    public qv3 E() {
        return this.n;
    }

    @DexIgnore
    public List<ev3> F() {
        return this.k;
    }

    @DexIgnore
    public vv3 G() {
        return this.e;
    }

    @DexIgnore
    public void b(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.A = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public void c(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.B = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public int d() {
        return this.z;
    }

    @DexIgnore
    public xu3 e() {
        return this.u;
    }

    @DexIgnore
    public List<yu3> f() {
        return this.i;
    }

    @DexIgnore
    public CookieHandler g() {
        return this.m;
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:4|5|6|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    public final synchronized SSLSocketFactory h() {
        if (E == null) {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init((KeyManager[]) null, (TrustManager[]) null, (SecureRandom) null);
            E = instance.getSocketFactory();
            throw new AssertionError();
        }
        return E;
    }

    @DexIgnore
    public av3 i() {
        return this.f;
    }

    @DexIgnore
    public boolean j() {
        return this.x;
    }

    @DexIgnore
    public boolean k() {
        return this.w;
    }

    @DexIgnore
    public HostnameVerifier l() {
        return this.r;
    }

    @DexIgnore
    public List<Protocol> m() {
        return this.h;
    }

    @DexIgnore
    public Proxy w() {
        return this.g;
    }

    @DexIgnore
    public ProxySelector x() {
        return this.l;
    }

    @DexIgnore
    public int y() {
        return this.A;
    }

    @DexIgnore
    public boolean z() {
        return this.y;
    }

    @DexIgnore
    public void a(long j2, TimeUnit timeUnit) {
        int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("timeout < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j2);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException("Timeout too large.");
            } else if (millis != 0 || i2 <= 0) {
                this.z = (int) millis;
            } else {
                throw new IllegalArgumentException("Timeout too small.");
            }
        } else {
            throw new IllegalArgumentException("unit == null");
        }
    }

    @DexIgnore
    public gv3 clone() {
        return new gv3(this);
    }

    @DexIgnore
    public qu3 b() {
        return this.t;
    }

    @DexIgnore
    public uu3 c() {
        return this.s;
    }

    @DexIgnore
    public gv3 a(ru3 ru3) {
        this.o = ru3;
        this.n = null;
        return this;
    }

    @DexIgnore
    public tu3 a(hv3 hv3) {
        return new tu3(this, hv3);
    }

    @DexIgnore
    public gv3 a() {
        gv3 gv3 = new gv3(this);
        if (gv3.l == null) {
            gv3.l = ProxySelector.getDefault();
        }
        if (gv3.m == null) {
            gv3.m = CookieHandler.getDefault();
        }
        if (gv3.p == null) {
            gv3.p = SocketFactory.getDefault();
        }
        if (gv3.q == null) {
            gv3.q = h();
        }
        if (gv3.r == null) {
            gv3.r = gx3.a;
        }
        if (gv3.s == null) {
            gv3.s = uu3.b;
        }
        if (gv3.t == null) {
            gv3.t = nw3.a;
        }
        if (gv3.u == null) {
            gv3.u = xu3.c();
        }
        if (gv3.h == null) {
            gv3.h = C;
        }
        if (gv3.i == null) {
            gv3.i = D;
        }
        if (gv3.v == null) {
            gv3.v = sv3.a;
        }
        return gv3;
    }

    @DexIgnore
    public gv3(gv3 gv3) {
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.w = true;
        this.x = true;
        this.y = true;
        this.z = 10000;
        this.A = 10000;
        this.B = 10000;
        this.e = gv3.e;
        this.f = gv3.f;
        this.g = gv3.g;
        this.h = gv3.h;
        this.i = gv3.i;
        this.j.addAll(gv3.j);
        this.k.addAll(gv3.k);
        this.l = gv3.l;
        this.m = gv3.m;
        this.o = gv3.o;
        ru3 ru3 = this.o;
        this.n = ru3 != null ? ru3.a : gv3.n;
        this.p = gv3.p;
        this.q = gv3.q;
        this.r = gv3.r;
        this.s = gv3.s;
        this.t = gv3.t;
        this.u = gv3.u;
        this.v = gv3.v;
        this.w = gv3.w;
        this.x = gv3.x;
        this.y = gv3.y;
        this.z = gv3.z;
        this.A = gv3.A;
        this.B = gv3.B;
    }
}
