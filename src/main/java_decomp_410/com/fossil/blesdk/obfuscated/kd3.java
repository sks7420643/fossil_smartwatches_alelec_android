package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kd3 {
    @DexIgnore
    public /* final */ fd3 a;
    @DexIgnore
    public /* final */ ud3 b;
    @DexIgnore
    public /* final */ pd3 c;

    @DexIgnore
    public kd3(fd3 fd3, ud3 ud3, pd3 pd3) {
        kd4.b(fd3, "mSleepOverviewDayView");
        kd4.b(ud3, "mSleepOverviewWeekView");
        kd4.b(pd3, "mSleepOverviewMonthView");
        this.a = fd3;
        this.b = ud3;
        this.c = pd3;
    }

    @DexIgnore
    public final fd3 a() {
        return this.a;
    }

    @DexIgnore
    public final pd3 b() {
        return this.c;
    }

    @DexIgnore
    public final ud3 c() {
        return this.b;
    }
}
