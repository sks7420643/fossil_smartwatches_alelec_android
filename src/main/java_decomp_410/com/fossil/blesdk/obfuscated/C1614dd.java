package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dd */
public class C1614dd implements com.fossil.blesdk.obfuscated.C1474bd {

    @DexIgnore
    /* renamed from: a */
    public java.lang.String f4355a;

    @DexIgnore
    /* renamed from: b */
    public int f4356b;

    @DexIgnore
    /* renamed from: c */
    public int f4357c;

    @DexIgnore
    public C1614dd(java.lang.String str, int i, int i2) {
        this.f4355a = str;
        this.f4356b = i;
        this.f4357c = i2;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1614dd)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1614dd ddVar = (com.fossil.blesdk.obfuscated.C1614dd) obj;
        if (android.text.TextUtils.equals(this.f4355a, ddVar.f4355a) && this.f4356b == ddVar.f4356b && this.f4357c == ddVar.f4357c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return com.fossil.blesdk.obfuscated.C1706e8.m6325a(this.f4355a, java.lang.Integer.valueOf(this.f4356b), java.lang.Integer.valueOf(this.f4357c));
    }
}
