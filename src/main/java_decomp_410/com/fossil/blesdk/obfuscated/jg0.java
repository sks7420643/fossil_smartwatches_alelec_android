package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jg0 implements ne0<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ ef0 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ ge0 c;
    @DexIgnore
    public /* final */ /* synthetic */ eg0 d;

    @DexIgnore
    public jg0(eg0 eg0, ef0 ef0, boolean z, ge0 ge0) {
        this.d = eg0;
        this.a = ef0;
        this.b = z;
        this.c = ge0;
    }

    @DexIgnore
    public final /* synthetic */ void onResult(me0 me0) {
        Status status = (Status) me0;
        cc0.a(this.d.g).e();
        if (status.L() && this.d.g()) {
            this.d.k();
        }
        this.a.a(status);
        if (this.b) {
            this.c.d();
        }
    }
}
