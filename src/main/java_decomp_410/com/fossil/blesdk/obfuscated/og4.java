package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class og4 {
    @DexIgnore
    public static final <T> Object a(Object obj) {
        if (Result.m9isSuccessimpl(obj)) {
            na4.a(obj);
            return obj;
        }
        Throwable r4 = Result.m6exceptionOrNullimpl(obj);
        if (r4 != null) {
            return new ng4(r4, false, 2, (fd4) null);
        }
        kd4.a();
        throw null;
    }
}
