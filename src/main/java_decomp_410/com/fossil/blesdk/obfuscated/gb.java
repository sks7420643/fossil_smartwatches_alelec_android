package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gb implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public ViewTreeObserver f;
    @DexIgnore
    public /* final */ Runnable g;

    @DexIgnore
    public gb(View view, Runnable runnable) {
        this.e = view;
        this.f = view.getViewTreeObserver();
        this.g = runnable;
    }

    @DexIgnore
    public static gb a(View view, Runnable runnable) {
        gb gbVar = new gb(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(gbVar);
        view.addOnAttachStateChangeListener(gbVar);
        return gbVar;
    }

    @DexIgnore
    public boolean onPreDraw() {
        a();
        this.g.run();
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        this.f = view.getViewTreeObserver();
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        a();
    }

    @DexIgnore
    public void a() {
        if (this.f.isAlive()) {
            this.f.removeOnPreDrawListener(this);
        } else {
            this.e.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.e.removeOnAttachStateChangeListener(this);
    }
}
