package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rd */
public abstract class C2800rd<T, VH extends androidx.recyclerview.widget.RecyclerView.ViewHolder> extends androidx.recyclerview.widget.RecyclerView.C0227g<VH> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2011id<T> f8913a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2011id.C2015c<T> f8914b; // = new com.fossil.blesdk.obfuscated.C2800rd.C2801a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rd$a */
    public class C2801a implements com.fossil.blesdk.obfuscated.C2011id.C2015c<T> {
        @DexIgnore
        public C2801a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11917a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2) {
            com.fossil.blesdk.obfuscated.C2800rd.this.mo15491a(qdVar2);
            com.fossil.blesdk.obfuscated.C2800rd.this.mo15492a(qdVar, qdVar2);
        }
    }

    @DexIgnore
    public C2800rd(com.fossil.blesdk.obfuscated.C2314le.C2318d<T> dVar) {
        this.f8913a = new com.fossil.blesdk.obfuscated.C2011id<>(this, dVar);
        this.f8913a.mo11907a(this.f8914b);
    }

    @DexIgnore
    /* renamed from: a */
    public T mo15490a(int i) {
        return this.f8913a.mo11906a(i);
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public void mo15491a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15492a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15493b(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar) {
        this.f8913a.mo11908a(qdVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.f8913a.mo11905a();
    }
}
