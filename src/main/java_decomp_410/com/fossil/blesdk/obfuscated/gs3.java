package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import com.portfolio.platform.ui.view.DashBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gs3 {
    @DexIgnore
    public static /* final */ a a; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(DashBar dashBar, long j) {
            kd4.b(dashBar, "dashBar");
            a(dashBar, new st3(4, 30, 20, 30), j);
        }

        @DexIgnore
        public final void b(DashBar dashBar, long j) {
            kd4.b(dashBar, "dashBar");
            a(dashBar, new st3(4, 10, 10, 20), j);
        }

        @DexIgnore
        public final void c(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            a(dashBar, new st3(z ? 4 : 3, 100, z ? 90 : 100, 100), j);
        }

        @DexIgnore
        public final void d(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 50 : 33;
            a(dashBar, new st3(i, i2, z ? 40 : i2, i2), j);
        }

        @DexIgnore
        public final void e(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 40 : 33;
            a(dashBar, new st3(i, i2, z ? 30 : 0, i2), j);
        }

        @DexIgnore
        public final void f(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 80 : 66;
            a(dashBar, new st3(i, i2, z ? 70 : i2, i2), j);
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 90 : 66;
            a(dashBar, new st3(i, i2, z ? 80 : i2, i2), j);
        }

        @DexIgnore
        public final void b(DashBar dashBar, boolean z, long j) {
            kd4.b(dashBar, "dashBar");
            int i = z ? 4 : 3;
            int i2 = z ? 60 : 33;
            a(dashBar, new st3(i, i2, z ? 50 : i2, i2), j);
        }

        @DexIgnore
        public final void a(DashBar dashBar, st3 st3, long j) {
            dashBar.setLength(st3.c());
            dashBar.setProgress(st3.d());
            if (st3.e()) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", new int[]{st3.b(), st3.a()});
                kd4.a((Object) ofInt, "progressAnimator");
                ofInt.setDuration(j);
                ofInt.start();
            }
        }
    }
}
