package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zq */
public class C3459zq extends com.fossil.blesdk.obfuscated.C2772qw<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C1438aq<?>> implements com.fossil.blesdk.obfuscated.C1440ar {

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1440ar.C1441a f11645d;

    @DexIgnore
    public C3459zq(long j) {
        super(j);
    }

    @DexIgnore
    /* renamed from: a */
    public /* bridge */ /* synthetic */ com.fossil.blesdk.obfuscated.C1438aq mo8905a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C1438aq aqVar) {
        return (com.fossil.blesdk.obfuscated.C1438aq) super.mo15393b(joVar, aqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15391a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        com.fossil.blesdk.obfuscated.C1440ar.C1441a aVar = this.f11645d;
        if (aVar != null && aqVar != null) {
            aVar.mo8909a(aqVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public /* bridge */ /* synthetic */ com.fossil.blesdk.obfuscated.C1438aq mo8904a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        return (com.fossil.blesdk.obfuscated.C1438aq) super.mo15396c(joVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8908a(com.fossil.blesdk.obfuscated.C1440ar.C1441a aVar) {
        this.f11645d = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15392b(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        if (aqVar == null) {
            return super.mo15392b(null);
        }
        return aqVar.mo8888b();
    }

    @DexIgnore
    @android.annotation.SuppressLint({"InlinedApi"})
    /* renamed from: a */
    public void mo8907a(int i) {
        if (i >= 40) {
            mo15389a();
        } else if (i >= 20 || i == 15) {
            mo15390a(mo15395c() / 2);
        }
    }
}
