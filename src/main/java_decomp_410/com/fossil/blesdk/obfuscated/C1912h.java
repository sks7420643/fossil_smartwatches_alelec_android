package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h */
public class C1912h {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.h$a")
    /* renamed from: com.fossil.blesdk.obfuscated.h$a */
    public static class C1913a {
        @DexIgnore
        /* renamed from: a */
        public static void m7649a(java.lang.Object obj, android.net.Uri uri) {
            ((android.media.MediaDescription.Builder) obj).setMediaUri(uri);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.net.Uri m7648a(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getMediaUri();
    }
}
