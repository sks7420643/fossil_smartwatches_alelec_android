package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v13 implements MembersInjector<u13> {
    @DexIgnore
    public static void a(u13 u13, ComplicationsPresenter complicationsPresenter) {
        u13.t = complicationsPresenter;
    }

    @DexIgnore
    public static void a(u13 u13, WatchAppsPresenter watchAppsPresenter) {
        u13.u = watchAppsPresenter;
    }

    @DexIgnore
    public static void a(u13 u13, CustomizeThemePresenter customizeThemePresenter) {
        u13.v = customizeThemePresenter;
    }

    @DexIgnore
    public static void a(u13 u13, j42 j42) {
        u13.w = j42;
    }
}
