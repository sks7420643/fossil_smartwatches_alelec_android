package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.he;
import com.fossil.blesdk.obfuscated.le;
import com.fossil.blesdk.obfuscated.qd;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class id<T> {
    @DexIgnore
    public /* final */ ue a;
    @DexIgnore
    public /* final */ he<T> b;
    @DexIgnore
    public Executor c; // = h3.d();
    @DexIgnore
    public /* final */ List<c<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public qd<T> f;
    @DexIgnore
    public qd<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public qd.e i; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qd.e {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(int i, int i2) {
            id.this.a.a(i, i2, (Object) null);
        }

        @DexIgnore
        public void b(int i, int i2) {
            id.this.a.b(i, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            id.this.a.c(i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ qd e;
        @DexIgnore
        public /* final */ /* synthetic */ qd f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ qd h;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ le.c e;

            @DexIgnore
            public a(le.c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public void run() {
                b bVar = b.this;
                id idVar = id.this;
                if (idVar.h == bVar.g) {
                    idVar.a(bVar.h, bVar.f, this.e, bVar.e.j, bVar.i);
                }
            }
        }

        @DexIgnore
        public b(qd qdVar, qd qdVar2, int i2, qd qdVar3, Runnable runnable) {
            this.e = qdVar;
            this.f = qdVar2;
            this.g = i2;
            this.h = qdVar3;
            this.i = runnable;
        }

        @DexIgnore
        public void run() {
            id.this.c.execute(new a(td.a(this.e.i, this.f.i, id.this.b.b())));
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        void a(qd<T> qdVar, qd<T> qdVar2);
    }

    @DexIgnore
    public id(RecyclerView.g gVar, le.d<T> dVar) {
        this.a = new ge(gVar);
        this.b = new he.a(dVar).a();
    }

    @DexIgnore
    public T a(int i2) {
        qd<T> qdVar = this.f;
        if (qdVar == null) {
            qd<T> qdVar2 = this.g;
            if (qdVar2 != null) {
                return qdVar2.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        qdVar.g(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int a() {
        qd<T> qdVar = this.f;
        if (qdVar != null) {
            return qdVar.size();
        }
        qd<T> qdVar2 = this.g;
        if (qdVar2 == null) {
            return 0;
        }
        return qdVar2.size();
    }

    @DexIgnore
    public void a(qd<T> qdVar) {
        a(qdVar, (Runnable) null);
    }

    @DexIgnore
    public void a(qd<T> qdVar, Runnable runnable) {
        if (qdVar != null) {
            if (this.f == null && this.g == null) {
                this.e = qdVar.g();
            } else if (qdVar.g() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        qd<T> qdVar2 = this.f;
        if (qdVar != qdVar2) {
            qd<T> qdVar3 = this.g;
            if (qdVar3 != null) {
                qdVar2 = qdVar3;
            }
            if (qdVar == null) {
                int a2 = a();
                qd<T> qdVar4 = this.f;
                if (qdVar4 != null) {
                    qdVar4.a(this.i);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.a.c(0, a2);
                a(qdVar2, (qd<T>) null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = qdVar;
                qdVar.a((List<T>) null, this.i);
                this.a.b(0, qdVar.size());
                a((qd) null, qdVar, runnable);
            } else {
                qd<T> qdVar5 = this.f;
                if (qdVar5 != null) {
                    qdVar5.a(this.i);
                    this.g = (qd) this.f.j();
                    this.f = null;
                }
                qd<T> qdVar6 = this.g;
                if (qdVar6 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new b(qdVar6, (qd) qdVar.j(), i2, qdVar, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(qd<T> qdVar, qd<T> qdVar2, le.c cVar, int i2, Runnable runnable) {
        qd<T> qdVar3 = this.g;
        if (qdVar3 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = qdVar;
        this.g = null;
        td.a(this.a, qdVar3.i, qdVar.i, cVar);
        qdVar.a((List<T>) qdVar2, this.i);
        if (!this.f.isEmpty()) {
            int a2 = td.a(cVar, (sd) qdVar3.i, (sd) qdVar2.i, i2);
            qd<T> qdVar4 = this.f;
            qdVar4.g(Math.max(0, Math.min(qdVar4.size() - 1, a2)));
        }
        a(qdVar3, this.f, runnable);
    }

    @DexIgnore
    public final void a(qd<T> qdVar, qd<T> qdVar2, Runnable runnable) {
        for (c<T> a2 : this.d) {
            a2.a(qdVar, qdVar2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void a(c<T> cVar) {
        this.d.add(cVar);
    }
}
