package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yi */
public final class C3365yi {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.fossil.blesdk.obfuscated.C3365yi f11259i; // = new com.fossil.blesdk.obfuscated.C3365yi.C3366a().mo18122a();

    @DexIgnore
    /* renamed from: a */
    public androidx.work.NetworkType f11260a; // = androidx.work.NetworkType.NOT_REQUIRED;

    @DexIgnore
    /* renamed from: b */
    public boolean f11261b;

    @DexIgnore
    /* renamed from: c */
    public boolean f11262c;

    @DexIgnore
    /* renamed from: d */
    public boolean f11263d;

    @DexIgnore
    /* renamed from: e */
    public boolean f11264e;

    @DexIgnore
    /* renamed from: f */
    public long f11265f; // = -1;

    @DexIgnore
    /* renamed from: g */
    public long f11266g; // = -1;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3439zi f11267h; // = new com.fossil.blesdk.obfuscated.C3439zi();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yi$a */
    public static final class C3366a {

        @DexIgnore
        /* renamed from: a */
        public boolean f11268a; // = false;

        @DexIgnore
        /* renamed from: b */
        public boolean f11269b; // = false;

        @DexIgnore
        /* renamed from: c */
        public androidx.work.NetworkType f11270c; // = androidx.work.NetworkType.NOT_REQUIRED;

        @DexIgnore
        /* renamed from: d */
        public boolean f11271d; // = false;

        @DexIgnore
        /* renamed from: e */
        public boolean f11272e; // = false;

        @DexIgnore
        /* renamed from: f */
        public long f11273f; // = -1;

        @DexIgnore
        /* renamed from: g */
        public long f11274g; // = -1;

        @DexIgnore
        /* renamed from: h */
        public com.fossil.blesdk.obfuscated.C3439zi f11275h; // = new com.fossil.blesdk.obfuscated.C3439zi();

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3365yi.C3366a mo18121a(androidx.work.NetworkType networkType) {
            this.f11270c = networkType;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3365yi mo18122a() {
            return new com.fossil.blesdk.obfuscated.C3365yi(this);
        }
    }

    @DexIgnore
    public C3365yi() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18104a(androidx.work.NetworkType networkType) {
        this.f11260a = networkType;
    }

    @DexIgnore
    /* renamed from: b */
    public androidx.work.NetworkType mo18107b() {
        return this.f11260a;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo18111c(boolean z) {
        this.f11262c = z;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo18113d(boolean z) {
        this.f11264e = z;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo18114e() {
        return this.f11267h.mo18496b() > 0;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C3365yi.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C3365yi yiVar = (com.fossil.blesdk.obfuscated.C3365yi) obj;
        if (this.f11261b == yiVar.f11261b && this.f11262c == yiVar.f11262c && this.f11263d == yiVar.f11263d && this.f11264e == yiVar.f11264e && this.f11265f == yiVar.f11265f && this.f11266g == yiVar.f11266g && this.f11260a == yiVar.f11260a) {
            return this.f11267h.equals(yiVar.f11267h);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo18116f() {
        return this.f11263d;
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo18117g() {
        return this.f11261b;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo18118h() {
        return this.f11262c;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f11265f;
        long j2 = this.f11266g;
        return (((((((((((((this.f11260a.hashCode() * 31) + (this.f11261b ? 1 : 0)) * 31) + (this.f11262c ? 1 : 0)) * 31) + (this.f11263d ? 1 : 0)) * 31) + (this.f11264e ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.f11267h.hashCode();
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo18120i() {
        return this.f11264e;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18106a(boolean z) {
        this.f11263d = z;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18109b(boolean z) {
        this.f11261b = z;
    }

    @DexIgnore
    /* renamed from: c */
    public long mo18110c() {
        return this.f11265f;
    }

    @DexIgnore
    /* renamed from: d */
    public long mo18112d() {
        return this.f11266g;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18103a(long j) {
        this.f11265f = j;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18108b(long j) {
        this.f11266g = j;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18105a(com.fossil.blesdk.obfuscated.C3439zi ziVar) {
        this.f11267h = ziVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3439zi mo18102a() {
        return this.f11267h;
    }

    @DexIgnore
    public C3365yi(com.fossil.blesdk.obfuscated.C3365yi.C3366a aVar) {
        this.f11261b = aVar.f11268a;
        this.f11262c = android.os.Build.VERSION.SDK_INT >= 23 && aVar.f11269b;
        this.f11260a = aVar.f11270c;
        this.f11263d = aVar.f11271d;
        this.f11264e = aVar.f11272e;
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            this.f11267h = aVar.f11275h;
            this.f11265f = aVar.f11273f;
            this.f11266g = aVar.f11274g;
        }
    }

    @DexIgnore
    public C3365yi(com.fossil.blesdk.obfuscated.C3365yi yiVar) {
        this.f11261b = yiVar.f11261b;
        this.f11262c = yiVar.f11262c;
        this.f11260a = yiVar.f11260a;
        this.f11263d = yiVar.f11263d;
        this.f11264e = yiVar.f11264e;
        this.f11267h = yiVar.f11267h;
    }
}
