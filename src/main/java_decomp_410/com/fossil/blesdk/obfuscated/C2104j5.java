package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j5 */
public class C2104j5 {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int[] f6280b; // = {0, 4, 8};

    @DexIgnore
    /* renamed from: c */
    public static android.util.SparseIntArray f6281c; // = new android.util.SparseIntArray();

    @DexIgnore
    /* renamed from: a */
    public java.util.HashMap<java.lang.Integer, com.fossil.blesdk.obfuscated.C2104j5.C2106b> f6282a; // = new java.util.HashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.j5$b")
    /* renamed from: com.fossil.blesdk.obfuscated.j5$b */
    public static class C2106b {

        @DexIgnore
        /* renamed from: A */
        public int f6283A;

        @DexIgnore
        /* renamed from: B */
        public int f6284B;

        @DexIgnore
        /* renamed from: C */
        public int f6285C;

        @DexIgnore
        /* renamed from: D */
        public int f6286D;

        @DexIgnore
        /* renamed from: E */
        public int f6287E;

        @DexIgnore
        /* renamed from: F */
        public int f6288F;

        @DexIgnore
        /* renamed from: G */
        public int f6289G;

        @DexIgnore
        /* renamed from: H */
        public int f6290H;

        @DexIgnore
        /* renamed from: I */
        public int f6291I;

        @DexIgnore
        /* renamed from: J */
        public int f6292J;

        @DexIgnore
        /* renamed from: K */
        public int f6293K;

        @DexIgnore
        /* renamed from: L */
        public int f6294L;

        @DexIgnore
        /* renamed from: M */
        public int f6295M;

        @DexIgnore
        /* renamed from: N */
        public int f6296N;

        @DexIgnore
        /* renamed from: O */
        public int f6297O;

        @DexIgnore
        /* renamed from: P */
        public int f6298P;

        @DexIgnore
        /* renamed from: Q */
        public float f6299Q;

        @DexIgnore
        /* renamed from: R */
        public float f6300R;

        @DexIgnore
        /* renamed from: S */
        public int f6301S;

        @DexIgnore
        /* renamed from: T */
        public int f6302T;

        @DexIgnore
        /* renamed from: U */
        public float f6303U;

        @DexIgnore
        /* renamed from: V */
        public boolean f6304V;

        @DexIgnore
        /* renamed from: W */
        public float f6305W;

        @DexIgnore
        /* renamed from: X */
        public float f6306X;

        @DexIgnore
        /* renamed from: Y */
        public float f6307Y;

        @DexIgnore
        /* renamed from: Z */
        public float f6308Z;

        @DexIgnore
        /* renamed from: a */
        public boolean f6309a;

        @DexIgnore
        /* renamed from: a0 */
        public float f6310a0;

        @DexIgnore
        /* renamed from: b */
        public int f6311b;

        @DexIgnore
        /* renamed from: b0 */
        public float f6312b0;

        @DexIgnore
        /* renamed from: c */
        public int f6313c;

        @DexIgnore
        /* renamed from: c0 */
        public float f6314c0;

        @DexIgnore
        /* renamed from: d */
        public int f6315d;

        @DexIgnore
        /* renamed from: d0 */
        public float f6316d0;

        @DexIgnore
        /* renamed from: e */
        public int f6317e;

        @DexIgnore
        /* renamed from: e0 */
        public float f6318e0;

        @DexIgnore
        /* renamed from: f */
        public int f6319f;

        @DexIgnore
        /* renamed from: f0 */
        public float f6320f0;

        @DexIgnore
        /* renamed from: g */
        public float f6321g;

        @DexIgnore
        /* renamed from: g0 */
        public float f6322g0;

        @DexIgnore
        /* renamed from: h */
        public int f6323h;

        @DexIgnore
        /* renamed from: h0 */
        public boolean f6324h0;

        @DexIgnore
        /* renamed from: i */
        public int f6325i;

        @DexIgnore
        /* renamed from: i0 */
        public boolean f6326i0;

        @DexIgnore
        /* renamed from: j */
        public int f6327j;

        @DexIgnore
        /* renamed from: j0 */
        public int f6328j0;

        @DexIgnore
        /* renamed from: k */
        public int f6329k;

        @DexIgnore
        /* renamed from: k0 */
        public int f6330k0;

        @DexIgnore
        /* renamed from: l */
        public int f6331l;

        @DexIgnore
        /* renamed from: l0 */
        public int f6332l0;

        @DexIgnore
        /* renamed from: m */
        public int f6333m;

        @DexIgnore
        /* renamed from: m0 */
        public int f6334m0;

        @DexIgnore
        /* renamed from: n */
        public int f6335n;

        @DexIgnore
        /* renamed from: n0 */
        public int f6336n0;

        @DexIgnore
        /* renamed from: o */
        public int f6337o;

        @DexIgnore
        /* renamed from: o0 */
        public int f6338o0;

        @DexIgnore
        /* renamed from: p */
        public int f6339p;

        @DexIgnore
        /* renamed from: p0 */
        public float f6340p0;

        @DexIgnore
        /* renamed from: q */
        public int f6341q;

        @DexIgnore
        /* renamed from: q0 */
        public float f6342q0;

        @DexIgnore
        /* renamed from: r */
        public int f6343r;

        @DexIgnore
        /* renamed from: r0 */
        public boolean f6344r0;

        @DexIgnore
        /* renamed from: s */
        public int f6345s;

        @DexIgnore
        /* renamed from: s0 */
        public int f6346s0;

        @DexIgnore
        /* renamed from: t */
        public int f6347t;

        @DexIgnore
        /* renamed from: t0 */
        public int f6348t0;

        @DexIgnore
        /* renamed from: u */
        public float f6349u;

        @DexIgnore
        /* renamed from: u0 */
        public int[] f6350u0;

        @DexIgnore
        /* renamed from: v */
        public float f6351v;

        @DexIgnore
        /* renamed from: v0 */
        public java.lang.String f6352v0;

        @DexIgnore
        /* renamed from: w */
        public java.lang.String f6353w;

        @DexIgnore
        /* renamed from: x */
        public int f6354x;

        @DexIgnore
        /* renamed from: y */
        public int f6355y;

        @DexIgnore
        /* renamed from: z */
        public float f6356z;

        @DexIgnore
        public C2106b() {
            this.f6309a = false;
            this.f6317e = -1;
            this.f6319f = -1;
            this.f6321g = -1.0f;
            this.f6323h = -1;
            this.f6325i = -1;
            this.f6327j = -1;
            this.f6329k = -1;
            this.f6331l = -1;
            this.f6333m = -1;
            this.f6335n = -1;
            this.f6337o = -1;
            this.f6339p = -1;
            this.f6341q = -1;
            this.f6343r = -1;
            this.f6345s = -1;
            this.f6347t = -1;
            this.f6349u = 0.5f;
            this.f6351v = 0.5f;
            this.f6353w = null;
            this.f6354x = -1;
            this.f6355y = 0;
            this.f6356z = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6283A = -1;
            this.f6284B = -1;
            this.f6285C = -1;
            this.f6286D = -1;
            this.f6287E = -1;
            this.f6288F = -1;
            this.f6289G = -1;
            this.f6290H = -1;
            this.f6291I = -1;
            this.f6292J = 0;
            this.f6293K = -1;
            this.f6294L = -1;
            this.f6295M = -1;
            this.f6296N = -1;
            this.f6297O = -1;
            this.f6298P = -1;
            this.f6299Q = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6300R = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6301S = 0;
            this.f6302T = 0;
            this.f6303U = 1.0f;
            this.f6304V = false;
            this.f6305W = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6306X = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6307Y = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6308Z = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6310a0 = 1.0f;
            this.f6312b0 = 1.0f;
            this.f6314c0 = Float.NaN;
            this.f6316d0 = Float.NaN;
            this.f6318e0 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6320f0 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6322g0 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f6324h0 = false;
            this.f6326i0 = false;
            this.f6328j0 = 0;
            this.f6330k0 = 0;
            this.f6332l0 = -1;
            this.f6334m0 = -1;
            this.f6336n0 = -1;
            this.f6338o0 = -1;
            this.f6340p0 = 1.0f;
            this.f6342q0 = 1.0f;
            this.f6344r0 = false;
            this.f6346s0 = -1;
            this.f6348t0 = -1;
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2104j5.C2106b clone() {
            com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = new com.fossil.blesdk.obfuscated.C2104j5.C2106b();
            bVar.f6309a = this.f6309a;
            bVar.f6311b = this.f6311b;
            bVar.f6313c = this.f6313c;
            bVar.f6317e = this.f6317e;
            bVar.f6319f = this.f6319f;
            bVar.f6321g = this.f6321g;
            bVar.f6323h = this.f6323h;
            bVar.f6325i = this.f6325i;
            bVar.f6327j = this.f6327j;
            bVar.f6329k = this.f6329k;
            bVar.f6331l = this.f6331l;
            bVar.f6333m = this.f6333m;
            bVar.f6335n = this.f6335n;
            bVar.f6337o = this.f6337o;
            bVar.f6339p = this.f6339p;
            bVar.f6341q = this.f6341q;
            bVar.f6343r = this.f6343r;
            bVar.f6345s = this.f6345s;
            bVar.f6347t = this.f6347t;
            bVar.f6349u = this.f6349u;
            bVar.f6351v = this.f6351v;
            bVar.f6353w = this.f6353w;
            bVar.f6283A = this.f6283A;
            bVar.f6284B = this.f6284B;
            bVar.f6349u = this.f6349u;
            bVar.f6349u = this.f6349u;
            bVar.f6349u = this.f6349u;
            bVar.f6349u = this.f6349u;
            bVar.f6349u = this.f6349u;
            bVar.f6285C = this.f6285C;
            bVar.f6286D = this.f6286D;
            bVar.f6287E = this.f6287E;
            bVar.f6288F = this.f6288F;
            bVar.f6289G = this.f6289G;
            bVar.f6290H = this.f6290H;
            bVar.f6291I = this.f6291I;
            bVar.f6292J = this.f6292J;
            bVar.f6293K = this.f6293K;
            bVar.f6294L = this.f6294L;
            bVar.f6295M = this.f6295M;
            bVar.f6296N = this.f6296N;
            bVar.f6297O = this.f6297O;
            bVar.f6298P = this.f6298P;
            bVar.f6299Q = this.f6299Q;
            bVar.f6300R = this.f6300R;
            bVar.f6301S = this.f6301S;
            bVar.f6302T = this.f6302T;
            bVar.f6303U = this.f6303U;
            bVar.f6304V = this.f6304V;
            bVar.f6305W = this.f6305W;
            bVar.f6306X = this.f6306X;
            bVar.f6307Y = this.f6307Y;
            bVar.f6308Z = this.f6308Z;
            bVar.f6310a0 = this.f6310a0;
            bVar.f6312b0 = this.f6312b0;
            bVar.f6314c0 = this.f6314c0;
            bVar.f6316d0 = this.f6316d0;
            bVar.f6318e0 = this.f6318e0;
            bVar.f6320f0 = this.f6320f0;
            bVar.f6322g0 = this.f6322g0;
            bVar.f6324h0 = this.f6324h0;
            bVar.f6326i0 = this.f6326i0;
            bVar.f6328j0 = this.f6328j0;
            bVar.f6330k0 = this.f6330k0;
            bVar.f6332l0 = this.f6332l0;
            bVar.f6334m0 = this.f6334m0;
            bVar.f6336n0 = this.f6336n0;
            bVar.f6338o0 = this.f6338o0;
            bVar.f6340p0 = this.f6340p0;
            bVar.f6342q0 = this.f6342q0;
            bVar.f6346s0 = this.f6346s0;
            bVar.f6348t0 = this.f6348t0;
            int[] iArr = this.f6350u0;
            if (iArr != null) {
                bVar.f6350u0 = java.util.Arrays.copyOf(iArr, iArr.length);
            }
            bVar.f6354x = this.f6354x;
            bVar.f6355y = this.f6355y;
            bVar.f6356z = this.f6356z;
            bVar.f6344r0 = this.f6344r0;
            return bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo12242a(androidx.constraintlayout.widget.ConstraintHelper constraintHelper, int i, androidx.constraintlayout.widget.Constraints.LayoutParams layoutParams) {
            mo12241a(i, layoutParams);
            if (constraintHelper instanceof androidx.constraintlayout.widget.Barrier) {
                this.f6348t0 = 1;
                androidx.constraintlayout.widget.Barrier barrier = (androidx.constraintlayout.widget.Barrier) constraintHelper;
                this.f6346s0 = barrier.getType();
                this.f6350u0 = barrier.getReferencedIds();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo12241a(int i, androidx.constraintlayout.widget.Constraints.LayoutParams layoutParams) {
            mo12240a(i, (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) layoutParams);
            this.f6303U = layoutParams.f818m0;
            this.f6306X = layoutParams.f821p0;
            this.f6307Y = layoutParams.f822q0;
            this.f6308Z = layoutParams.f823r0;
            this.f6310a0 = layoutParams.f824s0;
            this.f6312b0 = layoutParams.f825t0;
            this.f6314c0 = layoutParams.f826u0;
            this.f6316d0 = layoutParams.f827v0;
            this.f6318e0 = layoutParams.f828w0;
            this.f6320f0 = layoutParams.f829x0;
            this.f6322g0 = layoutParams.f830y0;
            this.f6305W = layoutParams.f820o0;
            this.f6304V = layoutParams.f819n0;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo12240a(int i, androidx.constraintlayout.widget.ConstraintLayout.LayoutParams layoutParams) {
            this.f6315d = i;
            this.f6323h = layoutParams.f784d;
            this.f6325i = layoutParams.f786e;
            this.f6327j = layoutParams.f788f;
            this.f6329k = layoutParams.f790g;
            this.f6331l = layoutParams.f792h;
            this.f6333m = layoutParams.f794i;
            this.f6335n = layoutParams.f796j;
            this.f6337o = layoutParams.f798k;
            this.f6339p = layoutParams.f800l;
            this.f6341q = layoutParams.f805p;
            this.f6343r = layoutParams.f806q;
            this.f6345s = layoutParams.f807r;
            this.f6347t = layoutParams.f808s;
            this.f6349u = layoutParams.f815z;
            this.f6351v = layoutParams.f752A;
            this.f6353w = layoutParams.f753B;
            this.f6354x = layoutParams.f802m;
            this.f6355y = layoutParams.f803n;
            this.f6356z = layoutParams.f804o;
            this.f6283A = layoutParams.f767P;
            this.f6284B = layoutParams.f768Q;
            this.f6285C = layoutParams.f769R;
            this.f6321g = layoutParams.f782c;
            this.f6317e = layoutParams.f778a;
            this.f6319f = layoutParams.f780b;
            this.f6311b = layoutParams.width;
            this.f6313c = layoutParams.height;
            this.f6286D = layoutParams.leftMargin;
            this.f6287E = layoutParams.rightMargin;
            this.f6288F = layoutParams.topMargin;
            this.f6289G = layoutParams.bottomMargin;
            this.f6299Q = layoutParams.f756E;
            this.f6300R = layoutParams.f755D;
            this.f6302T = layoutParams.f758G;
            this.f6301S = layoutParams.f757F;
            boolean z = layoutParams.f770S;
            this.f6324h0 = z;
            this.f6326i0 = layoutParams.f771T;
            this.f6328j0 = layoutParams.f759H;
            this.f6330k0 = layoutParams.f760I;
            this.f6324h0 = z;
            this.f6332l0 = layoutParams.f763L;
            this.f6334m0 = layoutParams.f764M;
            this.f6336n0 = layoutParams.f761J;
            this.f6338o0 = layoutParams.f762K;
            this.f6340p0 = layoutParams.f765N;
            this.f6342q0 = layoutParams.f766O;
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                this.f6290H = layoutParams.getMarginEnd();
                this.f6291I = layoutParams.getMarginStart();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12243a(androidx.constraintlayout.widget.ConstraintLayout.LayoutParams layoutParams) {
            layoutParams.f784d = this.f6323h;
            layoutParams.f786e = this.f6325i;
            layoutParams.f788f = this.f6327j;
            layoutParams.f790g = this.f6329k;
            layoutParams.f792h = this.f6331l;
            layoutParams.f794i = this.f6333m;
            layoutParams.f796j = this.f6335n;
            layoutParams.f798k = this.f6337o;
            layoutParams.f800l = this.f6339p;
            layoutParams.f805p = this.f6341q;
            layoutParams.f806q = this.f6343r;
            layoutParams.f807r = this.f6345s;
            layoutParams.f808s = this.f6347t;
            layoutParams.leftMargin = this.f6286D;
            layoutParams.rightMargin = this.f6287E;
            layoutParams.topMargin = this.f6288F;
            layoutParams.bottomMargin = this.f6289G;
            layoutParams.f813x = this.f6298P;
            layoutParams.f814y = this.f6297O;
            layoutParams.f815z = this.f6349u;
            layoutParams.f752A = this.f6351v;
            layoutParams.f802m = this.f6354x;
            layoutParams.f803n = this.f6355y;
            layoutParams.f804o = this.f6356z;
            layoutParams.f753B = this.f6353w;
            layoutParams.f767P = this.f6283A;
            layoutParams.f768Q = this.f6284B;
            layoutParams.f756E = this.f6299Q;
            layoutParams.f755D = this.f6300R;
            layoutParams.f758G = this.f6302T;
            layoutParams.f757F = this.f6301S;
            layoutParams.f770S = this.f6324h0;
            layoutParams.f771T = this.f6326i0;
            layoutParams.f759H = this.f6328j0;
            layoutParams.f760I = this.f6330k0;
            layoutParams.f763L = this.f6332l0;
            layoutParams.f764M = this.f6334m0;
            layoutParams.f761J = this.f6336n0;
            layoutParams.f762K = this.f6338o0;
            layoutParams.f765N = this.f6340p0;
            layoutParams.f766O = this.f6342q0;
            layoutParams.f769R = this.f6285C;
            layoutParams.f782c = this.f6321g;
            layoutParams.f778a = this.f6317e;
            layoutParams.f780b = this.f6319f;
            layoutParams.width = this.f6311b;
            layoutParams.height = this.f6313c;
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                layoutParams.setMarginStart(this.f6291I);
                layoutParams.setMarginEnd(this.f6290H);
            }
            layoutParams.mo1414a();
        }
    }

    /*
    static {
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintLeft_toLeftOf, 25);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintLeft_toRightOf, 26);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintRight_toLeftOf, 29);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintRight_toRightOf, 30);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintTop_toTopOf, 36);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintTop_toBottomOf, 35);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintBottom_toTopOf, 4);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintBottom_toBottomOf, 3);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintBaseline_toBaselineOf, 1);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_editor_absoluteX, 6);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_editor_absoluteY, 7);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintGuide_begin, 17);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintGuide_end, 18);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintGuide_percent, 19);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_orientation, 27);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintStart_toEndOf, 32);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintStart_toStartOf, 33);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintEnd_toStartOf, 10);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintEnd_toEndOf, 9);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginLeft, 13);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginTop, 16);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginRight, 14);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginBottom, 11);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginStart, 15);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_goneMarginEnd, 12);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintVertical_weight, 40);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHorizontal_weight, 39);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHorizontal_chainStyle, 41);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintVertical_chainStyle, 42);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHorizontal_bias, 20);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintVertical_bias, 37);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintDimensionRatio, 5);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintLeft_creator, 75);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintTop_creator, 75);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintRight_creator, 75);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintBottom_creator, 75);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintBaseline_creator, 75);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginLeft, 24);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginRight, 28);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginStart, 31);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginEnd, 8);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginTop, 34);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_marginBottom, 2);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_width, 23);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_layout_height, 21);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_visibility, 22);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_alpha, 43);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_elevation, 44);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_rotationX, 45);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_rotationY, 46);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_rotation, 60);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_scaleX, 47);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_scaleY, 48);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_transformPivotX, 49);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_transformPivotY, 50);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_translationX, 51);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_translationY, 52);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_translationZ, 53);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintWidth_default, 54);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHeight_default, 55);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintWidth_max, 56);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHeight_max, 57);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintWidth_min, 58);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHeight_min, 59);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintCircle, 61);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintCircleRadius, 62);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintCircleAngle, 63);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_android_id, 38);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintWidth_percent, 69);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_layout_constraintHeight_percent, 70);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_chainUseRtl, 71);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_barrierDirection, 72);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_constraint_referenced_ids, 73);
        f6281c.append(com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet_barrierAllowsGoneWidgets, 74);
    }
    */

    @DexIgnore
    /* renamed from: a */
    public void mo12234a(androidx.constraintlayout.widget.Constraints constraints) {
        int childCount = constraints.getChildCount();
        this.f6282a.clear();
        int i = 0;
        while (i < childCount) {
            android.view.View childAt = constraints.getChildAt(i);
            androidx.constraintlayout.widget.Constraints.LayoutParams layoutParams = (androidx.constraintlayout.widget.Constraints.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.f6282a.containsKey(java.lang.Integer.valueOf(id))) {
                    this.f6282a.put(java.lang.Integer.valueOf(id), new com.fossil.blesdk.obfuscated.C2104j5.C2106b());
                }
                com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = this.f6282a.get(java.lang.Integer.valueOf(id));
                if (childAt instanceof androidx.constraintlayout.widget.ConstraintHelper) {
                    bVar.mo12242a((androidx.constraintlayout.widget.ConstraintHelper) childAt, id, layoutParams);
                }
                bVar.mo12241a(id, layoutParams);
                i++;
            } else {
                throw new java.lang.RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.String mo12237b(int i) {
        switch (i) {
            case 1:
                return com.facebook.appevents.codeless.internal.ViewHierarchy.DIMENSION_LEFT_KEY;
            case 2:
                return "right";
            case 3:
                return com.facebook.appevents.codeless.internal.ViewHierarchy.DIMENSION_TOP_KEY;
            case 4:
                return "bottom";
            case 5:
                return "baseline";
            case 6:
                return com.facebook.share.internal.VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE;
            case 7:
                return "end";
            default:
                return "undefined";
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12238b(androidx.constraintlayout.widget.ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        java.util.HashSet hashSet = new java.util.HashSet(this.f6282a.keySet());
        int i = 0;
        while (i < childCount) {
            android.view.View childAt = constraintLayout.getChildAt(i);
            int id = childAt.getId();
            if (id != -1) {
                if (this.f6282a.containsKey(java.lang.Integer.valueOf(id))) {
                    hashSet.remove(java.lang.Integer.valueOf(id));
                    com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = this.f6282a.get(java.lang.Integer.valueOf(id));
                    if (childAt instanceof androidx.constraintlayout.widget.Barrier) {
                        bVar.f6348t0 = 1;
                    }
                    int i2 = bVar.f6348t0;
                    if (i2 != -1 && i2 == 1) {
                        androidx.constraintlayout.widget.Barrier barrier = (androidx.constraintlayout.widget.Barrier) childAt;
                        barrier.setId(id);
                        barrier.setType(bVar.f6346s0);
                        barrier.setAllowsGoneWidget(bVar.f6344r0);
                        int[] iArr = bVar.f6350u0;
                        if (iArr != null) {
                            barrier.setReferencedIds(iArr);
                        } else {
                            java.lang.String str = bVar.f6352v0;
                            if (str != null) {
                                bVar.f6350u0 = mo12236a((android.view.View) barrier, str);
                                barrier.setReferencedIds(bVar.f6350u0);
                            }
                        }
                    }
                    androidx.constraintlayout.widget.ConstraintLayout.LayoutParams layoutParams = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) childAt.getLayoutParams();
                    bVar.mo12243a(layoutParams);
                    childAt.setLayoutParams(layoutParams);
                    childAt.setVisibility(bVar.f6292J);
                    if (android.os.Build.VERSION.SDK_INT >= 17) {
                        childAt.setAlpha(bVar.f6303U);
                        childAt.setRotation(bVar.f6306X);
                        childAt.setRotationX(bVar.f6307Y);
                        childAt.setRotationY(bVar.f6308Z);
                        childAt.setScaleX(bVar.f6310a0);
                        childAt.setScaleY(bVar.f6312b0);
                        if (!java.lang.Float.isNaN(bVar.f6314c0)) {
                            childAt.setPivotX(bVar.f6314c0);
                        }
                        if (!java.lang.Float.isNaN(bVar.f6316d0)) {
                            childAt.setPivotY(bVar.f6316d0);
                        }
                        childAt.setTranslationX(bVar.f6318e0);
                        childAt.setTranslationY(bVar.f6320f0);
                        if (android.os.Build.VERSION.SDK_INT >= 21) {
                            childAt.setTranslationZ(bVar.f6322g0);
                            if (bVar.f6304V) {
                                childAt.setElevation(bVar.f6305W);
                            }
                        }
                    }
                }
                i++;
            } else {
                throw new java.lang.RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
        java.util.Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            java.lang.Integer num = (java.lang.Integer) it.next();
            com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar2 = this.f6282a.get(num);
            int i3 = bVar2.f6348t0;
            if (i3 != -1 && i3 == 1) {
                androidx.constraintlayout.widget.Barrier barrier2 = new androidx.constraintlayout.widget.Barrier(constraintLayout.getContext());
                barrier2.setId(num.intValue());
                int[] iArr2 = bVar2.f6350u0;
                if (iArr2 != null) {
                    barrier2.setReferencedIds(iArr2);
                } else {
                    java.lang.String str2 = bVar2.f6352v0;
                    if (str2 != null) {
                        bVar2.f6350u0 = mo12236a((android.view.View) barrier2, str2);
                        barrier2.setReferencedIds(bVar2.f6350u0);
                    }
                }
                barrier2.setType(bVar2.f6346s0);
                androidx.constraintlayout.widget.ConstraintLayout.LayoutParams generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                barrier2.mo1366a();
                bVar2.mo12243a(generateDefaultLayoutParams);
                constraintLayout.addView(barrier2, generateDefaultLayoutParams);
            }
            if (bVar2.f6309a) {
                androidx.constraintlayout.widget.Guideline guideline = new androidx.constraintlayout.widget.Guideline(constraintLayout.getContext());
                guideline.setId(num.intValue());
                androidx.constraintlayout.widget.ConstraintLayout.LayoutParams generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
                bVar2.mo12243a(generateDefaultLayoutParams2);
                constraintLayout.addView(guideline, generateDefaultLayoutParams2);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12239c(androidx.constraintlayout.widget.ConstraintLayout constraintLayout) {
        int childCount = constraintLayout.getChildCount();
        this.f6282a.clear();
        int i = 0;
        while (i < childCount) {
            android.view.View childAt = constraintLayout.getChildAt(i);
            androidx.constraintlayout.widget.ConstraintLayout.LayoutParams layoutParams = (androidx.constraintlayout.widget.ConstraintLayout.LayoutParams) childAt.getLayoutParams();
            int id = childAt.getId();
            if (id != -1) {
                if (!this.f6282a.containsKey(java.lang.Integer.valueOf(id))) {
                    this.f6282a.put(java.lang.Integer.valueOf(id), new com.fossil.blesdk.obfuscated.C2104j5.C2106b());
                }
                com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = this.f6282a.get(java.lang.Integer.valueOf(id));
                bVar.mo12240a(id, layoutParams);
                bVar.f6292J = childAt.getVisibility();
                if (android.os.Build.VERSION.SDK_INT >= 17) {
                    bVar.f6303U = childAt.getAlpha();
                    bVar.f6306X = childAt.getRotation();
                    bVar.f6307Y = childAt.getRotationX();
                    bVar.f6308Z = childAt.getRotationY();
                    bVar.f6310a0 = childAt.getScaleX();
                    bVar.f6312b0 = childAt.getScaleY();
                    float pivotX = childAt.getPivotX();
                    float pivotY = childAt.getPivotY();
                    if (!(((double) pivotX) == 0.0d && ((double) pivotY) == 0.0d)) {
                        bVar.f6314c0 = pivotX;
                        bVar.f6316d0 = pivotY;
                    }
                    bVar.f6318e0 = childAt.getTranslationX();
                    bVar.f6320f0 = childAt.getTranslationY();
                    if (android.os.Build.VERSION.SDK_INT >= 21) {
                        bVar.f6322g0 = childAt.getTranslationZ();
                        if (bVar.f6304V) {
                            bVar.f6305W = childAt.getElevation();
                        }
                    }
                }
                if (childAt instanceof androidx.constraintlayout.widget.Barrier) {
                    androidx.constraintlayout.widget.Barrier barrier = (androidx.constraintlayout.widget.Barrier) childAt;
                    bVar.f6344r0 = barrier.mo1362b();
                    bVar.f6350u0 = barrier.getReferencedIds();
                    bVar.f6346s0 = barrier.getType();
                }
                i++;
            } else {
                throw new java.lang.RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12233a(androidx.constraintlayout.widget.ConstraintLayout constraintLayout) {
        mo12238b(constraintLayout);
        constraintLayout.setConstraintSet((com.fossil.blesdk.obfuscated.C2104j5) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12231a(int i, int i2, int i3, int i4) {
        if (!this.f6282a.containsKey(java.lang.Integer.valueOf(i))) {
            this.f6282a.put(java.lang.Integer.valueOf(i), new com.fossil.blesdk.obfuscated.C2104j5.C2106b());
        }
        com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = this.f6282a.get(java.lang.Integer.valueOf(i));
        switch (i2) {
            case 1:
                if (i4 == 1) {
                    bVar.f6323h = i3;
                    bVar.f6325i = -1;
                    return;
                } else if (i4 == 2) {
                    bVar.f6325i = i3;
                    bVar.f6323h = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("left to " + mo12237b(i4) + " undefined");
                }
            case 2:
                if (i4 == 1) {
                    bVar.f6327j = i3;
                    bVar.f6329k = -1;
                    return;
                } else if (i4 == 2) {
                    bVar.f6329k = i3;
                    bVar.f6327j = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
                }
            case 3:
                if (i4 == 3) {
                    bVar.f6331l = i3;
                    bVar.f6333m = -1;
                    bVar.f6339p = -1;
                    return;
                } else if (i4 == 4) {
                    bVar.f6333m = i3;
                    bVar.f6331l = -1;
                    bVar.f6339p = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
                }
            case 4:
                if (i4 == 4) {
                    bVar.f6337o = i3;
                    bVar.f6335n = -1;
                    bVar.f6339p = -1;
                    return;
                } else if (i4 == 3) {
                    bVar.f6335n = i3;
                    bVar.f6337o = -1;
                    bVar.f6339p = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
                }
            case 5:
                if (i4 == 5) {
                    bVar.f6339p = i3;
                    bVar.f6337o = -1;
                    bVar.f6335n = -1;
                    bVar.f6331l = -1;
                    bVar.f6333m = -1;
                    return;
                }
                throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
            case 6:
                if (i4 == 6) {
                    bVar.f6343r = i3;
                    bVar.f6341q = -1;
                    return;
                } else if (i4 == 7) {
                    bVar.f6341q = i3;
                    bVar.f6343r = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
                }
            case 7:
                if (i4 == 7) {
                    bVar.f6347t = i3;
                    bVar.f6345s = -1;
                    return;
                } else if (i4 == 6) {
                    bVar.f6345s = i3;
                    bVar.f6347t = -1;
                    return;
                } else {
                    throw new java.lang.IllegalArgumentException("right to " + mo12237b(i4) + " undefined");
                }
            default:
                throw new java.lang.IllegalArgumentException(mo12237b(i2) + " to " + mo12237b(i4) + " unknown");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12230a(int i, float f) {
        mo12228a(i).f6349u = f;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2104j5.C2106b mo12228a(int i) {
        if (!this.f6282a.containsKey(java.lang.Integer.valueOf(i))) {
            this.f6282a.put(java.lang.Integer.valueOf(i), new com.fossil.blesdk.obfuscated.C2104j5.C2106b());
        }
        return this.f6282a.get(java.lang.Integer.valueOf(i));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12232a(android.content.Context context, int i) {
        android.content.res.XmlResourceParser xml = context.getResources().getXml(i);
        try {
            for (int eventType = xml.getEventType(); eventType != 1; eventType = xml.next()) {
                if (eventType == 0) {
                    xml.getName();
                } else if (eventType == 2) {
                    java.lang.String name = xml.getName();
                    com.fossil.blesdk.obfuscated.C2104j5.C2106b a = mo12229a(context, android.util.Xml.asAttributeSet(xml));
                    if (name.equalsIgnoreCase("Guideline")) {
                        a.f6309a = true;
                    }
                    this.f6282a.put(java.lang.Integer.valueOf(a.f6315d), a);
                }
            }
        } catch (org.xmlpull.v1.XmlPullParserException e) {
            e.printStackTrace();
        } catch (java.io.IOException e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m8817a(android.content.res.TypedArray typedArray, int i, int i2) {
        int resourceId = typedArray.getResourceId(i, i2);
        return resourceId == -1 ? typedArray.getInt(i, -1) : resourceId;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2104j5.C2106b mo12229a(android.content.Context context, android.util.AttributeSet attributeSet) {
        com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar = new com.fossil.blesdk.obfuscated.C2104j5.C2106b();
        android.content.res.TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C2295l5.ConstraintSet);
        mo12235a(bVar, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12235a(com.fossil.blesdk.obfuscated.C2104j5.C2106b bVar, android.content.res.TypedArray typedArray) {
        int indexCount = typedArray.getIndexCount();
        for (int i = 0; i < indexCount; i++) {
            int index = typedArray.getIndex(i);
            int i2 = f6281c.get(index);
            switch (i2) {
                case 1:
                    bVar.f6339p = m8817a(typedArray, index, bVar.f6339p);
                    break;
                case 2:
                    bVar.f6289G = typedArray.getDimensionPixelSize(index, bVar.f6289G);
                    break;
                case 3:
                    bVar.f6337o = m8817a(typedArray, index, bVar.f6337o);
                    break;
                case 4:
                    bVar.f6335n = m8817a(typedArray, index, bVar.f6335n);
                    break;
                case 5:
                    bVar.f6353w = typedArray.getString(index);
                    break;
                case 6:
                    bVar.f6283A = typedArray.getDimensionPixelOffset(index, bVar.f6283A);
                    break;
                case 7:
                    bVar.f6284B = typedArray.getDimensionPixelOffset(index, bVar.f6284B);
                    break;
                case 8:
                    bVar.f6290H = typedArray.getDimensionPixelSize(index, bVar.f6290H);
                    break;
                case 9:
                    bVar.f6347t = m8817a(typedArray, index, bVar.f6347t);
                    break;
                case 10:
                    bVar.f6345s = m8817a(typedArray, index, bVar.f6345s);
                    break;
                case 11:
                    bVar.f6296N = typedArray.getDimensionPixelSize(index, bVar.f6296N);
                    break;
                case 12:
                    bVar.f6297O = typedArray.getDimensionPixelSize(index, bVar.f6297O);
                    break;
                case 13:
                    bVar.f6293K = typedArray.getDimensionPixelSize(index, bVar.f6293K);
                    break;
                case 14:
                    bVar.f6295M = typedArray.getDimensionPixelSize(index, bVar.f6295M);
                    break;
                case 15:
                    bVar.f6298P = typedArray.getDimensionPixelSize(index, bVar.f6298P);
                    break;
                case 16:
                    bVar.f6294L = typedArray.getDimensionPixelSize(index, bVar.f6294L);
                    break;
                case 17:
                    bVar.f6317e = typedArray.getDimensionPixelOffset(index, bVar.f6317e);
                    break;
                case 18:
                    bVar.f6319f = typedArray.getDimensionPixelOffset(index, bVar.f6319f);
                    break;
                case 19:
                    bVar.f6321g = typedArray.getFloat(index, bVar.f6321g);
                    break;
                case 20:
                    bVar.f6349u = typedArray.getFloat(index, bVar.f6349u);
                    break;
                case 21:
                    bVar.f6313c = typedArray.getLayoutDimension(index, bVar.f6313c);
                    break;
                case 22:
                    bVar.f6292J = typedArray.getInt(index, bVar.f6292J);
                    bVar.f6292J = f6280b[bVar.f6292J];
                    break;
                case 23:
                    bVar.f6311b = typedArray.getLayoutDimension(index, bVar.f6311b);
                    break;
                case 24:
                    bVar.f6286D = typedArray.getDimensionPixelSize(index, bVar.f6286D);
                    break;
                case 25:
                    bVar.f6323h = m8817a(typedArray, index, bVar.f6323h);
                    break;
                case 26:
                    bVar.f6325i = m8817a(typedArray, index, bVar.f6325i);
                    break;
                case 27:
                    bVar.f6285C = typedArray.getInt(index, bVar.f6285C);
                    break;
                case 28:
                    bVar.f6287E = typedArray.getDimensionPixelSize(index, bVar.f6287E);
                    break;
                case 29:
                    bVar.f6327j = m8817a(typedArray, index, bVar.f6327j);
                    break;
                case 30:
                    bVar.f6329k = m8817a(typedArray, index, bVar.f6329k);
                    break;
                case 31:
                    bVar.f6291I = typedArray.getDimensionPixelSize(index, bVar.f6291I);
                    break;
                case 32:
                    bVar.f6341q = m8817a(typedArray, index, bVar.f6341q);
                    break;
                case 33:
                    bVar.f6343r = m8817a(typedArray, index, bVar.f6343r);
                    break;
                case 34:
                    bVar.f6288F = typedArray.getDimensionPixelSize(index, bVar.f6288F);
                    break;
                case 35:
                    bVar.f6333m = m8817a(typedArray, index, bVar.f6333m);
                    break;
                case 36:
                    bVar.f6331l = m8817a(typedArray, index, bVar.f6331l);
                    break;
                case 37:
                    bVar.f6351v = typedArray.getFloat(index, bVar.f6351v);
                    break;
                case 38:
                    bVar.f6315d = typedArray.getResourceId(index, bVar.f6315d);
                    break;
                case 39:
                    bVar.f6300R = typedArray.getFloat(index, bVar.f6300R);
                    break;
                case 40:
                    bVar.f6299Q = typedArray.getFloat(index, bVar.f6299Q);
                    break;
                case 41:
                    bVar.f6301S = typedArray.getInt(index, bVar.f6301S);
                    break;
                case 42:
                    bVar.f6302T = typedArray.getInt(index, bVar.f6302T);
                    break;
                case 43:
                    bVar.f6303U = typedArray.getFloat(index, bVar.f6303U);
                    break;
                case 44:
                    bVar.f6304V = true;
                    bVar.f6305W = typedArray.getDimension(index, bVar.f6305W);
                    break;
                case 45:
                    bVar.f6307Y = typedArray.getFloat(index, bVar.f6307Y);
                    break;
                case 46:
                    bVar.f6308Z = typedArray.getFloat(index, bVar.f6308Z);
                    break;
                case 47:
                    bVar.f6310a0 = typedArray.getFloat(index, bVar.f6310a0);
                    break;
                case 48:
                    bVar.f6312b0 = typedArray.getFloat(index, bVar.f6312b0);
                    break;
                case 49:
                    bVar.f6314c0 = typedArray.getFloat(index, bVar.f6314c0);
                    break;
                case 50:
                    bVar.f6316d0 = typedArray.getFloat(index, bVar.f6316d0);
                    break;
                case 51:
                    bVar.f6318e0 = typedArray.getDimension(index, bVar.f6318e0);
                    break;
                case 52:
                    bVar.f6320f0 = typedArray.getDimension(index, bVar.f6320f0);
                    break;
                case 53:
                    bVar.f6322g0 = typedArray.getDimension(index, bVar.f6322g0);
                    break;
                default:
                    switch (i2) {
                        case 60:
                            bVar.f6306X = typedArray.getFloat(index, bVar.f6306X);
                            break;
                        case 61:
                            bVar.f6354x = m8817a(typedArray, index, bVar.f6354x);
                            break;
                        case 62:
                            bVar.f6355y = typedArray.getDimensionPixelSize(index, bVar.f6355y);
                            break;
                        case 63:
                            bVar.f6356z = typedArray.getFloat(index, bVar.f6356z);
                            break;
                        default:
                            switch (i2) {
                                case 69:
                                    bVar.f6340p0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 70:
                                    bVar.f6342q0 = typedArray.getFloat(index, 1.0f);
                                    break;
                                case 71:
                                    android.util.Log.e("ConstraintSet", "CURRENTLY UNSUPPORTED");
                                    break;
                                case 72:
                                    bVar.f6346s0 = typedArray.getInt(index, bVar.f6346s0);
                                    break;
                                case 73:
                                    bVar.f6352v0 = typedArray.getString(index);
                                    break;
                                case 74:
                                    bVar.f6344r0 = typedArray.getBoolean(index, bVar.f6344r0);
                                    break;
                                case 75:
                                    android.util.Log.w("ConstraintSet", "unused attribute 0x" + java.lang.Integer.toHexString(index) + "   " + f6281c.get(index));
                                    break;
                                default:
                                    android.util.Log.w("ConstraintSet", "Unknown attribute 0x" + java.lang.Integer.toHexString(index) + "   " + f6281c.get(index));
                                    break;
                            }
                    }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int[] mo12236a(android.view.View view, java.lang.String str) {
        int i;
        java.lang.String[] split = str.split(",");
        android.content.Context context = view.getContext();
        int[] iArr = new int[split.length];
        int i2 = 0;
        int i3 = 0;
        while (i2 < split.length) {
            java.lang.String trim = split[i2].trim();
            try {
                i = com.fossil.blesdk.obfuscated.C2184k5.class.getField(trim).getInt((java.lang.Object) null);
            } catch (java.lang.Exception unused) {
                i = 0;
            }
            if (i == 0) {
                i = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            if (i == 0 && view.isInEditMode() && (view.getParent() instanceof androidx.constraintlayout.widget.ConstraintLayout)) {
                java.lang.Object a = ((androidx.constraintlayout.widget.ConstraintLayout) view.getParent()).mo1378a(0, (java.lang.Object) trim);
                if (a != null && (a instanceof java.lang.Integer)) {
                    i = ((java.lang.Integer) a).intValue();
                }
            }
            iArr[i3] = i;
            i2++;
            i3++;
        }
        return i3 != split.length ? java.util.Arrays.copyOf(iArr, i3) : iArr;
    }
}
