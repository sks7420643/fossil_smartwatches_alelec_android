package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tk4 implements Runnable {
    @DexIgnore
    public long e;
    @DexIgnore
    public uk4 f;

    @DexIgnore
    public tk4(long j, uk4 uk4) {
        kd4.b(uk4, "taskContext");
        this.e = j;
        this.f = uk4;
    }

    @DexIgnore
    public final TaskMode a() {
        return this.f.B();
    }

    @DexIgnore
    public tk4() {
        this(0, sk4.f);
    }
}
