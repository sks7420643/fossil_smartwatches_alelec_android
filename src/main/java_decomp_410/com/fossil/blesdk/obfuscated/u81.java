package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u81 extends m71<Integer> implements z81<Integer>, ia1, RandomAccess {
    @DexIgnore
    public int[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new u81().B();
    }
    */

    @DexIgnore
    public u81() {
        this(new int[10], 0);
    }

    @DexIgnore
    public final int a(int i) {
        f(i);
        return this.f[i];
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Integer) obj).intValue());
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Integer> collection) {
        a();
        v81.a(collection);
        if (!(collection instanceof u81)) {
            return super.addAll(collection);
        }
        u81 u81 = (u81) collection;
        int i = u81.g;
        if (i == 0) {
            return false;
        }
        int i2 = this.g;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.f;
            if (i3 > iArr.length) {
                this.f = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(u81.f, 0, this.f, this.g, u81.g);
            this.g = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final /* synthetic */ z81 b(int i) {
        if (i >= this.g) {
            return new u81(Arrays.copyOf(this.f, i), this.g);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u81)) {
            return super.equals(obj);
        }
        u81 u81 = (u81) obj;
        if (this.g != u81.g) {
            return false;
        }
        int[] iArr = u81.f;
        for (int i = 0; i < this.g; i++) {
            if (this.f[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final void f(int i) {
        if (i < 0 || i >= this.g) {
            throw new IndexOutOfBoundsException(g(i));
        }
    }

    @DexIgnore
    public final String g(int i) {
        int i2 = this.g;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(a(i));
    }

    @DexIgnore
    public final void h(int i) {
        a(this.g, i);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (i * 31) + this.f[i2];
        }
        return i;
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.g; i++) {
            if (obj.equals(Integer.valueOf(this.f[i]))) {
                int[] iArr = this.f;
                System.arraycopy(iArr, i + 1, iArr, i, (this.g - i) - 1);
                this.g--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            int[] iArr = this.f;
            System.arraycopy(iArr, i2, iArr, i, this.g - i2);
            this.g -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        a();
        f(i);
        int[] iArr = this.f;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }

    @DexIgnore
    public u81(int[] iArr, int i) {
        this.f = iArr;
        this.g = i;
    }

    @DexIgnore
    public final void a(int i, int i2) {
        a();
        if (i >= 0) {
            int i3 = this.g;
            if (i <= i3) {
                int[] iArr = this.f;
                if (i3 < iArr.length) {
                    System.arraycopy(iArr, i, iArr, i + 1, i3 - i);
                } else {
                    int[] iArr2 = new int[(((i3 * 3) / 2) + 1)];
                    System.arraycopy(iArr, 0, iArr2, 0, i);
                    System.arraycopy(this.f, i, iArr2, i + 1, this.g - i);
                    this.f = iArr2;
                }
                this.f[i] = i2;
                this.g++;
                this.modCount++;
                return;
            }
        }
        throw new IndexOutOfBoundsException(g(i));
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        f(i);
        int[] iArr = this.f;
        int i2 = iArr[i];
        int i3 = this.g;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i3 - i) - 1);
        }
        this.g--;
        this.modCount++;
        return Integer.valueOf(i2);
    }
}
