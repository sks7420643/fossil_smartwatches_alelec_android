package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.im */
public class C2037im<TResult> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.util.concurrent.Executor f6108i; // = com.fossil.blesdk.obfuscated.C1812fm.m7056b();

    @DexIgnore
    /* renamed from: j */
    public static volatile com.fossil.blesdk.obfuscated.C2037im.C2045g f6109j;

    @DexIgnore
    /* renamed from: k */
    public static com.fossil.blesdk.obfuscated.C2037im<?> f6110k; // = new com.fossil.blesdk.obfuscated.C2037im<>((java.lang.Object) null);

    @DexIgnore
    /* renamed from: l */
    public static com.fossil.blesdk.obfuscated.C2037im<java.lang.Boolean> f6111l; // = new com.fossil.blesdk.obfuscated.C2037im<>((java.lang.Boolean) true);

    @DexIgnore
    /* renamed from: m */
    public static com.fossil.blesdk.obfuscated.C2037im<java.lang.Boolean> f6112m; // = new com.fossil.blesdk.obfuscated.C2037im<>((java.lang.Boolean) false);

    @DexIgnore
    /* renamed from: n */
    public static com.fossil.blesdk.obfuscated.C2037im<?> f6113n; // = new com.fossil.blesdk.obfuscated.C2037im<>(true);

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f6114a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public boolean f6115b;

    @DexIgnore
    /* renamed from: c */
    public boolean f6116c;

    @DexIgnore
    /* renamed from: d */
    public TResult f6117d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.Exception f6118e;

    @DexIgnore
    /* renamed from: f */
    public boolean f6119f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2230km f6120g;

    @DexIgnore
    /* renamed from: h */
    public java.util.List<com.fossil.blesdk.obfuscated.C1958hm<TResult, java.lang.Void>> f6121h; // = new java.util.ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$a")
    /* renamed from: com.fossil.blesdk.obfuscated.im$a */
    public class C2038a implements com.fossil.blesdk.obfuscated.C1958hm<TResult, java.lang.Void> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2141jm f6122a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1958hm f6123b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ java.util.concurrent.Executor f6124c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1881gm f6125d;

        @DexIgnore
        public C2038a(com.fossil.blesdk.obfuscated.C2037im imVar, com.fossil.blesdk.obfuscated.C2141jm jmVar, com.fossil.blesdk.obfuscated.C1958hm hmVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
            this.f6122a = jmVar;
            this.f6123b = hmVar;
            this.f6124c = executor;
            this.f6125d = gmVar;
        }

        @DexIgnore
        public java.lang.Void then(com.fossil.blesdk.obfuscated.C2037im<TResult> imVar) {
            com.fossil.blesdk.obfuscated.C2037im.m8541d(this.f6122a, this.f6123b, imVar, this.f6124c, this.f6125d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$b")
    /* renamed from: com.fossil.blesdk.obfuscated.im$b */
    public class C2039b implements com.fossil.blesdk.obfuscated.C1958hm<TResult, java.lang.Void> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2141jm f6126a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1958hm f6127b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ java.util.concurrent.Executor f6128c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1881gm f6129d;

        @DexIgnore
        public C2039b(com.fossil.blesdk.obfuscated.C2037im imVar, com.fossil.blesdk.obfuscated.C2141jm jmVar, com.fossil.blesdk.obfuscated.C1958hm hmVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
            this.f6126a = jmVar;
            this.f6127b = hmVar;
            this.f6128c = executor;
            this.f6129d = gmVar;
        }

        @DexIgnore
        public java.lang.Void then(com.fossil.blesdk.obfuscated.C2037im<TResult> imVar) {
            com.fossil.blesdk.obfuscated.C2037im.m8540c(this.f6126a, this.f6127b, imVar, this.f6128c, this.f6129d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$c")
    /* renamed from: com.fossil.blesdk.obfuscated.im$c */
    public class C2040c implements com.fossil.blesdk.obfuscated.C1958hm<TResult, com.fossil.blesdk.obfuscated.C2037im<TContinuationResult>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1881gm f6130a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1958hm f6131b;

        @DexIgnore
        public C2040c(com.fossil.blesdk.obfuscated.C2037im imVar, com.fossil.blesdk.obfuscated.C1881gm gmVar, com.fossil.blesdk.obfuscated.C1958hm hmVar) {
            this.f6130a = gmVar;
            this.f6131b = hmVar;
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> then(com.fossil.blesdk.obfuscated.C2037im<TResult> imVar) {
            com.fossil.blesdk.obfuscated.C1881gm gmVar = this.f6130a;
            if (gmVar != null) {
                gmVar.mo11262a();
                throw null;
            } else if (imVar.mo12045e()) {
                return com.fossil.blesdk.obfuscated.C2037im.m8537b(imVar.mo12036a());
            } else {
                if (imVar.mo12043c()) {
                    return com.fossil.blesdk.obfuscated.C2037im.m8542h();
                }
                return imVar.mo12033a((com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult>) this.f6131b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$d")
    /* renamed from: com.fossil.blesdk.obfuscated.im$d */
    public static class C2041d implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1881gm f6132e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2141jm f6133f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1958hm f6134g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2037im f6135h;

        @DexIgnore
        public C2041d(com.fossil.blesdk.obfuscated.C1881gm gmVar, com.fossil.blesdk.obfuscated.C2141jm jmVar, com.fossil.blesdk.obfuscated.C1958hm hmVar, com.fossil.blesdk.obfuscated.C2037im imVar) {
            this.f6132e = gmVar;
            this.f6133f = jmVar;
            this.f6134g = hmVar;
            this.f6135h = imVar;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1881gm gmVar = this.f6132e;
            if (gmVar == null) {
                try {
                    this.f6133f.mo12394a(this.f6134g.then(this.f6135h));
                } catch (java.util.concurrent.CancellationException unused) {
                    this.f6133f.mo12395b();
                } catch (java.lang.Exception e) {
                    this.f6133f.mo12393a(e);
                }
            } else {
                gmVar.mo11262a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$e")
    /* renamed from: com.fossil.blesdk.obfuscated.im$e */
    public static class C2042e implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1881gm f6136e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2141jm f6137f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1958hm f6138g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2037im f6139h;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$e$a")
        /* renamed from: com.fossil.blesdk.obfuscated.im$e$a */
        public class C2043a implements com.fossil.blesdk.obfuscated.C1958hm<TContinuationResult, java.lang.Void> {
            @DexIgnore
            public C2043a() {
            }

            @DexIgnore
            public java.lang.Void then(com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> imVar) {
                com.fossil.blesdk.obfuscated.C1881gm gmVar = com.fossil.blesdk.obfuscated.C2037im.C2042e.this.f6136e;
                if (gmVar == null) {
                    if (imVar.mo12043c()) {
                        com.fossil.blesdk.obfuscated.C2037im.C2042e.this.f6137f.mo12395b();
                    } else if (imVar.mo12045e()) {
                        com.fossil.blesdk.obfuscated.C2037im.C2042e.this.f6137f.mo12393a(imVar.mo12036a());
                    } else {
                        com.fossil.blesdk.obfuscated.C2037im.C2042e.this.f6137f.mo12394a(imVar.mo12041b());
                    }
                    return null;
                }
                gmVar.mo11262a();
                throw null;
            }
        }

        @DexIgnore
        public C2042e(com.fossil.blesdk.obfuscated.C1881gm gmVar, com.fossil.blesdk.obfuscated.C2141jm jmVar, com.fossil.blesdk.obfuscated.C1958hm hmVar, com.fossil.blesdk.obfuscated.C2037im imVar) {
            this.f6136e = gmVar;
            this.f6137f = jmVar;
            this.f6138g = hmVar;
            this.f6139h = imVar;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1881gm gmVar = this.f6136e;
            if (gmVar == null) {
                try {
                    com.fossil.blesdk.obfuscated.C2037im imVar = (com.fossil.blesdk.obfuscated.C2037im) this.f6138g.then(this.f6139h);
                    if (imVar == null) {
                        this.f6137f.mo12394a(null);
                    } else {
                        imVar.mo12033a(new com.fossil.blesdk.obfuscated.C2037im.C2042e.C2043a());
                    }
                } catch (java.util.concurrent.CancellationException unused) {
                    this.f6137f.mo12395b();
                } catch (java.lang.Exception e) {
                    this.f6137f.mo12393a(e);
                }
            } else {
                gmVar.mo11262a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.im$f")
    /* renamed from: com.fossil.blesdk.obfuscated.im$f */
    public class C2044f extends com.fossil.blesdk.obfuscated.C2141jm<TResult> {
        @DexIgnore
        public C2044f(com.fossil.blesdk.obfuscated.C2037im imVar) {
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.im$g */
    public interface C2045g {
        @DexIgnore
        /* renamed from: a */
        void mo12050a(com.fossil.blesdk.obfuscated.C2037im<?> imVar, bolts.UnobservedTaskException unobservedTaskException);
    }

    /*
    static {
        com.fossil.blesdk.obfuscated.C1812fm.m7055a();
        com.fossil.blesdk.obfuscated.C1491bm.m5039b();
    }
    */

    @DexIgnore
    public C2037im() {
    }

    @DexIgnore
    /* renamed from: h */
    public static <TResult> com.fossil.blesdk.obfuscated.C2037im<TResult> m8542h() {
        return f6113n;
    }

    @DexIgnore
    /* renamed from: i */
    public static <TResult> com.fossil.blesdk.obfuscated.C2037im<TResult>.f m8543i() {
        return new com.fossil.blesdk.obfuscated.C2037im.C2044f(new com.fossil.blesdk.obfuscated.C2037im());
    }

    @DexIgnore
    /* renamed from: j */
    public static com.fossil.blesdk.obfuscated.C2037im.C2045g m8544j() {
        return f6109j;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo12043c() {
        boolean z;
        synchronized (this.f6114a) {
            z = this.f6116c;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo12044d() {
        boolean z;
        synchronized (this.f6114a) {
            z = this.f6115b;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo12045e() {
        boolean z;
        synchronized (this.f6114a) {
            z = mo12036a() != null;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo12046f() {
        synchronized (this.f6114a) {
            for (com.fossil.blesdk.obfuscated.C1958hm then : this.f6121h) {
                try {
                    then.then(this);
                } catch (java.lang.RuntimeException e) {
                    throw e;
                } catch (java.lang.Exception e2) {
                    throw new java.lang.RuntimeException(e2);
                }
            }
            this.f6121h = null;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo12047g() {
        synchronized (this.f6114a) {
            if (this.f6115b) {
                return false;
            }
            this.f6115b = true;
            this.f6116c = true;
            this.f6114a.notifyAll();
            mo12046f();
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Exception mo12036a() {
        java.lang.Exception exc;
        synchronized (this.f6114a) {
            if (this.f6118e != null) {
                this.f6119f = true;
                if (this.f6120g != null) {
                    this.f6120g.mo12837a();
                    this.f6120g = null;
                }
            }
            exc = this.f6118e;
        }
        return exc;
    }

    @DexIgnore
    /* renamed from: b */
    public TResult mo12041b() {
        TResult tresult;
        synchronized (this.f6114a) {
            tresult = this.f6117d;
        }
        return tresult;
    }

    @DexIgnore
    public C2037im(TResult tresult) {
        mo12038a(tresult);
    }

    @DexIgnore
    /* renamed from: d */
    public static <TContinuationResult, TResult> void m8541d(com.fossil.blesdk.obfuscated.C2141jm<TContinuationResult> jmVar, com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult> hmVar, com.fossil.blesdk.obfuscated.C2037im<TResult> imVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
        try {
            executor.execute(new com.fossil.blesdk.obfuscated.C2037im.C2041d(gmVar, jmVar, hmVar, imVar));
        } catch (java.lang.Exception e) {
            jmVar.mo12393a((java.lang.Exception) new bolts.ExecutorException(e));
        }
    }

    @DexIgnore
    /* renamed from: c */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12042c(com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult> hmVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
        return mo12034a(new com.fossil.blesdk.obfuscated.C2037im.C2040c(this, gmVar, hmVar), executor);
    }

    @DexIgnore
    /* renamed from: b */
    public static <TResult> com.fossil.blesdk.obfuscated.C2037im<TResult> m8538b(TResult tresult) {
        if (tresult == null) {
            return f6110k;
        }
        if (tresult instanceof java.lang.Boolean) {
            return ((java.lang.Boolean) tresult).booleanValue() ? f6111l : f6112m;
        }
        com.fossil.blesdk.obfuscated.C2141jm jmVar = new com.fossil.blesdk.obfuscated.C2141jm();
        jmVar.mo12394a(tresult);
        return jmVar.mo12392a();
    }

    @DexIgnore
    /* renamed from: c */
    public static <TContinuationResult, TResult> void m8540c(com.fossil.blesdk.obfuscated.C2141jm<TContinuationResult> jmVar, com.fossil.blesdk.obfuscated.C1958hm<TResult, com.fossil.blesdk.obfuscated.C2037im<TContinuationResult>> hmVar, com.fossil.blesdk.obfuscated.C2037im<TResult> imVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
        try {
            executor.execute(new com.fossil.blesdk.obfuscated.C2037im.C2042e(gmVar, jmVar, hmVar, imVar));
        } catch (java.lang.Exception e) {
            jmVar.mo12393a((java.lang.Exception) new bolts.ExecutorException(e));
        }
    }

    @DexIgnore
    public C2037im(boolean z) {
        if (z) {
            mo12047g();
        } else {
            mo12038a((java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12035a(com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult> hmVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
        boolean d;
        com.fossil.blesdk.obfuscated.C2141jm jmVar = new com.fossil.blesdk.obfuscated.C2141jm();
        synchronized (this.f6114a) {
            d = mo12044d();
            if (!d) {
                java.util.List<com.fossil.blesdk.obfuscated.C1958hm<TResult, java.lang.Void>> list = this.f6121h;
                com.fossil.blesdk.obfuscated.C2037im.C2038a aVar = new com.fossil.blesdk.obfuscated.C2037im.C2038a(this, jmVar, hmVar, executor, gmVar);
                list.add(aVar);
            }
        }
        if (d) {
            m8541d(jmVar, hmVar, this, executor, gmVar);
        }
        return jmVar.mo12392a();
    }

    @DexIgnore
    /* renamed from: b */
    public static <TResult> com.fossil.blesdk.obfuscated.C2037im<TResult> m8537b(java.lang.Exception exc) {
        com.fossil.blesdk.obfuscated.C2141jm jmVar = new com.fossil.blesdk.obfuscated.C2141jm();
        jmVar.mo12393a(exc);
        return jmVar.mo12392a();
    }

    @DexIgnore
    /* renamed from: b */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12040b(com.fossil.blesdk.obfuscated.C1958hm<TResult, com.fossil.blesdk.obfuscated.C2037im<TContinuationResult>> hmVar, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C1881gm gmVar) {
        boolean d;
        com.fossil.blesdk.obfuscated.C2141jm jmVar = new com.fossil.blesdk.obfuscated.C2141jm();
        synchronized (this.f6114a) {
            d = mo12044d();
            if (!d) {
                java.util.List<com.fossil.blesdk.obfuscated.C1958hm<TResult, java.lang.Void>> list = this.f6121h;
                com.fossil.blesdk.obfuscated.C2037im.C2039b bVar = new com.fossil.blesdk.obfuscated.C2037im.C2039b(this, jmVar, hmVar, executor, gmVar);
                list.add(bVar);
            }
        }
        if (d) {
            m8540c(jmVar, hmVar, this, executor, gmVar);
        }
        return jmVar.mo12392a();
    }

    @DexIgnore
    /* renamed from: a */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12033a(com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult> hmVar) {
        return mo12035a(hmVar, f6108i, (com.fossil.blesdk.obfuscated.C1881gm) null);
    }

    @DexIgnore
    /* renamed from: a */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12034a(com.fossil.blesdk.obfuscated.C1958hm<TResult, com.fossil.blesdk.obfuscated.C2037im<TContinuationResult>> hmVar, java.util.concurrent.Executor executor) {
        return mo12040b(hmVar, executor, (com.fossil.blesdk.obfuscated.C1881gm) null);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12038a(TResult tresult) {
        synchronized (this.f6114a) {
            if (this.f6115b) {
                return false;
            }
            this.f6115b = true;
            this.f6117d = tresult;
            this.f6114a.notifyAll();
            mo12046f();
            return true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public <TContinuationResult> com.fossil.blesdk.obfuscated.C2037im<TContinuationResult> mo12039b(com.fossil.blesdk.obfuscated.C1958hm<TResult, TContinuationResult> hmVar) {
        return mo12042c(hmVar, f6108i, (com.fossil.blesdk.obfuscated.C1881gm) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    @DexIgnore
    /* renamed from: a */
    public boolean mo12037a(java.lang.Exception exc) {
        synchronized (this.f6114a) {
            if (this.f6115b) {
                return false;
            }
            this.f6115b = true;
            this.f6118e = exc;
            this.f6119f = false;
            this.f6114a.notifyAll();
            mo12046f();
            if (!this.f6119f && m8544j() != null) {
                this.f6120g = new com.fossil.blesdk.obfuscated.C2230km(this);
            }
        }
    }
}
