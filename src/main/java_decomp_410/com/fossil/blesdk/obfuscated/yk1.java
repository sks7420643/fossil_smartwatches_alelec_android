package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yk1 {
    @DexIgnore
    public /* final */ hm0 a;
    @DexIgnore
    public long b;

    @DexIgnore
    public yk1(hm0 hm0) {
        bk0.a(hm0);
        this.a = hm0;
    }

    @DexIgnore
    public final void a() {
        this.b = 0;
    }

    @DexIgnore
    public final void b() {
        this.b = this.a.c();
    }

    @DexIgnore
    public final boolean a(long j) {
        if (this.b != 0 && this.a.c() - this.b < 3600000) {
            return false;
        }
        return true;
    }
}
