package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w8 */
public interface C3173w8 {
    @DexIgnore
    boolean isNestedScrollingEnabled();

    @DexIgnore
    void setNestedScrollingEnabled(boolean z);

    @DexIgnore
    void stopNestedScroll();
}
