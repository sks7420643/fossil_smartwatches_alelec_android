package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o11 extends yz0 {
    @DexIgnore
    public /* final */ /* synthetic */ DataSet s;
    @DexIgnore
    public /* final */ /* synthetic */ boolean t; // = false;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o11(n11 n11, ge0 ge0, DataSet dataSet, boolean z) {
        super(ge0);
        this.s = dataSet;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        ((x01) ((tz0) bVar).x()).a(new fq0(this.s, (g11) new u11(this), this.t));
    }
}
