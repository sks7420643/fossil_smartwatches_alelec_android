package com.fossil.blesdk.obfuscated;

import java.net.InetSocketAddress;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fm4 {
    @DexIgnore
    public /* final */ gl4 a;
    @DexIgnore
    public /* final */ Proxy b;
    @DexIgnore
    public /* final */ InetSocketAddress c;

    @DexIgnore
    public fm4(gl4 gl4, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (gl4 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.a = gl4;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    @DexIgnore
    public gl4 a() {
        return this.a;
    }

    @DexIgnore
    public Proxy b() {
        return this.b;
    }

    @DexIgnore
    public boolean c() {
        return this.a.i != null && this.b.type() == Proxy.Type.HTTP;
    }

    @DexIgnore
    public InetSocketAddress d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof fm4) {
            fm4 fm4 = (fm4) obj;
            return fm4.a.equals(this.a) && fm4.b.equals(this.b) && fm4.c.equals(this.c);
        }
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Route{" + this.c + "}";
    }
}
