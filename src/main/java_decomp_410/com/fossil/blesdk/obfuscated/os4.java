package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gr4;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class os4 extends gr4.a {
    @DexIgnore
    public static os4 a() {
        return new os4();
    }

    @DexIgnore
    public gr4<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (type == String.class || type == Boolean.TYPE || type == Boolean.class || type == Byte.TYPE || type == Byte.class || type == Character.TYPE || type == Character.class || type == Double.TYPE || type == Double.class || type == Float.TYPE || type == Float.class || type == Integer.TYPE || type == Integer.class || type == Long.TYPE || type == Long.class || type == Short.TYPE || type == Short.class) {
            return es4.a;
        }
        return null;
    }

    @DexIgnore
    public gr4<em4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == String.class) {
            return ns4.a;
        }
        if (type == Boolean.class || type == Boolean.TYPE) {
            return fs4.a;
        }
        if (type == Byte.class || type == Byte.TYPE) {
            return gs4.a;
        }
        if (type == Character.class || type == Character.TYPE) {
            return hs4.a;
        }
        if (type == Double.class || type == Double.TYPE) {
            return is4.a;
        }
        if (type == Float.class || type == Float.TYPE) {
            return js4.a;
        }
        if (type == Integer.class || type == Integer.TYPE) {
            return ks4.a;
        }
        if (type == Long.class || type == Long.TYPE) {
            return ls4.a;
        }
        if (type == Short.class || type == Short.TYPE) {
            return ms4.a;
        }
        return null;
    }
}
