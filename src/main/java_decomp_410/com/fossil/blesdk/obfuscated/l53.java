package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l53 {
    @DexIgnore
    public /* final */ k53 a;

    @DexIgnore
    public l53(k53 k53) {
        kd4.b(k53, "mWeatherSettingContractView");
        this.a = k53;
    }

    @DexIgnore
    public final k53 a() {
        return this.a;
    }
}
