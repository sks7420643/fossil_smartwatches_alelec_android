package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c52 implements Factory<ak3> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public c52(n42 n42, Provider<InAppNotificationRepository> provider) {
        this.a = n42;
        this.b = provider;
    }

    @DexIgnore
    public static c52 a(n42 n42, Provider<InAppNotificationRepository> provider) {
        return new c52(n42, provider);
    }

    @DexIgnore
    public static ak3 b(n42 n42, Provider<InAppNotificationRepository> provider) {
        return a(n42, provider.get());
    }

    @DexIgnore
    public static ak3 a(n42 n42, InAppNotificationRepository inAppNotificationRepository) {
        ak3 a2 = n42.a(inAppNotificationRepository);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ak3 get() {
        return b(this.a, this.b);
    }
}
