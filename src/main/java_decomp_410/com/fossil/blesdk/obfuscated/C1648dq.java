package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dq */
public class C1648dq {

    @DexIgnore
    /* renamed from: a */
    public boolean f4507a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Handler f4508b; // = new android.os.Handler(android.os.Looper.getMainLooper(), new com.fossil.blesdk.obfuscated.C1648dq.C1649a());

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dq$a */
    public static final class C1649a implements android.os.Handler.Callback {
        @DexIgnore
        public boolean handleMessage(android.os.Message message) {
            if (message.what != 1) {
                return false;
            }
            ((com.fossil.blesdk.obfuscated.C1438aq) message.obj).mo8887a();
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo10092a(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        if (this.f4507a) {
            this.f4508b.obtainMessage(1, aqVar).sendToTarget();
        } else {
            this.f4507a = true;
            aqVar.mo8887a();
            this.f4507a = false;
        }
    }
}
