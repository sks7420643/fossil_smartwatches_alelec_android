package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class if3 implements Factory<GoalTrackingDetailPresenter> {
    @DexIgnore
    public static GoalTrackingDetailPresenter a(ef3 ef3, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, h42 h42) {
        return new GoalTrackingDetailPresenter(ef3, goalTrackingRepository, goalTrackingDao, goalTrackingDatabase, h42);
    }
}
