package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class y72 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSettingRepository.Anon8 e;
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSetting f;
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSettingDataSource.PushMicroAppSettingToServerCallback g;

    @DexIgnore
    public /* synthetic */ y72(MicroAppSettingRepository.Anon8 anon8, MicroAppSetting microAppSetting, MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
        this.e = anon8;
        this.f = microAppSetting;
        this.g = pushMicroAppSettingToServerCallback;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
