package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gr */
public class C1886gr<Data> implements com.fossil.blesdk.obfuscated.C2912sr<byte[], Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1886gr.C1889b<Data> f5500a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.gr$a */
    public static class C1887a implements com.fossil.blesdk.obfuscated.C2984tr<byte[], java.nio.ByteBuffer> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.gr$a$a */
        public class C1888a implements com.fossil.blesdk.obfuscated.C1886gr.C1889b<java.nio.ByteBuffer> {
            @DexIgnore
            public C1888a(com.fossil.blesdk.obfuscated.C1886gr.C1887a aVar) {
            }

            @DexIgnore
            public java.lang.Class<java.nio.ByteBuffer> getDataClass() {
                return java.nio.ByteBuffer.class;
            }

            @DexIgnore
            /* renamed from: a */
            public java.nio.ByteBuffer m7498a(byte[] bArr) {
                return java.nio.ByteBuffer.wrap(bArr);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<byte[], java.nio.ByteBuffer> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1886gr(new com.fossil.blesdk.obfuscated.C1886gr.C1887a.C1888a(this));
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.gr$b */
    public interface C1889b<Data> {
        @DexIgnore
        /* renamed from: a */
        Data mo11291a(byte[] bArr);

        @DexIgnore
        java.lang.Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.gr$c */
    public static class C1890c<Data> implements com.fossil.blesdk.obfuscated.C2902so<Data> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ byte[] f5501e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C1886gr.C1889b<Data> f5502f;

        @DexIgnore
        public C1890c(byte[] bArr, com.fossil.blesdk.obfuscated.C1886gr.C1889b<Data> bVar) {
            this.f5501e = bArr;
            this.f5502f = bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Data> aVar) {
            aVar.mo9252a(this.f5502f.mo11291a(this.f5501e));
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<Data> getDataClass() {
            return this.f5502f.getDataClass();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr$d")
    /* renamed from: com.fossil.blesdk.obfuscated.gr$d */
    public static class C1891d implements com.fossil.blesdk.obfuscated.C2984tr<byte[], java.io.InputStream> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gr$d$a")
        /* renamed from: com.fossil.blesdk.obfuscated.gr$d$a */
        public class C1892a implements com.fossil.blesdk.obfuscated.C1886gr.C1889b<java.io.InputStream> {
            @DexIgnore
            public C1892a(com.fossil.blesdk.obfuscated.C1886gr.C1891d dVar) {
            }

            @DexIgnore
            public java.lang.Class<java.io.InputStream> getDataClass() {
                return java.io.InputStream.class;
            }

            @DexIgnore
            /* renamed from: a */
            public java.io.InputStream m7505a(byte[] bArr) {
                return new java.io.ByteArrayInputStream(bArr);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<byte[], java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1886gr(new com.fossil.blesdk.obfuscated.C1886gr.C1891d.C1892a(this));
        }
    }

    @DexIgnore
    public C1886gr(com.fossil.blesdk.obfuscated.C1886gr.C1889b<Data> bVar) {
        this.f5500a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(byte[] bArr) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(byte[] bArr, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(bArr), new com.fossil.blesdk.obfuscated.C1886gr.C1890c(bArr, this.f5500a));
    }
}
