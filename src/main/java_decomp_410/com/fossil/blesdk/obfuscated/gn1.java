package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gn1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gn1> CREATOR; // = new hn1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ ud0 f;
    @DexIgnore
    public /* final */ dk0 g;

    @DexIgnore
    public gn1(int i, ud0 ud0, dk0 dk0) {
        this.e = i;
        this.f = ud0;
        this.g = dk0;
    }

    @DexIgnore
    public final ud0 H() {
        return this.f;
    }

    @DexIgnore
    public final dk0 I() {
        return this.g;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, (Parcelable) this.f, i, false);
        kk0.a(parcel, 3, (Parcelable) this.g, i, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public gn1(int i) {
        this(new ud0(8, (PendingIntent) null), (dk0) null);
    }

    @DexIgnore
    public gn1(ud0 ud0, dk0 dk0) {
        this(1, ud0, (dk0) null);
    }
}
