package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fossil.blesdk.obfuscated.zj0;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class aq0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<aq0> CREATOR; // = new eq0();
    @DexIgnore
    public static /* final */ TimeUnit i; // = TimeUnit.MILLISECONDS;
    @DexIgnore
    public /* final */ ep0 e;
    @DexIgnore
    public /* final */ List<DataSet> f;
    @DexIgnore
    public /* final */ List<DataPoint> g;
    @DexIgnore
    public /* final */ g11 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public ep0 a;
        @DexIgnore
        public List<DataSet> b; // = new ArrayList();
        @DexIgnore
        public List<DataPoint> c; // = new ArrayList();
        @DexIgnore
        public List<zo0> d; // = new ArrayList();

        @DexIgnore
        public a a(ep0 ep0) {
            this.a = ep0;
            return this;
        }

        @DexIgnore
        public a a(DataSet dataSet) {
            bk0.a(dataSet != null, (Object) "Must specify a valid data set.");
            zo0 J = dataSet.J();
            bk0.b(!this.d.contains(J), "Data set for this data source %s is already added.", J);
            bk0.a(!dataSet.I().isEmpty(), (Object) "No data points specified in the input data set.");
            this.d.add(J);
            this.b.add(dataSet);
            return this;
        }

        @DexIgnore
        public aq0 a() {
            boolean z = true;
            bk0.b(this.a != null, "Must specify a valid session.");
            if (this.a.a(TimeUnit.MILLISECONDS) == 0) {
                z = false;
            }
            bk0.b(z, "Must specify a valid end time, cannot insert a continuing session.");
            for (DataSet I : this.b) {
                for (DataPoint a2 : I.I()) {
                    a(a2);
                }
            }
            for (DataPoint a3 : this.c) {
                a(a3);
            }
            return new aq0(this);
        }

        @DexIgnore
        public final void a(DataPoint dataPoint) {
            DataPoint dataPoint2 = dataPoint;
            long b2 = this.a.b(TimeUnit.NANOSECONDS);
            long a2 = this.a.a(TimeUnit.NANOSECONDS);
            long c2 = dataPoint2.c(TimeUnit.NANOSECONDS);
            if (c2 != 0) {
                if (c2 < b2 || c2 > a2) {
                    c2 = q11.a(c2, TimeUnit.NANOSECONDS, aq0.i);
                }
                bk0.b(c2 >= b2 && c2 <= a2, "Data point %s has time stamp outside session interval [%d, %d]", dataPoint2, Long.valueOf(b2), Long.valueOf(a2));
                if (dataPoint2.c(TimeUnit.NANOSECONDS) != c2) {
                    Log.w("Fitness", String.format("Data point timestamp [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.c(TimeUnit.NANOSECONDS)), Long.valueOf(c2), aq0.i}));
                    dataPoint2.a(c2, TimeUnit.NANOSECONDS);
                }
            }
            long b3 = this.a.b(TimeUnit.NANOSECONDS);
            long a3 = this.a.a(TimeUnit.NANOSECONDS);
            long b4 = dataPoint2.b(TimeUnit.NANOSECONDS);
            long a4 = dataPoint2.a(TimeUnit.NANOSECONDS);
            if (b4 != 0 && a4 != 0) {
                if (a4 > a3) {
                    a4 = q11.a(a4, TimeUnit.NANOSECONDS, aq0.i);
                }
                bk0.b(b4 >= b3 && a4 <= a3, "Data point %s has start and end times outside session interval [%d, %d]", dataPoint2, Long.valueOf(b3), Long.valueOf(a3));
                if (a4 != dataPoint2.a(TimeUnit.NANOSECONDS)) {
                    Log.w("Fitness", String.format("Data point end time [%d] is truncated to [%d] to match the precision [%s] of the session start and end time", new Object[]{Long.valueOf(dataPoint2.a(TimeUnit.NANOSECONDS)), Long.valueOf(a4), aq0.i}));
                    dataPoint.a(b4, a4, TimeUnit.NANOSECONDS);
                }
            }
        }
    }

    @DexIgnore
    public aq0(ep0 ep0, List<DataSet> list, List<DataPoint> list2, IBinder iBinder) {
        this.e = ep0;
        this.f = Collections.unmodifiableList(list);
        this.g = Collections.unmodifiableList(list2);
        this.h = h11.a(iBinder);
    }

    @DexIgnore
    public List<DataPoint> H() {
        return this.g;
    }

    @DexIgnore
    public List<DataSet> I() {
        return this.f;
    }

    @DexIgnore
    public ep0 J() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof aq0) {
                aq0 aq0 = (aq0) obj;
                if (zj0.a(this.e, aq0.e) && zj0.a(this.f, aq0.f) && zj0.a(this.g, aq0.g)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(this.e, this.f, this.g);
    }

    @DexIgnore
    public String toString() {
        zj0.a a2 = zj0.a((Object) this);
        a2.a(Constants.SESSION, this.e);
        a2.a("dataSets", this.f);
        a2.a("aggregateDataPoints", this.g);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) J(), i2, false);
        kk0.b(parcel, 2, I(), false);
        kk0.b(parcel, 3, H(), false);
        g11 g11 = this.h;
        kk0.a(parcel, 4, g11 == null ? null : g11.asBinder(), false);
        kk0.a(parcel, a2);
    }

    @DexIgnore
    public aq0(a aVar) {
        this(aVar.a, (List<DataSet>) aVar.b, (List<DataPoint>) aVar.c, (g11) null);
    }

    @DexIgnore
    public aq0(aq0 aq0, g11 g11) {
        this(aq0.e, aq0.f, aq0.g, g11);
    }

    @DexIgnore
    public aq0(ep0 ep0, List<DataSet> list, List<DataPoint> list2, g11 g11) {
        this.e = ep0;
        this.f = Collections.unmodifiableList(list);
        this.g = Collections.unmodifiableList(list2);
        this.h = g11;
    }
}
