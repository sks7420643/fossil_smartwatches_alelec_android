package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mn */
public class C2426mn extends java.io.ByteArrayOutputStream {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C1494bn f7551e;

    @DexIgnore
    public C2426mn(com.fossil.blesdk.obfuscated.C1494bn bnVar, int i) {
        this.f7551e = bnVar;
        this.buf = this.f7551e.mo9233a(java.lang.Math.max(i, 256));
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13686b(int i) {
        int i2 = this.count;
        if (i2 + i > this.buf.length) {
            byte[] a = this.f7551e.mo9233a((i2 + i) * 2);
            java.lang.System.arraycopy(this.buf, 0, a, 0, this.count);
            this.f7551e.mo9232a(this.buf);
            this.buf = a;
        }
    }

    @DexIgnore
    public void close() throws java.io.IOException {
        this.f7551e.mo9232a(this.buf);
        this.buf = null;
        super.close();
    }

    @DexIgnore
    public void finalize() {
        this.f7551e.mo9232a(this.buf);
    }

    @DexIgnore
    public synchronized void write(byte[] bArr, int i, int i2) {
        mo13686b(i2);
        super.write(bArr, i, i2);
    }

    @DexIgnore
    public synchronized void write(int i) {
        mo13686b(1);
        super.write(i);
    }
}
