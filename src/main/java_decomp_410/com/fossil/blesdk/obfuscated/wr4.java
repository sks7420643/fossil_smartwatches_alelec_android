package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wr4<T> extends o84<qr4<T>> {
    @DexIgnore
    public /* final */ Call<T> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements y84, er4<T> {
        @DexIgnore
        public /* final */ Call<?> e;
        @DexIgnore
        public /* final */ q84<? super qr4<T>> f;
        @DexIgnore
        public volatile boolean g;
        @DexIgnore
        public boolean h; // = false;

        @DexIgnore
        public a(Call<?> call, q84<? super qr4<T>> q84) {
            this.e = call;
            this.f = q84;
        }

        @DexIgnore
        public boolean a() {
            return this.g;
        }

        @DexIgnore
        public void dispose() {
            this.g = true;
            this.e.cancel();
        }

        @DexIgnore
        public void onFailure(Call<T> call, Throwable th) {
            if (!call.o()) {
                try {
                    this.f.onError(th);
                } catch (Throwable th2) {
                    a94.b(th2);
                    ia4.b(new CompositeException(th, th2));
                }
            }
        }

        @DexIgnore
        public void onResponse(Call<T> call, qr4<T> qr4) {
            if (!this.g) {
                try {
                    this.f.onNext(qr4);
                    if (!this.g) {
                        this.h = true;
                        this.f.onComplete();
                    }
                } catch (Throwable th) {
                    a94.b(th);
                    ia4.b(new CompositeException(th, th));
                }
            }
        }
    }

    @DexIgnore
    public wr4(Call<T> call) {
        this.e = call;
    }

    @DexIgnore
    public void b(q84<? super qr4<T>> q84) {
        Call<T> clone = this.e.clone();
        a aVar = new a(clone, q84);
        q84.onSubscribe(aVar);
        if (!aVar.a()) {
            clone.a(aVar);
        }
    }
}
