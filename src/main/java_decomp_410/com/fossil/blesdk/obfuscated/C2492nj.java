package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nj */
public class C2492nj implements com.fossil.blesdk.obfuscated.C1804fj {

    @DexIgnore
    /* renamed from: c */
    public /* final */ androidx.lifecycle.MutableLiveData<com.fossil.blesdk.obfuscated.C1804fj.C1806b> f7803c; // = new androidx.lifecycle.MutableLiveData<>();

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3369yl<com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1809c> f7804d; // = com.fossil.blesdk.obfuscated.C3369yl.m16890e();

    @DexIgnore
    public C2492nj() {
        mo13990a(com.fossil.blesdk.obfuscated.C1804fj.f5196b);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13990a(com.fossil.blesdk.obfuscated.C1804fj.C1806b bVar) {
        this.f7803c.mo2280a(bVar);
        if (bVar instanceof com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1809c) {
            this.f7804d.mo3823b((com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1809c) bVar);
        } else if (bVar instanceof com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1807a) {
            this.f7804d.mo3821a(((com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1807a) bVar).mo10949a());
        }
    }
}
