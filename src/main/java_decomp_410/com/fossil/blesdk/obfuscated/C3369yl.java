package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yl */
public final class C3369yl<V> extends androidx.work.impl.utils.futures.AbstractFuture<V> {
    @DexIgnore
    /* renamed from: e */
    public static <V> com.fossil.blesdk.obfuscated.C3369yl<V> m16890e() {
        return new com.fossil.blesdk.obfuscated.C3369yl<>();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo3821a(java.lang.Throwable th) {
        return super.mo3821a(th);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo3823b(V v) {
        return super.mo3823b(v);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo3820a(com.fossil.blesdk.obfuscated.zv1<? extends V> zv1) {
        return super.mo3820a(zv1);
    }
}
