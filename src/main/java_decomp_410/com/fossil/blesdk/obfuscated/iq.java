package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.rq;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class iq<T extends rq> {
    @DexIgnore
    public /* final */ Queue<T> a; // = uw.a(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }

    @DexIgnore
    public T b() {
        T t = (rq) this.a.poll();
        return t == null ? a() : t;
    }
}
