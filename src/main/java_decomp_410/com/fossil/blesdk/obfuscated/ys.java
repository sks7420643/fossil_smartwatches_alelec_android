package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ys implements oo<Drawable> {
    @DexIgnore
    public /* final */ oo<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public ys(oo<Bitmap> ooVar, boolean z) {
        this.b = ooVar;
        this.c = z;
    }

    @DexIgnore
    public aq<Drawable> a(Context context, aq<Drawable> aqVar, int i, int i2) {
        jq c2 = rn.a(context).c();
        Drawable drawable = aqVar.get();
        aq<Bitmap> a = xs.a(c2, drawable, i, i2);
        if (a != null) {
            aq<Bitmap> a2 = this.b.a(context, a, i, i2);
            if (!a2.equals(a)) {
                return a(context, a2);
            }
            a2.a();
            return aqVar;
        } else if (!this.c) {
            return aqVar;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    public oo<BitmapDrawable> a() {
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ys) {
            return this.b.equals(((ys) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final aq<Drawable> a(Context context, aq<Bitmap> aqVar) {
        return dt.a(context.getResources(), aqVar);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
