package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i6 {
    @DexIgnore
    public Activity a;
    @DexIgnore
    public Intent b; // = new Intent().setAction("android.intent.action.SEND");
    @DexIgnore
    public CharSequence c;
    @DexIgnore
    public ArrayList<String> d;
    @DexIgnore
    public ArrayList<String> e;
    @DexIgnore
    public ArrayList<String> f;
    @DexIgnore
    public ArrayList<Uri> g;

    @DexIgnore
    public i6(Activity activity) {
        this.a = activity;
        this.b.putExtra("androidx.core.app.EXTRA_CALLING_PACKAGE", activity.getPackageName());
        this.b.putExtra("androidx.core.app.EXTRA_CALLING_ACTIVITY", activity.getComponentName());
        this.b.addFlags(524288);
    }

    @DexIgnore
    public static i6 a(Activity activity) {
        return new i6(activity);
    }

    @DexIgnore
    public Intent b() {
        ArrayList<String> arrayList = this.d;
        if (arrayList != null) {
            a("android.intent.extra.EMAIL", arrayList);
            this.d = null;
        }
        ArrayList<String> arrayList2 = this.e;
        if (arrayList2 != null) {
            a("android.intent.extra.CC", arrayList2);
            this.e = null;
        }
        ArrayList<String> arrayList3 = this.f;
        if (arrayList3 != null) {
            a("android.intent.extra.BCC", arrayList3);
            this.f = null;
        }
        ArrayList<Uri> arrayList4 = this.g;
        boolean z = true;
        if (arrayList4 == null || arrayList4.size() <= 1) {
            z = false;
        }
        boolean equals = this.b.getAction().equals("android.intent.action.SEND_MULTIPLE");
        if (!z && equals) {
            this.b.setAction("android.intent.action.SEND");
            ArrayList<Uri> arrayList5 = this.g;
            if (arrayList5 == null || arrayList5.isEmpty()) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putExtra("android.intent.extra.STREAM", this.g.get(0));
            }
            this.g = null;
        }
        if (z && !equals) {
            this.b.setAction("android.intent.action.SEND_MULTIPLE");
            ArrayList<Uri> arrayList6 = this.g;
            if (arrayList6 == null || arrayList6.isEmpty()) {
                this.b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.b.putParcelableArrayListExtra("android.intent.extra.STREAM", this.g);
            }
        }
        return this.b;
    }

    @DexIgnore
    public void c() {
        this.a.startActivity(a());
    }

    @DexIgnore
    public final void a(String str, ArrayList<String> arrayList) {
        String[] stringArrayExtra = this.b.getStringArrayExtra(str);
        int length = stringArrayExtra != null ? stringArrayExtra.length : 0;
        String[] strArr = new String[(arrayList.size() + length)];
        arrayList.toArray(strArr);
        if (stringArrayExtra != null) {
            System.arraycopy(stringArrayExtra, 0, strArr, arrayList.size(), length);
        }
        this.b.putExtra(str, strArr);
    }

    @DexIgnore
    public Intent a() {
        return Intent.createChooser(b(), this.c);
    }

    @DexIgnore
    public i6 a(String str) {
        if (this.d == null) {
            this.d = new ArrayList<>();
        }
        this.d.add(str);
        return this;
    }

    @DexIgnore
    public i6 b(String str) {
        this.b.setType(str);
        return this;
    }
}
