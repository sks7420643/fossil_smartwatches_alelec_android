package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ot2 extends RecyclerView.g<c> implements Filterable {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((fd4) null);
    @DexIgnore
    public List<AppWrapper> e; // = new ArrayList();
    @DexIgnore
    public b f;
    @DexIgnore
    public List<AppWrapper> g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ot2.h;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ gh2 a;
        @DexIgnore
        public /* final */ /* synthetic */ ot2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.e.b.g;
                    if (b == null) {
                        kd4.a();
                        throw null;
                    } else if (((AppWrapper) b.get(adapterPosition)).getUri() != null) {
                        List b2 = this.e.b.g;
                        if (b2 != null) {
                            InstalledApp installedApp = ((AppWrapper) b2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a = ot2.i.a();
                                local.d(a, "isSelected = " + booleanValue);
                                b c = this.e.b.f;
                                if (c != null) {
                                    List b3 = this.e.b.g;
                                    if (b3 != null) {
                                        c.a((AppWrapper) b3.get(adapterPosition), !booleanValue);
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ot2 ot2, gh2 gh2) {
            super(gh2.d());
            kd4.b(gh2, "binding");
            this.b = ot2;
            this.a = gh2;
            this.a.s.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            kd4.b(appWrapper, "appWrapper");
            ImageView imageView = this.a.r;
            kd4.a((Object) imageView, "binding.ivAppIcon");
            fk2 a2 = ck2.a(imageView.getContext());
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new zj2(installedApp)).b(appWrapper.getIconResourceId()).c().a(this.a.r);
                FlexibleTextView flexibleTextView = this.a.q;
                kd4.a((Object) flexibleTextView, "binding.ftvAppName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (appWrapper.getUri() != null) {
                    FlexibleTextView flexibleTextView2 = this.a.q;
                    kd4.a((Object) flexibleTextView2, "binding.ftvAppName");
                    flexibleTextView2.setTextColor(flexibleTextView2.getTextColors().withAlpha(255));
                    SwitchCompat switchCompat = this.a.s;
                    kd4.a((Object) switchCompat, "binding.swEnabled");
                    switchCompat.setEnabled(true);
                    SwitchCompat switchCompat2 = this.a.s;
                    kd4.a((Object) switchCompat2, "binding.swEnabled");
                    Drawable background = switchCompat2.getBackground();
                    kd4.a((Object) background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    FlexibleTextView flexibleTextView3 = this.a.q;
                    kd4.a((Object) flexibleTextView3, "binding.ftvAppName");
                    flexibleTextView3.setTextColor(flexibleTextView3.getTextColors().withAlpha(100));
                    SwitchCompat switchCompat3 = this.a.s;
                    kd4.a((Object) switchCompat3, "binding.swEnabled");
                    switchCompat3.setEnabled(false);
                    SwitchCompat switchCompat4 = this.a.s;
                    kd4.a((Object) switchCompat4, "binding.swEnabled");
                    Drawable background2 = switchCompat4.getBackground();
                    kd4.a((Object) background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                SwitchCompat switchCompat5 = this.a.s;
                kd4.a((Object) switchCompat5, "binding.swEnabled");
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    switchCompat5.setChecked(isSelected.booleanValue());
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ ot2 a;

        @DexIgnore
        public d(ot2 ot2) {
            this.a = ot2;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            kd4.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.e;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.e) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp != null) {
                            String title = installedApp.getTitle();
                            if (title != null) {
                                if (title != null) {
                                    str = title.toLowerCase();
                                    kd4.a((Object) str, "(this as java.lang.String).toLowerCase()");
                                    if (str != null && StringsKt__StringsKt.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                                        arrayList.add(appWrapper);
                                    }
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            }
                        }
                        str = null;
                        arrayList.add(appWrapper);
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            kd4.b(charSequence, "charSequence");
            kd4.b(filterResults, "results");
            this.a.g = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = ot2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationAppsAdapter::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public ot2() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new d(this);
    }

    @DexIgnore
    public int getItemCount() {
        List<AppWrapper> list = this.g;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public long getItemId(int i2) {
        return (long) i2;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i2) {
        kd4.b(viewGroup, "parent");
        gh2 a2 = gh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        kd4.a((Object) a2, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i2) {
        kd4.b(cVar, "holder");
        List<AppWrapper> list = this.g;
        if (list != null) {
            cVar.a(list.get(i2));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        kd4.b(list, "listAppWrapper");
        this.e.clear();
        this.e.addAll(list);
        this.g = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.f = bVar;
    }
}
