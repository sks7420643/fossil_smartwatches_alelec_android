package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jw */
public final class C2166jw implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.Object f6604b;

    @DexIgnore
    public C2166jw(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(obj);
        this.f6604b = obj;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        messageDigest.update(this.f6604b.toString().getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a));
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C2166jw) {
            return this.f6604b.equals(((com.fossil.blesdk.obfuscated.C2166jw) obj).f6604b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f6604b.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "ObjectKey{object=" + this.f6604b + '}';
    }
}
