package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.adapter.rxjava2.HttpException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vr4<T> extends o84<T> {
    @DexIgnore
    public /* final */ o84<qr4<T>> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements q84<qr4<R>> {
        @DexIgnore
        public /* final */ q84<? super R> e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public a(q84<? super R> q84) {
            this.e = q84;
        }

        @DexIgnore
        /* renamed from: a */
        public void onNext(qr4<R> qr4) {
            if (qr4.d()) {
                this.e.onNext(qr4.a());
                return;
            }
            this.f = true;
            HttpException httpException = new HttpException(qr4);
            try {
                this.e.onError(httpException);
            } catch (Throwable th) {
                a94.b(th);
                ia4.b(new CompositeException(httpException, th));
            }
        }

        @DexIgnore
        public void onComplete() {
            if (!this.f) {
                this.e.onComplete();
            }
        }

        @DexIgnore
        public void onError(Throwable th) {
            if (!this.f) {
                this.e.onError(th);
                return;
            }
            AssertionError assertionError = new AssertionError("This should never happen! Report as a bug with the full stacktrace.");
            assertionError.initCause(th);
            ia4.b(assertionError);
        }

        @DexIgnore
        public void onSubscribe(y84 y84) {
            this.e.onSubscribe(y84);
        }
    }

    @DexIgnore
    public vr4(o84<qr4<T>> o84) {
        this.e = o84;
    }

    @DexIgnore
    public void b(q84<? super T> q84) {
        this.e.a(new a(q84));
    }
}
