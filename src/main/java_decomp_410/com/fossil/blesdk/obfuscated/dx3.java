package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jv3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dx3 {
    @DexIgnore
    kv3 a(jv3 jv3) throws IOException;

    @DexIgnore
    xo4 a(hv3 hv3, long j) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(ax3 ax3) throws IOException;

    @DexIgnore
    void a(hv3 hv3) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    jv3.b c() throws IOException;

    @DexIgnore
    boolean d();
}
