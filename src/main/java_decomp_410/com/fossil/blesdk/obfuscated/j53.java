package com.fossil.blesdk.obfuscated;

import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j53 extends u52 {
    @DexIgnore
    public abstract void a(int i, boolean z);

    @DexIgnore
    public abstract void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken);

    @DexIgnore
    public abstract WeatherWatchAppSetting h();

    @DexIgnore
    public abstract void i();
}
