package com.fossil.blesdk.obfuscated;

import android.os.Process;
import com.android.volley.Request;
import com.fossil.blesdk.obfuscated.lm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mm extends Thread {
    @DexIgnore
    public static /* final */ boolean k; // = xm.b;
    @DexIgnore
    public /* final */ BlockingQueue<Request<?>> e;
    @DexIgnore
    public /* final */ BlockingQueue<Request<?>> f;
    @DexIgnore
    public /* final */ lm g;
    @DexIgnore
    public /* final */ vm h;
    @DexIgnore
    public volatile boolean i; // = false;
    @DexIgnore
    public /* final */ b j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Request e;

        @DexIgnore
        public a(Request request) {
            this.e = request;
        }

        @DexIgnore
        public void run() {
            try {
                mm.this.f.put(this.e);
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Request.b {
        @DexIgnore
        public /* final */ Map<String, List<Request<?>>> a; // = new HashMap();
        @DexIgnore
        public /* final */ mm b;

        @DexIgnore
        public b(mm mmVar) {
            this.b = mmVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
            return false;
         */
        @DexIgnore
        public final synchronized boolean b(Request<?> request) {
            String cacheKey = request.getCacheKey();
            if (this.a.containsKey(cacheKey)) {
                List list = this.a.get(cacheKey);
                if (list == null) {
                    list = new ArrayList();
                }
                request.addMarker("waiting-for-response");
                list.add(request);
                this.a.put(cacheKey, list);
                if (xm.b) {
                    xm.b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                }
            } else {
                this.a.put(cacheKey, (Object) null);
                request.setNetworkRequestCompleteListener(this);
                if (xm.b) {
                    xm.b("new request, sending to network %s", cacheKey);
                }
            }
        }

        @DexIgnore
        public void a(Request<?> request, um<?> umVar) {
            List<Request> remove;
            lm.a aVar = umVar.b;
            if (aVar == null || aVar.a()) {
                a(request);
                return;
            }
            String cacheKey = request.getCacheKey();
            synchronized (this) {
                remove = this.a.remove(cacheKey);
            }
            if (remove != null) {
                if (xm.b) {
                    xm.d("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                }
                for (Request a2 : remove) {
                    this.b.h.a((Request<?>) a2, umVar);
                }
            }
        }

        @DexIgnore
        public synchronized void a(Request<?> request) {
            String cacheKey = request.getCacheKey();
            List remove = this.a.remove(cacheKey);
            if (remove != null && !remove.isEmpty()) {
                if (xm.b) {
                    xm.d("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), cacheKey);
                }
                Request request2 = (Request) remove.remove(0);
                this.a.put(cacheKey, remove);
                request2.setNetworkRequestCompleteListener(this);
                try {
                    this.b.f.put(request2);
                } catch (InterruptedException e) {
                    xm.c("Couldn't add request to queue. %s", e.toString());
                    Thread.currentThread().interrupt();
                    this.b.b();
                }
            }
            return;
        }
    }

    @DexIgnore
    public mm(BlockingQueue<Request<?>> blockingQueue, BlockingQueue<Request<?>> blockingQueue2, lm lmVar, vm vmVar) {
        this.e = blockingQueue;
        this.f = blockingQueue2;
        this.g = lmVar;
        this.h = vmVar;
        this.j = new b(this);
    }

    @DexIgnore
    public void run() {
        if (k) {
            xm.d("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.g.d();
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.i) {
                    Thread.currentThread().interrupt();
                    return;
                }
                xm.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        a(this.e.take());
    }

    @DexIgnore
    public void b() {
        this.i = true;
        interrupt();
    }

    @DexIgnore
    public void a(Request<?> request) throws InterruptedException {
        request.addMarker("cache-queue-take");
        if (request.isCanceled()) {
            request.finish("cache-discard-canceled");
            return;
        }
        lm.a a2 = this.g.a(request.getCacheKey());
        if (a2 == null) {
            request.addMarker("cache-miss");
            if (!this.j.b(request)) {
                this.f.put(request);
            }
        } else if (a2.a()) {
            request.addMarker("cache-hit-expired");
            request.setCacheEntry(a2);
            if (!this.j.b(request)) {
                this.f.put(request);
            }
        } else {
            request.addMarker("cache-hit");
            um<?> parseNetworkResponse = request.parseNetworkResponse(new sm(a2.a, a2.g));
            request.addMarker("cache-hit-parsed");
            if (!a2.b()) {
                this.h.a(request, parseNetworkResponse);
                return;
            }
            request.addMarker("cache-hit-refresh-needed");
            request.setCacheEntry(a2);
            parseNetworkResponse.d = true;
            if (!this.j.b(request)) {
                this.h.a(request, parseNetworkResponse, new a(request));
            } else {
                this.h.a(request, parseNetworkResponse);
            }
        }
    }
}
