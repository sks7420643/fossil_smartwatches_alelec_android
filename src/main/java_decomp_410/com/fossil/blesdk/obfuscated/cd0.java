package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cd0 implements Parcelable.Creator<GoogleSignInOptions> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList<Scope> arrayList = null;
        Account account = null;
        String str = null;
        String str2 = null;
        ArrayList<ac0> arrayList2 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 2:
                    arrayList = SafeParcelReader.c(parcel, a, Scope.CREATOR);
                    break;
                case 3:
                    account = SafeParcelReader.a(parcel, a, Account.CREATOR);
                    break;
                case 4:
                    z = SafeParcelReader.i(parcel, a);
                    break;
                case 5:
                    z2 = SafeParcelReader.i(parcel, a);
                    break;
                case 6:
                    z3 = SafeParcelReader.i(parcel, a);
                    break;
                case 7:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 8:
                    str2 = SafeParcelReader.f(parcel, a);
                    break;
                case 9:
                    arrayList2 = SafeParcelReader.c(parcel, a, ac0.CREATOR);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new GoogleSignInOptions(i, arrayList, account, z, z2, z3, str, str2, arrayList2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInOptions[i];
    }
}
