package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jo */
public interface C2143jo {

    @DexIgnore
    /* renamed from: a */
    public static final java.nio.charset.Charset f6538a = java.nio.charset.Charset.forName("UTF-8");

    @DexIgnore
    /* renamed from: a */
    void mo8934a(java.security.MessageDigest messageDigest);

    @DexIgnore
    boolean equals(java.lang.Object obj);

    @DexIgnore
    int hashCode();
}
