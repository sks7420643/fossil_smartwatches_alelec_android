package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pf */
public class C2647pf {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ java.lang.String[] f8351m; // = {"UPDATE", "DELETE", "INSERT"};

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.Integer> f8352a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String[] f8353b;

    @DexIgnore
    /* renamed from: c */
    public java.util.Map<java.lang.String, java.util.Set<java.lang.String>> f8354c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ androidx.room.RoomDatabase f8355d;

    @DexIgnore
    /* renamed from: e */
    public java.util.concurrent.atomic.AtomicBoolean f8356e; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: f */
    public volatile boolean f8357f; // = false;

    @DexIgnore
    /* renamed from: g */
    public volatile com.fossil.blesdk.obfuscated.C2221kg f8358g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2647pf.C2649b f8359h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2566of f8360i;
    @android.annotation.SuppressLint({"RestrictedApi"})

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C2282l3<com.fossil.blesdk.obfuscated.C2647pf.C2650c, com.fossil.blesdk.obfuscated.C2647pf.C2651d> f8361j; // = new com.fossil.blesdk.obfuscated.C2282l3<>();

    @DexIgnore
    /* renamed from: k */
    public com.fossil.blesdk.obfuscated.C2732qf f8362k;

    @DexIgnore
    /* renamed from: l */
    public java.lang.Runnable f8363l; // = new com.fossil.blesdk.obfuscated.C2647pf.C2648a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pf$a */
    public class C2648a implements java.lang.Runnable {
        @DexIgnore
        public C2648a() {
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* renamed from: a */
        public final java.util.Set<java.lang.Integer> mo14694a() {
            com.fossil.blesdk.obfuscated.C1922h4 h4Var = new com.fossil.blesdk.obfuscated.C1922h4();
            android.database.Cursor query = com.fossil.blesdk.obfuscated.C2647pf.this.f8355d.query(new com.fossil.blesdk.obfuscated.C1801fg("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (query.moveToNext()) {
                try {
                    h4Var.add(java.lang.Integer.valueOf(query.getInt(0)));
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            if (!h4Var.isEmpty()) {
                com.fossil.blesdk.obfuscated.C2647pf.this.f8358g.mo12781n();
            }
            return h4Var;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1874gg a;
            java.util.concurrent.locks.Lock closeLock = com.fossil.blesdk.obfuscated.C2647pf.this.f8355d.getCloseLock();
            java.util.Set<java.lang.Integer> set = null;
            try {
                closeLock.lock();
                if (!com.fossil.blesdk.obfuscated.C2647pf.this.mo14683a()) {
                    closeLock.unlock();
                } else if (!com.fossil.blesdk.obfuscated.C2647pf.this.f8356e.compareAndSet(true, false)) {
                    closeLock.unlock();
                } else if (com.fossil.blesdk.obfuscated.C2647pf.this.f8355d.inTransaction()) {
                    closeLock.unlock();
                } else {
                    if (com.fossil.blesdk.obfuscated.C2647pf.this.f8355d.mWriteAheadLoggingEnabled) {
                        a = com.fossil.blesdk.obfuscated.C2647pf.this.f8355d.getOpenHelper().mo11634a();
                        a.mo11234s();
                        set = mo14694a();
                        a.mo11236u();
                        a.mo11237v();
                    } else {
                        set = mo14694a();
                    }
                    closeLock.unlock();
                    if (set != null && !set.isEmpty()) {
                        synchronized (com.fossil.blesdk.obfuscated.C2647pf.this.f8361j) {
                            java.util.Iterator<java.util.Map.Entry<com.fossil.blesdk.obfuscated.C2647pf.C2650c, com.fossil.blesdk.obfuscated.C2647pf.C2651d>> it = com.fossil.blesdk.obfuscated.C2647pf.this.f8361j.iterator();
                            while (it.hasNext()) {
                                ((com.fossil.blesdk.obfuscated.C2647pf.C2651d) it.next().getValue()).mo14702a(set);
                            }
                        }
                    }
                }
            } catch (android.database.sqlite.SQLiteException | java.lang.IllegalStateException e) {
                try {
                    android.util.Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
                } catch (Throwable th) {
                    closeLock.unlock();
                    throw th;
                }
            } catch (Throwable th2) {
                a.mo11237v();
                throw th2;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pf$b")
    /* renamed from: com.fossil.blesdk.obfuscated.pf$b */
    public static class C2649b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ long[] f8365a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean[] f8366b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int[] f8367c;

        @DexIgnore
        /* renamed from: d */
        public boolean f8368d;

        @DexIgnore
        /* renamed from: e */
        public boolean f8369e;

        @DexIgnore
        public C2649b(int i) {
            this.f8365a = new long[i];
            this.f8366b = new boolean[i];
            this.f8367c = new int[i];
            java.util.Arrays.fill(this.f8365a, 0);
            java.util.Arrays.fill(this.f8366b, false);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14696a(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.f8365a[i];
                    this.f8365a[i] = 1 + j;
                    if (j == 0) {
                        this.f8368d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14699b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.f8365a[i];
                    this.f8365a[i] = j - 1;
                    if (j == 1) {
                        this.f8368d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        /* renamed from: a */
        public int[] mo14697a() {
            synchronized (this) {
                if (this.f8368d) {
                    if (!this.f8369e) {
                        int length = this.f8365a.length;
                        int i = 0;
                        while (true) {
                            int i2 = 1;
                            if (i < length) {
                                boolean z = this.f8365a[i] > 0;
                                if (z != this.f8366b[i]) {
                                    int[] iArr = this.f8367c;
                                    if (!z) {
                                        i2 = 2;
                                    }
                                    iArr[i] = i2;
                                } else {
                                    this.f8367c[i] = 0;
                                }
                                this.f8366b[i] = z;
                                i++;
                            } else {
                                this.f8369e = true;
                                this.f8368d = false;
                                int[] iArr2 = this.f8367c;
                                return iArr2;
                            }
                        }
                    }
                }
                return null;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14698b() {
            synchronized (this) {
                this.f8369e = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pf$c")
    /* renamed from: com.fossil.blesdk.obfuscated.pf$c */
    public static abstract class C2650c {
        @DexIgnore
        public /* final */ java.lang.String[] mTables;

        @DexIgnore
        public C2650c(java.lang.String str, java.lang.String... strArr) {
            this.mTables = (java.lang.String[]) java.util.Arrays.copyOf(strArr, strArr.length + 1);
            this.mTables[strArr.length] = str;
        }

        @DexIgnore
        public boolean isRemote() {
            return false;
        }

        @DexIgnore
        public abstract void onInvalidated(java.util.Set<java.lang.String> set);

        @DexIgnore
        public C2650c(java.lang.String[] strArr) {
            this.mTables = (java.lang.String[]) java.util.Arrays.copyOf(strArr, strArr.length);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pf$d")
    /* renamed from: com.fossil.blesdk.obfuscated.pf$d */
    public static class C2651d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int[] f8370a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String[] f8371b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2647pf.C2650c f8372c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ java.util.Set<java.lang.String> f8373d;

        @DexIgnore
        public C2651d(com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar, int[] iArr, java.lang.String[] strArr) {
            this.f8372c = cVar;
            this.f8370a = iArr;
            this.f8371b = strArr;
            if (iArr.length == 1) {
                com.fossil.blesdk.obfuscated.C1922h4 h4Var = new com.fossil.blesdk.obfuscated.C1922h4();
                h4Var.add(this.f8371b[0]);
                this.f8373d = java.util.Collections.unmodifiableSet(h4Var);
                return;
            }
            this.f8373d = null;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14702a(java.util.Set<java.lang.Integer> set) {
            int length = this.f8370a.length;
            java.util.Set set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(java.lang.Integer.valueOf(this.f8370a[i]))) {
                    if (length == 1) {
                        set2 = this.f8373d;
                    } else {
                        if (set2 == null) {
                            set2 = new com.fossil.blesdk.obfuscated.C1922h4(length);
                        }
                        set2.add(this.f8371b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.f8372c.onInvalidated(set2);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14703a(java.lang.String[] strArr) {
            java.util.Set<java.lang.String> set = null;
            if (this.f8371b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.f8371b[0])) {
                        set = this.f8373d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                com.fossil.blesdk.obfuscated.C1922h4 h4Var = new com.fossil.blesdk.obfuscated.C1922h4();
                for (java.lang.String str : strArr) {
                    java.lang.String[] strArr2 = this.f8371b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        java.lang.String str2 = strArr2[i2];
                        if (str2.equalsIgnoreCase(str)) {
                            h4Var.add(str2);
                            break;
                        }
                        i2++;
                    }
                }
                if (h4Var.size() > 0) {
                    set = h4Var;
                }
            }
            if (set != null) {
                this.f8372c.onInvalidated(set);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pf$e")
    /* renamed from: com.fossil.blesdk.obfuscated.pf$e */
    public static class C2652e extends com.fossil.blesdk.obfuscated.C2647pf.C2650c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2647pf f8374a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C2647pf.C2650c> f8375b;

        @DexIgnore
        public C2652e(com.fossil.blesdk.obfuscated.C2647pf pfVar, com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar) {
            super(cVar.mTables);
            this.f8374a = pfVar;
            this.f8375b = new java.lang.ref.WeakReference<>(cVar);
        }

        @DexIgnore
        public void onInvalidated(java.util.Set<java.lang.String> set) {
            com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar = (com.fossil.blesdk.obfuscated.C2647pf.C2650c) this.f8375b.get();
            if (cVar == null) {
                this.f8374a.mo14690c((com.fossil.blesdk.obfuscated.C2647pf.C2650c) this);
            } else {
                cVar.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public C2647pf(androidx.room.RoomDatabase roomDatabase, java.util.Map<java.lang.String, java.lang.String> map, java.util.Map<java.lang.String, java.util.Set<java.lang.String>> map2, java.lang.String... strArr) {
        this.f8355d = roomDatabase;
        this.f8359h = new com.fossil.blesdk.obfuscated.C2647pf.C2649b(strArr.length);
        this.f8352a = new com.fossil.blesdk.obfuscated.C1855g4<>();
        this.f8354c = map2;
        this.f8360i = new com.fossil.blesdk.obfuscated.C2566of(this.f8355d);
        int length = strArr.length;
        this.f8353b = new java.lang.String[length];
        for (int i = 0; i < length; i++) {
            java.lang.String lowerCase = strArr[i].toLowerCase(java.util.Locale.US);
            this.f8352a.put(lowerCase, java.lang.Integer.valueOf(i));
            java.lang.String str = map.get(strArr[i]);
            if (str != null) {
                this.f8353b[i] = str.toLowerCase(java.util.Locale.US);
            } else {
                this.f8353b[i] = lowerCase;
            }
        }
        for (java.util.Map.Entry next : map.entrySet()) {
            java.lang.String lowerCase2 = ((java.lang.String) next.getValue()).toLowerCase(java.util.Locale.US);
            if (this.f8352a.containsKey(lowerCase2)) {
                java.lang.String lowerCase3 = ((java.lang.String) next.getKey()).toLowerCase(java.util.Locale.US);
                com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.Integer> g4Var = this.f8352a;
                g4Var.put(lowerCase3, g4Var.get(lowerCase2));
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14679a(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        synchronized (this) {
            if (this.f8357f) {
                android.util.Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            ggVar.mo11230b("PRAGMA temp_store = MEMORY;");
            ggVar.mo11230b("PRAGMA recursive_triggers='ON';");
            ggVar.mo11230b("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            mo14685b(ggVar);
            this.f8358g = ggVar.mo11231c("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f8357f = true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14686b(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i) {
        java.lang.String str = this.f8353b[i];
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (java.lang.String a : f8351m) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            m12228a(sb, str, a);
            ggVar.mo11230b(sb.toString());
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final java.lang.String[] mo14691c(java.lang.String[] strArr) {
        java.lang.String[] b = mo14688b(strArr);
        int length = b.length;
        int i = 0;
        while (i < length) {
            java.lang.String str = b[i];
            if (this.f8352a.containsKey(str.toLowerCase(java.util.Locale.US))) {
                i++;
            } else {
                throw new java.lang.IllegalArgumentException("There is no table with name " + str);
            }
        }
        return b;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo14692d() {
        com.fossil.blesdk.obfuscated.C2732qf qfVar = this.f8362k;
        if (qfVar != null) {
            qfVar.mo15214a();
            this.f8362k = null;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo14693e() {
        if (this.f8355d.isOpen()) {
            mo14685b(this.f8355d.getOpenHelper().mo11634a());
        }
    }

    @DexIgnore
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: c */
    public void mo14690c(com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar) {
        com.fossil.blesdk.obfuscated.C2647pf.C2651d remove;
        synchronized (this.f8361j) {
            remove = this.f8361j.remove(cVar);
        }
        if (remove != null && this.f8359h.mo14699b(remove.f8370a)) {
            mo14693e();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.String[] mo14688b(java.lang.String[] strArr) {
        com.fossil.blesdk.obfuscated.C1922h4 h4Var = new com.fossil.blesdk.obfuscated.C1922h4();
        for (java.lang.String str : strArr) {
            java.lang.String lowerCase = str.toLowerCase(java.util.Locale.US);
            if (this.f8354c.containsKey(lowerCase)) {
                h4Var.addAll(this.f8354c.get(lowerCase));
            } else {
                h4Var.add(str);
            }
        }
        return (java.lang.String[]) h4Var.toArray(new java.lang.String[h4Var.size()]);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14689c() {
        mo14693e();
        this.f8363l.run();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14678a(android.content.Context context, java.lang.String str) {
        this.f8362k = new com.fossil.blesdk.obfuscated.C2732qf(context, str, this, this.f8355d.getQueryExecutor());
    }

    @DexIgnore
    /* renamed from: a */
    public static void m12228a(java.lang.StringBuilder sb, java.lang.String str, java.lang.String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14687b(com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar) {
        mo14681a((com.fossil.blesdk.obfuscated.C2647pf.C2650c) new com.fossil.blesdk.obfuscated.C2647pf.C2652e(this, cVar));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14684b() {
        if (this.f8356e.compareAndSet(false, true)) {
            this.f8355d.getQueryExecutor().execute(this.f8363l);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14685b(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        if (!ggVar.mo11239x()) {
            while (true) {
                try {
                    java.util.concurrent.locks.Lock closeLock = this.f8355d.getCloseLock();
                    closeLock.lock();
                    try {
                        int[] a = this.f8359h.mo14697a();
                        if (a == null) {
                            closeLock.unlock();
                            return;
                        }
                        int length = a.length;
                        ggVar.mo11234s();
                        for (int i = 0; i < length; i++) {
                            int i2 = a[i];
                            if (i2 == 1) {
                                mo14680a(ggVar, i);
                            } else if (i2 == 2) {
                                mo14686b(ggVar, i);
                            }
                        }
                        ggVar.mo11236u();
                        ggVar.mo11237v();
                        this.f8359h.mo14698b();
                        closeLock.unlock();
                    } catch (Throwable th) {
                        closeLock.unlock();
                        throw th;
                    }
                } catch (android.database.sqlite.SQLiteException | java.lang.IllegalStateException e) {
                    android.util.Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
                    return;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14680a(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i) {
        ggVar.mo11230b("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i + ", 0)");
        java.lang.String str = this.f8353b[i];
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (java.lang.String str2 : f8351m) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            m12228a(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            ggVar.mo11230b(sb.toString());
        }
    }

    @DexIgnore
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: a */
    public void mo14681a(com.fossil.blesdk.obfuscated.C2647pf.C2650c cVar) {
        com.fossil.blesdk.obfuscated.C2647pf.C2651d b;
        java.lang.String[] b2 = mo14688b(cVar.mTables);
        int[] iArr = new int[b2.length];
        int length = b2.length;
        int i = 0;
        while (i < length) {
            java.lang.Integer num = this.f8352a.get(b2[i].toLowerCase(java.util.Locale.US));
            if (num != null) {
                iArr[i] = num.intValue();
                i++;
            } else {
                throw new java.lang.IllegalArgumentException("There is no table with name " + b2[i]);
            }
        }
        com.fossil.blesdk.obfuscated.C2647pf.C2651d dVar = new com.fossil.blesdk.obfuscated.C2647pf.C2651d(cVar, iArr, b2);
        synchronized (this.f8361j) {
            b = this.f8361j.mo12618b(cVar, dVar);
        }
        if (b == null && this.f8359h.mo14696a(iArr)) {
            mo14693e();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14683a() {
        if (!this.f8355d.isOpen()) {
            return false;
        }
        if (!this.f8357f) {
            this.f8355d.getOpenHelper().mo11634a();
        }
        if (this.f8357f) {
            return true;
        }
        android.util.Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14682a(java.lang.String... strArr) {
        synchronized (this.f8361j) {
            java.util.Iterator<java.util.Map.Entry<com.fossil.blesdk.obfuscated.C2647pf.C2650c, com.fossil.blesdk.obfuscated.C2647pf.C2651d>> it = this.f8361j.iterator();
            while (it.hasNext()) {
                java.util.Map.Entry next = it.next();
                if (!((com.fossil.blesdk.obfuscated.C2647pf.C2650c) next.getKey()).isRemote()) {
                    ((com.fossil.blesdk.obfuscated.C2647pf.C2651d) next.getValue()).mo14703a(strArr);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <T> androidx.lifecycle.LiveData<T> mo14677a(java.lang.String[] strArr, boolean z, java.util.concurrent.Callable<T> callable) {
        return this.f8360i.mo14411a(mo14691c(strArr), z, callable);
    }
}
