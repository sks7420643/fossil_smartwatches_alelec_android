package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.s1 */
public class C2851s1 extends com.fossil.blesdk.obfuscated.C1525c1<com.fossil.blesdk.obfuscated.C1927h7> implements android.view.Menu {
    @DexIgnore
    public C2851s1(android.content.Context context, com.fossil.blesdk.obfuscated.C1927h7 h7Var) {
        super(context, h7Var);
    }

    @DexIgnore
    public android.view.MenuItem add(java.lang.CharSequence charSequence) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).add(charSequence));
    }

    @DexIgnore
    public int addIntentOptions(int i, int i2, int i3, android.content.ComponentName componentName, android.content.Intent[] intentArr, android.content.Intent intent, int i4, android.view.MenuItem[] menuItemArr) {
        android.view.MenuItem[] menuItemArr2 = menuItemArr;
        android.view.MenuItem[] menuItemArr3 = menuItemArr2 != null ? new android.view.MenuItem[menuItemArr2.length] : null;
        int addIntentOptions = ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr3);
        if (menuItemArr3 != null) {
            int length = menuItemArr3.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr2[i5] = mo9376a(menuItemArr3[i5]);
            }
        }
        return addIntentOptions;
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(java.lang.CharSequence charSequence) {
        return mo9377a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).addSubMenu(charSequence));
    }

    @DexIgnore
    public void clear() {
        mo9379b();
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).clear();
    }

    @DexIgnore
    public void close() {
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).close();
    }

    @DexIgnore
    public android.view.MenuItem findItem(int i) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).findItem(i));
    }

    @DexIgnore
    public android.view.MenuItem getItem(int i) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).getItem(i));
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        return ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).hasVisibleItems();
    }

    @DexIgnore
    public boolean isShortcutKey(int i, android.view.KeyEvent keyEvent) {
        return ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).isShortcutKey(i, keyEvent);
    }

    @DexIgnore
    public boolean performIdentifierAction(int i, int i2) {
        return ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).performIdentifierAction(i, i2);
    }

    @DexIgnore
    public boolean performShortcut(int i, android.view.KeyEvent keyEvent, int i2) {
        return ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).performShortcut(i, keyEvent, i2);
    }

    @DexIgnore
    public void removeGroup(int i) {
        mo9378a(i);
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).removeGroup(i);
    }

    @DexIgnore
    public void removeItem(int i) {
        mo9380b(i);
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).removeItem(i);
    }

    @DexIgnore
    public void setGroupCheckable(int i, boolean z, boolean z2) {
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).setGroupCheckable(i, z, z2);
    }

    @DexIgnore
    public void setGroupEnabled(int i, boolean z) {
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).setGroupEnabled(i, z);
    }

    @DexIgnore
    public void setGroupVisible(int i, boolean z) {
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).setGroupVisible(i, z);
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).setQwertyMode(z);
    }

    @DexIgnore
    public int size() {
        return ((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).size();
    }

    @DexIgnore
    public android.view.MenuItem add(int i) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).add(i));
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i) {
        return mo9377a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).addSubMenu(i));
    }

    @DexIgnore
    public android.view.MenuItem add(int i, int i2, int i3, java.lang.CharSequence charSequence) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).add(i, i2, i3, charSequence));
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i, int i2, int i3, java.lang.CharSequence charSequence) {
        return mo9377a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).addSubMenu(i, i2, i3, charSequence));
    }

    @DexIgnore
    public android.view.MenuItem add(int i, int i2, int i3, int i4) {
        return mo9376a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).add(i, i2, i3, i4));
    }

    @DexIgnore
    public android.view.SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return mo9377a(((com.fossil.blesdk.obfuscated.C1927h7) this.f4235a).addSubMenu(i, i2, i3, i4));
    }
}
