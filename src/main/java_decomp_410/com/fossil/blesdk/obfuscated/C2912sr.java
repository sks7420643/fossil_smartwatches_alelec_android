package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sr */
public interface C2912sr<Model, Data> {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.sr$a */
    public static class C2913a<Data> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2143jo f9449a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2143jo> f9450b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C2902so<Data> f9451c;

        @DexIgnore
        public C2913a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C2902so<Data> soVar) {
            this(joVar, java.util.Collections.emptyList(), soVar);
        }

        @DexIgnore
        public C2913a(com.fossil.blesdk.obfuscated.C2143jo joVar, java.util.List<com.fossil.blesdk.obfuscated.C2143jo> list, com.fossil.blesdk.obfuscated.C2902so<Data> soVar) {
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(joVar);
            this.f9449a = joVar;
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(list);
            this.f9450b = list;
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(soVar);
            this.f9451c = soVar;
        }
    }

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(Model model, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar);

    @DexIgnore
    /* renamed from: a */
    boolean mo8912a(Model model);
}
