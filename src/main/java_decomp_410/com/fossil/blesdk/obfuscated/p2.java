package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.ListMenuItemView;
import androidx.appcompat.widget.ListPopupWindow;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p2 extends ListPopupWindow implements o2 {
    @DexIgnore
    public static Method N;
    @DexIgnore
    public o2 M;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends l2 {
        @DexIgnore
        public /* final */ int s;
        @DexIgnore
        public /* final */ int t;
        @DexIgnore
        public o2 u;
        @DexIgnore
        public MenuItem v;

        @DexIgnore
        public a(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.s = 22;
                this.t = 21;
                return;
            }
            this.s = 21;
            this.t = 22;
        }

        @DexIgnore
        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i;
            g1 g1Var;
            if (this.u != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    g1Var = (g1) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    g1Var = (g1) adapter;
                }
                k1 k1Var = null;
                if (motionEvent.getAction() != 10) {
                    int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
                    if (pointToPosition != -1) {
                        int i2 = pointToPosition - i;
                        if (i2 >= 0 && i2 < g1Var.getCount()) {
                            k1Var = g1Var.getItem(i2);
                        }
                    }
                }
                MenuItem menuItem = this.v;
                if (menuItem != k1Var) {
                    h1 b = g1Var.b();
                    if (menuItem != null) {
                        this.u.b(b, menuItem);
                    }
                    this.v = k1Var;
                    if (k1Var != null) {
                        this.u.a(b, k1Var);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        @DexIgnore
        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.s) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.t) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((g1) getAdapter()).b().a(false);
                return true;
            }
        }

        @DexIgnore
        public void setHoverListener(o2 o2Var) {
            this.u = o2Var;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ void setSelector(Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    /*
    static {
        try {
            N = PopupWindow.class.getDeclaredMethod("setTouchModal", new Class[]{Boolean.TYPE});
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    */

    @DexIgnore
    public p2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    public l2 a(Context context, boolean z) {
        a aVar = new a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    @DexIgnore
    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.I.setExitTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void d(boolean z) {
        Method method = N;
        if (method != null) {
            try {
                method.invoke(this.I, new Object[]{Boolean.valueOf(z)});
            } catch (Exception unused) {
                Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
            }
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            this.I.setEnterTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void b(h1 h1Var, MenuItem menuItem) {
        o2 o2Var = this.M;
        if (o2Var != null) {
            o2Var.b(h1Var, menuItem);
        }
    }

    @DexIgnore
    public void a(o2 o2Var) {
        this.M = o2Var;
    }

    @DexIgnore
    public void a(h1 h1Var, MenuItem menuItem) {
        o2 o2Var = this.M;
        if (o2Var != null) {
            o2Var.a(h1Var, menuItem);
        }
    }
}
