package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t7 */
public final class C2944t7 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f9557a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f9558b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f9559c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.List<java.util.List<byte[]>> f9560d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f9561e; // = 0;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.String f9562f; // = (this.f9557a + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + this.f9558b + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + this.f9559c);

    @DexIgnore
    public C2944t7(java.lang.String str, java.lang.String str2, java.lang.String str3, java.util.List<java.util.List<byte[]>> list) {
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(str);
        this.f9557a = str;
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(str2);
        this.f9558b = str2;
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(str3);
        this.f9559c = str3;
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(list);
        this.f9560d = list;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<java.util.List<byte[]>> mo16274a() {
        return this.f9560d;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo16275b() {
        return this.f9561e;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo16276c() {
        return this.f9562f;
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo16277d() {
        return this.f9557a;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.String mo16278e() {
        return this.f9558b;
    }

    @DexIgnore
    /* renamed from: f */
    public java.lang.String mo16279f() {
        return this.f9559c;
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.f9557a + ", mProviderPackage: " + this.f9558b + ", mQuery: " + this.f9559c + ", mCertificates:");
        for (int i = 0; i < this.f9560d.size(); i++) {
            sb.append(" [");
            java.util.List list = this.f9560d.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append(" \"");
                sb.append(android.util.Base64.encodeToString((byte[]) list.get(i2), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.f9561e);
        return sb.toString();
    }
}
