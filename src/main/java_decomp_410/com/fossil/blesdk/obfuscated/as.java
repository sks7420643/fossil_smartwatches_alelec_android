package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class as<Model> implements sr<Model, Model> {
    @DexIgnore
    public static /* final */ as<?> a; // = new as<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Model> implements tr<Model, Model> {
        @DexIgnore
        public static /* final */ a<?> a; // = new a<>();

        @DexIgnore
        public static <T> a<T> a() {
            return a;
        }

        @DexIgnore
        public sr<Model, Model> a(wr wrVar) {
            return as.a();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model> implements so<Model> {
        @DexIgnore
        public /* final */ Model e;

        @DexIgnore
        public b(Model model) {
            this.e = model;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super Model> aVar) {
            aVar.a(this.e);
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Model> getDataClass() {
            return this.e.getClass();
        }
    }

    @DexIgnore
    public static <T> as<T> a() {
        return a;
    }

    @DexIgnore
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    public sr.a<Model> a(Model model, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(model), new b(model));
    }
}
