package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class do0 implements DynamiteModule.a.C0044a {
    @DexIgnore
    public final int a(Context context, String str, boolean z) throws DynamiteModule.LoadingException {
        return DynamiteModule.a(context, str, z);
    }

    @DexIgnore
    public final int a(Context context, String str) {
        return DynamiteModule.a(context, str);
    }
}
