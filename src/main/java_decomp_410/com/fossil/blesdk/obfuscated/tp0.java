package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tp0 implements Parcelable.Creator<ap0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                str2 = SafeParcelReader.f(parcel, a);
            } else if (a2 == 4) {
                str3 = SafeParcelReader.f(parcel, a);
            } else if (a2 == 5) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 != 6) {
                SafeParcelReader.v(parcel, a);
            } else {
                i2 = SafeParcelReader.q(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ap0(str, str2, str3, i, i2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ap0[i];
    }
}
