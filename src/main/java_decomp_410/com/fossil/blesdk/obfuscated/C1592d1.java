package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d1 */
public class C1592d1<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ T f4235a;

    @DexIgnore
    public C1592d1(T t) {
        if (t != null) {
            this.f4235a = t;
            return;
        }
        throw new java.lang.IllegalArgumentException("Wrapped Object can not be null.");
    }
}
