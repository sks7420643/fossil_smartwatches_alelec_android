package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r1 */
public final class C2780r1 {
    @DexIgnore
    /* renamed from: a */
    public static android.view.Menu m13038a(android.content.Context context, com.fossil.blesdk.obfuscated.C1927h7 h7Var) {
        return new com.fossil.blesdk.obfuscated.C2851s1(context, h7Var);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.MenuItem m13039a(android.content.Context context, com.fossil.blesdk.obfuscated.C2001i7 i7Var) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return new com.fossil.blesdk.obfuscated.C2366m1(context, i7Var);
        }
        return new com.fossil.blesdk.obfuscated.C2274l1(context, i7Var);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.SubMenu m13040a(android.content.Context context, com.fossil.blesdk.obfuscated.C2108j7 j7Var) {
        return new com.fossil.blesdk.obfuscated.C3161w1(context, j7Var);
    }
}
