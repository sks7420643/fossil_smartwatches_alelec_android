package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ms2 extends RecyclerView.g<a> {
    @DexIgnore
    public List<AddressWrapper> a;
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ TextView b;
        @DexIgnore
        public /* final */ ImageView c;
        @DexIgnore
        public /* final */ /* synthetic */ ms2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ms2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ms2$a$a  reason: collision with other inner class name */
        public static final class C0096a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0096a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (this.e.getAdapterPosition() != -1) {
                    b b = this.e.d.b;
                    if (b != null) {
                        List a = this.e.d.a;
                        if (a != null) {
                            b.a((AddressWrapper) a.get(adapterPosition));
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ms2 ms2, View view) {
            super(view);
            kd4.b(view, "view");
            this.d = ms2;
            view.setOnClickListener(new C0096a(this));
            View findViewById = view.findViewById(R.id.ftv_title);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(R.id.ftv_content);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                    View findViewById3 = view.findViewById(R.id.iv_icon);
                    if (findViewById3 != null) {
                        this.c = (ImageView) findViewById3;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final ImageView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AddressWrapper addressWrapper);
    }

    @DexIgnore
    public int getItemCount() {
        List<AddressWrapper> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        List<AddressWrapper> list = this.a;
        if (list != null) {
            AddressWrapper addressWrapper = list.get(i);
            aVar.c().setText(addressWrapper.getName());
            aVar.a().setText(addressWrapper.getAddress());
            int i2 = ns2.a[addressWrapper.getType().ordinal()];
            if (i2 == 1) {
                aVar.b().setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.ic_address_home));
            } else if (i2 == 2) {
                aVar.b().setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.ic_address_work));
            } else if (i2 == 3) {
                aVar.b().setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.ic_address_other));
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address_commute_time, viewGroup, false);
        kd4.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        kd4.b(list, "addressList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final AddressWrapper a(int i) {
        if (i != -1) {
            List<AddressWrapper> list = this.a;
            if (list == null) {
                kd4.a();
                throw null;
            } else if (i <= list.size()) {
                List<AddressWrapper> list2 = this.a;
                if (list2 != null) {
                    return list2.get(i);
                }
                kd4.a();
                throw null;
            }
        }
        return null;
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.b = bVar;
    }
}
