package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.Protocol;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pu3 {
    @DexIgnore
    public /* final */ Proxy a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ SocketFactory d;
    @DexIgnore
    public /* final */ SSLSocketFactory e;
    @DexIgnore
    public /* final */ HostnameVerifier f;
    @DexIgnore
    public /* final */ uu3 g;
    @DexIgnore
    public /* final */ qu3 h;
    @DexIgnore
    public /* final */ List<Protocol> i;
    @DexIgnore
    public /* final */ List<yu3> j;
    @DexIgnore
    public /* final */ ProxySelector k;

    @DexIgnore
    public pu3(String str, int i2, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, uu3 uu3, qu3 qu3, Proxy proxy, List<Protocol> list, List<yu3> list2, ProxySelector proxySelector) {
        if (str == null) {
            throw new NullPointerException("uriHost == null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("uriPort <= 0: " + i2);
        } else if (qu3 == null) {
            throw new IllegalArgumentException("authenticator == null");
        } else if (list == null) {
            throw new IllegalArgumentException("protocols == null");
        } else if (proxySelector != null) {
            this.a = proxy;
            this.b = str;
            this.c = i2;
            this.d = socketFactory;
            this.e = sSLSocketFactory;
            this.f = hostnameVerifier;
            this.g = uu3;
            this.h = qu3;
            this.i = wv3.a(list);
            this.j = wv3.a(list2);
            this.k = proxySelector;
        } else {
            throw new IllegalArgumentException("proxySelector == null");
        }
    }

    @DexIgnore
    public qu3 a() {
        return this.h;
    }

    @DexIgnore
    public uu3 b() {
        return this.g;
    }

    @DexIgnore
    public List<yu3> c() {
        return this.j;
    }

    @DexIgnore
    public HostnameVerifier d() {
        return this.f;
    }

    @DexIgnore
    public List<Protocol> e() {
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof pu3)) {
            return false;
        }
        pu3 pu3 = (pu3) obj;
        if (!wv3.a((Object) this.a, (Object) pu3.a) || !this.b.equals(pu3.b) || this.c != pu3.c || !wv3.a((Object) this.e, (Object) pu3.e) || !wv3.a((Object) this.f, (Object) pu3.f) || !wv3.a((Object) this.g, (Object) pu3.g) || !wv3.a((Object) this.h, (Object) pu3.h) || !wv3.a((Object) this.i, (Object) pu3.i) || !wv3.a((Object) this.j, (Object) pu3.j) || !wv3.a((Object) this.k, (Object) pu3.k)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public Proxy f() {
        return this.a;
    }

    @DexIgnore
    public ProxySelector g() {
        return this.k;
    }

    @DexIgnore
    public SocketFactory h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        Proxy proxy = this.a;
        int i2 = 0;
        int hashCode = (((((527 + (proxy != null ? proxy.hashCode() : 0)) * 31) + this.b.hashCode()) * 31) + this.c) * 31;
        SSLSocketFactory sSLSocketFactory = this.e;
        int hashCode2 = (hashCode + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.f;
        int hashCode3 = (hashCode2 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        uu3 uu3 = this.g;
        if (uu3 != null) {
            i2 = uu3.hashCode();
        }
        return ((((((((hashCode3 + i2) * 31) + this.h.hashCode()) * 31) + this.i.hashCode()) * 31) + this.j.hashCode()) * 31) + this.k.hashCode();
    }

    @DexIgnore
    public SSLSocketFactory i() {
        return this.e;
    }

    @DexIgnore
    public String j() {
        return this.b;
    }

    @DexIgnore
    public int k() {
        return this.c;
    }
}
