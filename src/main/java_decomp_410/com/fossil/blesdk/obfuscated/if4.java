package com.fossil.blesdk.obfuscated;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import kotlin.text.MatcherMatchResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class if4 {
    @DexIgnore
    public static final hf4 b(Matcher matcher, int i, CharSequence charSequence) {
        if (!matcher.find(i)) {
            return null;
        }
        return new MatcherMatchResult(matcher, charSequence);
    }

    @DexIgnore
    public static final hf4 b(Matcher matcher, CharSequence charSequence) {
        if (!matcher.matches()) {
            return null;
        }
        return new MatcherMatchResult(matcher, charSequence);
    }

    @DexIgnore
    public static final yd4 b(MatchResult matchResult) {
        return ee4.d(matchResult.start(), matchResult.end());
    }

    @DexIgnore
    public static final yd4 b(MatchResult matchResult, int i) {
        return ee4.d(matchResult.start(i), matchResult.end(i));
    }

    @DexIgnore
    public static final int b(Iterable<? extends df4> iterable) {
        int i = 0;
        for (df4 value : iterable) {
            i |= value.getValue();
        }
        return i;
    }
}
