package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import java.io.IOException;
import java.util.Arrays;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a81 extends y71 {
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;

    @DexIgnore
    public a81(byte[] bArr, int i2, int i3, boolean z) {
        super();
        this.j = Integer.MAX_VALUE;
        this.d = bArr;
        this.e = i3 + i2;
        this.g = i2;
        this.h = this.g;
    }

    @DexIgnore
    public final byte A() throws IOException {
        int i2 = this.g;
        if (i2 != this.e) {
            byte[] bArr = this.d;
            this.g = i2 + 1;
            return bArr[i2];
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final void a(int i2) throws zzuv {
        if (this.i != i2) {
            throw zzuv.zzwt();
        }
    }

    @DexIgnore
    public final boolean b(int i2) throws IOException {
        int d2;
        int i3 = i2 & 7;
        int i4 = 0;
        if (i3 == 0) {
            if (this.e - this.g >= 10) {
                while (i4 < 10) {
                    byte[] bArr = this.d;
                    int i5 = this.g;
                    this.g = i5 + 1;
                    if (bArr[i5] < 0) {
                        i4++;
                    }
                }
                throw zzuv.zzws();
            }
            while (i4 < 10) {
                if (A() < 0) {
                    i4++;
                }
            }
            throw zzuv.zzws();
            return true;
        } else if (i3 == 1) {
            f(8);
            return true;
        } else if (i3 == 2) {
            f(v());
            return true;
        } else if (i3 == 3) {
            do {
                d2 = d();
                if (d2 == 0) {
                    break;
                }
            } while (b(d2));
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } else if (i3 == 4) {
            return false;
        } else {
            if (i3 == 5) {
                f(4);
                return true;
            }
            throw zzuv.zzwu();
        }
    }

    @DexIgnore
    public final String c() throws IOException {
        int v = v();
        if (v > 0) {
            int i2 = this.e;
            int i3 = this.g;
            if (v <= i2 - i3) {
                String str = new String(this.d, i3, v, v81.a);
                this.g += v;
                return str;
            }
        }
        if (v == 0) {
            return "";
        }
        if (v < 0) {
            throw zzuv.zzwr();
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final int d() throws IOException {
        if (s()) {
            this.i = 0;
            return 0;
        }
        this.i = v();
        int i2 = this.i;
        if ((i2 >>> 3) != 0) {
            return i2;
        }
        throw new zzuv("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public final long e() throws IOException {
        return w();
    }

    @DexIgnore
    public final long f() throws IOException {
        return w();
    }

    @DexIgnore
    public final int g() throws IOException {
        return v();
    }

    @DexIgnore
    public final long h() throws IOException {
        return y();
    }

    @DexIgnore
    public final int i() throws IOException {
        return x();
    }

    @DexIgnore
    public final boolean j() throws IOException {
        return w() != 0;
    }

    @DexIgnore
    public final String k() throws IOException {
        int v = v();
        if (v > 0) {
            int i2 = this.e;
            int i3 = this.g;
            if (v <= i2 - i3) {
                String c = mb1.c(this.d, i3, v);
                this.g += v;
                return c;
            }
        }
        if (v == 0) {
            return "";
        }
        if (v <= 0) {
            throw zzuv.zzwr();
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final zzte l() throws IOException {
        byte[] bArr;
        int v = v();
        if (v > 0) {
            int i2 = this.e;
            int i3 = this.g;
            if (v <= i2 - i3) {
                zzte zzb = zzte.zzb(this.d, i3, v);
                this.g += v;
                return zzb;
            }
        }
        if (v == 0) {
            return zzte.zzbts;
        }
        if (v > 0) {
            int i4 = this.e;
            int i5 = this.g;
            if (v <= i4 - i5) {
                this.g = v + i5;
                bArr = Arrays.copyOfRange(this.d, i5, this.g);
                return zzte.zzi(bArr);
            }
        }
        if (v > 0) {
            throw zzuv.zzwq();
        } else if (v == 0) {
            bArr = v81.b;
            return zzte.zzi(bArr);
        } else {
            throw zzuv.zzwr();
        }
    }

    @DexIgnore
    public final int m() throws IOException {
        return v();
    }

    @DexIgnore
    public final int n() throws IOException {
        return v();
    }

    @DexIgnore
    public final int o() throws IOException {
        return x();
    }

    @DexIgnore
    public final long p() throws IOException {
        return y();
    }

    @DexIgnore
    public final int q() throws IOException {
        int v = v();
        return (-(v & 1)) ^ (v >>> 1);
    }

    @DexIgnore
    public final long r() throws IOException {
        long w = w();
        return (-(w & 1)) ^ (w >>> 1);
    }

    @DexIgnore
    public final boolean s() throws IOException {
        return this.g == this.e;
    }

    @DexIgnore
    public final int t() {
        return this.g - this.h;
    }

    @DexIgnore
    public final long u() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte A = A();
            j2 |= ((long) (A & Byte.MAX_VALUE)) << i2;
            if ((A & 128) == 0) {
                return j2;
            }
        }
        throw zzuv.zzws();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0066, code lost:
        if (r2[r3] >= 0) goto L_0x0068;
     */
    @DexIgnore
    public final int v() throws IOException {
        byte b;
        int i2 = this.g;
        int i3 = this.e;
        if (i3 != i2) {
            byte[] bArr = this.d;
            int i4 = i2 + 1;
            byte b2 = bArr[i2];
            if (b2 >= 0) {
                this.g = i4;
                return b2;
            } else if (i3 - i4 >= 9) {
                int i5 = i4 + 1;
                byte b3 = b2 ^ (bArr[i4] << 7);
                if (b3 < 0) {
                    b = b3 ^ Byte.MIN_VALUE;
                } else {
                    int i6 = i5 + 1;
                    byte b4 = b3 ^ (bArr[i5] << DateTimeFieldType.HOUR_OF_HALFDAY);
                    if (b4 >= 0) {
                        b = b4 ^ 16256;
                    } else {
                        i5 = i6 + 1;
                        byte b5 = b4 ^ (bArr[i6] << DateTimeFieldType.SECOND_OF_MINUTE);
                        if (b5 < 0) {
                            b = b5 ^ -2080896;
                        } else {
                            i6 = i5 + 1;
                            byte b6 = bArr[i5];
                            b = (b5 ^ (b6 << 28)) ^ 266354560;
                            if (b6 < 0) {
                                i5 = i6 + 1;
                                if (bArr[i6] < 0) {
                                    i6 = i5 + 1;
                                    if (bArr[i5] < 0) {
                                        i5 = i6 + 1;
                                        if (bArr[i6] < 0) {
                                            i6 = i5 + 1;
                                            if (bArr[i5] < 0) {
                                                i5 = i6 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i5 = i6;
                }
                this.g = i5;
                return b;
            }
        }
        return (int) u();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b0, code lost:
        if (((long) r2[r0]) >= 0) goto L_0x00b4;
     */
    @DexIgnore
    public final long w() throws IOException {
        long j2;
        int i2;
        long j3;
        long j4;
        byte b;
        int i3 = this.g;
        int i4 = this.e;
        if (i4 != i3) {
            byte[] bArr = this.d;
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            if (b2 >= 0) {
                this.g = i5;
                return (long) b2;
            } else if (i4 - i5 >= 9) {
                int i6 = i5 + 1;
                byte b3 = b2 ^ (bArr[i5] << 7);
                if (b3 < 0) {
                    b = b3 ^ Byte.MIN_VALUE;
                } else {
                    int i7 = i6 + 1;
                    byte b4 = b3 ^ (bArr[i6] << DateTimeFieldType.HOUR_OF_HALFDAY);
                    if (b4 >= 0) {
                        i2 = i7;
                        j2 = (long) (b4 ^ 16256);
                    } else {
                        i6 = i7 + 1;
                        byte b5 = b4 ^ (bArr[i7] << DateTimeFieldType.SECOND_OF_MINUTE);
                        if (b5 < 0) {
                            b = b5 ^ -2080896;
                        } else {
                            long j5 = (long) b5;
                            int i8 = i6 + 1;
                            long j6 = j5 ^ (((long) bArr[i6]) << 28);
                            if (j6 >= 0) {
                                j4 = 266354560;
                            } else {
                                int i9 = i8 + 1;
                                long j7 = j6 ^ (((long) bArr[i8]) << 35);
                                if (j7 < 0) {
                                    j3 = -34093383808L;
                                } else {
                                    i8 = i9 + 1;
                                    j6 = j7 ^ (((long) bArr[i9]) << 42);
                                    if (j6 >= 0) {
                                        j4 = 4363953127296L;
                                    } else {
                                        i9 = i8 + 1;
                                        j7 = j6 ^ (((long) bArr[i8]) << 49);
                                        if (j7 < 0) {
                                            j3 = -558586000294016L;
                                        } else {
                                            int i10 = i9 + 1;
                                            long j8 = (j7 ^ (((long) bArr[i9]) << 56)) ^ 71499008037633920L;
                                            if (j8 < 0) {
                                                i2 = i10 + 1;
                                            } else {
                                                i2 = i10;
                                            }
                                            j2 = j8;
                                        }
                                    }
                                }
                                j2 = j7 ^ j3;
                            }
                            j2 = j6 ^ j4;
                            i2 = i8;
                        }
                    }
                    this.g = i2;
                    return j2;
                }
                j2 = (long) b;
                this.g = i2;
                return j2;
            }
        }
        return u();
    }

    @DexIgnore
    public final int x() throws IOException {
        int i2 = this.g;
        if (this.e - i2 >= 4) {
            byte[] bArr = this.d;
            this.g = i2 + 4;
            return ((bArr[i2 + 3] & FileType.MASKED_INDEX) << 24) | (bArr[i2] & FileType.MASKED_INDEX) | ((bArr[i2 + 1] & FileType.MASKED_INDEX) << 8) | ((bArr[i2 + 2] & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY);
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final long y() throws IOException {
        int i2 = this.g;
        if (this.e - i2 >= 8) {
            byte[] bArr = this.d;
            this.g = i2 + 8;
            return ((((long) bArr[i2 + 7]) & 255) << 56) | (((long) bArr[i2]) & 255) | ((((long) bArr[i2 + 1]) & 255) << 8) | ((((long) bArr[i2 + 2]) & 255) << 16) | ((((long) bArr[i2 + 3]) & 255) << 24) | ((((long) bArr[i2 + 4]) & 255) << 32) | ((((long) bArr[i2 + 5]) & 255) << 40) | ((((long) bArr[i2 + 6]) & 255) << 48);
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final void z() {
        this.e += this.f;
        int i2 = this.e;
        int i3 = i2 - this.h;
        int i4 = this.j;
        if (i3 > i4) {
            this.f = i3 - i4;
            this.e = i2 - this.f;
            return;
        }
        this.f = 0;
    }

    @DexIgnore
    public final void e(int i2) {
        this.j = i2;
        z();
    }

    @DexIgnore
    public final void f(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.e;
            int i4 = this.g;
            if (i2 <= i3 - i4) {
                this.g = i4 + i2;
                return;
            }
        }
        if (i2 < 0) {
            throw zzuv.zzwr();
        }
        throw zzuv.zzwq();
    }

    @DexIgnore
    public final double a() throws IOException {
        return Double.longBitsToDouble(y());
    }

    @DexIgnore
    public final <T extends w91> T a(ha1<T> ha1, i81 i81) throws IOException {
        int v = v();
        if (this.a < this.b) {
            int d2 = d(v);
            this.a++;
            T t = (w91) ha1.a(this, i81);
            a(0);
            this.a--;
            e(d2);
            return t;
        }
        throw zzuv.zzwv();
    }

    @DexIgnore
    public final int d(int i2) throws zzuv {
        if (i2 >= 0) {
            int t = i2 + t();
            int i3 = this.j;
            if (t <= i3) {
                this.j = t;
                z();
                return i3;
            }
            throw zzuv.zzwq();
        }
        throw zzuv.zzwr();
    }

    @DexIgnore
    public final float b() throws IOException {
        return Float.intBitsToFloat(x());
    }
}
