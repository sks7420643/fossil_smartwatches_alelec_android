package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.zj0;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hq0 extends jk0 implements me0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hq0> CREATOR; // = new iq0();
    @DexIgnore
    public /* final */ List<zo0> e;
    @DexIgnore
    public /* final */ Status f;

    @DexIgnore
    public hq0(List<zo0> list, Status status) {
        this.e = Collections.unmodifiableList(list);
        this.f = status;
    }

    @DexIgnore
    public Status G() {
        return this.f;
    }

    @DexIgnore
    public List<zo0> H() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof hq0) {
                hq0 hq0 = (hq0) obj;
                if (this.f.equals(hq0.f) && zj0.a(this.e, hq0.e)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(this.f, this.e);
    }

    @DexIgnore
    public String toString() {
        zj0.a a = zj0.a((Object) this);
        a.a("status", this.f);
        a.a("dataSources", this.e);
        return a.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.b(parcel, 1, H(), false);
        kk0.a(parcel, 2, (Parcelable) G(), i, false);
        kk0.a(parcel, a);
    }
}
