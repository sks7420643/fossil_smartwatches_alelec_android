package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b62 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public b62(en2 en2, UserRepository userRepository, HybridPresetDao hybridPresetDao, DeviceDao deviceDao, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp) {
        kd4.b(en2, "mSharedPrefs");
        kd4.b(userRepository, "mUserRepository");
        kd4.b(hybridPresetDao, "mHybridPresetDao");
        kd4.b(deviceDao, "mDeviceDao");
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(portfolioApp, "mApp");
    }
}
