package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zl3 implements MembersInjector<ProfileSetupPresenter> {
    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, SignUpEmailUseCase signUpEmailUseCase) {
        profileSetupPresenter.f = signUpEmailUseCase;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, SignUpSocialUseCase signUpSocialUseCase) {
        profileSetupPresenter.g = signUpSocialUseCase;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, nr2 nr2) {
        profileSetupPresenter.h = nr2;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter, AnalyticsHelper analyticsHelper) {
        profileSetupPresenter.i = analyticsHelper;
    }

    @DexIgnore
    public static void a(ProfileSetupPresenter profileSetupPresenter) {
        profileSetupPresenter.u();
    }
}
