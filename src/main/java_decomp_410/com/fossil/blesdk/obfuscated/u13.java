package com.fossil.blesdk.obfuscated;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.n13;
import com.fossil.blesdk.obfuscated.qm2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.tutorial.CustomizeTutorialActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u13 extends as2 implements t13, View.OnClickListener, CustomizeWidget.c, ws3.g, qm2.b {
    @DexIgnore
    public s13 k;
    @DexIgnore
    public tr3<ib2> l;
    @DexIgnore
    public /* final */ ArrayList<Fragment> m; // = new ArrayList<>();
    @DexIgnore
    public j23 n;
    @DexIgnore
    public r43 o;
    @DexIgnore
    public CustomizeThemeFragment p;
    @DexIgnore
    public int q; // = 1;
    @DexIgnore
    public Integer r;
    @DexIgnore
    public ValueAnimator s;
    @DexIgnore
    public ComplicationsPresenter t;
    @DexIgnore
    public WatchAppsPresenter u;
    @DexIgnore
    public CustomizeThemePresenter v;
    @DexIgnore
    public j42 w;
    @DexIgnore
    public DianaCustomizeViewModel x;
    @DexIgnore
    public HashMap y;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public b(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionCancel()");
            if (transition != null) {
                transition.removeListener(this);
            }
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionEnd()");
            if (transition != null) {
                transition.removeListener(this);
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null && !activity.hasWindowFocus()) {
                kd4.a((Object) activity, "it");
                int intExtra = activity.getIntent().getIntExtra("KEY_CUSTOMIZE_TAB", 1);
                String stringExtra = activity.getIntent().getStringExtra("KEY_PRESET_ID");
                String stringExtra2 = activity.getIntent().getStringExtra("KEY_PRESET_COMPLICATION_POS_SELECTED");
                String stringExtra3 = activity.getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
                activity.finishAfterTransition();
                DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
                kd4.a((Object) stringExtra, "presetId");
                kd4.a((Object) stringExtra2, "complicationPos");
                aVar.a(activity, stringExtra, intExtra, stringExtra2, stringExtra3);
            }
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionPause()");
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionResume()");
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener() - onTransitionStart()");
            ib2 ib2 = (ib2) u13.a(this.a).a();
            if (ib2 != null) {
                aq2 aq2 = aq2.a;
                Object a2 = u13.a(this.a).a();
                if (a2 != null) {
                    CardView cardView = ((ib2) a2).t;
                    kd4.a((Object) cardView, "mBinding.get()!!.cvGroup");
                    aq2.a((View) cardView);
                    ViewPropertyAnimator animate = ib2.F.animate();
                    if (animate != null) {
                        ViewPropertyAnimator duration = animate.setDuration(500);
                        if (duration != null) {
                            duration.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate2 = ib2.E.animate();
                    if (animate2 != null) {
                        ViewPropertyAnimator duration2 = animate2.setDuration(500);
                        if (duration2 != null) {
                            duration2.alpha(1.0f);
                        }
                    }
                    ViewPropertyAnimator animate3 = ib2.D.animate();
                    if (animate3 != null) {
                        ViewPropertyAnimator duration3 = animate3.setDuration(500);
                        if (duration3 != null) {
                            duration3.alpha(1.0f);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ib2 a;

        @DexIgnore
        public c(ib2 ib2) {
            this.a = ib2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            kd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    j5Var.a((int) R.id.guideline_complications_holder, ((Float) animatedValue2).floatValue() + ((float) 1));
                    j5Var.a(this.a.r);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ib2 a;

        @DexIgnore
        public d(ib2 ib2) {
            this.a = ib2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            kd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                Object animatedValue2 = valueAnimator.getAnimatedValue();
                if (animatedValue2 != null) {
                    j5Var.a((int) R.id.guideline_complications_holder, ((Float) animatedValue2).floatValue() - ((float) 1));
                    j5Var.a(this.a.r);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ib2 a;

        @DexIgnore
        public e(ib2 ib2) {
            this.a = ib2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            kd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                j5Var.a(this.a.r);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ib2 a;

        @DexIgnore
        public f(ib2 ib2) {
            this.a = ib2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            j5 j5Var = new j5();
            j5Var.c(this.a.r);
            kd4.a((Object) valueAnimator, "value");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                j5Var.a((int) R.id.guideline, ((Float) animatedValue).floatValue());
                j5Var.a(this.a.r);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public h(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public i(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(false, str, ViewHierarchy.DIMENSION_LEFT_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(false, ViewHierarchy.DIMENSION_LEFT_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, ViewHierarchy.DIMENSION_LEFT_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public j(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(false, "right", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(false, "right", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(false, str, "right");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(false, "right", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "right");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public k(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(false, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(false, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(false, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(false, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(false, "bottom");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public l(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(true, str, ViewHierarchy.DIMENSION_TOP_KEY);
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(true, ViewHierarchy.DIMENSION_TOP_KEY, str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, ViewHierarchy.DIMENSION_TOP_KEY);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public m(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(true, "middle", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(true, "middle", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(true, str, "middle");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(true, "middle", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "middle");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements n13.b {
        @DexIgnore
        public /* final */ /* synthetic */ u13 a;

        @DexIgnore
        public n(u13 u13) {
            this.a = u13;
        }

        @DexIgnore
        public boolean a(View view, String str) {
            kd4.b(view, "view");
            kd4.b(str, "id");
            return this.a.a(true, "bottom", view, str);
        }

        @DexIgnore
        public void b(String str) {
            kd4.b(str, "label");
            this.a.a(true, "bottom", str);
        }

        @DexIgnore
        public boolean c(String str) {
            kd4.b(str, "fromPos");
            this.a.c(true, str, "bottom");
            return true;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "label");
            this.a.b(true, "bottom", str);
        }

        @DexIgnore
        public void a() {
            this.a.c(true, "bottom");
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "DianaCustomizeEditFragment";
    }

    @DexIgnore
    public boolean S0() {
        s13 s13 = this.k;
        if (s13 != null) {
            s13.h();
            return false;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void T0() {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "Inside .showNoActiveDeviceFlow");
        this.n = (j23) getChildFragmentManager().a("ComplicationsFragment");
        this.o = (r43) getChildFragmentManager().a("WatchAppsFragment");
        this.p = (CustomizeThemeFragment) getChildFragmentManager().a("CustomizeThemeFragment");
        if (this.n == null) {
            this.n = new j23();
        }
        if (this.o == null) {
            this.o = new r43();
        }
        if (this.p == null) {
            this.p = new CustomizeThemeFragment();
        }
        CustomizeThemeFragment customizeThemeFragment = this.p;
        if (customizeThemeFragment != null) {
            this.m.add(customizeThemeFragment);
        }
        j23 j23 = this.n;
        if (j23 != null) {
            this.m.add(j23);
        }
        r43 r43 = this.o;
        if (r43 != null) {
            this.m.add(r43);
        }
        l42 g2 = PortfolioApp.W.c().g();
        j23 j232 = this.n;
        if (j232 != null) {
            r43 r432 = this.o;
            if (r432 != null) {
                CustomizeThemeFragment customizeThemeFragment2 = this.p;
                if (customizeThemeFragment2 != null) {
                    g2.a(new j13(j232, r432, customizeThemeFragment2)).a(this);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeContract.View");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsContract.View");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsContract.View");
    }

    @DexIgnore
    public final void U(String str) {
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(true);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.setSelectedWc(true);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(true);
                            a2.K.setSelectedWc(false);
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.M.setSelectedWc(false);
                            a2.J.setSelectedWc(false);
                            a2.L.setSelectedWc(false);
                            a2.K.setSelectedWc(true);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void U0() {
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                CustomizeWidget customizeWidget = a2.M;
                Intent putExtra = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                kd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026plicationAppPos.TOP_FACE)");
                CustomizeWidget.a(customizeWidget, "SWAP_PRESET_COMPLICATION", putExtra, new n13(new h(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget2 = a2.L;
                Intent putExtra2 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_LEFT_KEY);
                kd4.a((Object) putExtra2, "Intent().putExtra(Custom\u2026licationAppPos.LEFT_FACE)");
                CustomizeWidget.a(customizeWidget2, "SWAP_PRESET_COMPLICATION", putExtra2, new n13(new i(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget3 = a2.K;
                Intent putExtra3 = new Intent().putExtra("KEY_POSITION", "right");
                kd4.a((Object) putExtra3, "Intent().putExtra(Custom\u2026icationAppPos.RIGHT_FACE)");
                CustomizeWidget.a(customizeWidget3, "SWAP_PRESET_COMPLICATION", putExtra3, new n13(new j(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget4 = a2.J;
                Intent putExtra4 = new Intent().putExtra("KEY_POSITION", "bottom");
                kd4.a((Object) putExtra4, "Intent().putExtra(Custom\u2026cationAppPos.BOTTOM_FACE)");
                CustomizeWidget.a(customizeWidget4, "SWAP_PRESET_COMPLICATION", putExtra4, new n13(new k(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget5 = a2.I;
                Intent putExtra5 = new Intent().putExtra("KEY_POSITION", ViewHierarchy.DIMENSION_TOP_KEY);
                kd4.a((Object) putExtra5, "Intent().putExtra(Custom\u2026, WatchAppPos.TOP_BUTTON)");
                CustomizeWidget.a(customizeWidget5, "SWAP_PRESET_WATCH_APP", putExtra5, new n13(new l(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget6 = a2.H;
                Intent putExtra6 = new Intent().putExtra("KEY_POSITION", "middle");
                kd4.a((Object) putExtra6, "Intent().putExtra(Custom\u2026atchAppPos.MIDDLE_BUTTON)");
                CustomizeWidget.a(customizeWidget6, "SWAP_PRESET_WATCH_APP", putExtra6, new n13(new m(this)), (CustomizeWidget.b) null, 8, (Object) null);
                CustomizeWidget customizeWidget7 = a2.G;
                Intent putExtra7 = new Intent().putExtra("KEY_POSITION", "bottom");
                kd4.a((Object) putExtra7, "Intent().putExtra(Custom\u2026atchAppPos.BOTTOM_BUTTON)");
                CustomizeWidget.a(customizeWidget7, "SWAP_PRESET_WATCH_APP", putExtra7, new n13(new n(this)), (CustomizeWidget.b) null, 8, (Object) null);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V(String str) {
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.setSelectedWc(true);
                            a2.H.setSelectedWc(false);
                            a2.G.setSelectedWc(false);
                        }
                    } else if (str.equals("middle")) {
                        a2.I.setSelectedWc(false);
                        a2.H.setSelectedWc(true);
                        a2.G.setSelectedWc(false);
                    }
                } else if (str.equals("bottom")) {
                    a2.I.setSelectedWc(false);
                    a2.H.setSelectedWc(false);
                    a2.G.setSelectedWc(true);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
    }

    @DexIgnore
    public void b(View view) {
    }

    @DexIgnore
    public void c(View view) {
    }

    @DexIgnore
    public void d(View view) {
    }

    @DexIgnore
    public void f(int i2) {
        int i3 = i2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "initTab - initTab=" + i3 + " - mPreTab=" + this.r);
        this.q = i3;
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                j5 j5Var = new j5();
                j5Var.c(a2.r);
                if (i3 == 0) {
                    Integer num = this.r;
                    if (num != null && num.intValue() == 1) {
                        s13 s13 = this.k;
                        if (s13 != null) {
                            s13.i();
                            a2.w.setImageBitmap(al2.a((View) a2.q));
                            this.s = ValueAnimator.ofFloat(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
                            ValueAnimator valueAnimator = this.s;
                            if (valueAnimator != null) {
                                valueAnimator.addUpdateListener(new c(a2));
                            }
                            ValueAnimator valueAnimator2 = this.s;
                            if (valueAnimator2 != null) {
                                valueAnimator2.start();
                            }
                            this.r = null;
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    } else {
                        j5Var.a((int) R.id.guideline, 1.0f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout = a2.s;
                    kd4.a((Object) constraintLayout, "it.clWatchApps");
                    constraintLayout.setVisibility(4);
                    View view = a2.y;
                    kd4.a((Object) view, "it.lineCenter");
                    view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view2 = a2.x;
                    kd4.a((Object) view2, "it.lineBottom");
                    view2.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view3 = a2.z;
                    kd4.a((Object) view3, "it.lineTop");
                    view3.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (i3 == 1) {
                    Integer num2 = this.r;
                    if (num2 != null && num2.intValue() == 0) {
                        a2.w.setImageBitmap(al2.a((View) a2.q));
                        this.s = ValueAnimator.ofFloat(new float[]{2.0f, 1.0f});
                        ValueAnimator valueAnimator3 = this.s;
                        if (valueAnimator3 != null) {
                            valueAnimator3.addUpdateListener(new d(a2));
                        }
                        ValueAnimator valueAnimator4 = this.s;
                        if (valueAnimator4 != null) {
                            valueAnimator4.start();
                        }
                        this.r = null;
                    } else if (num2 != null && num2.intValue() == 2) {
                        this.s = ValueAnimator.ofFloat(new float[]{0.25f, 1.0f});
                        ValueAnimator valueAnimator5 = this.s;
                        if (valueAnimator5 != null) {
                            valueAnimator5.addUpdateListener(new e(a2));
                        }
                        ValueAnimator valueAnimator6 = this.s;
                        if (valueAnimator6 != null) {
                            valueAnimator6.start();
                        }
                        this.r = null;
                    } else {
                        j5Var.a((int) R.id.guideline, 1.0f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout2 = a2.s;
                    kd4.a((Object) constraintLayout2, "it.clWatchApps");
                    constraintLayout2.setVisibility(4);
                    View view4 = a2.y;
                    kd4.a((Object) view4, "it.lineCenter");
                    view4.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view5 = a2.x;
                    kd4.a((Object) view5, "it.lineBottom");
                    view5.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    View view6 = a2.z;
                    kd4.a((Object) view6, "it.lineTop");
                    view6.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        String a3 = dianaCustomizeViewModel.g().a();
                        if (a3 != null) {
                            kd4.a((Object) a3, "pos");
                            U(a3);
                        }
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else if (i3 == 2) {
                    Integer num3 = this.r;
                    if (num3 != null && num3.intValue() == 1) {
                        j5Var.a((int) R.id.guideline_complications_holder, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        j5Var.a(a2.r);
                        this.s = ValueAnimator.ofFloat(new float[]{1.0f, 0.25f});
                        ValueAnimator valueAnimator7 = this.s;
                        if (valueAnimator7 != null) {
                            valueAnimator7.addUpdateListener(new f(a2));
                        }
                        ValueAnimator valueAnimator8 = this.s;
                        if (valueAnimator8 != null) {
                            valueAnimator8.start();
                        }
                        this.r = null;
                    } else {
                        j5Var.a((int) R.id.guideline, 0.25f);
                        j5Var.a(a2.r);
                    }
                    ConstraintLayout constraintLayout3 = a2.s;
                    kd4.a((Object) constraintLayout3, "it.clWatchApps");
                    constraintLayout3.setVisibility(0);
                    View view7 = a2.y;
                    kd4.a((Object) view7, "it.lineCenter");
                    view7.setAlpha(1.0f);
                    View view8 = a2.x;
                    kd4.a((Object) view8, "it.lineBottom");
                    view8.setAlpha(1.0f);
                    View view9 = a2.z;
                    kd4.a((Object) view9, "it.lineTop");
                    view9.setAlpha(1.0f);
                    DianaCustomizeViewModel dianaCustomizeViewModel2 = this.x;
                    if (dianaCustomizeViewModel2 != null) {
                        String a4 = dianaCustomizeViewModel2.j().a();
                        if (a4 != null) {
                            kd4.a((Object) a4, "pos");
                            V(a4);
                        }
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                }
                p(i2);
                s13 s132 = this.k;
                if (s132 != null) {
                    s132.a(i3);
                    a2.A.i(i3);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void g(boolean z) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (z) {
                activity.setResult(-1);
            } else {
                activity.setResult(0);
            }
            activity.finishAfterTransition();
        }
    }

    @DexIgnore
    public void l() {
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
        S(a2);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            j42 j42 = this.w;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) dianaCustomizeEditActivity, (kc.b) j42).a(DianaCustomizeViewModel.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                this.x = (DianaCustomizeViewModel) a2;
                s13 s13 = this.k;
                if (s13 != null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.x;
                    if (dianaCustomizeViewModel != null) {
                        s13.a(dianaCustomizeViewModel);
                    } else {
                        kd4.d("mShareViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            switch (view.getId()) {
                case R.id.ftv_set_to_watch /*2131362293*/:
                    s13 s13 = this.k;
                    if (s13 != null) {
                        s13.j();
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.tv_cancel /*2131362840*/:
                    s13 s132 = this.k;
                    if (s132 != null) {
                        s132.h();
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_bottom /*2131363027*/:
                    s13 s133 = this.k;
                    if (s133 != null) {
                        s133.b("bottom");
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_middle /*2131363028*/:
                    s13 s134 = this.k;
                    if (s134 != null) {
                        s134.b("middle");
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wa_top /*2131363029*/:
                    s13 s135 = this.k;
                    if (s135 != null) {
                        s135.b(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_bottom /*2131363030*/:
                    s13 s136 = this.k;
                    if (s136 != null) {
                        s136.a("bottom");
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_end /*2131363034*/:
                    s13 s137 = this.k;
                    if (s137 != null) {
                        s137.a("right");
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_start /*2131363037*/:
                    s13 s138 = this.k;
                    if (s138 != null) {
                        s138.a(ViewHierarchy.DIMENSION_LEFT_KEY);
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                case R.id.wc_top /*2131363038*/:
                    s13 s139 = this.k;
                    if (s139 != null) {
                        s139.a(ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ib2 ib2 = (ib2) qa.a(layoutInflater, R.layout.fragment_diana_customize_edit, viewGroup, false, O0());
        T0();
        this.l = new tr3<>(this, ib2);
        kd4.a((Object) ib2, "binding");
        return ib2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        s13 s13 = this.k;
        if (s13 != null) {
            s13.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        s13 s13 = this.k;
        if (s13 != null) {
            s13.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, Constants.ACTIVITY);
            Window window = activity.getWindow();
            kd4.a((Object) window, "activity.window");
            window.setEnterTransition(aq2.a.a());
            Window window2 = activity.getWindow();
            kd4.a((Object) window2, "activity.window");
            window2.setSharedElementEnterTransition(aq2.a.a(PortfolioApp.W.c()));
            Intent intent = activity.getIntent();
            kd4.a((Object) intent, "activity.intent");
            activity.setEnterSharedElementCallback(new zp2(intent, PortfolioApp.W.c()));
            a(activity);
            postponeEnterTransition();
        }
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                a2.M.a(this);
                a2.J.a(this);
                a2.L.a(this);
                a2.K.a(this);
                a2.M.setOnClickListener(this);
                a2.J.setOnClickListener(this);
                a2.L.setOnClickListener(this);
                a2.K.setOnClickListener(this);
                a2.I.setOnClickListener(this);
                a2.H.setOnClickListener(this);
                a2.G.setOnClickListener(this);
                a2.B.setOnClickListener(this);
                a2.u.setOnClickListener(this);
                RecyclerViewPager recyclerViewPager = a2.A;
                kd4.a((Object) recyclerViewPager, "it.rvPreset");
                recyclerViewPager.setAdapter(new cu3(getChildFragmentManager(), this.m));
                a2.A.setItemViewCacheSize(2);
                a2.A.a((RecyclerView.p) new g());
                qm2 qm2 = new qm2();
                kd4.a((Object) a2, "it");
                qm2.a(a2.d(), this);
            }
            U0();
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        if (i2 == 1) {
            R("set_complication_view");
        } else if (i2 == 2) {
            R("set_watch_apps_view");
        }
    }

    @DexIgnore
    public void q() {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public void r(String str) {
        kd4.b(str, "buttonsPosition");
        if (this.q == 2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedWatchApp position=" + str);
            V(str);
        }
    }

    @DexIgnore
    public final void b(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragExit - position=" + str + ", label=" + str2);
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.J.e();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.e();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.e();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.K.e();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "swapControl - fromPosition=" + str + ", toPosition=" + str2);
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str2.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str2.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str2.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                s13 s13 = this.k;
                if (s13 != null) {
                    s13.d(str, str2);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                switch (str2.hashCode()) {
                    case -1383228885:
                        if (str2.equals("bottom")) {
                            a2.J.e();
                            break;
                        }
                        break;
                    case 115029:
                        if (str2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.e();
                            break;
                        }
                        break;
                    case 3317767:
                        if (str2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.e();
                            break;
                        }
                        break;
                    case 108511772:
                        if (str2.equals("right")) {
                            a2.K.e();
                            break;
                        }
                        break;
                }
                a2.M.setDragMode(false);
                a2.L.setDragMode(false);
                a2.K.setDragMode(false);
                a2.J.setDragMode(false);
                s13 s132 = this.k;
                if (s132 != null) {
                    s132.c(str, str2);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void p() {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_Text__SaveChangesToThePresetBefore));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Discard));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_UnsavedChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH");
    }

    @DexIgnore
    public static final /* synthetic */ tr3 a(u13 u13) {
        tr3<ib2> tr3 = u13.l;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(View view, Boolean bool) {
        a(view, bool.booleanValue());
    }

    @DexIgnore
    public final Transition a(FragmentActivity fragmentActivity) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "handleTransitionListener()");
        Window window = fragmentActivity.getWindow();
        kd4.a((Object) window, "it.window");
        return window.getSharedElementEnterTransition().addListener(new b(this));
    }

    @DexIgnore
    public void a(View view, boolean z) {
        FLogger.INSTANCE.getLocal().d("DianaCustomizeEditFragment", "onHorizontalFling");
    }

    @DexIgnore
    public void p(String str) {
        kd4.b(str, "complicationsPosition");
        if (this.q == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedComplication data=" + str);
            U(str);
        }
    }

    @DexIgnore
    public final boolean a(boolean z, String str, View view, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dropControl - pos=" + str + ", view=" + view.getId() + ", id=" + str2);
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return true;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                r43 r43 = this.o;
                if (r43 != null) {
                    r43.T0();
                }
                s13 s13 = this.k;
                if (s13 != null) {
                    s13.b(str2, str);
                    return true;
                }
                kd4.d("mPresenter");
                throw null;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.J.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.L.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.K.e();
                        break;
                    }
                    break;
            }
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            j23 j23 = this.n;
            if (j23 != null) {
                j23.T0();
            }
            s13 s132 = this.k;
            if (s132 != null) {
                s132.a(str2, str);
                return true;
            }
            kd4.d("mPresenter");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public final void b(WatchFaceWrapper watchFaceWrapper) {
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null && watchFaceWrapper != null) {
                Drawable topComplication = watchFaceWrapper.getTopComplication();
                if (topComplication != null) {
                    a2.M.setBackgroundDrawableCus(topComplication);
                } else {
                    a2.M.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable rightComplication = watchFaceWrapper.getRightComplication();
                if (rightComplication != null) {
                    a2.K.setBackgroundDrawableCus(rightComplication);
                } else {
                    a2.K.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable bottomComplication = watchFaceWrapper.getBottomComplication();
                if (bottomComplication != null) {
                    a2.J.setBackgroundDrawableCus(bottomComplication);
                } else {
                    a2.J.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                Drawable leftComplication = watchFaceWrapper.getLeftComplication();
                if (leftComplication != null) {
                    a2.L.setBackgroundDrawableCus(leftComplication);
                } else {
                    a2.L.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                }
                a2.M.setSelectedWc(false);
                a2.J.setSelectedWc(false);
                a2.L.setSelectedWc(false);
                a2.K.setSelectedWc(false);
                startPostponedEnterTransition();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c(boolean z, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "cancelDrag - position=" + str);
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.e();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.e();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.e();
                }
                a2.I.setDragMode(false);
                a2.H.setDragMode(false);
                a2.G.setDragMode(false);
                r43 r43 = this.o;
                if (r43 != null) {
                    r43.T0();
                    return;
                }
                return;
            }
            switch (str.hashCode()) {
                case -1383228885:
                    if (str.equals("bottom")) {
                        a2.J.e();
                        break;
                    }
                    break;
                case 115029:
                    if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                        a2.M.e();
                        break;
                    }
                    break;
                case 3317767:
                    if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                        a2.L.e();
                        break;
                    }
                    break;
                case 108511772:
                    if (str.equals("right")) {
                        a2.K.e();
                        break;
                    }
                    break;
            }
            a2.M.setDragMode(false);
            a2.L.setDragMode(false);
            a2.K.setDragMode(false);
            a2.J.setDragMode(false);
            j23 j23 = this.n;
            if (j23 != null) {
                j23.T0();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(String str, String str2, String str3, boolean z) {
        kd4.b(str, "message");
        kd4.b(str2, "id");
        kd4.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z);
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneError_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_SETTING", bundle);
        }
    }

    @DexIgnore
    public final void a(boolean z, String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "dragEnter - position=" + str + ", label=" + str2);
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                int hashCode = str.hashCode();
                if (hashCode != -1383228885) {
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.I.d();
                        }
                    } else if (str.equals("middle")) {
                        a2.H.d();
                    }
                } else if (str.equals("bottom")) {
                    a2.G.d();
                }
            } else {
                switch (str.hashCode()) {
                    case -1383228885:
                        if (str.equals("bottom")) {
                            a2.J.d();
                            return;
                        }
                        return;
                    case 115029:
                        if (str.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            a2.M.d();
                            return;
                        }
                        return;
                    case 3317767:
                        if (str.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                            a2.L.d();
                            return;
                        }
                        return;
                    case 108511772:
                        if (str.equals("right")) {
                            a2.K.d();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(WatchFaceWrapper watchFaceWrapper) {
        if (this.q == 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditFragment", "updateSelectedCustomizeTheme watchFaceWrapper=" + watchFaceWrapper);
            b(watchFaceWrapper);
        }
    }

    @DexIgnore
    public void c(String str) {
        kd4.b(str, "watchAppId");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            CustomizeTutorialActivity.a aVar = CustomizeTutorialActivity.B;
            kd4.a((Object) activity, "it");
            aVar.a(activity, str);
        }
    }

    @DexIgnore
    public void a(f13 f13) {
        kd4.b(f13, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DianaCustomizeEditFragment", "showCurrentPreset complications=" + f13.a() + " watchApps=" + f13.e());
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 != null) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(f13.a());
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(f13.e());
                TextView textView = a2.C;
                kd4.a((Object) textView, "it.tvPresetName");
                textView.setText(f13.d());
                WatchFaceWrapper f2 = f13.f();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    h13 h13 = (h13) it.next();
                    String f3 = h13.f();
                    if (f3 != null) {
                        switch (f3.hashCode()) {
                            case -1383228885:
                                if (!f3.equals("bottom")) {
                                    break;
                                } else {
                                    a2.J.b(h13.d());
                                    a2.J.setBottomContent(h13.g());
                                    a2.J.h();
                                    CustomizeWidget customizeWidget = a2.J;
                                    kd4.a((Object) customizeWidget, "it.wcBottom");
                                    a(customizeWidget, h13.d());
                                    break;
                                }
                            case 115029:
                                if (!f3.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    break;
                                } else {
                                    a2.M.b(h13.d());
                                    a2.M.setBottomContent(h13.g());
                                    a2.M.h();
                                    CustomizeWidget customizeWidget2 = a2.M;
                                    kd4.a((Object) customizeWidget2, "it.wcTop");
                                    a(customizeWidget2, h13.d());
                                    break;
                                }
                            case 3317767:
                                if (!f3.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    break;
                                } else {
                                    a2.L.b(h13.d());
                                    a2.L.setBottomContent(h13.g());
                                    a2.L.h();
                                    CustomizeWidget customizeWidget3 = a2.L;
                                    kd4.a((Object) customizeWidget3, "it.wcStart");
                                    a(customizeWidget3, h13.d());
                                    break;
                                }
                            case 108511772:
                                if (!f3.equals("right")) {
                                    break;
                                } else {
                                    a2.K.b(h13.d());
                                    a2.K.setBottomContent(h13.g());
                                    a2.K.h();
                                    CustomizeWidget customizeWidget4 = a2.K;
                                    kd4.a((Object) customizeWidget4, "it.wcEnd");
                                    a(customizeWidget4, h13.d());
                                    break;
                                }
                        }
                    }
                }
                Iterator it2 = arrayList2.iterator();
                while (it2.hasNext()) {
                    h13 h132 = (h13) it2.next();
                    String f4 = h132.f();
                    if (f4 != null) {
                        int hashCode = f4.hashCode();
                        if (hashCode != -1383228885) {
                            if (hashCode != -1074341483) {
                                if (hashCode == 115029 && f4.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    FlexibleTextView flexibleTextView = a2.F;
                                    kd4.a((Object) flexibleTextView, "it.tvWaTop");
                                    flexibleTextView.setText(h132.e());
                                    a2.I.d(h132.d());
                                    a2.I.h();
                                    CustomizeWidget customizeWidget5 = a2.I;
                                    kd4.a((Object) customizeWidget5, "it.waTop");
                                    b(customizeWidget5, h132.d());
                                }
                            } else if (f4.equals("middle")) {
                                FlexibleTextView flexibleTextView2 = a2.E;
                                kd4.a((Object) flexibleTextView2, "it.tvWaMiddle");
                                flexibleTextView2.setText(h132.e());
                                a2.H.d(h132.d());
                                a2.H.h();
                                CustomizeWidget customizeWidget6 = a2.H;
                                kd4.a((Object) customizeWidget6, "it.waMiddle");
                                b(customizeWidget6, h132.d());
                            }
                        } else if (f4.equals("bottom")) {
                            FlexibleTextView flexibleTextView3 = a2.D;
                            kd4.a((Object) flexibleTextView3, "it.tvWaBottom");
                            flexibleTextView3.setText(h132.e());
                            a2.G.d(h132.d());
                            a2.G.h();
                            CustomizeWidget customizeWidget7 = a2.G;
                            kd4.a((Object) customizeWidget7, "it.waBottom");
                            b(customizeWidget7, h132.d());
                        }
                    }
                }
                a2.M.g();
                a2.K.g();
                a2.J.g();
                a2.L.g();
                if (f2 != null) {
                    WatchFaceWrapper.MetaData topMetaData = f2.getTopMetaData();
                    if (topMetaData != null) {
                        a2.M.a(topMetaData.getSelectedForegroundColor(), (Integer) null, topMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData rightMetaData = f2.getRightMetaData();
                    if (rightMetaData != null) {
                        a2.K.a(rightMetaData.getSelectedForegroundColor(), (Integer) null, rightMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData bottomMetaData = f2.getBottomMetaData();
                    if (bottomMetaData != null) {
                        a2.J.a(bottomMetaData.getSelectedForegroundColor(), (Integer) null, bottomMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData leftMetaData = f2.getLeftMetaData();
                    if (leftMetaData != null) {
                        a2.L.a(leftMetaData.getSelectedForegroundColor(), (Integer) null, leftMetaData.getUnselectedForegroundColor(), (Integer) null);
                    }
                }
                if (f2 != null) {
                    Drawable background = f2.getBackground();
                    if (background != null) {
                        a2.v.setImageDrawable(background);
                    }
                }
                if (this.q == 0) {
                    b(f13.f());
                    return;
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData topMetaData2 = f2.getTopMetaData();
                    if (topMetaData2 != null) {
                        a2.M.a((Integer) null, topMetaData2.getSelectedBackgroundColor(), (Integer) null, topMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData rightMetaData2 = f2.getRightMetaData();
                    if (rightMetaData2 != null) {
                        a2.K.a((Integer) null, rightMetaData2.getSelectedBackgroundColor(), (Integer) null, rightMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData bottomMetaData2 = f2.getBottomMetaData();
                    if (bottomMetaData2 != null) {
                        a2.J.a((Integer) null, bottomMetaData2.getSelectedBackgroundColor(), (Integer) null, bottomMetaData2.getUnselectedBackgroundColor());
                    }
                }
                if (f2 != null) {
                    WatchFaceWrapper.MetaData leftMetaData2 = f2.getLeftMetaData();
                    if (leftMetaData2 != null) {
                        a2.L.a((Integer) null, leftMetaData2.getSelectedBackgroundColor(), (Integer) null, leftMetaData2.getUnselectedBackgroundColor());
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void f(boolean z) {
        tr3<ib2> tr3 = this.l;
        if (tr3 != null) {
            ib2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageButton imageButton = a2.u;
                kd4.a((Object) imageButton, "it.ftvSetToWatch");
                imageButton.setEnabled(true);
                ImageButton imageButton2 = a2.u;
                kd4.a((Object) imageButton2, "it.ftvSetToWatch");
                imageButton2.setClickable(true);
                a2.u.setBackgroundResource(R.drawable.preset_saved);
                return;
            }
            ImageButton imageButton3 = a2.u;
            kd4.a((Object) imageButton3, "it.ftvSetToWatch");
            imageButton3.setClickable(false);
            ImageButton imageButton4 = a2.u;
            kd4.a((Object) imageButton4, "it.ftvSetToWatch");
            imageButton4.setEnabled(false);
            a2.u.setBackgroundResource(R.drawable.preset_unsaved);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, String str) {
        if (str.hashCode() == 96634189 && str.equals("empty")) {
            customizeWidget.setRemoveMode(true);
        } else {
            customizeWidget.setRemoveMode(false);
        }
    }

    @DexIgnore
    public void a(s13 s13) {
        kd4.b(s13, "presenter");
        this.k = s13;
    }

    @DexIgnore
    public void a(String str, String str2, String str3, boolean z) {
        kd4.b(str, "message");
        kd4.b(str2, "id");
        kd4.b(str3, "pos");
        if (isActive()) {
            Bundle bundle = new Bundle();
            bundle.putString("TO_ID", str2);
            bundle.putString("TO_POS", str3);
            bundle.putBoolean("TO_COMPLICATION", z);
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
            fVar.a((int) R.id.tv_description, str);
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
            fVar.a((int) R.id.tv_ok);
            fVar.a(getChildFragmentManager(), "DIALOG_SET_TO_WATCH_FAIL_PERMISSION", bundle);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1395717072) {
            if (hashCode != -523101473) {
                if (hashCode == 291193711 && str.equals("DIALOG_SET_TO_WATCH_FAIL_SETTING") && i2 == R.id.tv_ok && intent != null) {
                    String stringExtra = intent.getStringExtra("TO_POS");
                    String stringExtra2 = intent.getStringExtra("TO_ID");
                    boolean booleanExtra = intent.getBooleanExtra("TO_COMPLICATION", true);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("DianaCustomizeEditFragment", "onUserConfirmToSetUpSetting " + stringExtra2 + " of " + stringExtra);
                    if (this.q == 1) {
                        if (booleanExtra) {
                            s13 s13 = this.k;
                            if (s13 != null) {
                                kd4.a((Object) stringExtra, "toPos");
                                s13.a(stringExtra);
                                return;
                            }
                            kd4.d("mPresenter");
                            throw null;
                        }
                    } else if (!booleanExtra) {
                        s13 s132 = this.k;
                        if (s132 != null) {
                            kd4.a((Object) stringExtra, "toPos");
                            s132.b(stringExtra);
                            return;
                        }
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("DIALOG_SET_TO_WATCH")) {
            } else {
                if (i2 == R.id.tv_cancel) {
                    g(false);
                } else if (i2 == R.id.tv_ok) {
                    s13 s133 = this.k;
                    if (s133 != null) {
                        s133.j();
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("DIALOG_SET_TO_WATCH_FAIL_PERMISSION") && i2 == R.id.tv_ok && intent != null) {
            String stringExtra3 = intent.getStringExtra("TO_ID");
            bn2 bn2 = bn2.d;
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) stringExtra3, "complicationId");
                bn2.a(context, stringExtra3);
                return;
            }
            kd4.a();
            throw null;
        }
    }
}
