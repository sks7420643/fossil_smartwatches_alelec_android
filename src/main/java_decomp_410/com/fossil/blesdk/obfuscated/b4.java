package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.f4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b4 implements d4 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements f4.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            Canvas canvas2 = canvas;
            RectF rectF2 = rectF;
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = (rectF.height() - f2) - 1.0f;
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                b4.this.a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas2.translate(rectF2.left + f3, rectF2.top + f3);
                Paint paint2 = paint;
                canvas.drawArc(b4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(b4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(height, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(b4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(b4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.restoreToCount(save);
                float f5 = rectF2.top;
                canvas.drawRect((rectF2.left + f3) - 1.0f, f5, (rectF2.right - f3) + 1.0f, f5 + f3, paint2);
                float f6 = rectF2.bottom;
                Canvas canvas3 = canvas;
                canvas3.drawRect((rectF2.left + f3) - 1.0f, f6 - f3, (rectF2.right - f3) + 1.0f, f6, paint2);
            }
            canvas.drawRect(rectF2.left, rectF2.top + f, rectF2.right, rectF2.bottom - f, paint);
        }
    }

    @DexIgnore
    public void a() {
        f4.r = new a();
    }

    @DexIgnore
    public float b(c4 c4Var) {
        return j(c4Var).c();
    }

    @DexIgnore
    public void c(c4 c4Var) {
    }

    @DexIgnore
    public void c(c4 c4Var, float f) {
        j(c4Var).b(f);
        f(c4Var);
    }

    @DexIgnore
    public float d(c4 c4Var) {
        return j(c4Var).d();
    }

    @DexIgnore
    public ColorStateList e(c4 c4Var) {
        return j(c4Var).b();
    }

    @DexIgnore
    public void f(c4 c4Var) {
        Rect rect = new Rect();
        j(c4Var).b(rect);
        c4Var.a((int) Math.ceil((double) h(c4Var)), (int) Math.ceil((double) g(c4Var)));
        c4Var.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public float g(c4 c4Var) {
        return j(c4Var).e();
    }

    @DexIgnore
    public float h(c4 c4Var) {
        return j(c4Var).f();
    }

    @DexIgnore
    public void i(c4 c4Var) {
        j(c4Var).a(c4Var.a());
        f(c4Var);
    }

    @DexIgnore
    public final f4 j(c4 c4Var) {
        return (f4) c4Var.c();
    }

    @DexIgnore
    public void a(c4 c4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        f4 a2 = a(context, colorStateList, f, f2, f3);
        a2.a(c4Var.a());
        c4Var.a(a2);
        f(c4Var);
    }

    @DexIgnore
    public void b(c4 c4Var, float f) {
        j(c4Var).c(f);
    }

    @DexIgnore
    public final f4 a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new f4(context.getResources(), colorStateList, f, f2, f3);
    }

    @DexIgnore
    public void a(c4 c4Var, ColorStateList colorStateList) {
        j(c4Var).b(colorStateList);
    }

    @DexIgnore
    public void a(c4 c4Var, float f) {
        j(c4Var).a(f);
        f(c4Var);
    }

    @DexIgnore
    public float a(c4 c4Var) {
        return j(c4Var).g();
    }
}
