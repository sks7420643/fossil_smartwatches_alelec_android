package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zn */
public final class C3446zn implements java.io.Closeable {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.io.File f11584e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.io.File f11585f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.io.File f11586g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.io.File f11587h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f11588i;

    @DexIgnore
    /* renamed from: j */
    public long f11589j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ int f11590k;

    @DexIgnore
    /* renamed from: l */
    public long f11591l; // = 0;

    @DexIgnore
    /* renamed from: m */
    public java.io.Writer f11592m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ java.util.LinkedHashMap<java.lang.String, com.fossil.blesdk.obfuscated.C3446zn.C3450d> f11593n; // = new java.util.LinkedHashMap<>(0, 0.75f, true);

    @DexIgnore
    /* renamed from: o */
    public int f11594o;

    @DexIgnore
    /* renamed from: p */
    public long f11595p; // = 0;

    @DexIgnore
    /* renamed from: q */
    public /* final */ java.util.concurrent.ThreadPoolExecutor f11596q;

    @DexIgnore
    /* renamed from: r */
    public /* final */ java.util.concurrent.Callable<java.lang.Void> f11597r;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zn$a */
    public class C3447a implements java.util.concurrent.Callable<java.lang.Void> {
        @DexIgnore
        public C3447a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
            return null;
         */
        @DexIgnore
        public java.lang.Void call() throws java.lang.Exception {
            synchronized (com.fossil.blesdk.obfuscated.C3446zn.this) {
                if (com.fossil.blesdk.obfuscated.C3446zn.this.f11592m == null) {
                    return null;
                }
                com.fossil.blesdk.obfuscated.C3446zn.this.mo18521E();
                if (com.fossil.blesdk.obfuscated.C3446zn.this.mo18517A()) {
                    com.fossil.blesdk.obfuscated.C3446zn.this.mo18520D();
                    int unused = com.fossil.blesdk.obfuscated.C3446zn.this.f11594o = 0;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zn$b")
    /* renamed from: com.fossil.blesdk.obfuscated.zn$b */
    public static final class C3448b implements java.util.concurrent.ThreadFactory {
        @DexIgnore
        public C3448b() {
        }

        @DexIgnore
        public synchronized java.lang.Thread newThread(java.lang.Runnable runnable) {
            java.lang.Thread thread;
            thread = new java.lang.Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }

        @DexIgnore
        public /* synthetic */ C3448b(com.fossil.blesdk.obfuscated.C3446zn.C3447a aVar) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zn$c")
    /* renamed from: com.fossil.blesdk.obfuscated.zn$c */
    public final class C3449c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3446zn.C3450d f11599a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean[] f11600b;

        @DexIgnore
        /* renamed from: c */
        public boolean f11601c;

        @DexIgnore
        public /* synthetic */ C3449c(com.fossil.blesdk.obfuscated.C3446zn znVar, com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar, com.fossil.blesdk.obfuscated.C3446zn.C3447a aVar) {
            this(dVar);
        }

        @DexIgnore
        /* renamed from: c */
        public void mo18536c() throws java.io.IOException {
            com.fossil.blesdk.obfuscated.C3446zn.this.mo18523a(this, true);
            this.f11601c = true;
        }

        @DexIgnore
        public C3449c(com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar) {
            this.f11599a = dVar;
            this.f11600b = dVar.f11607e ? null : new boolean[com.fossil.blesdk.obfuscated.C3446zn.this.f11590k];
        }

        @DexIgnore
        /* renamed from: a */
        public java.io.File mo18533a(int i) throws java.io.IOException {
            java.io.File b;
            synchronized (com.fossil.blesdk.obfuscated.C3446zn.this) {
                if (this.f11599a.f11608f == this) {
                    if (!this.f11599a.f11607e) {
                        this.f11600b[i] = true;
                    }
                    b = this.f11599a.mo18540b(i);
                    if (!com.fossil.blesdk.obfuscated.C3446zn.this.f11584e.exists()) {
                        com.fossil.blesdk.obfuscated.C3446zn.this.f11584e.mkdirs();
                    }
                } else {
                    throw new java.lang.IllegalStateException();
                }
            }
            return b;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo18535b() {
            if (!this.f11601c) {
                try {
                    mo18534a();
                } catch (java.io.IOException unused) {
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo18534a() throws java.io.IOException {
            com.fossil.blesdk.obfuscated.C3446zn.this.mo18523a(this, false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zn$d")
    /* renamed from: com.fossil.blesdk.obfuscated.zn$d */
    public final class C3450d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f11603a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ long[] f11604b;

        @DexIgnore
        /* renamed from: c */
        public java.io.File[] f11605c;

        @DexIgnore
        /* renamed from: d */
        public java.io.File[] f11606d;

        @DexIgnore
        /* renamed from: e */
        public boolean f11607e;

        @DexIgnore
        /* renamed from: f */
        public com.fossil.blesdk.obfuscated.C3446zn.C3449c f11608f;

        @DexIgnore
        /* renamed from: g */
        public long f11609g;

        @DexIgnore
        public /* synthetic */ C3450d(com.fossil.blesdk.obfuscated.C3446zn znVar, java.lang.String str, com.fossil.blesdk.obfuscated.C3446zn.C3447a aVar) {
            this(str);
        }

        @DexIgnore
        public C3450d(java.lang.String str) {
            this.f11603a = str;
            this.f11604b = new long[com.fossil.blesdk.obfuscated.C3446zn.this.f11590k];
            this.f11605c = new java.io.File[com.fossil.blesdk.obfuscated.C3446zn.this.f11590k];
            this.f11606d = new java.io.File[com.fossil.blesdk.obfuscated.C3446zn.this.f11590k];
            java.lang.StringBuilder sb = new java.lang.StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < com.fossil.blesdk.obfuscated.C3446zn.this.f11590k; i++) {
                sb.append(i);
                this.f11605c[i] = new java.io.File(com.fossil.blesdk.obfuscated.C3446zn.this.f11584e, sb.toString());
                sb.append(".tmp");
                this.f11606d[i] = new java.io.File(com.fossil.blesdk.obfuscated.C3446zn.this.f11584e, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo18541b(java.lang.String[] strArr) throws java.io.IOException {
            if (strArr.length == com.fossil.blesdk.obfuscated.C3446zn.this.f11590k) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.f11604b[i] = java.lang.Long.parseLong(strArr[i]);
                        i++;
                    } catch (java.lang.NumberFormatException unused) {
                        mo18538a(strArr);
                        throw null;
                    }
                }
                return;
            }
            mo18538a(strArr);
            throw null;
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.String mo18539a() throws java.io.IOException {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            for (long append : this.f11604b) {
                sb.append(' ');
                sb.append(append);
            }
            return sb.toString();
        }

        @DexIgnore
        /* renamed from: b */
        public java.io.File mo18540b(int i) {
            return this.f11606d[i];
        }

        @DexIgnore
        /* renamed from: a */
        public final java.io.IOException mo18538a(java.lang.String[] strArr) throws java.io.IOException {
            throw new java.io.IOException("unexpected journal line: " + java.util.Arrays.toString(strArr));
        }

        @DexIgnore
        /* renamed from: a */
        public java.io.File mo18537a(int i) {
            return this.f11605c[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zn$e")
    /* renamed from: com.fossil.blesdk.obfuscated.zn$e */
    public final class C3451e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.io.File[] f11611a;

        @DexIgnore
        public /* synthetic */ C3451e(com.fossil.blesdk.obfuscated.C3446zn znVar, java.lang.String str, long j, java.io.File[] fileArr, long[] jArr, com.fossil.blesdk.obfuscated.C3446zn.C3447a aVar) {
            this(znVar, str, j, fileArr, jArr);
        }

        @DexIgnore
        /* renamed from: a */
        public java.io.File mo18542a(int i) {
            return this.f11611a[i];
        }

        @DexIgnore
        public C3451e(com.fossil.blesdk.obfuscated.C3446zn znVar, java.lang.String str, long j, java.io.File[] fileArr, long[] jArr) {
            this.f11611a = fileArr;
        }
    }

    @DexIgnore
    public C3446zn(java.io.File file, int i, int i2, long j) {
        java.io.File file2 = file;
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(0, 1, 60, java.util.concurrent.TimeUnit.SECONDS, new java.util.concurrent.LinkedBlockingQueue(), new com.fossil.blesdk.obfuscated.C3446zn.C3448b((com.fossil.blesdk.obfuscated.C3446zn.C3447a) null));
        this.f11596q = threadPoolExecutor;
        this.f11597r = new com.fossil.blesdk.obfuscated.C3446zn.C3447a();
        this.f11584e = file2;
        this.f11588i = i;
        this.f11585f = new java.io.File(file2, "journal");
        this.f11586g = new java.io.File(file2, "journal.tmp");
        this.f11587h = new java.io.File(file2, "journal.bkp");
        this.f11590k = i2;
        this.f11589j = j;
    }

    @DexIgnore
    /* renamed from: A */
    public final boolean mo18517A() {
        int i = this.f11594o;
        return i >= 2000 && i >= this.f11593n.size();
    }

    @DexIgnore
    /* renamed from: B */
    public final void mo18518B() throws java.io.IOException {
        m17422a(this.f11586g);
        java.util.Iterator<com.fossil.blesdk.obfuscated.C3446zn.C3450d> it = this.f11593n.values().iterator();
        while (it.hasNext()) {
            com.fossil.blesdk.obfuscated.C3446zn.C3450d next = it.next();
            int i = 0;
            if (next.f11608f == null) {
                while (i < this.f11590k) {
                    this.f11591l += next.f11604b[i];
                    i++;
                }
            } else {
                com.fossil.blesdk.obfuscated.C3446zn.C3449c unused = next.f11608f = null;
                while (i < this.f11590k) {
                    m17422a(next.mo18537a(i));
                    m17422a(next.mo18540b(i));
                    i++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.f11594o = r0 - r9.f11593n.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006c, code lost:
        if (r1.mo8863z() != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        mo18520D();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0072, code lost:
        r9.f11592m = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(r9.f11585f, true), com.fossil.blesdk.obfuscated.C1496bo.f3793a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005f */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x008c=Splitter:B:23:0x008c, B:16:0x005f=Splitter:B:16:0x005f} */
    /* renamed from: C */
    public final void mo18519C() throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C1433ao aoVar = new com.fossil.blesdk.obfuscated.C1433ao(new java.io.FileInputStream(this.f11585f), com.fossil.blesdk.obfuscated.C1496bo.f3793a);
        try {
            java.lang.String A = aoVar.mo8860A();
            java.lang.String A2 = aoVar.mo8860A();
            java.lang.String A3 = aoVar.mo8860A();
            java.lang.String A4 = aoVar.mo8860A();
            java.lang.String A5 = aoVar.mo8860A();
            if (!"libcore.io.DiskLruCache".equals(A) || !com.facebook.appevents.AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(A2) || !java.lang.Integer.toString(this.f11588i).equals(A3) || !java.lang.Integer.toString(this.f11590k).equals(A4) || !"".equals(A5)) {
                throw new java.io.IOException("unexpected journal header: [" + A + ", " + A2 + ", " + A4 + ", " + A5 + "]");
            }
            int i = 0;
            while (true) {
                mo18527g(aoVar.mo8860A());
                i++;
            }
        } finally {
            com.fossil.blesdk.obfuscated.C1496bo.m5047a((java.io.Closeable) aoVar);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: D */
    public final synchronized void mo18520D() throws java.io.IOException {
        if (this.f11592m != null) {
            m17424a(this.f11592m);
        }
        java.io.BufferedWriter bufferedWriter = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(this.f11586g), com.fossil.blesdk.obfuscated.C1496bo.f3793a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write(com.facebook.appevents.AppEventsConstants.EVENT_PARAM_VALUE_YES);
            bufferedWriter.write("\n");
            bufferedWriter.write(java.lang.Integer.toString(this.f11588i));
            bufferedWriter.write("\n");
            bufferedWriter.write(java.lang.Integer.toString(this.f11590k));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (com.fossil.blesdk.obfuscated.C3446zn.C3450d next : this.f11593n.values()) {
                if (next.f11608f != null) {
                    bufferedWriter.write("DIRTY " + next.f11603a + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.f11603a + next.mo18539a() + 10);
                }
            }
            m17424a((java.io.Writer) bufferedWriter);
            if (this.f11585f.exists()) {
                m17423a(this.f11585f, this.f11587h, true);
            }
            m17423a(this.f11586g, this.f11585f, false);
            this.f11587h.delete();
            this.f11592m = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(this.f11585f, true), com.fossil.blesdk.obfuscated.C1496bo.f3793a));
        } catch (Throwable th) {
            m17424a((java.io.Writer) bufferedWriter);
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: E */
    public final void mo18521E() throws java.io.IOException {
        while (this.f11591l > this.f11589j) {
            mo18528h((java.lang.String) this.f11593n.entrySet().iterator().next().getKey());
        }
    }

    @DexIgnore
    public synchronized void close() throws java.io.IOException {
        if (this.f11592m != null) {
            java.util.Iterator it = new java.util.ArrayList(this.f11593n.values()).iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar = (com.fossil.blesdk.obfuscated.C3446zn.C3450d) it.next();
                if (dVar.f11608f != null) {
                    dVar.f11608f.mo18534a();
                }
            }
            mo18521E();
            m17424a(this.f11592m);
            this.f11592m = null;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo18527g(java.lang.String str) throws java.io.IOException {
        java.lang.String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                str2 = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.f11593n.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i, indexOf2);
            }
            com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar = this.f11593n.get(str2);
            if (dVar == null) {
                dVar = new com.fossil.blesdk.obfuscated.C3446zn.C3450d(this, str2, (com.fossil.blesdk.obfuscated.C3446zn.C3447a) null);
                this.f11593n.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                java.lang.String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = dVar.f11607e = true;
                com.fossil.blesdk.obfuscated.C3446zn.C3449c unused2 = dVar.f11608f = null;
                dVar.mo18541b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                com.fossil.blesdk.obfuscated.C3446zn.C3449c unused3 = dVar.f11608f = new com.fossil.blesdk.obfuscated.C3446zn.C3449c(this, dVar, (com.fossil.blesdk.obfuscated.C3446zn.C3447a) null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new java.io.IOException("unexpected journal line: " + str);
            }
        } else {
            throw new java.io.IOException("unexpected journal line: " + str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008c, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        return false;
     */
    @DexIgnore
    /* renamed from: h */
    public synchronized boolean mo18528h(java.lang.String str) throws java.io.IOException {
        mo18529y();
        com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar = this.f11593n.get(str);
        if (dVar != null) {
            if (dVar.f11608f == null) {
                for (int i = 0; i < this.f11590k; i++) {
                    java.io.File a = dVar.mo18537a(i);
                    if (a.exists()) {
                        if (!a.delete()) {
                            throw new java.io.IOException("failed to delete " + a);
                        }
                    }
                    this.f11591l -= dVar.f11604b[i];
                    dVar.f11604b[i] = 0;
                }
                this.f11594o++;
                this.f11592m.append("REMOVE");
                this.f11592m.append(' ');
                this.f11592m.append(str);
                this.f11592m.append(10);
                this.f11593n.remove(str);
                if (mo18517A()) {
                    this.f11596q.submit(this.f11597r);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: y */
    public final void mo18529y() {
        if (this.f11592m == null) {
            throw new java.lang.IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    /* renamed from: z */
    public void mo18530z() throws java.io.IOException {
        close();
        com.fossil.blesdk.obfuscated.C1496bo.m5048a(this.f11584e);
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: b */
    public static void m17426b(java.io.Writer writer) throws java.io.IOException {
        if (android.os.Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        android.os.StrictMode.ThreadPolicy threadPolicy = android.os.StrictMode.getThreadPolicy();
        android.os.StrictMode.setThreadPolicy(new android.os.StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            android.os.StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3446zn.C3449c mo18525e(java.lang.String str) throws java.io.IOException {
        return mo18522a(str, -1);
    }

    @DexIgnore
    /* renamed from: f */
    public synchronized com.fossil.blesdk.obfuscated.C3446zn.C3451e mo18526f(java.lang.String str) throws java.io.IOException {
        mo18529y();
        com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar = this.f11593n.get(str);
        if (dVar == null) {
            return null;
        }
        if (!dVar.f11607e) {
            return null;
        }
        for (java.io.File exists : dVar.f11605c) {
            if (!exists.exists()) {
                return null;
            }
        }
        this.f11594o++;
        this.f11592m.append("READ");
        this.f11592m.append(' ');
        this.f11592m.append(str);
        this.f11592m.append(10);
        if (mo18517A()) {
            this.f11596q.submit(this.f11597r);
        }
        com.fossil.blesdk.obfuscated.C3446zn.C3451e eVar = new com.fossil.blesdk.obfuscated.C3446zn.C3451e(this, str, dVar.f11609g, dVar.f11605c, dVar.f11604b, (com.fossil.blesdk.obfuscated.C3446zn.C3447a) null);
        return eVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3446zn m17419a(java.io.File file, int i, int i2, long j) throws java.io.IOException {
        if (j <= 0) {
            throw new java.lang.IllegalArgumentException("maxSize <= 0");
        } else if (i2 > 0) {
            java.io.File file2 = new java.io.File(file, "journal.bkp");
            if (file2.exists()) {
                java.io.File file3 = new java.io.File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    m17423a(file2, file3, false);
                }
            }
            com.fossil.blesdk.obfuscated.C3446zn znVar = new com.fossil.blesdk.obfuscated.C3446zn(file, i, i2, j);
            if (znVar.f11585f.exists()) {
                try {
                    znVar.mo18519C();
                    znVar.mo18518B();
                    return znVar;
                } catch (java.io.IOException e) {
                    java.io.PrintStream printStream = java.lang.System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    znVar.mo18530z();
                }
            }
            file.mkdirs();
            com.fossil.blesdk.obfuscated.C3446zn znVar2 = new com.fossil.blesdk.obfuscated.C3446zn(file, i, i2, j);
            znVar2.mo18520D();
            return znVar2;
        } else {
            throw new java.lang.IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17422a(java.io.File file) throws java.io.IOException {
        if (file.exists() && !file.delete()) {
            throw new java.io.IOException();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17423a(java.io.File file, java.io.File file2, boolean z) throws java.io.IOException {
        if (z) {
            m17422a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new java.io.IOException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        return null;
     */
    @DexIgnore
    /* renamed from: a */
    public final synchronized com.fossil.blesdk.obfuscated.C3446zn.C3449c mo18522a(java.lang.String str, long j) throws java.io.IOException {
        mo18529y();
        com.fossil.blesdk.obfuscated.C3446zn.C3450d dVar = this.f11593n.get(str);
        if (j == -1 || (dVar != null && dVar.f11609g == j)) {
            if (dVar == null) {
                dVar = new com.fossil.blesdk.obfuscated.C3446zn.C3450d(this, str, (com.fossil.blesdk.obfuscated.C3446zn.C3447a) null);
                this.f11593n.put(str, dVar);
            } else if (dVar.f11608f != null) {
                return null;
            }
            com.fossil.blesdk.obfuscated.C3446zn.C3449c cVar = new com.fossil.blesdk.obfuscated.C3446zn.C3449c(this, dVar, (com.fossil.blesdk.obfuscated.C3446zn.C3447a) null);
            com.fossil.blesdk.obfuscated.C3446zn.C3449c unused = dVar.f11608f = cVar;
            this.f11592m.append("DIRTY");
            this.f11592m.append(' ');
            this.f11592m.append(str);
            this.f11592m.append(10);
            m17426b(this.f11592m);
            return cVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0107, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: a */
    public final synchronized void mo18523a(com.fossil.blesdk.obfuscated.C3446zn.C3449c cVar, boolean z) throws java.io.IOException {
        com.fossil.blesdk.obfuscated.C3446zn.C3450d a = cVar.f11599a;
        if (a.f11608f == cVar) {
            if (z && !a.f11607e) {
                int i = 0;
                while (i < this.f11590k) {
                    if (!cVar.f11600b[i]) {
                        cVar.mo18534a();
                        throw new java.lang.IllegalStateException("Newly created entry didn't create value for index " + i);
                    } else if (!a.mo18540b(i).exists()) {
                        cVar.mo18534a();
                        return;
                    } else {
                        i++;
                    }
                }
            }
            for (int i2 = 0; i2 < this.f11590k; i2++) {
                java.io.File b = a.mo18540b(i2);
                if (!z) {
                    m17422a(b);
                } else if (b.exists()) {
                    java.io.File a2 = a.mo18537a(i2);
                    b.renameTo(a2);
                    long j = a.f11604b[i2];
                    long length = a2.length();
                    a.f11604b[i2] = length;
                    this.f11591l = (this.f11591l - j) + length;
                }
            }
            this.f11594o++;
            com.fossil.blesdk.obfuscated.C3446zn.C3449c unused = a.f11608f = null;
            if (a.f11607e || z) {
                boolean unused2 = a.f11607e = true;
                this.f11592m.append("CLEAN");
                this.f11592m.append(' ');
                this.f11592m.append(a.f11603a);
                this.f11592m.append(a.mo18539a());
                this.f11592m.append(10);
                if (z) {
                    long j2 = this.f11595p;
                    this.f11595p = 1 + j2;
                    long unused3 = a.f11609g = j2;
                }
            } else {
                this.f11593n.remove(a.f11603a);
                this.f11592m.append("REMOVE");
                this.f11592m.append(' ');
                this.f11592m.append(a.f11603a);
                this.f11592m.append(10);
            }
            m17426b(this.f11592m);
            if (this.f11591l > this.f11589j || mo18517A()) {
                this.f11596q.submit(this.f11597r);
            }
        } else {
            throw new java.lang.IllegalStateException();
        }
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: a */
    public static void m17424a(java.io.Writer writer) throws java.io.IOException {
        if (android.os.Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        android.os.StrictMode.ThreadPolicy threadPolicy = android.os.StrictMode.getThreadPolicy();
        android.os.StrictMode.setThreadPolicy(new android.os.StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            android.os.StrictMode.setThreadPolicy(threadPolicy);
        }
    }
}
