package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.migration.MigrationHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i52 implements Factory<MigrationHelper> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<en2> b;
    @DexIgnore
    public /* final */ Provider<MigrationManager> c;
    @DexIgnore
    public /* final */ Provider<b62> d;

    @DexIgnore
    public i52(n42 n42, Provider<en2> provider, Provider<MigrationManager> provider2, Provider<b62> provider3) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static i52 a(n42 n42, Provider<en2> provider, Provider<MigrationManager> provider2, Provider<b62> provider3) {
        return new i52(n42, provider, provider2, provider3);
    }

    @DexIgnore
    public static MigrationHelper b(n42 n42, Provider<en2> provider, Provider<MigrationManager> provider2, Provider<b62> provider3) {
        return a(n42, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static MigrationHelper a(n42 n42, en2 en2, MigrationManager migrationManager, b62 b62) {
        MigrationHelper a2 = n42.a(en2, migrationManager, b62);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public MigrationHelper get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
