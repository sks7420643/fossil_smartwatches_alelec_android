package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i10 extends a10 {
    @DexIgnore
    public byte[] m; // = new byte[0];

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i10(GattCharacteristic.CharacteristicId characteristicId, Peripheral.c cVar) {
        super(BluetoothCommandId.READ_CHARACTERISTIC, characteristicId, cVar);
        kd4.b(characteristicId, "characteristicId");
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        kd4.b(peripheral, "peripheral");
        peripheral.b(i());
        b(true);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        return (gattOperationResult instanceof x10) && ((x10) gattOperationResult).b() == i();
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        this.m = ((x10) gattOperationResult).c();
    }

    @DexIgnore
    public ra0<GattOperationResult> f() {
        return b().g();
    }

    @DexIgnore
    public final byte[] j() {
        return this.m;
    }
}
