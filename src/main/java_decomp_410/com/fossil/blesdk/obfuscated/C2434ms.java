package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ms */
public class C2434ms implements com.fossil.blesdk.obfuscated.C2498no<android.graphics.drawable.BitmapDrawable> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f7576a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2498no<android.graphics.Bitmap> f7577b;

    @DexIgnore
    public C2434ms(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C2498no<android.graphics.Bitmap> noVar) {
        this.f7576a = jqVar;
        this.f7577b = noVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11698a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.BitmapDrawable> aqVar, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f7577b.mo11698a(new com.fossil.blesdk.obfuscated.C2675ps(aqVar.get().getBitmap(), this.f7576a), file, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.EncodeStrategy mo13706a(com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f7577b.mo13706a(loVar);
    }
}
