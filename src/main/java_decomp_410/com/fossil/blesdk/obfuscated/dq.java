package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dq {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((aq) message.obj).a();
            return true;
        }
    }

    @DexIgnore
    public synchronized void a(aq<?> aqVar) {
        if (this.a) {
            this.b.obtainMessage(1, aqVar).sendToTarget();
        } else {
            this.a = true;
            aqVar.a();
            this.a = false;
        }
    }
}
