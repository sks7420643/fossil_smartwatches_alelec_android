package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.wr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class va3 extends zr2 implements ua3 {
    @DexIgnore
    public tr3<aa2> j;
    @DexIgnore
    public ta3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "CaloriesOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onCreateView");
        this.j = new tr3<>(this, (aa2) qa.a(layoutInflater, R.layout.fragment_calories_overview_week, viewGroup, false, O0()));
        tr3<aa2> tr3 = this.j;
        if (tr3 != null) {
            aa2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onResume");
        ta3 ta3 = this.k;
        if (ta3 != null) {
            ta3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onStop");
        ta3 ta3 = this.k;
        if (ta3 != null) {
            ta3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(wr2 wr2) {
        kd4.b(wr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekFragment", "showWeekDetails");
        tr3<aa2> tr3 = this.j;
        if (tr3 != null) {
            aa2 a2 = tr3.a();
            if (a2 != null) {
                OverviewWeekChart overviewWeekChart = a2.q;
                if (overviewWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    wr2.a aVar = wr2.a;
                    kd4.a((Object) overviewWeekChart, "it");
                    Context context = overviewWeekChart.getContext();
                    kd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewWeekChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(ta3 ta3) {
        kd4.b(ta3, "presenter");
        this.k = ta3;
    }
}
