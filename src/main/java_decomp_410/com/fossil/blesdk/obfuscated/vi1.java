package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vi1 {
    @DexIgnore
    th1 a();

    @DexIgnore
    ul1 b();

    @DexIgnore
    hm0 c();

    @DexIgnore
    tg1 d();

    @DexIgnore
    Context getContext();
}
