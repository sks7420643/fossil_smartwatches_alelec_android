package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ts */
public class C2985ts extends com.fossil.blesdk.obfuscated.C2764qs {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ byte[] f9746b; // = "com.bumptech.glide.load.resource.bitmap.CenterCrop".getBytes(com.fossil.blesdk.obfuscated.C2143jo.f6538a);

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo8933a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2) {
        return com.fossil.blesdk.obfuscated.C1904gt.m7579a(jqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        return obj instanceof com.fossil.blesdk.obfuscated.C2985ts;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterCrop".hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        messageDigest.update(f9746b);
    }
}
