package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.pj0;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eg0 extends ge0 implements ch0 {
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ pj0 d;
    @DexIgnore
    public bh0 e; // = null;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ Looper h;
    @DexIgnore
    public /* final */ Queue<te0<?, ?>> i; // = new LinkedList();
    @DexIgnore
    public volatile boolean j;
    @DexIgnore
    public long k;
    @DexIgnore
    public long l;
    @DexIgnore
    public /* final */ kg0 m;
    @DexIgnore
    public /* final */ xd0 n;
    @DexIgnore
    public zg0 o;
    @DexIgnore
    public /* final */ Map<de0.c<?>, de0.f> p;
    @DexIgnore
    public Set<Scope> q;
    @DexIgnore
    public /* final */ kj0 r;
    @DexIgnore
    public /* final */ Map<de0<?>, Boolean> s;
    @DexIgnore
    public /* final */ de0.a<? extends ln1, vm1> t;
    @DexIgnore
    public /* final */ af0 u;
    @DexIgnore
    public /* final */ ArrayList<gi0> v;
    @DexIgnore
    public Integer w;
    @DexIgnore
    public Set<mh0> x;
    @DexIgnore
    public /* final */ ph0 y;
    @DexIgnore
    public /* final */ pj0.a z;

    @DexIgnore
    public eg0(Context context, Lock lock, Looper looper, kj0 kj0, xd0 xd0, de0.a<? extends ln1, vm1> aVar, Map<de0<?>, Boolean> map, List<ge0.b> list, List<ge0.c> list2, Map<de0.c<?>, de0.f> map2, int i2, int i3, ArrayList<gi0> arrayList, boolean z2) {
        Looper looper2 = looper;
        this.k = gm0.a() ? ButtonService.CONNECT_TIMEOUT : ScanService.BLE_SCAN_TIMEOUT;
        this.l = 5000;
        this.q = new HashSet();
        this.u = new af0();
        this.w = null;
        this.x = null;
        this.z = new fg0(this);
        this.g = context;
        this.b = lock;
        this.c = false;
        this.d = new pj0(looper, this.z);
        this.h = looper2;
        this.m = new kg0(this, looper);
        this.n = xd0;
        this.f = i2;
        if (this.f >= 0) {
            this.w = Integer.valueOf(i3);
        }
        this.s = map;
        this.p = map2;
        this.v = arrayList;
        this.y = new ph0(this.p);
        for (ge0.b a : list) {
            this.d.a(a);
        }
        for (ge0.c a2 : list2) {
            this.d.a(a2);
        }
        this.r = kj0;
        this.t = aVar;
    }

    @DexIgnore
    public static String c(int i2) {
        return i2 != 1 ? i2 != 2 ? i2 != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T a(T t2) {
        bk0.a(t2.i() != null, (Object) "This task can not be enqueued (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        bk0.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                this.i.add(t2);
                return t2;
            }
            T b3 = this.e.b(t2);
            this.b.unlock();
            return b3;
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T b(T t2) {
        bk0.a(t2.i() != null, (Object) "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.p.containsKey(t2.i());
        String b2 = t2.h() != null ? t2.h().b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b2);
        sb.append(" required for this call.");
        bk0.a(containsKey, (Object) sb.toString());
        this.b.lock();
        try {
            if (this.e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.j) {
                this.i.add(t2);
                while (!this.i.isEmpty()) {
                    te0 remove = this.i.remove();
                    this.y.a(remove);
                    remove.c(Status.k);
                }
                return t2;
            } else {
                T a = this.e.a(t2);
                this.b.unlock();
                return a;
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void c() {
        this.b.lock();
        try {
            boolean z2 = false;
            if (this.f >= 0) {
                if (this.w != null) {
                    z2 = true;
                }
                bk0.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<de0.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            a(this.w.intValue());
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.b.lock();
        try {
            this.y.a();
            if (this.e != null) {
                this.e.a();
            }
            this.u.a();
            for (te0 te0 : this.i) {
                te0.a((sh0) null);
                te0.a();
            }
            this.i.clear();
            if (this.e != null) {
                o();
                this.d.a();
                this.b.unlock();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final Context e() {
        return this.g;
    }

    @DexIgnore
    public final Looper f() {
        return this.h;
    }

    @DexIgnore
    public final boolean g() {
        bh0 bh0 = this.e;
        return bh0 != null && bh0.c();
    }

    @DexIgnore
    public final void h() {
        bh0 bh0 = this.e;
        if (bh0 != null) {
            bh0.e();
        }
    }

    @DexIgnore
    public final void k() {
        d();
        c();
    }

    @DexIgnore
    public final void l() {
        this.b.lock();
        try {
            if (this.j) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void m() {
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final void n() {
        this.b.lock();
        try {
            if (o()) {
                m();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final boolean o() {
        if (!this.j) {
            return false;
        }
        this.j = false;
        this.m.removeMessages(2);
        this.m.removeMessages(1);
        zg0 zg0 = this.o;
        if (zg0 != null) {
            zg0.a();
            this.o = null;
        }
        return true;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean p() {
        this.b.lock();
        try {
            if (this.x == null) {
                this.b.unlock();
                return false;
            }
            boolean z2 = !this.x.isEmpty();
            this.b.unlock();
            return z2;
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final String q() {
        StringWriter stringWriter = new StringWriter();
        a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    @DexIgnore
    public final void a(int i2) {
        this.b.lock();
        boolean z2 = true;
        if (!(i2 == 3 || i2 == 1 || i2 == 2)) {
            z2 = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i2);
            bk0.a(z2, (Object) sb.toString());
            b(i2);
            m();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final ud0 a() {
        boolean z2 = true;
        bk0.b(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.b.lock();
        try {
            if (this.f >= 0) {
                if (this.w == null) {
                    z2 = false;
                }
                bk0.b(z2, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.w == null) {
                this.w = Integer.valueOf(a((Iterable<de0.f>) this.p.values(), false));
            } else if (this.w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            b(this.w.intValue());
            this.d.b();
            return this.e.f();
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final he0<Status> b() {
        bk0.b(g(), "GoogleApiClient is not connected yet.");
        bk0.b(this.w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        ef0 ef0 = new ef0(this);
        if (this.p.containsKey(lk0.a)) {
            a(this, ef0, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            gg0 gg0 = new gg0(this, atomicReference, ef0);
            hg0 hg0 = new hg0(this, ef0);
            ge0.a aVar = new ge0.a(this.g);
            aVar.a((de0<? extends de0.d.C0010d>) lk0.c);
            aVar.a((ge0.b) gg0);
            aVar.a((ge0.c) hg0);
            aVar.a((Handler) this.m);
            ge0 a = aVar.a();
            atomicReference.set(a);
            a.c();
        }
        return ef0;
    }

    @DexIgnore
    public final void a(ge0 ge0, ef0 ef0, boolean z2) {
        lk0.d.a(ge0).a(new jg0(this, ef0, z2, ge0));
    }

    @DexIgnore
    public final void a(ge0.c cVar) {
        this.d.a(cVar);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        while (!this.i.isEmpty()) {
            b(this.i.remove());
        }
        this.d.a(bundle);
    }

    @DexIgnore
    public final void b(int i2) {
        Integer num = this.w;
        if (num == null) {
            this.w = Integer.valueOf(i2);
        } else if (num.intValue() != i2) {
            String c2 = c(i2);
            String c3 = c(this.w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 51 + String.valueOf(c3).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c2);
            sb.append(". Mode was already set to ");
            sb.append(c3);
            throw new IllegalStateException(sb.toString());
        }
        if (this.e == null) {
            boolean z2 = false;
            boolean z3 = false;
            for (de0.f next : this.p.values()) {
                if (next.l()) {
                    z2 = true;
                }
                if (next.d()) {
                    z3 = true;
                }
            }
            int intValue = this.w.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z2) {
                        if (this.c) {
                            this.e = new ni0(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, true);
                            return;
                        }
                        this.e = ii0.a(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v);
                        return;
                    }
                }
            } else if (!z2) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z3) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.c || z3) {
                this.e = new ng0(this.g, this, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this);
                return;
            }
            this.e = new ni0(this.g, this.b, this.h, this.n, this.p, this.r, this.s, this.t, this.v, this, false);
        }
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        if (!this.n.b(this.g, ud0.H())) {
            o();
        }
        if (!this.j) {
            this.d.a(ud0);
            this.d.a();
        }
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 == 1 && !z2 && !this.j) {
            this.j = true;
            if (this.o == null && !gm0.a()) {
                this.o = this.n.a(this.g.getApplicationContext(), (ah0) new lg0(this));
            }
            kg0 kg0 = this.m;
            kg0.sendMessageDelayed(kg0.obtainMessage(1), this.k);
            kg0 kg02 = this.m;
            kg02.sendMessageDelayed(kg02.obtainMessage(2), this.l);
        }
        this.y.b();
        this.d.a(i2);
        this.d.a();
        if (i2 == 2) {
            m();
        }
    }

    @DexIgnore
    public final void b(ge0.c cVar) {
        this.d.b(cVar);
    }

    @DexIgnore
    public final boolean a(cf0 cf0) {
        bh0 bh0 = this.e;
        return bh0 != null && bh0.a(cf0);
    }

    @DexIgnore
    public final void a(mh0 mh0) {
        this.b.lock();
        try {
            if (this.x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.x.remove(mh0)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!p()) {
                this.e.d();
            }
        } finally {
            this.b.unlock();
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("mContext=").println(this.g);
        printWriter.append(str).append("mResuming=").print(this.j);
        printWriter.append(" mWorkQueue.size()=").print(this.i.size());
        printWriter.append(" mUnconsumedApiCalls.size()=").println(this.y.a.size());
        bh0 bh0 = this.e;
        if (bh0 != null) {
            bh0.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public static int a(Iterable<de0.f> iterable, boolean z2) {
        boolean z3 = false;
        boolean z4 = false;
        for (de0.f next : iterable) {
            if (next.l()) {
                z3 = true;
            }
            if (next.d()) {
                z4 = true;
            }
        }
        if (!z3) {
            return 3;
        }
        if (!z4 || !z2) {
            return 1;
        }
        return 2;
    }
}
