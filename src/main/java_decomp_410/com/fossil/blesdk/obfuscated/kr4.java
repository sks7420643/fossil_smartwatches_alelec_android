package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jl4;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kr4<T> implements Call<T> {
    @DexIgnore
    public /* final */ pr4 e;
    @DexIgnore
    public /* final */ Object[] f;
    @DexIgnore
    public /* final */ jl4.a g;
    @DexIgnore
    public /* final */ gr4<em4, T> h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public jl4 j;
    @DexIgnore
    public Throwable k;
    @DexIgnore
    public boolean l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements kl4 {
        @DexIgnore
        public /* final */ /* synthetic */ er4 a;

        @DexIgnore
        public a(er4 er4) {
            this.a = er4;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.a.onFailure(kr4.this, th);
            } catch (Throwable th2) {
                ur4.a(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        public void onFailure(jl4 jl4, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        public void onResponse(jl4 jl4, Response response) {
            try {
                try {
                    this.a.onResponse(kr4.this, kr4.this.a(response));
                } catch (Throwable th) {
                    ur4.a(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                ur4.a(th2);
                a(th2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends em4 {
        @DexIgnore
        public /* final */ em4 f;
        @DexIgnore
        public /* final */ lo4 g;
        @DexIgnore
        public IOException h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends oo4 {
            @DexIgnore
            public a(yo4 yo4) {
                super(yo4);
            }

            @DexIgnore
            public long b(jo4 jo4, long j) throws IOException {
                try {
                    return super.b(jo4, j);
                } catch (IOException e) {
                    b.this.h = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public b(em4 em4) {
            this.f = em4;
            this.g = so4.a((yo4) new a(em4.E()));
        }

        @DexIgnore
        public long C() {
            return this.f.C();
        }

        @DexIgnore
        public am4 D() {
            return this.f.D();
        }

        @DexIgnore
        public lo4 E() {
            return this.g;
        }

        @DexIgnore
        public void G() throws IOException {
            IOException iOException = this.h;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        public void close() {
            this.f.close();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends em4 {
        @DexIgnore
        public /* final */ am4 f;
        @DexIgnore
        public /* final */ long g;

        @DexIgnore
        public c(am4 am4, long j) {
            this.f = am4;
            this.g = j;
        }

        @DexIgnore
        public long C() {
            return this.g;
        }

        @DexIgnore
        public am4 D() {
            return this.f;
        }

        @DexIgnore
        public lo4 E() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public kr4(pr4 pr4, Object[] objArr, jl4.a aVar, gr4<em4, T> gr4) {
        this.e = pr4;
        this.f = objArr;
        this.g = aVar;
        this.h = gr4;
    }

    @DexIgnore
    public void a(er4<T> er4) {
        jl4 jl4;
        Throwable th;
        ur4.a(er4, "callback == null");
        synchronized (this) {
            if (!this.l) {
                this.l = true;
                jl4 = this.j;
                th = this.k;
                if (jl4 == null && th == null) {
                    try {
                        jl4 a2 = a();
                        this.j = a2;
                        jl4 = a2;
                    } catch (Throwable th2) {
                        th = th2;
                        ur4.a(th);
                        this.k = th;
                    }
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            er4.onFailure(this, th);
            return;
        }
        if (this.i) {
            jl4.cancel();
        }
        jl4.a(new a(er4));
    }

    @DexIgnore
    public void cancel() {
        jl4 jl4;
        this.i = true;
        synchronized (this) {
            jl4 = this.j;
        }
        if (jl4 != null) {
            jl4.cancel();
        }
    }

    @DexIgnore
    public synchronized dm4 n() {
        jl4 jl4 = this.j;
        if (jl4 != null) {
            return jl4.n();
        } else if (this.k == null) {
            try {
                jl4 a2 = a();
                this.j = a2;
                return a2.n();
            } catch (RuntimeException e2) {
                e = e2;
                ur4.a(e);
                this.k = e;
                throw e;
            } catch (Error e3) {
                e = e3;
                ur4.a(e);
                this.k = e;
                throw e;
            } catch (IOException e4) {
                this.k = e4;
                throw new RuntimeException("Unable to create request.", e4);
            }
        } else if (this.k instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.k);
        } else if (this.k instanceof RuntimeException) {
            throw ((RuntimeException) this.k);
        } else {
            throw ((Error) this.k);
        }
    }

    @DexIgnore
    public boolean o() {
        boolean z = true;
        if (this.i) {
            return true;
        }
        synchronized (this) {
            if (this.j == null || !this.j.o()) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public qr4<T> r() throws IOException {
        jl4 jl4;
        synchronized (this) {
            if (!this.l) {
                this.l = true;
                if (this.k == null) {
                    jl4 = this.j;
                    if (jl4 == null) {
                        try {
                            jl4 = a();
                            this.j = jl4;
                        } catch (IOException | Error | RuntimeException e2) {
                            ur4.a(e2);
                            this.k = e2;
                            throw e2;
                        }
                    }
                } else if (this.k instanceof IOException) {
                    throw ((IOException) this.k);
                } else if (this.k instanceof RuntimeException) {
                    throw ((RuntimeException) this.k);
                } else {
                    throw ((Error) this.k);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.i) {
            jl4.cancel();
        }
        return a(jl4.r());
    }

    @DexIgnore
    public kr4<T> clone() {
        return new kr4<>(this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    public final jl4 a() throws IOException {
        jl4 a2 = this.g.a(this.e.a(this.f));
        if (a2 != null) {
            return a2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    public qr4<T> a(Response response) throws IOException {
        em4 y = response.y();
        Response.a H = response.H();
        H.a((em4) new c(y.D(), y.C()));
        Response a2 = H.a();
        int B = a2.B();
        if (B < 200 || B >= 300) {
            try {
                return qr4.a(ur4.a(y), a2);
            } finally {
                y.close();
            }
        } else if (B == 204 || B == 205) {
            y.close();
            return qr4.a(null, a2);
        } else {
            b bVar = new b(y);
            try {
                return qr4.a(this.h.a(bVar), a2);
            } catch (RuntimeException e2) {
                bVar.G();
                throw e2;
            }
        }
    }
}
