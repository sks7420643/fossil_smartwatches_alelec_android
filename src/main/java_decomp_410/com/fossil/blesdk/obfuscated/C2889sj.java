package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sj */
public class C2889sj {

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3361yf f9376a; // = new com.fossil.blesdk.obfuscated.C2889sj.C2890a(1, 2);

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C3361yf f9377b; // = new com.fossil.blesdk.obfuscated.C2889sj.C2891b(3, 4);

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C3361yf f9378c; // = new com.fossil.blesdk.obfuscated.C2889sj.C2892c(4, 5);

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C3361yf f9379d; // = new com.fossil.blesdk.obfuscated.C2889sj.C2893d(6, 7);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.sj$a */
    public static class C2890a extends com.fossil.blesdk.obfuscated.C3361yf {
        @DexIgnore
        public C2890a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            ggVar.mo11230b("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            ggVar.mo11230b("DROP TABLE IF EXISTS alarmInfo");
            ggVar.mo11230b("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sj$b")
    /* renamed from: com.fossil.blesdk.obfuscated.sj$b */
    public static class C2891b extends com.fossil.blesdk.obfuscated.C3361yf {
        @DexIgnore
        public C2891b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                ggVar.mo11230b("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sj$c")
    /* renamed from: com.fossil.blesdk.obfuscated.sj$c */
    public static class C2892c extends com.fossil.blesdk.obfuscated.C3361yf {
        @DexIgnore
        public C2892c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            ggVar.mo11230b("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            ggVar.mo11230b("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sj$d")
    /* renamed from: com.fossil.blesdk.obfuscated.sj$d */
    public static class C2893d extends com.fossil.blesdk.obfuscated.C3361yf {
        @DexIgnore
        public C2893d(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            ggVar.mo11230b("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sj$e")
    /* renamed from: com.fossil.blesdk.obfuscated.sj$e */
    public static class C2894e extends com.fossil.blesdk.obfuscated.C3361yf {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f9380a;

        @DexIgnore
        public C2894e(android.content.Context context, int i, int i2) {
            super(i, i2);
            this.f9380a = context;
        }

        @DexIgnore
        public void migrate(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            new com.fossil.blesdk.obfuscated.C2896sl(this.f9380a).mo16116a(true);
        }
    }
}
