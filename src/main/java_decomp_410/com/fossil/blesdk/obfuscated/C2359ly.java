package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ly */
public class C2359ly {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f7359a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f7360b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f7361c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.String f7362d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f7363e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.String f7364f;

    @DexIgnore
    public C2359ly(java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, java.lang.String str5, java.lang.String str6) {
        this.f7359a = str;
        this.f7360b = str2;
        this.f7361c = str3;
        this.f7362d = str4;
        this.f7363e = str5;
        this.f7364f = str6;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2359ly m10475a(android.content.Context context, p011io.fabric.sdk.android.services.common.IdManager idManager, java.lang.String str, java.lang.String str2) throws android.content.pm.PackageManager.NameNotFoundException {
        java.lang.String packageName = context.getPackageName();
        java.lang.String g = idManager.mo44551g();
        android.content.pm.PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        java.lang.String num = java.lang.Integer.toString(packageInfo.versionCode);
        java.lang.String str3 = packageInfo.versionName;
        if (str3 == null) {
            str3 = "0.0";
        }
        com.fossil.blesdk.obfuscated.C2359ly lyVar = new com.fossil.blesdk.obfuscated.C2359ly(str, str2, g, packageName, num, str3);
        return lyVar;
    }
}
