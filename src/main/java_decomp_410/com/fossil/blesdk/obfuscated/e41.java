package com.fossil.blesdk.obfuscated;

import android.location.Location;
import com.fossil.blesdk.obfuscated.ze0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e41 implements ze0.b<rc1> {
    @DexIgnore
    public /* final */ /* synthetic */ Location a;

    @DexIgnore
    public e41(d41 d41, Location location) {
        this.a = location;
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj) {
        ((rc1) obj).onLocationChanged(this.a);
    }
}
