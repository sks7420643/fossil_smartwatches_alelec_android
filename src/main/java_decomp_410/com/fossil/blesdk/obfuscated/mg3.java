package com.fossil.blesdk.obfuscated;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mg3 {
    @DexIgnore
    public /* final */ Date a;
    @DexIgnore
    public /* final */ long b;

    @DexIgnore
    public mg3(Date date, long j) {
        kd4.b(date, "date");
        this.a = date;
        this.b = j;
    }

    @DexIgnore
    public final Date a() {
        return this.a;
    }

    @DexIgnore
    public final long b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof mg3) {
                mg3 mg3 = (mg3) obj;
                if (kd4.a((Object) this.a, (Object) mg3.a)) {
                    if (this.b == mg3.b) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.a;
        int hashCode = date != null ? date.hashCode() : 0;
        long j = this.b;
        return (hashCode * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "ActivityDailyBest(date=" + this.a + ", value=" + this.b + ")";
    }
}
