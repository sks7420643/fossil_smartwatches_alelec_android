package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.aa */
public final class C1408aa {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f3428a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f3429b;

    @DexIgnore
    /* renamed from: c */
    public static java.lang.reflect.Field f3430c;

    @DexIgnore
    /* renamed from: d */
    public static boolean f3431d;

    @DexIgnore
    /* renamed from: a */
    public static void m4345a(android.widget.PopupWindow popupWindow, android.view.View view, int i, int i2, int i3) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            popupWindow.showAsDropDown(view, i, i2, i3);
            return;
        }
        if ((com.fossil.blesdk.obfuscated.C2634p8.m12135a(i3, com.fossil.blesdk.obfuscated.C1776f9.m6845k(view)) & 7) == 5) {
            i -= popupWindow.getWidth() - view.getWidth();
        }
        popupWindow.showAsDropDown(view, i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4346a(android.widget.PopupWindow popupWindow, boolean z) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 23) {
            popupWindow.setOverlapAnchor(z);
        } else if (i >= 21) {
            if (!f3431d) {
                try {
                    f3430c = android.widget.PopupWindow.class.getDeclaredField("mOverlapAnchor");
                    f3430c.setAccessible(true);
                } catch (java.lang.NoSuchFieldException e) {
                    android.util.Log.i("PopupWindowCompatApi21", "Could not fetch mOverlapAnchor field from PopupWindow", e);
                }
                f3431d = true;
            }
            java.lang.reflect.Field field = f3430c;
            if (field != null) {
                try {
                    field.set(popupWindow, java.lang.Boolean.valueOf(z));
                } catch (java.lang.IllegalAccessException e2) {
                    android.util.Log.i("PopupWindowCompatApi21", "Could not set overlap anchor field in PopupWindow", e2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4344a(android.widget.PopupWindow popupWindow, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            popupWindow.setWindowLayoutType(i);
            return;
        }
        if (!f3429b) {
            try {
                f3428a = android.widget.PopupWindow.class.getDeclaredMethod("setWindowLayoutType", new java.lang.Class[]{java.lang.Integer.TYPE});
                f3428a.setAccessible(true);
            } catch (java.lang.Exception unused) {
            }
            f3429b = true;
        }
        java.lang.reflect.Method method = f3428a;
        if (method != null) {
            try {
                method.invoke(popupWindow, new java.lang.Object[]{java.lang.Integer.valueOf(i)});
            } catch (java.lang.Exception unused2) {
            }
        }
    }
}
