package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u01 extends nz0 implements t01 {
    @DexIgnore
    public u01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
    }

    @DexIgnore
    public final void a(bq0 bq0) throws RemoteException {
        Parcel o = o();
        y01.a(o, (Parcelable) bq0);
        a(22, o);
    }
}
