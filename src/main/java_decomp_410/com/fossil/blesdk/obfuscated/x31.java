package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class x31 extends g41 implements w31 {
    @DexIgnore
    public x31() {
        super("com.google.android.gms.location.internal.ISettingsCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((vc1) q41.a(parcel, vc1.CREATOR));
        return true;
    }
}
