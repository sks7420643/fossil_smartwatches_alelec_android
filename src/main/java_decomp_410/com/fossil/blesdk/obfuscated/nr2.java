package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nr2 extends CoroutineUseCase<CoroutineUseCase.b, a, CoroutineUseCase.a> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public a(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore
    public nr2(UserRepository userRepository) {
        kd4.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public Object a(CoroutineUseCase.b bVar, yb4<Object> yb4) {
        return new a(this.d.getCurrentUser());
    }

    @DexIgnore
    public String c() {
        return "GetUser";
    }
}
