package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class je2 extends ie2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public long G;

    /*
    static {
        I.put(R.id.iv_close, 1);
        I.put(R.id.ftv_title, 2);
        I.put(R.id.sv_container, 3);
        I.put(R.id.ftv_inactivity_nudge_label, 4);
        I.put(R.id.sw_inactivity_nudge, 5);
        I.put(R.id.cl_inactivity_nudge_container, 6);
        I.put(R.id.ftv_inactivity_nudge_time_start_label, 7);
        I.put(R.id.ll_inactivity_nudge_time_start, 8);
        I.put(R.id.ftv_inactivity_nudge_time_start_value, 9);
        I.put(R.id.ftv_inactivity_nudge_time_end_label, 10);
        I.put(R.id.ll_inactivity_nudge_time_end, 11);
        I.put(R.id.ftv_inactivity_nudge_time_end_value, 12);
        I.put(R.id.ftv_remind_if_inactive_label, 13);
        I.put(R.id.ll_remind_if_inactive_time, 14);
        I.put(R.id.ftv_remind_if_inactive_time_value, 15);
        I.put(R.id.ftv_receive_a_reminder_label, 16);
        I.put(R.id.cl_reminders_container, 17);
        I.put(R.id.ftv_keep_going_label, 18);
        I.put(R.id.sw_keep_going, 19);
        I.put(R.id.cl_keep_going_container, 20);
        I.put(R.id.ftv_hybrid_watch_label, 21);
        I.put(R.id.ftv_sleep_summary_label, 22);
        I.put(R.id.sw_sleep_summary, 23);
        I.put(R.id.cl_sleep_summary_container, 24);
        I.put(R.id.ftv_receive_summary_label, 25);
        I.put(R.id.ftv_bedtime_reminder_label, 26);
        I.put(R.id.sw_bedtime_reminder, 27);
        I.put(R.id.cl_bedtime_reminder_container, 28);
        I.put(R.id.ftv_set_up_a_time_label, 29);
        I.put(R.id.fb_save, 30);
    }
    */

    @DexIgnore
    public je2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 31, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public je2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[28], objArr[6], objArr[20], objArr[17], objArr[24], objArr[30], objArr[26], objArr[21], objArr[4], objArr[10], objArr[12], objArr[7], objArr[9], objArr[18], objArr[16], objArr[25], objArr[13], objArr[15], objArr[29], objArr[22], objArr[2], objArr[1], objArr[11], objArr[8], objArr[14], objArr[3], objArr[27], objArr[5], objArr[19], objArr[23]);
        this.G = -1;
        this.F = objArr[0];
        this.F.setTag((Object) null);
        a(view);
        f();
    }
}
