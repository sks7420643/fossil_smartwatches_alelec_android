package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hj4 {
    @DexIgnore
    public static final Object a(yb4<? super qa4> yb4) {
        Object obj;
        CoroutineContext context = yb4.getContext();
        a(context);
        yb4<? super qa4> a = IntrinsicsKt__IntrinsicsJvmKt.a(yb4);
        if (!(a instanceof jh4)) {
            a = null;
        }
        jh4 jh4 = (jh4) a;
        if (jh4 == null) {
            obj = qa4.a;
        } else if (!jh4.k.b(context)) {
            obj = lh4.a((jh4<? super qa4>) jh4) ? cc4.a() : qa4.a;
        } else {
            jh4.d(qa4.a);
            obj = cc4.a();
        }
        if (obj == cc4.a()) {
            ic4.c(yb4);
        }
        return obj;
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "$this$checkCompletion");
        fi4 fi4 = (fi4) coroutineContext.get(fi4.d);
        if (fi4 != null && !fi4.isActive()) {
            throw fi4.y();
        }
    }
}
