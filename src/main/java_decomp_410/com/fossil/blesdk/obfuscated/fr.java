package com.fossil.blesdk.obfuscated;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fr<Data> implements sr<Uri, Data> {
    @DexIgnore
    public static /* final */ int c; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;
    @DexIgnore
    public /* final */ a<Data> b;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        so<Data> a(AssetManager assetManager, String str);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements tr<Uri, ParcelFileDescriptor>, a<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public b(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public sr<Uri, ParcelFileDescriptor> a(wr wrVar) {
            return new fr(this.a, this);
        }

        @DexIgnore
        public so<ParcelFileDescriptor> a(AssetManager assetManager, String str) {
            return new wo(assetManager, str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements tr<Uri, InputStream>, a<InputStream> {
        @DexIgnore
        public /* final */ AssetManager a;

        @DexIgnore
        public c(AssetManager assetManager) {
            this.a = assetManager;
        }

        @DexIgnore
        public sr<Uri, InputStream> a(wr wrVar) {
            return new fr(this.a, this);
        }

        @DexIgnore
        public so<InputStream> a(AssetManager assetManager, String str) {
            return new bp(assetManager, str);
        }
    }

    @DexIgnore
    public fr(AssetManager assetManager, a<Data> aVar) {
        this.a = assetManager;
        this.b = aVar;
    }

    @DexIgnore
    public sr.a<Data> a(Uri uri, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(uri), this.b.a(this.a, uri.toString().substring(c)));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
