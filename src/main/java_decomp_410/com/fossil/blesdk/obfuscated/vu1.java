package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru1;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vu1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> {
        @DexIgnore
        public /* final */ Field a;

        @DexIgnore
        public void a(T t, Object obj) {
            try {
                this.a.set(t, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        @DexIgnore
        public b(Field field) {
            this.a = field;
            field.setAccessible(true);
        }

        @DexIgnore
        public void a(T t, int i) {
            try {
                this.a.set(t, Integer.valueOf(i));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public static int a(ObjectInputStream objectInputStream) throws IOException {
        return objectInputStream.readInt();
    }

    @DexIgnore
    public static <E> void a(ru1<E> ru1, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(ru1.entrySet().size());
        for (ru1.a next : ru1.entrySet()) {
            objectOutputStream.writeObject(next.getElement());
            objectOutputStream.writeInt(next.getCount());
        }
    }

    @DexIgnore
    public static <E> void a(ru1<E> ru1, ObjectInputStream objectInputStream, int i) throws IOException, ClassNotFoundException {
        for (int i2 = 0; i2 < i; i2++) {
            ru1.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }

    @DexIgnore
    public static <K, V> void a(pu1<K, V> pu1, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(pu1.asMap().size());
        for (Map.Entry next : pu1.asMap().entrySet()) {
            objectOutputStream.writeObject(next.getKey());
            objectOutputStream.writeInt(((Collection) next.getValue()).size());
            for (Object writeObject : (Collection) next.getValue()) {
                objectOutputStream.writeObject(writeObject);
            }
        }
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls, String str) {
        try {
            return new b<>(cls.getDeclaredField(str));
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }
}
