package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jm */
public class C2141jm<TResult> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2037im<TResult> f6536a; // = new com.fossil.blesdk.obfuscated.C2037im<>();

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2037im<TResult> mo12392a() {
        return this.f6536a;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo12397b(TResult tresult) {
        return this.f6536a.mo12038a(tresult);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo12398c() {
        return this.f6536a.mo12047g();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12394a(TResult tresult) {
        if (!mo12397b(tresult)) {
            throw new java.lang.IllegalStateException("Cannot set the result of a completed task.");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo12396b(java.lang.Exception exc) {
        return this.f6536a.mo12037a(exc);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12395b() {
        if (!mo12398c()) {
            throw new java.lang.IllegalStateException("Cannot cancel a completed task.");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12393a(java.lang.Exception exc) {
        if (!mo12396b(exc)) {
            throw new java.lang.IllegalStateException("Cannot set the error on a completed task.");
        }
    }
}
