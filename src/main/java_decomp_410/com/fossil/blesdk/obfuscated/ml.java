package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ml implements ll {
    @DexIgnore
    public /* final */ RoomDatabase a;
    @DexIgnore
    public /* final */ lf b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lf<kl> {
        @DexIgnore
        public a(ml mlVar, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(kg kgVar, kl klVar) {
            String str = klVar.a;
            if (str == null) {
                kgVar.a(1);
            } else {
                kgVar.a(1, str);
            }
            String str2 = klVar.b;
            if (str2 == null) {
                kgVar.a(2);
            } else {
                kgVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag`(`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public ml(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        this.b = new a(this, roomDatabase);
    }

    @DexIgnore
    public void a(kl klVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(klVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public List<String> a(String str) {
        uf b2 = uf.b("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bg.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
