package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzxp;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qb1 extends ob1 {
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0061, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b9, code lost:
        return -1;
     */
    @DexIgnore
    public final int a(int i, byte[] bArr, int i2, int i3) {
        int i4;
        long j;
        long j2;
        byte[] bArr2 = bArr;
        int i5 = i2;
        int i6 = i3;
        if ((i5 | i6 | (bArr2.length - i6)) >= 0) {
            long j3 = (long) i5;
            int i7 = (int) (((long) i6) - j3);
            if (i7 >= 16) {
                long j4 = j3;
                i4 = 0;
                while (true) {
                    if (i4 >= i7) {
                        i4 = i7;
                        break;
                    }
                    long j5 = j4 + 1;
                    if (kb1.a(bArr2, j4) < 0) {
                        break;
                    }
                    i4++;
                    j4 = j5;
                }
            } else {
                i4 = 0;
            }
            int i8 = i7 - i4;
            long j6 = j3 + ((long) i4);
            while (true) {
                byte b = 0;
                while (true) {
                    if (i8 <= 0) {
                        j = j6;
                        break;
                    }
                    j = j6 + 1;
                    b = kb1.a(bArr2, j6);
                    if (b < 0) {
                        break;
                    }
                    i8--;
                    j6 = j;
                }
                if (i8 == 0) {
                    return 0;
                }
                int i9 = i8 - 1;
                if (b >= -32) {
                    if (b >= -16) {
                        if (i9 >= 3) {
                            i8 = i9 - 3;
                            long j7 = j + 1;
                            byte a = kb1.a(bArr2, j);
                            if (a > -65 || (((b << 28) + (a + 112)) >> 30) != 0) {
                                break;
                            }
                            long j8 = j7 + 1;
                            if (kb1.a(bArr2, j7) > -65) {
                                break;
                            }
                            j2 = j8 + 1;
                            if (kb1.a(bArr2, j8) > -65) {
                                break;
                            }
                        } else {
                            return a(bArr2, (int) b, j, i9);
                        }
                    } else if (i9 >= 2) {
                        i8 = i9 - 2;
                        long j9 = j + 1;
                        byte a2 = kb1.a(bArr2, j);
                        if (a2 > -65 || ((b == -32 && a2 < -96) || (b == -19 && a2 >= -96))) {
                            break;
                        }
                        long j10 = j9 + 1;
                        if (kb1.a(bArr2, j9) > -65) {
                            break;
                        }
                        j6 = j10;
                    } else {
                        return a(bArr2, (int) b, j, i9);
                    }
                } else if (i9 != 0) {
                    i8 = i9 - 1;
                    if (b < -62) {
                        break;
                    }
                    j2 = j + 1;
                    if (kb1.a(bArr2, j) > -65) {
                        break;
                    }
                } else {
                    return b;
                }
                j6 = j2;
            }
            return -1;
        }
        throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr2.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
    }

    @DexIgnore
    public final String b(byte[] bArr, int i, int i2) throws zzuv {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (r13 < i3) {
                byte a = kb1.a(bArr, (long) r13);
                if (!nb1.a(a)) {
                    break;
                }
                i = r13 + 1;
                nb1.a(a, cArr, i4);
                i4++;
            }
            int i5 = i4;
            while (r13 < i3) {
                int i6 = r13 + 1;
                byte a2 = kb1.a(bArr, (long) r13);
                if (nb1.a(a2)) {
                    int i7 = i5 + 1;
                    nb1.a(a2, cArr, i5);
                    while (i6 < i3) {
                        byte a3 = kb1.a(bArr, (long) i6);
                        if (!nb1.a(a3)) {
                            break;
                        }
                        i6++;
                        nb1.a(a3, cArr, i7);
                        i7++;
                    }
                    r13 = i6;
                    i5 = i7;
                } else if (nb1.b(a2)) {
                    if (i6 < i3) {
                        int i8 = i6 + 1;
                        nb1.a(a2, kb1.a(bArr, (long) i6), cArr, i5);
                        r13 = i8;
                        i5++;
                    } else {
                        throw zzuv.zzwx();
                    }
                } else if (nb1.c(a2)) {
                    if (i6 < i3 - 1) {
                        int i9 = i6 + 1;
                        int i10 = i9 + 1;
                        nb1.a(a2, kb1.a(bArr, (long) i6), kb1.a(bArr, (long) i9), cArr, i5);
                        r13 = i10;
                        i5++;
                    } else {
                        throw zzuv.zzwx();
                    }
                } else if (i6 < i3 - 2) {
                    int i11 = i6 + 1;
                    byte a4 = kb1.a(bArr, (long) i6);
                    int i12 = i11 + 1;
                    nb1.a(a2, a4, kb1.a(bArr, (long) i11), kb1.a(bArr, (long) i12), cArr, i5);
                    r13 = i12 + 1;
                    i5 = i5 + 1 + 1;
                } else {
                    throw zzuv.zzwx();
                }
            }
            return new String(cArr, 0, i5);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)}));
    }

    @DexIgnore
    public final int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        char c;
        long j;
        long j2;
        long j3;
        CharSequence charSequence2 = charSequence;
        byte[] bArr2 = bArr;
        int i3 = i;
        int i4 = i2;
        long j4 = (long) i3;
        long j5 = ((long) i4) + j4;
        int length = charSequence.length();
        if (length > i4 || bArr2.length - i4 < i3) {
            char charAt = charSequence2.charAt(length - 1);
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt);
            sb.append(" at index ");
            sb.append(i3 + i4);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i5 = 0;
        while (true) {
            c = 128;
            j = 1;
            if (i5 >= length) {
                break;
            }
            char charAt2 = charSequence2.charAt(i5);
            if (charAt2 >= 128) {
                break;
            }
            kb1.a(bArr2, j4, (byte) charAt2);
            i5++;
            j4 = 1 + j4;
        }
        if (i5 == length) {
            return (int) j4;
        }
        while (i5 < length) {
            char charAt3 = charSequence2.charAt(i5);
            if (charAt3 < c && j4 < j5) {
                long j6 = j4 + j;
                kb1.a(bArr2, j4, (byte) charAt3);
                j3 = j;
                j2 = j6;
            } else if (charAt3 < 2048 && j4 <= j5 - 2) {
                long j7 = j4 + j;
                kb1.a(bArr2, j4, (byte) ((charAt3 >>> 6) | 960));
                kb1.a(bArr2, j7, (byte) ((charAt3 & '?') | 128));
                j2 = j7 + j;
                j3 = j;
                i5++;
                c = 128;
                long j8 = j3;
                j4 = j2;
                j = j8;
            } else if ((charAt3 < 55296 || 57343 < charAt3) && j4 <= j5 - 3) {
                long j9 = j4 + j;
                kb1.a(bArr2, j4, (byte) ((charAt3 >>> 12) | 480));
                long j10 = j9 + j;
                kb1.a(bArr2, j9, (byte) (((charAt3 >>> 6) & 63) | 128));
                kb1.a(bArr2, j10, (byte) ((charAt3 & '?') | 128));
                j2 = j10 + 1;
                j3 = 1;
            } else if (j4 <= j5 - 4) {
                int i6 = i5 + 1;
                if (i6 != length) {
                    char charAt4 = charSequence2.charAt(i6);
                    if (Character.isSurrogatePair(charAt3, charAt4)) {
                        int codePoint = Character.toCodePoint(charAt3, charAt4);
                        long j11 = j4 + 1;
                        kb1.a(bArr2, j4, (byte) ((codePoint >>> 18) | 240));
                        long j12 = j11 + 1;
                        kb1.a(bArr2, j11, (byte) (((codePoint >>> 12) & 63) | 128));
                        long j13 = j12 + 1;
                        kb1.a(bArr2, j12, (byte) (((codePoint >>> 6) & 63) | 128));
                        j3 = 1;
                        j2 = j13 + 1;
                        kb1.a(bArr2, j13, (byte) ((codePoint & 63) | 128));
                        i5 = i6;
                        i5++;
                        c = 128;
                        long j82 = j3;
                        j4 = j2;
                        j = j82;
                    } else {
                        i5 = i6;
                    }
                }
                throw new zzxp(i5 - 1, length);
            } else {
                if (55296 <= charAt3 && charAt3 <= 57343) {
                    int i7 = i5 + 1;
                    if (i7 == length || !Character.isSurrogatePair(charAt3, charSequence2.charAt(i7))) {
                        throw new zzxp(i5, length);
                    }
                }
                StringBuilder sb2 = new StringBuilder(46);
                sb2.append("Failed writing ");
                sb2.append(charAt3);
                sb2.append(" at index ");
                sb2.append(j4);
                throw new ArrayIndexOutOfBoundsException(sb2.toString());
            }
            i5++;
            c = 128;
            long j822 = j3;
            j4 = j2;
            j = j822;
        }
        return (int) j4;
    }

    @DexIgnore
    public final void a(CharSequence charSequence, ByteBuffer byteBuffer) {
        long j;
        char c;
        long j2;
        CharSequence charSequence2 = charSequence;
        ByteBuffer byteBuffer2 = byteBuffer;
        long a = kb1.a(byteBuffer);
        long position = ((long) byteBuffer.position()) + a;
        long limit = ((long) byteBuffer.limit()) + a;
        int length = charSequence.length();
        if (((long) length) <= limit - position) {
            int i = 0;
            while (true) {
                c = 128;
                if (i >= length) {
                    break;
                }
                char charAt = charSequence2.charAt(i);
                if (charAt >= 128) {
                    break;
                }
                kb1.a(j, (byte) charAt);
                i++;
                position = j + 1;
            }
            if (i == length) {
                byteBuffer2.position((int) (j - a));
                return;
            }
            while (i < length) {
                char charAt2 = charSequence2.charAt(i);
                if (charAt2 < c && j < limit) {
                    kb1.a(j, (byte) charAt2);
                    j++;
                    j2 = a;
                } else if (charAt2 >= 2048 || j > limit - 2) {
                    j2 = a;
                    if ((charAt2 < 55296 || 57343 < charAt2) && j <= limit - 3) {
                        long j3 = j + 1;
                        kb1.a(j, (byte) ((charAt2 >>> 12) | 480));
                        long j4 = j3 + 1;
                        kb1.a(j3, (byte) (((charAt2 >>> 6) & 63) | 128));
                        kb1.a(j4, (byte) ((charAt2 & '?') | 128));
                        j = j4 + 1;
                    } else if (j <= limit - 4) {
                        int i2 = i + 1;
                        if (i2 != length) {
                            char charAt3 = charSequence2.charAt(i2);
                            if (Character.isSurrogatePair(charAt2, charAt3)) {
                                int codePoint = Character.toCodePoint(charAt2, charAt3);
                                long j5 = j + 1;
                                kb1.a(j, (byte) ((codePoint >>> 18) | 240));
                                long j6 = j5 + 1;
                                kb1.a(j5, (byte) (((codePoint >>> 12) & 63) | 128));
                                long j7 = j6 + 1;
                                kb1.a(j6, (byte) (((codePoint >>> 6) & 63) | 128));
                                long j8 = j7 + 1;
                                kb1.a(j7, (byte) ((codePoint & 63) | 128));
                                i = i2;
                                j = j8;
                            }
                        } else {
                            i2 = i;
                        }
                        throw new zzxp(i2 - 1, length);
                    } else {
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            int i3 = i + 1;
                            if (i3 == length || !Character.isSurrogatePair(charAt2, charSequence2.charAt(i3))) {
                                throw new zzxp(i, length);
                            }
                        }
                        StringBuilder sb = new StringBuilder(46);
                        sb.append("Failed writing ");
                        sb.append(charAt2);
                        sb.append(" at index ");
                        sb.append(j);
                        throw new ArrayIndexOutOfBoundsException(sb.toString());
                    }
                } else {
                    j2 = a;
                    long j9 = j + 1;
                    kb1.a(j, (byte) ((charAt2 >>> 6) | 960));
                    kb1.a(j9, (byte) ((charAt2 & '?') | 128));
                    j = j9 + 1;
                }
                i++;
                ByteBuffer byteBuffer3 = byteBuffer;
                a = j2;
                c = 128;
            }
            byteBuffer.position((int) (j - a));
            return;
        }
        char charAt4 = charSequence2.charAt(length - 1);
        int limit2 = byteBuffer.limit();
        StringBuilder sb2 = new StringBuilder(37);
        sb2.append("Failed writing ");
        sb2.append(charAt4);
        sb2.append(" at index ");
        sb2.append(limit2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    @DexIgnore
    public static int a(byte[] bArr, int i, long j, int i2) {
        if (i2 == 0) {
            return mb1.a(i);
        }
        if (i2 == 1) {
            return mb1.a(i, (int) kb1.a(bArr, j));
        }
        if (i2 == 2) {
            return mb1.a(i, (int) kb1.a(bArr, j), (int) kb1.a(bArr, j + 1));
        }
        throw new AssertionError();
    }
}
