package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c0 */
public class C1523c0 extends com.fossil.blesdk.obfuscated.C1761f0 implements android.content.DialogInterface {

    @DexIgnore
    /* renamed from: g */
    public /* final */ androidx.appcompat.app.AlertController f3949g; // = new androidx.appcompat.app.AlertController(getContext(), this, getWindow());

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.c0$a */
    public static class C1524a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ androidx.appcompat.app.AlertController.C0051f f3950a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f3951b;

        @DexIgnore
        public C1524a(android.content.Context context) {
            this(context, com.fossil.blesdk.obfuscated.C1523c0.m5181b(context, 0));
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9363a(android.view.View view) {
            this.f3950a.f196g = view;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public android.content.Context mo9369b() {
            return this.f3950a.f190a;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1523c0 mo9373c() {
            com.fossil.blesdk.obfuscated.C1523c0 a = mo9368a();
            a.show();
            return a;
        }

        @DexIgnore
        public C1524a(android.content.Context context, int i) {
            this.f3950a = new androidx.appcompat.app.AlertController.C0051f(new android.view.ContextThemeWrapper(context, com.fossil.blesdk.obfuscated.C1523c0.m5181b(context, i)));
            this.f3951b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9365a(java.lang.CharSequence charSequence) {
            this.f3950a.f197h = charSequence;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9371b(java.lang.CharSequence charSequence) {
            this.f3950a.f195f = charSequence;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9362a(android.graphics.drawable.Drawable drawable) {
            this.f3950a.f193d = drawable;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9372b(java.lang.CharSequence charSequence, android.content.DialogInterface.OnClickListener onClickListener) {
            androidx.appcompat.app.AlertController.C0051f fVar = this.f3950a;
            fVar.f198i = charSequence;
            fVar.f200k = onClickListener;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9366a(java.lang.CharSequence charSequence, android.content.DialogInterface.OnClickListener onClickListener) {
            androidx.appcompat.app.AlertController.C0051f fVar = this.f3950a;
            fVar.f201l = charSequence;
            fVar.f203n = onClickListener;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9370b(android.view.View view) {
            androidx.appcompat.app.AlertController.C0051f fVar = this.f3950a;
            fVar.f215z = view;
            fVar.f214y = 0;
            fVar.f179E = false;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9367a(boolean z) {
            this.f3950a.f207r = z;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9361a(android.content.DialogInterface.OnKeyListener onKeyListener) {
            this.f3950a.f210u = onKeyListener;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0.C1524a mo9364a(android.widget.ListAdapter listAdapter, android.content.DialogInterface.OnClickListener onClickListener) {
            androidx.appcompat.app.AlertController.C0051f fVar = this.f3950a;
            fVar.f212w = listAdapter;
            fVar.f213x = onClickListener;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1523c0 mo9368a() {
            com.fossil.blesdk.obfuscated.C1523c0 c0Var = new com.fossil.blesdk.obfuscated.C1523c0(this.f3950a.f190a, this.f3951b);
            this.f3950a.mo230a(c0Var.f3949g);
            c0Var.setCancelable(this.f3950a.f207r);
            if (this.f3950a.f207r) {
                c0Var.setCanceledOnTouchOutside(true);
            }
            c0Var.setOnCancelListener(this.f3950a.f208s);
            c0Var.setOnDismissListener(this.f3950a.f209t);
            android.content.DialogInterface.OnKeyListener onKeyListener = this.f3950a.f210u;
            if (onKeyListener != null) {
                c0Var.setOnKeyListener(onKeyListener);
            }
            return c0Var;
        }
    }

    @DexIgnore
    public C1523c0(android.content.Context context, int i) {
        super(context, m5181b(context, i));
    }

    @DexIgnore
    /* renamed from: b */
    public static int m5181b(android.content.Context context, int i) {
        if (((i >>> 24) & 255) >= 1) {
            return i;
        }
        android.util.TypedValue typedValue = new android.util.TypedValue();
        context.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        this.f3949g.mo202a();
    }

    @DexIgnore
    public boolean onKeyDown(int i, android.view.KeyEvent keyEvent) {
        if (this.f3949g.mo211a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    @DexIgnore
    public boolean onKeyUp(int i, android.view.KeyEvent keyEvent) {
        if (this.f3949g.mo217b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    @DexIgnore
    public void setTitle(java.lang.CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f3949g.mo216b(charSequence);
    }

    @DexIgnore
    /* renamed from: b */
    public android.widget.Button mo9356b(int i) {
        return this.f3949g.mo201a(i);
    }
}
