package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.blesdk.obfuscated.ns3;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.enums.PermissionCodes;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eq2 extends zr2 implements ws3.g {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public BlockingQueue<PermissionCodes> j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<PermissionCodes> {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        /* renamed from: a */
        public final int compare(PermissionCodes permissionCodes, PermissionCodes permissionCodes2) {
            return permissionCodes.ordinal() - permissionCodes2.ordinal();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = eq2.class.getSimpleName();
        kd4.a((Object) simpleName, "BasePermissionFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final boolean T0() {
        boolean a2 = ns3.a.a((Context) PortfolioApp.W.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "forceOpenBackgroundLocationPermission() - isBackgroundLocationPermissionGranted = " + a2);
        if (!a2) {
            f1();
        }
        return !a2;
    }

    @DexIgnore
    public final boolean U0() {
        boolean c = ns3.a.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "forceOpenBluetoothPermission() - isBluetoothEnabled = " + c);
        if (!c) {
            g1();
        }
        return !c;
    }

    @DexIgnore
    public final boolean V0() {
        boolean b2 = ns3.a.b(PortfolioApp.W.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "forceOpenLocationPermission() - isLocationPermissionGranted = " + b2);
        if (!b2) {
            h1();
        }
        return !b2;
    }

    @DexIgnore
    public final boolean W0() {
        boolean d = ns3.a.d();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "forceOpenLocationService() - isLocationOpen = " + d);
        if (!d) {
            i1();
        }
        return !d;
    }

    @DexIgnore
    public final void X0() {
        startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
    }

    @DexIgnore
    public final void Y0() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    @DexIgnore
    public final void Z0() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.W.c().getPackageName(), (String) null));
        startActivity(intent);
    }

    @DexIgnore
    public final void a(PermissionCodes permissionCodes) {
        BlockingQueue<PermissionCodes> blockingQueue = this.j;
        if (blockingQueue == null) {
            kd4.d("mPermissionQueue");
            throw null;
        } else if (!blockingQueue.contains(permissionCodes)) {
            BlockingQueue<PermissionCodes> blockingQueue2 = this.j;
            if (blockingQueue2 != null) {
                blockingQueue2.offer(permissionCodes);
            } else {
                kd4.d("mPermissionQueue");
                throw null;
            }
        }
    }

    @DexIgnore
    public abstract void a1();

    @DexIgnore
    public abstract void b1();

    @DexIgnore
    public abstract void c1();

    @DexIgnore
    public abstract void d1();

    @DexIgnore
    public abstract void e1();

    @DexIgnore
    public final void f1() {
        BlockingQueue<PermissionCodes> blockingQueue = this.j;
        if (blockingQueue != null) {
            PermissionCodes permissionCodes = (PermissionCodes) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + permissionCodes);
            if (!isActive()) {
                return;
            }
            if (permissionCodes == null || permissionCodes != PermissionCodes.BACKGROUND_LOCATION_PERMISSION_OFF) {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.C(childFragmentManager);
                return;
            }
            ds3 ds32 = ds3.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            kd4.a((Object) childFragmentManager2, "childFragmentManager");
            ds32.A(childFragmentManager2);
            return;
        }
        kd4.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.e(childFragmentManager);
        }
    }

    @DexIgnore
    public final void h1() {
        BlockingQueue<PermissionCodes> blockingQueue = this.j;
        if (blockingQueue != null) {
            PermissionCodes permissionCodes = (PermissionCodes) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + permissionCodes);
            if (!isActive()) {
                return;
            }
            if (permissionCodes == null || permissionCodes != PermissionCodes.LOCATION_PERMISSION_FEATURE_OFF) {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.C(childFragmentManager);
                return;
            }
            ds3 ds32 = ds3.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            kd4.a((Object) childFragmentManager2, "childFragmentManager");
            ds32.B(childFragmentManager2);
            return;
        }
        kd4.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        BlockingQueue<PermissionCodes> blockingQueue = this.j;
        if (blockingQueue != null) {
            PermissionCodes permissionCodes = (PermissionCodes) blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "requestLocationService() - permissionErrorCode = " + permissionCodes);
            if (!isActive()) {
                return;
            }
            if (permissionCodes == null || permissionCodes != PermissionCodes.LOCATION_SERVICE_FEATURE_OFF) {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.v(childFragmentManager);
                return;
            }
            ds3 ds32 = ds3.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            kd4.a((Object) childFragmentManager2, "childFragmentManager");
            ds32.u(childFragmentManager2);
            return;
        }
        kd4.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = new PriorityBlockingQueue(5, b.e);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        kd4.b(strArr, "permissions");
        kd4.b(iArr, "grantResults");
        if (i != 0) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            return;
        }
        boolean z = true;
        if (!(!(iArr.length == 0)) || iArr[0] != 0) {
            z = false;
        }
        if (z) {
            a(new PermissionCodes[0]);
        } else {
            c1();
        }
    }

    @DexIgnore
    public final void a(PermissionCodes... permissionCodesArr) {
        boolean z;
        kd4.b(permissionCodesArr, "permissionCodes");
        for (PermissionCodes a2 : permissionCodesArr) {
            a(a2);
        }
        BlockingQueue<PermissionCodes> blockingQueue = this.j;
        if (blockingQueue != null) {
            PermissionCodes permissionCodes = (PermissionCodes) blockingQueue.peek();
            FLogger.INSTANCE.getLocal().d(l, "processPermissionPopups() - permissionErrorCode = " + permissionCodes);
            if (permissionCodes == null) {
                e1();
                return;
            }
            switch (fq2.a[permissionCodes.ordinal()]) {
                case 1:
                    z = U0();
                    break;
                case 2:
                case 3:
                    z = V0();
                    break;
                case 4:
                case 5:
                    z = W0();
                    break;
                case 6:
                    z = T0();
                    break;
                default:
                    throw new NoWhenBranchMatchedException();
            }
            if (!z) {
                BlockingQueue<PermissionCodes> blockingQueue2 = this.j;
                if (blockingQueue2 != null) {
                    blockingQueue2.remove(permissionCodes);
                    a(new PermissionCodes[0]);
                    return;
                }
                kd4.d("mPermissionQueue");
                throw null;
            }
            return;
        }
        kd4.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (kd4.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == R.id.ftv_go_to_setting) {
                d1();
                Z0();
            } else if (i != R.id.tv_ok) {
                c1();
            } else {
                ns3.a aVar = ns3.a;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    aVar.b(activity, 0);
                    return;
                }
                kd4.a();
                throw null;
            }
        } else if (kd4.a((Object) str, (Object) ds3.c.a())) {
            if (i == R.id.ftv_go_to_setting) {
                d1();
                Z0();
            } else if (i != R.id.tv_ok) {
                c1();
            } else {
                ns3.a aVar2 = ns3.a;
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    kd4.a((Object) activity2, "activity!!");
                    aVar2.a((Activity) activity2, 1);
                    return;
                }
                kd4.a();
                throw null;
            }
        } else if (kd4.a((Object) str, (Object) "REQUEST_OPEN_LOCATION_SERVICE")) {
            if (i != R.id.ftv_go_to_setting) {
                b1();
                return;
            }
            d1();
            Y0();
        } else if (!kd4.a((Object) str, (Object) "BLUETOOTH_OFF")) {
        } else {
            if (i != R.id.ftv_go_to_setting) {
                a1();
                return;
            }
            d1();
            X0();
        }
    }
}
