package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g80 extends e80 {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ g80(short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public final long J() {
        return this.M;
    }

    @DexIgnore
    public final long K() {
        return this.N;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 8) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = n90.b(order.getInt(0));
            this.N = n90.b(order.getInt(4));
            wa0.a(wa0.a(a, JSONKey.WRITTEN_SIZE, Long.valueOf(this.M)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.N));
        }
        return a;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.WRITTEN_SIZE, Long.valueOf(this.M)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.N));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g80(short s, Peripheral peripheral, int i) {
        super(FileControlOperationCode.GET_SIZE_WRITTEN, s, RequestId.GET_FILE_SIZE_WRITTEN, peripheral, i);
        kd4.b(peripheral, "peripheral");
    }
}
