package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yl3 implements Factory<ProfileSetupPresenter> {
    @DexIgnore
    public static ProfileSetupPresenter a(vl3 vl3, GetRecommendedGoalUseCase getRecommendedGoalUseCase, UserRepository userRepository) {
        return new ProfileSetupPresenter(vl3, getRecommendedGoalUseCase, userRepository);
    }
}
