package com.fossil.blesdk.obfuscated;

import io.reactivex.BackpressureStrategy;
import io.reactivex.internal.observers.LambdaObserver;
import io.reactivex.internal.operators.observable.ObservableObserveOn;
import io.reactivex.internal.operators.observable.ObservableRetryPredicate;
import io.reactivex.internal.operators.observable.ObservableSubscribeOn;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class o84<T> implements p84<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[BackpressureStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[BackpressureStrategy.DROP.ordinal()] = 1;
            a[BackpressureStrategy.LATEST.ordinal()] = 2;
            a[BackpressureStrategy.MISSING.ordinal()] = 3;
            try {
                a[BackpressureStrategy.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public static <T> o84<T> a(T t) {
        k94.a(t, "The item is null");
        return ia4.a(new ba4(t));
    }

    @DexIgnore
    public static int d() {
        return k84.d();
    }

    @DexIgnore
    public final l84<T> b() {
        return ia4.a(new ca4(this));
    }

    @DexIgnore
    public abstract void b(q84<? super T> q84);

    @DexIgnore
    public final s84<T> c() {
        return ia4.a(new da4(this, null));
    }

    @DexIgnore
    public final o84<T> b(r84 r84) {
        k94.a(r84, "scheduler is null");
        return ia4.a(new ObservableSubscribeOn(this, r84));
    }

    @DexIgnore
    public final h84 a() {
        return ia4.a((h84) new aa4(this));
    }

    @DexIgnore
    public final o84<T> a(r84 r84) {
        return a(r84, false, d());
    }

    @DexIgnore
    public final o84<T> a(r84 r84, boolean z, int i) {
        k94.a(r84, "scheduler is null");
        k94.a(i, "bufferSize");
        return ia4.a(new ObservableObserveOn(this, r84, z, i));
    }

    @DexIgnore
    public final o84<T> a(long j) {
        return a(j, (h94<? super Throwable>) j94.a());
    }

    @DexIgnore
    public final o84<T> a(long j, h94<? super Throwable> h94) {
        if (j >= 0) {
            k94.a(h94, "predicate is null");
            return ia4.a(new ObservableRetryPredicate(this, j, h94));
        }
        throw new IllegalArgumentException("times >= 0 required but it was " + j);
    }

    @DexIgnore
    public final y84 a(e94<? super T> e94, e94<? super Throwable> e942) {
        return a(e94, e942, j94.a, j94.b());
    }

    @DexIgnore
    public final y84 a(e94<? super T> e94, e94<? super Throwable> e942, b94 b94, e94<? super y84> e943) {
        k94.a(e94, "onNext is null");
        k94.a(e942, "onError is null");
        k94.a(b94, "onComplete is null");
        k94.a(e943, "onSubscribe is null");
        LambdaObserver lambdaObserver = new LambdaObserver(e94, e942, b94, e943);
        a(lambdaObserver);
        return lambdaObserver;
    }

    @DexIgnore
    public final void a(q84<? super T> q84) {
        k94.a(q84, "observer is null");
        try {
            q84<? super Object> a2 = ia4.a(this, q84);
            k94.a(a2, "Plugin returned null Observer");
            b(a2);
        } catch (NullPointerException e) {
            throw e;
        } catch (Throwable th) {
            a94.b(th);
            ia4.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }

    @DexIgnore
    public final k84<T> a(BackpressureStrategy backpressureStrategy) {
        u94 u94 = new u94(this);
        int i = a.a[backpressureStrategy.ordinal()];
        if (i == 1) {
            return u94.b();
        }
        if (i == 2) {
            return u94.c();
        }
        if (i == 3) {
            return u94;
        }
        if (i != 4) {
            return u94.a();
        }
        return ia4.a(new x94(u94));
    }
}
