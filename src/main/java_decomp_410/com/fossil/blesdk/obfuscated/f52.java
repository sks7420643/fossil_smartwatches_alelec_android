package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f52 implements Factory<rc> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public f52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static f52 a(n42 n42) {
        return new f52(n42);
    }

    @DexIgnore
    public static rc b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static rc c(n42 n42) {
        rc i = n42.i();
        n44.a(i, "Cannot return null from a non-@Nullable @Provides method");
        return i;
    }

    @DexIgnore
    public rc get() {
        return b(this.a);
    }
}
