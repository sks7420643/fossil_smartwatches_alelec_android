package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.DatabaseHelper;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class co2 extends BaseDbProvider implements bo2 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public co2(Context context, String str) {
        super(context, str);
        kd4.b(context, "context");
        kd4.b(str, "dbPath");
    }

    @DexIgnore
    public boolean a() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            kd4.a((Object) str, "TAG");
            local.d(str, "Inside .clearAllServerSettings");
            g().deleteBuilder().delete();
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            kd4.a((Object) str2, "TAG");
            local2.e(str2, "Error Inside e = " + e);
            return false;
        }
    }

    @DexIgnore
    public boolean addOrUpdateServerSetting(ServerSetting serverSetting) {
        if (serverSetting == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            kd4.a((Object) str, "TAG");
            local.d(str, "serverSetting is null");
            return false;
        }
        try {
            Dao.CreateOrUpdateStatus createOrUpdate = g().createOrUpdate(serverSetting);
            kd4.a((Object) createOrUpdate, "status");
            if (createOrUpdate.isCreated() || createOrUpdate.isUpdated()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            kd4.a((Object) str2, "TAG");
            local2.e(str2, "addOrUpdateServerSetting + e = " + e);
            return false;
        }
    }

    @DexIgnore
    public final Dao<ServerSetting, String> g() {
        Dao<ServerSetting, String> dao = this.databaseHelper.getDao(ServerSetting.class);
        kd4.a((Object) dao, "databaseHelper.getDao(ServerSetting::class.java)");
        return dao;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{ServerSetting.class};
    }

    @DexIgnore
    public String getDbPath() {
        DatabaseHelper databaseHelper = this.databaseHelper;
        kd4.a((Object) databaseHelper, "databaseHelper");
        String dbPath = databaseHelper.getDbPath();
        kd4.a((Object) dbPath, "databaseHelper.dbPath");
        return dbPath;
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        if (str == null) {
            return null;
        }
        try {
            return g().queryBuilder().where().eq("key", str).queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            kd4.a((Object) str2, "TAG");
            local.e(str2, "getServerSettingByKey + e = " + e);
            return null;
        }
    }

    @DexIgnore
    public void a(List<ServerSetting> list) {
        Boolean valueOf = list != null ? Boolean.valueOf(list.isEmpty()) : null;
        if (valueOf == null) {
            kd4.a();
            throw null;
        } else if (valueOf.booleanValue()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            kd4.a((Object) str, "TAG");
            local.d(str, "serverSettingList is null or empty");
        } else {
            try {
                for (ServerSetting createOrUpdate : list) {
                    g().createOrUpdate(createOrUpdate);
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                kd4.a((Object) str2, "TAG");
                local2.e(str2, "addOrUpdateServerSettingList + e = " + e);
            }
        }
    }
}
