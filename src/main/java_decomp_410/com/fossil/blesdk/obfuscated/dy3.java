package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.hv3;
import com.fossil.blesdk.obfuscated.su3;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.NetworkPolicy;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dy3 implements Downloader {
    @DexIgnore
    public /* final */ gv3 a;

    @DexIgnore
    public dy3(Context context) {
        this(oy3.b(context));
    }

    @DexIgnore
    public static gv3 a() {
        gv3 gv3 = new gv3();
        gv3.a(15000, TimeUnit.MILLISECONDS);
        gv3.b(20000, TimeUnit.MILLISECONDS);
        gv3.c(20000, TimeUnit.MILLISECONDS);
        return gv3;
    }

    @DexIgnore
    public Downloader.Response load(Uri uri, int i) throws IOException {
        su3 su3;
        if (i == 0) {
            su3 = null;
        } else if (NetworkPolicy.isOfflineOnly(i)) {
            su3 = su3.m;
        } else {
            su3.b bVar = new su3.b();
            if (!NetworkPolicy.shouldReadFromDiskCache(i)) {
                bVar.b();
            }
            if (!NetworkPolicy.shouldWriteToDiskCache(i)) {
                bVar.c();
            }
            su3 = bVar.a();
        }
        hv3.b bVar2 = new hv3.b();
        bVar2.b(uri.toString());
        if (su3 != null) {
            bVar2.a(su3);
        }
        jv3 a2 = this.a.a(bVar2.a()).a();
        int e = a2.e();
        if (e < 300) {
            boolean z = a2.c() != null;
            kv3 a3 = a2.a();
            return new Downloader.Response(a3.y(), z, a3.z());
        }
        a2.a().close();
        throw new Downloader.ResponseException(e + " " + a2.h(), i, e);
    }

    @DexIgnore
    public dy3(File file) {
        this(file, oy3.a(file));
    }

    @DexIgnore
    public dy3(File file, long j) {
        this(a());
        try {
            this.a.a(new ru3(file, j));
        } catch (IOException unused) {
        }
    }

    @DexIgnore
    public dy3(gv3 gv3) {
        this.a = gv3;
    }
}
