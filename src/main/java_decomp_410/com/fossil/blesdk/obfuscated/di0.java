package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.DialogInterface;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class di0 implements Runnable {
    @DexIgnore
    public /* final */ ci0 e;
    @DexIgnore
    public /* final */ /* synthetic */ bi0 f;

    @DexIgnore
    public di0(bi0 bi0, ci0 ci0) {
        this.f = bi0;
        this.e = ci0;
    }

    @DexIgnore
    public final void run() {
        if (this.f.f) {
            ud0 a = this.e.a();
            if (a.K()) {
                bi0 bi0 = this.f;
                bi0.e.startActivityForResult(GoogleApiActivity.a(bi0.a(), a.J(), this.e.b(), false), 1);
            } else if (this.f.i.c(a.H())) {
                bi0 bi02 = this.f;
                bi02.i.a(bi02.a(), this.f.e, a.H(), 2, this.f);
            } else if (a.H() == 18) {
                Dialog a2 = xd0.a(this.f.a(), (DialogInterface.OnCancelListener) this.f);
                bi0 bi03 = this.f;
                bi03.i.a(bi03.a().getApplicationContext(), (ah0) new ei0(this, a2));
            } else {
                this.f.a(a, this.e.b());
            }
        }
    }
}
