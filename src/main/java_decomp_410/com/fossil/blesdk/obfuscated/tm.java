package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import com.android.volley.Request;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tm {
    @DexIgnore
    public /* final */ AtomicInteger a;
    @DexIgnore
    public /* final */ Set<Request<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<Request<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<Request<?>> d;
    @DexIgnore
    public /* final */ lm e;
    @DexIgnore
    public /* final */ qm f;
    @DexIgnore
    public /* final */ vm g;
    @DexIgnore
    public /* final */ rm[] h;
    @DexIgnore
    public mm i;
    @DexIgnore
    public /* final */ List<a> j;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(Request<T> request);
    }

    @DexIgnore
    public tm(lm lmVar, qm qmVar, int i2, vm vmVar) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = lmVar;
        this.f = qmVar;
        this.h = new rm[i2];
        this.g = vmVar;
    }

    @DexIgnore
    public int a() {
        return this.a.incrementAndGet();
    }

    @DexIgnore
    public void b() {
        c();
        this.i = new mm(this.c, this.d, this.e, this.g);
        this.i.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            rm rmVar = new rm(this.d, this.f, this.e, this.g);
            this.h[i2] = rmVar;
            rmVar.start();
        }
    }

    @DexIgnore
    public void c() {
        mm mmVar = this.i;
        if (mmVar != null) {
            mmVar.b();
        }
        for (rm rmVar : this.h) {
            if (rmVar != null) {
                rmVar.b();
            }
        }
    }

    @DexIgnore
    public <T> Request<T> a(Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(request);
        }
        request.setSequence(a());
        request.addMarker("add-to-queue");
        if (!request.shouldCache()) {
            this.d.add(request);
            return request;
        }
        this.c.add(request);
        return request;
    }

    @DexIgnore
    public <T> void b(Request<T> request) {
        synchronized (this.b) {
            this.b.remove(request);
        }
        synchronized (this.j) {
            for (a a2 : this.j) {
                a2.a(request);
            }
        }
    }

    @DexIgnore
    public tm(lm lmVar, qm qmVar, int i2) {
        this(lmVar, qmVar, i2, new om(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public tm(lm lmVar, qm qmVar) {
        this(lmVar, qmVar, 4);
    }
}
