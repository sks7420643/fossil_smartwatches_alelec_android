package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cv3;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.IOException;
import java.net.URI;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hv3 {
    @DexIgnore
    public /* final */ dv3 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ cv3 c;
    @DexIgnore
    public /* final */ iv3 d;
    @DexIgnore
    public /* final */ Object e;
    @DexIgnore
    public volatile URI f;
    @DexIgnore
    public volatile su3 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public dv3 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public cv3.b c;
        @DexIgnore
        public iv3 d;
        @DexIgnore
        public Object e;

        @DexIgnore
        public b() {
            this.b = "GET";
            this.c = new cv3.b();
        }

        @DexIgnore
        public b a(dv3 dv3) {
            if (dv3 != null) {
                this.a = dv3;
                return this;
            }
            throw new IllegalArgumentException("url == null");
        }

        @DexIgnore
        public b b(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                dv3 c2 = dv3.c(str);
                if (c2 != null) {
                    a(c2);
                    return this;
                }
                throw new IllegalArgumentException("unexpected url: " + str);
            }
            throw new IllegalArgumentException("url == null");
        }

        @DexIgnore
        public b a(String str, String str2) {
            this.c.a(str, str2);
            return this;
        }

        @DexIgnore
        public b(hv3 hv3) {
            this.a = hv3.a;
            this.b = hv3.b;
            this.d = hv3.d;
            this.e = hv3.e;
            this.c = hv3.c.a();
        }

        @DexIgnore
        public b a(String str) {
            this.c.b(str);
            return this;
        }

        @DexIgnore
        public b a(cv3 cv3) {
            this.c = cv3.a();
            return this;
        }

        @DexIgnore
        public b a(su3 su3) {
            String su32 = su3.toString();
            if (su32.isEmpty()) {
                a(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
                return this;
            }
            b(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, su32);
            return this;
        }

        @DexIgnore
        public b a(String str, iv3 iv3) {
            if (str == null || str.length() == 0) {
                throw new IllegalArgumentException("method == null || method.length() == 0");
            } else if (iv3 != null && !vw3.b(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (iv3 != null || !vw3.c(str)) {
                this.b = str;
                this.d = iv3;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        @DexIgnore
        public b b(String str, String str2) {
            this.c.d(str, str2);
            return this;
        }

        @DexIgnore
        public hv3 a() {
            if (this.a != null) {
                return new hv3(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    @DexIgnore
    public String f() {
        return this.b;
    }

    @DexIgnore
    public b g() {
        return new b();
    }

    @DexIgnore
    public URI h() throws IOException {
        try {
            URI uri = this.f;
            if (uri != null) {
                return uri;
            }
            URI k = this.a.k();
            this.f = k;
            return k;
        } catch (IllegalStateException e2) {
            throw new IOException(e2.getMessage());
        }
    }

    @DexIgnore
    public String i() {
        return this.a.toString();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request{method=");
        sb.append(this.b);
        sb.append(", url=");
        sb.append(this.a);
        sb.append(", tag=");
        Object obj = this.e;
        if (obj == this) {
            obj = null;
        }
        sb.append(obj);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public hv3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c.a();
        this.d = bVar.d;
        this.e = bVar.e != null ? bVar.e : this;
    }

    @DexIgnore
    public String a(String str) {
        return this.c.a(str);
    }

    @DexIgnore
    public List<String> b(String str) {
        return this.c.c(str);
    }

    @DexIgnore
    public cv3 c() {
        return this.c;
    }

    @DexIgnore
    public dv3 d() {
        return this.a;
    }

    @DexIgnore
    public boolean e() {
        return this.a.g();
    }

    @DexIgnore
    public iv3 a() {
        return this.d;
    }

    @DexIgnore
    public su3 b() {
        su3 su3 = this.g;
        if (su3 != null) {
            return su3;
        }
        su3 a2 = su3.a(this.c);
        this.g = a2;
        return a2;
    }
}
