package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qs */
public abstract class C2764qs implements com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> {
    @DexIgnore
    /* renamed from: a */
    public abstract android.graphics.Bitmap mo8933a(com.fossil.blesdk.obfuscated.C2149jq jqVar, android.graphics.Bitmap bitmap, int i, int i2);

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo12877a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar, int i, int i2) {
        if (com.fossil.blesdk.obfuscated.C3066uw.m14933b(i, i2)) {
            com.fossil.blesdk.obfuscated.C2149jq c = com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15645c();
            android.graphics.Bitmap bitmap = aqVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            android.graphics.Bitmap a = mo8933a(c, bitmap, i, i2);
            return bitmap.equals(a) ? aqVar : com.fossil.blesdk.obfuscated.C2675ps.m12395a(a, c);
        }
        throw new java.lang.IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
