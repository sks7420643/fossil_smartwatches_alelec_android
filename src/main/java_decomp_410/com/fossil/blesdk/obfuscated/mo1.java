package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo1<TResult> implements qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public tn1<? super TResult> c;

    @DexIgnore
    public mo1(Executor executor, tn1<? super TResult> tn1) {
        this.a = executor;
        this.c = tn1;
    }

    @DexIgnore
    public final void onComplete(wn1<TResult> wn1) {
        if (wn1.e()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new no1(this, wn1));
                }
            }
        }
    }
}
