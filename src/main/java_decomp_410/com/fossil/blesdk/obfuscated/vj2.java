package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj2 {
    @DexIgnore
    public /* final */ GetDianaDeviceSettingUseCase a;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase b;
    @DexIgnore
    public /* final */ HybridSyncUseCase c;
    @DexIgnore
    public /* final */ DianaSyncUseCase d;

    @DexIgnore
    public vj2(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, HybridSyncUseCase hybridSyncUseCase, DianaSyncUseCase dianaSyncUseCase) {
        kd4.b(getDianaDeviceSettingUseCase, "mGetDianaDeviceSettingUseCase");
        kd4.b(getHybridDeviceSettingUseCase, "mGetHybridDeviceSettingUseCase");
        kd4.b(hybridSyncUseCase, "mHybridSyncUseCase");
        kd4.b(dianaSyncUseCase, "mDianaSyncUseCase");
        this.a = getDianaDeviceSettingUseCase;
        this.b = getHybridDeviceSettingUseCase;
        this.c = hybridSyncUseCase;
        this.d = dianaSyncUseCase;
    }

    @DexIgnore
    public final CoroutineUseCase<on3, pn3, nn3> a(String str) {
        kd4.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = uj2.a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4) {
                return this.a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final CoroutineUseCase<zq2, ar2, yq2> b(String str) {
        kd4.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = uj2.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4) {
                return this.d;
            }
        }
        return this.d;
    }
}
