package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gf0 {
    @DexIgnore
    public static void a(Status status, xn1<Void> xn1) {
        a(status, (Object) null, xn1);
    }

    @DexIgnore
    public static <TResult> void a(Status status, TResult tresult, xn1<TResult> xn1) {
        if (status.L()) {
            xn1.a(tresult);
        } else {
            xn1.a((Exception) new ApiException(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static wn1<Void> a(wn1<Boolean> wn1) {
        return wn1.a((pn1<Boolean, TContinuationResult>) new lh0());
    }
}
