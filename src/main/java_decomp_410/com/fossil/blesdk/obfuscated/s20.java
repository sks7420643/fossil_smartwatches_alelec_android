package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class s20<K, V> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003f A[SYNTHETIC] */
    public final k20<K> a(Version version) {
        byte b;
        k20<K> k20 = null;
        for (k20<K> k202 : a()) {
            if (kd4.a((Object) k202.a(), (Object) version)) {
                return k202;
            }
            if (k202.a().getMajor() == version.getMajor()) {
                byte minor = k202.a().getMinor();
                if (k20 != null) {
                    Version a2 = k20.a();
                    if (a2 != null) {
                        b = a2.getMinor();
                        if (minor < b) {
                            k20 = k202;
                        }
                    }
                }
                b = 0;
                if (minor < b) {
                }
            }
        }
        return k20;
    }

    @DexIgnore
    public abstract k20<K>[] a();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003f A[SYNTHETIC] */
    public final l20<V> b(Version version) {
        byte b;
        l20<V> l20 = null;
        for (l20<V> l202 : b()) {
            if (kd4.a((Object) l202.a(), (Object) version)) {
                return l202;
            }
            if (l202.a().getMajor() == version.getMajor()) {
                byte minor = l202.a().getMinor();
                if (l20 != null) {
                    Version a2 = l20.a();
                    if (a2 != null) {
                        b = a2.getMinor();
                        if (minor < b) {
                            l20 = l202;
                        }
                    }
                }
                b = 0;
                if (minor < b) {
                }
            }
        }
        return l20;
    }

    @DexIgnore
    public abstract l20<V>[] b();

    @DexIgnore
    public final byte[] a(short s, Version version, K k) throws FileFormatException {
        kd4.b(version, "version");
        kd4.b(k, "entries");
        k20 a2 = a(version);
        if (a2 != null) {
            return a2.a(s, k);
        }
        FileFormatException.FileFormatErrorCode fileFormatErrorCode = FileFormatException.FileFormatErrorCode.UNSUPPORTED_VERSION;
        throw new FileFormatException(fileFormatErrorCode, "Not support version " + version + '.', (Throwable) null, 4, (fd4) null);
    }

    @DexIgnore
    public final V a(byte[] bArr) throws FileFormatException {
        kd4.b(bArr, "data");
        try {
            Version version = new Version(bArr[2], bArr[3]);
            l20 b = b(version);
            if (b != null) {
                return b.a(bArr);
            }
            FileFormatException.FileFormatErrorCode fileFormatErrorCode = FileFormatException.FileFormatErrorCode.UNSUPPORTED_VERSION;
            throw new FileFormatException(fileFormatErrorCode, "Not support version " + version + '.', (Throwable) null, 4, (fd4) null);
        } catch (Exception e) {
            throw new FileFormatException(FileFormatException.FileFormatErrorCode.INVALID_FILE_DATA, "Invalid file data.", e);
        }
    }
}
