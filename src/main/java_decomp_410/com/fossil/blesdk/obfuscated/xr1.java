package com.fossil.blesdk.obfuscated;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import com.google.android.material.card.MaterialCardView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xr1 {
    @DexIgnore
    public /* final */ MaterialCardView a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public xr1(MaterialCardView materialCardView) {
        this.a = materialCardView;
    }

    @DexIgnore
    public void a(TypedArray typedArray) {
        this.b = typedArray.getColor(cr1.MaterialCardView_strokeColor, -1);
        this.c = typedArray.getDimensionPixelSize(cr1.MaterialCardView_strokeWidth, 0);
        e();
        a();
    }

    @DexIgnore
    public void b(int i) {
        this.c = i;
        e();
        a();
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public void e() {
        this.a.setForeground(b());
    }

    @DexIgnore
    public final Drawable b() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(this.a.getRadius());
        int i = this.b;
        if (i != -1) {
            gradientDrawable.setStroke(this.c, i);
        }
        return gradientDrawable;
    }

    @DexIgnore
    public void a(int i) {
        this.b = i;
        e();
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.a.getContentPaddingLeft() + this.c, this.a.getContentPaddingTop() + this.c, this.a.getContentPaddingRight() + this.c, this.a.getContentPaddingBottom() + this.c);
    }
}
