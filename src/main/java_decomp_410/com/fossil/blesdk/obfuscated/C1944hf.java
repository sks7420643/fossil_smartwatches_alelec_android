package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hf */
public abstract class C1944hf extends com.fossil.blesdk.obfuscated.C1873gf {
    @DexIgnore
    public C1944hf(long j, androidx.renderscript.RenderScript renderScript) {
        super(j, renderScript);
        if (j == 0) {
            throw new androidx.renderscript.RSRuntimeException("Loading of ScriptIntrinsic failed.");
        }
    }
}
