package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a8 */
public final class C1398a8 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C3417z7 f3403a; // = new com.fossil.blesdk.obfuscated.C1398a8.C1403e((com.fossil.blesdk.obfuscated.C1398a8.C1401c) null, false);

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C3417z7 f3404b; // = new com.fossil.blesdk.obfuscated.C1398a8.C1403e((com.fossil.blesdk.obfuscated.C1398a8.C1401c) null, true);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C3417z7 f3405c; // = new com.fossil.blesdk.obfuscated.C1398a8.C1403e(com.fossil.blesdk.obfuscated.C1398a8.C1400b.f3409a, false);

    @DexIgnore
    /* renamed from: d */
    public static /* final */ com.fossil.blesdk.obfuscated.C3417z7 f3406d; // = new com.fossil.blesdk.obfuscated.C1398a8.C1403e(com.fossil.blesdk.obfuscated.C1398a8.C1400b.f3409a, true);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a8$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a8$a */
    public static class C1399a implements com.fossil.blesdk.obfuscated.C1398a8.C1401c {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ com.fossil.blesdk.obfuscated.C1398a8.C1399a f3407b; // = new com.fossil.blesdk.obfuscated.C1398a8.C1399a(true);

        @DexIgnore
        /* renamed from: a */
        public /* final */ boolean f3408a;

        /*
        static {
            new com.fossil.blesdk.obfuscated.C1398a8.C1399a(false);
        }
        */

        @DexIgnore
        public C1399a(boolean z) {
            this.f3408a = z;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo8595a(java.lang.CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            boolean z = false;
            while (i < i3) {
                int a = com.fossil.blesdk.obfuscated.C1398a8.m4283a(java.lang.Character.getDirectionality(charSequence.charAt(i)));
                if (a != 0) {
                    if (a != 1) {
                        continue;
                        i++;
                    } else if (!this.f3408a) {
                        return 1;
                    }
                } else if (this.f3408a) {
                    return 0;
                }
                z = true;
                i++;
            }
            if (z) {
                return this.f3408a ? 1 : 0;
            }
            return 2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a8$b")
    /* renamed from: com.fossil.blesdk.obfuscated.a8$b */
    public static class C1400b implements com.fossil.blesdk.obfuscated.C1398a8.C1401c {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ com.fossil.blesdk.obfuscated.C1398a8.C1400b f3409a; // = new com.fossil.blesdk.obfuscated.C1398a8.C1400b();

        @DexIgnore
        /* renamed from: a */
        public int mo8595a(java.lang.CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            int i4 = 2;
            while (i < i3 && i4 == 2) {
                i4 = com.fossil.blesdk.obfuscated.C1398a8.m4284b(java.lang.Character.getDirectionality(charSequence.charAt(i)));
                i++;
            }
            return i4;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a8$c */
    public interface C1401c {
        @DexIgnore
        /* renamed from: a */
        int mo8595a(java.lang.CharSequence charSequence, int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a8$d")
    /* renamed from: com.fossil.blesdk.obfuscated.a8$d */
    public static abstract class C1402d implements com.fossil.blesdk.obfuscated.C3417z7 {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1398a8.C1401c f3410a;

        @DexIgnore
        public C1402d(com.fossil.blesdk.obfuscated.C1398a8.C1401c cVar) {
            this.f3410a = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public abstract boolean mo8596a();

        @DexIgnore
        /* renamed from: a */
        public boolean mo8597a(java.lang.CharSequence charSequence, int i, int i2) {
            if (charSequence == null || i < 0 || i2 < 0 || charSequence.length() - i2 < i) {
                throw new java.lang.IllegalArgumentException();
            } else if (this.f3410a == null) {
                return mo8596a();
            } else {
                return mo8598b(charSequence, i, i2);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final boolean mo8598b(java.lang.CharSequence charSequence, int i, int i2) {
            int a = this.f3410a.mo8595a(charSequence, i, i2);
            if (a == 0) {
                return true;
            }
            if (a != 1) {
                return mo8596a();
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a8$e")
    /* renamed from: com.fossil.blesdk.obfuscated.a8$e */
    public static class C1403e extends com.fossil.blesdk.obfuscated.C1398a8.C1402d {

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f3411b;

        @DexIgnore
        public C1403e(com.fossil.blesdk.obfuscated.C1398a8.C1401c cVar, boolean z) {
            super(cVar);
            this.f3411b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8596a() {
            return this.f3411b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a8$f")
    /* renamed from: com.fossil.blesdk.obfuscated.a8$f */
    public static class C1404f extends com.fossil.blesdk.obfuscated.C1398a8.C1402d {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ com.fossil.blesdk.obfuscated.C1398a8.C1404f f3412b; // = new com.fossil.blesdk.obfuscated.C1398a8.C1404f();

        @DexIgnore
        public C1404f() {
            super((com.fossil.blesdk.obfuscated.C1398a8.C1401c) null);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo8596a() {
            return com.fossil.blesdk.obfuscated.C1467b8.m4811b(java.util.Locale.getDefault()) == 1;
        }
    }

    /*
    static {
        new com.fossil.blesdk.obfuscated.C1398a8.C1403e(com.fossil.blesdk.obfuscated.C1398a8.C1399a.f3407b, false);
        com.fossil.blesdk.obfuscated.C1398a8.C1404f fVar = com.fossil.blesdk.obfuscated.C1398a8.C1404f.f3412b;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static int m4283a(int i) {
        if (i != 0) {
            return (i == 1 || i == 2) ? 0 : 2;
        }
        return 1;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m4284b(int i) {
        if (i != 0) {
            if (i == 1 || i == 2) {
                return 0;
            }
            switch (i) {
                case 14:
                case 15:
                    break;
                case 16:
                case 17:
                    return 0;
                default:
                    return 2;
            }
        }
        return 1;
    }
}
