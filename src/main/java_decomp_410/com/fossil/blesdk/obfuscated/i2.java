package com.fossil.blesdk.obfuscated;

import android.view.Menu;
import android.view.Window;
import com.fossil.blesdk.obfuscated.p1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i2 {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(Menu menu, p1.a aVar);

    @DexIgnore
    boolean a();

    @DexIgnore
    void b();

    @DexIgnore
    boolean c();

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    void g();

    @DexIgnore
    void setWindowCallback(Window.Callback callback);

    @DexIgnore
    void setWindowTitle(CharSequence charSequence);
}
