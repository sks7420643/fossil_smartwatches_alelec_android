package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hi implements ji {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public hi(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof hi) && ((hi) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
