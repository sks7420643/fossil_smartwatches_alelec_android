package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d61 extends vb1<d61> {
    @DexIgnore
    public static volatile d61[] h;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Long e; // = null;
    @DexIgnore
    public Float f; // = null;
    @DexIgnore
    public Double g; // = null;

    @DexIgnore
    public d61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static d61[] e() {
        if (h == null) {
            synchronized (zb1.b) {
                if (h == null) {
                    h = new d61[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        String str = this.c;
        if (str != null) {
            ub1.a(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            ub1.a(2, str2);
        }
        Long l = this.e;
        if (l != null) {
            ub1.b(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            ub1.a(4, f2.floatValue());
        }
        Double d2 = this.g;
        if (d2 != null) {
            ub1.a(5, d2.doubleValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d61)) {
            return false;
        }
        d61 d61 = (d61) obj;
        String str = this.c;
        if (str == null) {
            if (d61.c != null) {
                return false;
            }
        } else if (!str.equals(d61.c)) {
            return false;
        }
        String str2 = this.d;
        if (str2 == null) {
            if (d61.d != null) {
                return false;
            }
        } else if (!str2.equals(d61.d)) {
            return false;
        }
        Long l = this.e;
        if (l == null) {
            if (d61.e != null) {
                return false;
            }
        } else if (!l.equals(d61.e)) {
            return false;
        }
        Float f2 = this.f;
        if (f2 == null) {
            if (d61.f != null) {
                return false;
            }
        } else if (!f2.equals(d61.f)) {
            return false;
        }
        Double d2 = this.g;
        if (d2 == null) {
            if (d61.g != null) {
                return false;
            }
        } else if (!d2.equals(d61.g)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(d61.b);
        }
        xb1 xb12 = d61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (d61.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.d;
        int hashCode3 = (hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l = this.e;
        int hashCode4 = (hashCode3 + (l == null ? 0 : l.hashCode())) * 31;
        Float f2 = this.f;
        int hashCode5 = (hashCode4 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.g;
        int hashCode6 = (hashCode5 + (d2 == null ? 0 : d2.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode6 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += ub1.b(1, str);
        }
        String str2 = this.d;
        if (str2 != null) {
            a += ub1.b(2, str2);
        }
        Long l = this.e;
        if (l != null) {
            a += ub1.c(3, l.longValue());
        }
        Float f2 = this.f;
        if (f2 != null) {
            f2.floatValue();
            a += ub1.c(4) + 4;
        }
        Double d2 = this.g;
        if (d2 == null) {
            return a;
        }
        d2.doubleValue();
        return a + ub1.c(5) + 8;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = tb1.b();
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 24) {
                this.e = Long.valueOf(tb1.f());
            } else if (c2 == 37) {
                this.f = Float.valueOf(Float.intBitsToFloat(tb1.g()));
            } else if (c2 == 41) {
                this.g = Double.valueOf(Double.longBitsToDouble(tb1.h()));
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
