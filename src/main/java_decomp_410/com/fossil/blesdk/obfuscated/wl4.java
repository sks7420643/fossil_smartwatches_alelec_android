package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wl4 extends RequestBody {
    @DexIgnore
    public static /* final */ am4 c; // = am4.a("application/x-www-form-urlencoded");
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public a() {
            this((Charset) null);
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(zl4.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(zl4.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(zl4.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(zl4.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a(Charset charset) {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public wl4 a() {
            return new wl4(this.a, this.b);
        }
    }

    @DexIgnore
    public wl4(List<String> list, List<String> list2) {
        this.a = jm4.a(list);
        this.b = jm4.a(list2);
    }

    @DexIgnore
    public long a() {
        return a((ko4) null, true);
    }

    @DexIgnore
    public am4 b() {
        return c;
    }

    @DexIgnore
    public void a(ko4 ko4) throws IOException {
        a(ko4, false);
    }

    @DexIgnore
    public final long a(ko4 ko4, boolean z) {
        jo4 jo4;
        if (z) {
            jo4 = new jo4();
        } else {
            jo4 = ko4.a();
        }
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                jo4.writeByte(38);
            }
            jo4.a(this.a.get(i));
            jo4.writeByte(61);
            jo4.a(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long B = jo4.B();
        jo4.w();
        return B;
    }
}
