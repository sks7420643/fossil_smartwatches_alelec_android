package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vk4 extends tk4 {
    @DexIgnore
    public /* final */ Runnable g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vk4(Runnable runnable, long j, uk4 uk4) {
        super(j, uk4);
        kd4.b(runnable, "block");
        kd4.b(uk4, "taskContext");
        this.g = runnable;
    }

    @DexIgnore
    public void run() {
        try {
            this.g.run();
        } finally {
            this.f.A();
        }
    }

    @DexIgnore
    public String toString() {
        return "Task[" + dh4.a((Object) this.g) + '@' + dh4.b(this.g) + ", " + this.e + ", " + this.f + ']';
    }
}
