package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ki0 implements ch0 {
    @DexIgnore
    public /* final */ /* synthetic */ ii0 a;

    @DexIgnore
    public ki0(ii0 ii0) {
        this.a = ii0;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        this.a.q.lock();
        try {
            this.a.a(bundle);
            ud0 unused = this.a.n = ud0.i;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ ki0(ii0 ii0, ji0 ji0) {
        this(ii0);
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        this.a.q.lock();
        try {
            ud0 unused = this.a.n = ud0;
            this.a.i();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a.q.lock();
        try {
            if (!this.a.p && this.a.o != null) {
                if (this.a.o.L()) {
                    boolean unused = this.a.p = true;
                    this.a.i.f(i);
                    this.a.q.unlock();
                    return;
                }
            }
            boolean unused2 = this.a.p = false;
            this.a.a(i, z);
        } finally {
            this.a.q.unlock();
        }
    }
}
