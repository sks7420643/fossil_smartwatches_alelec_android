package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j71;
import com.fossil.blesdk.obfuscated.k71;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class k71<MessageType extends j71<MessageType, BuilderType>, BuilderType extends k71<MessageType, BuilderType>> implements x91 {
    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public final /* synthetic */ x91 a(w91 w91) {
        if (b().getClass().isInstance(w91)) {
            a((j71) w91);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
