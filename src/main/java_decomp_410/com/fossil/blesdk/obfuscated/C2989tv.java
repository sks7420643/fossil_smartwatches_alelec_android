package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tv */
public class C2989tv implements com.bumptech.glide.request.RequestCoordinator, com.fossil.blesdk.obfuscated.C2604ov {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.bumptech.glide.request.RequestCoordinator f9765a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.Object f9766b;

    @DexIgnore
    /* renamed from: c */
    public volatile com.fossil.blesdk.obfuscated.C2604ov f9767c;

    @DexIgnore
    /* renamed from: d */
    public volatile com.fossil.blesdk.obfuscated.C2604ov f9768d;

    @DexIgnore
    /* renamed from: e */
    public com.bumptech.glide.request.RequestCoordinator.RequestState f9769e;

    @DexIgnore
    /* renamed from: f */
    public com.bumptech.glide.request.RequestCoordinator.RequestState f9770f;

    @DexIgnore
    /* renamed from: g */
    public boolean f9771g;

    @DexIgnore
    public C2989tv(java.lang.Object obj, com.bumptech.glide.request.RequestCoordinator requestCoordinator) {
        com.bumptech.glide.request.RequestCoordinator.RequestState requestState = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
        this.f9769e = requestState;
        this.f9770f = requestState;
        this.f9766b = obj;
        this.f9765a = requestCoordinator;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16566a(com.fossil.blesdk.obfuscated.C2604ov ovVar, com.fossil.blesdk.obfuscated.C2604ov ovVar2) {
        this.f9767c = ovVar;
        this.f9768d = ovVar2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: b */
    public void mo4019b(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        synchronized (this.f9766b) {
            if (!ovVar.equals(this.f9767c)) {
                this.f9770f = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED;
                return;
            }
            this.f9769e = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED;
            if (this.f9765a != null) {
                this.f9765a.mo4019b(this);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo4020c(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f9766b) {
            z = mo16569h() && ovVar.equals(this.f9767c) && !mo16567b();
        }
        return z;
    }

    @DexIgnore
    public void clear() {
        synchronized (this.f9766b) {
            this.f9771g = false;
            this.f9769e = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
            this.f9770f = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
            this.f9768d.clear();
            this.f9767c.clear();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo4021d(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f9766b) {
            z = mo16570i() && (ovVar.equals(this.f9767c) || this.f9769e != com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS);
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: e */
    public void mo4022e(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        synchronized (this.f9766b) {
            if (ovVar.equals(this.f9768d)) {
                this.f9770f = com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS;
                return;
            }
            this.f9769e = com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS;
            if (this.f9765a != null) {
                this.f9765a.mo4022e(this);
            }
            if (!this.f9770f.isComplete()) {
                this.f9768d.clear();
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo4023f(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f9766b) {
            z = mo16568g() && ovVar.equals(this.f9767c) && this.f9769e != com.bumptech.glide.request.RequestCoordinator.RequestState.PAUSED;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: g */
    public final boolean mo16568g() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f9765a;
        return requestCoordinator == null || requestCoordinator.mo4023f(this);
    }

    @DexIgnore
    /* renamed from: h */
    public final boolean mo16569h() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f9765a;
        return requestCoordinator == null || requestCoordinator.mo4020c(this);
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo16570i() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f9765a;
        return requestCoordinator == null || requestCoordinator.mo4021d(this);
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.f9766b) {
            z = this.f9769e == com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: j */
    public final boolean mo16571j() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f9765a;
        return requestCoordinator != null && requestCoordinator.mo4018a();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo4018a() {
        boolean z;
        synchronized (this.f9766b) {
            if (!mo16571j()) {
                if (!mo16567b()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo4035c() {
        synchronized (this.f9766b) {
            this.f9771g = true;
            try {
                if (!(this.f9769e == com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS || this.f9770f == com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING)) {
                    this.f9770f = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING;
                    this.f9768d.mo4035c();
                }
                if (this.f9771g && this.f9769e != com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                    this.f9769e = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING;
                    this.f9767c.mo4035c();
                }
            } finally {
                this.f9771g = false;
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo4037d() {
        synchronized (this.f9766b) {
            if (!this.f9770f.isComplete()) {
                this.f9770f = com.bumptech.glide.request.RequestCoordinator.RequestState.PAUSED;
                this.f9768d.mo4037d();
            }
            if (!this.f9769e.isComplete()) {
                this.f9769e = com.bumptech.glide.request.RequestCoordinator.RequestState.PAUSED;
                this.f9767c.mo4037d();
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo4039f() {
        boolean z;
        synchronized (this.f9766b) {
            z = this.f9769e == com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: a */
    public boolean mo4033a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        if (!(ovVar instanceof com.fossil.blesdk.obfuscated.C2989tv)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2989tv tvVar = (com.fossil.blesdk.obfuscated.C2989tv) ovVar;
        if (this.f9767c == null) {
            if (tvVar.f9767c != null) {
                return false;
            }
        } else if (!this.f9767c.mo4033a(tvVar.f9767c)) {
            return false;
        }
        if (this.f9768d == null) {
            if (tvVar.f9768d == null) {
                return true;
            }
            return false;
        } else if (!this.f9768d.mo4033a(tvVar.f9768d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo16567b() {
        boolean z;
        synchronized (this.f9766b) {
            if (this.f9769e != com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS) {
                if (this.f9770f != com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo4038e() {
        boolean z;
        synchronized (this.f9766b) {
            z = this.f9769e == com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }
}
