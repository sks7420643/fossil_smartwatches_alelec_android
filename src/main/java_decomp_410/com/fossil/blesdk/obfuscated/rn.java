package com.fossil.blesdk.obfuscated;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import com.bumptech.glide.GeneratedAppGlideModule;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.blesdk.obfuscated.as;
import com.fossil.blesdk.obfuscated.bs;
import com.fossil.blesdk.obfuscated.cs;
import com.fossil.blesdk.obfuscated.ds;
import com.fossil.blesdk.obfuscated.es;
import com.fossil.blesdk.obfuscated.fr;
import com.fossil.blesdk.obfuscated.fs;
import com.fossil.blesdk.obfuscated.gr;
import com.fossil.blesdk.obfuscated.gs;
import com.fossil.blesdk.obfuscated.hs;
import com.fossil.blesdk.obfuscated.ir;
import com.fossil.blesdk.obfuscated.jr;
import com.fossil.blesdk.obfuscated.jt;
import com.fossil.blesdk.obfuscated.kr;
import com.fossil.blesdk.obfuscated.pr;
import com.fossil.blesdk.obfuscated.to;
import com.fossil.blesdk.obfuscated.xr;
import com.fossil.blesdk.obfuscated.zo;
import com.fossil.blesdk.obfuscated.zr;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rn implements ComponentCallbacks2 {
    @DexIgnore
    public static volatile rn m;
    @DexIgnore
    public static volatile boolean n;
    @DexIgnore
    public /* final */ jq e;
    @DexIgnore
    public /* final */ ar f;
    @DexIgnore
    public /* final */ tn g;
    @DexIgnore
    public /* final */ Registry h;
    @DexIgnore
    public /* final */ gq i;
    @DexIgnore
    public /* final */ uu j;
    @DexIgnore
    public /* final */ mu k;
    @DexIgnore
    public /* final */ List<xn> l; // = new ArrayList();

    @DexIgnore
    public interface a {
        @DexIgnore
        rv build();
    }

    @DexIgnore
    public rn(Context context, qp qpVar, ar arVar, jq jqVar, gq gqVar, uu uuVar, mu muVar, int i2, a aVar, Map<Class<?>, yn<?, ?>> map, List<qv<Object>> list, boolean z, boolean z2, int i3, int i4) {
        mo moVar;
        mo moVar2;
        Context context2 = context;
        jq jqVar2 = jqVar;
        gq gqVar2 = gqVar;
        Class<co> cls = co.class;
        Class<byte[]> cls2 = byte[].class;
        MemoryCategory memoryCategory = MemoryCategory.NORMAL;
        this.e = jqVar2;
        this.i = gqVar2;
        this.f = arVar;
        this.j = uuVar;
        this.k = muVar;
        Resources resources = context.getResources();
        this.h = new Registry();
        this.h.a((ImageHeaderParser) new vs());
        if (Build.VERSION.SDK_INT >= 27) {
            this.h.a((ImageHeaderParser) new zs());
        }
        List<ImageHeaderParser> a2 = this.h.a();
        st stVar = new st(context2, a2, jqVar2, gqVar2);
        mo<ParcelFileDescriptor, Bitmap> b = it.b(jqVar);
        if (!z2 || Build.VERSION.SDK_INT < 28) {
            ws wsVar = new ws(this.h.a(), resources.getDisplayMetrics(), jqVar2, gqVar2);
            moVar = new rs(wsVar);
            moVar2 = new ft(wsVar, gqVar2);
        } else {
            moVar2 = new ct();
            moVar = new ss();
        }
        ot otVar = new ot(context2);
        xr.c cVar = new xr.c(resources);
        xr.d dVar = new xr.d(resources);
        xr.b bVar = new xr.b(resources);
        Class<byte[]> cls3 = cls2;
        xr.a aVar2 = new xr.a(resources);
        ns nsVar = new ns(gqVar2);
        xr.a aVar3 = aVar2;
        cu cuVar = new cu();
        fu fuVar = new fu();
        ContentResolver contentResolver = context.getContentResolver();
        Registry registry = this.h;
        registry.a(ByteBuffer.class, new hr());
        registry.a(InputStream.class, new yr(gqVar2));
        registry.a("Bitmap", ByteBuffer.class, Bitmap.class, moVar);
        registry.a("Bitmap", InputStream.class, Bitmap.class, moVar2);
        registry.a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, b);
        registry.a("Bitmap", AssetFileDescriptor.class, Bitmap.class, it.a(jqVar));
        registry.a(Bitmap.class, Bitmap.class, as.a.a());
        registry.a("Bitmap", Bitmap.class, Bitmap.class, new ht());
        registry.a(Bitmap.class, nsVar);
        registry.a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new ls(resources, moVar));
        registry.a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new ls(resources, moVar2));
        registry.a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new ls(resources, b));
        registry.a(BitmapDrawable.class, new ms(jqVar2, nsVar));
        registry.a("Gif", InputStream.class, ut.class, new bu(a2, stVar, gqVar2));
        registry.a("Gif", ByteBuffer.class, ut.class, stVar);
        registry.a(ut.class, new vt());
        registry.a(cls, cls, as.a.a());
        registry.a("Bitmap", cls, Bitmap.class, new zt(jqVar2));
        registry.a(Uri.class, Drawable.class, otVar);
        registry.a(Uri.class, Bitmap.class, new et(otVar, jqVar2));
        registry.a((to.a<?>) new jt.a());
        registry.a(File.class, ByteBuffer.class, new ir.b());
        registry.a(File.class, InputStream.class, new kr.e());
        registry.a(File.class, File.class, new qt());
        registry.a(File.class, ParcelFileDescriptor.class, new kr.b());
        registry.a(File.class, File.class, as.a.a());
        registry.a((to.a<?>) new zo.a(gqVar2));
        xr.c cVar2 = cVar;
        registry.a(Integer.TYPE, InputStream.class, cVar2);
        xr.b bVar2 = bVar;
        registry.a(Integer.TYPE, ParcelFileDescriptor.class, bVar2);
        registry.a(Integer.class, InputStream.class, cVar2);
        registry.a(Integer.class, ParcelFileDescriptor.class, bVar2);
        xr.d dVar2 = dVar;
        registry.a(Integer.class, Uri.class, dVar2);
        xr.a aVar4 = aVar3;
        registry.a(Integer.TYPE, AssetFileDescriptor.class, aVar4);
        registry.a(Integer.class, AssetFileDescriptor.class, aVar4);
        registry.a(Integer.TYPE, Uri.class, dVar2);
        registry.a(String.class, InputStream.class, new jr.c());
        registry.a(Uri.class, InputStream.class, new jr.c());
        registry.a(String.class, InputStream.class, new zr.c());
        registry.a(String.class, ParcelFileDescriptor.class, new zr.b());
        registry.a(String.class, AssetFileDescriptor.class, new zr.a());
        registry.a(Uri.class, InputStream.class, new es.a());
        registry.a(Uri.class, InputStream.class, new fr.c(context.getAssets()));
        registry.a(Uri.class, ParcelFileDescriptor.class, new fr.b(context.getAssets()));
        Context context3 = context;
        registry.a(Uri.class, InputStream.class, new fs.a(context3));
        registry.a(Uri.class, InputStream.class, new gs.a(context3));
        ContentResolver contentResolver2 = contentResolver;
        registry.a(Uri.class, InputStream.class, new bs.d(contentResolver2));
        registry.a(Uri.class, ParcelFileDescriptor.class, new bs.b(contentResolver2));
        registry.a(Uri.class, AssetFileDescriptor.class, new bs.a(contentResolver2));
        registry.a(Uri.class, InputStream.class, new cs.a());
        registry.a(URL.class, InputStream.class, new hs.a());
        registry.a(Uri.class, File.class, new pr.a(context3));
        registry.a(lr.class, InputStream.class, new ds.a());
        Class<byte[]> cls4 = cls3;
        registry.a(cls4, ByteBuffer.class, new gr.a());
        registry.a(cls4, InputStream.class, new gr.d());
        registry.a(Uri.class, Uri.class, as.a.a());
        registry.a(Drawable.class, Drawable.class, as.a.a());
        registry.a(Drawable.class, Drawable.class, new pt());
        registry.a(Bitmap.class, BitmapDrawable.class, new du(resources));
        cu cuVar2 = cuVar;
        registry.a(Bitmap.class, cls4, cuVar2);
        fu fuVar2 = fuVar;
        registry.a(Drawable.class, cls4, new eu(jqVar2, cuVar2, fuVar2));
        registry.a(ut.class, cls4, fuVar2);
        Context context4 = context;
        gq gqVar3 = gqVar;
        this.g = new tn(context4, gqVar3, this.h, new zv(), aVar, map, list, qpVar, z, i2);
    }

    @DexIgnore
    public static rn a(Context context) {
        if (m == null) {
            GeneratedAppGlideModule b = b(context.getApplicationContext());
            synchronized (rn.class) {
                if (m == null) {
                    a(context, b);
                }
            }
        }
        return m;
    }

    @DexIgnore
    public static void b(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        a(context, new sn(), generatedAppGlideModule);
    }

    @DexIgnore
    public jq c() {
        return this.e;
    }

    @DexIgnore
    public mu d() {
        return this.k;
    }

    @DexIgnore
    public Context e() {
        return this.g.getBaseContext();
    }

    @DexIgnore
    public tn f() {
        return this.g;
    }

    @DexIgnore
    public Registry g() {
        return this.h;
    }

    @DexIgnore
    public uu h() {
        return this.j;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        a();
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        a(i2);
    }

    @DexIgnore
    public static GeneratedAppGlideModule b(Context context) {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new Class[]{Context.class}).newInstance(new Object[]{context.getApplicationContext()});
        } catch (ClassNotFoundException unused) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e2) {
            a((Exception) e2);
            throw null;
        } catch (IllegalAccessException e3) {
            a((Exception) e3);
            throw null;
        } catch (NoSuchMethodException e4) {
            a((Exception) e4);
            throw null;
        } catch (InvocationTargetException e5) {
            a((Exception) e5);
            throw null;
        }
    }

    @DexIgnore
    public static uu c(Context context) {
        tw.a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return a(context).h();
    }

    @DexIgnore
    public static xn d(Context context) {
        return c(context).a(context);
    }

    @DexIgnore
    public static void a(Context context, GeneratedAppGlideModule generatedAppGlideModule) {
        if (!n) {
            n = true;
            b(context, generatedAppGlideModule);
            n = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    public gq b() {
        return this.i;
    }

    @DexIgnore
    public void b(xn xnVar) {
        synchronized (this.l) {
            if (this.l.contains(xnVar)) {
                this.l.remove(xnVar);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    @DexIgnore
    public static void a(Context context, sn snVar, GeneratedAppGlideModule generatedAppGlideModule) {
        Context applicationContext = context.getApplicationContext();
        List<bv> emptyList = Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.a()) {
            emptyList = new dv(applicationContext).a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.b().isEmpty()) {
            Set<Class<?>> b = generatedAppGlideModule.b();
            Iterator<bv> it = emptyList.iterator();
            while (it.hasNext()) {
                bv next = it.next();
                if (b.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (bv bvVar : emptyList) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + bvVar.getClass());
            }
        }
        snVar.a(generatedAppGlideModule != null ? generatedAppGlideModule.c() : null);
        for (bv a2 : emptyList) {
            a2.a(applicationContext, snVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, snVar);
        }
        rn a3 = snVar.a(applicationContext);
        for (bv next2 : emptyList) {
            try {
                next2.a(applicationContext, a3, a3.h);
            } catch (AbstractMethodError e2) {
                throw new IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + next2.getClass().getName(), e2);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.a(applicationContext, a3, a3.h);
        }
        applicationContext.registerComponentCallbacks(a3);
        m = a3;
    }

    @DexIgnore
    public static void a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    public void a() {
        uw.b();
        this.f.a();
        this.e.a();
        this.i.a();
    }

    @DexIgnore
    public void a(int i2) {
        uw.b();
        for (xn onTrimMemory : this.l) {
            onTrimMemory.onTrimMemory(i2);
        }
        this.f.a(i2);
        this.e.a(i2);
        this.i.a(i2);
    }

    @DexIgnore
    public static xn a(Fragment fragment) {
        return c(fragment.getContext()).a(fragment);
    }

    @DexIgnore
    public static xn a(View view) {
        return c(view.getContext()).a(view);
    }

    @DexIgnore
    public boolean a(bw<?> bwVar) {
        synchronized (this.l) {
            for (xn b : this.l) {
                if (b.b(bwVar)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    public void a(xn xnVar) {
        synchronized (this.l) {
            if (!this.l.contains(xnVar)) {
                this.l.add(xnVar);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
