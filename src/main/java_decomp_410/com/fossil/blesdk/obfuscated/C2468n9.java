package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n9 */
public class C2468n9 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f7688a;

    @DexIgnore
    public C2468n9(java.lang.Object obj) {
        this.f7688a = obj;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2468n9 mo13874a() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return new com.fossil.blesdk.obfuscated.C2468n9(((android.view.WindowInsets) this.f7688a).consumeSystemWindowInsets());
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo13876b() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((android.view.WindowInsets) this.f7688a).getSystemWindowInsetBottom();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo13877c() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((android.view.WindowInsets) this.f7688a).getSystemWindowInsetLeft();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo13878d() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((android.view.WindowInsets) this.f7688a).getSystemWindowInsetRight();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: e */
    public int mo13879e() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((android.view.WindowInsets) this.f7688a).getSystemWindowInsetTop();
        }
        return 0;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C2468n9.class != obj.getClass()) {
            return false;
        }
        java.lang.Object obj2 = this.f7688a;
        java.lang.Object obj3 = ((com.fossil.blesdk.obfuscated.C2468n9) obj).f7688a;
        if (obj2 != null) {
            return obj2.equals(obj3);
        }
        if (obj3 == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo13881f() {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((android.view.WindowInsets) this.f7688a).hasSystemWindowInsets();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo13882g() {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return ((android.view.WindowInsets) this.f7688a).isConsumed();
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        java.lang.Object obj = this.f7688a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2468n9 mo13875a(int i, int i2, int i3, int i4) {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return new com.fossil.blesdk.obfuscated.C2468n9(((android.view.WindowInsets) this.f7688a).replaceSystemWindowInsets(i, i2, i3, i4));
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2468n9 m11097a(java.lang.Object obj) {
        if (obj == null) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2468n9(obj);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m11098a(com.fossil.blesdk.obfuscated.C2468n9 n9Var) {
        if (n9Var == null) {
            return null;
        }
        return n9Var.f7688a;
    }
}
