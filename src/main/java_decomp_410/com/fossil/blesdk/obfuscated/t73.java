package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t73 implements Factory<zc3> {
    @DexIgnore
    public static zc3 a(n73 n73) {
        zc3 f = n73.f();
        n44.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
