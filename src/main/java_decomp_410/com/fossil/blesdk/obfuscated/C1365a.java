package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a */
public interface C1365a extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a$a */
    public static abstract class C1366a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C1365a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.a$a$a */
        public static class C1367a implements com.fossil.blesdk.obfuscated.C1365a {

            @DexIgnore
            /* renamed from: e */
            public android.os.IBinder f3274e;

            @DexIgnore
            public C1367a(android.os.IBinder iBinder) {
                this.f3274e = iBinder;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo8354a(java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3274e.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public android.os.IBinder asBinder() {
                return this.f3274e;
            }

            @DexIgnore
            /* renamed from: d */
            public void mo8355d(android.os.Bundle bundle) throws android.os.RemoteException {
                android.os.Parcel obtain = android.os.Parcel.obtain();
                android.os.Parcel obtain2 = android.os.Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f3274e.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C1365a m4085a(android.os.IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            android.os.IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.customtabs.ICustomTabsCallback");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof com.fossil.blesdk.obfuscated.C1365a)) {
                return new com.fossil.blesdk.obfuscated.C1365a.C1366a.C1367a(iBinder);
            }
            return (com.fossil.blesdk.obfuscated.C1365a) queryLocalInterface;
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo8354a(java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: d */
    void mo8355d(android.os.Bundle bundle) throws android.os.RemoteException;
}
