package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n14 implements p24 {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ g14 c;

    @DexIgnore
    public n14(g14 g14, List list, boolean z) {
        this.c = g14;
        this.a = list;
        this.b = z;
    }

    @DexIgnore
    public void a() {
        j04.c();
        this.c.a((List<q14>) this.a, this.b, true);
    }

    @DexIgnore
    public void b() {
        j04.d();
        this.c.a((List<q14>) this.a, 1, this.b, true);
    }
}
