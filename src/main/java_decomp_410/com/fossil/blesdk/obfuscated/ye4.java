package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ye4<T, R> implements re4<R> {
    @DexIgnore
    public /* final */ re4<T> a;
    @DexIgnore
    public /* final */ xc4<T, R> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<R>, rd4 {
        @DexIgnore
        public /* final */ Iterator<T> e;
        @DexIgnore
        public /* final */ /* synthetic */ ye4 f;

        @DexIgnore
        public a(ye4 ye4) {
            this.f = ye4;
            this.e = ye4.a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.e.hasNext();
        }

        @DexIgnore
        public R next() {
            return this.f.b.invoke(this.e.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public ye4(re4<? extends T> re4, xc4<? super T, ? extends R> xc4) {
        kd4.b(re4, "sequence");
        kd4.b(xc4, "transformer");
        this.a = re4;
        this.b = xc4;
    }

    @DexIgnore
    public Iterator<R> iterator() {
        return new a(this);
    }
}
