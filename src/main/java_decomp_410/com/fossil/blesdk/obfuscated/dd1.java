package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dd1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<dd1> CREATOR; // = new ed1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public dd1(String str, String str2, String str3) {
        this.g = str;
        this.e = str2;
        this.f = str3;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e, false);
        kk0.a(parcel, 2, this.f, false);
        kk0.a(parcel, 5, this.g, false);
        kk0.a(parcel, a);
    }
}
