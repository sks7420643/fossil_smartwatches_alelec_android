package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z03 {
    @DexIgnore
    public /* final */ x03 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> c;

    @DexIgnore
    public z03(x03 x03, int i, ArrayList<ContactWrapper> arrayList) {
        kd4.b(x03, "mView");
        this.a = x03;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final ArrayList<ContactWrapper> a() {
        ArrayList<ContactWrapper> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final x03 c() {
        return this.a;
    }
}
