package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g5 */
public class C1857g5 {

    @DexIgnore
    /* renamed from: a */
    public java.util.HashSet<com.fossil.blesdk.obfuscated.C1857g5> f5383a; // = new java.util.HashSet<>(2);

    @DexIgnore
    /* renamed from: b */
    public int f5384b; // = 0;

    @DexIgnore
    /* renamed from: a */
    public void mo11138a(com.fossil.blesdk.obfuscated.C1857g5 g5Var) {
        this.f5383a.add(g5Var);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11139b() {
        this.f5384b = 0;
        java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it = this.f5383a.iterator();
        while (it.hasNext()) {
            it.next().mo11139b();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo11140c() {
        return this.f5384b == 1;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10332d() {
        this.f5384b = 0;
        this.f5383a.clear();
    }

    @DexIgnore
    /* renamed from: e */
    public void mo10333e() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11137a() {
        this.f5384b = 1;
        java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it = this.f5383a.iterator();
        while (it.hasNext()) {
            it.next().mo10333e();
        }
    }
}
