package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i63 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        hybridCustomizeEditActivity.B = hybridCustomizeEditPresenter;
    }

    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, j42 j42) {
        hybridCustomizeEditActivity.C = j42;
    }
}
