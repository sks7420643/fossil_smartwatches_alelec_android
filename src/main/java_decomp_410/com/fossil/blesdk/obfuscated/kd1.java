package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kd1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<kd1> CREATOR; // = new ld1();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f;
    @DexIgnore
    public float g;
    @DexIgnore
    public long h;
    @DexIgnore
    public int i;

    @DexIgnore
    public kd1() {
        this(true, 50, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD, Integer.MAX_VALUE);
    }

    @DexIgnore
    public kd1(boolean z, long j, float f2, long j2, int i2) {
        this.e = z;
        this.f = j;
        this.g = f2;
        this.h = j2;
        this.i = i2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof kd1)) {
            return false;
        }
        kd1 kd1 = (kd1) obj;
        return this.e == kd1.e && this.f == kd1.f && Float.compare(this.g, kd1.g) == 0 && this.h == kd1.h && this.i == kd1.i;
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(Boolean.valueOf(this.e), Long.valueOf(this.f), Float.valueOf(this.g), Long.valueOf(this.h), Integer.valueOf(this.i));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=");
        sb.append(this.e);
        sb.append(" mMinimumSamplingPeriodMs=");
        sb.append(this.f);
        sb.append(" mSmallestAngleChangeRadians=");
        sb.append(this.g);
        long j = this.h;
        if (j != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
            sb.append(" expireIn=");
            sb.append(j - SystemClock.elapsedRealtime());
            sb.append("ms");
        }
        if (this.i != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.i);
        }
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f);
        kk0.a(parcel, 3, this.g);
        kk0.a(parcel, 4, this.h);
        kk0.a(parcel, 5, this.i);
        kk0.a(parcel, a);
    }
}
