package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import com.fossil.blesdk.obfuscated.sn0;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class un0<T> extends sn0.a {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public un0(T t) {
        this.e = t;
    }

    @DexIgnore
    public static <T> sn0 a(T t) {
        return new un0(t);
    }

    @DexIgnore
    public static <T> T d(sn0 sn0) {
        if (sn0 instanceof un0) {
            return ((un0) sn0).e;
        }
        IBinder asBinder = sn0.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i != 1) {
            int length = declaredFields.length;
            StringBuilder sb = new StringBuilder(64);
            sb.append("Unexpected number of IObjectWrapper declared fields: ");
            sb.append(length);
            throw new IllegalArgumentException(sb.toString());
        } else if (!field.isAccessible()) {
            field.setAccessible(true);
            try {
                return field.get(asBinder);
            } catch (NullPointerException e2) {
                throw new IllegalArgumentException("Binder object is null.", e2);
            } catch (IllegalAccessException e3) {
                throw new IllegalArgumentException("Could not access the field in remoteBinder.", e3);
            }
        } else {
            throw new IllegalArgumentException("IObjectWrapper declared field not private!");
        }
    }
}
