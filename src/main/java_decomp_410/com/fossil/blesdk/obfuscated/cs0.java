package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.qb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cs0 extends oj0<ds0> {
    @DexIgnore
    public /* final */ qb0.a E;

    @DexIgnore
    public cs0(Context context, Looper looper, kj0 kj0, qb0.a aVar, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 68, kj0, bVar, cVar);
        this.E = aVar;
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof ds0) {
            return (ds0) queryLocalInterface;
        }
        return new es0(iBinder);
    }

    @DexIgnore
    public final int i() {
        return 12800000;
    }

    @DexIgnore
    public final Bundle u() {
        qb0.a aVar = this.E;
        return aVar == null ? new Bundle() : aVar.a();
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
