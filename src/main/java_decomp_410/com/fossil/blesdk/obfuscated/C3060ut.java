package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ut */
public class C3060ut extends android.graphics.drawable.Drawable implements com.fossil.blesdk.obfuscated.C3383yt.C3385b, android.graphics.drawable.Animatable, com.fossil.blesdk.obfuscated.C2327li {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3060ut.C3061a f10051e;

    @DexIgnore
    /* renamed from: f */
    public boolean f10052f;

    @DexIgnore
    /* renamed from: g */
    public boolean f10053g;

    @DexIgnore
    /* renamed from: h */
    public boolean f10054h;

    @DexIgnore
    /* renamed from: i */
    public boolean f10055i;

    @DexIgnore
    /* renamed from: j */
    public int f10056j;

    @DexIgnore
    /* renamed from: k */
    public int f10057k;

    @DexIgnore
    /* renamed from: l */
    public boolean f10058l;

    @DexIgnore
    /* renamed from: m */
    public android.graphics.Paint f10059m;

    @DexIgnore
    /* renamed from: n */
    public android.graphics.Rect f10060n;

    @DexIgnore
    /* renamed from: o */
    public java.util.List<com.fossil.blesdk.obfuscated.C2327li.C2328a> f10061o;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ut$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ut$a */
    public static final class C3061a extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3383yt f10062a;

        @DexIgnore
        public C3061a(com.fossil.blesdk.obfuscated.C3383yt ytVar) {
            this.f10062a = ytVar;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return newDrawable();
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            return new com.fossil.blesdk.obfuscated.C3060ut(this);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public C3060ut(android.content.Context context, com.fossil.blesdk.obfuscated.C1570co coVar, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar, int i, int i2, android.graphics.Bitmap bitmap) {
        this(new com.fossil.blesdk.obfuscated.C3060ut.C3061a(r1));
        com.fossil.blesdk.obfuscated.C3383yt ytVar = new com.fossil.blesdk.obfuscated.C3383yt(com.fossil.blesdk.obfuscated.C2815rn.m13272a(context), coVar, i, i2, ooVar, bitmap);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16857a(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar, android.graphics.Bitmap bitmap) {
        this.f10051e.f10062a.mo18303a(ooVar, bitmap);
    }

    @DexIgnore
    /* renamed from: b */
    public final android.graphics.drawable.Drawable.Callback mo16858b() {
        android.graphics.drawable.Drawable.Callback callback = getCallback();
        while (callback instanceof android.graphics.drawable.Drawable) {
            callback = ((android.graphics.drawable.Drawable) callback).getCallback();
        }
        return callback;
    }

    @DexIgnore
    /* renamed from: c */
    public java.nio.ByteBuffer mo16859c() {
        return this.f10051e.f10062a.mo18306b();
    }

    @DexIgnore
    /* renamed from: d */
    public final android.graphics.Rect mo16860d() {
        if (this.f10060n == null) {
            this.f10060n = new android.graphics.Rect();
        }
        return this.f10060n;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        if (!this.f10054h) {
            if (this.f10058l) {
                android.view.Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), mo16860d());
                this.f10058l = false;
            }
            canvas.drawBitmap(this.f10051e.f10062a.mo18308c(), (android.graphics.Rect) null, mo16860d(), mo16869h());
        }
    }

    @DexIgnore
    /* renamed from: e */
    public android.graphics.Bitmap mo16862e() {
        return this.f10051e.f10062a.mo18310e();
    }

    @DexIgnore
    /* renamed from: f */
    public int mo16863f() {
        return this.f10051e.f10062a.mo18311f();
    }

    @DexIgnore
    /* renamed from: g */
    public int mo16864g() {
        return this.f10051e.f10062a.mo18309d();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable.ConstantState getConstantState() {
        return this.f10051e;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f10051e.f10062a.mo18312g();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.f10051e.f10062a.mo18314i();
    }

    @DexIgnore
    public int getOpacity() {
        return -2;
    }

    @DexIgnore
    /* renamed from: h */
    public final android.graphics.Paint mo16869h() {
        if (this.f10059m == null) {
            this.f10059m = new android.graphics.Paint(2);
        }
        return this.f10059m;
    }

    @DexIgnore
    /* renamed from: i */
    public int mo16870i() {
        return this.f10051e.f10062a.mo18313h();
    }

    @DexIgnore
    public boolean isRunning() {
        return this.f10052f;
    }

    @DexIgnore
    /* renamed from: j */
    public final void mo16872j() {
        java.util.List<com.fossil.blesdk.obfuscated.C2327li.C2328a> list = this.f10061o;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                this.f10061o.get(i).mo13295a(this);
            }
        }
    }

    @DexIgnore
    /* renamed from: k */
    public void mo16873k() {
        this.f10054h = true;
        this.f10051e.f10062a.mo18302a();
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo16874l() {
        this.f10056j = 0;
    }

    @DexIgnore
    /* renamed from: m */
    public final void mo16875m() {
        com.fossil.blesdk.obfuscated.C2992tw.m14461a(!this.f10054h, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.f10051e.f10062a.mo18311f() == 1) {
            invalidateSelf();
        } else if (!this.f10052f) {
            this.f10052f = true;
            this.f10051e.f10062a.mo18305a((com.fossil.blesdk.obfuscated.C3383yt.C3385b) this);
            invalidateSelf();
        }
    }

    @DexIgnore
    /* renamed from: n */
    public final void mo16876n() {
        this.f10052f = false;
        this.f10051e.f10062a.mo18307b(this);
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        super.onBoundsChange(rect);
        this.f10058l = true;
    }

    @DexIgnore
    public void setAlpha(int i) {
        mo16869h().setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        mo16869h().setColorFilter(colorFilter);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        com.fossil.blesdk.obfuscated.C2992tw.m14461a(!this.f10054h, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.f10055i = z;
        if (!z) {
            mo16876n();
        } else if (this.f10053g) {
            mo16875m();
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        this.f10053g = true;
        mo16874l();
        if (this.f10055i) {
            mo16875m();
        }
    }

    @DexIgnore
    public void stop() {
        this.f10053g = false;
        mo16876n();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16856a() {
        if (mo16858b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (mo16864g() == mo16863f() - 1) {
            this.f10056j++;
        }
        int i = this.f10057k;
        if (i != -1 && this.f10056j >= i) {
            mo16872j();
            stop();
        }
    }

    @DexIgnore
    public C3060ut(com.fossil.blesdk.obfuscated.C3060ut.C3061a aVar) {
        this.f10055i = true;
        this.f10057k = -1;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(aVar);
        this.f10051e = aVar;
    }
}
