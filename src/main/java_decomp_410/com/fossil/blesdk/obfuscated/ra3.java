package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ra3 implements Factory<CaloriesOverviewMonthPresenter> {
    @DexIgnore
    public static CaloriesOverviewMonthPresenter a(pa3 pa3, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new CaloriesOverviewMonthPresenter(pa3, userRepository, summariesRepository);
    }
}
