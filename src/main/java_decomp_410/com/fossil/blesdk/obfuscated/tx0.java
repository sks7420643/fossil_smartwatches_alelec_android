package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tx0 implements Cloneable {
    @DexIgnore
    public rx0<?, ?> e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public List<Object> g; // = new ArrayList();

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        if (this.f == null) {
            Iterator<Object> it = this.g.iterator();
            if (it.hasNext()) {
                it.next();
                throw new NoSuchMethodError();
            }
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final byte[] a() throws IOException {
        byte[] bArr = new byte[b()];
        a(px0.a(bArr));
        return bArr;
    }

    @DexIgnore
    public final int b() {
        if (this.f == null) {
            Iterator<Object> it = this.g.iterator();
            if (!it.hasNext()) {
                return 0;
            }
            it.next();
            throw new NoSuchMethodError();
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    /* renamed from: c */
    public final tx0 clone() {
        Object clone;
        tx0 tx0 = new tx0();
        try {
            tx0.e = this.e;
            if (this.g == null) {
                tx0.g = null;
            } else {
                tx0.g.addAll(this.g);
            }
            if (this.f != null) {
                if (this.f instanceof vx0) {
                    clone = (vx0) ((vx0) this.f).clone();
                } else if (this.f instanceof byte[]) {
                    clone = ((byte[]) this.f).clone();
                } else {
                    int i = 0;
                    if (this.f instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f;
                        byte[][] bArr2 = new byte[bArr.length][];
                        tx0.f = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f instanceof boolean[]) {
                        clone = ((boolean[]) this.f).clone();
                    } else if (this.f instanceof int[]) {
                        clone = ((int[]) this.f).clone();
                    } else if (this.f instanceof long[]) {
                        clone = ((long[]) this.f).clone();
                    } else if (this.f instanceof float[]) {
                        clone = ((float[]) this.f).clone();
                    } else if (this.f instanceof double[]) {
                        clone = ((double[]) this.f).clone();
                    } else if (this.f instanceof vx0[]) {
                        vx0[] vx0Arr = (vx0[]) this.f;
                        vx0[] vx0Arr2 = new vx0[vx0Arr.length];
                        tx0.f = vx0Arr2;
                        while (i < vx0Arr.length) {
                            vx0Arr2[i] = (vx0) vx0Arr[i].clone();
                            i++;
                        }
                    }
                }
                tx0.f = clone;
            }
            return tx0;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof tx0)) {
            return false;
        }
        tx0 tx0 = (tx0) obj;
        if (this.f == null || tx0.f == null) {
            List<Object> list = this.g;
            if (list != null) {
                List<Object> list2 = tx0.g;
                if (list2 != null) {
                    return list.equals(list2);
                }
            }
            try {
                return Arrays.equals(a(), tx0.a());
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        } else {
            rx0<?, ?> rx0 = this.e;
            if (rx0 != tx0.e) {
                return false;
            }
            if (!rx0.a.isArray()) {
                return this.f.equals(tx0.f);
            }
            Object obj2 = this.f;
            return obj2 instanceof byte[] ? Arrays.equals((byte[]) obj2, (byte[]) tx0.f) : obj2 instanceof int[] ? Arrays.equals((int[]) obj2, (int[]) tx0.f) : obj2 instanceof long[] ? Arrays.equals((long[]) obj2, (long[]) tx0.f) : obj2 instanceof float[] ? Arrays.equals((float[]) obj2, (float[]) tx0.f) : obj2 instanceof double[] ? Arrays.equals((double[]) obj2, (double[]) tx0.f) : obj2 instanceof boolean[] ? Arrays.equals((boolean[]) obj2, (boolean[]) tx0.f) : Arrays.deepEquals((Object[]) obj2, (Object[]) tx0.f);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return Arrays.hashCode(a()) + 527;
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }
}
