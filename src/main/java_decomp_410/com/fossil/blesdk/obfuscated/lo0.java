package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lo0 extends IInterface {
    @DexIgnore
    int a(sn0 sn0, String str, boolean z) throws RemoteException;

    @DexIgnore
    sn0 a(sn0 sn0, String str, int i) throws RemoteException;

    @DexIgnore
    int b(sn0 sn0, String str, boolean z) throws RemoteException;

    @DexIgnore
    sn0 b(sn0 sn0, String str, int i) throws RemoteException;

    @DexIgnore
    int n() throws RemoteException;
}
