package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vc3 extends zr2 implements uc3 {
    @DexIgnore
    public tr3<oc2> j;
    @DexIgnore
    public tc3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        this.j = new tr3<>(this, (oc2) qa.a(layoutInflater, R.layout.fragment_heartrate_overview_week, viewGroup, false, O0()));
        tr3<oc2> tr3 = this.j;
        if (tr3 != null) {
            oc2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        tc3 tc3 = this.k;
        if (tc3 != null) {
            tc3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        tc3 tc3 = this.k;
        if (tc3 != null) {
            tc3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(tc3 tc3) {
        kd4.b(tc3, "presenter");
        this.k = tc3;
    }

    @DexIgnore
    public void a(List<Integer> list, List<String> list2) {
        kd4.b(list, "data");
        kd4.b(list2, "listWeekDays");
        tr3<oc2> tr3 = this.j;
        if (tr3 != null) {
            oc2 a2 = tr3.a();
            if (a2 != null) {
                WeekHeartRateChart weekHeartRateChart = a2.q;
                if (weekHeartRateChart != null) {
                    weekHeartRateChart.setListWeekDays(list2);
                    weekHeartRateChart.a(list);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
