package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y64 implements z64 {
    @DexIgnore
    public /* final */ y44 a;
    @DexIgnore
    public b74 b;
    @DexIgnore
    public SSLSocketFactory c;
    @DexIgnore
    public boolean d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[HttpMethod.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[HttpMethod.GET.ordinal()] = 1;
            a[HttpMethod.POST.ordinal()] = 2;
            a[HttpMethod.PUT.ordinal()] = 3;
            try {
                a[HttpMethod.DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public y64() {
        this(new p44());
    }

    @DexIgnore
    public void a(b74 b74) {
        if (this.b != b74) {
            this.b = b74;
            c();
        }
    }

    @DexIgnore
    public final synchronized SSLSocketFactory b() {
        SSLSocketFactory a2;
        this.d = true;
        try {
            a2 = a74.a(this.b);
            this.a.d("Fabric", "Custom SSL pinning enabled");
        } catch (Exception e) {
            this.a.e("Fabric", "Exception while validating pinned certs", e);
            return null;
        }
        return a2;
    }

    @DexIgnore
    public final synchronized void c() {
        this.d = false;
        this.c = null;
    }

    @DexIgnore
    public y64(y44 y44) {
        this.a = y44;
    }

    @DexIgnore
    public HttpRequest a(HttpMethod httpMethod, String str, Map<String, String> map) {
        HttpRequest httpRequest;
        int i = a.a[httpMethod.ordinal()];
        if (i == 1) {
            httpRequest = HttpRequest.a((CharSequence) str, (Map<?, ?>) map, true);
        } else if (i == 2) {
            httpRequest = HttpRequest.b((CharSequence) str, (Map<?, ?>) map, true);
        } else if (i == 3) {
            httpRequest = HttpRequest.f((CharSequence) str);
        } else if (i == 4) {
            httpRequest = HttpRequest.b((CharSequence) str);
        } else {
            throw new IllegalArgumentException("Unsupported HTTP method!");
        }
        if (a(str) && this.b != null) {
            SSLSocketFactory a2 = a();
            if (a2 != null) {
                ((HttpsURLConnection) httpRequest.l()).setSSLSocketFactory(a2);
            }
        }
        return httpRequest;
    }

    @DexIgnore
    public final boolean a(String str) {
        return str != null && str.toLowerCase(Locale.US).startsWith(Utility.URL_SCHEME);
    }

    @DexIgnore
    public final synchronized SSLSocketFactory a() {
        if (this.c == null && !this.d) {
            this.c = b();
        }
        return this.c;
    }
}
