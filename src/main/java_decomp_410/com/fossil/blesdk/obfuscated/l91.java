package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l91 extends i91 {
    @DexIgnore
    public l91() {
        super();
    }

    @DexIgnore
    public static <E> z81<E> c(Object obj, long j) {
        return (z81) kb1.f(obj, j);
    }

    @DexIgnore
    public final <L> List<L> a(Object obj, long j) {
        z81 c = c(obj, j);
        if (c.A()) {
            return c;
        }
        int size = c.size();
        z81 b = c.b(size == 0 ? 10 : size << 1);
        kb1.a(obj, j, (Object) b);
        return b;
    }

    @DexIgnore
    public final void b(Object obj, long j) {
        c(obj, j).B();
    }

    @DexIgnore
    public final <E> void a(Object obj, Object obj2, long j) {
        z81 c = c(obj, j);
        z81 c2 = c(obj2, j);
        int size = c.size();
        int size2 = c2.size();
        if (size > 0 && size2 > 0) {
            if (!c.A()) {
                c = c.b(size2 + size);
            }
            c.addAll(c2);
        }
        if (size > 0) {
            c2 = c;
        }
        kb1.a(obj, j, (Object) c2);
    }
}
