package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xr */
public class C3305xr<Data> implements com.fossil.blesdk.obfuscated.C2912sr<java.lang.Integer, Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> f11051a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.Resources f11052b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xr$a */
    public static final class C3306a implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.Integer, android.content.res.AssetFileDescriptor> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.Resources f11053a;

        @DexIgnore
        public C3306a(android.content.res.Resources resources) {
            this.f11053a = resources;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.Integer, android.content.res.AssetFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3305xr(this.f11053a, wrVar.mo17509a(android.net.Uri.class, android.content.res.AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.xr$b */
    public static class C3307b implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.Integer, android.os.ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.Resources f11054a;

        @DexIgnore
        public C3307b(android.content.res.Resources resources) {
            this.f11054a = resources;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.Integer, android.os.ParcelFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3305xr(this.f11054a, wrVar.mo17509a(android.net.Uri.class, android.os.ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.xr$c */
    public static class C3308c implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.Integer, java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.Resources f11055a;

        @DexIgnore
        public C3308c(android.content.res.Resources resources) {
            this.f11055a = resources;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.Integer, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3305xr(this.f11055a, wrVar.mo17509a(android.net.Uri.class, java.io.InputStream.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xr$d")
    /* renamed from: com.fossil.blesdk.obfuscated.xr$d */
    public static class C3309d implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.Integer, android.net.Uri> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.Resources f11056a;

        @DexIgnore
        public C3309d(android.content.res.Resources resources) {
            this.f11056a = resources;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.Integer, android.net.Uri> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3305xr(this.f11056a, com.fossil.blesdk.obfuscated.C1442as.m4616a());
        }
    }

    @DexIgnore
    public C3305xr(android.content.res.Resources resources, com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> srVar) {
        this.f11052b = resources;
        this.f11051a = srVar;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo8912a(java.lang.Integer num) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(java.lang.Integer num, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.net.Uri a = mo17854a(num);
        if (a == null) {
            return null;
        }
        return this.f11051a.mo8911a(a, i, i2, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.net.Uri mo17854a(java.lang.Integer num) {
        try {
            return android.net.Uri.parse("android.resource://" + this.f11052b.getResourcePackageName(num.intValue()) + '/' + this.f11052b.getResourceTypeName(num.intValue()) + '/' + this.f11052b.getResourceEntryName(num.intValue()));
        } catch (android.content.res.Resources.NotFoundException e) {
            if (!android.util.Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            android.util.Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }
}
