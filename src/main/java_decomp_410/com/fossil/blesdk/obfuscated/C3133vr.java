package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vr */
public class C3133vr<Model, Data> implements com.fossil.blesdk.obfuscated.C2912sr<Model, Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, Data>> f10367a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> f10368b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vr$a */
    public static class C3134a<Data> implements com.fossil.blesdk.obfuscated.C2902so<Data>, com.fossil.blesdk.obfuscated.C2902so.C2903a<Data> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2902so<Data>> f10369e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> f10370f;

        @DexIgnore
        /* renamed from: g */
        public int f10371g; // = 0;

        @DexIgnore
        /* renamed from: h */
        public com.bumptech.glide.Priority f10372h;

        @DexIgnore
        /* renamed from: i */
        public com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Data> f10373i;

        @DexIgnore
        /* renamed from: j */
        public java.util.List<java.lang.Throwable> f10374j;

        @DexIgnore
        /* renamed from: k */
        public boolean f10375k;

        @DexIgnore
        public C3134a(java.util.List<com.fossil.blesdk.obfuscated.C2902so<Data>> list, com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
            this.f10370f = g8Var;
            com.fossil.blesdk.obfuscated.C2992tw.m14460a(list);
            this.f10369e = list;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Data> aVar) {
            this.f10372h = priority;
            this.f10373i = aVar;
            this.f10374j = this.f10370f.mo11162a();
            this.f10369e.get(this.f10371g).mo8870a(priority, this);
            if (this.f10375k) {
                cancel();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return this.f10369e.get(0).mo8872b();
        }

        @DexIgnore
        /* renamed from: c */
        public final void mo17166c() {
            if (!this.f10375k) {
                if (this.f10371g < this.f10369e.size() - 1) {
                    this.f10371g++;
                    mo8870a(this.f10372h, this.f10373i);
                    return;
                }
                com.fossil.blesdk.obfuscated.C2992tw.m14457a(this.f10374j);
                this.f10373i.mo9251a((java.lang.Exception) new com.bumptech.glide.load.engine.GlideException("Fetch failed", (java.util.List<java.lang.Throwable>) new java.util.ArrayList(this.f10374j)));
            }
        }

        @DexIgnore
        public void cancel() {
            this.f10375k = true;
            for (com.fossil.blesdk.obfuscated.C2902so<Data> cancel : this.f10369e) {
                cancel.cancel();
            }
        }

        @DexIgnore
        public java.lang.Class<Data> getDataClass() {
            return this.f10369e.get(0).getDataClass();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
            java.util.List<java.lang.Throwable> list = this.f10374j;
            if (list != null) {
                this.f10370f.mo11163a(list);
            }
            this.f10374j = null;
            for (com.fossil.blesdk.obfuscated.C2902so<Data> a : this.f10369e) {
                a.mo8869a();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9252a(Data data) {
            if (data != null) {
                this.f10373i.mo9252a(data);
            } else {
                mo17166c();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9251a(java.lang.Exception exc) {
            java.util.List<java.lang.Throwable> list = this.f10374j;
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(list);
            list.add(exc);
            mo17166c();
        }
    }

    @DexIgnore
    public C3133vr(java.util.List<com.fossil.blesdk.obfuscated.C2912sr<Model, Data>> list, com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
        this.f10367a = list;
        this.f10368b = g8Var;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(Model model, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        int size = this.f10367a.size();
        java.util.ArrayList arrayList = new java.util.ArrayList(size);
        com.fossil.blesdk.obfuscated.C2143jo joVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            com.fossil.blesdk.obfuscated.C2912sr srVar = this.f10367a.get(i3);
            if (srVar.mo8912a(model)) {
                com.fossil.blesdk.obfuscated.C2912sr.C2913a a = srVar.mo8911a(model, i, i2, loVar);
                if (a != null) {
                    joVar = a.f9449a;
                    arrayList.add(a.f9451c);
                }
            }
        }
        if (arrayList.isEmpty() || joVar == null) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(joVar, new com.fossil.blesdk.obfuscated.C3133vr.C3134a(arrayList, this.f10368b));
    }

    @DexIgnore
    public java.lang.String toString() {
        return "MultiModelLoader{modelLoaders=" + java.util.Arrays.toString(this.f10367a.toArray()) + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(Model model) {
        for (com.fossil.blesdk.obfuscated.C2912sr<Model, Data> a : this.f10367a) {
            if (a.mo8912a(model)) {
                return true;
            }
        }
        return false;
    }
}
