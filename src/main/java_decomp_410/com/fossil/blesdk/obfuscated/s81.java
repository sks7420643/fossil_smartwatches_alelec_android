package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s81 implements v91 {
    @DexIgnore
    public static /* final */ s81 a; // = new s81();

    @DexIgnore
    public static s81 a() {
        return a;
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        return t81.class.isAssignableFrom(cls);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final u91 a(Class<?> r5) {
        Class cls = t81.class;
        if (!cls.isAssignableFrom(r5)) {
            String valueOf = String.valueOf(r5.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (u91) t81.a(r5.asSubclass(cls)).a(t81.e.c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(r5.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
