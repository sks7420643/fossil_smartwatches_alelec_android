package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr3 implements Factory<SetNotificationUseCase> {
    @DexIgnore
    public /* final */ Provider<vy2> a;
    @DexIgnore
    public /* final */ Provider<px2> b;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<en2> f;

    @DexIgnore
    public pr3(Provider<vy2> provider, Provider<px2> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<en2> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static pr3 a(Provider<vy2> provider, Provider<px2> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<en2> provider6) {
        return new pr3(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static SetNotificationUseCase b(Provider<vy2> provider, Provider<px2> provider2, Provider<NotificationSettingsDatabase> provider3, Provider<NotificationsRepository> provider4, Provider<DeviceRepository> provider5, Provider<en2> provider6) {
        return new SetNotificationUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public SetNotificationUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
