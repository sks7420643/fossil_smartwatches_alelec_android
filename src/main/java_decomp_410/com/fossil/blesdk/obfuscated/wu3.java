package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.aw3;
import com.fossil.blesdk.obfuscated.dv3;
import com.fossil.blesdk.obfuscated.hv3;
import com.fossil.blesdk.obfuscated.jv3;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.net.Proxy;
import java.net.Socket;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wu3 {
    @DexIgnore
    public /* final */ xu3 a;
    @DexIgnore
    public /* final */ lv3 b;
    @DexIgnore
    public Socket c;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public sw3 e;
    @DexIgnore
    public aw3 f;
    @DexIgnore
    public Protocol g; // = Protocol.HTTP_1_1;
    @DexIgnore
    public long h;
    @DexIgnore
    public bv3 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public Object k;

    @DexIgnore
    public wu3(xu3 xu3, lv3 lv3) {
        this.a = xu3;
        this.b = lv3;
    }

    @DexIgnore
    public void a(Object obj) {
        if (!j()) {
            synchronized (this.a) {
                if (this.k == null) {
                    this.k = obj;
                } else {
                    throw new IllegalStateException("Connection already has an owner!");
                }
            }
        }
    }

    @DexIgnore
    public bv3 b() {
        return this.i;
    }

    @DexIgnore
    public long c() {
        aw3 aw3 = this.f;
        return aw3 == null ? this.h : aw3.y();
    }

    @DexIgnore
    public Protocol d() {
        return this.g;
    }

    @DexIgnore
    public lv3 e() {
        return this.b;
    }

    @DexIgnore
    public Socket f() {
        return this.c;
    }

    @DexIgnore
    public void g() {
        this.j++;
    }

    @DexIgnore
    public boolean h() {
        return !this.c.isClosed() && !this.c.isInputShutdown() && !this.c.isOutputShutdown();
    }

    @DexIgnore
    public boolean i() {
        return this.d;
    }

    @DexIgnore
    public boolean j() {
        return this.f != null;
    }

    @DexIgnore
    public boolean k() {
        aw3 aw3 = this.f;
        return aw3 == null || aw3.A();
    }

    @DexIgnore
    public boolean l() {
        sw3 sw3 = this.e;
        if (sw3 != null) {
            return sw3.e();
        }
        return true;
    }

    @DexIgnore
    public int m() {
        return this.j;
    }

    @DexIgnore
    public void n() {
        if (this.f == null) {
            this.h = System.nanoTime();
            return;
        }
        throw new IllegalStateException("framedConnection != null");
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.b.a.b);
        sb.append(":");
        sb.append(this.b.a.c);
        sb.append(", proxy=");
        sb.append(this.b.b);
        sb.append(" hostAddress=");
        sb.append(this.b.c.getAddress().getHostAddress());
        sb.append(" cipherSuite=");
        bv3 bv3 = this.i;
        sb.append(bv3 != null ? bv3.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public boolean a() {
        synchronized (this.a) {
            if (this.k == null) {
                return false;
            }
            this.k = null;
            return true;
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, hv3 hv3, List<yu3> list, boolean z) throws RouteException {
        Socket socket;
        List<yu3> list2 = list;
        if (!this.d) {
            mv3 mv3 = new mv3(list2);
            Proxy b2 = this.b.b();
            pu3 a2 = this.b.a();
            if (this.b.a.i() != null || list2.contains(yu3.h)) {
                RouteException routeException = null;
                while (!this.d) {
                    try {
                        if (b2.type() != Proxy.Type.DIRECT) {
                            if (b2.type() != Proxy.Type.HTTP) {
                                socket = new Socket(b2);
                                this.c = socket;
                                a(i2, i3, i4, hv3, mv3);
                                this.d = true;
                            }
                        }
                        socket = a2.h().createSocket();
                        this.c = socket;
                        a(i2, i3, i4, hv3, mv3);
                        this.d = true;
                    } catch (IOException e2) {
                        wv3.a(this.c);
                        this.c = null;
                        if (routeException == null) {
                            routeException = new RouteException(e2);
                        } else {
                            routeException.addConnectException(e2);
                        }
                        if (!z || !mv3.a(e2)) {
                            throw routeException;
                        }
                    }
                }
                return;
            }
            throw new RouteException(new UnknownServiceException("CLEARTEXT communication not supported: " + list2));
        }
        throw new IllegalStateException("already connected");
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, hv3 hv3, mv3 mv3) throws IOException {
        this.c.setSoTimeout(i3);
        uv3.c().a(this.c, this.b.c(), i2);
        if (this.b.a.i() != null) {
            a(i3, i4, hv3, mv3);
        }
        Protocol protocol = this.g;
        if (protocol == Protocol.SPDY_3 || protocol == Protocol.HTTP_2) {
            this.c.setSoTimeout(0);
            aw3.h hVar = new aw3.h(this.b.a.b, true, this.c);
            hVar.a(this.g);
            this.f = hVar.a();
            this.f.B();
            return;
        }
        this.e = new sw3(this.a, this, this.c);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v9, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r6v2, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r6v8 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f5 A[Catch:{ all -> 0x00eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fb A[Catch:{ all -> 0x00eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00fe  */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(int i2, int i3, hv3 hv3, mv3 mv3) throws IOException {
        Object r6;
        if (this.b.d()) {
            a(i2, i3, hv3);
        }
        pu3 a2 = this.b.a();
        String str = null;
        try {
            SSLSocket sSLSocket = (SSLSocket) a2.i().createSocket(this.c, a2.j(), a2.k(), true);
            try {
                yu3 a3 = mv3.a(sSLSocket);
                if (a3.b()) {
                    uv3.c().a(sSLSocket, a2.j(), a2.e());
                }
                sSLSocket.startHandshake();
                bv3 a4 = bv3.a(sSLSocket.getSession());
                if (a2.d().verify(a2.j(), sSLSocket.getSession())) {
                    a2.b().a(a2.j(), a4.c());
                    if (a3.b()) {
                        str = uv3.c().b(sSLSocket);
                    }
                    this.g = str != null ? Protocol.get(str) : Protocol.HTTP_1_1;
                    this.i = a4;
                    this.c = sSLSocket;
                    if (sSLSocket != 0) {
                        uv3.c().a(sSLSocket);
                        return;
                    }
                    return;
                }
                X509Certificate x509Certificate = (X509Certificate) a4.c().get(0);
                throw new SSLPeerUnverifiedException("Hostname " + a2.j() + " not verified:" + "\n    certificate: " + uu3.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + gx3.a(x509Certificate));
            } catch (AssertionError e2) {
                e = e2;
                str = sSLSocket;
                try {
                    if (!wv3.a(e)) {
                    }
                } catch (Throwable th) {
                    th = th;
                    r6 = str;
                    if (r6 != 0) {
                        uv3.c().a((SSLSocket) r6);
                    }
                    wv3.a((Socket) r6);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                r6 = sSLSocket;
                if (r6 != 0) {
                }
                wv3.a((Socket) r6);
                throw th;
            }
        } catch (AssertionError e3) {
            e = e3;
            if (!wv3.a(e)) {
                throw new IOException(e);
            }
            throw e;
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, hv3 hv3) throws IOException {
        hv3 a2 = a(hv3);
        sw3 sw3 = new sw3(this.a, this, this.c);
        sw3.a(i2, i3);
        dv3 d2 = a2.d();
        String str = "CONNECT " + d2.f() + ":" + d2.h() + " HTTP/1.1";
        do {
            sw3.a(a2.c(), str);
            sw3.c();
            jv3.b i4 = sw3.i();
            i4.a(a2);
            jv3 a3 = i4.a();
            long a4 = xw3.a(a3);
            if (a4 == -1) {
                a4 = 0;
            }
            yo4 b2 = sw3.b(a4);
            wv3.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            int e2 = a3.e();
            if (e2 != 200) {
                if (e2 == 407) {
                    a2 = xw3.a(this.b.a().a(), a3, this.b.b());
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a3.e());
                }
            } else if (sw3.a() > 0) {
                throw new IOException("TLS tunnel buffered too many bytes!");
            } else {
                return;
            }
        } while (a2 != null);
        throw new IOException("Failed to authenticate with proxy");
    }

    @DexIgnore
    public final hv3 a(hv3 hv3) throws IOException {
        dv3.b bVar = new dv3.b();
        bVar.e(Utility.URL_SCHEME);
        bVar.b(hv3.d().f());
        bVar.a(hv3.d().h());
        dv3 a2 = bVar.a();
        hv3.b bVar2 = new hv3.b();
        bVar2.a(a2);
        bVar2.b("Host", wv3.a(a2));
        bVar2.b("Proxy-Connection", "Keep-Alive");
        String a3 = hv3.a("User-Agent");
        if (a3 != null) {
            bVar2.b("User-Agent", a3);
        }
        String a4 = hv3.a("Proxy-Authorization");
        if (a4 != null) {
            bVar2.b("Proxy-Authorization", a4);
        }
        return bVar2.a();
    }

    @DexIgnore
    public void a(gv3 gv3, Object obj, hv3 hv3) throws RouteException {
        a(obj);
        if (!i()) {
            hv3 hv32 = hv3;
            a(gv3.d(), gv3.y(), gv3.C(), hv32, this.b.a.c(), gv3.z());
            if (j()) {
                gv3.e().c(this);
            }
            gv3.G().a(e());
        }
        a(gv3.y(), gv3.C());
    }

    @DexIgnore
    public dx3 a(uw3 uw3) throws IOException {
        aw3 aw3 = this.f;
        return aw3 != null ? new qw3(uw3, aw3) : new ww3(uw3, this.e);
    }

    @DexIgnore
    public void a(Protocol protocol) {
        if (protocol != null) {
            this.g = protocol;
            return;
        }
        throw new IllegalArgumentException("protocol == null");
    }

    @DexIgnore
    public void a(int i2, int i3) throws RouteException {
        if (!this.d) {
            throw new IllegalStateException("setTimeouts - not connected");
        } else if (this.e != null) {
            try {
                this.c.setSoTimeout(i2);
                this.e.a(i2, i3);
            } catch (IOException e2) {
                throw new RouteException(e2);
            }
        }
    }
}
