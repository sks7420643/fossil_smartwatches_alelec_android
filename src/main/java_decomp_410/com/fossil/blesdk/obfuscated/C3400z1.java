package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z1 */
public class C3400z1 extends com.fossil.blesdk.obfuscated.C1458b1 implements com.fossil.blesdk.obfuscated.C2382m8.C2383a {

    @DexIgnore
    /* renamed from: A */
    public /* final */ android.util.SparseBooleanArray f11445A; // = new android.util.SparseBooleanArray();

    @DexIgnore
    /* renamed from: B */
    public android.view.View f11446B;

    @DexIgnore
    /* renamed from: C */
    public com.fossil.blesdk.obfuscated.C3400z1.C3406e f11447C;

    @DexIgnore
    /* renamed from: D */
    public com.fossil.blesdk.obfuscated.C3400z1.C3401a f11448D;

    @DexIgnore
    /* renamed from: E */
    public com.fossil.blesdk.obfuscated.C3400z1.C3403c f11449E;

    @DexIgnore
    /* renamed from: F */
    public com.fossil.blesdk.obfuscated.C3400z1.C3402b f11450F;

    @DexIgnore
    /* renamed from: G */
    public /* final */ com.fossil.blesdk.obfuscated.C3400z1.C3407f f11451G; // = new com.fossil.blesdk.obfuscated.C3400z1.C3407f();

    @DexIgnore
    /* renamed from: H */
    public int f11452H;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C3400z1.C3404d f11453n;

    @DexIgnore
    /* renamed from: o */
    public android.graphics.drawable.Drawable f11454o;

    @DexIgnore
    /* renamed from: p */
    public boolean f11455p;

    @DexIgnore
    /* renamed from: q */
    public boolean f11456q;

    @DexIgnore
    /* renamed from: r */
    public boolean f11457r;

    @DexIgnore
    /* renamed from: s */
    public int f11458s;

    @DexIgnore
    /* renamed from: t */
    public int f11459t;

    @DexIgnore
    /* renamed from: u */
    public int f11460u;

    @DexIgnore
    /* renamed from: v */
    public boolean f11461v;

    @DexIgnore
    /* renamed from: w */
    public boolean f11462w;

    @DexIgnore
    /* renamed from: x */
    public boolean f11463x;

    @DexIgnore
    /* renamed from: y */
    public boolean f11464y;

    @DexIgnore
    /* renamed from: z */
    public int f11465z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$a */
    public class C3401a extends com.fossil.blesdk.obfuscated.C2518o1 {
        @DexIgnore
        public C3401a(android.content.Context context, com.fossil.blesdk.obfuscated.C3078v1 v1Var, android.view.View view) {
            super(context, v1Var, view, false, com.fossil.blesdk.obfuscated.C2777r.actionOverflowMenuStyle);
            if (!((com.fossil.blesdk.obfuscated.C2179k1) v1Var.getItem()).mo12572h()) {
                mo14234a(com.fossil.blesdk.obfuscated.C3400z1.this.f11453n == null ? (android.view.View) com.fossil.blesdk.obfuscated.C3400z1.this.f3643l : com.fossil.blesdk.obfuscated.C3400z1.this.f11453n);
            }
            mo14236a((com.fossil.blesdk.obfuscated.C2618p1.C2619a) com.fossil.blesdk.obfuscated.C3400z1.this.f11451G);
        }

        @DexIgnore
        /* renamed from: e */
        public void mo14242e() {
            com.fossil.blesdk.obfuscated.C3400z1 z1Var = com.fossil.blesdk.obfuscated.C3400z1.this;
            z1Var.f11448D = null;
            z1Var.f11452H = 0;
            super.mo14242e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$b")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$b */
    public class C3402b extends androidx.appcompat.view.menu.ActionMenuItemView.C0061b {
        @DexIgnore
        public C3402b() {
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2936t1 mo373a() {
            com.fossil.blesdk.obfuscated.C3400z1.C3401a aVar = com.fossil.blesdk.obfuscated.C3400z1.this.f11448D;
            if (aVar != null) {
                return aVar.mo14240c();
            }
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$c")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$c */
    public class C3403c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C3400z1.C3406e f11468e;

        @DexIgnore
        public C3403c(com.fossil.blesdk.obfuscated.C3400z1.C3406e eVar) {
            this.f11468e = eVar;
        }

        @DexIgnore
        public void run() {
            if (com.fossil.blesdk.obfuscated.C3400z1.this.f3638g != null) {
                com.fossil.blesdk.obfuscated.C3400z1.this.f3638g.mo11455a();
            }
            android.view.View view = (android.view.View) com.fossil.blesdk.obfuscated.C3400z1.this.f3643l;
            if (!(view == null || view.getWindowToken() == null || !this.f11468e.mo14244g())) {
                com.fossil.blesdk.obfuscated.C3400z1.this.f11447C = this.f11468e;
            }
            com.fossil.blesdk.obfuscated.C3400z1.this.f11449E = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$d")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$d */
    public class C3404d extends androidx.appcompat.widget.AppCompatImageView implements androidx.appcompat.widget.ActionMenuView.C0067a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$d$a")
        /* renamed from: com.fossil.blesdk.obfuscated.z1$d$a */
        public class C3405a extends com.fossil.blesdk.obfuscated.C2453n2 {
            @DexIgnore
            public C3405a(android.view.View view, com.fossil.blesdk.obfuscated.C3400z1 z1Var) {
                super(view);
            }

            @DexIgnore
            /* renamed from: b */
            public com.fossil.blesdk.obfuscated.C2936t1 mo371b() {
                com.fossil.blesdk.obfuscated.C3400z1.C3406e eVar = com.fossil.blesdk.obfuscated.C3400z1.this.f11447C;
                if (eVar == null) {
                    return null;
                }
                return eVar.mo14240c();
            }

            @DexIgnore
            /* renamed from: c */
            public boolean mo372c() {
                com.fossil.blesdk.obfuscated.C3400z1.this.mo18400j();
                return true;
            }

            @DexIgnore
            /* renamed from: d */
            public boolean mo560d() {
                com.fossil.blesdk.obfuscated.C3400z1 z1Var = com.fossil.blesdk.obfuscated.C3400z1.this;
                if (z1Var.f11449E != null) {
                    return false;
                }
                z1Var.mo18396f();
                return true;
            }
        }

        @DexIgnore
        public C3404d(android.content.Context context) {
            super(context, (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            com.fossil.blesdk.obfuscated.C1460b3.m4756a(this, getContentDescription());
            setOnTouchListener(new com.fossil.blesdk.obfuscated.C3400z1.C3404d.C3405a(this, com.fossil.blesdk.obfuscated.C3400z1.this));
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo352b() {
            return false;
        }

        @DexIgnore
        /* renamed from: c */
        public boolean mo353c() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            com.fossil.blesdk.obfuscated.C3400z1.this.mo18400j();
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            android.graphics.drawable.Drawable drawable = getDrawable();
            android.graphics.drawable.Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = java.lang.Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                com.fossil.blesdk.obfuscated.C1538c7.m5298a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$e")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$e */
    public class C3406e extends com.fossil.blesdk.obfuscated.C2518o1 {
        @DexIgnore
        public C3406e(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.View view, boolean z) {
            super(context, h1Var, view, z, com.fossil.blesdk.obfuscated.C2777r.actionOverflowMenuStyle);
            mo14232a(8388613);
            mo14236a((com.fossil.blesdk.obfuscated.C2618p1.C2619a) com.fossil.blesdk.obfuscated.C3400z1.this.f11451G);
        }

        @DexIgnore
        /* renamed from: e */
        public void mo14242e() {
            if (com.fossil.blesdk.obfuscated.C3400z1.this.f3638g != null) {
                com.fossil.blesdk.obfuscated.C3400z1.this.f3638g.close();
            }
            com.fossil.blesdk.obfuscated.C3400z1.this.f11447C = null;
            super.mo14242e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$f")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$f */
    public class C3407f implements com.fossil.blesdk.obfuscated.C2618p1.C2619a {
        @DexIgnore
        public C3407f() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            if (h1Var == null) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C3400z1.this.f11452H = ((com.fossil.blesdk.obfuscated.C3078v1) h1Var).getItem().getItemId();
            com.fossil.blesdk.obfuscated.C2618p1.C2619a c = com.fossil.blesdk.obfuscated.C3400z1.this.mo9017c();
            if (c != null) {
                return c.mo534a(h1Var);
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
            if (h1Var instanceof com.fossil.blesdk.obfuscated.C3078v1) {
                h1Var.mo11514m().mo11464a(false);
            }
            com.fossil.blesdk.obfuscated.C2618p1.C2619a c = com.fossil.blesdk.obfuscated.C3400z1.this.mo9017c();
            if (c != null) {
                c.mo533a(h1Var, z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$g")
    /* renamed from: com.fossil.blesdk.obfuscated.z1$g */
    public static class C3408g implements android.os.Parcelable {
        @DexIgnore
        public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3400z1.C3408g> CREATOR; // = new com.fossil.blesdk.obfuscated.C3400z1.C3408g.C3409a();

        @DexIgnore
        /* renamed from: e */
        public int f11474e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z1$g$a")
        /* renamed from: com.fossil.blesdk.obfuscated.z1$g$a */
        public static class C3409a implements android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3400z1.C3408g> {
            @DexIgnore
            public com.fossil.blesdk.obfuscated.C3400z1.C3408g createFromParcel(android.os.Parcel parcel) {
                return new com.fossil.blesdk.obfuscated.C3400z1.C3408g(parcel);
            }

            @DexIgnore
            public com.fossil.blesdk.obfuscated.C3400z1.C3408g[] newArray(int i) {
                return new com.fossil.blesdk.obfuscated.C3400z1.C3408g[i];
            }
        }

        @DexIgnore
        public C3408g() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(android.os.Parcel parcel, int i) {
            parcel.writeInt(this.f11474e);
        }

        @DexIgnore
        public C3408g(android.os.Parcel parcel) {
            this.f11474e = parcel.readInt();
        }
    }

    @DexIgnore
    public C3400z1(android.content.Context context) {
        super(context, com.fossil.blesdk.obfuscated.C3250x.abc_action_menu_layout, com.fossil.blesdk.obfuscated.C3250x.abc_action_menu_item_layout);
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo18397g() {
        com.fossil.blesdk.obfuscated.C3400z1.C3401a aVar = this.f11448D;
        if (aVar == null) {
            return false;
        }
        aVar.mo14239b();
        return true;
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo18398h() {
        return this.f11449E != null || mo18399i();
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo18399i() {
        com.fossil.blesdk.obfuscated.C3400z1.C3406e eVar = this.f11447C;
        return eVar != null && eVar.mo14241d();
    }

    @DexIgnore
    /* renamed from: j */
    public boolean mo18400j() {
        if (!this.f11456q || mo18399i()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f3638g;
        if (h1Var == null || this.f3643l == null || this.f11449E != null || h1Var.mo11511j().isEmpty()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C3400z1.C3406e eVar = new com.fossil.blesdk.obfuscated.C3400z1.C3406e(this.f3637f, this.f3638g, this.f11453n, true);
        this.f11449E = new com.fossil.blesdk.obfuscated.C3400z1.C3403c(eVar);
        ((android.view.View) this.f3643l).post(this.f11449E);
        super.mo1164a((com.fossil.blesdk.obfuscated.C3078v1) null);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1158a(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        super.mo1158a(context, h1Var);
        android.content.res.Resources resources = context.getResources();
        com.fossil.blesdk.obfuscated.C2849s0 a = com.fossil.blesdk.obfuscated.C2849s0.m13459a(context);
        if (!this.f11457r) {
            this.f11456q = a.mo15808g();
        }
        if (!this.f11463x) {
            this.f11458s = a.mo15803b();
        }
        if (!this.f11461v) {
            this.f11460u = a.mo15804c();
        }
        int i = this.f11458s;
        if (this.f11456q) {
            if (this.f11453n == null) {
                this.f11453n = new com.fossil.blesdk.obfuscated.C3400z1.C3404d(this.f3636e);
                if (this.f11455p) {
                    this.f11453n.setImageDrawable(this.f11454o);
                    this.f11454o = null;
                    this.f11455p = false;
                }
                int makeMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
                this.f11453n.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.f11453n.getMeasuredWidth();
        } else {
            this.f11453n = null;
        }
        this.f11459t = i;
        this.f11465z = (int) (resources.getDisplayMetrics().density * 56.0f);
        this.f11446B = null;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2697q1 mo9016b(android.view.ViewGroup viewGroup) {
        com.fossil.blesdk.obfuscated.C2697q1 q1Var = this.f3643l;
        com.fossil.blesdk.obfuscated.C2697q1 b = super.mo9016b(viewGroup);
        if (q1Var != b) {
            ((androidx.appcompat.widget.ActionMenuView) b).setPresenter(this);
        }
        return b;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo18392c(boolean z) {
        this.f11464y = z;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo18393d(boolean z) {
        this.f11456q = z;
        this.f11457r = true;
    }

    @DexIgnore
    /* renamed from: e */
    public android.graphics.drawable.Drawable mo18395e() {
        com.fossil.blesdk.obfuscated.C3400z1.C3404d dVar = this.f11453n;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.f11455p) {
            return this.f11454o;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo18396f() {
        com.fossil.blesdk.obfuscated.C3400z1.C3403c cVar = this.f11449E;
        if (cVar != null) {
            com.fossil.blesdk.obfuscated.C2697q1 q1Var = this.f3643l;
            if (q1Var != null) {
                ((android.view.View) q1Var).removeCallbacks(cVar);
                this.f11449E = null;
                return true;
            }
        }
        com.fossil.blesdk.obfuscated.C3400z1.C3406e eVar = this.f11447C;
        if (eVar == null) {
            return false;
        }
        eVar.mo14239b();
        return true;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo18394d() {
        return mo18396f() | mo18397g();
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Parcelable mo1165b() {
        com.fossil.blesdk.obfuscated.C3400z1.C3408g gVar = new com.fossil.blesdk.obfuscated.C3400z1.C3408g();
        gVar.f11474e = this.f11452H;
        return gVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13539b(boolean z) {
        if (z) {
            super.mo1164a((com.fossil.blesdk.obfuscated.C3078v1) null);
            return;
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f3638g;
        if (h1Var != null) {
            h1Var.mo11464a(false);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18389a(android.content.res.Configuration configuration) {
        if (!this.f11461v) {
            this.f11460u = com.fossil.blesdk.obfuscated.C2849s0.m13459a(this.f3637f).mo15804c();
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f3638g;
        if (h1Var != null) {
            h1Var.mo11489c(true);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18390a(android.graphics.drawable.Drawable drawable) {
        com.fossil.blesdk.obfuscated.C3400z1.C3404d dVar = this.f11453n;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.f11455p = true;
        this.f11454o = drawable;
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo9008a(com.fossil.blesdk.obfuscated.C2179k1 k1Var, android.view.View view, android.view.ViewGroup viewGroup) {
        android.view.View actionView = k1Var.getActionView();
        if (actionView == null || k1Var.mo12558f()) {
            actionView = super.mo9008a(k1Var, view, viewGroup);
        }
        actionView.setVisibility(k1Var.isActionViewExpanded() ? 8 : 0);
        androidx.appcompat.widget.ActionMenuView actionMenuView = (androidx.appcompat.widget.ActionMenuView) viewGroup;
        android.view.ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9012a(com.fossil.blesdk.obfuscated.C2179k1 k1Var, com.fossil.blesdk.obfuscated.C2697q1.C2698a aVar) {
        aVar.mo349a(k1Var, 0);
        androidx.appcompat.view.menu.ActionMenuItemView actionMenuItemView = (androidx.appcompat.view.menu.ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((androidx.appcompat.widget.ActionMenuView) this.f3643l);
        if (this.f11450F == null) {
            this.f11450F = new com.fossil.blesdk.obfuscated.C3400z1.C3402b();
        }
        actionMenuItemView.setPopupCallback(this.f11450F);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9014a(int i, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return k1Var.mo12572h();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1161a(boolean z) {
        super.mo1161a(z);
        ((android.view.View) this.f3643l).requestLayout();
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f3638g;
        boolean z2 = false;
        if (h1Var != null) {
            java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> c = h1Var.mo11486c();
            int size = c.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C2382m8 a = c.get(i).mo8358a();
                if (a != null) {
                    a.setSubUiVisibilityListener(this);
                }
            }
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var2 = this.f3638g;
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = h1Var2 != null ? h1Var2.mo11511j() : null;
        if (this.f11456q && j != null) {
            int size2 = j.size();
            if (size2 == 1) {
                z2 = !j.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z2 = true;
            }
        }
        if (z2) {
            if (this.f11453n == null) {
                this.f11453n = new com.fossil.blesdk.obfuscated.C3400z1.C3404d(this.f3636e);
            }
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f11453n.getParent();
            if (viewGroup != this.f3643l) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.f11453n);
                }
                androidx.appcompat.widget.ActionMenuView actionMenuView = (androidx.appcompat.widget.ActionMenuView) this.f3643l;
                actionMenuView.addView(this.f11453n, actionMenuView.mo507c());
            }
        } else {
            com.fossil.blesdk.obfuscated.C3400z1.C3404d dVar = this.f11453n;
            if (dVar != null) {
                android.view.ViewParent parent = dVar.getParent();
                com.fossil.blesdk.obfuscated.C2697q1 q1Var = this.f3643l;
                if (parent == q1Var) {
                    ((android.view.ViewGroup) q1Var).removeView(this.f11453n);
                }
            }
        }
        ((androidx.appcompat.widget.ActionMenuView) this.f3643l).setOverflowReserved(this.f11456q);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9015a(android.view.ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.f11453n) {
            return false;
        }
        return super.mo9015a(viewGroup, i);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        boolean z = false;
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C3078v1 v1Var2 = v1Var;
        while (v1Var2.mo16960t() != this.f3638g) {
            v1Var2 = (com.fossil.blesdk.obfuscated.C3078v1) v1Var2.mo16960t();
        }
        android.view.View a = mo18388a(v1Var2.getItem());
        if (a == null) {
            return false;
        }
        this.f11452H = v1Var.getItem().getItemId();
        int size = v1Var.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            android.view.MenuItem item = v1Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z = true;
                break;
            }
            i++;
        }
        this.f11448D = new com.fossil.blesdk.obfuscated.C3400z1.C3401a(this.f3637f, v1Var, a);
        this.f11448D.mo14237a(z);
        this.f11448D.mo14243f();
        super.mo1164a(v1Var);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.View mo18388a(android.view.MenuItem menuItem) {
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f3643l;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof com.fossil.blesdk.obfuscated.C2697q1.C2698a) && ((com.fossil.blesdk.obfuscated.C2697q1.C2698a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1162a() {
        int i;
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> arrayList;
        int i2;
        int i3;
        int i4;
        com.fossil.blesdk.obfuscated.C3400z1 z1Var = this;
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = z1Var.f3638g;
        int i5 = 0;
        if (h1Var != null) {
            arrayList = h1Var.mo11515n();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = z1Var.f11460u;
        int i7 = z1Var.f11459t;
        int makeMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) z1Var.f3643l;
        int i8 = i6;
        boolean z = false;
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i; i11++) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var = arrayList.get(i11);
            if (k1Var.mo12580k()) {
                i9++;
            } else if (k1Var.mo12579j()) {
                i10++;
            } else {
                z = true;
            }
            if (z1Var.f11464y && k1Var.isActionViewExpanded()) {
                i8 = 0;
            }
        }
        if (z1Var.f11456q && (z || i10 + i9 > i8)) {
            i8--;
        }
        int i12 = i8 - i9;
        android.util.SparseBooleanArray sparseBooleanArray = z1Var.f11445A;
        sparseBooleanArray.clear();
        if (z1Var.f11462w) {
            int i13 = z1Var.f11465z;
            i2 = i7 / i13;
            i3 = i13 + ((i7 % i13) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i14 = i7;
        int i15 = 0;
        int i16 = 0;
        while (i15 < i) {
            com.fossil.blesdk.obfuscated.C2179k1 k1Var2 = arrayList.get(i15);
            if (k1Var2.mo12580k()) {
                android.view.View a = z1Var.mo9008a(k1Var2, z1Var.f11446B, viewGroup);
                if (z1Var.f11446B == null) {
                    z1Var.f11446B = a;
                }
                if (z1Var.f11462w) {
                    i2 -= androidx.appcompat.widget.ActionMenuView.m319a(a, i3, i2, makeMeasureSpec, i5);
                } else {
                    a.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a.getMeasuredWidth();
                i14 -= measuredWidth;
                if (i16 != 0) {
                    measuredWidth = i16;
                }
                int groupId = k1Var2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                k1Var2.mo12555d(true);
                i4 = i;
                i16 = measuredWidth;
            } else if (k1Var2.mo12579j()) {
                int groupId2 = k1Var2.getGroupId();
                boolean z2 = sparseBooleanArray.get(groupId2);
                boolean z3 = (i12 > 0 || z2) && i14 > 0 && (!z1Var.f11462w || i2 > 0);
                boolean z4 = z3;
                if (z3) {
                    android.view.View a2 = z1Var.mo9008a(k1Var2, z1Var.f11446B, viewGroup);
                    i4 = i;
                    if (z1Var.f11446B == null) {
                        z1Var.f11446B = a2;
                    }
                    if (z1Var.f11462w) {
                        int a3 = androidx.appcompat.widget.ActionMenuView.m319a(a2, i3, i2, makeMeasureSpec, 0);
                        i2 -= a3;
                        if (a3 == 0) {
                            z4 = false;
                        }
                    } else {
                        a2.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a2.getMeasuredWidth();
                    i14 -= measuredWidth2;
                    if (i16 == 0) {
                        i16 = measuredWidth2;
                    }
                    z3 = z4 & (!z1Var.f11462w ? i14 + i16 > 0 : i14 >= 0);
                } else {
                    i4 = i;
                }
                if (z3 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z2) {
                    sparseBooleanArray.put(groupId2, false);
                    int i17 = 0;
                    while (i17 < i15) {
                        com.fossil.blesdk.obfuscated.C2179k1 k1Var3 = arrayList.get(i17);
                        if (k1Var3.getGroupId() == groupId2) {
                            if (k1Var3.mo12572h()) {
                                i12++;
                            }
                            k1Var3.mo12555d(false);
                        }
                        i17++;
                    }
                }
                if (z3) {
                    i12--;
                }
                k1Var2.mo12555d(z3);
            } else {
                i4 = i;
                k1Var2.mo12555d(false);
                i15++;
                i5 = 0;
                z1Var = this;
                i = i4;
            }
            i15++;
            i5 = 0;
            z1Var = this;
            i = i4;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        mo18394d();
        super.mo1160a(h1Var, z);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1159a(android.os.Parcelable parcelable) {
        if (parcelable instanceof com.fossil.blesdk.obfuscated.C3400z1.C3408g) {
            int i = ((com.fossil.blesdk.obfuscated.C3400z1.C3408g) parcelable).f11474e;
            if (i > 0) {
                android.view.MenuItem findItem = this.f3638g.findItem(i);
                if (findItem != null) {
                    mo1164a((com.fossil.blesdk.obfuscated.C3078v1) findItem.getSubMenu());
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18391a(androidx.appcompat.widget.ActionMenuView actionMenuView) {
        this.f3643l = actionMenuView;
        actionMenuView.mo374a(this.f3638g);
    }
}
