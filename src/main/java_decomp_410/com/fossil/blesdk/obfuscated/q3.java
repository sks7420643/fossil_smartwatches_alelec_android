package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.b;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class q3 extends Service {
    @DexIgnore
    public /* final */ Map<IBinder, IBinder.DeathRecipient> e; // = new g4();
    @DexIgnore
    public b.a f; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends b.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.q3$a$a  reason: collision with other inner class name */
        public class C0028a implements IBinder.DeathRecipient {
            @DexIgnore
            public /* final */ /* synthetic */ s3 a;

            @DexIgnore
            public C0028a(s3 s3Var) {
                this.a = s3Var;
            }

            @DexIgnore
            public void binderDied() {
                q3.this.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean a(long j) {
            return q3.this.a(j);
        }

        @DexIgnore
        public Bundle b(String str, Bundle bundle) {
            return q3.this.a(str, bundle);
        }

        @DexIgnore
        public boolean a(a aVar) {
            s3 s3Var = new s3(aVar);
            try {
                C0028a aVar2 = new C0028a(s3Var);
                synchronized (q3.this.e) {
                    aVar.asBinder().linkToDeath(aVar2, 0);
                    q3.this.e.put(aVar.asBinder(), aVar2);
                }
                return q3.this.b(s3Var);
            } catch (RemoteException unused) {
                return false;
            }
        }

        @DexIgnore
        public boolean b(a aVar, Bundle bundle) {
            return q3.this.a(new s3(aVar), bundle);
        }

        @DexIgnore
        public int b(a aVar, String str, Bundle bundle) {
            return q3.this.a(new s3(aVar), str, bundle);
        }

        @DexIgnore
        public boolean a(a aVar, Uri uri, Bundle bundle, List<Bundle> list) {
            return q3.this.a(new s3(aVar), uri, bundle, list);
        }

        @DexIgnore
        public boolean a(a aVar, Uri uri) {
            return q3.this.a(new s3(aVar), uri);
        }

        @DexIgnore
        public boolean a(a aVar, int i, Uri uri, Bundle bundle) {
            return q3.this.a(new s3(aVar), i, uri, bundle);
        }
    }

    @DexIgnore
    public abstract int a(s3 s3Var, String str, Bundle bundle);

    @DexIgnore
    public abstract Bundle a(String str, Bundle bundle);

    @DexIgnore
    public abstract boolean a(long j);

    @DexIgnore
    public boolean a(s3 s3Var) {
        try {
            synchronized (this.e) {
                IBinder a2 = s3Var.a();
                a2.unlinkToDeath(this.e.get(a2), 0);
                this.e.remove(a2);
            }
            return true;
        } catch (NoSuchElementException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract boolean a(s3 s3Var, int i, Uri uri, Bundle bundle);

    @DexIgnore
    public abstract boolean a(s3 s3Var, Uri uri);

    @DexIgnore
    public abstract boolean a(s3 s3Var, Uri uri, Bundle bundle, List<Bundle> list);

    @DexIgnore
    public abstract boolean a(s3 s3Var, Bundle bundle);

    @DexIgnore
    public abstract boolean b(s3 s3Var);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.f;
    }
}
