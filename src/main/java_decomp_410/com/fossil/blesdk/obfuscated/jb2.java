package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jb2 extends ib2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P; // = new SparseIntArray();
    @DexIgnore
    public long N;

    /*
    static {
        P.put(R.id.cl_top, 1);
        P.put(R.id.tv_cancel, 2);
        P.put(R.id.tv_preset_name, 3);
        P.put(R.id.ftv_set_to_watch, 4);
        P.put(R.id.cl_complications, 5);
        P.put(R.id.line_center, 6);
        P.put(R.id.line_top, 7);
        P.put(R.id.line_bottom, 8);
        P.put(R.id.iv_background, 9);
        P.put(R.id.circle, 10);
        P.put(R.id.wc_top, 11);
        P.put(R.id.wc_bottom, 12);
        P.put(R.id.wc_start, 13);
        P.put(R.id.wc_end, 14);
        P.put(R.id.guideline, 15);
        P.put(R.id.iv_complications_holder, 16);
        P.put(R.id.guideline_complications_holder, 17);
        P.put(R.id.cl_watch_apps, 18);
        P.put(R.id.wa_top, 19);
        P.put(R.id.tv_wa_top, 20);
        P.put(R.id.wa_middle, 21);
        P.put(R.id.tv_wa_middle, 22);
        P.put(R.id.wa_bottom, 23);
        P.put(R.id.tv_wa_bottom, 24);
        P.put(R.id.v_align, 25);
        P.put(R.id.cv_group, 26);
        P.put(R.id.rv_preset, 27);
    }
    */

    @DexIgnore
    public jb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 28, O, P));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.N != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.N = 1;
        }
        g();
    }

    @DexIgnore
    public jb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[10], objArr[5], objArr[0], objArr[1], objArr[18], objArr[26], objArr[4], objArr[15], objArr[17], objArr[9], objArr[16], objArr[8], objArr[6], objArr[7], objArr[27], objArr[2], objArr[3], objArr[24], objArr[22], objArr[20], objArr[25], objArr[23], objArr[21], objArr[19], objArr[12], objArr[14], objArr[13], objArr[11]);
        this.N = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
