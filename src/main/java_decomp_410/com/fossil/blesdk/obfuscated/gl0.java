package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gl0 implements ak0.b {
    @DexIgnore
    public final ApiException a(Status status) {
        return hj0.a(status);
    }
}
