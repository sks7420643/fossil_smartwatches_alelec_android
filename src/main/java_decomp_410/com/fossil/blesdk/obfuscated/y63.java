package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y63 implements MembersInjector<SearchMicroAppActivity> {
    @DexIgnore
    public static void a(SearchMicroAppActivity searchMicroAppActivity, SearchMicroAppPresenter searchMicroAppPresenter) {
        searchMicroAppActivity.B = searchMicroAppPresenter;
    }
}
