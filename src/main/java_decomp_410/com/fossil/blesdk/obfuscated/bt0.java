package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class bt0 implements gt0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public bt0(String str, boolean z) {
        this.a = str;
    }

    @DexIgnore
    public final Object a() {
        return Boolean.valueOf(wy0.a(ys0.h.getContentResolver(), this.a, this.b));
    }
}
