package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dy2 implements MembersInjector<cy2> {
    @DexIgnore
    public static void a(cy2 cy2, InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
        cy2.n = inactivityNudgeTimePresenter;
    }

    @DexIgnore
    public static void a(cy2 cy2, RemindTimePresenter remindTimePresenter) {
        cy2.o = remindTimePresenter;
    }
}
