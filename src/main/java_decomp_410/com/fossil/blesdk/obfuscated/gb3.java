package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gb3 extends zr2 implements fb3 {
    @DexIgnore
    public iu3 j;
    @DexIgnore
    public tr3<ac2> k;
    @DexIgnore
    public eb3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ gb3 e;

        @DexIgnore
        public b(gb3 gb3, ac2 ac2) {
            this.e = gb3;
        }

        @DexIgnore
        public final void onClick(View view) {
            gb3.a(this.e).c().a(1);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ iu3 a(gb3 gb3) {
        iu3 iu3 = gb3.j;
        if (iu3 != null) {
            return iu3;
        }
        kd4.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void c(boolean z) {
        tr3<ac2> tr3 = this.k;
        if (tr3 != null) {
            ac2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                kd4.a((Object) overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                kd4.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            kd4.a((Object) overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            kd4.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        ac2 ac2 = (ac2) qa.a(layoutInflater, R.layout.fragment_goal_tracking_overview_day, viewGroup, false, O0());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ic a2 = lc.a(activity).a(iu3.class);
            kd4.a((Object) a2, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.j = (iu3) a2;
            ac2.s.setOnClickListener(new b(this, ac2));
        }
        this.k = new tr3<>(this, ac2);
        tr3<ac2> tr3 = this.k;
        if (tr3 != null) {
            ac2 a3 = tr3.a();
            if (a3 != null) {
                return a3.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        eb3 eb3 = this.l;
        if (eb3 != null) {
            eb3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        eb3 eb3 = this.l;
        if (eb3 != null) {
            eb3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(wr2 wr2, ArrayList<String> arrayList) {
        kd4.b(wr2, "baseModel");
        kd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + wr2);
        tr3<ac2> tr3 = this.k;
        if (tr3 != null) {
            ac2 a2 = tr3.a();
            if (a2 != null) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                if (overviewDayGoalChart != null) {
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) ll2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayGoalChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(eb3 eb3) {
        kd4.b(eb3, "presenter");
        this.l = eb3;
    }
}
