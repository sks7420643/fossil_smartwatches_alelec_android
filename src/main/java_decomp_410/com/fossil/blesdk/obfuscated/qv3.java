package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qv3 {
    @DexIgnore
    jv3 a(hv3 hv3) throws IOException;

    @DexIgnore
    ow3 a(jv3 jv3) throws IOException;

    @DexIgnore
    void a();

    @DexIgnore
    void a(jv3 jv3, jv3 jv32) throws IOException;

    @DexIgnore
    void a(pw3 pw3);

    @DexIgnore
    void b(hv3 hv3) throws IOException;
}
