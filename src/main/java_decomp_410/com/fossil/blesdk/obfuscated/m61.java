package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m61 extends ContentObserver {
    @DexIgnore
    public m61(Handler handler) {
        super((Handler) null);
    }

    @DexIgnore
    public final void onChange(boolean z) {
        l61.e.set(true);
    }
}
