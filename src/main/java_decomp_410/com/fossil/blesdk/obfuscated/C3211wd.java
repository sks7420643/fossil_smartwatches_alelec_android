package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wd */
public class C3211wd<T> extends com.fossil.blesdk.obfuscated.C2723qd<T> implements com.fossil.blesdk.obfuscated.C2871sd.C2872a {

    @DexIgnore
    /* renamed from: s */
    public /* final */ com.fossil.blesdk.obfuscated.C3026ud<T> f10605s;

    @DexIgnore
    /* renamed from: t */
    public com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> f10606t; // = new com.fossil.blesdk.obfuscated.C3211wd.C3212a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wd$a */
    public class C3212a extends com.fossil.blesdk.obfuscated.C2644pd.C2645a<T> {
        @DexIgnore
        public C3212a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12730a(int i, com.fossil.blesdk.obfuscated.C2644pd<T> pdVar) {
            if (pdVar.mo14665a()) {
                com.fossil.blesdk.obfuscated.C3211wd.this.mo15185c();
            } else if (!com.fossil.blesdk.obfuscated.C3211wd.this.mo15192h()) {
                if (i == 0 || i == 3) {
                    java.util.List<T> list = pdVar.f8343a;
                    if (com.fossil.blesdk.obfuscated.C3211wd.this.f8608i.mo15936i() == 0) {
                        com.fossil.blesdk.obfuscated.C3211wd wdVar = com.fossil.blesdk.obfuscated.C3211wd.this;
                        wdVar.f8608i.mo15914a(pdVar.f8344b, list, pdVar.f8345c, pdVar.f8346d, wdVar.f8607h.f8631a, wdVar);
                    } else {
                        com.fossil.blesdk.obfuscated.C3211wd wdVar2 = com.fossil.blesdk.obfuscated.C3211wd.this;
                        wdVar2.f8608i.mo15922b(pdVar.f8346d, list, wdVar2.f8609j, wdVar2.f8607h.f8634d, wdVar2.f8611l, wdVar2);
                    }
                    com.fossil.blesdk.obfuscated.C3211wd wdVar3 = com.fossil.blesdk.obfuscated.C3211wd.this;
                    if (wdVar3.f8606g != null) {
                        boolean z = true;
                        boolean z2 = wdVar3.f8608i.size() == 0;
                        boolean z3 = !z2 && pdVar.f8344b == 0 && pdVar.f8346d == 0;
                        int size = com.fossil.blesdk.obfuscated.C3211wd.this.size();
                        if (z2 || (!(i == 0 && pdVar.f8345c == 0) && (i != 3 || pdVar.f8346d + com.fossil.blesdk.obfuscated.C3211wd.this.f8607h.f8631a < size))) {
                            z = false;
                        }
                        com.fossil.blesdk.obfuscated.C3211wd.this.mo15184a(z2, z3, z);
                        return;
                    }
                    return;
                }
                throw new java.lang.IllegalArgumentException("unexpected resultType" + i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.wd$b */
    public class C3213b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ int f10608e;

        @DexIgnore
        public C3213b(int i) {
            this.f10608e = i;
        }

        @DexIgnore
        public void run() {
            if (!com.fossil.blesdk.obfuscated.C3211wd.this.mo15192h()) {
                com.fossil.blesdk.obfuscated.C3211wd wdVar = com.fossil.blesdk.obfuscated.C3211wd.this;
                int i = wdVar.f8607h.f8631a;
                if (wdVar.f10605s.isInvalid()) {
                    com.fossil.blesdk.obfuscated.C3211wd.this.mo15185c();
                    return;
                }
                int i2 = this.f10608e * i;
                int min = java.lang.Math.min(i, com.fossil.blesdk.obfuscated.C3211wd.this.f8608i.size() - i2);
                com.fossil.blesdk.obfuscated.C3211wd wdVar2 = com.fossil.blesdk.obfuscated.C3211wd.this;
                wdVar2.f10605s.dispatchLoadRange(3, i2, min, wdVar2.f8604e, wdVar2.f10606t);
            }
        }
    }

    @DexIgnore
    public C3211wd(com.fossil.blesdk.obfuscated.C3026ud<T> udVar, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, com.fossil.blesdk.obfuscated.C2723qd.C2726c<T> cVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar, int i) {
        super(new com.fossil.blesdk.obfuscated.C2871sd(), executor, executor2, cVar, fVar);
        this.f10605s = udVar;
        int i2 = this.f8607h.f8631a;
        this.f8609j = i;
        if (this.f10605s.isInvalid()) {
            mo15185c();
            return;
        }
        int max = java.lang.Math.max(this.f8607h.f8635e / i2, 2) * i2;
        this.f10605s.dispatchLoadInitial(true, java.lang.Math.max(0, ((i - (max / 2)) / i2) * i2), max, i2, this.f8604e, this.f10606t);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12718a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar) {
        com.fossil.blesdk.obfuscated.C2871sd<T> sdVar = qdVar.f8608i;
        if (sdVar.isEmpty() || this.f8608i.size() != sdVar.size()) {
            throw new java.lang.IllegalArgumentException("Invalid snapshot provided - doesn't appear to be a snapshot of this PagedList");
        }
        int i = this.f8607h.f8631a;
        int e = this.f8608i.mo15931e() / i;
        int i2 = this.f8608i.mo15936i();
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + e;
            int i5 = 0;
            while (i5 < this.f8608i.mo15936i()) {
                int i6 = i4 + i5;
                if (!this.f8608i.mo15924b(i, i6) || sdVar.mo15924b(i, i6)) {
                    break;
                }
                i5++;
            }
            if (i5 > 0) {
                eVar.mo11912a(i4 * i, i * i5);
                i3 += i5 - 1;
            }
            i3++;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12721b(int i, int i2, int i3) {
        throw new java.lang.IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    /* renamed from: c */
    public void mo12722c(int i, int i2) {
        mo15186d(i, i2);
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2307ld<?, T> mo12723d() {
        return this.f10605s;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.Object mo12724e() {
        return java.lang.Integer.valueOf(this.f8609j);
    }

    @DexIgnore
    /* renamed from: f */
    public void mo12725f(int i) {
        this.f8605f.execute(new com.fossil.blesdk.obfuscated.C3211wd.C3213b(i));
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo12726g() {
        return false;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo12727h(int i) {
        com.fossil.blesdk.obfuscated.C2871sd<T> sdVar = this.f8608i;
        com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar = this.f8607h;
        sdVar.mo15912a(i, fVar.f8632b, fVar.f8631a, (com.fossil.blesdk.obfuscated.C2871sd.C2872a) this);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12719b() {
        throw new java.lang.IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    /* renamed from: b */
    public void mo12720b(int i, int i2) {
        mo15189f(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12715a(int i) {
        mo15187e(0, i);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12717a(int i, int i2, int i3) {
        throw new java.lang.IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12714a() {
        throw new java.lang.IllegalStateException("Contiguous callback on TiledPagedList");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12716a(int i, int i2) {
        mo15186d(i, i2);
    }
}
