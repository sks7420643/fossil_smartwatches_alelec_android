package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gi */
public abstract class C1876gi extends com.fossil.blesdk.obfuscated.C2490nh {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String[] f5445a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    /* renamed from: a */
    public void mo11242a(com.fossil.blesdk.obfuscated.C2654ph phVar) {
        android.view.View view = phVar.f8383b;
        java.lang.Integer num = (java.lang.Integer) phVar.f8382a.get("android:visibility:visibility");
        if (num == null) {
            num = java.lang.Integer.valueOf(view.getVisibility());
        }
        phVar.f8382a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + java.lang.Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + java.lang.Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        phVar.f8382a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11244b(com.fossil.blesdk.obfuscated.C2654ph phVar) {
        if (phVar == null) {
            return 8;
        }
        java.lang.Integer num = (java.lang.Integer) phVar.f8382a.get("android:visibilityPropagation:visibility");
        if (num == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    /* renamed from: c */
    public int mo11245c(com.fossil.blesdk.obfuscated.C2654ph phVar) {
        return m7429a(phVar, 0);
    }

    @DexIgnore
    /* renamed from: d */
    public int mo11246d(com.fossil.blesdk.obfuscated.C2654ph phVar) {
        return m7429a(phVar, 1);
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String[] mo11243a() {
        return f5445a;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m7429a(com.fossil.blesdk.obfuscated.C2654ph phVar, int i) {
        if (phVar == null) {
            return -1;
        }
        int[] iArr = (int[]) phVar.f8382a.get("android:visibilityPropagation:center");
        if (iArr == null) {
            return -1;
        }
        return iArr[i];
    }
}
