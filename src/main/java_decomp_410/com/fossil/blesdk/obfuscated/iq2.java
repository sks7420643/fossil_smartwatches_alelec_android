package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.debug.DebugActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iq2 implements MembersInjector<DebugActivity> {
    @DexIgnore
    public static void a(DebugActivity debugActivity, en2 en2) {
        debugActivity.B = en2;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, cr2 cr2) {
        debugActivity.C = cr2;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, FirmwareFileRepository firmwareFileRepository) {
        debugActivity.D = firmwareFileRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, GuestApiService guestApiService) {
        debugActivity.E = guestApiService;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, DianaPresetRepository dianaPresetRepository) {
        debugActivity.F = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, ShakeFeedbackService shakeFeedbackService) {
        debugActivity.G = shakeFeedbackService;
    }

    @DexIgnore
    public static void a(DebugActivity debugActivity, vj2 vj2) {
        debugActivity.H = vj2;
    }
}
