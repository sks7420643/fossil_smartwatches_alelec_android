package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hy0 extends qx0<hy0> implements Cloneable {
    @DexIgnore
    public byte[] g; // = yx0.e;
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public byte[][] i; // = yx0.d;

    @DexIgnore
    public hy0() {
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        if (!Arrays.equals(this.g, yx0.e)) {
            px0.a(1, this.g);
        }
        byte[][] bArr = this.i;
        if (bArr != null && bArr.length > 0) {
            int i2 = 0;
            while (true) {
                byte[][] bArr2 = this.i;
                if (i2 >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i2];
                if (bArr3 != null) {
                    px0.a(2, bArr3);
                }
                i2++;
            }
        }
        String str = this.h;
        if (str != null && !str.equals("")) {
            px0.a(4, this.h);
        }
        super.a(px0);
    }

    @DexIgnore
    public final int b() {
        int b = super.b();
        if (!Arrays.equals(this.g, yx0.e)) {
            b += px0.b(1, this.g);
        }
        byte[][] bArr = this.i;
        if (bArr != null && bArr.length > 0) {
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            while (true) {
                byte[][] bArr2 = this.i;
                if (i2 >= bArr2.length) {
                    break;
                }
                byte[] bArr3 = bArr2[i2];
                if (bArr3 != null) {
                    i4++;
                    i3 += px0.b(bArr3);
                }
                i2++;
            }
            b = b + i3 + (i4 * 1);
        }
        String str = this.h;
        return (str == null || str.equals("")) ? b : b + px0.b(4, this.h);
    }

    @DexIgnore
    public final /* synthetic */ vx0 c() throws CloneNotSupportedException {
        return (hy0) clone();
    }

    @DexIgnore
    public final /* synthetic */ qx0 d() throws CloneNotSupportedException {
        return (hy0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final hy0 clone() {
        try {
            hy0 hy0 = (hy0) super.clone();
            byte[][] bArr = this.i;
            if (bArr != null && bArr.length > 0) {
                hy0.i = (byte[][]) bArr.clone();
            }
            return hy0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof hy0)) {
            return false;
        }
        hy0 hy0 = (hy0) obj;
        if (!Arrays.equals(this.g, hy0.g)) {
            return false;
        }
        String str = this.h;
        if (str == null) {
            if (hy0.h != null) {
                return false;
            }
        } else if (!str.equals(hy0.h)) {
            return false;
        }
        if (!ux0.a(this.i, hy0.i)) {
            return false;
        }
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            return this.f.equals(hy0.f);
        }
        sx0 sx02 = hy0.f;
        return sx02 == null || sx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((hy0.class.getName().hashCode() + 527) * 31) + Arrays.hashCode(this.g)) * 31;
        String str = this.h;
        int i2 = 0;
        int hashCode2 = (((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + ux0.a(this.i)) * 31) + 1237) * 31;
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            i2 = this.f.hashCode();
        }
        return hashCode2 + i2;
    }
}
