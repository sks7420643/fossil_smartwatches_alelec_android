package com.fossil.blesdk.obfuscated;

import com.google.common.collect.Maps;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xu1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<E> extends AbstractSet<E> {
        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            return xu1.a((Set<?>) this, collection);
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            st1.a(collection);
            return super.retainAll(collection);
        }
    }

    @DexIgnore
    public static <E> HashSet<E> a() {
        return new HashSet<>();
    }

    @DexIgnore
    public static <E> LinkedHashSet<E> b(int i) {
        return new LinkedHashSet<>(Maps.a(i));
    }

    @DexIgnore
    public static <E> HashSet<E> a(int i) {
        return new HashSet<>(Maps.a(i));
    }

    @DexIgnore
    public static int a(Set<?> set) {
        Iterator<?> it = set.iterator();
        int i = 0;
        while (it.hasNext()) {
            Object next = it.next();
            i = ~(~(i + (next != null ? next.hashCode() : 0)));
        }
        return i;
    }

    @DexIgnore
    public static boolean a(Set<?> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean a(Set<?> set, Iterator<?> it) {
        boolean z = false;
        while (it.hasNext()) {
            z |= set.remove(it.next());
        }
        return z;
    }

    @DexIgnore
    public static boolean a(Set<?> set, Collection<?> collection) {
        st1.a(collection);
        if (collection instanceof ru1) {
            collection = ((ru1) collection).elementSet();
        }
        if (!(collection instanceof Set) || collection.size() <= set.size()) {
            return a(set, collection.iterator());
        }
        return lu1.a(set.iterator(), collection);
    }
}
