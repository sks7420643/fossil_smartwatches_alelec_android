package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.FacebookTimeSpentData;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xu3 {
    @DexIgnore
    public static /* final */ xu3 f;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ LinkedList<wu3> c; // = new LinkedList<>();
    @DexIgnore
    public Executor d; // = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), wv3.a("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ Runnable e; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            xu3.this.b();
        }
    }

    /*
    static {
        String property = System.getProperty("http.keepAlive");
        String property2 = System.getProperty("http.keepAliveDuration");
        String property3 = System.getProperty("http.maxConnections");
        long parseLong = property2 != null ? Long.parseLong(property2) : FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
        if (property != null && !Boolean.parseBoolean(property)) {
            f = new xu3(0, parseLong);
        } else if (property3 != null) {
            f = new xu3(Integer.parseInt(property3), parseLong);
        } else {
            f = new xu3(5, parseLong);
        }
    }
    */

    @DexIgnore
    public xu3(int i, long j) {
        this.a = i;
        this.b = j * 1000 * 1000;
    }

    @DexIgnore
    public static xu3 c() {
        return f;
    }

    @DexIgnore
    public void b(wu3 wu3) {
        if (wu3.j() || !wu3.a()) {
            return;
        }
        if (!wu3.h()) {
            wv3.a(wu3.f());
            return;
        }
        try {
            uv3.c().b(wu3.f());
            synchronized (this) {
                a(wu3);
                wu3.g();
                wu3.n();
            }
        } catch (SocketException e2) {
            uv3 c2 = uv3.c();
            c2.a("Unable to untagSocket(): " + e2);
            wv3.a(wu3.f());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0074, code lost:
        r0 = r2;
     */
    @DexIgnore
    public synchronized wu3 a(pu3 pu3) {
        wu3 wu3;
        wu3 = null;
        ListIterator<wu3> listIterator = this.c.listIterator(this.c.size());
        while (true) {
            if (!listIterator.hasPrevious()) {
                break;
            }
            wu3 previous = listIterator.previous();
            if (previous.e().a().equals(pu3) && previous.h()) {
                if (System.nanoTime() - previous.c() < this.b) {
                    listIterator.remove();
                    if (previous.j()) {
                        break;
                    }
                    try {
                        uv3.c().a(previous.f());
                        break;
                    } catch (SocketException e2) {
                        wv3.a(previous.f());
                        uv3 c2 = uv3.c();
                        c2.a("Unable to tagSocket(): " + e2);
                    }
                }
            }
        }
        if (wu3 != null && wu3.j()) {
            this.c.addFirst(wu3);
        }
        return wu3;
    }

    @DexIgnore
    public void c(wu3 wu3) {
        if (!wu3.j()) {
            throw new IllegalArgumentException();
        } else if (wu3.h()) {
            synchronized (this) {
                a(wu3);
            }
        }
    }

    @DexIgnore
    public final void b() {
        do {
        } while (a());
    }

    @DexIgnore
    public final void a(wu3 wu3) {
        boolean isEmpty = this.c.isEmpty();
        this.c.addFirst(wu3);
        if (isEmpty) {
            this.d.execute(this.e);
        } else {
            notifyAll();
        }
    }

    @DexIgnore
    public boolean a() {
        synchronized (this) {
            if (this.c.isEmpty()) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            long nanoTime = System.nanoTime();
            long j = this.b;
            ListIterator<wu3> listIterator = this.c.listIterator(this.c.size());
            long j2 = j;
            int i = 0;
            while (listIterator.hasPrevious()) {
                wu3 previous = listIterator.previous();
                long c2 = (previous.c() + this.b) - nanoTime;
                if (c2 > 0) {
                    if (previous.h()) {
                        if (previous.k()) {
                            i++;
                            j2 = Math.min(j2, c2);
                        }
                    }
                }
                listIterator.remove();
                arrayList.add(previous);
            }
            ListIterator<wu3> listIterator2 = this.c.listIterator(this.c.size());
            while (listIterator2.hasPrevious() && i > this.a) {
                wu3 previous2 = listIterator2.previous();
                if (previous2.k()) {
                    arrayList.add(previous2);
                    listIterator2.remove();
                    i--;
                }
            }
            if (arrayList.isEmpty()) {
                try {
                    long j3 = j2 / 1000000;
                    Long.signum(j3);
                    wait(j3, (int) (j2 - (1000000 * j3)));
                    return true;
                } catch (InterruptedException unused) {
                    int size = arrayList.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        wv3.a(((wu3) arrayList.get(i2)).f());
                    }
                    return true;
                }
            }
        }
    }
}
