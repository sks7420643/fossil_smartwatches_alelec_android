package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e91<K> implements Iterator<Map.Entry<K, Object>> {
    @DexIgnore
    public Iterator<Map.Entry<K, Object>> e;

    @DexIgnore
    public e91(Iterator<Map.Entry<K, Object>> it) {
        this.e = it;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        Map.Entry next = this.e.next();
        return next.getValue() instanceof a91 ? new d91(next) : next;
    }

    @DexIgnore
    public final void remove() {
        this.e.remove();
    }
}
