package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d33 {
    @DexIgnore
    public /* final */ u23 a;

    @DexIgnore
    public d33(u23 u23) {
        kd4.b(u23, "mCommuteTimeSettingsContractView");
        this.a = u23;
    }

    @DexIgnore
    public final u23 a() {
        return this.a;
    }
}
