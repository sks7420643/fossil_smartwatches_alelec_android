package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vl1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vl1> CREATOR; // = new wl1();
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public kl1 g;
    @DexIgnore
    public long h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public String j;
    @DexIgnore
    public hg1 k;
    @DexIgnore
    public long l;
    @DexIgnore
    public hg1 m;
    @DexIgnore
    public long n;
    @DexIgnore
    public hg1 o;

    @DexIgnore
    public vl1(vl1 vl1) {
        bk0.a(vl1);
        this.e = vl1.e;
        this.f = vl1.f;
        this.g = vl1.g;
        this.h = vl1.h;
        this.i = vl1.i;
        this.j = vl1.j;
        this.k = vl1.k;
        this.l = vl1.l;
        this.m = vl1.m;
        this.n = vl1.n;
        this.o = vl1.o;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e, false);
        kk0.a(parcel, 3, this.f, false);
        kk0.a(parcel, 4, (Parcelable) this.g, i2, false);
        kk0.a(parcel, 5, this.h);
        kk0.a(parcel, 6, this.i);
        kk0.a(parcel, 7, this.j, false);
        kk0.a(parcel, 8, (Parcelable) this.k, i2, false);
        kk0.a(parcel, 9, this.l);
        kk0.a(parcel, 10, (Parcelable) this.m, i2, false);
        kk0.a(parcel, 11, this.n);
        kk0.a(parcel, 12, (Parcelable) this.o, i2, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public vl1(String str, String str2, kl1 kl1, long j2, boolean z, String str3, hg1 hg1, long j3, hg1 hg12, long j4, hg1 hg13) {
        this.e = str;
        this.f = str2;
        this.g = kl1;
        this.h = j2;
        this.i = z;
        this.j = str3;
        this.k = hg1;
        this.l = j3;
        this.m = hg12;
        this.n = j4;
        this.o = hg13;
    }
}
