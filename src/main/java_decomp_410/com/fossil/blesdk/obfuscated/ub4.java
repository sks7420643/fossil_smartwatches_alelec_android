package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ub4 {
    @DexIgnore
    public static final <T> Set<T> a(T t) {
        Set<T> singleton = Collections.singleton(t);
        kd4.a((Object) singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }
}
