package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nw */
public class C2508nw extends java.io.InputStream {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.util.Queue<com.fossil.blesdk.obfuscated.C2508nw> f7887g; // = com.fossil.blesdk.obfuscated.C3066uw.m14928a(0);

    @DexIgnore
    /* renamed from: e */
    public java.io.InputStream f7888e;

    @DexIgnore
    /* renamed from: f */
    public java.io.IOException f7889f;

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2508nw m11476b(java.io.InputStream inputStream) {
        com.fossil.blesdk.obfuscated.C2508nw poll;
        synchronized (f7887g) {
            poll = f7887g.poll();
        }
        if (poll == null) {
            poll = new com.fossil.blesdk.obfuscated.C2508nw();
        }
        poll.mo14130a(inputStream);
        return poll;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14130a(java.io.InputStream inputStream) {
        this.f7888e = inputStream;
    }

    @DexIgnore
    public int available() throws java.io.IOException {
        return this.f7888e.available();
    }

    @DexIgnore
    public void close() throws java.io.IOException {
        this.f7888e.close();
    }

    @DexIgnore
    public void mark(int i) {
        this.f7888e.mark(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.f7888e.markSupported();
    }

    @DexIgnore
    public int read(byte[] bArr) {
        try {
            return this.f7888e.read(bArr);
        } catch (java.io.IOException e) {
            this.f7889f = e;
            return -1;
        }
    }

    @DexIgnore
    public synchronized void reset() throws java.io.IOException {
        this.f7888e.reset();
    }

    @DexIgnore
    public long skip(long j) {
        try {
            return this.f7888e.skip(j);
        } catch (java.io.IOException e) {
            this.f7889f = e;
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: y */
    public java.io.IOException mo14140y() {
        return this.f7889f;
    }

    @DexIgnore
    /* renamed from: z */
    public void mo14141z() {
        this.f7889f = null;
        this.f7888e = null;
        synchronized (f7887g) {
            f7887g.offer(this);
        }
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.f7888e.read(bArr, i, i2);
        } catch (java.io.IOException e) {
            this.f7889f = e;
            return -1;
        }
    }

    @DexIgnore
    public int read() {
        try {
            return this.f7888e.read();
        } catch (java.io.IOException e) {
            this.f7889f = e;
            return -1;
        }
    }
}
