package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface gw3 {
    @DexIgnore
    public static final gw3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements gw3 {
        @DexIgnore
        public void a(bw3 bw3) throws IOException {
            bw3.a(ErrorCode.REFUSED_STREAM);
        }
    }

    @DexIgnore
    void a(bw3 bw3) throws IOException;
}
