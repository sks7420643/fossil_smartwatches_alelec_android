package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomPageIndicator q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ RecyclerViewPager t;
    @DexIgnore
    public /* final */ FlexibleTextView u;

    @DexIgnore
    public uf2(Object obj, View view, int i, CustomPageIndicator customPageIndicator, FlexibleTextView flexibleTextView, ImageView imageView, RecyclerViewPager recyclerViewPager, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = customPageIndicator;
        this.r = flexibleTextView;
        this.s = imageView;
        this.t = recyclerViewPager;
        this.u = flexibleTextView2;
    }
}
