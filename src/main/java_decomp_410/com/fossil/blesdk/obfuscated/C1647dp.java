package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dp */
public class C1647dp {
    @DexIgnore
    /* renamed from: a */
    public boolean mo10089a(java.io.File file) {
        return file.exists();
    }

    @DexIgnore
    /* renamed from: b */
    public long mo10090b(java.io.File file) {
        return file.length();
    }

    @DexIgnore
    /* renamed from: a */
    public java.io.File mo10088a(java.lang.String str) {
        return new java.io.File(str);
    }
}
