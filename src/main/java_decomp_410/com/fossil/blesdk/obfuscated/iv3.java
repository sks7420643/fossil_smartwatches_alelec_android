package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class iv3 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends iv3 {
        @DexIgnore
        public /* final */ /* synthetic */ fv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ ByteString b;

        @DexIgnore
        public a(fv3 fv3, ByteString byteString) {
            this.a = fv3;
            this.b = byteString;
        }

        @DexIgnore
        public long contentLength() throws IOException {
            return (long) this.b.size();
        }

        @DexIgnore
        public fv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(ko4 ko4) throws IOException {
            ko4.a(this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends iv3 {
        @DexIgnore
        public /* final */ /* synthetic */ fv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public b(fv3 fv3, int i, byte[] bArr, int i2) {
            this.a = fv3;
            this.b = i;
            this.c = bArr;
            this.d = i2;
        }

        @DexIgnore
        public long contentLength() {
            return (long) this.b;
        }

        @DexIgnore
        public fv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(ko4 ko4) throws IOException {
            ko4.write(this.c, this.d, this.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends iv3 {
        @DexIgnore
        public /* final */ /* synthetic */ fv3 a;
        @DexIgnore
        public /* final */ /* synthetic */ File b;

        @DexIgnore
        public c(fv3 fv3, File file) {
            this.a = fv3;
            this.b = file;
        }

        @DexIgnore
        public long contentLength() {
            return this.b.length();
        }

        @DexIgnore
        public fv3 contentType() {
            return this.a;
        }

        @DexIgnore
        public void writeTo(ko4 ko4) throws IOException {
            yo4 yo4 = null;
            try {
                yo4 = so4.c(this.b);
                ko4.a(yo4);
            } finally {
                wv3.a((Closeable) yo4);
            }
        }
    }

    @DexIgnore
    public static iv3 create(fv3 fv3, String str) {
        Charset charset = wv3.c;
        if (fv3 != null) {
            charset = fv3.a();
            if (charset == null) {
                charset = wv3.c;
                fv3 = fv3.a(fv3 + "; charset=utf-8");
            }
        }
        return create(fv3, str.getBytes(charset));
    }

    @DexIgnore
    public abstract long contentLength() throws IOException;

    @DexIgnore
    public abstract fv3 contentType();

    @DexIgnore
    public abstract void writeTo(ko4 ko4) throws IOException;

    @DexIgnore
    public static iv3 create(fv3 fv3, ByteString byteString) {
        return new a(fv3, byteString);
    }

    @DexIgnore
    public static iv3 create(fv3 fv3, byte[] bArr) {
        return create(fv3, bArr, 0, bArr.length);
    }

    @DexIgnore
    public static iv3 create(fv3 fv3, byte[] bArr, int i, int i2) {
        if (bArr != null) {
            wv3.a((long) bArr.length, (long) i, (long) i2);
            return new b(fv3, i2, bArr, i);
        }
        throw new NullPointerException("content == null");
    }

    @DexIgnore
    public static iv3 create(fv3 fv3, File file) {
        if (file != null) {
            return new c(fv3, file);
        }
        throw new NullPointerException("content == null");
    }
}
