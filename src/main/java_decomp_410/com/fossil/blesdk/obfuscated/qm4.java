package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qm4 {
    @DexIgnore
    mm4 a(Response response) throws IOException;

    @DexIgnore
    void a();

    @DexIgnore
    void a(dm4 dm4) throws IOException;

    @DexIgnore
    void a(nm4 nm4);

    @DexIgnore
    void a(Response response, Response response2);

    @DexIgnore
    Response b(dm4 dm4) throws IOException;
}
