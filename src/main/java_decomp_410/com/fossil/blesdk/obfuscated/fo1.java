package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ eo1 f;

    @DexIgnore
    public fo1(eo1 eo1, wn1 wn1) {
        this.f = eo1;
        this.e = wn1;
    }

    @DexIgnore
    public final void run() {
        try {
            wn1 wn1 = (wn1) this.f.b.then(this.e);
            if (wn1 == null) {
                this.f.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            wn1.a(yn1.b, this.f);
            wn1.a(yn1.b, (sn1) this.f);
            wn1.a(yn1.b, (qn1) this.f);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.c.a((Exception) e2.getCause());
            } else {
                this.f.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.f.c.a(e3);
        }
    }
}
