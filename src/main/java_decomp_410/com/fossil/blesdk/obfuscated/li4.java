package com.fossil.blesdk.obfuscated;

import com.facebook.internal.AnalyticsEvents;
import com.fossil.blesdk.obfuscated.fi4;
import com.fossil.blesdk.obfuscated.sj4;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.CompletionHandlerException;
import kotlinx.coroutines.JobCancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class li4 implements fi4, kg4, ti4, zk4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e; // = AtomicReferenceFieldUpdater.newUpdater(li4.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;
    @DexIgnore
    public volatile ig4 parentHandle;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> extends eg4<T> {
        @DexIgnore
        public /* final */ li4 l;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(yb4<? super T> yb4, li4 li4) {
            super(yb4, 1);
            kd4.b(yb4, "delegate");
            kd4.b(li4, "job");
            this.l = li4;
        }

        @DexIgnore
        public Throwable a(fi4 fi4) {
            kd4.b(fi4, "parent");
            Object d = this.l.d();
            if (d instanceof c) {
                Throwable th = ((c) d).rootCause;
                if (th != null) {
                    return th;
                }
            }
            if (d instanceof ng4) {
                return ((ng4) d).a;
            }
            return fi4.y();
        }

        @DexIgnore
        public String i() {
            return "AwaitContinuation";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ki4<fi4> {
        @DexIgnore
        public /* final */ li4 i;
        @DexIgnore
        public /* final */ c j;
        @DexIgnore
        public /* final */ jg4 k;
        @DexIgnore
        public /* final */ Object l;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(li4 li4, c cVar, jg4 jg4, Object obj) {
            super(jg4.i);
            kd4.b(li4, "parent");
            kd4.b(cVar, "state");
            kd4.b(jg4, "child");
            this.i = li4;
            this.j = cVar;
            this.k = jg4;
            this.l = obj;
        }

        @DexIgnore
        public void b(Throwable th) {
            this.i.a(this.j, this.k, this.l);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            b((Throwable) obj);
            return qa4.a;
        }

        @DexIgnore
        public String toString() {
            return "ChildCompletion[" + this.k + ", " + this.l + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ai4 {
        @DexIgnore
        public volatile Object _exceptionsHolder;
        @DexIgnore
        public /* final */ qi4 e;
        @DexIgnore
        public volatile boolean isCompleting;
        @DexIgnore
        public volatile Throwable rootCause;

        @DexIgnore
        public c(qi4 qi4, boolean z, Throwable th) {
            kd4.b(qi4, "list");
            this.e = qi4;
            this.isCompleting = z;
            this.rootCause = th;
        }

        @DexIgnore
        public qi4 a() {
            return this.e;
        }

        @DexIgnore
        public final List<Throwable> b(Throwable th) {
            ArrayList<Throwable> arrayList;
            Object obj = this._exceptionsHolder;
            if (obj == null) {
                arrayList = b();
            } else if (obj instanceof Throwable) {
                ArrayList<Throwable> b = b();
                b.add(obj);
                arrayList = b;
            } else if (obj instanceof ArrayList) {
                arrayList = (ArrayList) obj;
            } else {
                throw new IllegalStateException(("State is " + obj).toString());
            }
            Throwable th2 = this.rootCause;
            if (th2 != null) {
                arrayList.add(0, th2);
            }
            if (th != null && (!kd4.a((Object) th, (Object) th2))) {
                arrayList.add(th);
            }
            this._exceptionsHolder = mi4.a;
            return arrayList;
        }

        @DexIgnore
        public final boolean c() {
            return this.rootCause != null;
        }

        @DexIgnore
        public final boolean d() {
            return this._exceptionsHolder == mi4.a;
        }

        @DexIgnore
        public boolean isActive() {
            return this.rootCause == null;
        }

        @DexIgnore
        public String toString() {
            return "Finishing[cancelling=" + c() + ", completing=" + this.isCompleting + ", rootCause=" + this.rootCause + ", exceptions=" + this._exceptionsHolder + ", list=" + a() + ']';
        }

        @DexIgnore
        public final void a(Throwable th) {
            kd4.b(th, "exception");
            Throwable th2 = this.rootCause;
            if (th2 == null) {
                this.rootCause = th;
            } else if (th != th2) {
                Object obj = this._exceptionsHolder;
                if (obj == null) {
                    this._exceptionsHolder = th;
                } else if (obj instanceof Throwable) {
                    if (th != obj) {
                        ArrayList<Throwable> b = b();
                        b.add(obj);
                        b.add(th);
                        this._exceptionsHolder = b;
                    }
                } else if (obj instanceof ArrayList) {
                    ((ArrayList) obj).add(th);
                } else {
                    throw new IllegalStateException(("State is " + obj).toString());
                }
            }
        }

        @DexIgnore
        public final ArrayList<Throwable> b() {
            return new ArrayList<>(4);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sj4.a {
        @DexIgnore
        public /* final */ /* synthetic */ li4 d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(sj4 sj4, sj4 sj42, li4 li4, Object obj) {
            super(sj42);
            this.d = li4;
            this.e = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(sj4 sj4) {
            kd4.b(sj4, "affected");
            if (this.d.d() == this.e) {
                return null;
            }
            return rj4.a();
        }
    }

    @DexIgnore
    public li4(boolean z) {
        this._state = z ? mi4.c : mi4.b;
    }

    @DexIgnore
    public void a(Object obj, int i) {
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public final boolean b(ai4 ai4, Object obj, int i) {
        if (ch4.a()) {
            if (!((ai4 instanceof rh4) || (ai4 instanceof ki4))) {
                throw new AssertionError();
            }
        }
        if (ch4.a() && !(!(obj instanceof ng4))) {
            throw new AssertionError();
        } else if (!e.compareAndSet(this, ai4, mi4.a(obj))) {
            return false;
        } else {
            g((Throwable) null);
            g(obj);
            a(ai4, obj, i);
            return true;
        }
    }

    @DexIgnore
    public boolean c() {
        return false;
    }

    @DexIgnore
    public final boolean c(Throwable th) {
        if (f()) {
            return true;
        }
        boolean z = th instanceof CancellationException;
        ig4 ig4 = this.parentHandle;
        if (ig4 == null || ig4 == ri4.e) {
            return z;
        }
        if (ig4.a(th) || z) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public /* synthetic */ void cancel() {
        a((CancellationException) null);
    }

    @DexIgnore
    public boolean d(Throwable th) {
        kd4.b(th, "cause");
        if (th instanceof CancellationException) {
            return true;
        }
        if (!a((Object) th) || !b()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean e() {
        return !(d() instanceof ai4);
    }

    @DexIgnore
    public boolean e(Throwable th) {
        kd4.b(th, "exception");
        return false;
    }

    @DexIgnore
    public void f(Throwable th) {
        kd4.b(th, "exception");
        throw th;
    }

    @DexIgnore
    public boolean f() {
        return false;
    }

    @DexIgnore
    public <R> R fold(R r, yc4<? super R, ? super CoroutineContext.a, ? extends R> yc4) {
        kd4.b(yc4, "operation");
        return fi4.a.a(this, r, yc4);
    }

    @DexIgnore
    public String g() {
        return dh4.a((Object) this);
    }

    @DexIgnore
    public void g(Object obj) {
    }

    @DexIgnore
    public void g(Throwable th) {
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        kd4.b(bVar, "key");
        return fi4.a.a((fi4) this, bVar);
    }

    @DexIgnore
    public final CoroutineContext.b<?> getKey() {
        return fi4.d;
    }

    @DexIgnore
    public final int h(Object obj) {
        if (obj instanceof rh4) {
            if (((rh4) obj).isActive()) {
                return 0;
            }
            if (!e.compareAndSet(this, obj, mi4.c)) {
                return -1;
            }
            h();
            return 1;
        } else if (!(obj instanceof zh4)) {
            return 0;
        } else {
            if (!e.compareAndSet(this, obj, ((zh4) obj).a())) {
                return -1;
            }
            h();
            return 1;
        }
    }

    @DexIgnore
    public void h() {
    }

    @DexIgnore
    public final String i() {
        return g() + '{' + i(d()) + '}';
    }

    @DexIgnore
    public boolean isActive() {
        Object d2 = d();
        return (d2 instanceof ai4) && ((ai4) d2).isActive();
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        kd4.b(bVar, "key");
        return fi4.a.b(this, bVar);
    }

    @DexIgnore
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        return fi4.a.a((fi4) this, coroutineContext);
    }

    @DexIgnore
    public final boolean start() {
        int h;
        do {
            h = h(d());
            if (h == 0) {
                return false;
            }
        } while (h != 1);
        return true;
    }

    @DexIgnore
    public String toString() {
        return i() + '@' + dh4.b(this);
    }

    @DexIgnore
    public final CancellationException y() {
        Object d2 = d();
        if (d2 instanceof c) {
            Throwable th = ((c) d2).rootCause;
            if (th != null) {
                CancellationException a2 = a(th, dh4.a((Object) this) + " is cancelling");
                if (a2 != null) {
                    return a2;
                }
            }
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (d2 instanceof ai4) {
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (d2 instanceof ng4) {
            return a(this, ((ng4) d2).a, (String) null, 1, (Object) null);
        } else {
            return new JobCancellationException(dh4.a((Object) this) + " has completed normally", (Throwable) null, this);
        }
    }

    @DexIgnore
    public CancellationException z() {
        Throwable th;
        Object d2 = d();
        CancellationException cancellationException = null;
        if (d2 instanceof c) {
            th = ((c) d2).rootCause;
        } else if (d2 instanceof ng4) {
            th = ((ng4) d2).a;
        } else if (!(d2 instanceof ai4)) {
            th = null;
        } else {
            throw new IllegalStateException(("Cannot be cancelling child in this state: " + d2).toString());
        }
        if (th instanceof CancellationException) {
            cancellationException = th;
        }
        CancellationException cancellationException2 = cancellationException;
        if (cancellationException2 != null) {
            return cancellationException2;
        }
        return new JobCancellationException("Parent job is " + i(d2), th, this);
    }

    @DexIgnore
    public final void a(fi4 fi4) {
        if (ch4.a()) {
            if (!(this.parentHandle == null)) {
                throw new AssertionError();
            }
        }
        if (fi4 == null) {
            this.parentHandle = ri4.e;
            return;
        }
        fi4.start();
        ig4 a2 = fi4.a((kg4) this);
        this.parentHandle = a2;
        if (e()) {
            a2.dispose();
            this.parentHandle = ri4.e;
        }
    }

    @DexIgnore
    public final Throwable e(Object obj) {
        if (!(obj instanceof ng4)) {
            obj = null;
        }
        ng4 ng4 = (ng4) obj;
        if (ng4 != null) {
            return ng4.a;
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003c, code lost:
        if (r8 == null) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        a(((com.fossil.blesdk.obfuscated.li4.c) r2).a(), r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0047, code lost:
        return true;
     */
    @DexIgnore
    public final boolean f(Object obj) {
        Throwable th = null;
        while (true) {
            Object d2 = d();
            if (d2 instanceof c) {
                synchronized (d2) {
                    if (((c) d2).d()) {
                        return false;
                    }
                    boolean c2 = ((c) d2).c();
                    if (obj != null || !c2) {
                        if (th == null) {
                            th = d(obj);
                        }
                        ((c) d2).a(th);
                    }
                    Throwable th2 = ((c) d2).rootCause;
                    if (!(!c2)) {
                        th2 = null;
                    }
                }
            } else if (!(d2 instanceof ai4)) {
                return false;
            } else {
                if (th == null) {
                    th = d(obj);
                }
                ai4 ai4 = (ai4) d2;
                if (!ai4.isActive()) {
                    int a2 = a(d2, (Object) new ng4(th, false, 2, (fd4) null), 0);
                    if (a2 == 0) {
                        throw new IllegalStateException(("Cannot happen in " + d2).toString());
                    } else if (a2 == 1 || a2 == 2) {
                        return true;
                    } else {
                        if (a2 != 3) {
                            throw new IllegalStateException("unexpected result".toString());
                        }
                    }
                } else if (a(ai4, th)) {
                    return true;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final String i(Object obj) {
        if (obj instanceof c) {
            c cVar = (c) obj;
            if (cVar.c()) {
                return "Cancelling";
            }
            if (cVar.isCompleting) {
                return "Completing";
            }
            return "Active";
        } else if (!(obj instanceof ai4)) {
            return obj instanceof ng4 ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_CANCELLED : AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_COMPLETED;
        } else {
            if (((ai4) obj).isActive()) {
                return "Active";
            }
            return "New";
        }
    }

    @DexIgnore
    public final Throwable d(Object obj) {
        if (obj != null ? obj instanceof Throwable : true) {
            return obj != null ? (Throwable) obj : a();
        }
        if (obj != null) {
            return ((ti4) obj).z();
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
    }

    @DexIgnore
    public final Object d() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof yj4)) {
                return obj;
            }
            ((yj4) obj).a(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0054, code lost:
        if (r3 == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0056, code lost:
        a(r0, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0059, code lost:
        r8 = a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x005d, code lost:
        if (r8 == null) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0063, code lost:
        if (b(r2, r8, r9) == false) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0065, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x006b, code lost:
        if (a(r2, r9, r10) == false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x006d, code lost:
        return 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x006e, code lost:
        return 3;
     */
    @DexIgnore
    public final int c(ai4 ai4, Object obj, int i) {
        qi4 b2 = b(ai4);
        if (b2 == null) {
            return 3;
        }
        Throwable th = null;
        c cVar = (c) (!(ai4 instanceof c) ? null : ai4);
        if (cVar == null) {
            cVar = new c(b2, false, (Throwable) null);
        }
        synchronized (cVar) {
            if (cVar.isCompleting) {
                return 0;
            }
            cVar.isCompleting = true;
            if (cVar != ai4 && !e.compareAndSet(this, ai4, cVar)) {
                return 3;
            }
            if (!cVar.d()) {
                boolean c2 = cVar.c();
                ng4 ng4 = (ng4) (!(obj instanceof ng4) ? null : obj);
                if (ng4 != null) {
                    cVar.a(ng4.a);
                }
                Throwable th2 = cVar.rootCause;
                if (!c2) {
                    th = th2;
                }
                qa4 qa4 = qa4.a;
            } else {
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
        }
    }

    @DexIgnore
    public boolean b(Throwable th) {
        return a((Object) th) && b();
    }

    @DexIgnore
    public final qi4 b(ai4 ai4) {
        qi4 a2 = ai4.a();
        if (a2 != null) {
            return a2;
        }
        if (ai4 instanceof rh4) {
            return new qi4();
        }
        if (ai4 instanceof ki4) {
            a((ki4<?>) (ki4) ai4);
            return null;
        }
        throw new IllegalStateException(("State should have list: " + ai4).toString());
    }

    @DexIgnore
    public final boolean a(c cVar, Object obj, int i) {
        boolean c2;
        Throwable a2;
        boolean z = false;
        if (!(d() == cVar)) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!(!cVar.d())) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (cVar.isCompleting) {
            ng4 ng4 = (ng4) (!(obj instanceof ng4) ? null : obj);
            Throwable th = ng4 != null ? ng4.a : null;
            synchronized (cVar) {
                c2 = cVar.c();
                List<Throwable> b2 = cVar.b(th);
                a2 = a(cVar, (List<? extends Throwable>) b2);
                if (a2 != null) {
                    a(a2, (List<? extends Throwable>) b2);
                }
            }
            if (!(a2 == null || a2 == th)) {
                obj = new ng4(a2, false, 2, (fd4) null);
            }
            if (a2 != null) {
                if (c(a2) || e(a2)) {
                    z = true;
                }
                if (z) {
                    if (obj != null) {
                        ((ng4) obj).b();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                    }
                }
            }
            if (!c2) {
                g(a2);
            }
            g(obj);
            if (e.compareAndSet(this, cVar, mi4.a(obj))) {
                a((ai4) cVar, obj, i);
                return true;
            }
            throw new IllegalArgumentException(("Unexpected state: " + this._state + ", expected: " + cVar + ", update: " + obj).toString());
        } else {
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    @DexIgnore
    public final boolean b(c cVar, jg4 jg4, Object obj) {
        while (fi4.a.a(jg4.i, false, false, new b(this, cVar, jg4, obj), 1, (Object) null) == ri4.e) {
            jg4 = a((sj4) jg4);
            if (jg4 == null) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Object b(yb4<Object> yb4) {
        Object d2;
        do {
            d2 = d();
            if (!(d2 instanceof ai4)) {
                if (!(d2 instanceof ng4)) {
                    return mi4.b(d2);
                }
                Throwable th = ((ng4) d2).a;
                if (ch4.d()) {
                    id4.a(0);
                    if (!(yb4 instanceof fc4)) {
                        throw th;
                    }
                    throw ck4.b(th, (fc4) yb4);
                }
                throw th;
            }
        } while (h(d2) < 0);
        return c(yb4);
    }

    @DexIgnore
    public final /* synthetic */ Object c(yb4<Object> yb4) {
        a aVar = new a(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), this);
        fg4.a((dg4<?>) aVar, a((xc4<? super Throwable, qa4>) new vi4(this, aVar)));
        Object e2 = aVar.e();
        if (e2 == cc4.a()) {
            ic4.c(yb4);
        }
        return e2;
    }

    @DexIgnore
    public final boolean c(Object obj) {
        int a2;
        do {
            Object d2 = d();
            if ((d2 instanceof ai4) && (!(d2 instanceof c) || !((c) d2).isCompleting)) {
                a2 = a(d2, (Object) new ng4(d(obj), false, 2, (fd4) null), 0);
                if (a2 != 0) {
                    if (a2 == 1 || a2 == 2) {
                        return true;
                    }
                }
            }
            return false;
        } while (a2 == 3);
        throw new IllegalStateException("unexpected result".toString());
    }

    @DexIgnore
    public final void b(qi4 qi4, Throwable th) {
        Object c2 = qi4.c();
        if (c2 != null) {
            CompletionHandlerException completionHandlerException = null;
            for (sj4 sj4 = (sj4) c2; !kd4.a((Object) sj4, (Object) qi4); sj4 = sj4.d()) {
                if (sj4 instanceof ki4) {
                    ki4 ki4 = (ki4) sj4;
                    try {
                        ki4.b(th);
                    } catch (Throwable th2) {
                        if (completionHandlerException != null) {
                            ja4.a(completionHandlerException, th2);
                            if (completionHandlerException != null) {
                            }
                        }
                        completionHandlerException = new CompletionHandlerException("Exception in completion handler " + ki4 + " for " + this, th2);
                        qa4 qa4 = qa4.a;
                    }
                }
            }
            if (completionHandlerException != null) {
                f((Throwable) completionHandlerException);
                return;
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final Throwable a(c cVar, List<? extends Throwable> list) {
        T t;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (!(((Throwable) t) instanceof CancellationException)) {
                    break;
                }
            }
            Throwable th = (Throwable) t;
            return th != null ? th : (Throwable) list.get(0);
        } else if (cVar.c()) {
            return a();
        } else {
            return null;
        }
    }

    @DexIgnore
    public final void a(Throwable th, List<? extends Throwable> list) {
        if (list.size() > 1) {
            Set a2 = nj4.a(list.size());
            Throwable b2 = ck4.b(th);
            for (Throwable b3 : list) {
                Throwable b4 = ck4.b(b3);
                if (b4 != th && b4 != b2 && !(b4 instanceof CancellationException) && a2.add(b4)) {
                    ja4.a(th, b4);
                }
            }
        }
    }

    @DexIgnore
    public final void b(ki4<?> ki4) {
        Object d2;
        kd4.b(ki4, "node");
        do {
            d2 = d();
            if (d2 instanceof ki4) {
                if (d2 != ki4) {
                    return;
                }
            } else if ((d2 instanceof ai4) && ((ai4) d2).a() != null) {
                ki4.j();
                return;
            } else {
                return;
            }
        } while (!e.compareAndSet(this, d2, mi4.c));
    }

    @DexIgnore
    public final void a(ai4 ai4, Object obj, int i) {
        ig4 ig4 = this.parentHandle;
        if (ig4 != null) {
            ig4.dispose();
            this.parentHandle = ri4.e;
        }
        Throwable th = null;
        ng4 ng4 = (ng4) (!(obj instanceof ng4) ? null : obj);
        if (ng4 != null) {
            th = ng4.a;
        }
        if (ai4 instanceof ki4) {
            try {
                ((ki4) ai4).b(th);
            } catch (Throwable th2) {
                f((Throwable) new CompletionHandlerException("Exception in completion handler " + ai4 + " for " + this, th2));
            }
        } else {
            qi4 a2 = ai4.a();
            if (a2 != null) {
                b(a2, th);
            }
        }
        a(obj, i);
    }

    @DexIgnore
    public final boolean b(Object obj, int i) {
        int a2;
        do {
            a2 = a(d(), obj, i);
            if (a2 == 0) {
                throw new IllegalStateException("Job " + this + " is already complete or completing, " + "but is being completed with " + obj, e(obj));
            } else if (a2 == 1) {
                return true;
            } else {
                if (a2 == 2) {
                    return false;
                }
            }
        } while (a2 == 3);
        throw new IllegalStateException("unexpected result".toString());
    }

    @DexIgnore
    public final void a(qi4 qi4, Throwable th) {
        g(th);
        Object c2 = qi4.c();
        if (c2 != null) {
            CompletionHandlerException completionHandlerException = null;
            for (sj4 sj4 = (sj4) c2; !kd4.a((Object) sj4, (Object) qi4); sj4 = sj4.d()) {
                if (sj4 instanceof gi4) {
                    ki4 ki4 = (ki4) sj4;
                    try {
                        ki4.b(th);
                    } catch (Throwable th2) {
                        if (completionHandlerException != null) {
                            ja4.a(completionHandlerException, th2);
                            if (completionHandlerException != null) {
                            }
                        }
                        completionHandlerException = new CompletionHandlerException("Exception in completion handler " + ki4 + " for " + this, th2);
                        qa4 qa4 = qa4.a;
                    }
                }
            }
            if (completionHandlerException != null) {
                f((Throwable) completionHandlerException);
            }
            c(th);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public static /* synthetic */ CancellationException a(li4 li4, Throwable th, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = null;
            }
            return li4.a(th, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }

    @DexIgnore
    public final CancellationException a(Throwable th, String str) {
        kd4.b(th, "$this$toCancellationException");
        CancellationException cancellationException = (CancellationException) (!(th instanceof CancellationException) ? null : th);
        if (cancellationException == null) {
            if (str == null) {
                str = dh4.a((Object) th) + " was cancelled";
            }
            cancellationException = new JobCancellationException(str, th, this);
        }
        return cancellationException;
    }

    @DexIgnore
    public final oh4 a(xc4<? super Throwable, qa4> xc4) {
        kd4.b(xc4, "handler");
        return a(false, true, xc4);
    }

    @DexIgnore
    public final ki4<?> a(xc4<? super Throwable, qa4> xc4, boolean z) {
        boolean z2 = true;
        gi4 gi4 = null;
        if (z) {
            if (xc4 instanceof gi4) {
                gi4 = xc4;
            }
            gi4 gi42 = gi4;
            if (gi42 != null) {
                if (gi42.h != this) {
                    z2 = false;
                }
                if (!z2) {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                } else if (gi42 != null) {
                    return gi42;
                }
            }
            return new di4(this, xc4);
        }
        if (xc4 instanceof ki4) {
            gi4 = xc4;
        }
        ki4<?> ki4 = gi4;
        if (ki4 != null) {
            if (ki4.h != this || (ki4 instanceof gi4)) {
                z2 = false;
            }
            if (!z2) {
                throw new IllegalArgumentException("Failed requirement.".toString());
            } else if (ki4 != null) {
                return ki4;
            }
        }
        return new ei4(this, xc4);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.fossil.blesdk.obfuscated.zh4] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(rh4 rh4) {
        qi4 qi4 = new qi4();
        if (!rh4.isActive()) {
            qi4 = new zh4(qi4);
        }
        e.compareAndSet(this, rh4, qi4);
    }

    @DexIgnore
    public final void a(ki4<?> ki4) {
        ki4.a(new qi4());
        e.compareAndSet(this, ki4, ki4.d());
    }

    @DexIgnore
    public void a(CancellationException cancellationException) {
        b((Throwable) cancellationException);
    }

    @DexIgnore
    public final void a(ti4 ti4) {
        kd4.b(ti4, "parentJob");
        a((Object) ti4);
    }

    @DexIgnore
    public final boolean a(Throwable th) {
        return a((Object) th);
    }

    @DexIgnore
    public final boolean a(Object obj) {
        if (!c() || !c(obj)) {
            return f(obj);
        }
        return true;
    }

    @DexIgnore
    public final JobCancellationException a() {
        return new JobCancellationException("Job was cancelled", (Throwable) null, this);
    }

    @DexIgnore
    public final boolean a(ai4 ai4, Throwable th) {
        if (ch4.a() && !(!(ai4 instanceof c))) {
            throw new AssertionError();
        } else if (!ch4.a() || ai4.isActive()) {
            qi4 b2 = b(ai4);
            if (b2 == null) {
                return false;
            }
            if (!e.compareAndSet(this, ai4, new c(b2, false, th))) {
                return false;
            }
            a(b2, th);
            return true;
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final int a(Object obj, Object obj2, int i) {
        if (!(obj instanceof ai4)) {
            return 0;
        }
        if (((obj instanceof rh4) || (obj instanceof ki4)) && !(obj instanceof jg4) && !(obj2 instanceof ng4)) {
            return !b((ai4) obj, obj2, i) ? 3 : 1;
        }
        return c((ai4) obj, obj2, i);
    }

    @DexIgnore
    public final jg4 a(ai4 ai4) {
        jg4 jg4 = (jg4) (!(ai4 instanceof jg4) ? null : ai4);
        if (jg4 != null) {
            return jg4;
        }
        qi4 a2 = ai4.a();
        if (a2 != null) {
            return a((sj4) a2);
        }
        return null;
    }

    @DexIgnore
    public final void a(c cVar, jg4 jg4, Object obj) {
        if (d() == cVar) {
            jg4 a2 = a((sj4) jg4);
            if ((a2 == null || !b(cVar, a2, obj)) && a(cVar, obj, 0)) {
            }
            return;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    public final jg4 a(sj4 sj4) {
        while (sj4.h()) {
            sj4 = sj4.f();
        }
        while (true) {
            sj4 = sj4.d();
            if (!sj4.h()) {
                if (sj4 instanceof jg4) {
                    return (jg4) sj4;
                }
                if (sj4 instanceof qi4) {
                    return null;
                }
            }
        }
    }

    @DexIgnore
    public final ig4 a(kg4 kg4) {
        kd4.b(kg4, "child");
        oh4 a2 = fi4.a.a(this, true, false, new jg4(this, kg4), 2, (Object) null);
        if (a2 != null) {
            return (ig4) a2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
    }

    @DexIgnore
    public final oh4 a(boolean z, boolean z2, xc4<? super Throwable, qa4> xc4) {
        Throwable th;
        kd4.b(xc4, "handler");
        Throwable th2 = null;
        ki4<?> ki4 = null;
        while (true) {
            Object d2 = d();
            if (d2 instanceof rh4) {
                rh4 rh4 = (rh4) d2;
                if (rh4.isActive()) {
                    if (ki4 == null) {
                        ki4 = a(xc4, z);
                    }
                    if (e.compareAndSet(this, d2, ki4)) {
                        return ki4;
                    }
                } else {
                    a(rh4);
                }
            } else if (d2 instanceof ai4) {
                qi4 a2 = ((ai4) d2).a();
                if (a2 != null) {
                    oh4 oh4 = ri4.e;
                    if (!z || !(d2 instanceof c)) {
                        th = null;
                    } else {
                        synchronized (d2) {
                            th = ((c) d2).rootCause;
                            if (th == null || ((xc4 instanceof jg4) && !((c) d2).isCompleting)) {
                                if (ki4 == null) {
                                    ki4 = a(xc4, z);
                                }
                                if (a(d2, a2, ki4)) {
                                    if (th == null) {
                                        return ki4;
                                    }
                                    oh4 = ki4;
                                }
                            }
                            qa4 qa4 = qa4.a;
                        }
                    }
                    if (th != null) {
                        if (z2) {
                            xc4.invoke(th);
                        }
                        return oh4;
                    }
                    if (ki4 == null) {
                        ki4 = a(xc4, z);
                    }
                    if (a(d2, a2, ki4)) {
                        return ki4;
                    }
                } else if (d2 != null) {
                    a((ki4<?>) (ki4) d2);
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>");
                }
            } else {
                if (z2) {
                    if (!(d2 instanceof ng4)) {
                        d2 = null;
                    }
                    ng4 ng4 = (ng4) d2;
                    if (ng4 != null) {
                        th2 = ng4.a;
                    }
                    xc4.invoke(th2);
                }
                return ri4.e;
            }
        }
    }

    @DexIgnore
    public final boolean a(Object obj, qi4 qi4, ki4<?> ki4) {
        int a2;
        d dVar = new d(ki4, ki4, this, obj);
        do {
            Object e2 = qi4.e();
            if (e2 != null) {
                a2 = ((sj4) e2).a(ki4, qi4, dVar);
                if (a2 == 1) {
                    return true;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
        } while (a2 != 2);
        return false;
    }
}
