package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e8 */
public class C1706e8 {
    @DexIgnore
    /* renamed from: a */
    public static boolean m6326a(java.lang.Object obj, java.lang.Object obj2) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return java.util.Objects.equals(obj, obj2);
        }
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m6325a(java.lang.Object... objArr) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return java.util.Objects.hash(objArr);
        }
        return java.util.Arrays.hashCode(objArr);
    }
}
