package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yx */
public class C3395yx extends com.fossil.blesdk.obfuscated.p64<com.crashlytics.android.answers.SessionEvent> {

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.j74 f11426g;

    @DexIgnore
    public C3395yx(android.content.Context context, com.fossil.blesdk.obfuscated.C1666dy dyVar, com.fossil.blesdk.obfuscated.n54 n54, com.fossil.blesdk.obfuscated.q64 q64) throws java.io.IOException {
        super(context, dyVar, n54, q64, 100);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18362a(com.fossil.blesdk.obfuscated.j74 j74) {
        this.f11426g = j74;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo18363c() {
        java.util.UUID randomUUID = java.util.UUID.randomUUID();
        return "sa" + "_" + randomUUID.toString() + "_" + this.f17563c.mo29580a() + ".tap";
    }

    @DexIgnore
    /* renamed from: e */
    public int mo18364e() {
        com.fossil.blesdk.obfuscated.j74 j74 = this.f11426g;
        return j74 == null ? super.mo18364e() : j74.f15936c;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo18365f() {
        com.fossil.blesdk.obfuscated.j74 j74 = this.f11426g;
        return j74 == null ? super.mo18365f() : j74.f15937d;
    }
}
