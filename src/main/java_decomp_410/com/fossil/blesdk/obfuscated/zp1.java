package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zp1 extends jk0 implements ip1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zp1> CREATOR; // = new aq1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ boolean h;

    @DexIgnore
    public zp1(String str, String str2, int i, boolean z) {
        this.e = str;
        this.f = str2;
        this.g = i;
        this.h = z;
    }

    @DexIgnore
    public final String H() {
        return this.f;
    }

    @DexIgnore
    public final String I() {
        return this.e;
    }

    @DexIgnore
    public final boolean J() {
        return this.h;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof zp1)) {
            return false;
        }
        return ((zp1) obj).e.equals(this.e);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final String toString() {
        String str = this.f;
        String str2 = this.e;
        int i = this.g;
        boolean z = this.h;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 45 + String.valueOf(str2).length());
        sb.append("Node{");
        sb.append(str);
        sb.append(", id=");
        sb.append(str2);
        sb.append(", hops=");
        sb.append(i);
        sb.append(", isNearby=");
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, I(), false);
        kk0.a(parcel, 3, H(), false);
        kk0.a(parcel, 4, this.g);
        kk0.a(parcel, 5, J());
        kk0.a(parcel, a);
    }
}
