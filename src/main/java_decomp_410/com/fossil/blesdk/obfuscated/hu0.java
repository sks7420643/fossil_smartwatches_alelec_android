package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;
import com.google.android.gms.internal.clearcut.zzbb;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hu0 extends gu0<ru0.d> {
    @DexIgnore
    public final int a(Map.Entry<?, ?> entry) {
        return ((ru0.d) entry.getKey()).e;
    }

    @DexIgnore
    public final ku0<ru0.d> a(Object obj) {
        return ((ru0.c) obj).zzjv;
    }

    @DexIgnore
    public final void a(ox0 ox0, Map.Entry<?, ?> entry) throws IOException {
        ru0.d dVar = (ru0.d) entry.getKey();
        switch (iu0.a[dVar.f.ordinal()]) {
            case 1:
                ox0.zza(dVar.e, ((Double) entry.getValue()).doubleValue());
                return;
            case 2:
                ox0.zza(dVar.e, ((Float) entry.getValue()).floatValue());
                return;
            case 3:
                ox0.b(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 4:
                ox0.zza(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 5:
                ox0.zzc(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 6:
                ox0.zzc(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 7:
                ox0.zzf(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 8:
                ox0.a(dVar.e, ((Boolean) entry.getValue()).booleanValue());
                return;
            case 9:
                ox0.zzd(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 10:
                ox0.b(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 11:
                ox0.a(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 12:
                ox0.zze(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 13:
                ox0.zzb(dVar.e, ((Long) entry.getValue()).longValue());
                return;
            case 14:
                ox0.zzc(dVar.e, ((Integer) entry.getValue()).intValue());
                return;
            case 15:
                ox0.a(dVar.e, (zzbb) entry.getValue());
                return;
            case 16:
                ox0.zza(dVar.e, (String) entry.getValue());
                return;
            case 17:
                ox0.b(dVar.e, (Object) entry.getValue(), (jw0) ew0.a().a(entry.getValue().getClass()));
                return;
            case 18:
                ox0.a(dVar.e, (Object) entry.getValue(), (jw0) ew0.a().a(entry.getValue().getClass()));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void a(Object obj, ku0<ru0.d> ku0) {
        ((ru0.c) obj).zzjv = ku0;
    }

    @DexIgnore
    public final boolean a(sv0 sv0) {
        return sv0 instanceof ru0.c;
    }

    @DexIgnore
    public final ku0<ru0.d> b(Object obj) {
        ku0<ru0.d> a = a(obj);
        if (!a.c()) {
            return a;
        }
        ku0<ru0.d> ku0 = (ku0) a.clone();
        a(obj, ku0);
        return ku0;
    }

    @DexIgnore
    public final void c(Object obj) {
        a(obj).h();
    }
}
