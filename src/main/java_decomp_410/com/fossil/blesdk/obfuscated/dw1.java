package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.FirebaseApp;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dw1 implements cw1 {
    @DexIgnore
    public static volatile cw1 b;
    @DexIgnore
    public /* final */ AppMeasurement a;

    @DexIgnore
    public dw1(AppMeasurement appMeasurement) {
        bk0.a(appMeasurement);
        this.a = appMeasurement;
        new ConcurrentHashMap();
    }

    @DexIgnore
    public static cw1 a(FirebaseApp firebaseApp, Context context, bx1 bx1) {
        bk0.a(firebaseApp);
        bk0.a(context);
        bk0.a(bx1);
        bk0.a(context.getApplicationContext());
        if (b == null) {
            synchronized (dw1.class) {
                if (b == null) {
                    Bundle bundle = new Bundle(1);
                    if (firebaseApp.d()) {
                        bx1.a(aw1.class, gw1.e, hw1.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", firebaseApp.isDataCollectionDefaultEnabled());
                    }
                    b = new dw1(xh1.a(context, og1.a(bundle)).y());
                }
            }
        }
        return b;
    }

    @DexIgnore
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (fw1.a(str) && fw1.a(str2, bundle) && fw1.a(str, str2, bundle)) {
            this.a.logEventInternal(str, str2, bundle);
        }
    }

    @DexIgnore
    public void a(String str, String str2, Object obj) {
        if (fw1.a(str) && fw1.a(str, str2)) {
            this.a.a(str, str2, obj);
        }
    }

    @DexIgnore
    public static final /* synthetic */ void a(yw1 yw1) {
        boolean z = ((aw1) yw1.a()).a;
        synchronized (dw1.class) {
            ((dw1) b).a.b(z);
        }
    }
}
