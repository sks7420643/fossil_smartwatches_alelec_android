package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q10 {
    @DexIgnore
    public static final JSONArray a(GattCharacteristic.CharacteristicId[] characteristicIdArr) {
        kd4.b(characteristicIdArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (GattCharacteristic.CharacteristicId logName$blesdk_productionRelease : characteristicIdArr) {
            jSONArray.put(logName$blesdk_productionRelease.getLogName$blesdk_productionRelease());
        }
        return jSONArray;
    }
}
