package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ba1 extends j61 implements g81 {
    @DexIgnore
    public ba1(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    public final Bundle zza(Bundle bundle) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) bundle);
        Parcel a = a(1, o);
        Bundle bundle2 = (Bundle) r61.a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
