package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nw extends InputStream {
    @DexIgnore
    public static /* final */ Queue<nw> g; // = uw.a(0);
    @DexIgnore
    public InputStream e;
    @DexIgnore
    public IOException f;

    @DexIgnore
    public static nw b(InputStream inputStream) {
        nw poll;
        synchronized (g) {
            poll = g.poll();
        }
        if (poll == null) {
            poll = new nw();
        }
        poll.a(inputStream);
        return poll;
    }

    @DexIgnore
    public void a(InputStream inputStream) {
        this.e = inputStream;
    }

    @DexIgnore
    public int available() throws IOException {
        return this.e.available();
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public void mark(int i) {
        this.e.mark(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.e.markSupported();
    }

    @DexIgnore
    public int read(byte[] bArr) {
        try {
            return this.e.read(bArr);
        } catch (IOException e2) {
            this.f = e2;
            return -1;
        }
    }

    @DexIgnore
    public synchronized void reset() throws IOException {
        this.e.reset();
    }

    @DexIgnore
    public long skip(long j) {
        try {
            return this.e.skip(j);
        } catch (IOException e2) {
            this.f = e2;
            return 0;
        }
    }

    @DexIgnore
    public IOException y() {
        return this.f;
    }

    @DexIgnore
    public void z() {
        this.f = null;
        this.e = null;
        synchronized (g) {
            g.offer(this);
        }
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.e.read(bArr, i, i2);
        } catch (IOException e2) {
            this.f = e2;
            return -1;
        }
    }

    @DexIgnore
    public int read() {
        try {
            return this.e.read();
        } catch (IOException e2) {
            this.f = e2;
            return -1;
        }
    }
}
