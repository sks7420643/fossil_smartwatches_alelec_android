package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ qj1 f;
    @DexIgnore
    public /* final */ /* synthetic */ qj1 g;
    @DexIgnore
    public /* final */ /* synthetic */ rj1 h;

    @DexIgnore
    public sj1(rj1 rj1, boolean z, qj1 qj1, qj1 qj12) {
        this.h = rj1;
        this.e = z;
        this.f = qj1;
        this.g = qj12;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0061, code lost:
        if (com.fossil.blesdk.obfuscated.nl1.e(r10.f.a, r10.g.a) != false) goto L_0x0064;
     */
    @DexIgnore
    public final void run() {
        boolean z;
        boolean z2 = false;
        if (this.h.l().s(this.h.p().B())) {
            z = this.e && this.h.c != null;
            if (z) {
                rj1 rj1 = this.h;
                rj1.a(rj1.c, true);
            }
        } else {
            if (this.e) {
                rj1 rj12 = this.h;
                qj1 qj1 = rj12.c;
                if (qj1 != null) {
                    rj12.a(qj1, true);
                }
            }
            z = false;
        }
        qj1 qj12 = this.f;
        if (qj12 != null) {
            long j = qj12.c;
            qj1 qj13 = this.g;
            if (j == qj13.c) {
                if (nl1.e(qj12.b, qj13.b)) {
                }
            }
        }
        z2 = true;
        if (z2) {
            Bundle bundle = new Bundle();
            rj1.a(this.g, bundle, true);
            qj1 qj14 = this.f;
            if (qj14 != null) {
                String str = qj14.a;
                if (str != null) {
                    bundle.putString("_pn", str);
                }
                bundle.putString("_pc", this.f.b);
                bundle.putLong("_pi", this.f.c);
            }
            if (this.h.l().s(this.h.p().B()) && z) {
                long C = this.h.t().C();
                if (C > 0) {
                    this.h.j().a(bundle, C);
                }
            }
            this.h.o().c("auto", "_vs", bundle);
        }
        rj1 rj13 = this.h;
        rj13.c = this.g;
        rj13.q().a(this.g);
    }
}
