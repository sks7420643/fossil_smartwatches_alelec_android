package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import com.portfolio.platform.enums.Unit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class lp2 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Unit.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[WeatherSettings.TEMP_UNIT.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[LocationSource.ErrorState.values().length];

    /*
    static {
        a[Unit.METRIC.ordinal()] = 1;
        a[Unit.IMPERIAL.ordinal()] = 2;
        b[WeatherSettings.TEMP_UNIT.CELSIUS.ordinal()] = 1;
        b[WeatherSettings.TEMP_UNIT.FAHRENHEIT.ordinal()] = 2;
        c[LocationSource.ErrorState.LOCATION_PERMISSION_OFF.ordinal()] = 1;
        c[LocationSource.ErrorState.BACKGROUND_PERMISSION_OFF.ordinal()] = 2;
        c[LocationSource.ErrorState.LOCATION_SERVICE_OFF.ordinal()] = 3;
    }
    */
}
