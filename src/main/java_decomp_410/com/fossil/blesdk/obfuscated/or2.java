package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class or2 implements Factory<nr2> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public or2(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static or2 a(Provider<UserRepository> provider) {
        return new or2(provider);
    }

    @DexIgnore
    public static nr2 b(Provider<UserRepository> provider) {
        return new nr2(provider.get());
    }

    @DexIgnore
    public nr2 get() {
        return b(this.a);
    }
}
