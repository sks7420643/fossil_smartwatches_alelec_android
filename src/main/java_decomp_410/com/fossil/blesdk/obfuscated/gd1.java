package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gd1 implements Parcelable.Creator<vc1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        Status status = null;
        wc1 wc1 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                status = (Status) SafeParcelReader.a(parcel, a, Status.CREATOR);
            } else if (a2 != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                wc1 = (wc1) SafeParcelReader.a(parcel, a, wc1.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new vc1(status, wc1);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new vc1[i];
    }
}
