package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x42 implements Factory<in2> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public x42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static x42 a(n42 n42) {
        return new x42(n42);
    }

    @DexIgnore
    public static in2 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static in2 c(n42 n42) {
        in2 f = n42.f();
        n44.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }

    @DexIgnore
    public in2 get() {
        return b(this.a);
    }
}
