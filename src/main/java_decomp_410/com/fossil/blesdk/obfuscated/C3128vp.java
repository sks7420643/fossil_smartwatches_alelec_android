package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vp */
public class C3128vp<Z> implements com.fossil.blesdk.obfuscated.C1438aq<Z> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ boolean f10351e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ boolean f10352f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C1438aq<Z> f10353g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3128vp.C3129a f10354h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f10355i;

    @DexIgnore
    /* renamed from: j */
    public int f10356j;

    @DexIgnore
    /* renamed from: k */
    public boolean f10357k;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.vp$a */
    public interface C3129a {
        @DexIgnore
        /* renamed from: a */
        void mo15336a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar);
    }

    @DexIgnore
    public C3128vp(com.fossil.blesdk.obfuscated.C1438aq<Z> aqVar, boolean z, boolean z2, com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(aqVar);
        this.f10353g = aqVar;
        this.f10351e = z;
        this.f10352f = z2;
        this.f10355i = joVar;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(aVar);
        this.f10354h = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo8887a() {
        if (this.f10356j > 0) {
            throw new java.lang.IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.f10357k) {
            this.f10357k = true;
            if (this.f10352f) {
                this.f10353g.mo8887a();
            }
        } else {
            throw new java.lang.IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return this.f10353g.mo8888b();
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<Z> mo8889c() {
        return this.f10353g.mo8889c();
    }

    @DexIgnore
    /* renamed from: d */
    public synchronized void mo17153d() {
        if (!this.f10357k) {
            this.f10356j++;
        } else {
            throw new java.lang.IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1438aq<Z> mo17154e() {
        return this.f10353g;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo17155f() {
        return this.f10351e;
    }

    @DexIgnore
    /* renamed from: g */
    public void mo17156g() {
        boolean z;
        synchronized (this) {
            if (this.f10356j > 0) {
                z = true;
                int i = this.f10356j - 1;
                this.f10356j = i;
                if (i != 0) {
                    z = false;
                }
            } else {
                throw new java.lang.IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.f10354h.mo15336a(this.f10355i, this);
        }
    }

    @DexIgnore
    public Z get() {
        return this.f10353g.get();
    }

    @DexIgnore
    public synchronized java.lang.String toString() {
        return "EngineResource{isMemoryCacheable=" + this.f10351e + ", listener=" + this.f10354h + ", key=" + this.f10355i + ", acquired=" + this.f10356j + ", isRecycled=" + this.f10357k + ", resource=" + this.f10353g + '}';
    }
}
