package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rw2 implements Factory<NotificationCallsAndMessagesPresenter> {
    @DexIgnore
    public static NotificationCallsAndMessagesPresenter a(mw2 mw2, j62 j62, px2 px2, rx2 rx2, sx2 sx2, vy2 vy2, NotificationSettingsDao notificationSettingsDao, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationCallsAndMessagesPresenter(mw2, j62, px2, rx2, sx2, vy2, notificationSettingsDao, notificationSettingsDatabase);
    }
}
