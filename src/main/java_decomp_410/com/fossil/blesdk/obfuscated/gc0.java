package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gc0 extends oc<Void> implements cf0 {
    @DexIgnore
    public Semaphore a; // = new Semaphore(0);
    @DexIgnore
    public Set<ge0> b;

    @DexIgnore
    public gc0(Context context, Set<ge0> set) {
        super(context);
        this.b = set;
    }

    @DexIgnore
    /* renamed from: a */
    public final Void loadInBackground() {
        int i = 0;
        for (ge0 a2 : this.b) {
            if (a2.a((cf0) this)) {
                i++;
            }
        }
        try {
            this.a.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    @DexIgnore
    public final void onComplete() {
        this.a.release();
    }

    @DexIgnore
    public final void onStartLoading() {
        this.a.drainPermits();
        forceLoad();
    }
}
