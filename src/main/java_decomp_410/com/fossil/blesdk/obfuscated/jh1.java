package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.util.Pair;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jh1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ fh1 e;

    @DexIgnore
    public jh1(fh1 fh1, String str, long j) {
        this.e = fh1;
        bk0.b(str);
        bk0.a(j > 0);
        this.a = String.valueOf(str).concat(":start");
        this.b = String.valueOf(str).concat(":count");
        this.c = String.valueOf(str).concat(":value");
        this.d = j;
    }

    @DexIgnore
    public final void a() {
        this.e.e();
        long b2 = this.e.c().b();
        SharedPreferences.Editor edit = this.e.s().edit();
        edit.remove(this.b);
        edit.remove(this.c);
        edit.putLong(this.a, b2);
        edit.apply();
    }

    @DexIgnore
    public final Pair<String, Long> b() {
        long j;
        this.e.e();
        this.e.e();
        long c2 = c();
        if (c2 == 0) {
            a();
            j = 0;
        } else {
            j = Math.abs(c2 - this.e.c().b());
        }
        long j2 = this.d;
        if (j < j2) {
            return null;
        }
        if (j > (j2 << 1)) {
            a();
            return null;
        }
        String string = this.e.s().getString(this.c, (String) null);
        long j3 = this.e.s().getLong(this.b, 0);
        a();
        if (string == null || j3 <= 0) {
            return fh1.w;
        }
        return new Pair<>(string, Long.valueOf(j3));
    }

    @DexIgnore
    public final long c() {
        return this.e.s().getLong(this.a, 0);
    }

    @DexIgnore
    public final void a(String str, long j) {
        this.e.e();
        if (c() == 0) {
            a();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.e.s().getLong(this.b, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.e.s().edit();
            edit.putString(this.c, str);
            edit.putLong(this.b, 1);
            edit.apply();
            return;
        }
        long j3 = j2 + 1;
        boolean z = (this.e.j().t().nextLong() & ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) < ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD / j3;
        SharedPreferences.Editor edit2 = this.e.s().edit();
        if (z) {
            edit2.putString(this.c, str);
        }
        edit2.putLong(this.b, j3);
        edit2.apply();
    }
}
