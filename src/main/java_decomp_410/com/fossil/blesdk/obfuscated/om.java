package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class om implements vm {
    @DexIgnore
    public /* final */ Executor a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ /* synthetic */ Handler e;

        @DexIgnore
        public a(om omVar, Handler handler) {
            this.e = handler;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.e.post(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ Request e;
        @DexIgnore
        public /* final */ um f;
        @DexIgnore
        public /* final */ Runnable g;

        @DexIgnore
        public b(Request request, um umVar, Runnable runnable) {
            this.e = request;
            this.f = umVar;
            this.g = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.e.isCanceled()) {
                this.e.finish("canceled-at-delivery");
                return;
            }
            if (this.f.a()) {
                this.e.deliverResponse(this.f.a);
            } else {
                this.e.deliverError(this.f.c);
            }
            if (this.f.d) {
                this.e.addMarker("intermediate-response");
            } else {
                this.e.finish("done");
            }
            Runnable runnable = this.g;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public om(Handler handler) {
        this.a = new a(this, handler);
    }

    @DexIgnore
    public void a(Request<?> request, um<?> umVar) {
        a(request, umVar, (Runnable) null);
    }

    @DexIgnore
    public void a(Request<?> request, um<?> umVar, Runnable runnable) {
        request.markDelivered();
        request.addMarker("post-response");
        this.a.execute(new b(request, umVar, runnable));
    }

    @DexIgnore
    public void a(Request<?> request, VolleyError volleyError) {
        request.addMarker("post-error");
        this.a.execute(new b(request, um.a(volleyError), (Runnable) null));
    }
}
