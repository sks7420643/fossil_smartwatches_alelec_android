package com.fossil.blesdk.obfuscated;

import com.tencent.wxop.stat.StatReportStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public /* synthetic */ class w04 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[StatReportStrategy.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|1|2|3|4|5|6|7|8|9|10|11|12|(3:13|14|16)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|16) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    /*
    static {
        a[StatReportStrategy.INSTANT.ordinal()] = 1;
        a[StatReportStrategy.PERIOD.ordinal()] = 2;
        a[StatReportStrategy.APP_LAUNCH.ordinal()] = 3;
        a[StatReportStrategy.DEVELOPER.ordinal()] = 4;
        a[StatReportStrategy.BATCH.ordinal()] = 5;
        a[StatReportStrategy.ONLY_WIFI.ordinal()] = 6;
        try {
            a[StatReportStrategy.ONLY_WIFI_NO_CACHE.ordinal()] = 7;
        } catch (NoSuchFieldError unused) {
        }
    }
    */
}
