package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class et implements mo<Uri, Bitmap> {
    @DexIgnore
    public /* final */ ot a;
    @DexIgnore
    public /* final */ jq b;

    @DexIgnore
    public et(ot otVar, jq jqVar) {
        this.a = otVar;
        this.b = jqVar;
    }

    @DexIgnore
    public boolean a(Uri uri, lo loVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @DexIgnore
    public aq<Bitmap> a(Uri uri, int i, int i2, lo loVar) {
        aq<Drawable> a2 = this.a.a(uri, i, i2, loVar);
        if (a2 == null) {
            return null;
        }
        return xs.a(this.b, a2.get(), i, i2);
    }
}
