package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ ImageView B;
    @DexIgnore
    public /* final */ ImageView C;
    @DexIgnore
    public /* final */ ImageView D;
    @DexIgnore
    public /* final */ View E;
    @DexIgnore
    public /* final */ ProgressBar F;
    @DexIgnore
    public /* final */ RecyclerViewPager G;
    @DexIgnore
    public /* final */ RecyclerViewPager H;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ CustomPageIndicator s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qf2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, CustomPageIndicator customPageIndicator, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, View view2, View view3, ProgressBar progressBar, RecyclerViewPager recyclerViewPager, RecyclerViewPager recyclerViewPager2, TextView textView, View view4) {
        super(obj, view, i);
        this.q = constraintLayout2;
        this.r = constraintLayout3;
        this.s = customPageIndicator;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView3;
        this.w = flexibleTextView4;
        this.x = flexibleTextView5;
        this.y = flexibleTextView6;
        this.z = flexibleTextView7;
        this.A = flexibleTextView10;
        this.B = imageView;
        this.C = imageView2;
        this.D = imageView4;
        this.E = view2;
        this.F = progressBar;
        this.G = recyclerViewPager;
        this.H = recyclerViewPager2;
    }
}
