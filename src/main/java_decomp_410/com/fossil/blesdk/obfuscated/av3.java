package com.fossil.blesdk.obfuscated;

import java.util.ArrayDeque;
import java.util.Deque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class av3 {
    @DexIgnore
    public /* final */ Deque<tu3> a; // = new ArrayDeque();

    @DexIgnore
    public av3() {
        new ArrayDeque();
        new ArrayDeque();
    }

    @DexIgnore
    public synchronized void a(tu3 tu3) {
        this.a.add(tu3);
    }

    @DexIgnore
    public synchronized void b(tu3 tu3) {
        if (!this.a.remove(tu3)) {
            throw new AssertionError("Call wasn't in-flight!");
        }
    }
}
