package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zm3 implements MembersInjector<PairingActivity> {
    @DexIgnore
    public static void a(PairingActivity pairingActivity, PairingPresenter pairingPresenter) {
        pairingActivity.B = pairingPresenter;
    }
}
