package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p52 implements Factory<ql2> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public p52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static p52 a(n42 n42) {
        return new p52(n42);
    }

    @DexIgnore
    public static ql2 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static ql2 c(n42 n42) {
        ql2 o = n42.o();
        n44.a(o, "Cannot return null from a non-@Nullable @Provides method");
        return o;
    }

    @DexIgnore
    public ql2 get() {
        return b(this.a);
    }
}
