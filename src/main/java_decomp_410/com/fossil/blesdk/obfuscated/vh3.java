package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.enums.GoalType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vh3 extends v52<uh3> {
    @DexIgnore
    void I0();

    @DexIgnore
    void a(int i, GoalType goalType);

    @DexIgnore
    void a(ActivitySettings activitySettings);

    @DexIgnore
    void d(int i, String str);

    @DexIgnore
    void i(int i);

    @DexIgnore
    void j(int i);

    @DexIgnore
    void l(int i);

    @DexIgnore
    void r0();

    @DexIgnore
    void w0();
}
