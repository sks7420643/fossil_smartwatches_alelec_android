package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wo */
public class C3232wo extends com.fossil.blesdk.obfuscated.C2754qo<android.os.ParcelFileDescriptor> {
    @DexIgnore
    public C3232wo(android.content.res.AssetManager assetManager, java.lang.String str) {
        super(assetManager, str);
    }

    @DexIgnore
    public java.lang.Class<android.os.ParcelFileDescriptor> getDataClass() {
        return android.os.ParcelFileDescriptor.class;
    }

    @DexIgnore
    /* renamed from: a */
    public android.os.ParcelFileDescriptor m15923a(android.content.res.AssetManager assetManager, java.lang.String str) throws java.io.IOException {
        return assetManager.openFd(str).getParcelFileDescriptor();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9243a(android.os.ParcelFileDescriptor parcelFileDescriptor) throws java.io.IOException {
        parcelFileDescriptor.close();
    }
}
