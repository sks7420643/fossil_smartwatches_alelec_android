package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t2 */
public class C2937t2 extends com.fossil.blesdk.obfuscated.C1864ga implements android.view.View.OnClickListener {

    @DexIgnore
    /* renamed from: A */
    public int f9519A; // = -1;

    @DexIgnore
    /* renamed from: B */
    public int f9520B; // = -1;

    @DexIgnore
    /* renamed from: C */
    public int f9521C; // = -1;

    @DexIgnore
    /* renamed from: p */
    public /* final */ androidx.appcompat.widget.SearchView f9522p;

    @DexIgnore
    /* renamed from: q */
    public /* final */ android.app.SearchableInfo f9523q;

    @DexIgnore
    /* renamed from: r */
    public /* final */ android.content.Context f9524r;

    @DexIgnore
    /* renamed from: s */
    public /* final */ java.util.WeakHashMap<java.lang.String, android.graphics.drawable.Drawable.ConstantState> f9525s;

    @DexIgnore
    /* renamed from: t */
    public /* final */ int f9526t;

    @DexIgnore
    /* renamed from: u */
    public boolean f9527u; // = false;

    @DexIgnore
    /* renamed from: v */
    public int f9528v; // = 1;

    @DexIgnore
    /* renamed from: w */
    public android.content.res.ColorStateList f9529w;

    @DexIgnore
    /* renamed from: x */
    public int f9530x; // = -1;

    @DexIgnore
    /* renamed from: y */
    public int f9531y; // = -1;

    @DexIgnore
    /* renamed from: z */
    public int f9532z; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.t2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.t2$a */
    public static final class C2938a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.widget.TextView f9533a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.widget.TextView f9534b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ android.widget.ImageView f9535c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ android.widget.ImageView f9536d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.widget.ImageView f9537e;

        @DexIgnore
        public C2938a(android.view.View view) {
            this.f9533a = (android.widget.TextView) view.findViewById(16908308);
            this.f9534b = (android.widget.TextView) view.findViewById(16908309);
            this.f9535c = (android.widget.ImageView) view.findViewById(16908295);
            this.f9536d = (android.widget.ImageView) view.findViewById(16908296);
            this.f9537e = (android.widget.ImageView) view.findViewById(com.fossil.blesdk.obfuscated.C3158w.edit_query);
        }
    }

    @DexIgnore
    public C2937t2(android.content.Context context, androidx.appcompat.widget.SearchView searchView, android.app.SearchableInfo searchableInfo, java.util.WeakHashMap<java.lang.String, android.graphics.drawable.Drawable.ConstantState> weakHashMap) {
        super(context, searchView.getSuggestionRowLayout(), (android.database.Cursor) null, true);
        android.app.SearchManager searchManager = (android.app.SearchManager) this.f4739h.getSystemService("search");
        this.f9522p = searchView;
        this.f9523q = searchableInfo;
        this.f9526t = searchView.getSuggestionCommitIconResId();
        this.f9524r = context;
        this.f9525s = weakHashMap;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16249a(int i) {
        this.f9528v = i;
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo10379b(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup) {
        android.view.View b = super.mo10379b(context, cursor, viewGroup);
        b.setTag(new com.fossil.blesdk.obfuscated.C2937t2.C2938a(b));
        ((android.widget.ImageView) b.findViewById(com.fossil.blesdk.obfuscated.C3158w.edit_query)).setImageResource(this.f9526t);
        return b;
    }

    @DexIgnore
    /* renamed from: d */
    public final android.graphics.drawable.Drawable mo16257d(android.database.Cursor cursor) {
        android.graphics.drawable.Drawable b = mo16253b(this.f9523q.getSearchActivity());
        if (b != null) {
            return b;
        }
        return this.f4739h.getPackageManager().getDefaultActivityIcon();
    }

    @DexIgnore
    /* renamed from: e */
    public final android.graphics.drawable.Drawable mo16258e(android.database.Cursor cursor) {
        int i = this.f9519A;
        if (i == -1) {
            return null;
        }
        android.graphics.drawable.Drawable b = mo16255b(cursor.getString(i));
        if (b != null) {
            return b;
        }
        return mo16257d(cursor);
    }

    @DexIgnore
    /* renamed from: f */
    public final android.graphics.drawable.Drawable mo16259f(android.database.Cursor cursor) {
        int i = this.f9520B;
        if (i == -1) {
            return null;
        }
        return mo16255b(cursor.getString(i));
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo16260g(android.database.Cursor cursor) {
        android.os.Bundle extras = cursor != null ? cursor.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    @DexIgnore
    public android.view.View getDropDownView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
        try {
            return super.getDropDownView(i, view, viewGroup);
        } catch (java.lang.RuntimeException e) {
            android.util.Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            android.view.View a = mo10375a(this.f4739h, this.f4738g, viewGroup);
            if (a != null) {
                ((com.fossil.blesdk.obfuscated.C2937t2.C2938a) a.getTag()).f9533a.setText(e.toString());
            }
            return a;
        }
    }

    @DexIgnore
    public android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
        try {
            return super.getView(i, view, viewGroup);
        } catch (java.lang.RuntimeException e) {
            android.util.Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", e);
            android.view.View b = mo10379b(this.f4739h, this.f4738g, viewGroup);
            if (b != null) {
                ((com.fossil.blesdk.obfuscated.C2937t2.C2938a) b.getTag()).f9533a.setText(e.toString());
            }
            return b;
        }
    }

    @DexIgnore
    public boolean hasStableIds() {
        return false;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        mo16260g(mo10374a());
    }

    @DexIgnore
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        mo16260g(mo10374a());
    }

    @DexIgnore
    public void onClick(android.view.View view) {
        java.lang.Object tag = view.getTag();
        if (tag instanceof java.lang.CharSequence) {
            this.f9522p.onQueryRefine((java.lang.CharSequence) tag);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.database.Cursor mo10813a(java.lang.CharSequence charSequence) {
        java.lang.String charSequence2 = charSequence == null ? "" : charSequence.toString();
        if (this.f9522p.getVisibility() == 0 && this.f9522p.getWindowVisibility() == 0) {
            try {
                android.database.Cursor a = mo16245a(this.f9523q, charSequence2, 50);
                if (a != null) {
                    a.getCount();
                    return a;
                }
            } catch (java.lang.RuntimeException e) {
                android.util.Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", e);
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.CharSequence mo16256b(java.lang.CharSequence charSequence) {
        if (this.f9529w == null) {
            android.util.TypedValue typedValue = new android.util.TypedValue();
            this.f4739h.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.textColorSearchUrl, typedValue, true);
            this.f9529w = this.f4739h.getResources().getColorStateList(typedValue.resourceId);
        }
        android.text.SpannableString spannableString = new android.text.SpannableString(charSequence);
        android.text.style.TextAppearanceSpan textAppearanceSpan = new android.text.style.TextAppearanceSpan((java.lang.String) null, 0, 0, this.f9529w, (android.content.res.ColorStateList) null);
        spannableString.setSpan(textAppearanceSpan, 0, charSequence.length(), 33);
        return spannableString;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10377a(android.database.Cursor cursor) {
        if (this.f9527u) {
            android.util.Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
                return;
            }
            return;
        }
        try {
            super.mo10377a(cursor);
            if (cursor != null) {
                this.f9530x = cursor.getColumnIndex("suggest_text_1");
                this.f9531y = cursor.getColumnIndex("suggest_text_2");
                this.f9532z = cursor.getColumnIndex("suggest_text_2_url");
                this.f9519A = cursor.getColumnIndex("suggest_icon_1");
                this.f9520B = cursor.getColumnIndex("suggest_icon_2");
                this.f9521C = cursor.getColumnIndex("suggest_flags");
            }
        } catch (java.lang.Exception e) {
            android.util.Log.e("SuggestionsAdapter", "error changing cursor and caching columns", e);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.CharSequence mo10380b(android.database.Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        java.lang.String a = m13985a(cursor, "suggest_intent_query");
        if (a != null) {
            return a;
        }
        if (this.f9523q.shouldRewriteQueryFromData()) {
            java.lang.String a2 = m13985a(cursor, "suggest_intent_data");
            if (a2 != null) {
                return a2;
            }
        }
        if (this.f9523q.shouldRewriteQueryFromText()) {
            java.lang.String a3 = m13985a(cursor, "suggest_text_1");
            if (a3 != null) {
                return a3;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public final android.graphics.drawable.Drawable mo16255b(java.lang.String str) {
        int parseInt;
        if (str == null || str.isEmpty() || "0".equals(str)) {
            return null;
        }
        try {
            java.lang.String str2 = "android.resource://" + this.f9524r.getPackageName() + com.zendesk.sdk.network.impl.ZendeskConfig.SLASH + parseInt;
            android.graphics.drawable.Drawable a = mo16248a(str2);
            if (a != null) {
                return a;
            }
            android.graphics.drawable.Drawable c = com.fossil.blesdk.obfuscated.C2185k6.m9358c(this.f9524r, parseInt);
            mo16252a(str2, c);
            return c;
        } catch (java.lang.NumberFormatException unused) {
            android.graphics.drawable.Drawable a2 = mo16248a(str);
            if (a2 != null) {
                return a2;
            }
            android.graphics.drawable.Drawable a3 = mo16247a(android.net.Uri.parse(str));
            mo16252a(str, a3);
            return a3;
        } catch (android.content.res.Resources.NotFoundException unused2) {
            android.util.Log.w("SuggestionsAdapter", "Icon resource not found: " + str);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10378a(android.view.View view, android.content.Context context, android.database.Cursor cursor) {
        java.lang.CharSequence charSequence;
        com.fossil.blesdk.obfuscated.C2937t2.C2938a aVar = (com.fossil.blesdk.obfuscated.C2937t2.C2938a) view.getTag();
        int i = this.f9521C;
        int i2 = i != -1 ? cursor.getInt(i) : 0;
        if (aVar.f9533a != null) {
            mo16251a(aVar.f9533a, (java.lang.CharSequence) m13984a(cursor, this.f9530x));
        }
        if (aVar.f9534b != null) {
            java.lang.String a = m13984a(cursor, this.f9532z);
            if (a != null) {
                charSequence = mo16256b((java.lang.CharSequence) a);
            } else {
                charSequence = m13984a(cursor, this.f9531y);
            }
            if (android.text.TextUtils.isEmpty(charSequence)) {
                android.widget.TextView textView = aVar.f9533a;
                if (textView != null) {
                    textView.setSingleLine(false);
                    aVar.f9533a.setMaxLines(2);
                }
            } else {
                android.widget.TextView textView2 = aVar.f9533a;
                if (textView2 != null) {
                    textView2.setSingleLine(true);
                    aVar.f9533a.setMaxLines(1);
                }
            }
            mo16251a(aVar.f9534b, charSequence);
        }
        android.widget.ImageView imageView = aVar.f9535c;
        if (imageView != null) {
            mo16250a(imageView, mo16258e(cursor), 4);
        }
        android.widget.ImageView imageView2 = aVar.f9536d;
        if (imageView2 != null) {
            mo16250a(imageView2, mo16259f(cursor), 8);
        }
        int i3 = this.f9528v;
        if (i3 == 2 || (i3 == 1 && (i2 & 1) != 0)) {
            aVar.f9537e.setVisibility(0);
            aVar.f9537e.setTag(aVar.f9533a.getText());
            aVar.f9537e.setOnClickListener(this);
            return;
        }
        aVar.f9537e.setVisibility(8);
    }

    @DexIgnore
    /* renamed from: b */
    public final android.graphics.drawable.Drawable mo16253b(android.content.ComponentName componentName) {
        java.lang.String flattenToShortString = componentName.flattenToShortString();
        android.graphics.drawable.Drawable.ConstantState constantState = null;
        if (this.f9525s.containsKey(flattenToShortString)) {
            android.graphics.drawable.Drawable.ConstantState constantState2 = this.f9525s.get(flattenToShortString);
            if (constantState2 == null) {
                return null;
            }
            return constantState2.newDrawable(this.f9524r.getResources());
        }
        android.graphics.drawable.Drawable a = mo16246a(componentName);
        if (a != null) {
            constantState = a.getConstantState();
        }
        this.f9525s.put(flattenToShortString, constantState);
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.drawable.Drawable mo16254b(android.net.Uri uri) throws java.io.FileNotFoundException {
        int i;
        java.lang.String authority = uri.getAuthority();
        if (!android.text.TextUtils.isEmpty(authority)) {
            try {
                android.content.res.Resources resourcesForApplication = this.f4739h.getPackageManager().getResourcesForApplication(authority);
                java.util.List<java.lang.String> pathSegments = uri.getPathSegments();
                if (pathSegments != null) {
                    int size = pathSegments.size();
                    if (size == 1) {
                        try {
                            i = java.lang.Integer.parseInt(pathSegments.get(0));
                        } catch (java.lang.NumberFormatException unused) {
                            throw new java.io.FileNotFoundException("Single path segment is not a resource ID: " + uri);
                        }
                    } else if (size == 2) {
                        i = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
                    } else {
                        throw new java.io.FileNotFoundException("More than two path segments: " + uri);
                    }
                    if (i != 0) {
                        return resourcesForApplication.getDrawable(i);
                    }
                    throw new java.io.FileNotFoundException("No resource found for: " + uri);
                }
                throw new java.io.FileNotFoundException("No path: " + uri);
            } catch (android.content.pm.PackageManager.NameNotFoundException unused2) {
                throw new java.io.FileNotFoundException("No package found for authority: " + uri);
            }
        } else {
            throw new java.io.FileNotFoundException("No authority: " + uri);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16251a(android.widget.TextView textView, java.lang.CharSequence charSequence) {
        textView.setText(charSequence);
        if (android.text.TextUtils.isEmpty(charSequence)) {
            textView.setVisibility(8);
        } else {
            textView.setVisibility(0);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16250a(android.widget.ImageView imageView, android.graphics.drawable.Drawable drawable, int i) {
        imageView.setImageDrawable(drawable);
        if (drawable == null) {
            imageView.setVisibility(i);
            return;
        }
        imageView.setVisibility(0);
        drawable.setVisible(false, false);
        drawable.setVisible(true, false);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002c, code lost:
        throw new java.io.FileNotFoundException("Resource does not exist: " + r7);
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo16247a(android.net.Uri uri) {
        java.io.InputStream openInputStream;
        try {
            if ("android.resource".equals(uri.getScheme())) {
                return mo16254b(uri);
            }
            openInputStream = this.f9524r.getContentResolver().openInputStream(uri);
            if (openInputStream != null) {
                android.graphics.drawable.Drawable createFromStream = android.graphics.drawable.Drawable.createFromStream(openInputStream, (java.lang.String) null);
                try {
                    openInputStream.close();
                } catch (java.io.IOException e) {
                    android.util.Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e);
                }
                return createFromStream;
            }
            throw new java.io.FileNotFoundException("Failed to open " + uri);
        } catch (java.io.FileNotFoundException e2) {
            android.util.Log.w("SuggestionsAdapter", "Icon not found: " + uri + ", " + e2.getMessage());
            return null;
        } catch (Throwable th) {
            try {
                openInputStream.close();
            } catch (java.io.IOException e3) {
                android.util.Log.e("SuggestionsAdapter", "Error closing icon stream for " + uri, e3);
            }
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo16248a(java.lang.String str) {
        android.graphics.drawable.Drawable.ConstantState constantState = this.f9525s.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16252a(java.lang.String str, android.graphics.drawable.Drawable drawable) {
        if (drawable != null) {
            this.f9525s.put(str, drawable.getConstantState());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo16246a(android.content.ComponentName componentName) {
        android.content.pm.PackageManager packageManager = this.f4739h.getPackageManager();
        try {
            android.content.pm.ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            android.graphics.drawable.Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            android.util.Log.w("SuggestionsAdapter", "Invalid icon resource " + iconResource + " for " + componentName.flattenToShortString());
            return null;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            android.util.Log.w("SuggestionsAdapter", e.toString());
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m13985a(android.database.Cursor cursor, java.lang.String str) {
        return m13984a(cursor, cursor.getColumnIndex(str));
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m13984a(android.database.Cursor cursor, int i) {
        if (i == -1) {
            return null;
        }
        try {
            return cursor.getString(i);
        } catch (java.lang.Exception e) {
            android.util.Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.database.Cursor mo16245a(android.app.SearchableInfo searchableInfo, java.lang.String str, int i) {
        java.lang.String[] strArr = null;
        if (searchableInfo == null) {
            return null;
        }
        java.lang.String suggestAuthority = searchableInfo.getSuggestAuthority();
        if (suggestAuthority == null) {
            return null;
        }
        android.net.Uri.Builder fragment = new android.net.Uri.Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        java.lang.String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        java.lang.String suggestSelection = searchableInfo.getSuggestSelection();
        if (suggestSelection != null) {
            strArr = new java.lang.String[]{str};
        } else {
            fragment.appendPath(str);
        }
        java.lang.String[] strArr2 = strArr;
        if (i > 0) {
            fragment.appendQueryParameter("limit", java.lang.String.valueOf(i));
        }
        return this.f4739h.getContentResolver().query(fragment.build(), (java.lang.String[]) null, suggestSelection, strArr2, (java.lang.String) null);
    }
}
