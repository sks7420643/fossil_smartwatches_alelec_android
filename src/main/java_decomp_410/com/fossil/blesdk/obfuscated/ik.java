package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.lk;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ik implements lk.a {
    @DexIgnore
    public static /* final */ String d; // = dj.a("WorkConstraintsTracker");
    @DexIgnore
    public /* final */ hk a;
    @DexIgnore
    public /* final */ lk<?>[] b;
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public ik(Context context, zl zlVar, hk hkVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = hkVar;
        this.b = new lk[]{new jk(applicationContext, zlVar), new kk(applicationContext, zlVar), new qk(applicationContext, zlVar), new mk(applicationContext, zlVar), new pk(applicationContext, zlVar), new ok(applicationContext, zlVar), new nk(applicationContext, zlVar)};
    }

    @DexIgnore
    public void a() {
        synchronized (this.c) {
            for (lk<?> a2 : this.b) {
                a2.a();
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        synchronized (this.c) {
            if (this.a != null) {
                this.a.a(list);
            }
        }
    }

    @DexIgnore
    public void c(List<hl> list) {
        synchronized (this.c) {
            for (lk<?> a2 : this.b) {
                a2.a((lk.a) null);
            }
            for (lk<?> a3 : this.b) {
                a3.a(list);
            }
            for (lk<?> a4 : this.b) {
                a4.a((lk.a) this);
            }
        }
    }

    @DexIgnore
    public boolean a(String str) {
        synchronized (this.c) {
            for (lk<?> lkVar : this.b) {
                if (lkVar.a(str)) {
                    dj.a().a(d, String.format("Work %s constrained by %s", new Object[]{str, lkVar.getClass().getSimpleName()}), new Throwable[0]);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public void a(List<String> list) {
        synchronized (this.c) {
            ArrayList arrayList = new ArrayList();
            for (String next : list) {
                if (a(next)) {
                    dj.a().a(d, String.format("Constraints met for %s", new Object[]{next}), new Throwable[0]);
                    arrayList.add(next);
                }
            }
            if (this.a != null) {
                this.a.b(arrayList);
            }
        }
    }
}
