package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rf */
public class C2803rf {
    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.room.RoomDatabase> androidx.room.RoomDatabase.C0272a<T> m13160a(android.content.Context context, java.lang.Class<T> cls, java.lang.String str) {
        if (str != null && str.trim().length() != 0) {
            return new androidx.room.RoomDatabase.C0272a<>(context, cls, str);
        }
        throw new java.lang.IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder");
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.room.RoomDatabase> androidx.room.RoomDatabase.C0272a<T> m13159a(android.content.Context context, java.lang.Class<T> cls) {
        return new androidx.room.RoomDatabase.C0272a<>(context, cls, (java.lang.String) null);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T, C> T m13161a(java.lang.Class<C> cls, java.lang.String str) {
        java.lang.String str2;
        java.lang.String str3;
        java.lang.String name = cls.getPackage().getName();
        java.lang.String canonicalName = cls.getCanonicalName();
        if (!name.isEmpty()) {
            canonicalName = canonicalName.substring(name.length() + 1);
        }
        try {
            if (name.isEmpty()) {
                str3 = str2;
            } else {
                str3 = name + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME + str2;
            }
            return java.lang.Class.forName(str3).newInstance();
        } catch (java.lang.ClassNotFoundException unused) {
            throw new java.lang.RuntimeException("cannot find implementation for " + cls.getCanonicalName() + ". " + str2 + " does not exist");
        } catch (java.lang.IllegalAccessException unused2) {
            throw new java.lang.RuntimeException("Cannot access the constructor" + cls.getCanonicalName());
        } catch (java.lang.InstantiationException unused3) {
            throw new java.lang.RuntimeException("Failed to create an instance of " + cls.getCanonicalName());
        }
    }
}
