package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.blesdk.obfuscated.xs2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ff3 extends zr2 implements ef3, View.OnClickListener, xs2.b, ws3.g {
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public Date j; // = new Date();
    @DexIgnore
    public xs2 k;
    @DexIgnore
    public tr3<wb2> l;
    @DexIgnore
    public df3 m;
    @DexIgnore
    public bu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ff3 a(Date date) {
            kd4.b(date, "date");
            ff3 ff3 = new ff3();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            ff3.setArguments(bundle);
            return ff3;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends bu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ff3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerViewEmptySupport recyclerViewEmptySupport, LinearLayoutManager linearLayoutManager, ff3 ff3, LinearLayoutManager linearLayoutManager2, wb2 wb2) {
            super(linearLayoutManager);
            this.e = ff3;
        }

        @DexIgnore
        public void a(int i) {
            df3 a = this.e.m;
            if (a != null) {
                a.i();
            }
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "GoalTrackingDetailFragment";
    }

    @DexIgnore
    public void c(qd<GoalTrackingData> qdVar) {
        kd4.b(qdVar, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailData - goalTrackingDataList=" + qdVar);
        tr3<wb2> tr3 = this.l;
        if (tr3 != null && tr3.a() != null) {
            xs2 xs2 = this.k;
            if (xs2 != null) {
                xs2.b(qdVar);
            }
        }
    }

    @DexIgnore
    public void f() {
        bu3 bu3 = this.n;
        if (bu3 != null) {
            bu3.a();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int i;
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("GoalTrackingDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case R.id.add /*2131361841*/:
                    Boolean s = rk2.s(this.j);
                    kd4.a((Object) s, "DateHelper.isToday(mDate)");
                    if (s.booleanValue()) {
                        Calendar instance = Calendar.getInstance();
                        int i4 = instance.get(10);
                        int i5 = instance.get(12);
                        i = instance.get(9);
                        if (i4 == 0) {
                            i2 = i5;
                            i3 = 12;
                        } else {
                            i3 = i4;
                            i2 = i5;
                        }
                    } else {
                        i3 = 12;
                        i2 = 0;
                        i = 0;
                    }
                    ds3 ds3 = ds3.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    kd4.a((Object) childFragmentManager, "childFragmentManager");
                    String[] strArr = new String[2];
                    String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am);
                    kd4.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        strArr[0] = upperCase;
                        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm);
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
                        if (a3 != null) {
                            String upperCase2 = a3.toUpperCase();
                            kd4.a((Object) upperCase2, "(this as java.lang.String).toUpperCase()");
                            strArr[1] = upperCase2;
                            ds3.a(childFragmentManager, i3, i2, i, strArr);
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                case R.id.iv_back /*2131362398*/:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case R.id.iv_back_date /*2131362399*/:
                    df3 df3 = this.m;
                    if (df3 != null) {
                        df3.k();
                        return;
                    }
                    return;
                case R.id.iv_next_date /*2131362447*/:
                    df3 df32 = this.m;
                    if (df32 != null) {
                        df32.j();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        wb2 wb2 = (wb2) qa.a(layoutInflater, R.layout.fragment_goal_tracking_detail, viewGroup, false, O0());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.j = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.j = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        this.k = new xs2(this, new a62());
        kd4.a((Object) wb2, "binding");
        a(wb2);
        df3 df3 = this.m;
        if (df3 != null) {
            df3.b(this.j);
        }
        this.l = new tr3<>(this, wb2);
        tr3<wb2> tr3 = this.l;
        if (tr3 != null) {
            wb2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        df3 df3 = this.m;
        if (df3 != null) {
            df3.h();
        }
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        df3 df3 = this.m;
        if (df3 != null) {
            df3.g();
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        df3 df3 = this.m;
        if (df3 != null) {
            df3.c(this.j);
        }
        df3 df32 = this.m;
        if (df32 != null) {
            df32.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        df3 df3 = this.m;
        if (df3 != null) {
            df3.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(wb2 wb2) {
        wb2.z.setOnClickListener(this);
        wb2.A.setOnClickListener(this);
        wb2.B.setOnClickListener(this);
        wb2.q.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        RecyclerViewEmptySupport recyclerViewEmptySupport = wb2.E;
        kd4.a((Object) recyclerViewEmptySupport, "it");
        recyclerViewEmptySupport.setLayoutManager(linearLayoutManager);
        RecyclerView.m layoutManager = recyclerViewEmptySupport.getLayoutManager();
        if (layoutManager != null) {
            this.n = new b(recyclerViewEmptySupport, (LinearLayoutManager) layoutManager, this, linearLayoutManager, wb2);
            recyclerViewEmptySupport.setAdapter(this.k);
            FlexibleTextView flexibleTextView = wb2.F;
            kd4.a((Object) flexibleTextView, "binding.tvNotFound");
            recyclerViewEmptySupport.setEmptyView(flexibleTextView);
            bu3 bu3 = this.n;
            if (bu3 != null) {
                recyclerViewEmptySupport.a((RecyclerView.q) bu3);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v11, types: [java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        Bundle bundle = null;
        if (hashCode != 193266439) {
            if (hashCode == 1983205541 && str.equals("GOAL_TRACKING_ADD") && i == R.id.fb_add) {
                if (intent != null) {
                    bundle = intent.getSerializableExtra("EXTRA_NUMBER_PICKER_RESULTS");
                }
                if (bundle != null) {
                    HashMap hashMap = (HashMap) bundle;
                    Integer num = (Integer) hashMap.get(Integer.valueOf(R.id.np_hour));
                    Integer num2 = (Integer) hashMap.get(Integer.valueOf(R.id.np_minute));
                    Integer num3 = (Integer) hashMap.get(Integer.valueOf(R.id.np_suffix));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailFragment", "onDialogFragmentResult GOAL_TRACKING_ADD: hour=" + num + ", minute=" + num2 + ", suffix=" + num3);
                    Calendar instance = Calendar.getInstance();
                    kd4.a((Object) instance, "calendar");
                    instance.setTime(this.j);
                    if (num != null && num2 != null && num3 != null) {
                        if (num.intValue() == 12) {
                            num = 0;
                        }
                        instance.set(9, num3.intValue());
                        instance.set(10, num.intValue());
                        instance.set(12, num2.intValue());
                        Calendar instance2 = Calendar.getInstance();
                        kd4.a((Object) instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        kd4.a((Object) time, "Calendar.getInstance().time");
                        long time2 = time.getTime();
                        Date time3 = instance.getTime();
                        kd4.a((Object) time3, "calendar.time");
                        if (time2 > time3.getTime()) {
                            df3 df3 = this.m;
                            if (df3 != null) {
                                Date time4 = instance.getTime();
                                kd4.a((Object) time4, "calendar.time");
                                df3.a(time4);
                                return;
                            }
                            return;
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        if (fragmentManager != null) {
                            ds3 ds3 = ds3.c;
                            kd4.a((Object) fragmentManager, "this");
                            ds3.j(fragmentManager);
                            return;
                        }
                        return;
                    }
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.util.HashMap<kotlin.Int, kotlin.Int>");
            }
        } else if (str.equals("GOAL_TRACKING_DELETE") && i == R.id.tv_ok) {
            if (intent != null) {
                bundle = intent.getExtras();
            }
            if (bundle != null) {
                GoalTrackingData goalTrackingData = (GoalTrackingData) bundle.getSerializable("GOAL_TRACKING_DELETE_BUNDLE");
                if (goalTrackingData != null) {
                    df3 df32 = this.m;
                    if (df32 != null) {
                        df32.a(goalTrackingData);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(GoalTrackingData goalTrackingData) {
        kd4.b(goalTrackingData, "item");
        ds3 ds3 = ds3.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        ds3.a(childFragmentManager, goalTrackingData);
    }

    @DexIgnore
    public void a(df3 df3) {
        kd4.b(df3, "presenter");
        this.m = df3;
    }

    @DexIgnore
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3);
        this.j = date;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTime(date);
        int i = instance.get(7);
        tr3<wb2> tr3 = this.l;
        if (tr3 != null) {
            wb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                kd4.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(instance.get(5)));
                if (z) {
                    ImageView imageView = a2.A;
                    kd4.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.A;
                    kd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.B;
                    kd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.w;
                        kd4.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(sm2.a(getContext(), (int) R.string.DashboardDiana_Main_Steps7days_CTA__Today));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(ll2.b.b(i));
                    return;
                }
                ImageView imageView4 = a2.B;
                kd4.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.w;
                kd4.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(ll2.b.b(i));
            }
        }
    }

    @DexIgnore
    public void a(GoalTrackingSummary goalTrackingSummary) {
        int i;
        int i2;
        GoalTrackingSummary goalTrackingSummary2 = goalTrackingSummary;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetail - goalTrackingSummary=" + goalTrackingSummary2);
        tr3<wb2> tr3 = this.l;
        if (tr3 != null) {
            wb2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "binding");
                View d = a2.d();
                kd4.a((Object) d, "binding.root");
                Context context = d.getContext();
                if (goalTrackingSummary2 != null) {
                    i = goalTrackingSummary.getTotalTracked();
                    i2 = goalTrackingSummary.getGoalTarget();
                } else {
                    i2 = 0;
                    i = 0;
                }
                FlexibleTextView flexibleTextView = a2.u;
                kd4.a((Object) flexibleTextView, "binding.ftvDailyValue");
                flexibleTextView.setText(il2.b((float) i, 1));
                FlexibleTextView flexibleTextView2 = a2.t;
                kd4.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                flexibleTextView2.setText("/ " + i2 + " " + " " + sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPage_Text__NumberNumberTimes));
                int i3 = i2 > 0 ? (i * 100) / i2 : -1;
                if (i3 >= 100) {
                    ImageView imageView = a2.B;
                    kd4.a((Object) imageView, "binding.ivNextDate");
                    imageView.setSelected(true);
                    ImageView imageView2 = a2.A;
                    kd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setSelected(true);
                    ConstraintLayout constraintLayout = a2.r;
                    kd4.a((Object) constraintLayout, "binding.clOverviewDay");
                    constraintLayout.setSelected(true);
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setSelected(true);
                    FlexibleTextView flexibleTextView4 = a2.v;
                    kd4.a((Object) flexibleTextView4, "binding.ftvDayOfMonth");
                    flexibleTextView4.setSelected(true);
                    View view = a2.C;
                    kd4.a((Object) view, "binding.line");
                    view.setSelected(true);
                    FlexibleTextView flexibleTextView5 = a2.u;
                    kd4.a((Object) flexibleTextView5, "binding.ftvDailyValue");
                    flexibleTextView5.setSelected(true);
                    FlexibleTextView flexibleTextView6 = a2.t;
                    kd4.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                    flexibleTextView6.setSelected(true);
                } else {
                    ImageView imageView3 = a2.B;
                    kd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setSelected(false);
                    ImageView imageView4 = a2.A;
                    kd4.a((Object) imageView4, "binding.ivBackDate");
                    imageView4.setSelected(false);
                    ConstraintLayout constraintLayout2 = a2.r;
                    kd4.a((Object) constraintLayout2, "binding.clOverviewDay");
                    constraintLayout2.setSelected(false);
                    FlexibleTextView flexibleTextView7 = a2.w;
                    kd4.a((Object) flexibleTextView7, "binding.ftvDayOfWeek");
                    flexibleTextView7.setSelected(false);
                    FlexibleTextView flexibleTextView8 = a2.v;
                    kd4.a((Object) flexibleTextView8, "binding.ftvDayOfMonth");
                    flexibleTextView8.setSelected(false);
                    View view2 = a2.C;
                    kd4.a((Object) view2, "binding.line");
                    view2.setSelected(false);
                    FlexibleTextView flexibleTextView9 = a2.u;
                    kd4.a((Object) flexibleTextView9, "binding.ftvDailyValue");
                    flexibleTextView9.setSelected(false);
                    FlexibleTextView flexibleTextView10 = a2.t;
                    kd4.a((Object) flexibleTextView10, "binding.ftvDailyUnit");
                    flexibleTextView10.setSelected(false);
                }
                if (i3 == -1) {
                    ProgressBar progressBar = a2.D;
                    kd4.a((Object) progressBar, "binding.pbGoal");
                    progressBar.setProgress(0);
                    FlexibleTextView flexibleTextView11 = a2.y;
                    kd4.a((Object) flexibleTextView11, "binding.ftvProgressValue");
                    flexibleTextView11.setText(sm2.a(context, (int) R.string.character_dash_double));
                } else {
                    ProgressBar progressBar2 = a2.D;
                    kd4.a((Object) progressBar2, "binding.pbGoal");
                    progressBar2.setProgress(i3);
                    FlexibleTextView flexibleTextView12 = a2.y;
                    kd4.a((Object) flexibleTextView12, "binding.ftvProgressValue");
                    flexibleTextView12.setText(i3 + "%");
                }
                FlexibleTextView flexibleTextView13 = a2.x;
                kd4.a((Object) flexibleTextView13, "binding.ftvGoalValue");
                pd4 pd4 = pd4.a;
                String a3 = sm2.a(context, (int) R.string.DashboardHybrid_GoalTracking_DetailPage_Title__OfNumberTimes);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026age_Title__OfNumberTimes)");
                Object[] objArr = {Integer.valueOf(i2)};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView13.setText(format);
            }
        }
    }

    @DexIgnore
    public void a(wr2 wr2, ArrayList<String> arrayList) {
        kd4.b(wr2, "baseModel");
        kd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailFragment", "showDayDetailChart - baseModel=" + wr2);
        tr3<wb2> tr3 = this.l;
        if (tr3 != null) {
            wb2 a2 = tr3.a();
            if (a2 != null) {
                OverviewDayGoalChart overviewDayGoalChart = a2.s;
                if (overviewDayGoalChart != null) {
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) ll2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayGoalChart.a(wr2);
                }
            }
        }
    }
}
