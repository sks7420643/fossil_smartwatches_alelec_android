package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y6 */
public class C3344y6 extends com.fossil.blesdk.obfuscated.C3169w6 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Class f11191a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.reflect.Constructor f11192b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.reflect.Method f11193c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.reflect.Method f11194d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.reflect.Method f11195e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.reflect.Method f11196f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.reflect.Method f11197g;

    @DexIgnore
    public C3344y6() {
        java.lang.reflect.Method method;
        java.lang.reflect.Method method2;
        java.lang.reflect.Method method3;
        java.lang.reflect.Method method4;
        java.lang.reflect.Constructor constructor;
        java.lang.reflect.Method method5;
        java.lang.Class cls = null;
        try {
            java.lang.Class c = mo18028c();
            constructor = mo18032e(c);
            method4 = mo18027b(c);
            method3 = mo18029c(c);
            method2 = mo18033f(c);
            method = mo18020a(c);
            java.lang.Class cls2 = c;
            method5 = mo18031d(c);
            cls = cls2;
        } catch (java.lang.ClassNotFoundException | java.lang.NoSuchMethodException e) {
            android.util.Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e.getClass().getName(), e);
            method5 = null;
            constructor = null;
            method4 = null;
            method3 = null;
            method2 = null;
            method = null;
        }
        this.f11191a = cls;
        this.f11192b = constructor;
        this.f11193c = method4;
        this.f11194d = method3;
        this.f11195e = method2;
        this.f11196f = method;
        this.f11197g = method5;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo18022a() {
        if (this.f11193c == null) {
            android.util.Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return this.f11193c != null;
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.Object mo18026b() {
        try {
            return this.f11192b.newInstance(new java.lang.Object[0]);
        } catch (java.lang.IllegalAccessException | java.lang.InstantiationException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo18030c(java.lang.Object obj) {
        try {
            return ((java.lang.Boolean) this.f11195e.invoke(obj, new java.lang.Object[0])).booleanValue();
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.reflect.Method mo18031d(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        java.lang.Class cls2 = java.lang.Integer.TYPE;
        java.lang.reflect.Method declaredMethod = android.graphics.Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", new java.lang.Class[]{java.lang.reflect.Array.newInstance(cls, 1).getClass(), cls2, cls2});
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.reflect.Constructor mo18032e(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        return cls.getConstructor(new java.lang.Class[0]);
    }

    @DexIgnore
    /* renamed from: f */
    public java.lang.reflect.Method mo18033f(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        return cls.getMethod("freeze", new java.lang.Class[0]);
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Typeface mo18025b(java.lang.Object obj) {
        try {
            java.lang.Object newInstance = java.lang.reflect.Array.newInstance(this.f11191a, 1);
            java.lang.reflect.Array.set(newInstance, 0, obj);
            return (android.graphics.Typeface) this.f11197g.invoke((java.lang.Object) null, new java.lang.Object[]{newInstance, -1, -1});
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo18023a(android.content.Context context, java.lang.Object obj, java.lang.String str, int i, int i2, int i3, android.graphics.fonts.FontVariationAxis[] fontVariationAxisArr) {
        try {
            return ((java.lang.Boolean) this.f11193c.invoke(obj, new java.lang.Object[]{context.getAssets(), str, 0, false, java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(i2), java.lang.Integer.valueOf(i3), fontVariationAxisArr})).booleanValue();
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class mo18028c() throws java.lang.ClassNotFoundException {
        return java.lang.Class.forName("android.graphics.FontFamily");
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.reflect.Method mo18029c(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        java.lang.Class cls2 = java.lang.Integer.TYPE;
        return cls.getMethod("addFontFromBuffer", new java.lang.Class[]{java.nio.ByteBuffer.class, cls2, android.graphics.fonts.FontVariationAxis[].class, cls2, cls2});
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.reflect.Method mo18027b(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        java.lang.Class cls2 = java.lang.Integer.TYPE;
        return cls.getMethod("addFontFromAssetManager", new java.lang.Class[]{android.content.res.AssetManager.class, java.lang.String.class, java.lang.Integer.TYPE, java.lang.Boolean.TYPE, cls2, cls2, cls2, android.graphics.fonts.FontVariationAxis[].class});
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo18024a(java.lang.Object obj, java.nio.ByteBuffer byteBuffer, int i, int i2, int i3) {
        try {
            return ((java.lang.Boolean) this.f11194d.invoke(obj, new java.lang.Object[]{byteBuffer, java.lang.Integer.valueOf(i), null, java.lang.Integer.valueOf(i2), java.lang.Integer.valueOf(i3)})).booleanValue();
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo18021a(java.lang.Object obj) {
        try {
            this.f11196f.invoke(obj, new java.lang.Object[0]);
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8578a(android.content.Context context, com.fossil.blesdk.obfuscated.C2528o6.C2530b bVar, android.content.res.Resources resources, int i) {
        if (!mo18022a()) {
            return super.mo8578a(context, bVar, resources, i);
        }
        java.lang.Object b = mo18026b();
        for (com.fossil.blesdk.obfuscated.C2528o6.C2531c cVar : bVar.mo14259a()) {
            if (!mo18023a(context, b, cVar.mo14260a(), cVar.mo14262c(), cVar.mo14264e(), cVar.mo14265f() ? 1 : 0, android.graphics.fonts.FontVariationAxis.fromFontVariationSettings(cVar.mo14263d()))) {
                mo18021a(b);
                return null;
            }
        }
        if (!mo18030c(b)) {
            return null;
        }
        return mo18025b(b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004b, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        if (r11 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        throw r13;
     */
    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8577a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        if (fVarArr.length < 1) {
            return null;
        }
        if (!mo18022a()) {
            com.fossil.blesdk.obfuscated.C3012u7.C3018f a = mo8581a(fVarArr, i);
            try {
                android.os.ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(a.mo16677c(), "r", cancellationSignal);
                if (openFileDescriptor == null) {
                    if (openFileDescriptor != null) {
                        openFileDescriptor.close();
                    }
                    return null;
                }
                android.graphics.Typeface build = new android.graphics.Typeface.Builder(openFileDescriptor.getFileDescriptor()).setWeight(a.mo16678d()).setItalic(a.mo16679e()).build();
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                return build;
            } catch (java.io.IOException unused) {
                return null;
            } catch (Throwable th) {
                r12.addSuppressed(th);
            }
        } else {
            java.util.Map<android.net.Uri, java.nio.ByteBuffer> a2 = com.fossil.blesdk.obfuscated.C3012u7.m14603a(context, fVarArr, cancellationSignal);
            java.lang.Object b = mo18026b();
            boolean z = false;
            for (com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar : fVarArr) {
                java.nio.ByteBuffer byteBuffer = a2.get(fVar.mo16677c());
                if (byteBuffer != null) {
                    if (!mo18024a(b, byteBuffer, fVar.mo16676b(), fVar.mo16678d(), fVar.mo16679e() ? 1 : 0)) {
                        mo18021a(b);
                        return null;
                    }
                    z = true;
                }
            }
            if (!z) {
                mo18021a(b);
                return null;
            } else if (!mo18030c(b)) {
                return null;
            } else {
                return android.graphics.Typeface.create(mo18025b(b), i);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8576a(android.content.Context context, android.content.res.Resources resources, int i, java.lang.String str, int i2) {
        if (!mo18022a()) {
            return super.mo8576a(context, resources, i, str, i2);
        }
        java.lang.Object b = mo18026b();
        if (!mo18023a(context, b, str, 0, -1, -1, (android.graphics.fonts.FontVariationAxis[]) null)) {
            mo18021a(b);
            return null;
        } else if (!mo18030c(b)) {
            return null;
        } else {
            return mo18025b(b);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.reflect.Method mo18020a(java.lang.Class cls) throws java.lang.NoSuchMethodException {
        return cls.getMethod("abortCreation", new java.lang.Class[0]);
    }
}
