package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oq3 implements Factory<FindDevicePresenter> {
    @DexIgnore
    public static FindDevicePresenter a(rc rcVar, DeviceRepository deviceRepository, en2 en2, lq3 lq3, er2 er2, fr2 fr2, GetAddress getAddress, gr2 gr2, PortfolioApp portfolioApp) {
        return new FindDevicePresenter(rcVar, deviceRepository, en2, lq3, er2, fr2, getAddress, gr2, portfolioApp);
    }
}
