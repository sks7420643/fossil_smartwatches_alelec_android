package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rc */
public final class C2796rc {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.Object f8899f; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: g */
    public static com.fossil.blesdk.obfuscated.C2796rc f8900g;

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f8901a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<com.fossil.blesdk.obfuscated.C2796rc.C2799c>> f8902b; // = new java.util.HashMap<>();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.HashMap<java.lang.String, java.util.ArrayList<com.fossil.blesdk.obfuscated.C2796rc.C2799c>> f8903c; // = new java.util.HashMap<>();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C2796rc.C2798b> f8904d; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.os.Handler f8905e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rc$a */
    public class C2797a extends android.os.Handler {
        @DexIgnore
        public C2797a(android.os.Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(android.os.Message message) {
            if (message.what != 1) {
                super.handleMessage(message);
            } else {
                com.fossil.blesdk.obfuscated.C2796rc.this.mo15482a();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.rc$b */
    public static final class C2798b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Intent f8907a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C2796rc.C2799c> f8908b;

        @DexIgnore
        public C2798b(android.content.Intent intent, java.util.ArrayList<com.fossil.blesdk.obfuscated.C2796rc.C2799c> arrayList) {
            this.f8907a = intent;
            this.f8908b = arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rc$c")
    /* renamed from: com.fossil.blesdk.obfuscated.rc$c */
    public static final class C2799c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.IntentFilter f8909a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.content.BroadcastReceiver f8910b;

        @DexIgnore
        /* renamed from: c */
        public boolean f8911c;

        @DexIgnore
        /* renamed from: d */
        public boolean f8912d;

        @DexIgnore
        public C2799c(android.content.IntentFilter intentFilter, android.content.BroadcastReceiver broadcastReceiver) {
            this.f8909a = intentFilter;
            this.f8910b = broadcastReceiver;
        }

        @DexIgnore
        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.f8910b);
            sb.append(" filter=");
            sb.append(this.f8909a);
            if (this.f8912d) {
                sb.append(" DEAD");
            }
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore
    public C2796rc(android.content.Context context) {
        this.f8901a = context;
        this.f8905e = new com.fossil.blesdk.obfuscated.C2796rc.C2797a(context.getMainLooper());
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2796rc m13141a(android.content.Context context) {
        com.fossil.blesdk.obfuscated.C2796rc rcVar;
        synchronized (f8899f) {
            if (f8900g == null) {
                f8900g = new com.fossil.blesdk.obfuscated.C2796rc(context.getApplicationContext());
            }
            rcVar = f8900g;
        }
        return rcVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15484a(android.content.BroadcastReceiver broadcastReceiver, android.content.IntentFilter intentFilter) {
        synchronized (this.f8902b) {
            com.fossil.blesdk.obfuscated.C2796rc.C2799c cVar = new com.fossil.blesdk.obfuscated.C2796rc.C2799c(intentFilter, broadcastReceiver);
            java.util.ArrayList arrayList = this.f8902b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new java.util.ArrayList(1);
                this.f8902b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(cVar);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                java.lang.String action = intentFilter.getAction(i);
                java.util.ArrayList arrayList2 = this.f8903c.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new java.util.ArrayList(1);
                    this.f8903c.put(action, arrayList2);
                }
                arrayList2.add(cVar);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15483a(android.content.BroadcastReceiver broadcastReceiver) {
        synchronized (this.f8902b) {
            java.util.ArrayList remove = this.f8902b.remove(broadcastReceiver);
            if (remove != null) {
                for (int size = remove.size() - 1; size >= 0; size--) {
                    com.fossil.blesdk.obfuscated.C2796rc.C2799c cVar = (com.fossil.blesdk.obfuscated.C2796rc.C2799c) remove.get(size);
                    cVar.f8912d = true;
                    for (int i = 0; i < cVar.f8909a.countActions(); i++) {
                        java.lang.String action = cVar.f8909a.getAction(i);
                        java.util.ArrayList arrayList = this.f8903c.get(action);
                        if (arrayList != null) {
                            for (int size2 = arrayList.size() - 1; size2 >= 0; size2--) {
                                com.fossil.blesdk.obfuscated.C2796rc.C2799c cVar2 = (com.fossil.blesdk.obfuscated.C2796rc.C2799c) arrayList.get(size2);
                                if (cVar2.f8910b == broadcastReceiver) {
                                    cVar2.f8912d = true;
                                    arrayList.remove(size2);
                                }
                            }
                            if (arrayList.size() <= 0) {
                                this.f8903c.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0171, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0173, code lost:
        return false;
     */
    @DexIgnore
    /* renamed from: a */
    public boolean mo15485a(android.content.Intent intent) {
        java.lang.String str;
        java.util.ArrayList arrayList;
        int i;
        java.lang.String str2;
        java.util.ArrayList arrayList2;
        android.content.Intent intent2 = intent;
        synchronized (this.f8902b) {
            java.lang.String action = intent.getAction();
            java.lang.String resolveTypeIfNeeded = intent2.resolveTypeIfNeeded(this.f8901a.getContentResolver());
            android.net.Uri data = intent.getData();
            java.lang.String scheme = intent.getScheme();
            java.util.Set<java.lang.String> categories = intent.getCategories();
            boolean z = (intent.getFlags() & 8) != 0;
            if (z) {
                android.util.Log.v("LocalBroadcastManager", "Resolving type " + resolveTypeIfNeeded + " scheme " + scheme + " of intent " + intent2);
            }
            java.util.ArrayList arrayList3 = this.f8903c.get(intent.getAction());
            if (arrayList3 != null) {
                if (z) {
                    android.util.Log.v("LocalBroadcastManager", "Action list: " + arrayList3);
                }
                java.util.ArrayList arrayList4 = null;
                int i2 = 0;
                while (i2 < arrayList3.size()) {
                    com.fossil.blesdk.obfuscated.C2796rc.C2799c cVar = (com.fossil.blesdk.obfuscated.C2796rc.C2799c) arrayList3.get(i2);
                    if (z) {
                        android.util.Log.v("LocalBroadcastManager", "Matching against filter " + cVar.f8909a);
                    }
                    if (cVar.f8911c) {
                        if (z) {
                            android.util.Log.v("LocalBroadcastManager", "  Filter's target already added");
                        }
                        i = i2;
                        arrayList = arrayList3;
                        str2 = action;
                        str = resolveTypeIfNeeded;
                        arrayList2 = arrayList4;
                    } else {
                        com.fossil.blesdk.obfuscated.C2796rc.C2799c cVar2 = cVar;
                        str2 = action;
                        arrayList2 = arrayList4;
                        i = i2;
                        arrayList = arrayList3;
                        str = resolveTypeIfNeeded;
                        int match = cVar.f8909a.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                        if (match >= 0) {
                            if (z) {
                                android.util.Log.v("LocalBroadcastManager", "  Filter matched!  match=0x" + java.lang.Integer.toHexString(match));
                            }
                            arrayList4 = arrayList2 == null ? new java.util.ArrayList() : arrayList2;
                            arrayList4.add(cVar2);
                            cVar2.f8911c = true;
                            i2 = i + 1;
                            action = str2;
                            arrayList3 = arrayList;
                            resolveTypeIfNeeded = str;
                        } else if (z) {
                            java.lang.String str3 = match != -4 ? match != -3 ? match != -2 ? match != -1 ? "unknown reason" : "type" : "data" : "action" : "category";
                            android.util.Log.v("LocalBroadcastManager", "  Filter did not match: " + str3);
                        }
                    }
                    arrayList4 = arrayList2;
                    i2 = i + 1;
                    action = str2;
                    arrayList3 = arrayList;
                    resolveTypeIfNeeded = str;
                }
                java.util.ArrayList arrayList5 = arrayList4;
                if (arrayList5 != null) {
                    for (int i3 = 0; i3 < arrayList5.size(); i3++) {
                        ((com.fossil.blesdk.obfuscated.C2796rc.C2799c) arrayList5.get(i3)).f8911c = false;
                    }
                    this.f8904d.add(new com.fossil.blesdk.obfuscated.C2796rc.C2798b(intent2, arrayList5));
                    if (!this.f8905e.hasMessages(1)) {
                        this.f8905e.sendEmptyMessage(1);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r2 >= r1.length) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r3 = r1[r2];
        r4 = r3.f8908b.size();
        r5 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0028, code lost:
        if (r5 >= r4) goto L_0x0042;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r6 = r3.f8908b.get(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0034, code lost:
        if (r6.f8912d != false) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0036, code lost:
        r6.f8910b.onReceive(r9.f8901a, r3.f8907a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        r5 = r5 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r2 = 0;
     */
    @DexIgnore
    /* renamed from: a */
    public void mo15482a() {
        while (true) {
            synchronized (this.f8902b) {
                int size = this.f8904d.size();
                if (size > 0) {
                    com.fossil.blesdk.obfuscated.C2796rc.C2798b[] bVarArr = new com.fossil.blesdk.obfuscated.C2796rc.C2798b[size];
                    this.f8904d.toArray(bVarArr);
                    this.f8904d.clear();
                } else {
                    return;
                }
            }
        }
        while (true) {
        }
    }
}
