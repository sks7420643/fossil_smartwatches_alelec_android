package com.fossil.blesdk.obfuscated;

import android.app.ListActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f04 extends ListActivity {
    @DexIgnore
    public void onPause() {
        super.onPause();
        i04.a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        i04.b(this);
    }
}
