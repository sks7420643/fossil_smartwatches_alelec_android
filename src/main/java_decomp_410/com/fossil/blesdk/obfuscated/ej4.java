package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.TimeoutKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ej4<U, T extends U> extends uf4<T> implements Runnable, yb4<T>, fc4 {
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ yb4<U> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ej4(long j, yb4<? super U> yb4) {
        super(yb4.getContext(), true);
        kd4.b(yb4, "uCont");
        this.h = j;
        this.i = yb4;
    }

    @DexIgnore
    public void a(Object obj, int i2) {
        if (obj instanceof ng4) {
            wi4.a(this.i, ((ng4) obj).a, i2);
        } else {
            wi4.b(this.i, obj, i2);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public String g() {
        return super.g() + "(timeMillis=" + this.h + ')';
    }

    @DexIgnore
    public fc4 getCallerFrame() {
        yb4<U> yb4 = this.i;
        if (!(yb4 instanceof fc4)) {
            yb4 = null;
        }
        return (fc4) yb4;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }

    @DexIgnore
    public void run() {
        a((Throwable) TimeoutKt.a(this.h, (fi4) this));
    }
}
