package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wg */
public class C3218wg extends com.fossil.blesdk.obfuscated.C1713eb {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wg$a */
    public class C3219a extends androidx.transition.Transition.C0327e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.graphics.Rect f10623a;

        @DexIgnore
        public C3219a(com.fossil.blesdk.obfuscated.C3218wg wgVar, android.graphics.Rect rect) {
            this.f10623a = rect;
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.Rect mo3568a(androidx.transition.Transition transition) {
            return this.f10623a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wg$b")
    /* renamed from: com.fossil.blesdk.obfuscated.wg$b */
    public class C3220b implements androidx.transition.Transition.C0328f {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.view.View f10624a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.util.ArrayList f10625b;

        @DexIgnore
        public C3220b(com.fossil.blesdk.obfuscated.C3218wg wgVar, android.view.View view, java.util.ArrayList arrayList) {
            this.f10624a = view;
            this.f10625b = arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo3569a(androidx.transition.Transition transition) {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo3463b(androidx.transition.Transition transition) {
        }

        @DexIgnore
        /* renamed from: c */
        public void mo3464c(androidx.transition.Transition transition) {
            transition.mo3541b((androidx.transition.Transition.C0328f) this);
            this.f10624a.setVisibility(8);
            int size = this.f10625b.size();
            for (int i = 0; i < size; i++) {
                ((android.view.View) this.f10625b.get(i)).setVisibility(0);
            }
        }

        @DexIgnore
        /* renamed from: d */
        public void mo3465d(androidx.transition.Transition transition) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wg$c")
    /* renamed from: com.fossil.blesdk.obfuscated.wg$c */
    public class C3221c implements androidx.transition.Transition.C0328f {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ java.lang.Object f10626a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.util.ArrayList f10627b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ java.lang.Object f10628c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ java.util.ArrayList f10629d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.Object f10630e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.ArrayList f10631f;

        @DexIgnore
        public C3221c(java.lang.Object obj, java.util.ArrayList arrayList, java.lang.Object obj2, java.util.ArrayList arrayList2, java.lang.Object obj3, java.util.ArrayList arrayList3) {
            this.f10626a = obj;
            this.f10627b = arrayList;
            this.f10628c = obj2;
            this.f10629d = arrayList2;
            this.f10630e = obj3;
            this.f10631f = arrayList3;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo3569a(androidx.transition.Transition transition) {
            java.lang.Object obj = this.f10626a;
            if (obj != null) {
                com.fossil.blesdk.obfuscated.C3218wg.this.mo9866a(obj, (java.util.ArrayList<android.view.View>) this.f10627b, (java.util.ArrayList<android.view.View>) null);
            }
            java.lang.Object obj2 = this.f10628c;
            if (obj2 != null) {
                com.fossil.blesdk.obfuscated.C3218wg.this.mo9866a(obj2, (java.util.ArrayList<android.view.View>) this.f10629d, (java.util.ArrayList<android.view.View>) null);
            }
            java.lang.Object obj3 = this.f10630e;
            if (obj3 != null) {
                com.fossil.blesdk.obfuscated.C3218wg.this.mo9866a(obj3, (java.util.ArrayList<android.view.View>) this.f10631f, (java.util.ArrayList<android.view.View>) null);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo3463b(androidx.transition.Transition transition) {
        }

        @DexIgnore
        /* renamed from: c */
        public void mo3464c(androidx.transition.Transition transition) {
        }

        @DexIgnore
        /* renamed from: d */
        public void mo3465d(androidx.transition.Transition transition) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wg$d")
    /* renamed from: com.fossil.blesdk.obfuscated.wg$d */
    public class C3222d extends androidx.transition.Transition.C0327e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.graphics.Rect f10633a;

        @DexIgnore
        public C3222d(com.fossil.blesdk.obfuscated.C3218wg wgVar, android.graphics.Rect rect) {
            this.f10633a = rect;
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.Rect mo3568a(androidx.transition.Transition transition) {
            android.graphics.Rect rect = this.f10633a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.f10633a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9867a(java.lang.Object obj) {
        return obj instanceof androidx.transition.Transition;
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object mo9868b(java.lang.Object obj) {
        if (obj != null) {
            return ((androidx.transition.Transition) obj).clone();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Object mo9873c(java.lang.Object obj) {
        if (obj == null) {
            return null;
        }
        androidx.transition.TransitionSet transitionSet = new androidx.transition.TransitionSet();
        transitionSet.mo3572a((androidx.transition.Transition) obj);
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9865a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList) {
        androidx.transition.Transition transition = (androidx.transition.Transition) obj;
        if (transition != null) {
            int i = 0;
            if (transition instanceof androidx.transition.TransitionSet) {
                androidx.transition.TransitionSet transitionSet = (androidx.transition.TransitionSet) transition;
                int A = transitionSet.mo3570A();
                while (i < A) {
                    mo9865a((java.lang.Object) transitionSet.mo3573b(i), arrayList);
                    i++;
                }
            } else if (!m15792a(transition) && com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.mo3561m())) {
                int size = arrayList.size();
                while (i < size) {
                    transition.mo3518a(arrayList.get(i));
                    i++;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9871b(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList) {
        androidx.transition.TransitionSet transitionSet = (androidx.transition.TransitionSet) obj;
        java.util.List<android.view.View> m = transitionSet.mo3561m();
        m.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C1713eb.m6362a(m, arrayList.get(i));
        }
        m.add(view);
        arrayList.add(view);
        mo9865a((java.lang.Object) transitionSet, arrayList);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9874c(java.lang.Object obj, android.view.View view) {
        if (view != null) {
            android.graphics.Rect rect = new android.graphics.Rect();
            mo10405a(view, rect);
            ((androidx.transition.Transition) obj).mo3529a((androidx.transition.Transition.C0327e) new com.fossil.blesdk.obfuscated.C3218wg.C3219a(this, rect));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object mo9869b(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3) {
        androidx.transition.TransitionSet transitionSet = new androidx.transition.TransitionSet();
        if (obj != null) {
            transitionSet.mo3572a((androidx.transition.Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.mo3572a((androidx.transition.Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.mo3572a((androidx.transition.Transition) obj3);
        }
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15792a(androidx.transition.Transition transition) {
        return !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.mo3558j()) || !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.mo3559k()) || !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.mo3560l());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9872b(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2) {
        androidx.transition.TransitionSet transitionSet = (androidx.transition.TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.mo3561m().clear();
            transitionSet.mo3561m().addAll(arrayList2);
            mo9866a((java.lang.Object) transitionSet, arrayList, arrayList2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9863a(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList) {
        ((androidx.transition.Transition) obj).mo3519a((androidx.transition.Transition.C0328f) new com.fossil.blesdk.obfuscated.C3218wg.C3220b(this, view, arrayList));
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo9859a(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3) {
        androidx.transition.Transition transition = (androidx.transition.Transition) obj;
        androidx.transition.Transition transition2 = (androidx.transition.Transition) obj2;
        androidx.transition.Transition transition3 = (androidx.transition.Transition) obj3;
        if (transition != null && transition2 != null) {
            transition = new androidx.transition.TransitionSet().mo3572a(transition).mo3572a(transition2).mo3574c(1);
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        androidx.transition.TransitionSet transitionSet = new androidx.transition.TransitionSet();
        if (transition != null) {
            transitionSet.mo3572a(transition);
        }
        transitionSet.mo3572a(transition3);
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9870b(java.lang.Object obj, android.view.View view) {
        if (obj != null) {
            ((androidx.transition.Transition) obj).mo3551d(view);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9860a(android.view.ViewGroup viewGroup, java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.C2412mh.m10754a(viewGroup, (androidx.transition.Transition) obj);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9864a(java.lang.Object obj, java.lang.Object obj2, java.util.ArrayList<android.view.View> arrayList, java.lang.Object obj3, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj4, java.util.ArrayList<android.view.View> arrayList3) {
        com.fossil.blesdk.obfuscated.C3218wg.C3221c cVar = new com.fossil.blesdk.obfuscated.C3218wg.C3221c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3);
        ((androidx.transition.Transition) obj).mo3519a((androidx.transition.Transition.C0328f) cVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9866a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2) {
        int i;
        androidx.transition.Transition transition = (androidx.transition.Transition) obj;
        int i2 = 0;
        if (transition instanceof androidx.transition.TransitionSet) {
            androidx.transition.TransitionSet transitionSet = (androidx.transition.TransitionSet) transition;
            int A = transitionSet.mo3570A();
            while (i2 < A) {
                mo9866a((java.lang.Object) transitionSet.mo3573b(i2), arrayList, arrayList2);
                i2++;
            }
        } else if (!m15792a(transition)) {
            java.util.List<android.view.View> m = transition.mo3561m();
            if (m.size() == arrayList.size() && m.containsAll(arrayList)) {
                if (arrayList2 == null) {
                    i = 0;
                } else {
                    i = arrayList2.size();
                }
                while (i2 < i) {
                    transition.mo3518a(arrayList2.get(i2));
                    i2++;
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    transition.mo3551d(arrayList.get(size));
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9862a(java.lang.Object obj, android.view.View view) {
        if (obj != null) {
            ((androidx.transition.Transition) obj).mo3518a(view);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9861a(java.lang.Object obj, android.graphics.Rect rect) {
        if (obj != null) {
            ((androidx.transition.Transition) obj).mo3529a((androidx.transition.Transition.C0327e) new com.fossil.blesdk.obfuscated.C3218wg.C3222d(this, rect));
        }
    }
}
