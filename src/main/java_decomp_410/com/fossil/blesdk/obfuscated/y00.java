package com.fossil.blesdk.obfuscated;

import android.os.Build;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.lang.reflect.Type;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y00 {
    @DexIgnore
    public static /* final */ Version a; // = new Version((byte) 4, (byte) 0);
    @DexIgnore
    public static /* final */ PhaseId[] b; // = {PhaseId.MAKE_DEVICE_READY, PhaseId.FETCH_DEVICE_INFORMATION, PhaseId.READ_DEVICE_INFO_CHARACTERISTICS, PhaseId.OTA, PhaseId.PLAY_ANIMATION, PhaseId.CLEAN_UP_DEVICE, PhaseId.SYNC, PhaseId.READ_RSSI, PhaseId.SYNC_FLOW, PhaseId.SEND_CUSTOM_COMMAND};
    @DexIgnore
    public static /* final */ Type[] c; // = {j30.class, m30.class};
    @DexIgnore
    public static /* final */ ConnectionParametersSet[] d; // = {new ConnectionParametersSet(12, 12, 30, 600), new ConnectionParametersSet(18, 18, 28, 600), new ConnectionParametersSet(24, 24, 26, 600), new ConnectionParametersSet(48, 48, 25, 600), new ConnectionParametersSet(72, 72, 23, 600)};
    @DexIgnore
    public static /* final */ ConnectionParametersSet[] e; // = {new ConnectionParametersSet(12, 12, 45, 600), new ConnectionParametersSet(24, 24, 22, 600), new ConnectionParametersSet(36, 36, 15, 600), new ConnectionParametersSet(104, 112, 4, 600)};
    @DexIgnore
    public static /* final */ y00 f; // = new y00();

    /*
    static {
        new Type[1][0] = h30.class;
    }
    */

    @DexIgnore
    public final boolean a(DeviceInformation deviceInformation, Phase phase) {
        kd4.b(deviceInformation, "deviceInformation");
        kd4.b(phase, "phase");
        if (f.b(deviceInformation)) {
            return za4.b((T[]) b, phase.g());
        }
        return true;
    }

    @DexIgnore
    public final boolean a(boolean z) {
        return false;
    }

    @DexIgnore
    public final long b(boolean z) {
        return z ? 1800000 : 30000;
    }

    @DexIgnore
    public final boolean b(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "deviceInformation");
        if (deviceInformation.getDeviceType() == DeviceType.SE1 || deviceInformation.getDeviceType() == DeviceType.SLIM || deviceInformation.getDeviceType() == DeviceType.MINI) {
            Version version = deviceInformation.getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.OTA.getFileHandleMask$blesdk_productionRelease()));
            if (version == null) {
                version = ua0.y.h();
            }
            if (version.compareTo(ua0.y.x()) < 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final Type[] c() {
        Object[] array = cb4.a((T[]) new Type[]{g40.class, v30.class, a40.class, b40.class, f30.class, d40.class, i40.class, i30.class, j30.class, k30.class, m30.class, k40.class, n30.class, o30.class, q30.class, t30.class, u30.class, l40.class, x30.class, y30.class, n40.class, z30.class, r40.class, u40.class, o40.class, s30.class, l30.class, c40.class, e40.class, f40.class, m40.class, h40.class, j40.class, s40.class, t40.class, p40.class, r30.class, q40.class, w30.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Version d() {
        return a;
    }

    @DexIgnore
    public final Type[] e() {
        Object[] array = cb4.a((T[]) new Type[]{v30.class, a40.class, b40.class, f30.class, d40.class, i30.class, j30.class, k30.class, m30.class, n30.class, o30.class, q30.class, u30.class, v40.class, w40.class, x40.class, p30.class, g30.class, x30.class, y30.class, z30.class, f40.class, m40.class, h40.class, j40.class, t40.class, c40.class, q40.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] f() {
        Object[] array = cb4.a((T[]) new Type[]{v30.class, a40.class, b40.class, f30.class, d40.class, i30.class, j30.class, k30.class, m30.class, n30.class, o30.class, q30.class, u30.class, v40.class, w40.class, x40.class, p30.class, g30.class, x30.class, y30.class, z30.class, f40.class, m40.class, h40.class, j40.class, t40.class, c40.class, q40.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] g() {
        Object[] array = cb4.a((T[]) new Type[]{v30.class, a40.class, b40.class, f30.class, d40.class, i30.class, j30.class, k30.class, m30.class, o30.class, q30.class, u30.class, v40.class, w40.class, x40.class, p30.class, g30.class, x30.class, y30.class, z30.class, m40.class, h40.class, j40.class, t40.class, c40.class, q40.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final Type[] h() {
        return c;
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT < 28;
    }

    @DexIgnore
    public final ConnectionParametersSet[] a(DeviceInformation deviceInformation) {
        kd4.b(deviceInformation, "deviceInformation");
        switch (x00.a[deviceInformation.getDeviceType().ordinal()]) {
            case 1:
                return d;
            case 2:
            case 3:
            case 4:
                if (b(deviceInformation)) {
                    return new ConnectionParametersSet[0];
                }
                return e;
            case 5:
            case 6:
                return new ConnectionParametersSet[0];
            default:
                throw new NoWhenBranchMatchedException();
        }
    }

    @DexIgnore
    public final Type[] b() {
        Object[] array = cb4.a((T[]) new Type[]{g40.class, v30.class, a40.class, b40.class, f30.class, d40.class, i40.class, i30.class, j30.class, k30.class, m30.class, k40.class, n30.class, o30.class, q30.class, t30.class, u30.class, l40.class, x30.class, y30.class, n40.class, z30.class, r40.class, u40.class, o40.class, s30.class, l30.class, c40.class, e40.class, f40.class, m40.class, h40.class, j40.class, s40.class, t40.class, p40.class, r30.class, q40.class, w30.class}).toArray(new Type[0]);
        if (array != null) {
            return (Type[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
