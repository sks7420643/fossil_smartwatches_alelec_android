package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a6 */
public abstract class C1384a6 extends android.app.Service {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ java.util.HashMap<android.content.ComponentName, com.fossil.blesdk.obfuscated.C1384a6.C1393h> f3365k; // = new java.util.HashMap<>();

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1384a6.C1386b f3366e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1384a6.C1393h f3367f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1384a6.C1385a f3368g;

    @DexIgnore
    /* renamed from: h */
    public boolean f3369h; // = false;

    @DexIgnore
    /* renamed from: i */
    public boolean f3370i; // = false;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C1384a6.C1388d> f3371j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$a */
    public final class C1385a extends android.os.AsyncTask<java.lang.Void, java.lang.Void, java.lang.Void> {
        @DexIgnore
        public C1385a() {
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Void doInBackground(java.lang.Void... voidArr) {
            while (true) {
                com.fossil.blesdk.obfuscated.C1384a6.C1389e a = com.fossil.blesdk.obfuscated.C1384a6.this.mo8545a();
                if (a == null) {
                    return null;
                }
                com.fossil.blesdk.obfuscated.C1384a6.this.mo8546a(a.getIntent());
                a.mo8566a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void onPostExecute(java.lang.Void voidR) {
            com.fossil.blesdk.obfuscated.C1384a6.this.mo8550d();
        }

        @DexIgnore
        /* renamed from: a */
        public void onCancelled(java.lang.Void voidR) {
            com.fossil.blesdk.obfuscated.C1384a6.this.mo8550d();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a6$b */
    public interface C1386b {
        @DexIgnore
        /* renamed from: a */
        android.os.IBinder mo8561a();

        @DexIgnore
        /* renamed from: b */
        com.fossil.blesdk.obfuscated.C1384a6.C1389e mo8562b();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$c")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$c */
    public static final class C1387c extends com.fossil.blesdk.obfuscated.C1384a6.C1393h {

        @DexIgnore
        /* renamed from: d */
        public /* final */ android.os.PowerManager.WakeLock f3373d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.os.PowerManager.WakeLock f3374e;

        @DexIgnore
        /* renamed from: f */
        public boolean f3375f;

        @DexIgnore
        /* renamed from: g */
        public boolean f3376g;

        @DexIgnore
        public C1387c(android.content.Context context, android.content.ComponentName componentName) {
            super(context, componentName);
            context.getApplicationContext();
            android.os.PowerManager powerManager = (android.os.PowerManager) context.getSystemService("power");
            this.f3373d = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.f3373d.setReferenceCounted(false);
            this.f3374e = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.f3374e.setReferenceCounted(false);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8563a() {
            synchronized (this) {
                if (this.f3376g) {
                    if (this.f3375f) {
                        this.f3373d.acquire(60000);
                    }
                    this.f3376g = false;
                    this.f3374e.release();
                }
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8564b() {
            synchronized (this) {
                if (!this.f3376g) {
                    this.f3376g = true;
                    this.f3374e.acquire(600000);
                    this.f3373d.release();
                }
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8565c() {
            synchronized (this) {
                this.f3375f = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$d")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$d */
    public final class C1388d implements com.fossil.blesdk.obfuscated.C1384a6.C1389e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Intent f3377a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f3378b;

        @DexIgnore
        public C1388d(android.content.Intent intent, int i) {
            this.f3377a = intent;
            this.f3378b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8566a() {
            com.fossil.blesdk.obfuscated.C1384a6.this.stopSelf(this.f3378b);
        }

        @DexIgnore
        public android.content.Intent getIntent() {
            return this.f3377a;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.a6$e */
    public interface C1389e {
        @DexIgnore
        /* renamed from: a */
        void mo8566a();

        @DexIgnore
        android.content.Intent getIntent();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$f")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$f */
    public static final class C1390f extends android.app.job.JobServiceEngine implements com.fossil.blesdk.obfuscated.C1384a6.C1386b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1384a6 f3380a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.Object f3381b; // = new java.lang.Object();

        @DexIgnore
        /* renamed from: c */
        public android.app.job.JobParameters f3382c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$f$a")
        /* renamed from: com.fossil.blesdk.obfuscated.a6$f$a */
        public final class C1391a implements com.fossil.blesdk.obfuscated.C1384a6.C1389e {

            @DexIgnore
            /* renamed from: a */
            public /* final */ android.app.job.JobWorkItem f3383a;

            @DexIgnore
            public C1391a(android.app.job.JobWorkItem jobWorkItem) {
                this.f3383a = jobWorkItem;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo8566a() {
                synchronized (com.fossil.blesdk.obfuscated.C1384a6.C1390f.this.f3381b) {
                    if (com.fossil.blesdk.obfuscated.C1384a6.C1390f.this.f3382c != null) {
                        com.fossil.blesdk.obfuscated.C1384a6.C1390f.this.f3382c.completeWork(this.f3383a);
                    }
                }
            }

            @DexIgnore
            public android.content.Intent getIntent() {
                return this.f3383a.getIntent();
            }
        }

        @DexIgnore
        public C1390f(com.fossil.blesdk.obfuscated.C1384a6 a6Var) {
            super(a6Var);
            this.f3380a = a6Var;
        }

        @DexIgnore
        /* renamed from: a */
        public android.os.IBinder mo8561a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0013, code lost:
            r1.getIntent().setExtrasClassLoader(r3.f3380a.getClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            return new com.fossil.blesdk.obfuscated.C1384a6.C1390f.C1391a(r3, r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r1 == null) goto L_0x0026;
         */
        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1384a6.C1389e mo8562b() {
            synchronized (this.f3381b) {
                if (this.f3382c == null) {
                    return null;
                }
                android.app.job.JobWorkItem dequeueWork = this.f3382c.dequeueWork();
            }
        }

        @DexIgnore
        public boolean onStartJob(android.app.job.JobParameters jobParameters) {
            this.f3382c = jobParameters;
            this.f3380a.mo8547a(false);
            return true;
        }

        @DexIgnore
        public boolean onStopJob(android.app.job.JobParameters jobParameters) {
            boolean b = this.f3380a.mo8548b();
            synchronized (this.f3381b) {
                this.f3382c = null;
            }
            return b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$g")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$g */
    public static final class C1392g extends com.fossil.blesdk.obfuscated.C1384a6.C1393h {
        @DexIgnore
        public C1392g(android.content.Context context, android.content.ComponentName componentName, int i) {
            super(context, componentName);
            mo8570a(i);
            new android.app.job.JobInfo.Builder(i, this.f3385a).setOverrideDeadline(0).build();
            android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a6$h")
    /* renamed from: com.fossil.blesdk.obfuscated.a6$h */
    public static abstract class C1393h {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ComponentName f3385a;

        @DexIgnore
        /* renamed from: b */
        public boolean f3386b;

        @DexIgnore
        /* renamed from: c */
        public int f3387c;

        @DexIgnore
        public C1393h(android.content.Context context, android.content.ComponentName componentName) {
            this.f3385a = componentName;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8563a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8570a(int i) {
            if (!this.f3386b) {
                this.f3386b = true;
                this.f3387c = i;
            } else if (this.f3387c != i) {
                throw new java.lang.IllegalArgumentException("Given job ID " + i + " is different than previous " + this.f3387c);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8564b() {
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8565c() {
        }
    }

    @DexIgnore
    public C1384a6() {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            this.f3371j = null;
        } else {
            this.f3371j = new java.util.ArrayList<>();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1384a6.C1393h m4218a(android.content.Context context, android.content.ComponentName componentName, boolean z, int i) {
        com.fossil.blesdk.obfuscated.C1384a6.C1393h hVar;
        com.fossil.blesdk.obfuscated.C1384a6.C1393h hVar2 = f3365k.get(componentName);
        if (hVar2 != null) {
            return hVar2;
        }
        if (android.os.Build.VERSION.SDK_INT < 26) {
            hVar = new com.fossil.blesdk.obfuscated.C1384a6.C1387c(context, componentName);
        } else if (z) {
            hVar = new com.fossil.blesdk.obfuscated.C1384a6.C1392g(context, componentName, i);
        } else {
            throw new java.lang.IllegalArgumentException("Can't be here without a job id");
        }
        com.fossil.blesdk.obfuscated.C1384a6.C1393h hVar3 = hVar;
        f3365k.put(componentName, hVar3);
        return hVar3;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo8546a(android.content.Intent intent);

    @DexIgnore
    /* renamed from: b */
    public boolean mo8548b() {
        com.fossil.blesdk.obfuscated.C1384a6.C1385a aVar = this.f3368g;
        if (aVar != null) {
            aVar.cancel(this.f3369h);
        }
        return mo8549c();
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo8549c() {
        return true;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo8550d() {
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C1384a6.C1388d> arrayList = this.f3371j;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.f3368g = null;
                if (this.f3371j != null && this.f3371j.size() > 0) {
                    mo8547a(false);
                } else if (!this.f3370i) {
                    this.f3367f.mo8563a();
                }
            }
        }
    }

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        com.fossil.blesdk.obfuscated.C1384a6.C1386b bVar = this.f3366e;
        if (bVar != null) {
            return bVar.mo8561a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            this.f3366e = new com.fossil.blesdk.obfuscated.C1384a6.C1390f(this);
            this.f3367f = null;
            return;
        }
        this.f3366e = null;
        this.f3367f = m4218a(this, new android.content.ComponentName(this, com.fossil.blesdk.obfuscated.C1384a6.class), false, 0);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C1384a6.C1388d> arrayList = this.f3371j;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.f3370i = true;
                this.f3367f.mo8563a();
            }
        }
    }

    @DexIgnore
    public int onStartCommand(android.content.Intent intent, int i, int i2) {
        if (this.f3371j == null) {
            return 2;
        }
        this.f3367f.mo8565c();
        synchronized (this.f3371j) {
            java.util.ArrayList<com.fossil.blesdk.obfuscated.C1384a6.C1388d> arrayList = this.f3371j;
            if (intent == null) {
                intent = new android.content.Intent();
            }
            arrayList.add(new com.fossil.blesdk.obfuscated.C1384a6.C1388d(intent, i2));
            mo8547a(true);
        }
        return 3;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8547a(boolean z) {
        if (this.f3368g == null) {
            this.f3368g = new com.fossil.blesdk.obfuscated.C1384a6.C1385a();
            com.fossil.blesdk.obfuscated.C1384a6.C1393h hVar = this.f3367f;
            if (hVar != null && z) {
                hVar.mo8564b();
            }
            this.f3368g.executeOnExecutor(android.os.AsyncTask.THREAD_POOL_EXECUTOR, new java.lang.Void[0]);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1384a6.C1389e mo8545a() {
        com.fossil.blesdk.obfuscated.C1384a6.C1386b bVar = this.f3366e;
        if (bVar != null) {
            return bVar.mo8562b();
        }
        synchronized (this.f3371j) {
            if (this.f3371j.size() <= 0) {
                return null;
            }
            com.fossil.blesdk.obfuscated.C1384a6.C1389e remove = this.f3371j.remove(0);
            return remove;
        }
    }
}
