package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pt1 {
    @DexIgnore
    public /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pt1 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pt1 pt1, String str) {
            super(pt1, (a) null);
            this.b = str;
        }

        @DexIgnore
        public CharSequence a(Object obj) {
            return obj == null ? this.b : pt1.this.a(obj);
        }

        @DexIgnore
        public pt1 a(String str) {
            throw new UnsupportedOperationException("already specified useForNull");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ pt1 a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public /* synthetic */ b(pt1 pt1, String str, a aVar) {
            this(pt1, str);
        }

        @DexIgnore
        public StringBuilder a(StringBuilder sb, Map<?, ?> map) {
            a(sb, (Iterable<? extends Map.Entry<?, ?>>) map.entrySet());
            return sb;
        }

        @DexIgnore
        public b(pt1 pt1, String str) {
            this.a = pt1;
            st1.a(str);
            this.b = str;
        }

        @DexIgnore
        public <A extends Appendable> A a(A a2, Iterator<? extends Map.Entry<?, ?>> it) throws IOException {
            st1.a(a2);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a2.append(this.a.a(entry.getKey()));
                a2.append(this.b);
                a2.append(this.a.a(entry.getValue()));
                while (it.hasNext()) {
                    a2.append(this.a.a);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a2.append(this.a.a(entry2.getKey()));
                    a2.append(this.b);
                    a2.append(this.a.a(entry2.getValue()));
                }
            }
            return a2;
        }

        @DexIgnore
        public StringBuilder a(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            a(sb, iterable.iterator());
            return sb;
        }

        @DexIgnore
        public StringBuilder a(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                a(sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public /* synthetic */ pt1(pt1 pt1, a aVar) {
        this(pt1);
    }

    @DexIgnore
    public static pt1 c(String str) {
        return new pt1(str);
    }

    @DexIgnore
    public b b(String str) {
        return new b(this, str, (a) null);
    }

    @DexIgnore
    public pt1(String str) {
        st1.a(str);
        this.a = str;
    }

    @DexIgnore
    public static pt1 a(char c) {
        return new pt1(String.valueOf(c));
    }

    @DexIgnore
    public <A extends Appendable> A a(A a2, Iterator<?> it) throws IOException {
        st1.a(a2);
        if (it.hasNext()) {
            a2.append(a((Object) it.next()));
            while (it.hasNext()) {
                a2.append(this.a);
                a2.append(a((Object) it.next()));
            }
        }
        return a2;
    }

    @DexIgnore
    public pt1(pt1 pt1) {
        this.a = pt1.a;
    }

    @DexIgnore
    public final StringBuilder a(StringBuilder sb, Iterable<?> iterable) {
        a(sb, iterable.iterator());
        return sb;
    }

    @DexIgnore
    public final StringBuilder a(StringBuilder sb, Iterator<?> it) {
        try {
            a(sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final String a(Iterable<?> iterable) {
        return a(iterable.iterator());
    }

    @DexIgnore
    public final String a(Iterator<?> it) {
        StringBuilder sb = new StringBuilder();
        a(sb, it);
        return sb.toString();
    }

    @DexIgnore
    public final String a(Object[] objArr) {
        return a((Iterable<?>) Arrays.asList(objArr));
    }

    @DexIgnore
    public pt1 a(String str) {
        st1.a(str);
        return new a(this, str);
    }

    @DexIgnore
    public CharSequence a(Object obj) {
        st1.a(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }
}
