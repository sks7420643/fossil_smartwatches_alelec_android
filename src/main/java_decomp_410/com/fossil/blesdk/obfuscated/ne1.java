package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ne1 extends IInterface {
    @DexIgnore
    void a() throws RemoteException;

    @DexIgnore
    void a(Bundle bundle) throws RemoteException;

    @DexIgnore
    void a(ve1 ve1) throws RemoteException;

    @DexIgnore
    void b() throws RemoteException;

    @DexIgnore
    void b(Bundle bundle) throws RemoteException;

    @DexIgnore
    void c() throws RemoteException;

    @DexIgnore
    void d() throws RemoteException;

    @DexIgnore
    sn0 getView() throws RemoteException;

    @DexIgnore
    void onLowMemory() throws RemoteException;

    @DexIgnore
    void onPause() throws RemoteException;
}
