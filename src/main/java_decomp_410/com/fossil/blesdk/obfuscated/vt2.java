package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vt2 implements MembersInjector<AlarmActivity> {
    @DexIgnore
    public static void a(AlarmActivity alarmActivity, AlarmPresenter alarmPresenter) {
        alarmActivity.B = alarmPresenter;
    }
}
