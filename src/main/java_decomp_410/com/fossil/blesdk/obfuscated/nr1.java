package com.fossil.blesdk.obfuscated;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nr1 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public nr1(View view) {
        this.a = view;
    }

    @DexIgnore
    public boolean a(int i) {
        if (this.e == i) {
            return false;
        }
        this.e = i;
        e();
        return true;
    }

    @DexIgnore
    public boolean b(int i) {
        if (this.d == i) {
            return false;
        }
        this.d = i;
        e();
        return true;
    }

    @DexIgnore
    public int c() {
        return this.d;
    }

    @DexIgnore
    public void d() {
        this.b = this.a.getTop();
        this.c = this.a.getLeft();
        e();
    }

    @DexIgnore
    public final void e() {
        View view = this.a;
        f9.d(view, this.d - (view.getTop() - this.b));
        View view2 = this.a;
        f9.c(view2, this.e - (view2.getLeft() - this.c));
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.e;
    }
}
