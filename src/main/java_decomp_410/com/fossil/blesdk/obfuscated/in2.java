package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.util.Arrays;
import java.util.Collection;
import java.util.StringTokenizer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class in2 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ a c; // = new a((fd4) null);
    @DexIgnore
    public CallbackManager a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return in2.b;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final String[] a(String str) {
            kd4.b(str, "fullName");
            String[] strArr = {"", ""};
            StringTokenizer stringTokenizer = new StringTokenizer(str);
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                kd4.a((Object) nextToken, "tokenizer.nextToken()");
                strArr[0] = nextToken;
            }
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken2 = stringTokenizer.nextToken();
                kd4.a((Object) nextToken2, "tokenizer.nextToken()");
                strArr[1] = nextToken2;
            }
            return strArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements GraphRequest.GraphJSONObjectCallback {
        @DexIgnore
        public /* final */ /* synthetic */ ln2 a;
        @DexIgnore
        public /* final */ /* synthetic */ AccessToken b;

        @DexIgnore
        public b(ln2 ln2, AccessToken accessToken) {
            this.a = ln2;
            this.b = accessToken;
        }

        @DexIgnore
        public final void onCompleted(JSONObject jSONObject, GraphResponse graphResponse) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = in2.c.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .fetchGraphDataFacebook response=");
            sb.append(graphResponse);
            sb.append(", error=");
            kd4.a((Object) graphResponse, "response");
            sb.append(graphResponse.getError());
            local.d(a2, sb.toString());
            if (graphResponse.getError() != null) {
                this.a.a(600, (ud0) null, "");
            } else if (jSONObject != null) {
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String optString = jSONObject.optString("email");
                kd4.a((Object) optString, "me.optString(\"email\")");
                signUpSocialAuth.setEmail(optString);
                String optString2 = jSONObject.optString("name");
                String optString3 = jSONObject.optString("first_name");
                String optString4 = jSONObject.optString("last_name");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = in2.c.a();
                local2.d(a3, "Facebook email is " + jSONObject.optString("email") + " facebook name " + jSONObject.optString("name"));
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = in2.c.a();
                local3.d(a4, "Facebook first name = " + optString3 + " last name = " + optString4);
                if (!TextUtils.isEmpty(optString3) && !TextUtils.isEmpty(optString4)) {
                    kd4.a((Object) optString3, "firstName");
                    signUpSocialAuth.setFirstName(optString3);
                    kd4.a((Object) optString4, "lastName");
                    signUpSocialAuth.setLastName(optString4);
                } else if (!TextUtils.isEmpty(optString2)) {
                    a aVar = in2.c;
                    kd4.a((Object) optString2, "name");
                    String[] a5 = aVar.a(optString2);
                    signUpSocialAuth.setFirstName(a5[0]);
                    signUpSocialAuth.setLastName(a5[1]);
                }
                String token = this.b.getToken();
                kd4.a((Object) token, "token.token");
                signUpSocialAuth.setToken(token);
                signUpSocialAuth.setService(Constants.FACEBOOK);
                this.a.a(signUpSocialAuth);
            } else {
                this.a.a(600, (ud0) null, "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements FacebookCallback<LoginResult> {
        @DexIgnore
        public /* final */ /* synthetic */ in2 a;
        @DexIgnore
        public /* final */ /* synthetic */ ln2 b;

        @DexIgnore
        public c(in2 in2, ln2 ln2) {
            this.a = in2;
            this.b = ln2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginResult loginResult) {
            kd4.b(loginResult, "loginResult");
            AccessToken accessToken = loginResult.getAccessToken();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = in2.c.a();
            local.d(a2, "Step 1: Login using facebook success token=" + accessToken);
            in2 in2 = this.a;
            kd4.a((Object) accessToken, Constants.PROFILE_KEY_ACCESS_TOKEN);
            in2.a(accessToken, this.b);
        }

        @DexIgnore
        public void onCancel() {
            FLogger.INSTANCE.getLocal().e(in2.c.a(), "loginWithEmail facebook is cancel");
            this.b.a(2, (ud0) null, "");
        }

        @DexIgnore
        public void onError(FacebookException facebookException) {
            kd4.b(facebookException, "e");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = in2.c.a();
            local.e(a2, "loginWithEmail facebook fail " + facebookException.getMessage());
            this.b.a(600, (ud0) null, "");
        }
    }

    /*
    static {
        String canonicalName = in2.class.getCanonicalName();
        if (canonicalName != null) {
            kd4.a((Object) canonicalName, "MFLoginFacebookManager::class.java.canonicalName!!");
            b = canonicalName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public final void a(Activity activity, ln2 ln2) {
        kd4.b(activity, "activityContext");
        kd4.b(ln2, Constants.CALLBACK);
        this.a = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(activity, (Collection<String>) Arrays.asList(new String[]{"email", "user_photos", "public_profile"}));
        LoginManager instance = LoginManager.getInstance();
        CallbackManager callbackManager = this.a;
        if (callbackManager != null) {
            instance.registerCallback(callbackManager, new c(this, ln2));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(AccessToken accessToken, ln2 ln2) {
        kd4.b(accessToken, "token");
        kd4.b(ln2, Constants.CALLBACK);
        GraphRequest newMeRequest = GraphRequest.newMeRequest(accessToken, new b(ln2, accessToken));
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id, name, email, first_name, last_name");
        kd4.a((Object) newMeRequest, "request");
        newMeRequest.setParameters(bundle);
        newMeRequest.executeAsync();
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        kd4.b(intent, "data");
        CallbackManager callbackManager = this.a;
        if (callbackManager == null) {
            return false;
        }
        if (callbackManager != null) {
            boolean onActivityResult = callbackManager.onActivityResult(i, i2, intent);
            this.a = null;
            return onActivityResult;
        }
        kd4.a();
        throw null;
    }
}
