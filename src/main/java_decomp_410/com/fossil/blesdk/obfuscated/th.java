package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class th implements uh {
    @DexIgnore
    public /* final */ ViewGroupOverlay a;

    @DexIgnore
    public th(ViewGroup viewGroup) {
        this.a = viewGroup.getOverlay();
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }

    @DexIgnore
    public void a(View view) {
        this.a.add(view);
    }

    @DexIgnore
    public void b(View view) {
        this.a.remove(view);
    }
}
