package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;
import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import com.google.android.gms.internal.measurement.zzuv;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fb1 {
    @DexIgnore
    public static /* final */ fb1 f; // = new fb1(0, new int[0], new Object[0], false);
    @DexIgnore
    public int a;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public fb1() {
        this(0, new int[8], new Object[8], true);
    }

    @DexIgnore
    public static fb1 a(fb1 fb1, fb1 fb12) {
        int i = fb1.a + fb12.a;
        int[] copyOf = Arrays.copyOf(fb1.b, i);
        System.arraycopy(fb12.b, 0, copyOf, fb1.a, fb12.a);
        Object[] copyOf2 = Arrays.copyOf(fb1.c, i);
        System.arraycopy(fb12.c, 0, copyOf2, fb1.a, fb12.a);
        return new fb1(i, copyOf, copyOf2, true);
    }

    @DexIgnore
    public static fb1 d() {
        return f;
    }

    @DexIgnore
    public static fb1 e() {
        return new fb1();
    }

    @DexIgnore
    public final void b(sb1 sb1) throws IOException {
        if (this.a != 0) {
            if (sb1.a() == t81.e.k) {
                for (int i = 0; i < this.a; i++) {
                    a(this.b[i], this.c[i], sb1);
                }
                return;
            }
            for (int i2 = this.a - 1; i2 >= 0; i2--) {
                a(this.b[i2], this.c[i2], sb1);
            }
        }
    }

    @DexIgnore
    public final int c() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += zztv.d(this.b[i3] >>> 3, (zzte) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof fb1)) {
            return false;
        }
        fb1 fb1 = (fb1) obj;
        int i = this.a;
        if (i == fb1.a) {
            int[] iArr = this.b;
            int[] iArr2 = fb1.b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = fb1.c;
                int i3 = this.a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    @DexIgnore
    public fb1(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    @DexIgnore
    public final void a() {
        this.e = false;
    }

    @DexIgnore
    public final int b() {
        int i;
        int i2 = this.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.a; i4++) {
            int i5 = this.b[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = zztv.e(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 1) {
                i = zztv.g(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 2) {
                i = zztv.c(i6, (zzte) this.c[i4]);
            } else if (i7 == 3) {
                i = (zztv.e(i6) << 1) + ((fb1) this.c[i4]).b();
            } else if (i7 == 5) {
                i = zztv.i(i6, ((Integer) this.c[i4]).intValue());
            } else {
                throw new IllegalStateException(zzuv.zzwu());
            }
            i3 += i;
        }
        this.d = i3;
        return i3;
    }

    @DexIgnore
    public final void a(sb1 sb1) throws IOException {
        if (sb1.a() == t81.e.l) {
            for (int i = this.a - 1; i >= 0; i--) {
                sb1.zza(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            sb1.zza(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    @DexIgnore
    public static void a(int i, Object obj, sb1 sb1) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            sb1.b(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            sb1.zzc(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            sb1.a(i2, (zzte) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                sb1.c(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(zzuv.zzwu());
        } else if (sb1.a() == t81.e.k) {
            sb1.b(i2);
            ((fb1) obj).b(sb1);
            sb1.a(i2);
        } else {
            sb1.a(i2);
            ((fb1) obj).b(sb1);
            sb1.b(i2);
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            z91.a(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    @DexIgnore
    public final void a(int i, Object obj) {
        if (this.e) {
            int i2 = this.a;
            if (i2 == this.b.length) {
                int i3 = this.a + (i2 < 4 ? 8 : i2 >> 1);
                this.b = Arrays.copyOf(this.b, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr = this.b;
            int i4 = this.a;
            iArr[i4] = i;
            this.c[i4] = obj;
            this.a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
