package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sd */
public final class C2871sd<T> extends java.util.AbstractList<T> {

    @DexIgnore
    /* renamed from: n */
    public static /* final */ java.util.List f9248n; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: e */
    public int f9249e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.ArrayList<java.util.List<T>> f9250f;

    @DexIgnore
    /* renamed from: g */
    public int f9251g;

    @DexIgnore
    /* renamed from: h */
    public int f9252h;

    @DexIgnore
    /* renamed from: i */
    public int f9253i;

    @DexIgnore
    /* renamed from: j */
    public int f9254j;

    @DexIgnore
    /* renamed from: k */
    public int f9255k;

    @DexIgnore
    /* renamed from: l */
    public int f9256l;

    @DexIgnore
    /* renamed from: m */
    public int f9257m;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.sd$a */
    public interface C2872a {
        @DexIgnore
        /* renamed from: a */
        void mo12714a();

        @DexIgnore
        /* renamed from: a */
        void mo12715a(int i);

        @DexIgnore
        /* renamed from: a */
        void mo12716a(int i, int i2);

        @DexIgnore
        /* renamed from: a */
        void mo12717a(int i, int i2, int i3);

        @DexIgnore
        /* renamed from: b */
        void mo12719b();

        @DexIgnore
        /* renamed from: b */
        void mo12720b(int i, int i2);

        @DexIgnore
        /* renamed from: b */
        void mo12721b(int i, int i2, int i3);

        @DexIgnore
        /* renamed from: c */
        void mo12722c(int i, int i2);

        @DexIgnore
        /* renamed from: f */
        void mo12725f(int i);
    }

    @DexIgnore
    public C2871sd() {
        this.f9249e = 0;
        this.f9250f = new java.util.ArrayList<>();
        this.f9251g = 0;
        this.f9252h = 0;
        this.f9253i = 0;
        this.f9254j = 0;
        this.f9255k = 1;
        this.f9256l = 0;
        this.f9257m = 0;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15913a(int i, java.util.List<T> list, int i2, int i3) {
        this.f9249e = i;
        this.f9250f.clear();
        this.f9250f.add(list);
        this.f9251g = i2;
        this.f9252h = i3;
        this.f9253i = list.size();
        this.f9254j = this.f9253i;
        this.f9255k = list.size();
        this.f9256l = 0;
        this.f9257m = 0;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15921b() {
        int i = this.f9251g;
        for (int size = this.f9250f.size() - 1; size >= 0; size--) {
            java.util.List list = this.f9250f.get(size);
            if (list != null && list != f9248n) {
                break;
            }
            i += this.f9255k;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo15928c(int i, int i2) {
        return mo15918a(i, i2, this.f9250f.size() - 1);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo15930d(int i, int i2) {
        return mo15918a(i, i2, 0);
    }

    @DexIgnore
    /* renamed from: e */
    public int mo15931e() {
        return this.f9249e;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo15932f() {
        return this.f9249e + this.f9252h + (this.f9254j / 2);
    }

    @DexIgnore
    /* renamed from: g */
    public int mo15933g() {
        return this.f9257m;
    }

    @DexIgnore
    public T get(int i) {
        int i2;
        if (i < 0 || i >= size()) {
            throw new java.lang.IndexOutOfBoundsException("Index: " + i + ", Size: " + size());
        }
        int i3 = i - this.f9249e;
        if (i3 >= 0 && i3 < this.f9254j) {
            if (mo15940m()) {
                int i4 = this.f9255k;
                i2 = i3 / i4;
                i3 %= i4;
            } else {
                int size = this.f9250f.size();
                i2 = 0;
                while (i2 < size) {
                    int size2 = this.f9250f.get(i2).size();
                    if (size2 > i3) {
                        break;
                    }
                    i3 -= size2;
                    i2++;
                }
            }
            java.util.List list = this.f9250f.get(i2);
            if (!(list == null || list.size() == 0)) {
                return list.get(i3);
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: h */
    public int mo15935h() {
        return this.f9256l;
    }

    @DexIgnore
    /* renamed from: i */
    public int mo15936i() {
        return this.f9250f.size();
    }

    @DexIgnore
    /* renamed from: j */
    public int mo15937j() {
        return this.f9252h;
    }

    @DexIgnore
    /* renamed from: k */
    public int mo15938k() {
        return this.f9254j;
    }

    @DexIgnore
    /* renamed from: l */
    public int mo15939l() {
        return this.f9251g;
    }

    @DexIgnore
    /* renamed from: m */
    public boolean mo15940m() {
        return this.f9255k > 0;
    }

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C2871sd<T> mo15941n() {
        return new com.fossil.blesdk.obfuscated.C2871sd<>(this);
    }

    @DexIgnore
    public int size() {
        return this.f9249e + this.f9254j + this.f9251g;
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("leading " + this.f9249e + ", storage " + this.f9254j + ", trailing " + mo15939l());
        for (int i = 0; i < this.f9250f.size(); i++) {
            sb.append(" ");
            sb.append(this.f9250f.get(i));
        }
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: c */
    public T mo15927c() {
        return this.f9250f.get(0).get(0);
    }

    @DexIgnore
    /* renamed from: d */
    public T mo15929d() {
        java.util.ArrayList<java.util.List<T>> arrayList = this.f9250f;
        java.util.List list = arrayList.get(arrayList.size() - 1);
        return list.get(list.size() - 1);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo15925b(int i, int i2, int i3) {
        if (this.f9253i + i3 <= i || this.f9250f.size() <= 1 || this.f9253i < i2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo15926b(boolean z, int i, int i2, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int i3 = 0;
        while (mo15930d(i, i2)) {
            java.util.List remove = this.f9250f.remove(0);
            int size = remove == null ? this.f9255k : remove.size();
            i3 += size;
            this.f9254j -= size;
            this.f9253i -= remove == null ? 0 : remove.size();
        }
        if (i3 > 0) {
            if (z) {
                int i4 = this.f9249e;
                this.f9249e = i4 + i3;
                aVar.mo12716a(i4, i3);
            } else {
                this.f9252h += i3;
                aVar.mo12720b(this.f9249e, i3);
            }
        }
        if (i3 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public C2871sd(com.fossil.blesdk.obfuscated.C2871sd<T> sdVar) {
        this.f9249e = sdVar.f9249e;
        this.f9250f = new java.util.ArrayList<>(sdVar.f9250f);
        this.f9251g = sdVar.f9251g;
        this.f9252h = sdVar.f9252h;
        this.f9253i = sdVar.f9253i;
        this.f9254j = sdVar.f9254j;
        this.f9255k = sdVar.f9255k;
        this.f9256l = sdVar.f9256l;
        this.f9257m = sdVar.f9257m;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15915a(int i, java.util.List<T> list, int i2, int i3, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        mo15913a(i, list, i2, i3);
        aVar.mo12715a(size());
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15910a() {
        int i = this.f9249e;
        int size = this.f9250f.size();
        for (int i2 = 0; i2 < size; i2++) {
            java.util.List list = this.f9250f.get(i2);
            if (list != null && list != f9248n) {
                break;
            }
            i += this.f9255k;
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo15918a(int i, int i2, int i3) {
        java.util.List list = this.f9250f.get(i3);
        return list == null || (this.f9253i > i && this.f9250f.size() > 2 && list != f9248n && this.f9253i - list.size() >= i2);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15923b(java.util.List<T> list, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int size = list.size();
        if (size == 0) {
            aVar.mo12719b();
            return;
        }
        int i = this.f9255k;
        if (i > 0 && size != i) {
            if (this.f9250f.size() != 1 || size <= this.f9255k) {
                this.f9255k = -1;
            } else {
                this.f9255k = size;
            }
        }
        this.f9250f.add(0, list);
        this.f9253i += size;
        this.f9254j += size;
        int min = java.lang.Math.min(this.f9249e, size);
        int i2 = size - min;
        if (min != 0) {
            this.f9249e -= min;
        }
        this.f9252h -= i2;
        this.f9256l += size;
        aVar.mo12717a(this.f9249e, min, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo15920a(boolean z, int i, int i2, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int i3 = 0;
        while (mo15928c(i, i2)) {
            java.util.ArrayList<java.util.List<T>> arrayList = this.f9250f;
            java.util.List remove = arrayList.remove(arrayList.size() - 1);
            int size = remove == null ? this.f9255k : remove.size();
            i3 += size;
            this.f9254j -= size;
            this.f9253i -= remove == null ? 0 : remove.size();
        }
        if (i3 > 0) {
            int i4 = this.f9249e + this.f9254j;
            if (z) {
                this.f9251g += i3;
                aVar.mo12716a(i4, i3);
            } else {
                aVar.mo12720b(i4, i3);
            }
        }
        if (i3 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15917a(java.util.List<T> list, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        java.util.ArrayList<java.util.List<T>> arrayList;
        int size = list.size();
        if (size == 0) {
            aVar.mo12714a();
            return;
        }
        if (this.f9255k > 0) {
            int size2 = arrayList.get(this.f9250f.size() - 1).size();
            int i = this.f9255k;
            if (size2 != i || size > i) {
                this.f9255k = -1;
            }
        }
        this.f9250f.add(list);
        this.f9253i += size;
        this.f9254j += size;
        int min = java.lang.Math.min(this.f9251g, size);
        int i2 = size - min;
        if (min != 0) {
            this.f9251g -= min;
        }
        this.f9257m += size;
        aVar.mo12721b((this.f9249e + this.f9254j) - size, min, i2);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15922b(int i, java.util.List<T> list, int i2, int i3, int i4, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        boolean z = i3 != Integer.MAX_VALUE;
        boolean z2 = i2 > mo15932f();
        if (!z || !mo15925b(i3, i4, list.size()) || !mo15919a(i, z2)) {
            mo15916a(i, list, aVar);
        } else {
            this.f9250f.set((i - this.f9249e) / this.f9255k, (java.lang.Object) null);
            this.f9254j -= list.size();
            if (z2) {
                this.f9250f.remove(0);
                this.f9249e += list.size();
            } else {
                java.util.ArrayList<java.util.List<T>> arrayList = this.f9250f;
                arrayList.remove(arrayList.size() - 1);
                this.f9251g += list.size();
            }
        }
        if (!z) {
            return;
        }
        if (z2) {
            mo15926b(true, i3, i4, aVar);
        } else {
            mo15920a(true, i3, i4, aVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo15919a(int i, boolean z) {
        if (this.f9255k < 1 || this.f9250f.size() < 2) {
            throw new java.lang.IllegalStateException("Trimming attempt before sufficient load");
        }
        int i2 = this.f9249e;
        if (i < i2) {
            return z;
        }
        if (i >= this.f9254j + i2) {
            return !z;
        }
        int i3 = (i - i2) / this.f9255k;
        if (z) {
            for (int i4 = 0; i4 < i3; i4++) {
                if (this.f9250f.get(i4) != null) {
                    return false;
                }
            }
        } else {
            for (int size = this.f9250f.size() - 1; size > i3; size--) {
                if (this.f9250f.get(size) != null) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo15924b(int i, int i2) {
        int i3 = this.f9249e / i;
        if (i2 < i3 || i2 >= this.f9250f.size() + i3) {
            return false;
        }
        java.util.List list = this.f9250f.get(i2 - i3);
        if (list == null || list == f9248n) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15914a(int i, java.util.List<T> list, int i2, int i3, int i4, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int size = (list.size() + (i4 - 1)) / i4;
        int i5 = 0;
        while (i5 < size) {
            int i6 = i5 * i4;
            int i7 = i5 + 1;
            java.util.List<T> subList = list.subList(i6, java.lang.Math.min(list.size(), i7 * i4));
            if (i5 == 0) {
                mo15913a(i, subList, (list.size() + i2) - subList.size(), i3);
            } else {
                mo15916a(i6 + i, subList, (com.fossil.blesdk.obfuscated.C2871sd.C2872a) null);
            }
            i5 = i7;
        }
        aVar.mo12715a(size());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15916a(int i, java.util.List<T> list, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int size = list.size();
        if (size != this.f9255k) {
            int size2 = size();
            int i2 = this.f9255k;
            boolean z = false;
            boolean z2 = i == size2 - (size2 % i2) && size < i2;
            if (this.f9251g == 0 && this.f9250f.size() == 1 && size > this.f9255k) {
                z = true;
            }
            if (!z && !z2) {
                throw new java.lang.IllegalArgumentException("page introduces incorrect tiling");
            } else if (z) {
                this.f9255k = size;
            }
        }
        int i3 = i / this.f9255k;
        mo15911a(i3, i3);
        int i4 = i3 - (this.f9249e / this.f9255k);
        java.util.List list2 = this.f9250f.get(i4);
        if (list2 == null || list2 == f9248n) {
            this.f9250f.set(i4, list);
            this.f9253i += size;
            if (aVar != null) {
                aVar.mo12722c(i, size);
                return;
            }
            return;
        }
        throw new java.lang.IllegalArgumentException("Invalid position " + i + ": data already loaded");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15911a(int i, int i2) {
        int i3;
        int i4 = this.f9249e / this.f9255k;
        if (i < i4) {
            int i5 = 0;
            while (true) {
                i3 = i4 - i;
                if (i5 >= i3) {
                    break;
                }
                this.f9250f.add(0, (java.lang.Object) null);
                i5++;
            }
            int i6 = i3 * this.f9255k;
            this.f9254j += i6;
            this.f9249e -= i6;
        } else {
            i = i4;
        }
        if (i2 >= this.f9250f.size() + i) {
            int min = java.lang.Math.min(this.f9251g, ((i2 + 1) - (this.f9250f.size() + i)) * this.f9255k);
            for (int size = this.f9250f.size(); size <= i2 - i; size++) {
                java.util.ArrayList<java.util.List<T>> arrayList = this.f9250f;
                arrayList.add(arrayList.size(), (java.lang.Object) null);
            }
            this.f9254j += min;
            this.f9251g -= min;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15912a(int i, int i2, int i3, com.fossil.blesdk.obfuscated.C2871sd.C2872a aVar) {
        int i4 = this.f9255k;
        if (i3 != i4) {
            if (i3 < i4) {
                throw new java.lang.IllegalArgumentException("Page size cannot be reduced");
            } else if (this.f9250f.size() == 1 && this.f9251g == 0) {
                this.f9255k = i3;
            } else {
                throw new java.lang.IllegalArgumentException("Page size can change only if last page is only one present");
            }
        }
        int size = size();
        int i5 = this.f9255k;
        int i6 = ((size + i5) - 1) / i5;
        int max = java.lang.Math.max((i - i2) / i5, 0);
        int min = java.lang.Math.min((i + i2) / this.f9255k, i6 - 1);
        mo15911a(max, min);
        int i7 = this.f9249e / this.f9255k;
        while (max <= min) {
            int i8 = max - i7;
            if (this.f9250f.get(i8) == null) {
                this.f9250f.set(i8, f9248n);
                aVar.mo12725f(max);
            }
            max++;
        }
    }
}
