package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class at3 implements Interceptor {
    @DexIgnore
    public static /* final */ Charset b; // = Charset.forName("UTF-8");
    @DexIgnore
    public volatile HttpLoggingInterceptor.Level a; // = HttpLoggingInterceptor.Level.NONE;

    @DexIgnore
    public at3 a(HttpLoggingInterceptor.Level level) {
        if (level != null) {
            this.a = level;
            return this;
        }
        throw new NullPointerException("level == null. Use Level.EMPTY instead.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d6  */
    public Response intercept(Interceptor.Chain chain) throws IOException {
        long j;
        long j2;
        String str;
        String str2;
        String str3;
        long j3;
        String str4;
        String str5;
        String str6;
        long j4;
        String str7;
        String str8;
        int b2;
        int i;
        Interceptor.Chain chain2 = chain;
        HttpLoggingInterceptor.Level level = this.a;
        dm4 n = chain.n();
        if (level == HttpLoggingInterceptor.Level.NONE) {
            return chain2.a(n);
        }
        boolean z = true;
        boolean z2 = level == HttpLoggingInterceptor.Level.BODY;
        boolean z3 = z2 || level == HttpLoggingInterceptor.Level.HEADERS;
        RequestBody a2 = n.a();
        if (a2 == null) {
            z = false;
        }
        nl4 c = chain.c();
        Protocol a3 = c != null ? c.a() : Protocol.HTTP_1_1;
        long nanoTime = System.nanoTime();
        StringBuilder sb = new StringBuilder();
        sb.append("--> Id: ");
        sb.append(nanoTime);
        sb.append("\n");
        sb.append("--> ");
        sb.append(n.e());
        sb.append(' ');
        sb.append(n.g());
        sb.append(' ');
        sb.append(a3);
        if (z3 || !z) {
            j = nanoTime;
        } else {
            sb.append(" (");
            j = nanoTime;
            sb.append(a2.a());
            sb.append("-byte body)");
        }
        sb.append("\n");
        if (z3) {
            if (z) {
                if (a2.b() != null) {
                    sb.append("--> Content-Type: ");
                    sb.append(a2.b());
                    sb.append("\n");
                }
                if (a2.a() != -1) {
                    sb.append("--> Content-Length: ");
                    str8 = "-byte body)";
                    sb.append(a2.a());
                    sb.append("\n");
                    yl4 c2 = n.c();
                    xz1 xz1 = new xz1();
                    b2 = c2.b();
                    str = "empty";
                    i = 0;
                    while (i < b2) {
                        int i2 = b2;
                        String a4 = c2.a(i);
                        long j5 = j;
                        if (!GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(a4) && !"Content-Length".equalsIgnoreCase(a4)) {
                            xz1.a(a4, c2.b(i));
                        }
                        i++;
                        b2 = i2;
                        j = j5;
                    }
                    j2 = j;
                    sb.append("--> Headers: ");
                    sb.append(xz1);
                    sb.append("\n");
                    if (z2 || !z) {
                        str2 = str8;
                        sb.append("--> END ");
                        sb.append(n.e());
                        sb.append("\n");
                    } else if (a(n.c())) {
                        sb.append("--> END ");
                        sb.append(n.e());
                        sb.append(" (encoded body omitted)");
                        sb.append("\n");
                        str2 = str8;
                    } else {
                        jo4 jo4 = new jo4();
                        a2.a(jo4);
                        Charset charset = b;
                        am4 b3 = a2.b();
                        if (b3 != null) {
                            charset = b3.a(b);
                        }
                        if (a(jo4)) {
                            sb.append("--> Body: ");
                            sb.append(charset != null ? jo4.a(charset) : str);
                            sb.append("\n");
                            sb.append("--> END ");
                            sb.append(n.e());
                            sb.append(" (");
                            sb.append(a2.a());
                            str2 = str8;
                            sb.append(str2);
                            sb.append("\n");
                        } else {
                            str2 = str8;
                            sb.append("--> END ");
                            sb.append(n.e());
                            sb.append("(binary ");
                            sb.append(a2.a());
                            sb.append("-byte body omitted)");
                            sb.append("\n");
                        }
                    }
                }
            }
            str8 = "-byte body)";
            yl4 c22 = n.c();
            xz1 xz12 = new xz1();
            b2 = c22.b();
            str = "empty";
            i = 0;
            while (i < b2) {
            }
            j2 = j;
            sb.append("--> Headers: ");
            sb.append(xz12);
            sb.append("\n");
            if (z2) {
            }
            str2 = str8;
            sb.append("--> END ");
            sb.append(n.e());
            sb.append("\n");
        } else {
            str = "empty";
            str2 = "-byte body)";
            j2 = j;
        }
        FLogger.INSTANCE.getLocal().d("OkHttp", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<-- Id: ");
        sb2.append(j2);
        sb2.append("\n");
        long nanoTime2 = System.nanoTime();
        try {
            Response a5 = chain2.a(n);
            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime2);
            em4 y = a5.y();
            if (y != null) {
                str3 = "OkHttp";
                str4 = "-byte body omitted)";
                j3 = y.C();
            } else {
                str3 = "OkHttp";
                str4 = "-byte body omitted)";
                j3 = -1;
            }
            if (j3 == -1) {
                str6 = "unknown-length";
                str5 = str2;
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(j3);
                str5 = str2;
                sb3.append("-byte");
                str6 = sb3.toString();
            }
            if (z3) {
                str7 = "";
                j4 = j3;
            } else {
                StringBuilder sb4 = new StringBuilder();
                j4 = j3;
                sb4.append(", ");
                sb4.append(str6);
                sb4.append(" body");
                str7 = sb4.toString();
            }
            sb2.append("<-- Code ");
            sb2.append(a5.B());
            sb2.append(' ');
            sb2.append(a5.F());
            sb2.append(' ');
            sb2.append(a5.L().g());
            sb2.append(" (");
            sb2.append(millis);
            sb2.append("ms");
            sb2.append(str7);
            sb2.append(')');
            sb2.append("\n");
            if (z3) {
                yl4 D = a5.D();
                xz1 xz13 = new xz1();
                int b4 = D.b();
                for (int i3 = 0; i3 < b4; i3++) {
                    xz13.a(D.a(i3), D.b(i3));
                }
                sb2.append("<-- Headers: ");
                sb2.append(xz13);
                sb2.append("\n");
                if (!z2 || !bn4.b(a5)) {
                    sb2.append("<-- END HTTP");
                    sb2.append("\n");
                } else if (a(a5.D())) {
                    sb2.append("<-- END HTTP (encoded body omitted)");
                    sb2.append("\n");
                } else if (y != null) {
                    lo4 E = y.E();
                    E.c(ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
                    jo4 a6 = E.a();
                    Charset charset2 = b;
                    am4 D2 = y.D();
                    if (D2 != null) {
                        charset2 = D2.a(b);
                    }
                    if (!a(a6)) {
                        sb2.append("<-- END HTTP (binary ");
                        sb2.append(a6.B());
                        sb2.append(str4);
                        sb2.append("\n");
                        FLogger.INSTANCE.getLocal().d(str3, sb2.toString());
                        return a5;
                    }
                    if (j4 != 0) {
                        sb2.append("<-- Body: ");
                        sb2.append(charset2 != null ? a6.clone().a(charset2) : str);
                        sb2.append("\n");
                    }
                    sb2.append("<-- END HTTP (");
                    sb2.append(a6.B());
                    sb2.append(str5);
                    sb2.append("\n");
                } else {
                    sb2.append("<-- END HTTP (");
                    sb2.append("-1");
                    sb2.append(str5);
                    sb2.append("\n");
                }
            }
            FLogger.INSTANCE.getLocal().d(str3, sb2.toString());
            return a5;
        } catch (Exception e) {
            Exception exc = e;
            sb2.append("<-- HTTP FAILED: ");
            sb2.append(exc);
            sb2.append("\n");
            FLogger.INSTANCE.getLocal().d("OkHttp", sb2.toString());
            throw exc;
        }
    }

    @DexIgnore
    public static boolean a(jo4 jo4) {
        try {
            jo4 jo42 = new jo4();
            jo4.a(jo42, 0, jo4.B() < 64 ? jo4.B() : 64);
            for (int i = 0; i < 16; i++) {
                if (jo42.g()) {
                    return true;
                }
                int A = jo42.A();
                if (Character.isISOControl(A) && !Character.isWhitespace(A)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean a(yl4 yl4) {
        String a2 = yl4.a(GraphRequest.CONTENT_ENCODING_HEADER);
        return a2 != null && !a2.equalsIgnoreCase("identity");
    }
}
