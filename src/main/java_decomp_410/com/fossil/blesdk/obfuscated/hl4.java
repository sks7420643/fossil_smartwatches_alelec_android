package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hl4 {
    @DexIgnore
    public static final hl4 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements hl4 {
        @DexIgnore
        public dm4 authenticate(fm4 fm4, Response response) {
            return null;
        }
    }

    @DexIgnore
    dm4 authenticate(fm4 fm4, Response response) throws IOException;
}
