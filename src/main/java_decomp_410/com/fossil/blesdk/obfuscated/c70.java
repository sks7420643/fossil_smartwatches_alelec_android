package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c70 extends g70 {
    @DexIgnore
    public int A; // = 23;
    @DexIgnore
    public /* final */ int B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c70(int i, Peripheral peripheral) {
        super(RequestId.REQUEST_MTU, peripheral);
        kd4.b(peripheral, "peripheral");
        this.B = i;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new l10(this.B, i().h());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.A = ((l10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(new JSONObject(), JSONKey.EXCHANGED_MTU, Integer.valueOf(this.A)), 7, (fd4) null));
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.REQUESTED_MTU, Integer.valueOf(this.B));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.EXCHANGED_MTU, Integer.valueOf(this.A));
    }
}
