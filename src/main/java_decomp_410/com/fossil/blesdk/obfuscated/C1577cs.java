package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cs */
public class C1577cs<Data> implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.Set<java.lang.String> f4189b; // = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"http", com.facebook.internal.Utility.URL_SCHEME})));

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, Data> f4190a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cs$a */
    public static class C1578a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1577cs(wrVar.mo17509a(com.fossil.blesdk.obfuscated.C2340lr.class, java.io.InputStream.class));
        }
    }

    @DexIgnore
    public C1577cs(com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, Data> srVar) {
        this.f4190a = srVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f4190a.mo8911a(new com.fossil.blesdk.obfuscated.C2340lr(uri.toString()), i, i2, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return f4189b.contains(uri.getScheme());
    }
}
