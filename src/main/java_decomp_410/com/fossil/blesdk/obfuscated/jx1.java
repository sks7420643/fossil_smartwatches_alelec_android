package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class jx1 implements Handler.Callback {
    @DexIgnore
    public /* final */ ix1 a;

    @DexIgnore
    public jx1(ix1 ix1) {
        this.a = ix1;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.a.a(message);
    }
}
