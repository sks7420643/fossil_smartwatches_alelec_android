package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class td {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends le.b {
        @DexIgnore
        public /* final */ /* synthetic */ sd a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ sd c;
        @DexIgnore
        public /* final */ /* synthetic */ le.d d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public a(sd sdVar, int i, sd sdVar2, le.d dVar, int i2, int i3) {
            this.a = sdVar;
            this.b = i;
            this.c = sdVar2;
            this.d = dVar;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        public int a() {
            return this.f;
        }

        @DexIgnore
        public int b() {
            return this.e;
        }

        @DexIgnore
        public Object c(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sd sdVar = this.c;
            Object obj2 = sdVar.get(i2 + sdVar.e());
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        public boolean a(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sd sdVar = this.c;
            Object obj2 = sdVar.get(i2 + sdVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        public boolean b(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sd sdVar = this.c;
            Object obj2 = sdVar.get(i2 + sdVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ue {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ue b;

        @DexIgnore
        public b(int i, ue ueVar) {
            this.a = i;
            this.b = ueVar;
        }

        @DexIgnore
        public void a(int i, int i2) {
            ue ueVar = this.b;
            int i3 = this.a;
            ueVar.a(i + i3, i2 + i3);
        }

        @DexIgnore
        public void b(int i, int i2) {
            this.b.b(i + this.a, i2);
        }

        @DexIgnore
        public void c(int i, int i2) {
            this.b.c(i + this.a, i2);
        }

        @DexIgnore
        public void a(int i, int i2, Object obj) {
            this.b.a(i + this.a, i2, obj);
        }
    }

    @DexIgnore
    public static <T> le.c a(sd<T> sdVar, sd<T> sdVar2, le.d<T> dVar) {
        int a2 = sdVar.a();
        int a3 = sdVar2.a();
        return le.a(new a(sdVar, a2, sdVar2, dVar, (sdVar.size() - a2) - sdVar.b(), (sdVar2.size() - a3) - sdVar2.b()), true);
    }

    @DexIgnore
    public static <T> void a(ue ueVar, sd<T> sdVar, sd<T> sdVar2, le.c cVar) {
        int b2 = sdVar.b();
        int b3 = sdVar2.b();
        int a2 = sdVar.a();
        int a3 = sdVar2.a();
        if (b2 == 0 && b3 == 0 && a2 == 0 && a3 == 0) {
            cVar.a(ueVar);
            return;
        }
        if (b2 > b3) {
            int i = b2 - b3;
            ueVar.c(sdVar.size() - i, i);
        } else if (b2 < b3) {
            ueVar.b(sdVar.size(), b3 - b2);
        }
        if (a2 > a3) {
            ueVar.c(0, a2 - a3);
        } else if (a2 < a3) {
            ueVar.b(0, a3 - a2);
        }
        if (a3 != 0) {
            cVar.a((ue) new b(a3, ueVar));
        } else {
            cVar.a(ueVar);
        }
    }

    @DexIgnore
    public static int a(le.c cVar, sd sdVar, sd sdVar2, int i) {
        int a2 = sdVar.a();
        int i2 = i - a2;
        int size = (sdVar.size() - a2) - sdVar.b();
        if (i2 >= 0 && i2 < size) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 / 2) * (i3 % 2 == 1 ? -1 : 1)) + i2;
                if (i4 >= 0 && i4 < sdVar.k()) {
                    int a3 = cVar.a(i4);
                    if (a3 != -1) {
                        return a3 + sdVar2.e();
                    }
                }
            }
        }
        return Math.max(0, Math.min(i, sdVar2.size() - 1));
    }
}
