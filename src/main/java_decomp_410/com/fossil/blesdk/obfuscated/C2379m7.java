package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m7 */
public final class C2379m7 {

    @DexIgnore
    /* renamed from: a */
    public boolean f7403a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2379m7.C2380a f7404b;

    @DexIgnore
    /* renamed from: c */
    public java.lang.Object f7405c;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.m7$a */
    public interface C2380a {
        @DexIgnore
        void onCancel();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r0.onCancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r1 == null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        if (android.os.Build.VERSION.SDK_INT < 16) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        ((android.os.CancellationSignal) r1).cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x002e, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0032, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0033, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        if (r0 == null) goto L_0x0017;
     */
    @DexIgnore
    /* renamed from: a */
    public void mo13523a() {
        synchronized (this) {
            if (!this.f7403a) {
                this.f7403a = true;
                com.fossil.blesdk.obfuscated.C2379m7.C2380a aVar = this.f7404b;
                java.lang.Object obj = this.f7405c;
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object mo13524b() {
        java.lang.Object obj;
        if (android.os.Build.VERSION.SDK_INT < 16) {
            return null;
        }
        synchronized (this) {
            if (this.f7405c == null) {
                this.f7405c = new android.os.CancellationSignal();
                if (this.f7403a) {
                    ((android.os.CancellationSignal) this.f7405c).cancel();
                }
            }
            obj = this.f7405c;
        }
        return obj;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo13525c() {
        boolean z;
        synchronized (this) {
            z = this.f7403a;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo13526d() {
        if (mo13525c()) {
            throw new androidx.core.p001os.OperationCanceledException();
        }
    }
}
