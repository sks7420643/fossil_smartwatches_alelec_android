package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ms3 {
    @DexIgnore
    public static ms3 a;

    @DexIgnore
    public static ms3 a() {
        if (a == null) {
            a = new ms3();
        }
        return a;
    }

    @DexIgnore
    public List<ContactGroup> b(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingSms = dn2.p.a().b().getContactGroupsMatchingSms(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingSms == null ? new ArrayList() : contactGroupsMatchingSms;
    }

    @DexIgnore
    public List<ContactGroup> a(String str, MFDeviceFamily mFDeviceFamily) {
        ArrayList arrayList = new ArrayList();
        if (str == null || mFDeviceFamily == null) {
            return arrayList;
        }
        List<ContactGroup> contactGroupsMatchingIncomingCall = dn2.p.a().b().getContactGroupsMatchingIncomingCall(str, mFDeviceFamily.ordinal());
        return contactGroupsMatchingIncomingCall == null ? new ArrayList() : contactGroupsMatchingIncomingCall;
    }

    @DexIgnore
    public Contact a(String str, int i) {
        Contact next;
        List<ContactGroup> allContactGroups = dn2.p.a().b().getAllContactGroups(i);
        if (allContactGroups == null) {
            return null;
        }
        loop0:
        for (ContactGroup contacts : allContactGroups) {
            Iterator<Contact> it = contacts.getContacts().iterator();
            while (true) {
                if (it.hasNext()) {
                    next = it.next();
                    if (!TextUtils.equals(str, next.getFirstName())) {
                        if (next.containsPhoneNumber(str)) {
                            break loop0;
                        }
                    } else {
                        break loop0;
                    }
                }
            }
            return next;
        }
        return null;
    }
}
