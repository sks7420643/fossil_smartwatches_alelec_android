package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wz3 {
    @DexIgnore
    boolean a();

    @DexIgnore
    boolean a(Intent intent, xz3 xz3);

    @DexIgnore
    boolean a(hz3 hz3);

    @DexIgnore
    boolean a(String str);
}
