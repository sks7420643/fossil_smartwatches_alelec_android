package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o63 {
    @DexIgnore
    public /* final */ l63 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public o63(l63 l63, String str, String str2) {
        kd4.b(l63, "mView");
        kd4.b(str, "mPresetId");
        kd4.b(str2, "mMicroAppPos");
        this.a = l63;
    }

    @DexIgnore
    public final l63 a() {
        return this.a;
    }
}
