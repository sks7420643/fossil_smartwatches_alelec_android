package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qh */
public class C2742qh {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<android.view.View, com.fossil.blesdk.obfuscated.C2654ph> f8671a; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.util.SparseArray<android.view.View> f8672b; // = new android.util.SparseArray<>();

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2103j4<android.view.View> f8673c; // = new com.fossil.blesdk.obfuscated.C2103j4<>();

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, android.view.View> f8674d; // = new com.fossil.blesdk.obfuscated.C1855g4<>();
}
