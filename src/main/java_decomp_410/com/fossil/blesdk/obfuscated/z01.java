package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z01 extends nz0 implements x01 {
    @DexIgnore
    public z01(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
    }

    @DexIgnore
    public final void a(fq0 fq0) throws RemoteException {
        Parcel o = o();
        y01.a(o, (Parcelable) fq0);
        a(2, o);
    }
}
