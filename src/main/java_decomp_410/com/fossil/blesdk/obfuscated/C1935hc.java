package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hc */
public class C1935hc {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hc$a */
    public static class C1936a implements com.fossil.blesdk.obfuscated.C1548cc<X> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1411ac f5715a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2374m3 f5716b;

        @DexIgnore
        public C1936a(com.fossil.blesdk.obfuscated.C1411ac acVar, com.fossil.blesdk.obfuscated.C2374m3 m3Var) {
            this.f5715a = acVar;
            this.f5716b = m3Var;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8689a(X x) {
            this.f5715a.mo2284b(this.f5716b.apply(x));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.hc$b */
    public static class C1937b implements com.fossil.blesdk.obfuscated.C1548cc<X> {

        @DexIgnore
        /* renamed from: a */
        public androidx.lifecycle.LiveData<Y> f5717a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2374m3 f5718b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1411ac f5719c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hc$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.hc$b$a */
        public class C1938a implements com.fossil.blesdk.obfuscated.C1548cc<Y> {
            @DexIgnore
            public C1938a() {
            }

            @DexIgnore
            /* renamed from: a */
            public void mo8689a(Y y) {
                com.fossil.blesdk.obfuscated.C1935hc.C1937b.this.f5719c.mo2284b(y);
            }
        }

        @DexIgnore
        public C1937b(com.fossil.blesdk.obfuscated.C2374m3 m3Var, com.fossil.blesdk.obfuscated.C1411ac acVar) {
            this.f5718b = m3Var;
            this.f5719c = acVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8689a(X x) {
            androidx.lifecycle.LiveData<Y> liveData = (androidx.lifecycle.LiveData) this.f5718b.apply(x);
            androidx.lifecycle.LiveData<Y> liveData2 = this.f5717a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.f5719c.mo8686a(liveData2);
                }
                this.f5717a = liveData;
                androidx.lifecycle.LiveData<Y> liveData3 = this.f5717a;
                if (liveData3 != null) {
                    this.f5719c.mo8687a(liveData3, new com.fossil.blesdk.obfuscated.C1935hc.C1937b.C1938a());
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <X, Y> androidx.lifecycle.LiveData<Y> m7843a(androidx.lifecycle.LiveData<X> liveData, com.fossil.blesdk.obfuscated.C2374m3<X, Y> m3Var) {
        com.fossil.blesdk.obfuscated.C1411ac acVar = new com.fossil.blesdk.obfuscated.C1411ac();
        acVar.mo8687a(liveData, new com.fossil.blesdk.obfuscated.C1935hc.C1936a(acVar, m3Var));
        return acVar;
    }

    @DexIgnore
    /* renamed from: b */
    public static <X, Y> androidx.lifecycle.LiveData<Y> m7844b(androidx.lifecycle.LiveData<X> liveData, com.fossil.blesdk.obfuscated.C2374m3<X, androidx.lifecycle.LiveData<Y>> m3Var) {
        com.fossil.blesdk.obfuscated.C1411ac acVar = new com.fossil.blesdk.obfuscated.C1411ac();
        acVar.mo8687a(liveData, new com.fossil.blesdk.obfuscated.C1935hc.C1937b(m3Var, acVar));
        return acVar;
    }
}
