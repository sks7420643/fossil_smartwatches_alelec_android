package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ow {
    @DexIgnore
    public static /* final */ Executor a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public /* final */ Handler e; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            this.e.post(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            runnable.run();
        }
    }

    @DexIgnore
    public static Executor a() {
        return b;
    }

    @DexIgnore
    public static Executor b() {
        return a;
    }
}
