package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.blesdk.obfuscated.m8;
import com.fossil.blesdk.obfuscated.p1;
import com.fossil.blesdk.obfuscated.q1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z1 extends b1 implements m8.a {
    @DexIgnore
    public /* final */ SparseBooleanArray A; // = new SparseBooleanArray();
    @DexIgnore
    public View B;
    @DexIgnore
    public e C;
    @DexIgnore
    public a D;
    @DexIgnore
    public c E;
    @DexIgnore
    public b F;
    @DexIgnore
    public /* final */ f G; // = new f();
    @DexIgnore
    public int H;
    @DexIgnore
    public d n;
    @DexIgnore
    public Drawable o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends o1 {
        @DexIgnore
        public a(Context context, v1 v1Var, View view) {
            super(context, v1Var, view, false, r.actionOverflowMenuStyle);
            if (!((k1) v1Var.getItem()).h()) {
                View view2 = z1.this.n;
                a(view2 == null ? (View) z1.this.l : view2);
            }
            a((p1.a) z1.this.G);
        }

        @DexIgnore
        public void e() {
            z1 z1Var = z1.this;
            z1Var.D = null;
            z1Var.H = 0;
            super.e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ActionMenuItemView.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public t1 a() {
            a aVar = z1.this.D;
            if (aVar != null) {
                return aVar.c();
            }
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public e e;

        @DexIgnore
        public c(e eVar) {
            this.e = eVar;
        }

        @DexIgnore
        public void run() {
            if (z1.this.g != null) {
                z1.this.g.a();
            }
            View view = (View) z1.this.l;
            if (!(view == null || view.getWindowToken() == null || !this.e.g())) {
                z1.this.C = this.e;
            }
            z1.this.E = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AppCompatImageView implements ActionMenuView.a {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends n2 {
            @DexIgnore
            public a(View view, z1 z1Var) {
                super(view);
            }

            @DexIgnore
            public t1 b() {
                e eVar = z1.this.C;
                if (eVar == null) {
                    return null;
                }
                return eVar.c();
            }

            @DexIgnore
            public boolean c() {
                z1.this.j();
                return true;
            }

            @DexIgnore
            public boolean d() {
                z1 z1Var = z1.this;
                if (z1Var.E != null) {
                    return false;
                }
                z1Var.f();
                return true;
            }
        }

        @DexIgnore
        public d(Context context) {
            super(context, (AttributeSet) null, r.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            b3.a(this, getContentDescription());
            setOnTouchListener(new a(this, z1.this));
        }

        @DexIgnore
        public boolean b() {
            return false;
        }

        @DexIgnore
        public boolean c() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            z1.this.j();
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                c7.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends o1 {
        @DexIgnore
        public e(Context context, h1 h1Var, View view, boolean z) {
            super(context, h1Var, view, z, r.actionOverflowMenuStyle);
            a(8388613);
            a((p1.a) z1.this.G);
        }

        @DexIgnore
        public void e() {
            if (z1.this.g != null) {
                z1.this.g.close();
            }
            z1.this.C = null;
            super.e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<g> CREATOR; // = new a();
        @DexIgnore
        public int e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements Parcelable.Creator<g> {
            @DexIgnore
            public g createFromParcel(Parcel parcel) {
                return new g(parcel);
            }

            @DexIgnore
            public g[] newArray(int i) {
                return new g[i];
            }
        }

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.e);
        }

        @DexIgnore
        public g(Parcel parcel) {
            this.e = parcel.readInt();
        }
    }

    @DexIgnore
    public z1(Context context) {
        super(context, x.abc_action_menu_layout, x.abc_action_menu_item_layout);
    }

    @DexIgnore
    public boolean g() {
        a aVar = this.D;
        if (aVar == null) {
            return false;
        }
        aVar.b();
        return true;
    }

    @DexIgnore
    public boolean h() {
        return this.E != null || i();
    }

    @DexIgnore
    public boolean i() {
        e eVar = this.C;
        return eVar != null && eVar.d();
    }

    @DexIgnore
    public boolean j() {
        if (!this.q || i()) {
            return false;
        }
        h1 h1Var = this.g;
        if (h1Var == null || this.l == null || this.E != null || h1Var.j().isEmpty()) {
            return false;
        }
        this.E = new c(new e(this.f, this.g, this.n, true));
        ((View) this.l).post(this.E);
        super.a((v1) null);
        return true;
    }

    @DexIgnore
    public void a(Context context, h1 h1Var) {
        super.a(context, h1Var);
        Resources resources = context.getResources();
        s0 a2 = s0.a(context);
        if (!this.r) {
            this.q = a2.g();
        }
        if (!this.x) {
            this.s = a2.b();
        }
        if (!this.v) {
            this.u = a2.c();
        }
        int i = this.s;
        if (this.q) {
            if (this.n == null) {
                this.n = new d(this.e);
                if (this.p) {
                    this.n.setImageDrawable(this.o);
                    this.o = null;
                    this.p = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.n.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.n.getMeasuredWidth();
        } else {
            this.n = null;
        }
        this.t = i;
        this.z = (int) (resources.getDisplayMetrics().density * 56.0f);
        this.B = null;
    }

    @DexIgnore
    public q1 b(ViewGroup viewGroup) {
        q1 q1Var = this.l;
        q1 b2 = super.b(viewGroup);
        if (q1Var != b2) {
            ((ActionMenuView) b2).setPresenter(this);
        }
        return b2;
    }

    @DexIgnore
    public void c(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void d(boolean z2) {
        this.q = z2;
        this.r = true;
    }

    @DexIgnore
    public Drawable e() {
        d dVar = this.n;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.p) {
            return this.o;
        }
        return null;
    }

    @DexIgnore
    public boolean f() {
        c cVar = this.E;
        if (cVar != null) {
            q1 q1Var = this.l;
            if (q1Var != null) {
                ((View) q1Var).removeCallbacks(cVar);
                this.E = null;
                return true;
            }
        }
        e eVar = this.C;
        if (eVar == null) {
            return false;
        }
        eVar.b();
        return true;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements p1.a {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public boolean a(h1 h1Var) {
            if (h1Var == null) {
                return false;
            }
            z1.this.H = ((v1) h1Var).getItem().getItemId();
            p1.a c = z1.this.c();
            if (c != null) {
                return c.a(h1Var);
            }
            return false;
        }

        @DexIgnore
        public void a(h1 h1Var, boolean z) {
            if (h1Var instanceof v1) {
                h1Var.m().a(false);
            }
            p1.a c = z1.this.c();
            if (c != null) {
                c.a(h1Var, z);
            }
        }
    }

    @DexIgnore
    public boolean d() {
        return f() | g();
    }

    @DexIgnore
    public Parcelable b() {
        g gVar = new g();
        gVar.e = this.H;
        return gVar;
    }

    @DexIgnore
    public void b(boolean z2) {
        if (z2) {
            super.a((v1) null);
            return;
        }
        h1 h1Var = this.g;
        if (h1Var != null) {
            h1Var.a(false);
        }
    }

    @DexIgnore
    public void a(Configuration configuration) {
        if (!this.v) {
            this.u = s0.a(this.f).c();
        }
        h1 h1Var = this.g;
        if (h1Var != null) {
            h1Var.c(true);
        }
    }

    @DexIgnore
    public void a(Drawable drawable) {
        d dVar = this.n;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.p = true;
        this.o = drawable;
    }

    @DexIgnore
    public View a(k1 k1Var, View view, ViewGroup viewGroup) {
        View actionView = k1Var.getActionView();
        if (actionView == null || k1Var.f()) {
            actionView = super.a(k1Var, view, viewGroup);
        }
        actionView.setVisibility(k1Var.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    public void a(k1 k1Var, q1.a aVar) {
        aVar.a(k1Var, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.l);
        if (this.F == null) {
            this.F = new b();
        }
        actionMenuItemView.setPopupCallback(this.F);
    }

    @DexIgnore
    public boolean a(int i, k1 k1Var) {
        return k1Var.h();
    }

    @DexIgnore
    public void a(boolean z2) {
        super.a(z2);
        ((View) this.l).requestLayout();
        h1 h1Var = this.g;
        boolean z3 = false;
        if (h1Var != null) {
            ArrayList<k1> c2 = h1Var.c();
            int size = c2.size();
            for (int i = 0; i < size; i++) {
                m8 a2 = c2.get(i).a();
                if (a2 != null) {
                    a2.setSubUiVisibilityListener(this);
                }
            }
        }
        h1 h1Var2 = this.g;
        ArrayList<k1> j = h1Var2 != null ? h1Var2.j() : null;
        if (this.q && j != null) {
            int size2 = j.size();
            if (size2 == 1) {
                z3 = !j.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.n == null) {
                this.n = new d(this.e);
            }
            ViewGroup viewGroup = (ViewGroup) this.n.getParent();
            if (viewGroup != this.l) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.n);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.l;
                actionMenuView.addView(this.n, actionMenuView.c());
            }
        } else {
            d dVar = this.n;
            if (dVar != null) {
                ViewParent parent = dVar.getParent();
                q1 q1Var = this.l;
                if (parent == q1Var) {
                    ((ViewGroup) q1Var).removeView(this.n);
                }
            }
        }
        ((ActionMenuView) this.l).setOverflowReserved(this.q);
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.n) {
            return false;
        }
        return super.a(viewGroup, i);
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        boolean z2 = false;
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        v1 v1Var2 = v1Var;
        while (v1Var2.t() != this.g) {
            v1Var2 = (v1) v1Var2.t();
        }
        View a2 = a(v1Var2.getItem());
        if (a2 == null) {
            return false;
        }
        this.H = v1Var.getItem().getItemId();
        int size = v1Var.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = v1Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        this.D = new a(this.f, v1Var, a2);
        this.D.a(z2);
        this.D.f();
        super.a(v1Var);
        return true;
    }

    @DexIgnore
    public final View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.l;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof q1.a) && ((q1.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a() {
        int i;
        ArrayList<k1> arrayList;
        int i2;
        int i3;
        int i4;
        z1 z1Var = this;
        h1 h1Var = z1Var.g;
        int i5 = 0;
        if (h1Var != null) {
            arrayList = h1Var.n();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = z1Var.u;
        int i7 = z1Var.t;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) z1Var.l;
        int i8 = i6;
        boolean z2 = false;
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < i; i11++) {
            k1 k1Var = arrayList.get(i11);
            if (k1Var.k()) {
                i9++;
            } else if (k1Var.j()) {
                i10++;
            } else {
                z2 = true;
            }
            if (z1Var.y && k1Var.isActionViewExpanded()) {
                i8 = 0;
            }
        }
        if (z1Var.q && (z2 || i10 + i9 > i8)) {
            i8--;
        }
        int i12 = i8 - i9;
        SparseBooleanArray sparseBooleanArray = z1Var.A;
        sparseBooleanArray.clear();
        if (z1Var.w) {
            int i13 = z1Var.z;
            i2 = i7 / i13;
            i3 = i13 + ((i7 % i13) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i14 = i7;
        int i15 = 0;
        int i16 = 0;
        while (i15 < i) {
            k1 k1Var2 = arrayList.get(i15);
            if (k1Var2.k()) {
                View a2 = z1Var.a(k1Var2, z1Var.B, viewGroup);
                if (z1Var.B == null) {
                    z1Var.B = a2;
                }
                if (z1Var.w) {
                    i2 -= ActionMenuView.a(a2, i3, i2, makeMeasureSpec, i5);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a2.getMeasuredWidth();
                i14 -= measuredWidth;
                if (i16 != 0) {
                    measuredWidth = i16;
                }
                int groupId = k1Var2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                k1Var2.d(true);
                i4 = i;
                i16 = measuredWidth;
            } else if (k1Var2.j()) {
                int groupId2 = k1Var2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i12 > 0 || z3) && i14 > 0 && (!z1Var.w || i2 > 0);
                boolean z5 = z4;
                if (z4) {
                    View a3 = z1Var.a(k1Var2, z1Var.B, viewGroup);
                    i4 = i;
                    if (z1Var.B == null) {
                        z1Var.B = a3;
                    }
                    if (z1Var.w) {
                        int a4 = ActionMenuView.a(a3, i3, i2, makeMeasureSpec, 0);
                        i2 -= a4;
                        if (a4 == 0) {
                            z5 = false;
                        }
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a3.getMeasuredWidth();
                    i14 -= measuredWidth2;
                    if (i16 == 0) {
                        i16 = measuredWidth2;
                    }
                    z4 = z5 & (!z1Var.w ? i14 + i16 > 0 : i14 >= 0);
                } else {
                    i4 = i;
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    int i17 = 0;
                    while (i17 < i15) {
                        k1 k1Var3 = arrayList.get(i17);
                        if (k1Var3.getGroupId() == groupId2) {
                            if (k1Var3.h()) {
                                i12++;
                            }
                            k1Var3.d(false);
                        }
                        i17++;
                    }
                }
                if (z4) {
                    i12--;
                }
                k1Var2.d(z4);
            } else {
                i4 = i;
                k1Var2.d(false);
                i15++;
                i5 = 0;
                z1Var = this;
                i = i4;
            }
            i15++;
            i5 = 0;
            z1Var = this;
            i = i4;
        }
        return true;
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z2) {
        d();
        super.a(h1Var, z2);
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        if (parcelable instanceof g) {
            int i = ((g) parcelable).e;
            if (i > 0) {
                MenuItem findItem = this.g.findItem(i);
                if (findItem != null) {
                    a((v1) findItem.getSubMenu());
                }
            }
        }
    }

    @DexIgnore
    public void a(ActionMenuView actionMenuView) {
        this.l = actionMenuView;
        actionMenuView.a(this.g);
    }
}
