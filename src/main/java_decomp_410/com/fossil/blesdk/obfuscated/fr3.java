package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ua.UAAccessToken;
import com.portfolio.platform.data.model.ua.UADataSource;
import com.portfolio.platform.data.model.ua.UADevice;
import com.portfolio.platform.data.model.ua.UAEmbedded;
import com.portfolio.platform.data.model.ua.UALink;
import com.portfolio.platform.data.model.ua.UALinks;
import com.portfolio.platform.underamour.UASharePref;
import com.portfolio.platform.underamour.UAValues;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fr3 {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public cr3 c;
    @DexIgnore
    public er3 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements e94<xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ fr3 a;
        @DexIgnore
        public /* final */ /* synthetic */ UADataSource b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements e94<xz1> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void a(xz1 xz1) {
                List<UALink> list;
                if (xz1 != null) {
                    FLogger.INSTANCE.getLocal().d(fr3.e, xz1.toString());
                    UADataSource uADataSource = (UADataSource) new Gson().a((JsonElement) xz1, UADataSource.class);
                    UASharePref a2 = UASharePref.c.a();
                    kd4.a((Object) uADataSource, "uaDataSourceResponse");
                    a2.a(uADataSource);
                    UADataSource c = UASharePref.c.a().c();
                    if (c != null) {
                        UALinks link = c.getLink();
                        if (link != null) {
                            list = link.getSelf();
                            if (list != null && (!list.isEmpty())) {
                                xz1 xz12 = (xz1) new Gson().a(new Gson().a((Object) list.get(0), (Type) UALink.class), xz1.class);
                                fr3 fr3 = this.a.a;
                                kd4.a((Object) xz12, "extraJsonObject");
                                fr3.a(xz12);
                                return;
                            }
                        }
                    }
                    list = null;
                    if (list != null) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fr3$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.fr3$b$b  reason: collision with other inner class name */
        public static final class C0085b<T> implements e94<Throwable> {
            @DexIgnore
            public static /* final */ C0085b a; // = new C0085b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(fr3.e, th.getMessage());
                }
            }
        }

        @DexIgnore
        public b(fr3 fr3, UADataSource uADataSource) {
            this.a = fr3;
            this.b = uADataSource;
        }

        @DexIgnore
        public final void a(xz1 xz1) {
            if (xz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) xz1, UAAccessToken.class);
                if (!qf4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                cr3 d = this.a.c;
                UADataSource uADataSource = this.b;
                String b2 = this.a.a;
                if (b2 != null) {
                    pd4 pd4 = pd4.a;
                    Locale locale = Locale.US;
                    kd4.a((Object) locale, "Locale.US");
                    String value = UAValues.Authorization.BEARER.getValue();
                    Object[] objArr = {UASharePref.c.a().a()};
                    String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                    d.a(uADataSource, b2, format).a(3).a(w84.a()).a(new a(this), (e94<? super Throwable>) C0085b.a);
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements e94<Throwable> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(fr3.e, th.getMessage());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements e94<xz1> {
        @DexIgnore
        public /* final */ /* synthetic */ fr3 a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements e94<xz1> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void a(xz1 xz1) {
                if (xz1 != null) {
                    FLogger.INSTANCE.getLocal().d(fr3.e, xz1.toString());
                    UADevice uADevice = (UADevice) new Gson().a((JsonElement) xz1, UADevice.class);
                    UASharePref a2 = UASharePref.c.a();
                    kd4.a((Object) uADevice, "uaDevice");
                    a2.a(uADevice);
                    this.a.a.a();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements e94<Throwable> {
            @DexIgnore
            public static /* final */ b a; // = new b();

            @DexIgnore
            public final void a(Throwable th) {
                if (th != null) {
                    FLogger.INSTANCE.getLocal().d(fr3.e, th.getMessage());
                }
            }
        }

        @DexIgnore
        public d(fr3 fr3) {
            this.a = fr3;
        }

        @DexIgnore
        public final void a(xz1 xz1) {
            if (xz1 != null) {
                UAAccessToken uAAccessToken = (UAAccessToken) new Gson().a((JsonElement) xz1, UAAccessToken.class);
                if (!qf4.b(UASharePref.c.a().a(), uAAccessToken.getAccessToken(), false, 2, (Object) null)) {
                    UASharePref.a(UASharePref.c.a(), uAAccessToken, false, 2, (Object) null);
                }
                cr3 d = this.a.c;
                String c = this.a.b;
                if (c != null) {
                    String b2 = this.a.a;
                    if (b2 != null) {
                        pd4 pd4 = pd4.a;
                        Locale locale = Locale.US;
                        kd4.a((Object) locale, "Locale.US");
                        String value = UAValues.Authorization.BEARER.getValue();
                        Object[] objArr = {UASharePref.c.a().a()};
                        String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        d.a(c, b2, format).a(3).a(w84.a()).a(new a(this), (e94<? super Throwable>) b.a);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements e94<Throwable> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(fr3.e, th.getMessage());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements e94<xz1> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public final void a(xz1 xz1) {
            if (xz1 != null) {
                FLogger.INSTANCE.getLocal().d(fr3.e, xz1.toString());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements e94<Throwable> {
        @DexIgnore
        public static /* final */ g a; // = new g();

        @DexIgnore
        public final void a(Throwable th) {
            if (th != null) {
                FLogger.INSTANCE.getLocal().d(fr3.e, th.getMessage());
            }
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = fr3.class.getSimpleName();
        kd4.a((Object) simpleName, "UADataSourceManager::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public fr3(cr3 cr3, er3 er3) {
        kd4.b(cr3, "uaApi");
        kd4.b(er3, "uaAuthorizationManager");
        this.c = cr3;
        this.d = er3;
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "deviceKey");
        this.b = str;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "clientId");
        this.a = str;
    }

    @DexIgnore
    public final void b() {
        if (this.b == null) {
            throw new IllegalArgumentException("deviceKey must be set");
        } else if (UASharePref.c.a().d() != null) {
            a();
        } else if (UASharePref.c.a().d() == null) {
            this.d.a().a(new d(this), (e94<? super Throwable>) e.a);
        }
    }

    @DexIgnore
    public final void a(xz1 xz1) {
        xz1 xz12 = new xz1();
        tz1 tz1 = new tz1();
        tz1.a((JsonElement) xz1);
        xz1 xz13 = new xz1();
        xz13.a("data_source", (JsonElement) tz1);
        xz12.a("priority_type", (JsonElement) new zz1(Constants.ACTIVITY));
        xz12.a("_links", (JsonElement) xz13);
        cr3 cr3 = this.c;
        String str = this.a;
        if (str != null) {
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            String value = UAValues.Authorization.BEARER.getValue();
            Object[] objArr = {UASharePref.c.a().a()};
            String format = String.format(locale, value, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            cr3.b(xz12, str, format).a(3).a(w84.a()).a(f.a, (e94<? super Throwable>) g.a);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        UAEmbedded uAEmbedded = new UAEmbedded();
        UADevice[] uADeviceArr = new UADevice[1];
        UADevice d2 = UASharePref.c.a().d();
        if (d2 != null) {
            uADeviceArr[0] = d2;
            uAEmbedded.setDevice(cb4.a((T[]) uADeviceArr));
            UADataSource uADataSource = new UADataSource();
            uADataSource.setEmbedded(uAEmbedded);
            this.d.a().a(new b(this, uADataSource), (e94<? super Throwable>) c.a);
            return;
        }
        kd4.a();
        throw null;
    }
}
