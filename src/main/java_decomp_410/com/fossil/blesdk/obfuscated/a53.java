package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.bt2;
import com.fossil.blesdk.obfuscated.q62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a53 extends zr2 implements k53, q62.b {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public tr3<mg2> j;
    @DexIgnore
    public j53 k;
    @DexIgnore
    public bt2 l;
    @DexIgnore
    public q62 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final a53 a() {
            return new a53();
        }

        @DexIgnore
        public final String b() {
            return a53.o;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ mg2 f;

        @DexIgnore
        public b(View view, mg2 mg2) {
            this.e = view;
            this.f = mg2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.u.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                mg2 mg2 = this.f;
                kd4.a((Object) mg2, "binding");
                mg2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = p23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a53 e;
        @DexIgnore
        public /* final */ /* synthetic */ mg2 f;

        @DexIgnore
        public c(a53 a53, mg2 mg2) {
            this.e = a53;
            this.f = mg2;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            q62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    kd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = a53.p.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    if (this.e.l != null && !TextUtils.isEmpty(item.getPlaceId())) {
                        this.f.q.setText("");
                        j53 c = a53.c(this.e);
                        String spannableString = fullText.toString();
                        kd4.a((Object) spannableString, "primaryText.toString()");
                        String placeId = item.getPlaceId();
                        kd4.a((Object) placeId, "item.placeId");
                        q62 a2 = this.e.m;
                        if (a2 != null) {
                            c.a(spannableString, placeId, a2.b());
                            q62 a3 = this.e.m;
                            if (a3 != null) {
                                a3.a();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ mg2 e;

        @DexIgnore
        public d(a53 a53, mg2 mg2) {
            this.e = mg2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.e.s;
            kd4.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ a53 e;

        @DexIgnore
        public e(a53 a53, mg2 mg2) {
            this.e = a53;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            kd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements bt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ a53 a;

        @DexIgnore
        public f(a53 a53) {
            this.a = a53;
        }

        @DexIgnore
        public void a(int i, boolean z) {
            a53.c(this.a).a(i, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ a53 e;

        @DexIgnore
        public g(a53 a53) {
            this.e = a53;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mg2 e;

        @DexIgnore
        public h(mg2 mg2) {
            this.e = mg2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = a53.class.getSimpleName();
        kd4.a((Object) simpleName, "WeatherSettingFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ j53 c(a53 a53) {
        j53 j53 = a53.k;
        if (j53 != null) {
            return j53;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        tr3<mg2> tr3 = this.j;
        if (tr3 != null) {
            mg2 a2 = tr3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        tr3<mg2> tr3 = this.j;
        if (tr3 == null) {
            kd4.d("mBinding");
            throw null;
        } else if (tr3.a() == null || this.l == null) {
            return true;
        } else {
            j53 j53 = this.k;
            if (j53 != null) {
                j53.i();
                return true;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T0() {
        tr3<mg2> tr3 = this.j;
        if (tr3 != null) {
            mg2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                kd4.a((Object) flexibleTextView, "it.ftvLocationError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.w;
                kd4.a((Object) flexibleTextView2, "it.tvAdded");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = a2.v;
                kd4.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        tr3<mg2> tr3 = this.j;
        if (tr3 != null) {
            mg2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                kd4.a((Object) flexibleTextView, "it.ftvLocationError");
                pd4 pd4 = pd4.a;
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                Object[] objArr = {appCompatAutoCompleteTextView.getText()};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                FlexibleTextView flexibleTextView2 = a2.t;
                kd4.a((Object) flexibleTextView2, "it.ftvLocationError");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.w;
                kd4.a((Object) flexibleTextView3, "it.tvAdded");
                flexibleTextView3.setVisibility(8);
                RecyclerView recyclerView = a2.v;
                kd4.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e0() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.n(childFragmentManager);
        }
    }

    @DexIgnore
    public void o(List<WeatherLocationWrapper> list) {
        kd4.b(list, "locations");
        bt2 bt2 = this.l;
        if (bt2 != null) {
            bt2.a((List<WeatherLocationWrapper>) qd4.c(list));
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        mg2 mg2 = (mg2) qa.a(layoutInflater, R.layout.fragment_weather_setting, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = mg2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new c(this, mg2));
        appCompatAutoCompleteTextView.addTextChangedListener(new d(this, mg2));
        appCompatAutoCompleteTextView.setOnKeyListener(new e(this, mg2));
        this.l = new bt2();
        bt2 bt2 = this.l;
        if (bt2 != null) {
            bt2.a((bt2.b) new f(this));
        }
        RecyclerView recyclerView = mg2.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        kd4.a((Object) mg2, "binding");
        View d2 = mg2.d();
        kd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, mg2));
        mg2.r.setOnClickListener(new g(this));
        mg2.s.setOnClickListener(new h(mg2));
        this.j = new tr3<>(this, mg2);
        return mg2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        j53 j53 = this.k;
        if (j53 != null) {
            j53.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        j53 j53 = this.k;
        if (j53 != null) {
            j53.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            j53 j53 = this.k;
            if (j53 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", j53.h());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void a(j53 j53) {
        kd4.b(j53, "presenter");
        this.k = j53;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                this.m = new q62(context, placesClient);
                q62 q62 = this.m;
                if (q62 != null) {
                    q62.a((q62.b) this);
                }
                tr3<mg2> tr3 = this.j;
                if (tr3 != null) {
                    mg2 a2 = tr3.a();
                    if (a2 != null) {
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        if (appCompatAutoCompleteTextView != null) {
                            appCompatAutoCompleteTextView.setAdapter(this.m);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
    }
}
