package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v1 */
public class C3078v1 extends com.fossil.blesdk.obfuscated.C1915h1 implements android.view.SubMenu {

    @DexIgnore
    /* renamed from: B */
    public com.fossil.blesdk.obfuscated.C1915h1 f10128B;

    @DexIgnore
    /* renamed from: C */
    public com.fossil.blesdk.obfuscated.C2179k1 f10129C;

    @DexIgnore
    public C3078v1(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        super(context);
        this.f10128B = h1Var;
        this.f10129C = k1Var;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11460a(com.fossil.blesdk.obfuscated.C1915h1.C1916a aVar) {
        this.f10128B.mo11460a(aVar);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo11484b(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return this.f10128B.mo11484b(k1Var);
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo11494d() {
        com.fossil.blesdk.obfuscated.C2179k1 k1Var = this.f10129C;
        int itemId = k1Var != null ? k1Var.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.mo11494d() + ":" + itemId;
    }

    @DexIgnore
    public android.view.MenuItem getItem() {
        return this.f10129C;
    }

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11514m() {
        return this.f10128B.mo11514m();
    }

    @DexIgnore
    /* renamed from: o */
    public boolean mo11516o() {
        return this.f10128B.mo11516o();
    }

    @DexIgnore
    /* renamed from: p */
    public boolean mo11517p() {
        return this.f10128B.mo11517p();
    }

    @DexIgnore
    /* renamed from: q */
    public boolean mo11520q() {
        return this.f10128B.mo11520q();
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z) {
        this.f10128B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    public android.view.SubMenu setHeaderIcon(android.graphics.drawable.Drawable drawable) {
        super.mo11450a(drawable);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderTitle(java.lang.CharSequence charSequence) {
        super.mo11452a(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderView(android.view.View view) {
        super.mo11451a(view);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setIcon(android.graphics.drawable.Drawable drawable) {
        this.f10129C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.f10128B.setQwertyMode(z);
    }

    @DexIgnore
    /* renamed from: t */
    public android.view.Menu mo16960t() {
        return this.f10128B;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11467a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        return super.mo11467a(h1Var, menuItem) || this.f10128B.mo11467a(h1Var, menuItem);
    }

    @DexIgnore
    public android.view.SubMenu setHeaderIcon(int i) {
        super.mo11493d(i);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderTitle(int i) {
        super.mo11499e(i);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setIcon(int i) {
        this.f10129C.setIcon(i);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11468a(com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return this.f10128B.mo11468a(k1Var);
    }
}
