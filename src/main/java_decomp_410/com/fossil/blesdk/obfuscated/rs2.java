package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rs2 extends RecyclerView.g<b> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<Complication> c;
    @DexIgnore
    public c d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public Complication d;
        @DexIgnore
        public /* final */ /* synthetic */ rs2 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.e.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    c c = this.e.e.c();
                    if (c != null) {
                        Object obj = this.e.e.c.get(this.e.getAdapterPosition());
                        kd4.a(obj, "mData[adapterPosition]");
                        c.a((Complication) obj);
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rs2$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.rs2$b$b  reason: collision with other inner class name */
        public static final class C0104b implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0104b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                kd4.b(customizeWidget, "view");
                this.a.e.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(rs2 rs2, View view) {
            super(view);
            kd4.b(view, "view");
            this.e = rs2;
            View findViewById = view.findViewById(R.id.wc_complication);
            kd4.a((Object) findViewById, "view.findViewById(R.id.wc_complication)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_complication_name);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.tv_complication_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(R.id.iv_indicator);
            this.a.setOnClickListener(new a(this));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x008a, code lost:
            if (r0 != null) goto L_0x008f;
         */
        @DexIgnore
        public final void a(Complication complication) {
            String str;
            kd4.b(complication, "complication");
            this.d = complication;
            if (this.d != null) {
                this.a.b(complication.getComplicationId());
                this.b.setText(sm2.a(PortfolioApp.W.c(), complication.getNameKey(), complication.getName()));
            }
            this.a.setSelectedWc(kd4.a((Object) complication.getComplicationId(), (Object) this.e.b));
            if (kd4.a((Object) complication.getComplicationId(), (Object) this.e.b)) {
                View view = this.c;
                kd4.a((Object) view, "ivIndicator");
                view.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_arrow));
            } else {
                View view2 = this.c;
                kd4.a((Object) view2, "ivIndicator");
                view2.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_grey));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            Complication complication2 = this.d;
            if (complication2 != null) {
                str = complication2.getComplicationId();
            }
            str = "";
            Intent putExtra = intent.putExtra("KEY_ID", str);
            kd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "COMPLICATION", putExtra, (View.OnDragListener) null, new C0104b(this), 4, (Object) null);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Complication complication);
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ rs2(ArrayList arrayList, c cVar, int i, fd4 fd4) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final c c() {
        return this.d;
    }

    @DexIgnore
    public final void d() {
        T t;
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (kd4.a((Object) ((Complication) t).getComplicationId(), (Object) "empty")) {
                break;
            }
        }
        Complication complication = (Complication) t;
        if (complication != null) {
            Collections.swap(this.c, this.c.indexOf(complication), 0);
        }
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "complicationId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_complication, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026plication, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public rs2(ArrayList<Complication> arrayList, c cVar) {
        kd4.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<Complication> list) {
        kd4.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        d();
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d("ComplicationsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        kd4.b(str, "complicationId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (kd4.a((Object) ((Complication) t).getComplicationId(), (Object) str)) {
                break;
            }
        }
        Complication complication = (Complication) t;
        if (complication != null) {
            return this.c.indexOf(complication);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        kd4.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            Complication complication = this.c.get(i);
            kd4.a((Object) complication, "mData[position]");
            bVar.a(complication);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        kd4.b(cVar, "listener");
        this.d = cVar;
    }
}
