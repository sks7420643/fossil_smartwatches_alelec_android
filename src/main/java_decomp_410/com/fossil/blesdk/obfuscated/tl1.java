package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Pair;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tl1 extends cl1 {
    @DexIgnore
    public tl1(dl1 dl1) {
        super(dl1);
    }

    @DexIgnore
    public static void b(Map<Integer, List<Long>> map, int i, long j) {
        List list = map.get(Integer.valueOf(i));
        if (list == null) {
            list = new ArrayList();
            map.put(Integer.valueOf(i), list);
        }
        list.add(Long.valueOf(j / 1000));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v25, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v8, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v6, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v27, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v9, resolved type: com.fossil.blesdk.obfuscated.g4} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v43, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v30, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r0v96, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02fb  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x03a8  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x03fd  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0450  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0471  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x05eb  */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x0606 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0657  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x065a  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0660  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x0669  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x0990  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x09a9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x09e8  */
    /* JADX WARNING: Removed duplicated region for block: B:279:0x09eb  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x09f2  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x09fa  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x02db  */
    public final a61[] a(String str, c61[] c61Arr, i61[] i61Arr) {
        Iterator it;
        g4 g4Var;
        g4 g4Var2;
        h61[] h61Arr;
        g4 g4Var3;
        Iterator it2;
        Map map;
        int i;
        g4 g4Var4;
        g4 g4Var5;
        Map map2;
        Boolean bool;
        Map map3;
        Map map4;
        Map map5;
        v51 v51;
        String str2;
        g4 g4Var6;
        Map map6;
        g4 g4Var7;
        Boolean bool2;
        boolean z;
        boolean z2;
        Boolean a;
        Boolean bool3;
        BitSet bitSet;
        g4 g4Var8;
        Boolean bool4;
        g4 g4Var9;
        String str3;
        g4 g4Var10;
        g4 g4Var11;
        g4 g4Var12;
        String str4;
        g4 g4Var13;
        int i2;
        HashSet hashSet;
        g4 g4Var14;
        Boolean bool5;
        long j;
        c61 c61;
        d61[] d61Arr;
        Long l;
        String str5;
        dg1 b;
        g4 g4Var15;
        HashSet hashSet2;
        String str6;
        c61 c612;
        dg1 dg1;
        Map map7;
        Iterator it3;
        Map map8;
        g4 g4Var16;
        g4 g4Var17;
        Map map9;
        Map map10;
        Map map11;
        Boolean bool6;
        HashSet hashSet3;
        g4 g4Var18;
        String str7;
        g4 g4Var19;
        Boolean bool7;
        g4 g4Var20;
        String str8;
        g4 g4Var21;
        Map map12;
        String str9;
        BitSet bitSet2;
        c61 c613;
        BitSet bitSet3;
        Boolean bool8;
        Map map13;
        Boolean bool9;
        boolean z3;
        boolean z4;
        Boolean a2;
        Boolean bool10;
        g4 g4Var22;
        g4 g4Var23;
        BitSet bitSet4;
        String str10;
        Long l2;
        Boolean bool11;
        c61 c614;
        g4 g4Var24;
        int i3;
        String str11;
        c61 c615;
        int length;
        int i4;
        int i5;
        c61 c616;
        Boolean bool12;
        BitSet bitSet5;
        Iterator<Integer> it4;
        g4 g4Var25;
        BitSet bitSet6;
        g4 g4Var26;
        g4 g4Var27;
        g4 g4Var28;
        boolean z5;
        String str12 = str;
        c61[] c61Arr2 = c61Arr;
        i61[] i61Arr2 = i61Arr;
        bk0.b(str);
        HashSet hashSet4 = new HashSet();
        g4 g4Var29 = new g4();
        g4 g4Var30 = new g4();
        g4 g4Var31 = new g4();
        g4 g4Var32 = new g4();
        g4 g4Var33 = new g4();
        boolean j2 = l().j(str12);
        Map<Integer, g61> e = o().e(str12);
        boolean z6 = true;
        if (e != null) {
            Iterator<Integer> it5 = e.keySet().iterator();
            while (it5.hasNext()) {
                int intValue = it5.next().intValue();
                g61 g61 = e.get(Integer.valueOf(intValue));
                BitSet bitSet7 = (BitSet) g4Var30.get(Integer.valueOf(intValue));
                BitSet bitSet8 = (BitSet) g4Var31.get(Integer.valueOf(intValue));
                Map<Integer, g61> map14 = e;
                if (j2) {
                    g4Var25 = new g4();
                    it4 = it5;
                    if (g61 != null) {
                        b61[] b61Arr = g61.e;
                        if (b61Arr != null) {
                            bitSet5 = bitSet8;
                            int length2 = b61Arr.length;
                            bool12 = z6;
                            int i6 = 0;
                            while (i6 < length2) {
                                int i7 = length2;
                                b61 b61 = b61Arr[i6];
                                b61[] b61Arr2 = b61Arr;
                                Integer num = b61.c;
                                if (num != null) {
                                    g4Var25.put(num, b61.d);
                                }
                                i6++;
                                length2 = i7;
                                b61Arr = b61Arr2;
                            }
                            g4Var32.put(Integer.valueOf(intValue), g4Var25);
                        }
                    }
                    bitSet5 = bitSet8;
                    bool12 = z6;
                    g4Var32.put(Integer.valueOf(intValue), g4Var25);
                } else {
                    it4 = it5;
                    bitSet5 = bitSet8;
                    bool12 = z6;
                    g4Var25 = null;
                }
                if (bitSet7 == null) {
                    bitSet7 = new BitSet();
                    g4Var30.put(Integer.valueOf(intValue), bitSet7);
                    bitSet6 = new BitSet();
                    g4Var31.put(Integer.valueOf(intValue), bitSet6);
                } else {
                    bitSet6 = bitSet5;
                }
                int i8 = 0;
                while (true) {
                    long[] jArr = g61.c;
                    g4Var26 = g4Var32;
                    if (i8 >= (jArr.length << 6)) {
                        break;
                    }
                    if (jl1.a(jArr, i8)) {
                        g4Var28 = g4Var31;
                        g4Var27 = g4Var30;
                        d().A().a("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue), Integer.valueOf(i8));
                        bitSet6.set(i8);
                        if (jl1.a(g61.d, i8)) {
                            bitSet7.set(i8);
                            z5 = true;
                            if (g4Var25 != null && !z5) {
                                g4Var25.remove(Integer.valueOf(i8));
                            }
                            i8++;
                            g4Var32 = g4Var26;
                            g4Var31 = g4Var28;
                            g4Var30 = g4Var27;
                        }
                    } else {
                        g4Var28 = g4Var31;
                        g4Var27 = g4Var30;
                    }
                    z5 = false;
                    g4Var25.remove(Integer.valueOf(i8));
                    i8++;
                    g4Var32 = g4Var26;
                    g4Var31 = g4Var28;
                    g4Var30 = g4Var27;
                }
                g4 g4Var34 = g4Var31;
                g4 g4Var35 = g4Var30;
                a61 a61 = new a61();
                g4Var29.put(Integer.valueOf(intValue), a61);
                a61.f = false;
                a61.e = g61;
                a61.d = new g61();
                a61.d.d = jl1.a(bitSet7);
                a61.d.c = jl1.a(bitSet6);
                if (j2) {
                    a61.d.e = a(g4Var25);
                    g4Var33.put(Integer.valueOf(intValue), new g4());
                }
                e = map14;
                it5 = it4;
                g4Var32 = g4Var26;
                z6 = bool12;
                g4Var31 = g4Var34;
                g4Var30 = g4Var35;
            }
        }
        Boolean bool13 = z6;
        g4 g4Var36 = g4Var32;
        g4 g4Var37 = g4Var31;
        g4 g4Var38 = g4Var30;
        String str13 = "Filter definition";
        String str14 = "Skipping failed audience ID";
        if (c61Arr2 != null) {
            g4 g4Var39 = new g4();
            int length3 = c61Arr2.length;
            long j3 = 0;
            c61 c617 = null;
            Long l3 = null;
            int i9 = 0;
            while (i9 < length3) {
                c61 c618 = c61Arr2[i9];
                String str15 = c618.d;
                d61[] d61Arr2 = c618.c;
                int i10 = i9;
                int i11 = length3;
                if (l().c(str12, jg1.T)) {
                    m();
                    Long l4 = (Long) jl1.b(c618, "_eid");
                    boolean z7 = l4 != null;
                    String str16 = str14;
                    if (z7 && str15.equals("_ep")) {
                        m();
                        str15 = jl1.b(c618, "_en");
                        if (TextUtils.isEmpty(str15)) {
                            d().s().a("Extra parameter without an event name. eventId", l4);
                            c614 = c617;
                            g4Var13 = g4Var39;
                            g4Var24 = g4Var33;
                            bool11 = bool13;
                            i3 = i11;
                            str11 = str16;
                        } else {
                            if (c617 == null || l3 == null || l4.longValue() != l3.longValue()) {
                                Pair<c61, Long> a3 = o().a(str12, l4);
                                c614 = c617;
                                if (a3 != null) {
                                    Object obj = a3.first;
                                    if (obj != null) {
                                        c617 = (c61) obj;
                                        j3 = ((Long) a3.second).longValue();
                                        m();
                                        l3 = (Long) jl1.b(c617, "_eid");
                                    }
                                }
                                g4Var13 = g4Var39;
                                g4Var24 = g4Var33;
                                bool11 = bool13;
                                i3 = i11;
                                str11 = str16;
                                d().s().a("Extra parameter without existing main event. eventName, eventId", str15, l4);
                            }
                            c61 c619 = c617;
                            l = l3;
                            j3--;
                            if (j3 <= 0) {
                                am1 o = o();
                                o.e();
                                o.d().A().a("Clearing complex main event info. appId", str12);
                                try {
                                    SQLiteDatabase v = o.v();
                                    c616 = c619;
                                    try {
                                        String[] strArr = new String[1];
                                        try {
                                            strArr[0] = str12;
                                            v.execSQL("delete from main_event_params where app_id=?", strArr);
                                        } catch (SQLiteException e2) {
                                            e = e2;
                                        }
                                    } catch (SQLiteException e3) {
                                        e = e3;
                                        o.d().s().a("Error clearing complex main event", e);
                                        g4Var14 = g4Var39;
                                        g4Var12 = g4Var33;
                                        c615 = c616;
                                        bool5 = bool13;
                                        i2 = i11;
                                        str4 = str16;
                                        d61[] d61Arr3 = c615.c;
                                        d61[] d61Arr4 = new d61[(d61Arr3.length + d61Arr2.length)];
                                        length = d61Arr3.length;
                                        i4 = 0;
                                        i5 = 0;
                                        while (i4 < length) {
                                        }
                                        if (i5 > 0) {
                                        }
                                    }
                                } catch (SQLiteException e4) {
                                    e = e4;
                                    c616 = c619;
                                    o.d().s().a("Error clearing complex main event", e);
                                    g4Var14 = g4Var39;
                                    g4Var12 = g4Var33;
                                    c615 = c616;
                                    bool5 = bool13;
                                    i2 = i11;
                                    str4 = str16;
                                    d61[] d61Arr32 = c615.c;
                                    d61[] d61Arr42 = new d61[(d61Arr32.length + d61Arr2.length)];
                                    length = d61Arr32.length;
                                    i4 = 0;
                                    i5 = 0;
                                    while (i4 < length) {
                                    }
                                    if (i5 > 0) {
                                    }
                                }
                                g4Var14 = g4Var39;
                                g4Var12 = g4Var33;
                                c615 = c616;
                                bool5 = bool13;
                                i2 = i11;
                                str4 = str16;
                            } else {
                                c61 c6110 = c619;
                                bool5 = bool13;
                                i2 = i11;
                                g4Var14 = g4Var39;
                                str4 = str16;
                                g4Var12 = g4Var33;
                                o().a(str, l4, j3, c6110);
                                c615 = c6110;
                            }
                            d61[] d61Arr322 = c615.c;
                            d61[] d61Arr422 = new d61[(d61Arr322.length + d61Arr2.length)];
                            length = d61Arr322.length;
                            i4 = 0;
                            i5 = 0;
                            while (i4 < length) {
                                d61 d61 = d61Arr322[i4];
                                m();
                                d61[] d61Arr5 = d61Arr322;
                                if (jl1.a(c618, d61.c) == null) {
                                    d61Arr422[i5] = d61;
                                    i5++;
                                }
                                i4++;
                                d61Arr322 = d61Arr5;
                            }
                            if (i5 > 0) {
                                int length4 = d61Arr2.length;
                                int i12 = 0;
                                while (i12 < length4) {
                                    d61Arr422[i5] = d61Arr2[i12];
                                    i12++;
                                    i5++;
                                }
                                if (i5 != d61Arr422.length) {
                                    d61Arr422 = Arrays.copyOf(d61Arr422, i5);
                                }
                                c61 = c615;
                                d61Arr = d61Arr422;
                                str5 = str15;
                                j = j3;
                                b = o().b(str12, c618.d);
                                if (b != null) {
                                    d().v().a("Event aggregate wasn't created during raw event logging. appId, event", tg1.a(str), i().a(str5));
                                    g4Var11 = g4Var36;
                                    str3 = str13;
                                    g4Var10 = g4Var37;
                                    g4Var9 = g4Var38;
                                    hashSet2 = hashSet4;
                                    g4Var15 = g4Var29;
                                    i61[] i61Arr3 = i61Arr;
                                    c612 = c618;
                                    str6 = str12;
                                    dg1 = new dg1(str, c618.d, 1, 1, c618.e.longValue(), 0, (Long) null, (Long) null, (Long) null, (Boolean) null);
                                } else {
                                    i61[] i61Arr4 = i61Arr;
                                    str3 = str13;
                                    hashSet2 = hashSet4;
                                    g4Var15 = g4Var29;
                                    c612 = c618;
                                    str6 = str12;
                                    g4Var11 = g4Var36;
                                    g4Var10 = g4Var37;
                                    g4Var9 = g4Var38;
                                    dg1 = new dg1(b.a, b.b, b.c + 1, b.d + 1, b.e, b.f, b.g, b.h, b.i, b.j);
                                }
                                o().a(dg1);
                                long j4 = dg1.c;
                                g4 g4Var40 = g4Var14;
                                map7 = (Map) g4Var40.get(str5);
                                if (map7 == null) {
                                    map7 = o().g(str6, str5);
                                    if (map7 == null) {
                                        map7 = new g4();
                                    }
                                    g4Var40.put(str5, map7);
                                }
                                Map map15 = map7;
                                it3 = map15.keySet().iterator();
                                while (it3.hasNext()) {
                                    int intValue2 = ((Integer) it3.next()).intValue();
                                    HashSet hashSet5 = hashSet2;
                                    if (hashSet5.contains(Integer.valueOf(intValue2))) {
                                        d().A().a(str4, Integer.valueOf(intValue2));
                                        hashSet3 = hashSet5;
                                    } else {
                                        String str17 = str4;
                                        g4 g4Var41 = g4Var15;
                                        a61 a612 = (a61) g4Var41.get(Integer.valueOf(intValue2));
                                        g4 g4Var42 = g4Var40;
                                        g4 g4Var43 = g4Var9;
                                        BitSet bitSet9 = (BitSet) g4Var43.get(Integer.valueOf(intValue2));
                                        c61 c6111 = c612;
                                        Iterator it6 = it3;
                                        g4 g4Var44 = g4Var10;
                                        BitSet bitSet10 = (BitSet) g4Var44.get(Integer.valueOf(intValue2));
                                        if (j2) {
                                            g4Var17 = g4Var11;
                                            map8 = (Map) g4Var17.get(Integer.valueOf(intValue2));
                                            str10 = str17;
                                            g4Var16 = g4Var12;
                                            map9 = (Map) g4Var16.get(Integer.valueOf(intValue2));
                                        } else {
                                            str10 = str17;
                                            g4Var16 = g4Var12;
                                            g4Var17 = g4Var11;
                                            map9 = null;
                                            map8 = null;
                                        }
                                        if (a612 == null) {
                                            a61 a613 = new a61();
                                            g4Var41.put(Integer.valueOf(intValue2), a613);
                                            Boolean bool14 = bool5;
                                            a613.f = bool14;
                                            BitSet bitSet11 = new BitSet();
                                            Map map16 = map9;
                                            g4Var43.put(Integer.valueOf(intValue2), bitSet11);
                                            BitSet bitSet12 = new BitSet();
                                            BitSet bitSet13 = bitSet11;
                                            g4Var44.put(Integer.valueOf(intValue2), bitSet12);
                                            if (j2) {
                                                Map g4Var45 = new g4();
                                                bitSet4 = bitSet12;
                                                g4Var17.put(Integer.valueOf(intValue2), g4Var45);
                                                Map g4Var46 = new g4();
                                                g4Var16.put(Integer.valueOf(intValue2), g4Var46);
                                                g4Var22 = g4Var17;
                                                g4Var23 = g4Var16;
                                                map10 = g4Var45;
                                                map11 = g4Var46;
                                                bool6 = bool14;
                                                bitSet9 = bitSet13;
                                            } else {
                                                bitSet4 = bitSet12;
                                                bool6 = bool14;
                                                g4Var22 = g4Var17;
                                                g4Var23 = g4Var16;
                                                bitSet9 = bitSet13;
                                                map10 = map8;
                                                map11 = map16;
                                            }
                                            bitSet10 = bitSet4;
                                        } else {
                                            Map map17 = map9;
                                            bool6 = bool5;
                                            g4Var22 = g4Var17;
                                            g4Var23 = g4Var16;
                                            map10 = map8;
                                            map11 = map17;
                                        }
                                        for (s51 s51 : (List) map15.get(Integer.valueOf(intValue2))) {
                                            Boolean bool15 = bool6;
                                            Map map18 = map15;
                                            if (d().a(2)) {
                                                g4Var18 = g4Var41;
                                                d().A().a("Evaluating filter. audience, filter, event", Integer.valueOf(intValue2), s51.c, i().a(s51.d));
                                                str7 = str3;
                                                d().A().a(str7, m().a(s51));
                                            } else {
                                                g4Var18 = g4Var41;
                                                str7 = str3;
                                            }
                                            Integer num2 = s51.c;
                                            if (num2 == null || num2.intValue() > 256) {
                                                g4Var20 = g4Var43;
                                                str8 = str7;
                                                g4Var21 = g4Var44;
                                                c613 = c6111;
                                                bitSet2 = bitSet10;
                                                bool7 = bool15;
                                                g4Var19 = g4Var18;
                                                str9 = str5;
                                                bitSet3 = bitSet9;
                                                map12 = map10;
                                                d().v().a("Invalid event filter ID. appId, id", tg1.a(str), String.valueOf(s51.c));
                                            } else {
                                                if (j2) {
                                                    if (s51 != null) {
                                                        Boolean bool16 = s51.h;
                                                        if (bool16 != null && bool16.booleanValue()) {
                                                            z3 = true;
                                                            if (s51 != null) {
                                                                Boolean bool17 = s51.i;
                                                                if (bool17 != null && bool17.booleanValue()) {
                                                                    z4 = true;
                                                                    if (bitSet9.get(s51.c.intValue()) || z3 || z4) {
                                                                        s51 s512 = s51;
                                                                        str8 = str7;
                                                                        g4Var21 = g4Var44;
                                                                        c613 = c6111;
                                                                        bool7 = bool15;
                                                                        bitSet2 = bitSet10;
                                                                        s51 s513 = s512;
                                                                        g4Var20 = g4Var43;
                                                                        s51 s514 = s512;
                                                                        String str18 = str5;
                                                                        str9 = str5;
                                                                        bitSet3 = bitSet9;
                                                                        map12 = map10;
                                                                        g4Var19 = g4Var18;
                                                                        String str19 = "Event filter result";
                                                                        a2 = a(s513, str18, d61Arr, j4);
                                                                        vg1 A = d().A();
                                                                        if (a2 == null) {
                                                                            bool10 = "null";
                                                                        } else {
                                                                            bool10 = a2;
                                                                        }
                                                                        A.a(str19, bool10);
                                                                        if (a2 == null) {
                                                                            hashSet5.add(Integer.valueOf(intValue2));
                                                                        } else {
                                                                            bitSet2.set(s514.c.intValue());
                                                                            if (a2.booleanValue()) {
                                                                                bitSet3.set(s514.c.intValue());
                                                                                if ((z3 || z4) && c613.e != null) {
                                                                                    if (z4) {
                                                                                        b(map11, s514.c.intValue(), c613.e.longValue());
                                                                                    } else {
                                                                                        map13 = map12;
                                                                                        a((Map<Integer, Long>) map13, s514.c.intValue(), c613.e.longValue());
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                        d().A().a("Event filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue2), s51.c);
                                                                        String str20 = str;
                                                                        i61[] i61Arr5 = i61Arr;
                                                                        str8 = str7;
                                                                        map15 = map18;
                                                                        bool8 = bool15;
                                                                        g4Var41 = g4Var18;
                                                                    }
                                                                }
                                                            }
                                                            z4 = false;
                                                            if (bitSet9.get(s51.c.intValue())) {
                                                            }
                                                            s51 s5122 = s51;
                                                            str8 = str7;
                                                            g4Var21 = g4Var44;
                                                            c613 = c6111;
                                                            bool7 = bool15;
                                                            bitSet2 = bitSet10;
                                                            s51 s5132 = s5122;
                                                            g4Var20 = g4Var43;
                                                            s51 s5142 = s5122;
                                                            String str182 = str5;
                                                            str9 = str5;
                                                            bitSet3 = bitSet9;
                                                            map12 = map10;
                                                            g4Var19 = g4Var18;
                                                            String str192 = "Event filter result";
                                                            a2 = a(s5132, str182, d61Arr, j4);
                                                            vg1 A2 = d().A();
                                                            if (a2 == null) {
                                                            }
                                                            A2.a(str192, bool10);
                                                            if (a2 == null) {
                                                            }
                                                        }
                                                    }
                                                    z3 = false;
                                                    if (s51 != null) {
                                                    }
                                                    z4 = false;
                                                    if (bitSet9.get(s51.c.intValue())) {
                                                    }
                                                    s51 s51222 = s51;
                                                    str8 = str7;
                                                    g4Var21 = g4Var44;
                                                    c613 = c6111;
                                                    bool7 = bool15;
                                                    bitSet2 = bitSet10;
                                                    s51 s51322 = s51222;
                                                    g4Var20 = g4Var43;
                                                    s51 s51422 = s51222;
                                                    String str1822 = str5;
                                                    str9 = str5;
                                                    bitSet3 = bitSet9;
                                                    map12 = map10;
                                                    g4Var19 = g4Var18;
                                                    String str1922 = "Event filter result";
                                                    a2 = a(s51322, str1822, d61Arr, j4);
                                                    vg1 A22 = d().A();
                                                    if (a2 == null) {
                                                    }
                                                    A22.a(str1922, bool10);
                                                    if (a2 == null) {
                                                    }
                                                } else {
                                                    g4Var20 = g4Var43;
                                                    str8 = str7;
                                                    g4Var21 = g4Var44;
                                                    map13 = map10;
                                                    c613 = c6111;
                                                    bitSet2 = bitSet10;
                                                    bool7 = bool15;
                                                    g4Var19 = g4Var18;
                                                    str9 = str5;
                                                    s51 s515 = s51;
                                                    bitSet3 = bitSet9;
                                                    String str21 = "Event filter result";
                                                    if (bitSet3.get(s515.c.intValue())) {
                                                        d().A().a("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue2), s515.c);
                                                    } else {
                                                        map12 = map13;
                                                        Boolean a4 = a(s515, str9, d61Arr, j4);
                                                        vg1 A3 = d().A();
                                                        if (a4 == null) {
                                                            bool9 = "null";
                                                        } else {
                                                            bool9 = a4;
                                                        }
                                                        A3.a(str21, bool9);
                                                        if (a4 == null) {
                                                            hashSet5.add(Integer.valueOf(intValue2));
                                                        } else {
                                                            bitSet2.set(s515.c.intValue());
                                                            if (a4.booleanValue()) {
                                                                bitSet3.set(s515.c.intValue());
                                                            }
                                                        }
                                                    }
                                                }
                                                i61[] i61Arr6 = i61Arr;
                                                bitSet9 = bitSet3;
                                                map10 = map13;
                                                bitSet10 = bitSet2;
                                                str5 = str9;
                                                g4Var44 = g4Var21;
                                                g4Var43 = g4Var20;
                                                bool8 = bool7;
                                                g4Var41 = g4Var19;
                                                String str22 = str;
                                                c6111 = c613;
                                                map15 = map18;
                                            }
                                            String str23 = str;
                                            i61[] i61Arr7 = i61Arr;
                                            bitSet9 = bitSet3;
                                            str5 = str9;
                                            map10 = map12;
                                            g4Var43 = g4Var20;
                                            bool8 = bool7;
                                            g4Var41 = g4Var19;
                                            c6111 = c613;
                                            bitSet10 = bitSet2;
                                            map15 = map18;
                                            g4Var44 = g4Var21;
                                        }
                                        String str24 = str;
                                        i61[] i61Arr8 = i61Arr;
                                        bool5 = bool6;
                                        g4Var15 = g4Var41;
                                        g4Var9 = g4Var43;
                                        c612 = c6111;
                                        g4Var10 = g4Var44;
                                        hashSet3 = hashSet5;
                                        it3 = it6;
                                        map15 = map15;
                                        g4Var40 = g4Var42;
                                    }
                                }
                                g4Var13 = g4Var40;
                                bool4 = bool5;
                                hashSet = hashSet2;
                                g4Var8 = g4Var15;
                                l3 = l;
                                c617 = c61;
                                j3 = j;
                                i9 = i10 + 1;
                                str12 = str;
                                c61Arr2 = c61Arr;
                                hashSet4 = hashSet;
                                length3 = i2;
                                g4Var39 = g4Var13;
                                str14 = str4;
                                g4Var33 = g4Var12;
                                g4Var36 = g4Var11;
                                g4Var37 = g4Var10;
                                str13 = str3;
                                g4Var38 = g4Var9;
                                bool13 = bool4;
                                g4Var29 = g4Var8;
                                i61[] i61Arr9 = i61Arr;
                            } else {
                                d().v().a("No unique parameters in main event. eventName", str15);
                                c61 = c615;
                                str5 = str15;
                                d61Arr = d61Arr2;
                                j = j3;
                                b = o().b(str12, c618.d);
                                if (b != null) {
                                }
                                o().a(dg1);
                                long j42 = dg1.c;
                                g4 g4Var402 = g4Var14;
                                map7 = (Map) g4Var402.get(str5);
                                if (map7 == null) {
                                }
                                Map map152 = map7;
                                it3 = map152.keySet().iterator();
                                while (it3.hasNext()) {
                                }
                                g4Var13 = g4Var402;
                                bool4 = bool5;
                                hashSet = hashSet2;
                                g4Var8 = g4Var15;
                                l3 = l;
                                c617 = c61;
                                j3 = j;
                                i9 = i10 + 1;
                                str12 = str;
                                c61Arr2 = c61Arr;
                                hashSet4 = hashSet;
                                length3 = i2;
                                g4Var39 = g4Var13;
                                str14 = str4;
                                g4Var33 = g4Var12;
                                g4Var36 = g4Var11;
                                g4Var37 = g4Var10;
                                str13 = str3;
                                g4Var38 = g4Var9;
                                bool13 = bool4;
                                g4Var29 = g4Var8;
                                i61[] i61Arr92 = i61Arr;
                            }
                        }
                        str3 = str13;
                        hashSet = hashSet4;
                        g4Var8 = g4Var29;
                        g4Var11 = g4Var36;
                        g4Var10 = g4Var37;
                        g4Var9 = g4Var38;
                        c617 = c614;
                        bool4 = bool11;
                        i9 = i10 + 1;
                        str12 = str;
                        c61Arr2 = c61Arr;
                        hashSet4 = hashSet;
                        length3 = i2;
                        g4Var39 = g4Var13;
                        str14 = str4;
                        g4Var33 = g4Var12;
                        g4Var36 = g4Var11;
                        g4Var37 = g4Var10;
                        str13 = str3;
                        g4Var38 = g4Var9;
                        bool13 = bool4;
                        g4Var29 = g4Var8;
                        i61[] i61Arr922 = i61Arr;
                    } else {
                        c61 = c617;
                        g4Var14 = g4Var39;
                        g4Var12 = g4Var33;
                        bool5 = bool13;
                        i2 = i11;
                        str4 = str16;
                        if (z7) {
                            m();
                            long j5 = 0L;
                            Object b2 = jl1.b(c618, "_epc");
                            if (b2 != null) {
                                j5 = b2;
                            }
                            long longValue = ((Long) j5).longValue();
                            if (longValue <= 0) {
                                d().v().a("Complex event with zero extra param count. eventName", str15);
                                l2 = l4;
                            } else {
                                l2 = l4;
                                o().a(str, l4, longValue, c618);
                            }
                            l = l2;
                            str5 = str15;
                            d61Arr = d61Arr2;
                            c61 = c618;
                            j = longValue;
                            b = o().b(str12, c618.d);
                            if (b != null) {
                            }
                            o().a(dg1);
                            long j422 = dg1.c;
                            g4 g4Var4022 = g4Var14;
                            map7 = (Map) g4Var4022.get(str5);
                            if (map7 == null) {
                            }
                            Map map1522 = map7;
                            it3 = map1522.keySet().iterator();
                            while (it3.hasNext()) {
                            }
                            g4Var13 = g4Var4022;
                            bool4 = bool5;
                            hashSet = hashSet2;
                            g4Var8 = g4Var15;
                            l3 = l;
                            c617 = c61;
                            j3 = j;
                            i9 = i10 + 1;
                            str12 = str;
                            c61Arr2 = c61Arr;
                            hashSet4 = hashSet;
                            length3 = i2;
                            g4Var39 = g4Var13;
                            str14 = str4;
                            g4Var33 = g4Var12;
                            g4Var36 = g4Var11;
                            g4Var37 = g4Var10;
                            str13 = str3;
                            g4Var38 = g4Var9;
                            bool13 = bool4;
                            g4Var29 = g4Var8;
                            i61[] i61Arr9222 = i61Arr;
                        }
                    }
                } else {
                    c61 = c617;
                    str4 = str14;
                    g4Var14 = g4Var39;
                    g4Var12 = g4Var33;
                    bool5 = bool13;
                    i2 = i11;
                }
                l = l3;
                str5 = str15;
                d61Arr = d61Arr2;
                j = j3;
                b = o().b(str12, c618.d);
                if (b != null) {
                }
                o().a(dg1);
                long j4222 = dg1.c;
                g4 g4Var40222 = g4Var14;
                map7 = (Map) g4Var40222.get(str5);
                if (map7 == null) {
                }
                Map map15222 = map7;
                it3 = map15222.keySet().iterator();
                while (it3.hasNext()) {
                }
                g4Var13 = g4Var40222;
                bool4 = bool5;
                hashSet = hashSet2;
                g4Var8 = g4Var15;
                l3 = l;
                c617 = c61;
                j3 = j;
                i9 = i10 + 1;
                str12 = str;
                c61Arr2 = c61Arr;
                hashSet4 = hashSet;
                length3 = i2;
                g4Var39 = g4Var13;
                str14 = str4;
                g4Var33 = g4Var12;
                g4Var36 = g4Var11;
                g4Var37 = g4Var10;
                str13 = str3;
                g4Var38 = g4Var9;
                bool13 = bool4;
                g4Var29 = g4Var8;
                i61[] i61Arr92222 = i61Arr;
            }
        }
        String str25 = str14;
        g4 g4Var47 = g4Var33;
        String str26 = str13;
        HashSet hashSet6 = hashSet4;
        g4 g4Var48 = g4Var29;
        g4 g4Var49 = g4Var36;
        Boolean bool18 = bool13;
        g4 g4Var50 = g4Var37;
        g4 g4Var51 = g4Var38;
        i61[] i61Arr10 = i61Arr;
        if (i61Arr10 != null) {
            g4 g4Var52 = new g4();
            int length5 = i61Arr10.length;
            int i13 = 0;
            while (i13 < length5) {
                i61 i61 = i61Arr10[i13];
                Map map19 = (Map) g4Var52.get(i61.d);
                if (map19 == null) {
                    map19 = o().h(str, i61.d);
                    if (map19 == null) {
                        map19 = new g4();
                    }
                    g4Var52.put(i61.d, map19);
                } else {
                    String str27 = str;
                }
                Iterator it7 = map19.keySet().iterator();
                while (it7.hasNext()) {
                    int intValue3 = ((Integer) it7.next()).intValue();
                    if (hashSet6.contains(Integer.valueOf(intValue3))) {
                        d().A().a(str25, Integer.valueOf(intValue3));
                    } else {
                        String str28 = str25;
                        g4 g4Var53 = g4Var48;
                        a61 a614 = (a61) g4Var53.get(Integer.valueOf(intValue3));
                        g4 g4Var54 = g4Var51;
                        BitSet bitSet14 = (BitSet) g4Var54.get(Integer.valueOf(intValue3));
                        g4 g4Var55 = g4Var50;
                        BitSet bitSet15 = (BitSet) g4Var55.get(Integer.valueOf(intValue3));
                        g4 g4Var56 = g4Var52;
                        if (j2) {
                            i = length5;
                            g4Var5 = g4Var49;
                            map = (Map) g4Var5.get(Integer.valueOf(intValue3));
                            it2 = it7;
                            g4Var4 = g4Var47;
                            map2 = (Map) g4Var4.get(Integer.valueOf(intValue3));
                        } else {
                            i = length5;
                            it2 = it7;
                            g4Var4 = g4Var47;
                            g4Var5 = g4Var49;
                            map2 = null;
                            map = null;
                        }
                        if (a614 == null) {
                            a61 a615 = new a61();
                            g4Var53.put(Integer.valueOf(intValue3), a615);
                            Boolean bool19 = bool18;
                            a615.f = bool19;
                            BitSet bitSet16 = new BitSet();
                            g4Var54.put(Integer.valueOf(intValue3), bitSet16);
                            bitSet15 = new BitSet();
                            Map map20 = map2;
                            g4Var55.put(Integer.valueOf(intValue3), bitSet15);
                            if (j2) {
                                Map g4Var57 = new g4();
                                bitSet = bitSet16;
                                g4Var5.put(Integer.valueOf(intValue3), g4Var57);
                                Map g4Var58 = new g4();
                                g4Var4.put(Integer.valueOf(intValue3), g4Var58);
                                g4Var49 = g4Var5;
                                map3 = g4Var58;
                                bool = bool19;
                                map4 = g4Var57;
                            } else {
                                bitSet = bitSet16;
                                g4Var49 = g4Var5;
                                bool = bool19;
                                map4 = map;
                                map3 = map20;
                            }
                            bitSet14 = bitSet;
                        } else {
                            Map map21 = map2;
                            g4Var49 = g4Var5;
                            map4 = map;
                            map3 = map21;
                            bool = bool18;
                        }
                        Iterator it8 = ((List) map19.get(Integer.valueOf(intValue3))).iterator();
                        while (true) {
                            if (!it8.hasNext()) {
                                g4Var47 = g4Var4;
                                String str29 = str;
                                g4Var52 = g4Var56;
                                g4Var50 = g4Var55;
                                str25 = str28;
                                g4Var48 = g4Var53;
                                g4Var51 = g4Var54;
                                length5 = i;
                                it7 = it2;
                                bool18 = bool;
                                i61[] i61Arr11 = i61Arr;
                                break;
                            }
                            map5 = map19;
                            v51 = (v51) it8.next();
                            Iterator it9 = it8;
                            str25 = str28;
                            if (d().a(2)) {
                                g4Var47 = g4Var4;
                                g4Var50 = g4Var55;
                                d().A().a("Evaluating filter. audience, filter, property", Integer.valueOf(intValue3), v51.c, i().c(v51.d));
                                str2 = str26;
                                d().A().a(str2, m().a(v51));
                            } else {
                                g4Var50 = g4Var55;
                                g4Var47 = g4Var4;
                                str2 = str26;
                            }
                            Integer num3 = v51.c;
                            if (num3 == null || num3.intValue() > 256) {
                                str26 = str2;
                                d().v().a("Invalid property filter ID. appId, id", tg1.a(str), String.valueOf(v51.c));
                                hashSet6.add(Integer.valueOf(intValue3));
                                String str30 = str;
                                g4Var52 = g4Var56;
                                i61[] i61Arr12 = i61Arr;
                                g4Var51 = g4Var54;
                                length5 = i;
                                it7 = it2;
                                map19 = map5;
                                bool18 = bool;
                                g4Var48 = g4Var53;
                            } else {
                                if (j2) {
                                    if (v51 != null) {
                                        Boolean bool20 = v51.f;
                                        if (bool20 != null && bool20.booleanValue()) {
                                            z = true;
                                            if (v51 != null) {
                                                Boolean bool21 = v51.g;
                                                if (bool21 != null && bool21.booleanValue()) {
                                                    z2 = true;
                                                    if (bitSet14.get(v51.c.intValue()) || z || z2) {
                                                        str26 = str2;
                                                        a = a(v51, i61);
                                                        g4Var49 = g4Var49;
                                                        vg1 A4 = d().A();
                                                        g4Var6 = g4Var53;
                                                        if (a == null) {
                                                            bool3 = "null";
                                                        } else {
                                                            bool3 = a;
                                                        }
                                                        A4.a("Property filter result", bool3);
                                                        if (a == null) {
                                                            hashSet6.add(Integer.valueOf(intValue3));
                                                        } else {
                                                            bitSet15.set(v51.c.intValue());
                                                            bitSet14.set(v51.c.intValue(), a.booleanValue());
                                                            if (a.booleanValue() && ((z || z2) && i61.c != null)) {
                                                                if (z2) {
                                                                    b(map3, v51.c.intValue(), i61.c.longValue());
                                                                } else {
                                                                    a((Map<Integer, Long>) map4, v51.c.intValue(), i61.c.longValue());
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        d().A().a("Property filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue3), v51.c);
                                                        str26 = str2;
                                                        it8 = it9;
                                                        map6 = map5;
                                                        str28 = str25;
                                                        g4Var7 = g4Var47;
                                                        g4Var55 = g4Var50;
                                                        String str31 = str;
                                                    }
                                                }
                                            }
                                            z2 = false;
                                            if (bitSet14.get(v51.c.intValue())) {
                                            }
                                            str26 = str2;
                                            a = a(v51, i61);
                                            g4Var49 = g4Var49;
                                            vg1 A42 = d().A();
                                            g4Var6 = g4Var53;
                                            if (a == null) {
                                            }
                                            A42.a("Property filter result", bool3);
                                            if (a == null) {
                                            }
                                        }
                                    }
                                    z = false;
                                    if (v51 != null) {
                                    }
                                    z2 = false;
                                    if (bitSet14.get(v51.c.intValue())) {
                                    }
                                    str26 = str2;
                                    a = a(v51, i61);
                                    g4Var49 = g4Var49;
                                    vg1 A422 = d().A();
                                    g4Var6 = g4Var53;
                                    if (a == null) {
                                    }
                                    A422.a("Property filter result", bool3);
                                    if (a == null) {
                                    }
                                } else {
                                    str26 = str2;
                                    g4Var6 = g4Var53;
                                    if (bitSet14.get(v51.c.intValue())) {
                                        d().A().a("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue3), v51.c);
                                    } else {
                                        Boolean a5 = a(v51, i61);
                                        vg1 A5 = d().A();
                                        if (a5 == null) {
                                            bool2 = "null";
                                        } else {
                                            bool2 = a5;
                                        }
                                        A5.a("Property filter result", bool2);
                                        if (a5 == null) {
                                            hashSet6.add(Integer.valueOf(intValue3));
                                        } else {
                                            bitSet15.set(v51.c.intValue());
                                            if (a5.booleanValue()) {
                                                bitSet14.set(v51.c.intValue());
                                            }
                                        }
                                    }
                                }
                                String str32 = str;
                                it8 = it9;
                                map6 = map5;
                                str28 = str25;
                                g4Var7 = g4Var47;
                                g4Var55 = g4Var50;
                                g4Var53 = g4Var6;
                            }
                        }
                        str26 = str2;
                        d().v().a("Invalid property filter ID. appId, id", tg1.a(str), String.valueOf(v51.c));
                        hashSet6.add(Integer.valueOf(intValue3));
                        String str302 = str;
                        g4Var52 = g4Var56;
                        i61[] i61Arr122 = i61Arr;
                        g4Var51 = g4Var54;
                        length5 = i;
                        it7 = it2;
                        map19 = map5;
                        bool18 = bool;
                        g4Var48 = g4Var53;
                    }
                }
                g4 g4Var59 = g4Var52;
                int i14 = length5;
                g4 g4Var60 = g4Var51;
                Boolean bool22 = bool18;
                g4 g4Var61 = g4Var48;
                i13++;
                i61Arr10 = i61Arr;
            }
        }
        g4 g4Var62 = g4Var51;
        g4 g4Var63 = g4Var48;
        a61[] a61Arr = new a61[g4Var62.size()];
        Iterator it10 = g4Var62.keySet().iterator();
        int i15 = 0;
        while (it10.hasNext()) {
            int intValue4 = ((Integer) it10.next()).intValue();
            if (!hashSet6.contains(Integer.valueOf(intValue4))) {
                g4 g4Var64 = g4Var63;
                a61 a616 = (a61) g4Var64.get(Integer.valueOf(intValue4));
                if (a616 == null) {
                    a616 = new a61();
                }
                int i16 = i15 + 1;
                a61Arr[i15] = a616;
                a616.c = Integer.valueOf(intValue4);
                a616.d = new g61();
                a616.d.d = jl1.a((BitSet) g4Var62.get(Integer.valueOf(intValue4)));
                g4 g4Var65 = g4Var50;
                a616.d.c = jl1.a((BitSet) g4Var65.get(Integer.valueOf(intValue4)));
                if (j2) {
                    g4Var2 = g4Var49;
                    a616.d.e = a((Map) g4Var2.get(Integer.valueOf(intValue4)));
                    g61 g612 = a616.d;
                    g4Var = g4Var47;
                    Map map22 = (Map) g4Var.get(Integer.valueOf(intValue4));
                    if (map22 == null) {
                        it = it10;
                        g4Var63 = g4Var64;
                        h61Arr = new h61[0];
                    } else {
                        h61Arr = new h61[map22.size()];
                        int i17 = 0;
                        for (Integer num4 : map22.keySet()) {
                            Iterator it11 = it10;
                            h61 h61 = new h61();
                            h61.c = num4;
                            List<Long> list = (List) map22.get(num4);
                            if (list != null) {
                                Collections.sort(list);
                                g4Var3 = g4Var64;
                                long[] jArr2 = new long[list.size()];
                                int i18 = 0;
                                for (Long longValue2 : list) {
                                    jArr2[i18] = longValue2.longValue();
                                    i18++;
                                }
                                h61.d = jArr2;
                            } else {
                                g4Var3 = g4Var64;
                            }
                            h61Arr[i17] = h61;
                            it10 = it11;
                            i17++;
                            g4Var64 = g4Var3;
                        }
                        it = it10;
                        g4Var63 = g4Var64;
                    }
                    g612.f = h61Arr;
                } else {
                    it = it10;
                    g4Var63 = g4Var64;
                    g4Var = g4Var47;
                    g4Var2 = g4Var49;
                }
                am1 o2 = o();
                g61 g613 = a616.d;
                o2.q();
                o2.e();
                bk0.b(str);
                bk0.a(g613);
                try {
                    byte[] bArr = new byte[g613.b()];
                    try {
                        ub1 a6 = ub1.a(bArr, 0, bArr.length);
                        g613.a(a6);
                        a6.b();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("app_id", str);
                        contentValues.put("audience_id", Integer.valueOf(intValue4));
                        contentValues.put("current_results", bArr);
                        try {
                            try {
                                if (o2.v().insertWithOnConflict("audience_filter_values", (String) null, contentValues, 5) == -1) {
                                    o2.d().s().a("Failed to insert filter results (got -1). appId", tg1.a(str));
                                }
                            } catch (SQLiteException e5) {
                                e = e5;
                                o2.d().s().a("Error storing filter results. appId", tg1.a(str), e);
                                it10 = it;
                                i15 = i16;
                                g4Var50 = g4Var65;
                                g4Var49 = g4Var2;
                                g4Var47 = g4Var;
                            }
                        } catch (SQLiteException e6) {
                            e = e6;
                            o2.d().s().a("Error storing filter results. appId", tg1.a(str), e);
                            it10 = it;
                            i15 = i16;
                            g4Var50 = g4Var65;
                            g4Var49 = g4Var2;
                            g4Var47 = g4Var;
                        }
                    } catch (IOException e7) {
                        e = e7;
                        String str33 = str;
                        o2.d().s().a("Configuration loss. Failed to serialize filter results. appId", tg1.a(str), e);
                        it10 = it;
                        i15 = i16;
                        g4Var50 = g4Var65;
                        g4Var49 = g4Var2;
                        g4Var47 = g4Var;
                    }
                } catch (IOException e8) {
                    e = e8;
                    String str34 = str;
                    o2.d().s().a("Configuration loss. Failed to serialize filter results. appId", tg1.a(str), e);
                    it10 = it;
                    i15 = i16;
                    g4Var50 = g4Var65;
                    g4Var49 = g4Var2;
                    g4Var47 = g4Var;
                }
                it10 = it;
                i15 = i16;
                g4Var50 = g4Var65;
                g4Var49 = g4Var2;
                g4Var47 = g4Var;
            } else {
                String str35 = str;
            }
        }
        return (a61[]) Arrays.copyOf(a61Arr, i15);
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final Boolean a(s51 s51, String str, d61[] d61Arr, long j) {
        Boolean bool;
        u51 u51 = s51.g;
        if (u51 != null) {
            Boolean a = a(j, u51);
            if (a == null) {
                return null;
            }
            if (!a.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (t51 t51 : s51.e) {
            if (TextUtils.isEmpty(t51.f)) {
                d().v().a("null or empty param name in filter. event", i().a(str));
                return null;
            }
            hashSet.add(t51.f);
        }
        g4 g4Var = new g4();
        for (d61 d61 : d61Arr) {
            if (hashSet.contains(d61.c)) {
                Long l = d61.e;
                if (l != null) {
                    g4Var.put(d61.c, l);
                } else {
                    Double d = d61.g;
                    if (d != null) {
                        g4Var.put(d61.c, d);
                    } else {
                        String str2 = d61.d;
                        if (str2 != null) {
                            g4Var.put(d61.c, str2);
                        } else {
                            d().v().a("Unknown value for param. event, param", i().a(str), i().b(d61.c));
                            return null;
                        }
                    }
                }
            }
        }
        for (t51 t512 : s51.e) {
            boolean equals = Boolean.TRUE.equals(t512.e);
            String str3 = t512.f;
            if (TextUtils.isEmpty(str3)) {
                d().v().a("Event has empty param name. event", i().a(str));
                return null;
            }
            Object obj = g4Var.get(str3);
            if (obj instanceof Long) {
                if (t512.d == null) {
                    d().v().a("No number filter for long param. event, param", i().a(str), i().b(str3));
                    return null;
                }
                Boolean a2 = a(((Long) obj).longValue(), t512.d);
                if (a2 == null) {
                    return null;
                }
                if ((true ^ a2.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof Double) {
                if (t512.d == null) {
                    d().v().a("No number filter for double param. event, param", i().a(str), i().b(str3));
                    return null;
                }
                Boolean a3 = a(((Double) obj).doubleValue(), t512.d);
                if (a3 == null) {
                    return null;
                }
                if ((true ^ a3.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj instanceof String) {
                w51 w51 = t512.c;
                if (w51 != null) {
                    bool = a((String) obj, w51);
                } else if (t512.d != null) {
                    String str4 = (String) obj;
                    if (jl1.a(str4)) {
                        bool = a(str4, t512.d);
                    } else {
                        d().v().a("Invalid param value for number filter. event, param", i().a(str), i().b(str3));
                        return null;
                    }
                } else {
                    d().v().a("No filter for String param. event, param", i().a(str), i().b(str3));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if ((true ^ bool.booleanValue()) ^ equals) {
                    return false;
                }
            } else if (obj == null) {
                d().A().a("Missing param for filter. event, param", i().a(str), i().b(str3));
                return false;
            } else {
                d().v().a("Unknown param type. event, param", i().a(str), i().b(str3));
                return null;
            }
        }
        return true;
    }

    @DexIgnore
    public final Boolean a(v51 v51, i61 i61) {
        t51 t51 = v51.e;
        if (t51 == null) {
            d().v().a("Missing property filter. property", i().c(i61.d));
            return null;
        }
        boolean equals = Boolean.TRUE.equals(t51.e);
        Long l = i61.f;
        if (l == null) {
            Double d = i61.h;
            if (d == null) {
                String str = i61.e;
                if (str != null) {
                    w51 w51 = t51.c;
                    if (w51 != null) {
                        return a(a(str, w51), equals);
                    }
                    if (t51.d == null) {
                        d().v().a("No string or number filter defined. property", i().c(i61.d));
                    } else if (jl1.a(str)) {
                        return a(a(i61.e, t51.d), equals);
                    } else {
                        d().v().a("Invalid user property value for Numeric number filter. property, value", i().c(i61.d), i61.e);
                    }
                    return null;
                }
                d().v().a("User property has no value, property", i().c(i61.d));
                return null;
            } else if (t51.d != null) {
                return a(a(d.doubleValue(), t51.d), equals);
            } else {
                d().v().a("No number filter for double property. property", i().c(i61.d));
                return null;
            }
        } else if (t51.d != null) {
            return a(a(l.longValue(), t51.d), equals);
        } else {
            d().v().a("No number filter for long property. property", i().c(i61.d));
            return null;
        }
    }

    @DexIgnore
    public static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    @DexIgnore
    public final Boolean a(String str, w51 w51) {
        String str2;
        ArrayList arrayList;
        bk0.a(w51);
        if (str == null) {
            return null;
        }
        Integer num = w51.c;
        if (num == null || num.intValue() == 0) {
            return null;
        }
        if (w51.c.intValue() == 6) {
            String[] strArr = w51.f;
            if (strArr == null || strArr.length == 0) {
                return null;
            }
        } else if (w51.d == null) {
            return null;
        }
        int intValue = w51.c.intValue();
        Boolean bool = w51.e;
        boolean z = bool != null && bool.booleanValue();
        if (z || intValue == 1 || intValue == 6) {
            str2 = w51.d;
        } else {
            str2 = w51.d.toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        String[] strArr2 = w51.f;
        if (strArr2 == null) {
            arrayList = null;
        } else if (z) {
            arrayList = Arrays.asList(strArr2);
        } else {
            ArrayList arrayList2 = new ArrayList();
            for (String upperCase : strArr2) {
                arrayList2.add(upperCase.toUpperCase(Locale.ENGLISH));
            }
            arrayList = arrayList2;
        }
        return a(str, intValue, z, str3, arrayList, intValue == 1 ? str3 : null);
    }

    @DexIgnore
    public final Boolean a(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    d().v().a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    @DexIgnore
    public final Boolean a(long j, u51 u51) {
        try {
            return a(new BigDecimal(j), u51, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public final Boolean a(double d, u51 u51) {
        try {
            return a(new BigDecimal(d), u51, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public final Boolean a(String str, u51 u51) {
        if (!jl1.a(str)) {
            return null;
        }
        try {
            return a(new BigDecimal(str), u51, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x006f, code lost:
        if (r3 != null) goto L_0x0071;
     */
    @DexIgnore
    public static Boolean a(BigDecimal bigDecimal, u51 u51, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        bk0.a(u51);
        Integer num = u51.c;
        if (!(num == null || num.intValue() == 0)) {
            if (u51.c.intValue() == 4) {
                if (u51.f == null || u51.g == null) {
                    return null;
                }
            } else if (u51.e == null) {
                return null;
            }
            int intValue = u51.c.intValue();
            if (u51.c.intValue() == 4) {
                if (jl1.a(u51.f) && jl1.a(u51.g)) {
                    try {
                        BigDecimal bigDecimal5 = new BigDecimal(u51.f);
                        bigDecimal3 = new BigDecimal(u51.g);
                        bigDecimal2 = bigDecimal5;
                        bigDecimal4 = null;
                    } catch (NumberFormatException unused) {
                    }
                }
                return null;
            } else if (!jl1.a(u51.e)) {
                return null;
            } else {
                try {
                    bigDecimal4 = new BigDecimal(u51.e);
                    bigDecimal2 = null;
                    bigDecimal3 = null;
                } catch (NumberFormatException unused2) {
                }
            }
            if (intValue == 4) {
                if (bigDecimal2 == null) {
                    return null;
                }
            }
            boolean z = false;
            if (intValue == 1) {
                if (bigDecimal.compareTo(bigDecimal4) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else if (intValue == 2) {
                if (bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else if (intValue != 3) {
                if (intValue == 4) {
                    if (!(bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal3) == 1)) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
            } else if (d != 0.0d) {
                if (bigDecimal.compareTo(bigDecimal4.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal4.add(new BigDecimal(d).multiply(new BigDecimal(2)))) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else {
                if (bigDecimal.compareTo(bigDecimal4) == 0) {
                    z = true;
                }
                return Boolean.valueOf(z);
            }
        }
        return null;
    }

    @DexIgnore
    public static b61[] a(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        int i = 0;
        b61[] b61Arr = new b61[map.size()];
        for (Integer next : map.keySet()) {
            b61 b61 = new b61();
            b61.c = next;
            b61.d = map.get(next);
            b61Arr[i] = b61;
            i++;
        }
        return b61Arr;
    }

    @DexIgnore
    public static void a(Map<Integer, Long> map, int i, long j) {
        Long l = map.get(Integer.valueOf(i));
        long j2 = j / 1000;
        if (l == null || j2 > l.longValue()) {
            map.put(Integer.valueOf(i), Long.valueOf(j2));
        }
    }
}
