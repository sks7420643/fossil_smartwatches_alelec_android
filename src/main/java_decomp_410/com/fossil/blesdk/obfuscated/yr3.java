package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yr3 implements Factory<xr3> {
    @DexIgnore
    public static xr3 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, h42 h42, ContentResolver contentResolver, hr2 hr2, en2 en2) {
        return new xr3(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, h42, contentResolver, hr2, en2);
    }
}
