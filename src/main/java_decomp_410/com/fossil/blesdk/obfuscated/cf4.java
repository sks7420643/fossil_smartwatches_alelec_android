package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cf4 implements re4<yd4> {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ yc4<CharSequence, Integer, Pair<Integer, Integer>> d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<yd4>, rd4 {
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public int f;
        @DexIgnore
        public int g;
        @DexIgnore
        public yd4 h;
        @DexIgnore
        public int i;
        @DexIgnore
        public /* final */ /* synthetic */ cf4 j;

        @DexIgnore
        public a(cf4 cf4) {
            this.j = cf4;
            this.f = ee4.a(cf4.b, 0, cf4.a.length());
            this.g = this.f;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0023, code lost:
            if (r6.i < com.fossil.blesdk.obfuscated.cf4.c(r6.j)) goto L_0x0025;
         */
        @DexIgnore
        public final void a() {
            int i2 = 0;
            if (this.g < 0) {
                this.e = 0;
                this.h = null;
                return;
            }
            if (this.j.c > 0) {
                this.i++;
            }
            if (this.g <= this.j.a.length()) {
                Pair pair = (Pair) this.j.d.invoke(this.j.a, Integer.valueOf(this.g));
                if (pair == null) {
                    this.h = new yd4(this.f, StringsKt__StringsKt.c(this.j.a));
                    this.g = -1;
                } else {
                    int intValue = ((Number) pair.component1()).intValue();
                    int intValue2 = ((Number) pair.component2()).intValue();
                    this.h = ee4.d(this.f, intValue);
                    this.f = intValue + intValue2;
                    int i3 = this.f;
                    if (intValue2 == 0) {
                        i2 = 1;
                    }
                    this.g = i3 + i2;
                }
                this.e = 1;
            }
            this.h = new yd4(this.f, StringsKt__StringsKt.c(this.j.a));
            this.g = -1;
            this.e = 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.e == -1) {
                a();
            }
            return this.e == 1;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public yd4 next() {
            if (this.e == -1) {
                a();
            }
            if (this.e != 0) {
                yd4 yd4 = this.h;
                if (yd4 != null) {
                    this.h = null;
                    this.e = -1;
                    return yd4;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public cf4(CharSequence charSequence, int i, int i2, yc4<? super CharSequence, ? super Integer, Pair<Integer, Integer>> yc4) {
        kd4.b(charSequence, "input");
        kd4.b(yc4, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = yc4;
    }

    @DexIgnore
    public Iterator<yd4> iterator() {
        return new a(this);
    }
}
