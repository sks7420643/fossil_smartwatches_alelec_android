package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cv3;
import com.fossil.blesdk.obfuscated.hv3;
import java.io.IOException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xw3 {
    @DexIgnore
    public static /* final */ Comparator<String> a; // = new a();
    @DexIgnore
    public static /* final */ String b; // = uv3.c().a();
    @DexIgnore
    public static /* final */ String c; // = (b + "-Sent-Millis");
    @DexIgnore
    public static /* final */ String d; // = (b + "-Received-Millis");
    @DexIgnore
    public static /* final */ String e; // = (b + "-Selected-Protocol");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<String> {
        @DexIgnore
        /* renamed from: a */
        public int compare(String str, String str2) {
            if (str == str2) {
                return 0;
            }
            if (str == null) {
                return -1;
            }
            if (str2 == null) {
                return 1;
            }
            return String.CASE_INSENSITIVE_ORDER.compare(str, str2);
        }
    }

    @DexIgnore
    public static long a(hv3 hv3) {
        return a(hv3.c());
    }

    @DexIgnore
    public static long b(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static Set<String> c(jv3 jv3) {
        return c(jv3.g());
    }

    @DexIgnore
    public static cv3 d(jv3 jv3) {
        return a(jv3.i().l().c(), jv3.g());
    }

    @DexIgnore
    public static long a(jv3 jv3) {
        return a(jv3.g());
    }

    @DexIgnore
    public static Map<String, List<String>> b(cv3 cv3, String str) {
        TreeMap treeMap = new TreeMap(a);
        int b2 = cv3.b();
        for (int i = 0; i < b2; i++) {
            String a2 = cv3.a(i);
            String b3 = cv3.b(i);
            ArrayList arrayList = new ArrayList();
            List list = (List) treeMap.get(a2);
            if (list != null) {
                arrayList.addAll(list);
            }
            arrayList.add(b3);
            treeMap.put(a2, Collections.unmodifiableList(arrayList));
        }
        if (str != null) {
            treeMap.put((Object) null, Collections.unmodifiableList(Collections.singletonList(str)));
        }
        return Collections.unmodifiableMap(treeMap);
    }

    @DexIgnore
    public static Set<String> c(cv3 cv3) {
        Set<String> emptySet = Collections.emptySet();
        int b2 = cv3.b();
        Set<String> set = emptySet;
        for (int i = 0; i < b2; i++) {
            if ("Vary".equalsIgnoreCase(cv3.a(i))) {
                String b3 = cv3.b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b3.split(",")) {
                    set.add(trim.trim());
                }
            }
        }
        return set;
    }

    @DexIgnore
    public static long a(cv3 cv3) {
        return b(cv3.a("Content-Length"));
    }

    @DexIgnore
    public static void a(hv3.b bVar, Map<String, List<String>> map) {
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            if (("Cookie".equalsIgnoreCase(str) || "Cookie2".equalsIgnoreCase(str)) && !((List) next.getValue()).isEmpty()) {
                bVar.a(str, a((List<String>) (List) next.getValue()));
            }
        }
    }

    @DexIgnore
    public static String a(List<String> list) {
        if (list.size() == 1) {
            return list.get(0);
        }
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            sb.append(list.get(i));
        }
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(jv3 jv3) {
        return b(jv3.g());
    }

    @DexIgnore
    public static boolean b(cv3 cv3) {
        return c(cv3).contains(Marker.ANY_MARKER);
    }

    @DexIgnore
    public static boolean a(jv3 jv3, cv3 cv3, hv3 hv3) {
        for (String next : c(jv3)) {
            if (!wv3.a((Object) cv3.c(next), (Object) hv3.b(next))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static cv3 a(cv3 cv3, cv3 cv32) {
        Set<String> c2 = c(cv32);
        if (c2.isEmpty()) {
            return new cv3.b().a();
        }
        cv3.b bVar = new cv3.b();
        int b2 = cv3.b();
        for (int i = 0; i < b2; i++) {
            String a2 = cv3.a(i);
            if (c2.contains(a2)) {
                bVar.a(a2, cv3.b(i));
            }
        }
        return bVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return !"Connection".equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    public static List<vu3> a(cv3 cv3, String str) {
        ArrayList arrayList = new ArrayList();
        int b2 = cv3.b();
        for (int i = 0; i < b2; i++) {
            if (str.equalsIgnoreCase(cv3.a(i))) {
                String b3 = cv3.b(i);
                int i2 = 0;
                while (i2 < b3.length()) {
                    int a2 = rw3.a(b3, i2, " ");
                    String trim = b3.substring(i2, a2).trim();
                    int b4 = rw3.b(b3, a2);
                    if (!b3.regionMatches(true, b4, "realm=\"", 0, 7)) {
                        break;
                    }
                    int i3 = b4 + 7;
                    int a3 = rw3.a(b3, i3, "\"");
                    String substring = b3.substring(i3, a3);
                    i2 = rw3.b(b3, rw3.a(b3, a3 + 1, ",") + 1);
                    arrayList.add(new vu3(trim, substring));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static hv3 a(qu3 qu3, jv3 jv3, Proxy proxy) throws IOException {
        if (jv3.e() == 407) {
            return qu3.a(proxy, jv3);
        }
        return qu3.b(proxy, jv3);
    }
}
