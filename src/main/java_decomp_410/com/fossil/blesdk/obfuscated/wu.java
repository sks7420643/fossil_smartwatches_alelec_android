package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wu {
    @DexIgnore
    public /* final */ Set<ov> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<ov> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(ov ovVar) {
        boolean z = true;
        if (ovVar == null) {
            return true;
        }
        boolean remove = this.a.remove(ovVar);
        if (!this.b.remove(ovVar) && !remove) {
            z = false;
        }
        if (z) {
            ovVar.clear();
        }
        return z;
    }

    @DexIgnore
    public void b(ov ovVar) {
        this.a.add(ovVar);
        if (!this.c) {
            ovVar.c();
            return;
        }
        ovVar.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(ovVar);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (T t : uw.a(this.a)) {
            if (t.isRunning()) {
                t.d();
                this.b.add(t);
            }
        }
    }

    @DexIgnore
    public void d() {
        for (T t : uw.a(this.a)) {
            if (!t.f() && !t.e()) {
                t.clear();
                if (!this.c) {
                    t.c();
                } else {
                    this.b.add(t);
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        this.c = false;
        for (T t : uw.a(this.a)) {
            if (!t.f() && !t.isRunning()) {
                t.c();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }

    @DexIgnore
    public void a() {
        for (T a2 : uw.a(this.a)) {
            a(a2);
        }
        this.b.clear();
    }

    @DexIgnore
    public void b() {
        this.c = true;
        for (T t : uw.a(this.a)) {
            if (t.isRunning() || t.f()) {
                t.clear();
                this.b.add(t);
            }
        }
    }
}
