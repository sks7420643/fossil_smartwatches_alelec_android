package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.Ringtone;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r62 extends RecyclerView.g<c> {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> a; // = new ArrayList<>();
    @DexIgnore
    public Ringtone b;
    @DexIgnore
    public /* final */ b c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Ringtone ringtone);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public View b;
        @DexIgnore
        public /* final */ TextView c;
        @DexIgnore
        public /* final */ /* synthetic */ r62 d;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                r62 r62 = this.e.d;
                Object obj = r62.a.get(this.e.getAdapterPosition());
                kd4.a(obj, "mRingPhones[adapterPosition]");
                r62.b = (Ringtone) obj;
                this.e.d.c.a(r62.c(this.e.d));
                this.e.d.notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(r62 r62, View view) {
            super(view);
            kd4.b(view, "view");
            this.d = r62;
            View findViewById = view.findViewById(R.id.iv_ringphone);
            kd4.a((Object) findViewById, "view.findViewById(R.id.iv_ringphone)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.iv_ringphone_selected);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.iv_ringphone_selected)");
            this.b = findViewById2;
            View findViewById3 = view.findViewById(R.id.tv_ring_phone);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.tv_ring_phone)");
            this.c = (TextView) findViewById3;
            this.a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(Ringtone ringtone) {
            kd4.b(ringtone, "ringTone");
            this.c.setText(ringtone.getRingtoneName());
            this.b.setVisibility(0);
            if (kd4.a((Object) ringtone.getRingtoneName(), (Object) r62.c(this.d).getRingtoneName())) {
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(8);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public r62(b bVar) {
        kd4.b(bVar, "mListener");
        this.c = bVar;
    }

    @DexIgnore
    public static final /* synthetic */ Ringtone c(r62 r62) {
        Ringtone ringtone = r62.b;
        if (ringtone != null) {
            return ringtone;
        }
        kd4.d("mSelectedRingPhone");
        throw null;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_ring_phone, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026ing_phone, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public final void a(List<Ringtone> list, Ringtone ringtone) {
        kd4.b(list, "data");
        kd4.b(ringtone, "selectedRingtone");
        this.a.clear();
        this.a.addAll(list);
        this.b = ringtone;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        kd4.b(cVar, "holder");
        int adapterPosition = cVar.getAdapterPosition();
        if (getItemCount() > adapterPosition && adapterPosition != -1) {
            Ringtone ringtone = this.a.get(adapterPosition);
            kd4.a((Object) ringtone, "mRingPhones[adapterPos]");
            cVar.a(ringtone);
        }
    }
}
