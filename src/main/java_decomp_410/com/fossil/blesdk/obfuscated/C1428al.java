package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.al */
public final class C1428al implements com.fossil.blesdk.obfuscated.C3443zk {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.room.RoomDatabase f3519a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf f3520b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.al$a")
    /* renamed from: com.fossil.blesdk.obfuscated.al$a */
    public class C1429a extends com.fossil.blesdk.obfuscated.C2322lf<com.fossil.blesdk.obfuscated.C3368yk> {
        @DexIgnore
        public C1429a(com.fossil.blesdk.obfuscated.C1428al alVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.fossil.blesdk.obfuscated.C3368yk ykVar) {
            java.lang.String str = ykVar.f11286a;
            if (str == null) {
                kgVar.mo11930a(1);
            } else {
                kgVar.mo11932a(1, str);
            }
            java.lang.String str2 = ykVar.f11287b;
            if (str2 == null) {
                kgVar.mo11930a(2);
            } else {
                kgVar.mo11932a(2, str2);
            }
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency`(`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public C1428al(androidx.room.RoomDatabase roomDatabase) {
        this.f3519a = roomDatabase;
        this.f3520b = new com.fossil.blesdk.obfuscated.C1428al.C1429a(this, roomDatabase);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8780a(com.fossil.blesdk.obfuscated.C3368yk ykVar) {
        this.f3519a.assertNotSuspendingTransaction();
        this.f3519a.beginTransaction();
        try {
            this.f3520b.insert(ykVar);
            this.f3519a.setTransactionSuccessful();
        } finally {
            this.f3519a.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo8781b(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f3519a.assertNotSuspendingTransaction();
        boolean z = false;
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f3519a, b, false);
        try {
            if (a.moveToFirst() && a.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo8782c(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f3519a.assertNotSuspendingTransaction();
        boolean z = false;
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f3519a, b, false);
        try {
            if (a.moveToFirst() && a.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            a.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<java.lang.String> mo8779a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f3519a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f3519a, b, false);
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }
}
