package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qh0 implements sh0 {
    @DexIgnore
    public /* final */ /* synthetic */ ph0 a;

    @DexIgnore
    public qh0(ph0 ph0) {
        this.a = ph0;
    }

    @DexIgnore
    public final void a(BasePendingResult<?> basePendingResult) {
        this.a.a.remove(basePendingResult);
    }
}
