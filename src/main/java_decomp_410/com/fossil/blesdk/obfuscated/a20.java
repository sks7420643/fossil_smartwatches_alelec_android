package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a20 extends GattOperationResult {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a20(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        this.b = characteristicId;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId b() {
        return this.b;
    }
}
