package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jg */
public interface C2125jg {
    @DexIgnore
    /* renamed from: a */
    int mo10918a();

    @DexIgnore
    /* renamed from: a */
    void mo10919a(com.fossil.blesdk.obfuscated.C2019ig igVar);

    @DexIgnore
    /* renamed from: b */
    java.lang.String mo10920b();
}
