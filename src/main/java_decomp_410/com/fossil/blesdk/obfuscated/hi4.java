package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hi4 extends li4 implements mg4 {
    @DexIgnore
    public /* final */ boolean f; // = j();

    @DexIgnore
    public hi4(fi4 fi4) {
        super(true);
        a(fi4);
    }

    @DexIgnore
    public boolean b() {
        return this.f;
    }

    @DexIgnore
    public boolean c() {
        return true;
    }

    @DexIgnore
    public final boolean j() {
        ig4 ig4 = this.parentHandle;
        if (!(ig4 instanceof jg4)) {
            ig4 = null;
        }
        jg4 jg4 = (jg4) ig4;
        if (jg4 != null) {
            li4 li4 = (li4) jg4.h;
            if (li4 != null) {
                while (!li4.b()) {
                    ig4 ig42 = li4.parentHandle;
                    if (!(ig42 instanceof jg4)) {
                        ig42 = null;
                    }
                    jg4 jg42 = (jg4) ig42;
                    if (jg42 != null) {
                        li4 = (li4) jg42.h;
                        if (li4 == null) {
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
}
