package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pz3 extends hz3 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;

    @DexIgnore
    public pz3(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxobject_message_action");
        this.d = bundle.getString("_wxobject_message_ext");
        this.e = bundle.getString("_wxapi_launch_req_lang");
        this.f = bundle.getString("_wxapi_launch_req_country");
    }

    @DexIgnore
    public boolean a() {
        String str;
        String str2 = this.c;
        if (str2 == null || str2.length() <= 2048) {
            String str3 = this.d;
            if (str3 == null || str3.length() <= 2048) {
                return true;
            }
            str = "checkArgs fail, messageExt is too long";
        } else {
            str = "checkArgs fail, messageAction is too long";
        }
        cz3.a("MicroMsg.SDK.LaunchFromWX.Req", str);
        return false;
    }

    @DexIgnore
    public int b() {
        return 6;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putString("_wxobject_message_action", this.c);
        bundle.putString("_wxobject_message_ext", this.d);
        bundle.putString("_wxapi_launch_req_lang", this.e);
        bundle.putString("_wxapi_launch_req_country", this.f);
    }
}
