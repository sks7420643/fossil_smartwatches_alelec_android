package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.fossil.blesdk.obfuscated.ak;
import com.fossil.blesdk.obfuscated.ck;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zj implements hk, mj, ck.b {
    @DexIgnore
    public static /* final */ String n; // = dj.a("DelayMetCommandHandler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ ak h;
    @DexIgnore
    public /* final */ ik i;
    @DexIgnore
    public /* final */ Object j; // = new Object();
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public PowerManager.WakeLock l;
    @DexIgnore
    public boolean m; // = false;

    @DexIgnore
    public zj(Context context, int i2, String str, ak akVar) {
        this.e = context;
        this.f = i2;
        this.h = akVar;
        this.g = str;
        this.i = new ik(this.e, akVar.d(), this);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        dj.a().a(n, String.format("onExecuted %s, %s", new Object[]{str, Boolean.valueOf(z)}), new Throwable[0]);
        a();
        if (z) {
            Intent b = xj.b(this.e, this.g);
            ak akVar = this.h;
            akVar.a((Runnable) new ak.b(akVar, b, this.f));
        }
        if (this.m) {
            Intent a = xj.a(this.e);
            ak akVar2 = this.h;
            akVar2.a((Runnable) new ak.b(akVar2, a, this.f));
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        if (list.contains(this.g)) {
            synchronized (this.j) {
                if (this.k == 0) {
                    this.k = 1;
                    dj.a().a(n, String.format("onAllConstraintsMet for %s", new Object[]{this.g}), new Throwable[0]);
                    if (this.h.c().c(this.g)) {
                        this.h.f().a(this.g, 600000, this);
                    } else {
                        a();
                    }
                } else {
                    dj.a().a(n, String.format("Already started work for %s", new Object[]{this.g}), new Throwable[0]);
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        synchronized (this.j) {
            if (this.k < 2) {
                this.k = 2;
                dj.a().a(n, String.format("Stopping work for WorkSpec %s", new Object[]{this.g}), new Throwable[0]);
                this.h.a((Runnable) new ak.b(this.h, xj.c(this.e, this.g), this.f));
                if (this.h.c().b(this.g)) {
                    dj.a().a(n, String.format("WorkSpec %s needs to be rescheduled", new Object[]{this.g}), new Throwable[0]);
                    this.h.a((Runnable) new ak.b(this.h, xj.b(this.e, this.g), this.f));
                } else {
                    dj.a().a(n, String.format("Processor does not have WorkSpec %s. No need to reschedule ", new Object[]{this.g}), new Throwable[0]);
                }
            } else {
                dj.a().a(n, String.format("Already stopped work for %s", new Object[]{this.g}), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        dj.a().a(n, String.format("Exceeded time limits on execution for %s", new Object[]{str}), new Throwable[0]);
        c();
    }

    @DexIgnore
    public void a(List<String> list) {
        c();
    }

    @DexIgnore
    public void b() {
        this.l = wl.a(this.e, String.format("%s (%s)", new Object[]{this.g, Integer.valueOf(this.f)}));
        dj.a().a(n, String.format("Acquiring wakelock %s for WorkSpec %s", new Object[]{this.l, this.g}), new Throwable[0]);
        this.l.acquire();
        hl e2 = this.h.e().g().d().e(this.g);
        if (e2 == null) {
            c();
            return;
        }
        this.m = e2.b();
        if (!this.m) {
            dj.a().a(n, String.format("No constraints for %s", new Object[]{this.g}), new Throwable[0]);
            b(Collections.singletonList(this.g));
            return;
        }
        this.i.c(Collections.singletonList(e2));
    }

    @DexIgnore
    public final void a() {
        synchronized (this.j) {
            this.i.a();
            this.h.f().a(this.g);
            if (this.l != null && this.l.isHeld()) {
                dj.a().a(n, String.format("Releasing wakelock %s for WorkSpec %s", new Object[]{this.l, this.g}), new Throwable[0]);
                this.l.release();
            }
        }
    }
}
