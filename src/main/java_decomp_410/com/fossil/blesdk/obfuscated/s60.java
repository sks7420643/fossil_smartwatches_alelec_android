package com.fossil.blesdk.obfuscated;

import java.util.UUID;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s60 {
    @DexIgnore
    public static final JSONArray a(UUID[] uuidArr) {
        kd4.b(uuidArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (UUID uuid : uuidArr) {
            jSONArray.put(uuid.toString());
        }
        return jSONArray;
    }
}
