package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yk4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c;
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater d;
    @DexIgnore
    public /* final */ AtomicReferenceArray<tk4> a; // = new AtomicReferenceArray<>(128);
    @DexIgnore
    public volatile int consumerIndex; // = 0;
    @DexIgnore
    public volatile Object lastScheduledTask; // = null;
    @DexIgnore
    public volatile int producerIndex; // = 0;

    /*
    static {
        Class<yk4> cls = yk4.class;
        b = AtomicReferenceFieldUpdater.newUpdater(cls, Object.class, "lastScheduledTask");
        c = AtomicIntegerFieldUpdater.newUpdater(cls, "producerIndex");
        d = AtomicIntegerFieldUpdater.newUpdater(cls, "consumerIndex");
    }
    */

    @DexIgnore
    public final tk4 b() {
        tk4 tk4 = (tk4) b.getAndSet(this, (Object) null);
        if (tk4 != null) {
            return tk4;
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & 127;
            if (((tk4) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                return (tk4) this.a.getAndSet(i2, (Object) null);
            }
        }
    }

    @DexIgnore
    public final int c() {
        return this.lastScheduledTask != null ? a() + 1 : a();
    }

    @DexIgnore
    public final int a() {
        return this.producerIndex - this.consumerIndex;
    }

    @DexIgnore
    public final boolean a(tk4 tk4, pk4 pk4) {
        kd4.b(tk4, "task");
        kd4.b(pk4, "globalQueue");
        tk4 tk42 = (tk4) b.getAndSet(this, tk4);
        if (tk42 != null) {
            return b(tk42, pk4);
        }
        return true;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v2, resolved type: com.fossil.blesdk.obfuscated.tk4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final boolean a(yk4 yk4, pk4 pk4) {
        tk4 tk4;
        yk4 yk42 = yk4;
        pk4 pk42 = pk4;
        kd4.b(yk42, "victim");
        kd4.b(pk42, "globalQueue");
        long a2 = wk4.f.a();
        int a3 = yk4.a();
        if (a3 == 0) {
            return a(a2, yk42, pk42);
        }
        int a4 = ee4.a(a3 / 2, 1);
        int i = 0;
        boolean z = false;
        while (i < a4) {
            while (true) {
                int i2 = yk42.consumerIndex;
                tk4 = null;
                if (i2 - yk42.producerIndex != 0) {
                    int i3 = i2 & 127;
                    tk4 tk42 = (tk4) yk4.a.get(i3);
                    if (tk42 != null) {
                        if (!(a2 - tk42.e >= wk4.a || yk4.a() > wk4.b)) {
                            break;
                        } else if (d.compareAndSet(yk42, i2, i2 + 1)) {
                            tk4 = yk4.a.getAndSet(i3, (Object) null);
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            if (tk4 == null) {
                break;
            }
            a(tk4, pk42);
            i++;
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(tk4 tk4, pk4 pk4) {
        kd4.b(tk4, "task");
        kd4.b(pk4, "globalQueue");
        boolean z = true;
        while (!a(tk4)) {
            b(pk4);
            z = false;
        }
        return z;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.fossil.blesdk.obfuscated.tk4} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void b(pk4 pk4) {
        tk4 tk4;
        int a2 = ee4.a(a() / 2, 1);
        int i = 0;
        while (i < a2) {
            while (true) {
                int i2 = this.consumerIndex;
                tk4 = null;
                if (i2 - this.producerIndex != 0) {
                    int i3 = i2 & 127;
                    if (((tk4) this.a.get(i3)) != null && d.compareAndSet(this, i2, i2 + 1)) {
                        tk4 = this.a.getAndSet(i3, (Object) null);
                        break;
                    }
                } else {
                    break;
                }
            }
            if (tk4 != null) {
                a(pk4, tk4);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(long j, yk4 yk4, pk4 pk4) {
        tk4 tk4 = (tk4) yk4.lastScheduledTask;
        if (tk4 == null || j - tk4.e < wk4.a || !b.compareAndSet(yk4, tk4, (Object) null)) {
            return false;
        }
        a(tk4, pk4);
        return true;
    }

    @DexIgnore
    public final void a(pk4 pk4, tk4 tk4) {
        if (!pk4.a(tk4)) {
            throw new IllegalStateException("GlobalQueue could not be closed yet".toString());
        }
    }

    @DexIgnore
    public final void a(pk4 pk4) {
        tk4 tk4;
        kd4.b(pk4, "globalQueue");
        tk4 tk42 = (tk4) b.getAndSet(this, (Object) null);
        if (tk42 != null) {
            a(pk4, tk42);
        }
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                tk4 = null;
            } else {
                int i2 = i & 127;
                if (((tk4) this.a.get(i2)) != null && d.compareAndSet(this, i, i + 1)) {
                    tk4 = (tk4) this.a.getAndSet(i2, (Object) null);
                }
            }
            if (tk4 != null) {
                a(pk4, tk4);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final boolean a(tk4 tk4) {
        if (a() == 127) {
            return false;
        }
        int i = this.producerIndex & 127;
        if (this.a.get(i) != null) {
            return false;
        }
        this.a.lazySet(i, tk4);
        c.incrementAndGet(this);
        return true;
    }
}
