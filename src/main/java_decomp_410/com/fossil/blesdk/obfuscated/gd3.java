package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.dg3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gd3 extends zr2 implements fd3 {
    @DexIgnore
    public tr3<uf2> j;
    @DexIgnore
    public ed3 k;
    @DexIgnore
    public dg3 l;
    @DexIgnore
    public int m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b e; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public static /* final */ c e; // = new c();

        @DexIgnore
        public final void onClick(View view) {
            SleepDetailActivity.a aVar = SleepDetailActivity.D;
            Date date = new Date();
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ gd3 a;

        @DexIgnore
        public d(gd3 gd3) {
            this.a = gd3;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            kd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            View childAt = recyclerView.getChildAt(0);
            kd4.a((Object) childAt, "it.getChildAt(0)");
            int measuredWidth = childAt.getMeasuredWidth();
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailFragment", "initUI - offset=" + computeHorizontalScrollOffset);
            if (measuredWidth > 0 && computeHorizontalScrollOffset % measuredWidth == 0) {
                int i3 = computeHorizontalScrollOffset / measuredWidth;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("SleepDetailFragment", "initUI - position=" + i3);
                if (i3 != -1) {
                    this.a.m = i3;
                }
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewDayFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onCreateView");
        uf2 uf2 = (uf2) qa.a(layoutInflater, R.layout.fragment_sleep_overview_day, viewGroup, false, O0());
        kd4.a((Object) uf2, "binding");
        a(uf2);
        this.j = new tr3<>(this, uf2);
        tr3<uf2> tr3 = this.j;
        if (tr3 != null) {
            uf2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onResume");
        ed3 ed3 = this.k;
        if (ed3 != null) {
            ed3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onStop");
        ed3 ed3 = this.k;
        if (ed3 != null) {
            ed3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void s(List<dg3.b> list) {
        kd4.b(list, "listOfSleepSessionUIData");
        if (list.isEmpty()) {
            tr3<uf2> tr3 = this.j;
            if (tr3 != null) {
                uf2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.u;
                    if (flexibleTextView != null) {
                        flexibleTextView.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        tr3<uf2> tr32 = this.j;
        if (tr32 != null) {
            uf2 a3 = tr32.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView2 = a3.u;
                if (flexibleTextView2 != null) {
                    flexibleTextView2.setVisibility(8);
                }
            }
        }
        if (list.size() > 1) {
            tr3<uf2> tr33 = this.j;
            if (tr33 != null) {
                uf2 a4 = tr33.a();
                if (a4 != null) {
                    CustomPageIndicator customPageIndicator = a4.q;
                    if (customPageIndicator != null) {
                        customPageIndicator.setVisibility(0);
                    }
                }
            }
        } else {
            tr3<uf2> tr34 = this.j;
            if (tr34 != null) {
                uf2 a5 = tr34.a();
                if (a5 != null) {
                    CustomPageIndicator customPageIndicator2 = a5.q;
                    if (customPageIndicator2 != null) {
                        customPageIndicator2.setVisibility(8);
                    }
                }
            }
        }
        dg3 dg3 = this.l;
        if (dg3 != null) {
            dg3.a(list);
        }
    }

    @DexIgnore
    public void a(ed3 ed3) {
        kd4.b(ed3, "presenter");
        this.k = ed3;
    }

    @DexIgnore
    public final void a(uf2 uf2) {
        uf2.r.setOnClickListener(b.e);
        uf2.s.setOnClickListener(c.e);
        this.l = new dg3(new ArrayList());
        RecyclerViewPager recyclerViewPager = uf2.t;
        kd4.a((Object) recyclerViewPager, "binding.rvSleeps");
        recyclerViewPager.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        RecyclerViewPager recyclerViewPager2 = uf2.t;
        kd4.a((Object) recyclerViewPager2, "binding.rvSleeps");
        recyclerViewPager2.setAdapter(this.l);
        uf2.t.a((RecyclerView.q) new d(this));
        uf2.t.i(this.m);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CustomPageIndicator.a(1, 0, 2, (fd4) null));
        uf2.q.a((RecyclerView) uf2.t, this.m, (List<CustomPageIndicator.a>) arrayList);
    }
}
