package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gg */
public interface C1874gg extends java.io.Closeable {
    @DexIgnore
    /* renamed from: a */
    android.database.Cursor mo11229a(com.fossil.blesdk.obfuscated.C2125jg jgVar);

    @DexIgnore
    /* renamed from: b */
    void mo11230b(java.lang.String str) throws android.database.SQLException;

    @DexIgnore
    /* renamed from: c */
    com.fossil.blesdk.obfuscated.C2221kg mo11231c(java.lang.String str);

    @DexIgnore
    /* renamed from: d */
    android.database.Cursor mo11232d(java.lang.String str);

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    /* renamed from: s */
    void mo11234s();

    @DexIgnore
    /* renamed from: t */
    java.util.List<android.util.Pair<java.lang.String, java.lang.String>> mo11235t();

    @DexIgnore
    /* renamed from: u */
    void mo11236u();

    @DexIgnore
    /* renamed from: v */
    void mo11237v();

    @DexIgnore
    /* renamed from: w */
    java.lang.String mo11238w();

    @DexIgnore
    /* renamed from: x */
    boolean mo11239x();
}
