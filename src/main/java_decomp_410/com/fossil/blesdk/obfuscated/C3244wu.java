package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wu */
public class C3244wu {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C2604ov> f10710a; // = java.util.Collections.newSetFromMap(new java.util.WeakHashMap());

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2604ov> f10711b; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: c */
    public boolean f10712c;

    @DexIgnore
    /* renamed from: a */
    public boolean mo17534a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z = true;
        if (ovVar == null) {
            return true;
        }
        boolean remove = this.f10710a.remove(ovVar);
        if (!this.f10711b.remove(ovVar) && !remove) {
            z = false;
        }
        if (z) {
            ovVar.clear();
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17536b(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        this.f10710a.add(ovVar);
        if (!this.f10712c) {
            ovVar.mo4035c();
            return;
        }
        ovVar.clear();
        if (android.util.Log.isLoggable("RequestTracker", 2)) {
            android.util.Log.v("RequestTracker", "Paused, delaying request");
        }
        this.f10711b.add(ovVar);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo17537c() {
        this.f10712c = true;
        for (T t : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f10710a)) {
            if (t.isRunning()) {
                t.mo4037d();
                this.f10711b.add(t);
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo17538d() {
        for (T t : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f10710a)) {
            if (!t.mo4039f() && !t.mo4038e()) {
                t.clear();
                if (!this.f10712c) {
                    t.mo4035c();
                } else {
                    this.f10711b.add(t);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo17539e() {
        this.f10712c = false;
        for (T t : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f10710a)) {
            if (!t.mo4039f() && !t.isRunning()) {
                t.mo4035c();
            }
        }
        this.f10711b.clear();
    }

    @DexIgnore
    public java.lang.String toString() {
        return super.toString() + "{numRequests=" + this.f10710a.size() + ", isPaused=" + this.f10712c + "}";
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17533a() {
        for (T a : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f10710a)) {
            mo17534a(a);
        }
        this.f10711b.clear();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17535b() {
        this.f10712c = true;
        for (T t : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f10710a)) {
            if (t.isRunning() || t.mo4039f()) {
                t.clear();
                this.f10711b.add(t);
            }
        }
    }
}
