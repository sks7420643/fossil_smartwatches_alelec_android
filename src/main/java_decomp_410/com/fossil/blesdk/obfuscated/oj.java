package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import com.fossil.blesdk.obfuscated.uj;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oj implements mj {
    @DexIgnore
    public static /* final */ String n; // = dj.a("Processor");
    @DexIgnore
    public Context e;
    @DexIgnore
    public xi f;
    @DexIgnore
    public zl g;
    @DexIgnore
    public WorkDatabase h;
    @DexIgnore
    public Map<String, uj> i; // = new HashMap();
    @DexIgnore
    public List<pj> j;
    @DexIgnore
    public Set<String> k;
    @DexIgnore
    public /* final */ List<mj> l;
    @DexIgnore
    public /* final */ Object m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public mj e;
        @DexIgnore
        public String f;
        @DexIgnore
        public zv1<Boolean> g;

        @DexIgnore
        public a(mj mjVar, String str, zv1<Boolean> zv1) {
            this.e = mjVar;
            this.f = str;
            this.g = zv1;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.g.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.e.a(this.f, z);
        }
    }

    @DexIgnore
    public oj(Context context, xi xiVar, zl zlVar, WorkDatabase workDatabase, List<pj> list) {
        this.e = context;
        this.f = xiVar;
        this.g = zlVar;
        this.h = workDatabase;
        this.j = list;
        this.k = new HashSet();
        this.l = new ArrayList();
        this.m = new Object();
    }

    @DexIgnore
    public boolean a(String str, WorkerParameters.a aVar) {
        synchronized (this.m) {
            if (this.i.containsKey(str)) {
                dj.a().a(n, String.format("Work %s is already enqueued for processing", new Object[]{str}), new Throwable[0]);
                return false;
            }
            uj.c cVar = new uj.c(this.e, this.f, this.g, this.h, str);
            cVar.a(this.j);
            cVar.a(aVar);
            uj a2 = cVar.a();
            zv1<Boolean> a3 = a2.a();
            a3.a(new a(this, str, a3), this.g.a());
            this.i.put(str, a2);
            this.g.b().execute(a2);
            dj.a().a(n, String.format("%s: processing %s", new Object[]{oj.class.getSimpleName(), str}), new Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        boolean containsKey;
        synchronized (this.m) {
            containsKey = this.i.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public boolean c(String str) {
        return a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public boolean d(String str) {
        synchronized (this.m) {
            dj.a().a(n, String.format("Processor cancelling %s", new Object[]{str}), new Throwable[0]);
            this.k.add(str);
            uj remove = this.i.remove(str);
            if (remove != null) {
                remove.a(true);
                dj.a().a(n, String.format("WorkerWrapper cancelled for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            dj.a().a(n, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public boolean e(String str) {
        synchronized (this.m) {
            dj.a().a(n, String.format("Processor stopping %s", new Object[]{str}), new Throwable[0]);
            uj remove = this.i.remove(str);
            if (remove != null) {
                remove.a(false);
                dj.a().a(n, String.format("WorkerWrapper stopped for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            dj.a().a(n, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public void b(mj mjVar) {
        synchronized (this.m) {
            this.l.remove(mjVar);
        }
    }

    @DexIgnore
    public boolean a(String str) {
        boolean contains;
        synchronized (this.m) {
            contains = this.k.contains(str);
        }
        return contains;
    }

    @DexIgnore
    public void a(mj mjVar) {
        synchronized (this.m) {
            this.l.add(mjVar);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.m) {
            this.i.remove(str);
            dj.a().a(n, String.format("%s %s executed; reschedule = %s", new Object[]{getClass().getSimpleName(), str, Boolean.valueOf(z)}), new Throwable[0]);
            for (mj a2 : this.l) {
                a2.a(str, z);
            }
        }
    }
}
