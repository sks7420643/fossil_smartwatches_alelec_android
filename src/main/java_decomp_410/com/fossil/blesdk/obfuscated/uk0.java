package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uk0 extends oj0<xk0> {
    @DexIgnore
    public uk0(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 39, kj0, bVar, cVar);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        if (queryLocalInterface instanceof xk0) {
            return (xk0) queryLocalInterface;
        }
        return new yk0(iBinder);
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.common.service.START";
    }
}
