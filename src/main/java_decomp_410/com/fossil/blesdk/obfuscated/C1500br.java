package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.br */
public final class C1500br {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f3879a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f3880b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.content.Context f3881c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f3882d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.br$a")
    /* renamed from: com.fossil.blesdk.obfuscated.br$a */
    public static final class C1501a {

        @DexIgnore
        /* renamed from: i */
        public static /* final */ int f3883i; // = (android.os.Build.VERSION.SDK_INT < 26 ? 4 : 1);

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f3884a;

        @DexIgnore
        /* renamed from: b */
        public android.app.ActivityManager f3885b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1500br.C1503c f3886c;

        @DexIgnore
        /* renamed from: d */
        public float f3887d; // = 2.0f;

        @DexIgnore
        /* renamed from: e */
        public float f3888e; // = ((float) f3883i);

        @DexIgnore
        /* renamed from: f */
        public float f3889f; // = 0.4f;

        @DexIgnore
        /* renamed from: g */
        public float f3890g; // = 0.33f;

        @DexIgnore
        /* renamed from: h */
        public int f3891h; // = 4194304;

        @DexIgnore
        public C1501a(android.content.Context context) {
            this.f3884a = context;
            this.f3885b = (android.app.ActivityManager) context.getSystemService(com.misfit.frameworks.common.constants.Constants.ACTIVITY);
            this.f3886c = new com.fossil.blesdk.obfuscated.C1500br.C1502b(context.getResources().getDisplayMetrics());
            if (android.os.Build.VERSION.SDK_INT >= 26 && com.fossil.blesdk.obfuscated.C1500br.m5075a(this.f3885b)) {
                this.f3888e = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1500br mo9264a() {
            return new com.fossil.blesdk.obfuscated.C1500br(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.br$b")
    /* renamed from: com.fossil.blesdk.obfuscated.br$b */
    public static final class C1502b implements com.fossil.blesdk.obfuscated.C1500br.C1503c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.util.DisplayMetrics f3892a;

        @DexIgnore
        public C1502b(android.util.DisplayMetrics displayMetrics) {
            this.f3892a = displayMetrics;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo9265a() {
            return this.f3892a.heightPixels;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo9266b() {
            return this.f3892a.widthPixels;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.br$c */
    public interface C1503c {
        @DexIgnore
        /* renamed from: a */
        int mo9265a();

        @DexIgnore
        /* renamed from: b */
        int mo9266b();
    }

    @DexIgnore
    public C1500br(com.fossil.blesdk.obfuscated.C1500br.C1501a aVar) {
        int i;
        this.f3881c = aVar.f3884a;
        if (m5075a(aVar.f3885b)) {
            i = aVar.f3891h / 2;
        } else {
            i = aVar.f3891h;
        }
        this.f3882d = i;
        int a = m5074a(aVar.f3885b, aVar.f3889f, aVar.f3890g);
        float b = (float) (aVar.f3886c.mo9266b() * aVar.f3886c.mo9265a() * 4);
        int round = java.lang.Math.round(aVar.f3888e * b);
        int round2 = java.lang.Math.round(b * aVar.f3887d);
        int i2 = a - this.f3882d;
        int i3 = round2 + round;
        if (i3 <= i2) {
            this.f3880b = round2;
            this.f3879a = round;
        } else {
            float f = (float) i2;
            float f2 = aVar.f3888e;
            float f3 = aVar.f3887d;
            float f4 = f / (f2 + f3);
            this.f3880b = java.lang.Math.round(f3 * f4);
            this.f3879a = java.lang.Math.round(f4 * aVar.f3888e);
        }
        if (android.util.Log.isLoggable("MemorySizeCalculator", 3)) {
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("Calculation complete, Calculated memory cache size: ");
            sb.append(mo9261a(this.f3880b));
            sb.append(", pool size: ");
            sb.append(mo9261a(this.f3879a));
            sb.append(", byte array size: ");
            sb.append(mo9261a(this.f3882d));
            sb.append(", memory class limited? ");
            sb.append(i3 > a);
            sb.append(", max size: ");
            sb.append(mo9261a(a));
            sb.append(", memoryClass: ");
            sb.append(aVar.f3885b.getMemoryClass());
            sb.append(", isLowMemoryDevice: ");
            sb.append(m5075a(aVar.f3885b));
            android.util.Log.d("MemorySizeCalculator", sb.toString());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo9260a() {
        return this.f3882d;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo9262b() {
        return this.f3879a;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo9263c() {
        return this.f3880b;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m5074a(android.app.ActivityManager activityManager, float f, float f2) {
        boolean a = m5075a(activityManager);
        float memoryClass = (float) (activityManager.getMemoryClass() * 1024 * 1024);
        if (a) {
            f = f2;
        }
        return java.lang.Math.round(memoryClass * f);
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo9261a(int i) {
        return android.text.format.Formatter.formatFileSize(this.f3881c, (long) i);
    }

    @DexIgnore
    @android.annotation.TargetApi(19)
    /* renamed from: a */
    public static boolean m5075a(android.app.ActivityManager activityManager) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return true;
    }
}
