package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ok extends lk<gk> {
    @DexIgnore
    public static /* final */ String e; // = dj.a("NetworkNotRoamingCtrlr");

    @DexIgnore
    public ok(Context context, zl zlVar) {
        super(xk.a(context, zlVar).c());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.b() == NetworkType.NOT_ROAMING;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(gk gkVar) {
        if (Build.VERSION.SDK_INT < 24) {
            dj.a().a(e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
            return !gkVar.a();
        } else if (!gkVar.a() || !gkVar.c()) {
            return true;
        } else {
            return false;
        }
    }
}
