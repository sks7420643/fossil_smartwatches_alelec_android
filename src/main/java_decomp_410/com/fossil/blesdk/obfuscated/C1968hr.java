package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hr */
public class C1968hr implements com.fossil.blesdk.obfuscated.C1963ho<java.nio.ByteBuffer> {
    @DexIgnore
    /* renamed from: a */
    public boolean mo11698a(java.nio.ByteBuffer byteBuffer, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        try {
            com.fossil.blesdk.obfuscated.C2255kw.m9872a(byteBuffer, file);
            return true;
        } catch (java.io.IOException e) {
            if (android.util.Log.isLoggable("ByteBufferEncoder", 3)) {
                android.util.Log.d("ByteBufferEncoder", "Failed to write data", e);
            }
            return false;
        }
    }
}
