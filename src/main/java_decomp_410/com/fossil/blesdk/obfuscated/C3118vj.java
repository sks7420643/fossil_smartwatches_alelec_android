package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vj */
public class C3118vj implements com.fossil.blesdk.obfuscated.C2656pj, com.fossil.blesdk.obfuscated.C1953hk, com.fossil.blesdk.obfuscated.C2419mj {

    @DexIgnore
    /* renamed from: l */
    public static /* final */ java.lang.String f10298l; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("GreedyScheduler");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f10299e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2968tj f10300f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2035ik f10301g;

    @DexIgnore
    /* renamed from: h */
    public java.util.List<com.fossil.blesdk.obfuscated.C1954hl> f10302h; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: i */
    public boolean f10303i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ java.lang.Object f10304j;

    @DexIgnore
    /* renamed from: k */
    public java.lang.Boolean f10305k;

    @DexIgnore
    public C3118vj(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar, com.fossil.blesdk.obfuscated.C2968tj tjVar) {
        this.f10299e = context;
        this.f10300f = tjVar;
        this.f10301g = new com.fossil.blesdk.obfuscated.C2035ik(context, zlVar, this);
        this.f10304j = new java.lang.Object();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9223a(com.fossil.blesdk.obfuscated.C1954hl... hlVarArr) {
        if (this.f10305k == null) {
            this.f10305k = java.lang.Boolean.valueOf(android.text.TextUtils.equals(this.f10299e.getPackageName(), mo17104a()));
        }
        if (!this.f10305k.booleanValue()) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f10298l, "Ignoring schedule request in non-main process", new java.lang.Throwable[0]);
            return;
        }
        mo17105b();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.util.ArrayList arrayList2 = new java.util.ArrayList();
        for (com.fossil.blesdk.obfuscated.C1954hl hlVar : hlVarArr) {
            if (hlVar.f5772b == androidx.work.WorkInfo.State.ENQUEUED && !hlVar.mo11673d() && hlVar.f5777g == 0 && !hlVar.mo11672c()) {
                if (!hlVar.mo11671b()) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Starting work for %s", new java.lang.Object[]{hlVar.f5771a}), new java.lang.Throwable[0]);
                    this.f10300f.mo16454a(hlVar.f5771a);
                } else if (android.os.Build.VERSION.SDK_INT >= 23 && hlVar.f5780j.mo18118h()) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Ignoring WorkSpec %s, Requires device idle.", new java.lang.Object[]{hlVar}), new java.lang.Throwable[0]);
                } else if (android.os.Build.VERSION.SDK_INT < 24 || !hlVar.f5780j.mo18114e()) {
                    arrayList.add(hlVar);
                    arrayList2.add(hlVar.f5771a);
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", new java.lang.Object[]{hlVar}), new java.lang.Throwable[0]);
                }
            }
        }
        synchronized (this.f10304j) {
            if (!arrayList.isEmpty()) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Starting tracking for [%s]", new java.lang.Object[]{android.text.TextUtils.join(",", arrayList2)}), new java.lang.Throwable[0]);
                this.f10302h.addAll(arrayList);
                this.f10301g.mo12013c(this.f10302h);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo3845b(java.util.List<java.lang.String> list) {
        for (java.lang.String next : list) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Constraints met: Scheduling work ID %s", new java.lang.Object[]{next}), new java.lang.Throwable[0]);
            this.f10300f.mo16454a(next);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17106b(java.lang.String str) {
        synchronized (this.f10304j) {
            int size = this.f10302h.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.f10302h.get(i).f5771a.equals(str)) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Stopping tracking for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
                    this.f10302h.remove(i);
                    this.f10301g.mo12013c(this.f10302h);
                    break;
                } else {
                    i++;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17105b() {
        if (!this.f10303i) {
            this.f10300f.mo16460e().mo14424a((com.fossil.blesdk.obfuscated.C2419mj) this);
            this.f10303i = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9222a(java.lang.String str) {
        if (this.f10305k == null) {
            this.f10305k = java.lang.Boolean.valueOf(android.text.TextUtils.equals(this.f10299e.getPackageName(), mo17104a()));
        }
        if (!this.f10305k.booleanValue()) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f10298l, "Ignoring schedule request in non-main process", new java.lang.Throwable[0]);
            return;
        }
        mo17105b();
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Cancelling work ID %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
        this.f10300f.mo16457b(str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3844a(java.util.List<java.lang.String> list) {
        for (java.lang.String next : list) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10298l, java.lang.String.format("Constraints not met: Cancelling work ID %s", new java.lang.Object[]{next}), new java.lang.Throwable[0]);
            this.f10300f.mo16457b(next);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3804a(java.lang.String str, boolean z) {
        mo17106b(str);
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo17104a() {
        int myPid = android.os.Process.myPid();
        android.app.ActivityManager activityManager = (android.app.ActivityManager) this.f10299e.getSystemService(com.misfit.frameworks.common.constants.Constants.ACTIVITY);
        if (activityManager == null) {
            return null;
        }
        java.util.List<android.app.ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (android.app.ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }
}
