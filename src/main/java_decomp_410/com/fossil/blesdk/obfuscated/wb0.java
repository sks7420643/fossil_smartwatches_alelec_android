package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wb0 {
    @DexIgnore
    he0<Status> a(ge0 ge0);

    @DexIgnore
    zb0 a(Intent intent);
}
