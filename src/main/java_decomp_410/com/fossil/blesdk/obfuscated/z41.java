package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z41 extends m31 {
    @DexIgnore
    public /* final */ /* synthetic */ rc1 s;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z41(x41 x41, ge0 ge0, rc1 rc1) {
        super(ge0);
        this.s = rc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        ((f41) bVar).a(af0.a(this.s, rc1.class.getSimpleName()), new n31(this));
    }
}
