package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rg1 extends ui1 {
    @DexIgnore
    public static /* final */ AtomicReference<String[]> c; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> d; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> e; // = new AtomicReference<>();

    @DexIgnore
    public rg1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, wi1.b, wi1.a, c);
    }

    @DexIgnore
    public final String b(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, xi1.b, xi1.a, d);
    }

    @DexIgnore
    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, yi1.b, yi1.a, e);
        }
        return "experiment_id" + "(" + str + ")";
    }

    @DexIgnore
    public final boolean p() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        b();
        return this.a.z() && this.a.d().a(3);
    }

    @DexIgnore
    public static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        bk0.a(strArr);
        bk0.a(strArr2);
        bk0.a(atomicReference);
        bk0.a(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (nl1.e(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    @DexIgnore
    public final String a(hg1 hg1) {
        if (hg1 == null) {
            return null;
        }
        if (!s()) {
            return hg1.toString();
        }
        return "origin=" + hg1.g + ",name=" + a(hg1.e) + ",params=" + a(hg1.f);
    }

    @DexIgnore
    public final String a(cg1 cg1) {
        if (cg1 == null) {
            return null;
        }
        if (!s()) {
            return cg1.toString();
        }
        return "Event{appId='" + cg1.a + "', name='" + a(cg1.b) + "', params=" + a(cg1.f) + "}";
    }

    @DexIgnore
    public final String a(eg1 eg1) {
        if (eg1 == null) {
            return null;
        }
        if (!s()) {
            return eg1.toString();
        }
        return a(eg1.H());
    }

    @DexIgnore
    public final String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!s()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String str : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(b(str));
            sb.append(SimpleComparison.EQUAL_TO_OPERATION);
            sb.append(bundle.get(str));
        }
        sb.append("}]");
        return sb.toString();
    }
}
