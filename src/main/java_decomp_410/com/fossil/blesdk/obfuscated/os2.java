package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class os2 extends te {
    @DexIgnore
    public int f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ os2 a;

        @DexIgnore
        public b(os2 os2) {
            this.a = os2;
        }

        @DexIgnore
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            kd4.b(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0) {
                try {
                    View c = this.a.c(recyclerView.getLayoutManager());
                    os2 os2 = this.a;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager == null) {
                        kd4.a();
                        throw null;
                    } else if (c != null) {
                        os2.a(layoutManager.l(c));
                    } else {
                        kd4.a();
                        throw null;
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("BlurLinearSnapHelper", "attachToRecyclerView - e=" + e);
                }
            }
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            kd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            this.a.c(recyclerView.getLayoutManager());
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public final void a(int i) {
        this.f = i;
    }

    @DexIgnore
    public final void b(RecyclerView.m mVar, View view) {
        View view2;
        if (view != null && mVar != null) {
            view.setAlpha(1.0f);
            int i = -1;
            int i2 = 0;
            int e = mVar.e() - 1;
            if (e >= 0) {
                while (true) {
                    if (!kd4.a((Object) mVar.d(i2), (Object) view)) {
                        if (i2 == e) {
                            break;
                        }
                        i2++;
                    } else {
                        i = i2;
                        break;
                    }
                }
            }
            if (i >= 0) {
                View view3 = null;
                if (i > 0 && i < mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = mVar.d(i + 1);
                } else if (i == 0) {
                    view2 = mVar.d(i + 1);
                } else if (i == mVar.e() - 1) {
                    view3 = mVar.d(i - 1);
                    view2 = null;
                } else {
                    view2 = null;
                }
                if (view3 != null) {
                    view3.setAlpha(0.8f);
                }
                if (view2 != null) {
                    view2.setAlpha(0.8f);
                }
            }
        }
    }

    @DexIgnore
    public View c(RecyclerView.m mVar) {
        View c = super.c(mVar);
        b(mVar, c);
        return c;
    }

    @DexIgnore
    public final int d() {
        return this.f;
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        super.a(recyclerView);
        if (recyclerView != null) {
            recyclerView.a((RecyclerView.q) new b(this));
        }
    }
}
