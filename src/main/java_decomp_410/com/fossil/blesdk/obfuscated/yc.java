package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import com.fossil.blesdk.obfuscated.xc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yc {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends xc.b {
        @DexIgnore
        public a(Context context, b bVar) {
            super(context, bVar);
        }

        @DexIgnore
        public void onLoadItem(String str, MediaBrowserService.Result<MediaBrowser.MediaItem> result) {
            ((b) this.e).a(str, new xc.c(result));
        }
    }

    @DexIgnore
    public interface b extends xc.d {
        @DexIgnore
        void a(String str, xc.c<Parcel> cVar);
    }

    @DexIgnore
    public static Object a(Context context, b bVar) {
        return new a(context, bVar);
    }
}
