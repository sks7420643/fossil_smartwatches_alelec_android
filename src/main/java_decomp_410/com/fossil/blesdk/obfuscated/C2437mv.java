package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mv */
public final class C2437mv implements com.bumptech.glide.request.RequestCoordinator, com.fossil.blesdk.obfuscated.C2604ov {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f7582a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.bumptech.glide.request.RequestCoordinator f7583b;

    @DexIgnore
    /* renamed from: c */
    public volatile com.fossil.blesdk.obfuscated.C2604ov f7584c;

    @DexIgnore
    /* renamed from: d */
    public volatile com.fossil.blesdk.obfuscated.C2604ov f7585d;

    @DexIgnore
    /* renamed from: e */
    public com.bumptech.glide.request.RequestCoordinator.RequestState f7586e;

    @DexIgnore
    /* renamed from: f */
    public com.bumptech.glide.request.RequestCoordinator.RequestState f7587f;

    @DexIgnore
    public C2437mv(java.lang.Object obj, com.bumptech.glide.request.RequestCoordinator requestCoordinator) {
        com.bumptech.glide.request.RequestCoordinator.RequestState requestState = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
        this.f7586e = requestState;
        this.f7587f = requestState;
        this.f7582a = obj;
        this.f7583b = requestCoordinator;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13715a(com.fossil.blesdk.obfuscated.C2604ov ovVar, com.fossil.blesdk.obfuscated.C2604ov ovVar2) {
        this.f7584c = ovVar;
        this.f7585d = ovVar2;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo13716b() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f7583b;
        return requestCoordinator == null || requestCoordinator.mo4023f(this);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo4035c() {
        synchronized (this.f7582a) {
            if (this.f7586e != com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                this.f7586e = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING;
                this.f7584c.mo4035c();
            }
        }
    }

    @DexIgnore
    public void clear() {
        synchronized (this.f7582a) {
            this.f7586e = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
            this.f7584c.clear();
            if (this.f7587f != com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED) {
                this.f7587f = com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
                this.f7585d.clear();
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo4037d() {
        synchronized (this.f7582a) {
            if (this.f7586e == com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                this.f7586e = com.bumptech.glide.request.RequestCoordinator.RequestState.PAUSED;
                this.f7584c.mo4037d();
            }
            if (this.f7587f == com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                this.f7587f = com.bumptech.glide.request.RequestCoordinator.RequestState.PAUSED;
                this.f7585d.mo4037d();
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo4038e() {
        boolean z;
        synchronized (this.f7582a) {
            z = this.f7586e == com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED && this.f7587f == com.bumptech.glide.request.RequestCoordinator.RequestState.CLEARED;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo4039f() {
        boolean z;
        synchronized (this.f7582a) {
            if (this.f7586e != com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS) {
                if (this.f7587f != com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: g */
    public final boolean mo13717g() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f7583b;
        return requestCoordinator == null || requestCoordinator.mo4020c(this);
    }

    @DexIgnore
    /* renamed from: h */
    public final boolean mo13719h() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f7583b;
        return requestCoordinator == null || requestCoordinator.mo4021d(this);
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo13720i() {
        com.bumptech.glide.request.RequestCoordinator requestCoordinator = this.f7583b;
        return requestCoordinator != null && requestCoordinator.mo4018a();
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.f7582a) {
            if (this.f7586e != com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                if (this.f7587f != com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: b */
    public void mo4019b(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        synchronized (this.f7582a) {
            if (!ovVar.equals(this.f7585d)) {
                this.f7586e = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED;
                if (this.f7587f != com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING) {
                    this.f7587f = com.bumptech.glide.request.RequestCoordinator.RequestState.RUNNING;
                    this.f7585d.mo4035c();
                }
            } else {
                this.f7587f = com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED;
                if (this.f7583b != null) {
                    this.f7583b.mo4019b(this);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final boolean mo13718g(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        return ovVar.equals(this.f7584c) || (this.f7586e == com.bumptech.glide.request.RequestCoordinator.RequestState.FAILED && ovVar.equals(this.f7585d));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo4033a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        if (!(ovVar instanceof com.fossil.blesdk.obfuscated.C2437mv)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2437mv mvVar = (com.fossil.blesdk.obfuscated.C2437mv) ovVar;
        if (!this.f7584c.mo4033a(mvVar.f7584c) || !this.f7585d.mo4033a(mvVar.f7585d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo4022e(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        synchronized (this.f7582a) {
            if (ovVar.equals(this.f7584c)) {
                this.f7586e = com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS;
            } else if (ovVar.equals(this.f7585d)) {
                this.f7587f = com.bumptech.glide.request.RequestCoordinator.RequestState.SUCCESS;
            }
            if (this.f7583b != null) {
                this.f7583b.mo4022e(this);
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo4023f(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f7582a) {
            z = mo13716b() && mo13718g(ovVar);
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo4018a() {
        boolean z;
        synchronized (this.f7582a) {
            if (!mo13720i()) {
                if (!mo4039f()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo4020c(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f7582a) {
            z = mo13717g() && mo13718g(ovVar);
        }
        return z;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo4021d(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        boolean z;
        synchronized (this.f7582a) {
            z = mo13719h() && mo13718g(ovVar);
        }
        return z;
    }
}
