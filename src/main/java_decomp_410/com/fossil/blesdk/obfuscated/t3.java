package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.c;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t3 extends Service {
    @DexIgnore
    public c.a e; // = new a(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends c.a {
        @DexIgnore
        public a(t3 t3Var) {
        }

        @DexIgnore
        public void a(a aVar, Bundle bundle) throws RemoteException {
            aVar.d(bundle);
        }

        @DexIgnore
        public void a(a aVar, String str, Bundle bundle) throws RemoteException {
            aVar.a(str, bundle);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.e;
    }
}
