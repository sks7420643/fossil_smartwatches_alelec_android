package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t9 */
public abstract class C2951t9 implements android.view.View.OnTouchListener {

    @DexIgnore
    /* renamed from: v */
    public static /* final */ int f9580v; // = android.view.ViewConfiguration.getTapTimeout();

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2951t9.C2952a f9581e; // = new com.fossil.blesdk.obfuscated.C2951t9.C2952a();

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.view.animation.Interpolator f9582f; // = new android.view.animation.AccelerateInterpolator();

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.view.View f9583g;

    @DexIgnore
    /* renamed from: h */
    public java.lang.Runnable f9584h;

    @DexIgnore
    /* renamed from: i */
    public float[] f9585i; // = {com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};

    @DexIgnore
    /* renamed from: j */
    public float[] f9586j; // = {Float.MAX_VALUE, Float.MAX_VALUE};

    @DexIgnore
    /* renamed from: k */
    public int f9587k;

    @DexIgnore
    /* renamed from: l */
    public int f9588l;

    @DexIgnore
    /* renamed from: m */
    public float[] f9589m; // = {com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};

    @DexIgnore
    /* renamed from: n */
    public float[] f9590n; // = {com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};

    @DexIgnore
    /* renamed from: o */
    public float[] f9591o; // = {Float.MAX_VALUE, Float.MAX_VALUE};

    @DexIgnore
    /* renamed from: p */
    public boolean f9592p;

    @DexIgnore
    /* renamed from: q */
    public boolean f9593q;

    @DexIgnore
    /* renamed from: r */
    public boolean f9594r;

    @DexIgnore
    /* renamed from: s */
    public boolean f9595s;

    @DexIgnore
    /* renamed from: t */
    public boolean f9596t;

    @DexIgnore
    /* renamed from: u */
    public boolean f9597u;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.t9$a")
    /* renamed from: com.fossil.blesdk.obfuscated.t9$a */
    public static class C2952a {

        @DexIgnore
        /* renamed from: a */
        public int f9598a;

        @DexIgnore
        /* renamed from: b */
        public int f9599b;

        @DexIgnore
        /* renamed from: c */
        public float f9600c;

        @DexIgnore
        /* renamed from: d */
        public float f9601d;

        @DexIgnore
        /* renamed from: e */
        public long f9602e; // = Long.MIN_VALUE;

        @DexIgnore
        /* renamed from: f */
        public long f9603f; // = 0;

        @DexIgnore
        /* renamed from: g */
        public int f9604g; // = 0;

        @DexIgnore
        /* renamed from: h */
        public int f9605h; // = 0;

        @DexIgnore
        /* renamed from: i */
        public long f9606i; // = -1;

        @DexIgnore
        /* renamed from: j */
        public float f9607j;

        @DexIgnore
        /* renamed from: k */
        public int f9608k;

        @DexIgnore
        /* renamed from: a */
        public final float mo16321a(float f) {
            return (-4.0f * f * f) + (f * 4.0f);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16325a(int i) {
            this.f9599b = i;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo16327b(int i) {
            this.f9598a = i;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo16328c() {
            return this.f9605h;
        }

        @DexIgnore
        /* renamed from: d */
        public int mo16329d() {
            float f = this.f9600c;
            return (int) (f / java.lang.Math.abs(f));
        }

        @DexIgnore
        /* renamed from: e */
        public int mo16330e() {
            float f = this.f9601d;
            return (int) (f / java.lang.Math.abs(f));
        }

        @DexIgnore
        /* renamed from: f */
        public boolean mo16331f() {
            return this.f9606i > 0 && android.view.animation.AnimationUtils.currentAnimationTimeMillis() > this.f9606i + ((long) this.f9608k);
        }

        @DexIgnore
        /* renamed from: g */
        public void mo16332g() {
            long currentAnimationTimeMillis = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            this.f9608k = com.fossil.blesdk.obfuscated.C2951t9.m14083a((int) (currentAnimationTimeMillis - this.f9602e), 0, this.f9599b);
            this.f9607j = mo16322a(currentAnimationTimeMillis);
            this.f9606i = currentAnimationTimeMillis;
        }

        @DexIgnore
        /* renamed from: h */
        public void mo16333h() {
            this.f9602e = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
            this.f9606i = -1;
            this.f9603f = this.f9602e;
            this.f9607j = 0.5f;
            this.f9604g = 0;
            this.f9605h = 0;
        }

        @DexIgnore
        /* renamed from: a */
        public final float mo16322a(long j) {
            if (j < this.f9602e) {
                return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            long j2 = this.f9606i;
            if (j2 < 0 || j < j2) {
                return com.fossil.blesdk.obfuscated.C2951t9.m14082a(((float) (j - this.f9602e)) / ((float) this.f9598a), (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f) * 0.5f;
            }
            long j3 = j - j2;
            float f = this.f9607j;
            return (1.0f - f) + (f * com.fossil.blesdk.obfuscated.C2951t9.m14082a(((float) j3) / ((float) this.f9608k), (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f));
        }

        @DexIgnore
        /* renamed from: b */
        public int mo16326b() {
            return this.f9604g;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16323a() {
            if (this.f9603f != 0) {
                long currentAnimationTimeMillis = android.view.animation.AnimationUtils.currentAnimationTimeMillis();
                float a = mo16321a(mo16322a(currentAnimationTimeMillis));
                this.f9603f = currentAnimationTimeMillis;
                float f = ((float) (currentAnimationTimeMillis - this.f9603f)) * a;
                this.f9604g = (int) (this.f9600c * f);
                this.f9605h = (int) (f * this.f9601d);
                return;
            }
            throw new java.lang.RuntimeException("Cannot compute scroll delta before calling start()");
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16324a(float f, float f2) {
            this.f9600c = f;
            this.f9601d = f2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.t9$b")
    /* renamed from: com.fossil.blesdk.obfuscated.t9$b */
    public class C2953b implements java.lang.Runnable {
        @DexIgnore
        public C2953b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2951t9 t9Var = com.fossil.blesdk.obfuscated.C2951t9.this;
            if (t9Var.f9595s) {
                if (t9Var.f9593q) {
                    t9Var.f9593q = false;
                    t9Var.f9581e.mo16333h();
                }
                com.fossil.blesdk.obfuscated.C2951t9.C2952a aVar = com.fossil.blesdk.obfuscated.C2951t9.this.f9581e;
                if (aVar.mo16331f() || !com.fossil.blesdk.obfuscated.C2951t9.this.mo16312c()) {
                    com.fossil.blesdk.obfuscated.C2951t9.this.f9595s = false;
                    return;
                }
                com.fossil.blesdk.obfuscated.C2951t9 t9Var2 = com.fossil.blesdk.obfuscated.C2951t9.this;
                if (t9Var2.f9594r) {
                    t9Var2.f9594r = false;
                    t9Var2.mo16304a();
                }
                aVar.mo16323a();
                com.fossil.blesdk.obfuscated.C2951t9.this.mo16305a(aVar.mo16326b(), aVar.mo16328c());
                com.fossil.blesdk.obfuscated.C1776f9.m6816a(com.fossil.blesdk.obfuscated.C2951t9.this.f9583g, (java.lang.Runnable) this);
            }
        }
    }

    @DexIgnore
    public C2951t9(android.view.View view) {
        this.f9583g = view;
        float f = android.content.res.Resources.getSystem().getDisplayMetrics().density;
        float f2 = (float) ((int) ((1575.0f * f) + 0.5f));
        mo16310c(f2, f2);
        float f3 = (float) ((int) ((f * 315.0f) + 0.5f));
        mo16313d(f3, f3);
        mo16314d(1);
        mo16307b(Float.MAX_VALUE, Float.MAX_VALUE);
        mo16316e(0.2f, 0.2f);
        mo16318f(1.0f, 1.0f);
        mo16311c(f9580v);
        mo16319f(500);
        mo16317e(500);
    }

    @DexIgnore
    /* renamed from: a */
    public static float m14082a(float f, float f2, float f3) {
        return f > f3 ? f3 : f < f2 ? f2 : f;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14083a(int i, int i2, int i3) {
        return i > i3 ? i3 : i < i2 ? i2 : i;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16303a(boolean z) {
        if (this.f9596t && !z) {
            mo16308b();
        }
        this.f9596t = z;
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo16305a(int i, int i2);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo16306a(int i);

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16307b(float f, float f2) {
        float[] fArr = this.f9586j;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public abstract boolean mo16309b(int i);

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16310c(float f, float f2) {
        float[] fArr = this.f9591o;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16313d(float f, float f2) {
        float[] fArr = this.f9590n;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16316e(float f, float f2) {
        float[] fArr = this.f9585i;
        fArr[0] = f;
        fArr[1] = f2;
        return this;
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16318f(float f, float f2) {
        float[] fArr = this.f9589m;
        fArr[0] = f / 1000.0f;
        fArr[1] = f2 / 1000.0f;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
        if (r0 != 3) goto L_0x0058;
     */
    @DexIgnore
    public boolean onTouch(android.view.View view, android.view.MotionEvent motionEvent) {
        if (!this.f9596t) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                }
            }
            mo16308b();
            if (!this.f9597u && this.f9595s) {
                return true;
            }
        }
        this.f9594r = true;
        this.f9592p = false;
        this.f9581e.mo16324a(mo16302a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.f9583g.getWidth()), mo16302a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.f9583g.getHeight()));
        if (!this.f9595s && mo16312c()) {
            mo16315d();
        }
        return !this.f9597u ? false : false;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo16308b() {
        if (this.f9593q) {
            this.f9595s = false;
        } else {
            this.f9581e.mo16332g();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16311c(int i) {
        this.f9588l = i;
        return this;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16314d(int i) {
        this.f9587k = i;
        return this;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16317e(int i) {
        this.f9581e.mo16325a(i);
        return this;
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2951t9 mo16319f(int i) {
        this.f9581e.mo16327b(i);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo16302a(int i, float f, float f2, float f3) {
        float a = mo16301a(this.f9585i[i], f2, this.f9586j[i], f);
        int i2 = (a > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (a == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
        if (i2 == 0) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f4 = this.f9589m[i];
        float f5 = this.f9590n[i];
        float f6 = this.f9591o[i];
        float f7 = f4 * f3;
        if (i2 > 0) {
            return m14082a(a * f7, f5, f6);
        }
        return -m14082a((-a) * f7, f5, f6);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo16312c() {
        com.fossil.blesdk.obfuscated.C2951t9.C2952a aVar = this.f9581e;
        int e = aVar.mo16330e();
        int d = aVar.mo16329d();
        return (e != 0 && mo16309b(e)) || (d != 0 && mo16306a(d));
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo16315d() {
        if (this.f9584h == null) {
            this.f9584h = new com.fossil.blesdk.obfuscated.C2951t9.C2953b();
        }
        this.f9595s = true;
        this.f9593q = true;
        if (!this.f9592p) {
            int i = this.f9588l;
            if (i > 0) {
                com.fossil.blesdk.obfuscated.C1776f9.m6817a(this.f9583g, this.f9584h, (long) i);
                this.f9592p = true;
            }
        }
        this.f9584h.run();
        this.f9592p = true;
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo16301a(float f, float f2, float f3, float f4) {
        float f5;
        float a = m14082a(f * f2, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3);
        float a2 = mo16300a(f2 - f4, a) - mo16300a(f4, a);
        if (a2 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f5 = -this.f9582f.getInterpolation(-a2);
        } else if (a2 <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            f5 = this.f9582f.getInterpolation(a2);
        }
        return m14082a(f5, -1.0f, 1.0f);
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo16300a(float f, float f2) {
        if (f2 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i = this.f9587k;
        if (i == 0 || i == 1) {
            if (f < f2) {
                if (f >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return 1.0f - (f / f2);
                }
                if (!this.f9595s || this.f9587k != 1) {
                    return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                return 1.0f;
            }
        } else if (i == 2 && f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return f / (-f2);
        }
        return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16304a() {
        long uptimeMillis = android.os.SystemClock.uptimeMillis();
        android.view.MotionEvent obtain = android.view.MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        this.f9583g.onTouchEvent(obtain);
        obtain.recycle();
    }
}
