package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ka {
    @DexIgnore
    public static /* final */ Interpolator w; // = new a();
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public float[] d;
    @DexIgnore
    public float[] e;
    @DexIgnore
    public float[] f;
    @DexIgnore
    public float[] g;
    @DexIgnore
    public int[] h;
    @DexIgnore
    public int[] i;
    @DexIgnore
    public int[] j;
    @DexIgnore
    public int k;
    @DexIgnore
    public VelocityTracker l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public OverScroller q;
    @DexIgnore
    public /* final */ c r;
    @DexIgnore
    public View s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ ViewGroup u;
    @DexIgnore
    public /* final */ Runnable v; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Interpolator {
        @DexIgnore
        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * f2 * f2 * f2) + 1.0f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            ka.this.f(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public int a(int i) {
            return i;
        }

        @DexIgnore
        public int a(View view) {
            return 0;
        }

        @DexIgnore
        public abstract int a(View view, int i, int i2);

        @DexIgnore
        public void a(int i, int i2) {
        }

        @DexIgnore
        public abstract void a(View view, float f, float f2);

        @DexIgnore
        public void a(View view, int i) {
        }

        @DexIgnore
        public abstract void a(View view, int i, int i2, int i3, int i4);

        @DexIgnore
        public int b(View view) {
            return 0;
        }

        @DexIgnore
        public abstract int b(View view, int i, int i2);

        @DexIgnore
        public void b(int i, int i2) {
        }

        @DexIgnore
        public boolean b(int i) {
            return false;
        }

        @DexIgnore
        public abstract boolean b(View view, int i);

        @DexIgnore
        public abstract void c(int i);
    }

    @DexIgnore
    public ka(Context context, ViewGroup viewGroup, c cVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (cVar != null) {
            this.u = viewGroup;
            this.r = cVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.b = viewConfiguration.getScaledTouchSlop();
            this.m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.q = new OverScroller(context, w);
        } else {
            throw new IllegalArgumentException("Callback may not be null");
        }
    }

    @DexIgnore
    public static ka a(ViewGroup viewGroup, c cVar) {
        return new ka(viewGroup.getContext(), viewGroup, cVar);
    }

    @DexIgnore
    public void b(float f2) {
        this.n = f2;
    }

    @DexIgnore
    public final void c() {
        float[] fArr = this.d;
        if (fArr != null) {
            Arrays.fill(fArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.e, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.g, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.i, 0);
            Arrays.fill(this.j, 0);
            this.k = 0;
        }
    }

    @DexIgnore
    public View d() {
        return this.s;
    }

    @DexIgnore
    public int e() {
        return this.o;
    }

    @DexIgnore
    public int f() {
        return this.b;
    }

    @DexIgnore
    public int g() {
        return this.a;
    }

    @DexIgnore
    public final void h() {
        this.l.computeCurrentVelocity(1000, this.m);
        a(a(this.l.getXVelocity(this.c), this.n, this.m), a(this.l.getYVelocity(this.c), this.n, this.m));
    }

    @DexIgnore
    public static ka a(ViewGroup viewGroup, float f2, c cVar) {
        ka a2 = a(viewGroup, cVar);
        a2.b = (int) (((float) a2.b) * (1.0f / f2));
        return a2;
    }

    @DexIgnore
    public void b() {
        this.c = -1;
        c();
        VelocityTracker velocityTracker = this.l;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.l = null;
        }
    }

    @DexIgnore
    public boolean d(int i2) {
        return ((1 << i2) & this.k) != 0;
    }

    @DexIgnore
    public boolean e(int i2, int i3) {
        if (this.t) {
            return b(i2, i3, (int) this.l.getXVelocity(this.c), (int) this.l.getYVelocity(this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    @DexIgnore
    public void f(int i2) {
        this.u.removeCallbacks(this.v);
        if (this.a != i2) {
            this.a = i2;
            this.r.c(i2);
            if (this.a == 0) {
                this.s = null;
            }
        }
    }

    @DexIgnore
    public void g(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public boolean d(int i2, int i3) {
        return a(this.s, i2, i3);
    }

    @DexIgnore
    public void a(View view, int i2) {
        if (view.getParent() == this.u) {
            this.s = view;
            this.c = i2;
            this.r.a(view, i2);
            f(1);
            return;
        }
        throw new IllegalArgumentException("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (" + this.u + ")");
    }

    @DexIgnore
    public boolean b(View view, int i2, int i3) {
        this.s = view;
        this.c = -1;
        boolean b2 = b(i2, i3, 0, 0);
        if (!b2 && this.a == 0 && this.s != null) {
            this.s = null;
        }
        return b2;
    }

    @DexIgnore
    public final boolean e(int i2) {
        if (d(i2)) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i2 + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    @DexIgnore
    public void a() {
        b();
        if (this.a == 2) {
            int currX = this.q.getCurrX();
            int currY = this.q.getCurrY();
            this.q.abortAnimation();
            int currX2 = this.q.getCurrX();
            int currY2 = this.q.getCurrY();
            this.r.a(this.s, currX2, currY2, currX2 - currX, currY2 - currY);
        }
        f(0);
    }

    @DexIgnore
    public final void c(int i2) {
        float[] fArr = this.d;
        if (fArr == null || fArr.length <= i2) {
            int i3 = i2 + 1;
            float[] fArr2 = new float[i3];
            float[] fArr3 = new float[i3];
            float[] fArr4 = new float[i3];
            float[] fArr5 = new float[i3];
            int[] iArr = new int[i3];
            int[] iArr2 = new int[i3];
            int[] iArr3 = new int[i3];
            float[] fArr6 = this.d;
            if (fArr6 != null) {
                System.arraycopy(fArr6, 0, fArr2, 0, fArr6.length);
                float[] fArr7 = this.e;
                System.arraycopy(fArr7, 0, fArr3, 0, fArr7.length);
                float[] fArr8 = this.f;
                System.arraycopy(fArr8, 0, fArr4, 0, fArr8.length);
                float[] fArr9 = this.g;
                System.arraycopy(fArr9, 0, fArr5, 0, fArr9.length);
                int[] iArr4 = this.h;
                System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.i;
                System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.j;
                System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.d = fArr2;
            this.e = fArr3;
            this.f = fArr4;
            this.g = fArr5;
            this.h = iArr;
            this.i = iArr2;
            this.j = iArr3;
        }
    }

    @DexIgnore
    public final boolean b(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.q.abortAnimation();
            f(0);
            return false;
        }
        this.q.startScroll(left, top, i6, i7, a(this.s, i6, i7, i4, i5));
        f(2);
        return true;
    }

    @DexIgnore
    public final int a(View view, int i2, int i3, int i4, int i5) {
        float f2;
        float f3;
        float f4;
        float f5;
        int a2 = a(i4, (int) this.n, (int) this.m);
        int a3 = a(i5, (int) this.n, (int) this.m);
        int abs = Math.abs(i2);
        int abs2 = Math.abs(i3);
        int abs3 = Math.abs(a2);
        int abs4 = Math.abs(a3);
        int i6 = abs3 + abs4;
        int i7 = abs + abs2;
        if (a2 != 0) {
            f3 = (float) abs3;
            f2 = (float) i6;
        } else {
            f3 = (float) abs;
            f2 = (float) i7;
        }
        float f6 = f3 / f2;
        if (a3 != 0) {
            f5 = (float) abs4;
            f4 = (float) i6;
        } else {
            f5 = (float) abs2;
            f4 = (float) i7;
        }
        float f7 = f5 / f4;
        return (int) ((((float) b(i2, a2, this.r.a(view))) * f6) + (((float) b(i3, a3, this.r.b(view))) * f7));
    }

    @DexIgnore
    public final int b(int i2, int i3, int i4) {
        int i5;
        if (i2 == 0) {
            return 0;
        }
        int width = this.u.getWidth();
        float f2 = (float) (width / 2);
        float a2 = f2 + (a(Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width))) * f2);
        int abs = Math.abs(i3);
        if (abs > 0) {
            i5 = Math.round(Math.abs(a2 / ((float) abs)) * 1000.0f) * 4;
        } else {
            i5 = (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f);
        }
        return Math.min(i5, 600);
    }

    @DexIgnore
    public final int a(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        if (abs > i4) {
            return i2 > 0 ? i4 : -i4;
        }
        return i2;
    }

    @DexIgnore
    public final void b(int i2) {
        if (this.d != null && d(i2)) {
            this.d[i2] = 0.0f;
            this.e[i2] = 0.0f;
            this.f[i2] = 0.0f;
            this.g[i2] = 0.0f;
            this.h[i2] = 0;
            this.i[i2] = 0;
            this.j[i2] = 0;
            this.k = (~(1 << i2)) & this.k;
        }
    }

    @DexIgnore
    public final float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (abs > f4) {
            return f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? f4 : -f4;
        }
        return f2;
    }

    @DexIgnore
    public final float a(float f2) {
        return (float) Math.sin((double) ((f2 - 0.5f) * 0.47123894f));
    }

    @DexIgnore
    public boolean a(boolean z) {
        if (this.a == 2) {
            boolean computeScrollOffset = this.q.computeScrollOffset();
            int currX = this.q.getCurrX();
            int currY = this.q.getCurrY();
            int left = currX - this.s.getLeft();
            int top = currY - this.s.getTop();
            if (left != 0) {
                f9.c(this.s, left);
            }
            if (top != 0) {
                f9.d(this.s, top);
            }
            if (!(left == 0 && top == 0)) {
                this.r.a(this.s, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.q.getFinalX() && currY == this.q.getFinalY()) {
                this.q.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z) {
                    this.u.post(this.v);
                } else {
                    f(0);
                }
            }
        }
        if (this.a == 2) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00dd, code lost:
        if (r12 != r11) goto L_0x00e6;
     */
    @DexIgnore
    public boolean c(MotionEvent motionEvent) {
        boolean z;
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            b();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent2);
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked != 2) {
                    if (actionMasked != 3) {
                        if (actionMasked == 5) {
                            int pointerId = motionEvent2.getPointerId(actionIndex);
                            float x = motionEvent2.getX(actionIndex);
                            float y = motionEvent2.getY(actionIndex);
                            b(x, y, pointerId);
                            int i2 = this.a;
                            if (i2 == 0) {
                                int i3 = this.h[pointerId];
                                int i4 = this.p;
                                if ((i3 & i4) != 0) {
                                    this.r.b(i3 & i4, pointerId);
                                }
                            } else if (i2 == 2) {
                                View b2 = b((int) x, (int) y);
                                if (b2 == this.s) {
                                    b(b2, pointerId);
                                }
                            }
                        } else if (actionMasked == 6) {
                            b(motionEvent2.getPointerId(actionIndex));
                        }
                    }
                } else if (!(this.d == null || this.e == null)) {
                    int pointerCount = motionEvent.getPointerCount();
                    for (int i5 = 0; i5 < pointerCount; i5++) {
                        int pointerId2 = motionEvent2.getPointerId(i5);
                        if (e(pointerId2)) {
                            float x2 = motionEvent2.getX(i5);
                            float y2 = motionEvent2.getY(i5);
                            float f2 = x2 - this.d[pointerId2];
                            float f3 = y2 - this.e[pointerId2];
                            View b3 = b((int) x2, (int) y2);
                            boolean z2 = b3 != null && a(b3, f2, f3);
                            if (z2) {
                                int left = b3.getLeft();
                                int i6 = (int) f2;
                                int a2 = this.r.a(b3, left + i6, i6);
                                int top = b3.getTop();
                                int i7 = (int) f3;
                                int b4 = this.r.b(b3, top + i7, i7);
                                int a3 = this.r.a(b3);
                                int b5 = this.r.b(b3);
                                if (a3 != 0) {
                                    if (a3 > 0) {
                                    }
                                }
                                if (b5 != 0) {
                                    if (b5 > 0 && b4 == top) {
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                            a(f2, f3, pointerId2);
                            if (this.a != 1) {
                                if (z2 && b(b3, pointerId2)) {
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                    b(motionEvent);
                }
                z = false;
            }
            b();
            z = false;
        } else {
            float x3 = motionEvent.getX();
            float y3 = motionEvent.getY();
            z = false;
            int pointerId3 = motionEvent2.getPointerId(0);
            b(x3, y3, pointerId3);
            View b6 = b((int) x3, (int) y3);
            if (b6 == this.s && this.a == 2) {
                b(b6, pointerId3);
            }
            int i8 = this.h[pointerId3];
            int i9 = this.p;
            if ((i8 & i9) != 0) {
                this.r.b(i8 & i9, pointerId3);
            }
        }
        if (this.a == 1) {
            return true;
        }
        return z;
    }

    @DexIgnore
    public final void b(float f2, float f3, int i2) {
        c(i2);
        float[] fArr = this.d;
        this.f[i2] = f2;
        fArr[i2] = f2;
        float[] fArr2 = this.e;
        this.g[i2] = f3;
        fArr2[i2] = f3;
        this.h[i2] = c((int) f2, (int) f3);
        this.k |= 1 << i2;
    }

    @DexIgnore
    public final void b(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i2 = 0; i2 < pointerCount; i2++) {
            int pointerId = motionEvent.getPointerId(i2);
            if (e(pointerId)) {
                float x = motionEvent.getX(i2);
                float y = motionEvent.getY(i2);
                this.f[pointerId] = x;
                this.g[pointerId] = y;
            }
        }
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        this.t = true;
        this.r.a(this.s, f2, f3);
        this.t = false;
        if (this.a == 1) {
            f(0);
        }
    }

    @DexIgnore
    public boolean b(View view, int i2) {
        if (view == this.s && this.c == i2) {
            return true;
        }
        if (view == null || !this.r.b(view, i2)) {
            return false;
        }
        this.c = i2;
        a(view, i2);
        return true;
    }

    @DexIgnore
    public void a(MotionEvent motionEvent) {
        int i2;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            b();
        }
        if (this.l == null) {
            this.l = VelocityTracker.obtain();
        }
        this.l.addMovement(motionEvent);
        int i3 = 0;
        if (actionMasked == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int pointerId = motionEvent.getPointerId(0);
            View b2 = b((int) x, (int) y);
            b(x, y, pointerId);
            b(b2, pointerId);
            int i4 = this.h[pointerId];
            int i5 = this.p;
            if ((i4 & i5) != 0) {
                this.r.b(i4 & i5, pointerId);
            }
        } else if (actionMasked == 1) {
            if (this.a == 1) {
                h();
            }
            b();
        } else if (actionMasked != 2) {
            if (actionMasked == 3) {
                if (this.a == 1) {
                    a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                b();
            } else if (actionMasked == 5) {
                int pointerId2 = motionEvent.getPointerId(actionIndex);
                float x2 = motionEvent.getX(actionIndex);
                float y2 = motionEvent.getY(actionIndex);
                b(x2, y2, pointerId2);
                if (this.a == 0) {
                    b(b((int) x2, (int) y2), pointerId2);
                    int i6 = this.h[pointerId2];
                    int i7 = this.p;
                    if ((i6 & i7) != 0) {
                        this.r.b(i6 & i7, pointerId2);
                    }
                } else if (d((int) x2, (int) y2)) {
                    b(this.s, pointerId2);
                }
            } else if (actionMasked == 6) {
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                if (this.a == 1 && pointerId3 == this.c) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (true) {
                        if (i3 >= pointerCount) {
                            i2 = -1;
                            break;
                        }
                        int pointerId4 = motionEvent.getPointerId(i3);
                        if (pointerId4 != this.c) {
                            View b3 = b((int) motionEvent.getX(i3), (int) motionEvent.getY(i3));
                            View view = this.s;
                            if (b3 == view && b(view, pointerId4)) {
                                i2 = this.c;
                                break;
                            }
                        }
                        i3++;
                    }
                    if (i2 == -1) {
                        h();
                    }
                }
                b(pointerId3);
            }
        } else if (this.a != 1) {
            int pointerCount2 = motionEvent.getPointerCount();
            while (i3 < pointerCount2) {
                int pointerId5 = motionEvent.getPointerId(i3);
                if (e(pointerId5)) {
                    float x3 = motionEvent.getX(i3);
                    float y3 = motionEvent.getY(i3);
                    float f2 = x3 - this.d[pointerId5];
                    float f3 = y3 - this.e[pointerId5];
                    a(f2, f3, pointerId5);
                    if (this.a != 1) {
                        View b4 = b((int) x3, (int) y3);
                        if (a(b4, f2, f3) && b(b4, pointerId5)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i3++;
            }
            b(motionEvent);
        } else if (e(this.c)) {
            int findPointerIndex = motionEvent.findPointerIndex(this.c);
            float x4 = motionEvent.getX(findPointerIndex);
            float y4 = motionEvent.getY(findPointerIndex);
            float[] fArr = this.f;
            int i8 = this.c;
            int i9 = (int) (x4 - fArr[i8]);
            int i10 = (int) (y4 - this.g[i8]);
            a(this.s.getLeft() + i9, this.s.getTop() + i10, i9, i10);
            b(motionEvent);
        }
    }

    @DexIgnore
    public View b(int i2, int i3) {
        for (int childCount = this.u.getChildCount() - 1; childCount >= 0; childCount--) {
            ViewGroup viewGroup = this.u;
            this.r.a(childCount);
            View childAt = viewGroup.getChildAt(childCount);
            if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    public final int c(int i2, int i3) {
        int i4 = i2 < this.u.getLeft() + this.o ? 1 : 0;
        if (i3 < this.u.getTop() + this.o) {
            i4 |= 4;
        }
        if (i2 > this.u.getRight() - this.o) {
            i4 |= 2;
        }
        return i3 > this.u.getBottom() - this.o ? i4 | 8 : i4;
    }

    @DexIgnore
    public final void a(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.i;
            iArr[i2] = iArr[i2] | i3;
            this.r.a(i3, i2);
        }
    }

    @DexIgnore
    public final boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (this.p & i3) == 0 || (this.j[i2] & i3) == i3 || (this.i[i2] & i3) == i3) {
            return false;
        }
        int i4 = this.b;
        if (abs <= ((float) i4) && abs2 <= ((float) i4)) {
            return false;
        }
        if (abs < abs2 * 0.5f && this.r.b(i3)) {
            int[] iArr = this.j;
            iArr[i2] = iArr[i2] | i3;
            return false;
        } else if ((this.i[i2] & i3) != 0 || abs <= ((float) this.b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    public final boolean a(View view, float f2, float f3) {
        if (view == null) {
            return false;
        }
        boolean z = this.r.a(view) > 0;
        boolean z2 = this.r.b(view) > 0;
        if (z && z2) {
            int i2 = this.b;
            if ((f2 * f2) + (f3 * f3) > ((float) (i2 * i2))) {
                return true;
            }
            return false;
        } else if (z) {
            if (Math.abs(f2) > ((float) this.b)) {
                return true;
            }
            return false;
        } else if (!z2 || Math.abs(f3) <= ((float) this.b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    public boolean a(int i2) {
        int length = this.d.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (a(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        if (!d(i3)) {
            return false;
        }
        boolean z = (i2 & 1) == 1;
        boolean z2 = (i2 & 2) == 2;
        float f2 = this.f[i3] - this.d[i3];
        float f3 = this.g[i3] - this.e[i3];
        if (z && z2) {
            int i4 = this.b;
            if ((f2 * f2) + (f3 * f3) > ((float) (i4 * i4))) {
                return true;
            }
            return false;
        } else if (z) {
            if (Math.abs(f2) > ((float) this.b)) {
                return true;
            }
            return false;
        } else if (!z2 || Math.abs(f3) <= ((float) this.b)) {
            return false;
        } else {
            return true;
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        int left = this.s.getLeft();
        int top = this.s.getTop();
        if (i4 != 0) {
            i2 = this.r.a(this.s, i2, i4);
            f9.c(this.s, i2 - left);
        }
        int i6 = i2;
        if (i5 != 0) {
            i3 = this.r.b(this.s, i3, i5);
            f9.d(this.s, i3 - top);
        }
        int i7 = i3;
        if (i4 != 0 || i5 != 0) {
            this.r.a(this.s, i6, i7, i6 - left, i7 - top);
        }
    }

    @DexIgnore
    public boolean a(View view, int i2, int i3) {
        if (view != null && i2 >= view.getLeft() && i2 < view.getRight() && i3 >= view.getTop() && i3 < view.getBottom()) {
            return true;
        }
        return false;
    }
}
