package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zj4 {
    @DexIgnore
    public /* final */ sj4 a;

    @DexIgnore
    public zj4(sj4 sj4) {
        kd4.b(sj4, "ref");
        this.a = sj4;
    }

    @DexIgnore
    public String toString() {
        return "Removed[" + this.a + ']';
    }
}
