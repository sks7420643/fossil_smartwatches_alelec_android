package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b61 extends vb1<b61> {
    @DexIgnore
    public static volatile b61[] e;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public Long d; // = null;

    @DexIgnore
    public b61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static b61[] e() {
        if (e == null) {
            synchronized (zb1.b) {
                if (e == null) {
                    e = new b61[0];
                }
            }
        }
        return e;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        Long l = this.d;
        if (l != null) {
            ub1.b(2, l.longValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b61)) {
            return false;
        }
        b61 b61 = (b61) obj;
        Integer num = this.c;
        if (num == null) {
            if (b61.c != null) {
                return false;
            }
        } else if (!num.equals(b61.c)) {
            return false;
        }
        Long l = this.d;
        if (l == null) {
            if (b61.d != null) {
                return false;
            }
        } else if (!l.equals(b61.d)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(b61.b);
        }
        xb1 xb12 = b61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (b61.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        Long l = this.d;
        int hashCode3 = (hashCode2 + (l == null ? 0 : l.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        Long l = this.d;
        return l != null ? a + ub1.c(2, l.longValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 16) {
                this.d = Long.valueOf(tb1.f());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
