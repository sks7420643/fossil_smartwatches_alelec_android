package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.ExistingWorkPolicy;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jj {
    @DexIgnore
    @Deprecated
    public static jj a() {
        tj a = tj.a();
        if (a != null) {
            return a;
        }
        throw new IllegalStateException("WorkManager is not initialized properly.  The most likely cause is that you disabled WorkManagerInitializer in your manifest but forgot to call WorkManager#initialize in your Application#onCreate or a ContentProvider.");
    }

    @DexIgnore
    public abstract fj a(String str, ExistingWorkPolicy existingWorkPolicy, List<ej> list);

    @DexIgnore
    public static void a(Context context, xi xiVar) {
        tj.a(context, xiVar);
    }

    @DexIgnore
    public fj a(String str, ExistingWorkPolicy existingWorkPolicy, ej ejVar) {
        return a(str, existingWorkPolicy, (List<ej>) Collections.singletonList(ejVar));
    }
}
