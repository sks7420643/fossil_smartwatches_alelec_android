package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface u63 extends v52<t63> {
    @DexIgnore
    void F(String str);

    @DexIgnore
    void a(int i, int i2, String str, String str2);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(MicroApp microApp);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void g(String str);

    @DexIgnore
    void w(List<MicroApp> list);

    @DexIgnore
    void z(String str);
}
