package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn0 implements Parcelable.Creator<mn0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        IBinder iBinder = null;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                iBinder = SafeParcelReader.p(parcel, a);
            } else if (a2 == 3) {
                z = SafeParcelReader.i(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                z2 = SafeParcelReader.i(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new mn0(str, iBinder, z, z2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new mn0[i];
    }
}
