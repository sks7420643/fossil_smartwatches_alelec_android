package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cu implements gu<Bitmap, byte[]> {
    @DexIgnore
    public /* final */ Bitmap.CompressFormat a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public cu() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @DexIgnore
    public aq<byte[]> a(aq<Bitmap> aqVar, lo loVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        aqVar.get().compress(this.a, this.b, byteArrayOutputStream);
        aqVar.a();
        return new kt(byteArrayOutputStream.toByteArray());
    }

    @DexIgnore
    public cu(Bitmap.CompressFormat compressFormat, int i) {
        this.a = compressFormat;
        this.b = i;
    }
}
