package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerViewHeartRateCalendar q;

    @DexIgnore
    public mc2(Object obj, View view, int i, RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar) {
        super(obj, view, i);
        this.q = recyclerViewHeartRateCalendar;
    }
}
