package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iv0 extends mt0<Long> implements wu0<Long>, RandomAccess {
    @DexIgnore
    public long[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new iv0().z();
    }
    */

    @DexIgnore
    public iv0() {
        this(new long[10], 0);
    }

    @DexIgnore
    public iv0(long[] jArr, int i) {
        this.f = jArr;
        this.g = i;
    }

    @DexIgnore
    public final long a(int i) {
        f(i);
        return this.f[i];
    }

    @DexIgnore
    public final void a(int i, long j) {
        a();
        if (i >= 0) {
            int i2 = this.g;
            if (i <= i2) {
                long[] jArr = this.f;
                if (i2 < jArr.length) {
                    System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
                } else {
                    long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
                    System.arraycopy(jArr, 0, jArr2, 0, i);
                    System.arraycopy(this.f, i, jArr2, i + 1, this.g - i);
                    this.f = jArr2;
                }
                this.f[i] = j;
                this.g++;
                this.modCount++;
                return;
            }
        }
        throw new IndexOutOfBoundsException(g(i));
    }

    @DexIgnore
    public final void a(long j) {
        a(this.g, j);
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Long) obj).longValue());
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Long> collection) {
        a();
        tu0.a(collection);
        if (!(collection instanceof iv0)) {
            return super.addAll(collection);
        }
        iv0 iv0 = (iv0) collection;
        int i = iv0.g;
        if (i == 0) {
            return false;
        }
        int i2 = this.g;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f;
            if (i3 > jArr.length) {
                this.f = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(iv0.f, 0, this.f, this.g, iv0.g);
            this.g = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final /* synthetic */ wu0 c(int i) {
        if (i >= this.g) {
            return new iv0(Arrays.copyOf(this.f, i), this.g);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof iv0)) {
            return super.equals(obj);
        }
        iv0 iv0 = (iv0) obj;
        if (this.g != iv0.g) {
            return false;
        }
        long[] jArr = iv0.f;
        for (int i = 0; i < this.g; i++) {
            if (this.f[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final void f(int i) {
        if (i < 0 || i >= this.g) {
            throw new IndexOutOfBoundsException(g(i));
        }
    }

    @DexIgnore
    public final String g(int i) {
        int i2 = this.g;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(a(i));
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (i * 31) + tu0.a(this.f[i2]);
        }
        return i;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        f(i);
        long[] jArr = this.f;
        long j = jArr[i];
        int i2 = this.g;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, i2 - i);
        }
        this.g--;
        this.modCount++;
        return Long.valueOf(j);
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.g; i++) {
            if (obj.equals(Long.valueOf(this.f[i]))) {
                long[] jArr = this.f;
                System.arraycopy(jArr, i + 1, jArr, i, this.g - i);
                this.g--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            long[] jArr = this.f;
            System.arraycopy(jArr, i2, jArr, i, this.g - i2);
            this.g -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        a();
        f(i);
        long[] jArr = this.f;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }
}
