package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yr4<T> {
    @DexIgnore
    public yr4(qr4<T> qr4, Throwable th) {
    }

    @DexIgnore
    public static <T> yr4<T> a(Throwable th) {
        if (th != null) {
            return new yr4<>((qr4) null, th);
        }
        throw new NullPointerException("error == null");
    }

    @DexIgnore
    public static <T> yr4<T> a(qr4<T> qr4) {
        if (qr4 != null) {
            return new yr4<>(qr4, (Throwable) null);
        }
        throw new NullPointerException("response == null");
    }
}
