package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e90 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId a;
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public e90(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr, byte[] bArr2, int i, int i2) {
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "phoneRandomNumber");
        kd4.b(bArr2, "deviceRandomNumber");
        this.a = characteristicId;
        this.b = bArr;
        this.c = bArr2;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    public final void a(byte[] bArr, byte[] bArr2) {
        kd4.b(bArr, "phoneRandomNumber");
        kd4.b(bArr2, "deviceRandomNumber");
        if (!Arrays.equals(this.b, bArr) || !Arrays.equals(this.c, bArr2)) {
            this.d = 0;
        }
        this.b = bArr;
        this.c = bArr2;
        this.d++;
        this.e = 0;
    }

    @DexIgnore
    public final void a(int i) {
        this.e += i;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = new byte[8];
        byte[] bArr2 = new byte[8];
        bArr[0] = this.a.getSocketId$blesdk_productionRelease();
        System.arraycopy(this.b, 0, bArr, 2, 6);
        System.arraycopy(this.c, 0, bArr2, 1, 7);
        k90.b(bArr, this.d);
        k90.b(bArr2, this.e);
        return ya4.a(bArr, bArr2);
    }
}
