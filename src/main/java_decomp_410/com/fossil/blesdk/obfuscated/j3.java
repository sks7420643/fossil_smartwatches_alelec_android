package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j3 {
    @DexIgnore
    public abstract void a(Runnable runnable);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public void b(Runnable runnable) {
        if (a()) {
            runnable.run();
        } else {
            c(runnable);
        }
    }

    @DexIgnore
    public abstract void c(Runnable runnable);
}
