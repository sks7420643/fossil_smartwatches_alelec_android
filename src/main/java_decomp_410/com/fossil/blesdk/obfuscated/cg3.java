package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.chart.HeartRateSleepSessionChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cg3 extends RecyclerView.g<b> {
    @DexIgnore
    public ArrayList<a> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<qt3> a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ short c;
        @DexIgnore
        public /* final */ short d;

        @DexIgnore
        public a() {
            this((ArrayList) null, 0, 0, 0, 15, (fd4) null);
        }

        @DexIgnore
        public a(ArrayList<qt3> arrayList, int i, short s, short s2) {
            kd4.b(arrayList, "heartRateSessionData");
            this.a = arrayList;
            this.b = i;
            this.c = s;
            this.d = s2;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<qt3> b() {
            return this.a;
        }

        @DexIgnore
        public final short c() {
            return this.d;
        }

        @DexIgnore
        public final short d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (kd4.a((Object) this.a, (Object) aVar.a)) {
                        if (this.b == aVar.b) {
                            if (this.c == aVar.c) {
                                if (this.d == aVar.d) {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            ArrayList<qt3> arrayList = this.a;
            return ((((((arrayList != null ? arrayList.hashCode() : 0) * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return "SleepHeartRateUIData(heartRateSessionData=" + this.a + ", duration=" + this.b + ", minHR=" + this.c + ", maxHR=" + this.d + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(ArrayList arrayList, int i, short s, short s2, int i2, fd4 fd4) {
            this((i2 & 1) != 0 ? new ArrayList() : arrayList, (i2 & 2) != 0 ? 1 : i, (i2 & 4) != 0 ? 0 : s, (i2 & 8) != 0 ? 0 : s2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ HeartRateSleepSessionChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            kd4.b(view, "itemView");
            View findViewById = view.findViewById(R.id.hrssc);
            kd4.a((Object) findViewById, "itemView.findViewById(R.id.hrssc)");
            this.a = (HeartRateSleepSessionChart) findViewById;
        }

        @DexIgnore
        public final HeartRateSleepSessionChart a() {
            return this.a;
        }
    }

    @DexIgnore
    public cg3(ArrayList<a> arrayList) {
        kd4.b(arrayList, "data");
        this.a = arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        kd4.b(bVar, "holder");
        a aVar = this.a.get(i);
        kd4.a((Object) aVar, "data[position]");
        a aVar2 = aVar;
        bVar.a().setMDuration(aVar2.a());
        bVar.a().setMMinHRValue(aVar2.d());
        bVar.a().setMMaxHRValue(aVar2.c());
        bVar.a().a(aVar2.b());
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_heart_rate_sleep_session, viewGroup, false);
        kd4.a((Object) inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    public final void a(ArrayList<a> arrayList) {
        kd4.b(arrayList, "data");
        this.a = arrayList;
        notifyDataSetChanged();
    }
}
