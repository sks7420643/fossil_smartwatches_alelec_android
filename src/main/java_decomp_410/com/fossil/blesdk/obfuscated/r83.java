package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r83 implements Factory<ActiveTimeOverviewMonthPresenter> {
    @DexIgnore
    public static ActiveTimeOverviewMonthPresenter a(p83 p83, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActiveTimeOverviewMonthPresenter(p83, userRepository, summariesRepository);
    }
}
