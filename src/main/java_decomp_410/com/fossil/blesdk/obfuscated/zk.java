package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zk {
    @DexIgnore
    List<String> a(String str);

    @DexIgnore
    void a(yk ykVar);

    @DexIgnore
    boolean b(String str);

    @DexIgnore
    boolean c(String str);
}
