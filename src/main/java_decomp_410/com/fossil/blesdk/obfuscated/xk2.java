package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.enums.FitnessSourceType;
import com.portfolio.platform.enums.GoalType;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xk2 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((fd4) null);
    @DexIgnore
    public /* final */ en2 a;
    @DexIgnore
    public /* final */ ActivitySummaryDao b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(long j) {
            if (j < ((long) 70)) {
                return 0;
            }
            return j < ((long) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL) ? 1 : 2;
        }

        @DexIgnore
        public final int a(ActivitySummary activitySummary, GoalType goalType) {
            kd4.b(goalType, "goalType");
            int i = wk2.a[goalType.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return VideoUploader.RETRY_DELAY_UNIT_MS;
                    }
                    return activitySummary != null ? activitySummary.getCaloriesGoal() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
                } else if (activitySummary != null) {
                    return activitySummary.getActiveTimeGoal();
                } else {
                    return 30;
                }
            } else if (activitySummary != null) {
                return activitySummary.getStepGoal();
            } else {
                return VideoUploader.RETRY_DELAY_UNIT_MS;
            }
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final int a(MFSleepDay mFSleepDay) {
            if (mFSleepDay != null) {
                return mFSleepDay.getGoalMinutes();
            }
            return 480;
        }

        @DexIgnore
        public final int a(GoalTrackingSummary goalTrackingSummary) {
            if (goalTrackingSummary != null) {
                return goalTrackingSummary.getGoalTarget();
            }
            return 8;
        }

        @DexIgnore
        public final String a(String str, MFSleepSession mFSleepSession) {
            kd4.b(str, ButtonService.USER_ID);
            kd4.b(mFSleepSession, "sleepSession");
            String value = FitnessSourceType.values()[mFSleepSession.getSource()].getValue();
            kd4.a((Object) value, "FitnessSourceType.values\u2026leepSession.source].value");
            if (value != null) {
                String lowerCase = value.toLowerCase();
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return str + ":" + lowerCase + ":" + mFSleepSession.getRealEndTime();
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
    }

    /*
    static {
        String simpleName = xk2.class.getSimpleName();
        kd4.a((Object) simpleName, "FitnessHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public xk2(en2 en2, ActivitySummaryDao activitySummaryDao) {
        kd4.b(en2, "mSharedPreferencesManager");
        kd4.b(activitySummaryDao, "mActivitySummaryDao");
        this.a = en2;
        this.b = activitySummaryDao;
    }

    @DexIgnore
    public final List<ActivitySample> a(List<ActivitySample> list, String str) {
        List<ActivitySample> list2 = list;
        kd4.b(list2, "activitySamples");
        kd4.b(str, ButtonService.USER_ID);
        long a2 = a(new Date());
        long j = 0;
        long j2 = 0;
        for (ActivitySample next : list) {
            DateTime component4 = next.component4();
            j += (long) next.component5();
            j2 = component4.getMillis();
        }
        FLogger.INSTANCE.getLocal().d(c, "addRealTimeStepToActivitySample - steps=" + j + ", realTimeSteps=" + a2);
        if (j < a2) {
            try {
                ActivityIntensities activityIntensities = new ActivityIntensities();
                double d2 = (double) (a2 - j);
                activityIntensities.setIntensity(d2);
                long j3 = j2 + 1;
                Date date = new Date(j3);
                DateTime dateTime = new DateTime(j3);
                DateTime dateTime2 = new DateTime(j2 + ((long) 100));
                int a3 = rk2.a();
                String value = FitnessSourceType.Device.getValue();
                kd4.a((Object) value, "FitnessSourceType.Device.value");
                long j4 = j3;
                list2.add(new ActivitySample(str, date, dateTime, dateTime2, d2, 0.0d, 0.0d, 0, activityIntensities, a3, value, j4, j4, j4));
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(c, "addRealTimeStepToActivitySample - e=" + e);
                e.printStackTrace();
            }
        }
        return list2;
    }

    @DexIgnore
    public final float a(Date date, boolean z) {
        kd4.b(date, "date");
        boolean before = ml2.a(date).before(PortfolioApp.W.c().k());
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (before) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = c;
                local.d(str, "Inside " + c + ".getCurrentSteps, this date -" + date + "- before user signing up date, return null.");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        ActivitySummary activitySummary = this.b.getActivitySummary(date);
        if (activitySummary != null) {
            f = (float) activitySummary.getSteps();
        }
        Boolean s = rk2.s(date);
        kd4.a((Object) s, "DateHelper.isToday(date)");
        if (!s.booleanValue() || !z) {
            return f;
        }
        long a2 = a(date);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local2.d(str2, "getCurrentSteps - steps=" + f + ", realTimeSteps=" + a2);
        return Math.max((float) a2, f);
    }

    @DexIgnore
    public final void a(Date date, long j) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "saveRealTimeStep - date=" + date + ", steps=" + j);
        if (j >= 0) {
            String v = this.a.v();
            if (!TextUtils.isEmpty(v)) {
                String[] a2 = gp4.a(v, "_");
                if (a2 != null && a2.length == 2) {
                    try {
                        long parseLong = Long.parseLong(a2[0]);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = c;
                        local2.d(str2, "saveRealTimeStep - lastSampleRawTimeStamp=" + date + ", savedRealTimeStepTimeStamp=" + new Date(parseLong));
                        if (rk2.d(new Date(parseLong), date)) {
                            en2 en2 = this.a;
                            en2.v(String.valueOf(date.getTime()) + "_" + j);
                            return;
                        }
                        FLogger.INSTANCE.getLocal().d(c, "saveRealTimeStep - Different date, clear realTimeStepStamp");
                        this.a.v("");
                    } catch (Exception e) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = c;
                        local3.e(str3, "saveRealTimeStep - e=" + e);
                        e.printStackTrace();
                    }
                }
            } else {
                en2 en22 = this.a;
                en22.v(String.valueOf(date.getTime()) + "_" + j);
            }
        }
    }

    @DexIgnore
    public final long a(Date date) {
        kd4.b(date, "date");
        String v = this.a.v();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "getRealTimeStep - data=" + v + ", date=" + rk2.h(date));
        if (!TextUtils.isEmpty(v)) {
            String[] a2 = gp4.a(v, "_");
            if (a2 != null && a2.length == 2) {
                try {
                    if (rk2.d(date, new Date(Long.parseLong(a2[0])))) {
                        return Long.parseLong(a2[1]);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = c;
                    local2.e(str2, "getRealTimeStep - e=" + e);
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }
}
