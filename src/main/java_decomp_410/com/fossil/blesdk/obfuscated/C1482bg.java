package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bg */
public class C1482bg {
    @DexIgnore
    /* renamed from: a */
    public static android.database.Cursor m4959a(androidx.room.RoomDatabase roomDatabase, com.fossil.blesdk.obfuscated.C2125jg jgVar, boolean z) {
        android.database.Cursor query = roomDatabase.query(jgVar);
        if (!z || !(query instanceof android.database.AbstractWindowedCursor)) {
            return query;
        }
        android.database.AbstractWindowedCursor abstractWindowedCursor = (android.database.AbstractWindowedCursor) query;
        int count = abstractWindowedCursor.getCount();
        return (android.os.Build.VERSION.SDK_INT < 23 || (abstractWindowedCursor.hasWindow() ? abstractWindowedCursor.getWindow().getNumRows() : count) < count) ? com.fossil.blesdk.obfuscated.C1416ag.m4426a(abstractWindowedCursor) : query;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public static void m4960a(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        java.util.ArrayList<java.lang.String> arrayList = new java.util.ArrayList<>();
        android.database.Cursor d = ggVar.mo11232d("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        while (d.moveToNext()) {
            try {
                arrayList.add(d.getString(0));
            } catch (Throwable th) {
                d.close();
                throw th;
            }
        }
        d.close();
        for (java.lang.String str : arrayList) {
            if (str.startsWith("room_fts_content_sync_")) {
                ggVar.mo11230b("DROP TRIGGER IF EXISTS " + str);
            }
        }
    }
}
