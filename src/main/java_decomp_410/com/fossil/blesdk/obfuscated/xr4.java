package com.fossil.blesdk.obfuscated;

import io.reactivex.exceptions.CompositeException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xr4<T> extends o84<qr4<T>> {
    @DexIgnore
    public /* final */ Call<T> e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements y84 {
        @DexIgnore
        public /* final */ Call<?> e;
        @DexIgnore
        public volatile boolean f;

        @DexIgnore
        public a(Call<?> call) {
            this.e = call;
        }

        @DexIgnore
        public boolean a() {
            return this.f;
        }

        @DexIgnore
        public void dispose() {
            this.f = true;
            this.e.cancel();
        }
    }

    @DexIgnore
    public xr4(Call<T> call) {
        this.e = call;
    }

    @DexIgnore
    public void b(q84<? super qr4<T>> q84) {
        boolean z;
        Call<T> clone = this.e.clone();
        a aVar = new a(clone);
        q84.onSubscribe(aVar);
        if (!aVar.a()) {
            try {
                qr4<T> r = clone.r();
                if (!aVar.a()) {
                    q84.onNext(r);
                }
                if (!aVar.a()) {
                    try {
                        q84.onComplete();
                    } catch (Throwable th) {
                        th = th;
                        z = true;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                z = false;
                a94.b(th);
                if (z) {
                    ia4.b(th);
                } else if (!aVar.a()) {
                    try {
                        q84.onError(th);
                    } catch (Throwable th3) {
                        a94.b(th3);
                        ia4.b(new CompositeException(th, th3));
                    }
                }
            }
        }
    }
}
