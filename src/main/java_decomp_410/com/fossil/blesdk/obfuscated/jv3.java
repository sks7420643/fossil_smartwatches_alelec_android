package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cv3;
import com.squareup.okhttp.Protocol;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jv3 {
    @DexIgnore
    public /* final */ hv3 a;
    @DexIgnore
    public /* final */ Protocol b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ bv3 e;
    @DexIgnore
    public /* final */ cv3 f;
    @DexIgnore
    public /* final */ kv3 g;
    @DexIgnore
    public jv3 h;
    @DexIgnore
    public jv3 i;
    @DexIgnore
    public /* final */ jv3 j;
    @DexIgnore
    public volatile su3 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public hv3 a;
        @DexIgnore
        public Protocol b;
        @DexIgnore
        public int c;
        @DexIgnore
        public String d;
        @DexIgnore
        public bv3 e;
        @DexIgnore
        public cv3.b f;
        @DexIgnore
        public kv3 g;
        @DexIgnore
        public jv3 h;
        @DexIgnore
        public jv3 i;
        @DexIgnore
        public jv3 j;

        @DexIgnore
        public b() {
            this.c = -1;
            this.f = new cv3.b();
        }

        @DexIgnore
        public b a(hv3 hv3) {
            this.a = hv3;
            return this;
        }

        @DexIgnore
        public b b(String str, String str2) {
            this.f.d(str, str2);
            return this;
        }

        @DexIgnore
        public b c(jv3 jv3) {
            if (jv3 != null) {
                a("networkResponse", jv3);
            }
            this.h = jv3;
            return this;
        }

        @DexIgnore
        public b d(jv3 jv3) {
            if (jv3 != null) {
                b(jv3);
            }
            this.j = jv3;
            return this;
        }

        @DexIgnore
        public b a(Protocol protocol) {
            this.b = protocol;
            return this;
        }

        @DexIgnore
        public final void b(jv3 jv3) {
            if (jv3.g != null) {
                throw new IllegalArgumentException("priorResponse.body != null");
            }
        }

        @DexIgnore
        public b a(int i2) {
            this.c = i2;
            return this;
        }

        @DexIgnore
        public b(jv3 jv3) {
            this.c = -1;
            this.a = jv3.a;
            this.b = jv3.b;
            this.c = jv3.c;
            this.d = jv3.d;
            this.e = jv3.e;
            this.f = jv3.f.a();
            this.g = jv3.g;
            this.h = jv3.h;
            this.i = jv3.i;
            this.j = jv3.j;
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b a(bv3 bv3) {
            this.e = bv3;
            return this;
        }

        @DexIgnore
        public b a(String str, String str2) {
            this.f.a(str, str2);
            return this;
        }

        @DexIgnore
        public b a(cv3 cv3) {
            this.f = cv3.a();
            return this;
        }

        @DexIgnore
        public b a(kv3 kv3) {
            this.g = kv3;
            return this;
        }

        @DexIgnore
        public b a(jv3 jv3) {
            if (jv3 != null) {
                a("cacheResponse", jv3);
            }
            this.i = jv3;
            return this;
        }

        @DexIgnore
        public final void a(String str, jv3 jv3) {
            if (jv3.g != null) {
                throw new IllegalArgumentException(str + ".body != null");
            } else if (jv3.h != null) {
                throw new IllegalArgumentException(str + ".networkResponse != null");
            } else if (jv3.i != null) {
                throw new IllegalArgumentException(str + ".cacheResponse != null");
            } else if (jv3.j != null) {
                throw new IllegalArgumentException(str + ".priorResponse != null");
            }
        }

        @DexIgnore
        public jv3 a() {
            if (this.a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.b == null) {
                throw new IllegalStateException("protocol == null");
            } else if (this.c >= 0) {
                return new jv3(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.c);
            }
        }
    }

    @DexIgnore
    public Protocol k() {
        return this.b;
    }

    @DexIgnore
    public hv3 l() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "Response{protocol=" + this.b + ", code=" + this.c + ", message=" + this.d + ", url=" + this.a.i() + '}';
    }

    @DexIgnore
    public jv3(b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f.a();
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
    }

    @DexIgnore
    public String a(String str) {
        return a(str, (String) null);
    }

    @DexIgnore
    public su3 b() {
        su3 su3 = this.k;
        if (su3 != null) {
            return su3;
        }
        su3 a2 = su3.a(this.f);
        this.k = a2;
        return a2;
    }

    @DexIgnore
    public jv3 c() {
        return this.i;
    }

    @DexIgnore
    public List<vu3> d() {
        String str;
        int i2 = this.c;
        if (i2 == 401) {
            str = "WWW-Authenticate";
        } else if (i2 != 407) {
            return Collections.emptyList();
        } else {
            str = "Proxy-Authenticate";
        }
        return xw3.a(g(), str);
    }

    @DexIgnore
    public int e() {
        return this.c;
    }

    @DexIgnore
    public bv3 f() {
        return this.e;
    }

    @DexIgnore
    public cv3 g() {
        return this.f;
    }

    @DexIgnore
    public String h() {
        return this.d;
    }

    @DexIgnore
    public jv3 i() {
        return this.h;
    }

    @DexIgnore
    public b j() {
        return new b();
    }

    @DexIgnore
    public String a(String str, String str2) {
        String a2 = this.f.a(str);
        return a2 != null ? a2 : str2;
    }

    @DexIgnore
    public kv3 a() {
        return this.g;
    }
}
