package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.fi4;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ki4<J extends fi4> extends rg4 implements oh4, ai4 {
    @DexIgnore
    public /* final */ J h;

    @DexIgnore
    public ki4(J j) {
        kd4.b(j, "job");
        this.h = j;
    }

    @DexIgnore
    public qi4 a() {
        return null;
    }

    @DexIgnore
    public void dispose() {
        J j = this.h;
        if (j != null) {
            ((li4) j).b((ki4<?>) this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
    }

    @DexIgnore
    public boolean isActive() {
        return true;
    }
}
