package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestCoordinator;
import com.bumptech.glide.request.SingleRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wn<TranscodeType> extends lv<wn<TranscodeType>> implements Cloneable, un<wn<TranscodeType>> {
    @DexIgnore
    public /* final */ Context E;
    @DexIgnore
    public /* final */ xn F;
    @DexIgnore
    public /* final */ Class<TranscodeType> G;
    @DexIgnore
    public /* final */ tn H;
    @DexIgnore
    public yn<?, ? super TranscodeType> I;
    @DexIgnore
    public Object J;
    @DexIgnore
    public List<qv<TranscodeType>> K;
    @DexIgnore
    public wn<TranscodeType> L;
    @DexIgnore
    public wn<TranscodeType> M;
    @DexIgnore
    public Float N;
    @DexIgnore
    public boolean O; // = true;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ImageView.ScaleType.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[Priority.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /*
        static {
            try {
                b[Priority.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[Priority.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[Priority.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[Priority.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            a[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            try {
                a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError unused5) {
            }
        }
        */
    }

    /*
    static {
        rv rvVar = (rv) ((rv) ((rv) new rv().a(pp.b)).a(Priority.LOW)).a(true);
    }
    */

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public wn(rn rnVar, xn xnVar, Class<TranscodeType> cls, Context context) {
        this.F = xnVar;
        this.G = cls;
        this.E = context;
        this.I = xnVar.b(cls);
        this.H = rnVar.f();
        a(xnVar.g());
        a((lv) xnVar.h());
    }

    @DexIgnore
    public nv<TranscodeType> U() {
        return c(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    public wn<TranscodeType> b(qv<TranscodeType> qvVar) {
        this.K = null;
        return a(qvVar);
    }

    @DexIgnore
    public nv<TranscodeType> c(int i, int i2) {
        pv pvVar = new pv(i, i2);
        a(pvVar, pvVar, ow.a());
        return pvVar;
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void a(List<qv<Object>> list) {
        for (qv<Object> a2 : list) {
            a(a2);
        }
    }

    @DexIgnore
    public final wn<TranscodeType> b(Object obj) {
        this.J = obj;
        this.P = true;
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> clone() {
        wn<TranscodeType> wnVar = (wn) super.clone();
        wnVar.I = wnVar.I.clone();
        return wnVar;
    }

    @DexIgnore
    public wn<TranscodeType> a(lv<?> lvVar) {
        tw.a(lvVar);
        return (wn) super.a(lvVar);
    }

    @DexIgnore
    public final <Y extends bw<TranscodeType>> Y b(Y y, qv<TranscodeType> qvVar, lv<?> lvVar, Executor executor) {
        tw.a(y);
        if (this.P) {
            ov a2 = a(y, qvVar, lvVar, executor);
            ov d = y.d();
            if (!a2.a(d) || a(lvVar, d)) {
                this.F.a((bw<?>) y);
                y.a(a2);
                this.F.a(y, a2);
                return y;
            }
            tw.a(d);
            if (!d.isRunning()) {
                d.c();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    @DexIgnore
    public wn<TranscodeType> a(qv<TranscodeType> qvVar) {
        if (qvVar != null) {
            if (this.K == null) {
                this.K = new ArrayList();
            }
            this.K.add(qvVar);
        }
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> a(wn<TranscodeType> wnVar) {
        this.M = wnVar;
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> a(Object obj) {
        b(obj);
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> a(String str) {
        b((Object) str);
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> a(Uri uri) {
        b((Object) uri);
        return this;
    }

    @DexIgnore
    public wn<TranscodeType> a(Integer num) {
        b((Object) num);
        return a((lv) rv.b(gw.a(this.E)));
    }

    @DexIgnore
    public wn<TranscodeType> a(byte[] bArr) {
        b((Object) bArr);
        wn a2 = !F() ? a((lv) rv.b(pp.a)) : this;
        return !a2.J() ? a2.a((lv) rv.c(true)) : a2;
    }

    @DexIgnore
    public final Priority b(Priority priority) {
        int i = a.b[priority.ordinal()];
        if (i == 1) {
            return Priority.NORMAL;
        }
        if (i == 2) {
            return Priority.HIGH;
        }
        if (i == 3 || i == 4) {
            return Priority.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + x());
    }

    @DexIgnore
    public <Y extends bw<TranscodeType>> Y a(Y y) {
        a(y, (qv) null, ow.b());
        return y;
    }

    @DexIgnore
    public <Y extends bw<TranscodeType>> Y a(Y y, qv<TranscodeType> qvVar, Executor executor) {
        b(y, qvVar, this, executor);
        return y;
    }

    @DexIgnore
    public final boolean a(lv<?> lvVar, ov ovVar) {
        return !lvVar.G() && ovVar.f();
    }

    @DexIgnore
    public cw<ImageView, TranscodeType> a(ImageView imageView) {
        lv lvVar;
        uw.b();
        tw.a(imageView);
        if (!M() && K() && imageView.getScaleType() != null) {
            switch (a.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    lvVar = clone().P();
                    break;
                case 2:
                    lvVar = clone().Q();
                    break;
                case 3:
                case 4:
                case 5:
                    lvVar = clone().R();
                    break;
                case 6:
                    lvVar = clone().Q();
                    break;
            }
        }
        lvVar = this;
        cw<ImageView, TranscodeType> a2 = this.H.a(imageView, this.G);
        b(a2, (qv) null, lvVar, ow.b());
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r27v0, types: [com.fossil.blesdk.obfuscated.lv, com.fossil.blesdk.obfuscated.lv<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ov b(Object obj, bw<TranscodeType> bwVar, qv<TranscodeType> qvVar, RequestCoordinator requestCoordinator, yn<?, ? super TranscodeType> ynVar, Priority priority, int i, int i2, lv<?> r27, Executor executor) {
        Priority priority2;
        Object obj2 = obj;
        RequestCoordinator requestCoordinator2 = requestCoordinator;
        Priority priority3 = priority;
        wn<TranscodeType> wnVar = this.L;
        if (wnVar != null) {
            if (!this.Q) {
                yn<?, ? super TranscodeType> ynVar2 = wnVar.O ? ynVar : wnVar.I;
                if (this.L.H()) {
                    priority2 = this.L.x();
                } else {
                    priority2 = b(priority3);
                }
                Priority priority4 = priority2;
                int l = this.L.l();
                int k = this.L.k();
                if (uw.b(i, i2) && !this.L.N()) {
                    l = r27.l();
                    k = r27.k();
                }
                int i3 = k;
                tv tvVar = new tv(obj2, requestCoordinator2);
                Object obj3 = obj;
                bw<TranscodeType> bwVar2 = bwVar;
                qv<TranscodeType> qvVar2 = qvVar;
                tv tvVar2 = tvVar;
                ov a2 = a(obj3, bwVar2, qvVar2, (lv<?>) r27, (RequestCoordinator) tvVar, ynVar, priority, i, i2, executor);
                this.Q = true;
                wn<TranscodeType> wnVar2 = this.L;
                ov a3 = wnVar2.a(obj3, bwVar2, qvVar2, (RequestCoordinator) tvVar2, ynVar2, priority4, l, i3, (lv<?>) wnVar2, executor);
                this.Q = false;
                tvVar2.a(a2, a3);
                return tvVar2;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.N == null) {
            return a(obj, bwVar, qvVar, (lv<?>) r27, requestCoordinator, ynVar, priority, i, i2, executor);
        } else {
            tv tvVar3 = new tv(obj2, requestCoordinator2);
            bw<TranscodeType> bwVar3 = bwVar;
            qv<TranscodeType> qvVar3 = qvVar;
            tv tvVar4 = tvVar3;
            yn<?, ? super TranscodeType> ynVar3 = ynVar;
            int i4 = i;
            int i5 = i2;
            Executor executor2 = executor;
            tvVar3.a(a(obj, bwVar3, qvVar3, (lv<?>) r27, (RequestCoordinator) tvVar4, ynVar3, priority, i4, i5, executor2), a(obj, bwVar3, qvVar3, (lv<?>) r27.clone().a(this.N.floatValue()), (RequestCoordinator) tvVar4, ynVar3, b(priority3), i4, i5, executor2));
            return tvVar3;
        }
    }

    @DexIgnore
    public final ov a(bw<TranscodeType> bwVar, qv<TranscodeType> qvVar, lv<?> lvVar, Executor executor) {
        return a(new Object(), bwVar, qvVar, (RequestCoordinator) null, this.I, lvVar.x(), lvVar.l(), lvVar.k(), lvVar, executor);
    }

    @DexIgnore
    public final ov a(Object obj, bw<TranscodeType> bwVar, qv<TranscodeType> qvVar, RequestCoordinator requestCoordinator, yn<?, ? super TranscodeType> ynVar, Priority priority, int i, int i2, lv<?> lvVar, Executor executor) {
        mv mvVar;
        mv mvVar2;
        if (this.M != null) {
            mvVar2 = new mv(obj, requestCoordinator);
            mvVar = mvVar2;
        } else {
            Object obj2 = obj;
            mvVar = null;
            mvVar2 = requestCoordinator;
        }
        ov b = b(obj, bwVar, qvVar, mvVar2, ynVar, priority, i, i2, lvVar, executor);
        if (mvVar == null) {
            return b;
        }
        int l = this.M.l();
        int k = this.M.k();
        if (uw.b(i, i2) && !this.M.N()) {
            l = lvVar.l();
            k = lvVar.k();
        }
        wn<TranscodeType> wnVar = this.M;
        mv mvVar3 = mvVar;
        mvVar3.a(b, wnVar.a(obj, bwVar, qvVar, (RequestCoordinator) mvVar3, wnVar.I, wnVar.x(), l, k, (lv<?>) this.M, executor));
        return mvVar3;
    }

    @DexIgnore
    public final ov a(Object obj, bw<TranscodeType> bwVar, qv<TranscodeType> qvVar, lv<?> lvVar, RequestCoordinator requestCoordinator, yn<?, ? super TranscodeType> ynVar, Priority priority, int i, int i2, Executor executor) {
        Context context = this.E;
        tn tnVar = this.H;
        return SingleRequest.a(context, tnVar, obj, this.J, this.G, lvVar, i, i2, priority, bwVar, qvVar, this.K, requestCoordinator, tnVar.d(), ynVar.a(), executor);
    }
}
