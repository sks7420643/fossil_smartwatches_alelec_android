package com.fossil.blesdk.obfuscated;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v14 {
    @DexIgnore
    public static x14 c;
    @DexIgnore
    public static t14 d; // = e24.b();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();
    @DexIgnore
    public Integer a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public v14(Context context) {
        try {
            a(context);
            this.a = e24.p(context.getApplicationContext());
            this.b = t04.a(context).b();
        } catch (Throwable th) {
            d.a(th);
        }
    }

    @DexIgnore
    public static synchronized x14 a(Context context) {
        x14 x14;
        synchronized (v14.class) {
            if (c == null) {
                c = new x14(context.getApplicationContext());
            }
            x14 = c;
        }
        return x14;
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        Object obj;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            j24.a(jSONObject2, "cn", this.b);
            if (this.a != null) {
                jSONObject2.put("tn", this.a);
            }
            if (thread == null) {
                str = "ev";
                obj = jSONObject2;
            } else {
                str = "errkv";
                obj = jSONObject2.toString();
            }
            jSONObject.put(str, obj);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.a(th);
        }
    }
}
