package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p4 */
public class C2627p4 extends com.fossil.blesdk.obfuscated.C2459n4 {
    @DexIgnore
    public C2627p4(com.fossil.blesdk.obfuscated.C2526o4 o4Var) {
        super(o4Var);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13840a(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        super.mo13840a(solverVariable);
        solverVariable.f647j--;
    }
}
