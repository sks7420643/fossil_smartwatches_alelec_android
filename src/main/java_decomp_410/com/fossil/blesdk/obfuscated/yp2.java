package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yp2 implements MembersInjector<WatchAppCommuteTimeManager> {
    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, PortfolioApp portfolioApp) {
        watchAppCommuteTimeManager.a = portfolioApp;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, ApiServiceV2 apiServiceV2) {
        watchAppCommuteTimeManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, LocationSource locationSource) {
        watchAppCommuteTimeManager.c = locationSource;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, UserRepository userRepository) {
        watchAppCommuteTimeManager.d = userRepository;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, en2 en2) {
        watchAppCommuteTimeManager.e = en2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, rc rcVar) {
        watchAppCommuteTimeManager.f = rcVar;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, lr2 lr2) {
        watchAppCommuteTimeManager.g = lr2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, DianaPresetRepository dianaPresetRepository) {
        watchAppCommuteTimeManager.h = dianaPresetRepository;
    }
}
