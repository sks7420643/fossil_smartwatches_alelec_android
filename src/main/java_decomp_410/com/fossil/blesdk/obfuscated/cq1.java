package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cq1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<cq1> CREATOR; // = new dq1();
    @DexIgnore
    public byte e;
    @DexIgnore
    public /* final */ byte f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public cq1(byte b, byte b2, String str) {
        this.e = b;
        this.f = b2;
        this.g = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || cq1.class != obj.getClass()) {
            return false;
        }
        cq1 cq1 = (cq1) obj;
        return this.e == cq1.e && this.f == cq1.f && this.g.equals(cq1.g);
    }

    @DexIgnore
    public final int hashCode() {
        return ((((this.e + 31) * 31) + this.f) * 31) + this.g.hashCode();
    }

    @DexIgnore
    public final String toString() {
        byte b = this.e;
        byte b2 = this.f;
        String str = this.g;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 73);
        sb.append("AmsEntityUpdateParcelable{, mEntityId=");
        sb.append(b);
        sb.append(", mAttributeId=");
        sb.append(b2);
        sb.append(", mValue='");
        sb.append(str);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e);
        kk0.a(parcel, 3, this.f);
        kk0.a(parcel, 4, this.g, false);
        kk0.a(parcel, a);
    }
}
