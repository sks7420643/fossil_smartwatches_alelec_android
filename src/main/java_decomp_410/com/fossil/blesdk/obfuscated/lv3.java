package com.fossil.blesdk.obfuscated;

import java.net.InetSocketAddress;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lv3 {
    @DexIgnore
    public /* final */ pu3 a;
    @DexIgnore
    public /* final */ Proxy b;
    @DexIgnore
    public /* final */ InetSocketAddress c;

    @DexIgnore
    public lv3(pu3 pu3, Proxy proxy, InetSocketAddress inetSocketAddress) {
        if (pu3 == null) {
            throw new NullPointerException("address == null");
        } else if (proxy == null) {
            throw new NullPointerException("proxy == null");
        } else if (inetSocketAddress != null) {
            this.a = pu3;
            this.b = proxy;
            this.c = inetSocketAddress;
        } else {
            throw new NullPointerException("inetSocketAddress == null");
        }
    }

    @DexIgnore
    public pu3 a() {
        return this.a;
    }

    @DexIgnore
    public Proxy b() {
        return this.b;
    }

    @DexIgnore
    public InetSocketAddress c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.e != null && this.b.type() == Proxy.Type.HTTP;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof lv3)) {
            return false;
        }
        lv3 lv3 = (lv3) obj;
        if (!this.a.equals(lv3.a) || !this.b.equals(lv3.b) || !this.c.equals(lv3.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }
}
