package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pq */
public class C2669pq implements com.fossil.blesdk.obfuscated.C2149jq {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ android.graphics.Bitmap.Config f8419j; // = android.graphics.Bitmap.Config.ARGB_8888;

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2762qq f8420a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Set<android.graphics.Bitmap.Config> f8421b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2669pq.C2670a f8422c;

    @DexIgnore
    /* renamed from: d */
    public long f8423d;

    @DexIgnore
    /* renamed from: e */
    public long f8424e;

    @DexIgnore
    /* renamed from: f */
    public int f8425f;

    @DexIgnore
    /* renamed from: g */
    public int f8426g;

    @DexIgnore
    /* renamed from: h */
    public int f8427h;

    @DexIgnore
    /* renamed from: i */
    public int f8428i;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.pq$a */
    public interface C2670a {
        @DexIgnore
        /* renamed from: a */
        void mo14824a(android.graphics.Bitmap bitmap);

        @DexIgnore
        /* renamed from: b */
        void mo14825b(android.graphics.Bitmap bitmap);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pq$b")
    /* renamed from: com.fossil.blesdk.obfuscated.pq$b */
    public static final class C2671b implements com.fossil.blesdk.obfuscated.C2669pq.C2670a {
        @DexIgnore
        /* renamed from: a */
        public void mo14824a(android.graphics.Bitmap bitmap) {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14825b(android.graphics.Bitmap bitmap) {
        }
    }

    @DexIgnore
    public C2669pq(long j, com.fossil.blesdk.obfuscated.C2762qq qqVar, java.util.Set<android.graphics.Bitmap.Config> set) {
        this.f8423d = j;
        this.f8420a = qqVar;
        this.f8421b = set;
        this.f8422c = new com.fossil.blesdk.obfuscated.C2669pq.C2671b();
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: f */
    public static java.util.Set<android.graphics.Bitmap.Config> m12354f() {
        java.util.HashSet hashSet = new java.util.HashSet(java.util.Arrays.asList(android.graphics.Bitmap.Config.values()));
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            hashSet.add((java.lang.Object) null);
        }
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            hashSet.remove(android.graphics.Bitmap.Config.HARDWARE);
        }
        return java.util.Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    /* renamed from: g */
    public static com.fossil.blesdk.obfuscated.C2762qq m12355g() {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return new com.fossil.blesdk.obfuscated.C2906sq();
        }
        return new com.fossil.blesdk.obfuscated.C1965hq();
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo12446a(android.graphics.Bitmap bitmap) {
        if (bitmap != null) {
            try {
                if (!bitmap.isRecycled()) {
                    if (bitmap.isMutable() && ((long) this.f8420a.mo11709b(bitmap)) <= this.f8423d) {
                        if (this.f8421b.contains(bitmap.getConfig())) {
                            int b = this.f8420a.mo11709b(bitmap);
                            this.f8420a.mo11708a(bitmap);
                            this.f8422c.mo14825b(bitmap);
                            this.f8427h++;
                            this.f8424e += (long) b;
                            if (android.util.Log.isLoggable("LruBitmapPool", 2)) {
                                android.util.Log.v("LruBitmapPool", "Put bitmap in pool=" + this.f8420a.mo11711c(bitmap));
                            }
                            mo14819b();
                            mo14822d();
                            return;
                        }
                    }
                    if (android.util.Log.isLoggable("LruBitmapPool", 2)) {
                        android.util.Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.f8420a.mo11711c(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.f8421b.contains(bitmap.getConfig()));
                    }
                    bitmap.recycle();
                    return;
                }
                throw new java.lang.IllegalStateException("Cannot pool recycled bitmap");
            } catch (Throwable th) {
                throw th;
            }
        } else {
            throw new java.lang.NullPointerException("Bitmap must not be null");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.Bitmap mo12447b(int i, int i2, android.graphics.Bitmap.Config config) {
        android.graphics.Bitmap c = mo14820c(i, i2, config);
        return c == null ? m12353d(i, i2, config) : c;
    }

    @DexIgnore
    /* renamed from: c */
    public final synchronized android.graphics.Bitmap mo14820c(int i, int i2, android.graphics.Bitmap.Config config) {
        android.graphics.Bitmap a;
        m12350a(config);
        a = this.f8420a.mo11707a(i, i2, config != null ? config : f8419j);
        if (a == null) {
            if (android.util.Log.isLoggable("LruBitmapPool", 3)) {
                android.util.Log.d("LruBitmapPool", "Missing bitmap=" + this.f8420a.mo11710b(i, i2, config));
            }
            this.f8426g++;
        } else {
            this.f8425f++;
            this.f8424e -= (long) this.f8420a.mo11709b(a);
            this.f8422c.mo14824a(a);
            m12352c(a);
        }
        if (android.util.Log.isLoggable("LruBitmapPool", 2)) {
            android.util.Log.v("LruBitmapPool", "Get bitmap=" + this.f8420a.mo11710b(i, i2, config));
        }
        mo14819b();
        return a;
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo14822d() {
        mo14818a(this.f8423d);
    }

    @DexIgnore
    /* renamed from: e */
    public long mo14823e() {
        return this.f8423d;
    }

    @DexIgnore
    /* renamed from: d */
    public static android.graphics.Bitmap m12353d(int i, int i2, android.graphics.Bitmap.Config config) {
        if (config == null) {
            config = f8419j;
        }
        return android.graphics.Bitmap.createBitmap(i, i2, config);
    }

    @DexIgnore
    @android.annotation.TargetApi(19)
    /* renamed from: b */
    public static void m12351b(android.graphics.Bitmap bitmap) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14819b() {
        if (android.util.Log.isLoggable("LruBitmapPool", 2)) {
            mo14821c();
        }
    }

    @DexIgnore
    public C2669pq(long j) {
        this(j, m12355g(), m12354f());
    }

    @DexIgnore
    /* renamed from: c */
    public static void m12352c(android.graphics.Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        m12351b(bitmap);
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo14821c() {
        android.util.Log.v("LruBitmapPool", "Hits=" + this.f8425f + ", misses=" + this.f8426g + ", puts=" + this.f8427h + ", evictions=" + this.f8428i + ", currentSize=" + this.f8424e + ", maxSize=" + this.f8423d + "\nStrategy=" + this.f8420a);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo12443a(int i, int i2, android.graphics.Bitmap.Config config) {
        android.graphics.Bitmap c = mo14820c(i, i2, config);
        if (c == null) {
            return m12353d(i, i2, config);
        }
        c.eraseColor(0);
        return c;
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: a */
    public static void m12350a(android.graphics.Bitmap.Config config) {
        if (android.os.Build.VERSION.SDK_INT >= 26 && config == android.graphics.Bitmap.Config.HARDWARE) {
            throw new java.lang.IllegalArgumentException("Cannot create a mutable Bitmap with config: " + config + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12444a() {
        if (android.util.Log.isLoggable("LruBitmapPool", 3)) {
            android.util.Log.d("LruBitmapPool", "clearMemory");
        }
        mo14818a(0);
    }

    @DexIgnore
    @android.annotation.SuppressLint({"InlinedApi"})
    /* renamed from: a */
    public void mo12445a(int i) {
        if (android.util.Log.isLoggable("LruBitmapPool", 3)) {
            android.util.Log.d("LruBitmapPool", "trimMemory, level=" + i);
        }
        if (i >= 40 || (android.os.Build.VERSION.SDK_INT >= 23 && i >= 20)) {
            mo12444a();
        } else if (i >= 20 || i == 15) {
            mo14818a(mo14823e() / 2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized void mo14818a(long j) {
        while (this.f8424e > j) {
            android.graphics.Bitmap a = this.f8420a.mo11706a();
            if (a == null) {
                if (android.util.Log.isLoggable("LruBitmapPool", 5)) {
                    android.util.Log.w("LruBitmapPool", "Size mismatch, resetting");
                    mo14821c();
                }
                this.f8424e = 0;
                return;
            }
            this.f8422c.mo14824a(a);
            this.f8424e -= (long) this.f8420a.mo11709b(a);
            this.f8428i++;
            if (android.util.Log.isLoggable("LruBitmapPool", 3)) {
                android.util.Log.d("LruBitmapPool", "Evicting bitmap=" + this.f8420a.mo11711c(a));
            }
            mo14819b();
            a.recycle();
        }
    }
}
