package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xj */
public class C3293xj implements com.fossil.blesdk.obfuscated.C2419mj {

    @DexIgnore
    /* renamed from: h */
    public static /* final */ java.lang.String f10989h; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("CommandHandler");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f10990e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C2419mj> f10991f; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.Object f10992g; // = new java.lang.Object();

    @DexIgnore
    public C3293xj(android.content.Context context) {
        this.f10990e = context;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.Intent m16360a(android.content.Context context, java.lang.String str) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.Intent m16364b(android.content.Context context, java.lang.String str) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    /* renamed from: c */
    public static android.content.Intent m16365c(android.content.Context context, java.lang.String str) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo17763d(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handling reschedule %s, %s", new java.lang.Object[]{intent, java.lang.Integer.valueOf(i)}), new java.lang.Throwable[0]);
        akVar.mo8769e().mo16465j();
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo17764e(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        java.lang.String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handling schedule work for %s", new java.lang.Object[]{string}), new java.lang.Throwable[0]);
        androidx.work.impl.WorkDatabase g = akVar.mo8769e().mo16462g();
        g.beginTransaction();
        try {
            com.fossil.blesdk.obfuscated.C1954hl e = g.mo3780d().mo12029e(string);
            if (e == null) {
                com.fossil.blesdk.obfuscated.C1635dj a = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                java.lang.String str = f10989h;
                a.mo9966e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new java.lang.Throwable[0]);
            } else if (e.f5772b.isFinished()) {
                com.fossil.blesdk.obfuscated.C1635dj a2 = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                java.lang.String str2 = f10989h;
                a2.mo9966e(str2, "Skipping scheduling " + string + "because it is finished.", new java.lang.Throwable[0]);
                g.endTransaction();
            } else {
                long a3 = e.mo11670a();
                if (!e.mo11671b()) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Setting up Alarms for %s at %s", new java.lang.Object[]{string, java.lang.Long.valueOf(a3)}), new java.lang.Throwable[0]);
                    com.fossil.blesdk.obfuscated.C3226wj.m15852a(this.f10990e, akVar.mo8769e(), string, a3);
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Opportunistically setting an alarm for %s at %s", new java.lang.Object[]{string, java.lang.Long.valueOf(a3)}), new java.lang.Throwable[0]);
                    com.fossil.blesdk.obfuscated.C3226wj.m15852a(this.f10990e, akVar.mo8769e(), string, a3);
                    akVar.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(akVar, m16359a(this.f10990e), i));
                }
                g.setTransactionSuccessful();
                g.endTransaction();
            }
        } finally {
            g.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo17765f(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        java.lang.String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handing stopWork work for %s", new java.lang.Object[]{string}), new java.lang.Throwable[0]);
        akVar.mo8769e().mo16457b(string);
        com.fossil.blesdk.obfuscated.C3226wj.m15851a(this.f10990e, akVar.mo8769e(), string);
        akVar.mo3804a(string, false);
    }

    @DexIgnore
    /* renamed from: g */
    public void mo17766g(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        java.lang.String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            mo17759a(intent, i, akVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            mo17763d(intent, i, akVar);
        } else if (!m16362a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f10989h, java.lang.String.format("Invalid request for %s, requires %s.", new java.lang.Object[]{action, "KEY_WORKSPEC_ID"}), new java.lang.Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            mo17764e(intent, i, akVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            mo17761b(intent, i, akVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            mo17765f(intent, i, akVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            mo17762c(intent, i, akVar);
        } else {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9966e(f10989h, java.lang.String.format("Ignoring intent %s", new java.lang.Object[]{intent}), new java.lang.Throwable[0]);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.Intent m16359a(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.Intent m16363b(android.content.Context context) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo17762c(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        android.os.Bundle extras = intent.getExtras();
        java.lang.String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handling onExecutionCompleted %s, %s", new java.lang.Object[]{intent, java.lang.Integer.valueOf(i)}), new java.lang.Throwable[0]);
        mo3804a(string, z);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.Intent m16361a(android.content.Context context, java.lang.String str, boolean z) {
        android.content.Intent intent = new android.content.Intent(context, androidx.work.impl.background.systemalarm.SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo17761b(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        android.os.Bundle extras = intent.getExtras();
        synchronized (this.f10992g) {
            java.lang.String string = extras.getString("KEY_WORKSPEC_ID");
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handing delay met for %s", new java.lang.Object[]{string}), new java.lang.Throwable[0]);
            if (!this.f10991f.containsKey(string)) {
                com.fossil.blesdk.obfuscated.C3441zj zjVar = new com.fossil.blesdk.obfuscated.C3441zj(this.f10990e, i, string, akVar);
                this.f10991f.put(string, zjVar);
                zjVar.mo18505b();
            } else {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", new java.lang.Object[]{string}), new java.lang.Throwable[0]);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3804a(java.lang.String str, boolean z) {
        synchronized (this.f10992g) {
            com.fossil.blesdk.obfuscated.C2419mj remove = this.f10991f.remove(str);
            if (remove != null) {
                remove.mo3804a(str, z);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17760a() {
        boolean z;
        synchronized (this.f10992g) {
            z = !this.f10991f.isEmpty();
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17759a(android.content.Intent intent, int i, com.fossil.blesdk.obfuscated.C1421ak akVar) {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10989h, java.lang.String.format("Handling constraints changed %s", new java.lang.Object[]{intent}), new java.lang.Throwable[0]);
        new com.fossil.blesdk.obfuscated.C3367yj(this.f10990e, i, akVar).mo18124a();
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m16362a(android.os.Bundle bundle, java.lang.String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (java.lang.String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
