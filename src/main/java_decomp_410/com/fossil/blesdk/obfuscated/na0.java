package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class na0 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<na0> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public na0 createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new na0(parcel, (fd4) null);
        }

        @DexIgnore
        public na0[] newArray(int i) {
            return new na0[i];
        }
    }

    @DexIgnore
    public /* synthetic */ na0(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final String a() {
        return this.f;
    }

    @DexIgnore
    public final String b() {
        return this.g;
    }

    @DexIgnore
    public final String c() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.e);
        }
        if (parcel != null) {
            parcel.writeString(this.f);
        }
        if (parcel != null) {
            parcel.writeString(this.g);
        }
    }

    @DexIgnore
    public na0(String str, String str2, String str3) {
        kd4.b(str, "url");
        kd4.b(str2, "accessKey");
        kd4.b(str3, "secretKey");
        this.e = str;
        this.f = str2;
        this.g = str3;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public na0(Parcel parcel) {
        this(r0, r2, r4);
        String readString = parcel.readString();
        if (readString != null) {
            String readString2 = parcel.readString();
            if (readString2 != null) {
                String readString3 = parcel.readString();
                if (readString3 != null) {
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
