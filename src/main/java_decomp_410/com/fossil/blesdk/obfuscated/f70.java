package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class f70 extends e70 {
    @DexIgnore
    public /* final */ byte[] A; // = new byte[0];
    @DexIgnore
    public byte[] B; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] C; // = new byte[0];
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public /* final */ boolean F; // = true;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f70(RequestId requestId, Peripheral peripheral, int i) {
        super(requestId, peripheral, i);
        kd4.b(requestId, "id");
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public final BluetoothCommand A() {
        ByteBuffer put = ByteBuffer.allocate(D().length + C().length).order(ByteOrder.LITTLE_ENDIAN).put(D()).put(C());
        GattCharacteristic.CharacteristicId B2 = B();
        byte[] array = put.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return new n10(B2, array, i().h());
    }

    @DexIgnore
    public abstract GattCharacteristic.CharacteristicId B();

    @DexIgnore
    public byte[] C() {
        return this.C;
    }

    @DexIgnore
    public byte[] D() {
        return this.A;
    }

    @DexIgnore
    public abstract GattCharacteristic.CharacteristicId E();

    @DexIgnore
    public boolean F() {
        return this.F;
    }

    @DexIgnore
    public byte[] G() {
        return this.B;
    }

    @DexIgnore
    public final boolean H() {
        return this.D;
    }

    @DexIgnore
    public final void a(byte b) {
        o70 b2 = b(b);
        b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.Companion.a(b2).getResultCode(), (BluetoothCommand.Result) null, b2, 5, (Object) null));
    }

    @DexIgnore
    public abstract o70 b(byte b);

    @DexIgnore
    public final void b(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
        e(c20);
        if (this.D && this.E) {
            a(n());
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        this.E = z;
    }

    @DexIgnore
    public boolean c(c20 c20) {
        kd4.b(c20, "characteristicChangeNotification");
        return false;
    }

    @DexIgnore
    public final void d(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.D = true;
        SdkLogEntry l = l();
        if (l != null) {
            l.setSuccess(true);
        }
        SdkLogEntry l2 = l();
        if (l2 != null) {
            JSONObject extraData = l2.getExtraData();
            if (extraData != null) {
                wa0.a(extraData, JSONKey.MESSAGE, Request.Result.ResultCode.SUCCESS.getLogName$blesdk_productionRelease());
            }
        }
        if (n().getResultCode() == Request.Result.ResultCode.NOT_START) {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.Companion.a(bluetoothCommand.e()).getResultCode(), bluetoothCommand.e(), (o70) null, 9, (Object) null));
            if (n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
                a(bluetoothCommand);
            }
        }
        SdkLogEntry l3 = l();
        if (l3 != null) {
            l3.setSuccess(true);
        }
        SdkLogEntry l4 = l();
        if (l4 != null) {
            JSONObject extraData2 = l4.getExtraData();
            if (extraData2 != null) {
                wa0.a(extraData2, JSONKey.MESSAGE, Request.Result.ResultCode.SUCCESS.getLogName$blesdk_productionRelease());
            }
        }
        y();
        if (this.E) {
            a(n());
        }
    }

    @DexIgnore
    public final void e(c20 c20) {
        if (d(c20)) {
            f(c20);
        } else if (c(c20)) {
            g(c20);
        }
    }

    @DexIgnore
    public void f(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
        byte[] b = c20.b();
        JSONObject jSONObject = new JSONObject();
        if (F()) {
            a(b[G().length]);
            if (n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
                jSONObject = a(ya4.a(b, G().length + 1, b.length));
            } else {
                this.E = true;
            }
        } else {
            jSONObject = a(ya4.a(b, G().length, b.length));
        }
        a(new Request.ResponseInfo(0, c20.a(), b, jSONObject, 1, (fd4) null));
    }

    @DexIgnore
    public void g(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        this.E = true;
        return new JSONObject();
    }

    @DexIgnore
    public final void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        super.a(bluetoothCommand);
    }

    @DexIgnore
    public final boolean d(c20 c20) {
        if (c20.a() == E() && c20.b().length >= G().length) {
            if (Arrays.equals(G(), ya4.a(c20.b(), 0, G().length))) {
                return true;
            }
        }
        return false;
    }
}
