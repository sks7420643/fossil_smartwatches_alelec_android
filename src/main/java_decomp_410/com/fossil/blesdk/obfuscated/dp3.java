package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dp3 implements Factory<SplashPresenter> {
    @DexIgnore
    public static SplashPresenter a(ap3 ap3, UserRepository userRepository, MigrationHelper migrationHelper, en2 en2) {
        return new SplashPresenter(ap3, userRepository, migrationHelper, en2);
    }
}
