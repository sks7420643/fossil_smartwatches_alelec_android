package com.fossil.blesdk.obfuscated;

import com.facebook.AccessToken;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t74 implements d84 {
    @DexIgnore
    public b84 a(n54 n54, JSONObject jSONObject) throws JSONException {
        int optInt = jSONObject.optInt("settings_version", 0);
        int optInt2 = jSONObject.optInt("cache_duration", 3600);
        return new b84(a(n54, (long) optInt2, jSONObject), b(jSONObject.getJSONObject("app")), g(jSONObject.getJSONObject(Constants.SESSION)), f(jSONObject.getJSONObject("prompt")), d(jSONObject.getJSONObject("features")), a(jSONObject.getJSONObject("analytics")), c(jSONObject.getJSONObject("beta")), optInt, optInt2);
    }

    @DexIgnore
    public final m74 b(JSONObject jSONObject) throws JSONException {
        return new m74(jSONObject.getString("identifier"), jSONObject.getString("status"), jSONObject.getString("url"), jSONObject.getString("reports_url"), jSONObject.getString("ndk_reports_url"), jSONObject.optBoolean("update_required", false), (!jSONObject.has("icon") || !jSONObject.getJSONObject("icon").has("hash")) ? null : e(jSONObject.getJSONObject("icon")));
    }

    @DexIgnore
    public final o74 c(JSONObject jSONObject) throws JSONException {
        return new o74(jSONObject.optString("update_endpoint", c84.a), jSONObject.optInt("update_suspend_duration", 3600));
    }

    @DexIgnore
    public final v74 d(JSONObject jSONObject) {
        return new v74(jSONObject.optBoolean("prompt_enabled", false), jSONObject.optBoolean("collect_logged_exceptions", true), jSONObject.optBoolean("collect_reports", true), jSONObject.optBoolean("collect_analytics", false), jSONObject.optBoolean("firebase_crashlytics_enabled", false));
    }

    @DexIgnore
    public final k74 e(JSONObject jSONObject) throws JSONException {
        return new k74(jSONObject.getString("hash"), jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    @DexIgnore
    public final x74 f(JSONObject jSONObject) throws JSONException {
        return new x74(jSONObject.optString("title", "Send Crash Report?"), jSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report."), jSONObject.optString("send_button_title", "Send"), jSONObject.optBoolean("show_cancel_button", true), jSONObject.optString("cancel_button_title", "Don't Send"), jSONObject.optBoolean("show_always_send_button", true), jSONObject.optString("always_send_button_title", "Always Send"));
    }

    @DexIgnore
    public final y74 g(JSONObject jSONObject) throws JSONException {
        return new y74(jSONObject.optInt("log_buffer_size", 64000), jSONObject.optInt("max_chained_exception_depth", 8), jSONObject.optInt("max_custom_exception_events", 64), jSONObject.optInt("max_custom_key_value_pairs", 64), jSONObject.optInt("identifier_mask", 255), jSONObject.optBoolean("send_session_without_crash", false), jSONObject.optInt("max_complete_sessions_count", 4));
    }

    @DexIgnore
    public final j74 a(JSONObject jSONObject) {
        return new j74(jSONObject.optString("url", "https://e.crashlytics.com/spi/v2/events"), jSONObject.optInt("flush_interval_secs", 600), jSONObject.optInt("max_byte_size_per_file", MFNetworkReturnCode.REQUEST_NOT_FOUND), jSONObject.optInt("max_file_count_per_send", 1), jSONObject.optInt("max_pending_send_file_count", 100), jSONObject.optBoolean("forward_to_google_analytics", false), jSONObject.optBoolean("include_purchase_events_in_forwarded_events", false), jSONObject.optBoolean("track_custom_events", true), jSONObject.optBoolean("track_predefined_events", true), jSONObject.optInt("sampling_rate", 1), jSONObject.optBoolean("flush_on_background", true));
    }

    @DexIgnore
    public final long a(n54 n54, long j, JSONObject jSONObject) throws JSONException {
        if (jSONObject.has(AccessToken.EXPIRES_AT_KEY)) {
            return jSONObject.getLong(AccessToken.EXPIRES_AT_KEY);
        }
        return n54.a() + (j * 1000);
    }
}
