package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.of */
public class C2566of {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Set<androidx.lifecycle.LiveData> f8113a; // = java.util.Collections.newSetFromMap(new java.util.IdentityHashMap());

    @DexIgnore
    /* renamed from: b */
    public /* final */ androidx.room.RoomDatabase f8114b;

    @DexIgnore
    public C2566of(androidx.room.RoomDatabase roomDatabase) {
        this.f8114b = roomDatabase;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> androidx.lifecycle.LiveData<T> mo14411a(java.lang.String[] strArr, boolean z, java.util.concurrent.Callable<T> callable) {
        com.fossil.blesdk.obfuscated.C3111vf vfVar = new com.fossil.blesdk.obfuscated.C3111vf(this.f8114b, this, z, callable, strArr);
        return vfVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14413b(androidx.lifecycle.LiveData liveData) {
        this.f8113a.remove(liveData);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14412a(androidx.lifecycle.LiveData liveData) {
        this.f8113a.add(liveData);
    }
}
