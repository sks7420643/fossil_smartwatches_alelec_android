package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class we {
    @DexIgnore
    public /* final */ RecyclerView.m a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ Rect c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends we {
        @DexIgnore
        public a(RecyclerView.m mVar) {
            super(mVar, (a) null);
        }

        @DexIgnore
        public int a() {
            return this.a.r();
        }

        @DexIgnore
        public int b() {
            return this.a.r() - this.a.p();
        }

        @DexIgnore
        public int c(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.g(view) + layoutParams.topMargin + layoutParams.bottomMargin;
        }

        @DexIgnore
        public int d(View view) {
            return this.a.f(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
        }

        @DexIgnore
        public int e(View view) {
            this.a.a(view, true, this.c);
            return this.c.right;
        }

        @DexIgnore
        public int f() {
            return this.a.o();
        }

        @DexIgnore
        public int g() {
            return (this.a.r() - this.a.o()) - this.a.p();
        }

        @DexIgnore
        public void a(int i) {
            this.a.e(i);
        }

        @DexIgnore
        public int b(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.h(view) + layoutParams.leftMargin + layoutParams.rightMargin;
        }

        @DexIgnore
        public int f(View view) {
            this.a.a(view, true, this.c);
            return this.c.left;
        }

        @DexIgnore
        public int a(View view) {
            return this.a.i(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin;
        }

        @DexIgnore
        public int c() {
            return this.a.p();
        }

        @DexIgnore
        public int d() {
            return this.a.s();
        }

        @DexIgnore
        public int e() {
            return this.a.i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends we {
        @DexIgnore
        public b(RecyclerView.m mVar) {
            super(mVar, (a) null);
        }

        @DexIgnore
        public int a() {
            return this.a.h();
        }

        @DexIgnore
        public int b() {
            return this.a.h() - this.a.n();
        }

        @DexIgnore
        public int c(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.h(view) + layoutParams.leftMargin + layoutParams.rightMargin;
        }

        @DexIgnore
        public int d(View view) {
            return this.a.j(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
        }

        @DexIgnore
        public int e(View view) {
            this.a.a(view, true, this.c);
            return this.c.bottom;
        }

        @DexIgnore
        public int f() {
            return this.a.q();
        }

        @DexIgnore
        public int g() {
            return (this.a.h() - this.a.q()) - this.a.n();
        }

        @DexIgnore
        public void a(int i) {
            this.a.f(i);
        }

        @DexIgnore
        public int b(View view) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
            return this.a.g(view) + layoutParams.topMargin + layoutParams.bottomMargin;
        }

        @DexIgnore
        public int f(View view) {
            this.a.a(view, true, this.c);
            return this.c.top;
        }

        @DexIgnore
        public int a(View view) {
            return this.a.e(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin;
        }

        @DexIgnore
        public int c() {
            return this.a.n();
        }

        @DexIgnore
        public int d() {
            return this.a.i();
        }

        @DexIgnore
        public int e() {
            return this.a.s();
        }
    }

    @DexIgnore
    public /* synthetic */ we(RecyclerView.m mVar, a aVar) {
        this(mVar);
    }

    @DexIgnore
    public static we a(RecyclerView.m mVar, int i) {
        if (i == 0) {
            return a(mVar);
        }
        if (i == 1) {
            return b(mVar);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    @DexIgnore
    public static we b(RecyclerView.m mVar) {
        return new b(mVar);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract int a(View view);

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public abstract int b(View view);

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract int c(View view);

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract int d(View view);

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public abstract int e(View view);

    @DexIgnore
    public abstract int f();

    @DexIgnore
    public abstract int f(View view);

    @DexIgnore
    public abstract int g();

    @DexIgnore
    public int h() {
        if (Integer.MIN_VALUE == this.b) {
            return 0;
        }
        return g() - this.b;
    }

    @DexIgnore
    public void i() {
        this.b = g();
    }

    @DexIgnore
    public we(RecyclerView.m mVar) {
        this.b = Integer.MIN_VALUE;
        this.c = new Rect();
        this.a = mVar;
    }

    @DexIgnore
    public static we a(RecyclerView.m mVar) {
        return new a(mVar);
    }
}
