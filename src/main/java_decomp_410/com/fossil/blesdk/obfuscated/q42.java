package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q42 implements Factory<ApiServiceV2> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<xo2> b;
    @DexIgnore
    public /* final */ Provider<bp2> c;

    @DexIgnore
    public q42(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static q42 a(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return new q42(n42, provider, provider2);
    }

    @DexIgnore
    public static ApiServiceV2 b(n42 n42, Provider<xo2> provider, Provider<bp2> provider2) {
        return a(n42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ApiServiceV2 a(n42 n42, xo2 xo2, bp2 bp2) {
        ApiServiceV2 a2 = n42.a(xo2, bp2);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public ApiServiceV2 get() {
        return b(this.a, this.b, this.c);
    }
}
