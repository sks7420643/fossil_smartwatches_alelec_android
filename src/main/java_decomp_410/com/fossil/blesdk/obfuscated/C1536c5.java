package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c5 */
public class C1536c5 {

    @DexIgnore
    /* renamed from: a */
    public static boolean[] f4002a; // = new boolean[3];

    @DexIgnore
    /* renamed from: a */
    public static void m5285a(com.fossil.blesdk.obfuscated.C3342y4 y4Var, com.fossil.blesdk.obfuscated.C2703q4 q4Var, androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        if (y4Var.f665C[0] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i = constraintWidget.f716s.f653e;
            int t = y4Var.mo1352t() - constraintWidget.f718u.f653e;
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintWidget.f716s;
            constraintAnchor.f657i = q4Var.mo15010a((java.lang.Object) constraintAnchor);
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintWidget.f718u;
            constraintAnchor2.f657i = q4Var.mo15010a((java.lang.Object) constraintAnchor2);
            q4Var.mo15013a(constraintWidget.f716s.f657i, i);
            q4Var.mo15013a(constraintWidget.f718u.f657i, t);
            constraintWidget.f689a = 2;
            constraintWidget.mo1288a(i, t);
        }
        if (y4Var.f665C[1] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            int i2 = constraintWidget.f717t.f653e;
            int j = y4Var.mo1332j() - constraintWidget.f719v.f653e;
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor3 = constraintWidget.f717t;
            constraintAnchor3.f657i = q4Var.mo15010a((java.lang.Object) constraintAnchor3);
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor4 = constraintWidget.f719v;
            constraintAnchor4.f657i = q4Var.mo15010a((java.lang.Object) constraintAnchor4);
            q4Var.mo15013a(constraintWidget.f717t.f657i, i2);
            q4Var.mo15013a(constraintWidget.f719v.f657i, j);
            if (constraintWidget.f679Q > 0 || constraintWidget.mo1350s() == 8) {
                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor5 = constraintWidget.f720w;
                constraintAnchor5.f657i = q4Var.mo15010a((java.lang.Object) constraintAnchor5);
                q4Var.mo15013a(constraintWidget.f720w.f657i, constraintWidget.f679Q + i2);
            }
            constraintWidget.f691b = 2;
            constraintWidget.mo1323e(i2, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b A[RETURN] */
    /* renamed from: a */
    public static boolean m5286a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i) {
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour[] dimensionBehaviourArr = constraintWidget.f665C;
        if (dimensionBehaviourArr[i] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return false;
        }
        char c = 1;
        if (constraintWidget.f669G != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            if (i != 0) {
                c = 0;
            }
            if (dimensionBehaviourArr[c] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            }
            return false;
        }
        if (i == 0) {
            if (constraintWidget.f697e == 0 && constraintWidget.f703h == 0 && constraintWidget.f705i == 0) {
                return true;
            }
            return false;
        } else if (constraintWidget.f699f != 0 || constraintWidget.f708k != 0 || constraintWidget.f709l != 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5283a(int i, androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintWidget;
        constraintWidget.mo1283J();
        com.fossil.blesdk.obfuscated.C1703e5 d = constraintWidget2.f716s.mo1266d();
        com.fossil.blesdk.obfuscated.C1703e5 d2 = constraintWidget2.f717t.mo1266d();
        com.fossil.blesdk.obfuscated.C1703e5 d3 = constraintWidget2.f718u.mo1266d();
        com.fossil.blesdk.obfuscated.C1703e5 d4 = constraintWidget2.f719v.mo1266d();
        boolean z = (i & 8) == 8;
        boolean z2 = constraintWidget2.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && m5286a(constraintWidget2, 0);
        if (!(d.f4705h == 4 || d3.f4705h == 4)) {
            if (constraintWidget2.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED || (z2 && constraintWidget.mo1350s() == 8)) {
                if (constraintWidget2.f716s.f652d == null && constraintWidget2.f718u.f652d == null) {
                    d.mo10329b(1);
                    d3.mo10329b(1);
                    if (z) {
                        d3.mo10327a(d, 1, constraintWidget.mo1340n());
                    } else {
                        d3.mo10326a(d, constraintWidget.mo1352t());
                    }
                } else if (constraintWidget2.f716s.f652d != null && constraintWidget2.f718u.f652d == null) {
                    d.mo10329b(1);
                    d3.mo10329b(1);
                    if (z) {
                        d3.mo10327a(d, 1, constraintWidget.mo1340n());
                    } else {
                        d3.mo10326a(d, constraintWidget.mo1352t());
                    }
                } else if (constraintWidget2.f716s.f652d == null && constraintWidget2.f718u.f652d != null) {
                    d.mo10329b(1);
                    d3.mo10329b(1);
                    d.mo10326a(d3, -constraintWidget.mo1352t());
                    if (z) {
                        d.mo10327a(d3, -1, constraintWidget.mo1340n());
                    } else {
                        d.mo10326a(d3, -constraintWidget.mo1352t());
                    }
                } else if (!(constraintWidget2.f716s.f652d == null || constraintWidget2.f718u.f652d == null)) {
                    d.mo10329b(2);
                    d3.mo10329b(2);
                    if (z) {
                        constraintWidget.mo1340n().mo11138a(d);
                        constraintWidget.mo1340n().mo11138a(d3);
                        d.mo10331b(d3, -1, constraintWidget.mo1340n());
                        d3.mo10331b(d, 1, constraintWidget.mo1340n());
                    } else {
                        d.mo10330b(d3, (float) (-constraintWidget.mo1352t()));
                        d3.mo10330b(d, (float) constraintWidget.mo1352t());
                    }
                }
            } else if (z2) {
                int t = constraintWidget.mo1352t();
                d.mo10329b(1);
                d3.mo10329b(1);
                if (constraintWidget2.f716s.f652d == null && constraintWidget2.f718u.f652d == null) {
                    if (z) {
                        d3.mo10327a(d, 1, constraintWidget.mo1340n());
                    } else {
                        d3.mo10326a(d, t);
                    }
                } else if (constraintWidget2.f716s.f652d == null || constraintWidget2.f718u.f652d != null) {
                    if (constraintWidget2.f716s.f652d != null || constraintWidget2.f718u.f652d == null) {
                        if (!(constraintWidget2.f716s.f652d == null || constraintWidget2.f718u.f652d == null)) {
                            if (z) {
                                constraintWidget.mo1340n().mo11138a(d);
                                constraintWidget.mo1340n().mo11138a(d3);
                            }
                            if (constraintWidget2.f669G == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d.mo10329b(3);
                                d3.mo10329b(3);
                                d.mo10330b(d3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d3.mo10330b(d, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                d.mo10329b(2);
                                d3.mo10329b(2);
                                d.mo10330b(d3, (float) (-t));
                                d3.mo10330b(d, (float) t);
                                constraintWidget2.mo1345p(t);
                            }
                        }
                    } else if (z) {
                        d.mo10327a(d3, -1, constraintWidget.mo1340n());
                    } else {
                        d.mo10326a(d3, -t);
                    }
                } else if (z) {
                    d3.mo10327a(d, 1, constraintWidget.mo1340n());
                } else {
                    d3.mo10326a(d, t);
                }
            }
        }
        boolean z3 = constraintWidget2.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && m5286a(constraintWidget2, 1);
        if (d2.f4705h != 4 && d4.f4705h != 4) {
            if (constraintWidget2.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED || (z3 && constraintWidget.mo1350s() == 8)) {
                if (constraintWidget2.f717t.f652d == null && constraintWidget2.f719v.f652d == null) {
                    d2.mo10329b(1);
                    d4.mo10329b(1);
                    if (z) {
                        d4.mo10327a(d2, 1, constraintWidget.mo1338m());
                    } else {
                        d4.mo10326a(d2, constraintWidget.mo1332j());
                    }
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintWidget2.f720w;
                    if (constraintAnchor.f652d != null) {
                        constraintAnchor.mo1266d().mo10329b(1);
                        d2.mo10324a(1, constraintWidget2.f720w.mo1266d(), -constraintWidget2.f679Q);
                    }
                } else if (constraintWidget2.f717t.f652d != null && constraintWidget2.f719v.f652d == null) {
                    d2.mo10329b(1);
                    d4.mo10329b(1);
                    if (z) {
                        d4.mo10327a(d2, 1, constraintWidget.mo1338m());
                    } else {
                        d4.mo10326a(d2, constraintWidget.mo1332j());
                    }
                    if (constraintWidget2.f679Q > 0) {
                        constraintWidget2.f720w.mo1266d().mo10324a(1, d2, constraintWidget2.f679Q);
                    }
                } else if (constraintWidget2.f717t.f652d == null && constraintWidget2.f719v.f652d != null) {
                    d2.mo10329b(1);
                    d4.mo10329b(1);
                    if (z) {
                        d2.mo10327a(d4, -1, constraintWidget.mo1338m());
                    } else {
                        d2.mo10326a(d4, -constraintWidget.mo1332j());
                    }
                    if (constraintWidget2.f679Q > 0) {
                        constraintWidget2.f720w.mo1266d().mo10324a(1, d2, constraintWidget2.f679Q);
                    }
                } else if (constraintWidget2.f717t.f652d != null && constraintWidget2.f719v.f652d != null) {
                    d2.mo10329b(2);
                    d4.mo10329b(2);
                    if (z) {
                        d2.mo10331b(d4, -1, constraintWidget.mo1338m());
                        d4.mo10331b(d2, 1, constraintWidget.mo1338m());
                        constraintWidget.mo1338m().mo11138a(d2);
                        constraintWidget.mo1340n().mo11138a(d4);
                    } else {
                        d2.mo10330b(d4, (float) (-constraintWidget.mo1332j()));
                        d4.mo10330b(d2, (float) constraintWidget.mo1332j());
                    }
                    if (constraintWidget2.f679Q > 0) {
                        constraintWidget2.f720w.mo1266d().mo10324a(1, d2, constraintWidget2.f679Q);
                    }
                }
            } else if (z3) {
                int j = constraintWidget.mo1332j();
                d2.mo10329b(1);
                d4.mo10329b(1);
                if (constraintWidget2.f717t.f652d == null && constraintWidget2.f719v.f652d == null) {
                    if (z) {
                        d4.mo10327a(d2, 1, constraintWidget.mo1338m());
                    } else {
                        d4.mo10326a(d2, j);
                    }
                } else if (constraintWidget2.f717t.f652d == null || constraintWidget2.f719v.f652d != null) {
                    if (constraintWidget2.f717t.f652d != null || constraintWidget2.f719v.f652d == null) {
                        if (constraintWidget2.f717t.f652d != null && constraintWidget2.f719v.f652d != null) {
                            if (z) {
                                constraintWidget.mo1338m().mo11138a(d2);
                                constraintWidget.mo1340n().mo11138a(d4);
                            }
                            if (constraintWidget2.f669G == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                d2.mo10329b(3);
                                d4.mo10329b(3);
                                d2.mo10330b(d4, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                d4.mo10330b(d2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                return;
                            }
                            d2.mo10329b(2);
                            d4.mo10329b(2);
                            d2.mo10330b(d4, (float) (-j));
                            d4.mo10330b(d2, (float) j);
                            constraintWidget2.mo1329h(j);
                            if (constraintWidget2.f679Q > 0) {
                                constraintWidget2.f720w.mo1266d().mo10324a(1, d2, constraintWidget2.f679Q);
                            }
                        }
                    } else if (z) {
                        d2.mo10327a(d4, -1, constraintWidget.mo1338m());
                    } else {
                        d2.mo10326a(d4, -j);
                    }
                } else if (z) {
                    d4.mo10327a(d2, 1, constraintWidget.mo1338m());
                } else {
                    d4.mo10326a(d2, j);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        if (r7.f698e0 == 2) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0036, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        if (r7.f700f0 == 2) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:124:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0109  */
    /* renamed from: a */
    public static boolean m5287a(com.fossil.blesdk.obfuscated.C3342y4 y4Var, com.fossil.blesdk.obfuscated.C2703q4 q4Var, int i, int i2, com.fossil.blesdk.obfuscated.C3261x4 x4Var) {
        boolean z;
        boolean z2;
        float f;
        int i3;
        int i4;
        float f2;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget;
        boolean z3;
        int i5;
        com.fossil.blesdk.obfuscated.C2703q4 q4Var2 = q4Var;
        int i6 = i;
        com.fossil.blesdk.obfuscated.C3261x4 x4Var2 = x4Var;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = x4Var2.f10827a;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = x4Var2.f10829c;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4 = x4Var2.f10828b;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget5 = x4Var2.f10830d;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget6 = x4Var2.f10831e;
        float f3 = x4Var2.f10837k;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget7 = x4Var2.f10832f;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget8 = x4Var2.f10833g;
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour = y4Var.f665C[i6];
        androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (i6 == 0) {
            z2 = constraintWidget6.f698e0 == 0;
            z = constraintWidget6.f698e0 == 1;
        } else {
            z2 = constraintWidget6.f700f0 == 0;
            z = constraintWidget6.f700f0 == 1;
        }
        boolean z4 = true;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget9 = constraintWidget2;
        int i7 = 0;
        boolean z5 = false;
        int i8 = 0;
        float f4 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f5 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (!z5) {
            if (constraintWidget9.mo1350s() != 8) {
                i8++;
                if (i6 == 0) {
                    i5 = constraintWidget9.mo1352t();
                } else {
                    i5 = constraintWidget9.mo1332j();
                }
                f4 += (float) i5;
                if (constraintWidget9 != constraintWidget4) {
                    f4 += (float) constraintWidget9.f663A[i2].mo1264b();
                }
                if (constraintWidget9 != constraintWidget5) {
                    f4 += (float) constraintWidget9.f663A[i2 + 1].mo1264b();
                }
                f5 = f5 + ((float) constraintWidget9.f663A[i2].mo1264b()) + ((float) constraintWidget9.f663A[i2 + 1].mo1264b());
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintWidget9.f663A[i2];
            if (constraintWidget9.mo1350s() != 8 && constraintWidget9.f665C[i6] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                i7++;
                if (i6 != 0) {
                    z3 = false;
                    if (constraintWidget9.f699f != 0) {
                        return false;
                    }
                    if (constraintWidget9.f708k == 0) {
                        if (constraintWidget9.f709l != 0) {
                        }
                    }
                    return z3;
                } else if (constraintWidget9.f697e != 0) {
                    return false;
                } else {
                    z3 = false;
                    if (!(constraintWidget9.f703h == 0 && constraintWidget9.f705i == 0)) {
                        return false;
                    }
                }
                if (constraintWidget9.f669G != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return z3;
                }
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintWidget9.f663A[i2 + 1].f652d;
            if (constraintAnchor2 != null) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget10 = constraintAnchor2.f650b;
                androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget10.f663A;
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget11 = constraintWidget10;
                if (constraintAnchorArr[i2].f652d != null && constraintAnchorArr[i2].f652d.f650b == constraintWidget9) {
                    constraintWidget = constraintWidget11;
                    if (constraintWidget == null) {
                        constraintWidget9 = constraintWidget;
                    } else {
                        z5 = true;
                    }
                }
            }
            constraintWidget = null;
            if (constraintWidget == null) {
            }
        }
        com.fossil.blesdk.obfuscated.C1703e5 d = constraintWidget2.f663A[i2].mo1266d();
        int i9 = i2 + 1;
        com.fossil.blesdk.obfuscated.C1703e5 d2 = constraintWidget3.f663A[i9].mo1266d();
        com.fossil.blesdk.obfuscated.C1703e5 e5Var = d.f4701d;
        if (e5Var == null) {
            return false;
        }
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget12 = constraintWidget2;
        com.fossil.blesdk.obfuscated.C1703e5 e5Var2 = d2.f4701d;
        if (e5Var2 == null || e5Var.f5384b != 1 || e5Var2.f5384b != 1) {
            return false;
        }
        if (i7 > 0 && i7 != i8) {
            return false;
        }
        if (z4 || z2 || z) {
            f = constraintWidget4 != null ? (float) constraintWidget4.f663A[i2].mo1264b() : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (constraintWidget5 != null) {
                f += (float) constraintWidget5.f663A[i9].mo1264b();
            }
        } else {
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        float f6 = d.f4701d.f4704g;
        float f7 = d2.f4701d.f4704g;
        float f8 = (f6 < f7 ? f7 - f6 : f6 - f7) - f4;
        if (i7 <= 0 || i7 != i8) {
            com.fossil.blesdk.obfuscated.C2703q4 q4Var3 = q4Var;
            if (f8 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z4 = true;
                z2 = false;
                z = false;
            }
            if (z4) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget13 = constraintWidget12;
                float b = f6 + ((f8 - f) * constraintWidget13.mo1303b(i6));
                while (constraintWidget13 != null) {
                    com.fossil.blesdk.obfuscated.C2783r4 r4Var = com.fossil.blesdk.obfuscated.C2703q4.f8531q;
                    if (r4Var != null) {
                        r4Var.f8856z--;
                        r4Var.f8848r++;
                        r4Var.f8854x++;
                    }
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget14 = constraintWidget13.f706i0[i6];
                    if (constraintWidget14 != null || constraintWidget13 == constraintWidget3) {
                        if (i6 == 0) {
                            i4 = constraintWidget13.mo1352t();
                        } else {
                            i4 = constraintWidget13.mo1332j();
                        }
                        float b2 = b + ((float) constraintWidget13.f663A[i2].mo1264b());
                        constraintWidget13.f663A[i2].mo1266d().mo10325a(d.f4703f, b2);
                        float f9 = b2 + ((float) i4);
                        constraintWidget13.f663A[i9].mo1266d().mo10325a(d.f4703f, f9);
                        constraintWidget13.f663A[i2].mo1266d().mo10328a(q4Var3);
                        constraintWidget13.f663A[i9].mo1266d().mo10328a(q4Var3);
                        b = f9 + ((float) constraintWidget13.f663A[i9].mo1264b());
                    }
                    constraintWidget13 = constraintWidget14;
                }
                return true;
            }
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget15 = constraintWidget12;
            if (!z2 && !z) {
                return true;
            }
            if (z2 || z) {
                f8 -= f;
            }
            float f10 = f8 / ((float) (i8 + 1));
            if (z) {
                f10 = f8 / (i8 > 1 ? (float) (i8 - 1) : 2.0f);
            }
            float f11 = constraintWidget15.mo1350s() != 8 ? f6 + f10 : f6;
            if (z && i8 > 1) {
                f11 = ((float) constraintWidget4.f663A[i2].mo1264b()) + f6;
            }
            if (z2 && constraintWidget4 != null) {
                f11 += (float) constraintWidget4.f663A[i2].mo1264b();
            }
            while (constraintWidget15 != null) {
                com.fossil.blesdk.obfuscated.C2783r4 r4Var2 = com.fossil.blesdk.obfuscated.C2703q4.f8531q;
                if (r4Var2 != null) {
                    r4Var2.f8856z--;
                    r4Var2.f8848r++;
                    r4Var2.f8854x++;
                }
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget16 = constraintWidget15.f706i0[i6];
                if (constraintWidget16 != null || constraintWidget15 == constraintWidget3) {
                    if (i6 == 0) {
                        i3 = constraintWidget15.mo1352t();
                    } else {
                        i3 = constraintWidget15.mo1332j();
                    }
                    float f12 = (float) i3;
                    if (constraintWidget15 != constraintWidget4) {
                        f11 += (float) constraintWidget15.f663A[i2].mo1264b();
                    }
                    constraintWidget15.f663A[i2].mo1266d().mo10325a(d.f4703f, f11);
                    constraintWidget15.f663A[i9].mo1266d().mo10325a(d.f4703f, f11 + f12);
                    constraintWidget15.f663A[i2].mo1266d().mo10328a(q4Var3);
                    constraintWidget15.f663A[i9].mo1266d().mo10328a(q4Var3);
                    f11 += f12 + ((float) constraintWidget15.f663A[i9].mo1264b());
                    if (constraintWidget16 != null) {
                        if (constraintWidget16.mo1350s() != 8) {
                            f11 += f10;
                        }
                        constraintWidget15 = constraintWidget16;
                    }
                }
                constraintWidget15 = constraintWidget16;
            }
            return true;
        } else if (constraintWidget9.mo1336l() != null && constraintWidget9.mo1336l().f665C[i6] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            return false;
        } else {
            float f13 = (f8 + f4) - f5;
            float f14 = f6;
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget17 = constraintWidget12;
            while (constraintWidget17 != null) {
                com.fossil.blesdk.obfuscated.C2783r4 r4Var3 = com.fossil.blesdk.obfuscated.C2703q4.f8531q;
                if (r4Var3 != null) {
                    r4Var3.f8856z--;
                    r4Var3.f8848r++;
                    r4Var3.f8854x++;
                }
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget18 = constraintWidget17.f706i0[i6];
                if (constraintWidget18 != null || constraintWidget17 == constraintWidget3) {
                    float f15 = f13 / ((float) i7);
                    if (f3 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        float[] fArr = constraintWidget17.f702g0;
                        if (fArr[i6] == -1.0f) {
                            f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            if (constraintWidget17.mo1350s() == 8) {
                                f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                            float b3 = f14 + ((float) constraintWidget17.f663A[i2].mo1264b());
                            constraintWidget17.f663A[i2].mo1266d().mo10325a(d.f4703f, b3);
                            float f16 = b3 + f2;
                            constraintWidget17.f663A[i9].mo1266d().mo10325a(d.f4703f, f16);
                            com.fossil.blesdk.obfuscated.C2703q4 q4Var4 = q4Var;
                            constraintWidget17.f663A[i2].mo1266d().mo10328a(q4Var4);
                            constraintWidget17.f663A[i9].mo1266d().mo10328a(q4Var4);
                            f14 = f16 + ((float) constraintWidget17.f663A[i9].mo1264b());
                        } else {
                            f15 = (fArr[i6] * f13) / f3;
                        }
                    }
                    f2 = f15;
                    if (constraintWidget17.mo1350s() == 8) {
                    }
                    float b32 = f14 + ((float) constraintWidget17.f663A[i2].mo1264b());
                    constraintWidget17.f663A[i2].mo1266d().mo10325a(d.f4703f, b32);
                    float f162 = b32 + f2;
                    constraintWidget17.f663A[i9].mo1266d().mo10325a(d.f4703f, f162);
                    com.fossil.blesdk.obfuscated.C2703q4 q4Var42 = q4Var;
                    constraintWidget17.f663A[i2].mo1266d().mo10328a(q4Var42);
                    constraintWidget17.f663A[i9].mo1266d().mo10328a(q4Var42);
                    f14 = f162 + ((float) constraintWidget17.f663A[i9].mo1264b());
                } else {
                    com.fossil.blesdk.obfuscated.C2703q4 q4Var5 = q4Var;
                }
                constraintWidget17 = constraintWidget18;
            }
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5284a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        int i4 = i3 + 1;
        constraintWidget.f663A[i3].mo1266d().f4703f = constraintWidget.mo1336l().f716s.mo1266d();
        constraintWidget.f663A[i3].mo1266d().f4704g = (float) i2;
        constraintWidget.f663A[i3].mo1266d().f5384b = 1;
        constraintWidget.f663A[i4].mo1266d().f4703f = constraintWidget.f663A[i3].mo1266d();
        constraintWidget.f663A[i4].mo1266d().f4704g = (float) constraintWidget.mo1318d(i);
        constraintWidget.f663A[i4].mo1266d().f5384b = 1;
    }
}
