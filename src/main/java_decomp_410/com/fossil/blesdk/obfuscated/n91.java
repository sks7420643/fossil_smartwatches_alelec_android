package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n91 implements oa1 {
    @DexIgnore
    public static /* final */ v91 b; // = new o91();
    @DexIgnore
    public /* final */ v91 a;

    @DexIgnore
    public n91() {
        this(new p91(s81.a(), a()));
    }

    @DexIgnore
    public final <T> na1<T> a(Class<T> cls) {
        Class<t81> cls2 = t81.class;
        pa1.a((Class<?>) cls);
        u91 a2 = this.a.a(cls);
        if (a2.b()) {
            if (cls2.isAssignableFrom(cls)) {
                return ca1.a(pa1.c(), l81.b(), a2.a());
            }
            return ca1.a(pa1.a(), l81.c(), a2.a());
        } else if (cls2.isAssignableFrom(cls)) {
            if (a(a2)) {
                return aa1.a(cls, a2, ga1.b(), i91.b(), pa1.c(), l81.b(), t91.b());
            }
            return aa1.a(cls, a2, ga1.b(), i91.b(), pa1.c(), (j81<?>) null, t91.b());
        } else if (a(a2)) {
            return aa1.a(cls, a2, ga1.a(), i91.a(), pa1.a(), l81.c(), t91.a());
        } else {
            return aa1.a(cls, a2, ga1.a(), i91.a(), pa1.b(), (j81<?>) null, t91.a());
        }
    }

    @DexIgnore
    public n91(v91 v91) {
        v81.a(v91, "messageInfoFactory");
        this.a = v91;
    }

    @DexIgnore
    public static boolean a(u91 u91) {
        return u91.c() == t81.e.i;
    }

    @DexIgnore
    public static v91 a() {
        try {
            return (v91) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }
}
