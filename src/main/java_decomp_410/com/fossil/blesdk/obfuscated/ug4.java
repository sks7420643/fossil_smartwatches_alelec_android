package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.zb4;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ug4 extends xb4 implements zb4 {
    @DexIgnore
    public ug4() {
        super(zb4.b);
    }

    @DexIgnore
    public abstract void a(CoroutineContext coroutineContext, Runnable runnable);

    @DexIgnore
    public void b(yb4<?> yb4) {
        kd4.b(yb4, "continuation");
        zb4.a.a((zb4) this, yb4);
    }

    @DexIgnore
    public boolean b(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        return true;
    }

    @DexIgnore
    public final <T> yb4<T> c(yb4<? super T> yb4) {
        kd4.b(yb4, "continuation");
        return new jh4(this, yb4);
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        kd4.b(bVar, "key");
        return zb4.a.a((zb4) this, bVar);
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        kd4.b(bVar, "key");
        return zb4.a.b(this, bVar);
    }

    @DexIgnore
    public String toString() {
        return dh4.a((Object) this) + '@' + dh4.b(this);
    }

    @DexIgnore
    public void b(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        a(coroutineContext, runnable);
    }
}
