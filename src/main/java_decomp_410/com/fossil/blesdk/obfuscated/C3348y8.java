package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y8 */
public interface C3348y8 extends com.fossil.blesdk.obfuscated.C3418z8 {
    @DexIgnore
    /* renamed from: a */
    void mo1439a(android.view.View view, int i);

    @DexIgnore
    /* renamed from: a */
    void mo1441a(android.view.View view, int i, int i2, int i3, int i4, int i5);

    @DexIgnore
    /* renamed from: a */
    void mo1442a(android.view.View view, int i, int i2, int[] iArr, int i3);

    @DexIgnore
    /* renamed from: a */
    boolean mo1454a(android.view.View view, android.view.View view2, int i, int i2);

    @DexIgnore
    /* renamed from: b */
    void mo1461b(android.view.View view, android.view.View view2, int i, int i2);
}
