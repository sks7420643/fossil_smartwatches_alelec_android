package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cv3;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.IOException;
import java.util.logging.Logger;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pv3 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(gv3.class.getName());
    @DexIgnore
    public static pv3 b;

    @DexIgnore
    public abstract dx3 a(wu3 wu3, uw3 uw3) throws IOException;

    @DexIgnore
    public abstract qv3 a(gv3 gv3);

    @DexIgnore
    public abstract void a(cv3.b bVar, String str);

    @DexIgnore
    public abstract void a(gv3 gv3, wu3 wu3, uw3 uw3, hv3 hv3) throws RouteException;

    @DexIgnore
    public abstract void a(wu3 wu3, Protocol protocol);

    @DexIgnore
    public abstract void a(xu3 xu3, wu3 wu3);

    @DexIgnore
    public abstract void a(yu3 yu3, SSLSocket sSLSocket, boolean z);

    @DexIgnore
    public abstract boolean a(wu3 wu3);

    @DexIgnore
    public abstract sv3 b(gv3 gv3);

    @DexIgnore
    public abstract void b(wu3 wu3, uw3 uw3);

    @DexIgnore
    public abstract boolean b(wu3 wu3);

    @DexIgnore
    public abstract int c(wu3 wu3);

    @DexIgnore
    public abstract vv3 c(gv3 gv3);
}
