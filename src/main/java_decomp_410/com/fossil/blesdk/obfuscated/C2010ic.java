package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ic */
public abstract class C2010ic {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.String, java.lang.Object> f5995a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public volatile boolean f5996b; // = false;

    @DexIgnore
    /* renamed from: a */
    public final void mo11902a() {
        this.f5996b = true;
        java.util.Map<java.lang.String, java.lang.Object> map = this.f5995a;
        if (map != null) {
            synchronized (map) {
                for (java.lang.Object a : this.f5995a.values()) {
                    m8335a(a);
                }
            }
        }
        mo11903b();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11903b() {
    }

    @DexIgnore
    /* renamed from: a */
    public <T> T mo11901a(java.lang.String str, T t) {
        T t2;
        synchronized (this.f5995a) {
            t2 = this.f5995a.get(str);
            if (t2 == null) {
                this.f5995a.put(str, t);
            }
        }
        if (t2 != null) {
            t = t2;
        }
        if (this.f5996b) {
            m8335a((java.lang.Object) t);
        }
        return t;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> T mo11900a(java.lang.String str) {
        T t;
        synchronized (this.f5995a) {
            t = this.f5995a.get(str);
        }
        return t;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8335a(java.lang.Object obj) {
        if (obj instanceof java.io.Closeable) {
            try {
                ((java.io.Closeable) obj).close();
            } catch (java.io.IOException e) {
                throw new java.lang.RuntimeException(e);
            }
        }
    }
}
