package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gb */
public class C1865gb implements android.view.ViewTreeObserver.OnPreDrawListener, android.view.View.OnAttachStateChangeListener {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.view.View f5408e;

    @DexIgnore
    /* renamed from: f */
    public android.view.ViewTreeObserver f5409f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.Runnable f5410g;

    @DexIgnore
    public C1865gb(android.view.View view, java.lang.Runnable runnable) {
        this.f5408e = view;
        this.f5409f = view.getViewTreeObserver();
        this.f5410g = runnable;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1865gb m7335a(android.view.View view, java.lang.Runnable runnable) {
        com.fossil.blesdk.obfuscated.C1865gb gbVar = new com.fossil.blesdk.obfuscated.C1865gb(view, runnable);
        view.getViewTreeObserver().addOnPreDrawListener(gbVar);
        view.addOnAttachStateChangeListener(gbVar);
        return gbVar;
    }

    @DexIgnore
    public boolean onPreDraw() {
        mo11180a();
        this.f5410g.run();
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(android.view.View view) {
        this.f5409f = view.getViewTreeObserver();
    }

    @DexIgnore
    public void onViewDetachedFromWindow(android.view.View view) {
        mo11180a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11180a() {
        if (this.f5409f.isAlive()) {
            this.f5409f.removeOnPreDrawListener(this);
        } else {
            this.f5408e.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.f5408e.removeOnAttachStateChangeListener(this);
    }
}
