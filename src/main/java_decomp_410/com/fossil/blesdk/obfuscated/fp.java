package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fp implements so<InputStream> {
    @DexIgnore
    public /* final */ Uri e;
    @DexIgnore
    public /* final */ hp f;
    @DexIgnore
    public InputStream g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements gp {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements gp {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    @DexIgnore
    public fp(Uri uri, hp hpVar) {
        this.e = uri;
        this.f = hpVar;
    }

    @DexIgnore
    public static fp a(Context context, Uri uri) {
        return a(context, uri, new a(context.getContentResolver()));
    }

    @DexIgnore
    public static fp b(Context context, Uri uri) {
        return a(context, uri, new b(context.getContentResolver()));
    }

    @DexIgnore
    public final InputStream c() throws FileNotFoundException {
        InputStream c = this.f.c(this.e);
        int a2 = c != null ? this.f.a(this.e) : -1;
        return a2 != -1 ? new vo(c, a2) : c;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public static fp a(Context context, Uri uri, gp gpVar) {
        return new fp(uri, new hp(rn.a(context).g().a(), gpVar, rn.a(context).b(), context.getContentResolver()));
    }

    @DexIgnore
    public DataSource b() {
        return DataSource.LOCAL;
    }

    @DexIgnore
    public void a(Priority priority, so.a<? super InputStream> aVar) {
        try {
            this.g = c();
            aVar.a(this.g);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    @DexIgnore
    public void a() {
        InputStream inputStream = this.g;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }
}
