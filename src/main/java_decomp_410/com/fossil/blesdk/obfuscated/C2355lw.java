package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lw */
public final class C2355lw<K, V> extends com.fossil.blesdk.obfuscated.C1855g4<K, V> {

    @DexIgnore
    /* renamed from: m */
    public int f7347m;

    @DexIgnore
    /* renamed from: a */
    public V mo1218a(int i, V v) {
        this.f7347m = 0;
        return super.mo1218a(i, v);
    }

    @DexIgnore
    public void clear() {
        this.f7347m = 0;
        super.clear();
    }

    @DexIgnore
    /* renamed from: d */
    public V mo1228d(int i) {
        this.f7347m = 0;
        return super.mo1228d(i);
    }

    @DexIgnore
    public int hashCode() {
        if (this.f7347m == 0) {
            this.f7347m = super.hashCode();
        }
        return this.f7347m;
    }

    @DexIgnore
    public V put(K k, V v) {
        this.f7347m = 0;
        return super.put(k, v);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1220a(androidx.collection.SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.f7347m = 0;
        super.mo1220a(simpleArrayMap);
    }
}
