package com.fossil.blesdk.obfuscated;

import kotlin.jvm.internal.FunctionReference;
import kotlin.jvm.internal.Lambda;
import kotlin.jvm.internal.PropertyReference1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nd4 {
    @DexIgnore
    public je4 a(Class cls, String str) {
        return new ld4(cls, str);
    }

    @DexIgnore
    public ke4 a(FunctionReference functionReference) {
        return functionReference;
    }

    @DexIgnore
    public me4 a(PropertyReference1 propertyReference1) {
        return propertyReference1;
    }

    @DexIgnore
    public he4 a(Class cls) {
        return new dd4(cls);
    }

    @DexIgnore
    public String a(Lambda lambda) {
        return a((hd4) lambda);
    }

    @DexIgnore
    public String a(hd4 hd4) {
        String obj = hd4.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
