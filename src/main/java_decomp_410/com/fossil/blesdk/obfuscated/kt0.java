package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jt0;
import com.fossil.blesdk.obfuscated.kt0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kt0<MessageType extends jt0<MessageType, BuilderType>, BuilderType extends kt0<MessageType, BuilderType>> implements tv0 {
    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public final /* synthetic */ tv0 a(sv0 sv0) {
        if (b().getClass().isInstance(sv0)) {
            a((jt0) sv0);
            return this;
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }
}
