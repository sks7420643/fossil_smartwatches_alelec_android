package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import com.google.android.material.button.MaterialButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vr1 {
    @DexIgnore
    public static /* final */ boolean w; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ MaterialButton a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public PorterDuff.Mode h;
    @DexIgnore
    public ColorStateList i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public /* final */ Paint l; // = new Paint(1);
    @DexIgnore
    public /* final */ Rect m; // = new Rect();
    @DexIgnore
    public /* final */ RectF n; // = new RectF();
    @DexIgnore
    public GradientDrawable o;
    @DexIgnore
    public Drawable p;
    @DexIgnore
    public GradientDrawable q;
    @DexIgnore
    public Drawable r;
    @DexIgnore
    public GradientDrawable s;
    @DexIgnore
    public GradientDrawable t;
    @DexIgnore
    public GradientDrawable u;
    @DexIgnore
    public boolean v; // = false;

    @DexIgnore
    public vr1(MaterialButton materialButton) {
        this.a = materialButton;
    }

    @DexIgnore
    public void a(TypedArray typedArray) {
        int i2 = 0;
        this.b = typedArray.getDimensionPixelOffset(cr1.MaterialButton_android_insetLeft, 0);
        this.c = typedArray.getDimensionPixelOffset(cr1.MaterialButton_android_insetRight, 0);
        this.d = typedArray.getDimensionPixelOffset(cr1.MaterialButton_android_insetTop, 0);
        this.e = typedArray.getDimensionPixelOffset(cr1.MaterialButton_android_insetBottom, 0);
        this.f = typedArray.getDimensionPixelSize(cr1.MaterialButton_cornerRadius, 0);
        this.g = typedArray.getDimensionPixelSize(cr1.MaterialButton_strokeWidth, 0);
        this.h = us1.a(typedArray.getInt(cr1.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.i = ws1.a(this.a.getContext(), typedArray, cr1.MaterialButton_backgroundTint);
        this.j = ws1.a(this.a.getContext(), typedArray, cr1.MaterialButton_strokeColor);
        this.k = ws1.a(this.a.getContext(), typedArray, cr1.MaterialButton_rippleColor);
        this.l.setStyle(Paint.Style.STROKE);
        this.l.setStrokeWidth((float) this.g);
        Paint paint = this.l;
        ColorStateList colorStateList = this.j;
        if (colorStateList != null) {
            i2 = colorStateList.getColorForState(this.a.getDrawableState(), 0);
        }
        paint.setColor(i2);
        int o2 = f9.o(this.a);
        int paddingTop = this.a.getPaddingTop();
        int n2 = f9.n(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        this.a.setInternalBackground(w ? b() : a());
        f9.b(this.a, o2 + this.b, paddingTop + this.d, n2 + this.c, paddingBottom + this.e);
    }

    @DexIgnore
    @TargetApi(21)
    public final Drawable b() {
        this.s = new GradientDrawable();
        this.s.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.s.setColor(-1);
        n();
        this.t = new GradientDrawable();
        this.t.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.t.setColor(0);
        this.t.setStroke(this.g, this.j);
        InsetDrawable a2 = a((Drawable) new LayerDrawable(new Drawable[]{this.s, this.t}));
        this.u = new GradientDrawable();
        this.u.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.u.setColor(-1);
        return new ur1(zs1.a(this.k), a2, this.u);
    }

    @DexIgnore
    public void c(ColorStateList colorStateList) {
        if (this.i != colorStateList) {
            this.i = colorStateList;
            if (w) {
                n();
                return;
            }
            Drawable drawable = this.p;
            if (drawable != null) {
                c7.a(drawable, this.i);
            }
        }
    }

    @DexIgnore
    public ColorStateList d() {
        return this.k;
    }

    @DexIgnore
    public ColorStateList e() {
        return this.j;
    }

    @DexIgnore
    public int f() {
        return this.g;
    }

    @DexIgnore
    public ColorStateList g() {
        return this.i;
    }

    @DexIgnore
    public PorterDuff.Mode h() {
        return this.h;
    }

    @DexIgnore
    public boolean i() {
        return this.v;
    }

    @DexIgnore
    public void j() {
        this.v = true;
        this.a.setSupportBackgroundTintList(this.i);
        this.a.setSupportBackgroundTintMode(this.h);
    }

    @DexIgnore
    public final GradientDrawable k() {
        if (!w || this.a.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.a.getBackground()).getDrawable(0)).getDrawable()).getDrawable(0);
    }

    @DexIgnore
    public final GradientDrawable l() {
        if (!w || this.a.getBackground() == null) {
            return null;
        }
        return (GradientDrawable) ((LayerDrawable) ((InsetDrawable) ((RippleDrawable) this.a.getBackground()).getDrawable(0)).getDrawable()).getDrawable(1);
    }

    @DexIgnore
    public final void m() {
        if (w && this.t != null) {
            this.a.setInternalBackground(b());
        } else if (!w) {
            this.a.invalidate();
        }
    }

    @DexIgnore
    public final void n() {
        GradientDrawable gradientDrawable = this.s;
        if (gradientDrawable != null) {
            c7.a((Drawable) gradientDrawable, this.i);
            PorterDuff.Mode mode = this.h;
            if (mode != null) {
                c7.a((Drawable) this.s, mode);
            }
        }
    }

    @DexIgnore
    public void c(int i2) {
        if (this.g != i2) {
            this.g = i2;
            this.l.setStrokeWidth((float) i2);
            m();
        }
    }

    @DexIgnore
    public int c() {
        return this.f;
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            Paint paint = this.l;
            int i2 = 0;
            if (colorStateList != null) {
                i2 = colorStateList.getColorForState(this.a.getDrawableState(), 0);
            }
            paint.setColor(i2);
            m();
        }
    }

    @DexIgnore
    public void b(int i2) {
        if (this.f != i2) {
            this.f = i2;
            if (w && this.s != null && this.t != null && this.u != null) {
                if (Build.VERSION.SDK_INT == 21) {
                    float f2 = ((float) i2) + 1.0E-5f;
                    k().setCornerRadius(f2);
                    l().setCornerRadius(f2);
                }
                float f3 = ((float) i2) + 1.0E-5f;
                this.s.setCornerRadius(f3);
                this.t.setCornerRadius(f3);
                this.u.setCornerRadius(f3);
            } else if (!w) {
                GradientDrawable gradientDrawable = this.o;
                if (gradientDrawable != null && this.q != null) {
                    float f4 = ((float) i2) + 1.0E-5f;
                    gradientDrawable.setCornerRadius(f4);
                    this.q.setCornerRadius(f4);
                    this.a.invalidate();
                }
            }
        }
    }

    @DexIgnore
    public void a(Canvas canvas) {
        if (canvas != null && this.j != null && this.g > 0) {
            this.m.set(this.a.getBackground().getBounds());
            RectF rectF = this.n;
            Rect rect = this.m;
            int i2 = this.g;
            rectF.set(((float) rect.left) + (((float) i2) / 2.0f) + ((float) this.b), ((float) rect.top) + (((float) i2) / 2.0f) + ((float) this.d), (((float) rect.right) - (((float) i2) / 2.0f)) - ((float) this.c), (((float) rect.bottom) - (((float) i2) / 2.0f)) - ((float) this.e));
            float f2 = ((float) this.f) - (((float) this.g) / 2.0f);
            canvas.drawRoundRect(this.n, f2, f2, this.l);
        }
    }

    @DexIgnore
    public final Drawable a() {
        this.o = new GradientDrawable();
        this.o.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.o.setColor(-1);
        this.p = c7.i(this.o);
        c7.a(this.p, this.i);
        PorterDuff.Mode mode = this.h;
        if (mode != null) {
            c7.a(this.p, mode);
        }
        this.q = new GradientDrawable();
        this.q.setCornerRadius(((float) this.f) + 1.0E-5f);
        this.q.setColor(-1);
        this.r = c7.i(this.q);
        c7.a(this.r, this.k);
        return a((Drawable) new LayerDrawable(new Drawable[]{this.p, this.r}));
    }

    @DexIgnore
    public final InsetDrawable a(Drawable drawable) {
        return new InsetDrawable(drawable, this.b, this.d, this.c, this.e);
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        if (this.h != mode) {
            this.h = mode;
            if (w) {
                n();
                return;
            }
            Drawable drawable = this.p;
            if (drawable != null) {
                PorterDuff.Mode mode2 = this.h;
                if (mode2 != null) {
                    c7.a(drawable, mode2);
                }
            }
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        GradientDrawable gradientDrawable = this.u;
        if (gradientDrawable != null) {
            gradientDrawable.setBounds(this.b, this.d, i3 - this.c, i2 - this.e);
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (w) {
            GradientDrawable gradientDrawable = this.s;
            if (gradientDrawable != null) {
                gradientDrawable.setColor(i2);
                return;
            }
        }
        if (!w) {
            GradientDrawable gradientDrawable2 = this.o;
            if (gradientDrawable2 != null) {
                gradientDrawable2.setColor(i2);
            }
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            if (w && (this.a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.a.getBackground()).setColor(colorStateList);
            } else if (!w) {
                Drawable drawable = this.r;
                if (drawable != null) {
                    c7.a(drawable, colorStateList);
                }
            }
        }
    }
}
