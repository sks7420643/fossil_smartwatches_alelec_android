package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.load.engine.GlideException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class op<DataType, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Class<DataType> a;
    @DexIgnore
    public /* final */ List<? extends mo<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ gu<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ g8<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface a<ResourceType> {
        @DexIgnore
        aq<ResourceType> a(aq<ResourceType> aqVar);
    }

    @DexIgnore
    public op(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends mo<DataType, ResourceType>> list, gu<ResourceType, Transcode> guVar, g8<List<Throwable>> g8Var) {
        this.a = cls;
        this.b = list;
        this.c = guVar;
        this.d = g8Var;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public aq<Transcode> a(to<DataType> toVar, int i, int i2, lo loVar, a<ResourceType> aVar) throws GlideException {
        return this.c.a(aVar.a(a(toVar, i, i2, loVar)), loVar);
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }

    @DexIgnore
    public final aq<ResourceType> a(to<DataType> toVar, int i, int i2, lo loVar) throws GlideException {
        List<Throwable> a2 = this.d.a();
        tw.a(a2);
        List list = a2;
        try {
            return a(toVar, i, i2, loVar, (List<Throwable>) list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final aq<ResourceType> a(to<DataType> toVar, int i, int i2, lo loVar, List<Throwable> list) throws GlideException {
        int size = this.b.size();
        aq<ResourceType> aqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            mo moVar = (mo) this.b.get(i3);
            try {
                if (moVar.a(toVar.b(), loVar)) {
                    aqVar = moVar.a(toVar.b(), i, i2, loVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + moVar, e2);
                }
                list.add(e2);
            }
            if (aqVar != null) {
                break;
            }
        }
        if (aqVar != null) {
            return aqVar;
        }
        throw new GlideException(this.e, (List<Throwable>) new ArrayList(list));
    }
}
