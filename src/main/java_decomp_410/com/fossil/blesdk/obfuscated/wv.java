package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wv<T> implements bw<T> {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public ov g;

    @DexIgnore
    public wv() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(Drawable drawable) {
    }

    @DexIgnore
    public final void a(aw awVar) {
    }

    @DexIgnore
    public final void a(ov ovVar) {
        this.g = ovVar;
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Drawable drawable) {
    }

    @DexIgnore
    public final void b(aw awVar) {
        awVar.a(this.e, this.f);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public final ov d() {
        return this.g;
    }

    @DexIgnore
    public wv(int i, int i2) {
        if (uw.b(i, i2)) {
            this.e = i;
            this.f = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }
}
