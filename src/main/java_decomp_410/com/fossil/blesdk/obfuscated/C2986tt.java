package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tt */
public final class C2986tt implements com.fossil.blesdk.obfuscated.C1570co.C1571a {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f9751a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f9752b;

    @DexIgnore
    public C2986tt(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f9751a = jqVar;
        this.f9752b = gqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo9599a(int i, int i2, android.graphics.Bitmap.Config config) {
        return this.f9751a.mo12447b(i, i2, config);
    }

    @DexIgnore
    /* renamed from: b */
    public byte[] mo9604b(int i) {
        com.fossil.blesdk.obfuscated.C1885gq gqVar = this.f9752b;
        if (gqVar == null) {
            return new byte[i];
        }
        return (byte[]) gqVar.mo11285b(i, byte[].class);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9600a(android.graphics.Bitmap bitmap) {
        this.f9751a.mo12446a(bitmap);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9601a(byte[] bArr) {
        com.fossil.blesdk.obfuscated.C1885gq gqVar = this.f9752b;
        if (gqVar != null) {
            gqVar.put(bArr);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int[] mo9603a(int i) {
        com.fossil.blesdk.obfuscated.C1885gq gqVar = this.f9752b;
        if (gqVar == null) {
            return new int[i];
        }
        return (int[]) gqVar.mo11285b(i, int[].class);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9602a(int[] iArr) {
        com.fossil.blesdk.obfuscated.C1885gq gqVar = this.f9752b;
        if (gqVar != null) {
            gqVar.put(iArr);
        }
    }
}
