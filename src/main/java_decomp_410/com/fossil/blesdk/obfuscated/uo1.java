package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uo1<TResult> extends wn1<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ ro1<TResult> b; // = new ro1<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw ((Throwable) cls.cast(this.f));
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final TResult b() {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new RuntimeExecutionException(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    public final boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    public final boolean f() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void g() {
        bk0.b(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final void h() {
        bk0.b(!this.c, "Task is already complete");
    }

    @DexIgnore
    public final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void j() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> b(pn1<TResult, wn1<TContinuationResult>> pn1) {
        return b(yn1.a, pn1);
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> b(Executor executor, pn1<TResult, wn1<TContinuationResult>> pn1) {
        uo1 uo1 = new uo1();
        this.b.a(new eo1(executor, pn1, uo1));
        j();
        return uo1;
    }

    @DexIgnore
    public final Exception a() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    public final boolean b(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final wn1<TResult> a(tn1<? super TResult> tn1) {
        a(yn1.a, tn1);
        return this;
    }

    @DexIgnore
    public final wn1<TResult> a(Executor executor, tn1<? super TResult> tn1) {
        this.b.a(new mo1(executor, tn1));
        j();
        return this;
    }

    @DexIgnore
    public final wn1<TResult> a(sn1 sn1) {
        a(yn1.a, sn1);
        return this;
    }

    @DexIgnore
    public final wn1<TResult> a(Executor executor, sn1 sn1) {
        this.b.a(new ko1(executor, sn1));
        j();
        return this;
    }

    @DexIgnore
    public final wn1<TResult> a(rn1<TResult> rn1) {
        a(yn1.a, rn1);
        return this;
    }

    @DexIgnore
    public final wn1<TResult> a(Executor executor, rn1<TResult> rn1) {
        this.b.a(new io1(executor, rn1));
        j();
        return this;
    }

    @DexIgnore
    public final boolean b(Exception exc) {
        bk0.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> a(pn1<TResult, TContinuationResult> pn1) {
        return a(yn1.a, pn1);
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> a(Executor executor, pn1<TResult, TContinuationResult> pn1) {
        uo1 uo1 = new uo1();
        this.b.a(new co1(executor, pn1, uo1));
        j();
        return uo1;
    }

    @DexIgnore
    public final wn1<TResult> a(Executor executor, qn1 qn1) {
        this.b.a(new go1(executor, qn1));
        j();
        return this;
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> a(Executor executor, vn1<TResult, TContinuationResult> vn1) {
        uo1 uo1 = new uo1();
        this.b.a(new oo1(executor, vn1, uo1));
        j();
        return uo1;
    }

    @DexIgnore
    public final <TContinuationResult> wn1<TContinuationResult> a(vn1<TResult, TContinuationResult> vn1) {
        return a(yn1.a, vn1);
    }

    @DexIgnore
    public final void a(TResult tresult) {
        synchronized (this.a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void a(Exception exc) {
        bk0.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }
}
