package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lq implements fq<byte[]> {
    @DexIgnore
    public String a() {
        return "ByteArrayPool";
    }

    @DexIgnore
    public int b() {
        return 1;
    }

    @DexIgnore
    public int a(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public byte[] newArray(int i) {
        return new byte[i];
    }
}
