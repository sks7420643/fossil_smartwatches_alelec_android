package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.qs2;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c43 extends zr2 implements b43 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<ka2> j;
    @DexIgnore
    public qs2 k;
    @DexIgnore
    public a43 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return c43.n;
        }

        @DexIgnore
        public final c43 b() {
            return new c43();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ c43 e;

        @DexIgnore
        public b(c43 c43) {
            this.e = c43;
        }

        @DexIgnore
        public final void onClick(View view) {
            ka2 a = this.e.U0().a();
            if (a != null) {
                a.s.setText("");
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ c43 e;

        @DexIgnore
        public c(c43 c43) {
            this.e = c43;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (TextUtils.isEmpty(charSequence)) {
                ka2 a = this.e.U0().a();
                if (a != null) {
                    ImageView imageView = a.r;
                    if (imageView != null) {
                        imageView.setVisibility(8);
                    }
                }
                this.e.a("");
                this.e.V0().h();
                return;
            }
            ka2 a2 = this.e.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                if (flexibleTextView != null) {
                    flexibleTextView.setVisibility(8);
                }
            }
            ka2 a3 = this.e.U0().a();
            if (a3 != null) {
                ImageView imageView2 = a3.r;
                if (imageView2 != null) {
                    imageView2.setVisibility(0);
                }
            }
            this.e.a(String.valueOf(charSequence));
            this.e.V0().a(String.valueOf(charSequence));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ c43 e;

        @DexIgnore
        public d(c43 c43) {
            this.e = c43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements qs2.d {
        @DexIgnore
        public /* final */ /* synthetic */ c43 a;

        @DexIgnore
        public e(c43 c43) {
            this.a = c43;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            ka2 a2 = this.a.U0().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                kd4.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.u;
                kd4.a((Object) flexibleTextView2, "it.tvNotFound");
                pd4 pd4 = pd4.a;
                String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Search_NoResults_Text__NothingFoundForInput);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object[] objArr = {str};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements qs2.e {
        @DexIgnore
        public /* final */ /* synthetic */ c43 a;

        @DexIgnore
        public f(c43 c43) {
            this.a = c43;
        }

        @DexIgnore
        public void a(Complication complication) {
            kd4.b(complication, "item");
            this.a.V0().a(complication);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ ka2 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(ka2 ka2, long j) {
            this.a = ka2;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.a.q;
            kd4.a((Object) flexibleTextView, "it");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                flexibleTextView.animate().setDuration(this.b).alpha(1.0f);
            } else {
                flexibleTextView.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = c43.class.getSimpleName();
        kd4.a((Object) simpleName, "ComplicationSearchFragment::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return n;
    }

    @DexIgnore
    public boolean S0() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void T0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            cl2 cl2 = cl2.a;
            tr3<ka2> tr3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (tr3 != null) {
                ka2 a2 = tr3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    kd4.a((Object) activity, "it");
                    cl2.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final tr3<ka2> U0() {
        tr3<ka2> tr3 = this.j;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final a43 V0() {
        a43 a43 = this.l;
        if (a43 != null) {
            return a43;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(List<Pair<Complication, String>> list) {
        kd4.b(list, "results");
        qs2 qs2 = this.k;
        if (qs2 != null) {
            qs2.b(list);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void e(List<Pair<Complication, String>> list) {
        kd4.b(list, "recentSearchResult");
        qs2 qs2 = this.k;
        if (qs2 != null) {
            qs2.a(list);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (ka2) qa.a(layoutInflater, R.layout.fragment_complication_search, viewGroup, false, O0()));
        tr3<ka2> tr3 = this.j;
        if (tr3 != null) {
            ka2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        a43 a43 = this.l;
        if (a43 != null) {
            a43.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a43 a43 = this.l;
        if (a43 != null) {
            a43.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "it");
            a(activity, 550);
        }
        this.k = new qs2();
        tr3<ka2> tr3 = this.j;
        if (tr3 != null) {
            ka2 a2 = tr3.a();
            if (a2 != null) {
                ka2 ka2 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = ka2.t;
                kd4.a((Object) recyclerViewEmptySupport, "this.rvResults");
                qs2 qs2 = this.k;
                if (qs2 != null) {
                    recyclerViewEmptySupport.setAdapter(qs2);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = ka2.t;
                    kd4.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = ka2.t;
                    FlexibleTextView flexibleTextView = ka2.u;
                    kd4.a((Object) flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = ka2.r;
                    kd4.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    ka2.r.setOnClickListener(new b(this));
                    ka2.s.addTextChangedListener(new c(this));
                    ka2.q.setOnClickListener(new d(this));
                    qs2 qs22 = this.k;
                    if (qs22 != null) {
                        qs22.a((qs2.d) new e(this));
                        qs2 qs23 = this.k;
                        if (qs23 != null) {
                            qs23.a((qs2.e) new f(this));
                        } else {
                            kd4.d("mAdapter");
                            throw null;
                        }
                    } else {
                        kd4.d("mAdapter");
                        throw null;
                    }
                } else {
                    kd4.d("mAdapter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void u() {
        qs2 qs2 = this.k;
        if (qs2 != null) {
            qs2.b((List<Pair<Complication, String>>) null);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = aq2.a.a(j2);
        Window window = fragmentActivity.getWindow();
        kd4.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        tr3<ka2> tr3 = this.j;
        if (tr3 != null) {
            ka2 a3 = tr3.a();
            if (a3 != null) {
                kd4.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, ka2 ka2) {
        FlexibleTextView flexibleTextView = ka2.q;
        kd4.a((Object) flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener(new g(ka2, j2));
    }

    @DexIgnore
    public void a(String str) {
        kd4.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        qs2 qs2 = this.k;
        if (qs2 != null) {
            qs2.a(str);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(a43 a43) {
        kd4.b(a43, "presenter");
        this.l = a43;
    }

    @DexIgnore
    public void a(Complication complication) {
        kd4.b(complication, "selectedComplication");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, new Intent().putExtra("SEARCH_COMPLICATION_RESULT_ID", complication.getComplicationId()));
            cl2 cl2 = cl2.a;
            tr3<ka2> tr3 = this.j;
            FlexibleTextView flexibleTextView = null;
            if (tr3 != null) {
                ka2 a2 = tr3.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    kd4.a((Object) activity, "it");
                    cl2.a(flexibleTextView, activity);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.View");
            }
            kd4.d("mBinding");
            throw null;
        }
    }
}
