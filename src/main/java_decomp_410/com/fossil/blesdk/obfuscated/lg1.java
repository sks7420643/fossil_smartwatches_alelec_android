package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lg1 extends k61 implements kg1 {
    @DexIgnore
    public lg1() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((hg1) r61.a(parcel, hg1.CREATOR), (rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                a((kl1) r61.a(parcel, kl1.CREATOR), (rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 4:
                a((rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                a((hg1) r61.a(parcel, hg1.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                c((rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<kl1> a = a((rl1) r61.a(parcel, rl1.CREATOR), r61.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                return true;
            case 9:
                byte[] a2 = a((hg1) r61.a(parcel, hg1.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                return true;
            case 10:
                a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String d = d((rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(d);
                return true;
            case 12:
                a((vl1) r61.a(parcel, vl1.CREATOR), (rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                a((vl1) r61.a(parcel, vl1.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<kl1> a3 = a(parcel.readString(), parcel.readString(), r61.a(parcel), (rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a3);
                return true;
            case 15:
                List<kl1> a4 = a(parcel.readString(), parcel.readString(), parcel.readString(), r61.a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 16:
                List<vl1> a5 = a(parcel.readString(), parcel.readString(), (rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 17:
                List<vl1> a6 = a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 18:
                b((rl1) r61.a(parcel, rl1.CREATOR));
                parcel2.writeNoException();
                return true;
            default:
                return false;
        }
    }
}
