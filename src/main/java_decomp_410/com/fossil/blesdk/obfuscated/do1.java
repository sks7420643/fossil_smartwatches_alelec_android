package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class do1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ co1 f;

    @DexIgnore
    public do1(co1 co1, wn1 wn1) {
        this.f = co1;
        this.e = wn1;
    }

    @DexIgnore
    public final void run() {
        if (this.e.c()) {
            this.f.c.f();
            return;
        }
        try {
            this.f.c.a(this.f.b.then(this.e));
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.c.a((Exception) e2.getCause());
            } else {
                this.f.c.a((Exception) e2);
            }
        } catch (Exception e3) {
            this.f.c.a(e3);
        }
    }
}
