package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.fj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nj implements fj {
    @DexIgnore
    public /* final */ MutableLiveData<fj.b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ yl<fj.b.c> d; // = yl.e();

    @DexIgnore
    public nj() {
        a(fj.b);
    }

    @DexIgnore
    public void a(fj.b bVar) {
        this.c.a(bVar);
        if (bVar instanceof fj.b.c) {
            this.d.b((fj.b.c) bVar);
        } else if (bVar instanceof fj.b.a) {
            this.d.a(((fj.b.a) bVar).a());
        }
    }
}
