package com.fossil.blesdk.obfuscated;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zr<Data> implements sr<String, Data> {
    @DexIgnore
    public /* final */ sr<Uri, Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tr<String, AssetFileDescriptor> {
        @DexIgnore
        public sr<String, AssetFileDescriptor> a(wr wrVar) {
            return new zr(wrVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements tr<String, ParcelFileDescriptor> {
        @DexIgnore
        public sr<String, ParcelFileDescriptor> a(wr wrVar) {
            return new zr(wrVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements tr<String, InputStream> {
        @DexIgnore
        public sr<String, InputStream> a(wr wrVar) {
            return new zr(wrVar.a(Uri.class, InputStream.class));
        }
    }

    @DexIgnore
    public zr(sr<Uri, Data> srVar) {
        this.a = srVar;
    }

    @DexIgnore
    public static Uri b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return c(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? c(str) : parse;
    }

    @DexIgnore
    public static Uri c(String str) {
        return Uri.fromFile(new File(str));
    }

    @DexIgnore
    public boolean a(String str) {
        return true;
    }

    @DexIgnore
    public sr.a<Data> a(String str, int i, int i2, lo loVar) {
        Uri b2 = b(str);
        if (b2 == null || !this.a.a(b2)) {
            return null;
        }
        return this.a.a(b2, i, i2, loVar);
    }
}
