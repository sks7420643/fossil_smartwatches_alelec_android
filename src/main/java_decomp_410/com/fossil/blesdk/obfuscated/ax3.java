package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.ProtocolException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ax3 implements xo4 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ jo4 g;

    @DexIgnore
    public ax3(int i) {
        this.g = new jo4();
        this.f = i;
    }

    @DexIgnore
    public void a(jo4 jo4, long j) throws IOException {
        if (!this.e) {
            wv3.a(jo4.B(), 0, j);
            if (this.f == -1 || this.g.B() <= ((long) this.f) - j) {
                this.g.a(jo4, j);
                return;
            }
            throw new ProtocolException("exceeded content-length limit of " + this.f + " bytes");
        }
        throw new IllegalStateException("closed");
    }

    @DexIgnore
    public zo4 b() {
        return zo4.d;
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.e) {
            this.e = true;
            if (this.g.B() < ((long) this.f)) {
                throw new ProtocolException("content-length promised " + this.f + " bytes, but received " + this.g.B());
            }
        }
    }

    @DexIgnore
    public long f() throws IOException {
        return this.g.B();
    }

    @DexIgnore
    public void flush() throws IOException {
    }

    @DexIgnore
    public ax3() {
        this(-1);
    }

    @DexIgnore
    public void a(xo4 xo4) throws IOException {
        jo4 jo4 = new jo4();
        jo4 jo42 = this.g;
        jo42.a(jo4, 0, jo42.B());
        xo4.a(jo4, jo4.B());
    }
}
