package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gb4 extends fb4 {
    @DexIgnore
    public static final <T> void a(List<T> list, Comparator<? super T> comparator) {
        kd4.b(list, "$this$sortWith");
        kd4.b(comparator, "comparator");
        if (list.size() > 1) {
            Collections.sort(list, comparator);
        }
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> void c(List<T> list) {
        kd4.b(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }
}
