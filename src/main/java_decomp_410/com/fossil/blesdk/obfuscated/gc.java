package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gc {
    @DexIgnore
    public /* final */ LifecycleRegistry a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public a c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ LifecycleRegistry e;
        @DexIgnore
        public /* final */ Lifecycle.Event f;
        @DexIgnore
        public boolean g; // = false;

        @DexIgnore
        public a(LifecycleRegistry lifecycleRegistry, Lifecycle.Event event) {
            this.e = lifecycleRegistry;
            this.f = event;
        }

        @DexIgnore
        public void run() {
            if (!this.g) {
                this.e.a(this.f);
                this.g = true;
            }
        }
    }

    @DexIgnore
    public gc(LifecycleOwner lifecycleOwner) {
        this.a = new LifecycleRegistry(lifecycleOwner);
    }

    @DexIgnore
    public final void a(Lifecycle.Event event) {
        a aVar = this.c;
        if (aVar != null) {
            aVar.run();
        }
        this.c = new a(this.a, event);
        this.b.postAtFrontOfQueue(this.c);
    }

    @DexIgnore
    public void b() {
        a(Lifecycle.Event.ON_START);
    }

    @DexIgnore
    public void c() {
        a(Lifecycle.Event.ON_CREATE);
    }

    @DexIgnore
    public void d() {
        a(Lifecycle.Event.ON_STOP);
        a(Lifecycle.Event.ON_DESTROY);
    }

    @DexIgnore
    public void e() {
        a(Lifecycle.Event.ON_START);
    }

    @DexIgnore
    public Lifecycle a() {
        return this.a;
    }
}
