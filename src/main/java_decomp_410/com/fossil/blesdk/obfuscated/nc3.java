package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nc3 implements Factory<uc3> {
    @DexIgnore
    public static uc3 a(kc3 kc3) {
        uc3 c = kc3.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
