package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i43 extends u52 {
    @DexIgnore
    public abstract void a(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    public abstract void a(DianaCustomizeViewModel dianaCustomizeViewModel);
}
