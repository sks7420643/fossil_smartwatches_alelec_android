package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ya */
public abstract class C3350ya {
    @DexIgnore
    /* renamed from: a */
    public abstract android.view.View mo1993a(int i);

    @DexIgnore
    /* renamed from: a */
    public androidx.fragment.app.Fragment mo1994a(android.content.Context context, java.lang.String str, android.os.Bundle bundle) {
        return androidx.fragment.app.Fragment.instantiate(context, str, bundle);
    }

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo1995a();
}
