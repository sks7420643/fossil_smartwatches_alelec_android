package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b5 */
public class C1464b5 extends androidx.constraintlayout.solver.widgets.ConstraintWidget {

    @DexIgnore
    /* renamed from: k0 */
    public androidx.constraintlayout.solver.widgets.ConstraintWidget[] f3673k0; // = new androidx.constraintlayout.solver.widgets.ConstraintWidget[4];

    @DexIgnore
    /* renamed from: l0 */
    public int f3674l0; // = 0;

    @DexIgnore
    /* renamed from: K */
    public void mo9048K() {
        this.f3674l0 = 0;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9049b(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        int i = this.f3674l0 + 1;
        androidx.constraintlayout.solver.widgets.ConstraintWidget[] constraintWidgetArr = this.f3673k0;
        if (i > constraintWidgetArr.length) {
            this.f3673k0 = (androidx.constraintlayout.solver.widgets.ConstraintWidget[]) java.util.Arrays.copyOf(constraintWidgetArr, constraintWidgetArr.length * 2);
        }
        androidx.constraintlayout.solver.widgets.ConstraintWidget[] constraintWidgetArr2 = this.f3673k0;
        int i2 = this.f3674l0;
        constraintWidgetArr2[i2] = constraintWidget;
        this.f3674l0 = i2 + 1;
    }
}
