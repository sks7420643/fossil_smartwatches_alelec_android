package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class og0 {
    @DexIgnore
    public /* final */ mg0 a;

    @DexIgnore
    public og0(mg0 mg0) {
        this.a = mg0;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(ng0 ng0) {
        ng0.e.lock();
        try {
            if (ng0.o == this.a) {
                a();
                ng0.e.unlock();
            }
        } finally {
            ng0.e.unlock();
        }
    }
}
