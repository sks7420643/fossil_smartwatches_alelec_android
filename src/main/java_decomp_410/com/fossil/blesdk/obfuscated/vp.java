package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vp<Z> implements aq<Z> {
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ aq<Z> g;
    @DexIgnore
    public /* final */ a h;
    @DexIgnore
    public /* final */ jo i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(jo joVar, vp<?> vpVar);
    }

    @DexIgnore
    public vp(aq<Z> aqVar, boolean z, boolean z2, jo joVar, a aVar) {
        tw.a(aqVar);
        this.g = aqVar;
        this.e = z;
        this.f = z2;
        this.i = joVar;
        tw.a(aVar);
        this.h = aVar;
    }

    @DexIgnore
    public synchronized void a() {
        if (this.j > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.k) {
            this.k = true;
            if (this.f) {
                this.g.a();
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    @DexIgnore
    public int b() {
        return this.g.b();
    }

    @DexIgnore
    public Class<Z> c() {
        return this.g.c();
    }

    @DexIgnore
    public synchronized void d() {
        if (!this.k) {
            this.j++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    @DexIgnore
    public aq<Z> e() {
        return this.g;
    }

    @DexIgnore
    public boolean f() {
        return this.e;
    }

    @DexIgnore
    public void g() {
        boolean z;
        synchronized (this) {
            if (this.j > 0) {
                z = true;
                int i2 = this.j - 1;
                this.j = i2;
                if (i2 != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.h.a(this.i, this);
        }
    }

    @DexIgnore
    public Z get() {
        return this.g.get();
    }

    @DexIgnore
    public synchronized String toString() {
        return "EngineResource{isMemoryCacheable=" + this.e + ", listener=" + this.h + ", key=" + this.i + ", acquired=" + this.j + ", isRecycled=" + this.k + ", resource=" + this.g + '}';
    }
}
