package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hd3 implements Factory<SleepOverviewDayPresenter> {
    @DexIgnore
    public static SleepOverviewDayPresenter a(fd3 fd3, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        return new SleepOverviewDayPresenter(fd3, sleepSummariesRepository, sleepSessionsRepository);
    }
}
