package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class he1 {
    @DexIgnore
    public /* final */ oe1 a;

    @DexIgnore
    public he1(oe1 oe1) {
        this.a = oe1;
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.d(z);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
