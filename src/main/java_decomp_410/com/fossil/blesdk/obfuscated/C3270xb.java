package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.xb */
public interface C3270xb extends androidx.lifecycle.LifecycleOwner {
    @DexIgnore
    androidx.lifecycle.LifecycleRegistry getLifecycle();
}
