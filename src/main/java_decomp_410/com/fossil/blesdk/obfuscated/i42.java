package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i42 implements Factory<h42> {
    @DexIgnore
    public static /* final */ i42 a; // = new i42();

    @DexIgnore
    public static i42 a() {
        return a;
    }

    @DexIgnore
    public static h42 b() {
        return new h42();
    }

    @DexIgnore
    public h42 get() {
        return b();
    }
}
