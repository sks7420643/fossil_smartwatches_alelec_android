package com.fossil.blesdk.obfuscated;

import com.google.android.gms.tasks.RuntimeExecutionException;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class po1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ oo1 f;

    @DexIgnore
    public po1(oo1 oo1, wn1 wn1) {
        this.f = oo1;
        this.e = wn1;
    }

    @DexIgnore
    public final void run() {
        try {
            wn1 then = this.f.b.then(this.e.b());
            if (then == null) {
                this.f.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            then.a(yn1.b, this.f);
            then.a(yn1.b, (sn1) this.f);
            then.a(yn1.b, (qn1) this.f);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.f.onFailure((Exception) e2.getCause());
            } else {
                this.f.onFailure(e2);
            }
        } catch (CancellationException unused) {
            this.f.onCanceled();
        } catch (Exception e3) {
            this.f.onFailure(e3);
        }
    }
}
