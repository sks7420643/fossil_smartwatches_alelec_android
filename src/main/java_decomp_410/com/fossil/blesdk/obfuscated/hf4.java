package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hf4 {
    @DexIgnore
    yd4 a();

    @DexIgnore
    String getValue();

    @DexIgnore
    hf4 next();
}
