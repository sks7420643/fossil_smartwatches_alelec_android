package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.cm4;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tl4 {
    @DexIgnore
    public int a; // = 64;
    @DexIgnore
    public int b; // = 5;
    @DexIgnore
    public Runnable c;
    @DexIgnore
    public ExecutorService d;
    @DexIgnore
    public /* final */ Deque<cm4.b> e; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<cm4.b> f; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Deque<cm4> g; // = new ArrayDeque();

    @DexIgnore
    public tl4(ExecutorService executorService) {
        this.d = executorService;
    }

    @DexIgnore
    public synchronized ExecutorService a() {
        if (this.d == null) {
            this.d = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), jm4.a("OkHttp Dispatcher", false));
        }
        return this.d;
    }

    @DexIgnore
    public void b(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.b = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final int c(cm4.b bVar) {
        int i = 0;
        for (cm4.b next : this.f) {
            if (!next.c().j && next.d().equals(bVar.d())) {
                i++;
            }
        }
        return i;
    }

    @DexIgnore
    public synchronized int c() {
        return this.f.size() + this.g.size();
    }

    @DexIgnore
    public void a(int i) {
        if (i >= 1) {
            synchronized (this) {
                this.a = i;
            }
            b();
            return;
        }
        throw new IllegalArgumentException("max < 1: " + i);
    }

    @DexIgnore
    public final boolean b() {
        int i;
        boolean z;
        ArrayList arrayList = new ArrayList();
        synchronized (this) {
            Iterator<cm4.b> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                cm4.b next = it.next();
                if (this.f.size() >= this.a) {
                    break;
                } else if (c(next) < this.b) {
                    it.remove();
                    arrayList.add(next);
                    this.f.add(next);
                }
            }
            z = c() > 0;
        }
        int size = arrayList.size();
        for (i = 0; i < size; i++) {
            ((cm4.b) arrayList.get(i)).a(a());
        }
        return z;
    }

    @DexIgnore
    public tl4() {
    }

    @DexIgnore
    public void a(cm4.b bVar) {
        synchronized (this) {
            this.e.add(bVar);
        }
        b();
    }

    @DexIgnore
    public synchronized void a(cm4 cm4) {
        this.g.add(cm4);
    }

    @DexIgnore
    public final <T> void a(Deque<T> deque, T t) {
        Runnable runnable;
        synchronized (this) {
            if (deque.remove(t)) {
                runnable = this.c;
            } else {
                throw new AssertionError("Call wasn't in-flight!");
            }
        }
        if (!b() && runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void b(cm4.b bVar) {
        a(this.f, bVar);
    }

    @DexIgnore
    public void b(cm4 cm4) {
        a(this.g, cm4);
    }
}
