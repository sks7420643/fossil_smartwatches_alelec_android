package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z13 implements MembersInjector<DianaCustomizeEditPresenter> {
    @DexIgnore
    public static void a(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
        dianaCustomizeEditPresenter.m();
    }
}
