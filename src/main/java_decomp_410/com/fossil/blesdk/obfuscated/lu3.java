package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lu3<T extends ListenableWorker> {
    @DexIgnore
    T a(Context context, WorkerParameters workerParameters);
}
