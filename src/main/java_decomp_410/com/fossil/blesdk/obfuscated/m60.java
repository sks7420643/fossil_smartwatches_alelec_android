package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m60 extends g70 {
    @DexIgnore
    public long A;
    @DexIgnore
    public Peripheral.State B; // = Peripheral.State.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean C;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m60(Peripheral peripheral, boolean z, long j) {
        super(RequestId.CONNECT, peripheral);
        kd4.b(peripheral, "peripheral");
        this.C = z;
        this.A = j;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new c10(this.C, i().h());
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public void a(d20 d20) {
        kd4.b(d20, "connectionStateChangedNotification");
    }

    @DexIgnore
    public long m() {
        return this.A;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(wa0.a(super.t(), JSONKey.AUTO_CONNECT, Boolean.valueOf(this.C)), JSONKey.PERIPHERAL_CURRENT_STATE, i().getState().getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, i().k());
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.NEW_STATE, this.B.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.B = ((c10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(new JSONObject(), JSONKey.NEW_STATE, this.B.getLogName$blesdk_productionRelease()), 7, (fd4) null));
    }
}
