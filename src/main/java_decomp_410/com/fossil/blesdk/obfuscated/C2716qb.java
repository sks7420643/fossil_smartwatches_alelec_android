package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qb */
public class C2716qb implements android.app.Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public void onActivityDestroyed(android.app.Activity activity) {
    }

    @DexIgnore
    public void onActivityPaused(android.app.Activity activity) {
    }

    @DexIgnore
    public void onActivityResumed(android.app.Activity activity) {
    }

    @DexIgnore
    public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle bundle) {
    }

    @DexIgnore
    public void onActivityStarted(android.app.Activity activity) {
    }
}
