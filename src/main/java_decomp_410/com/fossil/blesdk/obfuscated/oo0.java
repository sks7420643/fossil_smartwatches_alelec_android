package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oo0 extends yy0 implements no0 {
    @DexIgnore
    public oo0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    public final sn0 a(sn0 sn0, String str, int i, sn0 sn02) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        o.writeInt(i);
        az0.a(o, (IInterface) sn02);
        Parcel a = a(2, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final sn0 b(sn0 sn0, String str, int i, sn0 sn02) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        o.writeInt(i);
        az0.a(o, (IInterface) sn02);
        Parcel a = a(3, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
