package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ij3 extends zr2 implements hj3 {
    @DexIgnore
    public static /* final */ Pattern x;
    @DexIgnore
    public static /* final */ a y; // = new a((fd4) null);
    @DexIgnore
    public gj3 j;
    @DexIgnore
    public TextInputEditText k;
    @DexIgnore
    public TextInputEditText l;
    @DexIgnore
    public TextInputEditText m;
    @DexIgnore
    public TextInputLayout n;
    @DexIgnore
    public TextInputLayout o;
    @DexIgnore
    public TextInputLayout p;
    @DexIgnore
    public ProgressButton q;
    @DexIgnore
    public FlexibleTextView r;
    @DexIgnore
    public FlexibleTextView s;
    @DexIgnore
    public RTLImageView t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public HashMap w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ij3 a() {
            return new ij3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public b(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public final void onClick(View view) {
            ij3.c(this.e).clearFocus();
            ij3.b(this.e).clearFocus();
            ij3.a(this.e).clearFocus();
            kd4.a((Object) view, "it");
            view.setFocusable(true);
            if (this.e.T0()) {
                ij3.d(this.e).a(ij3.b(this.e).getEditableText().toString(), ij3.a(this.e).getEditableText().toString());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public c(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.getActivity() != null) {
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    if (!activity.isFinishing()) {
                        FragmentActivity activity2 = this.e.getActivity();
                        if (activity2 != null) {
                            kd4.a((Object) activity2, "activity!!");
                            if (!activity2.isDestroyed()) {
                                FragmentActivity activity3 = this.e.getActivity();
                                if (activity3 != null) {
                                    activity3.finish();
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public d(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            kd4.b(editable, "s");
            ij3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public e(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            kd4.b(editable, "s");
            ij3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public f(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                ij3.f(this.e).setVisibility(0);
                ij3.g(this.e).setVisibility(0);
                return;
            }
            Editable editableText = ij3.a(this.e).getEditableText();
            kd4.a((Object) editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.e.v) {
                ij3.f(this.e).setVisibility(8);
                ij3.g(this.e).setVisibility(8);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ij3 e;

        @DexIgnore
        public g(ij3 ij3) {
            this.e = ij3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            kd4.b(editable, "s");
            ij3.e(this.e).setEnabled(this.e.T0());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kd4.b(charSequence, "s");
        }
    }

    /*
    static {
        kd4.a((Object) ij3.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            x = compile;
        } else {
            kd4.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText a(ij3 ij3) {
        TextInputEditText textInputEditText = ij3.l;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        kd4.d("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText b(ij3 ij3) {
        TextInputEditText textInputEditText = ij3.k;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        kd4.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText c(ij3 ij3) {
        TextInputEditText textInputEditText = ij3.m;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        kd4.d("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ gj3 d(ij3 ij3) {
        gj3 gj3 = ij3.j;
        if (gj3 != null) {
            return gj3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton e(ij3 ij3) {
        ProgressButton progressButton = ij3.q;
        if (progressButton != null) {
            return progressButton;
        }
        kd4.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView f(ij3 ij3) {
        FlexibleTextView flexibleTextView = ij3.r;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        kd4.d("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView g(ij3 ij3) {
        FlexibleTextView flexibleTextView = ij3.s;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        kd4.d("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    public void A0() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.setEnabled(false);
                TextInputEditText textInputEditText = this.k;
                if (textInputEditText != null) {
                    this.u = textInputEditText.getEditableText().toString();
                    O(true);
                    return;
                }
                kd4.d("mEdtOld");
                throw null;
            }
            kd4.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public void J0() {
        if (isActive()) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_PasswordUpdated_Text__YourPasswordHasBeenChanged);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            T(a2);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        TextInputLayout textInputLayout = this.n;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z);
            if (z) {
                TextInputLayout textInputLayout2 = this.n;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_IncorrectPasswordError_Text__YouEnteredAWrongPassword));
                } else {
                    kd4.d("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.n;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    kd4.d("mTvOldError");
                    throw null;
                }
            }
        } else {
            kd4.d("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    public boolean S0() {
        if (getActivity() == null) {
            return false;
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "activity!!");
            if (activity.isFinishing()) {
                return false;
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                kd4.a((Object) activity2, "activity!!");
                if (activity2.isDestroyed()) {
                    return false;
                }
                FragmentActivity activity3 = getActivity();
                if (activity3 != null) {
                    activity3.finish();
                    return true;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final boolean T0() {
        boolean z;
        TextInputEditText textInputEditText = this.k;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.l;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.m;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    boolean z2 = true;
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.r;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_check_text_ok), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = true;
                        } else {
                            kd4.d("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.r;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            kd4.d("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (x.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.s;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.ic_check_text_ok), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            kd4.d("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.s;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(k6.c(PortfolioApp.W.c(), R.drawable.gray_dot), (Drawable) null, (Drawable) null, (Drawable) null);
                            z = false;
                        } else {
                            kd4.d("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.n;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.n;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                kd4.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            kd4.d("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (kd4.a((Object) this.u, (Object) obj)) {
                            TextInputLayout textInputLayout3 = this.n;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.n;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_IncorrectPasswordError_Text__YouEnteredAWrongPassword));
                                } else {
                                    kd4.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.n;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.n;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    kd4.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !kd4.a((Object) obj2, (Object) obj)) {
                            TextInputLayout textInputLayout7 = this.o;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.o;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    kd4.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.o;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.o;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_SamePassword_Text__YourNewPasswordCannotBeThe));
                                } else {
                                    kd4.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.o;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.o;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.p;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.p;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        kd4.d("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    kd4.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            kd4.d("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || kd4.a((Object) obj3, (Object) obj2)) {
                        TextInputLayout textInputLayout15 = this.p;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.p;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                kd4.d("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            kd4.d("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!kd4.a((Object) obj3, (Object) obj2))) {
                            TextInputLayout textInputLayout17 = this.p;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.p;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_ChangePassword_PasswordsdonotMatchError_Text__PasswordsDontMatch));
                                } else {
                                    kd4.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.v = z;
                    if (this.v) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!kd4.a((Object) obj, (Object) obj2)) && kd4.a((Object) obj2, (Object) obj3) && (!kd4.a((Object) obj, (Object) this.u))) {
                                TextInputLayout textInputLayout19 = this.o;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.o;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.p;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.p;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            kd4.d("mTvRepeatError");
                                            throw null;
                                        }
                                        kd4.d("mTvRepeatError");
                                        throw null;
                                    }
                                    kd4.d("mTvNewError");
                                    throw null;
                                }
                                kd4.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.v) {
                        if (obj2.length() != 0) {
                            z2 = false;
                        }
                        if (!z2) {
                            FlexibleTextView flexibleTextView5 = this.r;
                            if (flexibleTextView5 != null) {
                                flexibleTextView5.setVisibility(0);
                                FlexibleTextView flexibleTextView6 = this.s;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                } else {
                                    kd4.d("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                kd4.d("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                kd4.d("mEdtRepeat");
                throw null;
            }
            kd4.d("mEdtNew");
            throw null;
        }
        kd4.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public void m0() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                kd4.a((Object) fragmentManager, "fragmentManager!!");
                ds3.x(fragmentManager);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ye2 ye2 = (ye2) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_profile_change_pass, (ViewGroup) null, false, O0());
        kd4.a((Object) ye2, "binding");
        a(ye2);
        new tr3(this, ye2);
        ProgressButton progressButton = this.q;
        if (progressButton != null) {
            progressButton.setEnabled(false);
            return ye2.d();
        }
        kd4.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        gj3 gj3 = this.j;
        if (gj3 != null) {
            gj3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        gj3 gj3 = this.j;
        if (gj3 != null) {
            gj3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.b();
            } else {
                kd4.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void e() {
        if (isActive()) {
            ProgressButton progressButton = this.q;
            if (progressButton != null) {
                progressButton.c();
            } else {
                kd4.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(gj3 gj3) {
        kd4.b(gj3, "presenter");
        st1.a(gj3);
        kd4.a((Object) gj3, "checkNotNull(presenter)");
        this.j = gj3;
    }

    @DexIgnore
    public final void a(ye2 ye2) {
        TextInputEditText textInputEditText = ye2.r;
        kd4.a((Object) textInputEditText, "binding.etOldPass");
        this.k = textInputEditText;
        TextInputEditText textInputEditText2 = ye2.q;
        kd4.a((Object) textInputEditText2, "binding.etNewPass");
        this.l = textInputEditText2;
        TextInputEditText textInputEditText3 = ye2.s;
        kd4.a((Object) textInputEditText3, "binding.etRepeatPass");
        this.m = textInputEditText3;
        TextInputLayout textInputLayout = ye2.u;
        kd4.a((Object) textInputLayout, "binding.inputOldPass");
        this.n = textInputLayout;
        TextInputLayout textInputLayout2 = ye2.t;
        kd4.a((Object) textInputLayout2, "binding.inputNewPass");
        this.o = textInputLayout2;
        TextInputLayout textInputLayout3 = ye2.v;
        kd4.a((Object) textInputLayout3, "binding.inputRepeatPass");
        this.p = textInputLayout3;
        ProgressButton progressButton = ye2.x;
        kd4.a((Object) progressButton, "binding.save");
        this.q = progressButton;
        FlexibleTextView flexibleTextView = ye2.y;
        kd4.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
        this.r = flexibleTextView;
        FlexibleTextView flexibleTextView2 = ye2.z;
        kd4.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
        this.s = flexibleTextView2;
        RTLImageView rTLImageView = ye2.w;
        kd4.a((Object) rTLImageView, "binding.ivBack");
        this.t = rTLImageView;
        ProgressButton progressButton2 = this.q;
        if (progressButton2 != null) {
            progressButton2.setOnClickListener(new b(this));
            RTLImageView rTLImageView2 = this.t;
            if (rTLImageView2 != null) {
                rTLImageView2.setOnClickListener(new c(this));
                TextInputEditText textInputEditText4 = this.k;
                if (textInputEditText4 != null) {
                    textInputEditText4.addTextChangedListener(new d(this));
                    TextInputEditText textInputEditText5 = this.l;
                    if (textInputEditText5 != null) {
                        textInputEditText5.addTextChangedListener(new e(this));
                        TextInputEditText textInputEditText6 = this.l;
                        if (textInputEditText6 != null) {
                            textInputEditText6.setOnFocusChangeListener(new f(this));
                            TextInputEditText textInputEditText7 = this.m;
                            if (textInputEditText7 != null) {
                                textInputEditText7.addTextChangedListener(new g(this));
                            } else {
                                kd4.d("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            kd4.d("mEdtNew");
                            throw null;
                        }
                    } else {
                        kd4.d("mEdtNew");
                        throw null;
                    }
                } else {
                    kd4.d("mEdtOld");
                    throw null;
                }
            } else {
                kd4.d("mBackButton");
                throw null;
            }
        } else {
            kd4.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public void e(int i, String str) {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, str, childFragmentManager);
        }
    }
}
