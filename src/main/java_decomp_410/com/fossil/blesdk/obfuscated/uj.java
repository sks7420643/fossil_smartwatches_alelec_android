package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkInfo;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.RescheduleReceiver;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uj implements Runnable {
    @DexIgnore
    public static /* final */ String w; // = dj.a("WorkerWrapper");
    @DexIgnore
    public Context e;
    @DexIgnore
    public String f;
    @DexIgnore
    public List<pj> g;
    @DexIgnore
    public WorkerParameters.a h;
    @DexIgnore
    public hl i;
    @DexIgnore
    public ListenableWorker j;
    @DexIgnore
    public ListenableWorker.a k; // = ListenableWorker.a.a();
    @DexIgnore
    public xi l;
    @DexIgnore
    public zl m;
    @DexIgnore
    public WorkDatabase n;
    @DexIgnore
    public il o;
    @DexIgnore
    public zk p;
    @DexIgnore
    public ll q;
    @DexIgnore
    public List<String> r;
    @DexIgnore
    public String s;
    @DexIgnore
    public yl<Boolean> t; // = yl.e();
    @DexIgnore
    public zv1<ListenableWorker.a> u; // = null;
    @DexIgnore
    public volatile boolean v;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yl e;

        @DexIgnore
        public a(yl ylVar) {
            this.e = ylVar;
        }

        @DexIgnore
        public void run() {
            try {
                dj.a().a(uj.w, String.format("Starting work for %s", new Object[]{uj.this.i.c}), new Throwable[0]);
                uj.this.u = uj.this.j.j();
                this.e.a(uj.this.u);
            } catch (Throwable th) {
                this.e.a(th);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ yl e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore
        public b(yl ylVar, String str) {
            this.e = ylVar;
            this.f = str;
        }

        @DexIgnore
        @SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                ListenableWorker.a aVar = (ListenableWorker.a) this.e.get();
                if (aVar == null) {
                    dj.a().b(uj.w, String.format("%s returned a null result. Treating it as a failure.", new Object[]{uj.this.i.c}), new Throwable[0]);
                } else {
                    dj.a().a(uj.w, String.format("%s returned a %s result.", new Object[]{uj.this.i.c, aVar}), new Throwable[0]);
                    uj.this.k = aVar;
                }
            } catch (CancellationException e2) {
                dj.a().c(uj.w, String.format("%s was cancelled", new Object[]{this.f}), e2);
            } catch (InterruptedException | ExecutionException e3) {
                dj.a().b(uj.w, String.format("%s failed because it threw an exception/error", new Object[]{this.f}), e3);
            } catch (Throwable th) {
                uj.this.b();
                throw th;
            }
            uj.this.b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public Context a;
        @DexIgnore
        public ListenableWorker b;
        @DexIgnore
        public zl c;
        @DexIgnore
        public xi d;
        @DexIgnore
        public WorkDatabase e;
        @DexIgnore
        public String f;
        @DexIgnore
        public List<pj> g;
        @DexIgnore
        public WorkerParameters.a h; // = new WorkerParameters.a();

        @DexIgnore
        public c(Context context, xi xiVar, zl zlVar, WorkDatabase workDatabase, String str) {
            this.a = context.getApplicationContext();
            this.c = zlVar;
            this.d = xiVar;
            this.e = workDatabase;
            this.f = str;
        }

        @DexIgnore
        public c a(List<pj> list) {
            this.g = list;
            return this;
        }

        @DexIgnore
        public c a(WorkerParameters.a aVar) {
            if (aVar != null) {
                this.h = aVar;
            }
            return this;
        }

        @DexIgnore
        public uj a() {
            return new uj(this);
        }
    }

    @DexIgnore
    public uj(c cVar) {
        this.e = cVar.a;
        this.m = cVar.c;
        this.f = cVar.f;
        this.g = cVar.g;
        this.h = cVar.h;
        this.j = cVar.b;
        this.l = cVar.d;
        this.n = cVar.e;
        this.o = this.n.d();
        this.p = this.n.a();
        this.q = this.n.e();
    }

    @DexIgnore
    public zv1<Boolean> a() {
        return this.t;
    }

    @DexIgnore
    public void b() {
        boolean z = false;
        if (!i()) {
            this.n.beginTransaction();
            try {
                WorkInfo.State d = this.o.d(this.f);
                if (d == null) {
                    b(false);
                    z = true;
                } else if (d == WorkInfo.State.RUNNING) {
                    a(this.k);
                    z = this.o.d(this.f).isFinished();
                } else if (!d.isFinished()) {
                    c();
                }
                this.n.setTransactionSuccessful();
            } finally {
                this.n.endTransaction();
            }
        }
        List<pj> list = this.g;
        if (list != null) {
            if (z) {
                for (pj a2 : list) {
                    a2.a(this.f);
                }
            }
            qj.a(this.l, this.n, this.g);
        }
    }

    @DexIgnore
    public final void c() {
        this.n.beginTransaction();
        try {
            this.o.a(WorkInfo.State.ENQUEUED, this.f);
            this.o.b(this.f, System.currentTimeMillis());
            this.o.a(this.f, -1);
            this.n.setTransactionSuccessful();
        } finally {
            this.n.endTransaction();
            b(true);
        }
    }

    @DexIgnore
    public final void d() {
        this.n.beginTransaction();
        try {
            this.o.b(this.f, System.currentTimeMillis());
            this.o.a(WorkInfo.State.ENQUEUED, this.f);
            this.o.f(this.f);
            this.o.a(this.f, -1);
            this.n.setTransactionSuccessful();
        } finally {
            this.n.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final void e() {
        WorkInfo.State d = this.o.d(this.f);
        if (d == WorkInfo.State.RUNNING) {
            dj.a().a(w, String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", new Object[]{this.f}), new Throwable[0]);
            b(true);
            return;
        }
        dj.a().a(w, String.format("Status for %s is %s; not doing any work", new Object[]{this.f, d}), new Throwable[0]);
        b(false);
    }

    @DexIgnore
    public final void f() {
        aj a2;
        if (!i()) {
            this.n.beginTransaction();
            try {
                this.i = this.o.e(this.f);
                if (this.i == null) {
                    dj.a().b(w, String.format("Didn't find WorkSpec for id %s", new Object[]{this.f}), new Throwable[0]);
                    b(false);
                } else if (this.i.b != WorkInfo.State.ENQUEUED) {
                    e();
                    this.n.setTransactionSuccessful();
                    dj.a().a(w, String.format("%s is not in ENQUEUED state. Nothing more to do.", new Object[]{this.i.c}), new Throwable[0]);
                    this.n.endTransaction();
                } else {
                    if (this.i.d() || this.i.c()) {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (!(this.i.n == 0) && currentTimeMillis < this.i.a()) {
                            dj.a().a(w, String.format("Delaying execution for %s because it is being executed before schedule.", new Object[]{this.i.c}), new Throwable[0]);
                            b(true);
                            this.n.endTransaction();
                            return;
                        }
                    }
                    this.n.setTransactionSuccessful();
                    this.n.endTransaction();
                    if (this.i.d()) {
                        a2 = this.i.e;
                    } else {
                        bj b2 = this.l.c().b(this.i.d);
                        if (b2 == null) {
                            dj.a().b(w, String.format("Could not create Input Merger %s", new Object[]{this.i.d}), new Throwable[0]);
                            g();
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.i.e);
                        arrayList.addAll(this.o.g(this.f));
                        a2 = b2.a((List<aj>) arrayList);
                    }
                    WorkerParameters workerParameters = new WorkerParameters(UUID.fromString(this.f), a2, this.r, this.h, this.i.k, this.l.b(), this.m, this.l.i(), new xl(this.n, this.m));
                    if (this.j == null) {
                        this.j = this.l.i().b(this.e, this.i.c, workerParameters);
                    }
                    ListenableWorker listenableWorker = this.j;
                    if (listenableWorker == null) {
                        dj.a().b(w, String.format("Could not create Worker %s", new Object[]{this.i.c}), new Throwable[0]);
                        g();
                    } else if (listenableWorker.g()) {
                        dj.a().b(w, String.format("Received an already-used Worker %s; WorkerFactory should return new instances", new Object[]{this.i.c}), new Throwable[0]);
                        g();
                    } else {
                        this.j.i();
                        if (!j()) {
                            e();
                        } else if (!i()) {
                            yl e2 = yl.e();
                            this.m.a().execute(new a(e2));
                            e2.a((Runnable) new b(e2, this.s), (Executor) this.m.b());
                        }
                    }
                }
            } finally {
                this.n.endTransaction();
            }
        }
    }

    @DexIgnore
    public void g() {
        this.n.beginTransaction();
        try {
            a(this.f);
            this.o.a(this.f, ((ListenableWorker.a.C0006a) this.k).d());
            this.n.setTransactionSuccessful();
        } finally {
            this.n.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final void h() {
        this.n.beginTransaction();
        try {
            this.o.a(WorkInfo.State.SUCCEEDED, this.f);
            this.o.a(this.f, ((ListenableWorker.a.c) this.k).d());
            long currentTimeMillis = System.currentTimeMillis();
            for (String next : this.p.a(this.f)) {
                if (this.o.d(next) == WorkInfo.State.BLOCKED && this.p.b(next)) {
                    dj.a().c(w, String.format("Setting status to enqueued for %s", new Object[]{next}), new Throwable[0]);
                    this.o.a(WorkInfo.State.ENQUEUED, next);
                    this.o.b(next, currentTimeMillis);
                }
            }
            this.n.setTransactionSuccessful();
        } finally {
            this.n.endTransaction();
            b(false);
        }
    }

    @DexIgnore
    public final boolean i() {
        if (!this.v) {
            return false;
        }
        dj.a().a(w, String.format("Work interrupted for %s", new Object[]{this.s}), new Throwable[0]);
        WorkInfo.State d = this.o.d(this.f);
        if (d == null) {
            b(false);
        } else {
            b(!d.isFinished());
        }
        return true;
    }

    @DexIgnore
    public final boolean j() {
        this.n.beginTransaction();
        try {
            boolean z = true;
            if (this.o.d(this.f) == WorkInfo.State.ENQUEUED) {
                this.o.a(WorkInfo.State.RUNNING, this.f);
                this.o.h(this.f);
            } else {
                z = false;
            }
            this.n.setTransactionSuccessful();
            return z;
        } finally {
            this.n.endTransaction();
        }
    }

    @DexIgnore
    public void run() {
        this.r = this.q.a(this.f);
        this.s = a(this.r);
        f();
    }

    @DexIgnore
    public void a(boolean z) {
        this.v = true;
        i();
        zv1<ListenableWorker.a> zv1 = this.u;
        if (zv1 != null) {
            zv1.cancel(true);
        }
        ListenableWorker listenableWorker = this.j;
        if (listenableWorker != null) {
            listenableWorker.k();
        }
    }

    @DexIgnore
    public final void a(ListenableWorker.a aVar) {
        if (aVar instanceof ListenableWorker.a.c) {
            dj.a().c(w, String.format("Worker result SUCCESS for %s", new Object[]{this.s}), new Throwable[0]);
            if (this.i.d()) {
                d();
            } else {
                h();
            }
        } else if (aVar instanceof ListenableWorker.a.b) {
            dj.a().c(w, String.format("Worker result RETRY for %s", new Object[]{this.s}), new Throwable[0]);
            c();
        } else {
            dj.a().c(w, String.format("Worker result FAILURE for %s", new Object[]{this.s}), new Throwable[0]);
            if (this.i.d()) {
                d();
            } else {
                g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e A[Catch:{ all -> 0x0039 }] */
    public final void b(boolean z) {
        boolean z2;
        this.n.beginTransaction();
        try {
            List<String> c2 = this.n.d().c();
            if (c2 != null) {
                if (!c2.isEmpty()) {
                    z2 = false;
                    if (z2) {
                        rl.a(this.e, RescheduleReceiver.class, false);
                    }
                    this.n.setTransactionSuccessful();
                    this.n.endTransaction();
                    this.t.b(Boolean.valueOf(z));
                }
            }
            z2 = true;
            if (z2) {
            }
            this.n.setTransactionSuccessful();
            this.n.endTransaction();
            this.t.b(Boolean.valueOf(z));
        } catch (Throwable th) {
            this.n.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final void a(String str) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            if (this.o.d(str2) != WorkInfo.State.CANCELLED) {
                this.o.a(WorkInfo.State.FAILED, str2);
            }
            linkedList.addAll(this.p.a(str2));
        }
    }

    @DexIgnore
    public final String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Work [ id=");
        sb.append(this.f);
        sb.append(", tags={ ");
        boolean z = true;
        for (String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        sb.append(" } ]");
        return sb.toString();
    }
}
