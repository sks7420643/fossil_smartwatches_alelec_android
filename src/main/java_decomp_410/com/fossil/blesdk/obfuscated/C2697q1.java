package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q1 */
public interface C2697q1 {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.q1$a */
    public interface C2698a {
        @DexIgnore
        /* renamed from: a */
        void mo349a(com.fossil.blesdk.obfuscated.C2179k1 k1Var, int i);

        @DexIgnore
        /* renamed from: a */
        boolean mo351a();

        @DexIgnore
        com.fossil.blesdk.obfuscated.C2179k1 getItemData();
    }

    @DexIgnore
    /* renamed from: a */
    void mo374a(com.fossil.blesdk.obfuscated.C1915h1 h1Var);
}
