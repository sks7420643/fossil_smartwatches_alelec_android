package com.fossil.blesdk.obfuscated;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ib1 implements ListIterator<String> {
    @DexIgnore
    public ListIterator<String> e; // = this.g.e.listIterator(this.f);
    @DexIgnore
    public /* final */ /* synthetic */ int f;
    @DexIgnore
    public /* final */ /* synthetic */ hb1 g;

    @DexIgnore
    public ib1(hb1 hb1, int i) {
        this.g = hb1;
        this.f = i;
    }

    @DexIgnore
    public final /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.e.hasPrevious();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.e.next();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.e.nextIndex();
    }

    @DexIgnore
    public final /* synthetic */ Object previous() {
        return this.e.previous();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.e.previousIndex();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException();
    }
}
