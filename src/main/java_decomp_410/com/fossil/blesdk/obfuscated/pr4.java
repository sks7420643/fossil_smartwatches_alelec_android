package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.bm4;
import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.mr4;
import com.fossil.blesdk.obfuscated.yl4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import retrofit.RestMethodInfo;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr4 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ zl4 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ yl4 e;
    @DexIgnore
    public /* final */ am4 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ mr4<?>[] j;
    @DexIgnore
    public /* final */ boolean k;

    @DexIgnore
    public pr4(a aVar) {
        this.a = aVar.b;
        this.b = aVar.a.c;
        this.c = aVar.n;
        this.d = aVar.r;
        this.e = aVar.s;
        this.f = aVar.t;
        this.g = aVar.o;
        this.h = aVar.p;
        this.i = aVar.q;
        this.j = aVar.v;
        this.k = aVar.w;
    }

    @DexIgnore
    public static pr4 a(Retrofit retrofit3, Method method) {
        return new a(retrofit3, method).a();
    }

    @DexIgnore
    public dm4 a(Object[] objArr) throws IOException {
        mr4<?>[] mr4Arr = this.j;
        int length = objArr.length;
        if (length == mr4Arr.length) {
            or4 or4 = new or4(this.c, this.b, this.d, this.e, this.f, this.g, this.h, this.i);
            if (this.k) {
                length--;
            }
            ArrayList arrayList = new ArrayList(length);
            for (int i2 = 0; i2 < length; i2++) {
                arrayList.add(objArr[i2]);
                mr4Arr[i2].a(or4, objArr[i2]);
            }
            dm4.a a2 = or4.a();
            a2.a(jr4.class, new jr4(this.a, arrayList));
            return a2.a();
        }
        throw new IllegalArgumentException("Argument count (" + length + ") doesn't match expected count (" + mr4Arr.length + ")");
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Pattern x; // = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");
        @DexIgnore
        public static /* final */ Pattern y; // = Pattern.compile(RestMethodInfo.PARAM);
        @DexIgnore
        public /* final */ Retrofit a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Annotation[] c;
        @DexIgnore
        public /* final */ Annotation[][] d;
        @DexIgnore
        public /* final */ Type[] e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public boolean p;
        @DexIgnore
        public boolean q;
        @DexIgnore
        public String r;
        @DexIgnore
        public yl4 s;
        @DexIgnore
        public am4 t;
        @DexIgnore
        public Set<String> u;
        @DexIgnore
        public mr4<?>[] v;
        @DexIgnore
        public boolean w;

        @DexIgnore
        public a(Retrofit retrofit3, Method method) {
            this.a = retrofit3;
            this.b = method;
            this.c = method.getAnnotations();
            this.e = method.getGenericParameterTypes();
            this.d = method.getParameterAnnotations();
        }

        @DexIgnore
        public pr4 a() {
            for (Annotation a2 : this.c) {
                a(a2);
            }
            if (this.n != null) {
                if (!this.o) {
                    if (this.q) {
                        throw ur4.a(this.b, "Multipart can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    } else if (this.p) {
                        throw ur4.a(this.b, "FormUrlEncoded can only be specified on HTTP methods with request body (e.g., @POST).", new Object[0]);
                    }
                }
                int length = this.d.length;
                this.v = new mr4[length];
                int i2 = length - 1;
                int i3 = 0;
                while (true) {
                    boolean z = true;
                    if (i3 >= length) {
                        break;
                    }
                    mr4<?>[] mr4Arr = this.v;
                    Type type = this.e[i3];
                    Annotation[] annotationArr = this.d[i3];
                    if (i3 != i2) {
                        z = false;
                    }
                    mr4Arr[i3] = a(i3, type, annotationArr, z);
                    i3++;
                }
                if (this.r == null && !this.m) {
                    throw ur4.a(this.b, "Missing either @%s URL or @Url parameter.", this.n);
                } else if (!this.p && !this.q && !this.o && this.h) {
                    throw ur4.a(this.b, "Non-body HTTP method cannot contain @Body.", new Object[0]);
                } else if (this.p && !this.f) {
                    throw ur4.a(this.b, "Form-encoded method must contain at least one @Field.", new Object[0]);
                } else if (!this.q || this.g) {
                    return new pr4(this);
                } else {
                    throw ur4.a(this.b, "Multipart method must contain at least one @Part.", new Object[0]);
                }
            } else {
                throw ur4.a(this.b, "HTTP method annotation is required (e.g., @GET, @POST, etc.).", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(Annotation annotation) {
            if (annotation instanceof qs4) {
                a("DELETE", ((qs4) annotation).value(), false);
            } else if (annotation instanceof us4) {
                a("GET", ((us4) annotation).value(), false);
            } else if (annotation instanceof vs4) {
                a("HEAD", ((vs4) annotation).value(), false);
            } else if (annotation instanceof at4) {
                a("PATCH", ((at4) annotation).value(), true);
            } else if (annotation instanceof bt4) {
                a("POST", ((bt4) annotation).value(), true);
            } else if (annotation instanceof ct4) {
                a("PUT", ((ct4) annotation).value(), true);
            } else if (annotation instanceof zs4) {
                a("OPTIONS", ((zs4) annotation).value(), false);
            } else if (annotation instanceof ws4) {
                ws4 ws4 = (ws4) annotation;
                a(ws4.method(), ws4.path(), ws4.hasBody());
            } else if (annotation instanceof ys4) {
                String[] value = ((ys4) annotation).value();
                if (value.length != 0) {
                    this.s = a(value);
                    return;
                }
                throw ur4.a(this.b, "@Headers annotation is empty.", new Object[0]);
            } else if (!(annotation instanceof ts4)) {
            } else {
                if (!this.q) {
                    this.p = true;
                    return;
                }
                throw ur4.a(this.b, "Only one encoding annotation is allowed.", new Object[0]);
            }
        }

        @DexIgnore
        public final void a(String str, String str2, boolean z) {
            String str3 = this.n;
            if (str3 == null) {
                this.n = str;
                this.o = z;
                if (!str2.isEmpty()) {
                    int indexOf = str2.indexOf(63);
                    if (indexOf != -1 && indexOf < str2.length() - 1) {
                        String substring = str2.substring(indexOf + 1);
                        if (x.matcher(substring).find()) {
                            throw ur4.a(this.b, "URL query string \"%s\" must not have replace block. For dynamic query parameters use @Query.", substring);
                        }
                    }
                    this.r = str2;
                    this.u = a(str2);
                    return;
                }
                return;
            }
            throw ur4.a(this.b, "Only one HTTP method is allowed. Found: %s and %s.", str3, str);
        }

        @DexIgnore
        public final yl4 a(String[] strArr) {
            yl4.a aVar = new yl4.a();
            for (String str : strArr) {
                int indexOf = str.indexOf(58);
                if (indexOf == -1 || indexOf == 0 || indexOf == str.length() - 1) {
                    throw ur4.a(this.b, "@Headers value must be in the form \"Name: Value\". Found: \"%s\"", str);
                }
                String substring = str.substring(0, indexOf);
                String trim = str.substring(indexOf + 1).trim();
                if (GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(substring)) {
                    try {
                        this.t = am4.a(trim);
                    } catch (IllegalArgumentException e2) {
                        throw ur4.a(this.b, (Throwable) e2, "Malformed content type: %s", trim);
                    }
                } else {
                    aVar.a(substring, trim);
                }
            }
            return aVar.a();
        }

        @DexIgnore
        public final mr4<?> a(int i2, Type type, Annotation[] annotationArr, boolean z) {
            mr4<?> mr4;
            if (annotationArr != null) {
                mr4 = null;
                for (Annotation a2 : annotationArr) {
                    mr4<?> a3 = a(i2, type, annotationArr, a2);
                    if (a3 != null) {
                        if (mr4 == null) {
                            mr4 = a3;
                        } else {
                            throw ur4.a(this.b, i2, "Multiple Retrofit annotations found, only one allowed.", new Object[0]);
                        }
                    }
                }
            } else {
                mr4 = null;
            }
            if (mr4 != null) {
                return mr4;
            }
            if (z) {
                try {
                    if (ur4.b(type) == yb4.class) {
                        this.w = true;
                        return null;
                    }
                } catch (NoClassDefFoundError unused) {
                }
            }
            throw ur4.a(this.b, i2, "No Retrofit annotation found.", new Object[0]);
        }

        @DexIgnore
        public final mr4<?> a(int i2, Type type, Annotation[] annotationArr, Annotation annotation) {
            Class<bm4.b> cls = bm4.b.class;
            if (annotation instanceof kt4) {
                a(i2, type);
                if (this.m) {
                    throw ur4.a(this.b, i2, "Multiple @Url method annotations found.", new Object[0]);
                } else if (this.i) {
                    throw ur4.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.j) {
                    throw ur4.a(this.b, i2, "A @Url parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw ur4.a(this.b, i2, "A @Url parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw ur4.a(this.b, i2, "A @Url parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.r == null) {
                    this.m = true;
                    if (type == zl4.class || type == String.class || type == URI.class || ((type instanceof Class) && "android.net.Uri".equals(((Class) type).getName()))) {
                        return new mr4.n(this.b, i2);
                    }
                    throw ur4.a(this.b, i2, "@Url must be okhttp3.HttpUrl, String, java.net.URI, or android.net.Uri type.", new Object[0]);
                } else {
                    throw ur4.a(this.b, i2, "@Url cannot be used with @%s URL", this.n);
                }
            } else if (annotation instanceof ft4) {
                a(i2, type);
                if (this.j) {
                    throw ur4.a(this.b, i2, "A @Path parameter must not come after a @Query.", new Object[0]);
                } else if (this.k) {
                    throw ur4.a(this.b, i2, "A @Path parameter must not come after a @QueryName.", new Object[0]);
                } else if (this.l) {
                    throw ur4.a(this.b, i2, "A @Path parameter must not come after a @QueryMap.", new Object[0]);
                } else if (this.m) {
                    throw ur4.a(this.b, i2, "@Path parameters may not be used with @Url.", new Object[0]);
                } else if (this.r != null) {
                    this.i = true;
                    ft4 ft4 = (ft4) annotation;
                    String value = ft4.value();
                    a(i2, value);
                    return new mr4.i(this.b, i2, value, this.a.c(type, annotationArr), ft4.encoded());
                } else {
                    throw ur4.a(this.b, i2, "@Path can only be used with relative url on @%s", this.n);
                }
            } else if (annotation instanceof gt4) {
                a(i2, type);
                gt4 gt4 = (gt4) annotation;
                String value2 = gt4.value();
                boolean encoded = gt4.encoded();
                Class<?> b2 = ur4.b(type);
                this.j = true;
                if (Iterable.class.isAssignableFrom(b2)) {
                    if (type instanceof ParameterizedType) {
                        return new mr4.j(value2, this.a.c(ur4.b(0, (ParameterizedType) type), annotationArr), encoded).b();
                    }
                    Method method = this.b;
                    throw ur4.a(method, i2, b2.getSimpleName() + " must include generic type (e.g., " + b2.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b2.isArray()) {
                    return new mr4.j(value2, this.a.c(type, annotationArr), encoded);
                } else {
                    return new mr4.j(value2, this.a.c(a(b2.getComponentType()), annotationArr), encoded).a();
                }
            } else if (annotation instanceof it4) {
                a(i2, type);
                boolean encoded2 = ((it4) annotation).encoded();
                Class<?> b3 = ur4.b(type);
                this.k = true;
                if (Iterable.class.isAssignableFrom(b3)) {
                    if (type instanceof ParameterizedType) {
                        return new mr4.l(this.a.c(ur4.b(0, (ParameterizedType) type), annotationArr), encoded2).b();
                    }
                    Method method2 = this.b;
                    throw ur4.a(method2, i2, b3.getSimpleName() + " must include generic type (e.g., " + b3.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b3.isArray()) {
                    return new mr4.l(this.a.c(type, annotationArr), encoded2);
                } else {
                    return new mr4.l(this.a.c(a(b3.getComponentType()), annotationArr), encoded2).a();
                }
            } else if (annotation instanceof ht4) {
                a(i2, type);
                Class<?> b4 = ur4.b(type);
                this.l = true;
                if (Map.class.isAssignableFrom(b4)) {
                    Type b5 = ur4.b(type, b4, Map.class);
                    if (b5 instanceof ParameterizedType) {
                        ParameterizedType parameterizedType = (ParameterizedType) b5;
                        Type b6 = ur4.b(0, parameterizedType);
                        if (String.class == b6) {
                            return new mr4.k(this.b, i2, this.a.c(ur4.b(1, parameterizedType), annotationArr), ((ht4) annotation).encoded());
                        }
                        Method method3 = this.b;
                        throw ur4.a(method3, i2, "@QueryMap keys must be of type String: " + b6, new Object[0]);
                    }
                    throw ur4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                }
                throw ur4.a(this.b, i2, "@QueryMap parameter type must be Map.", new Object[0]);
            } else if (annotation instanceof xs4) {
                a(i2, type);
                String value3 = ((xs4) annotation).value();
                Class<?> b7 = ur4.b(type);
                if (Iterable.class.isAssignableFrom(b7)) {
                    if (type instanceof ParameterizedType) {
                        return new mr4.f(value3, this.a.c(ur4.b(0, (ParameterizedType) type), annotationArr)).b();
                    }
                    Method method4 = this.b;
                    throw ur4.a(method4, i2, b7.getSimpleName() + " must include generic type (e.g., " + b7.getSimpleName() + "<String>)", new Object[0]);
                } else if (!b7.isArray()) {
                    return new mr4.f(value3, this.a.c(type, annotationArr));
                } else {
                    return new mr4.f(value3, this.a.c(a(b7.getComponentType()), annotationArr)).a();
                }
            } else if (annotation instanceof rs4) {
                a(i2, type);
                if (this.p) {
                    rs4 rs4 = (rs4) annotation;
                    String value4 = rs4.value();
                    boolean encoded3 = rs4.encoded();
                    this.f = true;
                    Class<?> b8 = ur4.b(type);
                    if (Iterable.class.isAssignableFrom(b8)) {
                        if (type instanceof ParameterizedType) {
                            return new mr4.d(value4, this.a.c(ur4.b(0, (ParameterizedType) type), annotationArr), encoded3).b();
                        }
                        Method method5 = this.b;
                        throw ur4.a(method5, i2, b8.getSimpleName() + " must include generic type (e.g., " + b8.getSimpleName() + "<String>)", new Object[0]);
                    } else if (!b8.isArray()) {
                        return new mr4.d(value4, this.a.c(type, annotationArr), encoded3);
                    } else {
                        return new mr4.d(value4, this.a.c(a(b8.getComponentType()), annotationArr), encoded3).a();
                    }
                } else {
                    throw ur4.a(this.b, i2, "@Field parameters can only be used with form encoding.", new Object[0]);
                }
            } else if (annotation instanceof ss4) {
                a(i2, type);
                if (this.p) {
                    Class<?> b9 = ur4.b(type);
                    if (Map.class.isAssignableFrom(b9)) {
                        Type b10 = ur4.b(type, b9, Map.class);
                        if (b10 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType2 = (ParameterizedType) b10;
                            Type b11 = ur4.b(0, parameterizedType2);
                            if (String.class == b11) {
                                gr4 c2 = this.a.c(ur4.b(1, parameterizedType2), annotationArr);
                                this.f = true;
                                return new mr4.e(this.b, i2, c2, ((ss4) annotation).encoded());
                            }
                            Method method6 = this.b;
                            throw ur4.a(method6, i2, "@FieldMap keys must be of type String: " + b11, new Object[0]);
                        }
                        throw ur4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw ur4.a(this.b, i2, "@FieldMap parameter type must be Map.", new Object[0]);
                }
                throw ur4.a(this.b, i2, "@FieldMap parameters can only be used with form encoding.", new Object[0]);
            } else if (annotation instanceof dt4) {
                a(i2, type);
                if (this.q) {
                    dt4 dt4 = (dt4) annotation;
                    this.g = true;
                    String value5 = dt4.value();
                    Class<?> b12 = ur4.b(type);
                    if (!value5.isEmpty()) {
                        yl4 a2 = yl4.a("Content-Disposition", "form-data; name=\"" + value5 + "\"", "Content-Transfer-Encoding", dt4.encoding());
                        if (Iterable.class.isAssignableFrom(b12)) {
                            if (type instanceof ParameterizedType) {
                                Type b13 = ur4.b(0, (ParameterizedType) type);
                                if (!cls.isAssignableFrom(ur4.b(b13))) {
                                    return new mr4.g(this.b, i2, a2, this.a.a(b13, annotationArr, this.c)).b();
                                }
                                throw ur4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                            }
                            Method method7 = this.b;
                            throw ur4.a(method7, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (b12.isArray()) {
                            Class<?> a3 = a(b12.getComponentType());
                            if (!cls.isAssignableFrom(a3)) {
                                return new mr4.g(this.b, i2, a2, this.a.a((Type) a3, annotationArr, this.c)).a();
                            }
                            throw ur4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        } else if (!cls.isAssignableFrom(b12)) {
                            return new mr4.g(this.b, i2, a2, this.a.a(type, annotationArr, this.c));
                        } else {
                            throw ur4.a(this.b, i2, "@Part parameters using the MultipartBody.Part must not include a part name in the annotation.", new Object[0]);
                        }
                    } else if (Iterable.class.isAssignableFrom(b12)) {
                        if (!(type instanceof ParameterizedType)) {
                            Method method8 = this.b;
                            throw ur4.a(method8, i2, b12.getSimpleName() + " must include generic type (e.g., " + b12.getSimpleName() + "<String>)", new Object[0]);
                        } else if (cls.isAssignableFrom(ur4.b(ur4.b(0, (ParameterizedType) type)))) {
                            return mr4.m.a.b();
                        } else {
                            throw ur4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                        }
                    } else if (b12.isArray()) {
                        if (cls.isAssignableFrom(b12.getComponentType())) {
                            return mr4.m.a.a();
                        }
                        throw ur4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    } else if (cls.isAssignableFrom(b12)) {
                        return mr4.m.a;
                    } else {
                        throw ur4.a(this.b, i2, "@Part annotation must supply a name or use MultipartBody.Part parameter type.", new Object[0]);
                    }
                } else {
                    throw ur4.a(this.b, i2, "@Part parameters can only be used with multipart encoding.", new Object[0]);
                }
            } else if (annotation instanceof et4) {
                a(i2, type);
                if (this.q) {
                    this.g = true;
                    Class<?> b14 = ur4.b(type);
                    if (Map.class.isAssignableFrom(b14)) {
                        Type b15 = ur4.b(type, b14, Map.class);
                        if (b15 instanceof ParameterizedType) {
                            ParameterizedType parameterizedType3 = (ParameterizedType) b15;
                            Type b16 = ur4.b(0, parameterizedType3);
                            if (String.class == b16) {
                                Type b17 = ur4.b(1, parameterizedType3);
                                if (!cls.isAssignableFrom(ur4.b(b17))) {
                                    return new mr4.h(this.b, i2, this.a.a(b17, annotationArr, this.c), ((et4) annotation).encoding());
                                }
                                throw ur4.a(this.b, i2, "@PartMap values cannot be MultipartBody.Part. Use @Part List<Part> or a different value type instead.", new Object[0]);
                            }
                            Method method9 = this.b;
                            throw ur4.a(method9, i2, "@PartMap keys must be of type String: " + b16, new Object[0]);
                        }
                        throw ur4.a(this.b, i2, "Map must include generic types (e.g., Map<String, String>)", new Object[0]);
                    }
                    throw ur4.a(this.b, i2, "@PartMap parameter type must be Map.", new Object[0]);
                }
                throw ur4.a(this.b, i2, "@PartMap parameters can only be used with multipart encoding.", new Object[0]);
            } else if (!(annotation instanceof ps4)) {
                return null;
            } else {
                a(i2, type);
                if (this.p || this.q) {
                    throw ur4.a(this.b, i2, "@Body parameters cannot be used with form or multi-part encoding.", new Object[0]);
                } else if (!this.h) {
                    try {
                        gr4 a4 = this.a.a(type, annotationArr, this.c);
                        this.h = true;
                        return new mr4.c(this.b, i2, a4);
                    } catch (RuntimeException e2) {
                        throw ur4.a(this.b, e2, i2, "Unable to create @Body converter for %s", type);
                    }
                } else {
                    throw ur4.a(this.b, i2, "Multiple @Body method annotations found.", new Object[0]);
                }
            }
        }

        @DexIgnore
        public final void a(int i2, Type type) {
            if (ur4.c(type)) {
                throw ur4.a(this.b, i2, "Parameter type must not include a type variable or wildcard: %s", type);
            }
        }

        @DexIgnore
        public final void a(int i2, String str) {
            if (!y.matcher(str).matches()) {
                throw ur4.a(this.b, i2, "@Path parameter name must match %s. Found: %s", x.pattern(), str);
            } else if (!this.u.contains(str)) {
                throw ur4.a(this.b, i2, "URL \"%s\" does not contain \"{%s}\".", this.r, str);
            }
        }

        @DexIgnore
        public static Set<String> a(String str) {
            Matcher matcher = x.matcher(str);
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            while (matcher.find()) {
                linkedHashSet.add(matcher.group(1));
            }
            return linkedHashSet;
        }

        @DexIgnore
        public static Class<?> a(Class<?> cls) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            return Short.TYPE == cls ? Short.class : cls;
        }
    }
}
