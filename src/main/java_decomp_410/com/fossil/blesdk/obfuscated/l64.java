package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l64 implements j64 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public l64(long j, int i) {
        this.a = j;
        this.b = i;
    }

    @DexIgnore
    public long a(int i) {
        return (long) (((double) this.a) * Math.pow((double) this.b, (double) i));
    }
}
