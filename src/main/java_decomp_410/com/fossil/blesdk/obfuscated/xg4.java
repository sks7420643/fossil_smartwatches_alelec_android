package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.aj4;
import kotlin.coroutines.CoroutineContext;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xg4 extends xb4 implements aj4<String> {
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ long e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineContext.b<xg4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public xg4(long j) {
        super(f);
        this.e = j;
    }

    @DexIgnore
    public final long C() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof xg4) {
                if (this.e == ((xg4) obj).e) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public <R> R fold(R r, yc4<? super R, ? super CoroutineContext.a, ? extends R> yc4) {
        kd4.b(yc4, "operation");
        return aj4.a.a(this, r, yc4);
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        kd4.b(bVar, "key");
        return aj4.a.a(this, bVar);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.e;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        kd4.b(bVar, "key");
        return aj4.a.b(this, bVar);
    }

    @DexIgnore
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        return aj4.a.a(this, coroutineContext);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.e + ')';
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r9 != null) goto L_0x0018;
     */
    @DexIgnore
    public String a(CoroutineContext coroutineContext) {
        String str;
        kd4.b(coroutineContext, "context");
        yg4 yg4 = (yg4) coroutineContext.get(yg4.f);
        if (yg4 != null) {
            str = yg4.C();
        }
        str = "coroutine";
        Thread currentThread = Thread.currentThread();
        kd4.a((Object) currentThread, "currentThread");
        String name = currentThread.getName();
        kd4.a((Object) name, "oldName");
        int b = StringsKt__StringsKt.b((CharSequence) name, " @", 0, false, 6, (Object) null);
        if (b < 0) {
            b = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + b + 10);
        String substring = name.substring(0, b);
        kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb.append(substring);
        sb.append(" @");
        sb.append(str);
        sb.append('#');
        sb.append(this.e);
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
        currentThread.setName(sb2);
        return name;
    }

    @DexIgnore
    public void a(CoroutineContext coroutineContext, String str) {
        kd4.b(coroutineContext, "context");
        kd4.b(str, "oldState");
        Thread currentThread = Thread.currentThread();
        kd4.a((Object) currentThread, "Thread.currentThread()");
        currentThread.setName(str);
    }
}
