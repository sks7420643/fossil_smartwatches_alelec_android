package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ss2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.AuthType;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ev2 extends as2 implements og3, ss2.b, ws3.g, ks2 {
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public ng3 k;
    @DexIgnore
    public tr3<ed2> l;
    @DexIgnore
    public fk2 m;
    @DexIgnore
    public ss2 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ev2 a() {
            return new ev2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public b(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                AboutActivity.a aVar = AboutActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public c(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ConnectedAppsActivity.a aVar = ConnectedAppsActivity.D;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public d(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ProfileOptInActivity.a aVar = ProfileOptInActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public e(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ReplaceBatteryActivity.a aVar = ReplaceBatteryActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public f(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.doCameraTask();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public g(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.e.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.r(childFragmentManager);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public h(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ProfileEditActivity.a aVar = ProfileEditActivity.B;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public i(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity, false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public j(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity, false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public k(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ProfileGoalEditActivity.a aVar = ProfileGoalEditActivity.D;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public l(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                ProfileChangePasswordActivity.a aVar = ProfileChangePasswordActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public m(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PreferredUnitActivity.a aVar = PreferredUnitActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ev2 e;

        @DexIgnore
        public n(ev2 ev2) {
            this.e = ev2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                HelpActivity.a aVar = HelpActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    @nq4(122)
    public final void doCameraTask() {
        if (bn2.a(bn2.d, getActivity(), "EDIT_AVATAR", false, 4, (Object) null)) {
            ng3 ng3 = this.k;
            if (ng3 != null) {
                ng3.i();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void I() {
        ss2 ss2 = this.n;
        if (ss2 != null) {
            ss2.b();
        }
    }

    @DexIgnore
    public void L() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.C;
            kd4.a((Object) activity, "it");
            aVar.b(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        vl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void P(String str) {
        kd4.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "user select " + str);
        WatchSettingActivity.a aVar = WatchSettingActivity.C;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "activity!!");
            aVar.a(activity, str);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public String R0() {
        return "HomeProfileFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.z;
                kd4.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(0);
                j5 j5Var = new j5();
                j5Var.c(a2.y);
                j5Var.a(R.id.cl_avg_step, 6, 0, 6);
                j5Var.a(R.id.cl_avg_step, 7, R.id.cl_active_time, 6);
                j5Var.a(R.id.cl_avg_step, 3, 0, 3);
                j5Var.a(R.id.cl_avg_step, 4, R.id.cl_avg_sleep, 3);
                j5Var.a(R.id.cl_active_time, 6, R.id.cl_avg_step, 7);
                j5Var.a(R.id.cl_active_time, 7, 0, 7);
                j5Var.a(R.id.cl_active_time, 3, R.id.cl_avg_step, 3);
                j5Var.a(R.id.cl_active_time, 4, R.id.cl_avg_step, 4);
                j5Var.a(R.id.cl_calories, 6, R.id.cl_avg_step, 6);
                j5Var.a(R.id.cl_calories, 7, R.id.cl_avg_step, 7);
                j5Var.a(R.id.cl_calories, 3, R.id.cl_avg_step, 4);
                j5Var.a(R.id.cl_calories, 4, 0, 4);
                j5Var.a(R.id.cl_avg_sleep, 6, R.id.cl_active_time, 6);
                j5Var.a(R.id.cl_avg_sleep, 7, R.id.cl_active_time, 7);
                j5Var.a(R.id.cl_avg_sleep, 3, R.id.cl_calories, 3);
                j5Var.a(R.id.cl_avg_sleep, 4, R.id.cl_calories, 4);
                j5Var.a(a2.y);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U0() {
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.z;
                kd4.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(8);
                j5 j5Var = new j5();
                j5Var.c(a2.y);
                j5Var.a(R.id.cl_avg_step, 6, 0, 6);
                j5Var.a(R.id.cl_avg_step, 7, R.id.cl_calories, 6);
                j5Var.a(R.id.cl_avg_step, 3, 0, 3);
                j5Var.a(R.id.cl_avg_step, 4, R.id.cl_avg_sleep, 3);
                j5Var.a(R.id.cl_calories, 6, R.id.cl_avg_step, 7);
                j5Var.a(R.id.cl_calories, 7, 0, 7);
                j5Var.a(R.id.cl_calories, 3, R.id.cl_avg_step, 3);
                j5Var.a(R.id.cl_calories, 4, R.id.cl_avg_step, 4);
                j5Var.a(R.id.cl_avg_sleep, 6, R.id.cl_avg_step, 6);
                j5Var.a(R.id.cl_avg_sleep, 7, R.id.cl_avg_step, 7);
                j5Var.a(R.id.cl_avg_sleep, 3, R.id.cl_avg_step, 4);
                j5Var.a(R.id.cl_avg_sleep, 4, 0, 4);
                j5Var.a(a2.y);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(ArrayList<HomeProfilePresenter.b> arrayList) {
        kd4.b(arrayList, Constants.DEVICES);
        ss2 ss2 = this.n;
        if (ss2 != null) {
            ss2.a(arrayList);
        }
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (arrayList.isEmpty()) {
                FlexibleTextView flexibleTextView = a2.Q;
                kd4.a((Object) flexibleTextView, "it.tvDevice");
                flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Profile_Title__Devices));
                FlexibleTextView flexibleTextView2 = a2.B;
                kd4.a((Object) flexibleTextView2, "it.cvPairFirstWatch");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.E;
                kd4.a((Object) flexibleTextView3, "it.ivAddDevice");
                flexibleTextView3.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView4 = a2.Q;
            kd4.a((Object) flexibleTextView4, "it.tvDevice");
            flexibleTextView4.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_DianaProfile_Title__ActiveWatch));
            FlexibleTextView flexibleTextView5 = a2.B;
            kd4.a((Object) flexibleTextView5, "it.cvPairFirstWatch");
            flexibleTextView5.setVisibility(8);
            FlexibleTextView flexibleTextView6 = a2.E;
            kd4.a((Object) flexibleTextView6, "it.ivAddDevice");
            flexibleTextView6.setVisibility(0);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public void o() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.k(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            ng3 ng3 = this.k;
            if (ng3 != null) {
                ng3.a(intent);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ed2 ed2 = (ed2) qa.a(layoutInflater, R.layout.fragment_home_profile, viewGroup, false, O0());
        fk2 a2 = ck2.a((Fragment) this);
        kd4.a((Object) a2, "GlideApp.with(this)");
        this.m = a2;
        ArrayList arrayList = new ArrayList();
        fk2 fk2 = this.m;
        if (fk2 != null) {
            this.n = new ss2(arrayList, fk2, this, PortfolioApp.W.c());
            this.l = new tr3<>(this, ed2);
            tr3<ed2> tr3 = this.l;
            if (tr3 != null) {
                ed2 a3 = tr3.a();
                if (a3 != null) {
                    kd4.a((Object) a3, "mBinding.get()!!");
                    return a3.d();
                }
                kd4.a();
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mGlideRequests");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ng3 ng3 = this.k;
        if (ng3 != null) {
            if (ng3 != null) {
                ng3.g();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ng3 ng3 = this.k;
        if (ng3 != null) {
            if (ng3 != null) {
                ng3.f();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                a2.G.setOnClickListener(new f(this));
                a2.S.setOnClickListener(new g(this));
                a2.F.setOnClickListener(new h(this));
                a2.E.setOnClickListener(new i(this));
                a2.B.setOnClickListener(new j(this));
                a2.w.setOnClickListener(new k(this));
                a2.r.setOnClickListener(new l(this));
                a2.x.setOnClickListener(new m(this));
                a2.t.setOnClickListener(new n(this));
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.u.setOnClickListener(new d(this));
                a2.v.setOnClickListener(new e(this));
                RecyclerView recyclerView = a2.H;
                kd4.a((Object) recyclerView, "binding.rvDevices");
                recyclerView.setAdapter(this.n);
                RecyclerView recyclerView2 = a2.H;
                kd4.a((Object) recyclerView2, "binding.rvDevices");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView3 = a2.H;
                kd4.a((Object) recyclerView3, "binding.rvDevices");
                recyclerView3.setNestedScrollingEnabled(false);
            }
            R("profile_view");
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        if ((str.length() == 0) || getActivity() == null) {
            return;
        }
        if (str.hashCode() != -292748329 || !str.equals("CONFIRM_LOGOUT_ACCOUNT")) {
            super.a(str, i2, intent);
        } else if (i2 == R.id.tv_ok) {
            ng3 ng3 = this.k;
            if (ng3 != null) {
                ng3.h();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(ng3 ng3) {
        kd4.b(ng3, "presenter");
        this.k = ng3;
    }

    @DexIgnore
    public void a(ActivityStatistic activityStatistic) {
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                if (activityStatistic != null) {
                    ActivityStatistic.ActivityDailyBest stepsBestDay = activityStatistic.getStepsBestDay();
                    ActivityStatistic.ActivityDailyBest activeTimeBestDay = activityStatistic.getActiveTimeBestDay();
                    ActivityStatistic.CaloriesBestDay caloriesBestDay = activityStatistic.getCaloriesBestDay();
                    String a3 = sm2.a(getContext(), (int) R.string.character_dash_double);
                    if (stepsBestDay != null) {
                        AutoResizeTextView autoResizeTextView = a2.K;
                        kd4.a((Object) autoResizeTextView, "it.tvAvgActivity");
                        autoResizeTextView.setText(il2.a(stepsBestDay.getValue()));
                        FlexibleTextView flexibleTextView = a2.L;
                        kd4.a((Object) flexibleTextView, "it.tvAvgActivityDate");
                        flexibleTextView.setText(rk2.a(stepsBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView2 = a2.K;
                        kd4.a((Object) autoResizeTextView2, "it.tvAvgActivity");
                        autoResizeTextView2.setText(a3);
                        FlexibleTextView flexibleTextView2 = a2.L;
                        kd4.a((Object) flexibleTextView2, "it.tvAvgActivityDate");
                        flexibleTextView2.setText("");
                    }
                    if (activeTimeBestDay != null) {
                        AutoResizeTextView autoResizeTextView3 = a2.I;
                        kd4.a((Object) autoResizeTextView3, "it.tvAvgActiveTime");
                        autoResizeTextView3.setText(il2.a(activeTimeBestDay.getValue()));
                        FlexibleTextView flexibleTextView3 = a2.J;
                        kd4.a((Object) flexibleTextView3, "it.tvAvgActiveTimeDate");
                        flexibleTextView3.setText(rk2.a(activeTimeBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView4 = a2.I;
                        kd4.a((Object) autoResizeTextView4, "it.tvAvgActiveTime");
                        autoResizeTextView4.setText(a3);
                        FlexibleTextView flexibleTextView4 = a2.J;
                        kd4.a((Object) flexibleTextView4, "it.tvAvgActiveTimeDate");
                        flexibleTextView4.setText("");
                    }
                    if (caloriesBestDay != null) {
                        AutoResizeTextView autoResizeTextView5 = a2.M;
                        kd4.a((Object) autoResizeTextView5, "it.tvAvgCalories");
                        autoResizeTextView5.setText(il2.a(td4.a(caloriesBestDay.getValue())));
                        FlexibleTextView flexibleTextView5 = a2.N;
                        kd4.a((Object) flexibleTextView5, "it.tvAvgCaloriesDate");
                        flexibleTextView5.setText(rk2.a(caloriesBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView6 = a2.M;
                        kd4.a((Object) autoResizeTextView6, "it.tvAvgCalories");
                        autoResizeTextView6.setText(a3);
                        FlexibleTextView flexibleTextView6 = a2.N;
                        kd4.a((Object) flexibleTextView6, "it.tvAvgCaloriesDate");
                        flexibleTextView6.setText("");
                    }
                } else {
                    String a4 = sm2.a(getContext(), (int) R.string.character_dash_double);
                    AutoResizeTextView autoResizeTextView7 = a2.K;
                    kd4.a((Object) autoResizeTextView7, "it.tvAvgActivity");
                    autoResizeTextView7.setText(a4);
                    AutoResizeTextView autoResizeTextView8 = a2.M;
                    kd4.a((Object) autoResizeTextView8, "it.tvAvgCalories");
                    autoResizeTextView8.setText(a4);
                    AutoResizeTextView autoResizeTextView9 = a2.I;
                    kd4.a((Object) autoResizeTextView9, "it.tvAvgActiveTime");
                    autoResizeTextView9.setText(a4);
                    FlexibleTextView flexibleTextView7 = a2.L;
                    kd4.a((Object) flexibleTextView7, "it.tvAvgActivityDate");
                    flexibleTextView7.setText("");
                    FlexibleTextView flexibleTextView8 = a2.N;
                    kd4.a((Object) flexibleTextView8, "it.tvAvgCaloriesDate");
                    flexibleTextView8.setText("");
                    FlexibleTextView flexibleTextView9 = a2.J;
                    kd4.a((Object) flexibleTextView9, "it.tvAvgActiveTimeDate");
                    flexibleTextView9.setText("");
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeProfileFragment", "active serial =" + PortfolioApp.W.c().e());
            if (DeviceIdentityUtils.isDianaDevice(PortfolioApp.W.c().e())) {
                T0();
            } else {
                U0();
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(SleepStatistic sleepStatistic) {
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                if (sleepStatistic != null) {
                    SleepStatistic.SleepDailyBest sleepTimeBestDay = sleepStatistic.getSleepTimeBestDay();
                    if (sleepTimeBestDay != null) {
                        FlexibleTextView flexibleTextView = a2.O;
                        kd4.a((Object) flexibleTextView, "it.tvAvgSleep");
                        flexibleTextView.setText(ll2.b.d(sleepTimeBestDay.getValue()));
                        FlexibleTextView flexibleTextView2 = a2.P;
                        kd4.a((Object) flexibleTextView2, "it.tvAvgSleepDate");
                        flexibleTextView2.setText(rk2.a(sleepTimeBestDay.getDate()));
                        return;
                    }
                }
                FlexibleTextView flexibleTextView3 = a2.O;
                kd4.a((Object) flexibleTextView3, "it.tvAvgSleep");
                flexibleTextView3.setText(sm2.a(getContext(), (int) R.string.character_dash_time));
                FlexibleTextView flexibleTextView4 = a2.P;
                kd4.a((Object) flexibleTextView4, "it.tvAvgSleepDate");
                flexibleTextView4.setText("");
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(MFUser mFUser) {
        String str;
        String str2;
        kd4.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "updateUser");
        if (isActive()) {
            tr3<ed2> tr3 = this.l;
            if (tr3 != null) {
                ed2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.U;
                    kd4.a((Object) flexibleTextView, "it.tvUserName");
                    flexibleTextView.setText(mFUser.getFirstName() + " " + mFUser.getLastName());
                    if (!TextUtils.isEmpty(mFUser.getRegisterDate())) {
                        FlexibleTextView flexibleTextView2 = a2.T;
                        kd4.a((Object) flexibleTextView2, "it.tvMemberSince");
                        pd4 pd4 = pd4.a;
                        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_DianaProfile_Text__JoinedInMonthYear);
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026_Text__JoinedInMonthYear)");
                        Object[] objArr = new Object[1];
                        String registerDate = mFUser.getRegisterDate();
                        kd4.a((Object) registerDate, "user.registerDate");
                        if (registerDate != null) {
                            String substring = registerDate.substring(0, 4);
                            kd4.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            objArr[0] = substring;
                            String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                            kd4.a((Object) format, "java.lang.String.format(format, *args)");
                            flexibleTextView2.setText(format);
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String profilePicture = mFUser.getProfilePicture();
                    String str3 = mFUser.getFirstName() + " " + mFUser.getLastName();
                    if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
                        fk2 fk2 = this.m;
                        if (fk2 != null) {
                            fk2.a((Object) new bk2(mFUser.getProfilePicture(), str3)).a((lv<?>) new rv().a((oo<Bitmap>) new nk2())).a((ImageView) a2.G);
                            FossilCircleImageView fossilCircleImageView = a2.G;
                            kd4.a((Object) fossilCircleImageView, "it.ivUserAvatar");
                            Context context = getContext();
                            if (context != null) {
                                fossilCircleImageView.setBorderColor(k6.a(context, (int) R.color.gray));
                                FossilCircleImageView fossilCircleImageView2 = a2.G;
                                kd4.a((Object) fossilCircleImageView2, "it.ivUserAvatar");
                                fossilCircleImageView2.setBorderWidth(3);
                                FossilCircleImageView fossilCircleImageView3 = a2.G;
                                kd4.a((Object) fossilCircleImageView3, "it.ivUserAvatar");
                                Context context2 = getContext();
                                if (context2 != null) {
                                    fossilCircleImageView3.setBackground(k6.c(context2, R.drawable.oval_solid_light_grey));
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.d("mGlideRequests");
                            throw null;
                        }
                    } else {
                        FossilCircleImageView fossilCircleImageView4 = a2.G;
                        fk2 fk22 = this.m;
                        if (fk22 != null) {
                            fossilCircleImageView4.a(fk22, profilePicture, str3);
                            FossilCircleImageView fossilCircleImageView5 = a2.G;
                            kd4.a((Object) fossilCircleImageView5, "it.ivUserAvatar");
                            Context context3 = getContext();
                            if (context3 != null) {
                                fossilCircleImageView5.setBorderColor(k6.a(context3, (int) R.color.transparent));
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.d("mGlideRequests");
                            throw null;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "isUseDefaultBiometric = " + mFUser.isUseDefaultBiometric());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "weightInGrams = " + mFUser.getWeightInGrams());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "heightInCentimeters = " + mFUser.getHeightInCentimeters());
                    String string = PortfolioApp.W.c().getString(R.string.character_dash_double);
                    kd4.a((Object) string, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    if (mFUser.getWeightUnit() == Unit.IMPERIAL) {
                        if (mFUser.getWeightInGrams() == 0 || mFUser.isUseDefaultBiometric()) {
                            pd4 pd42 = pd4.a;
                            Object[] objArr2 = new Object[2];
                            objArr2[0] = string;
                            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs);
                            kd4.a((Object) a4, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a4 != null) {
                                String lowerCase = a4.toLowerCase();
                                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                                objArr2[1] = lowerCase;
                                str = String.format("%s %s", Arrays.copyOf(objArr2, objArr2.length));
                                kd4.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            pd4 pd43 = pd4.a;
                            Object[] objArr3 = new Object[2];
                            objArr3[0] = il2.a(pk2.f((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                            String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs);
                            kd4.a((Object) a5, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a5 != null) {
                                String lowerCase2 = a5.toLowerCase();
                                kd4.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                                objArr3[1] = lowerCase2;
                                str = String.format("%s %s", Arrays.copyOf(objArr3, objArr3.length));
                                kd4.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    } else if (mFUser.getWeightInGrams() == 0 || mFUser.isUseDefaultBiometric()) {
                        pd4 pd44 = pd4.a;
                        Object[] objArr4 = new Object[2];
                        objArr4[0] = string;
                        String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Kg);
                        kd4.a((Object) a6, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a6 != null) {
                            String lowerCase3 = a6.toLowerCase();
                            kd4.a((Object) lowerCase3, "(this as java.lang.String).toLowerCase()");
                            objArr4[1] = lowerCase3;
                            str = String.format("%s %s", Arrays.copyOf(objArr4, objArr4.length));
                            kd4.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        pd4 pd45 = pd4.a;
                        Object[] objArr5 = new Object[2];
                        objArr5[0] = il2.a(pk2.e((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                        String a7 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Kg);
                        kd4.a((Object) a7, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a7 != null) {
                            String lowerCase4 = a7.toLowerCase();
                            kd4.a((Object) lowerCase4, "(this as java.lang.String).toLowerCase()");
                            objArr5[1] = lowerCase4;
                            str = String.format("%s %s", Arrays.copyOf(objArr5, objArr5.length));
                            kd4.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String string2 = PortfolioApp.W.c().getString(R.string.character_dash_double);
                    kd4.a((Object) string2, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    if (mFUser.getHeightUnit() == Unit.IMPERIAL) {
                        if (mFUser.getHeightInCentimeters() == 0 || mFUser.isUseDefaultBiometric()) {
                            pd4 pd46 = pd4.a;
                            String a8 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.setting_units_height_single_character);
                            kd4.a((Object) a8, "LanguageHelper.getString\u2026_height_single_character)");
                            Object[] objArr6 = {string2};
                            str2 = String.format(a8, Arrays.copyOf(objArr6, objArr6.length));
                            kd4.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            Pair<Integer, Integer> b2 = pk2.b((float) mFUser.getHeightInCentimeters());
                            pd4 pd47 = pd4.a;
                            String a9 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.setting_units_height_double_character);
                            kd4.a((Object) a9, "LanguageHelper.getString\u2026_height_double_character)");
                            Object[] objArr7 = {String.valueOf(b2.getFirst().intValue()), String.valueOf(b2.getSecond().intValue())};
                            str2 = String.format(a9, Arrays.copyOf(objArr7, objArr7.length));
                            kd4.a((Object) str2, "java.lang.String.format(format, *args)");
                        }
                    } else if (mFUser.getHeightInCentimeters() == 0 || mFUser.isUseDefaultBiometric()) {
                        pd4 pd48 = pd4.a;
                        Object[] objArr8 = new Object[2];
                        objArr8[0] = string2;
                        String a10 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Cm);
                        kd4.a((Object) a10, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a10 != null) {
                            String lowerCase5 = a10.toLowerCase();
                            kd4.a((Object) lowerCase5, "(this as java.lang.String).toLowerCase()");
                            objArr8[1] = lowerCase5;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr8, objArr8.length));
                            kd4.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        pd4 pd49 = pd4.a;
                        Object[] objArr9 = new Object[2];
                        objArr9[0] = String.valueOf(mFUser.getHeightInCentimeters());
                        String a11 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Cm);
                        kd4.a((Object) a11, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a11 != null) {
                            String lowerCase6 = a11.toLowerCase();
                            kd4.a((Object) lowerCase6, "(this as java.lang.String).toLowerCase()");
                            objArr9[1] = lowerCase6;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr9, objArr9.length));
                            kd4.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String str4 = str + ", " + str2;
                    FlexibleTextView flexibleTextView3 = a2.R;
                    kd4.a((Object) flexibleTextView3, "it.tvHeightWeight");
                    flexibleTextView3.setText(str4);
                    if (mFUser.getAuthType() == AuthType.EMAIL) {
                        FlexibleTextView flexibleTextView4 = a2.r;
                        kd4.a((Object) flexibleTextView4, "it.btChangePassword");
                        flexibleTextView4.setVisibility(0);
                        View view = a2.V;
                        kd4.a((Object) view, "it.vChangePasswordSeparatorLine");
                        view.setVisibility(0);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = a2.r;
                    kd4.a((Object) flexibleTextView5, "it.btChangePassword");
                    flexibleTextView5.setVisibility(8);
                    View view2 = a2.V;
                    kd4.a((Object) view2, "it.vChangePasswordSeparatorLine");
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, String str) {
        kd4.b(str, "message");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(mg3 mg3) {
        kd4.b(mg3, "activityDailyBest");
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 != null) {
                AutoResizeTextView autoResizeTextView = a2.K;
                kd4.a((Object) autoResizeTextView, "it.tvAvgActivity");
                autoResizeTextView.setText(il2.a((int) mg3.b()));
                FlexibleTextView flexibleTextView = a2.L;
                kd4.a((Object) flexibleTextView, "it.tvAvgActivityDate");
                flexibleTextView.setText(rk2.a(mg3.a()));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(boolean z, boolean z2) {
        tr3<ed2> tr3 = this.l;
        if (tr3 != null) {
            ed2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                if (z2) {
                    FlexibleTextView flexibleTextView = a2.D;
                    kd4.a((Object) flexibleTextView, "it.ftvTitleLowBattery");
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Diana_LowBattery_Title__YourBatteryIsBelowNumber);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    Object[] objArr = {"25%"};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView2 = a2.C;
                    kd4.a((Object) flexibleTextView2, "it.ftvDescriptionLowBattery");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Diana_LowBattery_Text__PleaseRechargeYourWatchSoon));
                    FlexibleButton flexibleButton = a2.v;
                    kd4.a((Object) flexibleButton, "it.btReplaceBattery");
                    flexibleButton.setVisibility(8);
                } else {
                    FlexibleTextView flexibleTextView3 = a2.D;
                    kd4.a((Object) flexibleTextView3, "it.ftvTitleLowBattery");
                    flexibleTextView3.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Hybrid_LowBattery_Title__YourBatteryIsBelow10));
                    FlexibleTextView flexibleTextView4 = a2.C;
                    kd4.a((Object) flexibleTextView4, "it.ftvDescriptionLowBattery");
                    flexibleTextView4.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Hybrid_LowBattery_Text__ReplaceUsingAToolOrCoin));
                    FlexibleButton flexibleButton2 = a2.v;
                    kd4.a((Object) flexibleButton2, "it.btReplaceBattery");
                    flexibleButton2.setVisibility(0);
                }
                ConstraintLayout constraintLayout = a2.A;
                kd4.a((Object) constraintLayout, "it.clLowBattery");
                constraintLayout.setVisibility(0);
                return;
            }
            ConstraintLayout constraintLayout2 = a2.A;
            kd4.a((Object) constraintLayout2, "it.clLowBattery");
            constraintLayout2.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
