package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ze {
    @DexIgnore
    public static int a(RecyclerView.State state, we weVar, View view, View view2, RecyclerView.m mVar, boolean z, boolean z2) {
        int i;
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(mVar.l(view), mVar.l(view2));
        int max = Math.max(mVar.l(view), mVar.l(view2));
        if (z2) {
            i = Math.max(0, (state.a() - max) - 1);
        } else {
            i = Math.max(0, min);
        }
        if (!z) {
            return i;
        }
        return Math.round((((float) i) * (((float) Math.abs(weVar.a(view2) - weVar.d(view))) / ((float) (Math.abs(mVar.l(view) - mVar.l(view2)) + 1)))) + ((float) (weVar.f() - weVar.d(view))));
    }

    @DexIgnore
    public static int b(RecyclerView.State state, we weVar, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return state.a();
        }
        return (int) ((((float) (weVar.a(view2) - weVar.d(view))) / ((float) (Math.abs(mVar.l(view) - mVar.l(view2)) + 1))) * ((float) state.a()));
    }

    @DexIgnore
    public static int a(RecyclerView.State state, we weVar, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(mVar.l(view) - mVar.l(view2)) + 1;
        }
        return Math.min(weVar.g(), weVar.a(view2) - weVar.d(view));
    }
}
