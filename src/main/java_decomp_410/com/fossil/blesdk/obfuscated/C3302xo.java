package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xo */
public class C3302xo extends com.fossil.blesdk.obfuscated.C1435ap<android.os.ParcelFileDescriptor> {
    @DexIgnore
    public C3302xo(android.content.ContentResolver contentResolver, android.net.Uri uri) {
        super(contentResolver, uri);
    }

    @DexIgnore
    public java.lang.Class<android.os.ParcelFileDescriptor> getDataClass() {
        return android.os.ParcelFileDescriptor.class;
    }

    @DexIgnore
    /* renamed from: a */
    public android.os.ParcelFileDescriptor m16462a(android.net.Uri uri, android.content.ContentResolver contentResolver) throws java.io.FileNotFoundException {
        android.content.res.AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor.getParcelFileDescriptor();
        }
        throw new java.io.FileNotFoundException("FileDescriptor is null for: " + uri);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8871a(android.os.ParcelFileDescriptor parcelFileDescriptor) throws java.io.IOException {
        parcelFileDescriptor.close();
    }
}
