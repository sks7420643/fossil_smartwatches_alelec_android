package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.nn4;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.ConnectException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownServiceException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.connection.RouteException;
import okhttp3.internal.http2.ErrorCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tm4 extends nn4.h implements nl4 {
    @DexIgnore
    public /* final */ ol4 b;
    @DexIgnore
    public /* final */ fm4 c;
    @DexIgnore
    public Socket d;
    @DexIgnore
    public Socket e;
    @DexIgnore
    public xl4 f;
    @DexIgnore
    public Protocol g;
    @DexIgnore
    public nn4 h;
    @DexIgnore
    public lo4 i;
    @DexIgnore
    public ko4 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m; // = 1;
    @DexIgnore
    public /* final */ List<Reference<wm4>> n; // = new ArrayList();
    @DexIgnore
    public long o; // = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;

    @DexIgnore
    public tm4(ol4 ol4, fm4 fm4) {
        this.b = ol4;
        this.c = fm4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x012f  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0143 A[EDGE_INSN: B:63:0x0143->B:56:0x0143 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public void a(int i2, int i3, int i4, int i5, boolean z, jl4 jl4, vl4 vl4) {
        jl4 jl42 = jl4;
        vl4 vl42 = vl4;
        if (this.g == null) {
            List<pl4> b2 = this.c.a().b();
            sm4 sm4 = new sm4(b2);
            if (this.c.a().j() == null) {
                if (b2.contains(pl4.h)) {
                    String g2 = this.c.a().k().g();
                    if (!ao4.d().b(g2)) {
                        throw new RouteException(new UnknownServiceException("CLEARTEXT communication to " + g2 + " not permitted by network security policy"));
                    }
                } else {
                    throw new RouteException(new UnknownServiceException("CLEARTEXT communication not enabled for client"));
                }
            } else if (this.c.a().e().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
                throw new RouteException(new UnknownServiceException("H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"));
            }
            RouteException routeException = null;
            do {
                try {
                    if (this.c.c()) {
                        a(i2, i3, i4, jl4, vl4);
                        if (this.d != null) {
                            int i6 = i2;
                            int i7 = i3;
                        } else if (!this.c.c() && this.d == null) {
                            throw new RouteException(new ProtocolException("Too many tunnel connections attempted: 21"));
                        } else if (this.h == null) {
                            synchronized (this.b) {
                                this.m = this.h.A();
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        try {
                            a(i2, i3, jl42, vl42);
                        } catch (IOException e2) {
                            e = e2;
                            int i8 = i5;
                            jm4.a(this.e);
                            jm4.a(this.d);
                            this.e = null;
                            this.d = null;
                            this.i = null;
                            this.j = null;
                            this.f = null;
                            this.g = null;
                            this.h = null;
                            vl4.a(jl4, this.c.d(), this.c.b(), (Protocol) null, e);
                            if (routeException != null) {
                            }
                            if (z) {
                            }
                            throw routeException;
                        }
                    }
                    try {
                        a(sm4, i5, jl42, vl42);
                        vl42.a(jl42, this.c.d(), this.c.b(), this.g);
                        if (!this.c.c()) {
                        }
                        if (this.h == null) {
                        }
                    } catch (IOException e3) {
                        e = e3;
                    }
                } catch (IOException e4) {
                    e = e4;
                    int i9 = i2;
                    int i10 = i3;
                    int i82 = i5;
                    jm4.a(this.e);
                    jm4.a(this.d);
                    this.e = null;
                    this.d = null;
                    this.i = null;
                    this.j = null;
                    this.f = null;
                    this.g = null;
                    this.h = null;
                    vl4.a(jl4, this.c.d(), this.c.b(), (Protocol) null, e);
                    if (routeException != null) {
                        routeException = new RouteException(e);
                    } else {
                        routeException.addConnectException(e);
                    }
                    if (z) {
                        break;
                    } else if (!sm4.a(e)) {
                    }
                    throw routeException;
                }
            } while (!sm4.a(e));
            throw routeException;
        }
        throw new IllegalStateException("already connected");
    }

    @DexIgnore
    public void b() {
        jm4.a(this.d);
    }

    @DexIgnore
    public final dm4 c() throws IOException {
        dm4.a aVar = new dm4.a();
        aVar.a(this.c.a().k());
        aVar.a("CONNECT", (RequestBody) null);
        aVar.b("Host", jm4.a(this.c.a().k(), true));
        aVar.b("Proxy-Connection", "Keep-Alive");
        aVar.b("User-Agent", km4.a());
        dm4 a = aVar.a();
        Response.a aVar2 = new Response.a();
        aVar2.a(a);
        aVar2.a(Protocol.HTTP_1_1);
        aVar2.a(407);
        aVar2.a("Preemptive Authenticate");
        aVar2.a(jm4.c);
        aVar2.b(-1);
        aVar2.a(-1);
        aVar2.b("Proxy-Authenticate", "OkHttp-Preemptive");
        dm4 authenticate = this.c.a().g().authenticate(this.c, aVar2.a());
        return authenticate != null ? authenticate : a;
    }

    @DexIgnore
    public xl4 d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.h != null;
    }

    @DexIgnore
    public fm4 f() {
        return this.c;
    }

    @DexIgnore
    public Socket g() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Connection{");
        sb.append(this.c.a().k().g());
        sb.append(":");
        sb.append(this.c.a().k().k());
        sb.append(", proxy=");
        sb.append(this.c.b());
        sb.append(" hostAddress=");
        sb.append(this.c.d());
        sb.append(" cipherSuite=");
        xl4 xl4 = this.f;
        sb.append(xl4 != null ? xl4.a() : "none");
        sb.append(" protocol=");
        sb.append(this.g);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, jl4 jl4, vl4 vl4) throws IOException {
        dm4 c2 = c();
        zl4 g2 = c2.g();
        int i5 = 0;
        while (i5 < 21) {
            a(i2, i3, jl4, vl4);
            c2 = a(i3, i4, c2, g2);
            if (c2 != null) {
                jm4.a(this.d);
                this.d = null;
                this.j = null;
                this.i = null;
                vl4.a(jl4, this.c.d(), this.c.b(), (Protocol) null);
                i5++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, jl4 jl4, vl4 vl4) throws IOException {
        Socket socket;
        Proxy b2 = this.c.b();
        gl4 a = this.c.a();
        if (b2.type() == Proxy.Type.DIRECT || b2.type() == Proxy.Type.HTTP) {
            socket = a.i().createSocket();
        } else {
            socket = new Socket(b2);
        }
        this.d = socket;
        vl4.a(jl4, this.c.d(), b2);
        this.d.setSoTimeout(i3);
        try {
            ao4.d().a(this.d, this.c.d(), i2);
            try {
                this.i = so4.a(so4.b(this.d));
                this.j = so4.a(so4.a(this.d));
            } catch (NullPointerException e2) {
                if ("throw with null exception".equals(e2.getMessage())) {
                    throw new IOException(e2);
                }
            }
        } catch (ConnectException e3) {
            ConnectException connectException = new ConnectException("Failed to connect to " + this.c.d());
            connectException.initCause(e3);
            throw connectException;
        }
    }

    @DexIgnore
    public final void a(sm4 sm4, int i2, jl4 jl4, vl4 vl4) throws IOException {
        if (this.c.a().j() != null) {
            vl4.g(jl4);
            a(sm4);
            vl4.a(jl4, this.f);
            if (this.g == Protocol.HTTP_2) {
                a(i2);
            }
        } else if (this.c.a().e().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
            this.e = this.d;
            this.g = Protocol.H2_PRIOR_KNOWLEDGE;
            a(i2);
        } else {
            this.e = this.d;
            this.g = Protocol.HTTP_1_1;
        }
    }

    @DexIgnore
    public final void a(int i2) throws IOException {
        this.e.setSoTimeout(0);
        nn4.g gVar = new nn4.g(true);
        gVar.a(this.e, this.c.a().k().g(), this.i, this.j);
        gVar.a((nn4.h) this);
        gVar.a(i2);
        this.h = gVar.a();
        this.h.B();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: javax.net.ssl.SSLSocket} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.net.Socket, javax.net.ssl.SSLSocket] */
    /* JADX WARNING: type inference failed for: r1v2 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0111 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0117 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011a  */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void a(sm4 sm4) throws IOException {
        Object r1;
        Protocol protocol;
        gl4 a = this.c.a();
        String str = null;
        try {
            SSLSocket sSLSocket = (SSLSocket) a.j().createSocket(this.d, a.k().g(), a.k().k(), true);
            try {
                pl4 a2 = sm4.a(sSLSocket);
                if (a2.c()) {
                    ao4.d().a(sSLSocket, a.k().g(), a.e());
                }
                sSLSocket.startHandshake();
                SSLSession session = sSLSocket.getSession();
                xl4 a3 = xl4.a(session);
                if (a.d().verify(a.k().g(), session)) {
                    a.a().a(a.k().g(), a3.c());
                    if (a2.c()) {
                        str = ao4.d().b(sSLSocket);
                    }
                    this.e = sSLSocket;
                    this.i = so4.a(so4.b(this.e));
                    this.j = so4.a(so4.a(this.e));
                    this.f = a3;
                    if (str != null) {
                        protocol = Protocol.get(str);
                    } else {
                        protocol = Protocol.HTTP_1_1;
                    }
                    this.g = protocol;
                    if (sSLSocket != 0) {
                        ao4.d().a(sSLSocket);
                        return;
                    }
                    return;
                }
                X509Certificate x509Certificate = (X509Certificate) a3.c().get(0);
                throw new SSLPeerUnverifiedException("Hostname " + a.k().g() + " not verified:\n    certificate: " + ll4.a((Certificate) x509Certificate) + "\n    DN: " + x509Certificate.getSubjectDN().getName() + "\n    subjectAltNames: " + fo4.a(x509Certificate));
            } catch (AssertionError e2) {
                e = e2;
                str = sSLSocket;
                try {
                    if (!jm4.a(e)) {
                    }
                } catch (Throwable th) {
                    th = th;
                    r1 = str;
                    if (r1 != 0) {
                        ao4.d().a((SSLSocket) r1);
                    }
                    jm4.a((Socket) r1);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                r1 = sSLSocket;
                if (r1 != 0) {
                }
                jm4.a((Socket) r1);
                throw th;
            }
        } catch (AssertionError e3) {
            e = e3;
            if (!jm4.a(e)) {
                throw new IOException(e);
            }
            throw e;
        }
    }

    @DexIgnore
    public final dm4 a(int i2, int i3, dm4 dm4, zl4 zl4) throws IOException {
        String str = "CONNECT " + jm4.a(zl4, true) + " HTTP/1.1";
        while (true) {
            in4 in4 = new in4((OkHttpClient) null, (wm4) null, this.i, this.j);
            this.i.b().a((long) i2, TimeUnit.MILLISECONDS);
            this.j.b().a((long) i3, TimeUnit.MILLISECONDS);
            in4.a(dm4.c(), str);
            in4.a();
            Response.a a = in4.a(false);
            a.a(dm4);
            Response a2 = a.a();
            long a3 = bn4.a(a2);
            if (a3 == -1) {
                a3 = 0;
            }
            yo4 b2 = in4.b(a3);
            jm4.b(b2, Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
            b2.close();
            int B = a2.B();
            if (B != 200) {
                if (B == 407) {
                    dm4 authenticate = this.c.a().g().authenticate(this.c, a2);
                    if (authenticate == null) {
                        throw new IOException("Failed to authenticate with proxy");
                    } else if ("close".equalsIgnoreCase(a2.e("Connection"))) {
                        return authenticate;
                    } else {
                        dm4 = authenticate;
                    }
                } else {
                    throw new IOException("Unexpected response code for CONNECT: " + a2.B());
                }
            } else if (this.i.a().g() && this.j.a().g()) {
                return null;
            } else {
                throw new IOException("TLS tunnel buffered too many bytes!");
            }
        }
    }

    @DexIgnore
    public boolean a(gl4 gl4, fm4 fm4) {
        if (this.n.size() >= this.m || this.k || !hm4.a.a(this.c.a(), gl4)) {
            return false;
        }
        if (gl4.k().g().equals(f().a().k().g())) {
            return true;
        }
        if (this.h == null || fm4 == null || fm4.b().type() != Proxy.Type.DIRECT || this.c.b().type() != Proxy.Type.DIRECT || !this.c.d().equals(fm4.d()) || fm4.a().d() != fo4.a || !a(gl4.k())) {
            return false;
        }
        try {
            gl4.a().a(gl4.k().g(), d().c());
            return true;
        } catch (SSLPeerUnverifiedException unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean a(zl4 zl4) {
        if (zl4.k() != this.c.a().k().k()) {
            return false;
        }
        if (zl4.g().equals(this.c.a().k().g())) {
            return true;
        }
        if (this.f == null || !fo4.a.a(zl4.g(), (X509Certificate) this.f.c().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public zm4 a(OkHttpClient okHttpClient, Interceptor.Chain chain, wm4 wm4) throws SocketException {
        nn4 nn4 = this.h;
        if (nn4 != null) {
            return new mn4(okHttpClient, chain, wm4, nn4);
        }
        this.e.setSoTimeout(chain.a());
        this.i.b().a((long) chain.a(), TimeUnit.MILLISECONDS);
        this.j.b().a((long) chain.b(), TimeUnit.MILLISECONDS);
        return new in4(okHttpClient, wm4, this.i, this.j);
    }

    @DexIgnore
    public boolean a(boolean z) {
        int soTimeout;
        if (this.e.isClosed() || this.e.isInputShutdown() || this.e.isOutputShutdown()) {
            return false;
        }
        nn4 nn4 = this.h;
        if (nn4 != null) {
            return !nn4.z();
        }
        if (z) {
            try {
                soTimeout = this.e.getSoTimeout();
                this.e.setSoTimeout(1);
                if (this.i.g()) {
                    this.e.setSoTimeout(soTimeout);
                    return false;
                }
                this.e.setSoTimeout(soTimeout);
                return true;
            } catch (SocketTimeoutException unused) {
            } catch (IOException unused2) {
                return false;
            } catch (Throwable th) {
                this.e.setSoTimeout(soTimeout);
                throw th;
            }
        }
        return true;
    }

    @DexIgnore
    public void a(pn4 pn4) throws IOException {
        pn4.a(ErrorCode.REFUSED_STREAM);
    }

    @DexIgnore
    public void a(nn4 nn4) {
        synchronized (this.b) {
            this.m = nn4.A();
        }
    }

    @DexIgnore
    public Protocol a() {
        return this.g;
    }
}
