package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zc */
public class C3423zc {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f11533a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zc$a */
    public static class C3424a extends com.fossil.blesdk.obfuscated.C3352yc.C3353a {
        @DexIgnore
        public C3424a(android.content.Context context, com.fossil.blesdk.obfuscated.C3423zc.C3426c cVar) {
            super(context, cVar);
        }

        @DexIgnore
        public void onLoadChildren(java.lang.String str, android.service.media.MediaBrowserService.Result<java.util.List<android.media.browse.MediaBrowser.MediaItem>> result, android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            ((com.fossil.blesdk.obfuscated.C3423zc.C3426c) this.f10895e).mo17382a(str, new com.fossil.blesdk.obfuscated.C3423zc.C3425b(result), bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.zc$b */
    public static class C3425b {

        @DexIgnore
        /* renamed from: a */
        public android.service.media.MediaBrowserService.Result f11534a;

        @DexIgnore
        public C3425b(android.service.media.MediaBrowserService.Result result) {
            this.f11534a = result;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo18461a(java.util.List<android.os.Parcel> list, int i) {
            try {
                com.fossil.blesdk.obfuscated.C3423zc.f11533a.setInt(this.f11534a, i);
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.w("MBSCompatApi26", e);
            }
            this.f11534a.sendResult(mo18460a(list));
        }

        @DexIgnore
        /* renamed from: a */
        public java.util.List<android.media.browse.MediaBrowser.MediaItem> mo18460a(java.util.List<android.os.Parcel> list) {
            if (list == null) {
                return null;
            }
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (android.os.Parcel next : list) {
                next.setDataPosition(0);
                arrayList.add(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(next));
                next.recycle();
            }
            return arrayList;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.zc$c */
    public interface C3426c extends com.fossil.blesdk.obfuscated.C3352yc.C3354b {
        @DexIgnore
        /* renamed from: a */
        void mo17382a(java.lang.String str, com.fossil.blesdk.obfuscated.C3423zc.C3425b bVar, android.os.Bundle bundle);
    }

    /*
    static {
        try {
            f11533a = android.service.media.MediaBrowserService.Result.class.getDeclaredField("mFlags");
            f11533a.setAccessible(true);
        } catch (java.lang.NoSuchFieldException e) {
            android.util.Log.w("MBSCompatApi26", e);
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m17324a(android.content.Context context, com.fossil.blesdk.obfuscated.C3423zc.C3426c cVar) {
        return new com.fossil.blesdk.obfuscated.C3423zc.C3424a(context, cVar);
    }
}
