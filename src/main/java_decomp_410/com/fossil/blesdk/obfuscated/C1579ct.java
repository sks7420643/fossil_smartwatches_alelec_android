package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ct */
public final class C1579ct implements com.fossil.blesdk.obfuscated.C2427mo<java.io.InputStream, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2590os f4192a; // = new com.fossil.blesdk.obfuscated.C2590os();

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(java.io.InputStream inputStream, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return this.f4192a.mo9299a(android.graphics.ImageDecoder.createSource(com.fossil.blesdk.obfuscated.C2255kw.m9871a(inputStream)), i, i2, loVar);
    }
}
