package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sv */
public interface C2924sv {
    @DexIgnore
    /* renamed from: a */
    java.lang.Object mo4026a();

    @DexIgnore
    /* renamed from: a */
    void mo4028a(com.bumptech.glide.load.engine.GlideException glideException);

    @DexIgnore
    /* renamed from: a */
    void mo4030a(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar, com.bumptech.glide.load.DataSource dataSource);
}
