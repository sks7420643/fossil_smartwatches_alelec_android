package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wt extends mt<ut> implements wp {
    @DexIgnore
    public wt(ut utVar) {
        super(utVar);
    }

    @DexIgnore
    public void a() {
        ((ut) this.e).stop();
        ((ut) this.e).k();
    }

    @DexIgnore
    public int b() {
        return ((ut) this.e).i();
    }

    @DexIgnore
    public Class<ut> c() {
        return ut.class;
    }

    @DexIgnore
    public void d() {
        ((ut) this.e).e().prepareToDraw();
    }
}
