package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ld4 implements cd4 {
    @DexIgnore
    public /* final */ Class<?> e;

    @DexIgnore
    public ld4(Class<?> cls, String str) {
        kd4.b(cls, "jClass");
        kd4.b(str, "moduleName");
        this.e = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof ld4) && kd4.a((Object) a(), (Object) ((ld4) obj).a());
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
