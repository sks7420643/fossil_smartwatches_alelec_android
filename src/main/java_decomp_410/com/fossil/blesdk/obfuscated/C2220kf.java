package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kf */
public abstract class C2220kf<T> extends com.fossil.blesdk.obfuscated.C3217wf {
    @DexIgnore
    public C2220kf(androidx.room.RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    @DexIgnore
    public abstract void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, T t);

    @DexIgnore
    public abstract java.lang.String createQuery();

    @DexIgnore
    public final int handle(T t) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.mo12781n();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(java.lang.Iterable<T> iterable) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        int i = 0;
        try {
            for (T bind : iterable) {
                bind(acquire, bind);
                i += acquire.mo12781n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                i += acquire.mo12781n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
