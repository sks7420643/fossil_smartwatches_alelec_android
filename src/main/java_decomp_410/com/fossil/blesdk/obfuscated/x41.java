package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x41 implements nc1 {
    @DexIgnore
    public final he0<Status> a(ge0 ge0, rc1 rc1) {
        return ge0.b(new z41(this, ge0, rc1));
    }

    @DexIgnore
    public final he0<Status> a(ge0 ge0, LocationRequest locationRequest, rc1 rc1) {
        bk0.a(Looper.myLooper(), (Object) "Calling thread must be a prepared Looper thread.");
        return ge0.b(new y41(this, ge0, locationRequest, rc1));
    }
}
