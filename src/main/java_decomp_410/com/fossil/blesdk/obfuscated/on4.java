package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import com.fossil.blesdk.obfuscated.kn4;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.internal.http2.ErrorCode;
import okio.ByteString;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class on4 implements Closeable {
    @DexIgnore
    public static /* final */ Logger i; // = Logger.getLogger(ln4.class.getName());
    @DexIgnore
    public /* final */ lo4 e;
    @DexIgnore
    public /* final */ a f; // = new a(this.e);
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ kn4.a h; // = new kn4.a(4096, this.f);

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i, int i2, int i3, boolean z);

        @DexIgnore
        void a(int i, int i2, List<jn4> list) throws IOException;

        @DexIgnore
        void a(int i, long j);

        @DexIgnore
        void a(int i, ErrorCode errorCode);

        @DexIgnore
        void a(int i, ErrorCode errorCode, ByteString byteString);

        @DexIgnore
        void a(boolean z, int i, int i2);

        @DexIgnore
        void a(boolean z, int i, int i2, List<jn4> list);

        @DexIgnore
        void a(boolean z, int i, lo4 lo4, int i2) throws IOException;

        @DexIgnore
        void a(boolean z, tn4 tn4);
    }

    @DexIgnore
    public on4(lo4 lo4, boolean z) {
        this.e = lo4;
        this.g = z;
    }

    @DexIgnore
    public void a(b bVar) throws IOException {
        if (!this.g) {
            ByteString d = this.e.d((long) ln4.a.size());
            if (i.isLoggable(Level.FINE)) {
                i.fine(jm4.a("<< CONNECTION %s", d.hex()));
            }
            if (!ln4.a.equals(d)) {
                ln4.b("Expected a connection header but was %s", d.utf8());
                throw null;
            }
        } else if (!a(true, bVar)) {
            ln4.b("Required SETTINGS preface not received", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void b(b bVar, int i2, byte b2, int i3) throws IOException {
        if (i2 < 8) {
            ln4.b("TYPE_GOAWAY length < 8: %s", Integer.valueOf(i2));
            throw null;
        } else if (i3 == 0) {
            int readInt = this.e.readInt();
            int readInt2 = this.e.readInt();
            int i4 = i2 - 8;
            ErrorCode fromHttp2 = ErrorCode.fromHttp2(readInt2);
            if (fromHttp2 != null) {
                ByteString byteString = ByteString.EMPTY;
                if (i4 > 0) {
                    byteString = this.e.d((long) i4);
                }
                bVar.a(readInt, fromHttp2, byteString);
                return;
            }
            ln4.b("TYPE_GOAWAY unexpected error code: %d", Integer.valueOf(readInt2));
            throw null;
        } else {
            ln4.b("TYPE_GOAWAY streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void c(b bVar, int i2, byte b2, int i3) throws IOException {
        short s = 0;
        if (i3 != 0) {
            boolean z = (b2 & 1) != 0;
            if ((b2 & 8) != 0) {
                s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
            }
            if ((b2 & 32) != 0) {
                a(bVar, i3);
                i2 -= 5;
            }
            bVar.a(z, i3, -1, a(a(i2, b2, s), s, b2, i3));
            return;
        }
        ln4.b("PROTOCOL_ERROR: TYPE_HEADERS streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public final void d(b bVar, int i2, byte b2, int i3) throws IOException {
        boolean z = false;
        if (i2 != 8) {
            ln4.b("TYPE_PING length != 8: %s", Integer.valueOf(i2));
            throw null;
        } else if (i3 == 0) {
            int readInt = this.e.readInt();
            int readInt2 = this.e.readInt();
            if ((b2 & 1) != 0) {
                z = true;
            }
            bVar.a(z, readInt, readInt2);
        } else {
            ln4.b("TYPE_PING streamId != 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void e(b bVar, int i2, byte b2, int i3) throws IOException {
        if (i2 != 5) {
            ln4.b("TYPE_PRIORITY length: %d != 5", Integer.valueOf(i2));
            throw null;
        } else if (i3 != 0) {
            a(bVar, i3);
        } else {
            ln4.b("TYPE_PRIORITY streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void f(b bVar, int i2, byte b2, int i3) throws IOException {
        short s = 0;
        if (i3 != 0) {
            if ((b2 & 8) != 0) {
                s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
            }
            bVar.a(i3, this.e.readInt() & Integer.MAX_VALUE, a(a(i2 - 4, b2, s), s, b2, i3));
            return;
        }
        ln4.b("PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void g(b bVar, int i2, byte b2, int i3) throws IOException {
        if (i2 != 4) {
            ln4.b("TYPE_RST_STREAM length: %d != 4", Integer.valueOf(i2));
            throw null;
        } else if (i3 != 0) {
            int readInt = this.e.readInt();
            ErrorCode fromHttp2 = ErrorCode.fromHttp2(readInt);
            if (fromHttp2 != null) {
                bVar.a(i3, fromHttp2);
                return;
            }
            ln4.b("TYPE_RST_STREAM unexpected error code: %d", Integer.valueOf(readInt));
            throw null;
        } else {
            ln4.b("TYPE_RST_STREAM streamId == 0", new Object[0]);
            throw null;
        }
    }

    @DexIgnore
    public final void h(b bVar, int i2, byte b2, int i3) throws IOException {
        if (i3 != 0) {
            ln4.b("TYPE_SETTINGS streamId != 0", new Object[0]);
            throw null;
        } else if ((b2 & 1) != 0) {
            if (i2 == 0) {
                bVar.a();
            } else {
                ln4.b("FRAME_SIZE_ERROR ack frame should be empty!", new Object[0]);
                throw null;
            }
        } else if (i2 % 6 == 0) {
            tn4 tn4 = new tn4();
            for (int i4 = 0; i4 < i2; i4 += 6) {
                short readShort = this.e.readShort() & NotificationHandMovingConfig.HAND_DEGREE_DEVICE_DEFAULT_POSITION;
                int readInt = this.e.readInt();
                switch (readShort) {
                    case 2:
                        if (!(readInt == 0 || readInt == 1)) {
                            ln4.b("PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1", new Object[0]);
                            throw null;
                        }
                    case 3:
                        readShort = 4;
                        break;
                    case 4:
                        readShort = 7;
                        if (readInt >= 0) {
                            break;
                        } else {
                            ln4.b("PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1", new Object[0]);
                            throw null;
                        }
                    case 5:
                        if (readInt >= 16384 && readInt <= 16777215) {
                            break;
                        } else {
                            ln4.b("PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s", Integer.valueOf(readInt));
                            throw null;
                        }
                        break;
                }
                tn4.a(readShort, readInt);
            }
            bVar.a(false, tn4);
        } else {
            ln4.b("TYPE_SETTINGS length %% 6 != 0: %s", Integer.valueOf(i2));
            throw null;
        }
    }

    @DexIgnore
    public final void i(b bVar, int i2, byte b2, int i3) throws IOException {
        if (i2 == 4) {
            long readInt = ((long) this.e.readInt()) & 2147483647L;
            if (readInt != 0) {
                bVar.a(i3, readInt);
                return;
            }
            ln4.b("windowSizeIncrement was 0", Long.valueOf(readInt));
            throw null;
        }
        ln4.b("TYPE_WINDOW_UPDATE length !=4: %s", Integer.valueOf(i2));
        throw null;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements yo4 {
        @DexIgnore
        public /* final */ lo4 e;
        @DexIgnore
        public int f;
        @DexIgnore
        public byte g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;
        @DexIgnore
        public short j;

        @DexIgnore
        public a(lo4 lo4) {
            this.e = lo4;
        }

        @DexIgnore
        public long b(jo4 jo4, long j2) throws IOException {
            while (true) {
                int i2 = this.i;
                if (i2 == 0) {
                    this.e.skip((long) this.j);
                    this.j = 0;
                    if ((this.g & 4) != 0) {
                        return -1;
                    }
                    c();
                } else {
                    long b = this.e.b(jo4, Math.min(j2, (long) i2));
                    if (b == -1) {
                        return -1;
                    }
                    this.i = (int) (((long) this.i) - b);
                    return b;
                }
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            int i2 = this.h;
            int a = on4.a(this.e);
            this.i = a;
            this.f = a;
            byte readByte = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
            this.g = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
            if (on4.i.isLoggable(Level.FINE)) {
                on4.i.fine(ln4.a(true, this.h, this.f, readByte, this.g));
            }
            this.h = this.e.readInt() & Integer.MAX_VALUE;
            if (readByte != 9) {
                ln4.b("%s != TYPE_CONTINUATION", Byte.valueOf(readByte));
                throw null;
            } else if (this.h != i2) {
                ln4.b("TYPE_CONTINUATION streamId changed", new Object[0]);
                throw null;
            }
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public zo4 b() {
            return this.e.b();
        }
    }

    @DexIgnore
    public boolean a(boolean z, b bVar) throws IOException {
        try {
            this.e.g(9);
            int a2 = a(this.e);
            if (a2 < 0 || a2 > 16384) {
                ln4.b("FRAME_SIZE_ERROR: %s", Integer.valueOf(a2));
                throw null;
            }
            byte readByte = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
            if (!z || readByte == 4) {
                byte readByte2 = (byte) (this.e.readByte() & FileType.MASKED_INDEX);
                int readInt = this.e.readInt() & Integer.MAX_VALUE;
                if (i.isLoggable(Level.FINE)) {
                    i.fine(ln4.a(true, readInt, a2, readByte, readByte2));
                }
                switch (readByte) {
                    case 0:
                        a(bVar, a2, readByte2, readInt);
                        break;
                    case 1:
                        c(bVar, a2, readByte2, readInt);
                        break;
                    case 2:
                        e(bVar, a2, readByte2, readInt);
                        break;
                    case 3:
                        g(bVar, a2, readByte2, readInt);
                        break;
                    case 4:
                        h(bVar, a2, readByte2, readInt);
                        break;
                    case 5:
                        f(bVar, a2, readByte2, readInt);
                        break;
                    case 6:
                        d(bVar, a2, readByte2, readInt);
                        break;
                    case 7:
                        b(bVar, a2, readByte2, readInt);
                        break;
                    case 8:
                        i(bVar, a2, readByte2, readInt);
                        break;
                    default:
                        this.e.skip((long) a2);
                        break;
                }
                return true;
            }
            ln4.b("Expected a SETTINGS frame but was %s", Byte.valueOf(readByte));
            throw null;
        } catch (IOException unused) {
            return false;
        }
    }

    @DexIgnore
    public final List<jn4> a(int i2, short s, byte b2, int i3) throws IOException {
        a aVar = this.f;
        aVar.i = i2;
        aVar.f = i2;
        aVar.j = s;
        aVar.g = b2;
        aVar.h = i3;
        this.h.f();
        return this.h.c();
    }

    @DexIgnore
    public final void a(b bVar, int i2, byte b2, int i3) throws IOException {
        short s = 0;
        if (i3 != 0) {
            boolean z = true;
            boolean z2 = (b2 & 1) != 0;
            if ((b2 & 32) == 0) {
                z = false;
            }
            if (!z) {
                if ((b2 & 8) != 0) {
                    s = (short) (this.e.readByte() & FileType.MASKED_INDEX);
                }
                bVar.a(z2, i3, this.e, a(i2, b2, s));
                this.e.skip((long) s);
                return;
            }
            ln4.b("PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA", new Object[0]);
            throw null;
        }
        ln4.b("PROTOCOL_ERROR: TYPE_DATA streamId == 0", new Object[0]);
        throw null;
    }

    @DexIgnore
    public final void a(b bVar, int i2) throws IOException {
        int readInt = this.e.readInt();
        bVar.a(i2, readInt & Integer.MAX_VALUE, (this.e.readByte() & FileType.MASKED_INDEX) + 1, (Integer.MIN_VALUE & readInt) != 0);
    }

    @DexIgnore
    public static int a(lo4 lo4) throws IOException {
        return (lo4.readByte() & FileType.MASKED_INDEX) | ((lo4.readByte() & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((lo4.readByte() & FileType.MASKED_INDEX) << 8);
    }

    @DexIgnore
    public static int a(int i2, byte b2, short s) throws IOException {
        if ((b2 & 8) != 0) {
            i2--;
        }
        if (s <= i2) {
            return (short) (i2 - s);
        }
        ln4.b("PROTOCOL_ERROR padding %s > remaining length %s", Short.valueOf(s), Integer.valueOf(i2));
        throw null;
    }
}
