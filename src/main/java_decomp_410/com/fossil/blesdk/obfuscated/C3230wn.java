package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wn */
public class C3230wn<TranscodeType> extends com.fossil.blesdk.obfuscated.C2354lv<com.fossil.blesdk.obfuscated.C3230wn<TranscodeType>> implements java.lang.Cloneable, com.fossil.blesdk.obfuscated.C3050un<com.fossil.blesdk.obfuscated.C3230wn<TranscodeType>> {

    @DexIgnore
    /* renamed from: E */
    public /* final */ android.content.Context f10664E;

    @DexIgnore
    /* renamed from: F */
    public /* final */ com.fossil.blesdk.obfuscated.C3299xn f10665F;

    @DexIgnore
    /* renamed from: G */
    public /* final */ java.lang.Class<TranscodeType> f10666G;

    @DexIgnore
    /* renamed from: H */
    public /* final */ com.fossil.blesdk.obfuscated.C2977tn f10667H;

    @DexIgnore
    /* renamed from: I */
    public com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> f10668I;

    @DexIgnore
    /* renamed from: J */
    public java.lang.Object f10669J;

    @DexIgnore
    /* renamed from: K */
    public java.util.List<com.fossil.blesdk.obfuscated.C2771qv<TranscodeType>> f10670K;

    @DexIgnore
    /* renamed from: L */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> f10671L;

    @DexIgnore
    /* renamed from: M */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> f10672M;

    @DexIgnore
    /* renamed from: N */
    public java.lang.Float f10673N;

    @DexIgnore
    /* renamed from: O */
    public boolean f10674O; // = true;

    @DexIgnore
    /* renamed from: P */
    public boolean f10675P;

    @DexIgnore
    /* renamed from: Q */
    public boolean f10676Q;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wn$a */
    public static /* synthetic */ class C3231a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f10677a; // = new int[android.widget.ImageView.ScaleType.values().length];

        @DexIgnore
        /* renamed from: b */
        public static /* final */ /* synthetic */ int[] f10678b; // = new int[com.bumptech.glide.Priority.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /*
        static {
            try {
                f10678b[com.bumptech.glide.Priority.LOW.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError unused) {
            }
            try {
                f10678b[com.bumptech.glide.Priority.NORMAL.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError unused2) {
            }
            try {
                f10678b[com.bumptech.glide.Priority.HIGH.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError unused3) {
            }
            try {
                f10678b[com.bumptech.glide.Priority.IMMEDIATE.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError unused4) {
            }
            f10677a[android.widget.ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            f10677a[android.widget.ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            f10677a[android.widget.ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            f10677a[android.widget.ImageView.ScaleType.FIT_START.ordinal()] = 4;
            f10677a[android.widget.ImageView.ScaleType.FIT_END.ordinal()] = 5;
            f10677a[android.widget.ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            f10677a[android.widget.ImageView.ScaleType.CENTER.ordinal()] = 7;
            try {
                f10677a[android.widget.ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (java.lang.NoSuchFieldError unused5) {
            }
        }
        */
    }

    /*
    static {
        com.fossil.blesdk.obfuscated.C2842rv rvVar = (com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) new com.fossil.blesdk.obfuscated.C2842rv().mo13441a(com.fossil.blesdk.obfuscated.C2663pp.f8416b)).mo13432a(com.bumptech.glide.Priority.LOW)).mo13444a(true);
    }
    */

    @DexIgnore
    @android.annotation.SuppressLint({"CheckResult"})
    public C3230wn(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C3299xn xnVar, java.lang.Class<TranscodeType> cls, android.content.Context context) {
        this.f10665F = xnVar;
        this.f10666G = cls;
        this.f10664E = context;
        this.f10668I = xnVar.mo17820b(cls);
        this.f10667H = rnVar.mo15648f();
        mo17497a(xnVar.mo17825g());
        mo13438a((com.fossil.blesdk.obfuscated.C2354lv) xnVar.mo17826h());
    }

    @DexIgnore
    /* renamed from: U */
    public com.fossil.blesdk.obfuscated.C2506nv<TranscodeType> mo17483U() {
        return mo17504c(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17502b(com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar) {
        this.f10670K = null;
        return mo17491a(qvVar);
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2506nv<TranscodeType> mo17504c(int i, int i2) {
        com.fossil.blesdk.obfuscated.C2680pv pvVar = new com.fossil.blesdk.obfuscated.C2680pv(i, i2);
        mo17485a(pvVar, pvVar, com.fossil.blesdk.obfuscated.C2606ow.m11981a());
        return pvVar;
    }

    @DexIgnore
    @android.annotation.SuppressLint({"CheckResult"})
    /* renamed from: a */
    public final void mo17497a(java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> list) {
        for (com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object> a : list) {
            mo17491a(a);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17503b(java.lang.Object obj) {
        this.f10669J = obj;
        this.f10675P = true;
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> clone() {
        com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> wnVar = (com.fossil.blesdk.obfuscated.C3230wn) super.clone();
        wnVar.f10668I = wnVar.f10668I.clone();
        return wnVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> m15887a(com.fossil.blesdk.obfuscated.C2354lv<?> lvVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(lvVar);
        return (com.fossil.blesdk.obfuscated.C3230wn) super.mo13438a(lvVar);
    }

    @DexIgnore
    /* renamed from: b */
    public final <Y extends com.fossil.blesdk.obfuscated.C1517bw<TranscodeType>> Y mo17500b(Y y, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, com.fossil.blesdk.obfuscated.C2354lv<?> lvVar, java.util.concurrent.Executor executor) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(y);
        if (this.f10675P) {
            com.fossil.blesdk.obfuscated.C2604ov a = mo17487a(y, qvVar, lvVar, executor);
            com.fossil.blesdk.obfuscated.C2604ov d = y.mo9319d();
            if (!a.mo4033a(d) || mo17498a(lvVar, d)) {
                this.f10665F.mo17817a((com.fossil.blesdk.obfuscated.C1517bw<?>) y);
                y.mo9314a(a);
                this.f10665F.mo17818a(y, a);
                return y;
            }
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(d);
            if (!d.isRunning()) {
                d.mo4035c();
            }
            return y;
        }
        throw new java.lang.IllegalArgumentException("You must call #load() before calling #into()");
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17491a(com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar) {
        if (qvVar != null) {
            if (this.f10670K == null) {
                this.f10670K = new java.util.ArrayList();
            }
            this.f10670K.add(qvVar);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17492a(com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> wnVar) {
        this.f10672M = wnVar;
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17494a(java.lang.Object obj) {
        mo17503b(obj);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17495a(java.lang.String str) {
        mo17503b((java.lang.Object) str);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17490a(android.net.Uri uri) {
        mo17503b((java.lang.Object) uri);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17493a(java.lang.Integer num) {
        mo17503b((java.lang.Object) num);
        return mo13438a((com.fossil.blesdk.obfuscated.C2354lv) com.fossil.blesdk.obfuscated.C2842rv.m13422b(com.fossil.blesdk.obfuscated.C1908gw.m7612a(this.f10664E)));
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> mo17496a(byte[] bArr) {
        mo17503b((java.lang.Object) bArr);
        com.fossil.blesdk.obfuscated.C3230wn a = !mo13414F() ? mo13438a((com.fossil.blesdk.obfuscated.C2354lv) com.fossil.blesdk.obfuscated.C2842rv.m13423b(com.fossil.blesdk.obfuscated.C2663pp.f8415a)) : this;
        return !a.mo13418J() ? a.mo13438a((com.fossil.blesdk.obfuscated.C2354lv) com.fossil.blesdk.obfuscated.C2842rv.m13425c(true)) : a;
    }

    @DexIgnore
    /* renamed from: b */
    public final com.bumptech.glide.Priority mo17499b(com.bumptech.glide.Priority priority) {
        int i = com.fossil.blesdk.obfuscated.C3230wn.C3231a.f10678b[priority.ordinal()];
        if (i == 1) {
            return com.bumptech.glide.Priority.NORMAL;
        }
        if (i == 2) {
            return com.bumptech.glide.Priority.HIGH;
        }
        if (i == 3 || i == 4) {
            return com.bumptech.glide.Priority.IMMEDIATE;
        }
        throw new java.lang.IllegalArgumentException("unknown priority: " + mo13467x());
    }

    @DexIgnore
    /* renamed from: a */
    public <Y extends com.fossil.blesdk.obfuscated.C1517bw<TranscodeType>> Y mo17484a(Y y) {
        mo17485a(y, (com.fossil.blesdk.obfuscated.C2771qv) null, com.fossil.blesdk.obfuscated.C2606ow.m11982b());
        return y;
    }

    @DexIgnore
    /* renamed from: a */
    public <Y extends com.fossil.blesdk.obfuscated.C1517bw<TranscodeType>> Y mo17485a(Y y, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, java.util.concurrent.Executor executor) {
        mo17500b(y, qvVar, this, executor);
        return y;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo17498a(com.fossil.blesdk.obfuscated.C2354lv<?> lvVar, com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        return !lvVar.mo13415G() && ovVar.mo4039f();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1583cw<android.widget.ImageView, TranscodeType> mo17486a(android.widget.ImageView imageView) {
        com.fossil.blesdk.obfuscated.C2354lv lvVar;
        com.fossil.blesdk.obfuscated.C3066uw.m14931b();
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(imageView);
        if (!mo13421M() && mo13419K() && imageView.getScaleType() != null) {
            switch (com.fossil.blesdk.obfuscated.C3230wn.C3231a.f10677a[imageView.getScaleType().ordinal()]) {
                case 1:
                    lvVar = clone().mo13424P();
                    break;
                case 2:
                    lvVar = clone().mo13425Q();
                    break;
                case 3:
                case 4:
                case 5:
                    lvVar = clone().mo13426R();
                    break;
                case 6:
                    lvVar = clone().mo13425Q();
                    break;
            }
        }
        lvVar = this;
        com.fossil.blesdk.obfuscated.C1583cw<android.widget.ImageView, TranscodeType> a = this.f10667H.mo16510a(imageView, this.f10666G);
        mo17500b(a, (com.fossil.blesdk.obfuscated.C2771qv) null, lvVar, com.fossil.blesdk.obfuscated.C2606ow.m11982b());
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r27v0, types: [com.fossil.blesdk.obfuscated.lv, com.fossil.blesdk.obfuscated.lv<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: b */
    public final com.fossil.blesdk.obfuscated.C2604ov mo17501b(java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, com.bumptech.glide.request.RequestCoordinator requestCoordinator, com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> ynVar, com.bumptech.glide.Priority priority, int i, int i2, com.fossil.blesdk.obfuscated.C2354lv<?> r27, java.util.concurrent.Executor executor) {
        com.bumptech.glide.Priority priority2;
        java.lang.Object obj2 = obj;
        com.bumptech.glide.request.RequestCoordinator requestCoordinator2 = requestCoordinator;
        com.bumptech.glide.Priority priority3 = priority;
        com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> wnVar = this.f10671L;
        if (wnVar != null) {
            if (!this.f10676Q) {
                com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> ynVar2 = wnVar.f10674O ? ynVar : wnVar.f10668I;
                if (this.f10671L.mo13416H()) {
                    priority2 = this.f10671L.mo13467x();
                } else {
                    priority2 = mo17499b(priority3);
                }
                com.bumptech.glide.Priority priority4 = priority2;
                int l = this.f10671L.mo13464l();
                int k = this.f10671L.mo13463k();
                if (com.fossil.blesdk.obfuscated.C3066uw.m14933b(i, i2) && !this.f10671L.mo13422N()) {
                    l = r27.mo13464l();
                    k = r27.mo13463k();
                }
                int i3 = k;
                com.fossil.blesdk.obfuscated.C2989tv tvVar = new com.fossil.blesdk.obfuscated.C2989tv(obj2, requestCoordinator2);
                java.lang.Object obj3 = obj;
                com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar2 = bwVar;
                com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar2 = qvVar;
                com.fossil.blesdk.obfuscated.C2989tv tvVar2 = tvVar;
                com.fossil.blesdk.obfuscated.C2604ov a = mo17489a(obj3, bwVar2, qvVar2, (com.fossil.blesdk.obfuscated.C2354lv<?>) r27, (com.bumptech.glide.request.RequestCoordinator) tvVar, ynVar, priority, i, i2, executor);
                this.f10676Q = true;
                com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> wnVar2 = this.f10671L;
                com.fossil.blesdk.obfuscated.C2604ov a2 = wnVar2.mo17488a(obj3, bwVar2, qvVar2, (com.bumptech.glide.request.RequestCoordinator) tvVar2, ynVar2, priority4, l, i3, (com.fossil.blesdk.obfuscated.C2354lv<?>) wnVar2, executor);
                this.f10676Q = false;
                tvVar2.mo16566a(a, a2);
                return tvVar2;
            }
            throw new java.lang.IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.f10673N == null) {
            return mo17489a(obj, bwVar, qvVar, (com.fossil.blesdk.obfuscated.C2354lv<?>) r27, requestCoordinator, ynVar, priority, i, i2, executor);
        } else {
            com.fossil.blesdk.obfuscated.C2989tv tvVar3 = new com.fossil.blesdk.obfuscated.C2989tv(obj2, requestCoordinator2);
            com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar3 = bwVar;
            com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar3 = qvVar;
            com.fossil.blesdk.obfuscated.C2989tv tvVar4 = tvVar3;
            com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> ynVar3 = ynVar;
            int i4 = i;
            int i5 = i2;
            java.util.concurrent.Executor executor2 = executor;
            tvVar3.mo16566a(mo17489a(obj, bwVar3, qvVar3, (com.fossil.blesdk.obfuscated.C2354lv<?>) r27, (com.bumptech.glide.request.RequestCoordinator) tvVar4, ynVar3, priority, i4, i5, executor2), mo17489a(obj, bwVar3, qvVar3, (com.fossil.blesdk.obfuscated.C2354lv<?>) r27.clone().mo13430a(this.f10673N.floatValue()), (com.bumptech.glide.request.RequestCoordinator) tvVar4, ynVar3, mo17499b(priority3), i4, i5, executor2));
            return tvVar3;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2604ov mo17487a(com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, com.fossil.blesdk.obfuscated.C2354lv<?> lvVar, java.util.concurrent.Executor executor) {
        return mo17488a(new java.lang.Object(), bwVar, qvVar, (com.bumptech.glide.request.RequestCoordinator) null, this.f10668I, lvVar.mo13467x(), lvVar.mo13464l(), lvVar.mo13463k(), lvVar, executor);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2604ov mo17488a(java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, com.bumptech.glide.request.RequestCoordinator requestCoordinator, com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> ynVar, com.bumptech.glide.Priority priority, int i, int i2, com.fossil.blesdk.obfuscated.C2354lv<?> lvVar, java.util.concurrent.Executor executor) {
        com.fossil.blesdk.obfuscated.C2437mv mvVar;
        com.fossil.blesdk.obfuscated.C2437mv mvVar2;
        if (this.f10672M != null) {
            mvVar2 = new com.fossil.blesdk.obfuscated.C2437mv(obj, requestCoordinator);
            mvVar = mvVar2;
        } else {
            java.lang.Object obj2 = obj;
            mvVar = null;
            mvVar2 = requestCoordinator;
        }
        com.fossil.blesdk.obfuscated.C2604ov b = mo17501b(obj, bwVar, qvVar, mvVar2, ynVar, priority, i, i2, lvVar, executor);
        if (mvVar == null) {
            return b;
        }
        int l = this.f10672M.mo13464l();
        int k = this.f10672M.mo13463k();
        if (com.fossil.blesdk.obfuscated.C3066uw.m14933b(i, i2) && !this.f10672M.mo13422N()) {
            l = lvVar.mo13464l();
            k = lvVar.mo13463k();
        }
        com.fossil.blesdk.obfuscated.C3230wn<TranscodeType> wnVar = this.f10672M;
        com.fossil.blesdk.obfuscated.C2437mv mvVar3 = mvVar;
        mvVar3.mo13715a(b, wnVar.mo17488a(obj, bwVar, qvVar, (com.bumptech.glide.request.RequestCoordinator) mvVar3, wnVar.f10668I, wnVar.mo13467x(), l, k, (com.fossil.blesdk.obfuscated.C2354lv<?>) this.f10672M, executor));
        return mvVar3;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2604ov mo17489a(java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<TranscodeType> bwVar, com.fossil.blesdk.obfuscated.C2771qv<TranscodeType> qvVar, com.fossil.blesdk.obfuscated.C2354lv<?> lvVar, com.bumptech.glide.request.RequestCoordinator requestCoordinator, com.fossil.blesdk.obfuscated.C3371yn<?, ? super TranscodeType> ynVar, com.bumptech.glide.Priority priority, int i, int i2, java.util.concurrent.Executor executor) {
        android.content.Context context = this.f10664E;
        com.fossil.blesdk.obfuscated.C2977tn tnVar = this.f10667H;
        return com.bumptech.glide.request.SingleRequest.m2668a(context, tnVar, obj, this.f10669J, this.f10666G, lvVar, i, i2, priority, bwVar, qvVar, this.f10670K, requestCoordinator, tnVar.mo16515d(), ynVar.mo18131a(), executor);
    }
}
