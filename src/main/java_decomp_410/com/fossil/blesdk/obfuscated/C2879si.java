package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.si */
public class C2879si extends com.fossil.blesdk.obfuscated.C2810ri {

    @DexIgnore
    /* renamed from: n */
    public static /* final */ android.graphics.PorterDuff.Mode f9299n; // = android.graphics.PorterDuff.Mode.SRC_IN;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2879si.C2887h f9300f;

    @DexIgnore
    /* renamed from: g */
    public android.graphics.PorterDuffColorFilter f9301g;

    @DexIgnore
    /* renamed from: h */
    public android.graphics.ColorFilter f9302h;

    @DexIgnore
    /* renamed from: i */
    public boolean f9303i;

    @DexIgnore
    /* renamed from: j */
    public boolean f9304j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ float[] f9305k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ android.graphics.Matrix f9306l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ android.graphics.Rect f9307m;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$b")
    /* renamed from: com.fossil.blesdk.obfuscated.si$b */
    public static class C2881b extends com.fossil.blesdk.obfuscated.C2879si.C2885f {
        @DexIgnore
        public C2881b() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16032a(android.content.res.Resources resources, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser) {
            if (com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "pathData")) {
                android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6870d);
                mo16033a(a);
                a.recycle();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo16034b() {
            return true;
        }

        @DexIgnore
        public C2881b(com.fossil.blesdk.obfuscated.C2879si.C2881b bVar) {
            super(bVar);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo16033a(android.content.res.TypedArray typedArray) {
            java.lang.String string = typedArray.getString(0);
            if (string != null) {
                this.f9335b = string;
            }
            java.lang.String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.f9334a = com.fossil.blesdk.obfuscated.C3009u6.m14582a(string2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$c")
    /* renamed from: com.fossil.blesdk.obfuscated.si$c */
    public static class C2882c extends com.fossil.blesdk.obfuscated.C2879si.C2885f {

        @DexIgnore
        /* renamed from: d */
        public int[] f9308d;

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C2462n6 f9309e;

        @DexIgnore
        /* renamed from: f */
        public float f9310f; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: g */
        public com.fossil.blesdk.obfuscated.C2462n6 f9311g;

        @DexIgnore
        /* renamed from: h */
        public float f9312h; // = 1.0f;

        @DexIgnore
        /* renamed from: i */
        public int f9313i; // = 0;

        @DexIgnore
        /* renamed from: j */
        public float f9314j; // = 1.0f;

        @DexIgnore
        /* renamed from: k */
        public float f9315k; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: l */
        public float f9316l; // = 1.0f;

        @DexIgnore
        /* renamed from: m */
        public float f9317m; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: n */
        public android.graphics.Paint.Cap f9318n; // = android.graphics.Paint.Cap.BUTT;

        @DexIgnore
        /* renamed from: o */
        public android.graphics.Paint.Join f9319o; // = android.graphics.Paint.Join.MITER;

        @DexIgnore
        /* renamed from: p */
        public float f9320p; // = 4.0f;

        @DexIgnore
        public C2882c() {
        }

        @DexIgnore
        /* renamed from: a */
        public final android.graphics.Paint.Cap mo16035a(int i, android.graphics.Paint.Cap cap) {
            if (i == 0) {
                return android.graphics.Paint.Cap.BUTT;
            }
            if (i != 1) {
                return i != 2 ? cap : android.graphics.Paint.Cap.SQUARE;
            }
            return android.graphics.Paint.Cap.ROUND;
        }

        @DexIgnore
        public float getFillAlpha() {
            return this.f9314j;
        }

        @DexIgnore
        public int getFillColor() {
            return this.f9311g.mo13859a();
        }

        @DexIgnore
        public float getStrokeAlpha() {
            return this.f9312h;
        }

        @DexIgnore
        public int getStrokeColor() {
            return this.f9309e.mo13859a();
        }

        @DexIgnore
        public float getStrokeWidth() {
            return this.f9310f;
        }

        @DexIgnore
        public float getTrimPathEnd() {
            return this.f9316l;
        }

        @DexIgnore
        public float getTrimPathOffset() {
            return this.f9317m;
        }

        @DexIgnore
        public float getTrimPathStart() {
            return this.f9315k;
        }

        @DexIgnore
        public void setFillAlpha(float f) {
            this.f9314j = f;
        }

        @DexIgnore
        public void setFillColor(int i) {
            this.f9311g.mo13860a(i);
        }

        @DexIgnore
        public void setStrokeAlpha(float f) {
            this.f9312h = f;
        }

        @DexIgnore
        public void setStrokeColor(int i) {
            this.f9309e.mo13860a(i);
        }

        @DexIgnore
        public void setStrokeWidth(float f) {
            this.f9310f = f;
        }

        @DexIgnore
        public void setTrimPathEnd(float f) {
            this.f9316l = f;
        }

        @DexIgnore
        public void setTrimPathOffset(float f) {
            this.f9317m = f;
        }

        @DexIgnore
        public void setTrimPathStart(float f) {
            this.f9315k = f;
        }

        @DexIgnore
        /* renamed from: a */
        public final android.graphics.Paint.Join mo16036a(int i, android.graphics.Paint.Join join) {
            if (i == 0) {
                return android.graphics.Paint.Join.MITER;
            }
            if (i != 1) {
                return i != 2 ? join : android.graphics.Paint.Join.BEVEL;
            }
            return android.graphics.Paint.Join.ROUND;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16037a(android.content.res.Resources resources, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser) {
            android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6869c);
            mo16038a(a, xmlPullParser, theme);
            a.recycle();
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo16038a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources.Theme theme) {
            this.f9308d = null;
            if (com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "pathData")) {
                java.lang.String string = typedArray.getString(0);
                if (string != null) {
                    this.f9335b = string;
                }
                java.lang.String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.f9334a = com.fossil.blesdk.obfuscated.C3009u6.m14582a(string2);
                }
                android.content.res.Resources.Theme theme2 = theme;
                this.f9311g = com.fossil.blesdk.obfuscated.C2863s6.m13542a(typedArray, xmlPullParser, theme2, "fillColor", 1, 0);
                this.f9314j = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "fillAlpha", 12, this.f9314j);
                this.f9318n = mo16035a(com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.f9318n);
                this.f9319o = mo16036a(com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.f9319o);
                this.f9320p = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.f9320p);
                this.f9309e = com.fossil.blesdk.obfuscated.C2863s6.m13542a(typedArray, xmlPullParser, theme2, "strokeColor", 3, 0);
                this.f9312h = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "strokeAlpha", 11, this.f9312h);
                this.f9310f = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "strokeWidth", 4, this.f9310f);
                this.f9316l = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "trimPathEnd", 6, this.f9316l);
                this.f9317m = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "trimPathOffset", 7, this.f9317m);
                this.f9315k = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "trimPathStart", 5, this.f9315k);
                this.f9313i = com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "fillType", 13, this.f9313i);
            }
        }

        @DexIgnore
        public C2882c(com.fossil.blesdk.obfuscated.C2879si.C2882c cVar) {
            super(cVar);
            this.f9308d = cVar.f9308d;
            this.f9309e = cVar.f9309e;
            this.f9310f = cVar.f9310f;
            this.f9312h = cVar.f9312h;
            this.f9311g = cVar.f9311g;
            this.f9313i = cVar.f9313i;
            this.f9314j = cVar.f9314j;
            this.f9315k = cVar.f9315k;
            this.f9316l = cVar.f9316l;
            this.f9317m = cVar.f9317m;
            this.f9318n = cVar.f9318n;
            this.f9319o = cVar.f9319o;
            this.f9320p = cVar.f9320p;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16039a() {
            return this.f9311g.mo13864d() || this.f9309e.mo13864d();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16040a(int[] iArr) {
            return this.f9309e.mo13861a(iArr) | this.f9311g.mo13861a(iArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$d")
    /* renamed from: com.fossil.blesdk.obfuscated.si$d */
    public static class C2883d extends com.fossil.blesdk.obfuscated.C2879si.C2884e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.Matrix f9321a; // = new android.graphics.Matrix();

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.ArrayList<com.fossil.blesdk.obfuscated.C2879si.C2884e> f9322b; // = new java.util.ArrayList<>();

        @DexIgnore
        /* renamed from: c */
        public float f9323c; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: d */
        public float f9324d; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: e */
        public float f9325e; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: f */
        public float f9326f; // = 1.0f;

        @DexIgnore
        /* renamed from: g */
        public float f9327g; // = 1.0f;

        @DexIgnore
        /* renamed from: h */
        public float f9328h; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: i */
        public float f9329i; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: j */
        public /* final */ android.graphics.Matrix f9330j; // = new android.graphics.Matrix();

        @DexIgnore
        /* renamed from: k */
        public int f9331k;

        @DexIgnore
        /* renamed from: l */
        public int[] f9332l;

        @DexIgnore
        /* renamed from: m */
        public java.lang.String f9333m; // = null;

        @DexIgnore
        public C2883d(com.fossil.blesdk.obfuscated.C2879si.C2883d dVar, com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.Object> g4Var) {
            super();
            com.fossil.blesdk.obfuscated.C2879si.C2885f fVar;
            this.f9323c = dVar.f9323c;
            this.f9324d = dVar.f9324d;
            this.f9325e = dVar.f9325e;
            this.f9326f = dVar.f9326f;
            this.f9327g = dVar.f9327g;
            this.f9328h = dVar.f9328h;
            this.f9329i = dVar.f9329i;
            this.f9332l = dVar.f9332l;
            this.f9333m = dVar.f9333m;
            this.f9331k = dVar.f9331k;
            java.lang.String str = this.f9333m;
            if (str != null) {
                g4Var.put(str, this);
            }
            this.f9330j.set(dVar.f9330j);
            java.util.ArrayList<com.fossil.blesdk.obfuscated.C2879si.C2884e> arrayList = dVar.f9322b;
            for (int i = 0; i < arrayList.size(); i++) {
                com.fossil.blesdk.obfuscated.C2879si.C2884e eVar = arrayList.get(i);
                if (eVar instanceof com.fossil.blesdk.obfuscated.C2879si.C2883d) {
                    this.f9322b.add(new com.fossil.blesdk.obfuscated.C2879si.C2883d((com.fossil.blesdk.obfuscated.C2879si.C2883d) eVar, g4Var));
                } else {
                    if (eVar instanceof com.fossil.blesdk.obfuscated.C2879si.C2882c) {
                        fVar = new com.fossil.blesdk.obfuscated.C2879si.C2882c((com.fossil.blesdk.obfuscated.C2879si.C2882c) eVar);
                    } else if (eVar instanceof com.fossil.blesdk.obfuscated.C2879si.C2881b) {
                        fVar = new com.fossil.blesdk.obfuscated.C2879si.C2881b((com.fossil.blesdk.obfuscated.C2879si.C2881b) eVar);
                    } else {
                        throw new java.lang.IllegalStateException("Unknown object in the tree!");
                    }
                    this.f9322b.add(fVar);
                    java.lang.String str2 = fVar.f9335b;
                    if (str2 != null) {
                        g4Var.put(str2, fVar);
                    }
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16057a(android.content.res.Resources resources, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser) {
            android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6868b);
            mo16058a(a, xmlPullParser);
            a.recycle();
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo16059b() {
            this.f9330j.reset();
            this.f9330j.postTranslate(-this.f9324d, -this.f9325e);
            this.f9330j.postScale(this.f9326f, this.f9327g);
            this.f9330j.postRotate(this.f9323c, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.f9330j.postTranslate(this.f9328h + this.f9324d, this.f9329i + this.f9325e);
        }

        @DexIgnore
        public java.lang.String getGroupName() {
            return this.f9333m;
        }

        @DexIgnore
        public android.graphics.Matrix getLocalMatrix() {
            return this.f9330j;
        }

        @DexIgnore
        public float getPivotX() {
            return this.f9324d;
        }

        @DexIgnore
        public float getPivotY() {
            return this.f9325e;
        }

        @DexIgnore
        public float getRotation() {
            return this.f9323c;
        }

        @DexIgnore
        public float getScaleX() {
            return this.f9326f;
        }

        @DexIgnore
        public float getScaleY() {
            return this.f9327g;
        }

        @DexIgnore
        public float getTranslateX() {
            return this.f9328h;
        }

        @DexIgnore
        public float getTranslateY() {
            return this.f9329i;
        }

        @DexIgnore
        public void setPivotX(float f) {
            if (f != this.f9324d) {
                this.f9324d = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setPivotY(float f) {
            if (f != this.f9325e) {
                this.f9325e = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setRotation(float f) {
            if (f != this.f9323c) {
                this.f9323c = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setScaleX(float f) {
            if (f != this.f9326f) {
                this.f9326f = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setScaleY(float f) {
            if (f != this.f9327g) {
                this.f9327g = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setTranslateX(float f) {
            if (f != this.f9328h) {
                this.f9328h = f;
                mo16059b();
            }
        }

        @DexIgnore
        public void setTranslateY(float f) {
            if (f != this.f9329i) {
                this.f9329i = f;
                mo16059b();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo16058a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser) {
            this.f9332l = null;
            this.f9323c = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "rotation", 5, this.f9323c);
            this.f9324d = typedArray.getFloat(1, this.f9324d);
            this.f9325e = typedArray.getFloat(2, this.f9325e);
            this.f9326f = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "scaleX", 3, this.f9326f);
            this.f9327g = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "scaleY", 4, this.f9327g);
            this.f9328h = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "translateX", 6, this.f9328h);
            this.f9329i = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "translateY", 7, this.f9329i);
            java.lang.String string = typedArray.getString(0);
            if (string != null) {
                this.f9333m = string;
            }
            mo16059b();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16039a() {
            for (int i = 0; i < this.f9322b.size(); i++) {
                if (this.f9322b.get(i).mo16039a()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16040a(int[] iArr) {
            boolean z = false;
            for (int i = 0; i < this.f9322b.size(); i++) {
                z |= this.f9322b.get(i).mo16040a(iArr);
            }
            return z;
        }

        @DexIgnore
        public C2883d() {
            super();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$e")
    /* renamed from: com.fossil.blesdk.obfuscated.si$e */
    public static abstract class C2884e {
        @DexIgnore
        public C2884e() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16039a() {
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16040a(int[] iArr) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$f")
    /* renamed from: com.fossil.blesdk.obfuscated.si$f */
    public static abstract class C2885f extends com.fossil.blesdk.obfuscated.C2879si.C2884e {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3009u6.C3011b[] f9334a; // = null;

        @DexIgnore
        /* renamed from: b */
        public java.lang.String f9335b;

        @DexIgnore
        /* renamed from: c */
        public int f9336c;

        @DexIgnore
        public C2885f() {
            super();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16076a(android.graphics.Path path) {
            path.reset();
            com.fossil.blesdk.obfuscated.C3009u6.C3011b[] bVarArr = this.f9334a;
            if (bVarArr != null) {
                com.fossil.blesdk.obfuscated.C3009u6.C3011b.m14590a(bVarArr, path);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo16034b() {
            return false;
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3009u6.C3011b[] getPathData() {
            return this.f9334a;
        }

        @DexIgnore
        public java.lang.String getPathName() {
            return this.f9335b;
        }

        @DexIgnore
        public void setPathData(com.fossil.blesdk.obfuscated.C3009u6.C3011b[] bVarArr) {
            if (!com.fossil.blesdk.obfuscated.C3009u6.m14580a(this.f9334a, bVarArr)) {
                this.f9334a = com.fossil.blesdk.obfuscated.C3009u6.m14583a(bVarArr);
            } else {
                com.fossil.blesdk.obfuscated.C3009u6.m14585b(this.f9334a, bVarArr);
            }
        }

        @DexIgnore
        public C2885f(com.fossil.blesdk.obfuscated.C2879si.C2885f fVar) {
            super();
            this.f9335b = fVar.f9335b;
            this.f9336c = fVar.f9336c;
            this.f9334a = com.fossil.blesdk.obfuscated.C3009u6.m14583a(fVar.f9334a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$g")
    /* renamed from: com.fossil.blesdk.obfuscated.si$g */
    public static class C2886g {

        @DexIgnore
        /* renamed from: q */
        public static /* final */ android.graphics.Matrix f9337q; // = new android.graphics.Matrix();

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.Path f9338a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.graphics.Path f9339b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ android.graphics.Matrix f9340c;

        @DexIgnore
        /* renamed from: d */
        public android.graphics.Paint f9341d;

        @DexIgnore
        /* renamed from: e */
        public android.graphics.Paint f9342e;

        @DexIgnore
        /* renamed from: f */
        public android.graphics.PathMeasure f9343f;

        @DexIgnore
        /* renamed from: g */
        public int f9344g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ com.fossil.blesdk.obfuscated.C2879si.C2883d f9345h;

        @DexIgnore
        /* renamed from: i */
        public float f9346i;

        @DexIgnore
        /* renamed from: j */
        public float f9347j;

        @DexIgnore
        /* renamed from: k */
        public float f9348k;

        @DexIgnore
        /* renamed from: l */
        public float f9349l;

        @DexIgnore
        /* renamed from: m */
        public int f9350m;

        @DexIgnore
        /* renamed from: n */
        public java.lang.String f9351n;

        @DexIgnore
        /* renamed from: o */
        public java.lang.Boolean f9352o;

        @DexIgnore
        /* renamed from: p */
        public /* final */ com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, java.lang.Object> f9353p;

        @DexIgnore
        public C2886g() {
            this.f9340c = new android.graphics.Matrix();
            this.f9346i = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9347j = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9348k = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9349l = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9350m = 255;
            this.f9351n = null;
            this.f9352o = null;
            this.f9353p = new com.fossil.blesdk.obfuscated.C1855g4<>();
            this.f9345h = new com.fossil.blesdk.obfuscated.C2879si.C2883d();
            this.f9338a = new android.graphics.Path();
            this.f9339b = new android.graphics.Path();
        }

        @DexIgnore
        /* renamed from: a */
        public static float m13762a(float f, float f2, float f3, float f4) {
            return (f * f4) - (f2 * f3);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo16082a(com.fossil.blesdk.obfuscated.C2879si.C2883d dVar, android.graphics.Matrix matrix, android.graphics.Canvas canvas, int i, int i2, android.graphics.ColorFilter colorFilter) {
            dVar.f9321a.set(matrix);
            dVar.f9321a.preConcat(dVar.f9330j);
            canvas.save();
            for (int i3 = 0; i3 < dVar.f9322b.size(); i3++) {
                com.fossil.blesdk.obfuscated.C2879si.C2884e eVar = dVar.f9322b.get(i3);
                if (eVar instanceof com.fossil.blesdk.obfuscated.C2879si.C2883d) {
                    mo16082a((com.fossil.blesdk.obfuscated.C2879si.C2883d) eVar, dVar.f9321a, canvas, i, i2, colorFilter);
                } else if (eVar instanceof com.fossil.blesdk.obfuscated.C2879si.C2885f) {
                    mo16083a(dVar, (com.fossil.blesdk.obfuscated.C2879si.C2885f) eVar, canvas, i, i2, colorFilter);
                }
            }
            canvas.restore();
        }

        @DexIgnore
        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        @DexIgnore
        public int getRootAlpha() {
            return this.f9350m;
        }

        @DexIgnore
        public void setAlpha(float f) {
            setRootAlpha((int) (f * 255.0f));
        }

        @DexIgnore
        public void setRootAlpha(int i) {
            this.f9350m = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16081a(android.graphics.Canvas canvas, int i, int i2, android.graphics.ColorFilter colorFilter) {
            mo16082a(this.f9345h, f9337q, canvas, i, i2, colorFilter);
        }

        @DexIgnore
        public C2886g(com.fossil.blesdk.obfuscated.C2879si.C2886g gVar) {
            this.f9340c = new android.graphics.Matrix();
            this.f9346i = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9347j = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9348k = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9349l = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f9350m = 255;
            this.f9351n = null;
            this.f9352o = null;
            this.f9353p = new com.fossil.blesdk.obfuscated.C1855g4<>();
            this.f9345h = new com.fossil.blesdk.obfuscated.C2879si.C2883d(gVar.f9345h, this.f9353p);
            this.f9338a = new android.graphics.Path(gVar.f9338a);
            this.f9339b = new android.graphics.Path(gVar.f9339b);
            this.f9346i = gVar.f9346i;
            this.f9347j = gVar.f9347j;
            this.f9348k = gVar.f9348k;
            this.f9349l = gVar.f9349l;
            this.f9344g = gVar.f9344g;
            this.f9350m = gVar.f9350m;
            this.f9351n = gVar.f9351n;
            java.lang.String str = gVar.f9351n;
            if (str != null) {
                this.f9353p.put(str, this);
            }
            this.f9352o = gVar.f9352o;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo16083a(com.fossil.blesdk.obfuscated.C2879si.C2883d dVar, com.fossil.blesdk.obfuscated.C2879si.C2885f fVar, android.graphics.Canvas canvas, int i, int i2, android.graphics.ColorFilter colorFilter) {
            float f = ((float) i) / this.f9348k;
            float f2 = ((float) i2) / this.f9349l;
            float min = java.lang.Math.min(f, f2);
            android.graphics.Matrix matrix = dVar.f9321a;
            this.f9340c.set(matrix);
            this.f9340c.postScale(f, f2);
            float a = mo16080a(matrix);
            if (a != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                fVar.mo16076a(this.f9338a);
                android.graphics.Path path = this.f9338a;
                this.f9339b.reset();
                if (fVar.mo16034b()) {
                    this.f9339b.addPath(path, this.f9340c);
                    canvas.clipPath(this.f9339b);
                    return;
                }
                com.fossil.blesdk.obfuscated.C2879si.C2882c cVar = (com.fossil.blesdk.obfuscated.C2879si.C2882c) fVar;
                if (!(cVar.f9315k == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && cVar.f9316l == 1.0f)) {
                    float f3 = cVar.f9315k;
                    float f4 = cVar.f9317m;
                    float f5 = (f3 + f4) % 1.0f;
                    float f6 = (cVar.f9316l + f4) % 1.0f;
                    if (this.f9343f == null) {
                        this.f9343f = new android.graphics.PathMeasure();
                    }
                    this.f9343f.setPath(this.f9338a, false);
                    float length = this.f9343f.getLength();
                    float f7 = f5 * length;
                    float f8 = f6 * length;
                    path.reset();
                    if (f7 > f8) {
                        this.f9343f.getSegment(f7, length, path, true);
                        this.f9343f.getSegment(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f8, path, true);
                    } else {
                        this.f9343f.getSegment(f7, f8, path, true);
                    }
                    path.rLineTo(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                this.f9339b.addPath(path, this.f9340c);
                if (cVar.f9311g.mo13865e()) {
                    com.fossil.blesdk.obfuscated.C2462n6 n6Var = cVar.f9311g;
                    if (this.f9342e == null) {
                        this.f9342e = new android.graphics.Paint(1);
                        this.f9342e.setStyle(android.graphics.Paint.Style.FILL);
                    }
                    android.graphics.Paint paint = this.f9342e;
                    if (n6Var.mo13863c()) {
                        android.graphics.Shader b = n6Var.mo13862b();
                        b.setLocalMatrix(this.f9340c);
                        paint.setShader(b);
                        paint.setAlpha(java.lang.Math.round(cVar.f9314j * 255.0f));
                    } else {
                        paint.setShader((android.graphics.Shader) null);
                        paint.setAlpha(255);
                        paint.setColor(com.fossil.blesdk.obfuscated.C2879si.m13735a(n6Var.mo13859a(), cVar.f9314j));
                    }
                    paint.setColorFilter(colorFilter);
                    this.f9339b.setFillType(cVar.f9313i == 0 ? android.graphics.Path.FillType.WINDING : android.graphics.Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.f9339b, paint);
                }
                if (cVar.f9309e.mo13865e()) {
                    com.fossil.blesdk.obfuscated.C2462n6 n6Var2 = cVar.f9309e;
                    if (this.f9341d == null) {
                        this.f9341d = new android.graphics.Paint(1);
                        this.f9341d.setStyle(android.graphics.Paint.Style.STROKE);
                    }
                    android.graphics.Paint paint2 = this.f9341d;
                    android.graphics.Paint.Join join = cVar.f9319o;
                    if (join != null) {
                        paint2.setStrokeJoin(join);
                    }
                    android.graphics.Paint.Cap cap = cVar.f9318n;
                    if (cap != null) {
                        paint2.setStrokeCap(cap);
                    }
                    paint2.setStrokeMiter(cVar.f9320p);
                    if (n6Var2.mo13863c()) {
                        android.graphics.Shader b2 = n6Var2.mo13862b();
                        b2.setLocalMatrix(this.f9340c);
                        paint2.setShader(b2);
                        paint2.setAlpha(java.lang.Math.round(cVar.f9312h * 255.0f));
                    } else {
                        paint2.setShader((android.graphics.Shader) null);
                        paint2.setAlpha(255);
                        paint2.setColor(com.fossil.blesdk.obfuscated.C2879si.m13735a(n6Var2.mo13859a(), cVar.f9312h));
                    }
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(cVar.f9310f * min * a);
                    canvas.drawPath(this.f9339b, paint2);
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final float mo16080a(android.graphics.Matrix matrix) {
            float[] fArr = {com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1.0f, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
            matrix.mapVectors(fArr);
            float a = m13762a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = java.lang.Math.max((float) java.lang.Math.hypot((double) fArr[0], (double) fArr[1]), (float) java.lang.Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return java.lang.Math.abs(a) / max;
            }
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16084a() {
            if (this.f9352o == null) {
                this.f9352o = java.lang.Boolean.valueOf(this.f9345h.mo16039a());
            }
            return this.f9352o.booleanValue();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16085a(int[] iArr) {
            return this.f9345h.mo16040a(iArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$h")
    /* renamed from: com.fossil.blesdk.obfuscated.si$h */
    public static class C2887h extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public int f9354a;

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2879si.C2886g f9355b;

        @DexIgnore
        /* renamed from: c */
        public android.content.res.ColorStateList f9356c;

        @DexIgnore
        /* renamed from: d */
        public android.graphics.PorterDuff.Mode f9357d;

        @DexIgnore
        /* renamed from: e */
        public boolean f9358e;

        @DexIgnore
        /* renamed from: f */
        public android.graphics.Bitmap f9359f;

        @DexIgnore
        /* renamed from: g */
        public android.content.res.ColorStateList f9360g;

        @DexIgnore
        /* renamed from: h */
        public android.graphics.PorterDuff.Mode f9361h;

        @DexIgnore
        /* renamed from: i */
        public int f9362i;

        @DexIgnore
        /* renamed from: j */
        public boolean f9363j;

        @DexIgnore
        /* renamed from: k */
        public boolean f9364k;

        @DexIgnore
        /* renamed from: l */
        public android.graphics.Paint f9365l;

        @DexIgnore
        public C2887h(com.fossil.blesdk.obfuscated.C2879si.C2887h hVar) {
            this.f9356c = null;
            this.f9357d = com.fossil.blesdk.obfuscated.C2879si.f9299n;
            if (hVar != null) {
                this.f9354a = hVar.f9354a;
                this.f9355b = new com.fossil.blesdk.obfuscated.C2879si.C2886g(hVar.f9355b);
                android.graphics.Paint paint = hVar.f9355b.f9342e;
                if (paint != null) {
                    this.f9355b.f9342e = new android.graphics.Paint(paint);
                }
                android.graphics.Paint paint2 = hVar.f9355b.f9341d;
                if (paint2 != null) {
                    this.f9355b.f9341d = new android.graphics.Paint(paint2);
                }
                this.f9356c = hVar.f9356c;
                this.f9357d = hVar.f9357d;
                this.f9358e = hVar.f9358e;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16091a(android.graphics.Canvas canvas, android.graphics.ColorFilter colorFilter, android.graphics.Rect rect) {
            canvas.drawBitmap(this.f9359f, (android.graphics.Rect) null, rect, mo16090a(colorFilter));
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo16096b() {
            return this.f9355b.getRootAlpha() < 255;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo16097c(int i, int i2) {
            this.f9359f.eraseColor(0);
            this.f9355b.mo16081a(new android.graphics.Canvas(this.f9359f), i, i2, (android.graphics.ColorFilter) null);
        }

        @DexIgnore
        /* renamed from: d */
        public void mo16099d() {
            this.f9360g = this.f9356c;
            this.f9361h = this.f9357d;
            this.f9362i = this.f9355b.getRootAlpha();
            this.f9363j = this.f9358e;
            this.f9364k = false;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f9354a;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            return new com.fossil.blesdk.obfuscated.C2879si(this);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo16095b(int i, int i2) {
            if (this.f9359f == null || !mo16093a(i, i2)) {
                this.f9359f = android.graphics.Bitmap.createBitmap(i, i2, android.graphics.Bitmap.Config.ARGB_8888);
                this.f9364k = true;
            }
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return new com.fossil.blesdk.obfuscated.C2879si(this);
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.Paint mo16090a(android.graphics.ColorFilter colorFilter) {
            if (!mo16096b() && colorFilter == null) {
                return null;
            }
            if (this.f9365l == null) {
                this.f9365l = new android.graphics.Paint();
                this.f9365l.setFilterBitmap(true);
            }
            this.f9365l.setAlpha(this.f9355b.getRootAlpha());
            this.f9365l.setColorFilter(colorFilter);
            return this.f9365l;
        }

        @DexIgnore
        /* renamed from: c */
        public boolean mo16098c() {
            return this.f9355b.mo16084a();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16093a(int i, int i2) {
            return i == this.f9359f.getWidth() && i2 == this.f9359f.getHeight();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16092a() {
            return !this.f9364k && this.f9360g == this.f9356c && this.f9361h == this.f9357d && this.f9363j == this.f9358e && this.f9362i == this.f9355b.getRootAlpha();
        }

        @DexIgnore
        public C2887h() {
            this.f9356c = null;
            this.f9357d = com.fossil.blesdk.obfuscated.C2879si.f9299n;
            this.f9355b = new com.fossil.blesdk.obfuscated.C2879si.C2886g();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo16094a(int[] iArr) {
            boolean a = this.f9355b.mo16085a(iArr);
            this.f9364k |= a;
            return a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.si$i")
    /* renamed from: com.fossil.blesdk.obfuscated.si$i */
    public static class C2888i extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.drawable.Drawable.ConstantState f9366a;

        @DexIgnore
        public C2888i(android.graphics.drawable.Drawable.ConstantState constantState) {
            this.f9366a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.f9366a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.f9366a.getChangingConfigurations();
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            com.fossil.blesdk.obfuscated.C2879si siVar = new com.fossil.blesdk.obfuscated.C2879si();
            siVar.f8987e = (android.graphics.drawable.VectorDrawable) this.f9366a.newDrawable();
            return siVar;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            com.fossil.blesdk.obfuscated.C2879si siVar = new com.fossil.blesdk.obfuscated.C2879si();
            siVar.f8987e = (android.graphics.drawable.VectorDrawable) this.f9366a.newDrawable(resources);
            return siVar;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources, android.content.res.Resources.Theme theme) {
            com.fossil.blesdk.obfuscated.C2879si siVar = new com.fossil.blesdk.obfuscated.C2879si();
            siVar.f8987e = (android.graphics.drawable.VectorDrawable) this.f9366a.newDrawable(resources, theme);
            return siVar;
        }
    }

    @DexIgnore
    public C2879si() {
        this.f9304j = true;
        this.f9305k = new float[9];
        this.f9306l = new android.graphics.Matrix();
        this.f9307m = new android.graphics.Rect();
        this.f9300f = new com.fossil.blesdk.obfuscated.C2879si.C2887h();
    }

    @DexIgnore
    public static com.fossil.blesdk.obfuscated.C2879si createFromXmlInner(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        com.fossil.blesdk.obfuscated.C2879si siVar = new com.fossil.blesdk.obfuscated.C2879si();
        siVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return siVar;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo16005a(java.lang.String str) {
        return this.f9300f.f9355b.f9353p.get(str);
    }

    @DexIgnore
    public boolean canApplyTheme() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable == null) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1538c7.m5304a(drawable);
        return false;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.f9307m);
        if (this.f9307m.width() > 0 && this.f9307m.height() > 0) {
            android.graphics.ColorFilter colorFilter = this.f9302h;
            if (colorFilter == null) {
                colorFilter = this.f9301g;
            }
            canvas.getMatrix(this.f9306l);
            this.f9306l.getValues(this.f9305k);
            float abs = java.lang.Math.abs(this.f9305k[0]);
            float abs2 = java.lang.Math.abs(this.f9305k[4]);
            float abs3 = java.lang.Math.abs(this.f9305k[1]);
            float abs4 = java.lang.Math.abs(this.f9305k[3]);
            if (!(abs3 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && abs4 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = java.lang.Math.min(2048, (int) (((float) this.f9307m.width()) * abs));
            int min2 = java.lang.Math.min(2048, (int) (((float) this.f9307m.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                android.graphics.Rect rect = this.f9307m;
                canvas.translate((float) rect.left, (float) rect.top);
                if (mo16009a()) {
                    canvas.translate((float) this.f9307m.width(), com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.f9307m.offsetTo(0, 0);
                this.f9300f.mo16095b(min, min2);
                if (!this.f9304j) {
                    this.f9300f.mo16097c(min, min2);
                } else if (!this.f9300f.mo16092a()) {
                    this.f9300f.mo16097c(min, min2);
                    this.f9300f.mo16099d();
                }
                this.f9300f.mo16091a(canvas, colorFilter, this.f9307m);
                canvas.restoreToCount(save);
            }
        }
    }

    @DexIgnore
    public int getAlpha() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5308c(drawable);
        }
        return this.f9300f.f9355b.getRootAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f9300f.getChangingConfigurations();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable.ConstantState getConstantState() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null && android.os.Build.VERSION.SDK_INT >= 24) {
            return new com.fossil.blesdk.obfuscated.C2879si.C2888i(drawable.getConstantState());
        }
        this.f9300f.f9354a = getChangingConfigurations();
        return this.f9300f;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.f9300f.f9355b.f9347j;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.f9300f.f9355b.f9346i;
    }

    @DexIgnore
    public int getOpacity() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    @DexIgnore
    public void inflate(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, (android.content.res.Resources.Theme) null);
        }
    }

    @DexIgnore
    public void invalidateSelf() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5311f(drawable);
        }
        return this.f9300f.f9358e;
    }

    @DexIgnore
    public boolean isStateful() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.isStateful();
        }
        if (!super.isStateful()) {
            com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
            if (hVar != null) {
                if (!hVar.mo16098c()) {
                    android.content.res.ColorStateList colorStateList = this.f9300f.f9356c;
                    if (colorStateList == null || !colorStateList.isStateful()) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.f9303i && super.mutate() == this) {
            this.f9300f = new com.fossil.blesdk.obfuscated.C2879si.C2887h(this.f9300f);
            this.f9303i = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        android.content.res.ColorStateList colorStateList = hVar.f9356c;
        if (colorStateList != null) {
            android.graphics.PorterDuff.Mode mode = hVar.f9357d;
            if (mode != null) {
                this.f9301g = mo16004a(this.f9301g, colorStateList, mode);
                invalidateSelf();
                z = true;
            }
        }
        if (!hVar.mo16098c() || !hVar.mo16094a(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void scheduleSelf(java.lang.Runnable runnable, long j) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j);
        } else {
            super.scheduleSelf(runnable, j);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setAlpha(i);
        } else if (this.f9300f.f9355b.getRootAlpha() != i) {
            this.f9300f.f9355b.setRootAlpha(i);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5303a(drawable, z);
        } else {
            this.f9300f.f9358e = z;
        }
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.f9302h = colorFilter;
        invalidateSelf();
    }

    @DexIgnore
    public void setTint(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5307b(drawable, i);
        } else {
            setTintList(android.content.res.ColorStateList.valueOf(i));
        }
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5299a(drawable, colorStateList);
            return;
        }
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        if (hVar.f9356c != colorStateList) {
            hVar.f9356c = colorStateList;
            this.f9301g = mo16004a(this.f9301g, colorStateList, hVar.f9357d);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5302a(drawable, mode);
            return;
        }
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        if (hVar.f9357d != mode) {
            hVar.f9357d = mode;
            this.f9301g = mo16004a(this.f9301g, hVar.f9356c, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleSelf(java.lang.Runnable runnable) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.PorterDuffColorFilter mo16004a(android.graphics.PorterDuffColorFilter porterDuffColorFilter, android.content.res.ColorStateList colorStateList, android.graphics.PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new android.graphics.PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2879si m13737a(android.content.res.Resources resources, int i, android.content.res.Resources.Theme theme) {
        int next;
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            com.fossil.blesdk.obfuscated.C2879si siVar = new com.fossil.blesdk.obfuscated.C2879si();
            siVar.f8987e = com.fossil.blesdk.obfuscated.C2785r6.m13075a(resources, i, theme);
            new com.fossil.blesdk.obfuscated.C2879si.C2888i(siVar.f8987e.getConstantState());
            return siVar;
        }
        try {
            android.content.res.XmlResourceParser xml = resources.getXml(i);
            android.util.AttributeSet asAttributeSet = android.util.Xml.asAttributeSet(xml);
            while (true) {
                next = xml.next();
                if (next == 2 || next == 1) {
                    if (next != 2) {
                        return createFromXmlInner(resources, xml, asAttributeSet, theme);
                    }
                    throw new org.xmlpull.v1.XmlPullParserException("No start tag found");
                }
            }
            if (next != 2) {
            }
        } catch (org.xmlpull.v1.XmlPullParserException e) {
            android.util.Log.e("VectorDrawableCompat", "parser error", e);
            return null;
        } catch (java.io.IOException e2) {
            android.util.Log.e("VectorDrawableCompat", "parser error", e2);
            return null;
        }
    }

    @DexIgnore
    public void inflate(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5301a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        hVar.f9355b = new com.fossil.blesdk.obfuscated.C2879si.C2886g();
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6867a);
        mo16007a(a, xmlPullParser);
        a.recycle();
        hVar.f9354a = getChangingConfigurations();
        hVar.f9364k = true;
        mo16006a(resources, xmlPullParser, attributeSet, theme);
        this.f9301g = mo16004a(this.f9301g, hVar.f9356c, hVar.f9357d);
    }

    @DexIgnore
    public C2879si(com.fossil.blesdk.obfuscated.C2879si.C2887h hVar) {
        this.f9304j = true;
        this.f9305k = new float[9];
        this.f9306l = new android.graphics.Matrix();
        this.f9307m = new android.graphics.Rect();
        this.f9300f = hVar;
        this.f9301g = mo16004a(this.f9301g, hVar.f9356c, hVar.f9357d);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m13735a(int i, float f) {
        return (i & 16777215) | (((int) (((float) android.graphics.Color.alpha(i)) * f)) << 24);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.PorterDuff.Mode m13736a(int i, android.graphics.PorterDuff.Mode mode) {
        if (i == 3) {
            return android.graphics.PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return android.graphics.PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return android.graphics.PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return android.graphics.PorterDuff.Mode.MULTIPLY;
            case 15:
                return android.graphics.PorterDuff.Mode.SCREEN;
            case 16:
                return android.graphics.PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16007a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser) throws org.xmlpull.v1.XmlPullParserException {
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        com.fossil.blesdk.obfuscated.C2879si.C2886g gVar = hVar.f9355b;
        hVar.f9357d = m13736a(com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "tintMode", 6, -1), android.graphics.PorterDuff.Mode.SRC_IN);
        android.content.res.ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            hVar.f9356c = colorStateList;
        }
        hVar.f9358e = com.fossil.blesdk.obfuscated.C2863s6.m13544a(typedArray, xmlPullParser, "autoMirrored", 5, hVar.f9358e);
        gVar.f9348k = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "viewportWidth", 7, gVar.f9348k);
        gVar.f9349l = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "viewportHeight", 8, gVar.f9349l);
        if (gVar.f9348k <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new org.xmlpull.v1.XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (gVar.f9349l > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            gVar.f9346i = typedArray.getDimension(3, gVar.f9346i);
            gVar.f9347j = typedArray.getDimension(2, gVar.f9347j);
            if (gVar.f9346i <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new org.xmlpull.v1.XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (gVar.f9347j > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                gVar.setAlpha(com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "alpha", 4, gVar.getAlpha()));
                java.lang.String string = typedArray.getString(0);
                if (string != null) {
                    gVar.f9351n = string;
                    gVar.f9353p.put(string, gVar);
                }
            } else {
                throw new org.xmlpull.v1.XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new org.xmlpull.v1.XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16006a(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        com.fossil.blesdk.obfuscated.C2879si.C2887h hVar = this.f9300f;
        com.fossil.blesdk.obfuscated.C2879si.C2886g gVar = hVar.f9355b;
        java.util.ArrayDeque arrayDeque = new java.util.ArrayDeque();
        arrayDeque.push(gVar.f9345h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                java.lang.String name = xmlPullParser.getName();
                com.fossil.blesdk.obfuscated.C2879si.C2883d dVar = (com.fossil.blesdk.obfuscated.C2879si.C2883d) arrayDeque.peek();
                if ("path".equals(name)) {
                    com.fossil.blesdk.obfuscated.C2879si.C2882c cVar = new com.fossil.blesdk.obfuscated.C2879si.C2882c();
                    cVar.mo16037a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f9322b.add(cVar);
                    if (cVar.getPathName() != null) {
                        gVar.f9353p.put(cVar.getPathName(), cVar);
                    }
                    z = false;
                    hVar.f9354a = cVar.f9336c | hVar.f9354a;
                } else if ("clip-path".equals(name)) {
                    com.fossil.blesdk.obfuscated.C2879si.C2881b bVar = new com.fossil.blesdk.obfuscated.C2879si.C2881b();
                    bVar.mo16032a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f9322b.add(bVar);
                    if (bVar.getPathName() != null) {
                        gVar.f9353p.put(bVar.getPathName(), bVar);
                    }
                    hVar.f9354a = bVar.f9336c | hVar.f9354a;
                } else if ("group".equals(name)) {
                    com.fossil.blesdk.obfuscated.C2879si.C2883d dVar2 = new com.fossil.blesdk.obfuscated.C2879si.C2883d();
                    dVar2.mo16057a(resources, attributeSet, theme, xmlPullParser);
                    dVar.f9322b.add(dVar2);
                    arrayDeque.push(dVar2);
                    if (dVar2.getGroupName() != null) {
                        gVar.f9353p.put(dVar2.getGroupName(), dVar2);
                    }
                    hVar.f9354a = dVar2.f9331k | hVar.f9354a;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                arrayDeque.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            throw new org.xmlpull.v1.XmlPullParserException("no path defined");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16008a(boolean z) {
        this.f9304j = z;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo16009a() {
        if (android.os.Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || com.fossil.blesdk.obfuscated.C1538c7.m5310e(this) != 1) {
            return false;
        }
        return true;
    }
}
