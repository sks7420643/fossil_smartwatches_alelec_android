package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class di extends ci {
    @DexIgnore
    public static Method f;
    @DexIgnore
    public static boolean g;
    @DexIgnore
    public static Method h;
    @DexIgnore
    public static boolean i;
    @DexIgnore
    public static Method j;
    @DexIgnore
    public static boolean k;

    @DexIgnore
    public void a(View view, Matrix matrix) {
        c();
        Method method = j;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{matrix});
            } catch (InvocationTargetException unused) {
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    public void b(View view, Matrix matrix) {
        d();
        Method method = f;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    public void c(View view, Matrix matrix) {
        e();
        Method method = h;
        if (method != null) {
            try {
                method.invoke(view, new Object[]{matrix});
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    public final void d() {
        if (!g) {
            try {
                f = View.class.getDeclaredMethod("transformMatrixToGlobal", new Class[]{Matrix.class});
                f.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e);
            }
            g = true;
        }
    }

    @DexIgnore
    public final void e() {
        if (!i) {
            try {
                h = View.class.getDeclaredMethod("transformMatrixToLocal", new Class[]{Matrix.class});
                h.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e);
            }
            i = true;
        }
    }

    @DexIgnore
    public final void c() {
        if (!k) {
            try {
                j = View.class.getDeclaredMethod("setAnimationMatrix", new Class[]{Matrix.class});
                j.setAccessible(true);
            } catch (NoSuchMethodException e) {
                Log.i("ViewUtilsApi21", "Failed to retrieve setAnimationMatrix method", e);
            }
            k = true;
        }
    }
}
