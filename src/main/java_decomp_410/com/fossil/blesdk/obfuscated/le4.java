package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface le4<R> extends ge4<R> {

    @DexIgnore
    public interface a<R> {
    }

    @DexIgnore
    public interface b<R> extends a<R>, ke4<R> {
    }

    @DexIgnore
    boolean isConst();

    @DexIgnore
    boolean isLateinit();
}
