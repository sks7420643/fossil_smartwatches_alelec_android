package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b2 */
public class C1459b2 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.widget.CompoundButton f3646a;

    @DexIgnore
    /* renamed from: b */
    public android.content.res.ColorStateList f3647b; // = null;

    @DexIgnore
    /* renamed from: c */
    public android.graphics.PorterDuff.Mode f3648c; // = null;

    @DexIgnore
    /* renamed from: d */
    public boolean f3649d; // = false;

    @DexIgnore
    /* renamed from: e */
    public boolean f3650e; // = false;

    @DexIgnore
    /* renamed from: f */
    public boolean f3651f;

    @DexIgnore
    public C1459b2(android.widget.CompoundButton compoundButton) {
        this.f3646a = compoundButton;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9022a(android.util.AttributeSet attributeSet, int i) {
        android.content.res.TypedArray obtainStyledAttributes = this.f3646a.getContext().obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C1368a0.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_android_button)) {
                int resourceId = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_android_button, 0);
                if (resourceId != 0) {
                    this.f3646a.setButtonDrawable(com.fossil.blesdk.obfuscated.C2364m0.m10497c(this.f3646a.getContext(), resourceId));
                }
            }
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_buttonTint)) {
                com.fossil.blesdk.obfuscated.C3100v9.m15115a(this.f3646a, obtainStyledAttributes.getColorStateList(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_buttonTintMode)) {
                com.fossil.blesdk.obfuscated.C3100v9.m15116a(this.f3646a, com.fossil.blesdk.obfuscated.C2181k2.m9311a(obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.CompoundButton_buttonTintMode, -1), (android.graphics.PorterDuff.Mode) null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.res.ColorStateList mo9023b() {
        return this.f3647b;
    }

    @DexIgnore
    /* renamed from: c */
    public android.graphics.PorterDuff.Mode mo9024c() {
        return this.f3648c;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo9025d() {
        if (this.f3651f) {
            this.f3651f = false;
            return;
        }
        this.f3651f = true;
        mo9019a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9020a(android.content.res.ColorStateList colorStateList) {
        this.f3647b = colorStateList;
        this.f3649d = true;
        mo9019a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9021a(android.graphics.PorterDuff.Mode mode) {
        this.f3648c = mode;
        this.f3650e = true;
        mo9019a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9019a() {
        android.graphics.drawable.Drawable a = com.fossil.blesdk.obfuscated.C3100v9.m15114a(this.f3646a);
        if (a == null) {
            return;
        }
        if (this.f3649d || this.f3650e) {
            android.graphics.drawable.Drawable mutate = com.fossil.blesdk.obfuscated.C1538c7.m5314i(a).mutate();
            if (this.f3649d) {
                com.fossil.blesdk.obfuscated.C1538c7.m5299a(mutate, this.f3647b);
            }
            if (this.f3650e) {
                com.fossil.blesdk.obfuscated.C1538c7.m5302a(mutate, this.f3648c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f3646a.getDrawableState());
            }
            this.f3646a.setButtonDrawable(mutate);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo9018a(int i) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return i;
        }
        android.graphics.drawable.Drawable a = com.fossil.blesdk.obfuscated.C3100v9.m15114a(this.f3646a);
        return a != null ? i + a.getIntrinsicWidth() : i;
    }
}
