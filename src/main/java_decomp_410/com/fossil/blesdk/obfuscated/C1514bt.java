package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bt */
public final class C1514bt {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ java.io.File f3910d; // = new java.io.File("/proc/self/fd");

    @DexIgnore
    /* renamed from: e */
    public static volatile int f3911e; // = 700;

    @DexIgnore
    /* renamed from: f */
    public static volatile int f3912f; // = 128;

    @DexIgnore
    /* renamed from: g */
    public static volatile com.fossil.blesdk.obfuscated.C1514bt f3913g;

    @DexIgnore
    /* renamed from: a */
    public /* final */ boolean f3914a; // = m5111c();

    @DexIgnore
    /* renamed from: b */
    public int f3915b;

    @DexIgnore
    /* renamed from: c */
    public boolean f3916c; // = true;

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1514bt m5110b() {
        if (f3913g == null) {
            synchronized (com.fossil.blesdk.obfuscated.C1514bt.class) {
                if (f3913g == null) {
                    f3913g = new com.fossil.blesdk.obfuscated.C1514bt();
                }
            }
        }
        return f3913g;
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: c */
    public static boolean m5111c() {
        char c;
        java.lang.String str = android.os.Build.MODEL;
        if (str == null || str.length() < 7) {
            return true;
        }
        java.lang.String substring = android.os.Build.MODEL.substring(0, 7);
        switch (substring.hashCode()) {
            case -1398613787:
                if (substring.equals("SM-A520")) {
                    c = 6;
                    break;
                }
            case -1398431166:
                if (substring.equals("SM-G930")) {
                    c = 5;
                    break;
                }
            case -1398431161:
                if (substring.equals("SM-G935")) {
                    c = 4;
                    break;
                }
            case -1398431073:
                if (substring.equals("SM-G960")) {
                    c = 2;
                    break;
                }
            case -1398431068:
                if (substring.equals("SM-G965")) {
                    c = 3;
                    break;
                }
            case -1398343746:
                if (substring.equals("SM-J720")) {
                    c = 1;
                    break;
                }
            case -1398222624:
                if (substring.equals("SM-N935")) {
                    c = 0;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                if (android.os.Build.VERSION.SDK_INT != 26) {
                    return true;
                }
                return false;
            default:
                return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9293a(int i, int i2, boolean z, boolean z2) {
        if (!z || !this.f3914a || android.os.Build.VERSION.SDK_INT < 26 || z2 || i < f3912f || i2 < f3912f || !mo9291a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @android.annotation.TargetApi(26)
    /* renamed from: a */
    public boolean mo9292a(int i, int i2, android.graphics.BitmapFactory.Options options, boolean z, boolean z2) {
        boolean a = mo9293a(i, i2, z, z2);
        if (a) {
            options.inPreferredConfig = android.graphics.Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized boolean mo9291a() {
        int i = this.f3915b + 1;
        this.f3915b = i;
        if (i >= 50) {
            boolean z = false;
            this.f3915b = 0;
            int length = f3910d.list().length;
            if (length < f3911e) {
                z = true;
            }
            this.f3916c = z;
            if (!this.f3916c && android.util.Log.isLoggable("Downsampler", 5)) {
                android.util.Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + f3911e);
            }
        }
        return this.f3916c;
    }
}
