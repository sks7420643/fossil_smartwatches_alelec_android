package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jz2 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(iz2 iz2) {
        LoaderManager b = iz2.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
