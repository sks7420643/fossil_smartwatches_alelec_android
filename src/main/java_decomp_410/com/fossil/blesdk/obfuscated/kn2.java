package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.helper.AppHelper;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn2 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((fd4) null);
    @DexIgnore
    public SsoHandler a;
    @DexIgnore
    public boolean b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kn2.c;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ kn2 e;
        @DexIgnore
        public /* final */ /* synthetic */ ln2 f;

        @DexIgnore
        public b(kn2 kn2, ln2 ln2) {
            this.e = kn2;
            this.f = ln2;
        }

        @DexIgnore
        public final void run() {
            if (!this.e.a()) {
                this.f.a(600, (ud0) null, "");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WbAuthListener {
        @DexIgnore
        public /* final */ /* synthetic */ kn2 a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ ln2 c;

        @DexIgnore
        public c(kn2 kn2, Handler handler, ln2 ln2) {
            this.a = kn2;
            this.b = handler;
            this.c = ln2;
        }

        @DexIgnore
        public void cancel() {
            FLogger.INSTANCE.getLocal().d(kn2.d.a(), "Weibo loginWithEmail cancel");
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            this.c.a(2, (ud0) null, (String) null);
        }

        @DexIgnore
        public void onFailure(WbConnectErrorMessage wbConnectErrorMessage) {
            kd4.b(wbConnectErrorMessage, "wbConnectErrorMessage");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kn2.d.a();
            local.d(a2, "Weibo loginWithEmail failed - message: " + wbConnectErrorMessage.getErrorMessage() + "- code: " + wbConnectErrorMessage.getErrorCode());
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            this.c.a(600, (ud0) null, (String) null);
        }

        @DexIgnore
        public void onSuccess(Oauth2AccessToken oauth2AccessToken) {
            kd4.b(oauth2AccessToken, "oauth2AccessToken");
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            String token = oauth2AccessToken.getToken();
            String format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).format(new Date(oauth2AccessToken.getExpiresTime()));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kn2.d.a();
            local.d(a2, "Get access token from weibo success: " + token + " ,expire date: " + format);
            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
            signUpSocialAuth.setClientId(AppHelper.f.a(""));
            kd4.a((Object) token, "authToken");
            signUpSocialAuth.setToken(token);
            signUpSocialAuth.setService("weibo");
            this.c.a(signUpSocialAuth);
        }
    }

    /*
    static {
        String simpleName = kn2.class.getSimpleName();
        kd4.a((Object) simpleName, "MFLoginWeiboManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final boolean a() {
        return this.b;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        kd4.b(str, "apiKey");
        kd4.b(str2, "redirectUrl");
        kd4.b(str3, "scope");
        try {
            WbSdk.install(PortfolioApp.W.c(), new AuthInfo(PortfolioApp.W.c(), str, str2, str3));
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(c, "exception when install WBSdk");
        }
    }

    @DexIgnore
    public final void a(int i, int i2, Intent intent) {
        kd4.b(intent, "data");
        SsoHandler ssoHandler = this.a;
        if (ssoHandler == null) {
            return;
        }
        if (ssoHandler != null) {
            ssoHandler.authorizeCallBack(i, i2, intent);
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Activity activity, ln2 ln2) {
        kd4.b(activity, Constants.ACTIVITY);
        kd4.b(ln2, Constants.CALLBACK);
        try {
            this.b = false;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new b(this, ln2), 60000);
            this.a = new SsoHandler(activity);
            SsoHandler ssoHandler = this.a;
            if (ssoHandler != null) {
                ssoHandler.authorize(new c(this, handler, ln2));
            } else {
                kd4.a();
                throw null;
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "Weibo loginWithEmail failed - ex=" + e);
            ln2.a(600, (ud0) null, "");
        }
    }
}
