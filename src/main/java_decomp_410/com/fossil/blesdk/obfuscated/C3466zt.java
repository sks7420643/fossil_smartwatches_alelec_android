package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zt */
public final class C3466zt implements com.fossil.blesdk.obfuscated.C2427mo<com.fossil.blesdk.obfuscated.C1570co, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f11660a;

    @DexIgnore
    public C3466zt(com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        this.f11660a = jqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(com.fossil.blesdk.obfuscated.C1570co coVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(com.fossil.blesdk.obfuscated.C1570co coVar, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return com.fossil.blesdk.obfuscated.C2675ps.m12395a(coVar.mo9589a(), this.f11660a);
    }
}
