package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p9 */
public final class C2635p9 {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.p9$a */
    public interface C2636a {
        @DexIgnore
        void onTouchExplorationStateChanged(boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p9$b")
    /* renamed from: com.fossil.blesdk.obfuscated.p9$b */
    public static class C2637b implements android.view.accessibility.AccessibilityManager.TouchExplorationStateChangeListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2635p9.C2636a f8313a;

        @DexIgnore
        public C2637b(com.fossil.blesdk.obfuscated.C2635p9.C2636a aVar) {
            this.f8313a = aVar;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C2635p9.C2637b.class != obj.getClass()) {
                return false;
            }
            return this.f8313a.equals(((com.fossil.blesdk.obfuscated.C2635p9.C2637b) obj).f8313a);
        }

        @DexIgnore
        public int hashCode() {
            return this.f8313a.hashCode();
        }

        @DexIgnore
        public void onTouchExplorationStateChanged(boolean z) {
            this.f8313a.onTouchExplorationStateChanged(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12143a(android.view.accessibility.AccessibilityManager accessibilityManager, com.fossil.blesdk.obfuscated.C2635p9.C2636a aVar) {
        if (android.os.Build.VERSION.SDK_INT < 19 || aVar == null) {
            return false;
        }
        return accessibilityManager.addTouchExplorationStateChangeListener(new com.fossil.blesdk.obfuscated.C2635p9.C2637b(aVar));
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m12144b(android.view.accessibility.AccessibilityManager accessibilityManager, com.fossil.blesdk.obfuscated.C2635p9.C2636a aVar) {
        if (android.os.Build.VERSION.SDK_INT < 19 || aVar == null) {
            return false;
        }
        return accessibilityManager.removeTouchExplorationStateChangeListener(new com.fossil.blesdk.obfuscated.C2635p9.C2637b(aVar));
    }
}
