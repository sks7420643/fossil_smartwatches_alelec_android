package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fa2 extends ea2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.ib_back, 1);
        B.put(R.id.ftv_title, 2);
        B.put(R.id.autocomplete_places, 3);
        B.put(R.id.close_iv, 4);
        B.put(R.id.line, 5);
        B.put(R.id.ftv_address_error, 6);
        B.put(R.id.ll_recent_container, 7);
        B.put(R.id.rv_recent_addresses, 8);
    }
    */

    @DexIgnore
    public fa2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 9, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fa2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[3], objArr[4], objArr[6], objArr[2], objArr[1], objArr[5], objArr[7], objArr[8]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
