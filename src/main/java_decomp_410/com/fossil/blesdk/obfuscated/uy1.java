package com.fossil.blesdk.obfuscated;

import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class uy1 implements zw1 {
    @DexIgnore
    public /* final */ FirebaseInstanceId.a a;

    @DexIgnore
    public uy1(FirebaseInstanceId.a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public final void a(yw1 yw1) {
        FirebaseInstanceId.a aVar = this.a;
        synchronized (aVar) {
            if (aVar.a()) {
                FirebaseInstanceId.this.e();
            }
        }
    }
}
