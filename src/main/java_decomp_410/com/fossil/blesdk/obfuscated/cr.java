package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vw;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cr {
    @DexIgnore
    public /* final */ qw<jo, String> a; // = new qw<>(1000);
    @DexIgnore
    public /* final */ g8<b> b; // = vw.a(10, new a(this));

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vw.d<b> {
        @DexIgnore
        public a(cr crVar) {
        }

        @DexIgnore
        public b a() {
            try {
                return new b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vw.f {
        @DexIgnore
        public /* final */ MessageDigest e;
        @DexIgnore
        public /* final */ xw f; // = xw.b();

        @DexIgnore
        public b(MessageDigest messageDigest) {
            this.e = messageDigest;
        }

        @DexIgnore
        public xw i() {
            return this.f;
        }
    }

    @DexIgnore
    public final String a(jo joVar) {
        b a2 = this.b.a();
        tw.a(a2);
        b bVar = a2;
        try {
            joVar.a(bVar.e);
            return uw.a(bVar.e.digest());
        } finally {
            this.b.a(bVar);
        }
    }

    @DexIgnore
    public String b(jo joVar) {
        String a2;
        synchronized (this.a) {
            a2 = this.a.a(joVar);
        }
        if (a2 == null) {
            a2 = a(joVar);
        }
        synchronized (this.a) {
            this.a.b(joVar, a2);
        }
        return a2;
    }
}
