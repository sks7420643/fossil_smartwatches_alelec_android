package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nl0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nl0> CREATOR; // = new ol0();
    @DexIgnore
    public Bundle e;
    @DexIgnore
    public wd0[] f;

    @DexIgnore
    public nl0(Bundle bundle, wd0[] wd0Arr) {
        this.e = bundle;
        this.f = wd0Arr;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e, false);
        kk0.a(parcel, 2, (T[]) this.f, i, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public nl0() {
    }
}
