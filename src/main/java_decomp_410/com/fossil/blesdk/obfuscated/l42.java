package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.util.UserUtils;
import com.portfolio.platform.workers.TimeChangeReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l42 {
    @DexIgnore
    ae3 a(ee3 ee3);

    @DexIgnore
    am3 a(dm3 dm3);

    @DexIgnore
    an3 a(hn3 hn3);

    @DexIgnore
    aw2 a(ew2 ew2);

    @DexIgnore
    b53 a(c53 c53);

    @DexIgnore
    c63 a(d63 d63);

    @DexIgnore
    cf3 a(gf3 gf3);

    @DexIgnore
    ch3 a(gh3 gh3);

    @DexIgnore
    ck3 a(gk3 gk3);

    @DexIgnore
    d83 a(k83 k83);

    @DexIgnore
    d93 a(k93 k93);

    @DexIgnore
    da3 a(ka3 ka3);

    @DexIgnore
    db3 a(kb3 kb3);

    @DexIgnore
    dc3 a(kc3 kc3);

    @DexIgnore
    dd3 a(kd3 kd3);

    @DexIgnore
    dl3 a(gl3 gl3);

    @DexIgnore
    e53 a(f53 f53);

    @DexIgnore
    ez2 a(iz2 iz2);

    @DexIgnore
    fi3 a(ji3 ji3);

    @DexIgnore
    fj3 a(jj3 jj3);

    @DexIgnore
    fo3 a(jo3 jo3);

    @DexIgnore
    fv2 a(gv2 gv2);

    @DexIgnore
    fx2 a(kx2 kx2);

    @DexIgnore
    g23 a(l23 l23);

    @DexIgnore
    h43 a(l43 l43);

    @DexIgnore
    h73 a(i73 i73);

    @DexIgnore
    hp3 a(lp3 lp3);

    @DexIgnore
    hu2 a(ku2 ku2);

    @DexIgnore
    i13 a(j13 j13);

    @DexIgnore
    i33 a(m33 m33);

    @DexIgnore
    i53 a(l53 l53);

    @DexIgnore
    j03 a(n03 n03);

    @DexIgnore
    j63 a(o63 o63);

    @DexIgnore
    je3 a(ne3 ne3);

    @DexIgnore
    jq3 a(mq3 mq3);

    @DexIgnore
    kl3 a(nl3 nl3);

    @DexIgnore
    kw2 a(pw2 pw2);

    @DexIgnore
    lf3 a(pf3 pf3);

    @DexIgnore
    lh3 a(ph3 ph3);

    @DexIgnore
    m73 a(n73 n73);

    @DexIgnore
    mk3 a(pk3 pk3);

    @DexIgnore
    ny2 a(oy2 oy2);

    @DexIgnore
    o43 a(t43 t43);

    @DexIgnore
    oi3 a(ri3 ri3);

    @DexIgnore
    oz2 a(sz2 sz2);

    @DexIgnore
    pj3 a(sj3 sj3);

    @DexIgnore
    pu2 a(av2 av2);

    @DexIgnore
    q53 a(u53 u53);

    @DexIgnore
    qm3 a(tm3 tm3);

    @DexIgnore
    qv2 a(vv2 vv2);

    @DexIgnore
    r13 a(w13 w13);

    @DexIgnore
    r33 a(u33 u33);

    @DexIgnore
    s23 a(d33 d33);

    @DexIgnore
    s63 a(v63 v63);

    @DexIgnore
    sg3 a(wg3 wg3);

    @DexIgnore
    sp3 a(vp3 vp3);

    @DexIgnore
    te3 a(xe3 xe3);

    @DexIgnore
    th3 a(yh3 yh3);

    @DexIgnore
    tl3 a(wl3 wl3);

    @DexIgnore
    tq3 a(yq3 yq3);

    @DexIgnore
    uf3 a(yf3 yf3);

    @DexIgnore
    uk3 a(xk3 xk3);

    @DexIgnore
    un3 a(yn3 yn3);

    @DexIgnore
    uw2 a(yw2 yw2);

    @DexIgnore
    v03 a(z03 z03);

    @DexIgnore
    vo3 a(wo3 wo3);

    @DexIgnore
    w23 a(z23 z23);

    @DexIgnore
    wi3 a(aj3 aj3);

    @DexIgnore
    wt2 a(au2 au2);

    @DexIgnore
    yo3 a(bp3 bp3);

    @DexIgnore
    yz2 a(c03 c03);

    @DexIgnore
    z33 a(d43 d43);

    @DexIgnore
    z63 a(d73 d73);

    @DexIgnore
    zp3 a(cq3 cq3);

    @DexIgnore
    zx2 a(ey2 ey2);

    @DexIgnore
    void a(nn2 nn2);

    @DexIgnore
    void a(nr3 nr3);

    @DexIgnore
    void a(ql2 ql2);

    @DexIgnore
    void a(PortfolioApp portfolioApp);

    @DexIgnore
    void a(CloudImageHelper cloudImageHelper);

    @DexIgnore
    void a(URLRequestTaskHelper uRLRequestTaskHelper);

    @DexIgnore
    void a(AppHelper appHelper);

    @DexIgnore
    void a(DeviceHelper deviceHelper);

    @DexIgnore
    void a(LightAndHapticsManager lightAndHapticsManager);

    @DexIgnore
    void a(WeatherManager weatherManager);

    @DexIgnore
    void a(NotificationReceiver notificationReceiver);

    @DexIgnore
    void a(AlarmReceiver alarmReceiver);

    @DexIgnore
    void a(AppPackageRemoveReceiver appPackageRemoveReceiver);

    @DexIgnore
    void a(BootReceiver bootReceiver);

    @DexIgnore
    void a(LocaleChangedReceiver localeChangedReceiver);

    @DexIgnore
    void a(NetworkChangedReceiver networkChangedReceiver);

    @DexIgnore
    void a(FossilNotificationListenerService fossilNotificationListenerService);

    @DexIgnore
    void a(MFDeviceService mFDeviceService);

    @DexIgnore
    void a(ComplicationWeatherService complicationWeatherService);

    @DexIgnore
    void a(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService);

    @DexIgnore
    void a(FossilFirebaseMessagingService fossilFirebaseMessagingService);

    @DexIgnore
    void a(CommuteTimeService commuteTimeService);

    @DexIgnore
    void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager);

    @DexIgnore
    void a(BaseActivity baseActivity);

    @DexIgnore
    void a(DebugActivity debugActivity);

    @DexIgnore
    void a(LoginPresenter loginPresenter);

    @DexIgnore
    void a(ProfileSetupPresenter profileSetupPresenter);

    @DexIgnore
    void a(SignUpPresenter signUpPresenter);

    @DexIgnore
    void a(DeviceUtils deviceUtils);

    @DexIgnore
    void a(UserUtils userUtils);

    @DexIgnore
    void a(TimeChangeReceiver timeChangeReceiver);
}
