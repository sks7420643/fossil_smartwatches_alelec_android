package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class id0 {
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark; // = 2131099747;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_default; // = 2131099748;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_disabled; // = 2131099749;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_focused; // = 2131099750;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_pressed; // = 2131099751;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light; // = 2131099752;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_default; // = 2131099753;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_disabled; // = 2131099754;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_focused; // = 2131099755;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_pressed; // = 2131099756;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_tint; // = 2131099757;
}
