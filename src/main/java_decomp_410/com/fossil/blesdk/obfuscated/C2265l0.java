package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l0 */
public class C2265l0 extends androidx.appcompat.app.ActionBar implements androidx.appcompat.widget.ActionBarOverlayLayout.C0066d {

    @DexIgnore
    /* renamed from: B */
    public static /* final */ android.view.animation.Interpolator f7044B; // = new android.view.animation.AccelerateInterpolator();

    @DexIgnore
    /* renamed from: C */
    public static /* final */ android.view.animation.Interpolator f7045C; // = new android.view.animation.DecelerateInterpolator();

    @DexIgnore
    /* renamed from: A */
    public /* final */ com.fossil.blesdk.obfuscated.C2385m9 f7046A; // = new com.fossil.blesdk.obfuscated.C2265l0.C2268c();

    @DexIgnore
    /* renamed from: a */
    public android.content.Context f7047a;

    @DexIgnore
    /* renamed from: b */
    public android.content.Context f7048b;

    @DexIgnore
    /* renamed from: c */
    public androidx.appcompat.widget.ActionBarOverlayLayout f7049c;

    @DexIgnore
    /* renamed from: d */
    public androidx.appcompat.widget.ActionBarContainer f7050d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2101j2 f7051e;

    @DexIgnore
    /* renamed from: f */
    public androidx.appcompat.widget.ActionBarContextView f7052f;

    @DexIgnore
    /* renamed from: g */
    public android.view.View f7053g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2852s2 f7054h;

    @DexIgnore
    /* renamed from: i */
    public boolean f7055i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2265l0.C2269d f7056j;

    @DexIgnore
    /* renamed from: k */
    public androidx.appcompat.view.ActionMode f7057k;

    @DexIgnore
    /* renamed from: l */
    public androidx.appcompat.view.ActionMode.Callback f7058l;

    @DexIgnore
    /* renamed from: m */
    public boolean f7059m;

    @DexIgnore
    /* renamed from: n */
    public java.util.ArrayList<androidx.appcompat.app.ActionBar.C0044a> f7060n; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: o */
    public boolean f7061o;

    @DexIgnore
    /* renamed from: p */
    public int f7062p; // = 0;

    @DexIgnore
    /* renamed from: q */
    public boolean f7063q; // = true;

    @DexIgnore
    /* renamed from: r */
    public boolean f7064r;

    @DexIgnore
    /* renamed from: s */
    public boolean f7065s;

    @DexIgnore
    /* renamed from: t */
    public boolean f7066t;

    @DexIgnore
    /* renamed from: u */
    public boolean f7067u; // = true;

    @DexIgnore
    /* renamed from: v */
    public com.fossil.blesdk.obfuscated.C3337y0 f7068v;

    @DexIgnore
    /* renamed from: w */
    public boolean f7069w;

    @DexIgnore
    /* renamed from: x */
    public boolean f7070x;

    @DexIgnore
    /* renamed from: y */
    public /* final */ com.fossil.blesdk.obfuscated.C2190k9 f7071y; // = new com.fossil.blesdk.obfuscated.C2265l0.C2266a();

    @DexIgnore
    /* renamed from: z */
    public /* final */ com.fossil.blesdk.obfuscated.C2190k9 f7072z; // = new com.fossil.blesdk.obfuscated.C2265l0.C2267b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l0$a */
    public class C2266a extends com.fossil.blesdk.obfuscated.C2302l9 {
        @DexIgnore
        public C2266a() {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            com.fossil.blesdk.obfuscated.C2265l0 l0Var = com.fossil.blesdk.obfuscated.C2265l0.this;
            if (l0Var.f7063q) {
                android.view.View view2 = l0Var.f7053g;
                if (view2 != null) {
                    view2.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    com.fossil.blesdk.obfuscated.C2265l0.this.f7050d.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
            }
            com.fossil.blesdk.obfuscated.C2265l0.this.f7050d.setVisibility(8);
            com.fossil.blesdk.obfuscated.C2265l0.this.f7050d.setTransitioning(false);
            com.fossil.blesdk.obfuscated.C2265l0 l0Var2 = com.fossil.blesdk.obfuscated.C2265l0.this;
            l0Var2.f7068v = null;
            l0Var2.mo13008l();
            androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = com.fossil.blesdk.obfuscated.C2265l0.this.f7049c;
            if (actionBarOverlayLayout != null) {
                com.fossil.blesdk.obfuscated.C1776f9.m6797D(actionBarOverlayLayout);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l0$b */
    public class C2267b extends com.fossil.blesdk.obfuscated.C2302l9 {
        @DexIgnore
        public C2267b() {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            com.fossil.blesdk.obfuscated.C2265l0 l0Var = com.fossil.blesdk.obfuscated.C2265l0.this;
            l0Var.f7068v = null;
            l0Var.f7050d.requestLayout();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l0$c")
    /* renamed from: com.fossil.blesdk.obfuscated.l0$c */
    public class C2268c implements com.fossil.blesdk.obfuscated.C2385m9 {
        @DexIgnore
        public C2268c() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13014a(android.view.View view) {
            ((android.view.View) com.fossil.blesdk.obfuscated.C2265l0.this.f7050d.getParent()).invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l0$d")
    /* renamed from: com.fossil.blesdk.obfuscated.l0$d */
    public class C2269d extends androidx.appcompat.view.ActionMode implements com.fossil.blesdk.obfuscated.C1915h1.C1916a {

        @DexIgnore
        /* renamed from: g */
        public /* final */ android.content.Context f7076g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ com.fossil.blesdk.obfuscated.C1915h1 f7077h;

        @DexIgnore
        /* renamed from: i */
        public androidx.appcompat.view.ActionMode.Callback f7078i;

        @DexIgnore
        /* renamed from: j */
        public java.lang.ref.WeakReference<android.view.View> f7079j;

        @DexIgnore
        public C2269d(android.content.Context context, androidx.appcompat.view.ActionMode.Callback callback) {
            this.f7076g = context;
            this.f7078i = callback;
            com.fossil.blesdk.obfuscated.C1915h1 h1Var = new com.fossil.blesdk.obfuscated.C1915h1(context);
            h1Var.mo11485c(1);
            this.f7077h = h1Var;
            this.f7077h.mo11460a((com.fossil.blesdk.obfuscated.C1915h1.C1916a) this);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo328a() {
            com.fossil.blesdk.obfuscated.C2265l0 l0Var = com.fossil.blesdk.obfuscated.C2265l0.this;
            if (l0Var.f7056j == this) {
                if (!com.fossil.blesdk.obfuscated.C2265l0.m9930a(l0Var.f7064r, l0Var.f7065s, false)) {
                    com.fossil.blesdk.obfuscated.C2265l0 l0Var2 = com.fossil.blesdk.obfuscated.C2265l0.this;
                    l0Var2.f7057k = this;
                    l0Var2.f7058l = this.f7078i;
                } else {
                    this.f7078i.mo345a(this);
                }
                this.f7078i = null;
                com.fossil.blesdk.obfuscated.C2265l0.this.mo13002f(false);
                com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.mo414a();
                com.fossil.blesdk.obfuscated.C2265l0.this.f7051e.mo8490k().sendAccessibilityEvent(32);
                com.fossil.blesdk.obfuscated.C2265l0 l0Var3 = com.fossil.blesdk.obfuscated.C2265l0.this;
                l0Var3.f7049c.setHideOnContentScrollEnabled(l0Var3.f7070x);
                com.fossil.blesdk.obfuscated.C2265l0.this.f7056j = null;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo336b(java.lang.CharSequence charSequence) {
            com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.setTitle(charSequence);
        }

        @DexIgnore
        /* renamed from: c */
        public android.view.Menu mo337c() {
            return this.f7077h;
        }

        @DexIgnore
        /* renamed from: d */
        public android.view.MenuInflater mo338d() {
            return new com.fossil.blesdk.obfuscated.C3251x0(this.f7076g);
        }

        @DexIgnore
        /* renamed from: e */
        public java.lang.CharSequence mo339e() {
            return com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.getSubtitle();
        }

        @DexIgnore
        /* renamed from: g */
        public java.lang.CharSequence mo341g() {
            return com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.getTitle();
        }

        @DexIgnore
        /* renamed from: i */
        public void mo343i() {
            if (com.fossil.blesdk.obfuscated.C2265l0.this.f7056j == this) {
                this.f7077h.mo11524s();
                try {
                    this.f7078i.mo348b(this, this.f7077h);
                } finally {
                    this.f7077h.mo11521r();
                }
            }
        }

        @DexIgnore
        /* renamed from: j */
        public boolean mo344j() {
            return com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.mo417c();
        }

        @DexIgnore
        /* renamed from: k */
        public boolean mo13015k() {
            this.f7077h.mo11524s();
            try {
                return this.f7078i.mo346a((androidx.appcompat.view.ActionMode) this, (android.view.Menu) this.f7077h);
            } finally {
                this.f7077h.mo11521r();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo335b(int i) {
            mo336b((java.lang.CharSequence) com.fossil.blesdk.obfuscated.C2265l0.this.f7047a.getResources().getString(i));
        }

        @DexIgnore
        /* renamed from: b */
        public android.view.View mo334b() {
            java.lang.ref.WeakReference<android.view.View> weakReference = this.f7079j;
            if (weakReference != null) {
                return (android.view.View) weakReference.get();
            }
            return null;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo330a(android.view.View view) {
            com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.setCustomView(view);
            this.f7079j = new java.lang.ref.WeakReference<>(view);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo331a(java.lang.CharSequence charSequence) {
            com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.setSubtitle(charSequence);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo329a(int i) {
            mo331a((java.lang.CharSequence) com.fossil.blesdk.obfuscated.C2265l0.this.f7047a.getResources().getString(i));
        }

        @DexIgnore
        /* renamed from: a */
        public void mo333a(boolean z) {
            super.mo333a(z);
            com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.setTitleOptional(z);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo536a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
            androidx.appcompat.view.ActionMode.Callback callback = this.f7078i;
            if (callback != null) {
                return callback.mo347a((androidx.appcompat.view.ActionMode) this, menuItem);
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo535a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            if (this.f7078i != null) {
                mo343i();
                com.fossil.blesdk.obfuscated.C2265l0.this.f7052f.mo419e();
            }
        }
    }

    @DexIgnore
    public C2265l0(android.app.Activity activity, boolean z) {
        new java.util.ArrayList();
        android.view.View decorView = activity.getWindow().getDecorView();
        mo13001b(decorView);
        if (!z) {
            this.f7053g = decorView.findViewById(16908290);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m9930a(boolean z, boolean z2, boolean z3) {
        if (z3) {
            return true;
        }
        return !z && !z2;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2101j2 mo12999a(android.view.View view) {
        if (view instanceof com.fossil.blesdk.obfuscated.C2101j2) {
            return (com.fossil.blesdk.obfuscated.C2101j2) view;
        }
        if (view instanceof androidx.appcompat.widget.Toolbar) {
            return ((androidx.appcompat.widget.Toolbar) view).getWrapper();
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new java.lang.IllegalStateException(sb.toString());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo500b() {
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13001b(android.view.View view) {
        this.f7049c = (androidx.appcompat.widget.ActionBarOverlayLayout) view.findViewById(com.fossil.blesdk.obfuscated.C3158w.decor_content_parent);
        androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = this.f7049c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.f7051e = mo12999a(view.findViewById(com.fossil.blesdk.obfuscated.C3158w.action_bar));
        this.f7052f = (androidx.appcompat.widget.ActionBarContextView) view.findViewById(com.fossil.blesdk.obfuscated.C3158w.action_context_bar);
        this.f7050d = (androidx.appcompat.widget.ActionBarContainer) view.findViewById(com.fossil.blesdk.obfuscated.C3158w.action_bar_container);
        com.fossil.blesdk.obfuscated.C2101j2 j2Var = this.f7051e;
        if (j2Var == null || this.f7052f == null || this.f7050d == null) {
            throw new java.lang.IllegalStateException(com.fossil.blesdk.obfuscated.C2265l0.class.getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f7047a = j2Var.getContext();
        boolean z = (this.f7051e.mo8491l() & 4) != 0;
        if (z) {
            this.f7055i = true;
        }
        com.fossil.blesdk.obfuscated.C2849s0 a = com.fossil.blesdk.obfuscated.C2849s0.m13459a(this.f7047a);
        mo13007k(a.mo15802a() || z);
        mo13005i(a.mo15807f());
        android.content.res.TypedArray obtainStyledAttributes = this.f7047a.obtainStyledAttributes((android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C1368a0.ActionBar, com.fossil.blesdk.obfuscated.C2777r.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_hideOnContentScroll, false)) {
            mo13006j(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C1368a0.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            mo177a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo501c() {
        if (!this.f7065s) {
            this.f7065s = true;
            mo13009l(true);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo185d(boolean z) {
        mo13000a(z ? 4 : 0, 4);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo186e(boolean z) {
        this.f7069w = z;
        if (!z) {
            com.fossil.blesdk.obfuscated.C3337y0 y0Var = this.f7068v;
            if (y0Var != null) {
                y0Var.mo17953a();
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo13002f(boolean z) {
        com.fossil.blesdk.obfuscated.C2110j9 j9Var;
        com.fossil.blesdk.obfuscated.C2110j9 j9Var2;
        if (z) {
            mo13013p();
        } else {
            mo13011n();
        }
        if (mo13012o()) {
            if (z) {
                j9Var = this.f7051e.mo8461a(4, 100);
                j9Var2 = this.f7052f.mo17585a(0, 200);
            } else {
                j9Var2 = this.f7051e.mo8461a(0, 200);
                j9Var = this.f7052f.mo17585a(8, 100);
            }
            com.fossil.blesdk.obfuscated.C3337y0 y0Var = new com.fossil.blesdk.obfuscated.C3337y0();
            y0Var.mo17951a(j9Var, j9Var2);
            y0Var.mo17955c();
        } else if (z) {
            this.f7051e.setVisibility(4);
            this.f7052f.setVisibility(0);
        } else {
            this.f7051e.setVisibility(0);
            this.f7052f.setVisibility(8);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public int mo189g() {
        return this.f7051e.mo8491l();
    }

    @DexIgnore
    /* renamed from: h */
    public void mo13004h(boolean z) {
        com.fossil.blesdk.obfuscated.C3337y0 y0Var = this.f7068v;
        if (y0Var != null) {
            y0Var.mo17953a();
        }
        this.f7050d.setVisibility(0);
        if (this.f7062p != 0 || (!this.f7069w && !z)) {
            this.f7050d.setAlpha(1.0f);
            this.f7050d.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            if (this.f7063q) {
                android.view.View view = this.f7053g;
                if (view != null) {
                    view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
            }
            this.f7072z.mo8506b((android.view.View) null);
        } else {
            this.f7050d.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f = (float) (-this.f7050d.getHeight());
            if (z) {
                int[] iArr = {0, 0};
                this.f7050d.getLocationInWindow(iArr);
                f -= (float) iArr[1];
            }
            this.f7050d.setTranslationY(f);
            com.fossil.blesdk.obfuscated.C3337y0 y0Var2 = new com.fossil.blesdk.obfuscated.C3337y0();
            com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this.f7050d);
            a.mo12283b((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a.mo12279a(this.f7046A);
            y0Var2.mo17950a(a);
            if (this.f7063q) {
                android.view.View view2 = this.f7053g;
                if (view2 != null) {
                    view2.setTranslationY(f);
                    com.fossil.blesdk.obfuscated.C2110j9 a2 = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this.f7053g);
                    a2.mo12283b((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    y0Var2.mo17950a(a2);
                }
            }
            y0Var2.mo17949a(f7045C);
            y0Var2.mo17948a(250);
            y0Var2.mo17952a(this.f7072z);
            this.f7068v = y0Var2;
            y0Var2.mo17955c();
        }
        androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = this.f7049c;
        if (actionBarOverlayLayout != null) {
            com.fossil.blesdk.obfuscated.C1776f9.m6797D(actionBarOverlayLayout);
        }
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo13005i(boolean z) {
        this.f7061o = z;
        if (!this.f7061o) {
            this.f7051e.mo8467a((com.fossil.blesdk.obfuscated.C2852s2) null);
            this.f7050d.setTabContainer(this.f7054h);
        } else {
            this.f7050d.setTabContainer((com.fossil.blesdk.obfuscated.C2852s2) null);
            this.f7051e.mo8467a(this.f7054h);
        }
        boolean z2 = true;
        boolean z3 = mo13010m() == 2;
        com.fossil.blesdk.obfuscated.C2852s2 s2Var = this.f7054h;
        if (s2Var != null) {
            if (z3) {
                s2Var.setVisibility(0);
                androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = this.f7049c;
                if (actionBarOverlayLayout != null) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6797D(actionBarOverlayLayout);
                }
            } else {
                s2Var.setVisibility(8);
            }
        }
        this.f7051e.mo8475b(!this.f7061o && z3);
        androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout2 = this.f7049c;
        if (this.f7061o || !z3) {
            z2 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z2);
    }

    @DexIgnore
    /* renamed from: j */
    public void mo13006j(boolean z) {
        if (!z || this.f7049c.mo462j()) {
            this.f7070x = z;
            this.f7049c.setHideOnContentScrollEnabled(z);
            return;
        }
        throw new java.lang.IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    @DexIgnore
    /* renamed from: k */
    public void mo13007k(boolean z) {
        this.f7051e.mo8469a(z);
    }

    @DexIgnore
    /* renamed from: l */
    public void mo13008l() {
        androidx.appcompat.view.ActionMode.Callback callback = this.f7058l;
        if (callback != null) {
            callback.mo345a(this.f7057k);
            this.f7057k = null;
            this.f7058l = null;
        }
    }

    @DexIgnore
    /* renamed from: m */
    public int mo13010m() {
        return this.f7051e.mo8489j();
    }

    @DexIgnore
    /* renamed from: n */
    public final void mo13011n() {
        if (this.f7066t) {
            this.f7066t = false;
            androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = this.f7049c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            mo13009l(false);
        }
    }

    @DexIgnore
    /* renamed from: o */
    public final boolean mo13012o() {
        return com.fossil.blesdk.obfuscated.C1776f9.m6860z(this.f7050d);
    }

    @DexIgnore
    /* renamed from: p */
    public final void mo13013p() {
        if (!this.f7066t) {
            this.f7066t = true;
            androidx.appcompat.widget.ActionBarOverlayLayout actionBarOverlayLayout = this.f7049c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            mo13009l(false);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo502d() {
        com.fossil.blesdk.obfuscated.C3337y0 y0Var = this.f7068v;
        if (y0Var != null) {
            y0Var.mo17953a();
            this.f7068v = null;
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo13003g(boolean z) {
        com.fossil.blesdk.obfuscated.C3337y0 y0Var = this.f7068v;
        if (y0Var != null) {
            y0Var.mo17953a();
        }
        if (this.f7062p != 0 || (!this.f7069w && !z)) {
            this.f7071y.mo8506b((android.view.View) null);
            return;
        }
        this.f7050d.setAlpha(1.0f);
        this.f7050d.setTransitioning(true);
        com.fossil.blesdk.obfuscated.C3337y0 y0Var2 = new com.fossil.blesdk.obfuscated.C3337y0();
        float f = (float) (-this.f7050d.getHeight());
        if (z) {
            int[] iArr = {0, 0};
            this.f7050d.getLocationInWindow(iArr);
            f -= (float) iArr[1];
        }
        com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this.f7050d);
        a.mo12283b(f);
        a.mo12279a(this.f7046A);
        y0Var2.mo17950a(a);
        if (this.f7063q) {
            android.view.View view = this.f7053g;
            if (view != null) {
                com.fossil.blesdk.obfuscated.C2110j9 a2 = com.fossil.blesdk.obfuscated.C1776f9.m6801a(view);
                a2.mo12283b(f);
                y0Var2.mo17950a(a2);
            }
        }
        y0Var2.mo17949a(f7044B);
        y0Var2.mo17948a(250);
        y0Var2.mo17952a(this.f7071y);
        this.f7068v = y0Var2;
        y0Var2.mo17955c();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo184c(boolean z) {
        if (!this.f7055i) {
            mo185d(z);
        }
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo13009l(boolean z) {
        if (m9930a(this.f7064r, this.f7065s, this.f7066t)) {
            if (!this.f7067u) {
                this.f7067u = true;
                mo13004h(z);
            }
        } else if (this.f7067u) {
            this.f7067u = false;
            mo13003g(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo177a(float f) {
        com.fossil.blesdk.obfuscated.C1776f9.m6824b((android.view.View) this.f7050d, f);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo178a(android.content.res.Configuration configuration) {
        mo13005i(com.fossil.blesdk.obfuscated.C2849s0.m13459a(this.f7047a).mo15807f());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo498a(int i) {
        this.f7062p = i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo179a(java.lang.CharSequence charSequence) {
        this.f7051e.setTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13000a(int i, int i2) {
        int l = this.f7051e.mo8491l();
        if ((i2 & 4) != 0) {
            this.f7055i = true;
        }
        this.f7051e.mo8462a((i & i2) | ((~i2) & l));
    }

    @DexIgnore
    public C2265l0(android.app.Dialog dialog) {
        new java.util.ArrayList();
        mo13001b(dialog.getWindow().getDecorView());
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.appcompat.view.ActionMode mo176a(androidx.appcompat.view.ActionMode.Callback callback) {
        com.fossil.blesdk.obfuscated.C2265l0.C2269d dVar = this.f7056j;
        if (dVar != null) {
            dVar.mo328a();
        }
        this.f7049c.setHideOnContentScrollEnabled(false);
        this.f7052f.mo418d();
        com.fossil.blesdk.obfuscated.C2265l0.C2269d dVar2 = new com.fossil.blesdk.obfuscated.C2265l0.C2269d(this.f7052f.getContext(), callback);
        if (!dVar2.mo13015k()) {
            return null;
        }
        this.f7056j = dVar2;
        dVar2.mo343i();
        this.f7052f.mo415a(dVar2);
        mo13002f(true);
        this.f7052f.sendAccessibilityEvent(32);
        return dVar2;
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo188f() {
        com.fossil.blesdk.obfuscated.C2101j2 j2Var = this.f7051e;
        if (j2Var == null || !j2Var.mo8487h()) {
            return false;
        }
        this.f7051e.collapseActionView();
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo183b(boolean z) {
        if (z != this.f7059m) {
            this.f7059m = z;
            int size = this.f7060n.size();
            for (int i = 0; i < size; i++) {
                this.f7060n.get(i).mo194a(z);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo499a(boolean z) {
        this.f7063q = z;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo182b(java.lang.CharSequence charSequence) {
        this.f7051e.setWindowTitle(charSequence);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo497a() {
        if (this.f7065s) {
            this.f7065s = false;
            mo13009l(true);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo180a(int i, android.view.KeyEvent keyEvent) {
        com.fossil.blesdk.obfuscated.C2265l0.C2269d dVar = this.f7056j;
        if (dVar == null) {
            return false;
        }
        android.view.Menu c = dVar.mo337c();
        if (c == null) {
            return false;
        }
        boolean z = true;
        if (android.view.KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        c.setQwertyMode(z);
        return c.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    /* renamed from: h */
    public android.content.Context mo190h() {
        if (this.f7048b == null) {
            android.util.TypedValue typedValue = new android.util.TypedValue();
            this.f7047a.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarWidgetTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                this.f7048b = new android.view.ContextThemeWrapper(this.f7047a, i);
            } else {
                this.f7048b = this.f7047a;
            }
        }
        return this.f7048b;
    }
}
