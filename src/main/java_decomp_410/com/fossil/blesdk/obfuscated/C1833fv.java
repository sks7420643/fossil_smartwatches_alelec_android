package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fv */
public class C1833fv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1833fv.C1834a<?>> f5293a; // = new java.util.ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fv$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fv$a */
    public static final class C1834a<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Class<T> f5294a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1963ho<T> f5295b;

        @DexIgnore
        public C1834a(java.lang.Class<T> cls, com.fossil.blesdk.obfuscated.C1963ho<T> hoVar) {
            this.f5294a = cls;
            this.f5295b = hoVar;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo11050a(java.lang.Class<?> cls) {
            return this.f5294a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T> com.fossil.blesdk.obfuscated.C1963ho<T> mo11048a(java.lang.Class<T> cls) {
        for (com.fossil.blesdk.obfuscated.C1833fv.C1834a next : this.f5293a) {
            if (next.mo11050a(cls)) {
                return next.f5295b;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T> void mo11049a(java.lang.Class<T> cls, com.fossil.blesdk.obfuscated.C1963ho<T> hoVar) {
        this.f5293a.add(new com.fossil.blesdk.obfuscated.C1833fv.C1834a(cls, hoVar));
    }
}
