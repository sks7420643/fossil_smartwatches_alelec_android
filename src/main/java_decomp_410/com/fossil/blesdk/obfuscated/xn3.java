package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.oq4;
import com.fossil.blesdk.obfuscated.ws2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xn3 extends zr2 implements wn3 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<ue2> j;
    @DexIgnore
    public vn3 k;
    @DexIgnore
    public ws2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xn3.n;
        }

        @DexIgnore
        public final xn3 b() {
            return new xn3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ xn3 e;

        @DexIgnore
        public b(xn3 xn3) {
            this.e = xn3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ws2.b {
        @DexIgnore
        public /* final */ /* synthetic */ xn3 a;

        @DexIgnore
        public c(xn3 xn3) {
            this.a = xn3;
        }

        @DexIgnore
        public void a(ws2.c cVar) {
            String[] strArr;
            kd4.b(cVar, "permissionModel");
            if (cVar.d() == 15) {
                if (pq4.a((Context) PortfolioApp.W.c(), "android.permission.ACCESS_FINE_LOCATION")) {
                    Object[] array = cVar.e().toArray(new String[0]);
                    if (array != null) {
                        strArr = (String[]) array;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else {
                    Object[] array2 = cb4.c("android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_BACKGROUND_LOCATION").toArray(new String[0]);
                    if (array2 != null) {
                        strArr = (String[]) array2;
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                pq4.a((Fragment) this.a, cVar.c(), cVar.d(), (String[]) Arrays.copyOf(strArr, strArr.length));
                return;
            }
            xn3 xn3 = this.a;
            String c = cVar.c();
            int d = cVar.d();
            Object[] array3 = cVar.e().toArray(new String[0]);
            if (array3 != null) {
                String[] strArr2 = (String[]) array3;
                pq4.a((Fragment) xn3, c, d, (String[]) Arrays.copyOf(strArr2, strArr2.length));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void b(ws2.c cVar) {
            kd4.b(cVar, "permissionModel");
            int d = cVar.d();
            if (d == 1) {
                this.a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
            } else if (d == 3) {
                this.a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }
    }

    /*
    static {
        String simpleName = xn3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "PermissionFragment::class.java.simpleName!!");
            n = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        kd4.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = n;
        local.d(str, "onPermissionsGranted:" + i + ':' + list.size());
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        this.j = new tr3<>(this, (ue2) qa.a(layoutInflater, R.layout.fragment_permission, viewGroup, false, O0()));
        tr3<ue2> tr3 = this.j;
        if (tr3 != null) {
            ue2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        vn3 vn3 = this.k;
        if (vn3 != null) {
            vn3.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vn3 vn3 = this.k;
        if (vn3 != null) {
            vn3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<ue2> tr3 = this.j;
        if (tr3 != null) {
            ue2 a2 = tr3.a();
            if (a2 != null) {
                ImageView imageView = a2.r;
                if (imageView != null) {
                    imageView.setOnClickListener(new b(this));
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t(List<ws2.c> list) {
        kd4.b(list, "listPermissionModel");
        if (this.l == null) {
            int i = 0;
            if (list.size() > 1) {
                tr3<ue2> tr3 = this.j;
                if (tr3 != null) {
                    ue2 a2 = tr3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.q;
                        if (flexibleTextView != null) {
                            flexibleTextView.setVisibility(0);
                        }
                    }
                    i = 1;
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else {
                tr3<ue2> tr32 = this.j;
                if (tr32 != null) {
                    ue2 a3 = tr32.a();
                    if (a3 != null) {
                        FlexibleTextView flexibleTextView2 = a3.q;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setVisibility(8);
                        }
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            }
            ws2 ws2 = new ws2(i);
            ws2.a((ws2.b) new c(this));
            this.l = ws2;
            tr3<ue2> tr33 = this.j;
            if (tr33 != null) {
                ue2 a4 = tr33.a();
                RecyclerView recyclerView = a4 != null ? a4.s : null;
                if (recyclerView != null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setAdapter(this.l);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
        ws2 ws22 = this.l;
        if (ws22 != null) {
            ws22.a(list);
        }
    }

    @DexIgnore
    public void a(vn3 vn3) {
        kd4.b(vn3, "presenter");
        this.k = vn3;
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        kd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(n, "onPermissionsDenied:" + i + ':' + list.size());
        if (pq4.a((Fragment) this, list)) {
            boolean z = !kb4.b(bn2.d.a(100), (ArrayList<String>) list).isEmpty();
            boolean z2 = !kb4.b(bn2.d.a(101), (ArrayList<String>) list).isEmpty();
            if (z) {
                oq4.b bVar = new oq4.b(this);
                bVar.b(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.calls_and_messages_permission));
                bVar.a(bn2.d.b(100).getSecond());
                bVar.a().b();
            } else if (z2) {
                oq4.b bVar2 = new oq4.b(this);
                bVar2.b(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.calls_and_messages_permission));
                bVar2.a(bn2.d.b(101).getSecond());
                bVar2.a().b();
            } else {
                new oq4.b(this).a().b();
            }
        }
    }
}
