package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kq2;
import com.fossil.wearables.fossil.R;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jq2 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<kq2> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<pq2> b;
    @DexIgnore
    public b c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public ImageView b;
        @DexIgnore
        public RecyclerView c;
        @DexIgnore
        public /* final */ /* synthetic */ jq2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jq2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.jq2$a$a  reason: collision with other inner class name */
        public static final class C0091a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0091a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.e.d();
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public b(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.e.d();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(jq2 jq2, View view) {
            super(view);
            kd4.b(view, "itemView");
            this.d = jq2;
            View findViewById = view.findViewById(R.id.tv_debug_parent);
            if (findViewById != null) {
                TextView textView = (TextView) findViewById;
                textView.setOnClickListener(new C0091a(this));
                this.a = textView;
                View findViewById2 = view.findViewById(R.id.iv_arrow);
                if (findViewById2 != null) {
                    ImageView imageView = (ImageView) findViewById2;
                    imageView.setOnClickListener(new b(this));
                    this.b = imageView;
                    View findViewById3 = view.findViewById(R.id.rv_debug_parent);
                    if (findViewById3 != null) {
                        this.c = (RecyclerView) findViewById3;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final RecyclerView b() {
            return this.c;
        }

        @DexIgnore
        public final TextView c() {
            return this.a;
        }

        @DexIgnore
        public final void d() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                ArrayList<pq2> c2 = this.d.c();
                if (c2 != null) {
                    boolean c3 = c2.get(adapterPosition).c();
                    ArrayList<pq2> c4 = this.d.c();
                    if (c4 != null) {
                        c4.get(adapterPosition).a(!c3);
                        this.d.notifyItemChanged(adapterPosition);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final ImageView a() {
            return this.b;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements kq2.e {
        @DexIgnore
        public /* final */ /* synthetic */ jq2 a;

        @DexIgnore
        public c(pq2 pq2, jq2 jq2) {
            this.a = jq2;
        }

        @DexIgnore
        public void a(String str, int i, int i2, Object obj, Bundle bundle) {
            kd4.b(str, "tagName");
            b b = this.a.b();
            if (b != null) {
                b.a(str, i, i2, obj, bundle);
            }
        }

        @DexIgnore
        public void b(String str, int i, int i2, Object obj, Bundle bundle) {
            kd4.b(str, "tagName");
            b b = this.a.b();
            if (b != null) {
                b.b(str, i, i2, obj, bundle);
            }
        }
    }

    @DexIgnore
    public final void a(ArrayList<pq2> arrayList) {
        this.a.clear();
        if (arrayList != null) {
            for (pq2 pq2 : arrayList) {
                ArrayList<kq2> arrayList2 = this.a;
                kq2 kq2 = new kq2();
                kq2.a((kq2.e) new c(pq2, this));
                kq2.a((List<? extends lq2>) pq2.a());
                arrayList2.add(kq2);
            }
        }
        this.b = arrayList;
    }

    @DexIgnore
    public final b b() {
        return this.c;
    }

    @DexIgnore
    public final ArrayList<pq2> c() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        ArrayList<pq2> arrayList = this.b;
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_debug_parent, viewGroup, false);
        kd4.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        TextView c2 = aVar.c();
        ArrayList<pq2> arrayList = this.b;
        if (arrayList != null) {
            c2.setText(arrayList.get(i).b());
            RecyclerView b2 = aVar.b();
            b2.setLayoutManager(new LinearLayoutManager(b2.getContext()));
            kq2 kq2 = this.a.get(i);
            kd4.a((Object) kq2, "listDebugChildAdapter[position]");
            kq2 kq22 = kq2;
            kq22.a(i);
            b2.setAdapter(kq22);
            ArrayList<pq2> arrayList2 = this.b;
            if (arrayList2 == null) {
                kd4.a();
                throw null;
            } else if (arrayList2.get(i).c()) {
                aVar.a().setImageResource(R.drawable.ic_arrow_drop_up);
                aVar.b().setVisibility(0);
            } else {
                aVar.a().setImageResource(R.drawable.ic_arrow_drop_down);
                aVar.b().setVisibility(8);
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "itemClickListener");
        this.c = bVar;
    }
}
