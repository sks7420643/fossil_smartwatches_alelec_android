package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ei2 q;
    @DexIgnore
    public /* final */ ei2 r;
    @DexIgnore
    public /* final */ LinearLayout s;
    @DexIgnore
    public /* final */ LinearLayout t;
    @DexIgnore
    public /* final */ TodayHeartRateChart u;

    @DexIgnore
    public kc2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ei2 ei2, ei2 ei22, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, TodayHeartRateChart todayHeartRateChart) {
        super(obj, view, i);
        this.q = ei2;
        a((ViewDataBinding) this.q);
        this.r = ei22;
        a((ViewDataBinding) this.r);
        this.s = linearLayout;
        this.t = linearLayout2;
        this.u = todayHeartRateChart;
    }
}
