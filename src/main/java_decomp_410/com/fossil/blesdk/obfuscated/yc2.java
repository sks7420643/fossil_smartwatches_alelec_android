package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ wi2 A;
    @DexIgnore
    public /* final */ wi2 B;
    @DexIgnore
    public /* final */ wi2 C;
    @DexIgnore
    public /* final */ ImageView D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ NestedScrollView F;
    @DexIgnore
    public /* final */ ProgressBar G;
    @DexIgnore
    public /* final */ RingProgressBar H;
    @DexIgnore
    public /* final */ RingProgressBar I;
    @DexIgnore
    public /* final */ RingProgressBar J;
    @DexIgnore
    public /* final */ RingProgressBar K;
    @DexIgnore
    public /* final */ RecyclerViewPager L;
    @DexIgnore
    public /* final */ CustomSwipeRefreshLayout M;
    @DexIgnore
    public /* final */ ProgressBar N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ View P;
    @DexIgnore
    public /* final */ View Q;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ wi2 x;
    @DexIgnore
    public /* final */ wi2 y;
    @DexIgnore
    public /* final */ wi2 z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yc2(Object obj, View view, int i, AppBarLayout appBarLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ConstraintLayout constraintLayout, CoordinatorLayout coordinatorLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, CollapsingToolbarLayout collapsingToolbarLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, wi2 wi2, wi2 wi22, wi2 wi23, wi2 wi24, wi2 wi25, wi2 wi26, ImageView imageView, ImageView imageView2, NestedScrollView nestedScrollView, ProgressBar progressBar, RingProgressBar ringProgressBar, RingProgressBar ringProgressBar2, RingProgressBar ringProgressBar3, RingProgressBar ringProgressBar4, RecyclerViewPager recyclerViewPager, CustomSwipeRefreshLayout customSwipeRefreshLayout, ProgressBar progressBar2, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout4;
        this.t = constraintLayout5;
        this.u = constraintLayout6;
        this.v = flexibleTextView3;
        this.w = flexibleTextView6;
        this.x = wi2;
        a((ViewDataBinding) this.x);
        this.y = wi22;
        a((ViewDataBinding) this.y);
        this.z = wi23;
        a((ViewDataBinding) this.z);
        this.A = wi24;
        a((ViewDataBinding) this.A);
        this.B = wi25;
        a((ViewDataBinding) this.B);
        this.C = wi26;
        a((ViewDataBinding) this.C);
        this.D = imageView;
        this.E = imageView2;
        this.F = nestedScrollView;
        this.G = progressBar;
        this.H = ringProgressBar;
        this.I = ringProgressBar2;
        this.J = ringProgressBar3;
        this.K = ringProgressBar4;
        this.L = recyclerViewPager;
        this.M = customSwipeRefreshLayout;
        this.N = progressBar2;
        this.O = flexibleTextView8;
        this.P = view2;
        this.Q = view4;
    }
}
