package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.data.music.TrackInfo;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.TransmitDataPhase;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w50 extends TransmitDataPhase {
    @DexIgnore
    public /* final */ TrackInfo P;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ w50(Peripheral peripheral, Phase.a aVar, TrackInfo trackInfo, short s, String str, int i, fd4 fd4) {
        this(peripheral, aVar, trackInfo, r4, str);
        short b = (i & 8) != 0 ? z40.b.b(peripheral.k(), FileType.MUSIC_CONTROL) : s;
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public byte[] F() {
        x20 x20 = x20.c;
        short A = A();
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.MUSIC_CONTROL.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = ua0.y.g();
        }
        return x20.a(A, version, this.P);
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.TRACK_INFO, this.P.toJSONObject());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public w50(Peripheral peripheral, Phase.a aVar, TrackInfo trackInfo, short s, String str) {
        super(peripheral, aVar, PhaseId.SEND_TRACK_INFO, true, s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r9, 32, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(trackInfo, "trackInfo");
        String str2 = str;
        kd4.b(str2, "phaseUuid");
        this.P = trackInfo;
    }
}
