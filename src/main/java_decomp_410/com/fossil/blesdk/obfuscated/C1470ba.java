package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ba */
public final class C1470ba {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f3691a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f3692b;

    @DexIgnore
    /* renamed from: c */
    public static java.lang.reflect.Field f3693c;

    @DexIgnore
    /* renamed from: d */
    public static boolean f3694d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ba$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ba$a */
    public static class C1471a implements android.view.ActionMode.Callback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.ActionMode.Callback f3695a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.widget.TextView f3696b;

        @DexIgnore
        /* renamed from: c */
        public java.lang.Class f3697c;

        @DexIgnore
        /* renamed from: d */
        public java.lang.reflect.Method f3698d;

        @DexIgnore
        /* renamed from: e */
        public boolean f3699e;

        @DexIgnore
        /* renamed from: f */
        public boolean f3700f; // = false;

        @DexIgnore
        public C1471a(android.view.ActionMode.Callback callback, android.widget.TextView textView) {
            this.f3695a = callback;
            this.f3696b = textView;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo9113a(android.view.Menu menu) {
            java.lang.reflect.Method method;
            android.content.Context context = this.f3696b.getContext();
            android.content.pm.PackageManager packageManager = context.getPackageManager();
            if (!this.f3700f) {
                this.f3700f = true;
                try {
                    this.f3697c = java.lang.Class.forName("com.android.internal.view.menu.MenuBuilder");
                    this.f3698d = this.f3697c.getDeclaredMethod("removeItemAt", new java.lang.Class[]{java.lang.Integer.TYPE});
                    this.f3699e = true;
                } catch (java.lang.ClassNotFoundException | java.lang.NoSuchMethodException unused) {
                    this.f3697c = null;
                    this.f3698d = null;
                    this.f3699e = false;
                }
            }
            try {
                if (!this.f3699e || !this.f3697c.isInstance(menu)) {
                    method = menu.getClass().getDeclaredMethod("removeItemAt", new java.lang.Class[]{java.lang.Integer.TYPE});
                } else {
                    method = this.f3698d;
                }
                for (int size = menu.size() - 1; size >= 0; size--) {
                    android.view.MenuItem item = menu.getItem(size);
                    if (item.getIntent() != null && "android.intent.action.PROCESS_TEXT".equals(item.getIntent().getAction())) {
                        method.invoke(menu, new java.lang.Object[]{java.lang.Integer.valueOf(size)});
                    }
                }
                java.util.List<android.content.pm.ResolveInfo> a = mo9112a(context, packageManager);
                for (int i = 0; i < a.size(); i++) {
                    android.content.pm.ResolveInfo resolveInfo = a.get(i);
                    menu.add(0, 0, i + 100, resolveInfo.loadLabel(packageManager)).setIntent(mo9111a(resolveInfo, this.f3696b)).setShowAsAction(1);
                }
            } catch (java.lang.IllegalAccessException | java.lang.NoSuchMethodException | java.lang.reflect.InvocationTargetException unused2) {
            }
        }

        @DexIgnore
        public boolean onActionItemClicked(android.view.ActionMode actionMode, android.view.MenuItem menuItem) {
            return this.f3695a.onActionItemClicked(actionMode, menuItem);
        }

        @DexIgnore
        public boolean onCreateActionMode(android.view.ActionMode actionMode, android.view.Menu menu) {
            return this.f3695a.onCreateActionMode(actionMode, menu);
        }

        @DexIgnore
        public void onDestroyActionMode(android.view.ActionMode actionMode) {
            this.f3695a.onDestroyActionMode(actionMode);
        }

        @DexIgnore
        public boolean onPrepareActionMode(android.view.ActionMode actionMode, android.view.Menu menu) {
            mo9113a(menu);
            return this.f3695a.onPrepareActionMode(actionMode, menu);
        }

        @DexIgnore
        /* renamed from: a */
        public final java.util.List<android.content.pm.ResolveInfo> mo9112a(android.content.Context context, android.content.pm.PackageManager packageManager) {
            java.util.ArrayList arrayList = new java.util.ArrayList();
            if (!(context instanceof android.app.Activity)) {
                return arrayList;
            }
            for (android.content.pm.ResolveInfo next : packageManager.queryIntentActivities(mo9110a(), 0)) {
                if (mo9114a(next, context)) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo9114a(android.content.pm.ResolveInfo resolveInfo, android.content.Context context) {
            if (context.getPackageName().equals(resolveInfo.activityInfo.packageName)) {
                return true;
            }
            if (!resolveInfo.activityInfo.exported) {
                return false;
            }
            java.lang.String str = resolveInfo.activityInfo.permission;
            if (str == null || context.checkSelfPermission(str) == 0) {
                return true;
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public final android.content.Intent mo9111a(android.content.pm.ResolveInfo resolveInfo, android.widget.TextView textView) {
            return mo9110a().putExtra("android.intent.extra.PROCESS_TEXT_READONLY", !mo9115a(textView)).setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo9115a(android.widget.TextView textView) {
            return (textView instanceof android.text.Editable) && textView.onCheckIsTextEditor() && textView.isEnabled();
        }

        @DexIgnore
        /* renamed from: a */
        public final android.content.Intent mo9110a() {
            return new android.content.Intent().setAction("android.intent.action.PROCESS_TEXT").setType("text/plain");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field m4872a(java.lang.String str) {
        java.lang.reflect.Field field = null;
        try {
            field = android.widget.TextView.class.getDeclaredField(str);
            field.setAccessible(true);
            return field;
        } catch (java.lang.NoSuchFieldException unused) {
            android.util.Log.e("TextViewCompat", "Could not retrieve " + str + " field.");
            return field;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m4879b(android.widget.TextView textView, int i) {
        int i2;
        com.fossil.blesdk.obfuscated.C2109j8.m8870a(i);
        android.graphics.Paint.FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        if (android.os.Build.VERSION.SDK_INT < 16 || textView.getIncludeFontPadding()) {
            i2 = fontMetricsInt.bottom;
        } else {
            i2 = fontMetricsInt.descent;
        }
        if (i > java.lang.Math.abs(i2)) {
            textView.setPadding(textView.getPaddingLeft(), textView.getPaddingTop(), textView.getPaddingRight(), i - i2);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static int m4880c(android.widget.TextView textView) {
        return textView.getPaddingBottom() + textView.getPaint().getFontMetricsInt().bottom;
    }

    @DexIgnore
    /* renamed from: d */
    public static int m4882d(android.widget.TextView textView) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return textView.getMaxLines();
        }
        if (!f3694d) {
            f3693c = m4872a("mMaxMode");
            f3694d = true;
        }
        java.lang.reflect.Field field = f3693c;
        if (field == null || m4870a(field, textView) != 1) {
            return -1;
        }
        if (!f3692b) {
            f3691a = m4872a("mMaximum");
            f3692b = true;
        }
        java.lang.reflect.Field field2 = f3691a;
        if (field2 != null) {
            return m4870a(field2, textView);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: e */
    public static android.text.TextDirectionHeuristic m4884e(android.widget.TextView textView) {
        if (textView.getTransformationMethod() instanceof android.text.method.PasswordTransformationMethod) {
            return android.text.TextDirectionHeuristics.LTR;
        }
        boolean z = false;
        if (android.os.Build.VERSION.SDK_INT < 28 || (textView.getInputType() & 15) != 3) {
            if (textView.getLayoutDirection() == 1) {
                z = true;
            }
            switch (textView.getTextDirection()) {
                case 2:
                    return android.text.TextDirectionHeuristics.ANYRTL_LTR;
                case 3:
                    return android.text.TextDirectionHeuristics.LTR;
                case 4:
                    return android.text.TextDirectionHeuristics.RTL;
                case 5:
                    return android.text.TextDirectionHeuristics.LOCALE;
                case 6:
                    return android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR;
                case 7:
                    return android.text.TextDirectionHeuristics.FIRSTSTRONG_RTL;
                default:
                    if (z) {
                        return android.text.TextDirectionHeuristics.FIRSTSTRONG_RTL;
                    }
                    return android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR;
            }
        } else {
            byte directionality = java.lang.Character.getDirectionality(android.icu.text.DecimalFormatSymbols.getInstance(textView.getTextLocale()).getDigitStrings()[0].codePointAt(0));
            if (directionality == 1 || directionality == 2) {
                return android.text.TextDirectionHeuristics.RTL;
            }
            return android.text.TextDirectionHeuristics.LTR;
        }
    }

    @DexIgnore
    /* renamed from: f */
    public static com.fossil.blesdk.obfuscated.C3345y7.C3346a m4885f(android.widget.TextView textView) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return new com.fossil.blesdk.obfuscated.C3345y7.C3346a(textView.getTextMetricsParams());
        }
        com.fossil.blesdk.obfuscated.C3345y7.C3346a.C3347a aVar = new com.fossil.blesdk.obfuscated.C3345y7.C3346a.C3347a(new android.text.TextPaint(textView.getPaint()));
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            aVar.mo18056a(textView.getBreakStrategy());
            aVar.mo18059b(textView.getHyphenationFrequency());
        }
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            aVar.mo18057a(m4884e(textView));
        }
        return aVar.mo18058a();
    }

    @DexIgnore
    /* renamed from: c */
    public static void m4881c(android.widget.TextView textView, int i) {
        com.fossil.blesdk.obfuscated.C2109j8.m8870a(i);
        int fontMetricsInt = textView.getPaint().getFontMetricsInt((android.graphics.Paint.FontMetricsInt) null);
        if (i != fontMetricsInt) {
            textView.setLineSpacing((float) (i - fontMetricsInt), 1.0f);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m4870a(java.lang.reflect.Field field, android.widget.TextView textView) {
        try {
            return field.getInt(textView);
        } catch (java.lang.IllegalAccessException unused) {
            android.util.Log.d("TextViewCompat", "Could not retrieve value of " + field.getName() + " field.");
            return -1;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4874a(android.widget.TextView textView, android.graphics.drawable.Drawable drawable, android.graphics.drawable.Drawable drawable2, android.graphics.drawable.Drawable drawable3, android.graphics.drawable.Drawable drawable4) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 18) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        } else if (i >= 17) {
            boolean z = true;
            if (textView.getLayoutDirection() != 1) {
                z = false;
            }
            android.graphics.drawable.Drawable drawable5 = z ? drawable3 : drawable;
            if (!z) {
                drawable = drawable3;
            }
            textView.setCompoundDrawables(drawable5, drawable2, drawable, drawable4);
        } else {
            textView.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.drawable.Drawable[] m4877a(android.widget.TextView textView) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 18) {
            return textView.getCompoundDrawablesRelative();
        }
        if (i < 17) {
            return textView.getCompoundDrawables();
        }
        boolean z = true;
        if (textView.getLayoutDirection() != 1) {
            z = false;
        }
        android.graphics.drawable.Drawable[] compoundDrawables = textView.getCompoundDrawables();
        if (z) {
            android.graphics.drawable.Drawable drawable = compoundDrawables[2];
            android.graphics.drawable.Drawable drawable2 = compoundDrawables[0];
            compoundDrawables[0] = drawable;
            compoundDrawables[2] = drawable2;
        }
        return compoundDrawables;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m4878b(android.widget.TextView textView) {
        return textView.getPaddingTop() - textView.getPaint().getFontMetricsInt().top;
    }

    @DexIgnore
    /* renamed from: d */
    public static void m4883d(android.widget.TextView textView, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            textView.setTextAppearance(i);
        } else {
            textView.setTextAppearance(textView.getContext(), i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.ActionMode.Callback m4871a(android.widget.TextView textView, android.view.ActionMode.Callback callback) {
        int i = android.os.Build.VERSION.SDK_INT;
        return (i < 26 || i > 27 || (callback instanceof com.fossil.blesdk.obfuscated.C1470ba.C1471a)) ? callback : new com.fossil.blesdk.obfuscated.C1470ba.C1471a(callback, textView);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4873a(android.widget.TextView textView, int i) {
        int i2;
        com.fossil.blesdk.obfuscated.C2109j8.m8870a(i);
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            textView.setFirstBaselineToTopHeight(i);
            return;
        }
        android.graphics.Paint.FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        if (android.os.Build.VERSION.SDK_INT < 16 || textView.getIncludeFontPadding()) {
            i2 = fontMetricsInt.top;
        } else {
            i2 = fontMetricsInt.ascent;
        }
        if (i > java.lang.Math.abs(i2)) {
            textView.setPadding(textView.getPaddingLeft(), i - (-i2), textView.getPaddingRight(), textView.getPaddingBottom());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4875a(android.widget.TextView textView, com.fossil.blesdk.obfuscated.C3345y7.C3346a aVar) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            textView.setTextDirection(m4869a(aVar.mo18051c()));
        }
        if (android.os.Build.VERSION.SDK_INT < 23) {
            float textScaleX = aVar.mo18052d().getTextScaleX();
            textView.getPaint().set(aVar.mo18052d());
            if (textScaleX == textView.getTextScaleX()) {
                textView.setTextScaleX((textScaleX / 2.0f) + 1.0f);
            }
            textView.setTextScaleX(textScaleX);
            return;
        }
        textView.getPaint().set(aVar.mo18052d());
        textView.setBreakStrategy(aVar.mo18048a());
        textView.setHyphenationFrequency(aVar.mo18050b());
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4876a(android.widget.TextView textView, com.fossil.blesdk.obfuscated.C3345y7 y7Var) {
        if (m4885f(textView).mo18049a(y7Var.mo18036a())) {
            textView.setText(y7Var);
            return;
        }
        throw new java.lang.IllegalArgumentException("Given text can not be applied to TextView.");
    }

    @DexIgnore
    /* renamed from: a */
    public static int m4869a(android.text.TextDirectionHeuristic textDirectionHeuristic) {
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.FIRSTSTRONG_RTL || textDirectionHeuristic == android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 1;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.ANYRTL_LTR) {
            return 2;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.LTR) {
            return 3;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.RTL) {
            return 4;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.LOCALE) {
            return 5;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 6;
        }
        if (textDirectionHeuristic == android.text.TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 7;
        }
        return 1;
    }
}
