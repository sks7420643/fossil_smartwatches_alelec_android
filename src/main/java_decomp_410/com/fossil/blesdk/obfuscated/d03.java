package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d03 implements Factory<ArrayList<String>> {
    @DexIgnore
    public static ArrayList<String> a(c03 c03) {
        ArrayList<String> b = c03.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
