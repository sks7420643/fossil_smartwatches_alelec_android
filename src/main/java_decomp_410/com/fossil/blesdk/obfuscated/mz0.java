package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mz0 extends iz0 {
    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }
}
