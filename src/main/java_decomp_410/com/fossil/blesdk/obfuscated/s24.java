package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class s24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ p24 f;
    @DexIgnore
    public /* final */ /* synthetic */ q24 g;

    @DexIgnore
    public s24(q24 q24, List list, p24 p24) {
        this.g = q24;
        this.e = list;
        this.f = p24;
    }

    @DexIgnore
    public void run() {
        this.g.a((List<?>) this.e, this.f);
    }
}
