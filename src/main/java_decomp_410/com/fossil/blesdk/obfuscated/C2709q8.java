package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q8 */
public class C2709q8 {

    @DexIgnore
    /* renamed from: a */
    public static boolean f8561a; // = false;

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Method f8562b; // = null;

    @DexIgnore
    /* renamed from: c */
    public static boolean f8563c; // = false;

    @DexIgnore
    /* renamed from: d */
    public static java.lang.reflect.Field f8564d;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.q8$a */
    public interface C2710a {
        @DexIgnore
        boolean superDispatchKeyEvent(android.view.KeyEvent keyEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12638a(android.view.View view, android.view.KeyEvent keyEvent) {
        return com.fossil.blesdk.obfuscated.C1776f9.m6828b(view, keyEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12639a(com.fossil.blesdk.obfuscated.C2709q8.C2710a aVar, android.view.View view, android.view.Window.Callback callback, android.view.KeyEvent keyEvent) {
        if (aVar == null) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return aVar.superDispatchKeyEvent(keyEvent);
        }
        if (callback instanceof android.app.Activity) {
            return m12636a((android.app.Activity) callback, keyEvent);
        }
        if (callback instanceof android.app.Dialog) {
            return m12637a((android.app.Dialog) callback, keyEvent);
        }
        if ((view == null || !com.fossil.blesdk.obfuscated.C1776f9.m6821a(view, keyEvent)) && !aVar.superDispatchKeyEvent(keyEvent)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12635a(android.app.ActionBar actionBar, android.view.KeyEvent keyEvent) {
        if (!f8561a) {
            try {
                f8562b = actionBar.getClass().getMethod("onMenuKeyEvent", new java.lang.Class[]{android.view.KeyEvent.class});
            } catch (java.lang.NoSuchMethodException unused) {
            }
            f8561a = true;
        }
        java.lang.reflect.Method method = f8562b;
        if (method != null) {
            try {
                return ((java.lang.Boolean) method.invoke(actionBar, new java.lang.Object[]{keyEvent})).booleanValue();
            } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException unused2) {
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12636a(android.app.Activity activity, android.view.KeyEvent keyEvent) {
        activity.onUserInteraction();
        android.view.Window window = activity.getWindow();
        if (window.hasFeature(8)) {
            android.app.ActionBar actionBar = activity.getActionBar();
            if (keyEvent.getKeyCode() == 82 && actionBar != null && m12635a(actionBar, keyEvent)) {
                return true;
            }
        }
        if (window.superDispatchKeyEvent(keyEvent)) {
            return true;
        }
        android.view.View decorView = window.getDecorView();
        if (com.fossil.blesdk.obfuscated.C1776f9.m6821a(decorView, keyEvent)) {
            return true;
        }
        return keyEvent.dispatch(activity, decorView != null ? decorView.getKeyDispatcherState() : null, activity);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.DialogInterface.OnKeyListener m12634a(android.app.Dialog dialog) {
        if (!f8563c) {
            try {
                f8564d = android.app.Dialog.class.getDeclaredField("mOnKeyListener");
                f8564d.setAccessible(true);
            } catch (java.lang.NoSuchFieldException unused) {
            }
            f8563c = true;
        }
        java.lang.reflect.Field field = f8564d;
        if (field == null) {
            return null;
        }
        try {
            return (android.content.DialogInterface.OnKeyListener) field.get(dialog);
        } catch (java.lang.IllegalAccessException unused2) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12637a(android.app.Dialog dialog, android.view.KeyEvent keyEvent) {
        android.content.DialogInterface.OnKeyListener a = m12634a(dialog);
        if (a != null && a.onKey(dialog, keyEvent.getKeyCode(), keyEvent)) {
            return true;
        }
        android.view.Window window = dialog.getWindow();
        if (window.superDispatchKeyEvent(keyEvent)) {
            return true;
        }
        android.view.View decorView = window.getDecorView();
        if (com.fossil.blesdk.obfuscated.C1776f9.m6821a(decorView, keyEvent)) {
            return true;
        }
        return keyEvent.dispatch(dialog, decorView != null ? decorView.getKeyDispatcherState() : null, dialog);
    }
}
