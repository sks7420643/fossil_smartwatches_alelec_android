package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.iu */
public class C2075iu<Z> implements com.fossil.blesdk.obfuscated.C1906gu<Z, Z> {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2075iu<?> f6212a; // = new com.fossil.blesdk.obfuscated.C2075iu<>();

    @DexIgnore
    /* renamed from: a */
    public static <Z> com.fossil.blesdk.obfuscated.C1906gu<Z, Z> m8653a() {
        return f6212a;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<Z> mo9632a(com.fossil.blesdk.obfuscated.C1438aq<Z> aqVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return aqVar;
    }
}
