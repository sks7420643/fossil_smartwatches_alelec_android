package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hr2 {
    @DexIgnore
    public static /* final */ String c; // = "hr2";
    @DexIgnore
    public /* final */ NotificationsRepository a;
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public hr2(NotificationsRepository notificationsRepository) {
        st1.a(notificationsRepository);
        this.a = notificationsRepository;
    }

    @DexIgnore
    public void a(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public void a(String str, String str2, List<String> list, String str3) {
        FLogger.INSTANCE.getLocal().d(c, "start update contact");
        boolean z = false;
        for (Contact next : a()) {
            if (next.getContactId() == Integer.parseInt(str)) {
                next.setFirstName(str2);
                next.setPhotoThumbUri(str3);
                a(this.a, next, list);
                z = true;
            }
        }
        if (z) {
            a aVar = this.b;
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    @DexIgnore
    public final List<Contact> a() {
        List<ContactGroup> allContactGroups = this.a.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        List<ContactGroup> allContactGroups2 = this.a.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        ArrayList arrayList = new ArrayList();
        if (allContactGroups != null) {
            for (ContactGroup contacts : allContactGroups) {
                arrayList.addAll(contacts.getContacts());
            }
        }
        if (allContactGroups2 != null) {
            for (ContactGroup contacts2 : allContactGroups2) {
                arrayList.addAll(contacts2.getContacts());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void a(NotificationsRepository notificationsRepository, Contact contact, List<String> list) {
        notificationsRepository.saveContact(contact);
        a(contact, notificationsRepository, list);
    }

    @DexIgnore
    public final void a(Contact contact, NotificationsRepository notificationsRepository, List<String> list) {
        notificationsRepository.removePhoneNumberByContactGroupId(contact.getContactGroup().getDbRowId());
        if (list != null) {
            for (String number : list) {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setNumber(number);
                phoneNumber.setContact(contact);
                notificationsRepository.savePhoneNumber(phoneNumber);
            }
        }
    }
}
