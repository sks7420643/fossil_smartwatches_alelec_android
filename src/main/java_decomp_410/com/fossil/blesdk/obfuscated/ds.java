package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ds implements sr<lr, InputStream> {
    @DexIgnore
    public static /* final */ ko<Integer> b; // = ko.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    @DexIgnore
    public /* final */ rr<lr, lr> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<lr, InputStream> {
        @DexIgnore
        public /* final */ rr<lr, lr> a; // = new rr<>(500);

        @DexIgnore
        public sr<lr, InputStream> a(wr wrVar) {
            return new ds(this.a);
        }
    }

    @DexIgnore
    public ds(rr<lr, lr> rrVar) {
        this.a = rrVar;
    }

    @DexIgnore
    public boolean a(lr lrVar) {
        return true;
    }

    @DexIgnore
    public sr.a<InputStream> a(lr lrVar, int i, int i2, lo loVar) {
        rr<lr, lr> rrVar = this.a;
        if (rrVar != null) {
            lr a2 = rrVar.a(lrVar, 0, 0);
            if (a2 == null) {
                this.a.a(lrVar, 0, 0, lrVar);
            } else {
                lrVar = a2;
            }
        }
        return new sr.a<>(lrVar, new yo(lrVar, ((Integer) loVar.a(b)).intValue()));
    }
}
