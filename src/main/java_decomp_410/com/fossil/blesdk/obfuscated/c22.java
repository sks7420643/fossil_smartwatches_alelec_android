package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.enums.Action;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c22 {
    @DexIgnore
    public static /* final */ c22 g; // = new c22(4201, 4096, 1);
    @DexIgnore
    public static /* final */ c22 h; // = new c22(1033, 1024, 1);
    @DexIgnore
    public static /* final */ c22 i; // = new c22(67, 64, 1);
    @DexIgnore
    public static /* final */ c22 j; // = new c22(19, 16, 1);
    @DexIgnore
    public static /* final */ c22 k; // = new c22(285, 256, 0);
    @DexIgnore
    public static /* final */ c22 l; // = new c22(Action.Presenter.NEXT, 256, 1);
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public /* final */ d22 c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public c22(int i2, int i3, int i4) {
        this.e = i2;
        this.d = i3;
        this.f = i4;
        this.a = new int[i3];
        this.b = new int[i3];
        int i5 = 1;
        for (int i6 = 0; i6 < i3; i6++) {
            this.a[i6] = i5;
            i5 <<= 1;
            if (i5 >= i3) {
                i5 = (i5 ^ i2) & (i3 - 1);
            }
        }
        for (int i7 = 0; i7 < i3 - 1; i7++) {
            this.b[this.a[i7]] = i7;
        }
        this.c = new d22(this, new int[]{0});
        new d22(this, new int[]{1});
    }

    @DexIgnore
    public static int c(int i2, int i3) {
        return i2 ^ i3;
    }

    @DexIgnore
    public d22 a(int i2, int i3) {
        if (i2 < 0) {
            throw new IllegalArgumentException();
        } else if (i3 == 0) {
            return this.c;
        } else {
            int[] iArr = new int[(i2 + 1)];
            iArr[0] = i3;
            return new d22(this, iArr);
        }
    }

    @DexIgnore
    public d22 b() {
        return this.c;
    }

    @DexIgnore
    public int c(int i2) {
        if (i2 != 0) {
            return this.b[i2];
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public String toString() {
        return "GF(0x" + Integer.toHexString(this.e) + ',' + this.d + ')';
    }

    @DexIgnore
    public int b(int i2) {
        if (i2 != 0) {
            return this.a[(this.d - this.b[i2]) - 1];
        }
        throw new ArithmeticException();
    }

    @DexIgnore
    public int b(int i2, int i3) {
        if (i2 == 0 || i3 == 0) {
            return 0;
        }
        int[] iArr = this.a;
        int[] iArr2 = this.b;
        return iArr[(iArr2[i2] + iArr2[i3]) % (this.d - 1)];
    }

    @DexIgnore
    public int a(int i2) {
        return this.a[i2];
    }

    @DexIgnore
    public int a() {
        return this.f;
    }
}
