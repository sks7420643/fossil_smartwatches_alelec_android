package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e9 */
public interface C1708e9 {
    @DexIgnore
    android.content.res.ColorStateList getSupportBackgroundTintList();

    @DexIgnore
    android.graphics.PorterDuff.Mode getSupportBackgroundTintMode();

    @DexIgnore
    void setSupportBackgroundTintList(android.content.res.ColorStateList colorStateList);

    @DexIgnore
    void setSupportBackgroundTintMode(android.graphics.PorterDuff.Mode mode);
}
