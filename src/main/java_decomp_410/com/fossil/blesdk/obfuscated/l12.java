package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l12 {
    @DexIgnore
    public abstract l12 a(j12 j12) throws IOException;

    @DexIgnore
    public String toString() {
        return m12.a(this);
    }

    @DexIgnore
    public l12 clone() throws CloneNotSupportedException {
        return (l12) super.clone();
    }
}
