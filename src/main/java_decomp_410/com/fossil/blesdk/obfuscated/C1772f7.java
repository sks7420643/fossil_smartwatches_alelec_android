package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f7 */
public class C1772f7 extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable.Callback, com.fossil.blesdk.obfuscated.C1705e7, com.fossil.blesdk.obfuscated.C1603d7 {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ android.graphics.PorterDuff.Mode f5076k; // = android.graphics.PorterDuff.Mode.SRC_IN;

    @DexIgnore
    /* renamed from: e */
    public int f5077e;

    @DexIgnore
    /* renamed from: f */
    public android.graphics.PorterDuff.Mode f5078f;

    @DexIgnore
    /* renamed from: g */
    public boolean f5079g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C1772f7.C1773a f5080h;

    @DexIgnore
    /* renamed from: i */
    public boolean f5081i;

    @DexIgnore
    /* renamed from: j */
    public android.graphics.drawable.Drawable f5082j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.f7$a */
    public static abstract class C1773a extends android.graphics.drawable.Drawable.ConstantState {

        @DexIgnore
        /* renamed from: a */
        public int f5083a;

        @DexIgnore
        /* renamed from: b */
        public android.graphics.drawable.Drawable.ConstantState f5084b;

        @DexIgnore
        /* renamed from: c */
        public android.content.res.ColorStateList f5085c; // = null;

        @DexIgnore
        /* renamed from: d */
        public android.graphics.PorterDuff.Mode f5086d; // = com.fossil.blesdk.obfuscated.C1772f7.f5076k;

        @DexIgnore
        public C1773a(com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar, android.content.res.Resources resources) {
            if (aVar != null) {
                this.f5083a = aVar.f5083a;
                this.f5084b = aVar.f5084b;
                this.f5085c = aVar.f5085c;
                this.f5086d = aVar.f5086d;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10777a() {
            return this.f5084b != null;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            int i = this.f5083a;
            android.graphics.drawable.Drawable.ConstantState constantState = this.f5084b;
            return i | (constantState != null ? constantState.getChangingConfigurations() : 0);
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            return newDrawable((android.content.res.Resources) null);
        }

        @DexIgnore
        public abstract android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.f7$b */
    public static class C1774b extends com.fossil.blesdk.obfuscated.C1772f7.C1773a {
        @DexIgnore
        public C1774b(com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar, android.content.res.Resources resources) {
            super(aVar, resources);
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return new com.fossil.blesdk.obfuscated.C1772f7(this, resources);
        }
    }

    @DexIgnore
    public C1772f7(com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar, android.content.res.Resources resources) {
        this.f5080h = aVar;
        mo10744a(resources);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10744a(android.content.res.Resources resources) {
        com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
        if (aVar != null) {
            android.graphics.drawable.Drawable.ConstantState constantState = aVar.f5084b;
            if (constantState != null) {
                mo10344a(constantState.newDrawable(resources));
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo10746b() {
        return true;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1772f7.C1773a mo10747c() {
        return new com.fossil.blesdk.obfuscated.C1772f7.C1774b(this.f5080h, (android.content.res.Resources) null);
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        this.f5082j.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
        return changingConfigurations | (aVar != null ? aVar.getChangingConfigurations() : 0) | this.f5082j.getChangingConfigurations();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable.ConstantState getConstantState() {
        com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
        if (aVar == null || !aVar.mo10777a()) {
            return null;
        }
        this.f5080h.f5083a = getChangingConfigurations();
        return this.f5080h;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getCurrent() {
        return this.f5082j.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f5082j.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.f5082j.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.f5082j.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.f5082j.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.f5082j.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(android.graphics.Rect rect) {
        return this.f5082j.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.f5082j.getState();
    }

    @DexIgnore
    public android.graphics.Region getTransparentRegion() {
        return this.f5082j.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.f5082j.isAutoMirrored();
    }

    @DexIgnore
    public boolean isStateful() {
        android.content.res.ColorStateList colorStateList;
        if (mo10746b()) {
            com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
            if (aVar != null) {
                colorStateList = aVar.f5085c;
                return (colorStateList == null && colorStateList.isStateful()) || this.f5082j.isStateful();
            }
        }
        colorStateList = null;
        if (colorStateList == null) {
        }
    }

    @DexIgnore
    public void jumpToCurrentState() {
        this.f5082j.jumpToCurrentState();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        if (!this.f5081i && super.mutate() == this) {
            this.f5080h = mo10747c();
            android.graphics.drawable.Drawable drawable = this.f5082j;
            if (drawable != null) {
                drawable.mutate();
            }
            com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
            if (aVar != null) {
                android.graphics.drawable.Drawable drawable2 = this.f5082j;
                aVar.f5084b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.f5081i = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        android.graphics.drawable.Drawable drawable = this.f5082j;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.f5082j.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f5082j.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        this.f5082j.setAutoMirrored(z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.f5082j.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        this.f5082j.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.f5082j.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.f5082j.setFilterBitmap(z);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return mo10745a(iArr) || this.f5082j.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        setTintList(android.content.res.ColorStateList.valueOf(i));
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        this.f5080h.f5085c = colorStateList;
        mo10745a(getState());
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        this.f5080h.f5086d = mode;
        mo10745a(getState());
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f5082j.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo10745a(int[] iArr) {
        if (!mo10746b()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
        android.content.res.ColorStateList colorStateList = aVar.f5085c;
        android.graphics.PorterDuff.Mode mode = aVar.f5086d;
        if (colorStateList == null || mode == null) {
            this.f5079g = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.f5079g && colorForState == this.f5077e && mode == this.f5078f)) {
                setColorFilter(colorForState, mode);
                this.f5077e = colorForState;
                this.f5078f = mode;
                this.f5079g = true;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public C1772f7(android.graphics.drawable.Drawable drawable) {
        this.f5080h = mo10747c();
        mo10344a(drawable);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo10343a() {
        return this.f5082j;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10344a(android.graphics.drawable.Drawable drawable) {
        android.graphics.drawable.Drawable drawable2 = this.f5082j;
        if (drawable2 != null) {
            drawable2.setCallback((android.graphics.drawable.Drawable.Callback) null);
        }
        this.f5082j = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar = this.f5080h;
            if (aVar != null) {
                aVar.f5084b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }
}
