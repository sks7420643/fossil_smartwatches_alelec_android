package com.fossil.blesdk.obfuscated;

import java.util.Set;
import kotlin.collections.EmptySet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vb4 extends ub4 {
    @DexIgnore
    public static final <T> Set<T> a() {
        return EmptySet.INSTANCE;
    }

    @DexIgnore
    public static final <T> Set<T> a(Set<? extends T> set) {
        kd4.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return set;
        }
        return ub4.a(set.iterator().next());
    }
}
