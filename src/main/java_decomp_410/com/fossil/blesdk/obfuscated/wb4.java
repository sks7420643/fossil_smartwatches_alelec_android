package com.fossil.blesdk.obfuscated;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wb4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ xc4[] e;

        @DexIgnore
        public a(xc4[] xc4Arr) {
            this.e = xc4Arr;
        }

        @DexIgnore
        public final int compare(T t, T t2) {
            return wb4.b(t, t2, this.e);
        }
    }

    @DexIgnore
    public static final <T> int b(T t, T t2, xc4<? super T, ? extends Comparable<?>>[] xc4Arr) {
        for (xc4<? super T, ? extends Comparable<?>> xc4 : xc4Arr) {
            int a2 = a((Comparable) xc4.invoke(t), (Comparable) xc4.invoke(t2));
            if (a2 != 0) {
                return a2;
            }
        }
        return 0;
    }

    @DexIgnore
    public static final <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    @DexIgnore
    public static final <T> Comparator<T> a(xc4<? super T, ? extends Comparable<?>>... xc4Arr) {
        kd4.b(xc4Arr, "selectors");
        if (xc4Arr.length > 0) {
            return new a(xc4Arr);
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
}
