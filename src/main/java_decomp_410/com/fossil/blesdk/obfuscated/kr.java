package com.fossil.blesdk.obfuscated;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kr<Data> implements sr<File, Data> {
    @DexIgnore
    public /* final */ d<Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements tr<File, Data> {
        @DexIgnore
        public /* final */ d<Data> a;

        @DexIgnore
        public a(d<Data> dVar) {
            this.a = dVar;
        }

        @DexIgnore
        public final sr<File, Data> a(wr wrVar) {
            return new kr(this.a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a<ParcelFileDescriptor> {
        @DexIgnore
        public b() {
            super(new a());
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<ParcelFileDescriptor> {
            @DexIgnore
            public Class<ParcelFileDescriptor> getDataClass() {
                return ParcelFileDescriptor.class;
            }

            @DexIgnore
            public ParcelFileDescriptor a(File file) throws FileNotFoundException {
                return ParcelFileDescriptor.open(file, 268435456);
            }

            @DexIgnore
            public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
                parcelFileDescriptor.close();
            }
        }
    }

    @DexIgnore
    public interface d<Data> {
        @DexIgnore
        Data a(File file) throws FileNotFoundException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a<InputStream> {
        @DexIgnore
        public e() {
            super(new a());
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements d<InputStream> {
            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(File file) throws FileNotFoundException {
                return new FileInputStream(file);
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public kr(d<Data> dVar) {
        this.a = dVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public sr.a<Data> a(File file, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(file), new c(file, this.a));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Data> implements so<Data> {
        @DexIgnore
        public /* final */ File e;
        @DexIgnore
        public /* final */ d<Data> f;
        @DexIgnore
        public Data g;

        @DexIgnore
        public c(File file, d<Data> dVar) {
            this.e = file;
            this.f = dVar;
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super Data> aVar) {
            try {
                this.g = this.f.a(this.e);
                aVar.a(this.g);
            } catch (FileNotFoundException e2) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e2);
                }
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.f.getDataClass();
        }

        @DexIgnore
        public void a() {
            Data data = this.g;
            if (data != null) {
                try {
                    this.f.a(data);
                } catch (IOException unused) {
                }
            }
        }
    }
}
