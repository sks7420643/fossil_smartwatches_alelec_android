package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xf4 extends th4 {
    @DexIgnore
    public /* final */ Thread j;

    @DexIgnore
    public xf4(Thread thread) {
        kd4.b(thread, "thread");
        this.j = thread;
    }

    @DexIgnore
    public Thread I() {
        return this.j;
    }
}
