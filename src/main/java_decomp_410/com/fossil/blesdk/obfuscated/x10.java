package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x10 extends GattOperationResult {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId b;
    @DexIgnore
    public /* final */ byte[] c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x10(GattOperationResult.GattResult gattResult, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        this.b = characteristicId;
        this.c = bArr;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId b() {
        return this.b;
    }

    @DexIgnore
    public final byte[] c() {
        return this.c;
    }
}
