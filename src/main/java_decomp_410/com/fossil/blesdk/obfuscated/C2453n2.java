package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n2 */
public abstract class C2453n2 implements android.view.View.OnTouchListener, android.view.View.OnAttachStateChangeListener {

    @DexIgnore
    /* renamed from: e */
    public /* final */ float f7627e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f7628f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f7629g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.view.View f7630h;

    @DexIgnore
    /* renamed from: i */
    public java.lang.Runnable f7631i;

    @DexIgnore
    /* renamed from: j */
    public java.lang.Runnable f7632j;

    @DexIgnore
    /* renamed from: k */
    public boolean f7633k;

    @DexIgnore
    /* renamed from: l */
    public int f7634l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ int[] f7635m; // = new int[2];

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.n2$a */
    public class C2454a implements java.lang.Runnable {
        @DexIgnore
        public C2454a() {
        }

        @DexIgnore
        public void run() {
            android.view.ViewParent parent = com.fossil.blesdk.obfuscated.C2453n2.this.f7630h.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n2$b")
    /* renamed from: com.fossil.blesdk.obfuscated.n2$b */
    public class C2455b implements java.lang.Runnable {
        @DexIgnore
        public C2455b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2453n2.this.mo13798e();
        }
    }

    @DexIgnore
    public C2453n2(android.view.View view) {
        this.f7630h = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.f7627e = (float) android.view.ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f7628f = android.view.ViewConfiguration.getTapTimeout();
        this.f7629g = (this.f7628f + android.view.ViewConfiguration.getLongPressTimeout()) / 2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13793a() {
        java.lang.Runnable runnable = this.f7632j;
        if (runnable != null) {
            this.f7630h.removeCallbacks(runnable);
        }
        java.lang.Runnable runnable2 = this.f7631i;
        if (runnable2 != null) {
            this.f7630h.removeCallbacks(runnable2);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public abstract com.fossil.blesdk.obfuscated.C2936t1 mo371b();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r1 != 3) goto L_0x006d;
     */
    @DexIgnore
    /* renamed from: b */
    public final boolean mo13796b(android.view.MotionEvent motionEvent) {
        android.view.View view = this.f7630h;
        if (!view.isEnabled()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.f7634l);
                    if (findPointerIndex >= 0 && !m10987a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.f7627e)) {
                        mo13793a();
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        return true;
                    }
                }
            }
            mo13793a();
        } else {
            this.f7634l = motionEvent.getPointerId(0);
            if (this.f7631i == null) {
                this.f7631i = new com.fossil.blesdk.obfuscated.C2453n2.C2454a();
            }
            view.postDelayed(this.f7631i, (long) this.f7628f);
            if (this.f7632j == null) {
                this.f7632j = new com.fossil.blesdk.obfuscated.C2453n2.C2455b();
            }
            view.postDelayed(this.f7632j, (long) this.f7629g);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public abstract boolean mo372c();

    @DexIgnore
    /* renamed from: d */
    public boolean mo560d() {
        com.fossil.blesdk.obfuscated.C2936t1 b = mo371b();
        if (b == null || !b.mo862d()) {
            return true;
        }
        b.dismiss();
        return true;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo13798e() {
        mo13793a();
        android.view.View view = this.f7630h;
        if (view.isEnabled() && !view.isLongClickable() && mo372c()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = android.os.SystemClock.uptimeMillis();
            android.view.MotionEvent obtain = android.view.MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.f7633k = true;
        }
    }

    @DexIgnore
    public boolean onTouch(android.view.View view, android.view.MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.f7633k;
        if (z2) {
            z = mo13794a(motionEvent) || !mo560d();
        } else {
            z = mo13796b(motionEvent) && mo372c();
            if (z) {
                long uptimeMillis = android.os.SystemClock.uptimeMillis();
                android.view.MotionEvent obtain = android.view.MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
                this.f7630h.onTouchEvent(obtain);
                obtain.recycle();
            }
        }
        this.f7633k = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void onViewAttachedToWindow(android.view.View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(android.view.View view) {
        this.f7633k = false;
        this.f7634l = -1;
        java.lang.Runnable runnable = this.f7631i;
        if (runnable != null) {
            this.f7630h.removeCallbacks(runnable);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo13794a(android.view.MotionEvent motionEvent) {
        android.view.View view = this.f7630h;
        com.fossil.blesdk.obfuscated.C2936t1 b = mo371b();
        if (b != null && b.mo862d()) {
            com.fossil.blesdk.obfuscated.C2279l2 l2Var = (com.fossil.blesdk.obfuscated.C2279l2) b.mo864e();
            if (l2Var != null && l2Var.isShown()) {
                android.view.MotionEvent obtainNoHistory = android.view.MotionEvent.obtainNoHistory(motionEvent);
                mo13795a(view, obtainNoHistory);
                mo13797b(l2Var, obtainNoHistory);
                boolean a = l2Var.mo13089a(obtainNoHistory, this.f7634l);
                obtainNoHistory.recycle();
                int actionMasked = motionEvent.getActionMasked();
                return a && (actionMasked != 1 && actionMasked != 3);
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m10987a(android.view.View view, float f, float f2, float f3) {
        float f4 = -f3;
        return f >= f4 && f2 >= f4 && f < ((float) (view.getRight() - view.getLeft())) + f3 && f2 < ((float) (view.getBottom() - view.getTop())) + f3;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo13795a(android.view.View view, android.view.MotionEvent motionEvent) {
        int[] iArr = this.f7635m;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo13797b(android.view.View view, android.view.MotionEvent motionEvent) {
        int[] iArr = this.f7635m;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }
}
