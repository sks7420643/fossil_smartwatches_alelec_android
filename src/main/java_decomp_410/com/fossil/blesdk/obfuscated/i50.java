package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.enumerate.Priority;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i50 extends j50 {
    @DexIgnore
    public Priority S; // = Priority.LOW;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i50(Peripheral peripheral, Phase.a aVar, HashMap<GetFilePhase.GetFileOption, Object> hashMap) {
        super(peripheral, aVar, PhaseId.GET_HARDWARE_LOG_IN_BACKGROUND, hashMap, (String) null, 16, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(hashMap, "options");
    }

    @DexIgnore
    public void a(Priority priority) {
        kd4.b(priority, "<set-?>");
        this.S = priority;
    }

    @DexIgnore
    public Priority m() {
        return this.S;
    }
}
