package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public interface nc1 {
    @DexIgnore
    he0<Status> a(ge0 ge0, rc1 rc1);

    @DexIgnore
    he0<Status> a(ge0 ge0, LocationRequest locationRequest, rc1 rc1);
}
