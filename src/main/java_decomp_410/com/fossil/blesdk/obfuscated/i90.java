package com.fossil.blesdk.obfuscated;

import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i90 {
    @DexIgnore
    public static final JSONArray a(String[] strArr) {
        kd4.b(strArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (String put : strArr) {
            jSONArray.put(put);
        }
        return jSONArray;
    }
}
