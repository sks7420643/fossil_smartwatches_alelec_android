package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class us extends qs {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(jo.a);

    @DexIgnore
    public Bitmap a(jq jqVar, Bitmap bitmap, int i, int i2) {
        return gt.b(jqVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof us;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterInside".hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
