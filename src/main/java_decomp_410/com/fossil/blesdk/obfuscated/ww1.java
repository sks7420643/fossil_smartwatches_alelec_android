package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ww1 implements ez1 {
    @DexIgnore
    public /* final */ kw1 a;
    @DexIgnore
    public /* final */ jw1 b;

    @DexIgnore
    public ww1(kw1 kw1, jw1 jw1) {
        this.a = kw1;
        this.b = jw1;
    }

    @DexIgnore
    public static ez1 a(kw1 kw1, jw1 jw1) {
        return new ww1(kw1, jw1);
    }

    @DexIgnore
    public final Object get() {
        return this.a.a(this.b);
    }
}
