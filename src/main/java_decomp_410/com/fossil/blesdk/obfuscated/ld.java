package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.pd;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ld<Key, Value> {
    @DexIgnore
    public AtomicBoolean mInvalid; // = new AtomicBoolean(false);
    @DexIgnore
    public CopyOnWriteArrayList<c> mOnInvalidatedCallbacks; // = new CopyOnWriteArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements m3<List<X>, List<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ m3 a;

        @DexIgnore
        public a(m3 m3Var) {
            this.a = m3Var;
        }

        @DexIgnore
        /* renamed from: a */
        public List<Y> apply(List<X> list) {
            ArrayList arrayList = new ArrayList(list.size());
            for (int i = 0; i < list.size(); i++) {
                arrayList.add(this.a.apply(list.get(i)));
            }
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<Key, Value> {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends b<Key, ToValue> {
            @DexIgnore
            public /* final */ /* synthetic */ m3 a;

            @DexIgnore
            public a(m3 m3Var) {
                this.a = m3Var;
            }

            @DexIgnore
            public ld<Key, ToValue> create() {
                return b.this.create().mapByPage(this.a);
            }
        }

        @DexIgnore
        public abstract ld<Key, Value> create();

        @DexIgnore
        public <ToValue> b<Key, ToValue> map(m3<Value, ToValue> m3Var) {
            return mapByPage(ld.createListFunction(m3Var));
        }

        @DexIgnore
        public <ToValue> b<Key, ToValue> mapByPage(m3<List<Value>, List<ToValue>> m3Var) {
            return new a(m3Var);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public static <A, B> List<B> convert(m3<List<A>, List<B>> m3Var, List<A> list) {
        List<B> apply = m3Var.apply(list);
        if (apply.size() == list.size()) {
            return apply;
        }
        throw new IllegalStateException("Invalid Function " + m3Var + " changed return size. This is not supported.");
    }

    @DexIgnore
    public static <X, Y> m3<List<X>, List<Y>> createListFunction(m3<X, Y> m3Var) {
        return new a(m3Var);
    }

    @DexIgnore
    public void addInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.add(cVar);
    }

    @DexIgnore
    public void invalidate() {
        if (this.mInvalid.compareAndSet(false, true)) {
            Iterator<c> it = this.mOnInvalidatedCallbacks.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    @DexIgnore
    public abstract boolean isContiguous();

    @DexIgnore
    public boolean isInvalid() {
        return this.mInvalid.get();
    }

    @DexIgnore
    public abstract <ToValue> ld<Key, ToValue> map(m3<Value, ToValue> m3Var);

    @DexIgnore
    public abstract <ToValue> ld<Key, ToValue> mapByPage(m3<List<Value>, List<ToValue>> m3Var);

    @DexIgnore
    public void removeInvalidatedCallback(c cVar) {
        this.mOnInvalidatedCallbacks.remove(cVar);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ld b;
        @DexIgnore
        public /* final */ pd.a<T> c;
        @DexIgnore
        public /* final */ Object d; // = new Object();
        @DexIgnore
        public Executor e; // = null;
        @DexIgnore
        public boolean f; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ pd e;

            @DexIgnore
            public a(pd pdVar) {
                this.e = pdVar;
            }

            @DexIgnore
            public void run() {
                d dVar = d.this;
                dVar.c.a(dVar.a, this.e);
            }
        }

        @DexIgnore
        public d(ld ldVar, int i, Executor executor, pd.a<T> aVar) {
            this.b = ldVar;
            this.a = i;
            this.e = executor;
            this.c = aVar;
        }

        @DexIgnore
        public static void a(List<?> list, int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Position must be non-negative");
            } else if (list.size() + i > i2) {
                throw new IllegalArgumentException("List size + position too large, last item in list beyond totalCount.");
            } else if (list.size() == 0 && i2 > 0) {
                throw new IllegalArgumentException("Initial result cannot be empty if items are present in data set.");
            }
        }

        @DexIgnore
        public void a(Executor executor) {
            synchronized (this.d) {
                this.e = executor;
            }
        }

        @DexIgnore
        public boolean a() {
            if (!this.b.isInvalid()) {
                return false;
            }
            a(pd.c());
            return true;
        }

        @DexIgnore
        public void a(pd<T> pdVar) {
            Executor executor;
            synchronized (this.d) {
                if (!this.f) {
                    this.f = true;
                    executor = this.e;
                } else {
                    throw new IllegalStateException("callback.onResult already called, cannot call again.");
                }
            }
            if (executor != null) {
                executor.execute(new a(pdVar));
            } else {
                this.c.a(this.a, pdVar);
            }
        }
    }
}
