package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sh */
public class C2878sh extends com.fossil.blesdk.obfuscated.C3363yh implements com.fossil.blesdk.obfuscated.C3038uh {
    @DexIgnore
    public C2878sh(android.content.Context context, android.view.ViewGroup viewGroup, android.view.View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2878sh m13714a(android.view.ViewGroup viewGroup) {
        return (com.fossil.blesdk.obfuscated.C2878sh) com.fossil.blesdk.obfuscated.C3363yh.m16850c(viewGroup);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15988b(android.view.View view) {
        this.f11248a.mo18091b(view);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15987a(android.view.View view) {
        this.f11248a.mo18087a(view);
    }
}
