package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class be1 {
    @DexIgnore
    public /* final */ je1 a;
    @DexIgnore
    public he1 b;

    @DexIgnore
    public be1(je1 je1) {
        bk0.a(je1);
        this.a = je1;
    }

    @DexIgnore
    public final void a(zd1 zd1) {
        try {
            this.a.b(zd1.a());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final void b(zd1 zd1) {
        try {
            this.a.a(zd1.a());
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final jf1 a(kf1 kf1) {
        try {
            g51 a2 = this.a.a(kf1);
            if (a2 != null) {
                return new jf1(a2);
            }
            return null;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final he1 b() {
        try {
            if (this.b == null) {
                this.b = new he1(this.a.m());
            }
            return this.b;
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final void a() {
        try {
            this.a.clear();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
