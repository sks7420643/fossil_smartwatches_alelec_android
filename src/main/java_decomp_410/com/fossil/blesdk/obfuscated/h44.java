package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class h44 extends AppCompatActivity implements f44, j44 {
    @DexIgnore
    public void onCreate(Bundle bundle) {
        t34.a((Activity) this);
        super.onCreate(bundle);
    }
}
