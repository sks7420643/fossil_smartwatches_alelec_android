package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ls2;
import com.fossil.blesdk.obfuscated.q62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.enums.DirectionBy;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p23 extends zr2 implements u23, q62.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<ca2> j;
    @DexIgnore
    public t23 k;
    @DexIgnore
    public ls2 l;
    @DexIgnore
    public q62 m;
    @DexIgnore
    public String n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final p23 a() {
            return new p23();
        }

        @DexIgnore
        public final String b() {
            return p23.p;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ls2.b {
        @DexIgnore
        public /* final */ /* synthetic */ p23 a;
        @DexIgnore
        public /* final */ /* synthetic */ ca2 b;

        @DexIgnore
        public b(p23 p23, ca2 ca2) {
            this.a = p23;
            this.b = ca2;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "address");
            this.b.q.setText(str, false);
            this.a.U(str);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public c(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public d(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public e(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public f(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).m();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ ca2 f;

        @DexIgnore
        public g(View view, ca2 ca2) {
            this.e = view;
            this.f = ca2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.y.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ca2 ca2 = this.f;
                kd4.a((Object) ca2, "binding");
                ca2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = p23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;
        @DexIgnore
        public /* final */ /* synthetic */ ca2 f;

        @DexIgnore
        public h(p23 p23, ca2 ca2) {
            this.e = p23;
            this.f = ca2;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            q62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    kd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = p23.q.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    String spannableString = fullText.toString();
                    kd4.a((Object) spannableString, "primaryText.toString()");
                    if (!TextUtils.isEmpty(spannableString)) {
                        this.e.U(spannableString);
                        this.f.q.setText(spannableString, false);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;
        @DexIgnore
        public /* final */ /* synthetic */ ca2 f;

        @DexIgnore
        public i(p23 p23, ca2 ca2) {
            this.e = p23;
            this.f = ca2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.f.r;
            if (!kd4.a((Object) this.e.T0(), (Object) String.valueOf(editable))) {
                this.e.U((String) null);
            }
            ImageView imageView2 = this.f.r;
            kd4.a((Object) imageView2, "binding.closeIv");
            imageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public j(p23 p23, ca2 ca2) {
            this.e = p23;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            kd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public k(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).b("travel");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public l(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;
        @DexIgnore
        public /* final */ /* synthetic */ ca2 f;

        @DexIgnore
        public m(p23 p23, ca2 ca2) {
            this.e = p23;
            this.f = ca2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.f.q.setText("", false);
            p23.b(this.e).h();
            this.e.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ p23 e;

        @DexIgnore
        public n(p23 p23) {
            this.e = p23;
        }

        @DexIgnore
        public final void onClick(View view) {
            p23.b(this.e).b("eta");
        }
    }

    /*
    static {
        String simpleName = p23.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ t23 b(p23 p23) {
        t23 t23 = p23.k;
        if (t23 != null) {
            return t23;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void D(String str) {
        kd4.b(str, "userWorkDefaultAddress");
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                sh2 sh2 = a2.v;
                if (sh2 != null) {
                    FlexibleTextView flexibleTextView = sh2.r;
                    kd4.a((Object) flexibleTextView, "workButton.ftvContent");
                    flexibleTextView.setText(str);
                    Context context = getContext();
                    if (context != null) {
                        int a3 = (int) ts3.a(12, context);
                        sh2.t.setPadding(a3, a3, a3, a3);
                        if (!TextUtils.isEmpty(str)) {
                            ImageButton imageButton = sh2.t;
                            kd4.a((Object) imageButton, "workButton.ivEditButton");
                            imageButton.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.activeColorPrimary)));
                            sh2.t.setImageResource(R.drawable.ic_caret_right);
                            return;
                        }
                        ImageButton imageButton2 = sh2.t;
                        kd4.a((Object) imageButton2, "workButton.ivEditButton");
                        imageButton2.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.dianaInactiveTab)));
                        sh2.t.setImageResource(R.drawable.ic_caret_right);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L(boolean z) {
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.B;
                if (switchCompat != null) {
                    kd4.a((Object) switchCompat, "it");
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void M(boolean z) {
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        this.n = null;
                        V0();
                        return;
                    }
                }
                U0();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2 != null) goto L_0x0028;
     */
    @DexIgnore
    public boolean S0() {
        String str;
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 == null) {
                return true;
            }
            String str2 = this.n;
            if (str2 != null) {
                if (str2 != null) {
                    str = StringsKt__StringsKt.d(str2).toString();
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            str = "";
            String str3 = str;
            ImageView imageView = a2.w;
            kd4.a((Object) imageView, "it.ivArrivalTime");
            String str4 = imageView.getAlpha() == 1.0f ? "eta" : "travel";
            t23 t23 = this.k;
            if (t23 != null) {
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                String obj = appCompatAutoCompleteTextView.getText().toString();
                if (obj != null) {
                    String obj2 = StringsKt__StringsKt.d(obj).toString();
                    DirectionBy directionBy = DirectionBy.CAR;
                    SwitchCompat switchCompat = a2.B;
                    kd4.a((Object) switchCompat, "it.scAvoidTolls");
                    t23.a(str3, obj2, directionBy, switchCompat.isChecked(), str4);
                    return true;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            }
            kd4.d("mPresenter");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final String T0() {
        return this.n;
    }

    @DexIgnore
    public final void U(String str) {
        this.n = str;
    }

    @DexIgnore
    public void U0() {
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                LinearLayout linearLayout = a2.z;
                kd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                sh2 sh2 = a2.u;
                kd4.a((Object) sh2, "it.icHome");
                View d2 = sh2.d();
                kd4.a((Object) d2, "it.icHome.root");
                d2.setVisibility(0);
                sh2 sh22 = a2.v;
                kd4.a((Object) sh22, "it.icWork");
                View d3 = sh22.d();
                kd4.a((Object) d3, "it.icWork.root");
                d3.setVisibility(0);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void V0() {
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                kd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.z;
                kd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                sh2 sh2 = a2.u;
                kd4.a((Object) sh2, "it.icHome");
                View d2 = sh2.d();
                kd4.a((Object) d2, "it.icHome.root");
                d2.setVisibility(8);
                sh2 sh22 = a2.v;
                kd4.a((Object) sh22, "it.icWork");
                View d3 = sh22.d();
                kd4.a((Object) d3, "it.icWork.root");
                d3.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(String str, String str2) {
        kd4.b(str, "addressType");
        kd4.b(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                q62 q62 = this.m;
                if (q62 != null) {
                    q62.a((q62.b) null);
                }
                CommuteTimeSettingsDefaultAddressActivity.C.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            q62 q622 = this.m;
            if (q622 != null) {
                q622.a((q62.b) null);
            }
            CommuteTimeSettingsDefaultAddressActivity.C.a(this, bundle);
        }
    }

    @DexIgnore
    public void j(List<String> list) {
        kd4.b(list, "recentSearchedList");
        U0();
        if (!list.isEmpty()) {
            ls2 ls2 = this.l;
            if (ls2 != null) {
                ls2.a((List<String>) kb4.d(list));
            }
            tr3<ca2> tr3 = this.j;
            if (tr3 != null) {
                ca2 a2 = tr3.a();
                if (a2 != null) {
                    LinearLayout linearLayout = a2.z;
                    if (linearLayout != null) {
                        linearLayout.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        tr3<ca2> tr32 = this.j;
        if (tr32 != null) {
            ca2 a3 = tr32.a();
            if (a3 != null) {
                LinearLayout linearLayout2 = a3.z;
                if (linearLayout2 != null) {
                    linearLayout2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent == null) {
            return;
        }
        if (i2 == 111) {
            String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
            kd4.a((Object) stringExtra, "homeAddress");
            w(stringExtra);
            t23 t23 = this.k;
            if (t23 != null) {
                t23.a(stringExtra);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else if (i2 == 112) {
            String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
            kd4.a((Object) stringExtra2, "workAddress");
            D(stringExtra2);
            t23 t232 = this.k;
            if (t232 != null) {
                t232.a(stringExtra2);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ca2 ca2 = (ca2) qa.a(layoutInflater, R.layout.fragment_commute_time_settings, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = ca2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new h(this, ca2));
        appCompatAutoCompleteTextView.addTextChangedListener(new i(this, ca2));
        appCompatAutoCompleteTextView.setOnKeyListener(new j(this, ca2));
        ca2.t.setOnClickListener(new l(this));
        sh2 sh2 = ca2.u;
        sh2.u.setImageResource(R.drawable.ic_destination_home);
        FlexibleTextView flexibleTextView = sh2.s;
        kd4.a((Object) flexibleTextView, "it.ftvTitle");
        flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Home));
        sh2.q.setOnClickListener(new c(this));
        sh2.t.setOnClickListener(new d(this));
        sh2 sh22 = ca2.v;
        sh22.u.setImageResource(R.drawable.ic_destination_other);
        FlexibleTextView flexibleTextView2 = sh22.s;
        kd4.a((Object) flexibleTextView2, "it.ftvTitle");
        flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_CommuteTimeDestinationSettings_Label__Other));
        sh22.q.setOnClickListener(new e(this));
        sh22.t.setOnClickListener(new f(this));
        ca2.r.setOnClickListener(new m(this, ca2));
        ls2 ls2 = new ls2();
        ls2.a((ls2.b) new b(this, ca2));
        this.l = ls2;
        RecyclerView recyclerView = ca2.A;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        kd4.a((Object) ca2, "binding");
        View d2 = ca2.d();
        kd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new g(d2, ca2));
        ca2.w.setOnClickListener(new n(this));
        ca2.x.setOnClickListener(new k(this));
        this.j = new tr3<>(this, ca2);
        return ca2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        t23 t23 = this.k;
        if (t23 != null) {
            t23.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        t23 t23 = this.k;
        if (t23 != null) {
            t23.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            t23 t23 = this.k;
            if (t23 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", t23.i());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void w(String str) {
        kd4.b(str, "userHomeDefaultAddress");
        tr3<ca2> tr3 = this.j;
        if (tr3 != null) {
            ca2 a2 = tr3.a();
            if (a2 != null) {
                sh2 sh2 = a2.u;
                if (sh2 != null) {
                    FlexibleTextView flexibleTextView = sh2.r;
                    kd4.a((Object) flexibleTextView, "homeButton.ftvContent");
                    flexibleTextView.setText(str);
                    Context context = getContext();
                    if (context != null) {
                        int a3 = (int) ts3.a(12, context);
                        sh2.t.setPadding(a3, a3, a3, a3);
                        if (!TextUtils.isEmpty(str)) {
                            ImageButton imageButton = sh2.t;
                            kd4.a((Object) imageButton, "homeButton.ivEditButton");
                            imageButton.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor)));
                            sh2.t.setImageResource(R.drawable.ic_caret_right);
                            return;
                        }
                        ImageButton imageButton2 = sh2.t;
                        kd4.a((Object) imageButton2, "homeButton.ivEditButton");
                        imageButton2.setImageTintList(ColorStateList.valueOf(k6.a((Context) PortfolioApp.W.c(), (int) R.color.dianaInactiveTab)));
                        sh2.t.setImageResource(R.drawable.ic_caret_right);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void x(String str) {
        kd4.b(str, "userCurrentAddress");
        if (isActive()) {
            tr3<ca2> tr3 = this.j;
            if (tr3 != null) {
                ca2 a2 = tr3.a();
                if (a2 != null) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = appCompatAutoCompleteTextView.getText();
                    kd4.a((Object) text, "it.autocompletePlaces.text");
                    boolean z = true;
                    if (StringsKt__StringsKt.d(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText(str, false);
                            this.n = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(t23 t23) {
        kd4.b(t23, "presenter");
        this.k = t23;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                this.m = new q62(context, placesClient);
                q62 q62 = this.m;
                if (q62 != null) {
                    q62.a((q62.b) this);
                }
                tr3<ca2> tr3 = this.j;
                if (tr3 != null) {
                    ca2 a2 = tr3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        kd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2, false);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        U0();
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(p, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    tr3<ca2> tr3 = this.j;
                    if (tr3 != null) {
                        ca2 a2 = tr3.a();
                        if (a2 != null) {
                            a2.w.setAlpha(1.0f);
                            a2.x.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                tr3<ca2> tr32 = this.j;
                if (tr32 != null) {
                    ca2 a3 = tr32.a();
                    if (a3 != null) {
                        a3.x.setAlpha(1.0f);
                        a3.w.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
        }
    }
}
