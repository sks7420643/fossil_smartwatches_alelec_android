package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Map;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bq2 extends Transition {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ String[] f; // = {"transition:textResize"};
    @DexIgnore
    public static /* final */ a g; // = new a(Float.TYPE, "textSize");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Property<TextView, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(TextView textView) {
            kd4.b(textView, "textView");
            return Float.valueOf(textView.getTextSize());
        }

        @DexIgnore
        /* renamed from: a */
        public void set(TextView textView, Float f) {
            kd4.b(textView, "textView");
            if (f != null) {
                textView.setTextSize(0, f.floatValue());
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new b((fd4) null);
        String simpleName = bq2.class.getSimpleName();
        kd4.a((Object) simpleName, "TextResizeTransition::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public final void a(TransitionValues transitionValues) {
        View view = transitionValues.view;
        if (!(view instanceof TextView)) {
            return;
        }
        if (view != null) {
            TextView textView = (TextView) view;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "captureValues textView.textSize = " + textView.getTextSize());
            Map map = transitionValues.values;
            kd4.a((Object) map, "transitionValues.values");
            map.put("transition:textResize", Float.valueOf(textView.getTextSize()));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
    }

    @DexIgnore
    public void captureEndValues(TransitionValues transitionValues) {
        kd4.b(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(e, "captureEndValues()");
        a(transitionValues);
    }

    @DexIgnore
    public void captureStartValues(TransitionValues transitionValues) {
        kd4.b(transitionValues, "transitionValues");
        FLogger.INSTANCE.getLocal().d(e, "captureStartValues()");
        a(transitionValues);
    }

    @DexIgnore
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        kd4.b(viewGroup, "sceneRoot");
        if (transitionValues == null || transitionValues2 == null || transitionValues.values.get("transition:textResize") == null || transitionValues2.values.get("transition:textResize") == null) {
            return null;
        }
        Object obj = transitionValues.values.get("transition:textResize");
        if (obj != null) {
            float floatValue = ((Float) obj).floatValue();
            Object obj2 = transitionValues2.values.get("transition:textResize");
            if (obj2 != null) {
                float floatValue2 = ((Float) obj2).floatValue();
                if (floatValue == floatValue2) {
                    return null;
                }
                View view = transitionValues2.view;
                if (view != null) {
                    TextView textView = (TextView) view;
                    textView.setTextSize(0, floatValue2);
                    return ObjectAnimator.ofFloat(textView, g, new float[]{floatValue, floatValue2});
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.TextView");
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Float");
    }

    @DexIgnore
    public String[] getTransitionProperties() {
        return f;
    }
}
