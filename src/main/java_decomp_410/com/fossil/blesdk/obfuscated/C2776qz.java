package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qz */
public class C2776qz implements com.crashlytics.android.core.Report {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.io.File f8811a;

    @DexIgnore
    public C2776qz(java.io.File file) {
        this.f8811a = file;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.String> mo4192a() {
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo4193b() {
        return this.f8811a.getName();
    }

    @DexIgnore
    /* renamed from: c */
    public java.io.File mo4194c() {
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public java.io.File[] mo4195d() {
        return this.f8811a.listFiles();
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.String mo4196e() {
        return null;
    }

    @DexIgnore
    public com.crashlytics.android.core.Report.Type getType() {
        return com.crashlytics.android.core.Report.Type.NATIVE;
    }

    @DexIgnore
    public void remove() {
        for (java.io.File path : mo4195d()) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("CrashlyticsCore", "Removing native report file at " + path.getPath());
            r0[r2].delete();
        }
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Removing native report directory at " + this.f8811a);
        this.f8811a.delete();
    }
}
