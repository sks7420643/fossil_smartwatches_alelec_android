package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ln */
public abstract class C2336ln<T> extends com.android.volley.Request<T> {
    @DexIgnore
    public static /* final */ java.lang.String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ java.lang.String PROTOCOL_CONTENT_TYPE; // = java.lang.String.format("application/json; charset=%s", new java.lang.Object[]{PROTOCOL_CHARSET});
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C3047um.C3049b<T> mListener;
    @DexIgnore
    public /* final */ java.lang.Object mLock;
    @DexIgnore
    public /* final */ java.lang.String mRequestBody;

    @DexIgnore
    @java.lang.Deprecated
    public C2336ln(java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.C3047um.C3049b<T> bVar, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        this(-1, str, str2, bVar, aVar);
    }

    @DexIgnore
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public void deliverResponse(T t) {
        com.fossil.blesdk.obfuscated.C3047um.C3049b<T> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(t);
        }
    }

    @DexIgnore
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (java.io.UnsupportedEncodingException unused) {
            com.fossil.blesdk.obfuscated.C3296xm.m16424e("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    public java.lang.String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @java.lang.Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @java.lang.Deprecated
    public java.lang.String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    public abstract com.fossil.blesdk.obfuscated.C3047um<T> parseNetworkResponse(com.fossil.blesdk.obfuscated.C2897sm smVar);

    @DexIgnore
    public C2336ln(int i, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.C3047um.C3049b<T> bVar, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        super(i, str, aVar);
        this.mLock = new java.lang.Object();
        this.mListener = bVar;
        this.mRequestBody = str2;
    }
}
