package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class no4 implements xo4 {
    @DexIgnore
    public /* final */ xo4 e;

    @DexIgnore
    public no4(xo4 xo4) {
        if (xo4 != null) {
            this.e = xo4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public void a(jo4 jo4, long j) throws IOException {
        this.e.a(jo4, j);
    }

    @DexIgnore
    public zo4 b() {
        return this.e.b();
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public void flush() throws IOException {
        this.e.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.e.toString() + ")";
    }
}
