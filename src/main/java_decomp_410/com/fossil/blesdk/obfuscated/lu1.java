package com.fossil.blesdk.obfuscated;

import com.google.common.base.Predicates;
import com.google.common.collect.AbstractIterator;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lu1 {
    @DexIgnore
    public static /* final */ ev1<Object> a; // = new c();
    @DexIgnore
    public static /* final */ Iterator<Object> b; // = new d();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends wt1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Object[] g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Object[] objArr, int i3) {
            super(i, i2);
            this.g = objArr;
            this.h = i3;
        }

        @DexIgnore
        public T a(int i) {
            return this.g[this.h + i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends dv1<T> {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ Object f;

        @DexIgnore
        public b(Object obj) {
            this.f = obj;
        }

        @DexIgnore
        public boolean hasNext() {
            return !this.e;
        }

        @DexIgnore
        public T next() {
            if (!this.e) {
                this.e = true;
                return this.f;
            }
            throw new NoSuchElementException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ev1<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public boolean hasPrevious() {
            return false;
        }

        @DexIgnore
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int nextIndex() {
            return 0;
        }

        @DexIgnore
        public Object previous() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return -1;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Iterator<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            bu1.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends dv1<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator e;

        @DexIgnore
        public e(Iterator it) {
            this.e = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.e.hasNext();
        }

        @DexIgnore
        public T next() {
            return this.e.next();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends AbstractIterator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator g;
        @DexIgnore
        public /* final */ /* synthetic */ tt1 h;

        @DexIgnore
        public f(Iterator it, tt1 tt1) {
            this.g = it;
            this.h = tt1;
        }

        @DexIgnore
        public T a() {
            while (this.g.hasNext()) {
                T next = this.g.next();
                if (this.h.apply(next)) {
                    return next;
                }
            }
            return b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends bv1<F, T> {
        @DexIgnore
        public /* final */ /* synthetic */ ot1 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(Iterator it, ot1 ot1) {
            super(it);
            this.f = ot1;
        }

        @DexIgnore
        public T a(F f2) {
            return this.f.apply(f2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h<E> implements tu1<E> {
        @DexIgnore
        public /* final */ Iterator<? extends E> e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public E g;

        @DexIgnore
        public h(Iterator<? extends E> it) {
            st1.a(it);
            this.e = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f || this.e.hasNext();
        }

        @DexIgnore
        public E next() {
            if (!this.f) {
                return this.e.next();
            }
            E e2 = this.g;
            this.f = false;
            this.g = null;
            return e2;
        }

        @DexIgnore
        public E peek() {
            if (!this.f) {
                this.g = this.e.next();
                this.f = true;
            }
            return this.g;
        }

        @DexIgnore
        public void remove() {
            st1.b(!this.f, (Object) "Can't remove after you've peeked at next");
            this.e.remove();
        }
    }

    @DexIgnore
    public static <T> dv1<T> a() {
        return b();
    }

    @DexIgnore
    public static <T> ev1<T> b() {
        return a;
    }

    @DexIgnore
    public static <T> Iterator<T> c() {
        return b;
    }

    @DexIgnore
    public static String d(Iterator<?> it) {
        pt1 pt1 = cu1.a;
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        pt1.a(sb, it);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public static <T> dv1<T> e(Iterator<? extends T> it) {
        st1.a(it);
        if (it instanceof dv1) {
            return (dv1) it;
        }
        return new e(it);
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Object obj) {
        return b(it, Predicates.a(obj));
    }

    @DexIgnore
    public static boolean b(Iterator<?> it, Collection<?> collection) {
        return e(it, Predicates.a(Predicates.a(collection)));
    }

    @DexIgnore
    public static <T> dv1<T> c(Iterator<T> it, tt1<? super T> tt1) {
        st1.a(it);
        st1.a(tt1);
        return new f(it, tt1);
    }

    @DexIgnore
    public static <T> int d(Iterator<T> it, tt1<? super T> tt1) {
        st1.a(tt1, (Object) "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (tt1.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Collection<?> collection) {
        return e(it, Predicates.a(collection));
    }

    @DexIgnore
    public static <T> T b(Iterator<T> it) {
        T next = it.next();
        if (!it.hasNext()) {
            return next;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("expected one element but was: <");
        sb.append(next);
        for (int i = 0; i < 4 && it.hasNext(); i++) {
            sb.append(", ");
            sb.append(it.next());
        }
        if (it.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Iterator<?> it2) {
        while (it.hasNext()) {
            if (!it2.hasNext()) {
                return false;
            }
            if (!rt1.a(it.next(), it2.next())) {
                return false;
            }
        }
        return !it2.hasNext();
    }

    @DexIgnore
    public static <T> tu1<T> c(Iterator<? extends T> it) {
        if (it instanceof h) {
            return (h) it;
        }
        return new h(it);
    }

    @DexIgnore
    public static <T> boolean e(Iterator<T> it, tt1<? super T> tt1) {
        st1.a(tt1);
        boolean z = false;
        while (it.hasNext()) {
            if (tt1.apply(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it) {
        st1.a(collection);
        st1.a(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    @DexIgnore
    public static <T> boolean b(Iterator<T> it, tt1<? super T> tt1) {
        return d(it, tt1) != -1;
    }

    @DexIgnore
    public static <T> T b(Iterator<? extends T> it, T t) {
        return it.hasNext() ? it.next() : t;
    }

    @DexIgnore
    public static <T> boolean a(Iterator<T> it, tt1<? super T> tt1) {
        st1.a(tt1);
        while (it.hasNext()) {
            if (!tt1.apply(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <F, T> Iterator<T> a(Iterator<F> it, ot1<? super F, ? extends T> ot1) {
        st1.a(ot1);
        return new g(it, ot1);
    }

    @DexIgnore
    public static void a(Iterator<?> it) {
        st1.a(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @DexIgnore
    @SafeVarargs
    public static <T> dv1<T> a(T... tArr) {
        return a(tArr, 0, tArr.length, 0);
    }

    @DexIgnore
    public static <T> ev1<T> a(T[] tArr, int i, int i2, int i3) {
        st1.a(i2 >= 0);
        st1.b(i, i + i2, tArr.length);
        st1.b(i3, i2);
        if (i2 == 0) {
            return b();
        }
        return new a(i2, i3, tArr, i);
    }

    @DexIgnore
    public static <T> dv1<T> a(T t) {
        return new b(t);
    }
}
