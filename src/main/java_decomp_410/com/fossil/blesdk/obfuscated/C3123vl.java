package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vl */
public class C3123vl implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.lang.String f10320g; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("StopWorkRunnable");

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2968tj f10321e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.String f10322f;

    @DexIgnore
    public C3123vl(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str) {
        this.f10321e = tjVar;
        this.f10322f = str;
    }

    @DexIgnore
    public void run() {
        androidx.work.impl.WorkDatabase g = this.f10321e.mo16462g();
        com.fossil.blesdk.obfuscated.C2036il d = g.mo3780d();
        g.beginTransaction();
        try {
            if (d.mo12028d(this.f10322f) == androidx.work.WorkInfo.State.RUNNING) {
                d.mo12015a(androidx.work.WorkInfo.State.ENQUEUED, this.f10322f);
            }
            boolean e = this.f10321e.mo16460e().mo14431e(this.f10322f);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10320g, java.lang.String.format("StopWorkRunnable for %s; Processor.stopWork = %s", new java.lang.Object[]{this.f10322f, java.lang.Boolean.valueOf(e)}), new java.lang.Throwable[0]);
            g.setTransactionSuccessful();
        } finally {
            g.endTransaction();
        }
    }
}
