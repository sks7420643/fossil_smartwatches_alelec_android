package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ch */
public class C1555ch {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ android.graphics.Matrix f4093a; // = new com.fossil.blesdk.obfuscated.C1555ch.C1556a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ch$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ch$a */
    public static class C1556a extends android.graphics.Matrix {
        @DexIgnore
        /* renamed from: a */
        public void mo9490a() {
            throw new java.lang.IllegalStateException("Matrix can not be modified");
        }

        @DexIgnore
        public boolean postConcat(android.graphics.Matrix matrix) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postRotate(float f, float f2, float f3) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postScale(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postSkew(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postTranslate(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preConcat(android.graphics.Matrix matrix) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preRotate(float f, float f2, float f3) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preScale(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preSkew(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preTranslate(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void reset() {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void set(android.graphics.Matrix matrix) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean setConcat(android.graphics.Matrix matrix, android.graphics.Matrix matrix2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean setPolyToPoly(float[] fArr, int i, float[] fArr2, int i2, int i3) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean setRectToRect(android.graphics.RectF rectF, android.graphics.RectF rectF2, android.graphics.Matrix.ScaleToFit scaleToFit) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setRotate(float f, float f2, float f3) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setScale(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setSinCos(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setSkew(float f, float f2, float f3, float f4) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setTranslate(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setValues(float[] fArr) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postRotate(float f) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postScale(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean postSkew(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preRotate(float f) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preScale(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public boolean preSkew(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setRotate(float f) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setScale(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setSinCos(float f, float f2) {
            mo9490a();
            throw null;
        }

        @DexIgnore
        public void setSkew(float f, float f2) {
            mo9490a();
            throw null;
        }
    }
}
