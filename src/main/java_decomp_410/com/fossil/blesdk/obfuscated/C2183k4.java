package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k4 */
public class C2183k4<K, V> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.LinkedHashMap<K, V> f6696a;

    @DexIgnore
    /* renamed from: b */
    public int f6697b;

    @DexIgnore
    /* renamed from: c */
    public int f6698c;

    @DexIgnore
    /* renamed from: d */
    public int f6699d;

    @DexIgnore
    /* renamed from: e */
    public int f6700e;

    @DexIgnore
    /* renamed from: f */
    public int f6701f;

    @DexIgnore
    /* renamed from: g */
    public int f6702g;

    @DexIgnore
    /* renamed from: h */
    public int f6703h;

    @DexIgnore
    public C2183k4(int i) {
        if (i > 0) {
            this.f6698c = i;
            this.f6696a = new java.util.LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new java.lang.IllegalArgumentException("maxSize <= 0");
    }

    @DexIgnore
    /* renamed from: a */
    public V mo12622a(K k) {
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public final V mo12623a(K k, V v) {
        V put;
        if (k == null || v == null) {
            throw new java.lang.NullPointerException("key == null || value == null");
        }
        synchronized (this) {
            this.f6699d++;
            this.f6697b += mo12626b(k, v);
            put = this.f6696a.put(k, v);
            if (put != null) {
                this.f6697b -= mo12626b(k, put);
            }
        }
        if (put != null) {
            mo12625a(false, k, put, v);
        }
        mo12624a(this.f6698c);
        return put;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12625a(boolean z, K k, V v, V v2) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        r0 = mo12622a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        if (r0 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0022, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r4.f6700e++;
        r1 = r4.f6696a.put(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r1 == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        r4.f6696a.put(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        r4.f6697b += mo12626b(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0041, code lost:
        if (r1 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0043, code lost:
        mo12625a(false, r5, r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0047, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0048, code lost:
        mo12624a(r4.f6698c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004d, code lost:
        return r0;
     */
    @DexIgnore
    /* renamed from: b */
    public final V mo12627b(K k) {
        if (k != null) {
            synchronized (this) {
                V v = this.f6696a.get(k);
                if (v != null) {
                    this.f6702g++;
                    return v;
                }
                this.f6703h++;
            }
        } else {
            throw new java.lang.NullPointerException("key == null");
        }
    }

    @DexIgnore
    /* renamed from: c */
    public int mo12628c(K k, V v) {
        return 1;
    }

    @DexIgnore
    public final synchronized java.lang.String toString() {
        int i;
        i = this.f6702g + this.f6703h;
        return java.lang.String.format(java.util.Locale.US, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", new java.lang.Object[]{java.lang.Integer.valueOf(this.f6698c), java.lang.Integer.valueOf(this.f6702g), java.lang.Integer.valueOf(this.f6703h), java.lang.Integer.valueOf(i != 0 ? (this.f6702g * 100) / i : 0)});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0070, code lost:
        throw new java.lang.IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
     */
    @DexIgnore
    /* renamed from: a */
    public void mo12624a(int i) {
        java.lang.Object key;
        java.lang.Object value;
        while (true) {
            synchronized (this) {
                if (this.f6697b < 0 || (this.f6696a.isEmpty() && this.f6697b != 0)) {
                } else if (this.f6697b > i) {
                    if (this.f6696a.isEmpty()) {
                        break;
                    }
                    java.util.Map.Entry next = this.f6696a.entrySet().iterator().next();
                    key = next.getKey();
                    value = next.getValue();
                    this.f6696a.remove(key);
                    this.f6697b -= mo12626b(key, value);
                    this.f6701f++;
                }
            }
            mo12625a(true, key, value, (java.lang.Object) null);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo12626b(K k, V v) {
        int c = mo12628c(k, v);
        if (c >= 0) {
            return c;
        }
        throw new java.lang.IllegalStateException("Negative size: " + k + com.j256.ormlite.stmt.query.SimpleComparison.EQUAL_TO_OPERATION + v);
    }
}
