package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kc */
public class C2200kc {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2200kc.C2202b f6772a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ androidx.lifecycle.ViewModelStore f6773b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kc$a */
    public static class C2201a extends com.fossil.blesdk.obfuscated.C2200kc.C2204d {

        @DexIgnore
        /* renamed from: b */
        public static com.fossil.blesdk.obfuscated.C2200kc.C2201a f6774b;

        @DexIgnore
        /* renamed from: a */
        public android.app.Application f6775a;

        @DexIgnore
        public C2201a(android.app.Application application) {
            this.f6775a = application;
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2200kc.C2201a m9562a(android.app.Application application) {
            if (f6774b == null) {
                f6774b = new com.fossil.blesdk.obfuscated.C2200kc.C2201a(application);
            }
            return f6774b;
        }

        @DexIgnore
        /* renamed from: a */
        public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12712a(java.lang.Class<T> cls) {
            if (!com.fossil.blesdk.obfuscated.C2388mb.class.isAssignableFrom(cls)) {
                return super.mo12712a(cls);
            }
            try {
                return (com.fossil.blesdk.obfuscated.C2010ic) cls.getConstructor(new java.lang.Class[]{android.app.Application.class}).newInstance(new java.lang.Object[]{this.f6775a});
            } catch (java.lang.NoSuchMethodException e) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e);
            } catch (java.lang.IllegalAccessException e2) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e2);
            } catch (java.lang.InstantiationException e3) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e3);
            } catch (java.lang.reflect.InvocationTargetException e4) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e4);
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.kc$b */
    public interface C2202b {
        @DexIgnore
        /* renamed from: a */
        <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12712a(java.lang.Class<T> cls);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kc$c")
    /* renamed from: com.fossil.blesdk.obfuscated.kc$c */
    public static abstract class C2203c implements com.fossil.blesdk.obfuscated.C2200kc.C2202b {
        @DexIgnore
        /* renamed from: a */
        public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12712a(java.lang.Class<T> cls) {
            throw new java.lang.UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        @DexIgnore
        /* renamed from: a */
        public abstract <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12713a(java.lang.String str, java.lang.Class<T> cls);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kc$d")
    /* renamed from: com.fossil.blesdk.obfuscated.kc$d */
    public static class C2204d implements com.fossil.blesdk.obfuscated.C2200kc.C2202b {
        @DexIgnore
        /* renamed from: a */
        public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12712a(java.lang.Class<T> cls) {
            try {
                return (com.fossil.blesdk.obfuscated.C2010ic) cls.newInstance();
            } catch (java.lang.InstantiationException e) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e);
            } catch (java.lang.IllegalAccessException e2) {
                throw new java.lang.RuntimeException("Cannot create an instance of " + cls, e2);
            }
        }
    }

    @DexIgnore
    public C2200kc(androidx.lifecycle.ViewModelStore viewModelStore, com.fossil.blesdk.obfuscated.C2200kc.C2202b bVar) {
        this.f6772a = bVar;
        this.f6773b = viewModelStore;
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12710a(java.lang.Class<T> cls) {
        java.lang.String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return mo12711a("androidx.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new java.lang.IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends com.fossil.blesdk.obfuscated.C2010ic> T mo12711a(java.lang.String str, java.lang.Class<T> cls) {
        T t;
        T a = this.f6773b.mo2299a(str);
        if (cls.isInstance(a)) {
            return a;
        }
        com.fossil.blesdk.obfuscated.C2200kc.C2202b bVar = this.f6772a;
        if (bVar instanceof com.fossil.blesdk.obfuscated.C2200kc.C2203c) {
            t = ((com.fossil.blesdk.obfuscated.C2200kc.C2203c) bVar).mo12713a(str, cls);
        } else {
            t = bVar.mo12712a(cls);
        }
        this.f6773b.mo2301a(str, t);
        return t;
    }
}
