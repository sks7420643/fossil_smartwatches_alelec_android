package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a5 */
public class C1382a5 extends androidx.constraintlayout.solver.widgets.ConstraintWidget {

    @DexIgnore
    /* renamed from: k0 */
    public float f3355k0; // = -1.0f;

    @DexIgnore
    /* renamed from: l0 */
    public int f3356l0; // = -1;

    @DexIgnore
    /* renamed from: m0 */
    public int f3357m0; // = -1;

    @DexIgnore
    /* renamed from: n0 */
    public androidx.constraintlayout.solver.widgets.ConstraintAnchor f3358n0; // = this.f717t;

    @DexIgnore
    /* renamed from: o0 */
    public int f3359o0;

    @DexIgnore
    /* renamed from: p0 */
    public boolean f3360p0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.a5$a")
    /* renamed from: com.fossil.blesdk.obfuscated.a5$a */
    public static /* synthetic */ class C1383a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f3361a; // = new int[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT.ordinal()] = 1;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT.ordinal()] = 2;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP.ordinal()] = 3;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM.ordinal()] = 4;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BASELINE.ordinal()] = 5;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER.ordinal()] = 6;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_X.ordinal()] = 7;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER_Y.ordinal()] = 8;
            f3361a[androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.NONE.ordinal()] = 9;
        }
        */
    }

    @DexIgnore
    public C1382a5() {
        this.f3359o0 = 0;
        this.f3360p0 = false;
        new com.fossil.blesdk.obfuscated.C1596d5();
        this.f664B.clear();
        this.f664B.add(this.f3358n0);
        int length = this.f663A.length;
        for (int i = 0; i < length; i++) {
            this.f663A[i] = this.f3358n0;
        }
    }

    @DexIgnore
    /* renamed from: K */
    public int mo8535K() {
        return this.f3359o0;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.widgets.ConstraintAnchor mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type) {
        switch (com.fossil.blesdk.obfuscated.C1382a5.C1383a.f3361a[type.ordinal()]) {
            case 1:
            case 2:
                if (this.f3359o0 == 1) {
                    return this.f3358n0;
                }
                break;
            case 3:
            case 4:
                if (this.f3359o0 == 0) {
                    return this.f3358n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new java.lang.AssertionError(type.name());
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo1311b() {
        return true;
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintAnchor> mo1313c() {
        return this.f664B;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo8536e(float f) {
        if (f > -1.0f) {
            this.f3355k0 = f;
            this.f3356l0 = -1;
            this.f3357m0 = -1;
        }
    }

    @DexIgnore
    /* renamed from: u */
    public void mo8537u(int i) {
        if (i > -1) {
            this.f3355k0 = -1.0f;
            this.f3356l0 = i;
            this.f3357m0 = -1;
        }
    }

    @DexIgnore
    /* renamed from: v */
    public void mo8538v(int i) {
        if (i > -1) {
            this.f3355k0 = -1.0f;
            this.f3356l0 = -1;
            this.f3357m0 = i;
        }
    }

    @DexIgnore
    /* renamed from: w */
    public void mo8539w(int i) {
        if (this.f3359o0 != i) {
            this.f3359o0 = i;
            this.f664B.clear();
            if (this.f3359o0 == 1) {
                this.f3358n0 = this.f716s;
            } else {
                this.f3358n0 = this.f717t;
            }
            this.f664B.add(this.f3358n0);
            int length = this.f663A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.f663A[i2] = this.f3358n0;
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo1316c(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        if (mo1336l() != null) {
            int b = q4Var.mo15020b((java.lang.Object) this.f3358n0);
            if (this.f3359o0 == 1) {
                mo1351s(b);
                mo1353t(0);
                mo1329h(mo1336l().mo1332j());
                mo1345p(0);
                return;
            }
            mo1351s(0);
            mo1353t(b);
            mo1345p(mo1336l().mo1352t());
            mo1329h(0);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1287a(int i) {
        androidx.constraintlayout.solver.widgets.ConstraintWidget l = mo1336l();
        if (l != null) {
            if (mo8535K() == 1) {
                this.f717t.mo1266d().mo10324a(1, l.f717t.mo1266d(), 0);
                this.f719v.mo1266d().mo10324a(1, l.f717t.mo1266d(), 0);
                if (this.f3356l0 != -1) {
                    this.f716s.mo1266d().mo10324a(1, l.f716s.mo1266d(), this.f3356l0);
                    this.f718u.mo1266d().mo10324a(1, l.f716s.mo1266d(), this.f3356l0);
                } else if (this.f3357m0 != -1) {
                    this.f716s.mo1266d().mo10324a(1, l.f718u.mo1266d(), -this.f3357m0);
                    this.f718u.mo1266d().mo10324a(1, l.f718u.mo1266d(), -this.f3357m0);
                } else if (this.f3355k0 != -1.0f && l.mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i2 = (int) (((float) l.f667E) * this.f3355k0);
                    this.f716s.mo1266d().mo10324a(1, l.f716s.mo1266d(), i2);
                    this.f718u.mo1266d().mo10324a(1, l.f716s.mo1266d(), i2);
                }
            } else {
                this.f716s.mo1266d().mo10324a(1, l.f716s.mo1266d(), 0);
                this.f718u.mo1266d().mo10324a(1, l.f716s.mo1266d(), 0);
                if (this.f3356l0 != -1) {
                    this.f717t.mo1266d().mo10324a(1, l.f717t.mo1266d(), this.f3356l0);
                    this.f719v.mo1266d().mo10324a(1, l.f717t.mo1266d(), this.f3356l0);
                } else if (this.f3357m0 != -1) {
                    this.f717t.mo1266d().mo10324a(1, l.f719v.mo1266d(), -this.f3357m0);
                    this.f719v.mo1266d().mo10324a(1, l.f719v.mo1266d(), -this.f3357m0);
                } else if (this.f3355k0 != -1.0f && l.mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i3 = (int) (((float) l.f668F) * this.f3355k0);
                    this.f717t.mo1266d().mo10324a(1, l.f717t.mo1266d(), i3);
                    this.f719v.mo1266d().mo10324a(1, l.f717t.mo1266d(), i3);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1297a(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        com.fossil.blesdk.obfuscated.C3342y4 y4Var = (com.fossil.blesdk.obfuscated.C3342y4) mo1336l();
        if (y4Var != null) {
            androidx.constraintlayout.solver.widgets.ConstraintAnchor a = y4Var.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT);
            androidx.constraintlayout.solver.widgets.ConstraintAnchor a2 = y4Var.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT);
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f666D;
            boolean z = constraintWidget != null && constraintWidget.f665C[0] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (this.f3359o0 == 0) {
                a = y4Var.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP);
                a2 = y4Var.mo1284a(androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM);
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = this.f666D;
                z = constraintWidget2 != null && constraintWidget2.f665C[1] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            }
            if (this.f3356l0 != -1) {
                androidx.constraintlayout.solver.SolverVariable a3 = q4Var.mo15010a((java.lang.Object) this.f3358n0);
                q4Var.mo15011a(a3, q4Var.mo15010a((java.lang.Object) a), this.f3356l0, 6);
                if (z) {
                    q4Var.mo15022b(q4Var.mo15010a((java.lang.Object) a2), a3, 0, 5);
                }
            } else if (this.f3357m0 != -1) {
                androidx.constraintlayout.solver.SolverVariable a4 = q4Var.mo15010a((java.lang.Object) this.f3358n0);
                androidx.constraintlayout.solver.SolverVariable a5 = q4Var.mo15010a((java.lang.Object) a2);
                q4Var.mo15011a(a4, a5, -this.f3357m0, 6);
                if (z) {
                    q4Var.mo15022b(a4, q4Var.mo15010a((java.lang.Object) a), 0, 5);
                    q4Var.mo15022b(a5, a4, 0, 5);
                }
            } else if (this.f3355k0 != -1.0f) {
                q4Var.mo15018a(com.fossil.blesdk.obfuscated.C2703q4.m12574a(q4Var, q4Var.mo15010a((java.lang.Object) this.f3358n0), q4Var.mo15010a((java.lang.Object) a), q4Var.mo15010a((java.lang.Object) a2), this.f3355k0, this.f3360p0));
            }
        }
    }
}
