package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x7 */
public final class C3265x7 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f10858a;

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Method f10859b;

    /*
    static {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                f10859b = java.lang.Class.forName("libcore.icu.ICU").getMethod("addLikelySubtags", new java.lang.Class[]{java.util.Locale.class});
            } catch (java.lang.Exception e) {
                throw new java.lang.IllegalStateException(e);
            }
        } else {
            try {
                java.lang.Class<?> cls = java.lang.Class.forName("libcore.icu.ICU");
                if (cls != null) {
                    f10858a = cls.getMethod("getScript", new java.lang.Class[]{java.lang.String.class});
                    f10859b = cls.getMethod("addLikelySubtags", new java.lang.Class[]{java.lang.String.class});
                }
            } catch (java.lang.Exception e2) {
                f10858a = null;
                f10859b = null;
                android.util.Log.w("ICUCompat", e2);
            }
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m16170a(java.lang.String str) {
        try {
            if (f10858a != null) {
                return (java.lang.String) f10858a.invoke((java.lang.Object) null, new java.lang.Object[]{str});
            }
        } catch (java.lang.IllegalAccessException e) {
            android.util.Log.w("ICUCompat", e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            android.util.Log.w("ICUCompat", e2);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String m16172b(java.util.Locale locale) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            try {
                return ((java.util.Locale) f10859b.invoke((java.lang.Object) null, new java.lang.Object[]{locale})).getScript();
            } catch (java.lang.reflect.InvocationTargetException e) {
                android.util.Log.w("ICUCompat", e);
                return locale.getScript();
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.w("ICUCompat", e2);
                return locale.getScript();
            }
        } else {
            java.lang.String a = m16171a(locale);
            if (a != null) {
                return m16170a(a);
            }
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m16171a(java.util.Locale locale) {
        java.lang.String locale2 = locale.toString();
        try {
            if (f10859b != null) {
                return (java.lang.String) f10859b.invoke((java.lang.Object) null, new java.lang.Object[]{locale2});
            }
        } catch (java.lang.IllegalAccessException e) {
            android.util.Log.w("ICUCompat", e);
        } catch (java.lang.reflect.InvocationTargetException e2) {
            android.util.Log.w("ICUCompat", e2);
        }
        return locale2;
    }
}
