package com.fossil.blesdk.obfuscated;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fl2 {
    @DexIgnore
    public static fl2 b;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public fl2() {
        WindowManager windowManager = (WindowManager) PortfolioApp.R.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.a = point.x;
            return;
        }
        this.a = 0;
    }

    @DexIgnore
    public static fl2 b() {
        if (b == null) {
            b = new fl2();
        }
        return b;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }
}
