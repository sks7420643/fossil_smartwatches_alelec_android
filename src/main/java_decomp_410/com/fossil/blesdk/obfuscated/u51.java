package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u51 extends vb1<u51> {
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public Boolean d; // = null;
    @DexIgnore
    public String e; // = null;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;

    @DexIgnore
    public u51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        Boolean bool = this.d;
        if (bool != null) {
            ub1.a(2, bool.booleanValue());
        }
        String str = this.e;
        if (str != null) {
            ub1.a(3, str);
        }
        String str2 = this.f;
        if (str2 != null) {
            ub1.a(4, str2);
        }
        String str3 = this.g;
        if (str3 != null) {
            ub1.a(5, str3);
        }
        super.a(ub1);
    }

    @DexIgnore
    public final u51 b(tb1 tb1) throws IOException {
        int e2;
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                try {
                    e2 = tb1.e();
                    if (e2 < 0 || e2 > 4) {
                        StringBuilder sb = new StringBuilder(46);
                        sb.append(e2);
                        sb.append(" is not a valid enum ComparisonType");
                    } else {
                        this.c = Integer.valueOf(e2);
                    }
                } catch (IllegalArgumentException unused) {
                    tb1.f(tb1.a());
                    a(tb1, c2);
                }
            } else if (c2 == 16) {
                this.d = Boolean.valueOf(tb1.d());
            } else if (c2 == 26) {
                this.e = tb1.b();
            } else if (c2 == 34) {
                this.f = tb1.b();
            } else if (c2 == 42) {
                this.g = tb1.b();
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
        StringBuilder sb2 = new StringBuilder(46);
        sb2.append(e2);
        sb2.append(" is not a valid enum ComparisonType");
        throw new IllegalArgumentException(sb2.toString());
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof u51)) {
            return false;
        }
        u51 u51 = (u51) obj;
        Integer num = this.c;
        if (num == null) {
            if (u51.c != null) {
                return false;
            }
        } else if (!num.equals(u51.c)) {
            return false;
        }
        Boolean bool = this.d;
        if (bool == null) {
            if (u51.d != null) {
                return false;
            }
        } else if (!bool.equals(u51.d)) {
            return false;
        }
        String str = this.e;
        if (str == null) {
            if (u51.e != null) {
                return false;
            }
        } else if (!str.equals(u51.e)) {
            return false;
        }
        String str2 = this.f;
        if (str2 == null) {
            if (u51.f != null) {
                return false;
            }
        } else if (!str2.equals(u51.f)) {
            return false;
        }
        String str3 = this.g;
        if (str3 == null) {
            if (u51.g != null) {
                return false;
            }
        } else if (!str3.equals(u51.g)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(u51.b);
        }
        xb1 xb12 = u51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (u51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int intValue = (hashCode + (num == null ? 0 : num.intValue())) * 31;
        Boolean bool = this.d;
        int hashCode2 = (intValue + (bool == null ? 0 : bool.hashCode())) * 31;
        String str = this.e;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.f;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        String str3 = this.g;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        Boolean bool = this.d;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(2) + 1;
        }
        String str = this.e;
        if (str != null) {
            a += ub1.b(3, str);
        }
        String str2 = this.f;
        if (str2 != null) {
            a += ub1.b(4, str2);
        }
        String str3 = this.g;
        return str3 != null ? a + ub1.b(5, str3) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        b(tb1);
        return this;
    }
}
