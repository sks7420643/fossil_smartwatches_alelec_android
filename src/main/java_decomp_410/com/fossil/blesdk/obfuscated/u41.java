package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u41 implements Parcelable.Creator<t41> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        kd1 kd1 = t41.i;
        List<jj0> list = t41.h;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                kd1 = (kd1) SafeParcelReader.a(parcel, a, kd1.CREATOR);
            } else if (a2 == 2) {
                list = SafeParcelReader.c(parcel, a, jj0.CREATOR);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                str = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new t41(kd1, list, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new t41[i];
    }
}
