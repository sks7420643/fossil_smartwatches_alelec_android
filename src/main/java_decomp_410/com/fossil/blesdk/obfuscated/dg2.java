package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dg2 extends cg2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        u.put(R.id.iv_close, 2);
        u.put(R.id.progress_update, 3);
        u.put(R.id.ftv_title, 4);
        u.put(R.id.ftv_contact_cs, 5);
        u.put(R.id.fb_try_again, 6);
    }
    */

    @DexIgnore
    public dg2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 7, t, u));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.s != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.s = 1;
        }
        g();
    }

    @DexIgnore
    public dg2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[6], objArr[5], objArr[4], objArr[2], objArr[3], objArr[1]);
        this.s = -1;
        this.r = objArr[0];
        this.r.setTag((Object) null);
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
