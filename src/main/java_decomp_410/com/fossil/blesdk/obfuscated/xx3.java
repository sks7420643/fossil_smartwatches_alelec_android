package com.fossil.blesdk.obfuscated;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xx3 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ hy3 e;
    @DexIgnore
    public /* final */ WeakReference<ImageView> f;
    @DexIgnore
    public ux3 g;

    @DexIgnore
    public xx3(hy3 hy3, ImageView imageView, ux3 ux3) {
        this.e = hy3;
        this.f = new WeakReference<>(imageView);
        this.g = ux3;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.g = null;
        ImageView imageView = (ImageView) this.f.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = (ImageView) this.f.get();
        if (imageView == null) {
            return true;
        }
        ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
        if (!viewTreeObserver.isAlive()) {
            return true;
        }
        int width = imageView.getWidth();
        int height = imageView.getHeight();
        if (width > 0 && height > 0) {
            viewTreeObserver.removeOnPreDrawListener(this);
            hy3 hy3 = this.e;
            hy3.c();
            hy3.a(width, height);
            hy3.a(imageView, this.g);
        }
        return true;
    }
}
