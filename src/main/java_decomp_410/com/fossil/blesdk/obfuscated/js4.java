package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class js4 implements gr4<em4, Float> {
    @DexIgnore
    public static /* final */ js4 a; // = new js4();

    @DexIgnore
    public Float a(em4 em4) throws IOException {
        return Float.valueOf(em4.F());
    }
}
