package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xc */
public class C3273xc {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xc$a */
    public static class C3274a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f10893a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.os.Bundle f10894b;
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.xc$b */
    public static class C3275b extends android.service.media.MediaBrowserService {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C3273xc.C3277d f10895e;

        @DexIgnore
        public C3275b(android.content.Context context, com.fossil.blesdk.obfuscated.C3273xc.C3277d dVar) {
            attachBaseContext(context);
            this.f10895e = dVar;
        }

        @DexIgnore
        public android.service.media.MediaBrowserService.BrowserRoot onGetRoot(java.lang.String str, int i, android.os.Bundle bundle) {
            android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
            com.fossil.blesdk.obfuscated.C3273xc.C3274a a = this.f10895e.mo17377a(str, i, bundle == null ? null : new android.os.Bundle(bundle));
            if (a == null) {
                return null;
            }
            return new android.service.media.MediaBrowserService.BrowserRoot(a.f10893a, a.f10894b);
        }

        @DexIgnore
        public void onLoadChildren(java.lang.String str, android.service.media.MediaBrowserService.Result<java.util.List<android.media.browse.MediaBrowser.MediaItem>> result) {
            this.f10895e.mo17378b(str, new com.fossil.blesdk.obfuscated.C3273xc.C3276c(result));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xc$c")
    /* renamed from: com.fossil.blesdk.obfuscated.xc$c */
    public static class C3276c<T> {

        @DexIgnore
        /* renamed from: a */
        public android.service.media.MediaBrowserService.Result f10896a;

        @DexIgnore
        public C3276c(android.service.media.MediaBrowserService.Result result) {
            this.f10896a = result;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17669a(T t) {
            if (t instanceof java.util.List) {
                this.f10896a.sendResult(mo17668a((java.util.List<android.os.Parcel>) (java.util.List) t));
            } else if (t instanceof android.os.Parcel) {
                android.os.Parcel parcel = (android.os.Parcel) t;
                parcel.setDataPosition(0);
                this.f10896a.sendResult(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            } else {
                this.f10896a.sendResult((java.lang.Object) null);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public java.util.List<android.media.browse.MediaBrowser.MediaItem> mo17668a(java.util.List<android.os.Parcel> list) {
            if (list == null) {
                return null;
            }
            java.util.ArrayList arrayList = new java.util.ArrayList();
            for (android.os.Parcel next : list) {
                next.setDataPosition(0);
                arrayList.add(android.media.browse.MediaBrowser.MediaItem.CREATOR.createFromParcel(next));
                next.recycle();
            }
            return arrayList;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.xc$d */
    public interface C3277d {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C3273xc.C3274a mo17377a(java.lang.String str, int i, android.os.Bundle bundle);

        @DexIgnore
        /* renamed from: b */
        void mo17378b(java.lang.String str, com.fossil.blesdk.obfuscated.C3273xc.C3276c<java.util.List<android.os.Parcel>> cVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m16234a(android.content.Context context, com.fossil.blesdk.obfuscated.C3273xc.C3277d dVar) {
        return new com.fossil.blesdk.obfuscated.C3273xc.C3275b(context, dVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16235a(java.lang.Object obj) {
        ((android.service.media.MediaBrowserService) obj).onCreate();
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.IBinder m16233a(java.lang.Object obj, android.content.Intent intent) {
        return ((android.service.media.MediaBrowserService) obj).onBind(intent);
    }
}
