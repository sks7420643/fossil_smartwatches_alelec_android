package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ky1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ hy1 e;
    @DexIgnore
    public /* final */ /* synthetic */ jy1 f;

    @DexIgnore
    public ky1(jy1 jy1, hy1 hy1) {
        this.f = jy1;
        this.e = hy1;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f.e.d(this.e.a);
        this.e.a();
    }
}
