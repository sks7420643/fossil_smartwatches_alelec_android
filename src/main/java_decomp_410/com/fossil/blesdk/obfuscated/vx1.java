package com.fossil.blesdk.obfuscated;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class vx1 implements pn1 {
    @DexIgnore
    public /* final */ ux1 a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public vx1(ux1 ux1, Pair pair) {
        this.a = ux1;
        this.b = pair;
    }

    @DexIgnore
    public final Object then(wn1 wn1) {
        this.a.a(this.b, wn1);
        return wn1;
    }
}
