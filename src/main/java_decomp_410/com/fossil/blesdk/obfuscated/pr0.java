package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pr0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ mr0 e;
    @DexIgnore
    public /* final */ /* synthetic */ or0 f;

    @DexIgnore
    public pr0(or0 or0, mr0 mr0) {
        this.f = or0;
        this.e = mr0;
    }

    @DexIgnore
    public final void run() {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "bg processing of the intent starting now");
        }
        this.f.e.handleIntent(this.e.a);
        this.e.a();
    }
}
