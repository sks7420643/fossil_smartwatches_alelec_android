package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ed2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ FossilCircleImageView G;
    @DexIgnore
    public /* final */ RecyclerView H;
    @DexIgnore
    public /* final */ AutoResizeTextView I;
    @DexIgnore
    public /* final */ FlexibleTextView J;
    @DexIgnore
    public /* final */ AutoResizeTextView K;
    @DexIgnore
    public /* final */ FlexibleTextView L;
    @DexIgnore
    public /* final */ AutoResizeTextView M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ FlexibleTextView P;
    @DexIgnore
    public /* final */ FlexibleTextView Q;
    @DexIgnore
    public /* final */ FlexibleTextView R;
    @DexIgnore
    public /* final */ FlexibleTextView S;
    @DexIgnore
    public /* final */ FlexibleTextView T;
    @DexIgnore
    public /* final */ FlexibleTextView U;
    @DexIgnore
    public /* final */ View V;
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ ConstraintLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ed2(Object obj, View view, int i, Barrier barrier, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, Barrier barrier2, Guideline guideline, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, FlexibleTextView flexibleTextView11, RTLImageView rTLImageView, FossilCircleImageView fossilCircleImageView, FossilCircleImageView fossilCircleImageView2, LinearLayout linearLayout, RecyclerView recyclerView, AutoResizeTextView autoResizeTextView, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, AutoResizeTextView autoResizeTextView2, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, AutoResizeTextView autoResizeTextView3, FlexibleTextView flexibleTextView16, FlexibleTextView flexibleTextView17, FlexibleTextView flexibleTextView18, FlexibleTextView flexibleTextView19, FlexibleTextView flexibleTextView20, FlexibleTextView flexibleTextView21, FlexibleTextView flexibleTextView22, FlexibleTextView flexibleTextView23, FlexibleTextView flexibleTextView24, FlexibleTextView flexibleTextView25, FlexibleTextView flexibleTextView26, FlexibleTextView flexibleTextView27, View view2, Guideline guideline2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = flexibleTextView3;
        this.t = flexibleTextView4;
        this.u = flexibleTextView5;
        this.v = flexibleButton;
        this.w = flexibleTextView6;
        this.x = flexibleTextView7;
        this.y = constraintLayout;
        this.z = constraintLayout2;
        this.A = constraintLayout6;
        this.B = flexibleTextView8;
        this.C = flexibleTextView9;
        this.D = flexibleTextView10;
        this.E = flexibleTextView11;
        this.F = rTLImageView;
        this.G = fossilCircleImageView;
        this.H = recyclerView;
        this.I = autoResizeTextView;
        this.J = flexibleTextView12;
        this.K = autoResizeTextView2;
        this.L = flexibleTextView14;
        this.M = autoResizeTextView3;
        this.N = flexibleTextView16;
        this.O = flexibleTextView18;
        this.P = flexibleTextView19;
        this.Q = flexibleTextView21;
        this.R = flexibleTextView22;
        this.S = flexibleTextView23;
        this.T = flexibleTextView24;
        this.U = flexibleTextView27;
        this.V = view2;
    }
}
