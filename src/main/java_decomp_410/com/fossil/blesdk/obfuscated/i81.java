package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i81 {
    @DexIgnore
    public static volatile i81 b;
    @DexIgnore
    public static /* final */ i81 c; // = new i81(true);
    @DexIgnore
    public /* final */ Map<a, t81.d<?, ?>> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }
    }

    /*
    static {
        b();
    }
    */

    @DexIgnore
    public i81() {
        this.a = new HashMap();
    }

    @DexIgnore
    public static i81 a() {
        return r81.a(i81.class);
    }

    @DexIgnore
    public static Class<?> b() {
        try {
            return Class.forName("com.google.protobuf.Extension");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static i81 c() {
        return h81.b();
    }

    @DexIgnore
    public static i81 d() {
        i81 i81 = b;
        if (i81 == null) {
            synchronized (i81.class) {
                i81 = b;
                if (i81 == null) {
                    i81 = h81.c();
                    b = i81;
                }
            }
        }
        return i81;
    }

    @DexIgnore
    public final <ContainingType extends w91> t81.d<ContainingType, ?> a(ContainingType containingtype, int i) {
        return this.a.get(new a(containingtype, i));
    }

    @DexIgnore
    public i81(boolean z) {
        this.a = Collections.emptyMap();
    }
}
