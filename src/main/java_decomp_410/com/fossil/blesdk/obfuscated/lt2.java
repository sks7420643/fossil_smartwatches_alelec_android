package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lt2 extends le.d<DailyHeartRateSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        kd4.b(dailyHeartRateSummary, "oldItem");
        kd4.b(dailyHeartRateSummary2, "newItem");
        return kd4.a((Object) dailyHeartRateSummary, (Object) dailyHeartRateSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        kd4.b(dailyHeartRateSummary, "oldItem");
        kd4.b(dailyHeartRateSummary2, "newItem");
        return rk2.d(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
