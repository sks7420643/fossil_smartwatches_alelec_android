package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hy {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new HashMap();

    @DexIgnore
    public hy(String str) {
        this.a = str;
    }

    @DexIgnore
    public hy a(String str, String str2) {
        this.b.put(str, str2);
        return this;
    }

    @DexIgnore
    public mx a() {
        mx mxVar = new mx(this.a);
        for (String next : this.b.keySet()) {
            Object obj = this.b.get(next);
            if (obj instanceof String) {
                mxVar.a(next, (String) obj);
            } else if (obj instanceof Number) {
                mxVar.a(next, (Number) obj);
            }
        }
        return mxVar;
    }
}
