package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.os.Trace;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ar0 implements Closeable {
    @DexIgnore
    public static /* final */ ir0<Boolean> f; // = hr0.a().a("nts.enable_tracing", true);
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    @TargetApi(18)
    public ar0(String str) {
        this.e = pm0.d() && f.get().booleanValue();
        if (this.e) {
            Trace.beginSection(str.length() > 127 ? str.substring(0, 127) : str);
        }
    }

    @DexIgnore
    @TargetApi(18)
    public final void close() {
        if (this.e) {
            Trace.endSection();
        }
    }
}
