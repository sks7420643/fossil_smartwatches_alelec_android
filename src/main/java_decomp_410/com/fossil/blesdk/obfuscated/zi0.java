package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zi0<T> extends je0, Iterable<T> {
    @DexIgnore
    T get(int i);

    @DexIgnore
    int getCount();
}
