package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kv */
public class C2250kv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2250kv.C2251a<?>> f7018a; // = new java.util.ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kv$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kv$a */
    public static final class C2251a<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Class<T> f7019a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2498no<T> f7020b;

        @DexIgnore
        public C2251a(java.lang.Class<T> cls, com.fossil.blesdk.obfuscated.C2498no<T> noVar) {
            this.f7019a = cls;
            this.f7020b = noVar;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo12958a(java.lang.Class<?> cls) {
            return this.f7019a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Z> void mo12957a(java.lang.Class<Z> cls, com.fossil.blesdk.obfuscated.C2498no<Z> noVar) {
        this.f7018a.add(new com.fossil.blesdk.obfuscated.C2250kv.C2251a(cls, noVar));
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <Z> com.fossil.blesdk.obfuscated.C2498no<Z> mo12956a(java.lang.Class<Z> cls) {
        int size = this.f7018a.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C2250kv.C2251a aVar = this.f7018a.get(i);
            if (aVar.mo12958a(cls)) {
                return aVar.f7020b;
            }
        }
        return null;
    }
}
