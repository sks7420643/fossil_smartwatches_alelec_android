package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ri */
public abstract class C2810ri extends android.graphics.drawable.Drawable implements com.fossil.blesdk.obfuscated.C1603d7 {

    @DexIgnore
    /* renamed from: e */
    public android.graphics.drawable.Drawable f8987e;

    @DexIgnore
    public void applyTheme(android.content.res.Resources.Theme theme) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5300a(drawable, theme);
        }
    }

    @DexIgnore
    public void clearColorFilter() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    @DexIgnore
    public android.graphics.ColorFilter getColorFilter() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return com.fossil.blesdk.obfuscated.C1538c7.m5309d(drawable);
        }
        return null;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getCurrent() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getCurrent();
        }
        return super.getCurrent();
    }

    @DexIgnore
    public int getMinimumHeight() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getMinimumHeight();
        }
        return super.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getMinimumWidth();
        }
        return super.getMinimumWidth();
    }

    @DexIgnore
    public boolean getPadding(android.graphics.Rect rect) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getPadding(rect);
        }
        return super.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getState();
        }
        return super.getState();
    }

    @DexIgnore
    public android.graphics.Region getTransparentRegion() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.getTransparentRegion();
        }
        return super.getTransparentRegion();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5312g(drawable);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setLevel(i);
        }
        return super.onLevelChange(i);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    @DexIgnore
    public void setColorFilter(int i, android.graphics.PorterDuff.Mode mode) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            drawable.setFilterBitmap(z);
        }
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5297a(drawable, f, f2);
        }
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            com.fossil.blesdk.obfuscated.C1538c7.m5298a(drawable, i, i2, i3, i4);
        }
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        android.graphics.drawable.Drawable drawable = this.f8987e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return super.setState(iArr);
    }
}
