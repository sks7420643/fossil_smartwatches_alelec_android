package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzyh;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yb1 implements Cloneable {
    @DexIgnore
    public wb1<?, ?> e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public List<cc1> g; // = new ArrayList();

    @DexIgnore
    public final void a(cc1 cc1) throws IOException {
        List<cc1> list = this.g;
        if (list != null) {
            list.add(cc1);
            return;
        }
        Object obj = this.f;
        if (obj instanceof ac1) {
            byte[] bArr = cc1.b;
            tb1 a = tb1.a(bArr, 0, bArr.length);
            int e2 = a.e();
            if (e2 == bArr.length - ub1.d(e2)) {
                ac1 a2 = ((ac1) this.f).a(a);
                this.e = this.e;
                this.f = a2;
                this.g = null;
                return;
            }
            throw zzyh.zzzd();
        } else if (obj instanceof ac1[]) {
            this.e.a((List<cc1>) Collections.singletonList(cc1));
            throw null;
        } else if (obj instanceof w91) {
            this.e.a((List<cc1>) Collections.singletonList(cc1));
            throw null;
        } else if (obj instanceof w91[]) {
            this.e.a((List<cc1>) Collections.singletonList(cc1));
            throw null;
        } else {
            this.e.a((List<cc1>) Collections.singletonList(cc1));
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final yb1 clone() {
        yb1 yb1 = new yb1();
        try {
            yb1.e = this.e;
            if (this.g == null) {
                yb1.g = null;
            } else {
                yb1.g.addAll(this.g);
            }
            if (this.f != null) {
                if (this.f instanceof ac1) {
                    yb1.f = (ac1) ((ac1) this.f).clone();
                } else if (this.f instanceof byte[]) {
                    yb1.f = ((byte[]) this.f).clone();
                } else {
                    int i = 0;
                    if (this.f instanceof byte[][]) {
                        byte[][] bArr = (byte[][]) this.f;
                        byte[][] bArr2 = new byte[bArr.length][];
                        yb1.f = bArr2;
                        while (i < bArr.length) {
                            bArr2[i] = (byte[]) bArr[i].clone();
                            i++;
                        }
                    } else if (this.f instanceof boolean[]) {
                        yb1.f = ((boolean[]) this.f).clone();
                    } else if (this.f instanceof int[]) {
                        yb1.f = ((int[]) this.f).clone();
                    } else if (this.f instanceof long[]) {
                        yb1.f = ((long[]) this.f).clone();
                    } else if (this.f instanceof float[]) {
                        yb1.f = ((float[]) this.f).clone();
                    } else if (this.f instanceof double[]) {
                        yb1.f = ((double[]) this.f).clone();
                    } else if (this.f instanceof ac1[]) {
                        ac1[] ac1Arr = (ac1[]) this.f;
                        ac1[] ac1Arr2 = new ac1[ac1Arr.length];
                        yb1.f = ac1Arr2;
                        while (i < ac1Arr.length) {
                            ac1Arr2[i] = (ac1) ac1Arr[i].clone();
                            i++;
                        }
                    }
                }
            }
            return yb1;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yb1)) {
            return false;
        }
        yb1 yb1 = (yb1) obj;
        if (this.f == null || yb1.f == null) {
            List<cc1> list = this.g;
            if (list != null) {
                List<cc1> list2 = yb1.g;
                if (list2 != null) {
                    return list.equals(list2);
                }
            }
            try {
                return Arrays.equals(a(), yb1.a());
            } catch (IOException e2) {
                throw new IllegalStateException(e2);
            }
        } else {
            wb1<?, ?> wb1 = this.e;
            if (wb1 != yb1.e) {
                return false;
            }
            if (!wb1.a.isArray()) {
                return this.f.equals(yb1.f);
            }
            Object obj2 = this.f;
            if (obj2 instanceof byte[]) {
                return Arrays.equals((byte[]) obj2, (byte[]) yb1.f);
            }
            if (obj2 instanceof int[]) {
                return Arrays.equals((int[]) obj2, (int[]) yb1.f);
            }
            if (obj2 instanceof long[]) {
                return Arrays.equals((long[]) obj2, (long[]) yb1.f);
            }
            if (obj2 instanceof float[]) {
                return Arrays.equals((float[]) obj2, (float[]) yb1.f);
            }
            if (obj2 instanceof double[]) {
                return Arrays.equals((double[]) obj2, (double[]) yb1.f);
            }
            if (obj2 instanceof boolean[]) {
                return Arrays.equals((boolean[]) obj2, (boolean[]) yb1.f);
            }
            return Arrays.deepEquals((Object[]) obj2, (Object[]) yb1.f);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return Arrays.hashCode(a()) + 527;
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public final int zzf() {
        Object obj = this.f;
        if (obj != null) {
            wb1<?, ?> wb1 = this.e;
            if (wb1.b) {
                int length = Array.getLength(obj);
                int i = 0;
                while (i < length) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 == null) {
                        i++;
                    } else {
                        wb1.a(obj2);
                        throw null;
                    }
                }
                return 0;
            }
            wb1.a(obj);
            throw null;
        }
        int i2 = 0;
        for (cc1 next : this.g) {
            i2 += ub1.e(next.a) + 0 + next.b.length;
        }
        return i2;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Object obj = this.f;
        if (obj != null) {
            wb1<?, ?> wb1 = this.e;
            if (wb1.b) {
                int length = Array.getLength(obj);
                int i = 0;
                while (i < length) {
                    Object obj2 = Array.get(obj, i);
                    if (obj2 == null) {
                        i++;
                    } else {
                        wb1.a(obj2, ub1);
                        throw null;
                    }
                }
                return;
            }
            wb1.a(obj, ub1);
            throw null;
        }
        for (cc1 next : this.g) {
            ub1.b(next.a);
            ub1.a(next.b);
        }
    }

    @DexIgnore
    public final byte[] a() throws IOException {
        byte[] bArr = new byte[zzf()];
        a(ub1.b(bArr));
        return bArr;
    }
}
