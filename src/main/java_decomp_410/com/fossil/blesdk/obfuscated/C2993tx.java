package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tx */
public class C2993tx {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.Set<java.lang.String> f9780a; // = new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"app_clear_data", "app_exception", "app_remove", "app_upgrade", "app_install", "app_update", "firebase_campaign", "error", "first_open", "first_visit", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement", "ad_exposure", "adunit_exposure", "ad_query", "ad_activeview", "ad_impression", "ad_click", "screen_view", "firebase_extra_parameter"}));

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2926sx mo16586a(com.crashlytics.android.answers.SessionEvent sessionEvent) {
        android.os.Bundle bundle;
        java.lang.String str;
        boolean z = true;
        boolean z2 = com.crashlytics.android.answers.SessionEvent.Type.CUSTOM.equals(sessionEvent.f2166c) && sessionEvent.f2168e != null;
        boolean z3 = com.crashlytics.android.answers.SessionEvent.Type.PREDEFINED.equals(sessionEvent.f2166c) && sessionEvent.f2170g != null;
        if (!z2 && !z3) {
            return null;
        }
        if (z3) {
            bundle = mo16595b(sessionEvent);
        } else {
            bundle = new android.os.Bundle();
            java.util.Map<java.lang.String, java.lang.Object> map = sessionEvent.f2169f;
            if (map != null) {
                mo16594a(bundle, map);
            }
        }
        if (z3) {
            java.lang.String str2 = (java.lang.String) sessionEvent.f2171h.get("success");
            if (str2 == null || java.lang.Boolean.parseBoolean(str2)) {
                z = false;
            }
            str = mo16589a(sessionEvent.f2170g, z);
        } else {
            str = mo16598c(sessionEvent.f2168e);
        }
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Logging event into firebase...");
        return new com.fossil.blesdk.obfuscated.C2926sx(str, bundle);
    }

    @DexIgnore
    /* renamed from: b */
    public final android.os.Bundle mo16595b(com.crashlytics.android.answers.SessionEvent sessionEvent) {
        android.os.Bundle bundle = new android.os.Bundle();
        if ("purchase".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "item_id", (java.lang.String) sessionEvent.f2171h.get("itemId"));
            mo16593a(bundle, "item_name", (java.lang.String) sessionEvent.f2171h.get("itemName"));
            mo16593a(bundle, "item_category", (java.lang.String) sessionEvent.f2171h.get("itemType"));
            mo16590a(bundle, "value", mo16596b(sessionEvent.f2171h.get("itemPrice")));
            mo16593a(bundle, "currency", (java.lang.String) sessionEvent.f2171h.get("currency"));
        } else if ("addToCart".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "item_id", (java.lang.String) sessionEvent.f2171h.get("itemId"));
            mo16593a(bundle, "item_name", (java.lang.String) sessionEvent.f2171h.get("itemName"));
            mo16593a(bundle, "item_category", (java.lang.String) sessionEvent.f2171h.get("itemType"));
            mo16590a(bundle, "price", mo16596b(sessionEvent.f2171h.get("itemPrice")));
            mo16590a(bundle, "value", mo16596b(sessionEvent.f2171h.get("itemPrice")));
            mo16593a(bundle, "currency", (java.lang.String) sessionEvent.f2171h.get("currency"));
            bundle.putLong("quantity", 1);
        } else if ("startCheckout".equals(sessionEvent.f2170g)) {
            mo16592a(bundle, "quantity", java.lang.Long.valueOf((long) ((java.lang.Integer) sessionEvent.f2171h.get("itemCount")).intValue()));
            mo16590a(bundle, "value", mo16596b(sessionEvent.f2171h.get("totalPrice")));
            mo16593a(bundle, "currency", (java.lang.String) sessionEvent.f2171h.get("currency"));
        } else if ("contentView".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "content_type", (java.lang.String) sessionEvent.f2171h.get("contentType"));
            mo16593a(bundle, "item_id", (java.lang.String) sessionEvent.f2171h.get("contentId"));
            mo16593a(bundle, "item_name", (java.lang.String) sessionEvent.f2171h.get("contentName"));
        } else if ("search".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "search_term", (java.lang.String) sessionEvent.f2171h.get(com.zendesk.sdk.network.impl.ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME));
        } else if (com.facebook.share.widget.ShareDialog.WEB_SHARE_DIALOG.equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "method", (java.lang.String) sessionEvent.f2171h.get("method"));
            mo16593a(bundle, "content_type", (java.lang.String) sessionEvent.f2171h.get("contentType"));
            mo16593a(bundle, "item_id", (java.lang.String) sessionEvent.f2171h.get("contentId"));
            mo16593a(bundle, "item_name", (java.lang.String) sessionEvent.f2171h.get("contentName"));
        } else if ("rating".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "rating", java.lang.String.valueOf(sessionEvent.f2171h.get("rating")));
            mo16593a(bundle, "content_type", (java.lang.String) sessionEvent.f2171h.get("contentType"));
            mo16593a(bundle, "item_id", (java.lang.String) sessionEvent.f2171h.get("contentId"));
            mo16593a(bundle, "item_name", (java.lang.String) sessionEvent.f2171h.get("contentName"));
        } else if ("signUp".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "method", (java.lang.String) sessionEvent.f2171h.get("method"));
        } else if ("login".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "method", (java.lang.String) sessionEvent.f2171h.get("method"));
        } else if ("invite".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "method", (java.lang.String) sessionEvent.f2171h.get("method"));
        } else if ("levelStart".equals(sessionEvent.f2170g)) {
            mo16593a(bundle, "level_name", (java.lang.String) sessionEvent.f2171h.get("levelName"));
        } else if ("levelEnd".equals(sessionEvent.f2170g)) {
            mo16590a(bundle, "score", mo16587a(sessionEvent.f2171h.get("score")));
            mo16593a(bundle, "level_name", (java.lang.String) sessionEvent.f2171h.get("levelName"));
            mo16591a(bundle, "success", mo16597b((java.lang.String) sessionEvent.f2171h.get("success")));
        }
        mo16594a(bundle, sessionEvent.f2169f);
        return bundle;
    }

    @DexIgnore
    /* renamed from: c */
    public final java.lang.String mo16598c(java.lang.String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_event";
        }
        if (f9780a.contains(str)) {
            return "fabric_" + str;
        }
        java.lang.String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !java.lang.Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo16588a(java.lang.String str) {
        if (str == null || str.length() == 0) {
            return "fabric_unnamed_parameter";
        }
        java.lang.String replaceAll = str.replaceAll("[^\\p{Alnum}_]+", "_");
        if (replaceAll.startsWith("ga_") || replaceAll.startsWith("google_") || replaceAll.startsWith("firebase_") || !java.lang.Character.isLetter(replaceAll.charAt(0))) {
            replaceAll = "fabric_" + replaceAll;
        }
        return replaceAll.length() > 40 ? replaceAll.substring(0, 40) : replaceAll;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        if (r11.equals("purchase") != false) goto L_0x00ca;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[RETURN] */
    /* renamed from: a */
    public final java.lang.String mo16589a(java.lang.String str, boolean z) {
        char c;
        char c2 = 0;
        if (z) {
            int hashCode = str.hashCode();
            if (hashCode != -902468296) {
                if (hashCode != 103149417) {
                    if (hashCode == 1743324417 && str.equals("purchase")) {
                        c = 0;
                        if (c != 0) {
                            return "failed_ecommerce_purchase";
                        }
                        if (c == 1) {
                            return "failed_sign_up";
                        }
                        if (c == 2) {
                            return "failed_login";
                        }
                    }
                } else if (str.equals("login")) {
                    c = 2;
                    if (c != 0) {
                    }
                }
            } else if (str.equals("signUp")) {
                c = 1;
                if (c != 0) {
                }
            }
            c = 65535;
            if (c != 0) {
            }
        }
        switch (str.hashCode()) {
            case -2131650889:
                if (str.equals("levelEnd")) {
                    c2 = 11;
                    break;
                }
            case -1183699191:
                if (str.equals("invite")) {
                    c2 = 9;
                    break;
                }
            case -938102371:
                if (str.equals("rating")) {
                    c2 = 6;
                    break;
                }
            case -906336856:
                if (str.equals("search")) {
                    c2 = 4;
                    break;
                }
            case -902468296:
                if (str.equals("signUp")) {
                    c2 = 7;
                    break;
                }
            case -389087554:
                if (str.equals("contentView")) {
                    c2 = 3;
                    break;
                }
            case 23457852:
                if (str.equals("addToCart")) {
                    c2 = 1;
                    break;
                }
            case 103149417:
                if (str.equals("login")) {
                    c2 = 8;
                    break;
                }
            case 109400031:
                if (str.equals(com.facebook.share.widget.ShareDialog.WEB_SHARE_DIALOG)) {
                    c2 = 5;
                    break;
                }
            case 196004670:
                if (str.equals("levelStart")) {
                    c2 = 10;
                    break;
                }
            case 1664021448:
                if (str.equals("startCheckout")) {
                    c2 = 2;
                    break;
                }
            case 1743324417:
                break;
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                return "ecommerce_purchase";
            case 1:
                return "add_to_cart";
            case 2:
                return "begin_checkout";
            case 3:
                return "select_content";
            case 4:
                return "search";
            case 5:
                return com.facebook.share.widget.ShareDialog.WEB_SHARE_DIALOG;
            case 6:
                return "rate_content";
            case 7:
                return "sign_up";
            case 8:
                return "login";
            case 9:
                return "invite";
            case 10:
                return "level_start";
            case 11:
                return "level_end";
            default:
                return mo16598c(str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16592a(android.os.Bundle bundle, java.lang.String str, java.lang.Long l) {
        if (l != null) {
            bundle.putLong(str, l.longValue());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16591a(android.os.Bundle bundle, java.lang.String str, java.lang.Integer num) {
        if (num != null) {
            bundle.putInt(str, num.intValue());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16593a(android.os.Bundle bundle, java.lang.String str, java.lang.String str2) {
        if (str2 != null) {
            bundle.putString(str, str2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16590a(android.os.Bundle bundle, java.lang.String str, java.lang.Double d) {
        java.lang.Double a = mo16587a((java.lang.Object) d);
        if (a != null) {
            bundle.putDouble(str, a.doubleValue());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.Double mo16587a(java.lang.Object obj) {
        java.lang.String valueOf = java.lang.String.valueOf(obj);
        if (valueOf == null) {
            return null;
        }
        return java.lang.Double.valueOf(valueOf);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16594a(android.os.Bundle bundle, java.util.Map<java.lang.String, java.lang.Object> map) {
        for (java.util.Map.Entry next : map.entrySet()) {
            java.lang.Object value = next.getValue();
            java.lang.String a = mo16588a((java.lang.String) next.getKey());
            if (value instanceof java.lang.String) {
                bundle.putString(a, next.getValue().toString());
            } else if (value instanceof java.lang.Double) {
                bundle.putDouble(a, ((java.lang.Double) next.getValue()).doubleValue());
            } else if (value instanceof java.lang.Long) {
                bundle.putLong(a, ((java.lang.Long) next.getValue()).longValue());
            } else if (value instanceof java.lang.Integer) {
                bundle.putInt(a, ((java.lang.Integer) next.getValue()).intValue());
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.Integer mo16597b(java.lang.String str) {
        if (str == null) {
            return null;
        }
        return java.lang.Integer.valueOf(str.equals(com.facebook.internal.ServerProtocol.DIALOG_RETURN_SCOPES_TRUE) ? 1 : 0);
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.Double mo16596b(java.lang.Object obj) {
        java.lang.Long l = (java.lang.Long) obj;
        if (l == null) {
            return null;
        }
        return java.lang.Double.valueOf(new java.math.BigDecimal(l.longValue()).divide(com.fossil.blesdk.obfuscated.C1450ax.f3619c).doubleValue());
    }
}
