package com.fossil.blesdk.obfuscated;

import com.android.volley.Request;
import com.android.volley.VolleyError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vm {
    @DexIgnore
    void a(Request<?> request, VolleyError volleyError);

    @DexIgnore
    void a(Request<?> request, um<?> umVar);

    @DexIgnore
    void a(Request<?> request, um<?> umVar, Runnable runnable);
}
