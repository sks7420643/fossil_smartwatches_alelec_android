package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a03 extends v52<zz2> {
    @DexIgnore
    void a(ArrayList<String> arrayList);

    @DexIgnore
    void a(List<AppWrapper> list, int i);

    @DexIgnore
    void d();

    @DexIgnore
    void e();
}
