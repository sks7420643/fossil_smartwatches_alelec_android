package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface z81<E> extends List<E>, RandomAccess {
    @DexIgnore
    boolean A();

    @DexIgnore
    void B();

    @DexIgnore
    z81<E> b(int i);
}
