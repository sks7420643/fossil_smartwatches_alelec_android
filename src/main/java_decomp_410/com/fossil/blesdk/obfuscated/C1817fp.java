package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fp */
public class C1817fp implements com.fossil.blesdk.obfuscated.C2902so<java.io.InputStream> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.net.Uri f5238e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1964hp f5239f;

    @DexIgnore
    /* renamed from: g */
    public java.io.InputStream f5240g;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fp$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fp$a */
    public static class C1818a implements com.fossil.blesdk.obfuscated.C1884gp {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ java.lang.String[] f5241b; // = {"_data"};

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ContentResolver f5242a;

        @DexIgnore
        public C1818a(android.content.ContentResolver contentResolver) {
            this.f5242a = contentResolver;
        }

        @DexIgnore
        /* renamed from: a */
        public android.database.Cursor mo10997a(android.net.Uri uri) {
            java.lang.String lastPathSegment = uri.getLastPathSegment();
            return this.f5242a.query(android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, f5241b, "kind = 1 AND image_id = ?", new java.lang.String[]{lastPathSegment}, (java.lang.String) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fp$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fp$b */
    public static class C1819b implements com.fossil.blesdk.obfuscated.C1884gp {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ java.lang.String[] f5243b; // = {"_data"};

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ContentResolver f5244a;

        @DexIgnore
        public C1819b(android.content.ContentResolver contentResolver) {
            this.f5244a = contentResolver;
        }

        @DexIgnore
        /* renamed from: a */
        public android.database.Cursor mo10997a(android.net.Uri uri) {
            java.lang.String lastPathSegment = uri.getLastPathSegment();
            return this.f5244a.query(android.provider.MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, f5243b, "kind = 1 AND video_id = ?", new java.lang.String[]{lastPathSegment}, (java.lang.String) null);
        }
    }

    @DexIgnore
    public C1817fp(android.net.Uri uri, com.fossil.blesdk.obfuscated.C1964hp hpVar) {
        this.f5238e = uri;
        this.f5239f = hpVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1817fp m7092a(android.content.Context context, android.net.Uri uri) {
        return m7093a(context, uri, new com.fossil.blesdk.obfuscated.C1817fp.C1818a(context.getContentResolver()));
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1817fp m7094b(android.content.Context context, android.net.Uri uri) {
        return m7093a(context, uri, new com.fossil.blesdk.obfuscated.C1817fp.C1819b(context.getContentResolver()));
    }

    @DexIgnore
    /* renamed from: c */
    public final java.io.InputStream mo10996c() throws java.io.FileNotFoundException {
        java.io.InputStream c = this.f5239f.mo11703c(this.f5238e);
        int a = c != null ? this.f5239f.mo11700a(this.f5238e) : -1;
        return a != -1 ? new com.fossil.blesdk.obfuscated.C3127vo(c, a) : c;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public java.lang.Class<java.io.InputStream> getDataClass() {
        return java.io.InputStream.class;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1817fp m7093a(android.content.Context context, android.net.Uri uri, com.fossil.blesdk.obfuscated.C1884gp gpVar) {
        return new com.fossil.blesdk.obfuscated.C1817fp(uri, new com.fossil.blesdk.obfuscated.C1964hp(com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15649g().mo3929a(), gpVar, com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15643b(), context.getContentResolver()));
    }

    @DexIgnore
    /* renamed from: b */
    public com.bumptech.glide.load.DataSource mo8872b() {
        return com.bumptech.glide.load.DataSource.LOCAL;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super java.io.InputStream> aVar) {
        try {
            this.f5240g = mo10996c();
            aVar.mo9252a(this.f5240g);
        } catch (java.io.FileNotFoundException e) {
            if (android.util.Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                android.util.Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.mo9251a((java.lang.Exception) e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8869a() {
        java.io.InputStream inputStream = this.f5240g;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (java.io.IOException unused) {
            }
        }
    }
}
