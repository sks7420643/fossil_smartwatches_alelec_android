package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.nu0;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ku0<FieldDescriptorType extends nu0<FieldDescriptorType>> {
    @DexIgnore
    public static /* final */ ku0 d; // = new ku0(true);
    @DexIgnore
    public /* final */ mw0<FieldDescriptorType, Object> a; // = mw0.c(16);
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c; // = false;

    @DexIgnore
    public ku0() {
    }

    @DexIgnore
    public ku0(boolean z) {
        h();
    }

    @DexIgnore
    public static int a(zzfl zzfl, int i, Object obj) {
        int e = zzbn.e(i);
        if (zzfl == zzfl.GROUP) {
            tu0.a((sv0) obj);
            e <<= 1;
        }
        return e + b(zzfl, obj);
    }

    @DexIgnore
    public static Object a(Object obj) {
        if (obj instanceof yv0) {
            return ((yv0) obj).q();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if ((r3 instanceof com.fossil.blesdk.obfuscated.xu0) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if ((r3 instanceof com.fossil.blesdk.obfuscated.uu0) == false) goto L_0x0043;
     */
    @DexIgnore
    public static void a(zzfl zzfl, Object obj) {
        boolean z;
        tu0.a(obj);
        boolean z2 = false;
        switch (lu0.a[zzfl.zzek().ordinal()]) {
            case 1:
                z = obj instanceof Integer;
                break;
            case 2:
                z = obj instanceof Long;
                break;
            case 3:
                z = obj instanceof Float;
                break;
            case 4:
                z = obj instanceof Double;
                break;
            case 5:
                z = obj instanceof Boolean;
                break;
            case 6:
                z = obj instanceof String;
                break;
            case 7:
                if (!(obj instanceof zzbb)) {
                    break;
                }
            case 8:
                if (!(obj instanceof Integer)) {
                    break;
                }
            case 9:
                if (!(obj instanceof sv0)) {
                    break;
                }
        }
        z2 = z;
        if (!z2) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    @DexIgnore
    public static int b(nu0<?> nu0, Object obj) {
        zzfl d2 = nu0.d();
        int zzc = nu0.zzc();
        if (!nu0.c()) {
            return a(d2, zzc, obj);
        }
        int i = 0;
        List<Object> list = (List) obj;
        if (nu0.a()) {
            for (Object b2 : list) {
                i += b(d2, b2);
            }
            return zzbn.e(zzc) + i + zzbn.m(i);
        }
        for (Object a2 : list) {
            i += a(d2, zzc, a2);
        }
        return i;
    }

    @DexIgnore
    public static int b(zzfl zzfl, Object obj) {
        switch (lu0.b[zzfl.ordinal()]) {
            case 1:
                return zzbn.b(((Double) obj).doubleValue());
            case 2:
                return zzbn.b(((Float) obj).floatValue());
            case 3:
                return zzbn.d(((Long) obj).longValue());
            case 4:
                return zzbn.e(((Long) obj).longValue());
            case 5:
                return zzbn.f(((Integer) obj).intValue());
            case 6:
                return zzbn.g(((Long) obj).longValue());
            case 7:
                return zzbn.i(((Integer) obj).intValue());
            case 8:
                return zzbn.b(((Boolean) obj).booleanValue());
            case 9:
                return zzbn.b((sv0) obj);
            case 10:
                return obj instanceof xu0 ? zzbn.a((bv0) (xu0) obj) : zzbn.a((sv0) obj);
            case 11:
                return obj instanceof zzbb ? zzbn.a((zzbb) obj) : zzbn.a((String) obj);
            case 12:
                return obj instanceof zzbb ? zzbn.a((zzbb) obj) : zzbn.b((byte[]) obj);
            case 13:
                return zzbn.g(((Integer) obj).intValue());
            case 14:
                return zzbn.j(((Integer) obj).intValue());
            case 15:
                return zzbn.h(((Long) obj).longValue());
            case 16:
                return zzbn.h(((Integer) obj).intValue());
            case 17:
                return zzbn.f(((Long) obj).longValue());
            case 18:
                return obj instanceof uu0 ? zzbn.k(((uu0) obj).zzc()) : zzbn.k(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    @DexIgnore
    public static boolean b(Map.Entry<FieldDescriptorType, Object> entry) {
        nu0 nu0 = (nu0) entry.getKey();
        if (nu0.b() == zzfq.MESSAGE) {
            boolean c2 = nu0.c();
            Object value = entry.getValue();
            if (c2) {
                for (sv0 a2 : (List) value) {
                    if (!a2.a()) {
                        return false;
                    }
                }
            } else if (value instanceof sv0) {
                if (!((sv0) value).a()) {
                    return false;
                }
            } else if (value instanceof xu0) {
                return true;
            } else {
                throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
            }
        }
        return true;
    }

    @DexIgnore
    public static int c(Map.Entry<FieldDescriptorType, Object> entry) {
        nu0 nu0 = (nu0) entry.getKey();
        Object value = entry.getValue();
        if (nu0.b() != zzfq.MESSAGE || nu0.c() || nu0.a()) {
            return b((nu0<?>) nu0, value);
        }
        boolean z = value instanceof xu0;
        int zzc = ((nu0) entry.getKey()).zzc();
        return z ? zzbn.b(zzc, (bv0) (xu0) value) : zzbn.d(zzc, (sv0) value);
    }

    @DexIgnore
    public static <T extends nu0<T>> ku0<T> i() {
        return d;
    }

    @DexIgnore
    public final Object a(FieldDescriptorType fielddescriptortype) {
        Object obj = this.a.get(fielddescriptortype);
        if (!(obj instanceof xu0)) {
            return obj;
        }
        xu0.c();
        throw null;
    }

    @DexIgnore
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> a() {
        return this.c ? new av0(this.a.d().iterator()) : this.a.d().iterator();
    }

    @DexIgnore
    public final void a(ku0<FieldDescriptorType> ku0) {
        for (int i = 0; i < ku0.a.b(); i++) {
            a(ku0.a.a(i));
        }
        for (Map.Entry<FieldDescriptorType, Object> a2 : ku0.a.c()) {
            a(a2);
        }
    }

    @DexIgnore
    public final void a(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.c()) {
            a(fielddescriptortype.d(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                a(fielddescriptortype.d(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof xu0) {
            this.c = true;
        }
        this.a.put(fielddescriptortype, obj);
    }

    @DexIgnore
    public final void a(Map.Entry<FieldDescriptorType, Object> entry) {
        nu0 nu0 = (nu0) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof xu0) {
            xu0.c();
            throw null;
        } else if (nu0.c()) {
            Object a2 = a(nu0);
            if (a2 == null) {
                a2 = new ArrayList();
            }
            for (Object a3 : (List) value) {
                ((List) a2).add(a(a3));
            }
            this.a.put(nu0, a2);
        } else if (nu0.b() == zzfq.MESSAGE) {
            Object a4 = a(nu0);
            if (a4 == null) {
                this.a.put(nu0, a(value));
            } else if (!(a4 instanceof yv0)) {
                this.a.put(nu0, nu0.a(((sv0) a4).c(), (sv0) value).s());
            } else {
                nu0.a((yv0) a4, (yv0) value);
                throw null;
            }
        } else {
            this.a.put(nu0, a(value));
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.a.isEmpty();
    }

    @DexIgnore
    public final boolean c() {
        return this.b;
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        ku0 ku0 = new ku0();
        for (int i = 0; i < this.a.b(); i++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i);
            ku0.a((nu0) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.c()) {
            ku0.a((nu0) next.getKey(), next.getValue());
        }
        ku0.c = this.c;
        return ku0;
    }

    @DexIgnore
    public final boolean d() {
        for (int i = 0; i < this.a.b(); i++) {
            if (!b(this.a.a(i))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> b2 : this.a.c()) {
            if (!b(b2)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> e() {
        return this.c ? new av0(this.a.entrySet().iterator()) : this.a.entrySet().iterator();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ku0)) {
            return false;
        }
        return this.a.equals(((ku0) obj).a);
    }

    @DexIgnore
    public final int f() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.b(); i2++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i2);
            i += b((nu0<?>) (nu0) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.c()) {
            i += b((nu0<?>) (nu0) next.getKey(), next.getValue());
        }
        return i;
    }

    @DexIgnore
    public final int g() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.b(); i2++) {
            i += c(this.a.a(i2));
        }
        for (Map.Entry<FieldDescriptorType, Object> c2 : this.a.c()) {
            i += c(c2);
        }
        return i;
    }

    @DexIgnore
    public final void h() {
        if (!this.b) {
            this.a.g();
            this.b = true;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }
}
