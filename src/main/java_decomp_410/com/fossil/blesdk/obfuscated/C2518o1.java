package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o1 */
public class C2518o1 implements com.fossil.blesdk.obfuscated.C2100j1 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f7952a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1915h1 f7953b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ boolean f7954c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f7955d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f7956e;

    @DexIgnore
    /* renamed from: f */
    public android.view.View f7957f;

    @DexIgnore
    /* renamed from: g */
    public int f7958g;

    @DexIgnore
    /* renamed from: h */
    public boolean f7959h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f7960i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2452n1 f7961j;

    @DexIgnore
    /* renamed from: k */
    public android.widget.PopupWindow.OnDismissListener f7962k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ android.widget.PopupWindow.OnDismissListener f7963l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.o1$a */
    public class C2519a implements android.widget.PopupWindow.OnDismissListener {
        @DexIgnore
        public C2519a() {
        }

        @DexIgnore
        public void onDismiss() {
            com.fossil.blesdk.obfuscated.C2518o1.this.mo14242e();
        }
    }

    @DexIgnore
    public C2518o1(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.View view, boolean z, int i) {
        this(context, h1Var, view, z, i, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14235a(android.widget.PopupWindow.OnDismissListener onDismissListener) {
        this.f7962k = onDismissListener;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14239b() {
        if (mo14241d()) {
            this.f7961j.dismiss();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2452n1 mo14240c() {
        if (this.f7961j == null) {
            this.f7961j = mo14231a();
        }
        return this.f7961j;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo14241d() {
        com.fossil.blesdk.obfuscated.C2452n1 n1Var = this.f7961j;
        return n1Var != null && n1Var.mo862d();
    }

    @DexIgnore
    /* renamed from: e */
    public void mo14242e() {
        this.f7961j = null;
        android.widget.PopupWindow.OnDismissListener onDismissListener = this.f7962k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo14243f() {
        if (!mo14244g()) {
            throw new java.lang.IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo14244g() {
        if (mo14241d()) {
            return true;
        }
        if (this.f7957f == null) {
            return false;
        }
        mo14233a(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public C2518o1(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.View view, boolean z, int i, int i2) {
        this.f7958g = 8388611;
        this.f7963l = new com.fossil.blesdk.obfuscated.C2518o1.C2519a();
        this.f7952a = context;
        this.f7953b = h1Var;
        this.f7957f = view;
        this.f7954c = z;
        this.f7955d = i;
        this.f7956e = i2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14234a(android.view.View view) {
        this.f7957f = view;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14237a(boolean z) {
        this.f7959h = z;
        com.fossil.blesdk.obfuscated.C2452n1 n1Var = this.f7961j;
        if (n1Var != null) {
            n1Var.mo10263b(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14232a(int i) {
        this.f7958g = i;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14238a(int i, int i2) {
        if (mo14241d()) {
            return true;
        }
        if (this.f7957f == null) {
            return false;
        }
        mo14233a(i, i2, true, true);
        return true;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.fossil.blesdk.obfuscated.n1, com.fossil.blesdk.obfuscated.p1] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.fossil.blesdk.obfuscated.u1] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.fossil.blesdk.obfuscated.e1] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2452n1 mo14231a() {
        Object r0;
        android.view.Display defaultDisplay = ((android.view.WindowManager) this.f7952a.getSystemService("window")).getDefaultDisplay();
        android.graphics.Point point = new android.graphics.Point();
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        if (java.lang.Math.min(point.x, point.y) >= this.f7952a.getResources().getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2998u.abc_cascading_menus_min_smallest_width)) {
            com.fossil.blesdk.obfuscated.C1694e1 e1Var = new com.fossil.blesdk.obfuscated.C1694e1(this.f7952a, this.f7957f, this.f7955d, this.f7956e, this.f7954c);
            r0 = e1Var;
        } else {
            com.fossil.blesdk.obfuscated.C3000u1 u1Var = new com.fossil.blesdk.obfuscated.C3000u1(this.f7952a, this.f7953b, this.f7957f, this.f7955d, this.f7956e, this.f7954c);
            r0 = u1Var;
        }
        r0.mo10261a(this.f7953b);
        r0.mo10260a(this.f7963l);
        r0.mo10259a(this.f7957f);
        r0.mo9013a(this.f7960i);
        r0.mo10263b(this.f7959h);
        r0.mo10258a(this.f7958g);
        return r0;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14233a(int i, int i2, boolean z, boolean z2) {
        com.fossil.blesdk.obfuscated.C2452n1 c = mo14240c();
        c.mo10266c(z2);
        if (z) {
            if ((com.fossil.blesdk.obfuscated.C2634p8.m12135a(this.f7958g, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f7957f)) & 7) == 5) {
                i -= this.f7957f.getWidth();
            }
            c.mo10262b(i);
            c.mo10265c(i2);
            int i3 = (int) ((this.f7952a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c.mo13788a(new android.graphics.Rect(i - i3, i2 - i3, i + i3, i2 + i3));
        }
        c.mo724c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14236a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        this.f7960i = aVar;
        com.fossil.blesdk.obfuscated.C2452n1 n1Var = this.f7961j;
        if (n1Var != null) {
            n1Var.mo9013a(aVar);
        }
    }
}
