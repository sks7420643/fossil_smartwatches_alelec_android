package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b */
public interface C1455b extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.b$a")
    /* renamed from: com.fossil.blesdk.obfuscated.b$a */
    public static abstract class C1456a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C1455b {
        @DexIgnore
        public C1456a() {
            attachInterface(this, "android.support.customtabs.ICustomTabsService");
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: android.net.Uri} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: android.os.Bundle} */
        /* JADX WARNING: type inference failed for: r0v1 */
        /* JADX WARNING: type inference failed for: r0v23 */
        /* JADX WARNING: type inference failed for: r0v24 */
        /* JADX WARNING: type inference failed for: r0v25 */
        /* JADX WARNING: type inference failed for: r0v26 */
        /* JADX WARNING: type inference failed for: r0v27 */
        /* JADX WARNING: type inference failed for: r0v28 */
        /* JADX WARNING: Multi-variable type inference failed */
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i != 1598968902) {
                Object r0 = 0;
                switch (i) {
                    case 2:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        boolean a = mo8995a(parcel.readLong());
                        parcel2.writeNoException();
                        parcel2.writeInt(a);
                        return true;
                    case 3:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        boolean a2 = mo8996a(com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        parcel2.writeInt(a2);
                        return true;
                    case 4:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        com.fossil.blesdk.obfuscated.C1365a a3 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                        android.net.Uri uri = parcel.readInt() != 0 ? (android.net.Uri) android.net.Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r0 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean a4 = mo8999a(a3, uri, r0, (java.util.List<android.os.Bundle>) parcel.createTypedArrayList(android.os.Bundle.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeInt(a4);
                        return true;
                    case 5:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        java.lang.String readString = parcel.readString();
                        if (parcel.readInt() != 0) {
                            r0 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        android.os.Bundle b = mo9001b(readString, r0);
                        parcel2.writeNoException();
                        if (b != null) {
                            parcel2.writeInt(1);
                            b.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 6:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        com.fossil.blesdk.obfuscated.C1365a a5 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r0 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean b2 = mo9002b(a5, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(b2);
                        return true;
                    case 7:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        com.fossil.blesdk.obfuscated.C1365a a6 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r0 = (android.net.Uri) android.net.Uri.CREATOR.createFromParcel(parcel);
                        }
                        boolean a7 = mo8998a(a6, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a7);
                        return true;
                    case 8:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        com.fossil.blesdk.obfuscated.C1365a a8 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                        java.lang.String readString2 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            r0 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        int b3 = mo9000b(a8, readString2, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(b3);
                        return true;
                    case 9:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        com.fossil.blesdk.obfuscated.C1365a a9 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                        int readInt = parcel.readInt();
                        android.net.Uri uri2 = parcel.readInt() != 0 ? (android.net.Uri) android.net.Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r0 = (android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean a10 = mo8997a(a9, readInt, uri2, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a10);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("android.support.customtabs.ICustomTabsService");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    boolean mo8995a(long j) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    boolean mo8996a(com.fossil.blesdk.obfuscated.C1365a aVar) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    boolean mo8997a(com.fossil.blesdk.obfuscated.C1365a aVar, int i, android.net.Uri uri, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    boolean mo8998a(com.fossil.blesdk.obfuscated.C1365a aVar, android.net.Uri uri) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    boolean mo8999a(com.fossil.blesdk.obfuscated.C1365a aVar, android.net.Uri uri, android.os.Bundle bundle, java.util.List<android.os.Bundle> list) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: b */
    int mo9000b(com.fossil.blesdk.obfuscated.C1365a aVar, java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: b */
    android.os.Bundle mo9001b(java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: b */
    boolean mo9002b(com.fossil.blesdk.obfuscated.C1365a aVar, android.os.Bundle bundle) throws android.os.RemoteException;
}
