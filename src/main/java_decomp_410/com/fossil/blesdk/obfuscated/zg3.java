package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zg3 implements Factory<yg3> {
    @DexIgnore
    public static yg3 a(ug3 ug3) {
        return new yg3(ug3);
    }
}
