package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t34 {
    @DexIgnore
    public static void a(Activity activity) {
        n44.a(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof c44) {
            u34<Activity> d = ((c44) application).d();
            n44.a(d, "%s.activityInjector() returned null", application.getClass());
            d.a(activity);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), c44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(Service service) {
        n44.a(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof g44) {
            u34<Service> b = ((g44) application).b();
            n44.a(b, "%s.serviceInjector() returned null", application.getClass());
            b.a(service);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), g44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(BroadcastReceiver broadcastReceiver, Context context) {
        n44.a(broadcastReceiver, "broadcastReceiver");
        n44.a(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof d44) {
            u34<BroadcastReceiver> a = ((d44) application).a();
            n44.a(a, "%s.broadcastReceiverInjector() returned null", application.getClass());
            a.a(broadcastReceiver);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), d44.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(ContentProvider contentProvider) {
        n44.a(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof e44) {
            u34<ContentProvider> c = ((e44) application).c();
            n44.a(c, "%s.contentProviderInjector() returned null", application.getClass());
            c.a(contentProvider);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), e44.class.getCanonicalName()}));
    }
}
