package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wf1 implements Parcelable.Creator<StreetViewPanoramaOptions> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        StreetViewPanoramaCamera streetViewPanoramaCamera = null;
        String str = null;
        LatLng latLng = null;
        Integer num = null;
        mf1 mf1 = null;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    streetViewPanoramaCamera = SafeParcelReader.a(parcel, a, StreetViewPanoramaCamera.CREATOR);
                    break;
                case 3:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 4:
                    latLng = SafeParcelReader.a(parcel, a, LatLng.CREATOR);
                    break;
                case 5:
                    num = SafeParcelReader.r(parcel, a);
                    break;
                case 6:
                    b2 = SafeParcelReader.k(parcel, a);
                    break;
                case 7:
                    b3 = SafeParcelReader.k(parcel, a);
                    break;
                case 8:
                    b4 = SafeParcelReader.k(parcel, a);
                    break;
                case 9:
                    b5 = SafeParcelReader.k(parcel, a);
                    break;
                case 10:
                    b6 = SafeParcelReader.k(parcel, a);
                    break;
                case 11:
                    mf1 = SafeParcelReader.a(parcel, a, mf1.CREATOR);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new StreetViewPanoramaOptions(streetViewPanoramaCamera, str, latLng, num, b2, b3, b4, b5, b6, mf1);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new StreetViewPanoramaOptions[i];
    }
}
