package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.po */
public final class C2662po extends com.fossil.blesdk.obfuscated.C1435ap<android.content.res.AssetFileDescriptor> {
    @DexIgnore
    public C2662po(android.content.ContentResolver contentResolver, android.net.Uri uri) {
        super(contentResolver, uri);
    }

    @DexIgnore
    public java.lang.Class<android.content.res.AssetFileDescriptor> getDataClass() {
        return android.content.res.AssetFileDescriptor.class;
    }

    @DexIgnore
    /* renamed from: a */
    public android.content.res.AssetFileDescriptor m12323a(android.net.Uri uri, android.content.ContentResolver contentResolver) throws java.io.FileNotFoundException {
        android.content.res.AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor;
        }
        throw new java.io.FileNotFoundException("FileDescriptor is null for: " + uri);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8871a(android.content.res.AssetFileDescriptor assetFileDescriptor) throws java.io.IOException {
        assetFileDescriptor.close();
    }
}
