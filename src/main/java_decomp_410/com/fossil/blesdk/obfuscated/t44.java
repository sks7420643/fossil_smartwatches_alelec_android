package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface t44<T> {
    @DexIgnore
    public static final t44 a = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements t44<Object> {
        @DexIgnore
        public void a(Exception exc) {
        }

        @DexIgnore
        public void a(Object obj) {
        }

        @DexIgnore
        public b() {
        }
    }

    @DexIgnore
    void a(Exception exc);

    @DexIgnore
    void a(T t);
}
