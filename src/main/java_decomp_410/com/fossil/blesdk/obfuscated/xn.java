package com.fossil.blesdk.obfuscated;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.bumptech.glide.Priority;
import com.fossil.blesdk.obfuscated.lu;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xn implements ComponentCallbacks2, ru, un<wn<Drawable>> {
    @DexIgnore
    public static /* final */ rv q; // = ((rv) rv.b((Class<?>) Bitmap.class).O());
    @DexIgnore
    public /* final */ rn e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ qu g;
    @DexIgnore
    public /* final */ wu h;
    @DexIgnore
    public /* final */ vu i;
    @DexIgnore
    public /* final */ yu j;
    @DexIgnore
    public /* final */ Runnable k;
    @DexIgnore
    public /* final */ Handler l;
    @DexIgnore
    public /* final */ lu m;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<qv<Object>> n;
    @DexIgnore
    public rv o;
    @DexIgnore
    public boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            xn xnVar = xn.this;
            xnVar.g.a(xnVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements lu.a {
        @DexIgnore
        public /* final */ wu a;

        @DexIgnore
        public b(wu wuVar) {
            this.a = wuVar;
        }

        @DexIgnore
        public void a(boolean z) {
            if (z) {
                synchronized (xn.this) {
                    this.a.d();
                }
            }
        }
    }

    /*
    static {
        rv rvVar = (rv) rv.b((Class<?>) ut.class).O();
        rv rvVar2 = (rv) ((rv) rv.b(pp.b).a(Priority.LOW)).a(true);
    }
    */

    @DexIgnore
    public xn(rn rnVar, qu quVar, vu vuVar, Context context) {
        this(rnVar, quVar, vuVar, new wu(), rnVar.d(), context);
    }

    @DexIgnore
    public synchronized void a(rv rvVar) {
        this.o = (rv) ((rv) rvVar.clone()).a();
    }

    @DexIgnore
    public synchronized void b() {
        this.j.b();
        for (bw<?> a2 : this.j.f()) {
            a(a2);
        }
        this.j.e();
        this.h.a();
        this.g.b(this);
        this.g.b(this.m);
        this.l.removeCallbacks(this.k);
        this.e.b(this);
    }

    @DexIgnore
    public synchronized void c() {
        k();
        this.j.c();
    }

    @DexIgnore
    public wn<Bitmap> e() {
        return a(Bitmap.class).a((lv) q);
    }

    @DexIgnore
    public wn<Drawable> f() {
        return a(Drawable.class);
    }

    @DexIgnore
    public List<qv<Object>> g() {
        return this.n;
    }

    @DexIgnore
    public synchronized rv h() {
        return this.o;
    }

    @DexIgnore
    public synchronized void i() {
        this.h.b();
    }

    @DexIgnore
    public synchronized void j() {
        i();
        for (xn i2 : this.i.a()) {
            i2.i();
        }
    }

    @DexIgnore
    public synchronized void k() {
        this.h.c();
    }

    @DexIgnore
    public synchronized void l() {
        this.h.e();
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        if (i2 == 60 && this.p) {
            j();
        }
    }

    @DexIgnore
    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.h + ", treeNode=" + this.i + "}";
    }

    @DexIgnore
    public synchronized void a() {
        l();
        this.j.a();
    }

    @DexIgnore
    public xn(rn rnVar, qu quVar, vu vuVar, wu wuVar, mu muVar, Context context) {
        this.j = new yu();
        this.k = new a();
        this.l = new Handler(Looper.getMainLooper());
        this.e = rnVar;
        this.g = quVar;
        this.i = vuVar;
        this.h = wuVar;
        this.f = context;
        this.m = muVar.a(context.getApplicationContext(), new b(wuVar));
        if (uw.c()) {
            this.l.post(this.k);
        } else {
            quVar.a(this);
        }
        quVar.a(this.m);
        this.n = new CopyOnWriteArrayList<>(rnVar.f().b());
        a(rnVar.f().c());
        rnVar.a(this);
    }

    @DexIgnore
    public final void c(bw<?> bwVar) {
        boolean b2 = b(bwVar);
        ov d = bwVar.d();
        if (!b2 && !this.e.a(bwVar) && d != null) {
            bwVar.a((ov) null);
            d.clear();
        }
    }

    @DexIgnore
    public wn<Drawable> a(String str) {
        return f().a(str);
    }

    @DexIgnore
    public wn<Drawable> a(Uri uri) {
        return f().a(uri);
    }

    @DexIgnore
    public wn<Drawable> a(Integer num) {
        return f().a(num);
    }

    @DexIgnore
    public wn<Drawable> a(byte[] bArr) {
        return f().a(bArr);
    }

    @DexIgnore
    public wn<Drawable> a(Object obj) {
        return f().a(obj);
    }

    @DexIgnore
    public <ResourceType> wn<ResourceType> a(Class<ResourceType> cls) {
        return new wn<>(this.e, this, cls, this.f);
    }

    @DexIgnore
    public synchronized boolean b(bw<?> bwVar) {
        ov d = bwVar.d();
        if (d == null) {
            return true;
        }
        if (!this.h.a(d)) {
            return false;
        }
        this.j.b(bwVar);
        bwVar.a((ov) null);
        return true;
    }

    @DexIgnore
    public void a(bw<?> bwVar) {
        if (bwVar != null) {
            c(bwVar);
        }
    }

    @DexIgnore
    public synchronized void a(bw<?> bwVar, ov ovVar) {
        this.j.a(bwVar);
        this.h.b(ovVar);
    }

    @DexIgnore
    public <T> yn<?, T> b(Class<T> cls) {
        return this.e.f().a(cls);
    }
}
