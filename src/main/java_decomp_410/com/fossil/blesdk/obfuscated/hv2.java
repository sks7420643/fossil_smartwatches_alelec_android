package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hv2 implements Factory<yy2> {
    @DexIgnore
    public static yy2 a(gv2 gv2) {
        yy2 a = gv2.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
