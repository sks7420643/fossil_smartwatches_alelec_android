package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fj4 extends ug4 {
    @DexIgnore
    public static /* final */ fj4 e; // = new fj4();

    @DexIgnore
    public void a(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean b(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        return false;
    }

    @DexIgnore
    public String toString() {
        return "Unconfined";
    }
}
