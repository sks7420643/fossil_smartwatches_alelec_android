package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yv */
public abstract class C3393yv<Z> extends com.fossil.blesdk.obfuscated.C1583cw<android.widget.ImageView, Z> implements com.fossil.blesdk.obfuscated.C1750ew.C1751a {

    @DexIgnore
    /* renamed from: k */
    public android.graphics.drawable.Animatable f11419k;

    @DexIgnore
    public C3393yv(android.widget.ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9312a(android.graphics.drawable.Drawable drawable) {
        super.mo9312a(drawable);
        mo18353d((java.lang.Object) null);
        mo18352d(drawable);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9316b(android.graphics.drawable.Drawable drawable) {
        super.mo9316b(drawable);
        mo18353d((java.lang.Object) null);
        mo18352d(drawable);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9318c(android.graphics.drawable.Drawable drawable) {
        super.mo9318c(drawable);
        android.graphics.drawable.Animatable animatable = this.f11419k;
        if (animatable != null) {
            animatable.stop();
        }
        mo18353d((java.lang.Object) null);
        mo18352d(drawable);
    }

    @DexIgnore
    /* renamed from: c */
    public abstract void mo17218c(Z z);

    @DexIgnore
    /* renamed from: d */
    public void mo18352d(android.graphics.drawable.Drawable drawable) {
        ((android.widget.ImageView) this.f4201e).setImageDrawable(drawable);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo18353d(Z z) {
        mo17218c(z);
        mo18351b(z);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9315a(Z z, com.fossil.blesdk.obfuscated.C1750ew<? super Z> ewVar) {
        if (ewVar == null || !ewVar.mo10141a(z, this)) {
            mo18353d(z);
        } else {
            mo18351b(z);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo18351b(Z z) {
        if (z instanceof android.graphics.drawable.Animatable) {
            this.f11419k = (android.graphics.drawable.Animatable) z;
            this.f11419k.start();
            return;
        }
        this.f11419k = null;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14097c() {
        android.graphics.drawable.Animatable animatable = this.f11419k;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14094a() {
        android.graphics.drawable.Animatable animatable = this.f11419k;
        if (animatable != null) {
            animatable.start();
        }
    }
}
