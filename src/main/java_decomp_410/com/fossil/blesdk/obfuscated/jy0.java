package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzge$zzs;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jy0 extends qx0<jy0> implements Cloneable {
    @DexIgnore
    public long g; // = 0;
    @DexIgnore
    public long h; // = 0;
    @DexIgnore
    public String i; // = "";
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public String k; // = "";
    @DexIgnore
    public ky0[] l; // = ky0.f();
    @DexIgnore
    public byte[] m;
    @DexIgnore
    public zx0 n;
    @DexIgnore
    public byte[] o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public gy0 r;
    @DexIgnore
    public String s;
    @DexIgnore
    public long t;
    @DexIgnore
    public hy0 u;
    @DexIgnore
    public byte[] v;
    @DexIgnore
    public String w;
    @DexIgnore
    public int[] x;
    @DexIgnore
    public zzge$zzs y;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public jy0() {
        byte[] bArr = yx0.e;
        this.m = bArr;
        this.n = null;
        this.o = bArr;
        this.p = "";
        this.q = "";
        this.r = null;
        this.s = "";
        this.t = 180000;
        this.u = null;
        this.v = bArr;
        this.w = "";
        this.x = yx0.a;
        this.y = null;
        this.z = false;
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        long j2 = this.g;
        if (j2 != 0) {
            px0.a(1, j2);
        }
        String str = this.i;
        if (str != null && !str.equals("")) {
            px0.a(2, this.i);
        }
        ky0[] ky0Arr = this.l;
        int i2 = 0;
        if (ky0Arr != null && ky0Arr.length > 0) {
            int i3 = 0;
            while (true) {
                ky0[] ky0Arr2 = this.l;
                if (i3 >= ky0Arr2.length) {
                    break;
                }
                ky0 ky0 = ky0Arr2[i3];
                if (ky0 != null) {
                    px0.a(3, (vx0) ky0);
                }
                i3++;
            }
        }
        if (!Arrays.equals(this.m, yx0.e)) {
            px0.a(4, this.m);
        }
        if (!Arrays.equals(this.o, yx0.e)) {
            px0.a(6, this.o);
        }
        gy0 gy0 = this.r;
        if (gy0 != null) {
            px0.a(7, (vx0) gy0);
        }
        String str2 = this.p;
        if (str2 != null && !str2.equals("")) {
            px0.a(8, this.p);
        }
        zx0 zx0 = this.n;
        if (zx0 != null) {
            px0.a(9, (sv0) zx0);
        }
        int i4 = this.j;
        if (i4 != 0) {
            px0.b(11, i4);
        }
        String str3 = this.q;
        if (str3 != null && !str3.equals("")) {
            px0.a(13, this.q);
        }
        String str4 = this.s;
        if (str4 != null && !str4.equals("")) {
            px0.a(14, this.s);
        }
        long j3 = this.t;
        if (j3 != 180000) {
            px0.a(15, 0);
            px0.a(px0.b(j3));
        }
        hy0 hy0 = this.u;
        if (hy0 != null) {
            px0.a(16, (vx0) hy0);
        }
        long j4 = this.h;
        if (j4 != 0) {
            px0.a(17, j4);
        }
        if (!Arrays.equals(this.v, yx0.e)) {
            px0.a(18, this.v);
        }
        int[] iArr = this.x;
        if (iArr != null && iArr.length > 0) {
            while (true) {
                int[] iArr2 = this.x;
                if (i2 >= iArr2.length) {
                    break;
                }
                px0.b(20, iArr2[i2]);
                i2++;
            }
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs != null) {
            px0.a(23, (sv0) zzge_zzs);
        }
        String str5 = this.w;
        if (str5 != null && !str5.equals("")) {
            px0.a(24, this.w);
        }
        boolean z2 = this.z;
        if (z2) {
            px0.a(25, z2);
        }
        String str6 = this.k;
        if (str6 != null && !str6.equals("")) {
            px0.a(26, this.k);
        }
        super.a(px0);
    }

    @DexIgnore
    public final int b() {
        int[] iArr;
        int b = super.b();
        long j2 = this.g;
        if (j2 != 0) {
            b += px0.b(1, j2);
        }
        String str = this.i;
        if (str != null && !str.equals("")) {
            b += px0.b(2, this.i);
        }
        ky0[] ky0Arr = this.l;
        int i2 = 0;
        if (ky0Arr != null && ky0Arr.length > 0) {
            int i3 = b;
            int i4 = 0;
            while (true) {
                ky0[] ky0Arr2 = this.l;
                if (i4 >= ky0Arr2.length) {
                    break;
                }
                ky0 ky0 = ky0Arr2[i4];
                if (ky0 != null) {
                    i3 += px0.b(3, (vx0) ky0);
                }
                i4++;
            }
            b = i3;
        }
        if (!Arrays.equals(this.m, yx0.e)) {
            b += px0.b(4, this.m);
        }
        if (!Arrays.equals(this.o, yx0.e)) {
            b += px0.b(6, this.o);
        }
        gy0 gy0 = this.r;
        if (gy0 != null) {
            b += px0.b(7, (vx0) gy0);
        }
        String str2 = this.p;
        if (str2 != null && !str2.equals("")) {
            b += px0.b(8, this.p);
        }
        zx0 zx0 = this.n;
        if (zx0 != null) {
            b += zzbn.c(9, (sv0) zx0);
        }
        int i5 = this.j;
        if (i5 != 0) {
            b += px0.c(11) + px0.d(i5);
        }
        String str3 = this.q;
        if (str3 != null && !str3.equals("")) {
            b += px0.b(13, this.q);
        }
        String str4 = this.s;
        if (str4 != null && !str4.equals("")) {
            b += px0.b(14, this.s);
        }
        long j3 = this.t;
        if (j3 != 180000) {
            b += px0.c(15) + px0.c(px0.b(j3));
        }
        hy0 hy0 = this.u;
        if (hy0 != null) {
            b += px0.b(16, (vx0) hy0);
        }
        long j4 = this.h;
        if (j4 != 0) {
            b += px0.b(17, j4);
        }
        if (!Arrays.equals(this.v, yx0.e)) {
            b += px0.b(18, this.v);
        }
        int[] iArr2 = this.x;
        if (iArr2 != null && iArr2.length > 0) {
            int i6 = 0;
            while (true) {
                iArr = this.x;
                if (i2 >= iArr.length) {
                    break;
                }
                i6 += px0.d(iArr[i2]);
                i2++;
            }
            b = b + i6 + (iArr.length * 2);
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs != null) {
            b += zzbn.c(23, (sv0) zzge_zzs);
        }
        String str5 = this.w;
        if (str5 != null && !str5.equals("")) {
            b += px0.b(24, this.w);
        }
        if (this.z) {
            b += px0.c(25) + 1;
        }
        String str6 = this.k;
        return (str6 == null || str6.equals("")) ? b : b + px0.b(26, this.k);
    }

    @DexIgnore
    public final /* synthetic */ vx0 c() throws CloneNotSupportedException {
        return (jy0) clone();
    }

    @DexIgnore
    public final /* synthetic */ qx0 d() throws CloneNotSupportedException {
        return (jy0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final jy0 clone() {
        try {
            jy0 jy0 = (jy0) super.clone();
            ky0[] ky0Arr = this.l;
            if (ky0Arr != null && ky0Arr.length > 0) {
                jy0.l = new ky0[ky0Arr.length];
                int i2 = 0;
                while (true) {
                    ky0[] ky0Arr2 = this.l;
                    if (i2 >= ky0Arr2.length) {
                        break;
                    }
                    if (ky0Arr2[i2] != null) {
                        jy0.l[i2] = (ky0) ky0Arr2[i2].clone();
                    }
                    i2++;
                }
            }
            zx0 zx0 = this.n;
            if (zx0 != null) {
                jy0.n = zx0;
            }
            gy0 gy0 = this.r;
            if (gy0 != null) {
                jy0.r = (gy0) gy0.clone();
            }
            hy0 hy0 = this.u;
            if (hy0 != null) {
                jy0.u = (hy0) hy0.clone();
            }
            int[] iArr = this.x;
            if (iArr != null && iArr.length > 0) {
                jy0.x = (int[]) iArr.clone();
            }
            zzge$zzs zzge_zzs = this.y;
            if (zzge_zzs != null) {
                jy0.y = zzge_zzs;
            }
            return jy0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof jy0)) {
            return false;
        }
        jy0 jy0 = (jy0) obj;
        if (this.g != jy0.g || this.h != jy0.h) {
            return false;
        }
        String str = this.i;
        if (str == null) {
            if (jy0.i != null) {
                return false;
            }
        } else if (!str.equals(jy0.i)) {
            return false;
        }
        if (this.j != jy0.j) {
            return false;
        }
        String str2 = this.k;
        if (str2 == null) {
            if (jy0.k != null) {
                return false;
            }
        } else if (!str2.equals(jy0.k)) {
            return false;
        }
        if (!ux0.a((Object[]) this.l, (Object[]) jy0.l) || !Arrays.equals(this.m, jy0.m)) {
            return false;
        }
        zx0 zx0 = this.n;
        if (zx0 == null) {
            if (jy0.n != null) {
                return false;
            }
        } else if (!zx0.equals(jy0.n)) {
            return false;
        }
        if (!Arrays.equals(this.o, jy0.o)) {
            return false;
        }
        String str3 = this.p;
        if (str3 == null) {
            if (jy0.p != null) {
                return false;
            }
        } else if (!str3.equals(jy0.p)) {
            return false;
        }
        String str4 = this.q;
        if (str4 == null) {
            if (jy0.q != null) {
                return false;
            }
        } else if (!str4.equals(jy0.q)) {
            return false;
        }
        gy0 gy0 = this.r;
        if (gy0 == null) {
            if (jy0.r != null) {
                return false;
            }
        } else if (!gy0.equals(jy0.r)) {
            return false;
        }
        String str5 = this.s;
        if (str5 == null) {
            if (jy0.s != null) {
                return false;
            }
        } else if (!str5.equals(jy0.s)) {
            return false;
        }
        if (this.t != jy0.t) {
            return false;
        }
        hy0 hy0 = this.u;
        if (hy0 == null) {
            if (jy0.u != null) {
                return false;
            }
        } else if (!hy0.equals(jy0.u)) {
            return false;
        }
        if (!Arrays.equals(this.v, jy0.v)) {
            return false;
        }
        String str6 = this.w;
        if (str6 == null) {
            if (jy0.w != null) {
                return false;
            }
        } else if (!str6.equals(jy0.w)) {
            return false;
        }
        if (!ux0.a(this.x, jy0.x)) {
            return false;
        }
        zzge$zzs zzge_zzs = this.y;
        if (zzge_zzs == null) {
            if (jy0.y != null) {
                return false;
            }
        } else if (!zzge_zzs.equals(jy0.y)) {
            return false;
        }
        if (this.z != jy0.z) {
            return false;
        }
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            return this.f.equals(jy0.f);
        }
        sx0 sx02 = jy0.f;
        return sx02 == null || sx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        long j2 = this.g;
        long j3 = this.h;
        int hashCode = (((((jy0.class.getName().hashCode() + 527) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31 * 31;
        String str = this.i;
        int i2 = 0;
        int hashCode2 = (((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.j) * 31;
        String str2 = this.k;
        int hashCode3 = str2 == null ? 0 : str2.hashCode();
        int i3 = 1237;
        int a = ((((((hashCode2 + hashCode3) * 31 * 31) + 1237) * 31) + ux0.a((Object[]) this.l)) * 31) + Arrays.hashCode(this.m);
        zx0 zx0 = this.n;
        int hashCode4 = ((((a * 31) + (zx0 == null ? 0 : zx0.hashCode())) * 31) + Arrays.hashCode(this.o)) * 31;
        String str3 = this.p;
        int hashCode5 = (hashCode4 + (str3 == null ? 0 : str3.hashCode())) * 31;
        String str4 = this.q;
        int hashCode6 = hashCode5 + (str4 == null ? 0 : str4.hashCode());
        gy0 gy0 = this.r;
        int hashCode7 = ((hashCode6 * 31) + (gy0 == null ? 0 : gy0.hashCode())) * 31;
        String str5 = this.s;
        int hashCode8 = str5 == null ? 0 : str5.hashCode();
        long j4 = this.t;
        hy0 hy0 = this.u;
        int hashCode9 = (((((((hashCode7 + hashCode8) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + (hy0 == null ? 0 : hy0.hashCode())) * 31) + Arrays.hashCode(this.v)) * 31;
        String str6 = this.w;
        int hashCode10 = str6 == null ? 0 : str6.hashCode();
        zzge$zzs zzge_zzs = this.y;
        int a2 = (((((hashCode9 + hashCode10) * 31 * 31) + ux0.a(this.x)) * 31 * 31) + (zzge_zzs == null ? 0 : zzge_zzs.hashCode())) * 31;
        if (this.z) {
            i3 = 1231;
        }
        int i4 = (a2 + i3) * 31;
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            i2 = this.f.hashCode();
        }
        return i4 + i2;
    }
}
