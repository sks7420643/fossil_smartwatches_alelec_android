package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayDeque;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zx1 {
    @DexIgnore
    public static zx1 f;
    @DexIgnore
    public /* final */ SimpleArrayMap<String, String> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public Boolean b; // = null;
    @DexIgnore
    public Boolean c; // = null;
    @DexIgnore
    public /* final */ Queue<Intent> d; // = new ArrayDeque();
    @DexIgnore
    public /* final */ Queue<Intent> e; // = new ArrayDeque();

    @DexIgnore
    public static PendingIntent a(Context context, int i, Intent intent, int i2) {
        return PendingIntent.getBroadcast(context, i, b(context, "com.google.firebase.MESSAGING_EVENT", intent), 1073741824);
    }

    @DexIgnore
    public static synchronized zx1 b() {
        zx1 zx1;
        synchronized (zx1.class) {
            if (f == null) {
                f = new zx1();
            }
            zx1 = f;
        }
        return zx1;
    }

    @DexIgnore
    public static void c(Context context, Intent intent) {
        context.sendBroadcast(b(context, "com.google.firebase.MESSAGING_EVENT", intent));
    }

    @DexIgnore
    public final Intent a() {
        return this.e.poll();
    }

    @DexIgnore
    public static void b(Context context, Intent intent) {
        context.sendBroadcast(b(context, "com.google.firebase.INSTANCE_ID_EVENT", intent));
    }

    @DexIgnore
    public final int a(Context context, String str, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(str);
            Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Starting service: ".concat(valueOf) : new String("Starting service: "));
        }
        char c2 = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -842411455) {
            if (hashCode == 41532704 && str.equals("com.google.firebase.MESSAGING_EVENT")) {
                c2 = 1;
            }
        } else if (str.equals("com.google.firebase.INSTANCE_ID_EVENT")) {
            c2 = 0;
        }
        if (c2 == 0) {
            this.d.offer(intent);
        } else if (c2 != 1) {
            String valueOf2 = String.valueOf(str);
            Log.w("FirebaseInstanceId", valueOf2.length() != 0 ? "Unknown service action: ".concat(valueOf2) : new String("Unknown service action: "));
            return 500;
        } else {
            this.e.offer(intent);
        }
        Intent intent2 = new Intent(str);
        intent2.setPackage(context.getPackageName());
        return a(context, intent2);
    }

    @DexIgnore
    public static Intent b(Context context, String str, Intent intent) {
        Intent intent2 = new Intent(context, FirebaseInstanceIdReceiver.class);
        intent2.setAction(str);
        intent2.putExtra("wrapped_intent", intent);
        return intent2;
    }

    @DexIgnore
    public final boolean b(Context context) {
        if (this.c == null) {
            this.c = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0);
        }
        if (!this.b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.ACCESS_NETWORK_STATE this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.c.booleanValue();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00dc A[Catch:{ SecurityException -> 0x0122, IllegalStateException -> 0x00fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00e1 A[Catch:{ SecurityException -> 0x0122, IllegalStateException -> 0x00fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00ee A[Catch:{ SecurityException -> 0x0122, IllegalStateException -> 0x00fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f8 A[RETURN] */
    public final int a(Context context, Intent intent) {
        String str;
        ComponentName componentName;
        synchronized (this.a) {
            str = this.a.get(intent.getAction());
        }
        if (str == null) {
            ResolveInfo resolveService = context.getPackageManager().resolveService(intent, 0);
            if (resolveService != null) {
                ServiceInfo serviceInfo = resolveService.serviceInfo;
                if (serviceInfo != null) {
                    if (!context.getPackageName().equals(serviceInfo.packageName) || serviceInfo.name == null) {
                        String str2 = serviceInfo.packageName;
                        String str3 = serviceInfo.name;
                        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 94 + String.valueOf(str3).length());
                        sb.append("Error resolving target intent service, skipping classname enforcement. Resolved service was: ");
                        sb.append(str2);
                        sb.append(ZendeskConfig.SLASH);
                        sb.append(str3);
                        Log.e("FirebaseInstanceId", sb.toString());
                        if (a(context)) {
                            componentName = lb.b(context, intent);
                        } else {
                            componentName = context.startService(intent);
                            Log.d("FirebaseInstanceId", "Missing wake lock permission, service start may be delayed");
                        }
                        if (componentName != null) {
                            return -1;
                        }
                        Log.e("FirebaseInstanceId", "Error while delivering the message: ServiceIntent not found.");
                        return MFNetworkReturnCode.NOT_FOUND;
                    }
                    String str4 = serviceInfo.name;
                    if (str4.startsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                        String valueOf = String.valueOf(context.getPackageName());
                        String valueOf2 = String.valueOf(str4);
                        str4 = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                    }
                    str = str4;
                    synchronized (this.a) {
                        this.a.put(intent.getAction(), str);
                    }
                }
            }
            Log.e("FirebaseInstanceId", "Failed to resolve target intent service, skipping classname enforcement");
            if (a(context)) {
            }
            if (componentName != null) {
            }
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf3 = String.valueOf(str);
            Log.d("FirebaseInstanceId", valueOf3.length() != 0 ? "Restricting intent to a specific service: ".concat(valueOf3) : new String("Restricting intent to a specific service: "));
        }
        intent.setClassName(context.getPackageName(), str);
        try {
            if (a(context)) {
            }
            if (componentName != null) {
            }
        } catch (SecurityException e2) {
            Log.e("FirebaseInstanceId", "Error while delivering the message to the serviceIntent", e2);
            return 401;
        } catch (IllegalStateException e3) {
            String valueOf4 = String.valueOf(e3);
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf4).length() + 45);
            sb2.append("Failed to start service while in background: ");
            sb2.append(valueOf4);
            Log.e("FirebaseInstanceId", sb2.toString());
            return Action.ActivityTracker.TAG_ACTIVITY;
        }
    }

    @DexIgnore
    public final boolean a(Context context) {
        if (this.b == null) {
            this.b = Boolean.valueOf(context.checkCallingOrSelfPermission("android.permission.WAKE_LOCK") == 0);
        }
        if (!this.b.booleanValue() && Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Missing Permission: android.permission.WAKE_LOCK this should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        }
        return this.b.booleanValue();
    }
}
