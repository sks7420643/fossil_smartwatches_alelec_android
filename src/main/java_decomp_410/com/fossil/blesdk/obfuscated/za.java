package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class za implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<za> CREATOR; // = new a();
    @DexIgnore
    public ab[] e;
    @DexIgnore
    public int[] f;
    @DexIgnore
    public wa[] g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public int i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<za> {
        @DexIgnore
        public za createFromParcel(Parcel parcel) {
            return new za(parcel);
        }

        @DexIgnore
        public za[] newArray(int i) {
            return new za[i];
        }
    }

    @DexIgnore
    public za() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeTypedArray(this.e, i2);
        parcel.writeIntArray(this.f);
        parcel.writeTypedArray(this.g, i2);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
    }

    @DexIgnore
    public za(Parcel parcel) {
        this.e = (ab[]) parcel.createTypedArray(ab.CREATOR);
        this.f = parcel.createIntArray();
        this.g = (wa[]) parcel.createTypedArray(wa.CREATOR);
        this.h = parcel.readInt();
        this.i = parcel.readInt();
    }
}
