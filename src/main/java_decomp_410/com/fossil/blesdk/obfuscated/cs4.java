package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cs4<T> implements gr4<T, RequestBody> {
    @DexIgnore
    public static /* final */ am4 c; // = am4.a("application/json; charset=UTF-8");
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public cs4(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    public RequestBody a(T t) throws IOException {
        jo4 jo4 = new jo4();
        JsonWriter a2 = this.a.a((Writer) new OutputStreamWriter(jo4.e(), d));
        this.b.write(a2, t);
        a2.close();
        return RequestBody.a(c, jo4.y());
    }
}
