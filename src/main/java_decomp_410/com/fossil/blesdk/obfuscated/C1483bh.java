package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bh */
public class C1483bh {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f3737a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f3738b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.bh$a */
    public static class C1484a extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.widget.ImageView f3739a;

        @DexIgnore
        public C1484a(android.widget.ImageView imageView) {
            this.f3739a = imageView;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            android.widget.ImageView.ScaleType scaleType = (android.widget.ImageView.ScaleType) this.f3739a.getTag(com.fossil.blesdk.obfuscated.C1875gh.save_scale_type);
            this.f3739a.setScaleType(scaleType);
            this.f3739a.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_scale_type, (java.lang.Object) null);
            if (scaleType == android.widget.ImageView.ScaleType.MATRIX) {
                android.widget.ImageView imageView = this.f3739a;
                imageView.setImageMatrix((android.graphics.Matrix) imageView.getTag(com.fossil.blesdk.obfuscated.C1875gh.save_image_matrix));
                this.f3739a.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_image_matrix, (java.lang.Object) null);
            }
            animator.removeListener(this);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4969a(android.widget.ImageView imageView) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            android.widget.ImageView.ScaleType scaleType = imageView.getScaleType();
            imageView.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_scale_type, scaleType);
            android.widget.ImageView.ScaleType scaleType2 = android.widget.ImageView.ScaleType.MATRIX;
            if (scaleType == scaleType2) {
                imageView.setTag(com.fossil.blesdk.obfuscated.C1875gh.save_image_matrix, imageView.getImageMatrix());
            } else {
                imageView.setScaleType(scaleType2);
            }
            imageView.setImageMatrix(com.fossil.blesdk.obfuscated.C1555ch.f4093a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4971a(android.widget.ImageView imageView, android.graphics.Matrix matrix) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            imageView.setImageMatrix(matrix);
            return;
        }
        m4968a();
        java.lang.reflect.Method method = f3737a;
        if (method != null) {
            try {
                method.invoke(imageView, new java.lang.Object[]{matrix});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4968a() {
        if (!f3738b) {
            try {
                f3737a = android.widget.ImageView.class.getDeclaredMethod("animateTransform", new java.lang.Class[]{android.graphics.Matrix.class});
                f3737a.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ImageViewUtils", "Failed to retrieve animateTransform method", e);
            }
            f3738b = true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4970a(android.widget.ImageView imageView, android.animation.Animator animator) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            animator.addListener(new com.fossil.blesdk.obfuscated.C1483bh.C1484a(imageView));
        }
    }
}
