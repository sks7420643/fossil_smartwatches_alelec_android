package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class e5 extends g5 {
    @DexIgnore
    public ConstraintAnchor c;
    @DexIgnore
    public e5 d;
    @DexIgnore
    public float e;
    @DexIgnore
    public e5 f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public e5 i;
    @DexIgnore
    public f5 j; // = null;
    @DexIgnore
    public int k; // = 1;
    @DexIgnore
    public f5 l; // = null;
    @DexIgnore
    public int m; // = 1;

    @DexIgnore
    public e5(ConstraintAnchor constraintAnchor) {
        this.c = constraintAnchor;
    }

    @DexIgnore
    public String a(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    @DexIgnore
    public void a(e5 e5Var, float f2) {
        if (this.b == 0 || !(this.f == e5Var || this.g == f2)) {
            this.f = e5Var;
            this.g = f2;
            if (this.b == 1) {
                b();
            }
            a();
        }
    }

    @DexIgnore
    public void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void d() {
        super.d();
        this.d = null;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.j = null;
        this.k = 1;
        this.l = null;
        this.m = 1;
        this.f = null;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = null;
        this.h = 0;
    }

    @DexIgnore
    public void e() {
        float f2;
        float f3;
        float f4;
        float f5;
        boolean z = true;
        if (this.b != 1 && this.h != 4) {
            f5 f5Var = this.j;
            if (f5Var != null) {
                if (f5Var.b == 1) {
                    this.e = ((float) this.k) * f5Var.c;
                } else {
                    return;
                }
            }
            f5 f5Var2 = this.l;
            if (f5Var2 != null) {
                if (f5Var2.b == 1) {
                    float f6 = f5Var2.c;
                } else {
                    return;
                }
            }
            if (this.h == 1) {
                e5 e5Var = this.d;
                if (e5Var == null || e5Var.b == 1) {
                    e5 e5Var2 = this.d;
                    if (e5Var2 == null) {
                        this.f = this;
                        this.g = this.e;
                    } else {
                        this.f = e5Var2.f;
                        this.g = e5Var2.g + this.e;
                    }
                    a();
                    return;
                }
            }
            if (this.h == 2) {
                e5 e5Var3 = this.d;
                if (e5Var3 != null && e5Var3.b == 1) {
                    e5 e5Var4 = this.i;
                    if (e5Var4 != null) {
                        e5 e5Var5 = e5Var4.d;
                        if (e5Var5 != null && e5Var5.b == 1) {
                            if (q4.j() != null) {
                                q4.j().v++;
                            }
                            this.f = this.d.f;
                            e5 e5Var6 = this.i;
                            e5Var6.f = e5Var6.d.f;
                            ConstraintAnchor.Type type = this.c.c;
                            int i2 = 0;
                            if (!(type == ConstraintAnchor.Type.RIGHT || type == ConstraintAnchor.Type.BOTTOM)) {
                                z = false;
                            }
                            if (z) {
                                f3 = this.d.g;
                                f2 = this.i.d.g;
                            } else {
                                f3 = this.i.d.g;
                                f2 = this.d.g;
                            }
                            float f7 = f3 - f2;
                            ConstraintAnchor constraintAnchor = this.c;
                            ConstraintAnchor.Type type2 = constraintAnchor.c;
                            if (type2 == ConstraintAnchor.Type.LEFT || type2 == ConstraintAnchor.Type.RIGHT) {
                                f5 = f7 - ((float) this.c.b.t());
                                f4 = this.c.b.V;
                            } else {
                                f5 = f7 - ((float) constraintAnchor.b.j());
                                f4 = this.c.b.W;
                            }
                            int b = this.c.b();
                            int b2 = this.i.c.b();
                            if (this.c.g() == this.i.c.g()) {
                                f4 = 0.5f;
                                b2 = 0;
                            } else {
                                i2 = b;
                            }
                            float f8 = (float) i2;
                            float f9 = (float) b2;
                            float f10 = (f5 - f8) - f9;
                            if (z) {
                                e5 e5Var7 = this.i;
                                e5Var7.g = e5Var7.d.g + f9 + (f10 * f4);
                                this.g = (this.d.g - f8) - (f10 * (1.0f - f4));
                            } else {
                                this.g = this.d.g + f8 + (f10 * f4);
                                e5 e5Var8 = this.i;
                                e5Var8.g = (e5Var8.d.g - f9) - (f10 * (1.0f - f4));
                            }
                            a();
                            this.i.a();
                            return;
                        }
                    }
                }
            }
            if (this.h == 3) {
                e5 e5Var9 = this.d;
                if (e5Var9 != null && e5Var9.b == 1) {
                    e5 e5Var10 = this.i;
                    if (e5Var10 != null) {
                        e5 e5Var11 = e5Var10.d;
                        if (e5Var11 != null && e5Var11.b == 1) {
                            if (q4.j() != null) {
                                q4.j().w++;
                            }
                            e5 e5Var12 = this.d;
                            this.f = e5Var12.f;
                            e5 e5Var13 = this.i;
                            e5 e5Var14 = e5Var13.d;
                            e5Var13.f = e5Var14.f;
                            this.g = e5Var12.g + this.e;
                            e5Var13.g = e5Var14.g + e5Var13.e;
                            a();
                            this.i.a();
                            return;
                        }
                    }
                }
            }
            if (this.h == 5) {
                this.c.b.H();
            }
        }
    }

    @DexIgnore
    public float f() {
        return this.g;
    }

    @DexIgnore
    public void g() {
        ConstraintAnchor g2 = this.c.g();
        if (g2 != null) {
            if (g2.g() == this.c) {
                this.h = 4;
                g2.d().h = 4;
            }
            int b = this.c.b();
            ConstraintAnchor.Type type = this.c.c;
            if (type == ConstraintAnchor.Type.RIGHT || type == ConstraintAnchor.Type.BOTTOM) {
                b = -b;
            }
            a(g2.d(), b);
        }
    }

    @DexIgnore
    public String toString() {
        if (this.b != 1) {
            return "{ " + this.c + " UNRESOLVED} type: " + a(this.h);
        } else if (this.f == this) {
            return "[" + this.c + ", RESOLVED: " + this.g + "]  type: " + a(this.h);
        } else {
            return "[" + this.c + ", RESOLVED: " + this.f + ":" + this.g + "] type: " + a(this.h);
        }
    }

    @DexIgnore
    public void b(e5 e5Var, float f2) {
        this.i = e5Var;
    }

    @DexIgnore
    public void b(e5 e5Var, int i2, f5 f5Var) {
        this.i = e5Var;
        this.l = f5Var;
        this.m = i2;
    }

    @DexIgnore
    public void a(int i2, e5 e5Var, int i3) {
        this.h = i2;
        this.d = e5Var;
        this.e = (float) i3;
        this.d.a(this);
    }

    @DexIgnore
    public void a(e5 e5Var, int i2) {
        this.d = e5Var;
        this.e = (float) i2;
        this.d.a(this);
    }

    @DexIgnore
    public void a(e5 e5Var, int i2, f5 f5Var) {
        this.d = e5Var;
        this.d.a(this);
        this.j = f5Var;
        this.k = i2;
        this.j.a(this);
    }

    @DexIgnore
    public void a(q4 q4Var) {
        SolverVariable e2 = this.c.e();
        e5 e5Var = this.f;
        if (e5Var == null) {
            q4Var.a(e2, (int) (this.g + 0.5f));
        } else {
            q4Var.a(e2, q4Var.a((Object) e5Var.c), (int) (this.g + 0.5f), 6);
        }
    }
}
