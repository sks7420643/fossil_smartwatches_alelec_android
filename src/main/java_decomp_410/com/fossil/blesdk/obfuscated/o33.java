package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o33 implements Factory<SearchRingPhonePresenter> {
    @DexIgnore
    public static SearchRingPhonePresenter a(k33 k33) {
        return new SearchRingPhonePresenter(k33);
    }
}
