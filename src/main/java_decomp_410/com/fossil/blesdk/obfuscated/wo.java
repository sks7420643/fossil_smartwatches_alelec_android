package com.fossil.blesdk.obfuscated;

import android.content.res.AssetManager;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wo extends qo<ParcelFileDescriptor> {
    @DexIgnore
    public wo(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    public Class<ParcelFileDescriptor> getDataClass() {
        return ParcelFileDescriptor.class;
    }

    @DexIgnore
    public ParcelFileDescriptor a(AssetManager assetManager, String str) throws IOException {
        return assetManager.openFd(str).getParcelFileDescriptor();
    }

    @DexIgnore
    public void a(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        parcelFileDescriptor.close();
    }
}
