package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class d00 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ d00 d;

    @DexIgnore
    public d00(Throwable th, c00 c00) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = c00.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new d00(cause, c00) : null;
    }
}
