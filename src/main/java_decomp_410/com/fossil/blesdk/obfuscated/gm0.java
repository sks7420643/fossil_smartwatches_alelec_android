package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.sina.weibo.sdk.api.ImageObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gm0 {
    @DexIgnore
    public static int a(Context context, String str) {
        PackageInfo b = b(context, str);
        if (b != null) {
            ApplicationInfo applicationInfo = b.applicationInfo;
            if (applicationInfo != null) {
                Bundle bundle = applicationInfo.metaData;
                if (bundle == null) {
                    return -1;
                }
                return bundle.getInt("com.google.android.gms.version", -1);
            }
        }
        return -1;
    }

    @DexIgnore
    public static boolean a() {
        return false;
    }

    @DexIgnore
    public static PackageInfo b(Context context, String str) {
        try {
            return bn0.b(context).b(str, 128);
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean c(Context context, String str) {
        "com.google.android.gms".equals(str);
        try {
            if ((bn0.b(context).a(str, 0).flags & ImageObject.DATA_SIZE) != 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }
}
