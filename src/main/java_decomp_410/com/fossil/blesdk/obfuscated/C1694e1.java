package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e1 */
public final class C1694e1 extends com.fossil.blesdk.obfuscated.C2452n1 implements com.fossil.blesdk.obfuscated.C2618p1, android.view.View.OnKeyListener, android.widget.PopupWindow.OnDismissListener {

    @DexIgnore
    /* renamed from: F */
    public static /* final */ int f4637F; // = com.fossil.blesdk.obfuscated.C3250x.abc_cascading_menu_item_layout;

    @DexIgnore
    /* renamed from: A */
    public boolean f4638A;

    @DexIgnore
    /* renamed from: B */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f4639B;

    @DexIgnore
    /* renamed from: C */
    public android.view.ViewTreeObserver f4640C;

    @DexIgnore
    /* renamed from: D */
    public android.widget.PopupWindow.OnDismissListener f4641D;

    @DexIgnore
    /* renamed from: E */
    public boolean f4642E;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.Context f4643f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f4644g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ int f4645h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f4646i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ boolean f4647j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ android.os.Handler f4648k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1915h1> f4649l; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1694e1.C1699d> f4650m; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: n */
    public /* final */ android.view.ViewTreeObserver.OnGlobalLayoutListener f4651n; // = new com.fossil.blesdk.obfuscated.C1694e1.C1695a();

    @DexIgnore
    /* renamed from: o */
    public /* final */ android.view.View.OnAttachStateChangeListener f4652o; // = new com.fossil.blesdk.obfuscated.C1694e1.C1696b();

    @DexIgnore
    /* renamed from: p */
    public /* final */ com.fossil.blesdk.obfuscated.C2520o2 f4653p; // = new com.fossil.blesdk.obfuscated.C1694e1.C1697c();

    @DexIgnore
    /* renamed from: q */
    public int f4654q; // = 0;

    @DexIgnore
    /* renamed from: r */
    public int f4655r; // = 0;

    @DexIgnore
    /* renamed from: s */
    public android.view.View f4656s;

    @DexIgnore
    /* renamed from: t */
    public android.view.View f4657t;

    @DexIgnore
    /* renamed from: u */
    public int f4658u;

    @DexIgnore
    /* renamed from: v */
    public boolean f4659v;

    @DexIgnore
    /* renamed from: w */
    public boolean f4660w;

    @DexIgnore
    /* renamed from: x */
    public int f4661x;

    @DexIgnore
    /* renamed from: y */
    public int f4662y;

    @DexIgnore
    /* renamed from: z */
    public boolean f4663z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.e1$a */
    public class C1695a implements android.view.ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public C1695a() {
        }

        @DexIgnore
        public void onGlobalLayout() {
            if (com.fossil.blesdk.obfuscated.C1694e1.this.mo862d() && com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.size() > 0 && !com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.get(0).f4671a.mo876l()) {
                android.view.View view = com.fossil.blesdk.obfuscated.C1694e1.this.f4657t;
                if (view == null || !view.isShown()) {
                    com.fossil.blesdk.obfuscated.C1694e1.this.dismiss();
                    return;
                }
                for (com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar : com.fossil.blesdk.obfuscated.C1694e1.this.f4650m) {
                    dVar.f4671a.mo724c();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e1$b")
    /* renamed from: com.fossil.blesdk.obfuscated.e1$b */
    public class C1696b implements android.view.View.OnAttachStateChangeListener {
        @DexIgnore
        public C1696b() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(android.view.View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(android.view.View view) {
            android.view.ViewTreeObserver viewTreeObserver = com.fossil.blesdk.obfuscated.C1694e1.this.f4640C;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    com.fossil.blesdk.obfuscated.C1694e1.this.f4640C = view.getViewTreeObserver();
                }
                com.fossil.blesdk.obfuscated.C1694e1 e1Var = com.fossil.blesdk.obfuscated.C1694e1.this;
                e1Var.f4640C.removeGlobalOnLayoutListener(e1Var.f4651n);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e1$c")
    /* renamed from: com.fossil.blesdk.obfuscated.e1$c */
    public class C1697c implements com.fossil.blesdk.obfuscated.C2520o2 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e1$c$a")
        /* renamed from: com.fossil.blesdk.obfuscated.e1$c$a */
        public class C1698a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1694e1.C1699d f4667e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ android.view.MenuItem f4668f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1915h1 f4669g;

            @DexIgnore
            public C1698a(com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar, android.view.MenuItem menuItem, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
                this.f4667e = dVar;
                this.f4668f = menuItem;
                this.f4669g = h1Var;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar = this.f4667e;
                if (dVar != null) {
                    com.fossil.blesdk.obfuscated.C1694e1.this.f4642E = true;
                    dVar.f4672b.mo11464a(false);
                    com.fossil.blesdk.obfuscated.C1694e1.this.f4642E = false;
                }
                if (this.f4668f.isEnabled() && this.f4668f.hasSubMenu()) {
                    this.f4669g.mo11465a(this.f4668f, 4);
                }
            }
        }

        @DexIgnore
        public C1697c() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10277a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
            com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar = null;
            com.fossil.blesdk.obfuscated.C1694e1.this.f4648k.removeCallbacksAndMessages((java.lang.Object) null);
            int size = com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    i = -1;
                    break;
                } else if (h1Var == com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.get(i).f4672b) {
                    break;
                } else {
                    i++;
                }
            }
            if (i != -1) {
                int i2 = i + 1;
                if (i2 < com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.size()) {
                    dVar = com.fossil.blesdk.obfuscated.C1694e1.this.f4650m.get(i2);
                }
                com.fossil.blesdk.obfuscated.C1694e1.this.f4648k.postAtTime(new com.fossil.blesdk.obfuscated.C1694e1.C1697c.C1698a(dVar, menuItem, h1Var), h1Var, android.os.SystemClock.uptimeMillis() + 200);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo10278b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
            com.fossil.blesdk.obfuscated.C1694e1.this.f4648k.removeCallbacksAndMessages(h1Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e1$d")
    /* renamed from: com.fossil.blesdk.obfuscated.e1$d */
    public static class C1699d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2621p2 f4671a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1915h1 f4672b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f4673c;

        @DexIgnore
        public C1699d(com.fossil.blesdk.obfuscated.C2621p2 p2Var, com.fossil.blesdk.obfuscated.C1915h1 h1Var, int i) {
            this.f4671a = p2Var;
            this.f4672b = h1Var;
            this.f4673c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public android.widget.ListView mo10280a() {
            return this.f4671a.mo864e();
        }
    }

    @DexIgnore
    public C1694e1(android.content.Context context, android.view.View view, int i, int i2, boolean z) {
        this.f4643f = context;
        this.f4656s = view;
        this.f4645h = i;
        this.f4646i = i2;
        this.f4647j = z;
        this.f4663z = false;
        this.f4658u = mo10271i();
        android.content.res.Resources resources = context.getResources();
        this.f4644g = java.lang.Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2998u.abc_config_prefDialogWidth));
        this.f4648k = new android.os.Handler();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1159a(android.os.Parcelable parcelable) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10261a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        h1Var.mo11462a((com.fossil.blesdk.obfuscated.C2618p1) this, this.f4643f);
        if (mo862d()) {
            mo10268d(h1Var);
        } else {
            this.f4649l.add(h1Var);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1162a() {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Parcelable mo1165b() {
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10263b(boolean z) {
        this.f4663z = z;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo724c() {
        if (!mo862d()) {
            for (com.fossil.blesdk.obfuscated.C1915h1 d : this.f4649l) {
                mo10268d(d);
            }
            this.f4649l.clear();
            this.f4657t = this.f4656s;
            if (this.f4657t != null) {
                boolean z = this.f4640C == null;
                this.f4640C = this.f4657t.getViewTreeObserver();
                if (z) {
                    this.f4640C.addOnGlobalLayoutListener(this.f4651n);
                }
                this.f4657t.addOnAttachStateChangeListener(this.f4652o);
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo10267d(int i) {
        java.util.List<com.fossil.blesdk.obfuscated.C1694e1.C1699d> list = this.f4650m;
        android.widget.ListView a = list.get(list.size() - 1).mo10280a();
        int[] iArr = new int[2];
        a.getLocationOnScreen(iArr);
        android.graphics.Rect rect = new android.graphics.Rect();
        this.f4657t.getWindowVisibleDisplayFrame(rect);
        if (this.f4658u == 1) {
            if (iArr[0] + a.getWidth() + i > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @DexIgnore
    public void dismiss() {
        int size = this.f4650m.size();
        if (size > 0) {
            com.fossil.blesdk.obfuscated.C1694e1.C1699d[] dVarArr = (com.fossil.blesdk.obfuscated.C1694e1.C1699d[]) this.f4650m.toArray(new com.fossil.blesdk.obfuscated.C1694e1.C1699d[size]);
            for (int i = size - 1; i >= 0; i--) {
                com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar = dVarArr[i];
                if (dVar.f4671a.mo862d()) {
                    dVar.f4671a.dismiss();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public android.widget.ListView mo864e() {
        if (this.f4650m.isEmpty()) {
            return null;
        }
        java.util.List<com.fossil.blesdk.obfuscated.C1694e1.C1699d> list = this.f4650m;
        return list.get(list.size() - 1).mo10280a();
    }

    @DexIgnore
    /* renamed from: f */
    public boolean mo10269f() {
        return false;
    }

    @DexIgnore
    /* renamed from: h */
    public final com.fossil.blesdk.obfuscated.C2621p2 mo10270h() {
        com.fossil.blesdk.obfuscated.C2621p2 p2Var = new com.fossil.blesdk.obfuscated.C2621p2(this.f4643f, (android.util.AttributeSet) null, this.f4645h, this.f4646i);
        p2Var.mo14606a(this.f4653p);
        p2Var.mo853a((android.widget.AdapterView.OnItemClickListener) this);
        p2Var.mo854a((android.widget.PopupWindow.OnDismissListener) this);
        p2Var.mo852a(this.f4656s);
        p2Var.mo859c(this.f4655r);
        p2Var.mo855a(true);
        p2Var.mo865e(2);
        return p2Var;
    }

    @DexIgnore
    /* renamed from: i */
    public final int mo10271i() {
        return com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f4656s) == 1 ? 0 : 1;
    }

    @DexIgnore
    public void onDismiss() {
        com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar;
        int size = this.f4650m.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                dVar = null;
                break;
            }
            dVar = this.f4650m.get(i);
            if (!dVar.f4671a.mo862d()) {
                break;
            }
            i++;
        }
        if (dVar != null) {
            dVar.f4672b.mo11464a(false);
        }
    }

    @DexIgnore
    public boolean onKey(android.view.View view, int i, android.view.KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10262b(int i) {
        this.f4659v = true;
        this.f4661x = i;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.MenuItem mo10256a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C1915h1 h1Var2) {
        int size = h1Var.size();
        for (int i = 0; i < size; i++) {
            android.view.MenuItem item = h1Var.getItem(i);
            if (item.hasSubMenu() && h1Var2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.View mo10257a(com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        int i;
        com.fossil.blesdk.obfuscated.C1852g1 g1Var;
        android.view.MenuItem a = mo10256a(dVar.f4672b, h1Var);
        if (a == null) {
            return null;
        }
        android.widget.ListView a2 = dVar.mo10280a();
        android.widget.ListAdapter adapter = a2.getAdapter();
        int i2 = 0;
        if (adapter instanceof android.widget.HeaderViewListAdapter) {
            android.widget.HeaderViewListAdapter headerViewListAdapter = (android.widget.HeaderViewListAdapter) adapter;
            i = headerViewListAdapter.getHeadersCount();
            g1Var = (com.fossil.blesdk.obfuscated.C1852g1) headerViewListAdapter.getWrappedAdapter();
        } else {
            g1Var = (com.fossil.blesdk.obfuscated.C1852g1) adapter;
            i = 0;
        }
        int count = g1Var.getCount();
        while (true) {
            if (i2 >= count) {
                i2 = -1;
                break;
            } else if (a == g1Var.getItem(i2)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 == -1) {
            return null;
        }
        int firstVisiblePosition = (i2 + i) - a2.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a2.getChildCount()) {
            return null;
        }
        return a2.getChildAt(firstVisiblePosition);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo10268d(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        android.view.View view;
        com.fossil.blesdk.obfuscated.C1694e1.C1699d dVar;
        int i;
        int i2;
        int i3;
        android.view.LayoutInflater from = android.view.LayoutInflater.from(this.f4643f);
        com.fossil.blesdk.obfuscated.C1852g1 g1Var = new com.fossil.blesdk.obfuscated.C1852g1(h1Var, from, this.f4647j, f4637F);
        if (!mo862d() && this.f4663z) {
            g1Var.mo11100a(true);
        } else if (mo862d()) {
            g1Var.mo11100a(com.fossil.blesdk.obfuscated.C2452n1.m10961b(h1Var));
        }
        int a = com.fossil.blesdk.obfuscated.C2452n1.m10959a(g1Var, (android.view.ViewGroup) null, this.f4643f, this.f4644g);
        com.fossil.blesdk.obfuscated.C2621p2 h = mo10270h();
        h.mo721a((android.widget.ListAdapter) g1Var);
        h.mo857b(a);
        h.mo859c(this.f4655r);
        if (this.f4650m.size() > 0) {
            java.util.List<com.fossil.blesdk.obfuscated.C1694e1.C1699d> list = this.f4650m;
            dVar = list.get(list.size() - 1);
            view = mo10257a(dVar, h1Var);
        } else {
            dVar = null;
            view = null;
        }
        if (view != null) {
            h.mo14609d(false);
            h.mo14607a((java.lang.Object) null);
            int d = mo10267d(a);
            boolean z = d == 1;
            this.f4658u = d;
            if (android.os.Build.VERSION.SDK_INT >= 26) {
                h.mo852a(view);
                i2 = 0;
                i = 0;
            } else {
                int[] iArr = new int[2];
                this.f4656s.getLocationOnScreen(iArr);
                int[] iArr2 = new int[2];
                view.getLocationOnScreen(iArr2);
                if ((this.f4655r & 7) == 5) {
                    iArr[0] = iArr[0] + this.f4656s.getWidth();
                    iArr2[0] = iArr2[0] + view.getWidth();
                }
                i = iArr2[0] - iArr[0];
                i2 = iArr2[1] - iArr[1];
            }
            if ((this.f4655r & 5) != 5) {
                if (z) {
                    a = view.getWidth();
                }
                i3 = i - a;
                h.mo861d(i3);
                h.mo858b(true);
                h.mo871h(i2);
            } else if (!z) {
                a = view.getWidth();
                i3 = i - a;
                h.mo861d(i3);
                h.mo858b(true);
                h.mo871h(i2);
            }
            i3 = i + a;
            h.mo861d(i3);
            h.mo858b(true);
            h.mo871h(i2);
        } else {
            if (this.f4659v) {
                h.mo861d(this.f4661x);
            }
            if (this.f4660w) {
                h.mo871h(this.f4662y);
            }
            h.mo850a(mo13789g());
        }
        this.f4650m.add(new com.fossil.blesdk.obfuscated.C1694e1.C1699d(h, h1Var, this.f4658u));
        h.mo724c();
        android.widget.ListView e = h.mo864e();
        e.setOnKeyListener(this);
        if (dVar == null && this.f4638A && h1Var.mo11507h() != null) {
            android.widget.FrameLayout frameLayout = (android.widget.FrameLayout) from.inflate(com.fossil.blesdk.obfuscated.C3250x.abc_popup_menu_header_item_layout, e, false);
            frameLayout.setEnabled(false);
            ((android.widget.TextView) frameLayout.findViewById(16908310)).setText(h1Var.mo11507h());
            e.addHeaderView(frameLayout, (java.lang.Object) null, false);
            h.mo724c();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final int mo10264c(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        int size = this.f4650m.size();
        for (int i = 0; i < size; i++) {
            if (h1Var == this.f4650m.get(i).f4672b) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10265c(int i) {
        this.f4660w = true;
        this.f4662y = i;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10266c(boolean z) {
        this.f4638A = z;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1161a(boolean z) {
        for (com.fossil.blesdk.obfuscated.C1694e1.C1699d a : this.f4650m) {
            com.fossil.blesdk.obfuscated.C2452n1.m10960a(a.mo10280a().getAdapter()).notifyDataSetChanged();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9013a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        this.f4639B = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        for (com.fossil.blesdk.obfuscated.C1694e1.C1699d next : this.f4650m) {
            if (v1Var == next.f4672b) {
                next.mo10280a().requestFocus();
                return true;
            }
        }
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        mo10261a((com.fossil.blesdk.obfuscated.C1915h1) v1Var);
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f4639B;
        if (aVar != null) {
            aVar.mo534a(v1Var);
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        int c = mo10264c(h1Var);
        if (c >= 0) {
            int i = c + 1;
            if (i < this.f4650m.size()) {
                this.f4650m.get(i).f4672b.mo11464a(false);
            }
            com.fossil.blesdk.obfuscated.C1694e1.C1699d remove = this.f4650m.remove(c);
            remove.f4672b.mo11482b((com.fossil.blesdk.obfuscated.C2618p1) this);
            if (this.f4642E) {
                remove.f4671a.mo14608b((java.lang.Object) null);
                remove.f4671a.mo849a(0);
            }
            remove.f4671a.dismiss();
            int size = this.f4650m.size();
            if (size > 0) {
                this.f4658u = this.f4650m.get(size - 1).f4673c;
            } else {
                this.f4658u = mo10271i();
            }
            if (size == 0) {
                dismiss();
                com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f4639B;
                if (aVar != null) {
                    aVar.mo533a(h1Var, true);
                }
                android.view.ViewTreeObserver viewTreeObserver = this.f4640C;
                if (viewTreeObserver != null) {
                    if (viewTreeObserver.isAlive()) {
                        this.f4640C.removeGlobalOnLayoutListener(this.f4651n);
                    }
                    this.f4640C = null;
                }
                this.f4657t.removeOnAttachStateChangeListener(this.f4652o);
                this.f4641D.onDismiss();
            } else if (z) {
                this.f4650m.get(0).f4672b.mo11464a(false);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10258a(int i) {
        if (this.f4654q != i) {
            this.f4654q = i;
            this.f4655r = com.fossil.blesdk.obfuscated.C2634p8.m12135a(i, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f4656s));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10259a(android.view.View view) {
        if (this.f4656s != view) {
            this.f4656s = view;
            this.f4655r = com.fossil.blesdk.obfuscated.C2634p8.m12135a(this.f4654q, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f4656s));
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo862d() {
        return this.f4650m.size() > 0 && this.f4650m.get(0).f4671a.mo862d();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10260a(android.widget.PopupWindow.OnDismissListener onDismissListener) {
        this.f4641D = onDismissListener;
    }
}
