package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.blesdk.obfuscated.st2;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tt2<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, st2.a {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Cursor f;
    @DexIgnore
    public tt2<VH>.a g;
    @DexIgnore
    public DataSetObserver h;
    @DexIgnore
    public st2 i;
    @DexIgnore
    public FilterQueryProvider j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            tt2.this.b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends DataSetObserver {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onChanged() {
            tt2.this.e = true;
            tt2.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            tt2.this.e = false;
            tt2 tt2 = tt2.this;
            tt2.notifyItemRangeRemoved(0, tt2.getItemCount());
        }
    }

    /*
    static {
        new b((fd4) null);
        String simpleName = tt2.class.getSimpleName();
        kd4.a((Object) simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public tt2(Cursor cursor) {
        boolean z = cursor != null;
        this.f = cursor;
        this.e = z;
        this.g = new a();
        this.h = new c();
        if (z) {
            tt2<VH>.a aVar = this.g;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    kd4.a();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public abstract void a(VH vh, Cursor cursor, int i2);

    @DexIgnore
    public CharSequence b(Cursor cursor) {
        if (cursor != null) {
            String obj = cursor.toString();
            if (obj != null) {
                return obj;
            }
        }
        return "";
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (kd4.a((Object) cursor, (Object) this.f)) {
            return null;
        }
        Cursor cursor2 = this.f;
        if (cursor2 != null) {
            tt2<VH>.a aVar = this.g;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.h;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f = cursor;
        if (cursor != null) {
            tt2<VH>.a aVar2 = this.g;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.h;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.e = true;
            notifyDataSetChanged();
        } else {
            this.e = false;
            notifyItemRangeRemoved(0, getItemCount());
        }
        return cursor2;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.i == null) {
            this.i = new st2(this);
        }
        st2 st2 = this.i;
        if (st2 != null) {
            return st2;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public int getItemCount() {
        if (this.e) {
            Cursor cursor = this.f;
            if (cursor != null) {
                if (cursor != null) {
                    return cursor.getCount();
                }
                kd4.a();
                throw null;
            }
        }
        return 0;
    }

    @DexIgnore
    public void onBindViewHolder(VH vh, int i2) {
        kd4.b(vh, "holder");
        if (!this.e) {
            FLogger.INSTANCE.getLocal().d(k, ".Inside onBindViewHolder the cursor is invalid");
        }
        a(vh, this.f, i2);
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(k, ".Inside onContentChanged");
    }

    @DexIgnore
    public Cursor a() {
        return this.f;
    }

    @DexIgnore
    public void a(Cursor cursor) {
        kd4.b(cursor, "cursor");
        Cursor c2 = c(cursor);
        if (c2 != null) {
            c2.close();
        }
    }

    @DexIgnore
    public Cursor a(CharSequence charSequence) {
        kd4.b(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.j;
        if (filterQueryProvider == null) {
            return this.f;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a(FilterQueryProvider filterQueryProvider) {
        kd4.b(filterQueryProvider, "filterQueryProvider");
        this.j = filterQueryProvider;
    }
}
