package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pl */
public class C2659pl implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.lang.String f8407g; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("EnqueueRunnable");

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2811rj f8408e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2492nj f8409f; // = new com.fossil.blesdk.obfuscated.C2492nj();

    @DexIgnore
    public C2659pl(com.fossil.blesdk.obfuscated.C2811rj rjVar) {
        this.f8408e = rjVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14738a() {
        androidx.work.impl.WorkDatabase g = this.f8408e.mo15597g().mo16462g();
        g.beginTransaction();
        try {
            boolean b = m12305b(this.f8408e);
            g.setTransactionSuccessful();
            return b;
        } finally {
            g.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1804fj mo14739b() {
        return this.f8409f;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14740c() {
        com.fossil.blesdk.obfuscated.C2968tj g = this.f8408e.mo15597g();
        com.fossil.blesdk.obfuscated.C2744qj.m12826a(g.mo16458c(), g.mo16462g(), g.mo16461f());
    }

    @DexIgnore
    public void run() {
        try {
            if (!this.f8408e.mo15598h()) {
                if (mo14738a()) {
                    com.fossil.blesdk.obfuscated.C2813rl.m13254a(this.f8408e.mo15597g().mo16456b(), androidx.work.impl.background.systemalarm.RescheduleReceiver.class, true);
                    mo14740c();
                }
                this.f8409f.mo13990a(com.fossil.blesdk.obfuscated.C1804fj.f5195a);
                return;
            }
            throw new java.lang.IllegalStateException(java.lang.String.format("WorkContinuation has cycles (%s)", new java.lang.Object[]{this.f8408e}));
        } catch (Throwable th) {
            this.f8409f.mo13990a(new com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1807a(th));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m12305b(com.fossil.blesdk.obfuscated.C2811rj rjVar) {
        java.util.List<com.fossil.blesdk.obfuscated.C2811rj> e = rjVar.mo15595e();
        boolean z = false;
        if (e != null) {
            boolean z2 = false;
            for (com.fossil.blesdk.obfuscated.C2811rj next : e) {
                if (!next.mo15599i()) {
                    z2 |= m12305b(next);
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9966e(f8407g, java.lang.String.format("Already enqueued work ids (%s).", new java.lang.Object[]{android.text.TextUtils.join(", ", next.mo15593c())}), new java.lang.Throwable[0]);
                }
            }
            z = z2;
        }
        return m12302a(rjVar) | z;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12302a(com.fossil.blesdk.obfuscated.C2811rj rjVar) {
        boolean a = m12304a(rjVar.mo15597g(), rjVar.mo15596f(), (java.lang.String[]) com.fossil.blesdk.obfuscated.C2811rj.m13220a(rjVar).toArray(new java.lang.String[0]), rjVar.mo15594d(), rjVar.mo15592b());
        rjVar.mo15600j();
        return a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01a8 A[LOOP:6: B:107:0x01a2->B:109:0x01a8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x01d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x011f  */
    /* renamed from: a */
    public static boolean m12304a(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.util.List<? extends com.fossil.blesdk.obfuscated.C2224kj> list, java.lang.String[] strArr, java.lang.String str, androidx.work.ExistingWorkPolicy existingWorkPolicy) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j;
        int i;
        com.fossil.blesdk.obfuscated.C2968tj tjVar2 = tjVar;
        java.lang.String[] strArr2 = strArr;
        java.lang.String str2 = str;
        androidx.work.ExistingWorkPolicy existingWorkPolicy2 = existingWorkPolicy;
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        androidx.work.impl.WorkDatabase g = tjVar.mo16462g();
        boolean z5 = strArr2 != null && strArr2.length > 0;
        if (z5) {
            z3 = true;
            z2 = false;
            z = false;
            for (java.lang.String str3 : strArr2) {
                com.fossil.blesdk.obfuscated.C1954hl e = g.mo3780d().mo12029e(str3);
                if (e == null) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f8407g, java.lang.String.format("Prerequisite %s doesn't exist; not enqueuing", new java.lang.Object[]{str3}), new java.lang.Throwable[0]);
                    return false;
                }
                androidx.work.WorkInfo.State state = e.f5772b;
                z3 &= state == androidx.work.WorkInfo.State.SUCCEEDED;
                if (state == androidx.work.WorkInfo.State.FAILED) {
                    z2 = true;
                } else if (state == androidx.work.WorkInfo.State.CANCELLED) {
                    z = true;
                }
            }
        } else {
            z3 = true;
            z2 = false;
            z = false;
        }
        boolean z6 = !android.text.TextUtils.isEmpty(str);
        if (z6 && !z5) {
            java.util.List<com.fossil.blesdk.obfuscated.C1954hl.C1956b> a = g.mo3780d().mo12019a(str2);
            if (!a.isEmpty()) {
                if (existingWorkPolicy2 == androidx.work.ExistingWorkPolicy.APPEND) {
                    com.fossil.blesdk.obfuscated.C3443zk a2 = g.mo3777a();
                    java.util.ArrayList arrayList = new java.util.ArrayList();
                    for (com.fossil.blesdk.obfuscated.C1954hl.C1956b next : a) {
                        if (!a2.mo8782c(next.f5787a)) {
                            boolean z7 = (next.f5788b == androidx.work.WorkInfo.State.SUCCEEDED) & z3;
                            androidx.work.WorkInfo.State state2 = next.f5788b;
                            if (state2 == androidx.work.WorkInfo.State.FAILED) {
                                z2 = true;
                            } else if (state2 == androidx.work.WorkInfo.State.CANCELLED) {
                                z = true;
                            }
                            arrayList.add(next.f5787a);
                            z3 = z7;
                        }
                    }
                    strArr2 = (java.lang.String[]) arrayList.toArray(strArr2);
                    z5 = strArr2.length > 0;
                } else {
                    if (existingWorkPolicy2 == androidx.work.ExistingWorkPolicy.KEEP) {
                        for (com.fossil.blesdk.obfuscated.C1954hl.C1956b bVar : a) {
                            androidx.work.WorkInfo.State state3 = bVar.f5788b;
                            if (state3 == androidx.work.WorkInfo.State.ENQUEUED) {
                                return false;
                            }
                            if (state3 == androidx.work.WorkInfo.State.RUNNING) {
                                return false;
                            }
                        }
                    }
                    com.fossil.blesdk.obfuscated.C2575ol.m11833a(str2, tjVar2, false).run();
                    com.fossil.blesdk.obfuscated.C2036il d = g.mo3780d();
                    for (com.fossil.blesdk.obfuscated.C1954hl.C1956b bVar2 : a) {
                        d.mo12023b(bVar2.f5787a);
                    }
                    z4 = true;
                    for (com.fossil.blesdk.obfuscated.C2224kj kjVar : list) {
                        com.fossil.blesdk.obfuscated.C1954hl d2 = kjVar.mo12808d();
                        if (!z5 || z3) {
                            if (!d2.mo11673d()) {
                                d2.f5784n = currentTimeMillis;
                            } else {
                                j = currentTimeMillis;
                                d2.f5784n = 0;
                                i = android.os.Build.VERSION.SDK_INT;
                                if (i < 23 && i <= 25) {
                                    m12301a(d2);
                                } else if (android.os.Build.VERSION.SDK_INT <= 22 && m12303a(tjVar2, "androidx.work.impl.background.gcm.GcmScheduler")) {
                                    m12301a(d2);
                                }
                                if (d2.f5772b == androidx.work.WorkInfo.State.ENQUEUED) {
                                    z4 = true;
                                }
                                g.mo3780d().mo12020a(d2);
                                if (z5) {
                                    for (java.lang.String ykVar : strArr2) {
                                        g.mo3777a().mo8780a(new com.fossil.blesdk.obfuscated.C3368yk(kjVar.mo12806b(), ykVar));
                                    }
                                }
                                for (java.lang.String klVar : kjVar.mo12807c()) {
                                    g.mo3781e().mo13307a(new com.fossil.blesdk.obfuscated.C2229kl(klVar, kjVar.mo12806b()));
                                }
                                if (!z6) {
                                    g.mo3779c().mo10960a(new com.fossil.blesdk.obfuscated.C1736el(str2, kjVar.mo12806b()));
                                }
                                currentTimeMillis = j;
                            }
                        } else if (z2) {
                            d2.f5772b = androidx.work.WorkInfo.State.FAILED;
                        } else if (z) {
                            d2.f5772b = androidx.work.WorkInfo.State.CANCELLED;
                        } else {
                            d2.f5772b = androidx.work.WorkInfo.State.BLOCKED;
                        }
                        j = currentTimeMillis;
                        i = android.os.Build.VERSION.SDK_INT;
                        if (i < 23) {
                        }
                        m12301a(d2);
                        if (d2.f5772b == androidx.work.WorkInfo.State.ENQUEUED) {
                        }
                        g.mo3780d().mo12020a(d2);
                        if (z5) {
                        }
                        while (r3.hasNext()) {
                        }
                        if (!z6) {
                        }
                        currentTimeMillis = j;
                    }
                    return z4;
                }
            }
        }
        z4 = false;
        while (r8.hasNext()) {
        }
        return z4;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m12301a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        com.fossil.blesdk.obfuscated.C3365yi yiVar = hlVar.f5780j;
        if (yiVar.mo18116f() || yiVar.mo18120i()) {
            java.lang.String str = hlVar.f5773c;
            com.fossil.blesdk.obfuscated.C1419aj.C1420a aVar = new com.fossil.blesdk.obfuscated.C1419aj.C1420a();
            aVar.mo8752a(hlVar.f5775e);
            aVar.mo8754a("androidx.work.impl.workers.ConstraintTrackingWorker.ARGUMENT_CLASS_NAME", str);
            hlVar.f5773c = androidx.work.impl.workers.ConstraintTrackingWorker.class.getName();
            hlVar.f5775e = aVar.mo8756a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m12303a(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str) {
        try {
            java.lang.Class<?> cls = java.lang.Class.forName(str);
            for (com.fossil.blesdk.obfuscated.C2656pj pjVar : tjVar.mo16461f()) {
                if (cls.isAssignableFrom(pjVar.getClass())) {
                    return true;
                }
            }
        } catch (java.lang.ClassNotFoundException unused) {
        }
        return false;
    }
}
