package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jc {
    @DexIgnore
    public static final zg4 a(ic icVar) {
        kd4.b(icVar, "$this$viewModelScope");
        zg4 zg4 = (zg4) icVar.a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (zg4 != null) {
            return zg4;
        }
        Object a = icVar.a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new ob(zi4.a((fi4) null, 1, (Object) null).plus(nh4.c().C())));
        kd4.a(a, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (zg4) a;
    }
}
