package com.fossil.blesdk.obfuscated;

import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class th1 extends ui1 {
    @DexIgnore
    public static /* final */ AtomicLong l; // = new AtomicLong(Long.MIN_VALUE);
    @DexIgnore
    public wh1 c;
    @DexIgnore
    public wh1 d;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<vh1<?>> e; // = new PriorityBlockingQueue<>();
    @DexIgnore
    public /* final */ BlockingQueue<vh1<?>> f; // = new LinkedBlockingQueue();
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler g; // = new uh1(this, "Thread death: Uncaught exception on worker thread");
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler h; // = new uh1(this, "Thread death: Uncaught exception on network thread");
    @DexIgnore
    public /* final */ Object i; // = new Object();
    @DexIgnore
    public /* final */ Semaphore j; // = new Semaphore(2);
    @DexIgnore
    public volatile boolean k;

    @DexIgnore
    public th1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final <V> Future<V> a(Callable<V> callable) throws IllegalStateException {
        n();
        bk0.a(callable);
        vh1 vh1 = new vh1(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.e.isEmpty()) {
                d().v().a("Callable skipped the worker queue.");
            }
            vh1.run();
        } else {
            a((vh1<?>) vh1);
        }
        return vh1;
    }

    @DexIgnore
    public final <V> Future<V> b(Callable<V> callable) throws IllegalStateException {
        n();
        bk0.a(callable);
        vh1 vh1 = new vh1(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            vh1.run();
        } else {
            a((vh1<?>) vh1);
        }
        return vh1;
    }

    @DexIgnore
    public final void e() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    @DexIgnore
    public final void g() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    @DexIgnore
    public final boolean p() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        return Thread.currentThread() == this.c;
    }

    @DexIgnore
    public final void b(Runnable runnable) throws IllegalStateException {
        n();
        bk0.a(runnable);
        vh1 vh1 = new vh1(this, runnable, false, "Task exception on network thread");
        synchronized (this.i) {
            this.f.add(vh1);
            if (this.d == null) {
                this.d = new wh1(this, "Measurement Network", this.f);
                this.d.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                this.d.a();
            }
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        n();
        bk0.a(runnable);
        a((vh1<?>) new vh1(this, runnable, false, "Task exception on worker thread"));
    }

    @DexIgnore
    public final void a(vh1<?> vh1) {
        synchronized (this.i) {
            this.e.add(vh1);
            if (this.c == null) {
                this.c = new wh1(this, "Measurement Worker", this.e);
                this.c.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }
}
