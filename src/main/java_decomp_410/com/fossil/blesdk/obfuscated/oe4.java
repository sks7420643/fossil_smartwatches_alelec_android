package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oe4<T> implements re4<T> {
    @DexIgnore
    public /* final */ AtomicReference<re4<T>> a;

    @DexIgnore
    public oe4(re4<? extends T> re4) {
        kd4.b(re4, "sequence");
        this.a = new AtomicReference<>(re4);
    }

    @DexIgnore
    public Iterator<T> iterator() {
        re4 andSet = this.a.getAndSet((Object) null);
        if (andSet != null) {
            return andSet.iterator();
        }
        throw new IllegalStateException("This sequence can be consumed only once.");
    }
}
