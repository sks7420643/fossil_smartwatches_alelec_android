package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.de0;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bh0 {
    @DexIgnore
    <A extends de0.b, T extends te0<? extends me0, A>> T a(T t);

    @DexIgnore
    void a();

    @DexIgnore
    void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    @DexIgnore
    boolean a(cf0 cf0);

    @DexIgnore
    <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    boolean c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    ud0 f();
}
