package com.fossil.blesdk.obfuscated;

import java.util.List;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wj4 {
    @DexIgnore
    public static final pi4 a(MainDispatcherFactory mainDispatcherFactory, List<? extends MainDispatcherFactory> list) {
        kd4.b(mainDispatcherFactory, "$this$tryCreateDispatcher");
        kd4.b(list, "factories");
        try {
            return mainDispatcherFactory.createDispatcher(list);
        } catch (Throwable th) {
            return new xj4(th, mainDispatcherFactory.hintOnError());
        }
    }
}
