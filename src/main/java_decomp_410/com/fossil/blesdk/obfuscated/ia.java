package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import androidx.collection.SparseArrayCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ja;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ia extends l8 {
    @DexIgnore
    public static /* final */ Rect m; // = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);
    @DexIgnore
    public static /* final */ ja.a<q9> n; // = new a();
    @DexIgnore
    public static /* final */ ja.b<SparseArrayCompat<q9>, q9> o; // = new b();
    @DexIgnore
    public /* final */ Rect c; // = new Rect();
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public /* final */ Rect e; // = new Rect();
    @DexIgnore
    public /* final */ int[] f; // = new int[2];
    @DexIgnore
    public /* final */ AccessibilityManager g;
    @DexIgnore
    public /* final */ View h;
    @DexIgnore
    public c i;
    @DexIgnore
    public int j; // = Integer.MIN_VALUE;
    @DexIgnore
    public int k; // = Integer.MIN_VALUE;
    @DexIgnore
    public int l; // = Integer.MIN_VALUE;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ja.a<q9> {
        @DexIgnore
        public void a(q9 q9Var, Rect rect) {
            q9Var.a(rect);
        }
    }

    @DexIgnore
    public ia(View view) {
        if (view != null) {
            this.h = view;
            this.g = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (f9.i(view) == 0) {
                f9.f(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    @DexIgnore
    public static int i(int i2) {
        if (i2 == 19) {
            return 33;
        }
        if (i2 != 21) {
            return i2 != 22 ? 130 : 66;
        }
        return 17;
    }

    @DexIgnore
    public abstract int a(float f2, float f3);

    @DexIgnore
    public r9 a(View view) {
        if (this.i == null) {
            this.i = new c();
        }
        return this.i;
    }

    @DexIgnore
    public void a(int i2, AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public abstract void a(int i2, q9 q9Var);

    @DexIgnore
    public void a(int i2, boolean z) {
    }

    @DexIgnore
    public void a(AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public void a(q9 q9Var) {
    }

    @DexIgnore
    public abstract void a(List<Integer> list);

    @DexIgnore
    public abstract boolean a(int i2, int i3, Bundle bundle);

    @DexIgnore
    public final boolean b(int i2, Rect rect) {
        q9 q9Var;
        q9 q9Var2;
        SparseArrayCompat<q9> d2 = d();
        int i3 = this.k;
        int i4 = Integer.MIN_VALUE;
        if (i3 == Integer.MIN_VALUE) {
            q9Var = null;
        } else {
            q9Var = d2.b(i3);
        }
        q9 q9Var3 = q9Var;
        if (i2 == 1 || i2 == 2) {
            q9Var2 = (q9) ja.a(d2, o, n, q9Var3, i2, f9.k(this.h) == 1, false);
        } else if (i2 == 17 || i2 == 33 || i2 == 66 || i2 == 130) {
            Rect rect2 = new Rect();
            int i5 = this.k;
            if (i5 != Integer.MIN_VALUE) {
                a(i5, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                a(this.h, i2, rect2);
            }
            q9Var2 = (q9) ja.a(d2, o, n, q9Var3, rect2, i2);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        if (q9Var2 != null) {
            i4 = d2.d(d2.a(q9Var2));
        }
        return g(i4);
    }

    @DexIgnore
    public final boolean c(int i2, int i3) {
        if (i2 == Integer.MIN_VALUE || !this.g.isEnabled()) {
            return false;
        }
        ViewParent parent = this.h.getParent();
        if (parent == null) {
            return false;
        }
        return i9.a(parent, this.h, a(i2, i3));
    }

    @DexIgnore
    public final SparseArrayCompat<q9> d() {
        ArrayList arrayList = new ArrayList();
        a((List<Integer>) arrayList);
        SparseArrayCompat<q9> sparseArrayCompat = new SparseArrayCompat<>();
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            sparseArrayCompat.c(i2, d(i2));
        }
        return sparseArrayCompat;
    }

    @DexIgnore
    public q9 e(int i2) {
        if (i2 == -1) {
            return c();
        }
        return d(i2);
    }

    @DexIgnore
    public final boolean f(int i2) {
        if (this.g.isEnabled() && this.g.isTouchExplorationEnabled()) {
            int i3 = this.j;
            if (i3 != i2) {
                if (i3 != Integer.MIN_VALUE) {
                    a(i3);
                }
                this.j = i2;
                this.h.invalidate();
                c(i2, 32768);
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean g(int i2) {
        if (!this.h.isFocused() && !this.h.requestFocus()) {
            return false;
        }
        int i3 = this.k;
        if (i3 == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            b(i3);
        }
        this.k = i2;
        a(i2, true);
        c(i2, 8);
        return true;
    }

    @DexIgnore
    public final void h(int i2) {
        int i3 = this.l;
        if (i3 != i2) {
            this.l = i2;
            c(i2, 128);
            c(i3, 256);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ja.b<SparseArrayCompat<q9>, q9> {
        @DexIgnore
        public q9 a(SparseArrayCompat<q9> sparseArrayCompat, int i) {
            return sparseArrayCompat.f(i);
        }

        @DexIgnore
        public int a(SparseArrayCompat<q9> sparseArrayCompat) {
            return sparseArrayCompat.c();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends r9 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public q9 a(int i) {
            return q9.a(ia.this.e(i));
        }

        @DexIgnore
        public q9 b(int i) {
            int i2 = i == 2 ? ia.this.j : ia.this.k;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return a(i2);
        }

        @DexIgnore
        public boolean a(int i, int i2, Bundle bundle) {
            return ia.this.b(i, i2, bundle);
        }
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        if (!this.g.isEnabled() || !this.g.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int a2 = a(motionEvent.getX(), motionEvent.getY());
            h(a2);
            if (a2 != Integer.MIN_VALUE) {
                return true;
            }
            return false;
        } else if (action != 10 || this.l == Integer.MIN_VALUE) {
            return false;
        } else {
            h(Integer.MIN_VALUE);
            return true;
        }
    }

    @DexIgnore
    public final AccessibilityEvent c(int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        this.h.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    @DexIgnore
    public final q9 c() {
        q9 d2 = q9.d(this.h);
        f9.a(this.h, d2);
        ArrayList arrayList = new ArrayList();
        a((List<Integer>) arrayList);
        if (d2.b() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                d2.a(this.h, ((Integer) arrayList.get(i2)).intValue());
            }
            return d2;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    @DexIgnore
    public final q9 d(int i2) {
        q9 x = q9.x();
        x.h(true);
        x.i(true);
        x.a((CharSequence) "android.view.View");
        x.c(m);
        x.d(m);
        x.b(this.h);
        a(i2, x);
        if (x.h() == null && x.d() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        x.a(this.d);
        if (!this.d.equals(m)) {
            int a2 = x.a();
            if ((a2 & 64) != 0) {
                throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((a2 & 128) == 0) {
                x.e((CharSequence) this.h.getContext().getPackageName());
                x.c(this.h, i2);
                if (this.j == i2) {
                    x.a(true);
                    x.a(128);
                } else {
                    x.a(false);
                    x.a(64);
                }
                boolean z = this.k == i2;
                if (z) {
                    x.a(2);
                } else if (x.o()) {
                    x.a(1);
                }
                x.j(z);
                this.h.getLocationOnScreen(this.f);
                x.b(this.c);
                if (this.c.equals(m)) {
                    x.a(this.c);
                    if (x.b != -1) {
                        q9 x2 = q9.x();
                        for (int i3 = x.b; i3 != -1; i3 = x2.b) {
                            x2.b(this.h, -1);
                            x2.c(m);
                            a(i3, x2);
                            x2.a(this.d);
                            Rect rect = this.c;
                            Rect rect2 = this.d;
                            rect.offset(rect2.left, rect2.top);
                        }
                        x2.v();
                    }
                    this.c.offset(this.f[0] - this.h.getScrollX(), this.f[1] - this.h.getScrollY());
                }
                if (this.h.getLocalVisibleRect(this.e)) {
                    this.e.offset(this.f[0] - this.h.getScrollX(), this.f[1] - this.h.getScrollY());
                    if (this.c.intersect(this.e)) {
                        x.d(this.c);
                        if (a(this.c)) {
                            x.o(true);
                        }
                    }
                }
                return x;
            } else {
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    @DexIgnore
    public final boolean a(KeyEvent keyEvent) {
        int i2 = 0;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 61) {
            if (keyCode != 66) {
                switch (keyCode) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        if (!keyEvent.hasNoModifiers()) {
                            return false;
                        }
                        int i3 = i(keyCode);
                        int repeatCount = keyEvent.getRepeatCount() + 1;
                        boolean z = false;
                        while (i2 < repeatCount && b(i3, (Rect) null)) {
                            i2++;
                            z = true;
                        }
                        return z;
                    case 23:
                        break;
                    default:
                        return false;
                }
            }
            if (!keyEvent.hasNoModifiers() || keyEvent.getRepeatCount() != 0) {
                return false;
            }
            b();
            return true;
        } else if (keyEvent.hasNoModifiers()) {
            return b(2, (Rect) null);
        } else {
            if (keyEvent.hasModifiers(1)) {
                return b(1, (Rect) null);
            }
            return false;
        }
    }

    @DexIgnore
    public final boolean c(int i2, int i3, Bundle bundle) {
        if (i3 == 1) {
            return g(i2);
        }
        if (i3 == 2) {
            return b(i2);
        }
        if (i3 == 64) {
            return f(i2);
        }
        if (i3 != 128) {
            return a(i2, i3, bundle);
        }
        return a(i2);
    }

    @DexIgnore
    public final boolean b() {
        int i2 = this.k;
        return i2 != Integer.MIN_VALUE && a(i2, 16, (Bundle) null);
    }

    @DexIgnore
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        a(accessibilityEvent);
    }

    @DexIgnore
    public final AccessibilityEvent b(int i2, int i3) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i3);
        q9 e2 = e(i2);
        obtain.getText().add(e2.h());
        obtain.setContentDescription(e2.d());
        obtain.setScrollable(e2.s());
        obtain.setPassword(e2.r());
        obtain.setEnabled(e2.n());
        obtain.setChecked(e2.l());
        a(i2, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setClassName(e2.c());
            s9.a(obtain, this.h, i2);
            obtain.setPackageName(this.h.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    @DexIgnore
    public final void a(boolean z, int i2, Rect rect) {
        int i3 = this.k;
        if (i3 != Integer.MIN_VALUE) {
            b(i3);
        }
        if (z) {
            b(i2, rect);
        }
    }

    @DexIgnore
    public final void a(int i2, Rect rect) {
        e(i2).a(rect);
    }

    @DexIgnore
    public static Rect a(View view, int i2, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i2 == 17) {
            rect.set(width, 0, width, height);
        } else if (i2 == 33) {
            rect.set(0, height, width, height);
        } else if (i2 == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i2 == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    @DexIgnore
    public boolean b(int i2, int i3, Bundle bundle) {
        if (i2 != -1) {
            return c(i2, i3, bundle);
        }
        return a(i3, bundle);
    }

    @DexIgnore
    public final AccessibilityEvent a(int i2, int i3) {
        if (i2 != -1) {
            return b(i2, i3);
        }
        return c(i3);
    }

    @DexIgnore
    public final boolean b(int i2) {
        if (this.k != i2) {
            return false;
        }
        this.k = Integer.MIN_VALUE;
        a(i2, false);
        c(i2, 8);
        return true;
    }

    @DexIgnore
    public void a(View view, q9 q9Var) {
        super.a(view, q9Var);
        a(q9Var);
    }

    @DexIgnore
    public final boolean a(int i2, Bundle bundle) {
        return f9.a(this.h, i2, bundle);
    }

    @DexIgnore
    public final boolean a(Rect rect) {
        if (rect == null || rect.isEmpty() || this.h.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.h.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (view.getAlpha() <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        if (parent != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(int i2) {
        if (this.j != i2) {
            return false;
        }
        this.j = Integer.MIN_VALUE;
        this.h.invalidate();
        c(i2, 65536);
        return true;
    }
}
