package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z90 extends aa0 {
    @DexIgnore
    public static long k; // = 100;
    @DexIgnore
    public static /* final */ z90 l; // = new z90();

    @DexIgnore
    public z90() {
        super("raw_minute_data", 204800, "raw_minute_data", "raw_minute_data", new na0("", "", ""), 1800, new ba0(), SharedPreferenceFileName.MINUTE_DATA_REFERENCE, false);
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
