package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s51 extends vb1<s51> {
    @DexIgnore
    public static volatile s51[] j;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public t51[] e; // = t51.e();
    @DexIgnore
    public Boolean f; // = null;
    @DexIgnore
    public u51 g; // = null;
    @DexIgnore
    public Boolean h; // = null;
    @DexIgnore
    public Boolean i; // = null;

    @DexIgnore
    public s51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static s51[] e() {
        if (j == null) {
            synchronized (zb1.b) {
                if (j == null) {
                    j = new s51[0];
                }
            }
        }
        return j;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        t51[] t51Arr = this.e;
        if (t51Arr != null && t51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                t51[] t51Arr2 = this.e;
                if (i2 >= t51Arr2.length) {
                    break;
                }
                t51 t51 = t51Arr2[i2];
                if (t51 != null) {
                    ub1.a(3, (ac1) t51);
                }
                i2++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            ub1.a(4, bool.booleanValue());
        }
        u51 u51 = this.g;
        if (u51 != null) {
            ub1.a(5, (ac1) u51);
        }
        Boolean bool2 = this.h;
        if (bool2 != null) {
            ub1.a(6, bool2.booleanValue());
        }
        Boolean bool3 = this.i;
        if (bool3 != null) {
            ub1.a(7, bool3.booleanValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof s51)) {
            return false;
        }
        s51 s51 = (s51) obj;
        Integer num = this.c;
        if (num == null) {
            if (s51.c != null) {
                return false;
            }
        } else if (!num.equals(s51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (s51.d != null) {
                return false;
            }
        } else if (!str.equals(s51.d)) {
            return false;
        }
        if (!zb1.a((Object[]) this.e, (Object[]) s51.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (s51.f != null) {
                return false;
            }
        } else if (!bool.equals(s51.f)) {
            return false;
        }
        u51 u51 = this.g;
        if (u51 == null) {
            if (s51.g != null) {
                return false;
            }
        } else if (!u51.equals(s51.g)) {
            return false;
        }
        Boolean bool2 = this.h;
        if (bool2 == null) {
            if (s51.h != null) {
                return false;
            }
        } else if (!bool2.equals(s51.h)) {
            return false;
        }
        Boolean bool3 = this.i;
        if (bool3 == null) {
            if (s51.i != null) {
                return false;
            }
        } else if (!bool3.equals(s51.i)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(s51.b);
        }
        xb1 xb12 = s51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i2;
        int hashCode = (s51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i3 = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (((hashCode2 + (str == null ? 0 : str.hashCode())) * 31) + zb1.a((Object[]) this.e)) * 31;
        Boolean bool = this.f;
        int hashCode4 = hashCode3 + (bool == null ? 0 : bool.hashCode());
        u51 u51 = this.g;
        int i4 = hashCode4 * 31;
        if (u51 == null) {
            i2 = 0;
        } else {
            i2 = u51.hashCode();
        }
        int i5 = (i4 + i2) * 31;
        Boolean bool2 = this.h;
        int hashCode5 = (i5 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Boolean bool3 = this.i;
        int hashCode6 = (hashCode5 + (bool3 == null ? 0 : bool3.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode6 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        t51[] t51Arr = this.e;
        if (t51Arr != null && t51Arr.length > 0) {
            int i2 = 0;
            while (true) {
                t51[] t51Arr2 = this.e;
                if (i2 >= t51Arr2.length) {
                    break;
                }
                t51 t51 = t51Arr2[i2];
                if (t51 != null) {
                    a += ub1.b(3, (ac1) t51);
                }
                i2++;
            }
        }
        Boolean bool = this.f;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(4) + 1;
        }
        u51 u51 = this.g;
        if (u51 != null) {
            a += ub1.b(5, (ac1) u51);
        }
        Boolean bool2 = this.h;
        if (bool2 != null) {
            bool2.booleanValue();
            a += ub1.c(6) + 1;
        }
        Boolean bool3 = this.i;
        if (bool3 == null) {
            return a;
        }
        bool3.booleanValue();
        return a + ub1.c(7) + 1;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 26) {
                int a = dc1.a(tb1, 26);
                t51[] t51Arr = this.e;
                int length = t51Arr == null ? 0 : t51Arr.length;
                t51[] t51Arr2 = new t51[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.e, 0, t51Arr2, 0, length);
                }
                while (length < t51Arr2.length - 1) {
                    t51Arr2[length] = new t51();
                    tb1.a((ac1) t51Arr2[length]);
                    tb1.c();
                    length++;
                }
                t51Arr2[length] = new t51();
                tb1.a((ac1) t51Arr2[length]);
                this.e = t51Arr2;
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(tb1.d());
            } else if (c2 == 42) {
                if (this.g == null) {
                    this.g = new u51();
                }
                tb1.a((ac1) this.g);
            } else if (c2 == 48) {
                this.h = Boolean.valueOf(tb1.d());
            } else if (c2 == 56) {
                this.i = Boolean.valueOf(tb1.d());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
