package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ic0 extends oj0<uc0> {
    @DexIgnore
    public /* final */ GoogleSignInOptions E;

    @DexIgnore
    public ic0(Context context, Looper looper, kj0 kj0, GoogleSignInOptions googleSignInOptions, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 91, kj0, bVar, cVar);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.a().a() : googleSignInOptions;
        if (!kj0.d().isEmpty()) {
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(googleSignInOptions);
            for (Scope a : kj0.d()) {
                aVar.a(a, new Scope[0]);
            }
            googleSignInOptions = aVar.a();
        }
        this.E = googleSignInOptions;
    }

    @DexIgnore
    public final GoogleSignInOptions G() {
        return this.E;
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        if (queryLocalInterface instanceof uc0) {
            return (uc0) queryLocalInterface;
        }
        return new vc0(iBinder);
    }

    @DexIgnore
    public final boolean d() {
        return true;
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final Intent k() {
        return jc0.a(t(), this.E);
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
}
