package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yn */
public abstract class C3371yn<CHILD extends com.fossil.blesdk.obfuscated.C3371yn<CHILD, TranscodeType>, TranscodeType> implements java.lang.Cloneable {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1836fw<? super TranscodeType> f11292e; // = com.fossil.blesdk.obfuscated.C1663dw.m6070a();

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1836fw<? super TranscodeType> mo18131a() {
        return this.f11292e;
    }

    @DexIgnore
    public final CHILD clone() {
        try {
            return (com.fossil.blesdk.obfuscated.C3371yn) super.clone();
        } catch (java.lang.CloneNotSupportedException e) {
            throw new java.lang.RuntimeException(e);
        }
    }
}
