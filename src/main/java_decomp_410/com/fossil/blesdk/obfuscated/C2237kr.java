package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kr */
public class C2237kr<Data> implements com.fossil.blesdk.obfuscated.C2912sr<java.io.File, Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> f6945a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.kr$a */
    public static class C2238a<Data> implements com.fossil.blesdk.obfuscated.C2984tr<java.io.File, Data> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> f6946a;

        @DexIgnore
        public C2238a(com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> dVar) {
            this.f6946a = dVar;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.fossil.blesdk.obfuscated.C2912sr<java.io.File, Data> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C2237kr(this.f6946a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.kr$b */
    public static class C2239b extends com.fossil.blesdk.obfuscated.C2237kr.C2238a<android.os.ParcelFileDescriptor> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.kr$b$a */
        public class C2240a implements com.fossil.blesdk.obfuscated.C2237kr.C2242d<android.os.ParcelFileDescriptor> {
            @DexIgnore
            public java.lang.Class<android.os.ParcelFileDescriptor> getDataClass() {
                return android.os.ParcelFileDescriptor.class;
            }

            @DexIgnore
            /* renamed from: a */
            public android.os.ParcelFileDescriptor m9736a(java.io.File file) throws java.io.FileNotFoundException {
                return android.os.ParcelFileDescriptor.open(file, 268435456);
            }

            @DexIgnore
            /* renamed from: a */
            public void mo12864a(android.os.ParcelFileDescriptor parcelFileDescriptor) throws java.io.IOException {
                parcelFileDescriptor.close();
            }
        }

        @DexIgnore
        public C2239b() {
            super(new com.fossil.blesdk.obfuscated.C2237kr.C2239b.C2240a());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.kr$c */
    public static final class C2241c<Data> implements com.fossil.blesdk.obfuscated.C2902so<Data> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.io.File f6947e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> f6948f;

        @DexIgnore
        /* renamed from: g */
        public Data f6949g;

        @DexIgnore
        public C2241c(java.io.File file, com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> dVar) {
            this.f6947e = file;
            this.f6948f = dVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Data> aVar) {
            try {
                this.f6949g = this.f6948f.mo12862a(this.f6947e);
                aVar.mo9252a(this.f6949g);
            } catch (java.io.FileNotFoundException e) {
                if (android.util.Log.isLoggable("FileLoader", 3)) {
                    android.util.Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.mo9251a((java.lang.Exception) e);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<Data> getDataClass() {
            return this.f6948f.getDataClass();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
            Data data = this.f6949g;
            if (data != null) {
                try {
                    this.f6948f.mo12864a(data);
                } catch (java.io.IOException unused) {
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.kr$d */
    public interface C2242d<Data> {
        @DexIgnore
        /* renamed from: a */
        Data mo12862a(java.io.File file) throws java.io.FileNotFoundException;

        @DexIgnore
        /* renamed from: a */
        void mo12864a(Data data) throws java.io.IOException;

        @DexIgnore
        java.lang.Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$e")
    /* renamed from: com.fossil.blesdk.obfuscated.kr$e */
    public static class C2243e extends com.fossil.blesdk.obfuscated.C2237kr.C2238a<java.io.InputStream> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.kr$e$a")
        /* renamed from: com.fossil.blesdk.obfuscated.kr$e$a */
        public class C2244a implements com.fossil.blesdk.obfuscated.C2237kr.C2242d<java.io.InputStream> {
            @DexIgnore
            public java.lang.Class<java.io.InputStream> getDataClass() {
                return java.io.InputStream.class;
            }

            @DexIgnore
            /* renamed from: a */
            public java.io.InputStream m9745a(java.io.File file) throws java.io.FileNotFoundException {
                return new java.io.FileInputStream(file);
            }

            @DexIgnore
            /* renamed from: a */
            public void mo12864a(java.io.InputStream inputStream) throws java.io.IOException {
                inputStream.close();
            }
        }

        @DexIgnore
        public C2243e() {
            super(new com.fossil.blesdk.obfuscated.C2237kr.C2243e.C2244a());
        }
    }

    @DexIgnore
    public C2237kr(com.fossil.blesdk.obfuscated.C2237kr.C2242d<Data> dVar) {
        this.f6945a = dVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(java.io.File file) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(java.io.File file, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(file), new com.fossil.blesdk.obfuscated.C2237kr.C2241c(file, this.f6945a));
    }
}
