package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u42 implements Factory<AuthApiGuestService> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public u42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static u42 a(n42 n42) {
        return new u42(n42);
    }

    @DexIgnore
    public static AuthApiGuestService b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static AuthApiGuestService c(n42 n42) {
        AuthApiGuestService d = n42.d();
        n44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public AuthApiGuestService get() {
        return b(this.a);
    }
}
