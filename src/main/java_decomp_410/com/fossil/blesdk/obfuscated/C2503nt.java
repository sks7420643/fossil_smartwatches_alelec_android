package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nt */
public final class C2503nt extends com.fossil.blesdk.obfuscated.C2435mt<android.graphics.drawable.Drawable> {
    @DexIgnore
    public C2503nt(android.graphics.drawable.Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> m11435a(android.graphics.drawable.Drawable drawable) {
        if (drawable != null) {
            return new com.fossil.blesdk.obfuscated.C2503nt(drawable);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return java.lang.Math.max(1, this.f7580e.getIntrinsicWidth() * this.f7580e.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<android.graphics.drawable.Drawable> mo8889c() {
        return this.f7580e.getClass();
    }
}
