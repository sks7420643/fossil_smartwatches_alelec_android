package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.internal.clearcut.zzge$zzv$zzb;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uy0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uy0> CREATOR; // = new vy0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ String k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ int m;

    @DexIgnore
    public uy0(String str, int i2, int i3, String str2, String str3, String str4, boolean z, zzge$zzv$zzb zzge_zzv_zzb) {
        bk0.a(str);
        this.e = str;
        this.f = i2;
        this.g = i3;
        this.k = str2;
        this.h = str3;
        this.i = str4;
        this.j = !z;
        this.l = z;
        this.m = zzge_zzv_zzb.zzc();
    }

    @DexIgnore
    public uy0(String str, int i2, int i3, String str2, String str3, boolean z, String str4, boolean z2, int i4) {
        this.e = str;
        this.f = i2;
        this.g = i3;
        this.h = str2;
        this.i = str3;
        this.j = z;
        this.k = str4;
        this.l = z2;
        this.m = i4;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof uy0) {
            uy0 uy0 = (uy0) obj;
            return zj0.a(this.e, uy0.e) && this.f == uy0.f && this.g == uy0.g && zj0.a(this.k, uy0.k) && zj0.a(this.h, uy0.h) && zj0.a(this.i, uy0.i) && this.j == uy0.j && this.l == uy0.l && this.m == uy0.m;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.e, Integer.valueOf(this.f), Integer.valueOf(this.g), this.k, this.h, this.i, Boolean.valueOf(this.j), Boolean.valueOf(this.l), Integer.valueOf(this.m));
    }

    @DexIgnore
    public final String toString() {
        return "PlayLoggerContext[" + "package=" + this.e + ',' + "packageVersionCode=" + this.f + ',' + "logSource=" + this.g + ',' + "logSourceName=" + this.k + ',' + "uploadAccount=" + this.h + ',' + "loggingId=" + this.i + ',' + "logAndroidId=" + this.j + ',' + "isAnonymous=" + this.l + ',' + "qosTier=" + this.m + "]";
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e, false);
        kk0.a(parcel, 3, this.f);
        kk0.a(parcel, 4, this.g);
        kk0.a(parcel, 5, this.h, false);
        kk0.a(parcel, 6, this.i, false);
        kk0.a(parcel, 7, this.j);
        kk0.a(parcel, 8, this.k, false);
        kk0.a(parcel, 9, this.l);
        kk0.a(parcel, 10, this.m);
        kk0.a(parcel, a);
    }
}
