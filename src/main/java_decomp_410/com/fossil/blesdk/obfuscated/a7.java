package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.CancellationSignal;
import com.fossil.blesdk.obfuscated.o6;
import com.fossil.blesdk.obfuscated.u7;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a7 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements c<u7.f> {
        @DexIgnore
        public a(a7 a7Var) {
        }

        @DexIgnore
        public int a(u7.f fVar) {
            return fVar.d();
        }

        @DexIgnore
        public boolean b(u7.f fVar) {
            return fVar.e();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c<o6.c> {
        @DexIgnore
        public b(a7 a7Var) {
        }

        @DexIgnore
        public int a(o6.c cVar) {
            return cVar.e();
        }

        @DexIgnore
        public boolean b(o6.c cVar) {
            return cVar.f();
        }
    }

    @DexIgnore
    public interface c<T> {
        @DexIgnore
        int a(T t);

        @DexIgnore
        boolean b(T t);
    }

    @DexIgnore
    public static <T> T a(T[] tArr, int i, c<T> cVar) {
        int i2 = (i & 1) == 0 ? MFNetworkReturnCode.BAD_REQUEST : 700;
        boolean z = (i & 2) != 0;
        T t = null;
        int i3 = Integer.MAX_VALUE;
        for (T t2 : tArr) {
            int abs = (Math.abs(cVar.a(t2) - i2) * 2) + (cVar.b(t2) == z ? 0 : 1);
            if (t == null || i3 > abs) {
                t = t2;
                i3 = abs;
            }
        }
        return t;
    }

    @DexIgnore
    public u7.f a(u7.f[] fVarArr, int i) {
        return (u7.f) a(fVarArr, i, new a(this));
    }

    @DexIgnore
    public Typeface a(Context context, InputStream inputStream) {
        File a2 = b7.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!b7.a(a2, inputStream)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }

    @DexIgnore
    public Typeface a(Context context, CancellationSignal cancellationSignal, u7.f[] fVarArr, int i) {
        InputStream inputStream;
        InputStream inputStream2 = null;
        if (fVarArr.length < 1) {
            return null;
        }
        try {
            inputStream = context.getContentResolver().openInputStream(a(fVarArr, i).c());
            try {
                Typeface a2 = a(context, inputStream);
                b7.a((Closeable) inputStream);
                return a2;
            } catch (IOException unused) {
                b7.a((Closeable) inputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                inputStream2 = inputStream;
                b7.a((Closeable) inputStream2);
                throw th;
            }
        } catch (IOException unused2) {
            inputStream = null;
            b7.a((Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            b7.a((Closeable) inputStream2);
            throw th;
        }
    }

    @DexIgnore
    public final o6.c a(o6.b bVar, int i) {
        return (o6.c) a(bVar.a(), i, new b(this));
    }

    @DexIgnore
    public Typeface a(Context context, o6.b bVar, Resources resources, int i) {
        o6.c a2 = a(bVar, i);
        if (a2 == null) {
            return null;
        }
        return v6.a(context, resources, a2.b(), a2.a(), i);
    }

    @DexIgnore
    public Typeface a(Context context, Resources resources, int i, String str, int i2) {
        File a2 = b7.a(context);
        if (a2 == null) {
            return null;
        }
        try {
            if (!b7.a(a2, resources, i)) {
                return null;
            }
            Typeface createFromFile = Typeface.createFromFile(a2.getPath());
            a2.delete();
            return createFromFile;
        } catch (RuntimeException unused) {
            return null;
        } finally {
            a2.delete();
        }
    }
}
