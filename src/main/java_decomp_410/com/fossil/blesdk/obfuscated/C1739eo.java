package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eo */
public class C1739eo {

    @DexIgnore
    /* renamed from: a */
    public int[] f4872a; // = null;

    @DexIgnore
    /* renamed from: b */
    public int f4873b; // = 0;

    @DexIgnore
    /* renamed from: c */
    public int f4874c; // = 0;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1646do f4875d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C1646do> f4876e; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: f */
    public int f4877f;

    @DexIgnore
    /* renamed from: g */
    public int f4878g;

    @DexIgnore
    /* renamed from: h */
    public boolean f4879h;

    @DexIgnore
    /* renamed from: i */
    public int f4880i;

    @DexIgnore
    /* renamed from: j */
    public int f4881j;

    @DexIgnore
    /* renamed from: k */
    public int f4882k;

    @DexIgnore
    /* renamed from: l */
    public int f4883l;

    @DexIgnore
    /* renamed from: m */
    public int f4884m;

    @DexIgnore
    /* renamed from: a */
    public int mo10542a() {
        return this.f4878g;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo10543b() {
        return this.f4874c;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo10544c() {
        return this.f4873b;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo10545d() {
        return this.f4877f;
    }
}
