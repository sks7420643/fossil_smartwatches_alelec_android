package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wp1 extends lc1 implements vp1 {
    @DexIgnore
    public wp1() {
        super("com.google.android.gms.wearable.internal.IWearableListener");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                a((DataHolder) mc1.a(parcel, DataHolder.CREATOR));
                return true;
            case 2:
                a((xp1) mc1.a(parcel, xp1.CREATOR));
                return true;
            case 3:
                a((zp1) mc1.a(parcel, zp1.CREATOR));
                return true;
            case 4:
                b((zp1) mc1.a(parcel, zp1.CREATOR));
                return true;
            case 5:
                b((List<zp1>) parcel.createTypedArrayList(zp1.CREATOR));
                return true;
            case 6:
                a((eq1) mc1.a(parcel, eq1.CREATOR));
                return true;
            case 7:
                a((op1) mc1.a(parcel, op1.CREATOR));
                return true;
            case 8:
                a((kp1) mc1.a(parcel, kp1.CREATOR));
                return true;
            case 9:
                a((cq1) mc1.a(parcel, cq1.CREATOR));
                return true;
            default:
                return false;
        }
    }
}
