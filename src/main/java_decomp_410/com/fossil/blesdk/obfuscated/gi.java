package com.fossil.blesdk.obfuscated;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gi extends nh {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    public void a(ph phVar) {
        View view = phVar.b;
        Integer num = (Integer) phVar.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        phVar.a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        phVar.a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    public int b(ph phVar) {
        if (phVar == null) {
            return 8;
        }
        Integer num = (Integer) phVar.a.get("android:visibilityPropagation:visibility");
        if (num == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int c(ph phVar) {
        return a(phVar, 0);
    }

    @DexIgnore
    public int d(ph phVar) {
        return a(phVar, 1);
    }

    @DexIgnore
    public String[] a() {
        return a;
    }

    @DexIgnore
    public static int a(ph phVar, int i) {
        if (phVar == null) {
            return -1;
        }
        int[] iArr = (int[]) phVar.a.get("android:visibilityPropagation:center");
        if (iArr == null) {
            return -1;
        }
        return iArr[i];
    }
}
