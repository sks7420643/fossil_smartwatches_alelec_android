package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class id2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomizeWidget A;
    @DexIgnore
    public /* final */ CustomizeWidget B;
    @DexIgnore
    public /* final */ CustomizeWidget C;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ CardView r;
    @DexIgnore
    public /* final */ ImageButton s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ RecyclerViewPager u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ TextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public id2(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, CardView cardView, ImageButton imageButton, View view3, ImageView imageView, View view4, View view5, View view6, RecyclerViewPager recyclerViewPager, ImageView imageView2, TextView textView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view7, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = constraintLayout2;
        this.r = cardView;
        this.s = imageButton;
        this.t = imageView;
        this.u = recyclerViewPager;
        this.v = imageView2;
        this.w = textView;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = customizeWidget;
        this.B = customizeWidget2;
        this.C = customizeWidget3;
    }
}
