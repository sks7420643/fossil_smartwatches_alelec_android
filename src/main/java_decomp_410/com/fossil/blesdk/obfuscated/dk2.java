package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk2 extends rv implements Cloneable {
    @DexIgnore
    public dk2 O() {
        super.O();
        return this;
    }

    @DexIgnore
    public dk2 P() {
        return (dk2) super.P();
    }

    @DexIgnore
    public dk2 Q() {
        return (dk2) super.Q();
    }

    @DexIgnore
    public dk2 R() {
        return (dk2) super.R();
    }

    @DexIgnore
    public dk2 c() {
        return (dk2) super.c();
    }

    @DexIgnore
    public dk2 b(boolean z) {
        return (dk2) super.b(z);
    }

    @DexIgnore
    public dk2 clone() {
        return (dk2) super.clone();
    }

    @DexIgnore
    public dk2 b(int i) {
        return (dk2) super.b(i);
    }

    @DexIgnore
    public dk2 a(float f) {
        return (dk2) super.a(f);
    }

    @DexIgnore
    public dk2 a(pp ppVar) {
        return (dk2) super.a(ppVar);
    }

    @DexIgnore
    public dk2 a(Priority priority) {
        return (dk2) super.a(priority);
    }

    @DexIgnore
    public dk2 a(boolean z) {
        return (dk2) super.a(z);
    }

    @DexIgnore
    public dk2 a(int i, int i2) {
        return (dk2) super.a(i, i2);
    }

    @DexIgnore
    public dk2 a(jo joVar) {
        return (dk2) super.a(joVar);
    }

    @DexIgnore
    public <Y> dk2 a(ko<Y> koVar, Y y) {
        return (dk2) super.a(koVar, y);
    }

    @DexIgnore
    public dk2 a(Class<?> cls) {
        return (dk2) super.a(cls);
    }

    @DexIgnore
    public dk2 a(DownsampleStrategy downsampleStrategy) {
        return (dk2) super.a(downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public dk2 a(oo<Bitmap> r1) {
        return (dk2) super.a((oo<Bitmap>) r1);
    }

    @DexIgnore
    public dk2 a(lv<?> lvVar) {
        return (dk2) super.a(lvVar);
    }

    @DexIgnore
    public dk2 a() {
        return (dk2) super.a();
    }
}
