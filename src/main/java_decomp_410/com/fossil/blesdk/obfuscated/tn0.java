package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tn0 {
    @DexIgnore
    View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    @DexIgnore
    void a();

    @DexIgnore
    void a(Activity activity, Bundle bundle, Bundle bundle2);

    @DexIgnore
    void a(Bundle bundle);

    @DexIgnore
    void b();

    @DexIgnore
    void b(Bundle bundle);

    @DexIgnore
    void c();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    void onLowMemory();

    @DexIgnore
    void onPause();
}
