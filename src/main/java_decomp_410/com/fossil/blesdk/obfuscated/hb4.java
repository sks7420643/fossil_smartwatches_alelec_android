package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hb4 extends gb4 {
    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, Iterable<? extends T> iterable) {
        kd4.b(collection, "$this$addAll");
        kd4.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        for (Object add : iterable) {
            if (collection.add(add)) {
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static final <T> boolean b(Collection<? super T> collection, Iterable<? extends T> iterable) {
        kd4.b(collection, "$this$retainAll");
        kd4.b(iterable, MessengerShareContentUtility.ELEMENTS);
        return qd4.a((Object) collection).retainAll(db4.a(iterable, (Iterable<? extends T>) collection));
    }

    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, T[] tArr) {
        kd4.b(collection, "$this$addAll");
        kd4.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return collection.addAll(ya4.a(tArr));
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, xc4<? super T, Boolean> xc4) {
        kd4.b(iterable, "$this$retainAll");
        kd4.b(xc4, "predicate");
        return a(iterable, xc4, false);
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, xc4<? super T, Boolean> xc4, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (xc4.invoke(it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, xc4<? super T, Boolean> xc4) {
        kd4.b(list, "$this$removeAll");
        kd4.b(xc4, "predicate");
        return a(list, xc4, true);
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, xc4<? super T, Boolean> xc4, boolean z) {
        int i;
        if (list instanceof RandomAccess) {
            int a = cb4.a(list);
            if (a >= 0) {
                int i2 = 0;
                i = 0;
                while (true) {
                    T t = list.get(i2);
                    if (xc4.invoke(t).booleanValue() != z) {
                        if (i != i2) {
                            list.set(i, t);
                        }
                        i++;
                    }
                    if (i2 == a) {
                        break;
                    }
                    i2++;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int a2 = cb4.a(list);
            if (a2 < i) {
                return true;
            }
            while (true) {
                list.remove(a2);
                if (a2 == i) {
                    return true;
                }
                a2--;
            }
        } else if (list != null) {
            return a(qd4.b(list), xc4, z);
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }
}
