package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oz */
public class C2611oz extends com.fossil.blesdk.obfuscated.e54 implements com.fossil.blesdk.obfuscated.C1588cz {
    @DexIgnore
    public C2611oz(com.fossil.blesdk.obfuscated.v44 v44, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.z64 z64) {
        super(v44, str, str2, z64, p011io.fabric.sdk.android.services.network.HttpMethod.POST);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9713a(com.fossil.blesdk.obfuscated.C1520bz bzVar) {
        p011io.fabric.sdk.android.services.network.HttpRequest a = mo26836a();
        mo14567a(a, bzVar.f3946a);
        mo14566a(a, bzVar.f3947b);
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30060d("CrashlyticsCore", "Sending report to: " + mo26839b());
        int g2 = a.mo44621g();
        com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
        g3.mo30060d("CrashlyticsCore", "Result was: " + g2);
        return com.fossil.blesdk.obfuscated.w54.m29544a(g2) == 0;
    }

    @DexIgnore
    /* renamed from: a */
    public final p011io.fabric.sdk.android.services.network.HttpRequest mo14567a(p011io.fabric.sdk.android.services.network.HttpRequest httpRequest, java.lang.String str) {
        httpRequest.mo44611c("User-Agent", "Crashlytics Android SDK/" + this.f14377e.mo9330r());
        httpRequest.mo44611c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.mo44611c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f14377e.mo9330r());
        httpRequest.mo44611c("X-CRASHLYTICS-API-KEY", str);
        return httpRequest;
    }

    @DexIgnore
    /* renamed from: a */
    public final p011io.fabric.sdk.android.services.network.HttpRequest mo14566a(p011io.fabric.sdk.android.services.network.HttpRequest httpRequest, com.crashlytics.android.core.Report report) {
        httpRequest.mo44618e("report_id", report.mo4193b());
        for (java.io.File file : report.mo4195d()) {
            if (file.getName().equals("minidump")) {
                httpRequest.mo44600a("minidump_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("metadata")) {
                httpRequest.mo44600a("crash_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("binaryImages")) {
                httpRequest.mo44600a("binary_images_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals(com.misfit.frameworks.common.constants.Constants.SESSION)) {
                httpRequest.mo44600a("session_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("app")) {
                httpRequest.mo44600a("app_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("device")) {
                httpRequest.mo44600a("device_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("os")) {
                httpRequest.mo44600a("os_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("user")) {
                httpRequest.mo44600a("user_meta_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals(com.misfit.frameworks.buttonservice.log.FileLogWriter.LOG_FOLDER)) {
                httpRequest.mo44600a("logs_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            } else if (file.getName().equals("keys")) {
                httpRequest.mo44600a("keys_file", file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            }
        }
        return httpRequest;
    }
}
