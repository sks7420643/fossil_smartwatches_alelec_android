package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y11 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<y11> CREATOR; // = new z11();
    @DexIgnore
    public /* final */ zo0 e;

    @DexIgnore
    public y11(zo0 zo0) {
        this.e = zo0;
    }

    @DexIgnore
    public final zo0 H() {
        return this.e;
    }

    @DexIgnore
    public final String toString() {
        return String.format("ApplicationUnregistrationRequest{%s}", new Object[]{this.e});
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) this.e, i, false);
        kk0.a(parcel, a);
    }
}
