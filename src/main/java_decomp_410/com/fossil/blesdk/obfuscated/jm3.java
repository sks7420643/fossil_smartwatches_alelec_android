package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jm3 extends zr2 implements ml3, ws3.g {
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public ll3 j;
    @DexIgnore
    public tr3<eg2> k;
    @DexIgnore
    public s62 l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final jm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            jm3 jm3 = new jm3();
            jm3.setArguments(bundle);
            return jm3;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm3 e;

        @DexIgnore
        public b(jm3 jm3) {
            this.e = jm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm3 e;

        @DexIgnore
        public c(jm3 jm3) {
            this.e = jm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0().l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jm3 e;

        @DexIgnore
        public d(jm3 jm3) {
            this.e = jm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity, PortfolioApp.W.c().e());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final ll3 T0() {
        ll3 ll3 = this.j;
        if (ll3 != null) {
            return ll3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void X() {
        tr3<eg2> tr3 = this.k;
        if (tr3 != null) {
            eg2 a2 = tr3.a();
            DashBar dashBar = a2 != null ? a2.z : null;
            if (dashBar != null) {
                kd4.a((Object) dashBar, "mBinding.get()?.progressBar!!");
                dashBar.setVisibility(8);
                return;
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(boolean z) {
        if (isActive()) {
            tr3<eg2> tr3 = this.k;
            if (tr3 != null) {
                eg2 a2 = tr3.a();
                if (a2 != null) {
                    ll3 ll3 = this.j;
                    if (ll3 == null) {
                        kd4.d("mPresenter");
                        throw null;
                    } else if (ll3.j()) {
                        if (z) {
                            ConstraintLayout constraintLayout = a2.q;
                            kd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                            constraintLayout.setVisibility(8);
                            ConstraintLayout constraintLayout2 = a2.r;
                            kd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                            constraintLayout2.setVisibility(0);
                            FlexibleButton flexibleButton = a2.s;
                            kd4.a((Object) flexibleButton, "it.fbContinue");
                            flexibleButton.setVisibility(0);
                            FlexibleTextView flexibleTextView = a2.x;
                            kd4.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                            flexibleTextView.setVisibility(4);
                            ProgressBar progressBar = a2.A;
                            kd4.a((Object) progressBar, "it.progressUpdate");
                            progressBar.setProgress(1000);
                            FlexibleTextView flexibleTextView2 = a2.w;
                            kd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                            flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Updates_UpdateCompleted_Title__UpdateCompleted));
                            return;
                        }
                        ConstraintLayout constraintLayout3 = a2.q;
                        kd4.a((Object) constraintLayout3, "it.clUpdateFwFail");
                        constraintLayout3.setVisibility(0);
                        ConstraintLayout constraintLayout4 = a2.r;
                        kd4.a((Object) constraintLayout4, "it.clUpdatingFw");
                        constraintLayout4.setVisibility(8);
                    } else if (getActivity() == null) {
                    } else {
                        if (z) {
                            ll3 ll32 = this.j;
                            if (ll32 != null) {
                                ll32.h();
                                h();
                                return;
                            }
                            kd4.d("mPresenter");
                            throw null;
                        }
                        TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                        Context context = getContext();
                        if (context != null) {
                            kd4.a((Object) context, "context!!");
                            aVar.a(context, PortfolioApp.W.c().e());
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void f0() {
        if (isAdded()) {
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
            fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
            fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Text__PleaseCheckYourInternetConnectionAnd));
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionwithActions_CTA__TryAgain));
            fVar.a((int) R.id.tv_ok);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public void g(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i);
        tr3<eg2> tr3 = this.k;
        if (tr3 != null) {
            eg2 a2 = tr3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.A;
                if (progressBar != null) {
                    progressBar.setProgress(i);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void i(List<? extends Explore> list) {
        kd4.b(list, "data");
        tr3<eg2> tr3 = this.k;
        if (tr3 != null) {
            eg2 a2 = tr3.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                    FlexibleTextView flexibleTextView = a2.w;
                    kd4.a((Object) flexibleTextView, "it.ftvUpdate");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Title__ExploreYourWatch));
                } else {
                    FlexibleTextView flexibleTextView2 = a2.w;
                    kd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Title__ExploreYourWatch));
                }
            }
            s62 s62 = this.l;
            if (s62 != null) {
                s62.a(list);
            } else {
                kd4.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void n0() {
        if (isActive()) {
            tr3<eg2> tr3 = this.k;
            if (tr3 != null) {
                eg2 a2 = tr3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    kd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    kd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        eg2 eg2 = (eg2) qa.a(layoutInflater, R.layout.fragment_update_firmware, viewGroup, false, O0());
        this.k = new tr3<>(this, eg2);
        kd4.a((Object) eg2, "binding");
        return eg2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ll3 ll3 = this.j;
        if (ll3 != null) {
            ll3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ll3 ll3 = this.j;
        if (ll3 != null) {
            ll3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.m = arguments.getBoolean("IS_ONBOARDING_FLOW");
            ll3 ll3 = this.j;
            if (ll3 != null) {
                ll3.a(this.m);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        this.l = new s62(new ArrayList());
        tr3<eg2> tr3 = this.k;
        if (tr3 != null) {
            eg2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                kd4.a((Object) constraintLayout, "binding.clUpdateFwFail");
                constraintLayout.setVisibility(8);
                ConstraintLayout constraintLayout2 = a2.r;
                kd4.a((Object) constraintLayout2, "binding.clUpdatingFw");
                constraintLayout2.setVisibility(0);
                ProgressBar progressBar = a2.A;
                kd4.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                FlexibleButton flexibleButton = a2.s;
                kd4.a((Object) flexibleButton, "binding.fbContinue");
                flexibleButton.setVisibility(8);
                FlexibleTextView flexibleTextView = a2.x;
                kd4.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                a2.s.setOnClickListener(new b(this));
                RecyclerViewPager recyclerViewPager = a2.B;
                kd4.a((Object) recyclerViewPager, "binding.rvpTutorial");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.B;
                kd4.a((Object) recyclerViewPager2, "binding.rvpTutorial");
                s62 s62 = this.l;
                if (s62 != null) {
                    recyclerViewPager2.setAdapter(s62);
                    a2.y.a((RecyclerView) a2.B, 0);
                    a2.y.setOnPageChangeListener(new e());
                    a2.t.setOnClickListener(new c(this));
                    a2.v.setOnClickListener(new d(this));
                    return;
                }
                kd4.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void t() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.C;
            kd4.a((Object) activity, "it");
            ll3 ll3 = this.j;
            if (ll3 != null) {
                aVar.a(activity, ll3.j());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void v0() {
        if (isActive()) {
            tr3<eg2> tr3 = this.k;
            if (tr3 != null) {
                eg2 a2 = tr3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.q;
                    kd4.a((Object) constraintLayout, "it.clUpdateFwFail");
                    constraintLayout.setVisibility(8);
                    ConstraintLayout constraintLayout2 = a2.r;
                    kd4.a((Object) constraintLayout2, "it.clUpdatingFw");
                    constraintLayout2.setVisibility(0);
                    FlexibleButton flexibleButton = a2.s;
                    kd4.a((Object) flexibleButton, "it.fbContinue");
                    flexibleButton.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.x;
                    kd4.a((Object) flexibleTextView, "it.ftvUpdateWarning");
                    flexibleTextView.setVisibility(4);
                    ProgressBar progressBar = a2.A;
                    kd4.a((Object) progressBar, "it.progressUpdate");
                    progressBar.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    kd4.a((Object) flexibleTextView2, "it.ftvCountdownTime");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "it.ftvUpdate");
                    flexibleTextView3.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_AndroidQuickAccessPanel_FirmwareUpdateComplete_Text__FirmwareUpdateComplete));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ll3 ll3) {
        kd4.b(ll3, "presenter");
        this.j = ll3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -879828873) {
            if (hashCode == 927511079 && str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UpdateFirmwareFragment", "Update firmware fail isOnboardingFlow " + this.m);
                if (i == R.id.fb_try_again) {
                    ll3 ll3 = this.j;
                    if (ll3 != null) {
                        ll3.l();
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else if (i != R.id.ftv_contact_cs) {
                    if (i == R.id.iv_close) {
                        ll3 ll32 = this.j;
                        if (ll32 != null) {
                            ll32.h();
                            FragmentActivity activity = getActivity();
                            if (activity != null) {
                                activity.finish();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    }
                } else if (getActivity() != null) {
                    HelpActivity.a aVar = HelpActivity.C;
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        kd4.a((Object) activity2, "activity!!");
                        aVar.a(activity2);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            }
        } else if (str.equals("NETWORK_ERROR") && i == R.id.fb_ok) {
            ll3 ll33 = this.j;
            if (ll33 != null) {
                ll33.i();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void g() {
        tr3<eg2> tr3 = this.k;
        if (tr3 != null) {
            eg2 a2 = tr3.a();
            if (a2 != null) {
                DashBar dashBar = a2.z;
                if (dashBar != null) {
                    tr3<eg2> tr32 = this.k;
                    if (tr32 != null) {
                        eg2 a3 = tr32.a();
                        DashBar dashBar2 = a3 != null ? a3.z : null;
                        if (dashBar2 != null) {
                            kd4.a((Object) dashBar2, "mBinding.get()?.progressBar!!");
                            dashBar2.setVisibility(0);
                            gs3.a aVar = gs3.a;
                            kd4.a((Object) dashBar, "this");
                            aVar.f(dashBar, this.m, 500);
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
