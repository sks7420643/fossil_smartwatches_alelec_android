package com.fossil.blesdk.obfuscated;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.facebook.GraphRequest;
import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public class dn implements gn {
    @DexIgnore
    public /* final */ HttpClient a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends HttpEntityEnclosingRequestBase {
        @DexIgnore
        public a(String str) {
            setURI(URI.create(str));
        }

        @DexIgnore
        public String getMethod() {
            return "PATCH";
        }
    }

    @DexIgnore
    public dn(HttpClient httpClient) {
        this.a = httpClient;
    }

    @DexIgnore
    public static void a(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    @DexIgnore
    public static HttpUriRequest b(Request<?> request, Map<String, String> map) throws AuthFailureError {
        switch (request.getMethod()) {
            case -1:
                byte[] postBody = request.getPostBody();
                if (postBody == null) {
                    return new HttpGet(request.getUrl());
                }
                HttpPost httpPost = new HttpPost(request.getUrl());
                httpPost.addHeader(GraphRequest.CONTENT_TYPE_HEADER, request.getPostBodyContentType());
                httpPost.setEntity(new ByteArrayEntity(postBody));
                return httpPost;
            case 0:
                return new HttpGet(request.getUrl());
            case 1:
                HttpPost httpPost2 = new HttpPost(request.getUrl());
                httpPost2.addHeader(GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPost2, request);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(request.getUrl());
                httpPut.addHeader(GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPut, request);
                return httpPut;
            case 3:
                return new HttpDelete(request.getUrl());
            case 4:
                return new HttpHead(request.getUrl());
            case 5:
                return new HttpOptions(request.getUrl());
            case 6:
                return new HttpTrace(request.getUrl());
            case 7:
                a aVar = new a(request.getUrl());
                aVar.addHeader(GraphRequest.CONTENT_TYPE_HEADER, request.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) aVar, request);
                return aVar;
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    @DexIgnore
    public void a(HttpUriRequest httpUriRequest) throws IOException {
    }

    @DexIgnore
    public HttpResponse a(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError {
        HttpUriRequest b = b(request, map);
        a(b, map);
        a(b, request.getHeaders());
        a(b);
        HttpParams params = b.getParams();
        int timeoutMs = request.getTimeoutMs();
        HttpConnectionParams.setConnectionTimeout(params, VideoUploader.RETRY_DELAY_UNIT_MS);
        HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.a.execute(b);
    }

    @DexIgnore
    public static void a(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, Request<?> request) throws AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(body));
        }
    }
}
