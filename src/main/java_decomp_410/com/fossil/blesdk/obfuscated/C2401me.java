package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.me */
public class C2401me extends androidx.recyclerview.widget.RecyclerView.C0235l implements androidx.recyclerview.widget.RecyclerView.C0243p {

    @DexIgnore
    /* renamed from: D */
    public static /* final */ int[] f7448D; // = {16842919};

    @DexIgnore
    /* renamed from: E */
    public static /* final */ int[] f7449E; // = new int[0];

    @DexIgnore
    /* renamed from: A */
    public int f7450A; // = 0;

    @DexIgnore
    /* renamed from: B */
    public /* final */ java.lang.Runnable f7451B; // = new com.fossil.blesdk.obfuscated.C2401me.C2402a();

    @DexIgnore
    /* renamed from: C */
    public /* final */ androidx.recyclerview.widget.RecyclerView.C0244q f7452C; // = new com.fossil.blesdk.obfuscated.C2401me.C2403b();

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f7453a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f7454b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.graphics.drawable.StateListDrawable f7455c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.graphics.drawable.Drawable f7456d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f7457e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f7458f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.graphics.drawable.StateListDrawable f7459g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.graphics.drawable.Drawable f7460h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f7461i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ int f7462j;

    @DexIgnore
    /* renamed from: k */
    public int f7463k;

    @DexIgnore
    /* renamed from: l */
    public int f7464l;

    @DexIgnore
    /* renamed from: m */
    public float f7465m;

    @DexIgnore
    /* renamed from: n */
    public int f7466n;

    @DexIgnore
    /* renamed from: o */
    public int f7467o;

    @DexIgnore
    /* renamed from: p */
    public float f7468p;

    @DexIgnore
    /* renamed from: q */
    public int f7469q; // = 0;

    @DexIgnore
    /* renamed from: r */
    public int f7470r; // = 0;

    @DexIgnore
    /* renamed from: s */
    public androidx.recyclerview.widget.RecyclerView f7471s;

    @DexIgnore
    /* renamed from: t */
    public boolean f7472t; // = false;

    @DexIgnore
    /* renamed from: u */
    public boolean f7473u; // = false;

    @DexIgnore
    /* renamed from: v */
    public int f7474v; // = 0;

    @DexIgnore
    /* renamed from: w */
    public int f7475w; // = 0;

    @DexIgnore
    /* renamed from: x */
    public /* final */ int[] f7476x; // = new int[2];

    @DexIgnore
    /* renamed from: y */
    public /* final */ int[] f7477y; // = new int[2];

    @DexIgnore
    /* renamed from: z */
    public /* final */ android.animation.ValueAnimator f7478z; // = android.animation.ValueAnimator.ofFloat(new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.me$a")
    /* renamed from: com.fossil.blesdk.obfuscated.me$a */
    public class C2402a implements java.lang.Runnable {
        @DexIgnore
        public C2402a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2401me.this.mo13581a(500);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.me$b")
    /* renamed from: com.fossil.blesdk.obfuscated.me$b */
    public class C2403b extends androidx.recyclerview.widget.RecyclerView.C0244q {
        @DexIgnore
        public C2403b() {
        }

        @DexIgnore
        public void onScrolled(androidx.recyclerview.widget.RecyclerView recyclerView, int i, int i2) {
            com.fossil.blesdk.obfuscated.C2401me.this.mo13582a(recyclerView.computeHorizontalScrollOffset(), recyclerView.computeVerticalScrollOffset());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.me$c")
    /* renamed from: com.fossil.blesdk.obfuscated.me$c */
    public class C2404c extends android.animation.AnimatorListenerAdapter {

        @DexIgnore
        /* renamed from: a */
        public boolean f7481a; // = false;

        @DexIgnore
        public C2404c() {
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            this.f7481a = true;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            if (this.f7481a) {
                this.f7481a = false;
            } else if (((java.lang.Float) com.fossil.blesdk.obfuscated.C2401me.this.f7478z.getAnimatedValue()).floatValue() == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                com.fossil.blesdk.obfuscated.C2401me meVar = com.fossil.blesdk.obfuscated.C2401me.this;
                meVar.f7450A = 0;
                meVar.mo13591c(0);
            } else {
                com.fossil.blesdk.obfuscated.C2401me meVar2 = com.fossil.blesdk.obfuscated.C2401me.this;
                meVar2.f7450A = 2;
                meVar2.mo13595f();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.me$d")
    /* renamed from: com.fossil.blesdk.obfuscated.me$d */
    public class C2405d implements android.animation.ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public C2405d() {
        }

        @DexIgnore
        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            int floatValue = (int) (((java.lang.Float) valueAnimator.getAnimatedValue()).floatValue() * 255.0f);
            com.fossil.blesdk.obfuscated.C2401me.this.f7455c.setAlpha(floatValue);
            com.fossil.blesdk.obfuscated.C2401me.this.f7456d.setAlpha(floatValue);
            com.fossil.blesdk.obfuscated.C2401me.this.mo13595f();
        }
    }

    @DexIgnore
    public C2401me(androidx.recyclerview.widget.RecyclerView recyclerView, android.graphics.drawable.StateListDrawable stateListDrawable, android.graphics.drawable.Drawable drawable, android.graphics.drawable.StateListDrawable stateListDrawable2, android.graphics.drawable.Drawable drawable2, int i, int i2, int i3) {
        this.f7455c = stateListDrawable;
        this.f7456d = drawable;
        this.f7459g = stateListDrawable2;
        this.f7460h = drawable2;
        this.f7457e = java.lang.Math.max(i, stateListDrawable.getIntrinsicWidth());
        this.f7458f = java.lang.Math.max(i, drawable.getIntrinsicWidth());
        this.f7461i = java.lang.Math.max(i, stateListDrawable2.getIntrinsicWidth());
        this.f7462j = java.lang.Math.max(i, drawable2.getIntrinsicWidth());
        this.f7453a = i2;
        this.f7454b = i3;
        this.f7455c.setAlpha(255);
        this.f7456d.setAlpha(255);
        this.f7478z.addListener(new com.fossil.blesdk.obfuscated.C2401me.C2404c());
        this.f7478z.addUpdateListener(new com.fossil.blesdk.obfuscated.C2401me.C2405d());
        mo13584a(recyclerView);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13584a(androidx.recyclerview.widget.RecyclerView recyclerView) {
        androidx.recyclerview.widget.RecyclerView recyclerView2 = this.f7471s;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                mo13586b();
            }
            this.f7471s = recyclerView;
            if (this.f7471s != null) {
                mo13596g();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo2988a(boolean z) {
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13586b() {
        this.f7471s.mo2543b((androidx.recyclerview.widget.RecyclerView.C0235l) this);
        this.f7471s.mo2545b((androidx.recyclerview.widget.RecyclerView.C0243p) this);
        this.f7471s.mo2546b(this.f7452C);
        mo13579a();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo13591c(int i) {
        if (i == 2 && this.f7474v != 2) {
            this.f7455c.setState(f7448D);
            mo13579a();
        }
        if (i == 0) {
            mo13595f();
        } else {
            mo13597h();
        }
        if (this.f7474v == 2 && i != 2) {
            this.f7455c.setState(f7449E);
            mo13588b(1200);
        } else if (i == 1) {
            mo13588b((int) com.facebook.internal.FacebookWebFallbackDialog.OS_BACK_BUTTON_RESPONSE_TIMEOUT_MILLISECONDS);
        }
        this.f7474v = i;
    }

    @DexIgnore
    /* renamed from: d */
    public final int[] mo13593d() {
        int[] iArr = this.f7476x;
        int i = this.f7454b;
        iArr[0] = i;
        iArr[1] = this.f7470r - i;
        return iArr;
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo13594e() {
        return com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f7471s) == 1;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo13595f() {
        this.f7471s.invalidate();
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo13596g() {
        this.f7471s.mo2522a((androidx.recyclerview.widget.RecyclerView.C0235l) this);
        this.f7471s.mo2525a((androidx.recyclerview.widget.RecyclerView.C0243p) this);
        this.f7471s.mo2526a(this.f7452C);
    }

    @DexIgnore
    /* renamed from: h */
    public void mo13597h() {
        int i = this.f7450A;
        if (i != 0) {
            if (i == 3) {
                this.f7478z.cancel();
            } else {
                return;
            }
        }
        this.f7450A = 1;
        android.animation.ValueAnimator valueAnimator = this.f7478z;
        valueAnimator.setFloatValues(new float[]{((java.lang.Float) valueAnimator.getAnimatedValue()).floatValue(), 1.0f});
        this.f7478z.setDuration(500);
        this.f7478z.setStartDelay(0);
        this.f7478z.start();
    }

    @DexIgnore
    public void onDrawOver(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.State state) {
        if (this.f7469q != this.f7471s.getWidth() || this.f7470r != this.f7471s.getHeight()) {
            this.f7469q = this.f7471s.getWidth();
            this.f7470r = this.f7471s.getHeight();
            mo13591c(0);
        } else if (this.f7450A != 0) {
            if (this.f7472t) {
                mo13589b(canvas);
            }
            if (this.f7473u) {
                mo13583a(canvas);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13588b(int i) {
        mo13579a();
        this.f7471s.postDelayed(this.f7451B, (long) i);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13581a(int i) {
        int i2 = this.f7450A;
        if (i2 == 1) {
            this.f7478z.cancel();
        } else if (i2 != 2) {
            return;
        }
        this.f7450A = 3;
        android.animation.ValueAnimator valueAnimator = this.f7478z;
        valueAnimator.setFloatValues(new float[]{((java.lang.Float) valueAnimator.getAnimatedValue()).floatValue(), 0.0f});
        this.f7478z.setDuration((long) i);
        this.f7478z.start();
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13589b(android.graphics.Canvas canvas) {
        int i = this.f7469q;
        int i2 = this.f7457e;
        int i3 = i - i2;
        int i4 = this.f7464l;
        int i5 = this.f7463k;
        int i6 = i4 - (i5 / 2);
        this.f7455c.setBounds(0, 0, i2, i5);
        this.f7456d.setBounds(0, 0, this.f7458f, this.f7470r);
        if (mo13594e()) {
            this.f7456d.draw(canvas);
            canvas.translate((float) this.f7457e, (float) i6);
            canvas.scale(-1.0f, 1.0f);
            this.f7455c.draw(canvas);
            canvas.scale(1.0f, 1.0f);
            canvas.translate((float) (-this.f7457e), (float) (-i6));
            return;
        }
        canvas.translate((float) i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f7456d.draw(canvas);
        canvas.translate(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i6);
        this.f7455c.draw(canvas);
        canvas.translate((float) (-i3), (float) (-i6));
    }

    @DexIgnore
    /* renamed from: c */
    public final int[] mo13592c() {
        int[] iArr = this.f7477y;
        int i = this.f7454b;
        iArr[0] = i;
        iArr[1] = this.f7469q - i;
        return iArr;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13579a() {
        this.f7471s.removeCallbacks(this.f7451B);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13583a(android.graphics.Canvas canvas) {
        int i = this.f7470r;
        int i2 = this.f7461i;
        int i3 = i - i2;
        int i4 = this.f7467o;
        int i5 = this.f7466n;
        int i6 = i4 - (i5 / 2);
        this.f7459g.setBounds(0, 0, i5, i2);
        this.f7460h.setBounds(0, 0, this.f7469q, this.f7462j);
        canvas.translate(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i3);
        this.f7460h.draw(canvas);
        canvas.translate((float) i6, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f7459g.draw(canvas);
        canvas.translate((float) (-i6), (float) (-i3));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13582a(int i, int i2) {
        int computeVerticalScrollRange = this.f7471s.computeVerticalScrollRange();
        int i3 = this.f7470r;
        this.f7472t = computeVerticalScrollRange - i3 > 0 && i3 >= this.f7453a;
        int computeHorizontalScrollRange = this.f7471s.computeHorizontalScrollRange();
        int i4 = this.f7469q;
        this.f7473u = computeHorizontalScrollRange - i4 > 0 && i4 >= this.f7453a;
        if (this.f7472t || this.f7473u) {
            if (this.f7472t) {
                float f = (float) i3;
                this.f7464l = (int) ((f * (((float) i2) + (f / 2.0f))) / ((float) computeVerticalScrollRange));
                this.f7463k = java.lang.Math.min(i3, (i3 * i3) / computeVerticalScrollRange);
            }
            if (this.f7473u) {
                float f2 = (float) i4;
                this.f7467o = (int) ((f2 * (((float) i) + (f2 / 2.0f))) / ((float) computeHorizontalScrollRange));
                this.f7466n = java.lang.Math.min(i4, (i4 * i4) / computeHorizontalScrollRange);
            }
            int i5 = this.f7474v;
            if (i5 == 0 || i5 == 1) {
                mo13591c(1);
            }
        } else if (this.f7474v != 0) {
            mo13591c(0);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo2989b(androidx.recyclerview.widget.RecyclerView recyclerView, android.view.MotionEvent motionEvent) {
        int i = this.f7474v;
        if (i == 1) {
            boolean b = mo13590b(motionEvent.getX(), motionEvent.getY());
            boolean a = mo13585a(motionEvent.getX(), motionEvent.getY());
            if (motionEvent.getAction() != 0) {
                return false;
            }
            if (!b && !a) {
                return false;
            }
            if (a) {
                this.f7475w = 1;
                this.f7468p = (float) ((int) motionEvent.getX());
            } else if (b) {
                this.f7475w = 2;
                this.f7465m = (float) ((int) motionEvent.getY());
            }
            mo13591c(2);
        } else if (i == 2) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13587b(float f) {
        int[] d = mo13593d();
        float max = java.lang.Math.max((float) d[0], java.lang.Math.min((float) d[1], f));
        if (java.lang.Math.abs(((float) this.f7464l) - max) >= 2.0f) {
            int a = mo13578a(this.f7465m, max, d, this.f7471s.computeVerticalScrollRange(), this.f7471s.computeVerticalScrollOffset(), this.f7470r);
            if (a != 0) {
                this.f7471s.scrollBy(0, a);
            }
            this.f7465m = max;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo2987a(androidx.recyclerview.widget.RecyclerView recyclerView, android.view.MotionEvent motionEvent) {
        if (this.f7474v != 0) {
            if (motionEvent.getAction() == 0) {
                boolean b = mo13590b(motionEvent.getX(), motionEvent.getY());
                boolean a = mo13585a(motionEvent.getX(), motionEvent.getY());
                if (b || a) {
                    if (a) {
                        this.f7475w = 1;
                        this.f7468p = (float) ((int) motionEvent.getX());
                    } else if (b) {
                        this.f7475w = 2;
                        this.f7465m = (float) ((int) motionEvent.getY());
                    }
                    mo13591c(2);
                }
            } else if (motionEvent.getAction() == 1 && this.f7474v == 2) {
                this.f7465m = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                this.f7468p = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                mo13591c(1);
                this.f7475w = 0;
            } else if (motionEvent.getAction() == 2 && this.f7474v == 2) {
                mo13597h();
                if (this.f7475w == 1) {
                    mo13580a(motionEvent.getX());
                }
                if (this.f7475w == 2) {
                    mo13587b(motionEvent.getY());
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo13590b(float f, float f2) {
        if (!mo13594e() ? f >= ((float) (this.f7469q - this.f7457e)) : f <= ((float) (this.f7457e / 2))) {
            int i = this.f7464l;
            int i2 = this.f7463k;
            return f2 >= ((float) (i - (i2 / 2))) && f2 <= ((float) (i + (i2 / 2)));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13580a(float f) {
        int[] c = mo13592c();
        float max = java.lang.Math.max((float) c[0], java.lang.Math.min((float) c[1], f));
        if (java.lang.Math.abs(((float) this.f7467o) - max) >= 2.0f) {
            int a = mo13578a(this.f7468p, max, c, this.f7471s.computeHorizontalScrollRange(), this.f7471s.computeHorizontalScrollOffset(), this.f7469q);
            if (a != 0) {
                this.f7471s.scrollBy(a, 0);
            }
            this.f7468p = max;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo13578a(float f, float f2, int[] iArr, int i, int i2, int i3) {
        int i4 = iArr[1] - iArr[0];
        if (i4 == 0) {
            return 0;
        }
        int i5 = i - i3;
        int i6 = (int) (((f2 - f) / ((float) i4)) * ((float) i5));
        int i7 = i2 + i6;
        if (i7 >= i5 || i7 < 0) {
            return 0;
        }
        return i6;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13585a(float f, float f2) {
        if (f2 >= ((float) (this.f7470r - this.f7461i))) {
            int i = this.f7467o;
            int i2 = this.f7466n;
            return f >= ((float) (i - (i2 / 2))) && f <= ((float) (i + (i2 / 2)));
        }
    }
}
