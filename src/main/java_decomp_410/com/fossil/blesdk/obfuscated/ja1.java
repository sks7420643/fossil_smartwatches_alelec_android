package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ja1 {
    @DexIgnore
    public static /* final */ ja1 c; // = new ja1();
    @DexIgnore
    public /* final */ oa1 a; // = new n91();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, na1<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static ja1 a() {
        return c;
    }

    @DexIgnore
    public final <T> na1<T> a(Class<T> cls) {
        v81.a(cls, "messageType");
        na1<T> na1 = (na1) this.b.get(cls);
        if (na1 != null) {
            return na1;
        }
        na1<T> a2 = this.a.a(cls);
        v81.a(cls, "messageType");
        v81.a(a2, "schema");
        na1<T> putIfAbsent = this.b.putIfAbsent(cls, a2);
        return putIfAbsent != null ? putIfAbsent : a2;
    }

    @DexIgnore
    public final <T> na1<T> a(T t) {
        return a(t.getClass());
    }
}
