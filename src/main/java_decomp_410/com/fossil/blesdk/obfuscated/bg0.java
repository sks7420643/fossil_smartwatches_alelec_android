package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bg0 implements ge0.b, ge0.c {
    @DexIgnore
    public /* final */ /* synthetic */ sf0 e;

    @DexIgnore
    public bg0(sf0 sf0) {
        this.e = sf0;
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        this.e.b.lock();
        try {
            if (this.e.a(ud0)) {
                this.e.g();
                this.e.e();
            } else {
                this.e.b(ud0);
            }
        } finally {
            this.e.b.unlock();
        }
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        if (this.e.r.k()) {
            this.e.b.lock();
            try {
                if (this.e.k != null) {
                    this.e.k.a(new zf0(this.e));
                    this.e.b.unlock();
                }
            } finally {
                this.e.b.unlock();
            }
        } else {
            this.e.k.a(new zf0(this.e));
        }
    }

    @DexIgnore
    public final void f(int i) {
    }

    @DexIgnore
    public /* synthetic */ bg0(sf0 sf0, tf0 tf0) {
        this(sf0);
    }
}
