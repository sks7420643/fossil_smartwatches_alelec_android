package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.model.ShineDevice;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface gn3 extends bs2<dn3> {
    @DexIgnore
    void A();

    @DexIgnore
    void O();

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i, String str, String str2);

    @DexIgnore
    void a(String str, boolean z);

    @DexIgnore
    void a(String str, boolean z, boolean z2);

    @DexIgnore
    void b();

    @DexIgnore
    void b(String str, boolean z);

    @DexIgnore
    void c(int i, String str);

    @DexIgnore
    void h();

    @DexIgnore
    void h(List<Pair<ShineDevice, String>> list);

    @DexIgnore
    void k(List<Pair<ShineDevice, String>> list);

    @DexIgnore
    void l(String str);

    @DexIgnore
    void n();

    @DexIgnore
    void s(String str);

    @DexIgnore
    void t();

    @DexIgnore
    void y();
}
