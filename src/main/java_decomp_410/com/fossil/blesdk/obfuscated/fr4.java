package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dr4;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import retrofit2.Call;
import retrofit2.HttpException;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fr4 extends dr4.a {
    @DexIgnore
    public static /* final */ dr4.a a; // = new fr4();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R> implements dr4<R, CompletableFuture<R>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fr4$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.fr4$a$a  reason: collision with other inner class name */
        public class C0086a extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call e;

            @DexIgnore
            public C0086a(a aVar, Call call) {
                this.e = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.e.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements er4<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture e;

            @DexIgnore
            public b(a aVar, CompletableFuture completableFuture) {
                this.e = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.e.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, qr4<R> qr4) {
                if (qr4.d()) {
                    this.e.complete(qr4.a());
                } else {
                    this.e.completeExceptionally(new HttpException(qr4));
                }
            }
        }

        @DexIgnore
        public a(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<R> a(Call<R> call) {
            C0086a aVar = new C0086a(this, call);
            call.a(new b(this, aVar));
            return aVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<R> implements dr4<R, CompletableFuture<qr4<R>>> {
        @DexIgnore
        public /* final */ Type a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends CompletableFuture<qr4<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call e;

            @DexIgnore
            public a(b bVar, Call call) {
                this.e = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.e.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fr4$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.fr4$b$b  reason: collision with other inner class name */
        public class C0087b implements er4<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture e;

            @DexIgnore
            public C0087b(b bVar, CompletableFuture completableFuture) {
                this.e = completableFuture;
            }

            @DexIgnore
            public void onFailure(Call<R> call, Throwable th) {
                this.e.completeExceptionally(th);
            }

            @DexIgnore
            public void onResponse(Call<R> call, qr4<R> qr4) {
                this.e.complete(qr4);
            }
        }

        @DexIgnore
        public b(Type type) {
            this.a = type;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public CompletableFuture<qr4<R>> a(Call<R> call) {
            a aVar = new a(this, call);
            call.a(new C0087b(this, aVar));
            return aVar;
        }
    }

    @DexIgnore
    public dr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (dr4.a.a(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type a2 = dr4.a.a(0, (ParameterizedType) type);
            if (dr4.a.a(a2) != qr4.class) {
                return new a(a2);
            }
            if (a2 instanceof ParameterizedType) {
                return new b(dr4.a.a(0, (ParameterizedType) a2));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
