package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ou */
public class C2603ou implements com.fossil.blesdk.obfuscated.C2436mu {
    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2344lu mo13714a(android.content.Context context, com.fossil.blesdk.obfuscated.C2344lu.C2345a aVar) {
        boolean z = com.fossil.blesdk.obfuscated.C2185k6.m9349a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (android.util.Log.isLoggable("ConnectivityMonitor", 3)) {
            android.util.Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
        }
        if (z) {
            return new com.fossil.blesdk.obfuscated.C2504nu(context, aVar);
        }
        return new com.fossil.blesdk.obfuscated.C2923su();
    }
}
