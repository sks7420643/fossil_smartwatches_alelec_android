package com.fossil.blesdk.obfuscated;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.text.TextUtils;
import androidx.work.WorkInfo;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vj implements pj, hk, mj {
    @DexIgnore
    public static /* final */ String l; // = dj.a("GreedyScheduler");
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ tj f;
    @DexIgnore
    public /* final */ ik g;
    @DexIgnore
    public List<hl> h; // = new ArrayList();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Object j;
    @DexIgnore
    public Boolean k;

    @DexIgnore
    public vj(Context context, zl zlVar, tj tjVar) {
        this.e = context;
        this.f = tjVar;
        this.g = new ik(context, zlVar, this);
        this.j = new Object();
    }

    @DexIgnore
    public void a(hl... hlVarArr) {
        if (this.k == null) {
            this.k = Boolean.valueOf(TextUtils.equals(this.e.getPackageName(), a()));
        }
        if (!this.k.booleanValue()) {
            dj.a().c(l, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (hl hlVar : hlVarArr) {
            if (hlVar.b == WorkInfo.State.ENQUEUED && !hlVar.d() && hlVar.g == 0 && !hlVar.c()) {
                if (!hlVar.b()) {
                    dj.a().a(l, String.format("Starting work for %s", new Object[]{hlVar.a}), new Throwable[0]);
                    this.f.a(hlVar.a);
                } else if (Build.VERSION.SDK_INT >= 23 && hlVar.j.h()) {
                    dj.a().a(l, String.format("Ignoring WorkSpec %s, Requires device idle.", new Object[]{hlVar}), new Throwable[0]);
                } else if (Build.VERSION.SDK_INT < 24 || !hlVar.j.e()) {
                    arrayList.add(hlVar);
                    arrayList2.add(hlVar.a);
                } else {
                    dj.a().a(l, String.format("Ignoring WorkSpec %s, Requires ContentUri triggers.", new Object[]{hlVar}), new Throwable[0]);
                }
            }
        }
        synchronized (this.j) {
            if (!arrayList.isEmpty()) {
                dj.a().a(l, String.format("Starting tracking for [%s]", new Object[]{TextUtils.join(",", arrayList2)}), new Throwable[0]);
                this.h.addAll(arrayList);
                this.g.c(this.h);
            }
        }
    }

    @DexIgnore
    public void b(List<String> list) {
        for (String next : list) {
            dj.a().a(l, String.format("Constraints met: Scheduling work ID %s", new Object[]{next}), new Throwable[0]);
            this.f.a(next);
        }
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (this.j) {
            int size = this.h.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    break;
                } else if (this.h.get(i2).a.equals(str)) {
                    dj.a().a(l, String.format("Stopping tracking for %s", new Object[]{str}), new Throwable[0]);
                    this.h.remove(i2);
                    this.g.c(this.h);
                    break;
                } else {
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public final void b() {
        if (!this.i) {
            this.f.e().a((mj) this);
            this.i = true;
        }
    }

    @DexIgnore
    public void a(String str) {
        if (this.k == null) {
            this.k = Boolean.valueOf(TextUtils.equals(this.e.getPackageName(), a()));
        }
        if (!this.k.booleanValue()) {
            dj.a().c(l, "Ignoring schedule request in non-main process", new Throwable[0]);
            return;
        }
        b();
        dj.a().a(l, String.format("Cancelling work ID %s", new Object[]{str}), new Throwable[0]);
        this.f.b(str);
    }

    @DexIgnore
    public void a(List<String> list) {
        for (String next : list) {
            dj.a().a(l, String.format("Constraints not met: Cancelling work ID %s", new Object[]{next}), new Throwable[0]);
            this.f.b(next);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        b(str);
    }

    @DexIgnore
    public final String a() {
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) this.e.getSystemService(Constants.ACTIVITY);
        if (activityManager == null) {
            return null;
        }
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses == null || runningAppProcesses.isEmpty()) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid == myPid) {
                return next.processName;
            }
        }
        return null;
    }
}
