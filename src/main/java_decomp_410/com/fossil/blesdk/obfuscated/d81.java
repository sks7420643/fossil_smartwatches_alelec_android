package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;
import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d81 implements sb1 {
    @DexIgnore
    public /* final */ zztv a;

    @DexIgnore
    public d81(zztv zztv) {
        v81.a(zztv, "output");
        this.a = zztv;
        this.a.a = this;
    }

    @DexIgnore
    public static d81 a(zztv zztv) {
        d81 d81 = zztv.a;
        if (d81 != null) {
            return d81;
        }
        return new d81(zztv);
    }

    @DexIgnore
    public final void b(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void c(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void zza(int i, float f) throws IOException {
        this.a.a(i, f);
    }

    @DexIgnore
    public final void zzb(int i, long j) throws IOException {
        this.a.b(i, j);
    }

    @DexIgnore
    public final void zzc(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void zzd(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void zze(int i, int i2) throws IOException {
        this.a.c(i, i2);
    }

    @DexIgnore
    public final void zzf(int i, int i2) throws IOException {
        this.a.d(i, i2);
    }

    @DexIgnore
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.b(list.get(i4).doubleValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.k(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.b(list.get(i4).booleanValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.g(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.j(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.h(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.h(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.d(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.f(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zza(int i, double d) throws IOException {
        this.a.a(i, d);
    }

    @DexIgnore
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.i(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.d(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.e(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.g(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.b(list.get(i4).floatValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    public final int a() {
        return t81.e.k;
    }

    @DexIgnore
    public final void b(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void zza(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void a(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof zzte) {
            this.a.b(i, (zzte) obj);
        } else {
            this.a.b(i, (w91) obj);
        }
    }

    @DexIgnore
    public final void b(int i, Object obj, na1 na1) throws IOException {
        zztv zztv = this.a;
        zztv.a(i, 3);
        na1.a((w91) obj, (sb1) zztv.a);
        zztv.a(i, 4);
    }

    @DexIgnore
    public final void a(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += zztv.f(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) throws IOException {
        this.a.a(i, z);
    }

    @DexIgnore
    public final void a(int i, String str) throws IOException {
        this.a.a(i, str);
    }

    @DexIgnore
    public final void b(int i) throws IOException {
        this.a.a(i, 3);
    }

    @DexIgnore
    public final void a(int i, zzte zzte) throws IOException {
        this.a.a(i, zzte);
    }

    @DexIgnore
    public final void b(int i, List<?> list, na1 na1) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, (Object) list.get(i2), na1);
        }
    }

    @DexIgnore
    public final void zzb(int i, List<zzte> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.a(i, list.get(i2));
        }
    }

    @DexIgnore
    public final void a(int i, Object obj, na1 na1) throws IOException {
        this.a.a(i, (w91) obj, na1);
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        this.a.a(i, 4);
    }

    @DexIgnore
    public final void a(int i, List<?> list, na1 na1) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, (Object) list.get(i2), na1);
        }
    }

    @DexIgnore
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof h91) {
            h91 h91 = (h91) list;
            while (i2 < list.size()) {
                Object d = h91.d(i2);
                if (d instanceof String) {
                    this.a.a(i, (String) d);
                } else {
                    this.a.a(i, (zzte) d);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2));
            i2++;
        }
    }
}
