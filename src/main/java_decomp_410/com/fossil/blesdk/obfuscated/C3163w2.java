package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w2 */
public class C3163w2 extends android.content.ContextWrapper {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.Object f10456c; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: d */
    public static java.util.ArrayList<java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C3163w2>> f10457d;

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.res.Resources f10458a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.Resources.Theme f10459b;

    @DexIgnore
    public C3163w2(android.content.Context context) {
        super(context);
        if (com.fossil.blesdk.obfuscated.C1701e3.m6278b()) {
            this.f10458a = new com.fossil.blesdk.obfuscated.C1701e3(this, context.getResources());
            this.f10459b = this.f10458a.newTheme();
            this.f10459b.setTo(context.getTheme());
            return;
        }
        this.f10458a = new com.fossil.blesdk.obfuscated.C3340y2(this, context.getResources());
        this.f10459b = null;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15557a(android.content.Context context) {
        if ((context instanceof com.fossil.blesdk.obfuscated.C3163w2) || (context.getResources() instanceof com.fossil.blesdk.obfuscated.C3340y2) || (context.getResources() instanceof com.fossil.blesdk.obfuscated.C1701e3)) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT < 21 || com.fossil.blesdk.obfuscated.C1701e3.m6278b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.Context m15558b(android.content.Context context) {
        if (!m15557a(context)) {
            return context;
        }
        synchronized (f10456c) {
            if (f10457d == null) {
                f10457d = new java.util.ArrayList<>();
            } else {
                for (int size = f10457d.size() - 1; size >= 0; size--) {
                    java.lang.ref.WeakReference weakReference = f10457d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f10457d.remove(size);
                    }
                }
                for (int size2 = f10457d.size() - 1; size2 >= 0; size2--) {
                    java.lang.ref.WeakReference weakReference2 = f10457d.get(size2);
                    com.fossil.blesdk.obfuscated.C3163w2 w2Var = weakReference2 != null ? (com.fossil.blesdk.obfuscated.C3163w2) weakReference2.get() : null;
                    if (w2Var != null && w2Var.getBaseContext() == context) {
                        return w2Var;
                    }
                }
            }
            com.fossil.blesdk.obfuscated.C3163w2 w2Var2 = new com.fossil.blesdk.obfuscated.C3163w2(context);
            f10457d.add(new java.lang.ref.WeakReference(w2Var2));
            return w2Var2;
        }
    }

    @DexIgnore
    public android.content.res.AssetManager getAssets() {
        return this.f10458a.getAssets();
    }

    @DexIgnore
    public android.content.res.Resources getResources() {
        return this.f10458a;
    }

    @DexIgnore
    public android.content.res.Resources.Theme getTheme() {
        android.content.res.Resources.Theme theme = this.f10459b;
        return theme == null ? super.getTheme() : theme;
    }

    @DexIgnore
    public void setTheme(int i) {
        android.content.res.Resources.Theme theme = this.f10459b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
