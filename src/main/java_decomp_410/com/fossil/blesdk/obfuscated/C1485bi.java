package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bi */
public class C1485bi {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C1803fi f3743a;

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Field f3744b;

    @DexIgnore
    /* renamed from: c */
    public static boolean f3745c;

    @DexIgnore
    /* renamed from: d */
    public static /* final */ android.util.Property<android.view.View, java.lang.Float> f3746d; // = new com.fossil.blesdk.obfuscated.C1485bi.C1486a(java.lang.Float.class, "translationAlpha");

    @DexIgnore
    /* renamed from: e */
    public static /* final */ android.util.Property<android.view.View, android.graphics.Rect> f3747e; // = new com.fossil.blesdk.obfuscated.C1485bi.C1487b(android.graphics.Rect.class, "clipBounds");

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.bi$a */
    public static class C1486a extends android.util.Property<android.view.View, java.lang.Float> {
        @DexIgnore
        public C1486a(java.lang.Class cls, java.lang.String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Float get(android.view.View view) {
            return java.lang.Float.valueOf(com.fossil.blesdk.obfuscated.C1485bi.m4990c(view));
        }

        @DexIgnore
        /* renamed from: a */
        public void set(android.view.View view, java.lang.Float f) {
            com.fossil.blesdk.obfuscated.C1485bi.m4984a(view, f.floatValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bi$b")
    /* renamed from: com.fossil.blesdk.obfuscated.bi$b */
    public static class C1487b extends android.util.Property<android.view.View, android.graphics.Rect> {
        @DexIgnore
        public C1487b(java.lang.Class cls, java.lang.String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.Rect get(android.view.View view) {
            return com.fossil.blesdk.obfuscated.C1776f9.m6835e(view);
        }

        @DexIgnore
        /* renamed from: a */
        public void set(android.view.View view, android.graphics.Rect rect) {
            com.fossil.blesdk.obfuscated.C1776f9.m6810a(view, rect);
        }
    }

    /*
    static {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 22) {
            f3743a = new com.fossil.blesdk.obfuscated.C1732ei();
        } else if (i >= 21) {
            f3743a = new com.fossil.blesdk.obfuscated.C1634di();
        } else if (i >= 19) {
            f3743a = new com.fossil.blesdk.obfuscated.C1557ci();
        } else {
            f3743a = new com.fossil.blesdk.obfuscated.C1803fi();
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static void m4984a(android.view.View view, float f) {
        f3743a.mo9528a(view, f);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1418ai m4988b(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return new com.fossil.blesdk.obfuscated.C3437zh(view);
        }
        return com.fossil.blesdk.obfuscated.C3363yh.m16850c(view);
    }

    @DexIgnore
    /* renamed from: c */
    public static float m4990c(android.view.View view) {
        return f3743a.mo9529b(view);
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C2128ji m4992d(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return new com.fossil.blesdk.obfuscated.C2021ii(view);
        }
        return new com.fossil.blesdk.obfuscated.C1951hi(view.getWindowToken());
    }

    @DexIgnore
    /* renamed from: e */
    public static void m4993e(android.view.View view) {
        f3743a.mo9531c(view);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4983a(android.view.View view) {
        f3743a.mo9527a(view);
    }

    @DexIgnore
    /* renamed from: c */
    public static void m4991c(android.view.View view, android.graphics.Matrix matrix) {
        f3743a.mo9957c(view, matrix);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4985a(android.view.View view, int i) {
        m4982a();
        java.lang.reflect.Field field = f3744b;
        if (field != null) {
            try {
                f3744b.setInt(view, i | (field.getInt(view) & -13));
            } catch (java.lang.IllegalAccessException unused) {
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m4989b(android.view.View view, android.graphics.Matrix matrix) {
        f3743a.mo9955b(view, matrix);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4987a(android.view.View view, android.graphics.Matrix matrix) {
        f3743a.mo9954a(view, matrix);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4986a(android.view.View view, int i, int i2, int i3, int i4) {
        f3743a.mo10528a(view, i, i2, i3, i4);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4982a() {
        if (!f3745c) {
            try {
                f3744b = android.view.View.class.getDeclaredField("mViewFlags");
                f3744b.setAccessible(true);
            } catch (java.lang.NoSuchFieldException unused) {
                android.util.Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            f3745c = true;
        }
    }
}
