package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gv0 extends ev0 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public gv0() {
        super();
    }

    @DexIgnore
    public static <E> List<E> b(Object obj, long j) {
        return (List) hx0.f(obj, j);
    }

    @DexIgnore
    public final void a(Object obj, long j) {
        Object obj2;
        List list = (List) hx0.f(obj, j);
        if (list instanceof dv0) {
            obj2 = ((dv0) list).F();
        } else if (!c.isAssignableFrom(list.getClass())) {
            obj2 = Collections.unmodifiableList(list);
        } else {
            return;
        }
        hx0.a(obj, j, obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: com.fossil.blesdk.obfuscated.cv0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: com.fossil.blesdk.obfuscated.cv0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: com.fossil.blesdk.obfuscated.cv0} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final <E> void a(Object obj, Object obj2, long j) {
        List list;
        List b = b(obj2, j);
        int size = b.size();
        List b2 = b(obj, j);
        if (b2.isEmpty()) {
            b2 = b2 instanceof dv0 ? new cv0(size) : new ArrayList(size);
            hx0.a(obj, j, (Object) b2);
        } else {
            if (c.isAssignableFrom(b2.getClass())) {
                ArrayList arrayList = new ArrayList(b2.size() + size);
                arrayList.addAll(b2);
                list = arrayList;
            } else if (b2 instanceof ex0) {
                cv0 cv0 = new cv0(b2.size() + size);
                cv0.addAll((ex0) b2);
                list = cv0;
            }
            hx0.a(obj, j, (Object) list);
            b2 = list;
        }
        int size2 = b2.size();
        int size3 = b.size();
        if (size2 > 0 && size3 > 0) {
            b2.addAll(b);
        }
        if (size2 > 0) {
            b = b2;
        }
        hx0.a(obj, j, (Object) b);
    }
}
