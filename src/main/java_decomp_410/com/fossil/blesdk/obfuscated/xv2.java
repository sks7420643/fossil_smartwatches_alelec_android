package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xv2 implements Factory<HomeAlertsPresenter> {
    @DexIgnore
    public static HomeAlertsPresenter a(sv2 sv2, j62 j62, AlarmHelper alarmHelper, vy2 vy2, px2 px2, iw2 iw2, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, en2 en2, DNDSettingsDatabase dNDSettingsDatabase) {
        return new HomeAlertsPresenter(sv2, j62, alarmHelper, vy2, px2, iw2, notificationSettingsDatabase, setAlarms, alarmsRepository, en2, dNDSettingsDatabase);
    }
}
