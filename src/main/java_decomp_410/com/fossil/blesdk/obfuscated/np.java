package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.engine.DecodeJob;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class np<Transcode> {
    @DexIgnore
    public /* final */ List<sr.a<?>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<jo> b; // = new ArrayList();
    @DexIgnore
    public tn c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public DecodeJob.e h;
    @DexIgnore
    public lo i;
    @DexIgnore
    public Map<Class<?>, oo<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public jo n;
    @DexIgnore
    public Priority o;
    @DexIgnore
    public pp p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    public <R> void a(tn tnVar, Object obj, jo joVar, int i2, int i3, pp ppVar, Class<?> cls, Class<R> cls2, Priority priority, lo loVar, Map<Class<?>, oo<?>> map, boolean z, boolean z2, DecodeJob.e eVar) {
        this.c = tnVar;
        this.d = obj;
        this.n = joVar;
        this.e = i2;
        this.f = i3;
        this.p = ppVar;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = priority;
        this.i = loVar;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public gq b() {
        return this.c.a();
    }

    @DexIgnore
    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    @DexIgnore
    public tq d() {
        return this.h.a();
    }

    @DexIgnore
    public pp e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<sr.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List a2 = this.c.f().a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                sr.a a3 = ((sr) a2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a3 != null) {
                    this.a.add(a3);
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public Class<?> h() {
        return this.d.getClass();
    }

    @DexIgnore
    public lo i() {
        return this.i;
    }

    @DexIgnore
    public Priority j() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> k() {
        return this.c.f().c(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public jo l() {
        return this.n;
    }

    @DexIgnore
    public Class<?> m() {
        return this.k;
    }

    @DexIgnore
    public int n() {
        return this.e;
    }

    @DexIgnore
    public boolean o() {
        return this.r;
    }

    @DexIgnore
    public <Z> oo<Z> b(Class<Z> cls) {
        oo<Z> ooVar = this.j.get(cls);
        if (ooVar == null) {
            Iterator<Map.Entry<Class<?>, oo<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    ooVar = (oo) next.getValue();
                    break;
                }
            }
        }
        if (ooVar != null) {
            return ooVar;
        }
        if (!this.j.isEmpty() || !this.q) {
            return ks.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public List<jo> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<sr.a<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                sr.a aVar = g2.get(i2);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public boolean b(aq<?> aqVar) {
        return this.c.f().b(aqVar);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public <Data> yp<Data, ?, Transcode> a(Class<Data> cls) {
        return this.c.f().b(cls, this.g, this.k);
    }

    @DexIgnore
    public <Z> no<Z> a(aq<Z> aqVar) {
        return this.c.f().a(aqVar);
    }

    @DexIgnore
    public List<sr<File, ?>> a(File file) throws Registry.NoModelLoaderAvailableException {
        return this.c.f().a(file);
    }

    @DexIgnore
    public boolean a(jo joVar) {
        List<sr.a<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).a.equals(joVar)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public <X> ho<X> a(X x) throws Registry.NoSourceEncoderAvailableException {
        return this.c.f().c(x);
    }
}
