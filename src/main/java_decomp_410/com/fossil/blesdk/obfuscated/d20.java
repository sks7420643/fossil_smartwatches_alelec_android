package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d20 extends b20 {
    @DexIgnore
    public /* final */ Peripheral.State a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public d20(Peripheral.State state, int i) {
        kd4.b(state, "newState");
        this.a = state;
        this.b = i;
    }

    @DexIgnore
    public final Peripheral.State a() {
        return this.a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }
}
