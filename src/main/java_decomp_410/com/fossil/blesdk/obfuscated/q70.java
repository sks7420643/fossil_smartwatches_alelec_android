package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.calibration.HandMovingConfig;
import com.fossil.blesdk.device.data.calibration.HandMovingType;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q70 extends p70 {
    @DexIgnore
    public static /* final */ a N; // = new a((fd4) null);
    @DexIgnore
    public /* final */ HandMovingType L;
    @DexIgnore
    public /* final */ HandMovingConfig[] M;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final byte[] a(HandMovingConfig[] handMovingConfigArr) {
            ByteBuffer allocate = ByteBuffer.allocate(handMovingConfigArr.length * 5);
            for (HandMovingConfig data$blesdk_productionRelease : handMovingConfigArr) {
                allocate.put(data$blesdk_productionRelease.getData$blesdk_productionRelease());
            }
            byte[] array = allocate.array();
            kd4.a((Object) array, "array.array()");
            return array;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q70(Peripheral peripheral, HandMovingType handMovingType, HandMovingConfig[] handMovingConfigArr) {
        super(DeviceConfigOperationCode.MOVE_HANDS, RequestId.MOVE_HANDS, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(handMovingType, "handMovingType");
        kd4.b(handMovingConfigArr, "handMovingConfigs");
        this.L = handMovingType;
        this.M = handMovingConfigArr;
        c(true);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate((this.M.length * 5) + 2).order(ByteOrder.LITTLE_ENDIAN).put(this.L.getId$blesdk_productionRelease()).put((byte) this.M.length).put(N.a(this.M)).array();
        kd4.a((Object) array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(super.t(), JSONKey.MOVING_TYPE, this.L.getLogName$blesdk_productionRelease()), JSONKey.HAND_CONFIGS, j00.a(this.M));
    }
}
