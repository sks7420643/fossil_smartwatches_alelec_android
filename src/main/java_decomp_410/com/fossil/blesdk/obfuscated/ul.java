package com.fossil.blesdk.obfuscated;

import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ul implements Runnable {
    @DexIgnore
    public tj e;
    @DexIgnore
    public String f;
    @DexIgnore
    public WorkerParameters.a g;

    @DexIgnore
    public ul(tj tjVar, String str, WorkerParameters.a aVar) {
        this.e = tjVar;
        this.f = str;
        this.g = aVar;
    }

    @DexIgnore
    public void run() {
        this.e.e().a(this.f, this.g);
    }
}
