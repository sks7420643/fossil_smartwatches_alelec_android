package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oc */
public abstract class C2542oc<D> extends com.fossil.blesdk.obfuscated.C2719qc<D> {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ java.lang.String TAG; // = "AsyncTaskLoader";
    @DexIgnore
    public volatile com.fossil.blesdk.obfuscated.C2542oc<D>.a mCancellingTask;
    @DexIgnore
    public /* final */ java.util.concurrent.Executor mExecutor;
    @DexIgnore
    public android.os.Handler mHandler;
    @DexIgnore
    public long mLastLoadCompleteTime;
    @DexIgnore
    public volatile com.fossil.blesdk.obfuscated.C2542oc<D>.a mTask;
    @DexIgnore
    public long mUpdateThrottle;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oc$a */
    public final class C2543a extends androidx.loader.content.ModernAsyncTask<java.lang.Void, java.lang.Void, D> implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: n */
        public /* final */ java.util.concurrent.CountDownLatch f8043n; // = new java.util.concurrent.CountDownLatch(1);

        @DexIgnore
        /* renamed from: o */
        public boolean f8044o;

        @DexIgnore
        public C2543a() {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo2316b(D d) {
            try {
                com.fossil.blesdk.obfuscated.C2542oc.this.dispatchOnCancelled(this, d);
            } finally {
                this.f8043n.countDown();
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo2319c(D d) {
            try {
                com.fossil.blesdk.obfuscated.C2542oc.this.dispatchOnLoadComplete(this, d);
            } finally {
                this.f8043n.countDown();
            }
        }

        @DexIgnore
        /* renamed from: e */
        public void mo14326e() {
            try {
                this.f8043n.await();
            } catch (java.lang.InterruptedException unused) {
            }
        }

        @DexIgnore
        public void run() {
            this.f8044o = false;
            com.fossil.blesdk.obfuscated.C2542oc.this.executePendingTask();
        }

        @DexIgnore
        /* renamed from: a */
        public D mo2311a(java.lang.Void... voidArr) {
            try {
                return com.fossil.blesdk.obfuscated.C2542oc.this.onLoadInBackground();
            } catch (androidx.core.p001os.OperationCanceledException e) {
                if (mo2313a()) {
                    return null;
                }
                throw e;
            }
        }
    }

    @DexIgnore
    public C2542oc(android.content.Context context) {
        this(context, androidx.loader.content.ModernAsyncTask.f1182l);
    }

    @DexIgnore
    public void cancelLoadInBackground() {
    }

    @DexIgnore
    public void dispatchOnCancelled(com.fossil.blesdk.obfuscated.C2542oc<D>.a aVar, D d) {
        onCanceled(d);
        if (this.mCancellingTask == aVar) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = android.os.SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    @DexIgnore
    public void dispatchOnLoadComplete(com.fossil.blesdk.obfuscated.C2542oc<D>.a aVar, D d) {
        if (this.mTask != aVar) {
            dispatchOnCancelled(aVar, d);
        } else if (isAbandoned()) {
            onCanceled(d);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = android.os.SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d);
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    public void dump(java.lang.String str, java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        if (this.mTask != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.f8044o);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.f8044o);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            com.fossil.blesdk.obfuscated.C2189k8.m9376a(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            com.fossil.blesdk.obfuscated.C2189k8.m9375a(this.mLastLoadCompleteTime, android.os.SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }

    @DexIgnore
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.f8044o) {
                this.mTask.f8044o = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || android.os.SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.mo2310a(this.mExecutor, (Params[]) null);
                return;
            }
            this.mTask.f8044o = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    @DexIgnore
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }

    @DexIgnore
    public abstract D loadInBackground();

    @DexIgnore
    public boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!this.mStarted) {
            this.mContentChanged = true;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.f8044o) {
                this.mTask.f8044o = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
            return false;
        } else if (this.mTask.f8044o) {
            this.mTask.f8044o = false;
            this.mHandler.removeCallbacks(this.mTask);
            this.mTask = null;
            return false;
        } else {
            boolean a = this.mTask.mo2314a(false);
            if (a) {
                this.mCancellingTask = this.mTask;
                cancelLoadInBackground();
            }
            this.mTask = null;
            return a;
        }
    }

    @DexIgnore
    public void onCanceled(D d) {
    }

    @DexIgnore
    public void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new com.fossil.blesdk.obfuscated.C2542oc.C2543a();
        executePendingTask();
    }

    @DexIgnore
    public D onLoadInBackground() {
        return loadInBackground();
    }

    @DexIgnore
    public void setUpdateThrottle(long j) {
        this.mUpdateThrottle = j;
        if (j != 0) {
            this.mHandler = new android.os.Handler();
        }
    }

    @DexIgnore
    public void waitForLoader() {
        com.fossil.blesdk.obfuscated.C2542oc<D>.a aVar = this.mTask;
        if (aVar != null) {
            aVar.mo14326e();
        }
    }

    @DexIgnore
    public C2542oc(android.content.Context context, java.util.concurrent.Executor executor) {
        super(context);
        this.mLastLoadCompleteTime = -10000;
        this.mExecutor = executor;
    }
}
