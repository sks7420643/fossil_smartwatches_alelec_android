package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class es2 extends zr2 implements wk3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<sb2> j;
    @DexIgnore
    public vk3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return es2.m;
        }

        @DexIgnore
        public final es2 b() {
            return new es2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public b(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sb2 e;
        @DexIgnore
        public /* final */ /* synthetic */ es2 f;

        @DexIgnore
        public c(sb2 sb2, es2 es2) {
            this.e = sb2;
            this.f = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            vk3 a = es2.a(this.f);
            TextInputEditText textInputEditText = this.e.s;
            kd4.a((Object) textInputEditText, "binding.etEmail");
            a.b(String.valueOf(textInputEditText.getText()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ sb2 e;
        @DexIgnore
        public /* final */ /* synthetic */ es2 f;

        @DexIgnore
        public d(sb2 sb2, es2 es2) {
            this.e = sb2;
            this.f = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            vk3 a = es2.a(this.f);
            TextInputEditText textInputEditText = this.e.s;
            kd4.a((Object) textInputEditText, "binding.etEmail");
            a.b(String.valueOf(textInputEditText.getText()));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public e(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public f(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpActivity.a aVar = SignUpActivity.G;
            kd4.a((Object) view, "it");
            Context context = view.getContext();
            kd4.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public g(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ es2 e;

        @DexIgnore
        public h(es2 es2) {
            this.e = es2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x000c, code lost:
            if (r1 != null) goto L_0x0011;
         */
        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String str;
            vk3 a = es2.a(this.e);
            if (charSequence != null) {
                str = charSequence.toString();
            }
            str = "";
            a.a(str);
        }
    }

    /*
    static {
        String simpleName = es2.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "ForgotPasswordFragment::class.java.simpleName!!");
            m = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ vk3 a(es2 es2) {
        vk3 vk3 = es2.k;
        if (vk3 != null) {
            return vk3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void G(boolean z) {
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                if (flexibleTextView == null) {
                    return;
                }
                if (z) {
                    kd4.a((Object) flexibleTextView, "it");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                kd4.a((Object) flexibleTextView, "it");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void H(String str) {
        kd4.b(str, "message");
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                TextInputLayout textInputLayout = a2.x;
                kd4.a((Object) textInputLayout, "it.inputEmail");
                textInputLayout.setErrorEnabled(false);
                TextInputLayout textInputLayout2 = a2.x;
                kd4.a((Object) textInputLayout2, "it.inputEmail");
                textInputLayout2.setError(str);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void O(String str) {
        kd4.b(str, "email");
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                TextInputEditText textInputEditText = a2.s;
                if (textInputEditText != null) {
                    textInputEditText.setText(str);
                    textInputEditText.requestFocus();
                    cl2 cl2 = cl2.a;
                    kd4.a((Object) textInputEditText, "it");
                    Context context = textInputEditText.getContext();
                    kd4.a((Object) context, "it.context");
                    cl2.b(textInputEditText, context);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void a0() {
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.u;
                kd4.a((Object) flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.u;
                kd4.a((Object) flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.u;
                kd4.a((Object) flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(true);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public void k() {
        b();
    }

    @DexIgnore
    public void l0() {
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.u;
                kd4.a((Object) flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.u;
                kd4.a((Object) flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.u;
                kd4.a((Object) flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(false);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (sb2) qa.a(layoutInflater, R.layout.fragment_forgot_password, viewGroup, false, O0()));
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        vk3 vk3 = this.k;
        if (vk3 != null) {
            vk3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new b(this));
                a2.u.setOnClickListener(new c(a2, this));
                a2.v.setOnClickListener(new d(a2, this));
                a2.y.setOnClickListener(new e(this));
                a2.w.setOnClickListener(new f(this));
                a2.z.setOnClickListener(new g(this));
                a2.s.addTextChangedListener(new h(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void u0() {
        tr3<sb2> tr3 = this.j;
        if (tr3 != null) {
            sb2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                kd4.a((Object) constraintLayout, "it.clResetPwSuccess");
                if (constraintLayout.getVisibility() == 0) {
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_EmailResent_Text__EmailWithInstructionsResentRememberTo);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026ructionsResentRememberTo)");
                    T(a3);
                    return;
                }
                ConstraintLayout constraintLayout2 = a2.q;
                kd4.a((Object) constraintLayout2, "it.clResetPw");
                constraintLayout2.setVisibility(8);
                ConstraintLayout constraintLayout3 = a2.r;
                kd4.a((Object) constraintLayout3, "it.clResetPwSuccess");
                constraintLayout3.setVisibility(0);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(vk3 vk3) {
        kd4.b(vk3, "presenter");
        this.k = vk3;
    }

    @DexIgnore
    public void a(int i, String str) {
        kd4.b(str, "errorMessage");
        ds3 ds3 = ds3.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        ds3.a(i, str, childFragmentManager);
    }
}
