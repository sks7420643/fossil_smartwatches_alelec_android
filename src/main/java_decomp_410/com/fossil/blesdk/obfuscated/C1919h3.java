package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h3 */
public class C1919h3 extends com.fossil.blesdk.obfuscated.C2102j3 {

    @DexIgnore
    /* renamed from: c */
    public static volatile com.fossil.blesdk.obfuscated.C1919h3 f5659c;

    @DexIgnore
    /* renamed from: d */
    public static /* final */ java.util.concurrent.Executor f5660d; // = new com.fossil.blesdk.obfuscated.C1919h3.C1920a();

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.util.concurrent.Executor f5661e; // = new com.fossil.blesdk.obfuscated.C1919h3.C1921b();

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2102j3 f5662a; // = this.f5663b;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2102j3 f5663b; // = new com.fossil.blesdk.obfuscated.C1995i3();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.h3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.h3$a */
    public static class C1920a implements java.util.concurrent.Executor {
        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            com.fossil.blesdk.obfuscated.C1919h3.m7768c().mo11565c(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.h3$b")
    /* renamed from: com.fossil.blesdk.obfuscated.h3$b */
    public static class C1921b implements java.util.concurrent.Executor {
        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            com.fossil.blesdk.obfuscated.C1919h3.m7768c().mo11563a(runnable);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static java.util.concurrent.Executor m7767b() {
        return f5661e;
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C1919h3 m7768c() {
        if (f5659c != null) {
            return f5659c;
        }
        synchronized (com.fossil.blesdk.obfuscated.C1919h3.class) {
            if (f5659c == null) {
                f5659c = new com.fossil.blesdk.obfuscated.C1919h3();
            }
        }
        return f5659c;
    }

    @DexIgnore
    /* renamed from: d */
    public static java.util.concurrent.Executor m7769d() {
        return f5660d;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11563a(java.lang.Runnable runnable) {
        this.f5662a.mo11563a(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11564a() {
        return this.f5662a.mo11564a();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11565c(java.lang.Runnable runnable) {
        this.f5662a.mo11565c(runnable);
    }
}
