package com.fossil.blesdk.obfuscated;

import com.android.volley.VolleyError;
import com.fossil.blesdk.obfuscated.lm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class um<T> {
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ lm.a b;
    @DexIgnore
    public /* final */ VolleyError c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onErrorResponse(VolleyError volleyError);
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void onResponse(T t);
    }

    @DexIgnore
    public um(T t, lm.a aVar) {
        this.d = false;
        this.a = t;
        this.b = aVar;
        this.c = null;
    }

    @DexIgnore
    public static <T> um<T> a(T t, lm.a aVar) {
        return new um<>(t, aVar);
    }

    @DexIgnore
    public static <T> um<T> a(VolleyError volleyError) {
        return new um<>(volleyError);
    }

    @DexIgnore
    public boolean a() {
        return this.c == null;
    }

    @DexIgnore
    public um(VolleyError volleyError) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = volleyError;
    }
}
