package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yn3 {
    @DexIgnore
    public /* final */ wn3 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public yn3(wn3 wn3, ArrayList<Integer> arrayList) {
        kd4.b(wn3, "mView");
        this.a = wn3;
        this.b = arrayList;
    }

    @DexIgnore
    public final ArrayList<Integer> a() {
        ArrayList<Integer> arrayList = this.b;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final wn3 b() {
        return this.a;
    }
}
