package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ay */
public class C1451ay implements com.fossil.blesdk.obfuscated.C2356lx.C2358b {

    @DexIgnore
    /* renamed from: a */
    public /* final */ long f3620a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1837fx f3621b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.o44 f3622c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2356lx f3623d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2080ix f3624e;

    @DexIgnore
    public C1451ay(com.fossil.blesdk.obfuscated.C1837fx fxVar, com.fossil.blesdk.obfuscated.o44 o44, com.fossil.blesdk.obfuscated.C2356lx lxVar, com.fossil.blesdk.obfuscated.C2080ix ixVar, long j) {
        this.f3621b = fxVar;
        this.f3622c = o44;
        this.f3623d = lxVar;
        this.f3624e = ixVar;
        this.f3620a = j;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1451ay m4682a(com.fossil.blesdk.obfuscated.v44 v44, android.content.Context context, p011io.fabric.sdk.android.services.common.IdManager idManager, java.lang.String str, java.lang.String str2, long j) {
        android.content.Context context2 = context;
        p011io.fabric.sdk.android.services.common.IdManager idManager2 = idManager;
        com.fossil.blesdk.obfuscated.C1753ey eyVar = new com.fossil.blesdk.obfuscated.C1753ey(context, idManager, str, str2);
        com.fossil.blesdk.obfuscated.C1909gx gxVar = new com.fossil.blesdk.obfuscated.C1909gx(context, new com.fossil.blesdk.obfuscated.f74(v44));
        com.fossil.blesdk.obfuscated.y64 y64 = new com.fossil.blesdk.obfuscated.y64(com.fossil.blesdk.obfuscated.q44.m26805g());
        com.fossil.blesdk.obfuscated.o44 o44 = new com.fossil.blesdk.obfuscated.o44(context);
        java.util.concurrent.ScheduledExecutorService b = com.fossil.blesdk.obfuscated.q54.m26834b("Answers Events Handler");
        com.fossil.blesdk.obfuscated.C2356lx lxVar = new com.fossil.blesdk.obfuscated.C2356lx(b);
        com.fossil.blesdk.obfuscated.C1837fx fxVar = new com.fossil.blesdk.obfuscated.C1837fx(v44, context, gxVar, eyVar, y64, b, new com.fossil.blesdk.obfuscated.C2844rx(context));
        com.fossil.blesdk.obfuscated.C1451ay ayVar = new com.fossil.blesdk.obfuscated.C1451ay(fxVar, o44, lxVar, com.fossil.blesdk.obfuscated.C2080ix.m8688a(context), j);
        return ayVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8980b() {
        this.f3622c.mo29807a();
        this.f3621b.mo11060a();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8981c() {
        this.f3621b.mo11066b();
        this.f3622c.mo29808a(new com.fossil.blesdk.obfuscated.C1978hx(this, this.f3623d));
        this.f3623d.mo13476a((com.fossil.blesdk.obfuscated.C2356lx.C2358b) this);
        if (mo8982d()) {
            mo8975a(this.f3620a);
            this.f3624e.mo12163b();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo8982d() {
        return !this.f3624e.mo12162a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8978a(com.fossil.blesdk.obfuscated.C2439mx mxVar) {
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30060d("Answers", "Logged custom event: " + mxVar);
        this.f3621b.mo11061a(com.crashlytics.android.answers.SessionEvent.m2700a(mxVar));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8979a(java.lang.String str, java.lang.String str2) {
        if (android.os.Looper.myLooper() != android.os.Looper.getMainLooper()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Logged crash");
            this.f3621b.mo11070c(com.crashlytics.android.answers.SessionEvent.m2702a(str, str2));
            return;
        }
        throw new java.lang.IllegalStateException("onCrash called from main thread!!!");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8975a(long j) {
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Logged install");
        this.f3621b.mo11067b(com.crashlytics.android.answers.SessionEvent.m2698a(j));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8976a(android.app.Activity activity, com.crashlytics.android.answers.SessionEvent.Type type) {
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30060d("Answers", "Logged lifecycle event: " + type.name());
        this.f3621b.mo11061a(com.crashlytics.android.answers.SessionEvent.m2699a(type, activity));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8974a() {
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Answers", "Flush events when app is backgrounded");
        this.f3621b.mo11069c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8977a(com.fossil.blesdk.obfuscated.j74 j74, java.lang.String str) {
        this.f3623d.mo13477a(j74.f15942i);
        this.f3621b.mo11063a(j74, str);
    }
}
