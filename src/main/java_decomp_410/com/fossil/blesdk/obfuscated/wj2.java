package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wj2 implements Factory<vj2> {
    @DexIgnore
    public /* final */ Provider<GetDianaDeviceSettingUseCase> a;
    @DexIgnore
    public /* final */ Provider<GetHybridDeviceSettingUseCase> b;
    @DexIgnore
    public /* final */ Provider<HybridSyncUseCase> c;
    @DexIgnore
    public /* final */ Provider<DianaSyncUseCase> d;

    @DexIgnore
    public wj2(Provider<GetDianaDeviceSettingUseCase> provider, Provider<GetHybridDeviceSettingUseCase> provider2, Provider<HybridSyncUseCase> provider3, Provider<DianaSyncUseCase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static wj2 a(Provider<GetDianaDeviceSettingUseCase> provider, Provider<GetHybridDeviceSettingUseCase> provider2, Provider<HybridSyncUseCase> provider3, Provider<DianaSyncUseCase> provider4) {
        return new wj2(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static vj2 b(Provider<GetDianaDeviceSettingUseCase> provider, Provider<GetHybridDeviceSettingUseCase> provider2, Provider<HybridSyncUseCase> provider3, Provider<DianaSyncUseCase> provider4) {
        return new vj2(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public vj2 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
