package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zc1 implements Parcelable.Creator<LocationAvailability> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        id1[] id1Arr = null;
        int i = 1000;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                i3 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                j = SafeParcelReader.s(parcel, a);
            } else if (a2 == 4) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                id1Arr = SafeParcelReader.b(parcel, a, id1.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new LocationAvailability(i, i2, i3, j, id1Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
