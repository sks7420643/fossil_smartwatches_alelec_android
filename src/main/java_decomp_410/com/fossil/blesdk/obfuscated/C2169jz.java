package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jz */
public class C2169jz {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ com.fossil.blesdk.obfuscated.C2169jz.C2172c f6630d; // = new com.fossil.blesdk.obfuscated.C2169jz.C2172c();

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f6631a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2169jz.C2171b f6632b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1984hz f6633c;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.jz$b */
    public interface C2171b {
        @DexIgnore
        /* renamed from: a */
        java.io.File mo4162a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jz$c")
    /* renamed from: com.fossil.blesdk.obfuscated.jz$c */
    public static final class C2172c implements com.fossil.blesdk.obfuscated.C1984hz {
        @DexIgnore
        public C2172c() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11795a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11796a(long j, java.lang.String str) {
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2610oy mo11797b() {
            return null;
        }

        @DexIgnore
        /* renamed from: c */
        public byte[] mo11798c() {
            return null;
        }

        @DexIgnore
        /* renamed from: d */
        public void mo11799d() {
        }
    }

    @DexIgnore
    public C2169jz(android.content.Context context, com.fossil.blesdk.obfuscated.C2169jz.C2171b bVar) {
        this(context, bVar, (java.lang.String) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12515a(long j, java.lang.String str) {
        this.f6633c.mo11796a(j, str);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo12519b(java.lang.String str) {
        this.f6633c.mo11795a();
        this.f6633c = f6630d;
        if (str != null) {
            if (!p011io.fabric.sdk.android.services.common.CommonUtils.m36877a(this.f6631a, "com.crashlytics.CollectCustomLogs", true)) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Preferences requested no custom logs. Aborting log file creation.");
            } else {
                mo12516a(mo12512a(str), 65536);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public byte[] mo12520c() {
        return this.f6633c.mo11798c();
    }

    @DexIgnore
    public C2169jz(android.content.Context context, com.fossil.blesdk.obfuscated.C2169jz.C2171b bVar, java.lang.String str) {
        this.f6631a = context;
        this.f6632b = bVar;
        this.f6633c = f6630d;
        mo12519b(str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12514a() {
        this.f6633c.mo11799d();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12517a(java.util.Set<java.lang.String> set) {
        java.io.File[] listFiles = this.f6632b.mo4162a().listFiles();
        if (listFiles != null) {
            for (java.io.File file : listFiles) {
                if (!set.contains(mo12513a(file))) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12516a(java.io.File file, int i) {
        this.f6633c = new com.fossil.blesdk.obfuscated.C3155vz(file, i);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2610oy mo12518b() {
        return this.f6633c.mo11797b();
    }

    @DexIgnore
    /* renamed from: a */
    public final java.io.File mo12512a(java.lang.String str) {
        return new java.io.File(this.f6632b.mo4162a(), "crashlytics-userlog-" + str + ".temp");
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo12513a(java.io.File file) {
        java.lang.String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".temp");
        if (lastIndexOf == -1) {
            return name;
        }
        return name.substring(20, lastIndexOf);
    }
}
