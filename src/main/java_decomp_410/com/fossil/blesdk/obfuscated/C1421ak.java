package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ak */
public class C1421ak implements com.fossil.blesdk.obfuscated.C2419mj {

    @DexIgnore
    /* renamed from: o */
    public static /* final */ java.lang.String f3501o; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("SystemAlarmDispatcher");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f3502e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3444zl f3503f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C1560ck f3504g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2572oj f3505h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2968tj f3506i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C3293xj f3507j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ android.os.Handler f3508k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.List<android.content.Intent> f3509l;

    @DexIgnore
    /* renamed from: m */
    public android.content.Intent f3510m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C1421ak.C1424c f3511n;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ak$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ak$a */
    public class C1422a implements java.lang.Runnable {
        @DexIgnore
        public C1422a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1421ak.C1425d dVar;
            com.fossil.blesdk.obfuscated.C1421ak akVar;
            synchronized (com.fossil.blesdk.obfuscated.C1421ak.this.f3509l) {
                com.fossil.blesdk.obfuscated.C1421ak.this.f3510m = com.fossil.blesdk.obfuscated.C1421ak.this.f3509l.get(0);
            }
            android.content.Intent intent = com.fossil.blesdk.obfuscated.C1421ak.this.f3510m;
            if (intent != null) {
                java.lang.String action = intent.getAction();
                int intExtra = com.fossil.blesdk.obfuscated.C1421ak.this.f3510m.getIntExtra("KEY_START_ID", 0);
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C1421ak.f3501o, java.lang.String.format("Processing command %s, %s", new java.lang.Object[]{com.fossil.blesdk.obfuscated.C1421ak.this.f3510m, java.lang.Integer.valueOf(intExtra)}), new java.lang.Throwable[0]);
                android.os.PowerManager.WakeLock a = com.fossil.blesdk.obfuscated.C3228wl.m15862a(com.fossil.blesdk.obfuscated.C1421ak.this.f3502e, java.lang.String.format("%s (%s)", new java.lang.Object[]{action, java.lang.Integer.valueOf(intExtra)}));
                try {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C1421ak.f3501o, java.lang.String.format("Acquiring operation wake lock (%s) %s", new java.lang.Object[]{action, a}), new java.lang.Throwable[0]);
                    a.acquire();
                    com.fossil.blesdk.obfuscated.C1421ak.this.f3507j.mo17766g(com.fossil.blesdk.obfuscated.C1421ak.this.f3510m, intExtra, com.fossil.blesdk.obfuscated.C1421ak.this);
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C1421ak.f3501o, java.lang.String.format("Releasing operation wake lock (%s) %s", new java.lang.Object[]{action, a}), new java.lang.Throwable[0]);
                    a.release();
                    akVar = com.fossil.blesdk.obfuscated.C1421ak.this;
                    dVar = new com.fossil.blesdk.obfuscated.C1421ak.C1425d(akVar);
                } catch (Throwable th) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C1421ak.f3501o, java.lang.String.format("Releasing operation wake lock (%s) %s", new java.lang.Object[]{action, a}), new java.lang.Throwable[0]);
                    a.release();
                    com.fossil.blesdk.obfuscated.C1421ak akVar2 = com.fossil.blesdk.obfuscated.C1421ak.this;
                    akVar2.mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1425d(akVar2));
                    throw th;
                }
                akVar.mo8763a((java.lang.Runnable) dVar);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ak$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ak$b */
    public static class C1423b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C1421ak f3513e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ android.content.Intent f3514f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ int f3515g;

        @DexIgnore
        public C1423b(com.fossil.blesdk.obfuscated.C1421ak akVar, android.content.Intent intent, int i) {
            this.f3513e = akVar;
            this.f3514f = intent;
            this.f3515g = i;
        }

        @DexIgnore
        public void run() {
            this.f3513e.mo8764a(this.f3514f, this.f3515g);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ak$c */
    public interface C1424c {
        @DexIgnore
        /* renamed from: a */
        void mo3799a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ak$d")
    /* renamed from: com.fossil.blesdk.obfuscated.ak$d */
    public static class C1425d implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C1421ak f3516e;

        @DexIgnore
        public C1425d(com.fossil.blesdk.obfuscated.C1421ak akVar) {
            this.f3516e = akVar;
        }

        @DexIgnore
        public void run() {
            this.f3516e.mo8766b();
        }
    }

    @DexIgnore
    public C1421ak(android.content.Context context) {
        this(context, (com.fossil.blesdk.obfuscated.C2572oj) null, (com.fossil.blesdk.obfuscated.C2968tj) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3804a(java.lang.String str, boolean z) {
        mo8763a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C1421ak.C1423b(this, com.fossil.blesdk.obfuscated.C3293xj.m16361a(this.f3502e, str, z), 0));
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8766b() {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3501o, "Checking if commands are complete.", new java.lang.Throwable[0]);
        mo8761a();
        synchronized (this.f3509l) {
            if (this.f3510m != null) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3501o, java.lang.String.format("Removing command %s", new java.lang.Object[]{this.f3510m}), new java.lang.Throwable[0]);
                if (this.f3509l.remove(0).equals(this.f3510m)) {
                    this.f3510m = null;
                } else {
                    throw new java.lang.IllegalStateException("Dequeue-d command is not the first.");
                }
            }
            com.fossil.blesdk.obfuscated.C2973tl b = this.f3503f.mo8789b();
            if (!this.f3507j.mo17760a() && this.f3509l.isEmpty() && !b.mo16488a()) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3501o, "No more commands & intents.", new java.lang.Throwable[0]);
                if (this.f3511n != null) {
                    this.f3511n.mo3799a();
                }
            } else if (!this.f3509l.isEmpty()) {
                mo8772h();
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2572oj mo8767c() {
        return this.f3505h;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3444zl mo8768d() {
        return this.f3503f;
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2968tj mo8769e() {
        return this.f3506i;
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1560ck mo8770f() {
        return this.f3504g;
    }

    @DexIgnore
    /* renamed from: g */
    public void mo8771g() {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3501o, "Destroying SystemAlarmDispatcher", new java.lang.Throwable[0]);
        this.f3505h.mo14427b((com.fossil.blesdk.obfuscated.C2419mj) this);
        this.f3504g.mo9545a();
        this.f3511n = null;
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo8772h() {
        mo8761a();
        android.os.PowerManager.WakeLock a = com.fossil.blesdk.obfuscated.C3228wl.m15862a(this.f3502e, "ProcessCommand");
        try {
            a.acquire();
            this.f3506i.mo16463h().mo8788a(new com.fossil.blesdk.obfuscated.C1421ak.C1422a());
        } finally {
            a.release();
        }
    }

    @DexIgnore
    public C1421ak(android.content.Context context, com.fossil.blesdk.obfuscated.C2572oj ojVar, com.fossil.blesdk.obfuscated.C2968tj tjVar) {
        this.f3502e = context.getApplicationContext();
        this.f3507j = new com.fossil.blesdk.obfuscated.C3293xj(this.f3502e);
        this.f3504g = new com.fossil.blesdk.obfuscated.C1560ck();
        this.f3506i = tjVar == null ? com.fossil.blesdk.obfuscated.C2968tj.m14288a(context) : tjVar;
        this.f3505h = ojVar == null ? this.f3506i.mo16460e() : ojVar;
        this.f3503f = this.f3506i.mo16463h();
        this.f3505h.mo14424a((com.fossil.blesdk.obfuscated.C2419mj) this);
        this.f3509l = new java.util.ArrayList();
        this.f3510m = null;
        this.f3508k = new android.os.Handler(android.os.Looper.getMainLooper());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8764a(android.content.Intent intent, int i) {
        boolean z = false;
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3501o, java.lang.String.format("Adding command %s (%s)", new java.lang.Object[]{intent, java.lang.Integer.valueOf(i)}), new java.lang.Throwable[0]);
        mo8761a();
        java.lang.String action = intent.getAction();
        if (android.text.TextUtils.isEmpty(action)) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9966e(f3501o, "Unknown command. Ignoring", new java.lang.Throwable[0]);
            return false;
        } else if ("ACTION_CONSTRAINTS_CHANGED".equals(action) && mo8765a("ACTION_CONSTRAINTS_CHANGED")) {
            return false;
        } else {
            intent.putExtra("KEY_START_ID", i);
            synchronized (this.f3509l) {
                if (!this.f3509l.isEmpty()) {
                    z = true;
                }
                this.f3509l.add(intent);
                if (!z) {
                    mo8772h();
                }
            }
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8762a(com.fossil.blesdk.obfuscated.C1421ak.C1424c cVar) {
        if (this.f3511n != null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f3501o, "A completion listener for SystemAlarmDispatcher already exists.", new java.lang.Throwable[0]);
        } else {
            this.f3511n = cVar;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8763a(java.lang.Runnable runnable) {
        this.f3508k.post(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo8765a(java.lang.String str) {
        mo8761a();
        synchronized (this.f3509l) {
            for (android.content.Intent action : this.f3509l) {
                if (str.equals(action.getAction())) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo8761a() {
        if (this.f3508k.getLooper().getThread() != java.lang.Thread.currentThread()) {
            throw new java.lang.IllegalStateException("Needs to be invoked on the main thread.");
        }
    }
}
