package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x51 extends vb1<x51> {
    @DexIgnore
    public static volatile x51[] g;
    @DexIgnore
    public String c; // = null;
    @DexIgnore
    public Boolean d; // = null;
    @DexIgnore
    public Boolean e; // = null;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public x51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static x51[] e() {
        if (g == null) {
            synchronized (zb1.b) {
                if (g == null) {
                    g = new x51[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        String str = this.c;
        if (str != null) {
            ub1.a(1, str);
        }
        Boolean bool = this.d;
        if (bool != null) {
            ub1.a(2, bool.booleanValue());
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            ub1.a(3, bool2.booleanValue());
        }
        Integer num = this.f;
        if (num != null) {
            ub1.b(4, num.intValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof x51)) {
            return false;
        }
        x51 x51 = (x51) obj;
        String str = this.c;
        if (str == null) {
            if (x51.c != null) {
                return false;
            }
        } else if (!str.equals(x51.c)) {
            return false;
        }
        Boolean bool = this.d;
        if (bool == null) {
            if (x51.d != null) {
                return false;
            }
        } else if (!bool.equals(x51.d)) {
            return false;
        }
        Boolean bool2 = this.e;
        if (bool2 == null) {
            if (x51.e != null) {
                return false;
            }
        } else if (!bool2.equals(x51.e)) {
            return false;
        }
        Integer num = this.f;
        if (num == null) {
            if (x51.f != null) {
                return false;
            }
        } else if (!num.equals(x51.f)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(x51.b);
        }
        xb1 xb12 = x51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (x51.class.getName().hashCode() + 527) * 31;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        Boolean bool = this.d;
        int hashCode3 = (hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.e;
        int hashCode4 = (hashCode3 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        Integer num = this.f;
        int hashCode5 = (hashCode4 + (num == null ? 0 : num.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        String str = this.c;
        if (str != null) {
            a += ub1.b(1, str);
        }
        Boolean bool = this.d;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(2) + 1;
        }
        Boolean bool2 = this.e;
        if (bool2 != null) {
            bool2.booleanValue();
            a += ub1.c(3) + 1;
        }
        Integer num = this.f;
        return num != null ? a + ub1.c(4, num.intValue()) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                this.c = tb1.b();
            } else if (c2 == 16) {
                this.d = Boolean.valueOf(tb1.d());
            } else if (c2 == 24) {
                this.e = Boolean.valueOf(tb1.d());
            } else if (c2 == 32) {
                this.f = Integer.valueOf(tb1.e());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
