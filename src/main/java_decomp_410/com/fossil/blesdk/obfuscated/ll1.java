package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ll1 implements Parcelable.Creator<kl1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 2:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 3:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 4:
                    l = SafeParcelReader.t(parcel2, a);
                    break;
                case 5:
                    f = SafeParcelReader.o(parcel2, a);
                    break;
                case 6:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 7:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 8:
                    d = SafeParcelReader.m(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new kl1(i, str, j, l, f, str2, str3, d);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new kl1[i];
    }
}
