package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a1 */
public class C1369a1 implements com.fossil.blesdk.obfuscated.C2001i7 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f3278a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f3279b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f3280c;

    @DexIgnore
    /* renamed from: d */
    public java.lang.CharSequence f3281d;

    @DexIgnore
    /* renamed from: e */
    public java.lang.CharSequence f3282e;

    @DexIgnore
    /* renamed from: f */
    public android.content.Intent f3283f;

    @DexIgnore
    /* renamed from: g */
    public char f3284g;

    @DexIgnore
    /* renamed from: h */
    public int f3285h; // = 4096;

    @DexIgnore
    /* renamed from: i */
    public char f3286i;

    @DexIgnore
    /* renamed from: j */
    public int f3287j; // = 4096;

    @DexIgnore
    /* renamed from: k */
    public android.graphics.drawable.Drawable f3288k;

    @DexIgnore
    /* renamed from: l */
    public android.content.Context f3289l;

    @DexIgnore
    /* renamed from: m */
    public java.lang.CharSequence f3290m;

    @DexIgnore
    /* renamed from: n */
    public java.lang.CharSequence f3291n;

    @DexIgnore
    /* renamed from: o */
    public android.content.res.ColorStateList f3292o; // = null;

    @DexIgnore
    /* renamed from: p */
    public android.graphics.PorterDuff.Mode f3293p; // = null;

    @DexIgnore
    /* renamed from: q */
    public boolean f3294q; // = false;

    @DexIgnore
    /* renamed from: r */
    public boolean f3295r; // = false;

    @DexIgnore
    /* renamed from: s */
    public int f3296s; // = 16;

    @DexIgnore
    public C1369a1(android.content.Context context, int i, int i2, int i3, int i4, java.lang.CharSequence charSequence) {
        this.f3289l = context;
        this.f3278a = i2;
        this.f3279b = i;
        this.f3280c = i4;
        this.f3281d = charSequence;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2001i7 mo8357a(com.fossil.blesdk.obfuscated.C2382m8 m8Var) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2382m8 mo8358a() {
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo8359b() {
        if (this.f3288k == null) {
            return;
        }
        if (this.f3294q || this.f3295r) {
            this.f3288k = com.fossil.blesdk.obfuscated.C1538c7.m5314i(this.f3288k);
            this.f3288k = this.f3288k.mutate();
            if (this.f3294q) {
                com.fossil.blesdk.obfuscated.C1538c7.m5299a(this.f3288k, this.f3292o);
            }
            if (this.f3295r) {
                com.fossil.blesdk.obfuscated.C1538c7.m5302a(this.f3288k, this.f3293p);
            }
        }
    }

    @DexIgnore
    public boolean collapseActionView() {
        return false;
    }

    @DexIgnore
    public boolean expandActionView() {
        return false;
    }

    @DexIgnore
    public android.view.ActionProvider getActionProvider() {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public android.view.View getActionView() {
        return null;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return this.f3287j;
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.f3286i;
    }

    @DexIgnore
    public java.lang.CharSequence getContentDescription() {
        return this.f3290m;
    }

    @DexIgnore
    public int getGroupId() {
        return this.f3279b;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getIcon() {
        return this.f3288k;
    }

    @DexIgnore
    public android.content.res.ColorStateList getIconTintList() {
        return this.f3292o;
    }

    @DexIgnore
    public android.graphics.PorterDuff.Mode getIconTintMode() {
        return this.f3293p;
    }

    @DexIgnore
    public android.content.Intent getIntent() {
        return this.f3283f;
    }

    @DexIgnore
    public int getItemId() {
        return this.f3278a;
    }

    @DexIgnore
    public android.view.ContextMenu.ContextMenuInfo getMenuInfo() {
        return null;
    }

    @DexIgnore
    public int getNumericModifiers() {
        return this.f3285h;
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.f3284g;
    }

    @DexIgnore
    public int getOrder() {
        return this.f3280c;
    }

    @DexIgnore
    public android.view.SubMenu getSubMenu() {
        return null;
    }

    @DexIgnore
    public java.lang.CharSequence getTitle() {
        return this.f3281d;
    }

    @DexIgnore
    public java.lang.CharSequence getTitleCondensed() {
        java.lang.CharSequence charSequence = this.f3282e;
        return charSequence != null ? charSequence : this.f3281d;
    }

    @DexIgnore
    public java.lang.CharSequence getTooltipText() {
        return this.f3291n;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return false;
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return false;
    }

    @DexIgnore
    public boolean isCheckable() {
        return (this.f3296s & 1) != 0;
    }

    @DexIgnore
    public boolean isChecked() {
        return (this.f3296s & 2) != 0;
    }

    @DexIgnore
    public boolean isEnabled() {
        return (this.f3296s & 16) != 0;
    }

    @DexIgnore
    public boolean isVisible() {
        return (this.f3296s & 8) == 0;
    }

    @DexIgnore
    public android.view.MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c) {
        this.f3286i = java.lang.Character.toLowerCase(c);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setCheckable(boolean z) {
        this.f3296s = z | (this.f3296s & true) ? 1 : 0;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setChecked(boolean z) {
        this.f3296s = (z ? 2 : 0) | (this.f3296s & -3);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setEnabled(boolean z) {
        this.f3296s = (z ? 16 : 0) | (this.f3296s & -17);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIcon(android.graphics.drawable.Drawable drawable) {
        this.f3288k = drawable;
        mo8359b();
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintList(android.content.res.ColorStateList colorStateList) {
        this.f3292o = colorStateList;
        this.f3294q = true;
        mo8359b();
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintMode(android.graphics.PorterDuff.Mode mode) {
        this.f3293p = mode;
        this.f3295r = true;
        mo8359b();
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIntent(android.content.Intent intent) {
        this.f3283f = intent;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c) {
        this.f3284g = c;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2) {
        this.f3284g = c;
        this.f3286i = java.lang.Character.toLowerCase(c2);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
    }

    @DexIgnore
    public android.view.MenuItem setTitle(java.lang.CharSequence charSequence) {
        this.f3281d = charSequence;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitleCondensed(java.lang.CharSequence charSequence) {
        this.f3282e = charSequence;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setVisible(boolean z) {
        int i = 8;
        int i2 = this.f3296s & 8;
        if (z) {
            i = 0;
        }
        this.f3296s = i2 | i;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c, int i) {
        this.f3286i = java.lang.Character.toLowerCase(c);
        this.f3287j = android.view.KeyEvent.normalizeMetaState(i);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setContentDescription(java.lang.CharSequence charSequence) {
        this.f3290m = charSequence;
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c, int i) {
        this.f3284g = c;
        this.f3285h = android.view.KeyEvent.normalizeMetaState(i);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setShowAsActionFlags(int i) {
        setShowAsAction(i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitle(int i) {
        this.f3281d = this.f3289l.getResources().getString(i);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setTooltipText(java.lang.CharSequence charSequence) {
        this.f3291n = charSequence;
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setActionView(android.view.View view) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public android.view.MenuItem setIcon(int i) {
        this.f3288k = com.fossil.blesdk.obfuscated.C2185k6.m9358c(this.f3289l, i);
        mo8359b();
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2, int i, int i2) {
        this.f3284g = c;
        this.f3285h = android.view.KeyEvent.normalizeMetaState(i);
        this.f3286i = java.lang.Character.toLowerCase(c2);
        this.f3287j = android.view.KeyEvent.normalizeMetaState(i2);
        return this;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2001i7 setActionView(int i) {
        throw new java.lang.UnsupportedOperationException();
    }
}
