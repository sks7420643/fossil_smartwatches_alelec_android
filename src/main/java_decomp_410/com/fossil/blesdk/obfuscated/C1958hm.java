package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hm */
public interface C1958hm<TTaskResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(com.fossil.blesdk.obfuscated.C2037im<TTaskResult> imVar) throws java.lang.Exception;
}
