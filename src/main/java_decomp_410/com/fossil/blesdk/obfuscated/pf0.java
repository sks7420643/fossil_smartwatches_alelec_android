package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pf0 implements mg0 {
    @DexIgnore
    public /* final */ ng0 a;
    @DexIgnore
    public boolean b; // = false;

    @DexIgnore
    public pf0(ng0 ng0) {
        this.a = ng0;
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t) {
        try {
            this.a.r.y.a(t);
            eg0 eg0 = this.a.r;
            de0.f fVar = eg0.p.get(t.i());
            bk0.a(fVar, (Object) "Appropriate Api was not requested.");
            if (fVar.c() || !this.a.k.containsKey(t.i())) {
                boolean z = fVar instanceof gk0;
                de0.b bVar = fVar;
                if (z) {
                    bVar = ((gk0) fVar).G();
                }
                t.b(bVar);
                return t;
            }
            t.c(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.a.a((og0) new qf0(this, this));
        }
    }

    @DexIgnore
    public final void a(ud0 ud0, de0<?> de0, boolean z) {
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t) {
        a(t);
        return t;
    }

    @DexIgnore
    public final void c() {
    }

    @DexIgnore
    public final void d() {
        if (this.b) {
            this.b = false;
            this.a.r.y.a();
            a();
        }
    }

    @DexIgnore
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    public final void f(int i) {
        this.a.a((ud0) null);
        this.a.s.a(i, this.b);
    }

    @DexIgnore
    public final void b() {
        if (this.b) {
            this.b = false;
            this.a.a((og0) new rf0(this, this));
        }
    }

    @DexIgnore
    public final boolean a() {
        if (this.b) {
            return false;
        }
        if (this.a.r.p()) {
            this.b = true;
            for (mh0 a2 : this.a.r.x) {
                a2.a();
            }
            return false;
        }
        this.a.a((ud0) null);
        return true;
    }
}
