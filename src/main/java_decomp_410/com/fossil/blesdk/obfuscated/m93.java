package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m93 implements Factory<p93> {
    @DexIgnore
    public static p93 a(k93 k93) {
        p93 b = k93.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
