package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yt */
public class C3383yt {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1570co f11388a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Handler f11389b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C3383yt.C3385b> f11390c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3299xn f11391d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f11392e;

    @DexIgnore
    /* renamed from: f */
    public boolean f11393f;

    @DexIgnore
    /* renamed from: g */
    public boolean f11394g;

    @DexIgnore
    /* renamed from: h */
    public boolean f11395h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C3230wn<android.graphics.Bitmap> f11396i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C3383yt.C3384a f11397j;

    @DexIgnore
    /* renamed from: k */
    public boolean f11398k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C3383yt.C3384a f11399l;

    @DexIgnore
    /* renamed from: m */
    public android.graphics.Bitmap f11400m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C3383yt.C3384a f11401n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C3383yt.C3387d f11402o;

    @DexIgnore
    /* renamed from: p */
    public int f11403p;

    @DexIgnore
    /* renamed from: q */
    public int f11404q;

    @DexIgnore
    /* renamed from: r */
    public int f11405r;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yt$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yt$a */
    public static class C3384a extends com.fossil.blesdk.obfuscated.C3245wv<android.graphics.Bitmap> {

        @DexIgnore
        /* renamed from: h */
        public /* final */ android.os.Handler f11406h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ int f11407i;

        @DexIgnore
        /* renamed from: j */
        public /* final */ long f11408j;

        @DexIgnore
        /* renamed from: k */
        public android.graphics.Bitmap f11409k;

        @DexIgnore
        public C3384a(android.os.Handler handler, int i, long j) {
            this.f11406h = handler;
            this.f11407i = i;
            this.f11408j = j;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo9318c(android.graphics.drawable.Drawable drawable) {
            this.f11409k = null;
        }

        @DexIgnore
        /* renamed from: e */
        public android.graphics.Bitmap mo18320e() {
            return this.f11409k;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9315a(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.C1750ew<? super android.graphics.Bitmap> ewVar) {
            this.f11409k = bitmap;
            this.f11406h.sendMessageAtTime(this.f11406h.obtainMessage(1, this), this.f11408j);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.yt$b */
    public interface C3385b {
        @DexIgnore
        /* renamed from: a */
        void mo16856a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yt$c")
    /* renamed from: com.fossil.blesdk.obfuscated.yt$c */
    public class C3386c implements android.os.Handler.Callback {
        @DexIgnore
        public C3386c() {
        }

        @DexIgnore
        public boolean handleMessage(android.os.Message message) {
            int i = message.what;
            if (i == 1) {
                com.fossil.blesdk.obfuscated.C3383yt.this.mo18304a((com.fossil.blesdk.obfuscated.C3383yt.C3384a) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                com.fossil.blesdk.obfuscated.C3383yt.this.f11391d.mo17817a((com.fossil.blesdk.obfuscated.C1517bw<?>) (com.fossil.blesdk.obfuscated.C3383yt.C3384a) message.obj);
                return false;
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.yt$d */
    public interface C3387d {
        @DexIgnore
        /* renamed from: a */
        void mo18322a();
    }

    @DexIgnore
    public C3383yt(com.fossil.blesdk.obfuscated.C2815rn rnVar, com.fossil.blesdk.obfuscated.C1570co coVar, int i, int i2, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar, android.graphics.Bitmap bitmap) {
        this(rnVar.mo15645c(), com.fossil.blesdk.obfuscated.C2815rn.m13281d(rnVar.mo15647e()), coVar, (android.os.Handler) null, m17078a(com.fossil.blesdk.obfuscated.C2815rn.m13281d(rnVar.mo15647e()), i, i2), ooVar, bitmap);
    }

    @DexIgnore
    /* renamed from: n */
    public static com.fossil.blesdk.obfuscated.C2143jo m17079n() {
        return new com.fossil.blesdk.obfuscated.C2166jw(java.lang.Double.valueOf(java.lang.Math.random()));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [java.lang.Object, com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public void mo18303a(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3, android.graphics.Bitmap bitmap) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(r3);
        com.fossil.blesdk.obfuscated.C2581oo ooVar = r3;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(bitmap);
        this.f11400m = bitmap;
        this.f11396i = this.f11396i.mo13438a(new com.fossil.blesdk.obfuscated.C2842rv().mo13439a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3));
        this.f11403p = com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap);
        this.f11404q = bitmap.getWidth();
        this.f11405r = bitmap.getHeight();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18307b(com.fossil.blesdk.obfuscated.C3383yt.C3385b bVar) {
        this.f11390c.remove(bVar);
        if (this.f11390c.isEmpty()) {
            mo18318m();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public android.graphics.Bitmap mo18308c() {
        com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar = this.f11397j;
        return aVar != null ? aVar.mo18320e() : this.f11400m;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo18309d() {
        com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar = this.f11397j;
        if (aVar != null) {
            return aVar.f11407i;
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: e */
    public android.graphics.Bitmap mo18310e() {
        return this.f11400m;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo18311f() {
        return this.f11388a.mo9592c();
    }

    @DexIgnore
    /* renamed from: g */
    public int mo18312g() {
        return this.f11405r;
    }

    @DexIgnore
    /* renamed from: h */
    public int mo18313h() {
        return this.f11388a.mo9598h() + this.f11403p;
    }

    @DexIgnore
    /* renamed from: i */
    public int mo18314i() {
        return this.f11404q;
    }

    @DexIgnore
    /* renamed from: j */
    public final void mo18315j() {
        if (this.f11393f && !this.f11394g) {
            if (this.f11395h) {
                com.fossil.blesdk.obfuscated.C2992tw.m14461a(this.f11401n == null, "Pending target must be null when starting from the first frame");
                this.f11388a.mo9596f();
                this.f11395h = false;
            }
            com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar = this.f11401n;
            if (aVar != null) {
                this.f11401n = null;
                mo18304a(aVar);
                return;
            }
            this.f11394g = true;
            long uptimeMillis = android.os.SystemClock.uptimeMillis() + ((long) this.f11388a.mo9594d());
            this.f11388a.mo9591b();
            this.f11399l = new com.fossil.blesdk.obfuscated.C3383yt.C3384a(this.f11389b, this.f11388a.mo9597g(), uptimeMillis);
            this.f11396i.mo13438a((com.fossil.blesdk.obfuscated.C2354lv) com.fossil.blesdk.obfuscated.C2842rv.m13422b(m17079n())).mo17494a((java.lang.Object) this.f11388a).mo17484a(this.f11399l);
        }
    }

    @DexIgnore
    /* renamed from: k */
    public final void mo18316k() {
        android.graphics.Bitmap bitmap = this.f11400m;
        if (bitmap != null) {
            this.f11392e.mo12446a(bitmap);
            this.f11400m = null;
        }
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo18317l() {
        if (!this.f11393f) {
            this.f11393f = true;
            this.f11398k = false;
            mo18315j();
        }
    }

    @DexIgnore
    /* renamed from: m */
    public final void mo18318m() {
        this.f11393f = false;
    }

    @DexIgnore
    /* renamed from: b */
    public java.nio.ByteBuffer mo18306b() {
        return this.f11388a.mo9595e().asReadOnlyBuffer();
    }

    @DexIgnore
    public C3383yt(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C3299xn xnVar, com.fossil.blesdk.obfuscated.C1570co coVar, android.os.Handler handler, com.fossil.blesdk.obfuscated.C3230wn<android.graphics.Bitmap> wnVar, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar, android.graphics.Bitmap bitmap) {
        this.f11390c = new java.util.ArrayList();
        this.f11391d = xnVar;
        handler = handler == null ? new android.os.Handler(android.os.Looper.getMainLooper(), new com.fossil.blesdk.obfuscated.C3383yt.C3386c()) : handler;
        this.f11392e = jqVar;
        this.f11389b = handler;
        this.f11396i = wnVar;
        this.f11388a = coVar;
        mo18303a(ooVar, bitmap);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18305a(com.fossil.blesdk.obfuscated.C3383yt.C3385b bVar) {
        if (this.f11398k) {
            throw new java.lang.IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.f11390c.contains(bVar)) {
            boolean isEmpty = this.f11390c.isEmpty();
            this.f11390c.add(bVar);
            if (isEmpty) {
                mo18317l();
            }
        } else {
            throw new java.lang.IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18302a() {
        this.f11390c.clear();
        mo18316k();
        mo18318m();
        com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar = this.f11397j;
        if (aVar != null) {
            this.f11391d.mo17817a((com.fossil.blesdk.obfuscated.C1517bw<?>) aVar);
            this.f11397j = null;
        }
        com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar2 = this.f11399l;
        if (aVar2 != null) {
            this.f11391d.mo17817a((com.fossil.blesdk.obfuscated.C1517bw<?>) aVar2);
            this.f11399l = null;
        }
        com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar3 = this.f11401n;
        if (aVar3 != null) {
            this.f11391d.mo17817a((com.fossil.blesdk.obfuscated.C1517bw<?>) aVar3);
            this.f11401n = null;
        }
        this.f11388a.clear();
        this.f11398k = true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18304a(com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar) {
        com.fossil.blesdk.obfuscated.C3383yt.C3387d dVar = this.f11402o;
        if (dVar != null) {
            dVar.mo18322a();
        }
        this.f11394g = false;
        if (this.f11398k) {
            this.f11389b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f11393f) {
            this.f11401n = aVar;
        } else {
            if (aVar.mo18320e() != null) {
                mo18316k();
                com.fossil.blesdk.obfuscated.C3383yt.C3384a aVar2 = this.f11397j;
                this.f11397j = aVar;
                for (int size = this.f11390c.size() - 1; size >= 0; size--) {
                    this.f11390c.get(size).mo16856a();
                }
                if (aVar2 != null) {
                    this.f11389b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            mo18315j();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3230wn<android.graphics.Bitmap> m17078a(com.fossil.blesdk.obfuscated.C3299xn xnVar, int i, int i2) {
        return xnVar.mo17823e().mo13438a(((com.fossil.blesdk.obfuscated.C2842rv) ((com.fossil.blesdk.obfuscated.C2842rv) com.fossil.blesdk.obfuscated.C2842rv.m13423b(com.fossil.blesdk.obfuscated.C2663pp.f8415a).mo13449b(true)).mo13444a(true)).mo13431a(i, i2));
    }
}
