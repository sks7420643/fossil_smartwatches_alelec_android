package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vz2 implements Factory<NotificationDialLandingPresenter> {
    @DexIgnore
    public static NotificationDialLandingPresenter a(qz2 qz2, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        return new NotificationDialLandingPresenter(qz2, notificationsLoader, loaderManager);
    }
}
