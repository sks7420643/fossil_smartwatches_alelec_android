package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u0 */
public class C2999u0 extends android.content.ContextWrapper {

    @DexIgnore
    /* renamed from: a */
    public int f9800a;

    @DexIgnore
    /* renamed from: b */
    public android.content.res.Resources.Theme f9801b;

    @DexIgnore
    /* renamed from: c */
    public android.view.LayoutInflater f9802c;

    @DexIgnore
    /* renamed from: d */
    public android.content.res.Configuration f9803d;

    @DexIgnore
    /* renamed from: e */
    public android.content.res.Resources f9804e;

    @DexIgnore
    public C2999u0() {
        super((android.content.Context) null);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.content.res.Resources mo16622a() {
        if (this.f9804e == null) {
            android.content.res.Configuration configuration = this.f9803d;
            if (configuration == null) {
                this.f9804e = super.getResources();
            } else if (android.os.Build.VERSION.SDK_INT >= 17) {
                this.f9804e = createConfigurationContext(configuration).getResources();
            }
        }
        return this.f9804e;
    }

    @DexIgnore
    public void attachBaseContext(android.content.Context context) {
        super.attachBaseContext(context);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo16625b() {
        return this.f9800a;
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo16626c() {
        boolean z = this.f9801b == null;
        if (z) {
            this.f9801b = getResources().newTheme();
            android.content.res.Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f9801b.setTo(theme);
            }
        }
        mo16623a(this.f9801b, this.f9800a, z);
    }

    @DexIgnore
    public android.content.res.AssetManager getAssets() {
        return getResources().getAssets();
    }

    @DexIgnore
    public android.content.res.Resources getResources() {
        return mo16622a();
    }

    @DexIgnore
    public java.lang.Object getSystemService(java.lang.String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.f9802c == null) {
            this.f9802c = android.view.LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.f9802c;
    }

    @DexIgnore
    public android.content.res.Resources.Theme getTheme() {
        android.content.res.Resources.Theme theme = this.f9801b;
        if (theme != null) {
            return theme;
        }
        if (this.f9800a == 0) {
            this.f9800a = com.fossil.blesdk.obfuscated.C3398z.Theme_AppCompat_Light;
        }
        mo16626c();
        return this.f9801b;
    }

    @DexIgnore
    public void setTheme(int i) {
        if (this.f9800a != i) {
            this.f9800a = i;
            mo16626c();
        }
    }

    @DexIgnore
    public C2999u0(android.content.Context context, int i) {
        super(context);
        this.f9800a = i;
    }

    @DexIgnore
    public C2999u0(android.content.Context context, android.content.res.Resources.Theme theme) {
        super(context);
        this.f9801b = theme;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16623a(android.content.res.Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }
}
