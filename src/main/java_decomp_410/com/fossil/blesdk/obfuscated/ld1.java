package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ld1 implements Parcelable.Creator<kd1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        boolean z = true;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = Integer.MAX_VALUE;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                z = SafeParcelReader.i(parcel2, a);
            } else if (a2 == 2) {
                j = SafeParcelReader.s(parcel2, a);
            } else if (a2 == 3) {
                f = SafeParcelReader.n(parcel2, a);
            } else if (a2 == 4) {
                j2 = SafeParcelReader.s(parcel2, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel2, a);
            } else {
                i = SafeParcelReader.q(parcel2, a);
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new kd1(z, j, f, j2, i);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new kd1[i];
    }
}
