package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eu */
public final class C1748eu implements com.fossil.blesdk.obfuscated.C1906gu<android.graphics.drawable.Drawable, byte[]> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f4953a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1906gu<android.graphics.Bitmap, byte[]> f4954b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1906gu<com.fossil.blesdk.obfuscated.C3060ut, byte[]> f4955c;

    @DexIgnore
    public C1748eu(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1906gu<android.graphics.Bitmap, byte[]> guVar, com.fossil.blesdk.obfuscated.C1906gu<com.fossil.blesdk.obfuscated.C3060ut, byte[]> guVar2) {
        this.f4953a = jqVar;
        this.f4954b = guVar;
        this.f4955c = guVar2;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1438aq<com.fossil.blesdk.obfuscated.C3060ut> m6601a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> aqVar) {
        return aqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<byte[]> mo9632a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> aqVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.graphics.drawable.Drawable drawable = aqVar.get();
        if (drawable instanceof android.graphics.drawable.BitmapDrawable) {
            return this.f4954b.mo9632a(com.fossil.blesdk.obfuscated.C2675ps.m12395a(((android.graphics.drawable.BitmapDrawable) drawable).getBitmap(), this.f4953a), loVar);
        }
        if (!(drawable instanceof com.fossil.blesdk.obfuscated.C3060ut)) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C1906gu<com.fossil.blesdk.obfuscated.C3060ut, byte[]> guVar = this.f4955c;
        m6601a(aqVar);
        return guVar.mo9632a(aqVar, loVar);
    }
}
