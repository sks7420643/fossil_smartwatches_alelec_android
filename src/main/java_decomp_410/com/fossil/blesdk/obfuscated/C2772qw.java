package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qw */
public class C2772qw<T, Y> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<T, Y> f8801a; // = new java.util.LinkedHashMap(100, 0.75f, true);

    @DexIgnore
    /* renamed from: b */
    public long f8802b;

    @DexIgnore
    /* renamed from: c */
    public long f8803c;

    @DexIgnore
    public C2772qw(long j) {
        this.f8802b = j;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized Y mo15388a(T t) {
        return this.f8801a.get(t);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15391a(T t, Y y) {
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15392b(Y y) {
        return 1;
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized Y mo15393b(T t, Y y) {
        long b = (long) mo15392b(y);
        if (b >= this.f8802b) {
            mo15391a(t, y);
            return null;
        }
        if (y != null) {
            this.f8803c += b;
        }
        Y put = this.f8801a.put(t, y);
        if (put != null) {
            this.f8803c -= (long) mo15392b(put);
            if (!put.equals(y)) {
                mo15391a(t, put);
            }
        }
        mo15394b();
        return put;
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized long mo15395c() {
        return this.f8802b;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15389a() {
        mo15390a(0);
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized Y mo15396c(T t) {
        Y remove;
        remove = this.f8801a.remove(t);
        if (remove != null) {
            this.f8803c -= (long) mo15392b(remove);
        }
        return remove;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo15390a(long j) {
        while (this.f8803c > j) {
            java.util.Iterator<java.util.Map.Entry<T, Y>> it = this.f8801a.entrySet().iterator();
            java.util.Map.Entry next = it.next();
            java.lang.Object value = next.getValue();
            this.f8803c -= (long) mo15392b(value);
            java.lang.Object key = next.getKey();
            it.remove();
            mo15391a(key, value);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo15394b() {
        mo15390a(this.f8802b);
    }
}
