package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ng0 implements bh0, hi0 {
    @DexIgnore
    public /* final */ Lock e;
    @DexIgnore
    public /* final */ Condition f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ yd0 h;
    @DexIgnore
    public /* final */ pg0 i;
    @DexIgnore
    public /* final */ Map<de0.c<?>, de0.f> j;
    @DexIgnore
    public /* final */ Map<de0.c<?>, ud0> k; // = new HashMap();
    @DexIgnore
    public /* final */ kj0 l;
    @DexIgnore
    public /* final */ Map<de0<?>, Boolean> m;
    @DexIgnore
    public /* final */ de0.a<? extends ln1, vm1> n;
    @DexIgnore
    public volatile mg0 o;
    @DexIgnore
    public ud0 p; // = null;
    @DexIgnore
    public int q;
    @DexIgnore
    public /* final */ eg0 r;
    @DexIgnore
    public /* final */ ch0 s;

    @DexIgnore
    public ng0(Context context, eg0 eg0, Lock lock, Looper looper, yd0 yd0, Map<de0.c<?>, de0.f> map, kj0 kj0, Map<de0<?>, Boolean> map2, de0.a<? extends ln1, vm1> aVar, ArrayList<gi0> arrayList, ch0 ch0) {
        this.g = context;
        this.e = lock;
        this.h = yd0;
        this.j = map;
        this.l = kj0;
        this.m = map2;
        this.n = aVar;
        this.r = eg0;
        this.s = ch0;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            gi0 gi0 = arrayList.get(i2);
            i2++;
            gi0.a((hi0) this);
        }
        this.i = new pg0(this, looper);
        this.f = lock.newCondition();
        this.o = new dg0(this);
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t) {
        t.g();
        return this.o.a(t);
    }

    @DexIgnore
    public final boolean a(cf0 cf0) {
        return false;
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t) {
        t.g();
        return this.o.b(t);
    }

    @DexIgnore
    public final boolean c() {
        return this.o instanceof pf0;
    }

    @DexIgnore
    public final void d() {
        if (c()) {
            ((pf0) this.o).d();
        }
    }

    @DexIgnore
    public final void e() {
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.e.lock();
        try {
            this.o.e(bundle);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final ud0 f() {
        b();
        while (g()) {
            try {
                this.f.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ud0(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return ud0.i;
        }
        ud0 ud0 = this.p;
        if (ud0 != null) {
            return ud0;
        }
        return new ud0(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        return this.o instanceof sf0;
    }

    @DexIgnore
    public final void h() {
        this.e.lock();
        try {
            this.o = new sf0(this, this.l, this.m, this.h, this.n, this.e, this.g);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void i() {
        this.e.lock();
        try {
            this.r.o();
            this.o = new pf0(this);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a() {
        if (this.o.a()) {
            this.k.clear();
        }
    }

    @DexIgnore
    public final void b() {
        this.o.b();
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        this.e.lock();
        try {
            this.p = ud0;
            this.o = new dg0(this);
            this.o.c();
            this.f.signalAll();
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void f(int i2) {
        this.e.lock();
        try {
            this.o.f(i2);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a(ud0 ud0, de0<?> de0, boolean z) {
        this.e.lock();
        try {
            this.o.a(ud0, de0, z);
        } finally {
            this.e.unlock();
        }
    }

    @DexIgnore
    public final void a(og0 og0) {
        this.i.sendMessage(this.i.obtainMessage(1, og0));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.i.sendMessage(this.i.obtainMessage(2, runtimeException));
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append(str).append("mState=").println(this.o);
        for (de0 next : this.m.keySet()) {
            printWriter.append(str).append(next.b()).println(":");
            this.j.get(next.a()).a(concat, fileDescriptor, printWriter, strArr);
        }
    }
}
