package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class op1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<op1> CREATOR; // = new pp1();
    @DexIgnore
    public /* final */ qp1 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore
    public op1(qp1 qp1, int i, int i2, int i3) {
        this.e = qp1;
        this.f = i;
        this.g = i2;
        this.h = i3;
    }

    @DexIgnore
    public final void a(zo1 zo1) {
        int i = this.f;
        if (i == 1) {
            zo1.a(this.e);
        } else if (i == 2) {
            zo1.a(this.e, this.g, this.h);
        } else if (i == 3) {
            zo1.b(this.e, this.g, this.h);
        } else if (i != 4) {
            StringBuilder sb = new StringBuilder(25);
            sb.append("Unknown type: ");
            sb.append(i);
            Log.w("ChannelEventParcelable", sb.toString());
        } else {
            zo1.c(this.e, this.g, this.h);
        }
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.e);
        int i = this.f;
        String num = i != 1 ? i != 2 ? i != 3 ? i != 4 ? Integer.toString(i) : "OUTPUT_CLOSED" : "INPUT_CLOSED" : "CHANNEL_CLOSED" : "CHANNEL_OPENED";
        int i2 = this.g;
        String num2 = i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? Integer.toString(i2) : "CLOSE_REASON_LOCAL_CLOSE" : "CLOSE_REASON_REMOTE_CLOSE" : "CLOSE_REASON_DISCONNECTED" : "CLOSE_REASON_NORMAL";
        int i3 = this.h;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81 + String.valueOf(num).length() + String.valueOf(num2).length());
        sb.append("ChannelEventParcelable[, channel=");
        sb.append(valueOf);
        sb.append(", type=");
        sb.append(num);
        sb.append(", closeReason=");
        sb.append(num2);
        sb.append(", appErrorCode=");
        sb.append(i3);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, (Parcelable) this.e, i, false);
        kk0.a(parcel, 3, this.f);
        kk0.a(parcel, 4, this.g);
        kk0.a(parcel, 5, this.h);
        kk0.a(parcel, a);
    }
}
