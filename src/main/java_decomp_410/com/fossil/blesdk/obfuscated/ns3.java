package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.facebook.places.model.PlaceFields;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ns3 {
    @DexIgnore
    public static /* final */ a a; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final boolean a(Context context) {
            kd4.b(context, "context");
            return a(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }

        @DexIgnore
        public final boolean b(Context context) {
            kd4.b(context, "context");
            return a(context, "android.permission.ACCESS_FINE_LOCATION");
        }

        @DexIgnore
        public final boolean c() {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return defaultAdapter != null && defaultAdapter.isEnabled();
        }

        @DexIgnore
        public final boolean d() {
            LocationManager locationManager = (LocationManager) PortfolioApp.W.c().getSystemService(PlaceFields.LOCATION);
            if (locationManager == null) {
                return false;
            }
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);
            if (!(gp4.a(bestProvider) || kd4.a((Object) "passive", (Object) bestProvider) || (qf4.b(LocationUtils.HUAWEI_MODEL, Build.MANUFACTURER, true) && qf4.b(LocationUtils.HUAWEI_LOCAL_PROVIDER, bestProvider, true)))) {
                return true;
            }
            try {
                if (Settings.Secure.getInt(PortfolioApp.W.c().getContentResolver(), "location_mode") != 0) {
                    return true;
                }
                return false;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }

        @DexIgnore
        public final boolean e() {
            PortfolioApp c = PortfolioApp.W.c();
            String string = Settings.Secure.getString(c.getContentResolver(), "enabled_notification_listeners");
            String str = c.getPackageName() + ZendeskConfig.SLASH + FossilNotificationListenerService.class.getCanonicalName();
            FLogger.INSTANCE.getLocal().d("PermissionUtils", "isNotificationListenerEnabled - notificationServicePath=" + str + ", enabledNotificationListeners=" + string);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            kd4.a((Object) string, "enabledNotificationListeners");
            return StringsKt__StringsKt.a((CharSequence) string, (CharSequence) str, false, 2, (Object) null);
        }

        @DexIgnore
        public final boolean f(Context context) {
            return a(context, "android.permission.READ_PHONE_STATE");
        }

        @DexIgnore
        public final boolean g(Context context) {
            return a(context, "android.permission.READ_SMS");
        }

        @DexIgnore
        public final boolean h(Context context) {
            return a(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final boolean a(Context context, String... strArr) {
            kd4.b(context, "context");
            kd4.b(strArr, "perms");
            return pq4.a(context, (String[]) Arrays.copyOf(strArr, strArr.length));
        }

        @DexIgnore
        public final HashMap<String, Boolean> b() {
            PortfolioApp c = PortfolioApp.W.c();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put(InAppPermission.ACCESS_BACKGROUND_LOCATION, Boolean.valueOf(a((Context) c)));
            linkedHashMap.put(InAppPermission.ACCESS_FINE_LOCATION, Boolean.valueOf(b(c)));
            linkedHashMap.put(InAppPermission.LOCATION_SERVICE, Boolean.valueOf(d()));
            linkedHashMap.put(InAppPermission.BLUETOOTH, Boolean.valueOf(c()));
            linkedHashMap.put(InAppPermission.READ_CONTACTS, Boolean.valueOf(e(c)));
            linkedHashMap.put(InAppPermission.READ_PHONE_STATE, Boolean.valueOf(f(c)));
            linkedHashMap.put(InAppPermission.READ_SMS, Boolean.valueOf(g(c)));
            linkedHashMap.put(InAppPermission.NOTIFICATION_ACCESS, Boolean.valueOf(e()));
            linkedHashMap.put(InAppPermission.CAMERA, Boolean.valueOf(c(c)));
            linkedHashMap.put(InAppPermission.WRITE_EXTERNAL_STORAGE, Boolean.valueOf(h(c)));
            return linkedHashMap;
        }

        @DexIgnore
        public final String[] a() {
            HashMap b = b();
            Set keySet = b.keySet();
            kd4.a((Object) keySet, "permissionList.keys");
            ArrayList arrayList = new ArrayList();
            for (Object next : keySet) {
                String str = (String) next;
                kd4.a((Object) str, "it");
                if (((Boolean) rb4.b(b, str)).booleanValue()) {
                    arrayList.add(next);
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final boolean c(Context context) {
            return a(context, "android.permission.CAMERA");
        }

        @DexIgnore
        public final boolean c(Activity activity, int i) {
            kd4.b(activity, Constants.ACTIVITY);
            if (h(activity)) {
                return true;
            }
            v5.a(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
            return false;
        }

        @DexIgnore
        public final boolean d(Context context) {
            kd4.b(context, "context");
            return a(context, "android.permission.READ_CALL_LOG");
        }

        @DexIgnore
        public final boolean e(Context context) {
            kd4.b(context, "context");
            return a(context, "android.permission.READ_CONTACTS");
        }

        @DexIgnore
        public final boolean a(Activity activity, int i) {
            kd4.b(activity, Constants.ACTIVITY);
            if (a((Context) activity)) {
                return true;
            }
            v5.a(activity, new String[]{"android.permission.ACCESS_BACKGROUND_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity) {
            kd4.b(fragmentActivity, Constants.ACTIVITY);
            fragmentActivity.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        }

        @DexIgnore
        public final boolean a(Context context, String str) {
            if (Build.VERSION.SDK_INT >= 23 && k6.a(context, str) != 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final boolean b(Activity activity, int i) {
            kd4.b(activity, Constants.ACTIVITY);
            if (b(activity)) {
                return true;
            }
            v5.a(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, String... strArr) {
            kd4.b(fragment, "fragment");
            kd4.b(strArr, "perms");
            Context context = fragment.getContext();
            if (context == null) {
                kd4.a();
                throw null;
            } else if (pq4.a(context, (String[]) Arrays.copyOf(strArr, strArr.length))) {
                a((Object) fragment, i, (String[]) Arrays.copyOf(strArr, strArr.length));
            } else {
                fragment.requestPermissions(strArr, i);
            }
        }

        @DexIgnore
        public final String a(String str) {
            kd4.b(str, "permission");
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        String a = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___Title__TurnOnLocationServices);
                        kd4.a((Object) a, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                        return a;
                    }
                    break;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.allow_background_location_access);
                        kd4.a((Object) a2, "LanguageHelper.getString\u2026ckground_location_access)");
                        return a2;
                    }
                    break;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.allow_location_access);
                        kd4.a((Object) a3, "LanguageHelper.getString\u2026ng.allow_location_access)");
                        return a3;
                    }
                    break;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidInAppNotication_Title__AllowNotificationService);
                        kd4.a((Object) a4, "LanguageHelper.getString\u2026AllowNotificationService)");
                        return a4;
                    }
                    break;
            }
            return "";
        }

        @DexIgnore
        public final void a(Object obj, int i, String... strArr) {
            int[] iArr = new int[strArr.length];
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            pq4.a(i, strArr, iArr, obj);
        }
    }
}
