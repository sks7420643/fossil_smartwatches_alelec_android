package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wk4 {
    @DexIgnore
    public static /* final */ long a; // = gk4.a("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, (Object) null);
    @DexIgnore
    public static /* final */ int b; // = gk4.a("kotlinx.coroutines.scheduler.offload.threshold", 96, 0, 128, 4, (Object) null);
    @DexIgnore
    public static /* final */ int c; // = gk4.a("kotlinx.coroutines.scheduler.core.pool.size", ee4.a(ek4.a(), 2), 1, 0, 8, (Object) null);
    @DexIgnore
    public static /* final */ int d; // = gk4.a("kotlinx.coroutines.scheduler.max.pool.size", ee4.a(ek4.a() * 128, c, 2097150), 0, 2097150, 4, (Object) null);
    @DexIgnore
    public static /* final */ long e; // = TimeUnit.SECONDS.toNanos(gk4.a("kotlinx.coroutines.scheduler.keep.alive.sec", 5, 0, 0, 12, (Object) null));
    @DexIgnore
    public static xk4 f; // = rk4.a;

    /*
    static {
        int unused = gk4.a("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, (Object) null);
    }
    */
}
