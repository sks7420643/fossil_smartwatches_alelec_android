package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g54 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ g74 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l54 {
        @DexIgnore
        public /* final */ /* synthetic */ f54 e;

        @DexIgnore
        public a(f54 f54) {
            this.e = f54;
        }

        @DexIgnore
        public void a() {
            f54 a = g54.this.b();
            if (!this.e.equals(a)) {
                q44.g().d("Fabric", "Asychronously getting Advertising Info and storing it to preferences");
                g54.this.c(a);
            }
        }
    }

    @DexIgnore
    public g54(Context context) {
        this.a = context.getApplicationContext();
        this.b = new h74(context, "TwitterAdvertisingInfoPreferences");
    }

    @DexIgnore
    public final void b(f54 f54) {
        new Thread(new a(f54)).start();
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final void c(f54 f54) {
        if (a(f54)) {
            g74 g74 = this.b;
            g74.a(g74.edit().putString("advertising_id", f54.a).putBoolean("limit_ad_tracking_enabled", f54.b));
            return;
        }
        g74 g742 = this.b;
        g742.a(g742.edit().remove("advertising_id").remove("limit_ad_tracking_enabled"));
    }

    @DexIgnore
    public j54 d() {
        return new h54(this.a);
    }

    @DexIgnore
    public j54 e() {
        return new i54(this.a);
    }

    @DexIgnore
    public f54 a() {
        f54 c = c();
        if (a(c)) {
            q44.g().d("Fabric", "Using AdvertisingInfo from Preference Store");
            b(c);
            return c;
        }
        f54 b2 = b();
        c(b2);
        return b2;
    }

    @DexIgnore
    public final f54 b() {
        f54 a2 = d().a();
        if (!a(a2)) {
            a2 = e().a();
            if (!a(a2)) {
                q44.g().d("Fabric", "AdvertisingInfo not present");
            } else {
                q44.g().d("Fabric", "Using AdvertisingInfo from Service Provider");
            }
        } else {
            q44.g().d("Fabric", "Using AdvertisingInfo from Reflection Provider");
        }
        return a2;
    }

    @DexIgnore
    public final boolean a(f54 f54) {
        return f54 != null && !TextUtils.isEmpty(f54.a);
    }

    @DexIgnore
    public f54 c() {
        return new f54(this.b.get().getString("advertising_id", ""), this.b.get().getBoolean("limit_ad_tracking_enabled", false));
    }
}
