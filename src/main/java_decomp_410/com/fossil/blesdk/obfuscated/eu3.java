package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.oe;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eu3 extends oe.i {
    @DexIgnore
    public eu3(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
        kd4.b(canvas, "c");
        kd4.b(recyclerView, "recyclerView");
        kd4.b(viewHolder, "viewHolder");
        super.a(canvas, recyclerView, viewHolder, f, f2, i, z);
        View view = viewHolder.itemView;
        kd4.a((Object) view, "viewHolder.itemView");
        ColorDrawable colorDrawable = new ColorDrawable(-65536);
        float f3 = (float) 0;
        if (f < f3) {
            colorDrawable.setBounds((view.getRight() + Math.round(f)) - 0, view.getTop(), view.getRight(), view.getBottom());
        } else {
            colorDrawable.setBounds(0, 0, 0, 0);
        }
        colorDrawable.draw(canvas);
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize((float) PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.sp14));
        float f4 = (float) 2;
        float bottom = ((float) (view.getBottom() + view.getTop())) / f4;
        String a = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.commute_time_app_CTA__Delete);
        kd4.a((Object) a, "LanguageHelper.getString\u2026ute_time_app_CTA__Delete)");
        if (a != null) {
            String upperCase = a.toUpperCase();
            kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            canvas.drawText(upperCase, ((((float) view.getRight()) + f) - f3) + ((float) PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp24)), bottom - ((paint.descent() + paint.ascent()) / f4), paint);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public boolean b(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        kd4.b(recyclerView, "recyclerView");
        kd4.b(viewHolder, "viewHolder");
        kd4.b(viewHolder2, "target");
        return false;
    }
}
