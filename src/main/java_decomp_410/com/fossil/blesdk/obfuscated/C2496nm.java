package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nm */
public class C2496nm implements com.fossil.blesdk.obfuscated.C3229wm {

    @DexIgnore
    /* renamed from: a */
    public int f7829a;

    @DexIgnore
    /* renamed from: b */
    public int f7830b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f7831c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ float f7832d;

    @DexIgnore
    public C2496nm() {
        this(2500, 1, 1.0f);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo14040a() {
        return this.f7829a;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo14042b() {
        return this.f7830b;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo14043c() {
        return this.f7830b <= this.f7831c;
    }

    @DexIgnore
    public C2496nm(int i, int i2, float f) {
        this.f7829a = i;
        this.f7831c = i2;
        this.f7832d = f;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14041a(com.android.volley.VolleyError volleyError) throws com.android.volley.VolleyError {
        this.f7830b++;
        int i = this.f7829a;
        this.f7829a = i + ((int) (((float) i) * this.f7832d));
        if (!mo14043c()) {
            throw volleyError;
        }
    }
}
