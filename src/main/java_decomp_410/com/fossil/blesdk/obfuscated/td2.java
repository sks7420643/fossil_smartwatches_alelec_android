package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class td2 extends sd2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.iv_close, 1);
        A.put(R.id.ftv_title, 2);
        A.put(R.id.ll_allow_calls_from, 3);
        A.put(R.id.ftv_allow_calls_from_value, 4);
        A.put(R.id.v_line1, 5);
        A.put(R.id.ll_allow_messages_from, 6);
        A.put(R.id.ftv_allow_messages_from_value, 7);
        A.put(R.id.v_line2, 8);
        A.put(R.id.ftv_favorite_contacts_section, 9);
        A.put(R.id.ftv_add, 10);
        A.put(R.id.rv_favorite_contacts, 11);
    }
    */

    @DexIgnore
    public td2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public td2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[10], objArr[4], objArr[7], objArr[9], objArr[2], objArr[1], objArr[3], objArr[6], objArr[11], objArr[5], objArr[8]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
