package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ih */
public class C2020ih {

    @DexIgnore
    /* renamed from: a */
    public android.view.ViewGroup f6029a;

    @DexIgnore
    /* renamed from: b */
    public java.lang.Runnable f6030b;

    @DexIgnore
    /* renamed from: a */
    public void mo11941a() {
        if (m8391a(this.f6029a) == this) {
            java.lang.Runnable runnable = this.f6030b;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m8392a(android.view.View view, com.fossil.blesdk.obfuscated.C2020ih ihVar) {
        view.setTag(com.fossil.blesdk.obfuscated.C1875gh.transition_current_scene, ihVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2020ih m8391a(android.view.View view) {
        return (com.fossil.blesdk.obfuscated.C2020ih) view.getTag(com.fossil.blesdk.obfuscated.C1875gh.transition_current_scene);
    }
}
