package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jk4 {
    @DexIgnore
    public Object[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ CoroutineContext c;

    @DexIgnore
    public jk4(CoroutineContext coroutineContext, int i) {
        kd4.b(coroutineContext, "context");
        this.c = coroutineContext;
        this.a = new Object[i];
    }

    @DexIgnore
    public final CoroutineContext a() {
        return this.c;
    }

    @DexIgnore
    public final void b() {
        this.b = 0;
    }

    @DexIgnore
    public final Object c() {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        return objArr[i];
    }

    @DexIgnore
    public final void a(Object obj) {
        Object[] objArr = this.a;
        int i = this.b;
        this.b = i + 1;
        objArr[i] = obj;
    }
}
