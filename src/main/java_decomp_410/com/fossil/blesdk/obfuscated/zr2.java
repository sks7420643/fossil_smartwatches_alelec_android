package com.fossil.blesdk.obfuscated;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.pq4;
import com.fossil.blesdk.obfuscated.ul2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zr2 extends Fragment implements pq4.a, View.OnKeyListener, ul2.b {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public AnalyticsHelper e;
    @DexIgnore
    public /* final */ pa f; // = new v62(this);
    @DexIgnore
    public vl2 g;
    @DexIgnore
    public HashMap h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = zr2.class.getSimpleName();
        kd4.a((Object) simpleName, "BaseFragment::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public void L0() {
        FLogger.INSTANCE.getLocal().d(i, "Tracer started");
        PortfolioApp.W.c().a(this.g);
    }

    @DexIgnore
    public void M0() {
        FLogger.INSTANCE.getLocal().d(i, "Tracer ended");
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final pa O0() {
        return this.f;
    }

    @DexIgnore
    public final AnalyticsHelper P0() {
        AnalyticsHelper analyticsHelper = this.e;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        kd4.d("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    public final vl2 Q0() {
        return this.g;
    }

    @DexIgnore
    public final void R(String str) {
        kd4.b(str, "view");
        vl2 b = AnalyticsHelper.f.b();
        b.b(str);
        this.g = b;
        vl2 vl2 = this.g;
        if (vl2 != null) {
            vl2.a((ul2.b) this);
        }
    }

    @DexIgnore
    public String R0() {
        return i;
    }

    @DexIgnore
    public final void S(String str) {
        kd4.b(str, "title");
        FLogger.INSTANCE.getLocal().d(i, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public boolean S0() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof zr2)) {
            return false;
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return ((zr2) parentFragment).S0();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final void T(String str) {
        kd4.b(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(R.layout.toast_created_new_preset_successfully, activity != null ? (ViewGroup) activity.findViewById(R.id.cv_root) : null);
        View findViewById = inflate.findViewById(R.id.ftv_content);
        kd4.a((Object) findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(i, "hideProgressDialog");
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).h();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(i, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).p();
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = AnalyticsHelper.f.c();
    }

    @DexIgnore
    public void onDestroyView() {
        vl2 vl2 = this.g;
        if (vl2 != null) {
            vl2.c();
        }
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        kd4.b(view, "view");
        kd4.b(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(i, "onKey KEYCODE_BACK");
        return S0();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        kd4.b(strArr, "permissions");
        kd4.b(iArr, "grantResults");
        super.onRequestPermissionsResult(i2, strArr, iArr);
        pq4.a(i2, strArr, iArr, this);
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public final void a(Fragment fragment, String str, int i2) {
        kd4.b(fragment, "fragment");
        kd4.b(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        bb a2 = childFragmentManager.a();
        kd4.a((Object) a2, "fragmentManager.beginTransaction()");
        Fragment a3 = childFragmentManager.a(i2);
        if (a3 != null) {
            a2.d(a3);
        }
        a2.a((String) null);
        a2.b(i2, fragment, str);
        a2.b();
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        kd4.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "onPermissionsGranted: perm = " + str);
        }
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        kd4.b(list, "perms");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "onPermissionsDenied: perm = " + str);
        }
    }

    @DexIgnore
    public final void a(Intent intent, String str) {
        kd4.b(intent, "browserIntent");
        kd4.b(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        kd4.b(str, Constants.EVENT);
        kd4.b(map, "values");
        AnalyticsHelper analyticsHelper = this.e;
        if (analyticsHelper != null) {
            analyticsHelper.a(str, (Map<String, ? extends Object>) map);
        } else {
            kd4.d("mAnalyticHelper");
            throw null;
        }
    }
}
