package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bf */
public abstract class C1479bf extends androidx.recyclerview.widget.RecyclerView.C0242o {

    @DexIgnore
    /* renamed from: a */
    public androidx.recyclerview.widget.RecyclerView f3721a;

    @DexIgnore
    /* renamed from: b */
    public android.widget.Scroller f3722b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ androidx.recyclerview.widget.RecyclerView.C0244q f3723c; // = new com.fossil.blesdk.obfuscated.C1479bf.C1480a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.bf$a */
    public class C1480a extends androidx.recyclerview.widget.RecyclerView.C0244q {

        @DexIgnore
        /* renamed from: a */
        public boolean f3724a; // = false;

        @DexIgnore
        public C1480a() {
        }

        @DexIgnore
        public void onScrollStateChanged(androidx.recyclerview.widget.RecyclerView recyclerView, int i) {
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0 && this.f3724a) {
                this.f3724a = false;
                com.fossil.blesdk.obfuscated.C1479bf.this.mo9170c();
            }
        }

        @DexIgnore
        public void onScrolled(androidx.recyclerview.widget.RecyclerView recyclerView, int i, int i2) {
            if (i != 0 || i2 != 0) {
                this.f3724a = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bf$b")
    /* renamed from: com.fossil.blesdk.obfuscated.bf$b */
    public class C1481b extends com.fossil.blesdk.obfuscated.C2873se {
        @DexIgnore
        public C1481b(android.content.Context context) {
            super(context);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo3016a(android.view.View view, androidx.recyclerview.widget.RecyclerView.State state, androidx.recyclerview.widget.RecyclerView.C0251v.C0252a aVar) {
            com.fossil.blesdk.obfuscated.C1479bf bfVar = com.fossil.blesdk.obfuscated.C1479bf.this;
            androidx.recyclerview.widget.RecyclerView recyclerView = bfVar.f3721a;
            if (recyclerView != null) {
                int[] a = bfVar.mo9164a(recyclerView.getLayoutManager(), view);
                int i = a[0];
                int i2 = a[1];
                int d = mo15951d(java.lang.Math.max(java.lang.Math.abs(i), java.lang.Math.abs(i2)));
                if (d > 0) {
                    aVar.mo3029a(i, i2, d, this.f9259j);
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public float mo9171a(android.util.DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int mo9160a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, int i, int i2);

    @DexIgnore
    /* renamed from: a */
    public boolean mo2986a(int i, int i2) {
        androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = this.f3721a.getLayoutManager();
        if (layoutManager == null || this.f3721a.getAdapter() == null) {
            return false;
        }
        int minFlingVelocity = this.f3721a.getMinFlingVelocity();
        if ((java.lang.Math.abs(i2) > minFlingVelocity || java.lang.Math.abs(i) > minFlingVelocity) && mo9167b(layoutManager, i, i2)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int[] mo9164a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, android.view.View view);

    @DexIgnore
    /* renamed from: b */
    public final void mo9166b() throws java.lang.IllegalStateException {
        if (this.f3721a.getOnFlingListener() == null) {
            this.f3721a.mo2526a(this.f3723c);
            this.f3721a.setOnFlingListener(this);
            return;
        }
        throw new java.lang.IllegalStateException("An instance of OnFlingListener already set.");
    }

    @DexIgnore
    /* renamed from: c */
    public abstract android.view.View mo9169c(androidx.recyclerview.widget.RecyclerView.C0236m mVar);

    @DexIgnore
    /* renamed from: c */
    public void mo9170c() {
        androidx.recyclerview.widget.RecyclerView recyclerView = this.f3721a;
        if (recyclerView != null) {
            androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager != null) {
                android.view.View c = mo9169c(layoutManager);
                if (c != null) {
                    int[] a = mo9164a(layoutManager, c);
                    if (a[0] != 0 || a[1] != 0) {
                        this.f3721a.mo2621j(a[0], a[1]);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public int[] mo9168b(int i, int i2) {
        this.f3722b.fling(0, 0, i, i2, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return new int[]{this.f3722b.getFinalX(), this.f3722b.getFinalY()};
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9163a(androidx.recyclerview.widget.RecyclerView recyclerView) throws java.lang.IllegalStateException {
        androidx.recyclerview.widget.RecyclerView recyclerView2 = this.f3721a;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                mo9162a();
            }
            this.f3721a = recyclerView;
            if (this.f3721a != null) {
                mo9166b();
                this.f3722b = new android.widget.Scroller(this.f3721a.getContext(), new android.view.animation.DecelerateInterpolator());
                mo9170c();
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo9167b(androidx.recyclerview.widget.RecyclerView.C0236m mVar, int i, int i2) {
        if (!(mVar instanceof androidx.recyclerview.widget.RecyclerView.C0251v.C0253b)) {
            return false;
        }
        androidx.recyclerview.widget.RecyclerView.C0251v a = mo9161a(mVar);
        if (a == null) {
            return false;
        }
        int a2 = mo9160a(mVar, i, i2);
        if (a2 == -1) {
            return false;
        }
        a.mo3022c(a2);
        mVar.mo2924b(a);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9162a() {
        this.f3721a.mo2546b(this.f3723c);
        this.f3721a.setOnFlingListener((androidx.recyclerview.widget.RecyclerView.C0242o) null);
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2873se mo9165b(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        if (!(mVar instanceof androidx.recyclerview.widget.RecyclerView.C0251v.C0253b)) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C1479bf.C1481b(this.f3721a.getContext());
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.recyclerview.widget.RecyclerView.C0251v mo9161a(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        return mo9165b(mVar);
    }
}
