package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bi0 extends LifecycleCallback implements DialogInterface.OnCancelListener {
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public /* final */ AtomicReference<ci0> g;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ xd0 i;

    @DexIgnore
    public bi0(ye0 ye0) {
        this(ye0, xd0.a());
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null) {
            this.g.set(bundle.getBoolean("resolving_error", false) ? new ci0(new ud0(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    @DexIgnore
    public abstract void a(ud0 ud0, int i2);

    @DexIgnore
    public void b(Bundle bundle) {
        super.b(bundle);
        ci0 ci0 = this.g.get();
        if (ci0 != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", ci0.b());
            bundle.putInt("failed_status", ci0.a().H());
            bundle.putParcelable("failed_resolution", ci0.a().J());
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        this.f = true;
    }

    @DexIgnore
    public void e() {
        super.e();
        this.f = false;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public final void g() {
        this.g.set((Object) null);
        f();
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        a(new ud0(13, (PendingIntent) null), a(this.g.get()));
        g();
    }

    @DexIgnore
    public bi0(ye0 ye0, xd0 xd0) {
        super(ye0);
        this.g = new AtomicReference<>((Object) null);
        this.h = new ss0(Looper.getMainLooper());
        this.i = xd0;
    }

    @DexIgnore
    public final void b(ud0 ud0, int i2) {
        ci0 ci0 = new ci0(ud0, i2);
        if (this.g.compareAndSet((Object) null, ci0)) {
            this.h.post(new di0(this, ci0));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005c  */
    public void a(int i2, int i3, Intent intent) {
        ci0 ci0 = this.g.get();
        boolean z = true;
        if (i2 == 1) {
            if (i3 != -1) {
                if (i3 == 0) {
                    int i4 = 13;
                    if (intent != null) {
                        i4 = intent.getIntExtra("<<ResolutionFailureErrorDetail>>", 13);
                    }
                    ci0 ci02 = new ci0(new ud0(i4, (PendingIntent) null), a(ci0));
                    this.g.set(ci02);
                    ci0 = ci02;
                }
            }
            if (z) {
            }
        } else if (i2 == 2) {
            int c = this.i.c((Context) a());
            if (c != 0) {
                z = false;
            }
            if (ci0 != null) {
                if (ci0.a().H() == 18 && c == 18) {
                    return;
                }
                if (z) {
                    g();
                    return;
                } else if (ci0 != null) {
                    a(ci0.a(), ci0.b());
                    return;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    public static int a(ci0 ci0) {
        if (ci0 == null) {
            return -1;
        }
        return ci0.b();
    }
}
