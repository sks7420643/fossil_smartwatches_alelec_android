package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c03 {
    @DexIgnore
    public /* final */ a03 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<String> c;

    @DexIgnore
    public c03(a03 a03, int i, ArrayList<String> arrayList) {
        kd4.b(a03, "mView");
        this.a = a03;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = this.c;
        return arrayList == null ? new ArrayList<>() : arrayList;
    }

    @DexIgnore
    public final a03 c() {
        return this.a;
    }
}
