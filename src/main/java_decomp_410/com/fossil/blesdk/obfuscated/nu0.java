package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.nu0;
import com.google.android.gms.internal.clearcut.zzfl;
import com.google.android.gms.internal.clearcut.zzfq;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nu0<T extends nu0<T>> extends Comparable<T> {
    @DexIgnore
    tv0 a(tv0 tv0, sv0 sv0);

    @DexIgnore
    yv0 a(yv0 yv0, yv0 yv02);

    @DexIgnore
    boolean a();

    @DexIgnore
    zzfq b();

    @DexIgnore
    boolean c();

    @DexIgnore
    zzfl d();

    @DexIgnore
    int zzc();
}
