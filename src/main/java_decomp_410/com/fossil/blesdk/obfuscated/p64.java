package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class p64<T> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ o64<T> b;
    @DexIgnore
    public /* final */ n54 c;
    @DexIgnore
    public /* final */ q64 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ List<r64> f; // = new CopyOnWriteArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<b> {
        @DexIgnore
        public a(p64 p64) {
        }

        @DexIgnore
        /* renamed from: a */
        public int compare(b bVar, b bVar2) {
            return (int) (bVar.b - bVar2.b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ File a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public b(File file, long j) {
            this.a = file;
            this.b = j;
        }
    }

    @DexIgnore
    public p64(Context context, o64<T> o64, n54 n54, q64 q64, int i) throws IOException {
        this.a = context.getApplicationContext();
        this.b = o64;
        this.d = q64;
        this.c = n54;
        this.c.a();
        this.e = i;
    }

    @DexIgnore
    public void a(T t) throws IOException {
        byte[] a2 = this.b.a(t);
        a(a2.length);
        this.d.a(a2);
    }

    @DexIgnore
    public final void b(String str) {
        for (r64 a2 : this.f) {
            try {
                a2.a(str);
            } catch (Exception e2) {
                CommonUtils.a(this.a, "One of the roll over listeners threw an exception", (Throwable) e2);
            }
        }
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public List<File> d() {
        return this.d.a(1);
    }

    @DexIgnore
    public int e() {
        return MFNetworkReturnCode.REQUEST_NOT_FOUND;
    }

    @DexIgnore
    public int f() {
        return this.e;
    }

    @DexIgnore
    public boolean g() throws IOException {
        String str;
        boolean z = true;
        if (!this.d.b()) {
            str = c();
            this.d.a(str);
            CommonUtils.a(this.a, 4, "Fabric", String.format(Locale.US, "generated new file %s", new Object[]{str}));
            this.c.a();
        } else {
            str = null;
            z = false;
        }
        b(str);
        return z;
    }

    @DexIgnore
    public void a(r64 r64) {
        if (r64 != null) {
            this.f.add(r64);
        }
    }

    @DexIgnore
    public void b() {
        List<File> c2 = this.d.c();
        int f2 = f();
        if (c2.size() > f2) {
            int size = c2.size() - f2;
            CommonUtils.c(this.a, String.format(Locale.US, "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files", new Object[]{Integer.valueOf(c2.size()), Integer.valueOf(f2), Integer.valueOf(size)}));
            TreeSet treeSet = new TreeSet(new a(this));
            for (File next : c2) {
                treeSet.add(new b(next, a(next.getName())));
            }
            ArrayList arrayList = new ArrayList();
            Iterator it = treeSet.iterator();
            while (it.hasNext()) {
                arrayList.add(((b) it.next()).a);
                if (arrayList.size() == size) {
                    break;
                }
            }
            this.d.a((List<File>) arrayList);
        }
    }

    @DexIgnore
    public final void a(int i) throws IOException {
        if (!this.d.a(i, e())) {
            CommonUtils.a(this.a, 4, "Fabric", String.format(Locale.US, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", new Object[]{Integer.valueOf(this.d.a()), Integer.valueOf(i), Integer.valueOf(e())}));
            g();
        }
    }

    @DexIgnore
    public void a(List<File> list) {
        this.d.a(list);
    }

    @DexIgnore
    public void a() {
        q64 q64 = this.d;
        q64.a(q64.c());
        this.d.d();
    }

    @DexIgnore
    public long a(String str) {
        String[] split = str.split("_");
        if (split.length != 3) {
            return 0;
        }
        try {
            return Long.valueOf(split[2]).longValue();
        } catch (NumberFormatException unused) {
            return 0;
        }
    }
}
