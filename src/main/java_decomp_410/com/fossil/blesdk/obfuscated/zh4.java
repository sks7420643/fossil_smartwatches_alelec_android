package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zh4 implements ai4 {
    @DexIgnore
    public /* final */ qi4 e;

    @DexIgnore
    public zh4(qi4 qi4) {
        kd4.b(qi4, "list");
        this.e = qi4;
    }

    @DexIgnore
    public qi4 a() {
        return this.e;
    }

    @DexIgnore
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return ch4.c() ? a().a("New") : super.toString();
    }
}
