package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.b;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ff0<A extends de0.b, ResultT> {
    @DexIgnore
    public /* final */ wd0[] a; // = null;
    @DexIgnore
    public /* final */ boolean b; // = false;

    @DexIgnore
    public abstract void a(A a2, xn1<ResultT> xn1) throws RemoteException;

    @DexIgnore
    public boolean a() {
        return this.b;
    }

    @DexIgnore
    public final wd0[] b() {
        return this.a;
    }
}
