package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class u52 {
    @DexIgnore
    public /* final */ mg4 a; // = zi4.a((fi4) null, 1, (Object) null);
    @DexIgnore
    public /* final */ ug4 b; // = nh4.b();
    @DexIgnore
    public /* final */ ug4 c; // = nh4.a();
    @DexIgnore
    public /* final */ pi4 d; // = nh4.c();
    @DexIgnore
    public /* final */ zg4 e; // = ah4.a(this.a.plus(this.d));

    @DexIgnore
    public final ug4 b() {
        return this.c;
    }

    @DexIgnore
    public final ug4 c() {
        return this.b;
    }

    @DexIgnore
    public final pi4 d() {
        return this.d;
    }

    @DexIgnore
    public final zg4 e() {
        return this.e;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public void g() {
    }
}
