package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f1 */
public class C1763f1 implements com.fossil.blesdk.obfuscated.C2618p1, android.widget.AdapterView.OnItemClickListener {

    @DexIgnore
    /* renamed from: e */
    public android.content.Context f4983e;

    @DexIgnore
    /* renamed from: f */
    public android.view.LayoutInflater f4984f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1915h1 f4985g;

    @DexIgnore
    /* renamed from: h */
    public androidx.appcompat.view.menu.ExpandedMenuView f4986h;

    @DexIgnore
    /* renamed from: i */
    public int f4987i;

    @DexIgnore
    /* renamed from: j */
    public int f4988j;

    @DexIgnore
    /* renamed from: k */
    public int f4989k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f4990l;

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C1763f1.C1764a f4991m;

    @DexIgnore
    /* renamed from: n */
    public int f4992n;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.f1$a */
    public class C1764a extends android.widget.BaseAdapter {

        @DexIgnore
        /* renamed from: e */
        public int f4993e; // = -1;

        @DexIgnore
        public C1764a() {
            mo10686a();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10686a() {
            com.fossil.blesdk.obfuscated.C2179k1 f = com.fossil.blesdk.obfuscated.C1763f1.this.f4985g.mo11502f();
            if (f != null) {
                java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = com.fossil.blesdk.obfuscated.C1763f1.this.f4985g.mo11511j();
                int size = j.size();
                for (int i = 0; i < size; i++) {
                    if (j.get(i) == f) {
                        this.f4993e = i;
                        return;
                    }
                }
            }
            this.f4993e = -1;
        }

        @DexIgnore
        public int getCount() {
            int size = com.fossil.blesdk.obfuscated.C1763f1.this.f4985g.mo11511j().size() - com.fossil.blesdk.obfuscated.C1763f1.this.f4987i;
            return this.f4993e < 0 ? size : size - 1;
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
            if (view == null) {
                com.fossil.blesdk.obfuscated.C1763f1 f1Var = com.fossil.blesdk.obfuscated.C1763f1.this;
                view = f1Var.f4984f.inflate(f1Var.f4989k, viewGroup, false);
            }
            ((com.fossil.blesdk.obfuscated.C2697q1.C2698a) view).mo349a(getItem(i), 0);
            return view;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            mo10686a();
            super.notifyDataSetChanged();
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2179k1 getItem(int i) {
            java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = com.fossil.blesdk.obfuscated.C1763f1.this.f4985g.mo11511j();
            int i2 = i + com.fossil.blesdk.obfuscated.C1763f1.this.f4987i;
            int i3 = this.f4993e;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return j.get(i2);
        }
    }

    @DexIgnore
    public C1763f1(android.content.Context context, int i) {
        this(i, 0);
        this.f4983e = context;
        this.f4984f = android.view.LayoutInflater.from(this.f4983e);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1158a(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        int i = this.f4988j;
        if (i != 0) {
            this.f4983e = new android.view.ContextThemeWrapper(context, i);
            this.f4984f = android.view.LayoutInflater.from(this.f4983e);
        } else if (this.f4983e != null) {
            this.f4983e = context;
            if (this.f4984f == null) {
                this.f4984f = android.view.LayoutInflater.from(this.f4983e);
            }
        }
        this.f4985g = h1Var;
        com.fossil.blesdk.obfuscated.C1763f1.C1764a aVar = this.f4991m;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1162a() {
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1163a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10683b(android.os.Bundle bundle) {
        android.util.SparseArray sparseArray = new android.util.SparseArray();
        androidx.appcompat.view.menu.ExpandedMenuView expandedMenuView = this.f4986h;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo1166b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public android.widget.ListAdapter mo10684c() {
        if (this.f4991m == null) {
            this.f4991m = new com.fossil.blesdk.obfuscated.C1763f1.C1764a();
        }
        return this.f4991m;
    }

    @DexIgnore
    public int getId() {
        return this.f4992n;
    }

    @DexIgnore
    public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long j) {
        this.f4985g.mo11466a((android.view.MenuItem) this.f4991m.getItem(i), (com.fossil.blesdk.obfuscated.C2618p1) this, 0);
    }

    @DexIgnore
    public C1763f1(int i, int i2) {
        this.f4989k = i;
        this.f4988j = i2;
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Parcelable mo1165b() {
        if (this.f4986h == null) {
            return null;
        }
        android.os.Bundle bundle = new android.os.Bundle();
        mo10683b(bundle);
        return bundle;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2697q1 mo10681a(android.view.ViewGroup viewGroup) {
        if (this.f4986h == null) {
            this.f4986h = (androidx.appcompat.view.menu.ExpandedMenuView) this.f4984f.inflate(com.fossil.blesdk.obfuscated.C3250x.abc_expanded_menu_layout, viewGroup, false);
            if (this.f4991m == null) {
                this.f4991m = new com.fossil.blesdk.obfuscated.C1763f1.C1764a();
            }
            this.f4986h.setAdapter(this.f4991m);
            this.f4986h.setOnItemClickListener(this);
        }
        return this.f4986h;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1161a(boolean z) {
        com.fossil.blesdk.obfuscated.C1763f1.C1764a aVar = this.f4991m;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9013a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
        this.f4990l = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1164a(com.fossil.blesdk.obfuscated.C3078v1 v1Var) {
        if (!v1Var.hasVisibleItems()) {
            return false;
        }
        new com.fossil.blesdk.obfuscated.C1993i1(v1Var).mo11808a((android.os.IBinder) null);
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f4990l;
        if (aVar == null) {
            return true;
        }
        aVar.mo534a(v1Var);
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1160a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f4990l;
        if (aVar != null) {
            aVar.mo533a(h1Var, z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10682a(android.os.Bundle bundle) {
        android.util.SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.f4986h.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1159a(android.os.Parcelable parcelable) {
        mo10682a((android.os.Bundle) parcelable);
    }
}
