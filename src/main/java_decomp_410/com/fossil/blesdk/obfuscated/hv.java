package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hv {
    @DexIgnore
    public static /* final */ yp<?, ?, ?> c; // = new yp(Object.class, Object.class, Object.class, Collections.singletonList(new op(Object.class, Object.class, Object.class, Collections.emptyList(), new iu(), (g8<List<Throwable>>) null)), (g8<List<Throwable>>) null);
    @DexIgnore
    public /* final */ g4<sw, yp<?, ?, ?>> a; // = new g4<>();
    @DexIgnore
    public /* final */ AtomicReference<sw> b; // = new AtomicReference<>();

    @DexIgnore
    public boolean a(yp<?, ?, ?> ypVar) {
        return c.equals(ypVar);
    }

    @DexIgnore
    public final sw b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        sw andSet = this.b.getAndSet((Object) null);
        if (andSet == null) {
            andSet = new sw();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public <Data, TResource, Transcode> yp<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        yp<Data, TResource, Transcode> ypVar;
        sw b2 = b(cls, cls2, cls3);
        synchronized (this.a) {
            ypVar = this.a.get(b2);
        }
        this.b.set(b2);
        return ypVar;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, yp<?, ?, ?> ypVar) {
        synchronized (this.a) {
            g4<sw, yp<?, ?, ?>> g4Var = this.a;
            sw swVar = new sw(cls, cls2, cls3);
            if (ypVar == null) {
                ypVar = c;
            }
            g4Var.put(swVar, ypVar);
        }
    }
}
