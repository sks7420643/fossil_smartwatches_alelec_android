package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gs implements sr<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public sr<Uri, InputStream> a(wr wrVar) {
            return new gs(this.a);
        }
    }

    @DexIgnore
    public gs(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public sr.a<InputStream> a(Uri uri, int i, int i2, lo loVar) {
        if (!ep.a(i, i2) || !a(loVar)) {
            return null;
        }
        return new sr.a<>(new jw(uri), fp.b(this.a, uri));
    }

    @DexIgnore
    public final boolean a(lo loVar) {
        Long l = (Long) loVar.a(it.d);
        return l != null && l.longValue() == -1;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ep.c(uri);
    }
}
