package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i93 implements MembersInjector<ActivityOverviewDayPresenter> {
    @DexIgnore
    public static void a(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
        activityOverviewDayPresenter.i();
    }
}
