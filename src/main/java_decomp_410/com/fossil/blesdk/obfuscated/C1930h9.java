package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h9 */
public final class C1930h9 {
    @DexIgnore
    /* renamed from: a */
    public static boolean m7829a(android.view.ViewGroup viewGroup) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return viewGroup.isTransitionGroup();
        }
        java.lang.Boolean bool = (java.lang.Boolean) viewGroup.getTag(com.fossil.blesdk.obfuscated.C2942t5.tag_transition_group);
        return ((bool == null || !bool.booleanValue()) && viewGroup.getBackground() == null && com.fossil.blesdk.obfuscated.C1776f9.m6851q(viewGroup) == null) ? false : true;
    }
}
