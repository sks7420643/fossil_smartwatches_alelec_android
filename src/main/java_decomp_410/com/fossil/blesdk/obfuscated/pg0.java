package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import android.os.Message;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pg0 extends ss0 {
    @DexIgnore
    public /* final */ /* synthetic */ ng0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pg0(ng0 ng0, Looper looper) {
        super(looper);
        this.a = ng0;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((og0) message.obj).a(this.a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GACStateManager", sb.toString());
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
