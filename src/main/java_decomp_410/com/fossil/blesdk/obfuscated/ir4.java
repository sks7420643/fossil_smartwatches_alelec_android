package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jl4;
import com.fossil.blesdk.obfuscated.ur4;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.KotlinExtensions;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ir4<ResponseT, ReturnT> extends rr4<ReturnT> {
    @DexIgnore
    public /* final */ pr4 a;
    @DexIgnore
    public /* final */ jl4.a b;
    @DexIgnore
    public /* final */ gr4<em4, ResponseT> c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<ResponseT, ReturnT> extends ir4<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ dr4<ResponseT, ReturnT> d;

        @DexIgnore
        public a(pr4 pr4, jl4.a aVar, gr4<em4, ResponseT> gr4, dr4<ResponseT, ReturnT> dr4) {
            super(pr4, aVar, gr4);
            this.d = dr4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [retrofit2.Call, retrofit2.Call<ResponseT>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public ReturnT a(Call<ResponseT> r1, Object[] objArr) {
            return this.d.a(r1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<ResponseT> extends ir4<ResponseT, Object> {
        @DexIgnore
        public /* final */ dr4<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public b(pr4 pr4, jl4.a aVar, gr4<em4, ResponseT> gr4, dr4<ResponseT, Call<ResponseT>> dr4, boolean z) {
            super(pr4, aVar, gr4);
            this.d = dr4;
            this.e = z;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            yb4 yb4 = objArr[objArr.length - 1];
            try {
                if (this.e) {
                    return KotlinExtensions.b(call2, yb4);
                }
                return KotlinExtensions.a(call2, yb4);
            } catch (Exception e2) {
                return KotlinExtensions.a(e2, (yb4<?>) yb4);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<ResponseT> extends ir4<ResponseT, Object> {
        @DexIgnore
        public /* final */ dr4<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public c(pr4 pr4, jl4.a aVar, gr4<em4, ResponseT> gr4, dr4<ResponseT, Call<ResponseT>> dr4) {
            super(pr4, aVar, gr4);
            this.d = dr4;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            yb4 yb4 = objArr[objArr.length - 1];
            try {
                return KotlinExtensions.c(call2, yb4);
            } catch (Exception e) {
                return KotlinExtensions.a(e, (yb4<?>) yb4);
            }
        }
    }

    @DexIgnore
    public ir4(pr4 pr4, jl4.a aVar, gr4<em4, ResponseT> gr4) {
        this.a = pr4;
        this.b = aVar;
        this.c = gr4;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> ir4<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, pr4 pr4) {
        Type type;
        boolean z;
        Class<qr4> cls = qr4.class;
        boolean z2 = pr4.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type a2 = ur4.a(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (ur4.b(a2) != cls || !(a2 instanceof ParameterizedType)) {
                z = false;
            } else {
                a2 = ur4.b(0, (ParameterizedType) a2);
                z = true;
            }
            type = new ur4.b((Type) null, Call.class, a2);
            annotations = tr4.a(annotations);
        } else {
            type = method.getGenericReturnType();
            z = false;
        }
        dr4 a3 = a(retrofit3, method, type, annotations);
        Type a4 = a3.a();
        if (a4 == Response.class) {
            throw ur4.a(method, "'" + ur4.b(a4).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a4 == cls) {
            throw ur4.a(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!pr4.c.equals("HEAD") || Void.class.equals(a4)) {
            gr4 a5 = a(retrofit3, method, a4);
            jl4.a aVar = retrofit3.b;
            if (!z2) {
                return new a(pr4, aVar, a5, a3);
            }
            if (z) {
                return new c(pr4, aVar, a5, a3);
            }
            return new b(pr4, aVar, a5, a3, false);
        } else {
            throw ur4.a(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract ReturnT a(Call<ResponseT> call, Object[] objArr);

    @DexIgnore
    public static <ResponseT, ReturnT> dr4<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw ur4.a(method, (Throwable) e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> gr4<em4, ResponseT> a(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.b(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw ur4.a(method, (Throwable) e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    public final ReturnT a(Object[] objArr) {
        return a(new kr4(this.a, objArr, this.b, this.c), objArr);
    }
}
