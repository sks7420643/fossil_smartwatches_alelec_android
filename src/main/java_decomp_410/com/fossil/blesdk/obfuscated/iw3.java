package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iw3 {
    @DexIgnore
    public /* final */ CountDownLatch a; // = new CountDownLatch(1);
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public long c; // = -1;

    @DexIgnore
    public void a() {
        if (this.c == -1) {
            long j = this.b;
            if (j != -1) {
                this.c = j - 1;
                this.a.countDown();
                return;
            }
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public void b() {
        if (this.c != -1 || this.b == -1) {
            throw new IllegalStateException();
        }
        this.c = System.nanoTime();
        this.a.countDown();
    }

    @DexIgnore
    public void c() {
        if (this.b == -1) {
            this.b = System.nanoTime();
            return;
        }
        throw new IllegalStateException();
    }
}
