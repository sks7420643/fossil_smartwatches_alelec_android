package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wk3 extends v52<vk3> {
    @DexIgnore
    void G(boolean z);

    @DexIgnore
    void H(String str);

    @DexIgnore
    void O(String str);

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a0();

    @DexIgnore
    void i();

    @DexIgnore
    void k();

    @DexIgnore
    void l0();

    @DexIgnore
    void u0();
}
