package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ga1 {
    @DexIgnore
    public static /* final */ ea1 a; // = c();
    @DexIgnore
    public static /* final */ ea1 b; // = new fa1();

    @DexIgnore
    public static ea1 a() {
        return a;
    }

    @DexIgnore
    public static ea1 b() {
        return b;
    }

    @DexIgnore
    public static ea1 c() {
        try {
            return (ea1) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
