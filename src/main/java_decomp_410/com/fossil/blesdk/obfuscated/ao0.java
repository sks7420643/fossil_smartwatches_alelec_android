package com.fossil.blesdk.obfuscated;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ao0 implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ Intent f;

    @DexIgnore
    public ao0(Context context, Intent intent) {
        this.e = context;
        this.f = intent;
    }

    @DexIgnore
    public final void onClick(View view) {
        try {
            this.e.startActivity(this.f);
        } catch (ActivityNotFoundException e2) {
            Log.e("DeferredLifecycleHelper", "Failed to start resolution intent", e2);
        }
    }
}
