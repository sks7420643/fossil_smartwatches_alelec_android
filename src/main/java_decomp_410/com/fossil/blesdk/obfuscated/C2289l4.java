package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l4 */
public abstract class C2289l4<K, V> {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2289l4<K, V>.b f7129a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2289l4<K, V>.c f7130b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2289l4<K, V>.e f7131c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l4$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l4$a */
    public final class C2290a<T> implements java.util.Iterator<T> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f7132e;

        @DexIgnore
        /* renamed from: f */
        public int f7133f;

        @DexIgnore
        /* renamed from: g */
        public int f7134g;

        @DexIgnore
        /* renamed from: h */
        public boolean f7135h; // = false;

        @DexIgnore
        public C2290a(int i) {
            this.f7132e = i;
            this.f7133f = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f7134g < this.f7133f;
        }

        @DexIgnore
        public T next() {
            if (hasNext()) {
                T a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7134g, this.f7132e);
                this.f7134g++;
                this.f7135h = true;
                return a;
            }
            throw new java.util.NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            if (this.f7135h) {
                this.f7134g--;
                this.f7133f--;
                this.f7135h = false;
                com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(this.f7134g);
                return;
            }
            throw new java.lang.IllegalStateException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l4$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l4$b */
    public final class C2291b implements java.util.Set<java.util.Map.Entry<K, V>> {
        @DexIgnore
        public C2291b() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13151a(java.util.Map.Entry<K, V> entry) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ boolean add(java.lang.Object obj) {
            mo13151a((java.util.Map.Entry) obj);
            throw null;
        }

        @DexIgnore
        public boolean addAll(java.util.Collection<? extends java.util.Map.Entry<K, V>> collection) {
            int c = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
            for (java.util.Map.Entry entry : collection) {
                com.fossil.blesdk.obfuscated.C2289l4.this.mo11130a(entry.getKey(), entry.getValue());
            }
            return c != com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
        }

        @DexIgnore
        public void clear() {
            com.fossil.blesdk.obfuscated.C2289l4.this.mo11128a();
        }

        @DexIgnore
        public boolean contains(java.lang.Object obj) {
            if (!(obj instanceof java.util.Map.Entry)) {
                return false;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry) obj;
            int a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11125a(entry.getKey());
            if (a < 0) {
                return false;
            }
            return com.fossil.blesdk.obfuscated.C1997i4.m8225a(com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(a, 1), entry.getValue());
        }

        @DexIgnore
        public boolean containsAll(java.util.Collection<?> collection) {
            for (java.lang.Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C2289l4.m10052a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int c = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c() - 1; c >= 0; c--) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(c, 0);
                java.lang.Object a2 = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(c, 1);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                if (a2 == null) {
                    i2 = 0;
                } else {
                    i2 = a2.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }

        @DexIgnore
        public boolean isEmpty() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c() == 0;
        }

        @DexIgnore
        public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
            return new com.fossil.blesdk.obfuscated.C2289l4.C2293d();
        }

        @DexIgnore
        public boolean remove(java.lang.Object obj) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public boolean removeAll(java.util.Collection<?> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public boolean retainAll(java.util.Collection<?> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public int size() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
        }

        @DexIgnore
        public java.lang.Object[] toArray() {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            throw new java.lang.UnsupportedOperationException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l4$c")
    /* renamed from: com.fossil.blesdk.obfuscated.l4$c */
    public final class C2292c implements java.util.Set<K> {
        @DexIgnore
        public C2292c() {
        }

        @DexIgnore
        public boolean add(K k) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(java.util.Collection<? extends K> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            com.fossil.blesdk.obfuscated.C2289l4.this.mo11128a();
        }

        @DexIgnore
        public boolean contains(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11125a(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(java.util.Collection<?> collection) {
            return com.fossil.blesdk.obfuscated.C2289l4.m10051a(com.fossil.blesdk.obfuscated.C2289l4.this.mo11132b(), collection);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C2289l4.m10052a(this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2 = 0;
            for (int c = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c() - 1; c >= 0; c--) {
                java.lang.Object a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(c, 0);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                i2 += i;
            }
            return i2;
        }

        @DexIgnore
        public boolean isEmpty() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c() == 0;
        }

        @DexIgnore
        public java.util.Iterator<K> iterator() {
            return new com.fossil.blesdk.obfuscated.C2289l4.C2290a(0);
        }

        @DexIgnore
        public boolean remove(java.lang.Object obj) {
            int a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11125a(obj);
            if (a < 0) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(a);
            return true;
        }

        @DexIgnore
        public boolean removeAll(java.util.Collection<?> collection) {
            return com.fossil.blesdk.obfuscated.C2289l4.m10053b(com.fossil.blesdk.obfuscated.C2289l4.this.mo11132b(), collection);
        }

        @DexIgnore
        public boolean retainAll(java.util.Collection<?> collection) {
            return com.fossil.blesdk.obfuscated.C2289l4.m10054c(com.fossil.blesdk.obfuscated.C2289l4.this.mo11132b(), collection);
        }

        @DexIgnore
        public int size() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
        }

        @DexIgnore
        public java.lang.Object[] toArray() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo13144b(0);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo13143a(tArr, 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l4$d")
    /* renamed from: com.fossil.blesdk.obfuscated.l4$d */
    public final class C2293d implements java.util.Iterator<java.util.Map.Entry<K, V>>, java.util.Map.Entry<K, V> {

        @DexIgnore
        /* renamed from: e */
        public int f7139e;

        @DexIgnore
        /* renamed from: f */
        public int f7140f;

        @DexIgnore
        /* renamed from: g */
        public boolean f7141g; // = false;

        @DexIgnore
        public C2293d() {
            this.f7139e = r2.mo11133c() - 1;
            this.f7140f = -1;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!this.f7141g) {
                throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof java.util.Map.Entry)) {
                return false;
            } else {
                java.util.Map.Entry entry = (java.util.Map.Entry) obj;
                if (!com.fossil.blesdk.obfuscated.C1997i4.m8225a(entry.getKey(), com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 0)) || !com.fossil.blesdk.obfuscated.C1997i4.m8225a(entry.getValue(), com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 1))) {
                    return false;
                }
                return true;
            }
        }

        @DexIgnore
        public K getKey() {
            if (this.f7141g) {
                return com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 0);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public V getValue() {
            if (this.f7141g) {
                return com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 1);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public boolean hasNext() {
            return this.f7140f < this.f7139e;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.f7141g) {
                int i2 = 0;
                java.lang.Object a = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 0);
                java.lang.Object a2 = com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(this.f7140f, 1);
                if (a == null) {
                    i = 0;
                } else {
                    i = a.hashCode();
                }
                if (a2 != null) {
                    i2 = a2.hashCode();
                }
                return i ^ i2;
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public void remove() {
            if (this.f7141g) {
                com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(this.f7140f);
                this.f7140f--;
                this.f7139e--;
                this.f7141g = false;
                return;
            }
            throw new java.lang.IllegalStateException();
        }

        @DexIgnore
        public V setValue(V v) {
            if (this.f7141g) {
                return com.fossil.blesdk.obfuscated.C2289l4.this.mo11127a(this.f7140f, v);
            }
            throw new java.lang.IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public java.lang.String toString() {
            return getKey() + com.j256.ormlite.stmt.query.SimpleComparison.EQUAL_TO_OPERATION + getValue();
        }

        @DexIgnore
        public java.util.Map.Entry<K, V> next() {
            if (hasNext()) {
                this.f7140f++;
                this.f7141g = true;
                return this;
            }
            throw new java.util.NoSuchElementException();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l4$e")
    /* renamed from: com.fossil.blesdk.obfuscated.l4$e */
    public final class C2294e implements java.util.Collection<V> {
        @DexIgnore
        public C2294e() {
        }

        @DexIgnore
        public boolean add(V v) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public boolean addAll(java.util.Collection<? extends V> collection) {
            throw new java.lang.UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            com.fossil.blesdk.obfuscated.C2289l4.this.mo11128a();
        }

        @DexIgnore
        public boolean contains(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11131b(obj) >= 0;
        }

        @DexIgnore
        public boolean containsAll(java.util.Collection<?> collection) {
            for (java.lang.Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean isEmpty() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c() == 0;
        }

        @DexIgnore
        public java.util.Iterator<V> iterator() {
            return new com.fossil.blesdk.obfuscated.C2289l4.C2290a(1);
        }

        @DexIgnore
        public boolean remove(java.lang.Object obj) {
            int b = com.fossil.blesdk.obfuscated.C2289l4.this.mo11131b(obj);
            if (b < 0) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(b);
            return true;
        }

        @DexIgnore
        public boolean removeAll(java.util.Collection<?> collection) {
            int c = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (collection.contains(com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(i, 1))) {
                    com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public boolean retainAll(java.util.Collection<?> collection) {
            int c = com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (!collection.contains(com.fossil.blesdk.obfuscated.C2289l4.this.mo11126a(i, 1))) {
                    com.fossil.blesdk.obfuscated.C2289l4.this.mo11129a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public int size() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo11133c();
        }

        @DexIgnore
        public java.lang.Object[] toArray() {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo13144b(1);
        }

        @DexIgnore
        public <T> T[] toArray(T[] tArr) {
            return com.fossil.blesdk.obfuscated.C2289l4.this.mo13143a(tArr, 1);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <K, V> boolean m10051a(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        for (java.lang.Object containsKey : collection) {
            if (!map.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public static <K, V> boolean m10053b(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        int size = map.size();
        for (java.lang.Object remove : collection) {
            map.remove(remove);
        }
        return size != map.size();
    }

    @DexIgnore
    /* renamed from: c */
    public static <K, V> boolean m10054c(java.util.Map<K, V> map, java.util.Collection<?> collection) {
        int size = map.size();
        java.util.Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int mo11125a(java.lang.Object obj);

    @DexIgnore
    /* renamed from: a */
    public abstract java.lang.Object mo11126a(int i, int i2);

    @DexIgnore
    /* renamed from: a */
    public abstract V mo11127a(int i, V v);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11128a();

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11129a(int i);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11130a(K k, V v);

    @DexIgnore
    /* renamed from: b */
    public abstract int mo11131b(java.lang.Object obj);

    @DexIgnore
    /* renamed from: b */
    public abstract java.util.Map<K, V> mo11132b();

    @DexIgnore
    /* renamed from: c */
    public abstract int mo11133c();

    @DexIgnore
    /* renamed from: d */
    public java.util.Set<java.util.Map.Entry<K, V>> mo13145d() {
        if (this.f7129a == null) {
            this.f7129a = new com.fossil.blesdk.obfuscated.C2289l4.C2291b();
        }
        return this.f7129a;
    }

    @DexIgnore
    /* renamed from: e */
    public java.util.Set<K> mo13146e() {
        if (this.f7130b == null) {
            this.f7130b = new com.fossil.blesdk.obfuscated.C2289l4.C2292c();
        }
        return this.f7130b;
    }

    @DexIgnore
    /* renamed from: f */
    public java.util.Collection<V> mo13147f() {
        if (this.f7131c == null) {
            this.f7131c = new com.fossil.blesdk.obfuscated.C2289l4.C2294e();
        }
        return this.f7131c;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> T[] mo13143a(T[] tArr, int i) {
        int c = mo11133c();
        if (tArr.length < c) {
            tArr = (java.lang.Object[]) java.lang.reflect.Array.newInstance(tArr.getClass().getComponentType(), c);
        }
        for (int i2 = 0; i2 < c; i2++) {
            tArr[i2] = mo11126a(i2, i);
        }
        if (tArr.length > c) {
            tArr[c] = null;
        }
        return tArr;
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object[] mo13144b(int i) {
        int c = mo11133c();
        java.lang.Object[] objArr = new java.lang.Object[c];
        for (int i2 = 0; i2 < c; i2++) {
            objArr[i2] = mo11126a(i2, i);
        }
        return objArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> boolean m10052a(java.util.Set<T> set, java.lang.Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof java.util.Set) {
            java.util.Set set2 = (java.util.Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (java.lang.ClassCastException | java.lang.NullPointerException unused) {
            }
        }
        return false;
    }
}
