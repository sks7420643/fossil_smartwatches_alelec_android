package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b8 */
public final class C1467b8 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.Locale f3681a; // = new java.util.Locale("", "");

    @DexIgnore
    /* renamed from: a */
    public static int m4810a(java.util.Locale locale) {
        byte directionality = java.lang.Character.getDirectionality(locale.getDisplayName(locale).charAt(0));
        return (directionality == 1 || directionality == 2) ? 1 : 0;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m4811b(java.util.Locale locale) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return android.text.TextUtils.getLayoutDirectionFromLocale(locale);
        }
        if (locale == null || locale.equals(f3681a)) {
            return 0;
        }
        java.lang.String b = com.fossil.blesdk.obfuscated.C3265x7.m16172b(locale);
        if (b == null) {
            return m4810a(locale);
        }
        return (b.equalsIgnoreCase("Arab") || b.equalsIgnoreCase("Hebr")) ? 1 : 0;
    }
}
