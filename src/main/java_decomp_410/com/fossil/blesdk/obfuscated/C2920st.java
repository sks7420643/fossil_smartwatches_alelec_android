package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.st */
public class C2920st implements com.fossil.blesdk.obfuscated.C2427mo<java.nio.ByteBuffer, com.fossil.blesdk.obfuscated.C3060ut> {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C2920st.C2921a f9464f; // = new com.fossil.blesdk.obfuscated.C2920st.C2921a();

    @DexIgnore
    /* renamed from: g */
    public static /* final */ com.fossil.blesdk.obfuscated.C2920st.C2922b f9465g; // = new com.fossil.blesdk.obfuscated.C2920st.C2922b();

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f9466a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.bumptech.glide.load.ImageHeaderParser> f9467b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2920st.C2922b f9468c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2920st.C2921a f9469d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2986tt f9470e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.st$a")
    /* renamed from: com.fossil.blesdk.obfuscated.st$a */
    public static class C2921a {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1570co mo16174a(com.fossil.blesdk.obfuscated.C1570co.C1571a aVar, com.fossil.blesdk.obfuscated.C1739eo eoVar, java.nio.ByteBuffer byteBuffer, int i) {
            return new com.fossil.blesdk.obfuscated.C1883go(aVar, eoVar, byteBuffer, i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.st$b")
    /* renamed from: com.fossil.blesdk.obfuscated.st$b */
    public static class C2922b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Queue<com.fossil.blesdk.obfuscated.C1816fo> f9471a; // = com.fossil.blesdk.obfuscated.C3066uw.m14928a(0);

        @DexIgnore
        /* renamed from: a */
        public synchronized com.fossil.blesdk.obfuscated.C1816fo mo16175a(java.nio.ByteBuffer byteBuffer) {
            com.fossil.blesdk.obfuscated.C1816fo poll;
            poll = this.f9471a.poll();
            if (poll == null) {
                poll = new com.fossil.blesdk.obfuscated.C1816fo();
            }
            poll.mo10977a(byteBuffer);
            return poll;
        }

        @DexIgnore
        /* renamed from: a */
        public synchronized void mo16176a(com.fossil.blesdk.obfuscated.C1816fo foVar) {
            foVar.mo10978a();
            this.f9471a.offer(foVar);
        }
    }

    @DexIgnore
    public C2920st(android.content.Context context, java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this(context, list, jqVar, gqVar, f9465g, f9464f);
    }

    @DexIgnore
    public C2920st(android.content.Context context, java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1885gq gqVar, com.fossil.blesdk.obfuscated.C2920st.C2922b bVar, com.fossil.blesdk.obfuscated.C2920st.C2921a aVar) {
        this.f9466a = context.getApplicationContext();
        this.f9467b = list;
        this.f9469d = aVar;
        this.f9470e = new com.fossil.blesdk.obfuscated.C2986tt(jqVar, gqVar);
        this.f9468c = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.nio.ByteBuffer byteBuffer, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return !((java.lang.Boolean) loVar.mo13319a(com.fossil.blesdk.obfuscated.C1447au.f3606b)).booleanValue() && com.fossil.blesdk.obfuscated.C2049io.m8572a(this.f9467b, byteBuffer) == com.bumptech.glide.load.ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3243wt mo9299a(java.nio.ByteBuffer byteBuffer, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        com.fossil.blesdk.obfuscated.C1816fo a = this.f9468c.mo16175a(byteBuffer);
        try {
            com.fossil.blesdk.obfuscated.C3243wt a2 = mo16171a(byteBuffer, i, i2, a, loVar);
            return a2;
        } finally {
            this.f9468c.mo16176a(a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C3243wt mo16171a(java.nio.ByteBuffer byteBuffer, int i, int i2, com.fossil.blesdk.obfuscated.C1816fo foVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.graphics.Bitmap.Config config;
        long a = com.fossil.blesdk.obfuscated.C2682pw.m12452a();
        try {
            com.fossil.blesdk.obfuscated.C1739eo c = foVar.mo10982c();
            if (c.mo10543b() > 0) {
                if (c.mo10544c() == 0) {
                    if (loVar.mo13319a(com.fossil.blesdk.obfuscated.C1447au.f3605a) == com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565) {
                        config = android.graphics.Bitmap.Config.RGB_565;
                    } else {
                        config = android.graphics.Bitmap.Config.ARGB_8888;
                    }
                    android.graphics.Bitmap.Config config2 = config;
                    com.fossil.blesdk.obfuscated.C1570co a2 = this.f9469d.mo16174a(this.f9470e, c, byteBuffer, m13877a(c, i, i2));
                    a2.mo9590a(config2);
                    a2.mo9591b();
                    android.graphics.Bitmap a3 = a2.mo9589a();
                    if (a3 == null) {
                        if (android.util.Log.isLoggable("BufferGifDecoder", 2)) {
                            android.util.Log.v("BufferGifDecoder", "Decoded GIF from stream in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
                        }
                        return null;
                    }
                    com.fossil.blesdk.obfuscated.C3060ut utVar = new com.fossil.blesdk.obfuscated.C3060ut(this.f9466a, a2, com.fossil.blesdk.obfuscated.C2245ks.m9758a(), i, i2, a3);
                    com.fossil.blesdk.obfuscated.C3243wt wtVar = new com.fossil.blesdk.obfuscated.C3243wt(utVar);
                    if (android.util.Log.isLoggable("BufferGifDecoder", 2)) {
                        android.util.Log.v("BufferGifDecoder", "Decoded GIF from stream in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
                    }
                    return wtVar;
                }
            }
            return null;
        } finally {
            if (android.util.Log.isLoggable("BufferGifDecoder", 2)) {
                android.util.Log.v("BufferGifDecoder", "Decoded GIF from stream in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m13877a(com.fossil.blesdk.obfuscated.C1739eo eoVar, int i, int i2) {
        int i3;
        int min = java.lang.Math.min(eoVar.mo10542a() / i2, eoVar.mo10545d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = java.lang.Integer.highestOneBit(min);
        }
        int max = java.lang.Math.max(1, i3);
        if (android.util.Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            android.util.Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + eoVar.mo10545d() + "x" + eoVar.mo10542a() + "]");
        }
        return max;
    }
}
