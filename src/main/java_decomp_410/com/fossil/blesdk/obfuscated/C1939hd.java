package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hd */
public final class C1939hd {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hd$a */
    public static class C1940a {

        @DexIgnore
        /* renamed from: a */
        public long f5721a;

        @DexIgnore
        /* renamed from: b */
        public long f5722b;
    }

    @DexIgnore
    /* renamed from: a */
    public static long m7850a(java.io.File file) throws java.io.IOException {
        java.io.RandomAccessFile randomAccessFile = new java.io.RandomAccessFile(file, "r");
        try {
            return m7851a(randomAccessFile, m7852a(randomAccessFile));
        } finally {
            randomAccessFile.close();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1939hd.C1940a m7852a(java.io.RandomAccessFile randomAccessFile) throws java.io.IOException, java.util.zip.ZipException {
        long length = randomAccessFile.length() - 22;
        long j = 0;
        if (length >= 0) {
            long j2 = length - 65536;
            if (j2 >= 0) {
                j = j2;
            }
            int reverseBytes = java.lang.Integer.reverseBytes(101010256);
            do {
                randomAccessFile.seek(length);
                if (randomAccessFile.readInt() == reverseBytes) {
                    randomAccessFile.skipBytes(2);
                    randomAccessFile.skipBytes(2);
                    randomAccessFile.skipBytes(2);
                    randomAccessFile.skipBytes(2);
                    com.fossil.blesdk.obfuscated.C1939hd.C1940a aVar = new com.fossil.blesdk.obfuscated.C1939hd.C1940a();
                    aVar.f5722b = ((long) java.lang.Integer.reverseBytes(randomAccessFile.readInt())) & 4294967295L;
                    aVar.f5721a = ((long) java.lang.Integer.reverseBytes(randomAccessFile.readInt())) & 4294967295L;
                    return aVar;
                }
                length--;
            } while (length >= j);
            throw new java.util.zip.ZipException("End Of Central Directory signature not found");
        }
        throw new java.util.zip.ZipException("File too short to be a zip file: " + randomAccessFile.length());
    }

    @DexIgnore
    /* renamed from: a */
    public static long m7851a(java.io.RandomAccessFile randomAccessFile, com.fossil.blesdk.obfuscated.C1939hd.C1940a aVar) throws java.io.IOException {
        java.util.zip.CRC32 crc32 = new java.util.zip.CRC32();
        long j = aVar.f5722b;
        randomAccessFile.seek(aVar.f5721a);
        int min = (int) java.lang.Math.min(16384, j);
        byte[] bArr = new byte[androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE];
        int read = randomAccessFile.read(bArr, 0, min);
        while (read != -1) {
            crc32.update(bArr, 0, read);
            j -= (long) read;
            if (j == 0) {
                break;
            }
            read = randomAccessFile.read(bArr, 0, (int) java.lang.Math.min(16384, j));
        }
        return crc32.getValue();
    }
}
