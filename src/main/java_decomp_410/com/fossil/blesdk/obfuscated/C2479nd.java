package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nd */
public final class C2479nd<Key, Value> {

    @DexIgnore
    /* renamed from: a */
    public Key f7718a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2723qd.C2729f f7719b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, Value> f7720c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2723qd.C2726c f7721d;
    @android.annotation.SuppressLint({"RestrictedApi"})

    @DexIgnore
    /* renamed from: e */
    public java.util.concurrent.Executor f7722e; // = com.fossil.blesdk.obfuscated.C1919h3.m7767b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nd$a */
    public static class C2480a extends com.fossil.blesdk.obfuscated.C2639pb<com.fossil.blesdk.obfuscated.C2723qd<Value>> {

        @DexIgnore
        /* renamed from: g */
        public com.fossil.blesdk.obfuscated.C2723qd<Value> f7723g;

        @DexIgnore
        /* renamed from: h */
        public com.fossil.blesdk.obfuscated.C2307ld<Key, Value> f7724h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2311c f7725i; // = new com.fossil.blesdk.obfuscated.C2479nd.C2480a.C2481a();

        @DexIgnore
        /* renamed from: j */
        public /* final */ /* synthetic */ java.lang.Object f7726j;

        @DexIgnore
        /* renamed from: k */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2307ld.C2309b f7727k;

        @DexIgnore
        /* renamed from: l */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd.C2729f f7728l;

        @DexIgnore
        /* renamed from: m */
        public /* final */ /* synthetic */ java.util.concurrent.Executor f7729m;

        @DexIgnore
        /* renamed from: n */
        public /* final */ /* synthetic */ java.util.concurrent.Executor f7730n;

        @DexIgnore
        /* renamed from: o */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd.C2726c f7731o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nd$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.nd$a$a */
        public class C2481a implements com.fossil.blesdk.obfuscated.C2307ld.C2311c {
            @DexIgnore
            public C2481a() {
            }

            @DexIgnore
            /* renamed from: a */
            public void mo13244a() {
                com.fossil.blesdk.obfuscated.C2479nd.C2480a.this.mo14654c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C2480a(java.util.concurrent.Executor executor, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2307ld.C2309b bVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar, java.util.concurrent.Executor executor2, java.util.concurrent.Executor executor3, com.fossil.blesdk.obfuscated.C2723qd.C2726c cVar) {
            super(executor);
            this.f7726j = obj;
            this.f7727k = bVar;
            this.f7728l = fVar;
            this.f7729m = executor2;
            this.f7730n = executor3;
            this.f7731o = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2723qd<Value> m11198a() {
            java.lang.Object obj = this.f7726j;
            com.fossil.blesdk.obfuscated.C2723qd<Value> qdVar = this.f7723g;
            if (qdVar != null) {
                obj = qdVar.mo12724e();
            }
            do {
                com.fossil.blesdk.obfuscated.C2307ld<Key, Value> ldVar = this.f7724h;
                if (ldVar != null) {
                    ldVar.removeInvalidatedCallback(this.f7725i);
                }
                this.f7724h = this.f7727k.create();
                this.f7724h.addInvalidatedCallback(this.f7725i);
                com.fossil.blesdk.obfuscated.C2723qd.C2727d dVar = new com.fossil.blesdk.obfuscated.C2723qd.C2727d(this.f7724h, this.f7728l);
                dVar.mo15206b(this.f7729m);
                dVar.mo15204a(this.f7730n);
                dVar.mo15202a(this.f7731o);
                dVar.mo15203a(obj);
                this.f7723g = dVar.mo15205a();
            } while (this.f7723g.mo15192h());
            return this.f7723g;
        }
    }

    @DexIgnore
    public C2479nd(com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, Value> bVar, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar) {
        if (fVar == null) {
            throw new java.lang.IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.f7720c = bVar;
            this.f7719b = fVar;
        } else {
            throw new java.lang.IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: a */
    public androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.C2723qd<Value>> mo13928a() {
        return m11195a(this.f7718a, this.f7719b, this.f7721d, this.f7720c, com.fossil.blesdk.obfuscated.C1919h3.m7769d(), this.f7722e);
    }

    @DexIgnore
    @android.annotation.SuppressLint({"RestrictedApi"})
    /* renamed from: a */
    public static <Key, Value> androidx.lifecycle.LiveData<com.fossil.blesdk.obfuscated.C2723qd<Value>> m11195a(Key key, com.fossil.blesdk.obfuscated.C2723qd.C2729f fVar, com.fossil.blesdk.obfuscated.C2723qd.C2726c cVar, com.fossil.blesdk.obfuscated.C2307ld.C2309b<Key, Value> bVar, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2) {
        com.fossil.blesdk.obfuscated.C2479nd.C2480a aVar = new com.fossil.blesdk.obfuscated.C2479nd.C2480a(executor2, key, bVar, fVar, executor, executor2, cVar);
        return aVar.mo14653b();
    }
}
