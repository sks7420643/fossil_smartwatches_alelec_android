package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jr */
public final class C2151jr<Model, Data> implements com.fossil.blesdk.obfuscated.C2912sr<Model, Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2151jr.C2152a<Data> f6571a;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.jr$a */
    public interface C2152a<Data> {
        @DexIgnore
        /* renamed from: a */
        Data mo12455a(java.lang.String str) throws java.lang.IllegalArgumentException;

        @DexIgnore
        /* renamed from: a */
        void mo12456a(Data data) throws java.io.IOException;

        @DexIgnore
        java.lang.Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.jr$b */
    public static final class C2153b<Data> implements com.fossil.blesdk.obfuscated.C2902so<Data> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.lang.String f6572e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C2151jr.C2152a<Data> f6573f;

        @DexIgnore
        /* renamed from: g */
        public Data f6574g;

        @DexIgnore
        public C2153b(java.lang.String str, com.fossil.blesdk.obfuscated.C2151jr.C2152a<Data> aVar) {
            this.f6572e = str;
            this.f6573f = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super Data> aVar) {
            try {
                this.f6574g = this.f6573f.mo12455a(this.f6572e);
                aVar.mo9252a(this.f6574g);
            } catch (java.lang.IllegalArgumentException e) {
                aVar.mo9251a((java.lang.Exception) e);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<Data> getDataClass() {
            return this.f6573f.getDataClass();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
            try {
                this.f6573f.mo12456a(this.f6574g);
            } catch (java.io.IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.jr$c */
    public static final class C2154c<Model> implements com.fossil.blesdk.obfuscated.C2984tr<Model, java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2151jr.C2152a<java.io.InputStream> f6575a; // = new com.fossil.blesdk.obfuscated.C2151jr.C2154c.C2155a(this);

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.jr$c$a")
        /* renamed from: com.fossil.blesdk.obfuscated.jr$c$a */
        public class C2155a implements com.fossil.blesdk.obfuscated.C2151jr.C2152a<java.io.InputStream> {
            @DexIgnore
            public C2155a(com.fossil.blesdk.obfuscated.C2151jr.C2154c cVar) {
            }

            @DexIgnore
            public java.lang.Class<java.io.InputStream> getDataClass() {
                return java.io.InputStream.class;
            }

            @DexIgnore
            /* renamed from: a */
            public java.io.InputStream m9146a(java.lang.String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new java.lang.IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new java.io.ByteArrayInputStream(android.util.Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new java.lang.IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new java.lang.IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            /* renamed from: a */
            public void mo12456a(java.io.InputStream inputStream) throws java.io.IOException {
                inputStream.close();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<Model, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C2151jr(this.f6575a);
        }
    }

    @DexIgnore
    public C2151jr(com.fossil.blesdk.obfuscated.C2151jr.C2152a<Data> aVar) {
        this.f6571a = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(Model model, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(model), new com.fossil.blesdk.obfuscated.C2151jr.C2153b(model.toString(), this.f6571a));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
