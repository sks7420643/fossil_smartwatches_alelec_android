package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qq0 {
    @DexIgnore
    public static qq0 b;
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> a; // = new g4();

    @DexIgnore
    public qq0(Context context) {
    }

    @DexIgnore
    public static qq0 a(Context context) {
        qq0 qq0;
        synchronized (qq0.class) {
            if (b == null) {
                b = new qq0(context.getApplicationContext());
            }
            qq0 = b;
        }
        return qq0;
    }

    @DexIgnore
    public final synchronized void b(String str, String str2) {
        Map map = this.a.get(str2);
        if (map != null) {
            if ((map.remove(str) != null) && map.isEmpty()) {
                this.a.remove(str2);
            }
        }
    }

    @DexIgnore
    public final synchronized boolean c(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final synchronized boolean a(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            map = new g4();
            this.a.put(str2, map);
        }
        if (map.put(str, false) == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final synchronized boolean a(String str) {
        return this.a.containsKey(str);
    }
}
