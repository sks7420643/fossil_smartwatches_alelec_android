package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.kc;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lc {
    @DexIgnore
    public static Application a(Activity activity) {
        Application application = activity.getApplication();
        if (application != null) {
            return application;
        }
        throw new IllegalStateException("Your activity/fragment is not yet attached to Application. You can't request ViewModel before onCreate call.");
    }

    @DexIgnore
    public static Activity a(Fragment fragment) {
        FragmentActivity activity = fragment.getActivity();
        if (activity != null) {
            return activity;
        }
        throw new IllegalStateException("Can't create ViewModelProvider for detached fragment");
    }

    @DexIgnore
    public static kc a(FragmentActivity fragmentActivity) {
        return a(fragmentActivity, (kc.b) null);
    }

    @DexIgnore
    public static kc a(Fragment fragment, kc.b bVar) {
        Application a = a(a(fragment));
        if (bVar == null) {
            bVar = kc.a.a(a);
        }
        return new kc(fragment.getViewModelStore(), bVar);
    }

    @DexIgnore
    public static kc a(FragmentActivity fragmentActivity, kc.b bVar) {
        Application a = a((Activity) fragmentActivity);
        if (bVar == null) {
            bVar = kc.a.a(a);
        }
        return new kc(fragmentActivity.getViewModelStore(), bVar);
    }
}
