package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fl0 extends ks0 implements yj0 {
    @DexIgnore
    public fl0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    public final sn0 a(sn0 sn0, ek0 ek0) throws RemoteException {
        Parcel o = o();
        ms0.a(o, (IInterface) sn0);
        ms0.a(o, (Parcelable) ek0);
        Parcel a = a(2, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
