package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wc */
public abstract class C3180wc extends android.app.Service {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ boolean f10519j; // = android.util.Log.isLoggable("MBServiceCompat", 3);

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3180wc.C3188g f10520e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<android.os.IBinder, com.fossil.blesdk.obfuscated.C3180wc.C3186f> f10521f; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C3180wc.C3186f f10522g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C3180wc.C3210q f10523h; // = new com.fossil.blesdk.obfuscated.C3180wc.C3210q();

    @DexIgnore
    /* renamed from: i */
    public android.support.p000v4.media.session.MediaSessionCompat.Token f10524i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$a */
    public class C3181a extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> {

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3186f f10525f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ java.lang.String f10526g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ android.os.Bundle f10527h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ android.os.Bundle f10528i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C3181a(java.lang.Object obj, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, java.lang.String str, android.os.Bundle bundle, android.os.Bundle bundle2) {
            super(obj);
            this.f10525f = fVar;
            this.f10526g = str;
            this.f10527h = bundle;
            this.f10528i = bundle2;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17365a(java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list) {
            if (com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10525f.f10534b.asBinder()) == this.f10525f) {
                if ((mo17384a() & 1) != 0) {
                    list = com.fossil.blesdk.obfuscated.C3180wc.this.mo17347a(list, this.f10527h);
                }
                try {
                    this.f10525f.f10534b.mo17408a(this.f10526g, list, this.f10527h, this.f10528i);
                } catch (android.os.RemoteException unused) {
                    android.util.Log.w("MBServiceCompat", "Calling onLoadChildren() failed for id=" + this.f10526g + " package=" + this.f10525f.f10533a);
                }
            } else if (com.fossil.blesdk.obfuscated.C3180wc.f10519j) {
                android.util.Log.d("MBServiceCompat", "Not sending onLoadChildren result for connection that has been disconnected. pkg=" + this.f10525f.f10533a + " id=" + this.f10526g);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$b")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$b */
    public class C3182b extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.support.p000v4.media.MediaBrowserCompat.MediaItem> {

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10530f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C3182b(com.fossil.blesdk.obfuscated.C3180wc wcVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2687q qVar) {
            super(obj);
            this.f10530f = qVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17365a(android.support.p000v4.media.MediaBrowserCompat.MediaItem mediaItem) {
            if ((mo17384a() & 2) != 0) {
                this.f10530f.mo14923b(-1, (android.os.Bundle) null);
                return;
            }
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putParcelable("media_item", mediaItem);
            this.f10530f.mo14923b(0, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$c")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$c */
    public class C3183c extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> {

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10531f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C3183c(com.fossil.blesdk.obfuscated.C3180wc wcVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2687q qVar) {
            super(obj);
            this.f10531f = qVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17365a(java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list) {
            if ((mo17384a() & 4) != 0 || list == null) {
                this.f10531f.mo14923b(-1, (android.os.Bundle) null);
                return;
            }
            android.os.Bundle bundle = new android.os.Bundle();
            bundle.putParcelableArray("search_results", (android.os.Parcelable[]) list.toArray(new android.support.p000v4.media.MediaBrowserCompat.MediaItem[0]));
            this.f10531f.mo14923b(0, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$d")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$d */
    public class C3184d extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.os.Bundle> {

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10532f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C3184d(com.fossil.blesdk.obfuscated.C3180wc wcVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2687q qVar) {
            super(obj);
            this.f10532f = qVar;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo17365a(android.os.Bundle bundle) {
            this.f10532f.mo14923b(0, bundle);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17369a(android.os.Bundle bundle) {
            this.f10532f.mo14923b(-1, bundle);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$e")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$e */
    public static final class C3185e {
        @DexIgnore
        /* renamed from: a */
        public android.os.Bundle mo17371a() {
            throw null;
        }

        @DexIgnore
        /* renamed from: b */
        public java.lang.String mo17372b() {
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$f")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$f */
    public class C3186f implements android.os.IBinder.DeathRecipient {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f10533a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10534b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.util.HashMap<java.lang.String, java.util.List<com.fossil.blesdk.obfuscated.C1775f8<android.os.IBinder, android.os.Bundle>>> f10535c; // = new java.util.HashMap<>();

        @DexIgnore
        /* renamed from: d */
        public com.fossil.blesdk.obfuscated.C3180wc.C3185e f10536d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$f$a")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$f$a */
        public class C3187a implements java.lang.Runnable {
            @DexIgnore
            public C3187a() {
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.C3186f.this;
                com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(fVar.f10534b.asBinder());
            }
        }

        @DexIgnore
        public C3186f(java.lang.String str, int i, int i2, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            this.f10533a = str;
            new com.fossil.blesdk.obfuscated.C1413ad(str, i, i2);
            this.f10534b = oVar;
        }

        @DexIgnore
        public void binderDied() {
            com.fossil.blesdk.obfuscated.C3180wc.this.f10523h.post(new com.fossil.blesdk.obfuscated.C3180wc.C3186f.C3187a());
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.wc$g */
    public interface C3188g {
        @DexIgnore
        /* renamed from: a */
        android.os.IBinder mo17375a(android.content.Intent intent);

        @DexIgnore
        /* renamed from: e */
        void mo17376e();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$h")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$h */
    public class C3189h implements com.fossil.blesdk.obfuscated.C3180wc.C3188g, com.fossil.blesdk.obfuscated.C3273xc.C3277d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.List<android.os.Bundle> f10539a; // = new java.util.ArrayList();

        @DexIgnore
        /* renamed from: b */
        public java.lang.Object f10540b;

        @DexIgnore
        /* renamed from: c */
        public android.os.Messenger f10541c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$h$a")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$h$a */
        public class C3190a extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> {

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3273xc.C3276c f10543f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C3190a(com.fossil.blesdk.obfuscated.C3180wc.C3189h hVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C3273xc.C3276c cVar) {
                super(obj);
                this.f10543f = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo17365a(java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list) {
                java.util.ArrayList arrayList;
                if (list != null) {
                    arrayList = new java.util.ArrayList();
                    for (android.support.p000v4.media.MediaBrowserCompat.MediaItem writeToParcel : list) {
                        android.os.Parcel obtain = android.os.Parcel.obtain();
                        writeToParcel.writeToParcel(obtain, 0);
                        arrayList.add(obtain);
                    }
                } else {
                    arrayList = null;
                }
                this.f10543f.mo17669a(arrayList);
            }
        }

        @DexIgnore
        public C3189h() {
        }

        @DexIgnore
        /* renamed from: a */
        public android.os.IBinder mo17375a(android.content.Intent intent) {
            return com.fossil.blesdk.obfuscated.C3273xc.m16233a(this.f10540b, intent);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17378b(java.lang.String str, com.fossil.blesdk.obfuscated.C3273xc.C3276c<java.util.List<android.os.Parcel>> cVar) {
            com.fossil.blesdk.obfuscated.C3180wc.this.mo17355a(str, (com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>>) new com.fossil.blesdk.obfuscated.C3180wc.C3189h.C3190a(this, str, cVar));
        }

        @DexIgnore
        /* renamed from: e */
        public void mo17376e() {
            this.f10540b = com.fossil.blesdk.obfuscated.C3273xc.m16234a((android.content.Context) com.fossil.blesdk.obfuscated.C3180wc.this, (com.fossil.blesdk.obfuscated.C3273xc.C3277d) this);
            com.fossil.blesdk.obfuscated.C3273xc.m16235a(this.f10540b);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3273xc.C3274a mo17377a(java.lang.String str, int i, android.os.Bundle bundle) {
            android.os.Bundle bundle2;
            android.os.IBinder iBinder;
            if (bundle == null || bundle.getInt("extra_client_version", 0) == 0) {
                bundle2 = null;
            } else {
                bundle.remove("extra_client_version");
                this.f10541c = new android.os.Messenger(com.fossil.blesdk.obfuscated.C3180wc.this.f10523h);
                bundle2 = new android.os.Bundle();
                bundle2.putInt("extra_service_version", 2);
                com.fossil.blesdk.obfuscated.C3414z5.m17266a(bundle2, "extra_messenger", this.f10541c.getBinder());
                android.support.p000v4.media.session.MediaSessionCompat.Token token = com.fossil.blesdk.obfuscated.C3180wc.this.f10524i;
                if (token != null) {
                    com.fossil.blesdk.obfuscated.C2174k a = token.mo151a();
                    if (a == null) {
                        iBinder = null;
                    } else {
                        iBinder = a.asBinder();
                    }
                    com.fossil.blesdk.obfuscated.C3414z5.m17266a(bundle2, "extra_session_binder", iBinder);
                } else {
                    this.f10539a.add(bundle2);
                }
            }
            com.fossil.blesdk.obfuscated.C3180wc wcVar = com.fossil.blesdk.obfuscated.C3180wc.this;
            com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = new com.fossil.blesdk.obfuscated.C3180wc.C3186f(str, -1, i, bundle, (com.fossil.blesdk.obfuscated.C3180wc.C3208o) null);
            wcVar.f10522g = fVar;
            com.fossil.blesdk.obfuscated.C3180wc.C3185e a2 = com.fossil.blesdk.obfuscated.C3180wc.this.mo17346a(str, i, bundle);
            com.fossil.blesdk.obfuscated.C3180wc.this.f10522g = null;
            if (a2 == null) {
                return null;
            }
            if (bundle2 == null) {
                a2.mo17371a();
                throw null;
            }
            a2.mo17371a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$i")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$i */
    public class C3191i extends com.fossil.blesdk.obfuscated.C3180wc.C3189h implements com.fossil.blesdk.obfuscated.C3352yc.C3354b {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$i$a")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$i$a */
        public class C3192a extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.support.p000v4.media.MediaBrowserCompat.MediaItem> {

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3273xc.C3276c f10545f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C3192a(com.fossil.blesdk.obfuscated.C3180wc.C3191i iVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C3273xc.C3276c cVar) {
                super(obj);
                this.f10545f = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo17365a(android.support.p000v4.media.MediaBrowserCompat.MediaItem mediaItem) {
                if (mediaItem == null) {
                    this.f10545f.mo17669a(null);
                    return;
                }
                android.os.Parcel obtain = android.os.Parcel.obtain();
                mediaItem.writeToParcel(obtain, 0);
                this.f10545f.mo17669a(obtain);
            }
        }

        @DexIgnore
        public C3191i() {
            super();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17380a(java.lang.String str, com.fossil.blesdk.obfuscated.C3273xc.C3276c<android.os.Parcel> cVar) {
            com.fossil.blesdk.obfuscated.C3180wc.this.mo17361b(str, new com.fossil.blesdk.obfuscated.C3180wc.C3191i.C3192a(this, str, cVar));
        }

        @DexIgnore
        /* renamed from: e */
        public void mo17376e() {
            this.f10540b = com.fossil.blesdk.obfuscated.C3352yc.m16800a(com.fossil.blesdk.obfuscated.C3180wc.this, this);
            com.fossil.blesdk.obfuscated.C3273xc.m16235a(this.f10540b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$j")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$j */
    public class C3193j extends com.fossil.blesdk.obfuscated.C3180wc.C3191i implements com.fossil.blesdk.obfuscated.C3423zc.C3426c {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$j$a")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$j$a */
        public class C3194a extends com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> {

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3423zc.C3425b f10547f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C3194a(com.fossil.blesdk.obfuscated.C3180wc.C3193j jVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C3423zc.C3425b bVar) {
                super(obj);
                this.f10547f = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void mo17365a(java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list) {
                java.util.ArrayList arrayList;
                if (list != null) {
                    arrayList = new java.util.ArrayList();
                    for (android.support.p000v4.media.MediaBrowserCompat.MediaItem writeToParcel : list) {
                        android.os.Parcel obtain = android.os.Parcel.obtain();
                        writeToParcel.writeToParcel(obtain, 0);
                        arrayList.add(obtain);
                    }
                } else {
                    arrayList = null;
                }
                this.f10547f.mo18461a(arrayList, mo17384a());
            }
        }

        @DexIgnore
        public C3193j() {
            super();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17382a(java.lang.String str, com.fossil.blesdk.obfuscated.C3423zc.C3425b bVar, android.os.Bundle bundle) {
            com.fossil.blesdk.obfuscated.C3180wc.this.mo17356a(str, (com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>>) new com.fossil.blesdk.obfuscated.C3180wc.C3193j.C3194a(this, str, bVar), bundle);
        }

        @DexIgnore
        /* renamed from: e */
        public void mo17376e() {
            this.f10540b = com.fossil.blesdk.obfuscated.C3423zc.m17324a(com.fossil.blesdk.obfuscated.C3180wc.this, this);
            com.fossil.blesdk.obfuscated.C3273xc.m16235a(this.f10540b);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$k")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$k */
    public class C3195k extends com.fossil.blesdk.obfuscated.C3180wc.C3193j {
        @DexIgnore
        public C3195k(com.fossil.blesdk.obfuscated.C3180wc wcVar) {
            super();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$l")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$l */
    public class C3196l implements com.fossil.blesdk.obfuscated.C3180wc.C3188g {

        @DexIgnore
        /* renamed from: a */
        public android.os.Messenger f10548a;

        @DexIgnore
        public C3196l() {
        }

        @DexIgnore
        /* renamed from: a */
        public android.os.IBinder mo17375a(android.content.Intent intent) {
            if ("android.media.browse.MediaBrowserService".equals(intent.getAction())) {
                return this.f10548a.getBinder();
            }
            return null;
        }

        @DexIgnore
        /* renamed from: e */
        public void mo17376e() {
            this.f10548a = new android.os.Messenger(com.fossil.blesdk.obfuscated.C3180wc.this.f10523h);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$m")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$m */
    public static class C3197m<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Object f10550a;

        @DexIgnore
        /* renamed from: b */
        public boolean f10551b;

        @DexIgnore
        /* renamed from: c */
        public boolean f10552c;

        @DexIgnore
        /* renamed from: d */
        public boolean f10553d;

        @DexIgnore
        /* renamed from: e */
        public int f10554e;

        @DexIgnore
        public C3197m(java.lang.Object obj) {
            this.f10550a = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17385a(int i) {
            this.f10554e = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17365a(T t) {
            throw null;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17387b(T t) {
            if (this.f10552c || this.f10553d) {
                throw new java.lang.IllegalStateException("sendResult() called when either sendResult() or sendError() had already been called for: " + this.f10550a);
            }
            this.f10552c = true;
            mo17365a(t);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17384a() {
            return this.f10554e;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17369a(android.os.Bundle bundle) {
            throw new java.lang.UnsupportedOperationException("It is not supported to send an error for " + this.f10550a);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17386b(android.os.Bundle bundle) {
            if (this.f10552c || this.f10553d) {
                throw new java.lang.IllegalStateException("sendError() called when either sendResult() or sendError() had already been called for: " + this.f10550a);
            }
            this.f10553d = true;
            mo17369a(bundle);
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo17388b() {
            return this.f10551b || this.f10552c || this.f10553d;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$n */
    public class C3198n {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$a")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$a */
        public class C3199a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10556e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10557f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ int f10558g;

            @DexIgnore
            /* renamed from: h */
            public /* final */ /* synthetic */ int f10559h;

            @DexIgnore
            /* renamed from: i */
            public /* final */ /* synthetic */ android.os.Bundle f10560i;

            @DexIgnore
            public C3199a(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, int i, int i2, android.os.Bundle bundle) {
                this.f10556e = oVar;
                this.f10557f = str;
                this.f10558g = i;
                this.f10559h = i2;
                this.f10560i = bundle;
            }

            @DexIgnore
            public void run() {
                android.os.IBinder asBinder = this.f10556e.asBinder();
                com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(asBinder);
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = new com.fossil.blesdk.obfuscated.C3180wc.C3186f(this.f10557f, this.f10558g, this.f10559h, this.f10560i, this.f10556e);
                com.fossil.blesdk.obfuscated.C3180wc wcVar = com.fossil.blesdk.obfuscated.C3180wc.this;
                wcVar.f10522g = fVar;
                fVar.f10536d = wcVar.mo17346a(this.f10557f, this.f10559h, this.f10560i);
                com.fossil.blesdk.obfuscated.C3180wc wcVar2 = com.fossil.blesdk.obfuscated.C3180wc.this;
                wcVar2.f10522g = null;
                if (fVar.f10536d == null) {
                    android.util.Log.i("MBServiceCompat", "No root for client " + this.f10557f + " from service " + com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3199a.class.getName());
                    try {
                        this.f10556e.mo17407a();
                    } catch (android.os.RemoteException unused) {
                        android.util.Log.w("MBServiceCompat", "Calling onConnectFailed() failed. Ignoring. pkg=" + this.f10557f);
                    }
                } else {
                    try {
                        wcVar2.f10521f.put(asBinder, fVar);
                        asBinder.linkToDeath(fVar, 0);
                        if (com.fossil.blesdk.obfuscated.C3180wc.this.f10524i != null) {
                            fVar.f10536d.mo17372b();
                            throw null;
                        }
                    } catch (android.os.RemoteException unused2) {
                        android.util.Log.w("MBServiceCompat", "Calling onConnect() failed. Dropping client. pkg=" + this.f10557f);
                        com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(asBinder);
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$b")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$b */
        public class C3200b implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10562e;

            @DexIgnore
            public C3200b(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
                this.f10562e = oVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f remove = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(this.f10562e.asBinder());
                if (remove != null) {
                    remove.f10534b.asBinder().unlinkToDeath(remove, 0);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$c")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$c */
        public class C3201c implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10564e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10565f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ android.os.IBinder f10566g;

            @DexIgnore
            /* renamed from: h */
            public /* final */ /* synthetic */ android.os.Bundle f10567h;

            @DexIgnore
            public C3201c(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, android.os.IBinder iBinder, android.os.Bundle bundle) {
                this.f10564e = oVar;
                this.f10565f = str;
                this.f10566g = iBinder;
                this.f10567h = bundle;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10564e.asBinder());
                if (fVar == null) {
                    android.util.Log.w("MBServiceCompat", "addSubscription for callback that isn't registered id=" + this.f10565f);
                    return;
                }
                com.fossil.blesdk.obfuscated.C3180wc.this.mo17353a(this.f10565f, fVar, this.f10566g, this.f10567h);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$d")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$d */
        public class C3202d implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10569e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10570f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ android.os.IBinder f10571g;

            @DexIgnore
            public C3202d(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, android.os.IBinder iBinder) {
                this.f10569e = oVar;
                this.f10570f = str;
                this.f10571g = iBinder;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10569e.asBinder());
                if (fVar == null) {
                    android.util.Log.w("MBServiceCompat", "removeSubscription for callback that isn't registered id=" + this.f10570f);
                } else if (!com.fossil.blesdk.obfuscated.C3180wc.this.mo17358a(this.f10570f, fVar, this.f10571g)) {
                    android.util.Log.w("MBServiceCompat", "removeSubscription called for " + this.f10570f + " which is not subscribed");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$e")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$e */
        public class C3203e implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10573e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10574f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10575g;

            @DexIgnore
            public C3203e(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, com.fossil.blesdk.obfuscated.C2687q qVar) {
                this.f10573e = oVar;
                this.f10574f = str;
                this.f10575g = qVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10573e.asBinder());
                if (fVar == null) {
                    android.util.Log.w("MBServiceCompat", "getMediaItem for callback that isn't registered id=" + this.f10574f);
                    return;
                }
                com.fossil.blesdk.obfuscated.C3180wc.this.mo17354a(this.f10574f, fVar, this.f10575g);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$f")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$f */
        public class C3204f implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10577e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10578f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ int f10579g;

            @DexIgnore
            /* renamed from: h */
            public /* final */ /* synthetic */ int f10580h;

            @DexIgnore
            /* renamed from: i */
            public /* final */ /* synthetic */ android.os.Bundle f10581i;

            @DexIgnore
            public C3204f(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, int i, int i2, android.os.Bundle bundle) {
                this.f10577e = oVar;
                this.f10578f = str;
                this.f10579g = i;
                this.f10580h = i2;
                this.f10581i = bundle;
            }

            @DexIgnore
            public void run() {
                android.os.IBinder asBinder = this.f10577e.asBinder();
                com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(asBinder);
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = new com.fossil.blesdk.obfuscated.C3180wc.C3186f(this.f10578f, this.f10579g, this.f10580h, this.f10581i, this.f10577e);
                com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.put(asBinder, fVar);
                try {
                    asBinder.linkToDeath(fVar, 0);
                } catch (android.os.RemoteException unused) {
                    android.util.Log.w("MBServiceCompat", "IBinder is already dead.");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$g")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$g */
        public class C3205g implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10583e;

            @DexIgnore
            public C3205g(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
                this.f10583e = oVar;
            }

            @DexIgnore
            public void run() {
                android.os.IBinder asBinder = this.f10583e.asBinder();
                com.fossil.blesdk.obfuscated.C3180wc.C3186f remove = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.remove(asBinder);
                if (remove != null) {
                    asBinder.unlinkToDeath(remove, 0);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$h")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$h */
        public class C3206h implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10585e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10586f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ android.os.Bundle f10587g;

            @DexIgnore
            /* renamed from: h */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10588h;

            @DexIgnore
            public C3206h(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C2687q qVar) {
                this.f10585e = oVar;
                this.f10586f = str;
                this.f10587g = bundle;
                this.f10588h = qVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10585e.asBinder());
                if (fVar == null) {
                    android.util.Log.w("MBServiceCompat", "search for callback that isn't registered query=" + this.f10586f);
                    return;
                }
                com.fossil.blesdk.obfuscated.C3180wc.this.mo17359b(this.f10586f, this.f10587g, fVar, this.f10588h);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$n$i")
        /* renamed from: com.fossil.blesdk.obfuscated.wc$n$i */
        public class C3207i implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3180wc.C3208o f10590e;

            @DexIgnore
            /* renamed from: f */
            public /* final */ /* synthetic */ java.lang.String f10591f;

            @DexIgnore
            /* renamed from: g */
            public /* final */ /* synthetic */ android.os.Bundle f10592g;

            @DexIgnore
            /* renamed from: h */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2687q f10593h;

            @DexIgnore
            public C3207i(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C2687q qVar) {
                this.f10590e = oVar;
                this.f10591f = str;
                this.f10592g = bundle;
                this.f10593h = qVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10521f.get(this.f10590e.asBinder());
                if (fVar == null) {
                    android.util.Log.w("MBServiceCompat", "sendCustomAction for callback that isn't registered action=" + this.f10591f + ", extras=" + this.f10592g);
                    return;
                }
                com.fossil.blesdk.obfuscated.C3180wc.this.mo17350a(this.f10591f, this.f10592g, fVar, this.f10593h);
            }
        }

        @DexIgnore
        public C3198n() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17391a(java.lang.String str, int i, int i2, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            if (com.fossil.blesdk.obfuscated.C3180wc.this.mo17357a(str, i2)) {
                com.fossil.blesdk.obfuscated.C3180wc.C3210q qVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10523h;
                com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3199a aVar = new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3199a(oVar, str, i, i2, bundle);
                qVar.mo17411a(aVar);
                return;
            }
            throw new java.lang.IllegalArgumentException("Package/uid mismatch: uid=" + i2 + " package=" + str);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17396b(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            com.fossil.blesdk.obfuscated.C3180wc.this.f10523h.mo17411a(new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3205g(oVar));
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17397b(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C2687q qVar, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            if (!android.text.TextUtils.isEmpty(str) && qVar != null) {
                com.fossil.blesdk.obfuscated.C3180wc.C3210q qVar2 = com.fossil.blesdk.obfuscated.C3180wc.this.f10523h;
                com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3207i iVar = new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3207i(oVar, str, bundle, qVar);
                qVar2.mo17411a(iVar);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17389a(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            com.fossil.blesdk.obfuscated.C3180wc.this.f10523h.mo17411a(new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3200b(oVar));
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17393a(java.lang.String str, android.os.IBinder iBinder, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            com.fossil.blesdk.obfuscated.C3180wc.C3210q qVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10523h;
            com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3201c cVar = new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3201c(oVar, str, iBinder, bundle);
            qVar.mo17411a(cVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17394a(java.lang.String str, android.os.IBinder iBinder, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            com.fossil.blesdk.obfuscated.C3180wc.this.f10523h.mo17411a(new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3202d(oVar, str, iBinder));
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17395a(java.lang.String str, com.fossil.blesdk.obfuscated.C2687q qVar, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            if (!android.text.TextUtils.isEmpty(str) && qVar != null) {
                com.fossil.blesdk.obfuscated.C3180wc.this.f10523h.mo17411a(new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3203e(oVar, str, qVar));
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17390a(com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar, java.lang.String str, int i, int i2, android.os.Bundle bundle) {
            com.fossil.blesdk.obfuscated.C3180wc.C3210q qVar = com.fossil.blesdk.obfuscated.C3180wc.this.f10523h;
            com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3204f fVar = new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3204f(oVar, str, i, i2, bundle);
            qVar.mo17411a(fVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17392a(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C2687q qVar, com.fossil.blesdk.obfuscated.C3180wc.C3208o oVar) {
            if (!android.text.TextUtils.isEmpty(str) && qVar != null) {
                com.fossil.blesdk.obfuscated.C3180wc.C3210q qVar2 = com.fossil.blesdk.obfuscated.C3180wc.this.f10523h;
                com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3206h hVar = new com.fossil.blesdk.obfuscated.C3180wc.C3198n.C3206h(oVar, str, bundle, qVar);
                qVar2.mo17411a(hVar);
            }
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.wc$o */
    public interface C3208o {
        @DexIgnore
        /* renamed from: a */
        void mo17407a() throws android.os.RemoteException;

        @DexIgnore
        /* renamed from: a */
        void mo17408a(java.lang.String str, java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle bundle, android.os.Bundle bundle2) throws android.os.RemoteException;

        @DexIgnore
        android.os.IBinder asBinder();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$p")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$p */
    public static class C3209p implements com.fossil.blesdk.obfuscated.C3180wc.C3208o {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.os.Messenger f10595a;

        @DexIgnore
        public C3209p(android.os.Messenger messenger) {
            this.f10595a = messenger;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17407a() throws android.os.RemoteException {
            mo17410a(2, (android.os.Bundle) null);
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this.f10595a.getBinder();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17408a(java.lang.String str, java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle bundle, android.os.Bundle bundle2) throws android.os.RemoteException {
            android.os.Bundle bundle3 = new android.os.Bundle();
            bundle3.putString("data_media_item_id", str);
            bundle3.putBundle("data_options", bundle);
            bundle3.putBundle("data_notify_children_changed_options", bundle2);
            if (list != null) {
                bundle3.putParcelableArrayList("data_media_item_list", list instanceof java.util.ArrayList ? (java.util.ArrayList) list : new java.util.ArrayList(list));
            }
            mo17410a(3, bundle3);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo17410a(int i, android.os.Bundle bundle) throws android.os.RemoteException {
            android.os.Message obtain = android.os.Message.obtain();
            obtain.what = i;
            obtain.arg1 = 2;
            obtain.setData(bundle);
            this.f10595a.send(obtain);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wc$q")
    /* renamed from: com.fossil.blesdk.obfuscated.wc$q */
    public final class C3210q extends android.os.Handler {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3180wc.C3198n f10596a; // = new com.fossil.blesdk.obfuscated.C3180wc.C3198n();

        @DexIgnore
        public C3210q() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17411a(java.lang.Runnable runnable) {
            if (java.lang.Thread.currentThread() == getLooper().getThread()) {
                runnable.run();
            } else {
                post(runnable);
            }
        }

        @DexIgnore
        public void handleMessage(android.os.Message message) {
            android.os.Bundle data = message.getData();
            switch (message.what) {
                case 1:
                    android.os.Bundle bundle = data.getBundle("data_root_hints");
                    android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle);
                    this.f10596a.mo17391a(data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle, (com.fossil.blesdk.obfuscated.C3180wc.C3208o) new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 2:
                    this.f10596a.mo17389a(new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 3:
                    android.os.Bundle bundle2 = data.getBundle("data_options");
                    android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle2);
                    this.f10596a.mo17393a(data.getString("data_media_item_id"), com.fossil.blesdk.obfuscated.C3414z5.m17265a(data, "data_callback_token"), bundle2, (com.fossil.blesdk.obfuscated.C3180wc.C3208o) new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 4:
                    this.f10596a.mo17394a(data.getString("data_media_item_id"), com.fossil.blesdk.obfuscated.C3414z5.m17265a(data, "data_callback_token"), (com.fossil.blesdk.obfuscated.C3180wc.C3208o) new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 5:
                    this.f10596a.mo17395a(data.getString("data_media_item_id"), (com.fossil.blesdk.obfuscated.C2687q) data.getParcelable("data_result_receiver"), (com.fossil.blesdk.obfuscated.C3180wc.C3208o) new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 6:
                    android.os.Bundle bundle3 = data.getBundle("data_root_hints");
                    android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle3);
                    com.fossil.blesdk.obfuscated.C3180wc.C3198n nVar = this.f10596a;
                    com.fossil.blesdk.obfuscated.C3180wc.C3209p pVar = new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo);
                    nVar.mo17390a((com.fossil.blesdk.obfuscated.C3180wc.C3208o) pVar, data.getString("data_package_name"), data.getInt("data_calling_pid"), data.getInt("data_calling_uid"), bundle3);
                    return;
                case 7:
                    this.f10596a.mo17396b(new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 8:
                    android.os.Bundle bundle4 = data.getBundle("data_search_extras");
                    android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle4);
                    this.f10596a.mo17392a(data.getString("data_search_query"), bundle4, (com.fossil.blesdk.obfuscated.C2687q) data.getParcelable("data_result_receiver"), (com.fossil.blesdk.obfuscated.C3180wc.C3208o) new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                case 9:
                    android.os.Bundle bundle5 = data.getBundle("data_custom_action_extras");
                    android.support.p000v4.media.session.MediaSessionCompat.m150a(bundle5);
                    this.f10596a.mo17397b(data.getString("data_custom_action"), bundle5, (com.fossil.blesdk.obfuscated.C2687q) data.getParcelable("data_result_receiver"), new com.fossil.blesdk.obfuscated.C3180wc.C3209p(message.replyTo));
                    return;
                default:
                    android.util.Log.w("MBServiceCompat", "Unhandled message: " + message + "\n  Service version: " + 2 + "\n  Client version: " + message.arg1);
                    return;
            }
        }

        @DexIgnore
        public boolean sendMessageAtTime(android.os.Message message, long j) {
            android.os.Bundle data = message.getData();
            data.setClassLoader(android.support.p000v4.media.MediaBrowserCompat.class.getClassLoader());
            data.putInt("data_calling_uid", android.os.Binder.getCallingUid());
            data.putInt("data_calling_pid", android.os.Binder.getCallingPid());
            return super.sendMessageAtTime(message, j);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C3180wc.C3185e mo17346a(java.lang.String str, int i, android.os.Bundle bundle);

    @DexIgnore
    /* renamed from: a */
    public void mo17348a(java.lang.String str) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17349a(java.lang.String str, android.os.Bundle bundle) {
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo17355a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> mVar);

    @DexIgnore
    /* renamed from: a */
    public void mo17356a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> mVar, android.os.Bundle bundle) {
        mVar.mo17385a(1);
        mo17355a(str, mVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17361b(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.support.p000v4.media.MediaBrowserCompat.MediaItem> mVar) {
        mVar.mo17385a(2);
        mVar.mo17387b(null);
    }

    @DexIgnore
    public void dump(java.io.FileDescriptor fileDescriptor, java.io.PrintWriter printWriter, java.lang.String[] strArr) {
    }

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        return this.f10520e.mo17375a(intent);
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.f10520e = new com.fossil.blesdk.obfuscated.C3180wc.C3195k(this);
        } else if (i >= 26) {
            this.f10520e = new com.fossil.blesdk.obfuscated.C3180wc.C3193j();
        } else if (i >= 23) {
            this.f10520e = new com.fossil.blesdk.obfuscated.C3180wc.C3191i();
        } else if (i >= 21) {
            this.f10520e = new com.fossil.blesdk.obfuscated.C3180wc.C3189h();
        } else {
            this.f10520e = new com.fossil.blesdk.obfuscated.C3180wc.C3196l();
        }
        this.f10520e.mo17376e();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17351a(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.os.Bundle> mVar) {
        mVar.mo17386b((android.os.Bundle) null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17360b(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>> mVar) {
        mVar.mo17385a(4);
        mVar.mo17387b(null);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17357a(java.lang.String str, int i) {
        if (str == null) {
            return false;
        }
        for (java.lang.String equals : getPackageManager().getPackagesForUid(i)) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17359b(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, com.fossil.blesdk.obfuscated.C2687q qVar) {
        com.fossil.blesdk.obfuscated.C3180wc.C3183c cVar = new com.fossil.blesdk.obfuscated.C3180wc.C3183c(this, str, qVar);
        mo17360b(str, bundle, cVar);
        if (!cVar.mo17388b()) {
            throw new java.lang.IllegalStateException("onSearch must call detach() or sendResult() before returning for query=" + str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17353a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, android.os.IBinder iBinder, android.os.Bundle bundle) {
        java.util.List<com.fossil.blesdk.obfuscated.C1775f8> list = fVar.f10535c.get(str);
        if (list == null) {
            list = new java.util.ArrayList<>();
        }
        for (com.fossil.blesdk.obfuscated.C1775f8 f8Var : list) {
            if (iBinder == f8Var.f5093a && com.fossil.blesdk.obfuscated.C3104vc.m15174a(bundle, (android.os.Bundle) f8Var.f5094b)) {
                return;
            }
        }
        list.add(new com.fossil.blesdk.obfuscated.C1775f8(iBinder, bundle));
        fVar.f10535c.put(str, list);
        mo17352a(str, fVar, bundle, (android.os.Bundle) null);
        mo17349a(str, bundle);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17358a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, android.os.IBinder iBinder) {
        boolean z = true;
        boolean z2 = false;
        if (iBinder == null) {
            try {
                if (fVar.f10535c.remove(str) == null) {
                    z = false;
                }
                return z;
            } finally {
                mo17348a(str);
            }
        } else {
            java.util.List list = fVar.f10535c.get(str);
            if (list != null) {
                java.util.Iterator it = list.iterator();
                while (it.hasNext()) {
                    if (iBinder == ((com.fossil.blesdk.obfuscated.C1775f8) it.next()).f5093a) {
                        it.remove();
                        z2 = true;
                    }
                }
                if (list.size() == 0) {
                    fVar.f10535c.remove(str);
                }
            }
            mo17348a(str);
            return z2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17352a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, android.os.Bundle bundle, android.os.Bundle bundle2) {
        com.fossil.blesdk.obfuscated.C3180wc.C3181a aVar = new com.fossil.blesdk.obfuscated.C3180wc.C3181a(str, fVar, str, bundle, bundle2);
        if (bundle == null) {
            mo17355a(str, (com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>>) aVar);
        } else {
            mo17356a(str, (com.fossil.blesdk.obfuscated.C3180wc.C3197m<java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem>>) aVar, bundle);
        }
        if (!aVar.mo17388b()) {
            throw new java.lang.IllegalStateException("onLoadChildren must call detach() or sendResult() before returning for package=" + fVar.f10533a + " id=" + str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> mo17347a(java.util.List<android.support.p000v4.media.MediaBrowserCompat.MediaItem> list, android.os.Bundle bundle) {
        if (list == null) {
            return null;
        }
        int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
        int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
        if (i == -1 && i2 == -1) {
            return list;
        }
        int i3 = i2 * i;
        int i4 = i3 + i2;
        if (i < 0 || i2 < 1 || i3 >= list.size()) {
            return java.util.Collections.emptyList();
        }
        if (i4 > list.size()) {
            i4 = list.size();
        }
        return list.subList(i3, i4);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17354a(java.lang.String str, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, com.fossil.blesdk.obfuscated.C2687q qVar) {
        com.fossil.blesdk.obfuscated.C3180wc.C3182b bVar = new com.fossil.blesdk.obfuscated.C3180wc.C3182b(this, str, qVar);
        mo17361b(str, bVar);
        if (!bVar.mo17388b()) {
            throw new java.lang.IllegalStateException("onLoadItem must call detach() or sendResult() before returning for id=" + str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17350a(java.lang.String str, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C3180wc.C3186f fVar, com.fossil.blesdk.obfuscated.C2687q qVar) {
        com.fossil.blesdk.obfuscated.C3180wc.C3184d dVar = new com.fossil.blesdk.obfuscated.C3180wc.C3184d(this, str, qVar);
        mo17351a(str, bundle, (com.fossil.blesdk.obfuscated.C3180wc.C3197m<android.os.Bundle>) dVar);
        if (!dVar.mo17388b()) {
            throw new java.lang.IllegalStateException("onCustomAction must call detach() or sendResult() or sendError() before returning for action=" + str + " extras=" + bundle);
        }
    }
}
