package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.aj */
public final class C1419aj {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.lang.String f3495b; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("Data");

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C1419aj f3496c; // = new com.fossil.blesdk.obfuscated.C1419aj.C1420a().mo8756a();

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.Object> f3497a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.aj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.aj$a */
    public static final class C1420a {

        @DexIgnore
        /* renamed from: a */
        public java.util.Map<java.lang.String, java.lang.Object> f3498a; // = new java.util.HashMap();

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1419aj.C1420a mo8754a(java.lang.String str, java.lang.String str2) {
            this.f3498a.put(str, str2);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1419aj.C1420a mo8752a(com.fossil.blesdk.obfuscated.C1419aj ajVar) {
            mo8755a(ajVar.f3497a);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1419aj.C1420a mo8755a(java.util.Map<java.lang.String, java.lang.Object> map) {
            for (java.util.Map.Entry next : map.entrySet()) {
                mo8753a((java.lang.String) next.getKey(), next.getValue());
            }
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1419aj.C1420a mo8753a(java.lang.String str, java.lang.Object obj) {
            if (obj == null) {
                this.f3498a.put(str, (java.lang.Object) null);
            } else {
                java.lang.Class<?> cls = obj.getClass();
                if (cls == java.lang.Boolean.class || cls == java.lang.Byte.class || cls == java.lang.Integer.class || cls == java.lang.Long.class || cls == java.lang.Float.class || cls == java.lang.Double.class || cls == java.lang.String.class || cls == java.lang.Boolean[].class || cls == java.lang.Byte[].class || cls == java.lang.Integer[].class || cls == java.lang.Long[].class || cls == java.lang.Float[].class || cls == java.lang.Double[].class || cls == java.lang.String[].class) {
                    this.f3498a.put(str, obj);
                } else if (cls == boolean[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4449a((boolean[]) obj));
                } else if (cls == byte[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4450a((byte[]) obj));
                } else if (cls == int[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4453a((int[]) obj));
                } else if (cls == long[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4454a((long[]) obj));
                } else if (cls == float[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4452a((float[]) obj));
                } else if (cls == double[].class) {
                    this.f3498a.put(str, com.fossil.blesdk.obfuscated.C1419aj.m4451a((double[]) obj));
                } else {
                    throw new java.lang.IllegalArgumentException(java.lang.String.format("Key %s has invalid type %s", new java.lang.Object[]{str, cls}));
                }
            }
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1419aj mo8756a() {
            com.fossil.blesdk.obfuscated.C1419aj ajVar = new com.fossil.blesdk.obfuscated.C1419aj((java.util.Map<java.lang.String, ?>) this.f3498a);
            com.fossil.blesdk.obfuscated.C1419aj.m4448a(ajVar);
            return ajVar;
        }
    }

    @DexIgnore
    public C1419aj() {
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo8747a(java.lang.String str) {
        java.lang.Object obj = this.f3497a.get(str);
        if (obj instanceof java.lang.String) {
            return (java.lang.String) obj;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8749b() {
        return this.f3497a.size();
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C1419aj.class != obj.getClass()) {
            return false;
        }
        return this.f3497a.equals(((com.fossil.blesdk.obfuscated.C1419aj) obj).f3497a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3497a.hashCode() * 31;
    }

    @DexIgnore
    public C1419aj(com.fossil.blesdk.obfuscated.C1419aj ajVar) {
        this.f3497a = new java.util.HashMap(ajVar.f3497a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[SYNTHETIC, Splitter:B:24:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006b A[SYNTHETIC, Splitter:B:36:0x006b] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:14:0x0035=Splitter:B:14:0x0035, B:28:0x0058=Splitter:B:28:0x0058} */
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1419aj m4455b(byte[] bArr) {
        java.io.ObjectInputStream objectInputStream;
        java.lang.Throwable e;
        if (bArr.length <= 10240) {
            java.util.HashMap hashMap = new java.util.HashMap();
            java.io.ByteArrayInputStream byteArrayInputStream = new java.io.ByteArrayInputStream(bArr);
            try {
                objectInputStream = new java.io.ObjectInputStream(byteArrayInputStream);
                try {
                    for (int readInt = objectInputStream.readInt(); readInt > 0; readInt--) {
                        hashMap.put(objectInputStream.readUTF(), objectInputStream.readObject());
                    }
                    try {
                        objectInputStream.close();
                    } catch (java.io.IOException e2) {
                        android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e2);
                    }
                } catch (java.io.IOException | java.lang.ClassNotFoundException e3) {
                    e = e3;
                    try {
                        android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e);
                        if (objectInputStream != null) {
                        }
                        byteArrayInputStream.close();
                        return new com.fossil.blesdk.obfuscated.C1419aj((java.util.Map<java.lang.String, ?>) hashMap);
                    } catch (Throwable th) {
                        th = th;
                        if (objectInputStream != null) {
                        }
                        try {
                            byteArrayInputStream.close();
                        } catch (java.io.IOException e4) {
                            android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e4);
                        }
                        throw th;
                    }
                }
                try {
                    byteArrayInputStream.close();
                } catch (java.io.IOException e5) {
                    android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e5);
                }
            } catch (java.io.IOException | java.lang.ClassNotFoundException e6) {
                java.lang.Throwable th2 = e6;
                objectInputStream = null;
                e = th2;
                android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e);
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (java.io.IOException e7) {
                        android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e7);
                    }
                }
                byteArrayInputStream.close();
                return new com.fossil.blesdk.obfuscated.C1419aj((java.util.Map<java.lang.String, ?>) hashMap);
            } catch (Throwable th3) {
                objectInputStream = null;
                th = th3;
                if (objectInputStream != null) {
                    try {
                        objectInputStream.close();
                    } catch (java.io.IOException e8) {
                        android.util.Log.e(f3495b, "Error in Data#fromByteArray: ", e8);
                    }
                }
                byteArrayInputStream.close();
                throw th;
            }
            return new com.fossil.blesdk.obfuscated.C1419aj((java.util.Map<java.lang.String, ?>) hashMap);
        }
        throw new java.lang.IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
    }

    @DexIgnore
    public C1419aj(java.util.Map<java.lang.String, ?> map) {
        this.f3497a = new java.util.HashMap(map);
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.Object> mo8748a() {
        return java.util.Collections.unmodifiableMap(this.f3497a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0078 A[SYNTHETIC, Splitter:B:31:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x008f A[SYNTHETIC, Splitter:B:41:0x008f] */
    /* renamed from: a */
    public static byte[] m4448a(com.fossil.blesdk.obfuscated.C1419aj ajVar) {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        java.io.ObjectOutputStream objectOutputStream = null;
        try {
            java.io.ObjectOutputStream objectOutputStream2 = new java.io.ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream2.writeInt(ajVar.mo8749b());
                for (java.util.Map.Entry next : ajVar.f3497a.entrySet()) {
                    objectOutputStream2.writeUTF((java.lang.String) next.getKey());
                    objectOutputStream2.writeObject(next.getValue());
                }
                try {
                    objectOutputStream2.close();
                } catch (java.io.IOException e) {
                    android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e);
                }
                try {
                    byteArrayOutputStream.close();
                } catch (java.io.IOException e2) {
                    android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e2);
                }
                if (byteArrayOutputStream.size() <= 10240) {
                    return byteArrayOutputStream.toByteArray();
                }
                throw new java.lang.IllegalStateException("Data cannot occupy more than 10240 bytes when serialized");
            } catch (java.io.IOException e3) {
                e = e3;
                objectOutputStream = objectOutputStream2;
                try {
                    android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    if (objectOutputStream != null) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (java.io.IOException e4) {
                        android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e4);
                    }
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (java.io.IOException e5) {
                        android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e5);
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (objectOutputStream2 != null) {
                    try {
                        objectOutputStream2.close();
                    } catch (java.io.IOException e6) {
                        android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e6);
                    }
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (java.io.IOException e7) {
            e = e7;
            android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e);
            byte[] byteArray2 = byteArrayOutputStream.toByteArray();
            if (objectOutputStream != null) {
                try {
                    objectOutputStream.close();
                } catch (java.io.IOException e8) {
                    android.util.Log.e(f3495b, "Error in Data#toByteArray: ", e8);
                }
            }
            byteArrayOutputStream.close();
            return byteArray2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Boolean[] m4449a(boolean[] zArr) {
        java.lang.Boolean[] boolArr = new java.lang.Boolean[zArr.length];
        for (int i = 0; i < zArr.length; i++) {
            boolArr[i] = java.lang.Boolean.valueOf(zArr[i]);
        }
        return boolArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Byte[] m4450a(byte[] bArr) {
        java.lang.Byte[] bArr2 = new java.lang.Byte[bArr.length];
        for (int i = 0; i < bArr.length; i++) {
            bArr2[i] = java.lang.Byte.valueOf(bArr[i]);
        }
        return bArr2;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Integer[] m4453a(int[] iArr) {
        java.lang.Integer[] numArr = new java.lang.Integer[iArr.length];
        for (int i = 0; i < iArr.length; i++) {
            numArr[i] = java.lang.Integer.valueOf(iArr[i]);
        }
        return numArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Long[] m4454a(long[] jArr) {
        java.lang.Long[] lArr = new java.lang.Long[jArr.length];
        for (int i = 0; i < jArr.length; i++) {
            lArr[i] = java.lang.Long.valueOf(jArr[i]);
        }
        return lArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Float[] m4452a(float[] fArr) {
        java.lang.Float[] fArr2 = new java.lang.Float[fArr.length];
        for (int i = 0; i < fArr.length; i++) {
            fArr2[i] = java.lang.Float.valueOf(fArr[i]);
        }
        return fArr2;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Double[] m4451a(double[] dArr) {
        java.lang.Double[] dArr2 = new java.lang.Double[dArr.length];
        for (int i = 0; i < dArr.length; i++) {
            dArr2[i] = java.lang.Double.valueOf(dArr[i]);
        }
        return dArr2;
    }
}
