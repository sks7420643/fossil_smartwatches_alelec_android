package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface g81 extends IInterface {
    @DexIgnore
    Bundle zza(Bundle bundle) throws RemoteException;
}
