package com.fossil.blesdk.obfuscated;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.facebook.GraphRequest;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pg1 extends pk1 {
    @DexIgnore
    public /* final */ qg1 c; // = new qg1(this, getContext(), "google_app_measurement_local.db");
    @DexIgnore
    public boolean d;

    @DexIgnore
    public pg1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final SQLiteDatabase A() throws SQLiteException {
        if (this.d) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.d = true;
        return null;
    }

    @DexIgnore
    public final void B() {
        f();
        e();
        try {
            int delete = A().delete(GraphRequest.DEBUG_MESSAGES_KEY, (String) null, (String[]) null) + 0;
            if (delete > 0) {
                d().A().a("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            d().s().a("Error resetting local analytics data. error", e);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v1, types: [int, boolean] */
    /* JADX WARNING: type inference failed for: r8v0 */
    /* JADX WARNING: type inference failed for: r3v2 */
    /* JADX WARNING: type inference failed for: r8v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r8v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r8v5 */
    /* JADX WARNING: type inference failed for: r8v6 */
    /* JADX WARNING: type inference failed for: r8v7 */
    /* JADX WARNING: type inference failed for: r8v11 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cc, code lost:
        r8 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e1, code lost:
        if (r8.inTransaction() != false) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e3, code lost:
        r8.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f6, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00fb, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00ff, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0100, code lost:
        r10 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x010c, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0111, code lost:
        r10.close();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x0130 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0130 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:9:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00dd A[SYNTHETIC, Splitter:B:55:0x00dd] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0130 A[SYNTHETIC] */
    public final boolean a(int i, byte[] bArr) {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Cursor cursor2;
        f();
        e();
        Object r3 = 0;
        if (this.d) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", Integer.valueOf(i));
        contentValues.put("entry", bArr);
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        while (i3 < i2) {
            Object r8 = 0;
            try {
                sQLiteDatabase = A();
                if (sQLiteDatabase == null) {
                    try {
                        this.d = true;
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return r3;
                    } catch (SQLiteFullException e) {
                        e = e;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        cursor = null;
                        r8 = sQLiteDatabase;
                        if (r8 != 0) {
                        }
                        d().s().a("Error writing entry to local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (r8 == 0) {
                        }
                        i3++;
                        r3 = 0;
                        i2 = 5;
                    } catch (Throwable th) {
                        th = th;
                        cursor = null;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    sQLiteDatabase.beginTransaction();
                    long j = 0;
                    cursor = sQLiteDatabase.rawQuery("select count(1) from messages", (String[]) null);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                j = cursor.getLong(r3);
                            }
                        } catch (SQLiteFullException e3) {
                            e = e3;
                            r8 = cursor;
                            cursor2 = r8;
                            d().s().a("Error writing entry to local database", e);
                            this.d = true;
                            cursor2 = r8;
                            if (r8 != 0) {
                                r8.close();
                            }
                            if (sQLiteDatabase == null) {
                                sQLiteDatabase.close();
                            }
                            i3++;
                            r3 = 0;
                            i2 = 5;
                        } catch (SQLiteDatabaseLockedException unused2) {
                            r8 = cursor;
                            try {
                                cursor2 = r8;
                                SystemClock.sleep((long) i4);
                                cursor2 = r8;
                                i4 += 20;
                                if (r8 != 0) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                i3++;
                                r3 = 0;
                                i2 = 5;
                            } catch (Throwable th2) {
                                th = th2;
                                cursor = cursor2;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                throw th;
                            }
                        } catch (SQLiteException e4) {
                            e = e4;
                            r8 = sQLiteDatabase;
                            if (r8 != 0) {
                            }
                            d().s().a("Error writing entry to local database", e);
                            this.d = true;
                            if (cursor != null) {
                            }
                            if (r8 == 0) {
                            }
                            i3++;
                            r3 = 0;
                            i2 = 5;
                        } catch (Throwable th3) {
                            th = th3;
                            if (cursor != null) {
                                cursor.close();
                            }
                            if (sQLiteDatabase != null) {
                                sQLiteDatabase.close();
                            }
                            throw th;
                        }
                    }
                    if (j >= 100000) {
                        d().s().a("Data loss, local db full");
                        long j2 = (100000 - j) + 1;
                        String[] strArr = new String[1];
                        strArr[r3] = Long.toString(j2);
                        long delete = (long) sQLiteDatabase.delete(GraphRequest.DEBUG_MESSAGES_KEY, "rowid in (select rowid from messages order by rowid asc limit ?)", strArr);
                        if (delete != j2) {
                            d().s().a("Different delete count than expected in local db. expected, received, difference", Long.valueOf(j2), Long.valueOf(delete), Long.valueOf(j2 - delete));
                        }
                    }
                    sQLiteDatabase.insertOrThrow(GraphRequest.DEBUG_MESSAGES_KEY, (String) null, contentValues);
                    sQLiteDatabase.setTransactionSuccessful();
                    sQLiteDatabase.endTransaction();
                    if (cursor != null) {
                        cursor.close();
                    }
                    if (sQLiteDatabase == null) {
                        return true;
                    }
                    sQLiteDatabase.close();
                    return true;
                }
            } catch (SQLiteFullException e5) {
                e = e5;
                sQLiteDatabase = null;
                cursor2 = r8;
                d().s().a("Error writing entry to local database", e);
                this.d = true;
                cursor2 = r8;
                if (r8 != 0) {
                }
                if (sQLiteDatabase == null) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (SQLiteDatabaseLockedException unused3) {
                sQLiteDatabase = null;
                cursor2 = r8;
                SystemClock.sleep((long) i4);
                cursor2 = r8;
                i4 += 20;
                if (r8 != 0) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (SQLiteException e6) {
                e = e6;
                cursor = null;
                if (r8 != 0) {
                }
                d().s().a("Error writing entry to local database", e);
                this.d = true;
                if (cursor != null) {
                }
                if (r8 == 0) {
                }
                i3++;
                r3 = 0;
                i2 = 5;
            } catch (Throwable th4) {
                th = th4;
                sQLiteDatabase = null;
                cursor = null;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        d().v().a("Failed to write entry to local database");
        return false;
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final boolean a(hg1 hg1) {
        Parcel obtain = Parcel.obtain();
        hg1.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(0, marshall);
        }
        d().v().a("Event is too long for local database. Sending event directly to service");
        return false;
    }

    @DexIgnore
    public final boolean a(kl1 kl1) {
        Parcel obtain = Parcel.obtain();
        kl1.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return a(1, marshall);
        }
        d().v().a("User property too long for local database. Sending directly to service");
        return false;
    }

    @DexIgnore
    public final boolean a(vl1 vl1) {
        j();
        byte[] a = nl1.a((Parcelable) vl1);
        if (a.length <= 131072) {
            return a(2, a);
        }
        d().v().a("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:57|58|59|60) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:72|73|74|75) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:42|43|44|45|162) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0179, code lost:
        r5 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        r10 = null;
        r5 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0040, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0041, code lost:
        r10 = null;
        r5 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0046, code lost:
        r10 = null;
        r5 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        d().s().a("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r13.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        d().s().a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r13.recycle();
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        d().s().a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        r13.recycle();
        r0 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x00a2 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x00d4 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:72:0x0108 */
    /* JADX WARNING: Removed duplicated region for block: B:102:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0189 A[SYNTHETIC, Splitter:B:113:0x0189] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x01a1  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x01b4  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:146:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x01d7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x01d7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x01d7 A[SYNTHETIC] */
    public final List<jk0> a(int i) {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Parcel obtain;
        Parcel obtain2;
        Parcel obtain3;
        e();
        f();
        if (this.d) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!getContext().getDatabasePath("google_app_measurement_local.db").exists()) {
            return arrayList;
        }
        int i2 = 5;
        int i3 = 0;
        int i4 = 5;
        while (i3 < i2) {
            try {
                SQLiteDatabase A = A();
                if (A == null) {
                    try {
                        this.d = true;
                        if (A != null) {
                            A.close();
                        }
                        return null;
                    } catch (SQLiteFullException e) {
                        e = e;
                        sQLiteDatabase = A;
                        cursor = null;
                        d().s().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        i3++;
                        i2 = 5;
                    } catch (SQLiteDatabaseLockedException unused) {
                    } catch (SQLiteException e2) {
                        e = e2;
                        sQLiteDatabase = A;
                        cursor = null;
                        if (sQLiteDatabase != null) {
                        }
                        d().s().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        i3++;
                        i2 = 5;
                    } catch (Throwable th) {
                        th = th;
                        sQLiteDatabase = A;
                        cursor = null;
                        if (cursor != null) {
                        }
                        if (sQLiteDatabase != null) {
                        }
                        throw th;
                    }
                } else {
                    A.beginTransaction();
                    sQLiteDatabase = A;
                    try {
                        cursor = A.query(GraphRequest.DEBUG_MESSAGES_KEY, new String[]{"rowid", "type", "entry"}, (String) null, (String[]) null, (String) null, (String) null, "rowid asc", Integer.toString(100));
                        long j = -1;
                        while (cursor.moveToNext()) {
                            try {
                                j = cursor.getLong(0);
                                int i5 = cursor.getInt(1);
                                byte[] blob = cursor.getBlob(2);
                                if (i5 == 0) {
                                    obtain3 = Parcel.obtain();
                                    obtain3.unmarshall(blob, 0, blob.length);
                                    obtain3.setDataPosition(0);
                                    hg1 createFromParcel = hg1.CREATOR.createFromParcel(obtain3);
                                    obtain3.recycle();
                                    if (createFromParcel != null) {
                                        arrayList.add(createFromParcel);
                                    }
                                } else if (i5 == 1) {
                                    obtain2 = Parcel.obtain();
                                    obtain2.unmarshall(blob, 0, blob.length);
                                    obtain2.setDataPosition(0);
                                    kl1 kl1 = kl1.CREATOR.createFromParcel(obtain2);
                                    obtain2.recycle();
                                    if (kl1 != null) {
                                        arrayList.add(kl1);
                                    }
                                } else if (i5 == 2) {
                                    obtain = Parcel.obtain();
                                    obtain.unmarshall(blob, 0, blob.length);
                                    obtain.setDataPosition(0);
                                    vl1 vl1 = vl1.CREATOR.createFromParcel(obtain);
                                    obtain.recycle();
                                    if (vl1 != null) {
                                        arrayList.add(vl1);
                                    }
                                } else {
                                    d().s().a("Unknown record type in local database");
                                }
                            } catch (SQLiteFullException e3) {
                                e = e3;
                            } catch (SQLiteDatabaseLockedException unused2) {
                                SystemClock.sleep((long) i4);
                                i4 += 20;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase == null) {
                                }
                                i3++;
                                i2 = 5;
                            } catch (SQLiteException e4) {
                                e = e4;
                                if (sQLiteDatabase != null) {
                                }
                                d().s().a("Error reading entries from local database", e);
                                this.d = true;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                i3++;
                                i2 = 5;
                            } catch (Throwable th2) {
                                obtain3.recycle();
                                throw th2;
                            }
                        }
                        if (sQLiteDatabase.delete(GraphRequest.DEBUG_MESSAGES_KEY, "rowid <= ?", new String[]{Long.toString(j)}) < arrayList.size()) {
                            d().s().a("Fewer entries removed from local database than expected");
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                        sQLiteDatabase.endTransaction();
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        return arrayList;
                    } catch (SQLiteFullException e5) {
                        e = e5;
                        cursor = null;
                        d().s().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        i3++;
                        i2 = 5;
                    } catch (SQLiteDatabaseLockedException unused3) {
                        cursor = null;
                        SystemClock.sleep((long) i4);
                        i4 += 20;
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase == null) {
                            sQLiteDatabase.close();
                        }
                        i3++;
                        i2 = 5;
                    } catch (SQLiteException e6) {
                        e = e6;
                        cursor = null;
                        if (sQLiteDatabase != null) {
                            try {
                                if (sQLiteDatabase.inTransaction()) {
                                    sQLiteDatabase.endTransaction();
                                }
                            } catch (Throwable th3) {
                                th = th3;
                                if (cursor != null) {
                                }
                                if (sQLiteDatabase != null) {
                                }
                                throw th;
                            }
                        }
                        d().s().a("Error reading entries from local database", e);
                        this.d = true;
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        i3++;
                        i2 = 5;
                    } catch (Throwable th4) {
                        th = th4;
                        cursor = null;
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (sQLiteDatabase != null) {
                            sQLiteDatabase.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteFullException e7) {
                e = e7;
                sQLiteDatabase = null;
                cursor = null;
                d().s().a("Error reading entries from local database", e);
                this.d = true;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                i2 = 5;
            } catch (SQLiteDatabaseLockedException unused4) {
                sQLiteDatabase = null;
                cursor = null;
                SystemClock.sleep((long) i4);
                i4 += 20;
                if (cursor != null) {
                }
                if (sQLiteDatabase == null) {
                }
                i3++;
                i2 = 5;
            } catch (SQLiteException e8) {
                e = e8;
                sQLiteDatabase = null;
                cursor = null;
                if (sQLiteDatabase != null) {
                }
                d().s().a("Error reading entries from local database", e);
                this.d = true;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                i3++;
                i2 = 5;
            } catch (Throwable th5) {
                th = th5;
                sQLiteDatabase = null;
                cursor = null;
                if (cursor != null) {
                }
                if (sQLiteDatabase != null) {
                }
                throw th;
            }
        }
        d().v().a("Failed to read events from database in reasonable time");
        return null;
    }
}
