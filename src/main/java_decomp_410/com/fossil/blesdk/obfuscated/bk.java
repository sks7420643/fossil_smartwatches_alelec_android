package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bk implements pj {
    @DexIgnore
    public static /* final */ String f; // = dj.a("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context e;

    @DexIgnore
    public bk(Context context) {
        this.e = context.getApplicationContext();
    }

    @DexIgnore
    public void a(hl... hlVarArr) {
        for (hl a : hlVarArr) {
            a(a);
        }
    }

    @DexIgnore
    public void a(String str) {
        this.e.startService(xj.c(this.e, str));
    }

    @DexIgnore
    public final void a(hl hlVar) {
        dj.a().a(f, String.format("Scheduling work with workSpecId %s", new Object[]{hlVar.a}), new Throwable[0]);
        this.e.startService(xj.b(this.e, hlVar.a));
    }
}
