package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tg */
public class C2964tg {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.tg$a */
    public interface C2965a {
        @DexIgnore
        void onAnimationPause(android.animation.Animator animator);

        @DexIgnore
        void onAnimationResume(android.animation.Animator animator);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14194a(android.animation.Animator animator, android.animation.AnimatorListenerAdapter animatorListenerAdapter) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            animator.addPauseListener(animatorListenerAdapter);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m14195b(android.animation.Animator animator) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            animator.resume();
            return;
        }
        java.util.ArrayList<android.animation.Animator.AnimatorListener> listeners = animator.getListeners();
        if (listeners != null) {
            int size = listeners.size();
            for (int i = 0; i < size; i++) {
                android.animation.Animator.AnimatorListener animatorListener = listeners.get(i);
                if (animatorListener instanceof com.fossil.blesdk.obfuscated.C2964tg.C2965a) {
                    ((com.fossil.blesdk.obfuscated.C2964tg.C2965a) animatorListener).onAnimationResume(animator);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14193a(android.animation.Animator animator) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            animator.pause();
            return;
        }
        java.util.ArrayList<android.animation.Animator.AnimatorListener> listeners = animator.getListeners();
        if (listeners != null) {
            int size = listeners.size();
            for (int i = 0; i < size; i++) {
                android.animation.Animator.AnimatorListener animatorListener = listeners.get(i);
                if (animatorListener instanceof com.fossil.blesdk.obfuscated.C2964tg.C2965a) {
                    ((com.fossil.blesdk.obfuscated.C2964tg.C2965a) animatorListener).onAnimationPause(animator);
                }
            }
        }
    }
}
