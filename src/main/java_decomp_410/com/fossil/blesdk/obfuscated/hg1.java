package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hg1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hg1> CREATOR; // = new ig1();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ eg1 f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore
    public hg1(String str, eg1 eg1, String str2, long j) {
        this.e = str;
        this.f = eg1;
        this.g = str2;
        this.h = j;
    }

    @DexIgnore
    public final String toString() {
        String str = this.g;
        String str2 = this.e;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 21 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("origin=");
        sb.append(str);
        sb.append(",name=");
        sb.append(str2);
        sb.append(",params=");
        sb.append(valueOf);
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, this.e, false);
        kk0.a(parcel, 3, (Parcelable) this.f, i, false);
        kk0.a(parcel, 4, this.g, false);
        kk0.a(parcel, 5, this.h);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public hg1(hg1 hg1, long j) {
        bk0.a(hg1);
        this.e = hg1.e;
        this.f = hg1.f;
        this.g = hg1.g;
        this.h = j;
    }
}
