package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h5 */
public class C1924h5 {

    @DexIgnore
    /* renamed from: a */
    public int f5679a;

    @DexIgnore
    /* renamed from: b */
    public int f5680b;

    @DexIgnore
    /* renamed from: c */
    public int f5681c;

    @DexIgnore
    /* renamed from: d */
    public int f5682d;

    @DexIgnore
    /* renamed from: e */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C1924h5.C1925a> f5683e; // = new java.util.ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.h5$a")
    /* renamed from: com.fossil.blesdk.obfuscated.h5$a */
    public static class C1925a {

        @DexIgnore
        /* renamed from: a */
        public androidx.constraintlayout.solver.widgets.ConstraintAnchor f5684a;

        @DexIgnore
        /* renamed from: b */
        public androidx.constraintlayout.solver.widgets.ConstraintAnchor f5685b;

        @DexIgnore
        /* renamed from: c */
        public int f5686c;

        @DexIgnore
        /* renamed from: d */
        public androidx.constraintlayout.solver.widgets.ConstraintAnchor.Strength f5687d;

        @DexIgnore
        /* renamed from: e */
        public int f5688e;

        @DexIgnore
        public C1925a(androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor) {
            this.f5684a = constraintAnchor;
            this.f5685b = constraintAnchor.mo1269g();
            this.f5686c = constraintAnchor.mo1264b();
            this.f5687d = constraintAnchor.mo1268f();
            this.f5688e = constraintAnchor.mo1259a();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11596a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
            constraintWidget.mo1284a(this.f5684a.mo1270h()).mo1263a(this.f5685b, this.f5686c, this.f5687d, this.f5688e);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo11597b(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
            this.f5684a = constraintWidget.mo1284a(this.f5684a.mo1270h());
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = this.f5684a;
            if (constraintAnchor != null) {
                this.f5685b = constraintAnchor.mo1269g();
                this.f5686c = this.f5684a.mo1264b();
                this.f5687d = this.f5684a.mo1268f();
                this.f5688e = this.f5684a.mo1259a();
                return;
            }
            this.f5685b = null;
            this.f5686c = 0;
            this.f5687d = androidx.constraintlayout.solver.widgets.ConstraintAnchor.Strength.STRONG;
            this.f5688e = 0;
        }
    }

    @DexIgnore
    public C1924h5(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        this.f5679a = constraintWidget.mo1357w();
        this.f5680b = constraintWidget.mo1358x();
        this.f5681c = constraintWidget.mo1352t();
        this.f5682d = constraintWidget.mo1332j();
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintAnchor> c = constraintWidget.mo1313c();
        int size = c.size();
        for (int i = 0; i < size; i++) {
            this.f5683e.add(new com.fossil.blesdk.obfuscated.C1924h5.C1925a(c.get(i)));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11594a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        constraintWidget.mo1351s(this.f5679a);
        constraintWidget.mo1353t(this.f5680b);
        constraintWidget.mo1345p(this.f5681c);
        constraintWidget.mo1329h(this.f5682d);
        int size = this.f5683e.size();
        for (int i = 0; i < size; i++) {
            this.f5683e.get(i).mo11596a(constraintWidget);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11595b(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        this.f5679a = constraintWidget.mo1357w();
        this.f5680b = constraintWidget.mo1358x();
        this.f5681c = constraintWidget.mo1352t();
        this.f5682d = constraintWidget.mo1332j();
        int size = this.f5683e.size();
        for (int i = 0; i < size; i++) {
            this.f5683e.get(i).mo11597b(constraintWidget);
        }
    }
}
