package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ci2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ai2 r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public ci2(Object obj, View view, int i, ConstraintLayout constraintLayout, ai2 ai2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = ai2;
        a((ViewDataBinding) this.r);
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
    }

    @DexIgnore
    public static ci2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qa.a());
    }

    @DexIgnore
    @Deprecated
    public static ci2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ci2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_heart_rate_week, viewGroup, z, obj);
    }
}
