package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t6 */
public final class C2943t6 {
    /*
    static {
        new java.lang.ThreadLocal();
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static int m14030a(int i, int i2) {
        return 255 - (((255 - i2) * (255 - i)) / 255);
    }

    @DexIgnore
    /* renamed from: b */
    public static int m14032b(int i, int i2) {
        int alpha = android.graphics.Color.alpha(i2);
        int alpha2 = android.graphics.Color.alpha(i);
        int a = m14030a(alpha2, alpha);
        return android.graphics.Color.argb(a, m14031a(android.graphics.Color.red(i), alpha2, android.graphics.Color.red(i2), alpha, a), m14031a(android.graphics.Color.green(i), alpha2, android.graphics.Color.green(i2), alpha, a), m14031a(android.graphics.Color.blue(i), alpha2, android.graphics.Color.blue(i2), alpha, a));
    }

    @DexIgnore
    /* renamed from: c */
    public static int m14033c(int i, int i2) {
        if (i2 >= 0 && i2 <= 255) {
            return (i & 16777215) | (i2 << 24);
        }
        throw new java.lang.IllegalArgumentException("alpha must be between 0 and 255.");
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14031a(int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            return 0;
        }
        return (((i * 255) * i2) + ((i3 * i4) * (255 - i2))) / (i5 * 255);
    }
}
