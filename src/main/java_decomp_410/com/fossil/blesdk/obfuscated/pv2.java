package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pv2 extends xs3 implements sy2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public /* final */ pa m; // = new v62(this);
    @DexIgnore
    public tr3<kb2> n;
    @DexIgnore
    public ry2 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return pv2.q;
        }

        @DexIgnore
        public final pv2 b() {
            return new pv2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ pv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ kb2 b;

        @DexIgnore
        public b(pv2 pv2, kb2 kb2) {
            this.a = pv2;
            this.b = kb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ry2 a2 = pv2.a(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.v;
            kd4.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.u;
            kd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ pv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ kb2 b;

        @DexIgnore
        public c(pv2 pv2, kb2 kb2) {
            this.a = pv2;
            this.b = kb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ry2 a2 = pv2.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            kd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.u;
            kd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ pv2 a;
        @DexIgnore
        public /* final */ /* synthetic */ kb2 b;

        @DexIgnore
        public d(pv2 pv2, kb2 kb2) {
            this.a = pv2;
            this.b = kb2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ry2 a2 = pv2.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            kd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.v;
            kd4.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ pv2 e;

        @DexIgnore
        public e(pv2 pv2) {
            this.e = pv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            pv2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public f(kb2 kb2, Ref$ObjectRef ref$ObjectRef) {
            this.e = kb2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.e.q;
            kd4.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.c(3);
                        kb2 kb2 = this.e;
                        kd4.a((Object) kb2, "it");
                        View d = kb2.d();
                        kd4.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = pv2.class.getSimpleName();
        kd4.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ry2 a(pv2 pv2) {
        ry2 ry2 = pv2.o;
        if (ry2 != null) {
            return ry2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void d(String str) {
        kd4.b(str, "title");
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        kb2 kb2 = (kb2) qa.a(layoutInflater, R.layout.fragment_do_not_disturb_scheduled_time, viewGroup, false, this.m);
        kb2.r.setOnClickListener(new e(this));
        kd4.a((Object) kb2, "binding");
        a(kb2);
        this.n = new tr3<>(this, kb2);
        return kb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        ry2 ry2 = this.o;
        if (ry2 != null) {
            ry2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ry2 ry2 = this.o;
        if (ry2 != null) {
            ry2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                ref$ObjectRef.element = new f(a2, ref$ObjectRef);
                kd4.a((Object) a2, "it");
                View d2 = a2.d();
                kd4.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i) {
        ry2 ry2 = this.o;
        if (ry2 != null) {
            ry2.a(i);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ry2 ry2) {
        kd4.b(ry2, "presenter");
        this.o = ry2;
    }

    @DexIgnore
    public final void a(kb2 kb2) {
        NumberPicker numberPicker = kb2.t;
        kd4.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = kb2.t;
        kd4.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        kb2.t.setOnValueChangedListener(new b(this, kb2));
        NumberPicker numberPicker3 = kb2.v;
        kd4.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = kb2.v;
        kd4.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        kb2.v.setOnValueChangedListener(new c(this, kb2));
        String[] strArr = {sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm)};
        NumberPicker numberPicker5 = kb2.u;
        kd4.a((Object) numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = kb2.u;
        kd4.a((Object) numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        kb2.u.setDisplayedValues(strArr);
        kb2.u.setOnValueChangedListener(new d(this, kb2));
    }

    @DexIgnore
    public void a(int i) {
        int i2;
        int i3 = i / 60;
        int i4 = i % 60;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 == 0) {
            i3 = 12;
        }
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                kd4.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                kd4.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i4);
                NumberPicker numberPicker3 = a2.u;
                kd4.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
