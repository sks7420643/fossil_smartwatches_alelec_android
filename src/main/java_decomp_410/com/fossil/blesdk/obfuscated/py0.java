package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class py0 extends rt0 implements oy0 {
    @DexIgnore
    public py0() {
        super("com.google.android.gms.clearcut.internal.IClearcutLoggerCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 1:
                f((Status) mu0.a(parcel, Status.CREATOR));
                return true;
            case 2:
                g((Status) mu0.a(parcel, Status.CREATOR));
                throw null;
            case 3:
                a((Status) mu0.a(parcel, Status.CREATOR), parcel.readLong());
                throw null;
            case 4:
                h((Status) mu0.a(parcel, Status.CREATOR));
                throw null;
            case 5:
                b((Status) mu0.a(parcel, Status.CREATOR), parcel.readLong());
                throw null;
            case 6:
                a((Status) mu0.a(parcel, Status.CREATOR), (rd0[]) parcel.createTypedArray(rd0.CREATOR));
                throw null;
            case 7:
                a((DataHolder) mu0.a(parcel, DataHolder.CREATOR));
                throw null;
            case 8:
                a((Status) mu0.a(parcel, Status.CREATOR), (pd0) mu0.a(parcel, pd0.CREATOR));
                throw null;
            case 9:
                b((Status) mu0.a(parcel, Status.CREATOR), (pd0) mu0.a(parcel, pd0.CREATOR));
                throw null;
            default:
                return false;
        }
    }
}
