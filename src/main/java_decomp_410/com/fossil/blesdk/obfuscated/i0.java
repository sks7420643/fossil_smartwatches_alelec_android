package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import com.fossil.blesdk.obfuscated.h1;
import com.fossil.blesdk.obfuscated.p1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i0 extends ActionBar {
    @DexIgnore
    public j2 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Window.Callback c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public ArrayList<ActionBar.a> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Runnable g; // = new a();
    @DexIgnore
    public /* final */ Toolbar.e h; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            i0.this.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Toolbar.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            return i0.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements h1.a {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(h1 h1Var) {
            i0 i0Var = i0.this;
            if (i0Var.c == null) {
                return;
            }
            if (i0Var.a.a()) {
                i0.this.c.onPanelClosed(108, h1Var);
            } else if (i0.this.c.onPreparePanel(0, (View) null, h1Var)) {
                i0.this.c.onMenuOpened(108, h1Var);
            }
        }

        @DexIgnore
        public boolean a(h1 h1Var, MenuItem menuItem) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends z0 {
        @DexIgnore
        public e(Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        public View onCreatePanelView(int i) {
            if (i == 0) {
                return new View(i0.this.a.getContext());
            }
            return super.onCreatePanelView(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, View view, Menu menu) {
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (onPreparePanel) {
                i0 i0Var = i0.this;
                if (!i0Var.b) {
                    i0Var.a.b();
                    i0.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    @DexIgnore
    public i0(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new a3(toolbar, false);
        this.c = new e(callback);
        this.a.setWindowCallback(this.c);
        toolbar.setOnMenuItemClickListener(this.h);
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void a(float f2) {
        f9.b((View) this.a.k(), f2);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void c(boolean z) {
    }

    @DexIgnore
    public void d(boolean z) {
        a(z ? 4 : 0, 4);
    }

    @DexIgnore
    public void e(boolean z) {
    }

    @DexIgnore
    public boolean e() {
        return this.a.e();
    }

    @DexIgnore
    public boolean f() {
        if (!this.a.h()) {
            return false;
        }
        this.a.collapseActionView();
        return true;
    }

    @DexIgnore
    public int g() {
        return this.a.l();
    }

    @DexIgnore
    public Context h() {
        return this.a.getContext();
    }

    @DexIgnore
    public boolean i() {
        this.a.k().removeCallbacks(this.g);
        f9.a((View) this.a.k(), this.g);
        return true;
    }

    @DexIgnore
    public void j() {
        this.a.k().removeCallbacks(this.g);
    }

    @DexIgnore
    public boolean k() {
        return this.a.f();
    }

    @DexIgnore
    public final Menu l() {
        if (!this.d) {
            this.a.a((p1.a) new c(), (h1.a) new d());
            this.d = true;
        }
        return this.a.i();
    }

    @DexIgnore
    public Window.Callback m() {
        return this.c;
    }

    @DexIgnore
    public void n() {
        Menu l = l();
        h1 h1Var = l instanceof h1 ? (h1) l : null;
        if (h1Var != null) {
            h1Var.s();
        }
        try {
            l.clear();
            if (!this.c.onCreatePanelMenu(0, l) || !this.c.onPreparePanel(0, (View) null, l)) {
                l.clear();
            }
        } finally {
            if (h1Var != null) {
                h1Var.r();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements p1.a {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean a(h1 h1Var) {
            Window.Callback callback = i0.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, h1Var);
            return true;
        }

        @DexIgnore
        public void a(h1 h1Var, boolean z) {
            if (!this.e) {
                this.e = true;
                i0.this.a.g();
                Window.Callback callback = i0.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, h1Var);
                }
                this.e = false;
            }
        }
    }

    @DexIgnore
    public void a(Configuration configuration) {
        super.a(configuration);
    }

    @DexIgnore
    public void b(boolean z) {
        if (z != this.e) {
            this.e = z;
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.get(i).a(z);
            }
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.a.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.a.a((i & i2) | ((~i2) & this.a.l()));
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            k();
        }
        return true;
    }

    @DexIgnore
    public boolean a(int i, KeyEvent keyEvent) {
        Menu l = l();
        if (l == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        l.setQwertyMode(z);
        return l.performShortcut(i, keyEvent, 0);
    }
}
