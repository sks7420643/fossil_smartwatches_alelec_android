package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tw0 implements Comparable<tw0>, Map.Entry<K, V> {
    @DexIgnore
    public /* final */ K e;
    @DexIgnore
    public V f;
    @DexIgnore
    public /* final */ /* synthetic */ mw0 g;

    @DexIgnore
    public tw0(mw0 mw0, K k, V v) {
        this.g = mw0;
        this.e = k;
        this.f = v;
    }

    @DexIgnore
    public tw0(mw0 mw0, Map.Entry<K, V> entry) {
        this(mw0, (Comparable) entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }

    @DexIgnore
    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((tw0) obj).getKey());
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return a(this.e, entry.getKey()) && a(this.f, entry.getValue());
    }

    @DexIgnore
    public final /* synthetic */ Object getKey() {
        return this.e;
    }

    @DexIgnore
    public final V getValue() {
        return this.f;
    }

    @DexIgnore
    public final int hashCode() {
        K k = this.e;
        int i = 0;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.f;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public final V setValue(V v) {
        this.g.e();
        V v2 = this.f;
        this.f = v;
        return v2;
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.e);
        String valueOf2 = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append(SimpleComparison.EQUAL_TO_OPERATION);
        sb.append(valueOf2);
        return sb.toString();
    }
}
