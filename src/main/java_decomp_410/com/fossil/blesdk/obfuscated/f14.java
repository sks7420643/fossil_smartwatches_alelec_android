package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f14 implements p24 {
    @DexIgnore
    public /* final */ /* synthetic */ c14 a;

    @DexIgnore
    public f14(c14 c14) {
        this.a = c14;
    }

    @DexIgnore
    public void a() {
        j04.c();
        if (g14.i().f > 0) {
            j04.a(this.a.d, -1);
        }
    }

    @DexIgnore
    public void b() {
        g14.i().a(this.a.a, (p24) null, this.a.c, true);
        j04.d();
    }
}
