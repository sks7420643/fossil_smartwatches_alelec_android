package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ps2 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ Category a;
    @DexIgnore
    public int b;
    @DexIgnore
    public ArrayList<Category> c;
    @DexIgnore
    public c d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public FlexibleTextView a;
        @DexIgnore
        public /* final */ /* synthetic */ ps2 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ps2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ps2$a$a  reason: collision with other inner class name */
        public static final class C0099a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0099a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.b.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CategoriesAdapter", "onItemClick pos=" + this.e.getAdapterPosition() + " currentPos=" + this.e.b.b);
                    a aVar = this.e;
                    aVar.b.b = aVar.getAdapterPosition();
                    if (this.e.b.b != 0) {
                        c b = this.e.b.b();
                        if (b != null) {
                            Object obj = this.e.b.c.get(this.e.b.b);
                            kd4.a(obj, "mData[mSelectedIndex]");
                            b.a((Category) obj);
                            return;
                        }
                        return;
                    }
                    c b2 = this.e.b.b();
                    if (b2 != null) {
                        b2.a();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ps2 ps2, View view) {
            super(view);
            kd4.b(view, "view");
            this.b = ps2;
            View findViewById = view.findViewById(R.id.ftv_category);
            kd4.a((Object) findViewById, "view.findViewById(R.id.ftv_category)");
            this.a = (FlexibleTextView) findViewById;
            this.a.setOnClickListener(new C0099a(this));
        }

        @DexIgnore
        public final void a(Category category, int i) {
            kd4.b(category, "category");
            Context context = this.a.getContext();
            FlexibleTextView flexibleTextView = this.a;
            flexibleTextView.setCompoundDrawablePadding((int) ts3.a(6, flexibleTextView.getContext()));
            if (TextUtils.isEmpty(category.getId())) {
                FlexibleTextView flexibleTextView2 = this.a;
                kd4.a((Object) context, "context");
                flexibleTextView2.setText(context.getResources().getString(R.string.Onboarding_Sections_without_Device_Profile_Without_Watch_None));
            } else {
                String a2 = sm2.a(PortfolioApp.W.c(), category.getName(), category.getEnglishName());
                if (i == 0) {
                    this.a.setText(a2);
                } else {
                    kd4.a((Object) a2, "name");
                    if (a2 != null) {
                        String upperCase = a2.toUpperCase();
                        kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        this.a.setText(upperCase);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            if (i == 0) {
                this.a.setTextColor(k6.a(context, (int) R.color.placeholderText));
                Drawable c = k6.c(context, R.drawable.ic_category_search);
                if (c != null) {
                    c.setTintList(ColorStateList.valueOf(k6.a(context, (int) R.color.primaryColor)));
                }
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds(c, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.setBackgroundColor(k6.a(context, (int) R.color.dianaInactiveTab));
            } else if (i == this.b.b) {
                this.a.setSelected(true);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                FlexibleTextView flexibleTextView3 = this.a;
                flexibleTextView3.setTextColor(k6.a(flexibleTextView3.getContext(), (int) R.color.white));
                this.a.setBackground(k6.c(context, R.drawable.bg_border_active_color_primary));
            } else {
                this.a.setSelected(false);
                this.a.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.a.setTextColor(k6.a(context, (int) R.color.primaryColor));
                this.a.setBackground(k6.c(context, R.drawable.bg_controls_btn_secondary_outline));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(Category category);
    }

    /*
    static {
        new b((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ps2(ArrayList arrayList, c cVar, int i, fd4 fd4) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public ps2(ArrayList<Category> arrayList, c cVar) {
        kd4.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.a = new Category("Search", "Search", "Customization_Complications_Elements_Input__Search", "", "", -1);
        this.b = -1;
    }

    @DexIgnore
    public final c b() {
        return this.d;
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026_category, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<Category> list) {
        kd4.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        this.c.add(0, this.a);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        kd4.b(str, "category");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (kd4.a((Object) ((Category) t).getId(), (Object) str)) {
                break;
            }
        }
        Category category = (Category) t;
        if (category != null) {
            return this.c.indexOf(category);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            Category category = this.c.get(i);
            kd4.a((Object) category, "mData[position]");
            aVar.a(category, i);
        }
    }

    @DexIgnore
    public final void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CategoriesAdapter", "setSelectedCategory pos=" + i);
        if (i < getItemCount()) {
            this.b = i;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        kd4.b(cVar, "listener");
        this.d = cVar;
    }
}
