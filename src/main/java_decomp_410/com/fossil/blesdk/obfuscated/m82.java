package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m82 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ WebView r;

    @DexIgnore
    public m82(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, WebView webView) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = webView;
    }
}
