package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.common.IdManager;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ky extends v44<Boolean> implements p54 {
    @DexIgnore
    public Map<IdManager.DeviceIdentifierType, String> j() {
        return Collections.emptyMap();
    }

    @DexIgnore
    public String p() {
        return "com.crashlytics.sdk.android:beta";
    }

    @DexIgnore
    public String r() {
        return "1.2.10.27";
    }

    @DexIgnore
    public Boolean k() {
        q44.g().d("Beta", "Beta kit initializing...");
        return true;
    }
}
