package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class li1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ hg1 e;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 f;
    @DexIgnore
    public /* final */ /* synthetic */ zh1 g;

    @DexIgnore
    public li1(zh1 zh1, hg1 hg1, rl1 rl1) {
        this.g = zh1;
        this.e = hg1;
        this.f = rl1;
    }

    @DexIgnore
    public final void run() {
        hg1 b = this.g.b(this.e, this.f);
        this.g.e.y();
        this.g.e.a(b, this.f);
    }
}
