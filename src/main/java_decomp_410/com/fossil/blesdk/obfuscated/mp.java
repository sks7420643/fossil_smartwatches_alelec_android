package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mp {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(jo joVar, Exception exc, so<?> soVar, DataSource dataSource);

        @DexIgnore
        void a(jo joVar, Object obj, so<?> soVar, DataSource dataSource, jo joVar2);

        @DexIgnore
        void j();
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    void cancel();
}
