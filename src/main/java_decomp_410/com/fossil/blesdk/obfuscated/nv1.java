package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.LinkedList;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nv1 {
    @DexIgnore
    public /* final */ Readable a;
    @DexIgnore
    public /* final */ Reader b;
    @DexIgnore
    public /* final */ CharBuffer c; // = hv1.a();
    @DexIgnore
    public /* final */ char[] d; // = this.c.array();
    @DexIgnore
    public /* final */ Queue<String> e; // = new LinkedList();
    @DexIgnore
    public /* final */ lv1 f; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends lv1 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(String str, String str2) {
            nv1.this.e.add(str);
        }
    }

    @DexIgnore
    public nv1(Readable readable) {
        st1.a(readable);
        this.a = readable;
        this.b = readable instanceof Reader ? (Reader) readable : null;
    }

    @DexIgnore
    public String a() throws IOException {
        int i;
        while (true) {
            if (this.e.peek() != null) {
                break;
            }
            this.c.clear();
            Reader reader = this.b;
            if (reader != null) {
                char[] cArr = this.d;
                i = reader.read(cArr, 0, cArr.length);
            } else {
                i = this.a.read(this.c);
            }
            if (i == -1) {
                this.f.a();
                break;
            }
            this.f.a(this.d, 0, i);
        }
        return this.e.poll();
    }
}
