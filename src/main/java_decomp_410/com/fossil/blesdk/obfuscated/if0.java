package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.google.android.gms.common.api.AvailabilityException;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class if0 implements rn1<Map<yh0<?>, String>> {
    @DexIgnore
    public cf0 a;
    @DexIgnore
    public /* final */ /* synthetic */ ni0 b;

    @DexIgnore
    public if0(ni0 ni0, cf0 cf0) {
        this.b = ni0;
        this.a = cf0;
    }

    @DexIgnore
    public final void a() {
        this.a.onComplete();
    }

    @DexIgnore
    public final void onComplete(wn1<Map<yh0<?>, String>> wn1) {
        this.b.j.lock();
        try {
            if (!this.b.r) {
                this.a.onComplete();
                return;
            }
            if (wn1.e()) {
                Map unused = this.b.t = new g4(this.b.f.size());
                for (mi0 h : this.b.f.values()) {
                    this.b.t.put(h.h(), ud0.i);
                }
            } else if (wn1.a() instanceof AvailabilityException) {
                AvailabilityException availabilityException = (AvailabilityException) wn1.a();
                if (this.b.p) {
                    Map unused2 = this.b.t = new g4(this.b.f.size());
                    for (mi0 mi0 : this.b.f.values()) {
                        yh0 h2 = mi0.h();
                        ud0 connectionResult = availabilityException.getConnectionResult(mi0);
                        if (this.b.a((mi0<?>) mi0, connectionResult)) {
                            this.b.t.put(h2, new ud0(16));
                        } else {
                            this.b.t.put(h2, connectionResult);
                        }
                    }
                } else {
                    Map unused3 = this.b.t = availabilityException.zaj();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", wn1.a());
                Map unused4 = this.b.t = Collections.emptyMap();
            }
            if (this.b.c()) {
                this.b.s.putAll(this.b.t);
                if (this.b.k() == null) {
                    this.b.i();
                    this.b.j();
                    this.b.m.signalAll();
                }
            }
            this.a.onComplete();
            this.b.j.unlock();
        } finally {
            this.b.j.unlock();
        }
    }
}
