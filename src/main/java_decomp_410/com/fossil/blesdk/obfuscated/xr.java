package com.fossil.blesdk.obfuscated;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xr<Data> implements sr<Integer, Data> {
    @DexIgnore
    public /* final */ sr<Uri, Data> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tr<Integer, AssetFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public a(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public sr<Integer, AssetFileDescriptor> a(wr wrVar) {
            return new xr(this.a, wrVar.a(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements tr<Integer, ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public b(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public sr<Integer, ParcelFileDescriptor> a(wr wrVar) {
            return new xr(this.a, wrVar.a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements tr<Integer, InputStream> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public c(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public sr<Integer, InputStream> a(wr wrVar) {
            return new xr(this.a, wrVar.a(Uri.class, InputStream.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements tr<Integer, Uri> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public d(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        public sr<Integer, Uri> a(wr wrVar) {
            return new xr(this.a, as.a());
        }
    }

    @DexIgnore
    public xr(Resources resources, sr<Uri, Data> srVar) {
        this.b = resources;
        this.a = srVar;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean a(Integer num) {
        return true;
    }

    @DexIgnore
    public sr.a<Data> a(Integer num, int i, int i2, lo loVar) {
        Uri a2 = a(num);
        if (a2 == null) {
            return null;
        }
        return this.a.a(a2, i, i2, loVar);
    }

    @DexIgnore
    public final Uri a(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }
}
