package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mh0<R extends me0> extends qe0<R> implements ne0<R> {
    @DexIgnore
    public pe0<? super R, ? extends me0> a;
    @DexIgnore
    public mh0<? extends me0> b;
    @DexIgnore
    public volatile oe0<? super R> c;
    @DexIgnore
    public /* final */ Object d;
    @DexIgnore
    public Status e;
    @DexIgnore
    public /* final */ WeakReference<ge0> f;
    @DexIgnore
    public /* final */ oh0 g;

    @DexIgnore
    public final void a(Status status) {
        synchronized (this.d) {
            this.e = status;
            b(this.e);
        }
    }

    @DexIgnore
    public final void b(Status status) {
        synchronized (this.d) {
            if (this.a != null) {
                Status a2 = this.a.a(status);
                bk0.a(a2, (Object) "onFailure must not return null");
                this.b.a(a2);
            } else if (b()) {
                this.c.a(status);
            }
        }
    }

    @DexIgnore
    public final void onResult(R r) {
        synchronized (this.d) {
            if (!r.G().L()) {
                a(r.G());
                a((me0) r);
            } else if (this.a != null) {
                gh0.a().submit(new nh0(this, r));
            } else if (b()) {
                this.c.a(r);
            }
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }

    @DexIgnore
    public static void a(me0 me0) {
        if (me0 instanceof je0) {
            try {
                ((je0) me0).a();
            } catch (RuntimeException e2) {
                String valueOf = String.valueOf(me0);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("TransformedResultImpl", sb.toString(), e2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        return (this.c == null || ((ge0) this.f.get()) == null) ? false : true;
    }
}
