package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ Throwable f;

    @DexIgnore
    public v24(Context context, Throwable th) {
        this.e = context;
        this.f = th;
    }

    @DexIgnore
    public final void run() {
        try {
            if (h04.s()) {
                new c14(new n04(this.e, j04.a(this.e, false, (k04) null), 99, this.f, p04.m)).a();
            }
        } catch (Throwable th) {
            t14 f2 = j04.m;
            f2.c("reportSdkSelfException error: " + th);
        }
    }
}
