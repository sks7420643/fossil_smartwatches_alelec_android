package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.AvailabilityException;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ai0 {
    @DexIgnore
    public /* final */ g4<yh0<?>, ud0> a; // = new g4<>();
    @DexIgnore
    public /* final */ g4<yh0<?>, String> b; // = new g4<>();
    @DexIgnore
    public /* final */ xn1<Map<yh0<?>, String>> c; // = new xn1<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public ai0(Iterable<? extends fe0<?>> iterable) {
        for (fe0 h : iterable) {
            this.a.put(h.h(), null);
        }
        this.d = this.a.keySet().size();
    }

    @DexIgnore
    public final wn1<Map<yh0<?>, String>> a() {
        return this.c.a();
    }

    @DexIgnore
    public final Set<yh0<?>> b() {
        return this.a.keySet();
    }

    @DexIgnore
    public final void a(yh0<?> yh0, ud0 ud0, String str) {
        this.a.put(yh0, ud0);
        this.b.put(yh0, str);
        this.d--;
        if (!ud0.L()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.a((Exception) new AvailabilityException(this.a));
            return;
        }
        this.c.a(this.b);
    }
}
