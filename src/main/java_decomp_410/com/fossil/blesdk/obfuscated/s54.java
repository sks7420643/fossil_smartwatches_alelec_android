package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s54 implements r54 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public s54(Class cls, Object obj) throws NoSuchMethodException {
        this.b = obj;
        this.a = cls.getDeclaredMethod("isDataCollectionDefaultEnabled", new Class[0]);
    }

    @DexIgnore
    public static r54 a(Context context) {
        try {
            Class<?> loadClass = context.getClassLoader().loadClass("com.google.firebase.FirebaseApp");
            return new s54(loadClass, loadClass.getDeclaredMethod("getInstance", new Class[0]).invoke(loadClass, new Object[0]));
        } catch (ClassNotFoundException unused) {
            q44.g().d("Fabric", "Could not find class: com.google.firebase.FirebaseApp");
            return null;
        } catch (NoSuchMethodException e) {
            y44 g = q44.g();
            g.d("Fabric", "Could not find method: " + e.getMessage());
            return null;
        } catch (Exception e2) {
            q44.g().b("Fabric", "Unexpected error loading FirebaseApp instance.", e2);
            return null;
        }
    }

    @DexIgnore
    public boolean a() {
        try {
            return ((Boolean) this.a.invoke(this.b, new Object[0])).booleanValue();
        } catch (Exception e) {
            q44.g().b("Fabric", "Cannot check isDataCollectionDefaultEnabled on FirebaseApp.", e);
            return false;
        }
    }
}
