package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hl1 implements Callable<String> {
    @DexIgnore
    public /* final */ /* synthetic */ rl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ dl1 f;

    @DexIgnore
    public hl1(dl1 dl1, rl1 rl1) {
        this.f = dl1;
        this.e = rl1;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        ql1 ql1;
        if (this.f.i().i(this.e.e)) {
            ql1 = this.f.d(this.e);
        } else {
            ql1 = this.f.l().b(this.e.e);
        }
        if (ql1 != null) {
            return ql1.a();
        }
        this.f.d().v().a("App info was null when attempting to get app instance id");
        return null;
    }
}
