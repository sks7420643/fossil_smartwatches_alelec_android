package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.Protocol;
import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zw3 {
    @DexIgnore
    public static String a(hv3 hv3, Proxy.Type type, Protocol protocol) {
        StringBuilder sb = new StringBuilder();
        sb.append(hv3.f());
        sb.append(' ');
        if (a(hv3, type)) {
            sb.append(hv3.d());
        } else {
            sb.append(a(hv3.d()));
        }
        sb.append(' ');
        sb.append(a(protocol));
        return sb.toString();
    }

    @DexIgnore
    public static boolean a(hv3 hv3, Proxy.Type type) {
        return !hv3.e() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(dv3 dv3) {
        String b = dv3.b();
        String d = dv3.d();
        if (d == null) {
            return b;
        }
        return b + '?' + d;
    }

    @DexIgnore
    public static String a(Protocol protocol) {
        return protocol == Protocol.HTTP_1_0 ? "HTTP/1.0" : "HTTP/1.1";
    }
}
