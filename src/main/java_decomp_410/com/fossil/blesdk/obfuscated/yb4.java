package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface yb4<T> {
    @DexIgnore
    CoroutineContext getContext();

    @DexIgnore
    void resumeWith(Object obj);
}
