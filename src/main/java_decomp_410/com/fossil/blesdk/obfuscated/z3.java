package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.fossil.blesdk.obfuscated.f4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z3 extends b4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements f4.a {
        @DexIgnore
        public a(z3 z3Var) {
        }

        @DexIgnore
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    public void a() {
        f4.r = new a(this);
    }
}
