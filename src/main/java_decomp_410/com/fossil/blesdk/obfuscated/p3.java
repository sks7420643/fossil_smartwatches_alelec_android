package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p3 {
    @DexIgnore
    public /* final */ Intent a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public ArrayList<Bundle> b;
        @DexIgnore
        public Bundle c;
        @DexIgnore
        public ArrayList<Bundle> d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public a() {
            this((r3) null);
        }

        @DexIgnore
        public p3 a() {
            ArrayList<Bundle> arrayList = this.b;
            if (arrayList != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            ArrayList<Bundle> arrayList2 = this.d;
            if (arrayList2 != null) {
                this.a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.e);
            return new p3(this.a, this.c);
        }

        @DexIgnore
        public a(r3 r3Var) {
            this.a = new Intent("android.intent.action.VIEW");
            this.b = null;
            this.c = null;
            this.d = null;
            this.e = true;
            if (r3Var == null) {
                Bundle bundle = new Bundle();
                if (r3Var == null) {
                    z5.a(bundle, "android.support.customtabs.extra.SESSION", (IBinder) null);
                    this.a.putExtras(bundle);
                    return;
                }
                r3Var.a();
                throw null;
            }
            r3Var.b();
            throw null;
        }
    }

    @DexIgnore
    public p3(Intent intent, Bundle bundle) {
        this.a = intent;
        this.b = bundle;
    }

    @DexIgnore
    public void a(Context context, Uri uri) {
        this.a.setData(uri);
        k6.a(context, this.a, this.b);
    }
}
