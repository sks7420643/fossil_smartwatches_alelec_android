package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x0 */
public class C3251x0 extends android.view.MenuInflater {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.Class<?>[] f10758e; // = {android.content.Context.class};

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.Class<?>[] f10759f; // = f10758e;

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object[] f10760a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.Object[] f10761b; // = this.f10760a;

    @DexIgnore
    /* renamed from: c */
    public android.content.Context f10762c;

    @DexIgnore
    /* renamed from: d */
    public java.lang.Object f10763d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.x0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.x0$a */
    public static class C3252a implements android.view.MenuItem.OnMenuItemClickListener {

        @DexIgnore
        /* renamed from: c */
        public static /* final */ java.lang.Class<?>[] f10764c; // = {android.view.MenuItem.class};

        @DexIgnore
        /* renamed from: a */
        public java.lang.Object f10765a;

        @DexIgnore
        /* renamed from: b */
        public java.lang.reflect.Method f10766b;

        @DexIgnore
        public C3252a(java.lang.Object obj, java.lang.String str) {
            this.f10765a = obj;
            java.lang.Class<?> cls = obj.getClass();
            try {
                this.f10766b = cls.getMethod(str, f10764c);
            } catch (java.lang.Exception e) {
                android.view.InflateException inflateException = new android.view.InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        @DexIgnore
        public boolean onMenuItemClick(android.view.MenuItem menuItem) {
            try {
                if (this.f10766b.getReturnType() == java.lang.Boolean.TYPE) {
                    return ((java.lang.Boolean) this.f10766b.invoke(this.f10765a, new java.lang.Object[]{menuItem})).booleanValue();
                }
                this.f10766b.invoke(this.f10765a, new java.lang.Object[]{menuItem});
                return true;
            } catch (java.lang.Exception e) {
                throw new java.lang.RuntimeException(e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.x0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.x0$b */
    public class C3253b {

        @DexIgnore
        /* renamed from: A */
        public com.fossil.blesdk.obfuscated.C2382m8 f10767A;

        @DexIgnore
        /* renamed from: B */
        public java.lang.CharSequence f10768B;

        @DexIgnore
        /* renamed from: C */
        public java.lang.CharSequence f10769C;

        @DexIgnore
        /* renamed from: D */
        public android.content.res.ColorStateList f10770D; // = null;

        @DexIgnore
        /* renamed from: E */
        public android.graphics.PorterDuff.Mode f10771E; // = null;

        @DexIgnore
        /* renamed from: a */
        public android.view.Menu f10773a;

        @DexIgnore
        /* renamed from: b */
        public int f10774b;

        @DexIgnore
        /* renamed from: c */
        public int f10775c;

        @DexIgnore
        /* renamed from: d */
        public int f10776d;

        @DexIgnore
        /* renamed from: e */
        public int f10777e;

        @DexIgnore
        /* renamed from: f */
        public boolean f10778f;

        @DexIgnore
        /* renamed from: g */
        public boolean f10779g;

        @DexIgnore
        /* renamed from: h */
        public boolean f10780h;

        @DexIgnore
        /* renamed from: i */
        public int f10781i;

        @DexIgnore
        /* renamed from: j */
        public int f10782j;

        @DexIgnore
        /* renamed from: k */
        public java.lang.CharSequence f10783k;

        @DexIgnore
        /* renamed from: l */
        public java.lang.CharSequence f10784l;

        @DexIgnore
        /* renamed from: m */
        public int f10785m;

        @DexIgnore
        /* renamed from: n */
        public char f10786n;

        @DexIgnore
        /* renamed from: o */
        public int f10787o;

        @DexIgnore
        /* renamed from: p */
        public char f10788p;

        @DexIgnore
        /* renamed from: q */
        public int f10789q;

        @DexIgnore
        /* renamed from: r */
        public int f10790r;

        @DexIgnore
        /* renamed from: s */
        public boolean f10791s;

        @DexIgnore
        /* renamed from: t */
        public boolean f10792t;

        @DexIgnore
        /* renamed from: u */
        public boolean f10793u;

        @DexIgnore
        /* renamed from: v */
        public int f10794v;

        @DexIgnore
        /* renamed from: w */
        public int f10795w;

        @DexIgnore
        /* renamed from: x */
        public java.lang.String f10796x;

        @DexIgnore
        /* renamed from: y */
        public java.lang.String f10797y;

        @DexIgnore
        /* renamed from: z */
        public java.lang.String f10798z;

        @DexIgnore
        public C3253b(android.view.Menu menu) {
            this.f10773a = menu;
            mo17580d();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17575a(android.util.AttributeSet attributeSet) {
            android.content.res.TypedArray obtainStyledAttributes = com.fossil.blesdk.obfuscated.C3251x0.this.f10762c.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C1368a0.MenuGroup);
            this.f10774b = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_id, 0);
            this.f10775c = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_menuCategory, 0);
            this.f10776d = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_orderInCategory, 0);
            this.f10777e = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_checkableBehavior, 0);
            this.f10778f = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_visible, true);
            this.f10779g = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        /* renamed from: b */
        public void mo17578b(android.util.AttributeSet attributeSet) {
            android.content.res.TypedArray obtainStyledAttributes = com.fossil.blesdk.obfuscated.C3251x0.this.f10762c.obtainStyledAttributes(attributeSet, com.fossil.blesdk.obfuscated.C1368a0.MenuItem);
            this.f10781i = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_id, 0);
            this.f10782j = (obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_menuCategory, this.f10775c) & -65536) | (obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_orderInCategory, this.f10776d) & 65535);
            this.f10783k = obtainStyledAttributes.getText(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_title);
            this.f10784l = obtainStyledAttributes.getText(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_titleCondensed);
            this.f10785m = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_icon, 0);
            this.f10786n = mo17572a(obtainStyledAttributes.getString(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_alphabeticShortcut));
            this.f10787o = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_alphabeticModifiers, 4096);
            this.f10788p = mo17572a(obtainStyledAttributes.getString(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_numericShortcut));
            this.f10789q = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_numericModifiers, 4096);
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_checkable)) {
                this.f10790r = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.f10790r = this.f10777e;
            }
            this.f10791s = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_checked, false);
            this.f10792t = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_visible, this.f10778f);
            this.f10793u = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_enabled, this.f10779g);
            this.f10794v = obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_showAsAction, -1);
            this.f10798z = obtainStyledAttributes.getString(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_android_onClick);
            this.f10795w = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_actionLayout, 0);
            this.f10796x = obtainStyledAttributes.getString(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_actionViewClass);
            this.f10797y = obtainStyledAttributes.getString(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_actionProviderClass);
            boolean z = this.f10797y != null;
            if (z && this.f10795w == 0 && this.f10796x == null) {
                this.f10767A = (com.fossil.blesdk.obfuscated.C2382m8) mo17573a(this.f10797y, com.fossil.blesdk.obfuscated.C3251x0.f10759f, com.fossil.blesdk.obfuscated.C3251x0.this.f10761b);
            } else {
                if (z) {
                    android.util.Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.f10767A = null;
            }
            this.f10768B = obtainStyledAttributes.getText(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_contentDescription);
            this.f10769C = obtainStyledAttributes.getText(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_tooltipText);
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_iconTintMode)) {
                this.f10771E = com.fossil.blesdk.obfuscated.C2181k2.m9311a(obtainStyledAttributes.getInt(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_iconTintMode, -1), this.f10771E);
            } else {
                this.f10771E = null;
            }
            if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_iconTint)) {
                this.f10770D = obtainStyledAttributes.getColorStateList(com.fossil.blesdk.obfuscated.C1368a0.MenuItem_iconTint);
            } else {
                this.f10770D = null;
            }
            obtainStyledAttributes.recycle();
            this.f10780h = false;
        }

        @DexIgnore
        /* renamed from: c */
        public boolean mo17579c() {
            return this.f10780h;
        }

        @DexIgnore
        /* renamed from: d */
        public void mo17580d() {
            this.f10774b = 0;
            this.f10775c = 0;
            this.f10776d = 0;
            this.f10777e = 0;
            this.f10778f = true;
            this.f10779g = true;
        }

        @DexIgnore
        /* renamed from: a */
        public final char mo17572a(java.lang.String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo17576a(android.view.MenuItem menuItem) {
            boolean z = false;
            menuItem.setChecked(this.f10791s).setVisible(this.f10792t).setEnabled(this.f10793u).setCheckable(this.f10790r >= 1).setTitleCondensed(this.f10784l).setIcon(this.f10785m);
            int i = this.f10794v;
            if (i >= 0) {
                menuItem.setShowAsAction(i);
            }
            if (this.f10798z != null) {
                if (!com.fossil.blesdk.obfuscated.C3251x0.this.f10762c.isRestricted()) {
                    menuItem.setOnMenuItemClickListener(new com.fossil.blesdk.obfuscated.C3251x0.C3252a(com.fossil.blesdk.obfuscated.C3251x0.this.mo17567a(), this.f10798z));
                } else {
                    throw new java.lang.IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            boolean z2 = menuItem instanceof com.fossil.blesdk.obfuscated.C2179k1;
            if (z2) {
                com.fossil.blesdk.obfuscated.C2179k1 k1Var = (com.fossil.blesdk.obfuscated.C2179k1) menuItem;
            }
            if (this.f10790r >= 2) {
                if (z2) {
                    ((com.fossil.blesdk.obfuscated.C2179k1) menuItem).mo12553c(true);
                } else if (menuItem instanceof com.fossil.blesdk.obfuscated.C2274l1) {
                    ((com.fossil.blesdk.obfuscated.C2274l1) menuItem).mo13019a(true);
                }
            }
            java.lang.String str = this.f10796x;
            if (str != null) {
                menuItem.setActionView((android.view.View) mo17573a(str, com.fossil.blesdk.obfuscated.C3251x0.f10758e, com.fossil.blesdk.obfuscated.C3251x0.this.f10760a));
                z = true;
            }
            int i2 = this.f10795w;
            if (i2 > 0) {
                if (!z) {
                    menuItem.setActionView(i2);
                } else {
                    android.util.Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            com.fossil.blesdk.obfuscated.C2382m8 m8Var = this.f10767A;
            if (m8Var != null) {
                com.fossil.blesdk.obfuscated.C2945t8.m14045a(menuItem, m8Var);
            }
            com.fossil.blesdk.obfuscated.C2945t8.m14049a(menuItem, this.f10768B);
            com.fossil.blesdk.obfuscated.C2945t8.m14051b(menuItem, this.f10769C);
            com.fossil.blesdk.obfuscated.C2945t8.m14046a(menuItem, this.f10786n, this.f10787o);
            com.fossil.blesdk.obfuscated.C2945t8.m14050b(menuItem, this.f10788p, this.f10789q);
            android.graphics.PorterDuff.Mode mode = this.f10771E;
            if (mode != null) {
                com.fossil.blesdk.obfuscated.C2945t8.m14048a(menuItem, mode);
            }
            android.content.res.ColorStateList colorStateList = this.f10770D;
            if (colorStateList != null) {
                com.fossil.blesdk.obfuscated.C2945t8.m14047a(menuItem, colorStateList);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public android.view.SubMenu mo17577b() {
            this.f10780h = true;
            android.view.SubMenu addSubMenu = this.f10773a.addSubMenu(this.f10774b, this.f10781i, this.f10782j, this.f10783k);
            mo17576a(addSubMenu.getItem());
            return addSubMenu;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17574a() {
            this.f10780h = true;
            mo17576a(this.f10773a.add(this.f10774b, this.f10781i, this.f10782j, this.f10783k));
        }

        @DexIgnore
        /* renamed from: a */
        public final <T> T mo17573a(java.lang.String str, java.lang.Class<?>[] clsArr, java.lang.Object[] objArr) {
            try {
                java.lang.reflect.Constructor<?> constructor = com.fossil.blesdk.obfuscated.C3251x0.this.f10762c.getClassLoader().loadClass(str).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (java.lang.Exception e) {
                android.util.Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e);
                return null;
            }
        }
    }

    @DexIgnore
    public C3251x0(android.content.Context context) {
        super(context);
        this.f10762c = context;
        this.f10760a = new java.lang.Object[]{context};
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17569a(org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.view.Menu menu) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        com.fossil.blesdk.obfuscated.C3251x0.C3253b bVar = new com.fossil.blesdk.obfuscated.C3251x0.C3253b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                if (xmlPullParser.getName().equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new java.lang.RuntimeException("Expecting menu, got " + r15);
                }
            }
        }
        int i = eventType;
        java.lang.String str = null;
        boolean z = false;
        boolean z2 = false;
        while (!z) {
            if (i != 1) {
                if (i != 2) {
                    if (i == 3) {
                        java.lang.String name = xmlPullParser.getName();
                        if (z2 && name.equals(str)) {
                            str = null;
                            z2 = false;
                        } else if (name.equals("group")) {
                            bVar.mo17580d();
                        } else if (name.equals("item")) {
                            if (!bVar.mo17579c()) {
                                com.fossil.blesdk.obfuscated.C2382m8 m8Var = bVar.f10767A;
                                if (m8Var == null || !m8Var.hasSubMenu()) {
                                    bVar.mo17574a();
                                } else {
                                    bVar.mo17577b();
                                }
                            }
                        } else if (name.equals("menu")) {
                            z = true;
                        }
                    }
                } else if (!z2) {
                    java.lang.String name2 = xmlPullParser.getName();
                    if (name2.equals("group")) {
                        bVar.mo17575a(attributeSet);
                    } else if (name2.equals("item")) {
                        bVar.mo17578b(attributeSet);
                    } else if (name2.equals("menu")) {
                        mo17569a(xmlPullParser, attributeSet, bVar.mo17577b());
                    } else {
                        str = name2;
                        z2 = true;
                    }
                }
                i = xmlPullParser.next();
            } else {
                throw new java.lang.RuntimeException("Unexpected end of document");
            }
        }
    }

    @DexIgnore
    public void inflate(int i, android.view.Menu menu) {
        if (!(menu instanceof com.fossil.blesdk.obfuscated.C1927h7)) {
            super.inflate(i, menu);
            return;
        }
        android.content.res.XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.f10762c.getResources().getLayout(i);
            mo17569a(xmlResourceParser, android.util.Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (org.xmlpull.v1.XmlPullParserException e) {
            throw new android.view.InflateException("Error inflating menu XML", e);
        } catch (java.io.IOException e2) {
            throw new android.view.InflateException("Error inflating menu XML", e2);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo17567a() {
        if (this.f10763d == null) {
            this.f10763d = mo17568a(this.f10762c);
        }
        return this.f10763d;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.Object mo17568a(java.lang.Object obj) {
        return (!(obj instanceof android.app.Activity) && (obj instanceof android.content.ContextWrapper)) ? mo17568a(((android.content.ContextWrapper) obj).getBaseContext()) : obj;
    }
}
