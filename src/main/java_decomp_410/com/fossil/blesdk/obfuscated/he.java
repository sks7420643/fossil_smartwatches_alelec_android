package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class he<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ le.d<T> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public static /* final */ Object d; // = new Object();
        @DexIgnore
        public static Executor e;
        @DexIgnore
        public Executor a;
        @DexIgnore
        public Executor b;
        @DexIgnore
        public /* final */ le.d<T> c;

        @DexIgnore
        public a(le.d<T> dVar) {
            this.c = dVar;
        }

        @DexIgnore
        public he<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new he<>(this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public he(Executor executor, Executor executor2, le.d<T> dVar) {
        this.a = executor2;
        this.b = dVar;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public le.d<T> b() {
        return this.b;
    }
}
