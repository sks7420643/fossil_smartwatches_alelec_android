package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dr */
public final class C1650dr implements java.util.concurrent.ExecutorService {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ long f4509f; // = java.util.concurrent.TimeUnit.SECONDS.toMillis(10);

    @DexIgnore
    /* renamed from: g */
    public static volatile int f4510g;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.ExecutorService f4511e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dr$a */
    public static final class C1651a implements java.util.concurrent.ThreadFactory {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f4512a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1650dr.C1653b f4513b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ boolean f4514c;

        @DexIgnore
        /* renamed from: d */
        public int f4515d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$a$a */
        public class C1652a extends java.lang.Thread {
            @DexIgnore
            public C1652a(java.lang.Runnable runnable, java.lang.String str) {
                super(runnable, str);
            }

            @DexIgnore
            public void run() {
                android.os.Process.setThreadPriority(9);
                if (com.fossil.blesdk.obfuscated.C1650dr.C1651a.this.f4514c) {
                    android.os.StrictMode.setThreadPolicy(new android.os.StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    com.fossil.blesdk.obfuscated.C1650dr.C1651a.this.f4513b.mo10112a(th);
                }
            }
        }

        @DexIgnore
        public C1651a(java.lang.String str, com.fossil.blesdk.obfuscated.C1650dr.C1653b bVar, boolean z) {
            this.f4512a = str;
            this.f4513b = bVar;
            this.f4514c = z;
        }

        @DexIgnore
        public synchronized java.lang.Thread newThread(java.lang.Runnable runnable) {
            com.fossil.blesdk.obfuscated.C1650dr.C1651a.C1652a aVar;
            aVar = new com.fossil.blesdk.obfuscated.C1650dr.C1651a.C1652a(runnable, "glide-" + this.f4512a + "-thread-" + this.f4515d);
            this.f4515d = this.f4515d + 1;
            return aVar;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.dr$b */
    public interface C1653b {

        @DexIgnore
        /* renamed from: a */
        public static final com.fossil.blesdk.obfuscated.C1650dr.C1653b f4517a = new com.fossil.blesdk.obfuscated.C1650dr.C1653b.C1655b();

        @DexIgnore
        /* renamed from: b */
        public static final com.fossil.blesdk.obfuscated.C1650dr.C1653b f4518b = f4517a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$b$a */
        public class C1654a implements com.fossil.blesdk.obfuscated.C1650dr.C1653b {
            @DexIgnore
            /* renamed from: a */
            public void mo10112a(java.lang.Throwable th) {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$b$b */
        public class C1655b implements com.fossil.blesdk.obfuscated.C1650dr.C1653b {
            @DexIgnore
            /* renamed from: a */
            public void mo10112a(java.lang.Throwable th) {
                if (th != null && android.util.Log.isLoggable("GlideExecutor", 6)) {
                    android.util.Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$b$c")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$b$c */
        public class C1656c implements com.fossil.blesdk.obfuscated.C1650dr.C1653b {
            @DexIgnore
            /* renamed from: a */
            public void mo10112a(java.lang.Throwable th) {
                if (th != null) {
                    throw new java.lang.RuntimeException("Request threw uncaught throwable", th);
                }
            }
        }

        /*
        static {
            new com.fossil.blesdk.obfuscated.C1650dr.C1653b.C1654a();
            new com.fossil.blesdk.obfuscated.C1650dr.C1653b.C1656c();
        }
        */

        @DexIgnore
        /* renamed from: a */
        void mo10112a(java.lang.Throwable th);
    }

    @DexIgnore
    public C1650dr(java.util.concurrent.ExecutorService executorService) {
        this.f4511e = executorService;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1650dr m6027a(int i, java.lang.String str, com.fossil.blesdk.obfuscated.C1650dr.C1653b bVar) {
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(i, i, 0, java.util.concurrent.TimeUnit.MILLISECONDS, new java.util.concurrent.PriorityBlockingQueue(), new com.fossil.blesdk.obfuscated.C1650dr.C1651a(str, bVar, true));
        return new com.fossil.blesdk.obfuscated.C1650dr(threadPoolExecutor);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1650dr m6029b(int i, java.lang.String str, com.fossil.blesdk.obfuscated.C1650dr.C1653b bVar) {
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(i, i, 0, java.util.concurrent.TimeUnit.MILLISECONDS, new java.util.concurrent.PriorityBlockingQueue(), new com.fossil.blesdk.obfuscated.C1650dr.C1651a(str, bVar, false));
        return new com.fossil.blesdk.obfuscated.C1650dr(threadPoolExecutor);
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C1650dr m6030c() {
        return m6027a(1, "disk-cache", com.fossil.blesdk.obfuscated.C1650dr.C1653b.f4518b);
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C1650dr m6031d() {
        return m6029b(m6025a(), "source", com.fossil.blesdk.obfuscated.C1650dr.C1653b.f4518b);
    }

    @DexIgnore
    /* renamed from: e */
    public static com.fossil.blesdk.obfuscated.C1650dr m6032e() {
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(0, Integer.MAX_VALUE, f4509f, java.util.concurrent.TimeUnit.MILLISECONDS, new java.util.concurrent.SynchronousQueue(), new com.fossil.blesdk.obfuscated.C1650dr.C1651a("source-unlimited", com.fossil.blesdk.obfuscated.C1650dr.C1653b.f4518b, false));
        return new com.fossil.blesdk.obfuscated.C1650dr(threadPoolExecutor);
    }

    @DexIgnore
    public boolean awaitTermination(long j, java.util.concurrent.TimeUnit timeUnit) throws java.lang.InterruptedException {
        return this.f4511e.awaitTermination(j, timeUnit);
    }

    @DexIgnore
    public void execute(java.lang.Runnable runnable) {
        this.f4511e.execute(runnable);
    }

    @DexIgnore
    public <T> java.util.List<java.util.concurrent.Future<T>> invokeAll(java.util.Collection<? extends java.util.concurrent.Callable<T>> collection) throws java.lang.InterruptedException {
        return this.f4511e.invokeAll(collection);
    }

    @DexIgnore
    public <T> T invokeAny(java.util.Collection<? extends java.util.concurrent.Callable<T>> collection) throws java.lang.InterruptedException, java.util.concurrent.ExecutionException {
        return this.f4511e.invokeAny(collection);
    }

    @DexIgnore
    public boolean isShutdown() {
        return this.f4511e.isShutdown();
    }

    @DexIgnore
    public boolean isTerminated() {
        return this.f4511e.isTerminated();
    }

    @DexIgnore
    public void shutdown() {
        this.f4511e.shutdown();
    }

    @DexIgnore
    public java.util.List<java.lang.Runnable> shutdownNow() {
        return this.f4511e.shutdownNow();
    }

    @DexIgnore
    public java.util.concurrent.Future<?> submit(java.lang.Runnable runnable) {
        return this.f4511e.submit(runnable);
    }

    @DexIgnore
    public java.lang.String toString() {
        return this.f4511e.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1650dr m6026a(int i, com.fossil.blesdk.obfuscated.C1650dr.C1653b bVar) {
        java.util.concurrent.ThreadPoolExecutor threadPoolExecutor = new java.util.concurrent.ThreadPoolExecutor(i, i, 0, java.util.concurrent.TimeUnit.MILLISECONDS, new java.util.concurrent.PriorityBlockingQueue(), new com.fossil.blesdk.obfuscated.C1650dr.C1651a("animation", bVar, true));
        return new com.fossil.blesdk.obfuscated.C1650dr(threadPoolExecutor);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C1650dr m6028b() {
        return m6026a(m6025a() >= 4 ? 2 : 1, com.fossil.blesdk.obfuscated.C1650dr.C1653b.f4518b);
    }

    @DexIgnore
    public <T> java.util.List<java.util.concurrent.Future<T>> invokeAll(java.util.Collection<? extends java.util.concurrent.Callable<T>> collection, long j, java.util.concurrent.TimeUnit timeUnit) throws java.lang.InterruptedException {
        return this.f4511e.invokeAll(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> T invokeAny(java.util.Collection<? extends java.util.concurrent.Callable<T>> collection, long j, java.util.concurrent.TimeUnit timeUnit) throws java.lang.InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        return this.f4511e.invokeAny(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> java.util.concurrent.Future<T> submit(java.lang.Runnable runnable, T t) {
        return this.f4511e.submit(runnable, t);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m6025a() {
        if (f4510g == 0) {
            f4510g = java.lang.Math.min(4, com.fossil.blesdk.obfuscated.C1743er.m6567a());
        }
        return f4510g;
    }

    @DexIgnore
    public <T> java.util.concurrent.Future<T> submit(java.util.concurrent.Callable<T> callable) {
        return this.f4511e.submit(callable);
    }
}
