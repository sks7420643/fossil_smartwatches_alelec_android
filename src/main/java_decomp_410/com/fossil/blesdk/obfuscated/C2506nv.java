package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nv */
public interface C2506nv<R> extends java.util.concurrent.Future<R>, com.fossil.blesdk.obfuscated.C1517bw<R> {
}
