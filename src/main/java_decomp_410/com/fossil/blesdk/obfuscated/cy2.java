package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cy2 extends zr2 implements by2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public ay2 j;
    @DexIgnore
    public tr3<ie2> k;
    @DexIgnore
    public vx2 l;
    @DexIgnore
    public ky2 m;
    @DexIgnore
    public InactivityNudgeTimePresenter n;
    @DexIgnore
    public RemindTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cy2.q;
        }

        @DexIgnore
        public final cy2 b() {
            return new cy2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public b(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).l();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public c(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public d(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public e(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public f(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public g(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ts3.a(view);
            vx2 a = this.e.l;
            if (a != null) {
                a.p(0);
            }
            vx2 a2 = this.e.l;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, vx2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public h(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ts3.a(view);
            vx2 a = this.e.l;
            if (a != null) {
                a.p(1);
            }
            vx2 a2 = this.e.l;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, vx2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public i(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ts3.a(view);
            ky2 c = this.e.m;
            if (c != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                c.show(childFragmentManager, ky2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cy2 e;

        @DexIgnore
        public j(cy2 cy2) {
            this.e = cy2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cy2.b(this.e).l();
        }
    }

    /*
    static {
        String simpleName = cy2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationWatchReminde\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ay2 b(cy2 cy2) {
        ay2 ay2 = cy2.j;
        if (ay2 != null) {
            return ay2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void C(boolean z) {
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.E;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void D(boolean z) {
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.C;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                }
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setAlpha(z ? 1.0f : 0.5f);
                }
                ConstraintLayout constraintLayout2 = a2.q;
                kd4.a((Object) constraintLayout2, "clInactivityNudgeContainer");
                int childCount = constraintLayout2.getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    View childAt = a2.q.getChildAt(i2);
                    kd4.a((Object) childAt, "child");
                    childAt.setEnabled(z);
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L(String str) {
        kd4.b(str, LogBuilder.KEY_TIME);
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        ay2 ay2 = this.j;
        if (ay2 != null) {
            ay2.l();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d(SpannableString spannableString) {
        kd4.b(spannableString, LogBuilder.KEY_TIME);
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void e(boolean z) {
        int i2;
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                if (flexibleButton != null) {
                    flexibleButton.setEnabled(z);
                }
                FlexibleButton flexibleButton2 = a2.s;
                if (flexibleButton2 != null) {
                    if (z) {
                        i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.colorPrimary);
                    } else {
                        i2 = k6.a((Context) PortfolioApp.W.c(), (int) R.color.fossilCoolGray);
                    }
                    flexibleButton2.setBackgroundColor(i2);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ie2 ie2 = (ie2) qa.a(layoutInflater, R.layout.fragment_notification_watch_reminders, viewGroup, false, O0());
        this.l = (vx2) getChildFragmentManager().a(vx2.r.a());
        if (this.l == null) {
            this.l = vx2.r.b();
        }
        this.m = (ky2) getChildFragmentManager().a(ky2.r.a());
        if (this.m == null) {
            this.m = ky2.r.b();
        }
        FlexibleTextView flexibleTextView = ie2.w;
        kd4.a((Object) flexibleTextView, "binding.ftvTitle");
        flexibleTextView.setSelected(true);
        ie2.x.setOnClickListener(new b(this));
        ie2.C.setOnClickListener(new c(this));
        ie2.D.setOnClickListener(new d(this));
        ie2.E.setOnClickListener(new e(this));
        ie2.B.setOnClickListener(new f(this));
        ie2.z.setOnClickListener(new g(this));
        ie2.y.setOnClickListener(new h(this));
        ie2.A.setOnClickListener(new i(this));
        ie2.s.setOnClickListener(new j(this));
        this.k = new tr3<>(this, ie2);
        l42 g2 = PortfolioApp.W.c().g();
        vx2 vx2 = this.l;
        if (vx2 != null) {
            ky2 ky2 = this.m;
            if (ky2 != null) {
                g2.a(new oy2(vx2, ky2)).a(this);
                R("reminder_view");
                kd4.a((Object) ie2, "binding");
                return ie2.d();
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimeContract.View");
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimeContract.View");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        ay2 ay2 = this.j;
        if (ay2 != null) {
            ay2.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ay2 ay2 = this.j;
        if (ay2 != null) {
            ay2.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                kd4.a((Object) constraintLayout, "clRemindersContainer");
                constraintLayout.setVisibility(8);
                FlexibleButton flexibleButton = a2.s;
                kd4.a((Object) flexibleButton, "fbSave");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void u(boolean z) {
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.B;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void w(boolean z) {
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.D;
                if (switchCompat != null) {
                    switchCompat.setChecked(z);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(SpannableString spannableString) {
        kd4.b(spannableString, LogBuilder.KEY_TIME);
        tr3<ie2> tr3 = this.k;
        if (tr3 != null) {
            ie2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ay2 ay2) {
        kd4.b(ay2, "presenter");
        this.j = ay2;
    }
}
