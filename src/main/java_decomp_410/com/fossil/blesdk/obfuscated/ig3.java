package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ig3 extends fg3 {
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ b g; // = new b(this);
    @DexIgnore
    public /* final */ gg3 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ ig3 a;

        @DexIgnore
        public b(ig3 ig3) {
            this.a = ig3;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            kd4.b(context, "context");
            kd4.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeUpdateFirmwarePresenter", "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            this.a.f = otaEvent.getSerial();
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && qf4.b(otaEvent.getSerial(), PortfolioApp.W.c().e(), true)) {
                this.a.i().g((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ig3(gg3 gg3) {
        kd4.b(gg3, "mView");
        this.h = gg3;
    }

    @DexIgnore
    public void f() {
        PortfolioApp c = PortfolioApp.W.c();
        b bVar = this.g;
        c.registerReceiver(bVar, new IntentFilter(PortfolioApp.W.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        h();
    }

    @DexIgnore
    public void g() {
        try {
            PortfolioApp.W.c().unregisterReceiver(this.g);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeUpdateFirmwarePresenter", "stop - e=" + e);
        }
    }

    @DexIgnore
    public final void h() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.f);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeUpdateFirmwarePresenter", "serial=" + this.f + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__NeverMissAStepAndKeep));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_diana_two);
            explore3.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Description__GlanceableContentReadyOnTheWrist));
            explore4.setBackground(R.drawable.update_fw_diana_four);
        } else {
            explore.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__TrackActivitySleepAndPersonalGoals));
            explore.setBackground(R.drawable.update_fw_hybrid_one);
            explore2.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ReceiveDiscreteVibrationNotificationsForYour));
            explore2.setBackground(R.drawable.update_fw_hybrid_two);
            explore3.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__ControlTheWorldFromYourWrist));
            explore3.setBackground(R.drawable.update_fw_hybrid_three);
            explore4.setDescription(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Description__NoChargingNeededYourWatchIs));
            explore4.setBackground(R.drawable.update_fw_hybrid_four);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.h.i(arrayList);
    }

    @DexIgnore
    public final gg3 i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h.a(this);
    }
}
