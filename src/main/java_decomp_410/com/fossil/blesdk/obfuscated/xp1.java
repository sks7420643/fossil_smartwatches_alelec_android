package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xp1 extends jk0 implements hp1 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xp1> CREATOR; // = new yp1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ byte[] g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore
    public xp1(int i, String str, byte[] bArr, String str2) {
        this.e = i;
        this.f = str;
        this.g = bArr;
        this.h = str2;
    }

    @DexIgnore
    public final byte[] H() {
        return this.g;
    }

    @DexIgnore
    public final int I() {
        return this.e;
    }

    @DexIgnore
    public final String J() {
        return this.h;
    }

    @DexIgnore
    public final String toString() {
        int i = this.e;
        String str = this.f;
        byte[] bArr = this.g;
        String valueOf = String.valueOf(bArr == null ? "null" : Integer.valueOf(bArr.length));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length());
        sb.append("MessageEventParcelable[");
        sb.append(i);
        sb.append(",");
        sb.append(str);
        sb.append(", size=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final String w() {
        return this.f;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, I());
        kk0.a(parcel, 3, w(), false);
        kk0.a(parcel, 4, H(), false);
        kk0.a(parcel, 5, J(), false);
        kk0.a(parcel, a);
    }
}
