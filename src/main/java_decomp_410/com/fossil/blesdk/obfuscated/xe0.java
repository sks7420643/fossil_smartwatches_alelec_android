package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xe0 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public xe0(Activity activity) {
        bk0.a(activity, (Object) "Activity must not be null");
        this.a = activity;
    }

    @DexIgnore
    public Activity a() {
        return (Activity) this.a;
    }

    @DexIgnore
    public FragmentActivity b() {
        return (FragmentActivity) this.a;
    }

    @DexIgnore
    public boolean c() {
        return this.a instanceof FragmentActivity;
    }

    @DexIgnore
    public final boolean d() {
        return this.a instanceof Activity;
    }
}
