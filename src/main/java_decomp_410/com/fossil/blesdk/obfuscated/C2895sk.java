package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sk */
public class C2895sk extends com.fossil.blesdk.obfuscated.C2971tk<java.lang.Boolean> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.String f9387i; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("BatteryNotLowTracker");

    @DexIgnore
    public C2895sk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(context, zlVar);
    }

    @DexIgnore
    /* renamed from: d */
    public android.content.IntentFilter mo15616d() {
        android.content.IntentFilter intentFilter = new android.content.IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Boolean m13788a() {
        android.content.Intent registerReceiver = this.f10017b.registerReceiver((android.content.BroadcastReceiver) null, new android.content.IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9387i, "getInitialState - null intent received", new java.lang.Throwable[0]);
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("plugged", 0);
        int intExtra2 = registerReceiver.getIntExtra("status", -1);
        float intExtra3 = ((float) registerReceiver.getIntExtra(com.zendesk.sdk.support.help.HelpRecyclerViewAdapter.CategoryViewHolder.ROTATION_PROPERTY_NAME, -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
        boolean z = true;
        if (intExtra == 0 && intExtra2 != 1 && intExtra3 <= 0.15f) {
            z = false;
        }
        return java.lang.Boolean.valueOf(z);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15614a(android.content.Context context, android.content.Intent intent) {
        if (intent.getAction() != null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9387i, java.lang.String.format("Received %s", new java.lang.Object[]{intent.getAction()}), new java.lang.Throwable[0]);
            java.lang.String action = intent.getAction();
            char c = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -1980154005) {
                if (hashCode == 490310653 && action.equals("android.intent.action.BATTERY_LOW")) {
                    c = 1;
                }
            } else if (action.equals("android.intent.action.BATTERY_OKAY")) {
                c = 0;
            }
            if (c == 0) {
                mo16804a(true);
            } else if (c == 1) {
                mo16804a(false);
            }
        }
    }
}
