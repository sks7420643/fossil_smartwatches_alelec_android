package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.StreetViewPanoramaOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class df1 extends a51 implements cf1 {
    @DexIgnore
    public df1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICreator");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final le1 a(sn0 sn0, GoogleMapOptions googleMapOptions) throws RemoteException {
        le1 le1;
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        c51.a(o, (Parcelable) googleMapOptions);
        Parcel a = a(3, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            le1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapViewDelegate");
            if (queryLocalInterface instanceof le1) {
                le1 = queryLocalInterface;
            } else {
                le1 = new gf1(readStrongBinder);
            }
        }
        a.recycle();
        return le1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ke1 c(sn0 sn0) throws RemoteException {
        ke1 ke1;
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        Parcel a = a(2, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            ke1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IMapFragmentDelegate");
            if (queryLocalInterface instanceof ke1) {
                ke1 = queryLocalInterface;
            } else {
                ke1 = new ff1(readStrongBinder);
            }
        }
        a.recycle();
        return ke1;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ie1 zze() throws RemoteException {
        ie1 ie1;
        Parcel a = a(4, o());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            ie1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
            if (queryLocalInterface instanceof ie1) {
                ie1 = queryLocalInterface;
            } else {
                ie1 = new ue1(readStrongBinder);
            }
        }
        a.recycle();
        return ie1;
    }

    @DexIgnore
    public final d51 zzf() throws RemoteException {
        Parcel a = a(5, o());
        d51 a2 = e51.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(sn0 sn0, int i) throws RemoteException {
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        o.writeInt(i);
        b(6, o);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final ne1 a(sn0 sn0, StreetViewPanoramaOptions streetViewPanoramaOptions) throws RemoteException {
        ne1 ne1;
        Parcel o = o();
        c51.a(o, (IInterface) sn0);
        c51.a(o, (Parcelable) streetViewPanoramaOptions);
        Parcel a = a(7, o);
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            ne1 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
            if (queryLocalInterface instanceof ne1) {
                ne1 = queryLocalInterface;
            } else {
                ne1 = new ye1(readStrongBinder);
            }
        }
        a.recycle();
        return ne1;
    }
}
