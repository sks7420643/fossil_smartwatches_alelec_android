package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qa {
    @DexIgnore
    public static na a; // = new oa();
    @DexIgnore
    public static pa b; // = null;

    @DexIgnore
    public static pa a() {
        return b;
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, i, viewGroup, z, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z, pa paVar) {
        int i2 = 0;
        boolean z2 = viewGroup != null && z;
        if (z2) {
            i2 = viewGroup.getChildCount();
        }
        View inflate = layoutInflater.inflate(i, viewGroup, z);
        if (z2) {
            return a(paVar, viewGroup, i2, i);
        }
        return a(paVar, inflate, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pa paVar, View[] viewArr, int i) {
        return a.a(paVar, viewArr, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pa paVar, View view, int i) {
        return a.a(paVar, view, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i) {
        return a(activity, i, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Activity activity, int i, pa paVar) {
        activity.setContentView(i);
        return a(paVar, (ViewGroup) activity.getWindow().getDecorView().findViewById(16908290), 0, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T a(pa paVar, ViewGroup viewGroup, int i, int i2) {
        int childCount = viewGroup.getChildCount();
        int i3 = childCount - i;
        if (i3 == 1) {
            return a(paVar, viewGroup.getChildAt(childCount - 1), i2);
        }
        View[] viewArr = new View[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            viewArr[i4] = viewGroup.getChildAt(i4 + i);
        }
        return a(paVar, viewArr, i2);
    }
}
