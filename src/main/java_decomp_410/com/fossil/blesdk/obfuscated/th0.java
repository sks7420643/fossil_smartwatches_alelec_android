package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ve0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class th0<T> extends fh0 {
    @DexIgnore
    public /* final */ xn1<T> a;

    @DexIgnore
    public th0(int i, xn1<T> xn1) {
        super(i);
        this.a = xn1;
    }

    @DexIgnore
    public void a(Status status) {
        this.a.b((Exception) new ApiException(status));
    }

    @DexIgnore
    public abstract void d(ve0.a<?> aVar) throws RemoteException;

    @DexIgnore
    public void a(RuntimeException runtimeException) {
        this.a.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(ve0.a<?> aVar) throws DeadObjectException {
        try {
            d(aVar);
        } catch (DeadObjectException e) {
            a(ig0.a((RemoteException) e));
            throw e;
        } catch (RemoteException e2) {
            a(ig0.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
