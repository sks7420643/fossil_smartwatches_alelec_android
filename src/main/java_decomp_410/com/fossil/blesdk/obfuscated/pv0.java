package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pv0 {
    @DexIgnore
    public static /* final */ nv0 a; // = c();
    @DexIgnore
    public static /* final */ nv0 b; // = new ov0();

    @DexIgnore
    public static nv0 a() {
        return a;
    }

    @DexIgnore
    public static nv0 b() {
        return b;
    }

    @DexIgnore
    public static nv0 c() {
        try {
            return (nv0) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
