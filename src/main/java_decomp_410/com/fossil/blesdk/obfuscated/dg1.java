package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dg1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ Long g;
    @DexIgnore
    public /* final */ Long h;
    @DexIgnore
    public /* final */ Long i;
    @DexIgnore
    public /* final */ Boolean j;

    @DexIgnore
    public dg1(String str, String str2, long j2, long j3, long j4, long j5, Long l, Long l2, Long l3, Boolean bool) {
        long j6 = j2;
        long j7 = j3;
        long j8 = j5;
        bk0.b(str);
        bk0.b(str2);
        boolean z = true;
        bk0.a(j6 >= 0);
        bk0.a(j7 >= 0);
        bk0.a(j8 < 0 ? false : z);
        this.a = str;
        this.b = str2;
        this.c = j6;
        this.d = j7;
        this.e = j4;
        this.f = j8;
        this.g = l;
        this.h = l2;
        this.i = l3;
        this.j = bool;
    }

    @DexIgnore
    public final dg1 a(long j2) {
        return new dg1(this.a, this.b, this.c, this.d, j2, this.f, this.g, this.h, this.i, this.j);
    }

    @DexIgnore
    public final dg1 a(long j2, long j3) {
        return new dg1(this.a, this.b, this.c, this.d, this.e, j2, Long.valueOf(j3), this.h, this.i, this.j);
    }

    @DexIgnore
    public final dg1 a(Long l, Long l2, Boolean bool) {
        return new dg1(this.a, this.b, this.c, this.d, this.e, this.f, this.g, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }
}
