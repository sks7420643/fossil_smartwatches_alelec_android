package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tk1 extends pk1 {
    @DexIgnore
    public Handler c;
    @DexIgnore
    public long d; // = c().c();
    @DexIgnore
    public long e; // = this.d;
    @DexIgnore
    public /* final */ fm1 f; // = new uk1(this, this.a);
    @DexIgnore
    public /* final */ fm1 g; // = new vk1(this, this.a);

    @DexIgnore
    public tk1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final void A() {
        synchronized (this) {
            if (this.c == null) {
                this.c = new m51(Looper.getMainLooper());
            }
        }
    }

    @DexIgnore
    public final void B() {
        e();
        d(c().b());
    }

    @DexIgnore
    public final long C() {
        long c2 = c().c();
        long j = c2 - this.e;
        this.e = c2;
        return j;
    }

    @DexIgnore
    public final void D() {
        e();
        a(false, false);
        n().a(c().c());
    }

    @DexIgnore
    public final void a(long j) {
        e();
        A();
        if (l().d(p().B(), jg1.j0)) {
            k().v.a(false);
        }
        d().A().a("Activity resumed, time", Long.valueOf(j));
        this.d = j;
        this.e = this.d;
        if (l().q(p().B())) {
            b(c().b());
            return;
        }
        this.f.a();
        this.g.a();
        if (k().a(c().b())) {
            k().r.a(true);
            k().t.a(0);
        }
        if (k().r.a()) {
            this.f.a(Math.max(0, k().p.a() - k().t.a()));
        } else {
            this.g.a(Math.max(0, 3600000 - k().t.a()));
        }
    }

    @DexIgnore
    public final void b(long j) {
        e();
        A();
        a(j, false);
    }

    @DexIgnore
    public final void c(long j) {
        e();
        A();
        if (l().d(p().B(), jg1.j0)) {
            k().v.a(true);
        }
        this.f.a();
        this.g.a();
        d().A().a("Activity paused, time", Long.valueOf(j));
        if (this.d != 0) {
            k().t.a(k().t.a() + (j - this.d));
        }
    }

    @DexIgnore
    public final void d(long j) {
        e();
        d().A().a("Session started, time", Long.valueOf(c().c()));
        long j2 = null;
        Long valueOf = l().o(p().B()) ? Long.valueOf(j / 1000) : null;
        if (l().p(p().B())) {
            j2 = -1L;
        }
        long j3 = j;
        o().a("auto", "_sid", (Object) valueOf, j3);
        o().a("auto", "_sno", (Object) j2, j3);
        k().r.a(false);
        Bundle bundle = new Bundle();
        if (l().o(p().B())) {
            bundle.putLong("_sid", valueOf.longValue());
        }
        o().a("auto", "_s", j, bundle);
        k().s.a(j);
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final void a(long j, boolean z) {
        e();
        A();
        this.f.a();
        this.g.a();
        if (k().a(j)) {
            k().r.a(true);
            k().t.a(0);
        }
        if (z && l().r(p().B())) {
            k().s.a(j);
        }
        if (k().r.a()) {
            d(j);
        } else {
            this.g.a(Math.max(0, 3600000 - k().t.a()));
        }
    }

    @DexIgnore
    public final boolean a(boolean z, boolean z2) {
        e();
        v();
        long c2 = c().c();
        k().s.a(c().b());
        long j = c2 - this.d;
        if (z || j >= 1000) {
            k().t.a(j);
            d().A().a("Recording user engagement, ms", Long.valueOf(j));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j);
            rj1.a(r().A(), bundle, true);
            if (l().s(p().B())) {
                if (l().d(p().B(), jg1.m0)) {
                    if (!z2) {
                        C();
                    }
                } else if (z2) {
                    bundle.putLong("_fr", 1);
                } else {
                    C();
                }
            }
            if (!l().d(p().B(), jg1.m0) || !z2) {
                o().b("auto", "_e", bundle);
            }
            this.d = c2;
            this.g.a();
            this.g.a(Math.max(0, 3600000 - k().t.a()));
            return true;
        }
        d().A().a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j));
        return false;
    }
}
