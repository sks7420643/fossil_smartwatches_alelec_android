package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nt extends mt<Drawable> {
    @DexIgnore
    public nt(Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    public static aq<Drawable> a(Drawable drawable) {
        if (drawable != null) {
            return new nt(drawable);
        }
        return null;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public int b() {
        return Math.max(1, this.e.getIntrinsicWidth() * this.e.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    public Class<Drawable> c() {
        return this.e.getClass();
    }
}
