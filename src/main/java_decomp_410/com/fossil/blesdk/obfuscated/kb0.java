package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;
import android.util.Base64;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import com.fossil.blesdk.utils.EncryptionAES128;
import java.security.NoSuchAlgorithmException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kb0 {
    @DexIgnore
    public static /* final */ EncryptionAES128.Transformation a; // = EncryptionAES128.Transformation.CBC_PKCS5_PADDING;
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[16];
    @DexIgnore
    public static /* final */ kb0 c; // = new kb0();

    @DexIgnore
    public final byte[] a() throws NoSuchAlgorithmException {
        SharedPreferences a2 = ya0.a(SharedPreferenceFileName.TEXT_ENCRYPTION_PREFERENCE);
        if (a2 != null) {
            String string = a2.getString("text_encryption_secret_key", (String) null);
            if (string == null) {
                string = a2.getString("text_encryption_key", (String) null);
                a2.edit().putString("text_encryption_secret_key", string).apply();
            }
            if (string == null) {
                string = Base64.encodeToString(EncryptionAES128.a.a().getEncoded(), 0);
                a2.edit().putString("text_encryption_secret_key", string).apply();
            }
            byte[] decode = Base64.decode(string, 0);
            kd4.a((Object) decode, "Base64.decode(base64, Base64.DEFAULT)");
            return decode;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final String b(String str) {
        kd4.b(str, "text");
        String str2 = new String();
        try {
            byte[] a2 = a();
            EncryptionAES128 encryptionAES128 = EncryptionAES128.a;
            EncryptionAES128.Transformation transformation = a;
            byte[] bArr = b;
            byte[] bytes = str.getBytes(ua0.y.f());
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            String encodeToString = Base64.encodeToString(encryptionAES128.b(transformation, a2, bArr, bytes), 2);
            kd4.a((Object) encodeToString, "Base64.encodeToString(encrypted, Base64.NO_WRAP)");
            return encodeToString;
        } catch (Exception e) {
            da0.l.a(e);
            return str2;
        }
    }

    @DexIgnore
    public final String a(String str) {
        kd4.b(str, "encryptedText");
        try {
            byte[] a2 = a();
            EncryptionAES128 encryptionAES128 = EncryptionAES128.a;
            EncryptionAES128.Transformation transformation = a;
            byte[] bArr = b;
            byte[] decode = Base64.decode(str, 2);
            kd4.a((Object) decode, "Base64.decode(encryptedText, Base64.NO_WRAP)");
            return new String(encryptionAES128.a(transformation, a2, bArr, decode), ua0.y.f());
        } catch (Exception e) {
            try {
                byte[] decode2 = Base64.decode(str, 2);
                kd4.a((Object) decode2, "Base64.decode(encryptedText, Base64.NO_WRAP)");
                String str2 = new String(decode2, bf4.a);
                new JSONObject(str2);
                return str2;
            } catch (JSONException unused) {
                da0.l.a(e);
                return null;
            }
        }
    }
}
