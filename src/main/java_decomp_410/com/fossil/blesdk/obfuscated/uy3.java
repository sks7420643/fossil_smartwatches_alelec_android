package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uy3 {
    @DexIgnore
    public Context a; // = null;

    @DexIgnore
    public uy3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void a(ry3 ry3) {
        if (ry3 != null) {
            String ry32 = ry3.toString();
            if (a()) {
                a(wy3.d(ry32));
            }
        }
    }

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public final ry3 c() {
        String c = a() ? wy3.c(b()) : null;
        if (c != null) {
            return ry3.a(c);
        }
        return null;
    }
}
