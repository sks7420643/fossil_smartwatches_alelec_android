package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ String h;
    @DexIgnore
    public /* final */ /* synthetic */ boolean i;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 j;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 k;

    @DexIgnore
    public fk1(vj1 vj1, AtomicReference atomicReference, String str, String str2, String str3, boolean z, rl1 rl1) {
        this.k = vj1;
        this.e = atomicReference;
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.i = z;
        this.j = rl1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.e) {
            try {
                kg1 d = this.k.d;
                if (d == null) {
                    this.k.d().s().a("Failed to get user properties", tg1.a(this.f), this.g, this.h);
                    this.e.set(Collections.emptyList());
                    this.e.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f)) {
                    this.e.set(d.a(this.g, this.h, this.i, this.j));
                } else {
                    this.e.set(d.a(this.f, this.g, this.h, this.i));
                }
                this.k.C();
                this.e.notify();
            } catch (RemoteException e2) {
                try {
                    this.k.d().s().a("Failed to get user properties", tg1.a(this.f), this.g, e2);
                    this.e.set(Collections.emptyList());
                    this.e.notify();
                } catch (Throwable th) {
                    this.e.notify();
                    throw th;
                }
            }
        }
    }
}
