package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oc4 extends lc4 {
    @DexIgnore
    public void a(Throwable th, Throwable th2) {
        kd4.b(th, "cause");
        kd4.b(th2, "exception");
        th.addSuppressed(th2);
    }
}
