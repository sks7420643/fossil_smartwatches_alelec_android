package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hq2 {
    @DexIgnore
    public Object a;
    @DexIgnore
    public String b;

    @DexIgnore
    public hq2(String str) {
        kd4.b(str, "tagName");
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Object b() {
        return this.a;
    }

    @DexIgnore
    public final void a(Object obj) {
        this.a = obj;
    }
}
