package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface nv0 {
    @DexIgnore
    int a(int i, Object obj, Object obj2);

    @DexIgnore
    mv0<?, ?> a(Object obj);

    @DexIgnore
    Object b(Object obj);

    @DexIgnore
    Object c(Object obj);

    @DexIgnore
    boolean d(Object obj);

    @DexIgnore
    Map<?, ?> e(Object obj);

    @DexIgnore
    Map<?, ?> f(Object obj);

    @DexIgnore
    Object zzb(Object obj, Object obj2);
}
