package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bw */
public interface C1517bw<R> extends com.fossil.blesdk.obfuscated.C2835ru {
    @DexIgnore
    /* renamed from: a */
    void mo9312a(android.graphics.drawable.Drawable drawable);

    @DexIgnore
    /* renamed from: a */
    void mo9313a(com.fossil.blesdk.obfuscated.C1449aw awVar);

    @DexIgnore
    /* renamed from: a */
    void mo9314a(com.fossil.blesdk.obfuscated.C2604ov ovVar);

    @DexIgnore
    /* renamed from: a */
    void mo9315a(R r, com.fossil.blesdk.obfuscated.C1750ew<? super R> ewVar);

    @DexIgnore
    /* renamed from: b */
    void mo9316b(android.graphics.drawable.Drawable drawable);

    @DexIgnore
    /* renamed from: b */
    void mo9317b(com.fossil.blesdk.obfuscated.C1449aw awVar);

    @DexIgnore
    /* renamed from: c */
    void mo9318c(android.graphics.drawable.Drawable drawable);

    @DexIgnore
    /* renamed from: d */
    com.fossil.blesdk.obfuscated.C2604ov mo9319d();
}
