package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eo3 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, SignUpPresenter signUpPresenter) {
        signUpActivity.B = signUpPresenter;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, in2 in2) {
        signUpActivity.C = in2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, jn2 jn2) {
        signUpActivity.D = jn2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, kn2 kn2) {
        signUpActivity.E = kn2;
    }

    @DexIgnore
    public static void a(SignUpActivity signUpActivity, MFLoginWechatManager mFLoginWechatManager) {
        signUpActivity.F = mFLoginWechatManager;
    }
}
