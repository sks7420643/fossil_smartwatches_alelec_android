package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ry */
public class C2845ry implements com.fossil.blesdk.obfuscated.C1588cz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1757ez f9157a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2611oz f9158b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ry$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ry$a */
    public static /* synthetic */ class C2846a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f9159a; // = new int[com.crashlytics.android.core.Report.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            f9159a[com.crashlytics.android.core.Report.Type.JAVA.ordinal()] = 1;
            f9159a[com.crashlytics.android.core.Report.Type.NATIVE.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public C2845ry(com.fossil.blesdk.obfuscated.C1757ez ezVar, com.fossil.blesdk.obfuscated.C2611oz ozVar) {
        this.f9157a = ezVar;
        this.f9158b = ozVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9713a(com.fossil.blesdk.obfuscated.C1520bz bzVar) {
        int i = com.fossil.blesdk.obfuscated.C2845ry.C2846a.f9159a[bzVar.f3947b.getType().ordinal()];
        if (i == 1) {
            this.f9157a.mo9713a(bzVar);
            return true;
        } else if (i != 2) {
            return false;
        } else {
            this.f9158b.mo9713a(bzVar);
            return true;
        }
    }
}
