package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iu3 extends ic {
    @DexIgnore
    public /* final */ MutableLiveData<Integer> c; // = new MutableLiveData<>();

    @DexIgnore
    public final MutableLiveData<Integer> c() {
        return this.c;
    }
}
