package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ls4 implements gr4<em4, Long> {
    @DexIgnore
    public static /* final */ ls4 a; // = new ls4();

    @DexIgnore
    public Long a(em4 em4) throws IOException {
        return Long.valueOf(em4.F());
    }
}
