package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.o81;
import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import com.google.android.gms.internal.measurement.zzxs;
import com.google.android.gms.internal.measurement.zzxx;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m81<FieldDescriptorType extends o81<FieldDescriptorType>> {
    @DexIgnore
    public static /* final */ m81 d; // = new m81(true);
    @DexIgnore
    public /* final */ qa1<FieldDescriptorType, Object> a; // = qa1.c(16);
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c; // = false;

    @DexIgnore
    public m81() {
    }

    @DexIgnore
    public static <T extends o81<T>> m81<T> i() {
        return d;
    }

    @DexIgnore
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> a() {
        if (this.c) {
            return new e91(this.a.e().iterator());
        }
        return this.a.e().iterator();
    }

    @DexIgnore
    public final boolean b() {
        return this.a.isEmpty();
    }

    @DexIgnore
    public final boolean c() {
        return this.b;
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        m81 m81 = new m81();
        for (int i = 0; i < this.a.c(); i++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i);
            m81.a((o81) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.d()) {
            m81.a((o81) next.getKey(), next.getValue());
        }
        m81.c = this.c;
        return m81;
    }

    @DexIgnore
    public final boolean d() {
        for (int i = 0; i < this.a.c(); i++) {
            if (!b(this.a.a(i))) {
                return false;
            }
        }
        for (Map.Entry<FieldDescriptorType, Object> b2 : this.a.d()) {
            if (!b(b2)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final Iterator<Map.Entry<FieldDescriptorType, Object>> e() {
        if (this.c) {
            return new e91(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m81)) {
            return false;
        }
        return this.a.equals(((m81) obj).a);
    }

    @DexIgnore
    public final void f() {
        if (!this.b) {
            this.a.b();
            this.b = true;
        }
    }

    @DexIgnore
    public final int g() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.c(); i2++) {
            Map.Entry<FieldDescriptorType, Object> a2 = this.a.a(i2);
            i += b((o81<?>) (o81) a2.getKey(), a2.getValue());
        }
        for (Map.Entry next : this.a.d()) {
            i += b((o81<?>) (o81) next.getKey(), next.getValue());
        }
        return i;
    }

    @DexIgnore
    public final int h() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.c(); i2++) {
            i += c(this.a.a(i2));
        }
        for (Map.Entry<FieldDescriptorType, Object> c2 : this.a.d()) {
            i += c(c2);
        }
        return i;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public static boolean b(Map.Entry<FieldDescriptorType, Object> entry) {
        o81 o81 = (o81) entry.getKey();
        if (o81.h() == zzxx.MESSAGE) {
            if (o81.g()) {
                for (w91 a2 : (List) entry.getValue()) {
                    if (!a2.a()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof w91) {
                    if (!((w91) value).a()) {
                        return false;
                    }
                } else if (value instanceof a91) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static int c(Map.Entry<FieldDescriptorType, Object> entry) {
        o81 o81 = (o81) entry.getKey();
        Object value = entry.getValue();
        if (o81.h() != zzxx.MESSAGE || o81.g() || o81.e()) {
            return b((o81<?>) o81, value);
        }
        if (value instanceof a91) {
            return zztv.b(((o81) entry.getKey()).zzc(), (f91) (a91) value);
        }
        return zztv.d(((o81) entry.getKey()).zzc(), (w91) value);
    }

    @DexIgnore
    public m81(boolean z) {
        f();
    }

    @DexIgnore
    public final Object a(FieldDescriptorType fielddescriptortype) {
        Object obj = this.a.get(fielddescriptortype);
        if (!(obj instanceof a91)) {
            return obj;
        }
        a91.c();
        throw null;
    }

    @DexIgnore
    public final void a(FieldDescriptorType fielddescriptortype, Object obj) {
        if (!fielddescriptortype.g()) {
            a(fielddescriptortype.f(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                a(fielddescriptortype.f(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof a91) {
            this.c = true;
        }
        this.a.put(fielddescriptortype, obj);
    }

    @DexIgnore
    public static int b(zzxs zzxs, Object obj) {
        switch (n81.b[zzxs.ordinal()]) {
            case 1:
                return zztv.b(((Double) obj).doubleValue());
            case 2:
                return zztv.b(((Float) obj).floatValue());
            case 3:
                return zztv.d(((Long) obj).longValue());
            case 4:
                return zztv.e(((Long) obj).longValue());
            case 5:
                return zztv.f(((Integer) obj).intValue());
            case 6:
                return zztv.g(((Long) obj).longValue());
            case 7:
                return zztv.i(((Integer) obj).intValue());
            case 8:
                return zztv.b(((Boolean) obj).booleanValue());
            case 9:
                return zztv.b((w91) obj);
            case 10:
                if (obj instanceof a91) {
                    return zztv.a((f91) (a91) obj);
                }
                return zztv.a((w91) obj);
            case 11:
                if (obj instanceof zzte) {
                    return zztv.a((zzte) obj);
                }
                return zztv.a((String) obj);
            case 12:
                if (obj instanceof zzte) {
                    return zztv.a((zzte) obj);
                }
                return zztv.b((byte[]) obj);
            case 13:
                return zztv.g(((Integer) obj).intValue());
            case 14:
                return zztv.j(((Integer) obj).intValue());
            case 15:
                return zztv.h(((Long) obj).longValue());
            case 16:
                return zztv.h(((Integer) obj).intValue());
            case 17:
                return zztv.f(((Long) obj).longValue());
            case 18:
                if (obj instanceof w81) {
                    return zztv.k(((w81) obj).zzc());
                }
                return zztv.k(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001b, code lost:
        if ((r3 instanceof com.fossil.blesdk.obfuscated.a91) == false) goto L_0x0043;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
        if ((r3 instanceof com.fossil.blesdk.obfuscated.w81) == false) goto L_0x0043;
     */
    @DexIgnore
    public static void a(zzxs zzxs, Object obj) {
        boolean z;
        v81.a(obj);
        boolean z2 = false;
        switch (n81.a[zzxs.zzyv().ordinal()]) {
            case 1:
                z = obj instanceof Integer;
                break;
            case 2:
                z = obj instanceof Long;
                break;
            case 3:
                z = obj instanceof Float;
                break;
            case 4:
                z = obj instanceof Double;
                break;
            case 5:
                z = obj instanceof Boolean;
                break;
            case 6:
                z = obj instanceof String;
                break;
            case 7:
                if (!(obj instanceof zzte)) {
                    break;
                }
            case 8:
                if (!(obj instanceof Integer)) {
                    break;
                }
            case 9:
                if (!(obj instanceof w91)) {
                    break;
                }
        }
        z2 = z;
        if (!z2) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    @DexIgnore
    public final void a(m81<FieldDescriptorType> m81) {
        for (int i = 0; i < m81.a.c(); i++) {
            a(m81.a.a(i));
        }
        for (Map.Entry<FieldDescriptorType, Object> a2 : m81.a.d()) {
            a(a2);
        }
    }

    @DexIgnore
    public static Object a(Object obj) {
        if (obj instanceof da1) {
            return ((da1) obj).p();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public static int b(o81<?> o81, Object obj) {
        zzxs f = o81.f();
        int zzc = o81.zzc();
        if (!o81.g()) {
            return a(f, zzc, obj);
        }
        int i = 0;
        if (o81.e()) {
            for (Object b2 : (List) obj) {
                i += b(f, b2);
            }
            return zztv.e(zzc) + i + zztv.m(i);
        }
        for (Object a2 : (List) obj) {
            i += a(f, zzc, a2);
        }
        return i;
    }

    @DexIgnore
    public final void a(Map.Entry<FieldDescriptorType, Object> entry) {
        Object obj;
        o81 o81 = (o81) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof a91) {
            a91.c();
            throw null;
        } else if (o81.g()) {
            Object a2 = a(o81);
            if (a2 == null) {
                a2 = new ArrayList();
            }
            for (Object a3 : (List) value) {
                ((List) a2).add(a(a3));
            }
            this.a.put(o81, a2);
        } else if (o81.h() == zzxx.MESSAGE) {
            Object a4 = a(o81);
            if (a4 == null) {
                this.a.put(o81, a(value));
                return;
            }
            if (a4 instanceof da1) {
                obj = o81.a((da1) a4, (da1) value);
            } else {
                obj = o81.a(((w91) a4).c(), (w91) value).t();
            }
            this.a.put(o81, obj);
        } else {
            this.a.put(o81, a(value));
        }
    }

    @DexIgnore
    public static int a(zzxs zzxs, int i, Object obj) {
        int e = zztv.e(i);
        if (zzxs == zzxs.GROUP) {
            v81.a((w91) obj);
            e <<= 1;
        }
        return e + b(zzxs, obj);
    }
}
