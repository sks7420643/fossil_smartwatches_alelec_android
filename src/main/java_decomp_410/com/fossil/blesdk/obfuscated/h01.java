package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h01 extends k21<e11> {
    @DexIgnore
    public static /* final */ de0.g<h01> E; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0<Object> F; // = new de0<>("Fitness.SESSIONS_API", new k01(), E);
    @DexIgnore
    public static /* final */ de0<so0> G; // = new de0<>("Fitness.SESSIONS_CLIENT", new m01(), E);

    @DexIgnore
    public h01(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 58, bVar, cVar, kj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
        if (queryLocalInterface instanceof e11) {
            return (e11) queryLocalInterface;
        }
        return new f11(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.SessionsApi";
    }
}
