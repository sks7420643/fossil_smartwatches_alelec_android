package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class po2<T> extends qo2<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ServerError b;
    @DexIgnore
    public /* final */ Throwable c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public po2(int i, ServerError serverError, Throwable th, String str) {
        super((fd4) null);
        this.a = i;
        this.b = serverError;
        this.c = th;
        this.d = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final ServerError c() {
        return this.b;
    }

    @DexIgnore
    public final Throwable d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof po2) {
                po2 po2 = (po2) obj;
                if (!(this.a == po2.a) || !kd4.a((Object) this.b, (Object) po2.b) || !kd4.a((Object) this.c, (Object) po2.c) || !kd4.a((Object) this.d, (Object) po2.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a * 31;
        ServerError serverError = this.b;
        int i2 = 0;
        int hashCode = (i + (serverError != null ? serverError.hashCode() : 0)) * 31;
        Throwable th = this.c;
        int hashCode2 = (hashCode + (th != null ? th.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode2 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.a + ", serverError=" + this.b + ", throwable=" + this.c + ", errorItems=" + this.d + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ po2(int i, ServerError serverError, Throwable th, String str, int i2, fd4 fd4) {
        this(i, serverError, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str);
    }
}
