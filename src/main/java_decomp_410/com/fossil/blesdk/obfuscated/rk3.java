package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rk3 implements Factory<ExploreWatchPresenter> {
    @DexIgnore
    public static ExploreWatchPresenter a(ok3 ok3, vj2 vj2, DeviceRepository deviceRepository) {
        return new ExploreWatchPresenter(ok3, vj2, deviceRepository);
    }
}
