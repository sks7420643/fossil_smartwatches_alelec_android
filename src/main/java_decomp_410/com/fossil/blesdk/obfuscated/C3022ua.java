package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ua */
public class C3022ua extends com.fossil.blesdk.obfuscated.C2469na {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ android.util.SparseIntArray f9896a; // = new android.util.SparseIntArray(0);

    @DexIgnore
    /* renamed from: a */
    public androidx.databinding.ViewDataBinding mo13885a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View view, int i) {
        if (f9896a.get(i) <= 0 || view.getTag() != null) {
            return null;
        }
        throw new java.lang.RuntimeException("view must have a tag");
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.databinding.ViewDataBinding mo13886a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || f9896a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new java.lang.RuntimeException("view must have a tag");
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C2469na> mo13887a() {
        return new java.util.ArrayList(0);
    }
}
