package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lv */
public abstract class C2354lv<T extends com.fossil.blesdk.obfuscated.C2354lv<T>> implements java.lang.Cloneable {

    @DexIgnore
    /* renamed from: A */
    public boolean f7318A;

    @DexIgnore
    /* renamed from: B */
    public boolean f7319B;

    @DexIgnore
    /* renamed from: C */
    public boolean f7320C; // = true;

    @DexIgnore
    /* renamed from: D */
    public boolean f7321D;

    @DexIgnore
    /* renamed from: e */
    public int f7322e;

    @DexIgnore
    /* renamed from: f */
    public float f7323f; // = 1.0f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2663pp f7324g; // = com.fossil.blesdk.obfuscated.C2663pp.f8418d;

    @DexIgnore
    /* renamed from: h */
    public com.bumptech.glide.Priority f7325h; // = com.bumptech.glide.Priority.NORMAL;

    @DexIgnore
    /* renamed from: i */
    public android.graphics.drawable.Drawable f7326i;

    @DexIgnore
    /* renamed from: j */
    public int f7327j;

    @DexIgnore
    /* renamed from: k */
    public android.graphics.drawable.Drawable f7328k;

    @DexIgnore
    /* renamed from: l */
    public int f7329l;

    @DexIgnore
    /* renamed from: m */
    public boolean f7330m; // = true;

    @DexIgnore
    /* renamed from: n */
    public int f7331n; // = -1;

    @DexIgnore
    /* renamed from: o */
    public int f7332o; // = -1;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C2143jo f7333p; // = com.fossil.blesdk.obfuscated.C2077iw.m8663a();

    @DexIgnore
    /* renamed from: q */
    public boolean f7334q;

    @DexIgnore
    /* renamed from: r */
    public boolean f7335r; // = true;

    @DexIgnore
    /* renamed from: s */
    public android.graphics.drawable.Drawable f7336s;

    @DexIgnore
    /* renamed from: t */
    public int f7337t;

    @DexIgnore
    /* renamed from: u */
    public com.fossil.blesdk.obfuscated.C2337lo f7338u; // = new com.fossil.blesdk.obfuscated.C2337lo();

    @DexIgnore
    /* renamed from: v */
    public java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> f7339v; // = new com.fossil.blesdk.obfuscated.C2355lw();

    @DexIgnore
    /* renamed from: w */
    public java.lang.Class<?> f7340w; // = java.lang.Object.class;

    @DexIgnore
    /* renamed from: x */
    public boolean f7341x;

    @DexIgnore
    /* renamed from: y */
    public android.content.res.Resources.Theme f7342y;

    @DexIgnore
    /* renamed from: z */
    public boolean f7343z;

    @DexIgnore
    /* renamed from: b */
    public static boolean m10342b(int i, int i2) {
        return (i & i2) != 0;
    }

    @DexIgnore
    /* renamed from: A */
    public final float mo13409A() {
        return this.f7323f;
    }

    @DexIgnore
    /* renamed from: B */
    public final android.content.res.Resources.Theme mo13410B() {
        return this.f7342y;
    }

    @DexIgnore
    /* renamed from: C */
    public final java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> mo13411C() {
        return this.f7339v;
    }

    @DexIgnore
    /* renamed from: D */
    public final boolean mo13412D() {
        return this.f7321D;
    }

    @DexIgnore
    /* renamed from: E */
    public final boolean mo13413E() {
        return this.f7318A;
    }

    @DexIgnore
    /* renamed from: F */
    public final boolean mo13414F() {
        return mo13445a(4);
    }

    @DexIgnore
    /* renamed from: G */
    public final boolean mo13415G() {
        return this.f7330m;
    }

    @DexIgnore
    /* renamed from: H */
    public final boolean mo13416H() {
        return mo13445a(8);
    }

    @DexIgnore
    /* renamed from: I */
    public boolean mo13417I() {
        return this.f7320C;
    }

    @DexIgnore
    /* renamed from: J */
    public final boolean mo13418J() {
        return mo13445a(256);
    }

    @DexIgnore
    /* renamed from: K */
    public final boolean mo13419K() {
        return this.f7335r;
    }

    @DexIgnore
    /* renamed from: L */
    public final boolean mo13420L() {
        return this.f7334q;
    }

    @DexIgnore
    /* renamed from: M */
    public final boolean mo13421M() {
        return mo13445a(2048);
    }

    @DexIgnore
    /* renamed from: N */
    public final boolean mo13422N() {
        return com.fossil.blesdk.obfuscated.C3066uw.m14933b(this.f7332o, this.f7331n);
    }

    @DexIgnore
    /* renamed from: O */
    public T mo13423O() {
        this.f7341x = true;
        mo13427S();
        return this;
    }

    @DexIgnore
    /* renamed from: P */
    public T mo13424P() {
        return mo13448b(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2118c, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) new com.fossil.blesdk.obfuscated.C2985ts());
    }

    @DexIgnore
    /* renamed from: Q */
    public T mo13425Q() {
        return mo13434a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2117b, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) new com.fossil.blesdk.obfuscated.C3059us());
    }

    @DexIgnore
    /* renamed from: R */
    public T mo13426R() {
        return mo13434a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2116a, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) new com.fossil.blesdk.obfuscated.C1446at());
    }

    @DexIgnore
    /* renamed from: S */
    public final T mo13427S() {
        return this;
    }

    @DexIgnore
    /* renamed from: T */
    public final T mo13428T() {
        if (!this.f7341x) {
            mo13427S();
            return this;
        }
        throw new java.lang.IllegalStateException("You cannot modify locked T, consider clone()");
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13430a(float f) {
        if (this.f7343z) {
            return clone().mo13430a(f);
        }
        if (f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 1.0f) {
            throw new java.lang.IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.f7323f = f;
        this.f7322e |= 2;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public T mo13449b(boolean z) {
        if (this.f7343z) {
            return clone().mo13449b(z);
        }
        this.f7321D = z;
        this.f7322e |= 1048576;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: c */
    public final T mo13451c(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3) {
        return mo13435a(downsampleStrategy, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: d */
    public final T mo13453d(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3) {
        if (this.f7343z) {
            return clone().mo13453d(downsampleStrategy, r3);
        }
        mo13433a(downsampleStrategy);
        return mo13439a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3);
    }

    @DexIgnore
    /* renamed from: e */
    public final int mo13455e() {
        return this.f7327j;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2354lv)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2354lv lvVar = (com.fossil.blesdk.obfuscated.C2354lv) obj;
        if (java.lang.Float.compare(lvVar.f7323f, this.f7323f) == 0 && this.f7327j == lvVar.f7327j && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f7326i, (java.lang.Object) lvVar.f7326i) && this.f7329l == lvVar.f7329l && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f7328k, (java.lang.Object) lvVar.f7328k) && this.f7337t == lvVar.f7337t && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f7336s, (java.lang.Object) lvVar.f7336s) && this.f7330m == lvVar.f7330m && this.f7331n == lvVar.f7331n && this.f7332o == lvVar.f7332o && this.f7334q == lvVar.f7334q && this.f7335r == lvVar.f7335r && this.f7318A == lvVar.f7318A && this.f7319B == lvVar.f7319B && this.f7324g.equals(lvVar.f7324g) && this.f7325h == lvVar.f7325h && this.f7338u.equals(lvVar.f7338u) && this.f7339v.equals(lvVar.f7339v) && this.f7340w.equals(lvVar.f7340w) && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f7333p, (java.lang.Object) lvVar.f7333p) && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f7342y, (java.lang.Object) lvVar.f7342y)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public final android.graphics.drawable.Drawable mo13457f() {
        return this.f7326i;
    }

    @DexIgnore
    /* renamed from: g */
    public final android.graphics.drawable.Drawable mo13458g() {
        return this.f7336s;
    }

    @DexIgnore
    /* renamed from: h */
    public final int mo13459h() {
        return this.f7337t;
    }

    @DexIgnore
    public int hashCode() {
        return com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7342y, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7333p, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7340w, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7339v, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7338u, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7325h, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7324g, com.fossil.blesdk.obfuscated.C3066uw.m14924a(this.f7319B, com.fossil.blesdk.obfuscated.C3066uw.m14924a(this.f7318A, com.fossil.blesdk.obfuscated.C3066uw.m14924a(this.f7335r, com.fossil.blesdk.obfuscated.C3066uw.m14924a(this.f7334q, com.fossil.blesdk.obfuscated.C3066uw.m14919a(this.f7332o, com.fossil.blesdk.obfuscated.C3066uw.m14919a(this.f7331n, com.fossil.blesdk.obfuscated.C3066uw.m14924a(this.f7330m, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7336s, com.fossil.blesdk.obfuscated.C3066uw.m14919a(this.f7337t, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7328k, com.fossil.blesdk.obfuscated.C3066uw.m14919a(this.f7329l, com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f7326i, com.fossil.blesdk.obfuscated.C3066uw.m14919a(this.f7327j, com.fossil.blesdk.obfuscated.C3066uw.m14917a(this.f7323f)))))))))))))))))))));
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo13461i() {
        return this.f7319B;
    }

    @DexIgnore
    /* renamed from: j */
    public final com.fossil.blesdk.obfuscated.C2337lo mo13462j() {
        return this.f7338u;
    }

    @DexIgnore
    /* renamed from: k */
    public final int mo13463k() {
        return this.f7331n;
    }

    @DexIgnore
    /* renamed from: l */
    public final int mo13464l() {
        return this.f7332o;
    }

    @DexIgnore
    /* renamed from: m */
    public final android.graphics.drawable.Drawable mo13465m() {
        return this.f7328k;
    }

    @DexIgnore
    /* renamed from: w */
    public final int mo13466w() {
        return this.f7329l;
    }

    @DexIgnore
    /* renamed from: x */
    public final com.bumptech.glide.Priority mo13467x() {
        return this.f7325h;
    }

    @DexIgnore
    /* renamed from: y */
    public final java.lang.Class<?> mo13468y() {
        return this.f7340w;
    }

    @DexIgnore
    /* renamed from: z */
    public final com.fossil.blesdk.obfuscated.C2143jo mo13469z() {
        return this.f7333p;
    }

    @DexIgnore
    /* renamed from: c */
    public T mo13450c() {
        return mo13437a(com.fossil.blesdk.obfuscated.C1447au.f3606b, true);
    }

    @DexIgnore
    public T clone() {
        try {
            T t = (com.fossil.blesdk.obfuscated.C2354lv) super.clone();
            t.f7338u = new com.fossil.blesdk.obfuscated.C2337lo();
            t.f7338u.mo13320a(this.f7338u);
            t.f7339v = new com.fossil.blesdk.obfuscated.C2355lw();
            t.f7339v.putAll(this.f7339v);
            t.f7341x = false;
            t.f7343z = false;
            return t;
        } catch (java.lang.CloneNotSupportedException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.C2663pp mo13454d() {
        return this.f7324g;
    }

    @DexIgnore
    /* renamed from: b */
    public T mo13447b(int i) {
        if (this.f7343z) {
            return clone().mo13447b(i);
        }
        this.f7329l = i;
        this.f7322e |= 128;
        this.f7328k = null;
        this.f7322e &= -65;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13441a(com.fossil.blesdk.obfuscated.C2663pp ppVar) {
        if (this.f7343z) {
            return clone().mo13441a(ppVar);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(ppVar);
        this.f7324g = ppVar;
        this.f7322e |= 4;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13432a(com.bumptech.glide.Priority priority) {
        if (this.f7343z) {
            return clone().mo13432a(priority);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(priority);
        this.f7325h = priority;
        this.f7322e |= 8;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public T mo13446b() {
        return mo13451c(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2117b, new com.fossil.blesdk.obfuscated.C3059us());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: b */
    public final T mo13448b(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3) {
        if (this.f7343z) {
            return clone().mo13448b(downsampleStrategy, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3);
        }
        mo13433a(downsampleStrategy);
        return mo13440a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3, false);
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13444a(boolean z) {
        if (this.f7343z) {
            return clone().mo13444a(true);
        }
        this.f7330m = !z;
        this.f7322e |= 256;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13431a(int i, int i2) {
        if (this.f7343z) {
            return clone().mo13431a(i, i2);
        }
        this.f7332o = i;
        this.f7331n = i2;
        this.f7322e |= androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13436a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        if (this.f7343z) {
            return clone().mo13436a(joVar);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(joVar);
        this.f7333p = joVar;
        this.f7322e |= 1024;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.ko<Y>, java.lang.Object, com.fossil.blesdk.obfuscated.ko] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public <Y> T mo13437a(com.fossil.blesdk.obfuscated.C2232ko<Y> r2, Y y) {
        if (this.f7343z) {
            return clone().mo13437a(r2, y);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(r2);
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(y);
        this.f7338u.mo13318a(r2, y);
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13442a(java.lang.Class<?> cls) {
        if (this.f7343z) {
            return clone().mo13442a(cls);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(cls);
        this.f7340w = cls;
        this.f7322e |= 4096;
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13433a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy) {
        com.fossil.blesdk.obfuscated.C2232ko koVar = com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2121f;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(downsampleStrategy);
        return mo13437a(koVar, downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public final T mo13434a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3) {
        return mo13435a(downsampleStrategy, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3, false);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public final T mo13435a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy, com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r2, boolean z) {
        T t;
        if (z) {
            t = mo13453d(downsampleStrategy, r2);
        } else {
            t = mo13448b(downsampleStrategy, (com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r2);
        }
        t.f7320C = true;
        return t;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public T mo13439a(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r2) {
        return mo13440a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r2, true);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    public T mo13440a(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> r3, boolean z) {
        if (this.f7343z) {
            return clone().mo13440a((com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap>) r3, z);
        }
        com.fossil.blesdk.obfuscated.C3382ys ysVar = new com.fossil.blesdk.obfuscated.C3382ys(r3, z);
        mo13443a(android.graphics.Bitmap.class, r3, z);
        mo13443a(android.graphics.drawable.Drawable.class, ysVar, z);
        ysVar.mo18297a();
        mo13443a(android.graphics.drawable.BitmapDrawable.class, ysVar, z);
        mo13443a(com.fossil.blesdk.obfuscated.C3060ut.class, new com.fossil.blesdk.obfuscated.C3313xt(r3), z);
        mo13428T();
        return this;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.blesdk.obfuscated.oo<Y>, java.lang.Object, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: a */
    public <Y> T mo13443a(java.lang.Class<Y> r2, com.fossil.blesdk.obfuscated.C2581oo<Y> r3, boolean z) {
        if (this.f7343z) {
            return clone().mo13443a(r2, r3, z);
        }
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(r2);
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(r3);
        this.f7339v.put(r2, r3);
        this.f7322e |= 2048;
        this.f7335r = true;
        this.f7322e |= 65536;
        this.f7320C = false;
        if (z) {
            this.f7322e |= 131072;
            this.f7334q = true;
        }
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13438a(com.fossil.blesdk.obfuscated.C2354lv<?> lvVar) {
        if (this.f7343z) {
            return clone().mo13438a(lvVar);
        }
        if (m10342b(lvVar.f7322e, 2)) {
            this.f7323f = lvVar.f7323f;
        }
        if (m10342b(lvVar.f7322e, 262144)) {
            this.f7318A = lvVar.f7318A;
        }
        if (m10342b(lvVar.f7322e, 1048576)) {
            this.f7321D = lvVar.f7321D;
        }
        if (m10342b(lvVar.f7322e, 4)) {
            this.f7324g = lvVar.f7324g;
        }
        if (m10342b(lvVar.f7322e, 8)) {
            this.f7325h = lvVar.f7325h;
        }
        if (m10342b(lvVar.f7322e, 16)) {
            this.f7326i = lvVar.f7326i;
            this.f7327j = 0;
            this.f7322e &= -33;
        }
        if (m10342b(lvVar.f7322e, 32)) {
            this.f7327j = lvVar.f7327j;
            this.f7326i = null;
            this.f7322e &= -17;
        }
        if (m10342b(lvVar.f7322e, 64)) {
            this.f7328k = lvVar.f7328k;
            this.f7329l = 0;
            this.f7322e &= -129;
        }
        if (m10342b(lvVar.f7322e, 128)) {
            this.f7329l = lvVar.f7329l;
            this.f7328k = null;
            this.f7322e &= -65;
        }
        if (m10342b(lvVar.f7322e, 256)) {
            this.f7330m = lvVar.f7330m;
        }
        if (m10342b(lvVar.f7322e, (int) androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN)) {
            this.f7332o = lvVar.f7332o;
            this.f7331n = lvVar.f7331n;
        }
        if (m10342b(lvVar.f7322e, 1024)) {
            this.f7333p = lvVar.f7333p;
        }
        if (m10342b(lvVar.f7322e, 4096)) {
            this.f7340w = lvVar.f7340w;
        }
        if (m10342b(lvVar.f7322e, 8192)) {
            this.f7336s = lvVar.f7336s;
            this.f7337t = 0;
            this.f7322e &= -16385;
        }
        if (m10342b(lvVar.f7322e, (int) androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE)) {
            this.f7337t = lvVar.f7337t;
            this.f7336s = null;
            this.f7322e &= -8193;
        }
        if (m10342b(lvVar.f7322e, 32768)) {
            this.f7342y = lvVar.f7342y;
        }
        if (m10342b(lvVar.f7322e, 65536)) {
            this.f7335r = lvVar.f7335r;
        }
        if (m10342b(lvVar.f7322e, 131072)) {
            this.f7334q = lvVar.f7334q;
        }
        if (m10342b(lvVar.f7322e, 2048)) {
            this.f7339v.putAll(lvVar.f7339v);
            this.f7320C = lvVar.f7320C;
        }
        if (m10342b(lvVar.f7322e, 524288)) {
            this.f7319B = lvVar.f7319B;
        }
        if (!this.f7335r) {
            this.f7339v.clear();
            this.f7322e &= -2049;
            this.f7334q = false;
            this.f7322e &= -131073;
            this.f7320C = true;
        }
        this.f7322e |= lvVar.f7322e;
        this.f7338u.mo13320a(lvVar.f7338u);
        mo13428T();
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo13429a() {
        if (!this.f7341x || this.f7343z) {
            this.f7343z = true;
            return mo13423O();
        }
        throw new java.lang.IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo13445a(int i) {
        return m10342b(this.f7322e, i);
    }
}
