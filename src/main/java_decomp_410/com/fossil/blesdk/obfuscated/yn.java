package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.yn;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yn<CHILD extends yn<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    @DexIgnore
    public fw<? super TranscodeType> e; // = dw.a();

    @DexIgnore
    public final fw<? super TranscodeType> a() {
        return this.e;
    }

    @DexIgnore
    public final CHILD clone() {
        try {
            return (yn) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }
}
