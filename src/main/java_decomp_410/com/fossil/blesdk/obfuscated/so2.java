package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class so2 {
    @DexIgnore
    public List<MicroAppSetting> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public void a(xz1 xz1) {
        this.a = new ArrayList();
        if (xz1.d(CloudLogWriter.ITEMS_PARAM)) {
            try {
                tz1 b2 = xz1.b(CloudLogWriter.ITEMS_PARAM);
                if (b2.size() > 0) {
                    for (int i = 0; i < b2.size(); i++) {
                        xz1 d = b2.get(i).d();
                        MicroAppSetting microAppSetting = new MicroAppSetting();
                        if (d.d("appId")) {
                            microAppSetting.setMicroAppId(d.a("appId").f());
                        }
                        if (d.d(MicroAppSetting.LIKE)) {
                            microAppSetting.setLike(d.a(MicroAppSetting.LIKE).a());
                        }
                        if (d.d(MicroAppSetting.SETTING)) {
                            if (d.c(MicroAppSetting.SETTING).size() == 0) {
                                microAppSetting.setSetting("");
                            } else {
                                microAppSetting.setSetting(d.c(MicroAppSetting.SETTING).toString());
                            }
                        }
                        this.a.add(microAppSetting);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (xz1.d("_range")) {
            try {
                this.b = (Range) new Gson().a(xz1.c("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public List<MicroAppSetting> a() {
        return this.a;
    }
}
