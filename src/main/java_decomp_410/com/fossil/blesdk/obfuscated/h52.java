package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h52 implements Factory<f42> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public h52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static h52 a(n42 n42) {
        return new h52(n42);
    }

    @DexIgnore
    public static f42 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static f42 c(n42 n42) {
        f42 k = n42.k();
        n44.a(k, "Cannot return null from a non-@Nullable @Provides method");
        return k;
    }

    @DexIgnore
    public f42 get() {
        return b(this.a);
    }
}
