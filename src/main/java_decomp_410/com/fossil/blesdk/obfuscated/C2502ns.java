package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ns */
public class C2502ns implements com.fossil.blesdk.obfuscated.C2498no<android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Integer> f7863b; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<android.graphics.Bitmap.CompressFormat> f7864c; // = com.fossil.blesdk.obfuscated.C2232ko.m9711a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f7865a;

    @DexIgnore
    public C2502ns(com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f7865a = gqVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|(2:38|39)|40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r6 == null) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00bf */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bc A[SYNTHETIC, Splitter:B:38:0x00bc] */
    /* renamed from: a */
    public boolean mo11698a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.graphics.Bitmap bitmap = aqVar.get();
        android.graphics.Bitmap.CompressFormat a = mo14090a(bitmap, loVar);
        com.fossil.blesdk.obfuscated.C3246ww.m16049a("encode: [%dx%d] %s", java.lang.Integer.valueOf(bitmap.getWidth()), java.lang.Integer.valueOf(bitmap.getHeight()), a);
        try {
            long a2 = com.fossil.blesdk.obfuscated.C2682pw.m12452a();
            int intValue = ((java.lang.Integer) loVar.mo13319a(f7863b)).intValue();
            boolean z = false;
            com.fossil.blesdk.obfuscated.C2818ro roVar = null;
            try {
                java.io.FileOutputStream fileOutputStream = new java.io.FileOutputStream(file);
                try {
                    roVar = this.f7865a != null ? new com.fossil.blesdk.obfuscated.C2818ro(fileOutputStream, this.f7865a) : fileOutputStream;
                    bitmap.compress(a, intValue, roVar);
                    roVar.close();
                    z = true;
                } catch (java.io.IOException e) {
                    e = e;
                    roVar = fileOutputStream;
                    try {
                        if (android.util.Log.isLoggable("BitmapEncoder", 3)) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (roVar != null) {
                            roVar.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    roVar = fileOutputStream;
                    if (roVar != null) {
                    }
                    throw th;
                }
            } catch (java.io.IOException e2) {
                e = e2;
                if (android.util.Log.isLoggable("BitmapEncoder", 3)) {
                    android.util.Log.d("BitmapEncoder", "Failed to encode Bitmap", e);
                }
            }
            try {
                roVar.close();
            } catch (java.io.IOException unused) {
            }
            if (android.util.Log.isLoggable("BitmapEncoder", 2)) {
                android.util.Log.v("BitmapEncoder", "Compressed with type: " + a + " of size " + com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap) + " in " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a2) + ", options format: " + loVar.mo13319a(f7864c) + ", hasAlpha: " + bitmap.hasAlpha());
            }
            return z;
        } finally {
            com.fossil.blesdk.obfuscated.C3246ww.m16046a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.Bitmap.CompressFormat mo14090a(android.graphics.Bitmap bitmap, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.graphics.Bitmap.CompressFormat compressFormat = (android.graphics.Bitmap.CompressFormat) loVar.mo13319a(f7864c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return android.graphics.Bitmap.CompressFormat.PNG;
        }
        return android.graphics.Bitmap.CompressFormat.JPEG;
    }

    @DexIgnore
    /* renamed from: a */
    public com.bumptech.glide.load.EncodeStrategy mo13706a(com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return com.bumptech.glide.load.EncodeStrategy.TRANSFORMED;
    }
}
