package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q03 implements Factory<l03> {
    @DexIgnore
    public static l03 a(n03 n03) {
        l03 d = n03.d();
        n44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
