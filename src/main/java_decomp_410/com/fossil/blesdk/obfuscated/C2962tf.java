package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tf */
public class C2962tf extends com.fossil.blesdk.obfuscated.C1945hg.C1946a {

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2124jf f9651b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2962tf.C2963a f9652c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.String f9653d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f9654e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.tf$a */
    public static abstract class C2963a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public C2963a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void dropAllTables(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void onCreate(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void onOpen(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void onPostMigrate(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void onPreMigrate(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        public abstract void validateMigration(com.fossil.blesdk.obfuscated.C1874gg ggVar);
    }

    @DexIgnore
    public C2962tf(com.fossil.blesdk.obfuscated.C2124jf jfVar, com.fossil.blesdk.obfuscated.C2962tf.C2963a aVar, java.lang.String str, java.lang.String str2) {
        super(aVar.version);
        this.f9651b = jfVar;
        this.f9652c = aVar;
        this.f9653d = str;
        this.f9654e = str2;
    }

    @DexIgnore
    /* renamed from: h */
    public static boolean m14184h(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        android.database.Cursor d = ggVar.mo11232d("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (d.moveToFirst() && d.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            d.close();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11638a(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        super.mo11638a(ggVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: b */
    public void mo11642b(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i, int i2) {
        boolean z;
        com.fossil.blesdk.obfuscated.C2124jf jfVar = this.f9651b;
        if (jfVar != null) {
            java.util.List<com.fossil.blesdk.obfuscated.C3361yf> a = jfVar.f6413d.mo3296a(i, i2);
            if (a != null) {
                this.f9652c.onPreMigrate(ggVar);
                for (com.fossil.blesdk.obfuscated.C3361yf migrate : a) {
                    migrate.migrate(ggVar);
                }
                this.f9652c.validateMigration(ggVar);
                this.f9652c.onPostMigrate(ggVar);
                mo16389g(ggVar);
                z = true;
                if (z) {
                    com.fossil.blesdk.obfuscated.C2124jf jfVar2 = this.f9651b;
                    if (jfVar2 == null || jfVar2.mo12340a(i, i2)) {
                        throw new java.lang.IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
                    }
                    this.f9652c.dropAllTables(ggVar);
                    this.f9652c.createAllTables(ggVar);
                    return;
                }
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11643c(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        mo16389g(ggVar);
        this.f9652c.createAllTables(ggVar);
        this.f9652c.onCreate(ggVar);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11644d(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        super.mo11644d(ggVar);
        mo16387e(ggVar);
        this.f9652c.onOpen(ggVar);
        this.f9651b = null;
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo16387e(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        java.lang.String str = null;
        if (m14184h(ggVar)) {
            android.database.Cursor a = ggVar.mo11229a(new com.fossil.blesdk.obfuscated.C1801fg("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a.moveToFirst()) {
                    str = a.getString(0);
                }
            } finally {
                a.close();
            }
        }
        if (!this.f9653d.equals(str) && !this.f9654e.equals(str)) {
            throw new java.lang.IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo16388f(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        ggVar.mo11230b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo16389g(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        mo16388f(ggVar);
        ggVar.mo11230b(com.fossil.blesdk.obfuscated.C2875sf.m13669a(this.f9653d));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11639a(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i, int i2) {
        mo11642b(ggVar, i, i2);
    }
}
