package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f51 extends a51 implements d51 {
    @DexIgnore
    public f51(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    public final sn0 zza(Bitmap bitmap) throws RemoteException {
        Parcel o = o();
        c51.a(o, (Parcelable) bitmap);
        Parcel a = a(6, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
