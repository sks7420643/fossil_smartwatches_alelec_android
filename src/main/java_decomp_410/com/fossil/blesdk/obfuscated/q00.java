package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.FeatureErrorCode;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.PhaseId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class q00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Peripheral.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[Peripheral.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[Device.State.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[FeatureErrorCode.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e; // = new int[PhaseId.values().length];

    /*
    static {
        a[Peripheral.State.CONNECTED.ordinal()] = 1;
        a[Peripheral.State.DISCONNECTED.ordinal()] = 2;
        a[Peripheral.State.CONNECTING.ordinal()] = 3;
        a[Peripheral.State.DISCONNECTING.ordinal()] = 4;
        b[Peripheral.State.DISCONNECTED.ordinal()] = 1;
        b[Peripheral.State.CONNECTING.ordinal()] = 2;
        b[Peripheral.State.CONNECTED.ordinal()] = 3;
        b[Peripheral.State.DISCONNECTING.ordinal()] = 4;
        c[Device.State.DISCONNECTED.ordinal()] = 1;
        c[Device.State.CONNECTING.ordinal()] = 2;
        c[Device.State.CONNECTED.ordinal()] = 3;
        c[Device.State.UPGRADING_FIRMWARE.ordinal()] = 4;
        c[Device.State.DISCONNECTING.ordinal()] = 5;
        d[FeatureErrorCode.BLUETOOTH_OFF.ordinal()] = 1;
        e[PhaseId.READ_RSSI.ordinal()] = 1;
        e[PhaseId.STREAMING.ordinal()] = 2;
        e[PhaseId.DISCONNECT_HID.ordinal()] = 3;
        e[PhaseId.CONNECT_HID.ordinal()] = 4;
        e[PhaseId.OTA.ordinal()] = 5;
    }
    */
}
