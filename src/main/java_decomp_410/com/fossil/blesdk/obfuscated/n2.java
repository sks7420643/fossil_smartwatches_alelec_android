package com.fossil.blesdk.obfuscated;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class n2 implements View.OnTouchListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public /* final */ float e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ View h;
    @DexIgnore
    public Runnable i;
    @DexIgnore
    public Runnable j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l;
    @DexIgnore
    public /* final */ int[] m; // = new int[2];

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            ViewParent parent = n2.this.h.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            n2.this.e();
        }
    }

    @DexIgnore
    public n2(View view) {
        this.h = view;
        view.setLongClickable(true);
        view.addOnAttachStateChangeListener(this);
        this.e = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f = ViewConfiguration.getTapTimeout();
        this.g = (this.f + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    @DexIgnore
    public final void a() {
        Runnable runnable = this.j;
        if (runnable != null) {
            this.h.removeCallbacks(runnable);
        }
        Runnable runnable2 = this.i;
        if (runnable2 != null) {
            this.h.removeCallbacks(runnable2);
        }
    }

    @DexIgnore
    public abstract t1 b();

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        if (r1 != 3) goto L_0x006d;
     */
    @DexIgnore
    public final boolean b(MotionEvent motionEvent) {
        View view = this.h;
        if (!view.isEnabled()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int findPointerIndex = motionEvent.findPointerIndex(this.l);
                    if (findPointerIndex >= 0 && !a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.e)) {
                        a();
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        return true;
                    }
                }
            }
            a();
        } else {
            this.l = motionEvent.getPointerId(0);
            if (this.i == null) {
                this.i = new a();
            }
            view.postDelayed(this.i, (long) this.f);
            if (this.j == null) {
                this.j = new b();
            }
            view.postDelayed(this.j, (long) this.g);
        }
        return false;
    }

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public boolean d() {
        t1 b2 = b();
        if (b2 == null || !b2.d()) {
            return true;
        }
        b2.dismiss();
        return true;
    }

    @DexIgnore
    public void e() {
        a();
        View view = this.h;
        if (view.isEnabled() && !view.isLongClickable() && c()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.k = true;
        }
    }

    @DexIgnore
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.k;
        if (z2) {
            z = a(motionEvent) || !d();
        } else {
            z = b(motionEvent) && c();
            if (z) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
                this.h.onTouchEvent(obtain);
                obtain.recycle();
            }
        }
        this.k = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        this.k = false;
        this.l = -1;
        Runnable runnable = this.i;
        if (runnable != null) {
            this.h.removeCallbacks(runnable);
        }
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        View view = this.h;
        t1 b2 = b();
        if (b2 != null && b2.d()) {
            l2 l2Var = (l2) b2.e();
            if (l2Var != null && l2Var.isShown()) {
                MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
                a(view, obtainNoHistory);
                b(l2Var, obtainNoHistory);
                boolean a2 = l2Var.a(obtainNoHistory, this.l);
                obtainNoHistory.recycle();
                int actionMasked = motionEvent.getActionMasked();
                return a2 && (actionMasked != 1 && actionMasked != 3);
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean a(View view, float f2, float f3, float f4) {
        float f5 = -f4;
        return f2 >= f5 && f3 >= f5 && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    @DexIgnore
    public final boolean a(View view, MotionEvent motionEvent) {
        int[] iArr = this.m;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    @DexIgnore
    public final boolean b(View view, MotionEvent motionEvent) {
        int[] iArr = this.m;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }
}
