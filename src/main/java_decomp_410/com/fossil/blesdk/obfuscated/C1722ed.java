package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ed */
public class C1722ed extends android.content.BroadcastReceiver {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ed$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ed$a */
    public static class C1723a extends android.support.p000v4.media.MediaBrowserCompat.C0002b {

        @DexIgnore
        /* renamed from: c */
        public /* final */ android.content.Context f4776c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ android.content.Intent f4777d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.content.BroadcastReceiver.PendingResult f4778e;

        @DexIgnore
        /* renamed from: f */
        public android.support.p000v4.media.MediaBrowserCompat f4779f;

        @DexIgnore
        public C1723a(android.content.Context context, android.content.Intent intent, android.content.BroadcastReceiver.PendingResult pendingResult) {
            this.f4776c = context;
            this.f4777d = intent;
            this.f4778e = pendingResult;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10456a(android.support.p000v4.media.MediaBrowserCompat mediaBrowserCompat) {
            this.f4779f = mediaBrowserCompat;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo15b() {
            mo10457d();
        }

        @DexIgnore
        /* renamed from: c */
        public void mo16c() {
            mo10457d();
        }

        @DexIgnore
        /* renamed from: d */
        public final void mo10457d() {
            this.f4779f.mo2b();
            this.f4778e.finish();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13a() {
            try {
                new android.support.p000v4.media.session.MediaControllerCompat(this.f4776c, this.f4779f.mo3c()).mo95a((android.view.KeyEvent) this.f4777d.getParcelableExtra("android.intent.extra.KEY_EVENT"));
            } catch (android.os.RemoteException e) {
                android.util.Log.e("MediaButtonReceiver", "Failed to create a media controller", e);
            }
            mo10457d();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6423a(android.content.Context context, android.content.Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @DexIgnore
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        if (intent == null || !"android.intent.action.MEDIA_BUTTON".equals(intent.getAction()) || !intent.hasExtra("android.intent.extra.KEY_EVENT")) {
            android.util.Log.d("MediaButtonReceiver", "Ignore unsupported intent: " + intent);
            return;
        }
        android.content.ComponentName a = m6422a(context, "android.intent.action.MEDIA_BUTTON");
        if (a != null) {
            intent.setComponent(a);
            m6423a(context, intent);
            return;
        }
        android.content.ComponentName a2 = m6422a(context, "android.media.browse.MediaBrowserService");
        if (a2 != null) {
            android.content.BroadcastReceiver.PendingResult goAsync = goAsync();
            android.content.Context applicationContext = context.getApplicationContext();
            com.fossil.blesdk.obfuscated.C1722ed.C1723a aVar = new com.fossil.blesdk.obfuscated.C1722ed.C1723a(applicationContext, intent, goAsync);
            android.support.p000v4.media.MediaBrowserCompat mediaBrowserCompat = new android.support.p000v4.media.MediaBrowserCompat(applicationContext, a2, aVar, (android.os.Bundle) null);
            aVar.mo10456a(mediaBrowserCompat);
            mediaBrowserCompat.mo1a();
            return;
        }
        throw new java.lang.IllegalStateException("Could not find any Service that handles android.intent.action.MEDIA_BUTTON or implements a media browser service.");
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.ComponentName m6422a(android.content.Context context, java.lang.String str) {
        android.content.pm.PackageManager packageManager = context.getPackageManager();
        android.content.Intent intent = new android.content.Intent(str);
        intent.setPackage(context.getPackageName());
        java.util.List<android.content.pm.ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices.size() == 1) {
            android.content.pm.ResolveInfo resolveInfo = queryIntentServices.get(0);
            return new android.content.ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
        } else if (queryIntentServices.isEmpty()) {
            return null;
        } else {
            throw new java.lang.IllegalStateException("Expected 1 service that handles " + str + ", found " + queryIntentServices.size());
        }
    }
}
