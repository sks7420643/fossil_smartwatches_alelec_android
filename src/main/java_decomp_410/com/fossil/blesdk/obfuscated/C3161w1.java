package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w1 */
public class C3161w1 extends com.fossil.blesdk.obfuscated.C2851s1 implements android.view.SubMenu {
    @DexIgnore
    public C3161w1(android.content.Context context, com.fossil.blesdk.obfuscated.C2108j7 j7Var) {
        super(context, j7Var);
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2108j7 mo17263c() {
        return (com.fossil.blesdk.obfuscated.C2108j7) this.f4235a;
    }

    @DexIgnore
    public void clearHeader() {
        mo17263c().clearHeader();
    }

    @DexIgnore
    public android.view.MenuItem getItem() {
        return mo9376a(mo17263c().getItem());
    }

    @DexIgnore
    public android.view.SubMenu setHeaderIcon(int i) {
        mo17263c().setHeaderIcon(i);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderTitle(int i) {
        mo17263c().setHeaderTitle(i);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderView(android.view.View view) {
        mo17263c().setHeaderView(view);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setIcon(int i) {
        mo17263c().setIcon(i);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderIcon(android.graphics.drawable.Drawable drawable) {
        mo17263c().setHeaderIcon(drawable);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setHeaderTitle(java.lang.CharSequence charSequence) {
        mo17263c().setHeaderTitle(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.SubMenu setIcon(android.graphics.drawable.Drawable drawable) {
        mo17263c().setIcon(drawable);
        return this;
    }
}
