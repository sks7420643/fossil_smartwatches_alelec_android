package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.pd;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class md<Key, Value> extends jd<Key, Value> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<Value> {
        @DexIgnore
        public abstract void a(List<Value> list);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Value> extends a<Value> {
        @DexIgnore
        public /* final */ ld.d<Value> a;

        @DexIgnore
        public b(md mdVar, int i, Executor executor, pd.a<Value> aVar) {
            this.a = new ld.d<>(mdVar, i, executor, aVar);
        }

        @DexIgnore
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.a(new pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<Value> extends a<Value> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Value> extends c<Value> {
        @DexIgnore
        public /* final */ ld.d<Value> a;

        @DexIgnore
        public d(md mdVar, boolean z, pd.a<Value> aVar) {
            this.a = new ld.d<>(mdVar, 0, (Executor) null, aVar);
        }

        @DexIgnore
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.a(new pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Key> {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public e(Key key, int i, boolean z) {
            this.a = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<Key> {
        @DexIgnore
        public /* final */ Key a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(Key key, int i) {
            this.a = key;
            this.b = i;
        }
    }

    @DexIgnore
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, pd.a<Value> aVar) {
        loadAfter(new f(getKey(value), i2), new b(this, 1, executor, aVar));
    }

    @DexIgnore
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, pd.a<Value> aVar) {
        loadBefore(new f(getKey(value), i2), new b(this, 2, executor, aVar));
    }

    @DexIgnore
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, pd.a<Value> aVar) {
        d dVar = new d(this, z, aVar);
        loadInitial(new e(key, i, z), dVar);
        dVar.a.a(executor);
    }

    @DexIgnore
    public final Key getKey(int i, Value value) {
        if (value == null) {
            return null;
        }
        return getKey(value);
    }

    @DexIgnore
    public abstract Key getKey(Value value);

    @DexIgnore
    public abstract void loadAfter(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadBefore(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadInitial(e<Key> eVar, c<Value> cVar);

    @DexIgnore
    public final <ToValue> md<Key, ToValue> map(m3<Value, ToValue> m3Var) {
        return mapByPage((m3) ld.createListFunction(m3Var));
    }

    @DexIgnore
    public final <ToValue> md<Key, ToValue> mapByPage(m3<List<Value>, List<ToValue>> m3Var) {
        return new xd(this, m3Var);
    }
}
