package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ k04 f;

    @DexIgnore
    public y04(Context context, k04 k04) {
        this.e = context;
        this.f = k04;
    }

    @DexIgnore
    public final void run() {
        Context context = this.e;
        if (context == null) {
            j04.m.d("The Context of StatService.onResume() can not be null!");
        } else {
            j04.a(context, e24.k(context), this.f);
        }
    }
}
