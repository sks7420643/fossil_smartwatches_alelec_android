package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cj {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends cj {
        @DexIgnore
        public bj a(String str) {
            return null;
        }
    }

    @DexIgnore
    public static cj a() {
        return new a();
    }

    @DexIgnore
    public abstract bj a(String str);

    @DexIgnore
    public final bj b(String str) {
        bj a2 = a(str);
        return a2 == null ? bj.a(str) : a2;
    }
}
