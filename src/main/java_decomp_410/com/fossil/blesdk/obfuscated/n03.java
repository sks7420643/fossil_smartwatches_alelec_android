package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n03 {
    @DexIgnore
    public /* final */ l03 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<ContactWrapper> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public n03(l03 l03, int i, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager) {
        kd4.b(l03, "mView");
        kd4.b(loaderManager, "mLoaderManager");
        this.a = l03;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<ContactWrapper> a() {
        ArrayList<ContactWrapper> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final l03 d() {
        return this.a;
    }
}
