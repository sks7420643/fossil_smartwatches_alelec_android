package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yw2 {
    @DexIgnore
    public /* final */ ww2 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public yw2(ww2 ww2, LoaderManager loaderManager) {
        kd4.b(ww2, "mView");
        kd4.b(loaderManager, "mLoaderManager");
        this.a = ww2;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final ww2 b() {
        return this.a;
    }
}
