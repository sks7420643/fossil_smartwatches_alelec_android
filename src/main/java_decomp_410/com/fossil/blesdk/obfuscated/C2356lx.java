package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lx */
public class C2356lx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.ScheduledExecutorService f7352a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2356lx.C2358b> f7353b; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: c */
    public volatile boolean f7354c; // = true;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledFuture<?>> f7355d; // = new java.util.concurrent.atomic.AtomicReference<>();

    @DexIgnore
    /* renamed from: e */
    public boolean f7356e; // = true;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lx$a")
    /* renamed from: com.fossil.blesdk.obfuscated.lx$a */
    public class C2357a implements java.lang.Runnable {
        @DexIgnore
        public C2357a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2356lx.this.f7355d.set((java.lang.Object) null);
            com.fossil.blesdk.obfuscated.C2356lx.this.mo13475a();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.lx$b */
    public interface C2358b {
        @DexIgnore
        /* renamed from: a */
        void mo8974a();
    }

    @DexIgnore
    public C2356lx(java.util.concurrent.ScheduledExecutorService scheduledExecutorService) {
        this.f7352a = scheduledExecutorService;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13478b() {
        if (this.f7354c && !this.f7356e) {
            this.f7356e = true;
            try {
                this.f7355d.compareAndSet((java.lang.Object) null, this.f7352a.schedule(new com.fossil.blesdk.obfuscated.C2356lx.C2357a(), 5000, java.util.concurrent.TimeUnit.MILLISECONDS));
            } catch (java.util.concurrent.RejectedExecutionException e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30058b("Answers", "Failed to schedule background detector", e);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo13479c() {
        this.f7356e = false;
        java.util.concurrent.ScheduledFuture andSet = this.f7355d.getAndSet((java.lang.Object) null);
        if (andSet != null) {
            andSet.cancel(false);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13477a(boolean z) {
        this.f7354c = z;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13475a() {
        for (com.fossil.blesdk.obfuscated.C2356lx.C2358b a : this.f7353b) {
            a.mo8974a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13476a(com.fossil.blesdk.obfuscated.C2356lx.C2358b bVar) {
        this.f7353b.add(bVar);
    }
}
