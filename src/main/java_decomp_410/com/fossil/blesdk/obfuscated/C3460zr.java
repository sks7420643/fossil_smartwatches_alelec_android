package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zr */
public class C3460zr<Data> implements com.fossil.blesdk.obfuscated.C2912sr<java.lang.String, Data> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> f11646a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zr$a */
    public static final class C3461a implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.String, android.content.res.AssetFileDescriptor> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.String, android.content.res.AssetFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3460zr(wrVar.mo17509a(android.net.Uri.class, android.content.res.AssetFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.zr$b */
    public static class C3462b implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.String, android.os.ParcelFileDescriptor> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.String, android.os.ParcelFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3460zr(wrVar.mo17509a(android.net.Uri.class, android.os.ParcelFileDescriptor.class));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zr$c")
    /* renamed from: com.fossil.blesdk.obfuscated.zr$c */
    public static class C3463c implements com.fossil.blesdk.obfuscated.C2984tr<java.lang.String, java.io.InputStream> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.lang.String, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C3460zr(wrVar.mo17509a(android.net.Uri.class, java.io.InputStream.class));
        }
    }

    @DexIgnore
    public C3460zr(com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> srVar) {
        this.f11646a = srVar;
    }

    @DexIgnore
    /* renamed from: b */
    public static android.net.Uri m17529b(java.lang.String str) {
        if (android.text.TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return m17530c(str);
        }
        android.net.Uri parse = android.net.Uri.parse(str);
        return parse.getScheme() == null ? m17530c(str) : parse;
    }

    @DexIgnore
    /* renamed from: c */
    public static android.net.Uri m17530c(java.lang.String str) {
        return android.net.Uri.fromFile(new java.io.File(str));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(java.lang.String str) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(java.lang.String str, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.net.Uri b = m17529b(str);
        if (b == null || !this.f11646a.mo8912a(b)) {
            return null;
        }
        return this.f11646a.mo8911a(b, i, i2, loVar);
    }
}
