package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bn1 extends ls0 implements an1 {
    @DexIgnore
    public bn1() {
        super("com.google.android.gms.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 3) {
            a((ud0) ms0.a(parcel, ud0.CREATOR), (xm1) ms0.a(parcel, xm1.CREATOR));
        } else if (i == 4) {
            d((Status) ms0.a(parcel, Status.CREATOR));
        } else if (i == 6) {
            e((Status) ms0.a(parcel, Status.CREATOR));
        } else if (i == 7) {
            a((Status) ms0.a(parcel, Status.CREATOR), (GoogleSignInAccount) ms0.a(parcel, GoogleSignInAccount.CREATOR));
        } else if (i != 8) {
            return false;
        } else {
            a((gn1) ms0.a(parcel, gn1.CREATOR));
        }
        parcel2.writeNoException();
        return true;
    }
}
