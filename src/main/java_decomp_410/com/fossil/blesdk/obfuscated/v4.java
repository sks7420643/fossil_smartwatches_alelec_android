package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v4 extends b5 {
    @DexIgnore
    public int m0; // = 0;
    @DexIgnore
    public ArrayList<e5> n0; // = new ArrayList<>(4);
    @DexIgnore
    public boolean o0; // = true;

    @DexIgnore
    public void G() {
        super.G();
        this.n0.clear();
    }

    @DexIgnore
    public void H() {
        e5 e5Var;
        float f;
        e5 e5Var2;
        int i = this.m0;
        float f2 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                e5Var = this.u.d();
            } else if (i == 2) {
                e5Var = this.t.d();
            } else if (i == 3) {
                e5Var = this.v.d();
            } else {
                return;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            e5Var = this.s.d();
        }
        int size = this.n0.size();
        e5 e5Var3 = null;
        int i2 = 0;
        while (i2 < size) {
            e5 e5Var4 = this.n0.get(i2);
            if (e5Var4.b == 1) {
                int i3 = this.m0;
                if (i3 == 0 || i3 == 2) {
                    f = e5Var4.g;
                    if (f < f2) {
                        e5Var2 = e5Var4.f;
                    } else {
                        i2++;
                    }
                } else {
                    f = e5Var4.g;
                    if (f > f2) {
                        e5Var2 = e5Var4.f;
                    } else {
                        i2++;
                    }
                }
                e5Var3 = e5Var2;
                f2 = f;
                i2++;
            } else {
                return;
            }
        }
        if (q4.j() != null) {
            q4.j().y++;
        }
        e5Var.f = e5Var3;
        e5Var.g = f2;
        e5Var.a();
        int i4 = this.m0;
        if (i4 == 0) {
            this.u.d().a(e5Var3, f2);
        } else if (i4 == 1) {
            this.s.d().a(e5Var3, f2);
        } else if (i4 == 2) {
            this.v.d().a(e5Var3, f2);
        } else if (i4 == 3) {
            this.t.d().a(e5Var3, f2);
        }
    }

    @DexIgnore
    public boolean L() {
        return this.o0;
    }

    @DexIgnore
    public void a(int i) {
        e5 e5Var;
        e5 e5Var2;
        ConstraintWidget constraintWidget = this.D;
        if (constraintWidget != null && ((y4) constraintWidget).u(2)) {
            int i2 = this.m0;
            if (i2 == 0) {
                e5Var = this.s.d();
            } else if (i2 == 1) {
                e5Var = this.u.d();
            } else if (i2 == 2) {
                e5Var = this.t.d();
            } else if (i2 == 3) {
                e5Var = this.v.d();
            } else {
                return;
            }
            e5Var.b(5);
            int i3 = this.m0;
            if (i3 == 0 || i3 == 1) {
                this.t.d().a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.v.d().a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                this.s.d().a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.u.d().a((e5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.n0.clear();
            for (int i4 = 0; i4 < this.l0; i4++) {
                ConstraintWidget constraintWidget2 = this.k0[i4];
                if (this.o0 || constraintWidget2.b()) {
                    int i5 = this.m0;
                    if (i5 == 0) {
                        e5Var2 = constraintWidget2.s.d();
                    } else if (i5 == 1) {
                        e5Var2 = constraintWidget2.u.d();
                    } else if (i5 == 2) {
                        e5Var2 = constraintWidget2.t.d();
                    } else if (i5 != 3) {
                        e5Var2 = null;
                    } else {
                        e5Var2 = constraintWidget2.v.d();
                    }
                    if (e5Var2 != null) {
                        this.n0.add(e5Var2);
                        e5Var2.a(e5Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public void c(boolean z) {
        this.o0 = z;
    }

    @DexIgnore
    public void u(int i) {
        this.m0 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r1 = true;
     */
    @DexIgnore
    public void a(q4 q4Var) {
        ConstraintAnchor[] constraintAnchorArr;
        boolean z;
        ConstraintAnchor[] constraintAnchorArr2 = this.A;
        constraintAnchorArr2[0] = this.s;
        constraintAnchorArr2[2] = this.t;
        constraintAnchorArr2[1] = this.u;
        constraintAnchorArr2[3] = this.v;
        int i = 0;
        while (true) {
            constraintAnchorArr = this.A;
            if (i >= constraintAnchorArr.length) {
                break;
            }
            constraintAnchorArr[i].i = q4Var.a((Object) constraintAnchorArr[i]);
            i++;
        }
        int i2 = this.m0;
        if (i2 >= 0 && i2 < 4) {
            ConstraintAnchor constraintAnchor = constraintAnchorArr[i2];
            int i3 = 0;
            while (true) {
                if (i3 >= this.l0) {
                    z = false;
                    break;
                }
                ConstraintWidget constraintWidget = this.k0[i3];
                if (this.o0 || constraintWidget.b()) {
                    int i4 = this.m0;
                    if ((i4 != 0 && i4 != 1) || constraintWidget.k() != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        int i5 = this.m0;
                        if ((i5 == 2 || i5 == 3) && constraintWidget.r() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i3++;
            }
            int i6 = this.m0;
            if (i6 == 0 || i6 == 1 ? l().k() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT : l().r() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < this.l0; i7++) {
                ConstraintWidget constraintWidget2 = this.k0[i7];
                if (this.o0 || constraintWidget2.b()) {
                    SolverVariable a = q4Var.a((Object) constraintWidget2.A[this.m0]);
                    ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.A;
                    int i8 = this.m0;
                    constraintAnchorArr3[i8].i = a;
                    if (i8 == 0 || i8 == 2) {
                        q4Var.b(constraintAnchor.i, a, z);
                    } else {
                        q4Var.a(constraintAnchor.i, a, z);
                    }
                }
            }
            int i9 = this.m0;
            if (i9 == 0) {
                q4Var.a(this.u.i, this.s.i, 0, 6);
                if (!z) {
                    q4Var.a(this.s.i, this.D.u.i, 0, 5);
                }
            } else if (i9 == 1) {
                q4Var.a(this.s.i, this.u.i, 0, 6);
                if (!z) {
                    q4Var.a(this.s.i, this.D.s.i, 0, 5);
                }
            } else if (i9 == 2) {
                q4Var.a(this.v.i, this.t.i, 0, 6);
                if (!z) {
                    q4Var.a(this.t.i, this.D.v.i, 0, 5);
                }
            } else if (i9 == 3) {
                q4Var.a(this.t.i, this.v.i, 0, 6);
                if (!z) {
                    q4Var.a(this.t.i, this.D.t.i, 0, 5);
                }
            }
        }
    }
}
