package com.fossil.blesdk.obfuscated;

import kotlinx.coroutines.scheduling.TaskMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sk4 implements uk4 {
    @DexIgnore
    public static /* final */ TaskMode e; // = TaskMode.NON_BLOCKING;
    @DexIgnore
    public static /* final */ sk4 f; // = new sk4();

    @DexIgnore
    public void A() {
    }

    @DexIgnore
    public TaskMode B() {
        return e;
    }
}
