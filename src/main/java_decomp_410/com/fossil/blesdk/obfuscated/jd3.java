package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jd3 implements MembersInjector<SleepOverviewFragment> {
    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewDayPresenter sleepOverviewDayPresenter) {
        sleepOverviewFragment.k = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
        sleepOverviewFragment.l = sleepOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        sleepOverviewFragment.m = sleepOverviewMonthPresenter;
    }
}
