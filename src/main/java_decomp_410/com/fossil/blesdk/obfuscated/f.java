package com.fossil.blesdk.obfuscated;

import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import com.fossil.blesdk.obfuscated.e;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f {

    @DexIgnore
    public interface a extends e.d {
        @DexIgnore
        void a(String str, Bundle bundle);

        @DexIgnore
        void a(String str, List<?> list, Bundle bundle);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends e.C0013e<T> {
        @DexIgnore
        public b(T t) {
            super(t);
        }

        @DexIgnore
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) this.a).a(str, list, bundle);
        }

        @DexIgnore
        public void onError(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) this.a).a(str, bundle);
        }
    }

    @DexIgnore
    public static Object a(a aVar) {
        return new b(aVar);
    }
}
