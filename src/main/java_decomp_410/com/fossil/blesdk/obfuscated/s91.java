package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzvp;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s91 implements r91 {
    @DexIgnore
    public final Object a(Object obj) {
        ((zzvp) obj).zzsw();
        return obj;
    }

    @DexIgnore
    public final Object b(Object obj) {
        return zzvp.zzxg().zzxh();
    }

    @DexIgnore
    public final q91<?, ?> c(Object obj) {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Map<?, ?> d(Object obj) {
        return (zzvp) obj;
    }

    @DexIgnore
    public final Map<?, ?> e(Object obj) {
        return (zzvp) obj;
    }

    @DexIgnore
    public final boolean f(Object obj) {
        return !((zzvp) obj).isMutable();
    }

    @DexIgnore
    public final int a(int i, Object obj, Object obj2) {
        zzvp zzvp = (zzvp) obj;
        if (zzvp.isEmpty()) {
            return 0;
        }
        Iterator it = zzvp.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Object b(Object obj, Object obj2) {
        zzvp zzvp = (zzvp) obj;
        zzvp zzvp2 = (zzvp) obj2;
        if (!zzvp2.isEmpty()) {
            if (!zzvp.isMutable()) {
                zzvp = zzvp.zzxh();
            }
            zzvp.zza(zzvp2);
        }
        return zzvp;
    }
}
