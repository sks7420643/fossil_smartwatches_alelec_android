package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vy2 extends CoroutineUseCase<CoroutineUseCase.b, a, CoroutineUseCase.a> {
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Context e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public a(List<AppWrapper> list) {
            kd4.b(list, "apps");
            st1.a(list, "apps cannot be null!", new Object[0]);
            kd4.a((Object) list, "checkNotNull(apps, \"apps cannot be null!\")");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    @DexIgnore
    public vy2(Context context) {
        kd4.b(context, "context");
        String simpleName = vy2.class.getSimpleName();
        kd4.a((Object) simpleName, "GetApps::class.java.simpleName");
        this.d = simpleName;
        st1.a(context);
        kd4.a((Object) context, "checkNotNull(context)");
        this.e = context;
    }

    @DexIgnore
    public Object a(CoroutineUseCase.b bVar, yb4<Object> yb4) {
        FLogger.INSTANCE.getLocal().d(this.d, "executeUseCase GetApps");
        List<AppFilter> allAppFilters = dn2.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<AppHelper.a> it = AppHelper.f.b(this.e).iterator();
        while (it.hasNext()) {
            AppHelper.a next = it.next();
            if (TextUtils.isEmpty(next.b()) || !qf4.b(next.b(), this.e.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), dc4.a(false));
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    kd4.a((Object) next2, "appFilter");
                    if (kd4.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        gb4.c(linkedList);
        return new a(linkedList);
    }

    @DexIgnore
    public String c() {
        return this.d;
    }
}
