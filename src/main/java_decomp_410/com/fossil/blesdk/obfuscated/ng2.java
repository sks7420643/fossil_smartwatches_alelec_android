package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ng2 extends mg2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.back_iv, 1);
        A.put(R.id.ftv_title, 2);
        A.put(R.id.autocomplete_places, 3);
        A.put(R.id.clear_iv, 4);
        A.put(R.id.line, 5);
        A.put(R.id.ftv_location_error, 6);
        A.put(R.id.tv_added, 7);
        A.put(R.id.recycler_view, 8);
    }
    */

    @DexIgnore
    public ng2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 9, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ng2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[3], objArr[1], objArr[4], objArr[6], objArr[2], objArr[5], objArr[8], objArr[7]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
