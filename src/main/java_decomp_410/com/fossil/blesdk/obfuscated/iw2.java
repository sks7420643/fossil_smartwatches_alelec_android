package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iw2 extends i62<b, c, i62.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.b {
        @DexIgnore
        public /* final */ List<AppWrapper> a;

        @DexIgnore
        public b(List<AppWrapper> list) {
            kd4.b(list, "appWrapperList");
            this.a = list;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = iw2.class.getSimpleName();
        kd4.a((Object) simpleName, "SaveAppsNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public iw2(NotificationsRepository notificationsRepository) {
        kd4.b(notificationsRepository, "notificationsRepository");
        st1.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        kd4.a((Object) notificationsRepository, "checkNotNull(notificatio\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(next.getCurrentHandGroup());
                InstalledApp installedApp = next.getInstalledApp();
                String str = null;
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved: App name = ");
                InstalledApp installedApp2 = next.getInstalledApp();
                if (installedApp2 != null) {
                    str = installedApp2.getIdentifier();
                }
                sb.append(str);
                sb.append(", hour = ");
                sb.append(next.getCurrentHandGroup());
                local.d(str2, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        kd4.b(bVar, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (AppWrapper next : bVar.a()) {
            InstalledApp installedApp = next.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                kd4.a();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        a((List<AppWrapper>) arrayList2);
        b(arrayList);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAppsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = next.getInstalledApp();
                Integer num = null;
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(next.getCurrentHandGroup());
                    InstalledApp installedApp2 = next.getInstalledApp();
                    String identifier = installedApp2 != null ? installedApp2.getIdentifier() : null;
                    if (identifier != null) {
                        appFilter.setType(identifier);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Removed: App name = ");
                        InstalledApp installedApp3 = next.getInstalledApp();
                        sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                        sb.append(", row id = ");
                        InstalledApp installedApp4 = next.getInstalledApp();
                        if (installedApp4 != null) {
                            num = Integer.valueOf(installedApp4.getDbRowId());
                        }
                        sb.append(num);
                        sb.append(", hour = ");
                        sb.append(next.getCurrentHandGroup());
                        local.d(str, sb.toString());
                        arrayList.add(appFilter);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }
}
