package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$IntRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class su2 extends as2 implements v73, ks2, ws3.g {
    @DexIgnore
    public static /* final */ String y; // = y;
    @DexIgnore
    public static /* final */ a z; // = new a((fd4) null);
    @DexIgnore
    public DashboardActivityPresenter k;
    @DexIgnore
    public DashboardActiveTimePresenter l;
    @DexIgnore
    public DashboardCaloriesPresenter m;
    @DexIgnore
    public DashboardHeartRatePresenter n;
    @DexIgnore
    public DashboardSleepPresenter o;
    @DexIgnore
    public DashboardGoalTrackingPresenter p;
    @DexIgnore
    public tr3<yc2> q;
    @DexIgnore
    public u73 r;
    @DexIgnore
    public /* final */ ArrayList<Fragment> s; // = new ArrayList<>();
    @DexIgnore
    public int t;
    @DexIgnore
    public hu3 u;
    @DexIgnore
    public ObjectAnimator v;
    @DexIgnore
    public int w; // = -1;
    @DexIgnore
    public HashMap x;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return su2.y;
        }

        @DexIgnore
        public final su2 b() {
            return new su2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 a;
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar b;

        @DexIgnore
        public b(su2 su2, ProgressBar progressBar) {
            this.a = su2;
            this.b = progressBar;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.b.setProgress(0);
            this.b.setVisibility(4);
            this.a.w = -1;
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerView.p {
        @DexIgnore
        public void a(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public boolean b(RecyclerView recyclerView, MotionEvent motionEvent) {
            kd4.b(recyclerView, "p0");
            kd4.b(motionEvent, "p1");
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public e(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity, false);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public f(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final boolean onLongClick(View view) {
            DebugActivity.a aVar = DebugActivity.P;
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                aVar.a(activity);
                return false;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public g(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(0);
            su2.b(this.e).a(0);
            this.e.q(0);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public h(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(1);
            su2.b(this.e).a(1);
            this.e.q(1);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public i(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(2);
            su2.b(this.e).a(2);
            this.e.q(2);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public j(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(3);
            su2.b(this.e).a(3);
            this.e.q(3);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public k(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(5);
            su2.b(this.e).a(5);
            this.e.q(5);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ su2 e;

        @DexIgnore
        public l(su2 su2) {
            this.e = su2;
        }

        @DexIgnore
        public final void onClick(View view) {
            su2.b(this.e).b(4);
            su2.b(this.e).a(4);
            this.e.q(4);
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements AppBarLayout.c {
        @DexIgnore
        public /* final */ /* synthetic */ yc2 a;

        @DexIgnore
        public m(yc2 yc2) {
            this.a = yc2;
        }

        @DexIgnore
        public void a(AppBarLayout appBarLayout, int i) {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.a.M;
            kd4.a((Object) customSwipeRefreshLayout, "binding.srlPullToSync");
            customSwipeRefreshLayout.setEnabled(Math.abs(i) == 0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$IntRef b;

        @DexIgnore
        public n(Ref$IntRef ref$IntRef, ProgressBar progressBar, Ref$IntRef ref$IntRef2, su2 su2, int i) {
            this.a = progressBar;
            this.b = ref$IntRef2;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            ProgressBar progressBar = this.a;
            kd4.a((Object) progressBar, "it");
            int i = this.b.element;
            kd4.a((Object) valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                progressBar.setProgress(i + ((Integer) animatedValue).intValue());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar a;
        @DexIgnore
        public /* final */ /* synthetic */ su2 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public o(Ref$IntRef ref$IntRef, ProgressBar progressBar, Ref$IntRef ref$IntRef2, su2 su2, int i) {
            this.a = progressBar;
            this.b = su2;
            this.c = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            kd4.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            kd4.b(animator, "animation");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = su2.z.a();
            local.d(a2, "onAnimationEnd " + this.c);
            su2 su2 = this.b;
            ProgressBar progressBar = this.a;
            kd4.a((Object) progressBar, "it");
            su2.a(progressBar, this.c);
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            kd4.b(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            kd4.b(animator, "animation");
        }
    }

    @DexIgnore
    public static final /* synthetic */ u73 b(su2 su2) {
        u73 u73 = su2.r;
        if (u73 != null) {
            return u73;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void C() {
        if (isActive()) {
            FLogger.INSTANCE.getLocal().e(y, "confirmCancelingWorkout");
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.c(childFragmentManager);
            FLogger.INSTANCE.getLocal().e(y, "confirmCancelingWorkout");
        }
    }

    @DexIgnore
    public void N(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append(y);
        sb.append(" visible=");
        sb.append(z2);
        sb.append(", tracer=");
        sb.append(Q0());
        sb.append(", isRunning=");
        vl2 Q0 = Q0();
        sb.append(Q0 != null ? Boolean.valueOf(Q0.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z2) {
            vl2 Q02 = Q0();
            if (Q02 != null) {
                Q02.d();
            }
            if (this.q != null) {
                int i2 = this.w;
                if (i2 == 2 || i2 == -1) {
                    U0();
                    return;
                }
                return;
            }
            return;
        }
        vl2 Q03 = Q0();
        if (Q03 != null) {
            Q03.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z2) {
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                if (z2) {
                    ProgressBar progressBar = a2.N;
                    kd4.a((Object) progressBar, "syncProgress");
                    progressBar.setVisibility(0);
                } else {
                    ProgressBar progressBar2 = a2.N;
                    kd4.a((Object) progressBar2, "syncProgress");
                    progressBar2.setVisibility(4);
                }
                View view = a2.P;
                kd4.a((Object) view, "vBorderBottom");
                view.setVisibility(4);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String R0() {
        return y;
    }

    @DexIgnore
    public void S() {
        if (isActive()) {
            tr3<yc2> tr3 = this.q;
            if (tr3 != null) {
                yc2 a2 = tr3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.t;
                    kd4.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(0);
                    ProgressBar progressBar = a2.G;
                    kd4.a((Object) progressBar, "it.pbProgress");
                    progressBar.setMax(100);
                    a2.M.setDisableSwipe(true);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final void T0() {
        u73 u73 = this.r;
        if (u73 == null) {
            return;
        }
        if (u73 != null) {
            int i2 = u73.i();
            if (!this.s.isEmpty()) {
                int size = this.s.size();
                int i3 = 0;
                while (i3 < size) {
                    if (this.s.get(i3) instanceof ks2) {
                        Fragment fragment = this.s.get(i3);
                        if (fragment != null) {
                            ((ks2) fragment).N(i3 == i2);
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                        }
                    }
                    i3++;
                }
            }
            if (i2 == 0) {
                R("steps_view");
            } else if (i2 == 1) {
                R("active_minutes_view");
            } else if (i2 == 2) {
                R("calories_view");
            } else if (i2 == 3) {
                R("heart_rate_view");
            } else if (i2 == 5) {
                R("sleep_view");
            }
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void U() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.H(childFragmentManager);
        }
    }

    @DexIgnore
    public final void U0() {
        FLogger.INSTANCE.getLocal().d(y, "cancelSyncProgress");
        ObjectAnimator objectAnimator = this.v;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.N;
                kd4.a((Object) progressBar, "it.syncProgress");
                progressBar.setVisibility(4);
                ProgressBar progressBar2 = a2.N;
                kd4.a((Object) progressBar2, "it.syncProgress");
                progressBar2.setProgress(0);
                a2.M.c();
            }
            u73 u73 = this.r;
            if (u73 != null) {
                u73.a(false);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void V() {
        FLogger.INSTANCE.getLocal().e(y, "showAutoSync");
        p(0);
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                CustomSwipeRefreshLayout customSwipeRefreshLayout = a2.M;
                if (customSwipeRefreshLayout != null) {
                    customSwipeRefreshLayout.g();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V0() {
        Object a2 = getChildFragmentManager().a(a93.q.a());
        Object a3 = getChildFragmentManager().a("DashboardActiveTimeFragment");
        Object a4 = getChildFragmentManager().a("DashboardCaloriesFragment");
        Object a5 = getChildFragmentManager().a(ac3.q.a());
        Object a6 = getChildFragmentManager().a(ad3.q.a());
        Object a7 = getChildFragmentManager().a(ab3.q.a());
        if (a2 == null) {
            a2 = a93.q.b();
        }
        if (a3 == null) {
            a3 = a83.p.a();
        }
        if (a4 == null) {
            a4 = new aa3();
        }
        if (a5 == null) {
            a5 = ac3.q.b();
        }
        if (a6 == null) {
            a6 = ad3.q.b();
        }
        if (a7 == null) {
            a7 = ab3.q.b();
        }
        this.s.clear();
        this.s.add(a2);
        this.s.add(a3);
        this.s.add(a4);
        this.s.add(a5);
        this.s.add(a7);
        this.s.add(a6);
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a8 = tr3.a();
            if (a8 != null) {
                u73 u73 = this.r;
                if (u73 != null) {
                    if (u73 == null) {
                        kd4.d("mPresenter");
                        throw null;
                    } else if (u73.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                        ConstraintLayout constraintLayout = a8.x.q;
                        kd4.a((Object) constraintLayout, "icActiveTime.clRoot");
                        constraintLayout.setVisibility(0);
                        ConstraintLayout constraintLayout2 = a8.B.q;
                        kd4.a((Object) constraintLayout2, "icHeartRate.clRoot");
                        constraintLayout2.setVisibility(0);
                        ConstraintLayout constraintLayout3 = a8.A.q;
                        kd4.a((Object) constraintLayout3, "icGoalTracking.clRoot");
                        constraintLayout3.setVisibility(8);
                    } else {
                        ConstraintLayout constraintLayout4 = a8.x.q;
                        kd4.a((Object) constraintLayout4, "icActiveTime.clRoot");
                        constraintLayout4.setVisibility(8);
                        ConstraintLayout constraintLayout5 = a8.B.q;
                        kd4.a((Object) constraintLayout5, "icHeartRate.clRoot");
                        constraintLayout5.setVisibility(8);
                        ConstraintLayout constraintLayout6 = a8.A.q;
                        kd4.a((Object) constraintLayout6, "icGoalTracking.clRoot");
                        constraintLayout6.setVisibility(0);
                    }
                }
                RecyclerViewPager recyclerViewPager = a8.L;
                kd4.a((Object) recyclerViewPager, "rvTabs");
                recyclerViewPager.setAdapter(new cu3(getChildFragmentManager(), this.s));
                a8.L.setItemViewCacheSize(6);
                a8.L.a((RecyclerView.p) new c());
            }
            l42 g2 = PortfolioApp.W.c().g();
            if (a2 != null) {
                a93 a93 = (a93) a2;
                if (a3 != null) {
                    a83 a83 = (a83) a3;
                    aa3 aa3 = (aa3) a4;
                    if (a5 != null) {
                        ac3 ac3 = (ac3) a5;
                        if (a6 != null) {
                            ad3 ad3 = (ad3) a6;
                            if (a7 != null) {
                                g2.a(new n73(a93, a83, aa3, ac3, ad3, (ab3) a7)).a(this);
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W0() {
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                RingProgressBar ringProgressBar = a2.I;
                kd4.a((Object) ringProgressBar, "it.rpbBiggest");
                a(ringProgressBar, RingProgressBar.Type.STEPS);
                u73 u73 = this.r;
                if (u73 == null) {
                    kd4.d("mPresenter");
                    throw null;
                } else if (u73.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    RingProgressBar ringProgressBar2 = a2.K;
                    kd4.a((Object) ringProgressBar2, "it.rpbSmallest");
                    ringProgressBar2.setVisibility(0);
                    RingProgressBar ringProgressBar3 = a2.H;
                    kd4.a((Object) ringProgressBar3, "it.rpbBig");
                    a(ringProgressBar3, RingProgressBar.Type.ACTIVE_TIME);
                    RingProgressBar ringProgressBar4 = a2.J;
                    kd4.a((Object) ringProgressBar4, "it.rpbMedium");
                    a(ringProgressBar4, RingProgressBar.Type.CALORIES);
                    RingProgressBar ringProgressBar5 = a2.K;
                    kd4.a((Object) ringProgressBar5, "it.rpbSmallest");
                    a(ringProgressBar5, RingProgressBar.Type.SLEEP);
                } else {
                    RingProgressBar ringProgressBar6 = a2.K;
                    kd4.a((Object) ringProgressBar6, "it.rpbSmallest");
                    ringProgressBar6.setVisibility(0);
                    RingProgressBar ringProgressBar7 = a2.H;
                    kd4.a((Object) ringProgressBar7, "it.rpbBig");
                    a(ringProgressBar7, RingProgressBar.Type.CALORIES);
                    RingProgressBar ringProgressBar8 = a2.J;
                    kd4.a((Object) ringProgressBar8, "it.rpbMedium");
                    a(ringProgressBar8, RingProgressBar.Type.SLEEP);
                    RingProgressBar ringProgressBar9 = a2.K;
                    kd4.a((Object) ringProgressBar9, "it.rpbSmallest");
                    a(ringProgressBar9, RingProgressBar.Type.GOAL);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void j(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.e(str, "syncCompleted - success: " + z2);
        if (!z2) {
            U0();
        } else {
            p(1);
        }
    }

    @DexIgnore
    public void o(boolean z2) {
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                NestedScrollView nestedScrollView = a2.F;
                kd4.a((Object) nestedScrollView, "it.nsvLowBattery");
                nestedScrollView.setVisibility(0);
                return;
            }
            NestedScrollView nestedScrollView2 = a2.F;
            kd4.a((Object) nestedScrollView2, "it.nsvLowBattery");
            nestedScrollView2.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FLogger.INSTANCE.getLocal().d(y, "onActivityCreated");
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.q = new tr3<>(this, (yc2) qa.a(layoutInflater, R.layout.fragment_home_dashboard, viewGroup, false, O0()));
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        u73 u73 = this.r;
        if (u73 == null) {
            return;
        }
        if (u73 != null) {
            u73.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        u73 u73 = this.r;
        if (u73 == null) {
            return;
        }
        if (u73 != null) {
            q(u73.i());
            W0();
            u73 u732 = this.r;
            if (u732 != null) {
                u732.f();
                vl2 Q0 = Q0();
                if (Q0 != null) {
                    Q0.d();
                    return;
                }
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        V0();
        tr3<yc2> tr3 = this.q;
        hu3 hu3 = null;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                kd4.a((Object) flexibleTextView, "binding.ftvDescription");
                pd4 pd4 = pd4.a;
                String a3 = sm2.a(getContext(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Text__PairABrandSmartwatchToStart);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026rABrandSmartwatchToStart)");
                Object[] objArr = {PortfolioApp.W.c().i()};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                ProgressBar progressBar = a2.N;
                kd4.a((Object) progressBar, "binding.syncProgress");
                progressBar.setMax(10000);
                a2.M.setOnRefreshListener(new d(a2, this));
                View headView = a2.M.getHeadView();
                if (headView instanceof hu3) {
                    hu3 = headView;
                }
                this.u = hu3;
                a2.w.setOnClickListener(new e(this));
                if (!PortfolioApp.W.c().D()) {
                    a2.E.setOnLongClickListener(new f(this));
                }
                a2.q.a((AppBarLayout.c) new m(a2));
                wi2 wi2 = a2.y;
                wi2.t.setImageResource(R.drawable.ic_customize_steps);
                FlexibleTextView flexibleTextView2 = wi2.r;
                kd4.a((Object) flexibleTextView2, "ftvTabUnit");
                flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Label__Steps));
                wi2.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_activity);
                wi2.q.setOnClickListener(new g(this));
                wi2 wi22 = a2.x;
                wi22.t.setImageResource(R.drawable.ic_customize_stopwatch);
                FlexibleTextView flexibleTextView3 = wi22.r;
                kd4.a((Object) flexibleTextView3, "ftvTabUnit");
                flexibleTextView3.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Label__Mins));
                wi22.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_active_time);
                wi22.q.setOnClickListener(new h(this));
                wi2 wi23 = a2.z;
                wi23.t.setImageResource(R.drawable.ic_customize_calories);
                FlexibleTextView flexibleTextView4 = wi23.r;
                kd4.a((Object) flexibleTextView4, "ftvTabUnit");
                flexibleTextView4.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Label__Cals));
                wi23.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_calories);
                wi23.q.setOnClickListener(new i(this));
                wi2 wi24 = a2.B;
                wi24.t.setImageResource(R.drawable.ic_customize_heartrate);
                FlexibleTextView flexibleTextView5 = wi24.r;
                kd4.a((Object) flexibleTextView5, "ftvTabUnit");
                flexibleTextView5.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Label__Resting));
                wi24.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_heart_rate);
                wi24.q.setOnClickListener(new j(this));
                wi2 wi25 = a2.C;
                wi25.t.setImageResource(R.drawable.ic_customize_sleep);
                FlexibleTextView flexibleTextView6 = wi25.r;
                kd4.a((Object) flexibleTextView6, "ftvTabUnit");
                flexibleTextView6.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Dashboard_Label__HrsMins));
                wi25.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_sleep);
                wi25.q.setOnClickListener(new k(this));
                wi2 wi26 = a2.A;
                wi26.t.setImageResource(R.drawable.ic_goal);
                wi26.q.setBackgroundResource(R.drawable.selector_bg_dashboard_tab_goal_tracking);
                wi26.q.setOnClickListener(new l(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        StringBuilder sb = new StringBuilder();
        sb.append("runProgress state ");
        sb.append(i2);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        kd4.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d(str, sb.toString());
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.N;
                if (progressBar != null) {
                    Ref$IntRef ref$IntRef = new Ref$IntRef();
                    ref$IntRef.element = 0;
                    Ref$IntRef ref$IntRef2 = new Ref$IntRef();
                    ref$IntRef2.element = 0;
                    this.w = i2;
                    int i3 = 1000;
                    if (i2 == 0) {
                        ref$IntRef.element = 0;
                        ref$IntRef2.element = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                        i3 = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                    } else if (i2 == 1) {
                        ref$IntRef.element = FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
                        i3 = 7000;
                        ref$IntRef2.element = 300;
                    } else if (i2 != 2) {
                        i3 = 0;
                    } else {
                        ref$IntRef.element = 9000;
                        ref$IntRef2.element = 1000;
                    }
                    kd4.a((Object) progressBar, "it");
                    progressBar.setProgress(ref$IntRef.element);
                    this.v = ObjectAnimator.ofInt(this, "", new int[]{i3});
                    ObjectAnimator objectAnimator = this.v;
                    if (objectAnimator != null) {
                        objectAnimator.setDuration((long) ref$IntRef2.element);
                        Ref$IntRef ref$IntRef3 = ref$IntRef2;
                        ProgressBar progressBar2 = progressBar;
                        Ref$IntRef ref$IntRef4 = ref$IntRef;
                        int i4 = i2;
                        objectAnimator.addUpdateListener(new n(ref$IntRef3, progressBar2, ref$IntRef4, this, i4));
                        objectAnimator.addListener(new o(ref$IntRef3, progressBar2, ref$IntRef4, this, i4));
                        objectAnimator.start();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void q(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "scroll to position=" + i2);
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.y.q;
                kd4.a((Object) constraintLayout, "icActivity.clRoot");
                constraintLayout.setSelected(false);
                ConstraintLayout constraintLayout2 = a2.x.q;
                kd4.a((Object) constraintLayout2, "icActiveTime.clRoot");
                constraintLayout2.setSelected(false);
                ConstraintLayout constraintLayout3 = a2.z.q;
                kd4.a((Object) constraintLayout3, "icCalorie.clRoot");
                constraintLayout3.setSelected(false);
                ConstraintLayout constraintLayout4 = a2.B.q;
                kd4.a((Object) constraintLayout4, "icHeartRate.clRoot");
                constraintLayout4.setSelected(false);
                ConstraintLayout constraintLayout5 = a2.C.q;
                kd4.a((Object) constraintLayout5, "icSleep.clRoot");
                constraintLayout5.setSelected(false);
                ConstraintLayout constraintLayout6 = a2.A.q;
                kd4.a((Object) constraintLayout6, "icGoalTracking.clRoot");
                constraintLayout6.setSelected(false);
                ImageView imageView = a2.y.t;
                Context context = getContext();
                if (context != null) {
                    imageView.setColorFilter(k6.a(context, (int) R.color.onDianaInactiveTab));
                    FlexibleTextView flexibleTextView = a2.y.r;
                    Context context2 = getContext();
                    if (context2 != null) {
                        flexibleTextView.setTextColor(k6.a(context2, (int) R.color.onDianaInactiveTab));
                        FlexibleTextView flexibleTextView2 = a2.y.s;
                        Context context3 = getContext();
                        if (context3 != null) {
                            flexibleTextView2.setTextColor(k6.a(context3, (int) R.color.onDianaInactiveTab));
                            ImageView imageView2 = a2.x.t;
                            Context context4 = getContext();
                            if (context4 != null) {
                                imageView2.setColorFilter(k6.a(context4, (int) R.color.onDianaInactiveTab));
                                FlexibleTextView flexibleTextView3 = a2.x.r;
                                Context context5 = getContext();
                                if (context5 != null) {
                                    flexibleTextView3.setTextColor(k6.a(context5, (int) R.color.onDianaInactiveTab));
                                    FlexibleTextView flexibleTextView4 = a2.x.s;
                                    Context context6 = getContext();
                                    if (context6 != null) {
                                        flexibleTextView4.setTextColor(k6.a(context6, (int) R.color.onDianaInactiveTab));
                                        ImageView imageView3 = a2.z.t;
                                        Context context7 = getContext();
                                        if (context7 != null) {
                                            imageView3.setColorFilter(k6.a(context7, (int) R.color.onDianaInactiveTab));
                                            FlexibleTextView flexibleTextView5 = a2.z.r;
                                            Context context8 = getContext();
                                            if (context8 != null) {
                                                flexibleTextView5.setTextColor(k6.a(context8, (int) R.color.onDianaInactiveTab));
                                                FlexibleTextView flexibleTextView6 = a2.z.s;
                                                Context context9 = getContext();
                                                if (context9 != null) {
                                                    flexibleTextView6.setTextColor(k6.a(context9, (int) R.color.onDianaInactiveTab));
                                                    ImageView imageView4 = a2.B.t;
                                                    Context context10 = getContext();
                                                    if (context10 != null) {
                                                        imageView4.setColorFilter(k6.a(context10, (int) R.color.onDianaInactiveTab));
                                                        FlexibleTextView flexibleTextView7 = a2.B.r;
                                                        Context context11 = getContext();
                                                        if (context11 != null) {
                                                            flexibleTextView7.setTextColor(k6.a(context11, (int) R.color.onDianaInactiveTab));
                                                            FlexibleTextView flexibleTextView8 = a2.B.s;
                                                            Context context12 = getContext();
                                                            if (context12 != null) {
                                                                flexibleTextView8.setTextColor(k6.a(context12, (int) R.color.onDianaInactiveTab));
                                                                ImageView imageView5 = a2.C.t;
                                                                Context context13 = getContext();
                                                                if (context13 != null) {
                                                                    imageView5.setColorFilter(k6.a(context13, (int) R.color.onDianaInactiveTab));
                                                                    FlexibleTextView flexibleTextView9 = a2.C.r;
                                                                    Context context14 = getContext();
                                                                    if (context14 != null) {
                                                                        flexibleTextView9.setTextColor(k6.a(context14, (int) R.color.onDianaInactiveTab));
                                                                        FlexibleTextView flexibleTextView10 = a2.C.s;
                                                                        Context context15 = getContext();
                                                                        if (context15 != null) {
                                                                            flexibleTextView10.setTextColor(k6.a(context15, (int) R.color.onDianaInactiveTab));
                                                                            ImageView imageView6 = a2.A.t;
                                                                            Context context16 = getContext();
                                                                            if (context16 != null) {
                                                                                imageView6.setColorFilter(k6.a(context16, (int) R.color.onDianaInactiveTab));
                                                                                FlexibleTextView flexibleTextView11 = a2.A.r;
                                                                                Context context17 = getContext();
                                                                                if (context17 != null) {
                                                                                    flexibleTextView11.setTextColor(k6.a(context17, (int) R.color.onDianaInactiveTab));
                                                                                    FlexibleTextView flexibleTextView12 = a2.A.s;
                                                                                    Context context18 = getContext();
                                                                                    if (context18 != null) {
                                                                                        flexibleTextView12.setTextColor(k6.a(context18, (int) R.color.onDianaInactiveTab));
                                                                                        if (i2 == 0) {
                                                                                            View view = a2.Q;
                                                                                            Context context19 = getContext();
                                                                                            if (context19 != null) {
                                                                                                view.setBackgroundColor(k6.a(context19, (int) R.color.dianaStepsTab));
                                                                                                ConstraintLayout constraintLayout7 = a2.y.q;
                                                                                                kd4.a((Object) constraintLayout7, "icActivity.clRoot");
                                                                                                ConstraintLayout constraintLayout8 = a2.y.q;
                                                                                                kd4.a((Object) constraintLayout8, "icActivity.clRoot");
                                                                                                constraintLayout7.setSelected(true ^ constraintLayout8.isSelected());
                                                                                                ImageView imageView7 = a2.y.t;
                                                                                                Context context20 = getContext();
                                                                                                if (context20 != null) {
                                                                                                    imageView7.setColorFilter(k6.a(context20, (int) R.color.onDianaStepsTab));
                                                                                                    FlexibleTextView flexibleTextView13 = a2.y.r;
                                                                                                    Context context21 = getContext();
                                                                                                    if (context21 != null) {
                                                                                                        flexibleTextView13.setTextColor(k6.a(context21, (int) R.color.onDianaStepsTab));
                                                                                                        FlexibleTextView flexibleTextView14 = a2.y.s;
                                                                                                        Context context22 = getContext();
                                                                                                        if (context22 != null) {
                                                                                                            flexibleTextView14.setTextColor(k6.a(context22, (int) R.color.onDianaStepsTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        } else if (i2 == 1) {
                                                                                            View view2 = a2.Q;
                                                                                            Context context23 = getContext();
                                                                                            if (context23 != null) {
                                                                                                view2.setBackgroundColor(k6.a(context23, (int) R.color.dianaActiveMinutesTab));
                                                                                                ConstraintLayout constraintLayout9 = a2.x.q;
                                                                                                kd4.a((Object) constraintLayout9, "icActiveTime.clRoot");
                                                                                                ConstraintLayout constraintLayout10 = a2.x.q;
                                                                                                kd4.a((Object) constraintLayout10, "icActiveTime.clRoot");
                                                                                                constraintLayout9.setSelected(true ^ constraintLayout10.isSelected());
                                                                                                ImageView imageView8 = a2.x.t;
                                                                                                Context context24 = getContext();
                                                                                                if (context24 != null) {
                                                                                                    imageView8.setColorFilter(k6.a(context24, (int) R.color.onDianaActiveMinutesTab));
                                                                                                    FlexibleTextView flexibleTextView15 = a2.x.r;
                                                                                                    Context context25 = getContext();
                                                                                                    if (context25 != null) {
                                                                                                        flexibleTextView15.setTextColor(k6.a(context25, (int) R.color.onDianaActiveMinutesTab));
                                                                                                        FlexibleTextView flexibleTextView16 = a2.x.s;
                                                                                                        Context context26 = getContext();
                                                                                                        if (context26 != null) {
                                                                                                            flexibleTextView16.setTextColor(k6.a(context26, (int) R.color.onDianaActiveMinutesTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        } else if (i2 == 2) {
                                                                                            View view3 = a2.Q;
                                                                                            Context context27 = getContext();
                                                                                            if (context27 != null) {
                                                                                                view3.setBackgroundColor(k6.a(context27, (int) R.color.dianaActiveCaloriesTab));
                                                                                                ConstraintLayout constraintLayout11 = a2.z.q;
                                                                                                kd4.a((Object) constraintLayout11, "icCalorie.clRoot");
                                                                                                ConstraintLayout constraintLayout12 = a2.z.q;
                                                                                                kd4.a((Object) constraintLayout12, "icCalorie.clRoot");
                                                                                                constraintLayout11.setSelected(true ^ constraintLayout12.isSelected());
                                                                                                ImageView imageView9 = a2.z.t;
                                                                                                Context context28 = getContext();
                                                                                                if (context28 != null) {
                                                                                                    imageView9.setColorFilter(k6.a(context28, (int) R.color.onDianaActiveCaloriesTab));
                                                                                                    FlexibleTextView flexibleTextView17 = a2.z.r;
                                                                                                    Context context29 = getContext();
                                                                                                    if (context29 != null) {
                                                                                                        flexibleTextView17.setTextColor(k6.a(context29, (int) R.color.onDianaActiveCaloriesTab));
                                                                                                        FlexibleTextView flexibleTextView18 = a2.z.s;
                                                                                                        Context context30 = getContext();
                                                                                                        if (context30 != null) {
                                                                                                            flexibleTextView18.setTextColor(k6.a(context30, (int) R.color.onDianaActiveCaloriesTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        } else if (i2 == 3) {
                                                                                            View view4 = a2.Q;
                                                                                            Context context31 = getContext();
                                                                                            if (context31 != null) {
                                                                                                view4.setBackgroundColor(k6.a(context31, (int) R.color.dianaHeartRateTab));
                                                                                                ConstraintLayout constraintLayout13 = a2.B.q;
                                                                                                kd4.a((Object) constraintLayout13, "icHeartRate.clRoot");
                                                                                                ConstraintLayout constraintLayout14 = a2.B.q;
                                                                                                kd4.a((Object) constraintLayout14, "icHeartRate.clRoot");
                                                                                                constraintLayout13.setSelected(true ^ constraintLayout14.isSelected());
                                                                                                ImageView imageView10 = a2.B.t;
                                                                                                Context context32 = getContext();
                                                                                                if (context32 != null) {
                                                                                                    imageView10.setColorFilter(k6.a(context32, (int) R.color.onDianaHeartRateTab));
                                                                                                    FlexibleTextView flexibleTextView19 = a2.B.r;
                                                                                                    Context context33 = getContext();
                                                                                                    if (context33 != null) {
                                                                                                        flexibleTextView19.setTextColor(k6.a(context33, (int) R.color.onDianaHeartRateTab));
                                                                                                        FlexibleTextView flexibleTextView20 = a2.B.s;
                                                                                                        Context context34 = getContext();
                                                                                                        if (context34 != null) {
                                                                                                            flexibleTextView20.setTextColor(k6.a(context34, (int) R.color.onDianaHeartRateTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        } else if (i2 == 4) {
                                                                                            View view5 = a2.Q;
                                                                                            Context context35 = getContext();
                                                                                            if (context35 != null) {
                                                                                                view5.setBackgroundColor(k6.a(context35, (int) R.color.hybridGoalTrackingTab));
                                                                                                ConstraintLayout constraintLayout15 = a2.A.q;
                                                                                                kd4.a((Object) constraintLayout15, "icGoalTracking.clRoot");
                                                                                                ConstraintLayout constraintLayout16 = a2.A.q;
                                                                                                kd4.a((Object) constraintLayout16, "icGoalTracking.clRoot");
                                                                                                constraintLayout15.setSelected(true ^ constraintLayout16.isSelected());
                                                                                                ImageView imageView11 = a2.A.t;
                                                                                                Context context36 = getContext();
                                                                                                if (context36 != null) {
                                                                                                    imageView11.setColorFilter(k6.a(context36, (int) R.color.onHybridGoalTrackingTab));
                                                                                                    FlexibleTextView flexibleTextView21 = a2.A.r;
                                                                                                    Context context37 = getContext();
                                                                                                    if (context37 != null) {
                                                                                                        flexibleTextView21.setTextColor(k6.a(context37, (int) R.color.onHybridGoalTrackingTab));
                                                                                                        FlexibleTextView flexibleTextView22 = a2.A.s;
                                                                                                        Context context38 = getContext();
                                                                                                        if (context38 != null) {
                                                                                                            flexibleTextView22.setTextColor(k6.a(context38, (int) R.color.onHybridGoalTrackingTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        } else if (i2 == 5) {
                                                                                            View view6 = a2.Q;
                                                                                            Context context39 = getContext();
                                                                                            if (context39 != null) {
                                                                                                view6.setBackgroundColor(k6.a(context39, (int) R.color.dianaSleepTab));
                                                                                                ConstraintLayout constraintLayout17 = a2.C.q;
                                                                                                kd4.a((Object) constraintLayout17, "icSleep.clRoot");
                                                                                                ConstraintLayout constraintLayout18 = a2.C.q;
                                                                                                kd4.a((Object) constraintLayout18, "icSleep.clRoot");
                                                                                                constraintLayout17.setSelected(true ^ constraintLayout18.isSelected());
                                                                                                ImageView imageView12 = a2.C.t;
                                                                                                Context context40 = getContext();
                                                                                                if (context40 != null) {
                                                                                                    imageView12.setColorFilter(k6.a(context40, (int) R.color.onDianaSleepTab));
                                                                                                    FlexibleTextView flexibleTextView23 = a2.C.r;
                                                                                                    Context context41 = getContext();
                                                                                                    if (context41 != null) {
                                                                                                        flexibleTextView23.setTextColor(k6.a(context41, (int) R.color.onDianaSleepTab));
                                                                                                        FlexibleTextView flexibleTextView24 = a2.C.s;
                                                                                                        Context context42 = getContext();
                                                                                                        if (context42 != null) {
                                                                                                            flexibleTextView24.setTextColor(k6.a(context42, (int) R.color.onDianaSleepTab));
                                                                                                        } else {
                                                                                                            kd4.a();
                                                                                                            throw null;
                                                                                                        }
                                                                                                    } else {
                                                                                                        kd4.a();
                                                                                                        throw null;
                                                                                                    }
                                                                                                } else {
                                                                                                    kd4.a();
                                                                                                    throw null;
                                                                                                }
                                                                                            } else {
                                                                                                kd4.a();
                                                                                                throw null;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        kd4.a();
                                                                                        throw null;
                                                                                    }
                                                                                } else {
                                                                                    kd4.a();
                                                                                    throw null;
                                                                                }
                                                                            } else {
                                                                                kd4.a();
                                                                                throw null;
                                                                            }
                                                                        } else {
                                                                            kd4.a();
                                                                            throw null;
                                                                        }
                                                                    } else {
                                                                        kd4.a();
                                                                        throw null;
                                                                    }
                                                                } else {
                                                                    kd4.a();
                                                                    throw null;
                                                                }
                                                            } else {
                                                                kd4.a();
                                                                throw null;
                                                            }
                                                        } else {
                                                            kd4.a();
                                                            throw null;
                                                        }
                                                    } else {
                                                        kd4.a();
                                                        throw null;
                                                    }
                                                } else {
                                                    kd4.a();
                                                    throw null;
                                                }
                                            } else {
                                                kd4.a();
                                                throw null;
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            tr3<yc2> tr32 = this.q;
            if (tr32 != null) {
                yc2 a3 = tr32.a();
                if (a3 != null) {
                    RecyclerViewPager recyclerViewPager = a3.L;
                    if (recyclerViewPager != null) {
                        recyclerViewPager.i(i2);
                    }
                }
                this.t = i2;
                u73 u73 = this.r;
                if (u73 != null) {
                    u73.a(this.t);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void z() {
        if (isActive()) {
            tr3<yc2> tr3 = this.q;
            if (tr3 != null) {
                yc2 a2 = tr3.a();
                if (a2 != null) {
                    AppBarLayout appBarLayout = a2.q;
                    if (appBarLayout != null) {
                        appBarLayout.a(true, true);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CustomSwipeRefreshLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ yc2 a;
        @DexIgnore
        public /* final */ /* synthetic */ su2 b;

        @DexIgnore
        public d(yc2 yc2, su2 su2) {
            this.a = yc2;
            this.b = su2;
        }

        @DexIgnore
        public void a(boolean z) {
            FLogger.INSTANCE.getLocal().e(su2.z.a(), "onEndSwipe");
            this.b.O(z);
        }

        @DexIgnore
        public void b() {
            FLogger.INSTANCE.getLocal().e(su2.z.a(), "onStartSwipe");
            ProgressBar progressBar = this.a.N;
            kd4.a((Object) progressBar, "binding.syncProgress");
            progressBar.setVisibility(4);
            View view = this.a.P;
            kd4.a((Object) view, "binding.vBorderBottom");
            view.setVisibility(0);
        }

        @DexIgnore
        public void a() {
            FLogger.INSTANCE.getLocal().d(su2.z.a(), "onRefresh");
            su2.b(this.b).j();
        }
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "updateOtaProgress " + i2 + " isActive " + isActive());
        if (isActive()) {
            tr3<yc2> tr3 = this.q;
            if (tr3 != null) {
                yc2 a2 = tr3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.t;
                    kd4.a((Object) constraintLayout, "it.clUpdateFw");
                    if (constraintLayout.getVisibility() != 0) {
                        ConstraintLayout constraintLayout2 = a2.t;
                        kd4.a((Object) constraintLayout2, "it.clUpdateFw");
                        constraintLayout2.setVisibility(0);
                        ProgressBar progressBar = a2.G;
                        kd4.a((Object) progressBar, "it.pbProgress");
                        progressBar.setMax(100);
                    }
                    ProgressBar progressBar2 = a2.N;
                    kd4.a((Object) progressBar2, "it.syncProgress");
                    if (progressBar2.getVisibility() == 0) {
                        U0();
                    }
                    a2.M.setDisableSwipe(true);
                    ProgressBar progressBar3 = a2.G;
                    kd4.a((Object) progressBar3, "it.pbProgress");
                    progressBar3.setProgress(i2);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(RingProgressBar ringProgressBar, RingProgressBar.Type type) {
        int i2 = tu2.a[type.ordinal()];
        if (i2 == 1) {
            ringProgressBar.setIconSource(R.drawable.ic_visualization_active_time);
            Context context = getContext();
            if (context != null) {
                ringProgressBar.setProgressColor(k6.a(context, (int) R.color.dianaActiveMinutesRing));
            } else {
                kd4.a();
                throw null;
            }
        } else if (i2 == 2) {
            ringProgressBar.setIconSource(R.drawable.ic_visualization_steps);
            u73 u73 = this.r;
            if (u73 == null) {
                kd4.d("mPresenter");
                throw null;
            } else if (u73.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                Context context2 = getContext();
                if (context2 != null) {
                    ringProgressBar.setProgressColor(k6.a(context2, (int) R.color.dianaStepsRing));
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                Context context3 = getContext();
                if (context3 != null) {
                    ringProgressBar.setProgressColor(k6.a(context3, (int) R.color.hybridStepsRing));
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else if (i2 == 3) {
            ringProgressBar.setIconSource(R.drawable.ic_visualization_calories);
            u73 u732 = this.r;
            if (u732 == null) {
                kd4.d("mPresenter");
                throw null;
            } else if (u732.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                Context context4 = getContext();
                if (context4 != null) {
                    ringProgressBar.setProgressColor(k6.a(context4, (int) R.color.dianaActiveCaloriesRing));
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                Context context5 = getContext();
                if (context5 != null) {
                    ringProgressBar.setProgressColor(k6.a(context5, (int) R.color.hybridActiveCaloriesRing));
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else if (i2 == 4) {
            ringProgressBar.setIconSource(R.drawable.ic_visualization_sleep);
            u73 u733 = this.r;
            if (u733 == null) {
                kd4.d("mPresenter");
                throw null;
            } else if (u733.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                Context context6 = getContext();
                if (context6 != null) {
                    ringProgressBar.setProgressColor(k6.a(context6, (int) R.color.dianaSleepRing));
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                Context context7 = getContext();
                if (context7 != null) {
                    ringProgressBar.setProgressColor(k6.a(context7, (int) R.color.hybridSleepRing));
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else if (i2 == 5) {
            ringProgressBar.setIconSource(R.drawable.ic_visualization_goal);
            Context context8 = getContext();
            if (context8 != null) {
                ringProgressBar.setProgressColor(k6.a(context8, (int) R.color.hybridGoalTrackingRing));
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        if (isActive()) {
            tr3<yc2> tr3 = this.q;
            if (tr3 != null) {
                yc2 a2 = tr3.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.t;
                    kd4.a((Object) constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(8);
                    a2.M.setDisableSwipe(false);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(u73 u73) {
        kd4.b(u73, "presenter");
        this.r = u73;
    }

    @DexIgnore
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary) {
        float f2;
        float f3;
        float f4;
        float f5;
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                int a3 = xk2.d.a(activitySummary, GoalType.ACTIVE_TIME);
                float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                if (a3 > 0) {
                    f2 = (activitySummary != null ? (float) activitySummary.getActiveTime() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a3);
                } else {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a4 = xk2.d.a(activitySummary, GoalType.TOTAL_STEPS);
                if (a4 > 0) {
                    f3 = (activitySummary != null ? (float) activitySummary.getSteps() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a4);
                } else {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a5 = xk2.d.a(activitySummary, GoalType.CALORIES);
                if (a5 > 0) {
                    f4 = (activitySummary != null ? (float) activitySummary.getCalories() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a5);
                } else {
                    f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a6 = xk2.d.a(mFSleepDay);
                if (a6 > 0) {
                    f5 = (mFSleepDay != null ? (float) mFSleepDay.getSleepMinutes() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) a6);
                } else {
                    f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int a7 = xk2.d.a(goalTrackingSummary);
                if (a7 > 0) {
                    if (goalTrackingSummary != null) {
                        f6 = (float) goalTrackingSummary.getTotalTracked();
                    }
                    f6 /= (float) a7;
                }
                boolean z2 = true;
                boolean z3 = f3 >= 1.0f && f4 >= 1.0f && f5 >= 1.0f;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = y;
                local.d(str, "updateVisualization steps: " + f3 + ", time: " + f2 + ", calories: " + f4 + ", sleep: " + f5);
                u73 u73 = this.r;
                if (u73 == null) {
                    kd4.d("mPresenter");
                    throw null;
                } else if (u73.h() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    if (!z3 || f2 < 1.0f) {
                        z2 = false;
                    }
                    a2.I.a(f3, z2);
                    a2.H.a(f2, z2);
                    a2.J.a(f4, z2);
                    a2.K.a(f5, z2);
                } else {
                    if (!z3 || f6 < 1.0f) {
                        z2 = false;
                    }
                    a2.I.a(f3, z2);
                    a2.H.a(f4, z2);
                    a2.J.a(f5, z2);
                    a2.K.a(f6, z2);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z2) {
        double d2;
        int i2;
        double d3;
        int i3;
        int i4;
        PortfolioApp portfolioApp;
        String str;
        String str2;
        Integer num3 = num;
        Integer num4 = num2;
        boolean z3 = z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = y;
        StringBuilder sb = new StringBuilder();
        sb.append("setDataSummaryForTabs - latestGoalTrackingTarget=");
        sb.append(num3);
        sb.append(", ");
        sb.append("heartRateResting=");
        sb.append(num4);
        sb.append(", isNewSession=");
        sb.append(z3);
        sb.append(", mBinding.get()=");
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            sb.append(tr3.a());
            sb.append(", hashCode=");
            sb.append(hashCode());
            local.d(str3, sb.toString());
            tr3<yc2> tr32 = this.q;
            if (tr32 != null) {
                yc2 a2 = tr32.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.W.c();
                    int sleepMinutes = mFSleepDay != null ? mFSleepDay.getSleepMinutes() : 0;
                    int intValue = num4 != null ? num2.intValue() : 0;
                    if (activitySummary != null) {
                        d3 = activitySummary.getSteps();
                        i2 = activitySummary.getActiveTime();
                        d2 = activitySummary.getCalories();
                    } else {
                        d3 = 0.0d;
                        i2 = 0;
                        d2 = 0.0d;
                    }
                    if (goalTrackingSummary != null) {
                        i3 = goalTrackingSummary.getGoalTarget();
                        i4 = goalTrackingSummary.getTotalTracked();
                    } else {
                        i4 = 0;
                        i3 = 0;
                    }
                    if (i3 == 0 && num3 != null) {
                        i3 = num.intValue();
                    }
                    int i5 = i3;
                    String a3 = sm2.a((Context) c2, (int) R.string.character_dash);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str4 = y;
                    String str5 = a3;
                    StringBuilder sb2 = new StringBuilder();
                    PortfolioApp portfolioApp2 = c2;
                    sb2.append("setDataSummaryForTabs - steps=");
                    sb2.append(d3);
                    sb2.append(", activeTime=");
                    sb2.append(i2);
                    sb2.append(", ");
                    sb2.append("calories=");
                    sb2.append(d2);
                    sb2.append(", goalTarget=");
                    sb2.append(i5);
                    sb2.append(", goalTotalTracked=");
                    sb2.append(i4);
                    sb2.append(", ");
                    sb2.append("sleepMinutes=");
                    sb2.append(sleepMinutes);
                    sb2.append(", resting=");
                    sb2.append(intValue);
                    local2.d(str4, sb2.toString());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str6 = y;
                    local3.d(str6, "setDataSummaryForTabs - icActivity=" + a2.y + ", icActiveTime=" + a2.x + ", " + "icCalorie=" + a2.z + ", icSleep=" + a2.C);
                    FlexibleTextView flexibleTextView = a2.y.s;
                    kd4.a((Object) flexibleTextView, "binding.icActivity.ftvTabValue");
                    int i6 = (d3 > 0.0d ? 1 : (d3 == 0.0d ? 0 : -1));
                    flexibleTextView.setText((i6 != 0 || !z3) ? ol2.a.b(Integer.valueOf(td4.a(d3))) : str5);
                    FlexibleTextView flexibleTextView2 = a2.x.s;
                    kd4.a((Object) flexibleTextView2, "binding.icActiveTime.ftvTabValue");
                    flexibleTextView2.setText((i2 != 0 || !z3) ? ol2.a.a(Integer.valueOf(i2)) : str5);
                    FlexibleTextView flexibleTextView3 = a2.z.s;
                    kd4.a((Object) flexibleTextView3, "binding.icCalorie.ftvTabValue");
                    flexibleTextView3.setText((d2 != 0.0d || !z3) ? ol2.a.a(Float.valueOf((float) d2)) : str5);
                    FlexibleTextView flexibleTextView4 = a2.C.s;
                    kd4.a((Object) flexibleTextView4, "binding.icSleep.ftvTabValue");
                    if (sleepMinutes != 0 || !z3) {
                        portfolioApp = portfolioApp2;
                        StringBuilder sb3 = new StringBuilder();
                        pd4 pd4 = pd4.a;
                        Locale locale = Locale.US;
                        kd4.a((Object) locale, "Locale.US");
                        Object[] objArr = {Integer.valueOf(sleepMinutes / 60)};
                        String format = String.format(locale, "%d", Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format);
                        sb3.append(":");
                        pd4 pd42 = pd4.a;
                        Locale locale2 = Locale.US;
                        kd4.a((Object) locale2, "Locale.US");
                        Object[] objArr2 = {Integer.valueOf(sleepMinutes % 60)};
                        String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
                        kd4.a((Object) format2, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format2);
                        str = sb3.toString();
                    } else {
                        portfolioApp = portfolioApp2;
                        str = sm2.a((Context) portfolioApp, (int) R.string.character_dash_time);
                    }
                    flexibleTextView4.setText(str);
                    FlexibleTextView flexibleTextView5 = a2.A.r;
                    kd4.a((Object) flexibleTextView5, "binding.icGoalTracking.ftvTabUnit");
                    if (i5 != 0 || !z3) {
                        pd4 pd43 = pd4.a;
                        String a4 = sm2.a((Context) portfolioApp, (int) R.string.DashboardHybrid_Main_GoalTrackingToday_Label__OfNumber);
                        kd4.a((Object) a4, "LanguageHelper.getString\u2026ingToday_Label__OfNumber)");
                        Object[] objArr3 = {Integer.valueOf(i5)};
                        str2 = String.format(a4, Arrays.copyOf(objArr3, objArr3.length));
                        kd4.a((Object) str2, "java.lang.String.format(format, *args)");
                    } else {
                        str2 = str5;
                    }
                    flexibleTextView5.setText(str2);
                    FlexibleTextView flexibleTextView6 = a2.A.s;
                    kd4.a((Object) flexibleTextView6, "binding.icGoalTracking.ftvTabValue");
                    flexibleTextView6.setText((i4 != 0 || !z3) ? String.valueOf(i4) : str5);
                    FlexibleTextView flexibleTextView7 = a2.B.s;
                    kd4.a((Object) flexibleTextView7, "binding.icHeartRate.ftvTabValue");
                    flexibleTextView7.setText((i6 != 0 || !z3) ? String.valueOf(intValue) : str5);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        kd4.b(date, "date");
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.O;
                kd4.a((Object) flexibleTextView, "tvToday");
                pd4 pd4 = pd4.a;
                String string = PortfolioApp.W.c().getString(R.string.DashboardDiana_Main_StepsToday_Title__TodayMonthDate);
                kd4.a((Object) string, "PortfolioApp.instance.ge\u2026ay_Title__TodayMonthDate)");
                Object[] objArr = {nl2.a(date)};
                String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        if ((str.length() > 0) && kd4.a((Object) str, (Object) "ASK_TO_CANCEL_WORKOUT") && getActivity() != null) {
            if (i2 == R.id.tv_cancel) {
                FLogger.INSTANCE.getLocal().e(y, "don't quit");
                u73 u73 = this.r;
                if (u73 != null) {
                    u73.a(PortfolioApp.W.c().e(), false);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else if (i2 == R.id.tv_ok) {
                FLogger.INSTANCE.getLocal().e(y, "quit and sync");
                u73 u732 = this.r;
                if (u732 != null) {
                    u732.a(PortfolioApp.W.c().e(), true);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        hu3 hu3 = this.u;
        if (hu3 != null) {
            hu3.a(str, str2);
        }
    }

    @DexIgnore
    public final void a(ProgressBar progressBar, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "animateSyncProgressEnd " + progressBar);
        if (i2 == 0) {
            progressBar.setVisibility(0);
        } else if (i2 == 1) {
            p(2);
        } else if (i2 == 2) {
            TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, progressBar.getY(), progressBar.getY() - ((float) progressBar.getHeight()));
            translateAnimation.setDuration((long) 300);
            translateAnimation.setAnimationListener(new b(this, progressBar));
            progressBar.startAnimation(translateAnimation);
            tr3<yc2> tr3 = this.q;
            if (tr3 != null) {
                yc2 a2 = tr3.a();
                if (a2 != null) {
                    CustomSwipeRefreshLayout customSwipeRefreshLayout = a2.M;
                    if (customSwipeRefreshLayout != null) {
                        customSwipeRefreshLayout.c();
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(boolean z2, boolean z3, boolean z4) {
        tr3<yc2> tr3 = this.q;
        if (tr3 != null) {
            yc2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (z2 || !z4) {
                ConstraintLayout constraintLayout = a2.u;
                kd4.a((Object) constraintLayout, "binding.clVisualization");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.s;
                kd4.a((Object) constraintLayout2, "binding.clNoDevice");
                constraintLayout2.setVisibility(8);
                a2.M.setByPass(!z3);
                if (!z4 && !z3) {
                    ImageView imageView = a2.D;
                    kd4.a((Object) imageView, "binding.ivNoWatchFound");
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            ConstraintLayout constraintLayout3 = a2.u;
            kd4.a((Object) constraintLayout3, "binding.clVisualization");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.s;
            kd4.a((Object) constraintLayout4, "binding.clNoDevice");
            constraintLayout4.setVisibility(0);
            a2.M.setByPass(true);
            ImageView imageView2 = a2.D;
            kd4.a((Object) imageView2, "binding.ivNoWatchFound");
            imageView2.setVisibility(8);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
