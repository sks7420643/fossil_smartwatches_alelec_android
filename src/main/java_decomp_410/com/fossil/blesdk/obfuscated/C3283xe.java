package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xe */
public class C3283xe extends com.fossil.blesdk.obfuscated.C1479bf {

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3214we f10911d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C3214we f10912e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xe$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xe$a */
    public class C3284a extends com.fossil.blesdk.obfuscated.C2873se {
        @DexIgnore
        public C3284a(android.content.Context context) {
            super(context);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo3016a(android.view.View view, androidx.recyclerview.widget.RecyclerView.State state, androidx.recyclerview.widget.RecyclerView.C0251v.C0252a aVar) {
            com.fossil.blesdk.obfuscated.C3283xe xeVar = com.fossil.blesdk.obfuscated.C3283xe.this;
            int[] a = xeVar.mo9164a(xeVar.f3721a.getLayoutManager(), view);
            int i = a[0];
            int i2 = a[1];
            int d = mo15951d(java.lang.Math.max(java.lang.Math.abs(i), java.lang.Math.abs(i2)));
            if (d > 0) {
                aVar.mo3029a(i, i2, d, this.f9259j);
            }
        }

        @DexIgnore
        /* renamed from: e */
        public int mo15952e(int i) {
            return java.lang.Math.min(100, super.mo15952e(i));
        }

        @DexIgnore
        /* renamed from: a */
        public float mo9171a(android.util.DisplayMetrics displayMetrics) {
            return 100.0f / ((float) displayMetrics.densityDpi);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int[] mo9164a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, android.view.View view) {
        int[] iArr = new int[2];
        if (mVar.mo2426a()) {
            iArr[0] = mo17690a(mVar, view, mo17693d(mVar));
        } else {
            iArr[0] = 0;
        }
        if (mVar.mo2437b()) {
            iArr[1] = mo17690a(mVar, view, mo17694e(mVar));
        } else {
            iArr[1] = 0;
        }
        return iArr;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2873se mo9165b(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        if (!(mVar instanceof androidx.recyclerview.widget.RecyclerView.C0251v.C0253b)) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C3283xe.C3284a(this.f3721a.getContext());
    }

    @DexIgnore
    /* renamed from: c */
    public android.view.View mo9169c(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        if (mVar.mo2437b()) {
            return mo17691a(mVar, mo17694e(mVar));
        }
        if (mVar.mo2426a()) {
            return mo17691a(mVar, mo17693d(mVar));
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.C3214we mo17693d(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        com.fossil.blesdk.obfuscated.C3214we weVar = this.f10912e;
        if (weVar == null || weVar.f10613a != mVar) {
            this.f10912e = com.fossil.blesdk.obfuscated.C3214we.m15739a(mVar);
        }
        return this.f10912e;
    }

    @DexIgnore
    /* renamed from: e */
    public final com.fossil.blesdk.obfuscated.C3214we mo17694e(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        com.fossil.blesdk.obfuscated.C3214we weVar = this.f10911d;
        if (weVar == null || weVar.f10613a != mVar) {
            this.f10911d = com.fossil.blesdk.obfuscated.C3214we.m15741b(mVar);
        }
        return this.f10911d;
    }

    @DexIgnore
    /* renamed from: b */
    public final android.view.View mo17692b(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int e = mVar.mo2941e();
        android.view.View view = null;
        if (e == 0) {
            return null;
        }
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < e; i2++) {
            android.view.View d = mVar.mo2936d(i2);
            int d2 = weVar.mo17437d(d);
            if (d2 < i) {
                view = d;
                i = d2;
            }
        }
        return view;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo9160a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, int i, int i2) {
        int j = mVar.mo2957j();
        if (j == 0) {
            return -1;
        }
        android.view.View view = null;
        if (mVar.mo2437b()) {
            view = mo17692b(mVar, mo17694e(mVar));
        } else if (mVar.mo2426a()) {
            view = mo17692b(mVar, mo17693d(mVar));
        }
        if (view == null) {
            return -1;
        }
        int l = mVar.mo2962l(view);
        if (l == -1) {
            return -1;
        }
        boolean z = false;
        boolean z2 = !mVar.mo2426a() ? i2 > 0 : i > 0;
        if (mVar instanceof androidx.recyclerview.widget.RecyclerView.C0251v.C0253b) {
            android.graphics.PointF a = ((androidx.recyclerview.widget.RecyclerView.C0251v.C0253b) mVar).mo2410a(j - 1);
            if (a != null && (a.x < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || a.y < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                z = true;
            }
        }
        return z ? z2 ? l - 1 : l : z2 ? l + 1 : l;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo17690a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, android.view.View view, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int i;
        int d = weVar.mo17437d(view) + (weVar.mo17433b(view) / 2);
        if (mVar.mo2948f()) {
            i = weVar.mo17440f() + (weVar.mo17442g() / 2);
        } else {
            i = weVar.mo17429a() / 2;
        }
        return d - i;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.View mo17691a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we weVar) {
        int i;
        int e = mVar.mo2941e();
        android.view.View view = null;
        if (e == 0) {
            return null;
        }
        if (mVar.mo2948f()) {
            i = weVar.mo17440f() + (weVar.mo17442g() / 2);
        } else {
            i = weVar.mo17429a() / 2;
        }
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 0; i3 < e; i3++) {
            android.view.View d = mVar.mo2936d(i3);
            int abs = java.lang.Math.abs((weVar.mo17437d(d) + (weVar.mo17433b(d) / 2)) - i);
            if (abs < i2) {
                view = d;
                i2 = abs;
            }
        }
        return view;
    }
}
