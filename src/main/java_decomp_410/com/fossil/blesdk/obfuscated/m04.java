package com.fossil.blesdk.obfuscated;

import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m04 {
    @DexIgnore
    public String a;
    @DexIgnore
    public JSONArray b;
    @DexIgnore
    public JSONObject c; // = null;

    @DexIgnore
    public m04() {
    }

    @DexIgnore
    public m04(String str, String[] strArr, Properties properties) {
        JSONObject jSONObject;
        this.a = str;
        if (properties != null) {
            jSONObject = new JSONObject(properties);
        } else if (strArr != null) {
            this.b = new JSONArray();
            for (String put : strArr) {
                this.b.put(put);
            }
            return;
        } else {
            jSONObject = new JSONObject();
        }
        this.c = jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof m04) {
            return toString().equals(((m04) obj).toString());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return toString().hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.a);
        sb.append(",");
        JSONArray jSONArray = this.b;
        if (jSONArray != null) {
            sb.append(jSONArray.toString());
        }
        JSONObject jSONObject = this.c;
        if (jSONObject != null) {
            sb.append(jSONObject.toString());
        }
        return sb.toString();
    }
}
