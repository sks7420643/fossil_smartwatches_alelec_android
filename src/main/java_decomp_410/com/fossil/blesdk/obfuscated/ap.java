package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ap<T> implements so<T> {
    @DexIgnore
    public /* final */ Uri e;
    @DexIgnore
    public /* final */ ContentResolver f;
    @DexIgnore
    public T g;

    @DexIgnore
    public ap(ContentResolver contentResolver, Uri uri) {
        this.f = contentResolver;
        this.e = uri;
    }

    @DexIgnore
    public abstract T a(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;

    @DexIgnore
    public final void a(Priority priority, so.a<? super T> aVar) {
        try {
            this.g = a(this.e, this.f);
            aVar.a(this.g);
        } catch (FileNotFoundException e2) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e2);
            }
            aVar.a((Exception) e2);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    public DataSource b() {
        return DataSource.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public void a() {
        T t = this.g;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
