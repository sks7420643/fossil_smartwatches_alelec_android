package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface yc4<P1, P2, R> extends ka4<R> {
    @DexIgnore
    R invoke(P1 p1, P2 p2);
}
