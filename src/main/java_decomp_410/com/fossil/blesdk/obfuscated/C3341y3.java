package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y3 */
public final class C3341y3 {
    @DexIgnore
    public static /* final */ int[] CardView; // = {16843071, 16843072, com.fossil.wearables.fossil.R.attr.cardBackgroundColor, com.fossil.wearables.fossil.R.attr.cardCornerRadius, com.fossil.wearables.fossil.R.attr.cardElevation, com.fossil.wearables.fossil.R.attr.cardMaxElevation, com.fossil.wearables.fossil.R.attr.cardPreventCornerOverlap, com.fossil.wearables.fossil.R.attr.cardUseCompatPadding, com.fossil.wearables.fossil.R.attr.contentPadding, com.fossil.wearables.fossil.R.attr.contentPaddingBottom, com.fossil.wearables.fossil.R.attr.contentPaddingLeft, com.fossil.wearables.fossil.R.attr.contentPaddingRight, com.fossil.wearables.fossil.R.attr.contentPaddingTop};
    @DexIgnore
    public static /* final */ int CardView_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int CardView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int CardView_cardBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int CardView_cardCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int CardView_cardElevation; // = 4;
    @DexIgnore
    public static /* final */ int CardView_cardMaxElevation; // = 5;
    @DexIgnore
    public static /* final */ int CardView_cardPreventCornerOverlap; // = 6;
    @DexIgnore
    public static /* final */ int CardView_cardUseCompatPadding; // = 7;
    @DexIgnore
    public static /* final */ int CardView_contentPadding; // = 8;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingBottom; // = 9;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingLeft; // = 10;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingRight; // = 11;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingTop; // = 12;
}
