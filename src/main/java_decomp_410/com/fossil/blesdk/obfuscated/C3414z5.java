package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z5 */
public final class C3414z5 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z5$a")
    /* renamed from: com.fossil.blesdk.obfuscated.z5$a */
    public static class C3415a {

        @DexIgnore
        /* renamed from: a */
        public static java.lang.reflect.Method f11501a;

        @DexIgnore
        /* renamed from: b */
        public static boolean f11502b;

        @DexIgnore
        /* renamed from: c */
        public static java.lang.reflect.Method f11503c;

        @DexIgnore
        /* renamed from: d */
        public static boolean f11504d;

        @DexIgnore
        /* renamed from: a */
        public static android.os.IBinder m17267a(android.os.Bundle bundle, java.lang.String str) {
            if (!f11502b) {
                try {
                    f11501a = android.os.Bundle.class.getMethod("getIBinder", new java.lang.Class[]{java.lang.String.class});
                    f11501a.setAccessible(true);
                } catch (java.lang.NoSuchMethodException e) {
                    android.util.Log.i("BundleCompatBaseImpl", "Failed to retrieve getIBinder method", e);
                }
                f11502b = true;
            }
            java.lang.reflect.Method method = f11501a;
            if (method != null) {
                try {
                    return (android.os.IBinder) method.invoke(bundle, new java.lang.Object[]{str});
                } catch (java.lang.IllegalAccessException | java.lang.IllegalArgumentException | java.lang.reflect.InvocationTargetException e2) {
                    android.util.Log.i("BundleCompatBaseImpl", "Failed to invoke getIBinder via reflection", e2);
                    f11501a = null;
                }
            }
            return null;
        }

        @DexIgnore
        /* renamed from: a */
        public static void m17268a(android.os.Bundle bundle, java.lang.String str, android.os.IBinder iBinder) {
            if (!f11504d) {
                try {
                    f11503c = android.os.Bundle.class.getMethod("putIBinder", new java.lang.Class[]{java.lang.String.class, android.os.IBinder.class});
                    f11503c.setAccessible(true);
                } catch (java.lang.NoSuchMethodException e) {
                    android.util.Log.i("BundleCompatBaseImpl", "Failed to retrieve putIBinder method", e);
                }
                f11504d = true;
            }
            java.lang.reflect.Method method = f11503c;
            if (method != null) {
                try {
                    method.invoke(bundle, new java.lang.Object[]{str, iBinder});
                } catch (java.lang.IllegalAccessException | java.lang.IllegalArgumentException | java.lang.reflect.InvocationTargetException e2) {
                    android.util.Log.i("BundleCompatBaseImpl", "Failed to invoke putIBinder via reflection", e2);
                    f11503c = null;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.IBinder m17265a(android.os.Bundle bundle, java.lang.String str) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return bundle.getBinder(str);
        }
        return com.fossil.blesdk.obfuscated.C3414z5.C3415a.m17267a(bundle, str);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17266a(android.os.Bundle bundle, java.lang.String str, android.os.IBinder iBinder) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            bundle.putBinder(str, iBinder);
        } else {
            com.fossil.blesdk.obfuscated.C3414z5.C3415a.m17268a(bundle, str, iBinder);
        }
    }
}
