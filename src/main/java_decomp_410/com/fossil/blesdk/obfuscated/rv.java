package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rv extends lv<rv> {
    @DexIgnore
    public static rv E;
    @DexIgnore
    public static rv F;

    @DexIgnore
    public static rv b(pp ppVar) {
        return (rv) new rv().a(ppVar);
    }

    @DexIgnore
    public static rv c(boolean z) {
        if (z) {
            if (E == null) {
                E = (rv) ((rv) new rv().a(true)).a();
            }
            return E;
        }
        if (F == null) {
            F = (rv) ((rv) new rv().a(false)).a();
        }
        return F;
    }

    @DexIgnore
    public static rv b(jo joVar) {
        return (rv) new rv().a(joVar);
    }

    @DexIgnore
    public static rv b(Class<?> cls) {
        return (rv) new rv().a(cls);
    }
}
