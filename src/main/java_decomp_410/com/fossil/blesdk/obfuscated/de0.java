package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0.d;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ij0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class de0<O extends d> {
    @DexIgnore
    public /* final */ a<?, O> a;
    @DexIgnore
    public /* final */ g<?> b;
    @DexIgnore
    public /* final */ String c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<T extends f, O> extends e<T, O> {
        @DexIgnore
        public abstract T a(Context context, Looper looper, kj0 kj0, O o, ge0.b bVar, ge0.c cVar);
    }

    @DexIgnore
    public interface b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<C extends b> {
    }

    @DexIgnore
    public interface d {

        @DexIgnore
        public interface a extends c, C0010d {
            @DexIgnore
            Account h();
        }

        @DexIgnore
        public interface b extends c {
            @DexIgnore
            GoogleSignInAccount a();
        }

        @DexIgnore
        public interface c extends d {
        }

        @DexIgnore
        /* renamed from: com.fossil.blesdk.obfuscated.de0$d$d  reason: collision with other inner class name */
        public interface C0010d extends d {
        }

        @DexIgnore
        public interface e extends c, C0010d {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<T extends b, O> {
        @DexIgnore
        public int a() {
            return Integer.MAX_VALUE;
        }

        @DexIgnore
        public List<Scope> a(O o) {
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public interface f extends b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ij0.c cVar);

        @DexIgnore
        void a(ij0.e eVar);

        @DexIgnore
        void a(tj0 tj0, Set<Scope> set);

        @DexIgnore
        void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

        @DexIgnore
        boolean c();

        @DexIgnore
        boolean d();

        @DexIgnore
        boolean e();

        @DexIgnore
        String f();

        @DexIgnore
        boolean h();

        @DexIgnore
        int i();

        @DexIgnore
        wd0[] j();

        @DexIgnore
        Intent k();

        @DexIgnore
        boolean l();

        @DexIgnore
        IBinder m();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<C extends f> extends c<C> {
    }

    @DexIgnore
    public interface h<T extends IInterface> extends b {
        @DexIgnore
        T a(IBinder iBinder);

        @DexIgnore
        void a(int i, T t);

        @DexIgnore
        String n();

        @DexIgnore
        String o();
    }

    @DexIgnore
    public <C extends f> de0(String str, a<C, O> aVar, g<C> gVar) {
        bk0.a(aVar, (Object) "Cannot construct an Api with a null ClientBuilder");
        bk0.a(gVar, (Object) "Cannot construct an Api with a null ClientKey");
        this.c = str;
        this.a = aVar;
        this.b = gVar;
    }

    @DexIgnore
    public final c<?> a() {
        g<?> gVar = this.b;
        if (gVar != null) {
            return gVar;
        }
        throw new IllegalStateException("This API was constructed with null client keys. This should not be possible.");
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final e<?, O> c() {
        return this.a;
    }

    @DexIgnore
    public final a<?, O> d() {
        bk0.b(this.a != null, "This API was constructed with a SimpleClientBuilder. Use getSimpleClientBuilder");
        return this.a;
    }
}
