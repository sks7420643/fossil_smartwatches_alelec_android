package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kv2 implements Factory<d23> {
    @DexIgnore
    public static d23 a(gv2 gv2) {
        d23 d = gv2.d();
        n44.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
