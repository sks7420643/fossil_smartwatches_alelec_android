package com.fossil.blesdk.obfuscated;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class in0 extends gn0 {
    @DexIgnore
    public static /* final */ WeakReference<byte[]> g; // = new WeakReference<>((Object) null);
    @DexIgnore
    public WeakReference<byte[]> f; // = g;

    @DexIgnore
    public in0(byte[] bArr) {
        super(bArr);
    }

    @DexIgnore
    public final byte[] o() {
        byte[] bArr;
        synchronized (this) {
            bArr = (byte[]) this.f.get();
            if (bArr == null) {
                bArr = p();
                this.f = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    @DexIgnore
    public abstract byte[] p();
}
