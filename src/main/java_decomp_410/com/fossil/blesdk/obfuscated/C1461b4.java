package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.b4 */
public class C1461b4 implements com.fossil.blesdk.obfuscated.C1595d4 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.graphics.RectF f3664a; // = new android.graphics.RectF();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.b4$a")
    /* renamed from: com.fossil.blesdk.obfuscated.b4$a */
    public class C1462a implements com.fossil.blesdk.obfuscated.C1768f4.C1769a {
        @DexIgnore
        public C1462a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9043a(android.graphics.Canvas canvas, android.graphics.RectF rectF, float f, android.graphics.Paint paint) {
            android.graphics.Canvas canvas2 = canvas;
            android.graphics.RectF rectF2 = rectF;
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = (rectF.height() - f2) - 1.0f;
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                com.fossil.blesdk.obfuscated.C1461b4.this.f3664a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas2.translate(rectF2.left + f3, rectF2.top + f3);
                android.graphics.Paint paint2 = paint;
                canvas.drawArc(com.fossil.blesdk.obfuscated.C1461b4.this.f3664a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(com.fossil.blesdk.obfuscated.C1461b4.this.f3664a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(height, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(com.fossil.blesdk.obfuscated.C1461b4.this.f3664a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(com.fossil.blesdk.obfuscated.C1461b4.this.f3664a, 180.0f, 90.0f, true, paint2);
                canvas2.restoreToCount(save);
                float f5 = rectF2.top;
                canvas.drawRect((rectF2.left + f3) - 1.0f, f5, (rectF2.right - f3) + 1.0f, f5 + f3, paint2);
                float f6 = rectF2.bottom;
                android.graphics.Canvas canvas3 = canvas;
                canvas3.drawRect((rectF2.left + f3) - 1.0f, f6 - f3, (rectF2.right - f3) + 1.0f, f6, paint2);
            }
            canvas.drawRect(rectF2.left, rectF2.top + f, rectF2.right, rectF2.bottom - f, paint);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8511a() {
        com.fossil.blesdk.obfuscated.C1768f4.f5010r = new com.fossil.blesdk.obfuscated.C1461b4.C1462a();
    }

    @DexIgnore
    /* renamed from: b */
    public float mo8515b(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10718c();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8517c(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8518c(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        mo9042j(c4Var).mo10715b(f);
        mo8521f(c4Var);
    }

    @DexIgnore
    /* renamed from: d */
    public float mo8519d(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10720d();
    }

    @DexIgnore
    /* renamed from: e */
    public android.content.res.ColorStateList mo8520e(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10714b();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo8521f(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        android.graphics.Rect rect = new android.graphics.Rect();
        mo9042j(c4Var).mo10717b(rect);
        c4Var.mo1209a((int) java.lang.Math.ceil((double) mo8523h(c4Var)), (int) java.lang.Math.ceil((double) mo8522g(c4Var)));
        c4Var.mo1210a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    /* renamed from: g */
    public float mo8522g(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10723e();
    }

    @DexIgnore
    /* renamed from: h */
    public float mo8523h(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10724f();
    }

    @DexIgnore
    /* renamed from: i */
    public void mo8524i(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        mo9042j(c4Var).mo10713a(c4Var.mo1212a());
        mo8521f(c4Var);
    }

    @DexIgnore
    /* renamed from: j */
    public final com.fossil.blesdk.obfuscated.C1768f4 mo9042j(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return (com.fossil.blesdk.obfuscated.C1768f4) c4Var.mo1214c();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8513a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.Context context, android.content.res.ColorStateList colorStateList, float f, float f2, float f3) {
        com.fossil.blesdk.obfuscated.C1768f4 a = mo9041a(context, colorStateList, f, f2, f3);
        a.mo10713a(c4Var.mo1212a());
        c4Var.mo1211a(a);
        mo8521f(c4Var);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8516b(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        mo9042j(c4Var).mo10719c(f);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1768f4 mo9041a(android.content.Context context, android.content.res.ColorStateList colorStateList, float f, float f2, float f3) {
        com.fossil.blesdk.obfuscated.C1768f4 f4Var = new com.fossil.blesdk.obfuscated.C1768f4(context.getResources(), colorStateList, f, f2, f3);
        return f4Var;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8514a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.res.ColorStateList colorStateList) {
        mo9042j(c4Var).mo10716b(colorStateList);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8512a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        mo9042j(c4Var).mo10708a(f);
        mo8521f(c4Var);
    }

    @DexIgnore
    /* renamed from: a */
    public float mo8510a(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo9042j(c4Var).mo10725g();
    }
}
