package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import androidx.work.impl.background.systemalarm.ConstraintProxy;
import com.fossil.blesdk.obfuscated.ak;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yj {
    @DexIgnore
    public static /* final */ String e; // = dj.a("ConstraintsCmdHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ak c;
    @DexIgnore
    public /* final */ ik d; // = new ik(this.a, this.c.d(), (hk) null);

    @DexIgnore
    public yj(Context context, int i, ak akVar) {
        this.a = context;
        this.b = i;
        this.c = akVar;
    }

    @DexIgnore
    public void a() {
        List<hl> a2 = this.c.e().g().d().a();
        ConstraintProxy.a(this.a, a2);
        this.d.c(a2);
        ArrayList<hl> arrayList = new ArrayList<>(a2.size());
        long currentTimeMillis = System.currentTimeMillis();
        for (hl next : a2) {
            String str = next.a;
            if (currentTimeMillis >= next.a() && (!next.b() || this.d.a(str))) {
                arrayList.add(next);
            }
        }
        for (hl hlVar : arrayList) {
            String str2 = hlVar.a;
            Intent a3 = xj.a(this.a, str2);
            dj.a().a(e, String.format("Creating a delay_met command for workSpec with id (%s)", new Object[]{str2}), new Throwable[0]);
            ak akVar = this.c;
            akVar.a((Runnable) new ak.b(akVar, a3, this.b));
        }
        this.d.a();
    }
}
