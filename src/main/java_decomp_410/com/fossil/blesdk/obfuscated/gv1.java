package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.Reader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gv1 {
    @DexIgnore
    public abstract Reader a() throws IOException;

    @DexIgnore
    public <T> T a(mv1<T> mv1) throws IOException {
        st1.a(mv1);
        jv1 y = jv1.y();
        try {
            Reader a = a();
            y.a(a);
            T a2 = hv1.a(a, mv1);
            y.close();
            return a2;
        } catch (Throwable th) {
            y.close();
            throw th;
        }
    }
}
