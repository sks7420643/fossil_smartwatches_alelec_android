package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mi4 {
    @DexIgnore
    public static /* final */ dk4 a; // = new dk4("SEALED");
    @DexIgnore
    public static /* final */ rh4 b; // = new rh4(false);
    @DexIgnore
    public static /* final */ rh4 c; // = new rh4(true);

    @DexIgnore
    public static final Object a(Object obj) {
        return obj instanceof ai4 ? new bi4((ai4) obj) : obj;
    }

    @DexIgnore
    public static final Object b(Object obj) {
        bi4 bi4 = (bi4) (!(obj instanceof bi4) ? null : obj);
        if (bi4 == null) {
            return obj;
        }
        ai4 ai4 = bi4.a;
        return ai4 != null ? ai4 : obj;
    }
}
