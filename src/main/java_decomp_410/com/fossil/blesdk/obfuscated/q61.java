package com.fossil.blesdk.obfuscated;

import android.database.ContentObserver;
import android.os.Handler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q61 extends ContentObserver {
    @DexIgnore
    public /* final */ /* synthetic */ o61 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q61(o61 o61, Handler handler) {
        super((Handler) null);
        this.a = o61;
    }

    @DexIgnore
    public final void onChange(boolean z) {
        this.a.b();
    }
}
