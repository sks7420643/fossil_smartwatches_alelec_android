package com.fossil.blesdk.obfuscated;

import android.os.Process;
import android.os.StrictMode;
import android.util.Log;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dr implements ExecutorService {
    @DexIgnore
    public static /* final */ long f; // = TimeUnit.SECONDS.toMillis(10);
    @DexIgnore
    public static volatile int g;
    @DexIgnore
    public /* final */ ExecutorService e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ b b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public int d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$a$a  reason: collision with other inner class name */
        public class C0011a extends Thread {
            @DexIgnore
            public C0011a(Runnable runnable, String str) {
                super(runnable, str);
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(9);
                if (a.this.c) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    a.this.b.a(th);
                }
            }
        }

        @DexIgnore
        public a(String str, b bVar, boolean z) {
            this.a = str;
            this.b = bVar;
            this.c = z;
        }

        @DexIgnore
        public synchronized Thread newThread(Runnable runnable) {
            C0011a aVar;
            aVar = new C0011a(runnable, "glide-" + this.a + "-thread-" + this.d);
            this.d = this.d + 1;
            return aVar;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        public static final b a = new C0012b();
        @DexIgnore
        public static final b b = a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements b {
            @DexIgnore
            public void a(Throwable th) {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dr$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.dr$b$b  reason: collision with other inner class name */
        public class C0012b implements b {
            @DexIgnore
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class c implements b {
            @DexIgnore
            public void a(Throwable th) {
                if (th != null) {
                    throw new RuntimeException("Request threw uncaught throwable", th);
                }
            }
        }

        /*
        static {
            new a();
            new c();
        }
        */

        @DexIgnore
        void a(Throwable th);
    }

    @DexIgnore
    public dr(ExecutorService executorService) {
        this.e = executorService;
    }

    @DexIgnore
    public static dr a(int i, String str, b bVar) {
        return new dr(new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new a(str, bVar, true)));
    }

    @DexIgnore
    public static dr b(int i, String str, b bVar) {
        return new dr(new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new a(str, bVar, false)));
    }

    @DexIgnore
    public static dr c() {
        return a(1, "disk-cache", b.b);
    }

    @DexIgnore
    public static dr d() {
        return b(a(), "source", b.b);
    }

    @DexIgnore
    public static dr e() {
        return new dr(new ThreadPoolExecutor(0, Integer.MAX_VALUE, f, TimeUnit.MILLISECONDS, new SynchronousQueue(), new a("source-unlimited", b.b, false)));
    }

    @DexIgnore
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.e.awaitTermination(j, timeUnit);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.e.execute(runnable);
    }

    @DexIgnore
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.e.invokeAll(collection);
    }

    @DexIgnore
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return this.e.invokeAny(collection);
    }

    @DexIgnore
    public boolean isShutdown() {
        return this.e.isShutdown();
    }

    @DexIgnore
    public boolean isTerminated() {
        return this.e.isTerminated();
    }

    @DexIgnore
    public void shutdown() {
        this.e.shutdown();
    }

    @DexIgnore
    public List<Runnable> shutdownNow() {
        return this.e.shutdownNow();
    }

    @DexIgnore
    public Future<?> submit(Runnable runnable) {
        return this.e.submit(runnable);
    }

    @DexIgnore
    public String toString() {
        return this.e.toString();
    }

    @DexIgnore
    public static dr a(int i, b bVar) {
        return new dr(new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new a("animation", bVar, true)));
    }

    @DexIgnore
    public static dr b() {
        return a(a() >= 4 ? 2 : 1, b.b);
    }

    @DexIgnore
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.e.invokeAll(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.e.invokeAny(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.e.submit(runnable, t);
    }

    @DexIgnore
    public static int a() {
        if (g == 0) {
            g = Math.min(4, er.a());
        }
        return g;
    }

    @DexIgnore
    public <T> Future<T> submit(Callable<T> callable) {
        return this.e.submit(callable);
    }
}
