package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.transition.Transition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ph {
    @DexIgnore
    public /* final */ Map<String, Object> a; // = new HashMap();
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ ArrayList<Transition> c; // = new ArrayList<>();

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof ph)) {
            return false;
        }
        ph phVar = (ph) obj;
        return this.b == phVar.b && this.a.equals(phVar.a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        String str = (("TransitionValues@" + Integer.toHexString(hashCode()) + ":\n") + "    view = " + this.b + "\n") + "    values:";
        for (String next : this.a.keySet()) {
            str = str + "    " + next + ": " + this.a.get(next) + "\n";
        }
        return str;
    }
}
