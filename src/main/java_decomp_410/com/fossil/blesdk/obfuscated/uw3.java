package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.facebook.appevents.AppEventsConstants;
import com.fossil.blesdk.obfuscated.cv3;
import com.fossil.blesdk.obfuscated.ev3;
import com.fossil.blesdk.obfuscated.hv3;
import com.fossil.blesdk.obfuscated.jv3;
import com.fossil.blesdk.obfuscated.pw3;
import com.misfit.frameworks.common.enums.Action;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.http.RequestException;
import com.squareup.okhttp.internal.http.RouteException;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.CookieHandler;
import java.net.ProtocolException;
import java.net.Proxy;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uw3 {
    @DexIgnore
    public static /* final */ kv3 u; // = new a();
    @DexIgnore
    public /* final */ gv3 a;
    @DexIgnore
    public wu3 b;
    @DexIgnore
    public pu3 c;
    @DexIgnore
    public bx3 d;
    @DexIgnore
    public lv3 e;
    @DexIgnore
    public /* final */ jv3 f;
    @DexIgnore
    public dx3 g;
    @DexIgnore
    public long h; // = -1;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ hv3 k;
    @DexIgnore
    public hv3 l;
    @DexIgnore
    public jv3 m;
    @DexIgnore
    public jv3 n;
    @DexIgnore
    public xo4 o;
    @DexIgnore
    public ko4 p;
    @DexIgnore
    public /* final */ boolean q;
    @DexIgnore
    public /* final */ boolean r;
    @DexIgnore
    public ow3 s;
    @DexIgnore
    public pw3 t;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends kv3 {
        @DexIgnore
        public fv3 A() {
            return null;
        }

        @DexIgnore
        public lo4 B() {
            return new jo4();
        }

        @DexIgnore
        public long z() {
            return 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ev3.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;

        @DexIgnore
        public c(int i, hv3 hv3) {
            this.a = i;
        }

        @DexIgnore
        public wu3 a() {
            return uw3.this.b;
        }

        @DexIgnore
        public jv3 a(hv3 hv3) throws IOException {
            this.b++;
            if (this.a > 0) {
                ev3 ev3 = uw3.this.a.F().get(this.a - 1);
                pu3 a2 = a().e().a();
                if (!hv3.d().f().equals(a2.j()) || hv3.d().h() != a2.k()) {
                    throw new IllegalStateException("network interceptor " + ev3 + " must retain the same host and port");
                } else if (this.b > 1) {
                    throw new IllegalStateException("network interceptor " + ev3 + " must call proceed() exactly once");
                }
            }
            if (this.a < uw3.this.a.F().size()) {
                c cVar = new c(this.a + 1, hv3);
                ev3 ev32 = uw3.this.a.F().get(this.a);
                jv3 a3 = ev32.a(cVar);
                if (cVar.b == 1) {
                    return a3;
                }
                throw new IllegalStateException("network interceptor " + ev32 + " must call proceed() exactly once");
            }
            uw3.this.g.a(hv3);
            hv3 unused = uw3.this.l = hv3;
            if (uw3.this.j() && hv3.a() != null) {
                ko4 a4 = so4.a(uw3.this.g.a(hv3, hv3.a().contentLength()));
                hv3.a().writeTo(a4);
                a4.close();
            }
            jv3 c2 = uw3.this.k();
            int e = c2.e();
            if ((e != 204 && e != 205) || c2.a().z() <= 0) {
                return c2;
            }
            throw new ProtocolException("HTTP " + e + " had non-zero Content-Length: " + c2.a().z());
        }
    }

    @DexIgnore
    public uw3(gv3 gv3, hv3 hv3, boolean z, boolean z2, boolean z3, wu3 wu3, bx3 bx3, ax3 ax3, jv3 jv3) {
        this.a = gv3;
        this.k = hv3;
        this.j = z;
        this.q = z2;
        this.r = z3;
        this.b = wu3;
        this.d = bx3;
        this.o = ax3;
        this.f = jv3;
        if (wu3 != null) {
            pv3.b.b(wu3, this);
            this.e = wu3.e();
            return;
        }
        this.e = null;
    }

    @DexIgnore
    public hv3 d() throws IOException {
        Proxy proxy;
        if (this.n != null) {
            if (h() != null) {
                proxy = h().b();
            } else {
                proxy = this.a.w();
            }
            int e2 = this.n.e();
            if (e2 != 307 && e2 != 308) {
                if (e2 != 401) {
                    if (e2 != 407) {
                        switch (e2) {
                            case 300:
                            case Action.Presenter.NEXT /*301*/:
                            case Action.Presenter.PREVIOUS /*302*/:
                            case Action.Presenter.BLACKOUT /*303*/:
                                break;
                            default:
                                return null;
                        }
                    } else if (proxy.type() != Proxy.Type.HTTP) {
                        throw new ProtocolException("Received HTTP_PROXY_AUTH (407) code while not using proxy");
                    }
                }
                return xw3.a(this.a.b(), this.n, proxy);
            } else if (!this.k.f().equals("GET") && !this.k.f().equals("HEAD")) {
                return null;
            }
            if (!this.a.j()) {
                return null;
            }
            String a2 = this.n.a("Location");
            if (a2 == null) {
                return null;
            }
            dv3 a3 = this.k.d().a(a2);
            if (a3 == null) {
                return null;
            }
            if (!a3.j().equals(this.k.d().j()) && !this.a.k()) {
                return null;
            }
            hv3.b g2 = this.k.g();
            if (vw3.b(this.k.f())) {
                g2.a("GET", (iv3) null);
                g2.a("Transfer-Encoding");
                g2.a("Content-Length");
                g2.a(GraphRequest.CONTENT_TYPE_HEADER);
            }
            if (!a(a3)) {
                g2.a("Authorization");
            }
            g2.a(a3);
            return g2.a();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public wu3 e() {
        return this.b;
    }

    @DexIgnore
    public hv3 f() {
        return this.k;
    }

    @DexIgnore
    public jv3 g() {
        jv3 jv3 = this.n;
        if (jv3 != null) {
            return jv3;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public lv3 h() {
        return this.e;
    }

    @DexIgnore
    public final void i() throws IOException {
        qv3 a2 = pv3.b.a(this.a);
        if (a2 != null) {
            if (pw3.a(this.n, this.l)) {
                this.s = a2.a(c(this.n));
            } else if (vw3.a(this.l.f())) {
                try {
                    a2.b(this.l);
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexIgnore
    public boolean j() {
        return vw3.b(this.k.f());
    }

    @DexIgnore
    public final jv3 k() throws IOException {
        this.g.a();
        jv3.b c2 = this.g.c();
        c2.a(this.l);
        c2.a(this.b.b());
        c2.b(xw3.c, Long.toString(this.h));
        c2.b(xw3.d, Long.toString(System.currentTimeMillis()));
        jv3 a2 = c2.a();
        if (!this.r) {
            jv3.b j2 = a2.j();
            j2.a(this.g.a(a2));
            a2 = j2.a();
        }
        pv3.b.a(this.b, a2.k());
        return a2;
    }

    @DexIgnore
    public void l() throws IOException {
        jv3 jv3;
        if (this.n == null) {
            if (this.l == null && this.m == null) {
                throw new IllegalStateException("call sendRequest() first!");
            }
            hv3 hv3 = this.l;
            if (hv3 != null) {
                if (this.r) {
                    this.g.a(hv3);
                    jv3 = k();
                } else if (!this.q) {
                    jv3 = new c(0, hv3).a(this.l);
                } else {
                    ko4 ko4 = this.p;
                    if (ko4 != null && ko4.a().B() > 0) {
                        this.p.c();
                    }
                    if (this.h == -1) {
                        if (xw3.a(this.l) == -1) {
                            xo4 xo4 = this.o;
                            if (xo4 instanceof ax3) {
                                long f2 = ((ax3) xo4).f();
                                hv3.b g2 = this.l.g();
                                g2.b("Content-Length", Long.toString(f2));
                                this.l = g2.a();
                            }
                        }
                        this.g.a(this.l);
                    }
                    xo4 xo42 = this.o;
                    if (xo42 != null) {
                        ko4 ko42 = this.p;
                        if (ko42 != null) {
                            ko42.close();
                        } else {
                            xo42.close();
                        }
                        xo4 xo43 = this.o;
                        if (xo43 instanceof ax3) {
                            this.g.a((ax3) xo43);
                        }
                    }
                    jv3 = k();
                }
                a(jv3.g());
                jv3 jv32 = this.m;
                if (jv32 != null) {
                    if (a(jv32, jv3)) {
                        jv3.b j2 = this.m.j();
                        j2.a(this.k);
                        j2.d(c(this.f));
                        j2.a(a(this.m.g(), jv3.g()));
                        j2.a(c(this.m));
                        j2.c(c(jv3));
                        this.n = j2.a();
                        jv3.a().close();
                        m();
                        qv3 a2 = pv3.b.a(this.a);
                        a2.a();
                        a2.a(this.m, c(this.n));
                        this.n = a(this.n);
                        return;
                    }
                    wv3.a((Closeable) this.m.a());
                }
                jv3.b j3 = jv3.j();
                j3.a(this.k);
                j3.d(c(this.f));
                j3.a(c(this.m));
                j3.c(c(jv3));
                this.n = j3.a();
                if (b(this.n)) {
                    i();
                    this.n = a(a(this.s, this.n));
                }
            }
        }
    }

    @DexIgnore
    public void m() throws IOException {
        dx3 dx3 = this.g;
        if (!(dx3 == null || this.b == null)) {
            dx3.b();
        }
        this.b = null;
    }

    @DexIgnore
    public void n() throws RequestException, RouteException, IOException {
        if (this.t == null) {
            if (this.g == null) {
                hv3 a2 = a(this.k);
                qv3 a3 = pv3.b.a(this.a);
                jv3 a4 = a3 != null ? a3.a(a2) : null;
                this.t = new pw3.b(System.currentTimeMillis(), a2, a4).c();
                pw3 pw3 = this.t;
                this.l = pw3.a;
                this.m = pw3.b;
                if (a3 != null) {
                    a3.a(pw3);
                }
                if (a4 != null && this.m == null) {
                    wv3.a((Closeable) a4.a());
                }
                if (this.l != null) {
                    if (this.b == null) {
                        b();
                    }
                    this.g = pv3.b.a(this.b, this);
                    if (this.q && j() && this.o == null) {
                        long a5 = xw3.a(a2);
                        if (!this.j) {
                            this.g.a(this.l);
                            this.o = this.g.a(this.l, a5);
                        } else if (a5 > 2147483647L) {
                            throw new IllegalStateException("Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB.");
                        } else if (a5 != -1) {
                            this.g.a(this.l);
                            this.o = new ax3((int) a5);
                        } else {
                            this.o = new ax3();
                        }
                    }
                } else {
                    if (this.b != null) {
                        pv3.b.a(this.a.e(), this.b);
                        this.b = null;
                    }
                    jv3 jv3 = this.m;
                    if (jv3 != null) {
                        jv3.b j2 = jv3.j();
                        j2.a(this.k);
                        j2.d(c(this.f));
                        j2.a(c(this.m));
                        this.n = j2.a();
                    } else {
                        jv3.b bVar = new jv3.b();
                        bVar.a(this.k);
                        bVar.d(c(this.f));
                        bVar.a(Protocol.HTTP_1_1);
                        bVar.a(504);
                        bVar.a("Unsatisfiable Request (only-if-cached)");
                        bVar.a(u);
                        this.n = bVar.a();
                    }
                    this.n = a(this.n);
                }
            } else {
                throw new IllegalStateException();
            }
        }
    }

    @DexIgnore
    public void o() {
        if (this.h == -1) {
            this.h = System.currentTimeMillis();
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public static jv3 c(jv3 jv3) {
        if (jv3 == null || jv3.a() == null) {
            return jv3;
        }
        jv3.b j2 = jv3.j();
        j2.a((kv3) null);
        return j2.a();
    }

    @DexIgnore
    public final void b() throws RequestException, RouteException {
        if (this.b == null) {
            if (this.d == null) {
                this.c = a(this.a, this.l);
                try {
                    this.d = bx3.a(this.c, this.l, this.a);
                } catch (IOException e2) {
                    throw new RequestException(e2);
                }
            }
            this.b = c();
            pv3.b.a(this.a, this.b, this, this.l);
            this.e = this.b.e();
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final boolean a(RouteException routeException) {
        if (!this.a.z()) {
            return false;
        }
        IOException lastConnectException = routeException.getLastConnectException();
        if ((lastConnectException instanceof ProtocolException) || (lastConnectException instanceof InterruptedIOException)) {
            return false;
        }
        if ((!(lastConnectException instanceof SSLHandshakeException) || !(lastConnectException.getCause() instanceof CertificateException)) && !(lastConnectException instanceof SSLPeerUnverifiedException)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final wu3 c() throws RouteException {
        wu3 a2;
        xu3 e2 = this.a.e();
        while (true) {
            a2 = e2.a(this.c);
            if (a2 == null) {
                try {
                    return new wu3(e2, this.d.e());
                } catch (IOException e3) {
                    throw new RouteException(e3);
                }
            } else if (this.l.f().equals("GET") || pv3.b.b(a2)) {
                return a2;
            } else {
                wv3.a(a2.f());
            }
        }
        return a2;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements yo4 {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ /* synthetic */ lo4 f;
        @DexIgnore
        public /* final */ /* synthetic */ ow3 g;
        @DexIgnore
        public /* final */ /* synthetic */ ko4 h;

        @DexIgnore
        public b(uw3 uw3, lo4 lo4, ow3 ow3, ko4 ko4) {
            this.f = lo4;
            this.g = ow3;
            this.h = ko4;
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            try {
                long b = this.f.b(jo4, j);
                if (b == -1) {
                    if (!this.e) {
                        this.e = true;
                        this.h.close();
                    }
                    return -1;
                }
                jo4.a(this.h.a(), jo4.B() - b, b);
                this.h.d();
                return b;
            } catch (IOException e2) {
                if (!this.e) {
                    this.e = true;
                    this.g.abort();
                }
                throw e2;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.e && !wv3.a((yo4) this, 100, TimeUnit.MILLISECONDS)) {
                this.e = true;
                this.g.abort();
            }
            this.f.close();
        }

        @DexIgnore
        public zo4 b() {
            return this.f.b();
        }
    }

    @DexIgnore
    public uw3 a(IOException iOException, xo4 xo4) {
        bx3 bx3 = this.d;
        if (!(bx3 == null || this.b == null)) {
            a(bx3, iOException);
        }
        boolean z = xo4 == null || (xo4 instanceof ax3);
        if (this.d == null && this.b == null) {
            return null;
        }
        bx3 bx32 = this.d;
        if ((bx32 != null && !bx32.a()) || !a(iOException) || !z) {
            return null;
        }
        return new uw3(this.a, this.k, this.j, this.q, this.r, a(), this.d, (ax3) xo4, this.f);
    }

    @DexIgnore
    public uw3 b(RouteException routeException) {
        bx3 bx3 = this.d;
        if (!(bx3 == null || this.b == null)) {
            a(bx3, routeException.getLastConnectException());
        }
        if (this.d == null && this.b == null) {
            return null;
        }
        bx3 bx32 = this.d;
        if ((bx32 != null && !bx32.a()) || !a(routeException)) {
            return null;
        }
        return new uw3(this.a, this.k, this.j, this.q, this.r, a(), this.d, (ax3) this.o, this.f);
    }

    @DexIgnore
    public static boolean b(jv3 jv3) {
        if (jv3.l().f().equals("HEAD")) {
            return false;
        }
        int e2 = jv3.e();
        if (((e2 >= 100 && e2 < 200) || e2 == 204 || e2 == 304) && xw3.a(jv3) == -1 && !"chunked".equalsIgnoreCase(jv3.a("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void a(bx3 bx3, IOException iOException) {
        if (pv3.b.c(this.b) <= 0) {
            bx3.a(this.b.e(), iOException);
        }
    }

    @DexIgnore
    public final boolean a(IOException iOException) {
        if (this.a.z() && !(iOException instanceof ProtocolException) && !(iOException instanceof InterruptedIOException)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public wu3 a() {
        ko4 ko4 = this.p;
        if (ko4 != null) {
            wv3.a((Closeable) ko4);
        } else {
            xo4 xo4 = this.o;
            if (xo4 != null) {
                wv3.a((Closeable) xo4);
            }
        }
        jv3 jv3 = this.n;
        if (jv3 == null) {
            wu3 wu3 = this.b;
            if (wu3 != null) {
                wv3.a(wu3.f());
            }
            this.b = null;
            return null;
        }
        wv3.a((Closeable) jv3.a());
        dx3 dx3 = this.g;
        if (dx3 == null || this.b == null || dx3.d()) {
            wu3 wu32 = this.b;
            if (wu32 != null && !pv3.b.a(wu32)) {
                this.b = null;
            }
            wu3 wu33 = this.b;
            this.b = null;
            return wu33;
        }
        wv3.a(this.b.f());
        this.b = null;
        return null;
    }

    @DexIgnore
    public final jv3 a(jv3 jv3) throws IOException {
        if (!this.i || !"gzip".equalsIgnoreCase(this.n.a(GraphRequest.CONTENT_ENCODING_HEADER)) || jv3.a() == null) {
            return jv3;
        }
        qo4 qo4 = new qo4(jv3.a().B());
        cv3.b a2 = jv3.g().a();
        a2.b(GraphRequest.CONTENT_ENCODING_HEADER);
        a2.b("Content-Length");
        cv3 a3 = a2.a();
        jv3.b j2 = jv3.j();
        j2.a(a3);
        j2.a((kv3) new yw3(a3, so4.a((yo4) qo4)));
        return j2.a();
    }

    @DexIgnore
    public final hv3 a(hv3 hv3) throws IOException {
        hv3.b g2 = hv3.g();
        if (hv3.a("Host") == null) {
            g2.b("Host", wv3.a(hv3.d()));
        }
        wu3 wu3 = this.b;
        if ((wu3 == null || wu3.d() != Protocol.HTTP_1_0) && hv3.a("Connection") == null) {
            g2.b("Connection", "Keep-Alive");
        }
        if (hv3.a("Accept-Encoding") == null) {
            this.i = true;
            g2.b("Accept-Encoding", "gzip");
        }
        CookieHandler g3 = this.a.g();
        if (g3 != null) {
            xw3.a(g2, g3.get(hv3.h(), xw3.b(g2.a().c(), (String) null)));
        }
        if (hv3.a("User-Agent") == null) {
            g2.b("User-Agent", xv3.a());
        }
        return g2.a();
    }

    @DexIgnore
    public final jv3 a(ow3 ow3, jv3 jv3) throws IOException {
        if (ow3 == null) {
            return jv3;
        }
        xo4 a2 = ow3.a();
        if (a2 == null) {
            return jv3;
        }
        b bVar = new b(this, jv3.a().B(), ow3, so4.a(a2));
        jv3.b j2 = jv3.j();
        j2.a((kv3) new yw3(jv3.g(), so4.a((yo4) bVar)));
        return j2.a();
    }

    @DexIgnore
    public static boolean a(jv3 jv3, jv3 jv32) {
        if (jv32.e() == 304) {
            return true;
        }
        Date b2 = jv3.g().b("Last-Modified");
        if (b2 == null) {
            return false;
        }
        Date b3 = jv32.g().b("Last-Modified");
        if (b3 == null || b3.getTime() >= b2.getTime()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static cv3 a(cv3 cv3, cv3 cv32) throws IOException {
        cv3.b bVar = new cv3.b();
        int b2 = cv3.b();
        for (int i2 = 0; i2 < b2; i2++) {
            String a2 = cv3.a(i2);
            String b3 = cv3.b(i2);
            if ((!"Warning".equalsIgnoreCase(a2) || !b3.startsWith(AppEventsConstants.EVENT_PARAM_VALUE_YES)) && (!xw3.a(a2) || cv32.a(a2) == null)) {
                bVar.a(a2, b3);
            }
        }
        int b4 = cv32.b();
        for (int i3 = 0; i3 < b4; i3++) {
            String a3 = cv32.a(i3);
            if (!"Content-Length".equalsIgnoreCase(a3) && xw3.a(a3)) {
                bVar.a(a3, cv32.b(i3));
            }
        }
        return bVar.a();
    }

    @DexIgnore
    public void a(cv3 cv3) throws IOException {
        CookieHandler g2 = this.a.g();
        if (g2 != null) {
            g2.put(this.k.h(), xw3.b(cv3, (String) null));
        }
    }

    @DexIgnore
    public boolean a(dv3 dv3) {
        dv3 d2 = this.k.d();
        return d2.f().equals(dv3.f()) && d2.h() == dv3.h() && d2.j().equals(dv3.j());
    }

    @DexIgnore
    public static pu3 a(gv3 gv3, hv3 hv3) {
        uu3 uu3;
        HostnameVerifier hostnameVerifier;
        SSLSocketFactory sSLSocketFactory;
        if (hv3.e()) {
            SSLSocketFactory B = gv3.B();
            hostnameVerifier = gv3.l();
            sSLSocketFactory = B;
            uu3 = gv3.c();
        } else {
            sSLSocketFactory = null;
            hostnameVerifier = null;
            uu3 = null;
        }
        return new pu3(hv3.d().f(), hv3.d().h(), gv3.A(), sSLSocketFactory, hostnameVerifier, uu3, gv3.b(), gv3.w(), gv3.m(), gv3.f(), gv3.x());
    }
}
