package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.enums.SyncResponseCode;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yq2 implements CoroutineUseCase.a {
    @DexIgnore
    public /* final */ SyncResponseCode a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public yq2(SyncResponseCode syncResponseCode, ArrayList<Integer> arrayList) {
        kd4.b(syncResponseCode, "lastErrorCode");
        this.a = syncResponseCode;
        this.b = arrayList;
    }

    @DexIgnore
    public final SyncResponseCode a() {
        return this.a;
    }

    @DexIgnore
    public final ArrayList<Integer> b() {
        return this.b;
    }
}
