package com.fossil.blesdk.obfuscated;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n34 implements ErrorResponse {
    @DexIgnore
    public Throwable a;
    @DexIgnore
    public qr4 b;

    @DexIgnore
    public n34(Throwable th) {
        this.a = th;
    }

    @DexIgnore
    public static n34 a(Throwable th) {
        return new n34(th);
    }

    @DexIgnore
    public int G() {
        qr4 qr4 = this.b;
        if (qr4 != null) {
            return qr4.b();
        }
        return -1;
    }

    @DexIgnore
    public boolean b() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }

    @DexIgnore
    public static n34 a(qr4 qr4) {
        return new n34(qr4);
    }

    @DexIgnore
    public n34(qr4 qr4) {
        this.b = qr4;
    }

    @DexIgnore
    public String a() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        qr4 qr4 = this.b;
        if (qr4 != null) {
            if (r34.b(qr4.e())) {
                sb.append(this.b.e());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }
}
