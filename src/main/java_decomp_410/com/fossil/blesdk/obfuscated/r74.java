package com.fossil.blesdk.obfuscated;

import com.facebook.AccessToken;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r74 implements p74 {
    @DexIgnore
    public /* final */ v44 a;

    @DexIgnore
    public r74(v44 v44) {
        this.a = v44;
    }

    @DexIgnore
    public JSONObject a() {
        Throwable th;
        FileInputStream fileInputStream;
        JSONObject jSONObject;
        q44.g().d("Fabric", "Reading cached settings...");
        FileInputStream fileInputStream2 = null;
        try {
            File file = new File(new f74(this.a).a(), "com.crashlytics.settings.json");
            if (file.exists()) {
                fileInputStream = new FileInputStream(file);
                try {
                    jSONObject = new JSONObject(CommonUtils.b((InputStream) fileInputStream));
                    fileInputStream2 = fileInputStream;
                } catch (Exception e) {
                    e = e;
                    try {
                        q44.g().e("Fabric", "Failed to fetch cached settings", e);
                        CommonUtils.a((Closeable) fileInputStream, "Error while closing settings cache file.");
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        fileInputStream2 = fileInputStream;
                        CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
                        throw th;
                    }
                }
            } else {
                q44.g().d("Fabric", "No cached settings found.");
                jSONObject = null;
            }
            CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
            return jSONObject;
        } catch (Exception e2) {
            e = e2;
            fileInputStream = null;
            q44.g().e("Fabric", "Failed to fetch cached settings", e);
            CommonUtils.a((Closeable) fileInputStream, "Error while closing settings cache file.");
            return null;
        } catch (Throwable th3) {
            th = th3;
            CommonUtils.a((Closeable) fileInputStream2, "Error while closing settings cache file.");
            throw th;
        }
    }

    @DexIgnore
    public void a(long j, JSONObject jSONObject) {
        q44.g().d("Fabric", "Writing settings to cache file...");
        if (jSONObject != null) {
            FileWriter fileWriter = null;
            try {
                jSONObject.put(AccessToken.EXPIRES_AT_KEY, j);
                FileWriter fileWriter2 = new FileWriter(new File(new f74(this.a).a(), "com.crashlytics.settings.json"));
                try {
                    fileWriter2.write(jSONObject.toString());
                    fileWriter2.flush();
                    CommonUtils.a((Closeable) fileWriter2, "Failed to close settings writer.");
                } catch (Exception e) {
                    e = e;
                    fileWriter = fileWriter2;
                    try {
                        q44.g().e("Fabric", "Failed to cache settings", e);
                        CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
                    } catch (Throwable th) {
                        th = th;
                        CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileWriter = fileWriter2;
                    CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                q44.g().e("Fabric", "Failed to cache settings", e);
                CommonUtils.a((Closeable) fileWriter, "Failed to close settings writer.");
            }
        }
    }
}
