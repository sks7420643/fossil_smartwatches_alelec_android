package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k2 */
public class C2181k2 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ android.graphics.Rect f6687a; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: b */
    public static java.lang.Class<?> f6688b;

    /*
    static {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            try {
                f6688b = java.lang.Class.forName("android.graphics.Insets");
            } catch (java.lang.ClassNotFoundException unused) {
            }
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static boolean m9312a(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT < 15 && (drawable instanceof android.graphics.drawable.InsetDrawable)) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT < 15 && (drawable instanceof android.graphics.drawable.GradientDrawable)) {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT < 17 && (drawable instanceof android.graphics.drawable.LayerDrawable)) {
            return false;
        }
        if (drawable instanceof android.graphics.drawable.DrawableContainer) {
            android.graphics.drawable.Drawable.ConstantState constantState = drawable.getConstantState();
            if (!(constantState instanceof android.graphics.drawable.DrawableContainer.DrawableContainerState)) {
                return true;
            }
            for (android.graphics.drawable.Drawable a : ((android.graphics.drawable.DrawableContainer.DrawableContainerState) constantState).getChildren()) {
                if (!m9312a(a)) {
                    return false;
                }
            }
            return true;
        } else if (drawable instanceof com.fossil.blesdk.obfuscated.C1705e7) {
            return m9312a(((com.fossil.blesdk.obfuscated.C1705e7) drawable).mo10343a());
        } else {
            if (drawable instanceof com.fossil.blesdk.obfuscated.C2615p0) {
                return m9312a(((com.fossil.blesdk.obfuscated.C2615p0) drawable).mo14574a());
            }
            if (drawable instanceof android.graphics.drawable.ScaleDrawable) {
                return m9312a(((android.graphics.drawable.ScaleDrawable) drawable).getDrawable());
            }
            return true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m9313b(android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT == 21 && "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName())) {
            m9314c(drawable);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m9314c(android.graphics.drawable.Drawable drawable) {
        int[] state = drawable.getState();
        if (state == null || state.length == 0) {
            drawable.setState(com.fossil.blesdk.obfuscated.C3003u2.f9838e);
        } else {
            drawable.setState(com.fossil.blesdk.obfuscated.C3003u2.f9839f);
        }
        drawable.setState(state);
    }

    @DexIgnore
    /* renamed from: d */
    public static android.graphics.Rect m9315d(android.graphics.drawable.Drawable drawable) {
        if (f6688b != null) {
            try {
                android.graphics.drawable.Drawable h = com.fossil.blesdk.obfuscated.C1538c7.m5313h(drawable);
                java.lang.Object invoke = h.getClass().getMethod("getOpticalInsets", new java.lang.Class[0]).invoke(h, new java.lang.Object[0]);
                if (invoke != null) {
                    android.graphics.Rect rect = new android.graphics.Rect();
                    for (java.lang.reflect.Field field : f6688b.getFields()) {
                        java.lang.String name = field.getName();
                        char c = 65535;
                        switch (name.hashCode()) {
                            case -1383228885:
                                if (name.equals("bottom")) {
                                    c = 3;
                                    break;
                                }
                                break;
                            case 115029:
                                if (name.equals(com.facebook.appevents.codeless.internal.ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    c = 1;
                                    break;
                                }
                                break;
                            case 3317767:
                                if (name.equals(com.facebook.appevents.codeless.internal.ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    c = 0;
                                    break;
                                }
                                break;
                            case 108511772:
                                if (name.equals("right")) {
                                    c = 2;
                                    break;
                                }
                                break;
                        }
                        if (c == 0) {
                            rect.left = field.getInt(invoke);
                        } else if (c == 1) {
                            rect.top = field.getInt(invoke);
                        } else if (c == 2) {
                            rect.right = field.getInt(invoke);
                        } else if (c == 3) {
                            rect.bottom = field.getInt(invoke);
                        }
                    }
                    return rect;
                }
            } catch (java.lang.Exception unused) {
                android.util.Log.e("DrawableUtils", "Couldn't obtain the optical insets. Ignoring.");
            }
        }
        return f6687a;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.PorterDuff.Mode m9311a(int i, android.graphics.PorterDuff.Mode mode) {
        if (i == 3) {
            return android.graphics.PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return android.graphics.PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return android.graphics.PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return android.graphics.PorterDuff.Mode.MULTIPLY;
            case 15:
                return android.graphics.PorterDuff.Mode.SCREEN;
            case 16:
                return android.graphics.PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }
}
