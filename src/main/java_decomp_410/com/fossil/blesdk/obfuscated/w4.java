package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class w4 {
    @DexIgnore
    public static void a(y4 y4Var, q4 q4Var, int i) {
        int i2;
        x4[] x4VarArr;
        int i3;
        if (i == 0) {
            int i4 = y4Var.s0;
            x4VarArr = y4Var.v0;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = y4Var.t0;
            i2 = i5;
            x4VarArr = y4Var.u0;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            x4 x4Var = x4VarArr[i6];
            x4Var.a();
            if (!y4Var.u(4)) {
                a(y4Var, q4Var, i, i3, x4Var);
            } else if (!c5.a(y4Var, q4Var, i, i3, x4Var)) {
                a(y4Var, q4Var, i, i3, x4Var);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r2.e0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (r2.f0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        r5 = false;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0366  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0454  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x04ae  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x04b1  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x04b7  */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x04ba  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x04ce  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0367 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x016b  */
    public static void a(y4 y4Var, q4 q4Var, int i, int i2, x4 x4Var) {
        boolean z;
        boolean z2;
        ArrayList<ConstraintWidget> arrayList;
        SolverVariable solverVariable;
        ConstraintWidget constraintWidget;
        ConstraintAnchor constraintAnchor;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        int i3;
        ConstraintWidget constraintWidget2;
        int i4;
        SolverVariable solverVariable2;
        SolverVariable solverVariable3;
        ConstraintAnchor constraintAnchor4;
        ConstraintWidget constraintWidget3;
        ConstraintWidget constraintWidget4;
        SolverVariable solverVariable4;
        SolverVariable solverVariable5;
        ConstraintAnchor constraintAnchor5;
        float f;
        ArrayList<ConstraintWidget> arrayList2;
        int i5;
        float f2;
        boolean z3;
        int i6;
        ConstraintWidget constraintWidget5;
        boolean z4;
        int i7;
        y4 y4Var2 = y4Var;
        q4 q4Var2 = q4Var;
        x4 x4Var2 = x4Var;
        ConstraintWidget constraintWidget6 = x4Var2.a;
        ConstraintWidget constraintWidget7 = x4Var2.c;
        ConstraintWidget constraintWidget8 = x4Var2.b;
        ConstraintWidget constraintWidget9 = x4Var2.d;
        ConstraintWidget constraintWidget10 = x4Var2.e;
        float f3 = x4Var2.k;
        ConstraintWidget constraintWidget11 = x4Var2.f;
        ConstraintWidget constraintWidget12 = x4Var2.g;
        boolean z5 = y4Var2.C[i] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (i == 0) {
            z2 = constraintWidget10.e0 == 0;
            z = constraintWidget10.e0 == 1;
        } else {
            z2 = constraintWidget10.f0 == 0;
            z = constraintWidget10.f0 == 1;
        }
        boolean z6 = true;
        boolean z7 = z2;
        ConstraintWidget constraintWidget13 = constraintWidget6;
        boolean z8 = z;
        boolean z9 = z6;
        boolean z10 = false;
        while (true) {
            ConstraintWidget constraintWidget14 = null;
            if (z10) {
                break;
            }
            ConstraintAnchor constraintAnchor6 = constraintWidget13.A[i2];
            int i8 = (z5 || z9) ? 1 : 4;
            int b = constraintAnchor6.b();
            ConstraintAnchor constraintAnchor7 = constraintAnchor6.d;
            if (!(constraintAnchor7 == null || constraintWidget13 == constraintWidget6)) {
                b += constraintAnchor7.b();
            }
            int i9 = b;
            if (z9 && constraintWidget13 != constraintWidget6 && constraintWidget13 != constraintWidget8) {
                f2 = f3;
                z3 = z10;
                i6 = 6;
            } else if (!z7 || !z5) {
                f2 = f3;
                i6 = i8;
                z3 = z10;
            } else {
                f2 = f3;
                z3 = z10;
                i6 = 4;
            }
            ConstraintAnchor constraintAnchor8 = constraintAnchor6.d;
            if (constraintAnchor8 != null) {
                if (constraintWidget13 == constraintWidget8) {
                    z4 = z7;
                    constraintWidget5 = constraintWidget10;
                    q4Var2.b(constraintAnchor6.i, constraintAnchor8.i, i9, 5);
                } else {
                    constraintWidget5 = constraintWidget10;
                    z4 = z7;
                    q4Var2.b(constraintAnchor6.i, constraintAnchor8.i, i9, 6);
                }
                q4Var2.a(constraintAnchor6.i, constraintAnchor6.d.i, i9, i6);
            } else {
                constraintWidget5 = constraintWidget10;
                z4 = z7;
            }
            if (z5) {
                if (constraintWidget13.s() == 8 || constraintWidget13.C[i] != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    i7 = 0;
                } else {
                    ConstraintAnchor[] constraintAnchorArr = constraintWidget13.A;
                    i7 = 0;
                    q4Var2.b(constraintAnchorArr[i2 + 1].i, constraintAnchorArr[i2].i, 0, 5);
                }
                q4Var2.b(constraintWidget13.A[i2].i, y4Var2.A[i2].i, i7, 6);
            }
            ConstraintAnchor constraintAnchor9 = constraintWidget13.A[i2 + 1].d;
            if (constraintAnchor9 != null) {
                ConstraintWidget constraintWidget15 = constraintAnchor9.b;
                ConstraintAnchor[] constraintAnchorArr2 = constraintWidget15.A;
                if (constraintAnchorArr2[i2].d != null && constraintAnchorArr2[i2].d.b == constraintWidget13) {
                    constraintWidget14 = constraintWidget15;
                }
            }
            if (constraintWidget14 != null) {
                constraintWidget13 = constraintWidget14;
                z10 = z3;
            } else {
                z10 = true;
            }
            f3 = f2;
            z7 = z4;
            constraintWidget10 = constraintWidget5;
        }
        ConstraintWidget constraintWidget16 = constraintWidget10;
        float f4 = f3;
        boolean z11 = z7;
        if (constraintWidget9 != null) {
            ConstraintAnchor[] constraintAnchorArr3 = constraintWidget7.A;
            int i10 = i2 + 1;
            if (constraintAnchorArr3[i10].d != null) {
                ConstraintAnchor constraintAnchor10 = constraintWidget9.A[i10];
                q4Var2.c(constraintAnchor10.i, constraintAnchorArr3[i10].d.i, -constraintAnchor10.b(), 5);
                if (z5) {
                    int i11 = i2 + 1;
                    SolverVariable solverVariable6 = y4Var2.A[i11].i;
                    ConstraintAnchor[] constraintAnchorArr4 = constraintWidget7.A;
                    q4Var2.b(solverVariable6, constraintAnchorArr4[i11].i, constraintAnchorArr4[i11].b(), 6);
                }
                arrayList = x4Var2.h;
                if (arrayList != null) {
                    int size = arrayList.size();
                    if (size > 1) {
                        float f5 = (!x4Var2.n || x4Var2.p) ? f4 : (float) x4Var2.j;
                        float f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        ConstraintWidget constraintWidget17 = null;
                        int i12 = 0;
                        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        while (i12 < size) {
                            ConstraintWidget constraintWidget18 = arrayList.get(i12);
                            float f8 = constraintWidget18.g0[i];
                            if (f8 < f6) {
                                if (x4Var2.p) {
                                    ConstraintAnchor[] constraintAnchorArr5 = constraintWidget18.A;
                                    q4Var2.a(constraintAnchorArr5[i2 + 1].i, constraintAnchorArr5[i2].i, 0, 4);
                                    arrayList2 = arrayList;
                                    i5 = size;
                                    i12++;
                                    size = i5;
                                    arrayList = arrayList2;
                                    f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                } else {
                                    f8 = 1.0f;
                                    f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                }
                            }
                            if (f8 == f6) {
                                ConstraintAnchor[] constraintAnchorArr6 = constraintWidget18.A;
                                q4Var2.a(constraintAnchorArr6[i2 + 1].i, constraintAnchorArr6[i2].i, 0, 6);
                                arrayList2 = arrayList;
                                i5 = size;
                                i12++;
                                size = i5;
                                arrayList = arrayList2;
                                f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            } else {
                                if (constraintWidget17 != null) {
                                    ConstraintAnchor[] constraintAnchorArr7 = constraintWidget17.A;
                                    SolverVariable solverVariable7 = constraintAnchorArr7[i2].i;
                                    int i13 = i2 + 1;
                                    SolverVariable solverVariable8 = constraintAnchorArr7[i13].i;
                                    ConstraintAnchor[] constraintAnchorArr8 = constraintWidget18.A;
                                    arrayList2 = arrayList;
                                    SolverVariable solverVariable9 = constraintAnchorArr8[i2].i;
                                    SolverVariable solverVariable10 = constraintAnchorArr8[i13].i;
                                    i5 = size;
                                    n4 c = q4Var.c();
                                    c.a(f7, f5, f8, solverVariable7, solverVariable8, solverVariable9, solverVariable10);
                                    q4Var2.a(c);
                                } else {
                                    arrayList2 = arrayList;
                                    i5 = size;
                                }
                                f7 = f8;
                                constraintWidget17 = constraintWidget18;
                                i12++;
                                size = i5;
                                arrayList = arrayList2;
                                f6 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                        }
                    }
                }
                if (constraintWidget8 == null && (constraintWidget8 == constraintWidget9 || z9)) {
                    ConstraintAnchor[] constraintAnchorArr9 = constraintWidget6.A;
                    ConstraintAnchor constraintAnchor11 = constraintAnchorArr9[i2];
                    int i14 = i2 + 1;
                    ConstraintAnchor constraintAnchor12 = constraintWidget7.A[i14];
                    SolverVariable solverVariable11 = constraintAnchorArr9[i2].d != null ? constraintAnchorArr9[i2].d.i : null;
                    ConstraintAnchor[] constraintAnchorArr10 = constraintWidget7.A;
                    SolverVariable solverVariable12 = constraintAnchorArr10[i14].d != null ? constraintAnchorArr10[i14].d.i : null;
                    if (constraintWidget8 == constraintWidget9) {
                        ConstraintAnchor[] constraintAnchorArr11 = constraintWidget8.A;
                        constraintAnchor11 = constraintAnchorArr11[i2];
                        constraintAnchor12 = constraintAnchorArr11[i14];
                    }
                    if (!(solverVariable11 == null || solverVariable12 == null)) {
                        if (i == 0) {
                            f = constraintWidget16.V;
                        } else {
                            f = constraintWidget16.W;
                        }
                        q4Var.a(constraintAnchor11.i, solverVariable11, constraintAnchor11.b(), f, solverVariable12, constraintAnchor12.i, constraintAnchor12.b(), 5);
                    }
                } else if (z11 || constraintWidget8 == null) {
                    int i15 = 8;
                    if (z8 && constraintWidget8 != null) {
                        int i16 = x4Var2.j;
                        boolean z12 = i16 <= 0 && x4Var2.i == i16;
                        constraintWidget = constraintWidget8;
                        ConstraintWidget constraintWidget19 = constraintWidget;
                        while (constraintWidget != null) {
                            ConstraintWidget constraintWidget20 = constraintWidget.i0[i];
                            while (constraintWidget20 != null && constraintWidget20.s() == i15) {
                                constraintWidget20 = constraintWidget20.i0[i];
                            }
                            if (constraintWidget == constraintWidget8 || constraintWidget == constraintWidget9 || constraintWidget20 == null) {
                                constraintWidget2 = constraintWidget19;
                                i4 = 8;
                            } else {
                                ConstraintWidget constraintWidget21 = constraintWidget20 == constraintWidget9 ? null : constraintWidget20;
                                ConstraintAnchor constraintAnchor13 = constraintWidget.A[i2];
                                SolverVariable solverVariable13 = constraintAnchor13.i;
                                ConstraintAnchor constraintAnchor14 = constraintAnchor13.d;
                                if (constraintAnchor14 != null) {
                                    SolverVariable solverVariable14 = constraintAnchor14.i;
                                }
                                int i17 = i2 + 1;
                                SolverVariable solverVariable15 = constraintWidget19.A[i17].i;
                                int b2 = constraintAnchor13.b();
                                int b3 = constraintWidget.A[i17].b();
                                if (constraintWidget21 != null) {
                                    constraintAnchor4 = constraintWidget21.A[i2];
                                    solverVariable3 = constraintAnchor4.i;
                                    ConstraintAnchor constraintAnchor15 = constraintAnchor4.d;
                                    solverVariable2 = constraintAnchor15 != null ? constraintAnchor15.i : null;
                                } else {
                                    constraintAnchor4 = constraintWidget.A[i17].d;
                                    solverVariable3 = constraintAnchor4 != null ? constraintAnchor4.i : null;
                                    solverVariable2 = constraintWidget.A[i17].i;
                                }
                                if (constraintAnchor4 != null) {
                                    b3 += constraintAnchor4.b();
                                }
                                int i18 = b3;
                                if (constraintWidget19 != null) {
                                    b2 += constraintWidget19.A[i17].b();
                                }
                                int i19 = b2;
                                int i20 = z12 ? 6 : 4;
                                if (solverVariable13 == null || solverVariable15 == null || solverVariable3 == null || solverVariable2 == null) {
                                    constraintWidget3 = constraintWidget21;
                                    constraintWidget2 = constraintWidget19;
                                    i4 = 8;
                                } else {
                                    constraintWidget3 = constraintWidget21;
                                    int i21 = i18;
                                    constraintWidget2 = constraintWidget19;
                                    i4 = 8;
                                    q4Var.a(solverVariable13, solverVariable15, i19, 0.5f, solverVariable3, solverVariable2, i21, i20);
                                }
                                constraintWidget20 = constraintWidget3;
                            }
                            if (constraintWidget.s() == i4) {
                                constraintWidget = constraintWidget2;
                            }
                            constraintWidget19 = constraintWidget;
                            i15 = 8;
                            constraintWidget = constraintWidget20;
                        }
                        ConstraintAnchor constraintAnchor16 = constraintWidget8.A[i2];
                        constraintAnchor = constraintWidget6.A[i2].d;
                        int i22 = i2 + 1;
                        constraintAnchor2 = constraintWidget9.A[i22];
                        constraintAnchor3 = constraintWidget7.A[i22].d;
                        if (constraintAnchor != null) {
                            i3 = 5;
                        } else if (constraintWidget8 != constraintWidget9) {
                            i3 = 5;
                            q4Var2.a(constraintAnchor16.i, constraintAnchor.i, constraintAnchor16.b(), 5);
                        } else {
                            i3 = 5;
                            if (constraintAnchor3 != null) {
                                q4Var.a(constraintAnchor16.i, constraintAnchor.i, constraintAnchor16.b(), 0.5f, constraintAnchor2.i, constraintAnchor3.i, constraintAnchor2.b(), 5);
                            }
                        }
                        if (!(constraintAnchor3 == null || constraintWidget8 == constraintWidget9)) {
                            q4Var2.a(constraintAnchor2.i, constraintAnchor3.i, -constraintAnchor2.b(), i3);
                        }
                    }
                } else {
                    int i23 = x4Var2.j;
                    boolean z13 = i23 > 0 && x4Var2.i == i23;
                    ConstraintWidget constraintWidget22 = constraintWidget8;
                    ConstraintWidget constraintWidget23 = constraintWidget22;
                    while (constraintWidget22 != null) {
                        ConstraintWidget constraintWidget24 = constraintWidget22.i0[i];
                        while (true) {
                            if (constraintWidget24 != null) {
                                if (constraintWidget24.s() != 8) {
                                    break;
                                }
                                constraintWidget24 = constraintWidget24.i0[i];
                            } else {
                                break;
                            }
                        }
                        if (constraintWidget24 != null || constraintWidget22 == constraintWidget9) {
                            ConstraintAnchor constraintAnchor17 = constraintWidget22.A[i2];
                            SolverVariable solverVariable16 = constraintAnchor17.i;
                            ConstraintAnchor constraintAnchor18 = constraintAnchor17.d;
                            SolverVariable solverVariable17 = constraintAnchor18 != null ? constraintAnchor18.i : null;
                            if (constraintWidget23 != constraintWidget22) {
                                solverVariable17 = constraintWidget23.A[i2 + 1].i;
                            } else if (constraintWidget22 == constraintWidget8 && constraintWidget23 == constraintWidget22) {
                                ConstraintAnchor[] constraintAnchorArr12 = constraintWidget6.A;
                                solverVariable17 = constraintAnchorArr12[i2].d != null ? constraintAnchorArr12[i2].d.i : null;
                            }
                            int b4 = constraintAnchor17.b();
                            int i24 = i2 + 1;
                            int b5 = constraintWidget22.A[i24].b();
                            if (constraintWidget24 != null) {
                                constraintAnchor5 = constraintWidget24.A[i2];
                                solverVariable5 = constraintAnchor5.i;
                                solverVariable4 = constraintWidget22.A[i24].i;
                            } else {
                                constraintAnchor5 = constraintWidget7.A[i24].d;
                                solverVariable5 = constraintAnchor5 != null ? constraintAnchor5.i : null;
                                solverVariable4 = constraintWidget22.A[i24].i;
                            }
                            if (constraintAnchor5 != null) {
                                b5 += constraintAnchor5.b();
                            }
                            if (constraintWidget23 != null) {
                                b4 += constraintWidget23.A[i24].b();
                            }
                            if (!(solverVariable16 == null || solverVariable17 == null || solverVariable5 == null || solverVariable4 == null)) {
                                if (constraintWidget22 == constraintWidget8) {
                                    b4 = constraintWidget8.A[i2].b();
                                }
                                int i25 = b4;
                                int b6 = constraintWidget22 == constraintWidget9 ? constraintWidget9.A[i24].b() : b5;
                                int i26 = i25;
                                SolverVariable solverVariable18 = solverVariable5;
                                SolverVariable solverVariable19 = solverVariable4;
                                int i27 = b6;
                                constraintWidget4 = constraintWidget24;
                                q4Var.a(solverVariable16, solverVariable17, i26, 0.5f, solverVariable18, solverVariable19, i27, z13 ? 6 : 4);
                                if (constraintWidget22.s() == 8) {
                                    constraintWidget23 = constraintWidget22;
                                }
                                constraintWidget22 = constraintWidget4;
                            }
                        }
                        constraintWidget4 = constraintWidget24;
                        if (constraintWidget22.s() == 8) {
                        }
                        constraintWidget22 = constraintWidget4;
                    }
                }
                if ((!z11 || z8) && constraintWidget8 != null) {
                    ConstraintAnchor constraintAnchor19 = constraintWidget8.A[i2];
                    int i28 = i2 + 1;
                    ConstraintAnchor constraintAnchor20 = constraintWidget9.A[i28];
                    ConstraintAnchor constraintAnchor21 = constraintAnchor19.d;
                    solverVariable = constraintAnchor21 == null ? constraintAnchor21.i : null;
                    ConstraintAnchor constraintAnchor22 = constraintAnchor20.d;
                    SolverVariable solverVariable20 = constraintAnchor22 == null ? constraintAnchor22.i : null;
                    if (constraintWidget7 != constraintWidget9) {
                        ConstraintAnchor constraintAnchor23 = constraintWidget7.A[i28].d;
                        solverVariable20 = constraintAnchor23 != null ? constraintAnchor23.i : null;
                    }
                    SolverVariable solverVariable21 = solverVariable20;
                    if (constraintWidget8 == constraintWidget9) {
                        ConstraintAnchor[] constraintAnchorArr13 = constraintWidget8.A;
                        ConstraintAnchor constraintAnchor24 = constraintAnchorArr13[i2];
                        constraintAnchor20 = constraintAnchorArr13[i28];
                        constraintAnchor19 = constraintAnchor24;
                    }
                    if (solverVariable != null && solverVariable21 != null) {
                        int b7 = constraintAnchor19.b();
                        if (constraintWidget9 != null) {
                            constraintWidget7 = constraintWidget9;
                        }
                        q4Var.a(constraintAnchor19.i, solverVariable, b7, 0.5f, solverVariable21, constraintAnchor20.i, constraintWidget7.A[i28].b(), 5);
                        return;
                    }
                }
                return;
            }
        }
        if (z5) {
        }
        arrayList = x4Var2.h;
        if (arrayList != null) {
        }
        if (constraintWidget8 == null) {
        }
        if (z11) {
        }
        int i152 = 8;
        int i162 = x4Var2.j;
        if (i162 <= 0) {
        }
        constraintWidget = constraintWidget8;
        ConstraintWidget constraintWidget192 = constraintWidget;
        while (constraintWidget != null) {
        }
        ConstraintAnchor constraintAnchor162 = constraintWidget8.A[i2];
        constraintAnchor = constraintWidget6.A[i2].d;
        int i222 = i2 + 1;
        constraintAnchor2 = constraintWidget9.A[i222];
        constraintAnchor3 = constraintWidget7.A[i222].d;
        if (constraintAnchor != null) {
        }
        q4Var2.a(constraintAnchor2.i, constraintAnchor3.i, -constraintAnchor2.b(), i3);
        if (!z11) {
        }
        ConstraintAnchor constraintAnchor192 = constraintWidget8.A[i2];
        int i282 = i2 + 1;
        ConstraintAnchor constraintAnchor202 = constraintWidget9.A[i282];
        ConstraintAnchor constraintAnchor212 = constraintAnchor192.d;
        if (constraintAnchor212 == null) {
        }
        ConstraintAnchor constraintAnchor222 = constraintAnchor202.d;
        if (constraintAnchor222 == null) {
        }
        if (constraintWidget7 != constraintWidget9) {
        }
        SolverVariable solverVariable212 = solverVariable20;
        if (constraintWidget8 == constraintWidget9) {
        }
        if (solverVariable != null) {
        }
    }
}
