package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.fossil.blesdk.obfuscated.hg;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mg implements hg {
    @DexIgnore
    public /* final */ a a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ lg[] e;
        @DexIgnore
        public /* final */ hg.a f;
        @DexIgnore
        public boolean g;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mg$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mg$a$a  reason: collision with other inner class name */
        public class C0022a implements DatabaseErrorHandler {
            @DexIgnore
            public /* final */ /* synthetic */ hg.a a;
            @DexIgnore
            public /* final */ /* synthetic */ lg[] b;

            @DexIgnore
            public C0022a(hg.a aVar, lg[] lgVarArr) {
                this.a = aVar;
                this.b = lgVarArr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.b(a.a(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public a(Context context, String str, lg[] lgVarArr, hg.a aVar) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, aVar.a, new C0022a(aVar, lgVarArr));
            this.f = aVar;
            this.e = lgVarArr;
        }

        @DexIgnore
        public lg a(SQLiteDatabase sQLiteDatabase) {
            return a(this.e, sQLiteDatabase);
        }

        @DexIgnore
        public synchronized void close() {
            super.close();
            this.e[0] = null;
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.f.a((gg) a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.f.c(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g = true;
            this.f.a(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.g) {
                this.f.d(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.g = true;
            this.f.b(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public synchronized gg y() {
            this.g = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.g) {
                close();
                return y();
            }
            return a(writableDatabase);
        }

        @DexIgnore
        public static lg a(lg[] lgVarArr, SQLiteDatabase sQLiteDatabase) {
            lg lgVar = lgVarArr[0];
            if (lgVar == null || !lgVar.a(sQLiteDatabase)) {
                lgVarArr[0] = new lg(sQLiteDatabase);
            }
            return lgVarArr[0];
        }
    }

    @DexIgnore
    public mg(Context context, String str, hg.a aVar) {
        this.a = a(context, str, aVar);
    }

    @DexIgnore
    public final a a(Context context, String str, hg.a aVar) {
        return new a(context, str, new lg[1], aVar);
    }

    @DexIgnore
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public String getDatabaseName() {
        return this.a.getDatabaseName();
    }

    @DexIgnore
    public void a(boolean z) {
        this.a.setWriteAheadLoggingEnabled(z);
    }

    @DexIgnore
    public gg a() {
        return this.a.y();
    }
}
