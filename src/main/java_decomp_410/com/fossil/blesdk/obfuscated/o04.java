package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class o04 {
    @DexIgnore
    public static String l;
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public u14 d; // = null;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;
    @DexIgnore
    public String g; // = null;
    @DexIgnore
    public String h; // = null;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public Context j;
    @DexIgnore
    public k04 k; // = null;

    @DexIgnore
    public o04(Context context, int i2, k04 k04) {
        this.j = context;
        this.b = System.currentTimeMillis() / 1000;
        this.c = i2;
        this.g = h04.d(context);
        this.h = e24.m(context);
        this.a = h04.b(context);
        if (k04 != null) {
            this.k = k04;
            if (e24.c(k04.a())) {
                this.a = k04.a();
            }
            if (e24.c(k04.b())) {
                this.g = k04.b();
            }
            if (e24.c(k04.c())) {
                this.h = k04.c();
            }
            this.i = k04.d();
        }
        this.f = h04.c(context);
        this.d = g14.b(context).a(context);
        f a2 = a();
        f fVar = f.NETWORK_DETECTOR;
        this.e = a2 != fVar ? e24.v(context).intValue() : -fVar.a();
        if (!wy3.b(l)) {
            String e2 = h04.e(context);
            l = e2;
            if (!e24.c(e2)) {
                l = "0";
            }
        }
    }

    @DexIgnore
    public abstract f a();

    @DexIgnore
    public abstract boolean a(JSONObject jSONObject);

    @DexIgnore
    public long b() {
        return this.b;
    }

    @DexIgnore
    public boolean b(JSONObject jSONObject) {
        try {
            j24.a(jSONObject, "ky", this.a);
            jSONObject.put("et", a().a());
            if (this.d != null) {
                jSONObject.put("ui", this.d.b());
                j24.a(jSONObject, "mc", this.d.c());
                int d2 = this.d.d();
                jSONObject.put("ut", d2);
                if (d2 == 0 && e24.z(this.j) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            j24.a(jSONObject, "cui", this.f);
            if (a() != f.SESSION_ENV) {
                j24.a(jSONObject, "av", this.h);
                j24.a(jSONObject, "ch", this.g);
            }
            if (this.i) {
                jSONObject.put("impt", 1);
            }
            j24.a(jSONObject, "mid", l);
            jSONObject.put("idx", this.e);
            jSONObject.put("si", this.c);
            jSONObject.put("ts", this.b);
            jSONObject.put("dts", e24.a(this.j, false));
            return a(jSONObject);
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public k04 c() {
        return this.k;
    }

    @DexIgnore
    public Context d() {
        return this.j;
    }

    @DexIgnore
    public boolean e() {
        return this.i;
    }

    @DexIgnore
    public String f() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }
}
