package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pm */
public final class C2660pm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f8411a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f8412b;

    @DexIgnore
    public C2660pm(java.lang.String str, java.lang.String str2) {
        this.f8411a = str;
        this.f8412b = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo14804a() {
        return this.f8411a;
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.String mo14805b() {
        return this.f8412b;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C2660pm.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2660pm pmVar = (com.fossil.blesdk.obfuscated.C2660pm) obj;
        if (!android.text.TextUtils.equals(this.f8411a, pmVar.f8411a) || !android.text.TextUtils.equals(this.f8412b, pmVar.f8412b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.f8411a.hashCode() * 31) + this.f8412b.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Header[name=" + this.f8411a + ",value=" + this.f8412b + "]";
    }
}
