package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.md */
public abstract class C2390md<Key, Value> extends com.fossil.blesdk.obfuscated.C2120jd<Key, Value> {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$a")
    /* renamed from: com.fossil.blesdk.obfuscated.md$a */
    public static abstract class C2391a<Value> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo13573a(java.util.List<Value> list);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$b")
    /* renamed from: com.fossil.blesdk.obfuscated.md$b */
    public static class C2392b<Value> extends com.fossil.blesdk.obfuscated.C2390md.C2391a<Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2312d<Value> f7419a;

        @DexIgnore
        public C2392b(com.fossil.blesdk.obfuscated.C2390md mdVar, int i, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f7419a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(mdVar, i, executor, aVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13573a(java.util.List<Value> list) {
            if (!this.f7419a.mo13247a()) {
                this.f7419a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$c")
    /* renamed from: com.fossil.blesdk.obfuscated.md$c */
    public static abstract class C2393c<Value> extends com.fossil.blesdk.obfuscated.C2390md.C2391a<Value> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$d")
    /* renamed from: com.fossil.blesdk.obfuscated.md$d */
    public static class C2394d<Value> extends com.fossil.blesdk.obfuscated.C2390md.C2393c<Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2312d<Value> f7420a;

        @DexIgnore
        public C2394d(com.fossil.blesdk.obfuscated.C2390md mdVar, boolean z, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f7420a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(mdVar, 0, (java.util.concurrent.Executor) null, aVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13573a(java.util.List<Value> list) {
            if (!this.f7420a.mo13247a()) {
                this.f7420a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$e")
    /* renamed from: com.fossil.blesdk.obfuscated.md$e */
    public static class C2395e<Key> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f7421a;

        @DexIgnore
        public C2395e(Key key, int i, boolean z) {
            this.f7421a = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.md$f")
    /* renamed from: com.fossil.blesdk.obfuscated.md$f */
    public static class C2396f<Key> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ Key f7422a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f7423b;

        @DexIgnore
        public C2396f(Key key, int i) {
            this.f7422a = key;
            this.f7423b = i;
        }
    }

    @DexIgnore
    public final void dispatchLoadAfter(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        loadAfter(new com.fossil.blesdk.obfuscated.C2390md.C2396f(getKey(value), i2), new com.fossil.blesdk.obfuscated.C2390md.C2392b(this, 1, executor, aVar));
    }

    @DexIgnore
    public final void dispatchLoadBefore(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        loadBefore(new com.fossil.blesdk.obfuscated.C2390md.C2396f(getKey(value), i2), new com.fossil.blesdk.obfuscated.C2390md.C2392b(this, 2, executor, aVar));
    }

    @DexIgnore
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        com.fossil.blesdk.obfuscated.C2390md.C2394d dVar = new com.fossil.blesdk.obfuscated.C2390md.C2394d(this, z, aVar);
        loadInitial(new com.fossil.blesdk.obfuscated.C2390md.C2395e(key, i, z), dVar);
        dVar.f7420a.mo13246a(executor);
    }

    @DexIgnore
    public final Key getKey(int i, Value value) {
        if (value == null) {
            return null;
        }
        return getKey(value);
    }

    @DexIgnore
    public abstract Key getKey(Value value);

    @DexIgnore
    public abstract void loadAfter(com.fossil.blesdk.obfuscated.C2390md.C2396f<Key> fVar, com.fossil.blesdk.obfuscated.C2390md.C2391a<Value> aVar);

    @DexIgnore
    public abstract void loadBefore(com.fossil.blesdk.obfuscated.C2390md.C2396f<Key> fVar, com.fossil.blesdk.obfuscated.C2390md.C2391a<Value> aVar);

    @DexIgnore
    public abstract void loadInitial(com.fossil.blesdk.obfuscated.C2390md.C2395e<Key> eVar, com.fossil.blesdk.obfuscated.C2390md.C2393c<Value> cVar);

    @DexIgnore
    public final <ToValue> com.fossil.blesdk.obfuscated.C2390md<Key, ToValue> map(com.fossil.blesdk.obfuscated.C2374m3<Value, ToValue> m3Var) {
        return mapByPage((com.fossil.blesdk.obfuscated.C2374m3) com.fossil.blesdk.obfuscated.C2307ld.createListFunction(m3Var));
    }

    @DexIgnore
    public final <ToValue> com.fossil.blesdk.obfuscated.C2390md<Key, ToValue> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<Value>, java.util.List<ToValue>> m3Var) {
        return new com.fossil.blesdk.obfuscated.C3278xd(this, m3Var);
    }
}
