package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i3 */
public class C1995i3 extends com.fossil.blesdk.obfuscated.C2102j3 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f5923a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.concurrent.ExecutorService f5924b; // = java.util.concurrent.Executors.newFixedThreadPool(4, new com.fossil.blesdk.obfuscated.C1995i3.C1996a(this));

    @DexIgnore
    /* renamed from: c */
    public volatile android.os.Handler f5925c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.i3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.i3$a */
    public class C1996a implements java.util.concurrent.ThreadFactory {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.concurrent.atomic.AtomicInteger f5926a; // = new java.util.concurrent.atomic.AtomicInteger(0);

        @DexIgnore
        public C1996a(com.fossil.blesdk.obfuscated.C1995i3 i3Var) {
        }

        @DexIgnore
        public java.lang.Thread newThread(java.lang.Runnable runnable) {
            java.lang.Thread thread = new java.lang.Thread(runnable);
            thread.setName(java.lang.String.format("arch_disk_io_%d", new java.lang.Object[]{java.lang.Integer.valueOf(this.f5926a.getAndIncrement())}));
            return thread;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11563a(java.lang.Runnable runnable) {
        this.f5924b.execute(runnable);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11565c(java.lang.Runnable runnable) {
        if (this.f5925c == null) {
            synchronized (this.f5923a) {
                if (this.f5925c == null) {
                    this.f5925c = m8217a(android.os.Looper.getMainLooper());
                }
            }
        }
        this.f5925c.post(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11564a() {
        return android.os.Looper.getMainLooper().getThread() == java.lang.Thread.currentThread();
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Handler m8217a(android.os.Looper looper) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 28) {
            return android.os.Handler.createAsync(looper);
        }
        if (i >= 16) {
            java.lang.Class<android.os.Handler> cls = android.os.Handler.class;
            try {
                return cls.getDeclaredConstructor(new java.lang.Class[]{android.os.Looper.class, android.os.Handler.Callback.class, java.lang.Boolean.TYPE}).newInstance(new java.lang.Object[]{looper, null, true});
            } catch (java.lang.IllegalAccessException | java.lang.InstantiationException | java.lang.NoSuchMethodException unused) {
            } catch (java.lang.reflect.InvocationTargetException unused2) {
                return new android.os.Handler(looper);
            }
        }
        return new android.os.Handler(looper);
    }
}
