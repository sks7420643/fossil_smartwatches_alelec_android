package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ vl1 g;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 h;
    @DexIgnore
    public /* final */ /* synthetic */ vl1 i;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 j;

    @DexIgnore
    public dk1(vj1 vj1, boolean z, boolean z2, vl1 vl1, rl1 rl1, vl1 vl12) {
        this.j = vj1;
        this.e = z;
        this.f = z2;
        this.g = vl1;
        this.h = rl1;
        this.i = vl12;
    }

    @DexIgnore
    public final void run() {
        kg1 d = this.j.d;
        if (d == null) {
            this.j.d().s().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.e) {
            this.j.a(d, this.f ? null : this.g, this.h);
        } else {
            try {
                if (TextUtils.isEmpty(this.i.e)) {
                    d.a(this.g, this.h);
                } else {
                    d.a(this.g);
                }
            } catch (RemoteException e2) {
                this.j.d().s().a("Failed to send conditional user property to the service", e2);
            }
        }
        this.j.C();
    }
}
