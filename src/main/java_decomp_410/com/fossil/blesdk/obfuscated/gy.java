package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gy {
    @DexIgnore
    public static /* final */ iy a;

    /* JADX WARNING: Removed duplicated region for block: B:7:0x0011  */
    /*
    static {
        iy iyVar;
        try {
            iyVar = fy.a();
        } catch (IllegalStateException | NoClassDefFoundError unused) {
            iyVar = null;
            if (iyVar == null) {
            }
            a = iyVar;
        } catch (Throwable th) {
            Log.w("AnswersOptionalLogger", "Unexpected error creating AnswersKitEventLogger", th);
            iyVar = null;
            if (iyVar == null) {
            }
            a = iyVar;
        }
        if (iyVar == null) {
            iyVar = jy.a();
        }
        a = iyVar;
    }
    */

    @DexIgnore
    public static iy a() {
        return a;
    }
}
