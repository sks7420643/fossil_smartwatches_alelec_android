package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.model.file.AssetFile;
import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o20 extends s20<AssetFile[], byte[]> {
    @DexIgnore
    public static /* final */ k20<AssetFile[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ l20<byte[]>[] b; // = {new c(FileType.ASSET), new d(FileType.ASSET)};
    @DexIgnore
    public static /* final */ o20 c; // = new o20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends q20<AssetFile[]> {
        @DexIgnore
        public byte[] a(AssetFile[] assetFileArr) {
            kd4.b(assetFileArr, "entries");
            return o20.c.a(assetFileArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends r20<AssetFile[]> {
        @DexIgnore
        public byte[] a(AssetFile[] assetFileArr) {
            kd4.b(assetFileArr, "entries");
            return o20.c.a(assetFileArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends t20<byte[]> {
        @DexIgnore
        public c(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public byte[] b(byte[] bArr) {
            kd4.b(bArr, "data");
            return bArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends u20<byte[]> {
        @DexIgnore
        public d(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public byte[] b(byte[] bArr) {
            kd4.b(bArr, "data");
            return bArr;
        }
    }

    @DexIgnore
    public k20<AssetFile[]>[] a() {
        return a;
    }

    @DexIgnore
    public l20<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    public final byte[] a(AssetFile[] assetFileArr) {
        kd4.b(assetFileArr, "entries");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (AssetFile entryData$blesdk_productionRelease : assetFileArr) {
            byteArrayOutputStream.write(entryData$blesdk_productionRelease.getEntryData$blesdk_productionRelease());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        kd4.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }
}
