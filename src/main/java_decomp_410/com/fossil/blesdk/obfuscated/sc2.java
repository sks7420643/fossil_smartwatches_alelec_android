package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ LinearLayout A;
    @DexIgnore
    public /* final */ LinearLayout B;
    @DexIgnore
    public /* final */ LinearLayout C;
    @DexIgnore
    public /* final */ RecyclerView D;
    @DexIgnore
    public /* final */ SwitchCompat E;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ ui2 x;
    @DexIgnore
    public /* final */ LinearLayout y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sc2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, ui2 ui2, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, LinearLayout linearLayout4, LinearLayout linearLayout5, RecyclerView recyclerView, SwitchCompat switchCompat, View view2, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleTextView;
        this.t = flexibleTextView4;
        this.u = flexibleTextView8;
        this.v = flexibleTextView10;
        this.w = flexibleTextView12;
        this.x = ui2;
        a((ViewDataBinding) this.x);
        this.y = linearLayout;
        this.z = linearLayout2;
        this.A = linearLayout3;
        this.B = linearLayout4;
        this.C = linearLayout5;
        this.D = recyclerView;
        this.E = switchCompat;
    }
}
