package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m13 implements Factory<q43> {
    @DexIgnore
    public static q43 a(j13 j13) {
        q43 c = j13.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
