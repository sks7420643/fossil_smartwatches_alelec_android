package com.fossil.blesdk.obfuscated;

import androidx.work.WorkInfo;
import com.fossil.blesdk.obfuscated.hl;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface il {
    @DexIgnore
    int a(WorkInfo.State state, String... strArr);

    @DexIgnore
    int a(String str, long j);

    @DexIgnore
    List<hl> a();

    @DexIgnore
    List<hl> a(int i);

    @DexIgnore
    List<hl.b> a(String str);

    @DexIgnore
    void a(hl hlVar);

    @DexIgnore
    void a(String str, aj ajVar);

    @DexIgnore
    List<hl> b();

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(String str, long j);

    @DexIgnore
    List<String> c();

    @DexIgnore
    List<String> c(String str);

    @DexIgnore
    int d();

    @DexIgnore
    WorkInfo.State d(String str);

    @DexIgnore
    hl e(String str);

    @DexIgnore
    int f(String str);

    @DexIgnore
    List<aj> g(String str);

    @DexIgnore
    int h(String str);
}
