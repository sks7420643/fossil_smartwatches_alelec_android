package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import com.crashlytics.android.answers.SessionEvent;
import com.fossil.blesdk.obfuscated.o44;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hx extends o44.b {
    @DexIgnore
    public /* final */ ay a;
    @DexIgnore
    public /* final */ lx b;

    @DexIgnore
    public hx(ay ayVar, lx lxVar) {
        this.a = ayVar;
        this.b = lxVar;
    }

    @DexIgnore
    public void a(Activity activity) {
    }

    @DexIgnore
    public void a(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public void b(Activity activity) {
        this.a.a(activity, SessionEvent.Type.PAUSE);
        this.b.b();
    }

    @DexIgnore
    public void b(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public void c(Activity activity) {
        this.a.a(activity, SessionEvent.Type.RESUME);
        this.b.c();
    }

    @DexIgnore
    public void d(Activity activity) {
        this.a.a(activity, SessionEvent.Type.START);
    }

    @DexIgnore
    public void e(Activity activity) {
        this.a.a(activity, SessionEvent.Type.STOP);
    }
}
