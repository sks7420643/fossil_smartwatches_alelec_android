package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g8 */
public interface C1862g8<T> {
    @DexIgnore
    /* renamed from: a */
    T mo11162a();

    @DexIgnore
    /* renamed from: a */
    boolean mo11163a(T t);
}
