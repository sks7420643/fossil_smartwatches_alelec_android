package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vn0<T extends tn0> {
    @DexIgnore
    void a(T t);
}
