package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s10 extends GattOperationResult {
    @DexIgnore
    public /* final */ Peripheral.HIDState b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s10(GattOperationResult.GattResult gattResult, Peripheral.HIDState hIDState, Peripheral.HIDState hIDState2) {
        super(gattResult);
        kd4.b(gattResult, "gattResult");
        kd4.b(hIDState, "previousHIDState");
        kd4.b(hIDState2, "newState");
        this.b = hIDState2;
    }

    @DexIgnore
    public final Peripheral.HIDState b() {
        return this.b;
    }
}
