package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qi */
public class C2743qi implements android.view.animation.Interpolator {

    @DexIgnore
    /* renamed from: a */
    public float[] f8682a;

    @DexIgnore
    /* renamed from: b */
    public float[] f8683b;

    @DexIgnore
    public C2743qi(android.content.Context context, android.util.AttributeSet attributeSet, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        this(context.getResources(), context.getTheme(), attributeSet, xmlPullParser);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15241a(android.content.res.TypedArray typedArray, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        if (com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "pathData")) {
            java.lang.String a = com.fossil.blesdk.obfuscated.C2863s6.m13543a(typedArray, xmlPullParser, "pathData", 4);
            android.graphics.Path b = com.fossil.blesdk.obfuscated.C3009u6.m14584b(a);
            if (b != null) {
                mo15242a(b);
                return;
            }
            throw new android.view.InflateException("The path is null, which is created from " + a);
        } else if (!com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "controlX1")) {
            throw new android.view.InflateException("pathInterpolator requires the controlX1 attribute");
        } else if (com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "controlY1")) {
            float a2 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "controlX1", 0, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a3 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "controlY1", 1, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            boolean a4 = com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "controlX2");
            if (a4 != com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "controlY2")) {
                throw new android.view.InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
            } else if (!a4) {
                mo15239a(a2, a3);
            } else {
                mo15240a(a2, a3, com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "controlX2", 2, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES), com.fossil.blesdk.obfuscated.C2863s6.m13539a(typedArray, xmlPullParser, "controlY2", 3, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            }
        } else {
            throw new android.view.InflateException("pathInterpolator requires the controlY1 attribute");
        }
    }

    @DexIgnore
    public float getInterpolation(float f) {
        if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (f >= 1.0f) {
            return 1.0f;
        }
        int i = 0;
        int length = this.f8682a.length - 1;
        while (length - i > 1) {
            int i2 = (i + length) / 2;
            if (f < this.f8682a[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float[] fArr = this.f8682a;
        float f2 = fArr[length] - fArr[i];
        if (f2 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return this.f8683b[i];
        }
        float[] fArr2 = this.f8683b;
        float f3 = fArr2[i];
        return f3 + (((f - fArr[i]) / f2) * (fArr2[length] - f3));
    }

    @DexIgnore
    public C2743qi(android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6878l);
        mo15241a(a, xmlPullParser);
        a.recycle();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15239a(float f, float f2) {
        android.graphics.Path path = new android.graphics.Path();
        path.moveTo(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.quadTo(f, f2, 1.0f, 1.0f);
        mo15242a(path);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15240a(float f, float f2, float f3, float f4) {
        android.graphics.Path path = new android.graphics.Path();
        path.moveTo(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
        mo15242a(path);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15242a(android.graphics.Path path) {
        int i = 0;
        android.graphics.PathMeasure pathMeasure = new android.graphics.PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int min = java.lang.Math.min(3000, ((int) (length / 0.002f)) + 1);
        if (min > 0) {
            this.f8682a = new float[min];
            this.f8683b = new float[min];
            float[] fArr = new float[2];
            for (int i2 = 0; i2 < min; i2++) {
                pathMeasure.getPosTan((((float) i2) * length) / ((float) (min - 1)), fArr, (float[]) null);
                this.f8682a[i2] = fArr[0];
                this.f8683b[i2] = fArr[1];
            }
            if (((double) java.lang.Math.abs(this.f8682a[0])) <= 1.0E-5d && ((double) java.lang.Math.abs(this.f8683b[0])) <= 1.0E-5d) {
                int i3 = min - 1;
                if (((double) java.lang.Math.abs(this.f8682a[i3] - 1.0f)) <= 1.0E-5d && ((double) java.lang.Math.abs(this.f8683b[i3] - 1.0f)) <= 1.0E-5d) {
                    int i4 = 0;
                    float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    while (i < min) {
                        float[] fArr2 = this.f8682a;
                        int i5 = i4 + 1;
                        float f2 = fArr2[i4];
                        if (f2 >= f) {
                            fArr2[i] = f2;
                            i++;
                            f = f2;
                            i4 = i5;
                        } else {
                            throw new java.lang.IllegalArgumentException("The Path cannot loop back on itself, x :" + f2);
                        }
                    }
                    if (pathMeasure.nextContour()) {
                        throw new java.lang.IllegalArgumentException("The Path should be continuous, can't have 2+ contours");
                    }
                    return;
                }
            }
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("The Path must start at (0,0) and end at (1,1) start: ");
            sb.append(this.f8682a[0]);
            sb.append(",");
            sb.append(this.f8683b[0]);
            sb.append(" end:");
            int i6 = min - 1;
            sb.append(this.f8682a[i6]);
            sb.append(",");
            sb.append(this.f8683b[i6]);
            throw new java.lang.IllegalArgumentException(sb.toString());
        }
        throw new java.lang.IllegalArgumentException("The Path has a invalid length " + length);
    }
}
