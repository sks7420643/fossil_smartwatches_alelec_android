package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.enums.Action;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g12 extends l12 {
    @DexIgnore
    public static volatile g12[] x;
    @DexIgnore
    public i12 a;
    @DexIgnore
    public i12 b;
    @DexIgnore
    public i12 c;
    @DexIgnore
    public i12 d;
    @DexIgnore
    public i12 e;
    @DexIgnore
    public i12 f;
    @DexIgnore
    public i12 g;
    @DexIgnore
    public i12 h;
    @DexIgnore
    public i12 i;
    @DexIgnore
    public i12 j;
    @DexIgnore
    public i12 k;
    @DexIgnore
    public i12 l;
    @DexIgnore
    public i12 m;
    @DexIgnore
    public i12 n;
    @DexIgnore
    public i12 o;
    @DexIgnore
    public i12 p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public f12[] v;
    @DexIgnore
    public f12[] w;

    @DexIgnore
    public g12() {
        a();
    }

    @DexIgnore
    public static g12[] b() {
        if (x == null) {
            synchronized (k12.a) {
                if (x == null) {
                    x = new g12[0];
                }
            }
        }
        return x;
    }

    @DexIgnore
    public g12 a() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = 0;
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = f12.b();
        this.w = f12.b();
        return this;
    }

    @DexIgnore
    public g12 a(j12 j12) throws IOException {
        while (true) {
            int j2 = j12.j();
            switch (j2) {
                case 0:
                    return this;
                case 10:
                    if (this.a == null) {
                        this.a = new i12();
                    }
                    j12.a((l12) this.a);
                    break;
                case 18:
                    if (this.b == null) {
                        this.b = new i12();
                    }
                    j12.a((l12) this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new i12();
                    }
                    j12.a((l12) this.c);
                    break;
                case 34:
                    if (this.d == null) {
                        this.d = new i12();
                    }
                    j12.a((l12) this.d);
                    break;
                case 42:
                    if (this.e == null) {
                        this.e = new i12();
                    }
                    j12.a((l12) this.e);
                    break;
                case 50:
                    if (this.f == null) {
                        this.f = new i12();
                    }
                    j12.a((l12) this.f);
                    break;
                case 58:
                    if (this.g == null) {
                        this.g = new i12();
                    }
                    j12.a((l12) this.g);
                    break;
                case 66:
                    if (this.h == null) {
                        this.h = new i12();
                    }
                    j12.a((l12) this.h);
                    break;
                case 74:
                    j12.i();
                    break;
                case 80:
                    this.q = j12.d();
                    break;
                case 90:
                    this.r = j12.i();
                    break;
                case 98:
                    j12.i();
                    break;
                case 106:
                    this.s = j12.i();
                    break;
                case 122:
                    this.t = j12.i();
                    break;
                case 130:
                    this.u = j12.i();
                    break;
                case 138:
                    j12.i();
                    break;
                case 144:
                    j12.c();
                    break;
                case 154:
                    int a2 = n12.a(j12, 154);
                    f12[] f12Arr = this.v;
                    int length = f12Arr == null ? 0 : f12Arr.length;
                    f12[] f12Arr2 = new f12[(a2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.v, 0, f12Arr2, 0, length);
                    }
                    while (length < f12Arr2.length - 1) {
                        f12Arr2[length] = new f12();
                        j12.a((l12) f12Arr2[length]);
                        j12.j();
                        length++;
                    }
                    f12Arr2[length] = new f12();
                    j12.a((l12) f12Arr2[length]);
                    this.v = f12Arr2;
                    break;
                case 162:
                    int a3 = n12.a(j12, 162);
                    f12[] f12Arr3 = this.w;
                    int length2 = f12Arr3 == null ? 0 : f12Arr3.length;
                    f12[] f12Arr4 = new f12[(a3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.w, 0, f12Arr4, 0, length2);
                    }
                    while (length2 < f12Arr4.length - 1) {
                        f12Arr4[length2] = new f12();
                        j12.a((l12) f12Arr4[length2]);
                        j12.j();
                        length2++;
                    }
                    f12Arr4[length2] = new f12();
                    j12.a((l12) f12Arr4[length2]);
                    this.w = f12Arr4;
                    break;
                case 170:
                    if (this.i == null) {
                        this.i = new i12();
                    }
                    j12.a((l12) this.i);
                    break;
                case 176:
                    j12.c();
                    break;
                case 186:
                    j12.i();
                    break;
                case 194:
                    if (this.p == null) {
                        this.p = new i12();
                    }
                    j12.a((l12) this.p);
                    break;
                case Action.Selfie.TAKE_BURST /*202*/:
                    if (this.j == null) {
                        this.j = new i12();
                    }
                    j12.a((l12) this.j);
                    break;
                case 208:
                    j12.c();
                    break;
                case 218:
                    if (this.k == null) {
                        this.k = new i12();
                    }
                    j12.a((l12) this.k);
                    break;
                case 226:
                    if (this.l == null) {
                        this.l = new i12();
                    }
                    j12.a((l12) this.l);
                    break;
                case 234:
                    if (this.m == null) {
                        this.m = new i12();
                    }
                    j12.a((l12) this.m);
                    break;
                case 242:
                    if (this.n == null) {
                        this.n = new i12();
                    }
                    j12.a((l12) this.n);
                    break;
                case 250:
                    if (this.o == null) {
                        this.o = new i12();
                    }
                    j12.a((l12) this.o);
                    break;
                case 256:
                    j12.c();
                    break;
                default:
                    if (n12.b(j12, j2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
