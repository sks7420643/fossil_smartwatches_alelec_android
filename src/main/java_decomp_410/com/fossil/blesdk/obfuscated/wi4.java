package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wi4 {
    @DexIgnore
    public static final <T> void a(yb4<? super T> yb4, T t, int i) {
        kd4.b(yb4, "$this$resumeMode");
        if (i == 0) {
            Result.a aVar = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 1) {
            lh4.a(yb4, t);
        } else if (i == 2) {
            lh4.b(yb4, t);
        } else if (i == 3) {
            jh4 jh4 = (jh4) yb4;
            CoroutineContext context = jh4.getContext();
            Object b = ThreadContextKt.b(context, jh4.j);
            try {
                yb4<T> yb42 = jh4.l;
                Result.a aVar2 = Result.Companion;
                yb42.resumeWith(Result.m3constructorimpl(t));
                qa4 qa4 = qa4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean a(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final <T> void b(yb4<? super T> yb4, Throwable th, int i) {
        kd4.b(yb4, "$this$resumeWithExceptionMode");
        kd4.b(th, "exception");
        if (i == 0) {
            Result.a aVar = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        } else if (i == 1) {
            lh4.a(yb4, th);
        } else if (i == 2) {
            lh4.b(yb4, th);
        } else if (i == 3) {
            jh4 jh4 = (jh4) yb4;
            CoroutineContext context = jh4.getContext();
            Object b = ThreadContextKt.b(context, jh4.j);
            try {
                yb4<T> yb42 = jh4.l;
                Result.a aVar2 = Result.Companion;
                yb42.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(th, (yb4<?>) yb42))));
                qa4 qa4 = qa4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void a(yb4<? super T> yb4, Throwable th, int i) {
        kd4.b(yb4, "$this$resumeUninterceptedWithExceptionMode");
        kd4.b(th, "exception");
        if (i == 0) {
            yb4<? super T> a = IntrinsicsKt__IntrinsicsJvmKt.a(yb4);
            Result.a aVar = Result.Companion;
            a.resumeWith(Result.m3constructorimpl(na4.a(th)));
        } else if (i == 1) {
            lh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), th);
        } else if (i == 2) {
            Result.a aVar2 = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        } else if (i == 3) {
            CoroutineContext context = yb4.getContext();
            Object b = ThreadContextKt.b(context, (Object) null);
            try {
                Result.a aVar3 = Result.Companion;
                yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
                qa4 qa4 = qa4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final <T> void b(yb4<? super T> yb4, T t, int i) {
        kd4.b(yb4, "$this$resumeUninterceptedMode");
        if (i == 0) {
            yb4<? super T> a = IntrinsicsKt__IntrinsicsJvmKt.a(yb4);
            Result.a aVar = Result.Companion;
            a.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 1) {
            lh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), t);
        } else if (i == 2) {
            Result.a aVar2 = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(t));
        } else if (i == 3) {
            CoroutineContext context = yb4.getContext();
            Object b = ThreadContextKt.b(context, (Object) null);
            try {
                Result.a aVar3 = Result.Companion;
                yb4.resumeWith(Result.m3constructorimpl(t));
                qa4 qa4 = qa4.a;
            } finally {
                ThreadContextKt.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }
}
