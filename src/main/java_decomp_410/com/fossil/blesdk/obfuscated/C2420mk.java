package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mk */
public class C2420mk extends com.fossil.blesdk.obfuscated.C2331lk<com.fossil.blesdk.obfuscated.C1878gk> {
    @DexIgnore
    public C2420mk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(com.fossil.blesdk.obfuscated.C3294xk.m16376a(context, zlVar).mo17770c());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12371a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        return hlVar.f5780j.mo18107b() == androidx.work.NetworkType.CONNECTED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12373b(com.fossil.blesdk.obfuscated.C1878gk gkVar) {
        if (android.os.Build.VERSION.SDK_INT < 26) {
            return !gkVar.mo11251a();
        }
        if (!gkVar.mo11251a() || !gkVar.mo11254d()) {
            return true;
        }
        return false;
    }
}
