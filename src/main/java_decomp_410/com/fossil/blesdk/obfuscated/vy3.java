package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vy3 {
    @DexIgnore
    public static vy3 c;
    @DexIgnore
    public Map<Integer, uy3> a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public vy3(Context context) {
        this.b = context.getApplicationContext();
        this.a = new HashMap(3);
        this.a.put(1, new ty3(context));
        this.a.put(2, new qy3(context));
        this.a.put(4, new sy3(context));
    }

    @DexIgnore
    public static synchronized vy3 a(Context context) {
        vy3 vy3;
        synchronized (vy3.class) {
            if (c == null) {
                c = new vy3(context);
            }
            vy3 = c;
        }
        return vy3;
    }

    @DexIgnore
    public final ry3 a() {
        return a((List<Integer>) new ArrayList(Arrays.asList(new Integer[]{1, 2, 4})));
    }

    @DexIgnore
    public final ry3 a(List<Integer> list) {
        if (list.size() >= 0) {
            for (Integer num : list) {
                uy3 uy3 = this.a.get(num);
                if (uy3 != null) {
                    ry3 c2 = uy3.c();
                    if (c2 != null && wy3.b(c2.c)) {
                        return c2;
                    }
                }
            }
        }
        return new ry3();
    }

    @DexIgnore
    public final void a(String str) {
        ry3 a2 = a();
        a2.c = str;
        if (!wy3.a(a2.a)) {
            a2.a = wy3.a(this.b);
        }
        if (!wy3.a(a2.b)) {
            a2.b = wy3.b(this.b);
        }
        a2.d = System.currentTimeMillis();
        for (Map.Entry<Integer, uy3> value : this.a.entrySet()) {
            ((uy3) value.getValue()).a(a2);
        }
    }
}
