package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rr2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ in2 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return rr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<BaseActivity> a;

        @DexIgnore
        public b(WeakReference<BaseActivity> weakReference) {
            kd4.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<BaseActivity> a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, ud0 ud0) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            kd4.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = rr2.class.getSimpleName();
        kd4.a((Object) simpleName, "LoginFacebookUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public rr2(in2 in2) {
        kd4.b(in2, "mLoginFacebookManager");
        this.d = in2;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ln2 {
        @DexIgnore
        public /* final */ /* synthetic */ rr2 a;

        @DexIgnore
        public e(rr2 rr2) {
            this.a = rr2;
        }

        @DexIgnore
        public void a(SignUpSocialAuth signUpSocialAuth) {
            kd4.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rr2.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        public void a(int i, ud0 ud0, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = rr2.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + ud0);
            this.a.a(new c(i, ud0));
        }
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.W.c().w()) {
                return a(new c(601, (ud0) null));
            }
            in2 in2 = this.d;
            WeakReference<BaseActivity> a2 = bVar != null ? bVar.a() : null;
            if (a2 != null) {
                Object obj = a2.get();
                if (obj != null) {
                    kd4.a(obj, "requestValues?.activityContext!!.get()!!");
                    in2.a((Activity) obj, (ln2) new e(this));
                    return qa4.a;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new c(600, (ud0) null);
        }
    }
}
