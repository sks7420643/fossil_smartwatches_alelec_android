package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o42 implements Factory<AlarmHelper> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<en2> c;
    @DexIgnore
    public /* final */ Provider<UserRepository> d;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> e;

    @DexIgnore
    public o42(n42 n42, Provider<Context> provider, Provider<en2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
    }

    @DexIgnore
    public static o42 a(n42 n42, Provider<Context> provider, Provider<en2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        return new o42(n42, provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static AlarmHelper b(n42 n42, Provider<Context> provider, Provider<en2> provider2, Provider<UserRepository> provider3, Provider<AlarmsRepository> provider4) {
        return a(n42, provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public static AlarmHelper a(n42 n42, Context context, en2 en2, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        AlarmHelper a2 = n42.a(context, en2, userRepository, alarmsRepository);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AlarmHelper get() {
        return b(this.a, this.b, this.c, this.d, this.e);
    }
}
