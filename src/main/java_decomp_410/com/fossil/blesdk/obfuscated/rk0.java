package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rk0 extends mk0 {
    @DexIgnore
    public /* final */ ue0<Status> e;

    @DexIgnore
    public rk0(ue0<Status> ue0) {
        this.e = ue0;
    }

    @DexIgnore
    public final void e(int i) throws RemoteException {
        this.e.a(new Status(i));
    }
}
