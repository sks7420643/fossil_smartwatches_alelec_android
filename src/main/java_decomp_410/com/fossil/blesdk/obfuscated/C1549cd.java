package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cd */
public final class C1549cd implements com.fossil.blesdk.obfuscated.C1474bd {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.media.session.MediaSessionManager.RemoteUserInfo f4075a;

    @DexIgnore
    public C1549cd(java.lang.String str, int i, int i2) {
        this.f4075a = new android.media.session.MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1549cd)) {
            return false;
        }
        return this.f4075a.equals(((com.fossil.blesdk.obfuscated.C1549cd) obj).f4075a);
    }

    @DexIgnore
    public int hashCode() {
        return com.fossil.blesdk.obfuscated.C1706e8.m6325a(this.f4075a);
    }
}
