package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn1 extends ks0 implements cn1 {
    @DexIgnore
    public dn1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @DexIgnore
    public final void a(tj0 tj0, int i, boolean z) throws RemoteException {
        Parcel o = o();
        ms0.a(o, (IInterface) tj0);
        o.writeInt(i);
        ms0.a(o, z);
        b(9, o);
    }

    @DexIgnore
    public final void c(int i) throws RemoteException {
        Parcel o = o();
        o.writeInt(i);
        b(7, o);
    }

    @DexIgnore
    public final void a(en1 en1, an1 an1) throws RemoteException {
        Parcel o = o();
        ms0.a(o, (Parcelable) en1);
        ms0.a(o, (IInterface) an1);
        b(12, o);
    }
}
