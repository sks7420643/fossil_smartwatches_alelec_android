package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xp3 implements Factory<WatchSettingViewModel> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<SetVibrationStrengthUseCase> b;
    @DexIgnore
    public /* final */ Provider<UnlinkDeviceUseCase> c;
    @DexIgnore
    public /* final */ Provider<vj2> d;
    @DexIgnore
    public /* final */ Provider<en2> e;
    @DexIgnore
    public /* final */ Provider<SwitchActiveDeviceUseCase> f;
    @DexIgnore
    public /* final */ Provider<tq2> g;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> h;

    @DexIgnore
    public xp3(Provider<DeviceRepository> provider, Provider<SetVibrationStrengthUseCase> provider2, Provider<UnlinkDeviceUseCase> provider3, Provider<vj2> provider4, Provider<en2> provider5, Provider<SwitchActiveDeviceUseCase> provider6, Provider<tq2> provider7, Provider<PortfolioApp> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static xp3 a(Provider<DeviceRepository> provider, Provider<SetVibrationStrengthUseCase> provider2, Provider<UnlinkDeviceUseCase> provider3, Provider<vj2> provider4, Provider<en2> provider5, Provider<SwitchActiveDeviceUseCase> provider6, Provider<tq2> provider7, Provider<PortfolioApp> provider8) {
        return new xp3(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static WatchSettingViewModel b(Provider<DeviceRepository> provider, Provider<SetVibrationStrengthUseCase> provider2, Provider<UnlinkDeviceUseCase> provider3, Provider<vj2> provider4, Provider<en2> provider5, Provider<SwitchActiveDeviceUseCase> provider6, Provider<tq2> provider7, Provider<PortfolioApp> provider8) {
        return new WatchSettingViewModel(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get());
    }

    @DexIgnore
    public WatchSettingViewModel get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
