package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.fossil.blesdk.obfuscated.p1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class o1 implements j1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ h1 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public View f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public p1.a i;
    @DexIgnore
    public n1 j;
    @DexIgnore
    public PopupWindow.OnDismissListener k;
    @DexIgnore
    public /* final */ PopupWindow.OnDismissListener l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements PopupWindow.OnDismissListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onDismiss() {
            o1.this.e();
        }
    }

    @DexIgnore
    public o1(Context context, h1 h1Var, View view, boolean z, int i2) {
        this(context, h1Var, view, z, i2, 0);
    }

    @DexIgnore
    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    @DexIgnore
    public void b() {
        if (d()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    public n1 c() {
        if (this.j == null) {
            this.j = a();
        }
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        n1 n1Var = this.j;
        return n1Var != null && n1Var.d();
    }

    @DexIgnore
    public void e() {
        this.j = null;
        PopupWindow.OnDismissListener onDismissListener = this.k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public void f() {
        if (!g()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    public boolean g() {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public o1(Context context, h1 h1Var, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new a();
        this.a = context;
        this.b = h1Var;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public void a(View view) {
        this.f = view;
    }

    @DexIgnore
    public void a(boolean z) {
        this.h = z;
        n1 n1Var = this.j;
        if (n1Var != null) {
            n1Var.b(z);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        a(i2, i3, true, true);
        return true;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.fossil.blesdk.obfuscated.n1, com.fossil.blesdk.obfuscated.p1] */
    /* JADX WARNING: type inference failed for: r7v1, types: [com.fossil.blesdk.obfuscated.u1] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.fossil.blesdk.obfuscated.e1] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final n1 a() {
        Object r0;
        Display defaultDisplay = ((WindowManager) this.a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        if (Math.min(point.x, point.y) >= this.a.getResources().getDimensionPixelSize(u.abc_cascading_menus_min_smallest_width)) {
            r0 = new e1(this.a, this.f, this.d, this.e, this.c);
        } else {
            r0 = new u1(this.a, this.b, this.f, this.d, this.e, this.c);
        }
        r0.a(this.b);
        r0.a(this.l);
        r0.a(this.f);
        r0.a(this.i);
        r0.b(this.h);
        r0.a(this.g);
        return r0;
    }

    @DexIgnore
    public final void a(int i2, int i3, boolean z, boolean z2) {
        n1 c2 = c();
        c2.c(z2);
        if (z) {
            if ((p8.a(this.g, f9.k(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            c2.b(i2);
            c2.c(i3);
            int i4 = (int) ((this.a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c2.a(new Rect(i2 - i4, i3 - i4, i2 + i4, i3 + i4));
        }
        c2.c();
    }

    @DexIgnore
    public void a(p1.a aVar) {
        this.i = aVar;
        n1 n1Var = this.j;
        if (n1Var != null) {
            n1Var.a(aVar);
        }
    }
}
