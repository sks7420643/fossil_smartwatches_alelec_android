package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class i82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon7.Anon1 e;
    @DexIgnore
    private /* final */ /* synthetic */ List f;
    @DexIgnore
    private /* final */ /* synthetic */ String g;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.GetActivePresetCallback h;

    @DexIgnore
    public /* synthetic */ i82(PresetRepository.Anon7.Anon1 anon1, List list, String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        this.e = anon1;
        this.f = list;
        this.g = str;
        this.h = getActivePresetCallback;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g, this.h);
    }
}
