package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.o81;
import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j81<T extends o81<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract m81<T> a(Object obj);

    @DexIgnore
    public abstract Object a(i81 i81, w91 w91, int i);

    @DexIgnore
    public abstract <UT, UB> UB a(ma1 ma1, Object obj, i81 i81, m81<T> m81, UB ub, eb1<UT, UB> eb1) throws IOException;

    @DexIgnore
    public abstract void a(ma1 ma1, Object obj, i81 i81, m81<T> m81) throws IOException;

    @DexIgnore
    public abstract void a(sb1 sb1, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract void a(zzte zzte, Object obj, i81 i81, m81<T> m81) throws IOException;

    @DexIgnore
    public abstract boolean a(w91 w91);

    @DexIgnore
    public abstract m81<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
