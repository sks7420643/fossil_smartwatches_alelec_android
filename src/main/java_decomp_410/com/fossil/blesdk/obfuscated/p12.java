package com.fossil.blesdk.obfuscated;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.oned.Code128Writer;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p12 implements r12 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[BarcodeFormat.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|(3:25|26|28)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|28) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[BarcodeFormat.EAN_8.ordinal()] = 1;
            a[BarcodeFormat.UPC_E.ordinal()] = 2;
            a[BarcodeFormat.EAN_13.ordinal()] = 3;
            a[BarcodeFormat.UPC_A.ordinal()] = 4;
            a[BarcodeFormat.QR_CODE.ordinal()] = 5;
            a[BarcodeFormat.CODE_39.ordinal()] = 6;
            a[BarcodeFormat.CODE_93.ordinal()] = 7;
            a[BarcodeFormat.CODE_128.ordinal()] = 8;
            a[BarcodeFormat.ITF.ordinal()] = 9;
            a[BarcodeFormat.PDF_417.ordinal()] = 10;
            a[BarcodeFormat.CODABAR.ordinal()] = 11;
            a[BarcodeFormat.DATA_MATRIX.ordinal()] = 12;
            try {
                a[BarcodeFormat.AZTEC.ordinal()] = 13;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public b22 a(String str, BarcodeFormat barcodeFormat, int i, int i2, Map<EncodeHintType, ?> map) throws WriterException {
        r12 r12;
        switch (a.a[barcodeFormat.ordinal()]) {
            case 1:
                r12 = new c32();
                break;
            case 2:
                r12 = new l32();
                break;
            case 3:
                r12 = new b32();
                break;
            case 4:
                r12 = new h32();
                break;
            case 5:
                r12 = new t32();
                break;
            case 6:
                r12 = new x22();
                break;
            case 7:
                r12 = new z22();
                break;
            case 8:
                r12 = new Code128Writer();
                break;
            case 9:
                r12 = new e32();
                break;
            case 10:
                r12 = new m32();
                break;
            case 11:
                r12 = new u22();
                break;
            case 12:
                r12 = new f22();
                break;
            case 13:
                r12 = new s12();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format " + barcodeFormat);
        }
        return r12.a(str, barcodeFormat, i, i2, map);
    }
}
