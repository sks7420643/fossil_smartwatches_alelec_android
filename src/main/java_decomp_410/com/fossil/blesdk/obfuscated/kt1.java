package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kt1 extends GradientDrawable {
    @DexIgnore
    public /* final */ Paint a; // = new Paint(1);
    @DexIgnore
    public /* final */ RectF b;
    @DexIgnore
    public int c;

    @DexIgnore
    public kt1() {
        c();
        this.b = new RectF();
    }

    @DexIgnore
    public boolean a() {
        return !this.b.isEmpty();
    }

    @DexIgnore
    public void b() {
        a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void c() {
        this.a.setStyle(Paint.Style.FILL_AND_STROKE);
        this.a.setColor(-1);
        this.a.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        b(canvas);
        super.draw(canvas);
        canvas.drawRect(this.b, this.a);
        a(canvas);
    }

    @DexIgnore
    public void a(float f, float f2, float f3, float f4) {
        RectF rectF = this.b;
        if (f != rectF.left || f2 != rectF.top || f3 != rectF.right || f4 != rectF.bottom) {
            this.b.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (a(callback)) {
            ((View) callback).setLayerType(2, (Paint) null);
        } else {
            c(canvas);
        }
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.c = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), (Paint) null);
            return;
        }
        this.c = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), (Paint) null, 31);
    }

    @DexIgnore
    public void a(RectF rectF) {
        a(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        if (!a(getCallback())) {
            canvas.restoreToCount(this.c);
        }
    }

    @DexIgnore
    public final boolean a(Drawable.Callback callback) {
        return callback instanceof View;
    }
}
