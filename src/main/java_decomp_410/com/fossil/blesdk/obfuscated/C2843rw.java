package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rw */
public class C2843rw extends java.io.FilterInputStream {

    @DexIgnore
    /* renamed from: e */
    public int f9144e; // = Integer.MIN_VALUE;

    @DexIgnore
    public C2843rw(java.io.InputStream inputStream) {
        super(inputStream);
    }

    @DexIgnore
    public int available() throws java.io.IOException {
        int i = this.f9144e;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return java.lang.Math.min(i, super.available());
    }

    @DexIgnore
    /* renamed from: h */
    public final long mo15771h(long j) {
        int i = this.f9144e;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo15772i(long j) {
        int i = this.f9144e;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.f9144e = (int) (((long) i) - j);
        }
    }

    @DexIgnore
    public synchronized void mark(int i) {
        super.mark(i);
        this.f9144e = i;
    }

    @DexIgnore
    public int read() throws java.io.IOException {
        if (mo15771h(1) == -1) {
            return -1;
        }
        int read = super.read();
        mo15772i(1);
        return read;
    }

    @DexIgnore
    public synchronized void reset() throws java.io.IOException {
        super.reset();
        this.f9144e = Integer.MIN_VALUE;
    }

    @DexIgnore
    public long skip(long j) throws java.io.IOException {
        long h = mo15771h(j);
        if (h == -1) {
            return 0;
        }
        long skip = super.skip(h);
        mo15772i(skip);
        return skip;
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) throws java.io.IOException {
        int h = (int) mo15771h((long) i2);
        if (h == -1) {
            return -1;
        }
        int read = super.read(bArr, i, h);
        mo15772i((long) read);
        return read;
    }
}
