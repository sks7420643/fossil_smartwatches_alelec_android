package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qc2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ CustomizeWidget v;
    @DexIgnore
    public /* final */ CustomizeWidget w;
    @DexIgnore
    public /* final */ CustomizeWidget x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qc2(Object obj, View view, int i, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ConstraintLayout constraintLayout, LinearLayout linearLayout, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, TextView textView, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = imageView;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView4;
        this.u = flexibleTextView7;
        this.v = customizeWidget;
        this.w = customizeWidget2;
        this.x = customizeWidget3;
    }
}
