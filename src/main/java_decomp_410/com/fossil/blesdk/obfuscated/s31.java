package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class s31 extends g41 implements r31 {
    @DexIgnore
    public s31() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a((o31) q41.a(parcel, o31.CREATOR));
        return true;
    }
}
