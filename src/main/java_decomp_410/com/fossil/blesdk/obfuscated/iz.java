package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iz implements Report {
    @DexIgnore
    public /* final */ File[] a;
    @DexIgnore
    public /* final */ Map<String, String> b; // = new HashMap(xz.g);
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public iz(String str, File[] fileArr) {
        this.a = fileArr;
        this.c = str;
    }

    @DexIgnore
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public File c() {
        return this.a[0];
    }

    @DexIgnore
    public File[] d() {
        return this.a;
    }

    @DexIgnore
    public String e() {
        return this.a[0].getName();
    }

    @DexIgnore
    public Report.Type getType() {
        return Report.Type.JAVA;
    }

    @DexIgnore
    public void remove() {
        for (File file : this.a) {
            q44.g().d("CrashlyticsCore", "Removing invalid report file at " + file.getPath());
            file.delete();
        }
    }
}
