package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f91 {
    @DexIgnore
    public volatile w91 a;
    @DexIgnore
    public volatile zzte b;

    /*
    static {
        i81.c();
    }
    */

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    public final w91 a(w91 w91) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = w91;
                    this.b = zzte.zzbts;
                    this.a = w91;
                    this.b = zzte.zzbts;
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public final w91 b(w91 w91) {
        w91 w912 = this.a;
        this.b = null;
        this.a = w91;
        return w912;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f91)) {
            return false;
        }
        f91 f91 = (f91) obj;
        w91 w91 = this.a;
        w91 w912 = f91.a;
        if (w91 == null && w912 == null) {
            return a().equals(f91.a());
        }
        if (w91 != null && w912 != null) {
            return w91.equals(w912);
        }
        if (w91 != null) {
            return w91.equals(f91.a(w91.b()));
        }
        return a(w912.b()).equals(w912);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final int b() {
        if (this.b != null) {
            return this.b.size();
        }
        if (this.a != null) {
            return this.a.e();
        }
        return 0;
    }

    @DexIgnore
    public final zzte a() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                zzte zzte = this.b;
                return zzte;
            }
            if (this.a == null) {
                this.b = zzte.zzbts;
            } else {
                this.b = this.a.d();
            }
            zzte zzte2 = this.b;
            return zzte2;
        }
    }
}
