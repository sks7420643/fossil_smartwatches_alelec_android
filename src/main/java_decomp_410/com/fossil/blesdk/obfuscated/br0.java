package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public class br0 {
    @DexIgnore
    public static Map<String, br0> b; // = new g4();
    @DexIgnore
    public static jr0 c;
    @DexIgnore
    public String a; // = "";

    /*
    static {
        hr0.a().a("gcm_check_for_different_iid_in_token", true);
        TimeUnit.DAYS.toMillis(7);
    }
    */

    @DexIgnore
    public br0(Context context, String str) {
        context.getApplicationContext();
        this.a = str;
    }

    @DexIgnore
    public static int a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 38);
            sb.append("Never happens: can't find own package ");
            sb.append(valueOf);
            Log.w("InstanceID", sb.toString());
            return 0;
        }
    }

    @DexIgnore
    public static jr0 b() {
        return c;
    }

    @DexIgnore
    public static synchronized br0 a(Context context, Bundle bundle) {
        br0 br0;
        synchronized (br0.class) {
            String string = bundle == null ? "" : bundle.getString("subtype");
            if (string == null) {
                string = "";
            }
            Context applicationContext = context.getApplicationContext();
            if (c == null) {
                String packageName = applicationContext.getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 73);
                sb.append("Instance ID SDK is deprecated, ");
                sb.append(packageName);
                sb.append(" should update to use Firebase Instance ID");
                Log.w("InstanceID", sb.toString());
                c = new jr0(applicationContext);
                new gr0(applicationContext);
            }
            Integer.toString(a(applicationContext));
            br0 = b.get(string);
            if (br0 == null) {
                br0 = new br0(applicationContext, string);
                b.put(string, br0);
            }
        }
        return br0;
    }

    @DexIgnore
    public final void a() {
        c.b(this.a);
    }
}
