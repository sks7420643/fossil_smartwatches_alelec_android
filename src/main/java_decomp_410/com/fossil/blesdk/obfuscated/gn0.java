package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gn0 extends vl0 {
    @DexIgnore
    public int e;

    @DexIgnore
    public gn0(byte[] bArr) {
        bk0.a(bArr.length == 25);
        this.e = Arrays.hashCode(bArr);
    }

    @DexIgnore
    public static byte[] b(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof ul0)) {
            try {
                ul0 ul0 = (ul0) obj;
                if (ul0.zzc() != hashCode()) {
                    return false;
                }
                sn0 zzb = ul0.zzb();
                if (zzb == null) {
                    return false;
                }
                return Arrays.equals(o(), (byte[]) un0.d(zzb));
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.e;
    }

    @DexIgnore
    public abstract byte[] o();

    @DexIgnore
    public final sn0 zzb() {
        return un0.a(o());
    }

    @DexIgnore
    public final int zzc() {
        return hashCode();
    }
}
