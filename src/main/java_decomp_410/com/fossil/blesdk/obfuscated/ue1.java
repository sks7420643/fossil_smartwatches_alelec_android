package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ue1 extends a51 implements ie1 {
    @DexIgnore
    public ue1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    public final sn0 a(float f) throws RemoteException {
        Parcel o = o();
        o.writeFloat(f);
        Parcel a = a(4, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final sn0 a(LatLng latLng, float f) throws RemoteException {
        Parcel o = o();
        c51.a(o, (Parcelable) latLng);
        o.writeFloat(f);
        Parcel a = a(9, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
