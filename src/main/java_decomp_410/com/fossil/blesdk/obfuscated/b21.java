package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class b21 extends j01 implements a21 {
    @DexIgnore
    public b21() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((w11) y01.a(parcel, w11.CREATOR), p01.a(parcel.readStrongBinder()));
        } else if (i == 2) {
            a((kq0) y01.a(parcel, kq0.CREATOR), h11.a(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            a((y11) y01.a(parcel, y11.CREATOR), h11.a(parcel.readStrongBinder()));
        }
        return true;
    }
}
