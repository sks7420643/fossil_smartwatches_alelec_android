package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hl */
public class C1954hl {

    @DexIgnore
    /* renamed from: a */
    public java.lang.String f5771a;

    @DexIgnore
    /* renamed from: b */
    public androidx.work.WorkInfo.State f5772b; // = androidx.work.WorkInfo.State.ENQUEUED;

    @DexIgnore
    /* renamed from: c */
    public java.lang.String f5773c;

    @DexIgnore
    /* renamed from: d */
    public java.lang.String f5774d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1419aj f5775e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1419aj f5776f;

    @DexIgnore
    /* renamed from: g */
    public long f5777g;

    @DexIgnore
    /* renamed from: h */
    public long f5778h;

    @DexIgnore
    /* renamed from: i */
    public long f5779i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C3365yi f5780j;

    @DexIgnore
    /* renamed from: k */
    public int f5781k;

    @DexIgnore
    /* renamed from: l */
    public androidx.work.BackoffPolicy f5782l;

    @DexIgnore
    /* renamed from: m */
    public long f5783m;

    @DexIgnore
    /* renamed from: n */
    public long f5784n;

    @DexIgnore
    /* renamed from: o */
    public long f5785o;

    @DexIgnore
    /* renamed from: p */
    public long f5786p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hl$a */
    public static class C1955a implements com.fossil.blesdk.obfuscated.C2374m3<java.util.List<com.fossil.blesdk.obfuscated.C1954hl.C1957c>, java.util.List<androidx.work.WorkInfo>> {
        @DexIgnore
        /* renamed from: a */
        public java.util.List<androidx.work.WorkInfo> apply(java.util.List<com.fossil.blesdk.obfuscated.C1954hl.C1957c> list) {
            if (list == null) {
                return null;
            }
            java.util.ArrayList arrayList = new java.util.ArrayList(list.size());
            for (com.fossil.blesdk.obfuscated.C1954hl.C1957c a : list) {
                arrayList.add(a.mo11681a());
            }
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hl$b")
    /* renamed from: com.fossil.blesdk.obfuscated.hl$b */
    public static class C1956b {

        @DexIgnore
        /* renamed from: a */
        public java.lang.String f5787a;

        @DexIgnore
        /* renamed from: b */
        public androidx.work.WorkInfo.State f5788b;

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1954hl.C1956b.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1954hl.C1956b bVar = (com.fossil.blesdk.obfuscated.C1954hl.C1956b) obj;
            if (this.f5788b != bVar.f5788b) {
                return false;
            }
            return this.f5787a.equals(bVar.f5787a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.f5787a.hashCode() * 31) + this.f5788b.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hl$c")
    /* renamed from: com.fossil.blesdk.obfuscated.hl$c */
    public static class C1957c {

        @DexIgnore
        /* renamed from: a */
        public java.lang.String f5789a;

        @DexIgnore
        /* renamed from: b */
        public androidx.work.WorkInfo.State f5790b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1419aj f5791c;

        @DexIgnore
        /* renamed from: d */
        public int f5792d;

        @DexIgnore
        /* renamed from: e */
        public java.util.List<java.lang.String> f5793e;

        @DexIgnore
        /* renamed from: f */
        public java.util.List<com.fossil.blesdk.obfuscated.C1419aj> f5794f;

        @DexIgnore
        /* renamed from: a */
        public androidx.work.WorkInfo mo11681a() {
            java.util.List<com.fossil.blesdk.obfuscated.C1419aj> list = this.f5794f;
            androidx.work.WorkInfo workInfo = new androidx.work.WorkInfo(java.util.UUID.fromString(this.f5789a), this.f5790b, this.f5791c, this.f5793e, (list == null || list.isEmpty()) ? com.fossil.blesdk.obfuscated.C1419aj.f3496c : this.f5794f.get(0), this.f5792d);
            return workInfo;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1954hl.C1957c.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1954hl.C1957c cVar = (com.fossil.blesdk.obfuscated.C1954hl.C1957c) obj;
            if (this.f5792d != cVar.f5792d) {
                return false;
            }
            java.lang.String str = this.f5789a;
            if (str == null ? cVar.f5789a != null : !str.equals(cVar.f5789a)) {
                return false;
            }
            if (this.f5790b != cVar.f5790b) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1419aj ajVar = this.f5791c;
            if (ajVar == null ? cVar.f5791c != null : !ajVar.equals(cVar.f5791c)) {
                return false;
            }
            java.util.List<java.lang.String> list = this.f5793e;
            if (list == null ? cVar.f5793e != null : !list.equals(cVar.f5793e)) {
                return false;
            }
            java.util.List<com.fossil.blesdk.obfuscated.C1419aj> list2 = this.f5794f;
            java.util.List<com.fossil.blesdk.obfuscated.C1419aj> list3 = cVar.f5794f;
            if (list2 != null) {
                return list2.equals(list3);
            }
            if (list3 == null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            java.lang.String str = this.f5789a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            androidx.work.WorkInfo.State state = this.f5790b;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            com.fossil.blesdk.obfuscated.C1419aj ajVar = this.f5791c;
            int hashCode3 = (((hashCode2 + (ajVar != null ? ajVar.hashCode() : 0)) * 31) + this.f5792d) * 31;
            java.util.List<java.lang.String> list = this.f5793e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            java.util.List<com.fossil.blesdk.obfuscated.C1419aj> list2 = this.f5794f;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode4 + i;
        }
    }

    /*
    static {
        com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkSpec");
        new com.fossil.blesdk.obfuscated.C1954hl.C1955a();
    }
    */

    @DexIgnore
    public C1954hl(java.lang.String str, java.lang.String str2) {
        com.fossil.blesdk.obfuscated.C1419aj ajVar = com.fossil.blesdk.obfuscated.C1419aj.f3496c;
        this.f5775e = ajVar;
        this.f5776f = ajVar;
        this.f5780j = com.fossil.blesdk.obfuscated.C3365yi.f11259i;
        this.f5782l = androidx.work.BackoffPolicy.EXPONENTIAL;
        this.f5783m = 30000;
        this.f5786p = -1;
        this.f5771a = str;
        this.f5773c = str2;
    }

    @DexIgnore
    /* renamed from: a */
    public long mo11670a() {
        long j;
        boolean z = false;
        if (mo11672c()) {
            if (this.f5782l == androidx.work.BackoffPolicy.LINEAR) {
                z = true;
            }
            if (z) {
                j = this.f5783m * ((long) this.f5781k);
            } else {
                j = (long) java.lang.Math.scalb((float) this.f5783m, this.f5781k - 1);
            }
            return this.f5784n + java.lang.Math.min(18000000, j);
        }
        long j2 = 0;
        if (mo11673d()) {
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            long j3 = this.f5784n;
            if (j3 == 0) {
                j3 = this.f5777g + currentTimeMillis;
            }
            if (this.f5779i != this.f5778h) {
                z = true;
            }
            if (z) {
                if (this.f5784n == 0) {
                    j2 = this.f5779i * -1;
                }
                return j3 + this.f5778h + j2;
            }
            if (this.f5784n != 0) {
                j2 = this.f5778h;
            }
            return j3 + j2;
        }
        long j4 = this.f5784n;
        if (j4 == 0) {
            j4 = java.lang.System.currentTimeMillis();
        }
        return j4 + this.f5777g;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo11671b() {
        return !com.fossil.blesdk.obfuscated.C3365yi.f11259i.equals(this.f5780j);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo11672c() {
        return this.f5772b == androidx.work.WorkInfo.State.ENQUEUED && this.f5781k > 0;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo11673d() {
        return this.f5778h != 0;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C1954hl.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1954hl hlVar = (com.fossil.blesdk.obfuscated.C1954hl) obj;
        if (this.f5777g != hlVar.f5777g || this.f5778h != hlVar.f5778h || this.f5779i != hlVar.f5779i || this.f5781k != hlVar.f5781k || this.f5783m != hlVar.f5783m || this.f5784n != hlVar.f5784n || this.f5785o != hlVar.f5785o || this.f5786p != hlVar.f5786p || !this.f5771a.equals(hlVar.f5771a) || this.f5772b != hlVar.f5772b || !this.f5773c.equals(hlVar.f5773c)) {
            return false;
        }
        java.lang.String str = this.f5774d;
        if (str == null ? hlVar.f5774d != null : !str.equals(hlVar.f5774d)) {
            return false;
        }
        if (this.f5775e.equals(hlVar.f5775e) && this.f5776f.equals(hlVar.f5776f) && this.f5780j.equals(hlVar.f5780j) && this.f5782l == hlVar.f5782l) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((this.f5771a.hashCode() * 31) + this.f5772b.hashCode()) * 31) + this.f5773c.hashCode()) * 31;
        java.lang.String str = this.f5774d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j = this.f5777g;
        long j2 = this.f5778h;
        long j3 = this.f5779i;
        long j4 = this.f5783m;
        long j5 = this.f5784n;
        long j6 = this.f5785o;
        long j7 = this.f5786p;
        return ((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.f5775e.hashCode()) * 31) + this.f5776f.hashCode()) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + this.f5780j.hashCode()) * 31) + this.f5781k) * 31) + this.f5782l.hashCode()) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)));
    }

    @DexIgnore
    public java.lang.String toString() {
        return "{WorkSpec: " + this.f5771a + "}";
    }

    @DexIgnore
    public C1954hl(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        com.fossil.blesdk.obfuscated.C1419aj ajVar = com.fossil.blesdk.obfuscated.C1419aj.f3496c;
        this.f5775e = ajVar;
        this.f5776f = ajVar;
        this.f5780j = com.fossil.blesdk.obfuscated.C3365yi.f11259i;
        this.f5782l = androidx.work.BackoffPolicy.EXPONENTIAL;
        this.f5783m = 30000;
        this.f5786p = -1;
        this.f5771a = hlVar.f5771a;
        this.f5773c = hlVar.f5773c;
        this.f5772b = hlVar.f5772b;
        this.f5774d = hlVar.f5774d;
        this.f5775e = new com.fossil.blesdk.obfuscated.C1419aj(hlVar.f5775e);
        this.f5776f = new com.fossil.blesdk.obfuscated.C1419aj(hlVar.f5776f);
        this.f5777g = hlVar.f5777g;
        this.f5778h = hlVar.f5778h;
        this.f5779i = hlVar.f5779i;
        this.f5780j = new com.fossil.blesdk.obfuscated.C3365yi(hlVar.f5780j);
        this.f5781k = hlVar.f5781k;
        this.f5782l = hlVar.f5782l;
        this.f5783m = hlVar.f5783m;
        this.f5784n = hlVar.f5784n;
        this.f5785o = hlVar.f5785o;
        this.f5786p = hlVar.f5786p;
    }
}
