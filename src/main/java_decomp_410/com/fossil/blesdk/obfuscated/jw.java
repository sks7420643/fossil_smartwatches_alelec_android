package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jw implements jo {
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public jw(Object obj) {
        tw.a(obj);
        this.b = obj;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(this.b.toString().getBytes(jo.a));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof jw) {
            return this.b.equals(((jw) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ObjectKey{object=" + this.b + '}';
    }
}
