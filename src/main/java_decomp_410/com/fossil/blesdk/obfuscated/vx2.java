package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vx2 extends xs3 implements ux2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public /* final */ pa m; // = new v62(this);
    @DexIgnore
    public tr3<kb2> n;
    @DexIgnore
    public tx2 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return vx2.q;
        }

        @DexIgnore
        public final vx2 b() {
            return new vx2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ vx2 b;

        @DexIgnore
        public b(kb2 kb2, vx2 vx2, boolean z) {
            this.a = kb2;
            this.b = vx2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            tx2 a2 = vx2.a(this.b);
            NumberPicker numberPicker2 = this.a.t;
            kd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.a.u;
            kd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ vx2 b;

        @DexIgnore
        public c(kb2 kb2, vx2 vx2, boolean z) {
            this.a = kb2;
            this.b = vx2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            tx2 a2 = vx2.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.v;
            kd4.a((Object) numberPicker2, "binding.numberPickerTwo");
            a2.a(valueOf, String.valueOf(numberPicker2.getValue()), false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ vx2 b;

        @DexIgnore
        public d(kb2 kb2, vx2 vx2, boolean z) {
            this.a = kb2;
            this.b = vx2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            tx2 a2 = vx2.a(this.b);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.a.v;
            kd4.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.u;
            kd4.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ vx2 b;

        @DexIgnore
        public e(kb2 kb2, vx2 vx2, boolean z) {
            this.a = kb2;
            this.b = vx2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            tx2 a2 = vx2.a(this.b);
            NumberPicker numberPicker2 = this.a.t;
            kd4.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.a.v;
            kd4.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vx2 e;

        @DexIgnore
        public f(vx2 vx2) {
            this.e = vx2;
        }

        @DexIgnore
        public final void onClick(View view) {
            vx2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ kb2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public g(kb2 kb2, Ref$ObjectRef ref$ObjectRef) {
            this.e = kb2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.e.q;
            kd4.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.c(3);
                        kb2 kb2 = this.e;
                        kd4.a((Object) kb2, "it");
                        View d = kb2.d();
                        kd4.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = vx2.class.getSimpleName();
        kd4.a((Object) simpleName, "InactivityNudgeTimeFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ tx2 a(vx2 vx2) {
        tx2 tx2 = vx2.o;
        if (tx2 != null) {
            return tx2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void J(boolean z) {
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.v;
                kd4.a((Object) numberPicker, "binding.numberPickerTwo");
                numberPicker.setMinValue(0);
                NumberPicker numberPicker2 = a2.v;
                kd4.a((Object) numberPicker2, "binding.numberPickerTwo");
                numberPicker2.setMaxValue(59);
                a2.v.setOnValueChangedListener(new b(a2, this, z));
                if (z) {
                    NumberPicker numberPicker3 = a2.t;
                    kd4.a((Object) numberPicker3, "binding.numberPickerOne");
                    numberPicker3.setMinValue(0);
                    NumberPicker numberPicker4 = a2.t;
                    kd4.a((Object) numberPicker4, "binding.numberPickerOne");
                    numberPicker4.setMaxValue(23);
                    a2.t.setOnValueChangedListener(new c(a2, this, z));
                    NumberPicker numberPicker5 = a2.u;
                    kd4.a((Object) numberPicker5, "binding.numberPickerThree");
                    numberPicker5.setVisibility(8);
                    return;
                }
                NumberPicker numberPicker6 = a2.t;
                kd4.a((Object) numberPicker6, "binding.numberPickerOne");
                numberPicker6.setMinValue(1);
                NumberPicker numberPicker7 = a2.t;
                kd4.a((Object) numberPicker7, "binding.numberPickerOne");
                numberPicker7.setMaxValue(12);
                a2.t.setOnValueChangedListener(new d(a2, this, z));
                String[] strArr = {sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Am), sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_EditAlarm_EditAlarm_Title__Pm)};
                NumberPicker numberPicker8 = a2.u;
                kd4.a((Object) numberPicker8, "binding.numberPickerThree");
                numberPicker8.setVisibility(0);
                NumberPicker numberPicker9 = a2.u;
                kd4.a((Object) numberPicker9, "binding.numberPickerThree");
                numberPicker9.setMinValue(0);
                NumberPicker numberPicker10 = a2.u;
                kd4.a((Object) numberPicker10, "binding.numberPickerThree");
                numberPicker10.setMaxValue(1);
                a2.u.setDisplayedValues(strArr);
                a2.u.setOnValueChangedListener(new e(a2, this, z));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void d(String str) {
        kd4.b(str, "title");
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        kb2 kb2 = (kb2) qa.a(layoutInflater, R.layout.fragment_do_not_disturb_scheduled_time, viewGroup, false, this.m);
        kb2.r.setOnClickListener(new f(this));
        this.n = new tr3<>(this, kb2);
        kd4.a((Object) kb2, "binding");
        return kb2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        tx2 tx2 = this.o;
        if (tx2 != null) {
            tx2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        tx2 tx2 = this.o;
        if (tx2 != null) {
            tx2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                ref$ObjectRef.element = new g(a2, ref$ObjectRef);
                kd4.a((Object) a2, "it");
                View d2 = a2.d();
                kd4.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i) {
        tx2 tx2 = this.o;
        if (tx2 != null) {
            tx2.a(i);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void u(String str) {
        kd4.b(str, "description");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null) {
                kd4.a((Object) fragmentManager, "fragmentManager!!");
                ds3.a(fragmentManager, str);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(tx2 tx2) {
        kd4.b(tx2, "presenter");
        this.o = tx2;
    }

    @DexIgnore
    public void a(int i, boolean z) {
        int i2 = i / 60;
        int i3 = i % 60;
        int i4 = 0;
        if (!z) {
            if (i2 >= 12) {
                i4 = 1;
                i2 -= 12;
            }
            if (i2 == 0) {
                i2 = 12;
            }
        }
        tr3<kb2> tr3 = this.n;
        if (tr3 != null) {
            kb2 a2 = tr3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                kd4.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i2);
                NumberPicker numberPicker2 = a2.v;
                kd4.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i3);
                NumberPicker numberPicker3 = a2.u;
                kd4.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i4);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
