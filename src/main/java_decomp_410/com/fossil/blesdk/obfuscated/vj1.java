package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vj1 extends pk1 {
    @DexIgnore
    public /* final */ hk1 c;
    @DexIgnore
    public kg1 d;
    @DexIgnore
    public volatile Boolean e;
    @DexIgnore
    public /* final */ fm1 f;
    @DexIgnore
    public /* final */ yk1 g;
    @DexIgnore
    public /* final */ List<Runnable> h; // = new ArrayList();
    @DexIgnore
    public /* final */ fm1 i;

    @DexIgnore
    public vj1(xh1 xh1) {
        super(xh1);
        this.g = new yk1(xh1.c());
        this.c = new hk1(this);
        this.f = new wj1(this, xh1);
        this.i = new ak1(this, xh1);
    }

    @DexIgnore
    public final void A() {
        e();
        v();
        this.c.a();
        try {
            dm0.a().a(getContext(), this.c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.d = null;
    }

    @DexIgnore
    public final boolean B() {
        e();
        v();
        return this.d != null;
    }

    @DexIgnore
    public final void C() {
        e();
        this.g.b();
        this.f.a(jg1.Q.a().longValue());
    }

    @DexIgnore
    public final void D() {
        e();
        if (B()) {
            d().A().a("Inactivity, disconnecting from the service");
            A();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010d  */
    public final void E() {
        boolean z;
        boolean z2;
        boolean z3;
        e();
        v();
        if (!B()) {
            boolean z4 = false;
            if (this.e == null) {
                e();
                v();
                Boolean v = k().v();
                if (v == null || !v.booleanValue()) {
                    b();
                    if (p().F() != 1) {
                        d().A().a("Checking service availability");
                        int a = j().a((int) zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                        if (a != 0) {
                            if (a != 1) {
                                if (a == 2) {
                                    d().z().a("Service container out of date");
                                    if (j().u() >= 14500) {
                                        Boolean v2 = k().v();
                                        if (v2 == null || v2.booleanValue()) {
                                            z = true;
                                            z2 = false;
                                            if (!z && l().r()) {
                                                d().s().a("No way to upload. Consider using the full version of Analytics");
                                                z2 = false;
                                            }
                                            if (z2) {
                                                k().b(z);
                                            }
                                        }
                                    }
                                } else if (a == 3) {
                                    d().v().a("Service disabled");
                                } else if (a == 9) {
                                    d().v().a("Service invalid");
                                } else if (a != 18) {
                                    d().v().a("Unexpected service status", Integer.valueOf(a));
                                } else {
                                    d().v().a("Service updating");
                                }
                                z = false;
                                z2 = false;
                                d().s().a("No way to upload. Consider using the full version of Analytics");
                                z2 = false;
                                if (z2) {
                                }
                            } else {
                                d().A().a("Service missing");
                            }
                            z3 = false;
                            z2 = true;
                            d().s().a("No way to upload. Consider using the full version of Analytics");
                            z2 = false;
                            if (z2) {
                            }
                        } else {
                            d().A().a("Service available");
                        }
                    }
                    z3 = true;
                    z2 = true;
                    d().s().a("No way to upload. Consider using the full version of Analytics");
                    z2 = false;
                    if (z2) {
                    }
                } else {
                    z = true;
                }
                this.e = Boolean.valueOf(z);
            }
            if (this.e.booleanValue()) {
                this.c.b();
            } else if (!l().r()) {
                b();
                List<ResolveInfo> queryIntentServices = getContext().getPackageManager().queryIntentServices(new Intent().setClassName(getContext(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    z4 = true;
                }
                if (z4) {
                    Intent intent = new Intent("com.google.android.gms.measurement.START");
                    Context context = getContext();
                    b();
                    intent.setComponent(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementService"));
                    this.c.a(intent);
                    return;
                }
                d().s().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    @DexIgnore
    public final void F() {
        e();
        v();
        a((Runnable) new yj1(this, a(true)));
    }

    @DexIgnore
    public final void G() {
        e();
        v();
        a((Runnable) new bk1(this, a(true)));
    }

    @DexIgnore
    public final boolean H() {
        b();
        return true;
    }

    @DexIgnore
    public final Boolean I() {
        return this.e;
    }

    @DexIgnore
    public final void J() {
        e();
        d().A().a("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable run : this.h) {
            try {
                run.run();
            } catch (Exception e2) {
                d().s().a("Task exception while flushing queue", e2);
            }
        }
        this.h.clear();
        this.i.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    public final void a(kg1 kg1, jk0 jk0, rl1 rl1) {
        int i2;
        int size;
        int i3;
        e();
        f();
        v();
        boolean H = H();
        int i4 = 0;
        int i5 = 100;
        while (i4 < 1001 && i5 == 100) {
            ArrayList arrayList = new ArrayList();
            if (H) {
                List<jk0> a = s().a(100);
                if (a != null) {
                    arrayList.addAll(a);
                    i2 = a.size();
                    if (jk0 != null && i2 < 100) {
                        arrayList.add(jk0);
                    }
                    size = arrayList.size();
                    i3 = 0;
                    while (i3 < size) {
                        Object obj = arrayList.get(i3);
                        i3++;
                        jk0 jk02 = (jk0) obj;
                        if (jk02 instanceof hg1) {
                            try {
                                kg1.a((hg1) jk02, rl1);
                            } catch (RemoteException e2) {
                                d().s().a("Failed to send event to the service", e2);
                            }
                        } else if (jk02 instanceof kl1) {
                            try {
                                kg1.a((kl1) jk02, rl1);
                            } catch (RemoteException e3) {
                                d().s().a("Failed to send attribute to the service", e3);
                            }
                        } else if (jk02 instanceof vl1) {
                            try {
                                kg1.a((vl1) jk02, rl1);
                            } catch (RemoteException e4) {
                                d().s().a("Failed to send conditional property to the service", e4);
                            }
                        } else {
                            d().s().a("Discarding data. Unrecognized parcel type.");
                        }
                    }
                    i4++;
                    i5 = i2;
                }
            }
            i2 = 0;
            arrayList.add(jk0);
            size = arrayList.size();
            i3 = 0;
            while (i3 < size) {
            }
            i4++;
            i5 = i2;
        }
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final void a(hg1 hg1, String str) {
        bk0.a(hg1);
        e();
        v();
        boolean H = H();
        a((Runnable) new ck1(this, H, H && s().a(hg1), hg1, a(true), str));
    }

    @DexIgnore
    public final void a(vl1 vl1) {
        bk0.a(vl1);
        e();
        v();
        b();
        a((Runnable) new dk1(this, true, s().a(vl1), new vl1(vl1), a(true), vl1));
    }

    @DexIgnore
    public final void a(AtomicReference<List<vl1>> atomicReference, String str, String str2, String str3) {
        e();
        v();
        a((Runnable) new ek1(this, atomicReference, str, str2, str3, a(false)));
    }

    @DexIgnore
    public final void a(AtomicReference<List<kl1>> atomicReference, String str, String str2, String str3, boolean z) {
        e();
        v();
        a((Runnable) new fk1(this, atomicReference, str, str2, str3, z, a(false)));
    }

    @DexIgnore
    public final void a(kl1 kl1) {
        e();
        v();
        a((Runnable) new gk1(this, H() && s().a(kl1), kl1, a(true)));
    }

    @DexIgnore
    public final void a(AtomicReference<String> atomicReference) {
        e();
        v();
        a((Runnable) new xj1(this, atomicReference, a(false)));
    }

    @DexIgnore
    public final void a(qj1 qj1) {
        e();
        v();
        a((Runnable) new zj1(this, qj1));
    }

    @DexIgnore
    public final void a(kg1 kg1) {
        e();
        bk0.a(kg1);
        this.d = kg1;
        C();
        J();
    }

    @DexIgnore
    public final void a(ComponentName componentName) {
        e();
        if (this.d != null) {
            this.d = null;
            d().A().a("Disconnected from device MeasurementService", componentName);
            e();
            E();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        e();
        if (B()) {
            runnable.run();
        } else if (((long) this.h.size()) >= 1000) {
            d().s().a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.i.a(60000);
            E();
        }
    }

    @DexIgnore
    public final rl1 a(boolean z) {
        b();
        return p().a(z ? d().C() : null);
    }
}
