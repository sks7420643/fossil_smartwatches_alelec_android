package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g53 implements Factory<CommuteTimeWatchAppSettingsViewModel> {
    @DexIgnore
    public /* final */ Provider<en2> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public g53(Provider<en2> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static g53 a(Provider<en2> provider, Provider<UserRepository> provider2) {
        return new g53(provider, provider2);
    }

    @DexIgnore
    public static CommuteTimeWatchAppSettingsViewModel b(Provider<en2> provider, Provider<UserRepository> provider2) {
        return new CommuteTimeWatchAppSettingsViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel get() {
        return b(this.a, this.b);
    }
}
