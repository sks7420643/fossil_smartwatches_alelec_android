package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e61 extends vb1<e61> {
    @DexIgnore
    public f61[] c; // = f61.e();

    @DexIgnore
    public e61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        f61[] f61Arr = this.c;
        if (f61Arr != null && f61Arr.length > 0) {
            int i = 0;
            while (true) {
                f61[] f61Arr2 = this.c;
                if (i >= f61Arr2.length) {
                    break;
                }
                f61 f61 = f61Arr2[i];
                if (f61 != null) {
                    ub1.a(1, (ac1) f61);
                }
                i++;
            }
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof e61)) {
            return false;
        }
        e61 e61 = (e61) obj;
        if (!zb1.a((Object[]) this.c, (Object[]) e61.c)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(e61.b);
        }
        xb1 xb12 = e61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((e61.class.getName().hashCode() + 527) * 31) + zb1.a((Object[]) this.c)) * 31;
        xb1 xb1 = this.b;
        return hashCode + ((xb1 == null || xb1.a()) ? 0 : this.b.hashCode());
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        f61[] f61Arr = this.c;
        if (f61Arr != null && f61Arr.length > 0) {
            int i = 0;
            while (true) {
                f61[] f61Arr2 = this.c;
                if (i >= f61Arr2.length) {
                    break;
                }
                f61 f61 = f61Arr2[i];
                if (f61 != null) {
                    a += ub1.b(1, (ac1) f61);
                }
                i++;
            }
        }
        return a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                int a = dc1.a(tb1, 10);
                f61[] f61Arr = this.c;
                int length = f61Arr == null ? 0 : f61Arr.length;
                f61[] f61Arr2 = new f61[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, f61Arr2, 0, length);
                }
                while (length < f61Arr2.length - 1) {
                    f61Arr2[length] = new f61();
                    tb1.a((ac1) f61Arr2[length]);
                    tb1.c();
                    length++;
                }
                f61Arr2[length] = new f61();
                tb1.a((ac1) f61Arr2[length]);
                this.c = f61Arr2;
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
