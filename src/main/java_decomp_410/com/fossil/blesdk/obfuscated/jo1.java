package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jo1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ io1 f;

    @DexIgnore
    public jo1(io1 io1, wn1 wn1) {
        this.f = io1;
        this.e = wn1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f.b) {
            if (this.f.c != null) {
                this.f.c.onComplete(this.e);
            }
        }
    }
}
