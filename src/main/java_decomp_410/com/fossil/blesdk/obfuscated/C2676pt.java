package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pt */
public class C2676pt implements com.fossil.blesdk.obfuscated.C2427mo<android.graphics.drawable.Drawable, android.graphics.drawable.Drawable> {
    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(android.graphics.drawable.Drawable drawable, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> mo9299a(android.graphics.drawable.Drawable drawable, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return com.fossil.blesdk.obfuscated.C2503nt.m11435a(drawable);
    }
}
