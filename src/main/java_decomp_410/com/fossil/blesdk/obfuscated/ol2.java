package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.enums.Unit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ol2 {
    @DexIgnore
    public static /* final */ ol2 a; // = new ol2();

    @DexIgnore
    public final String a(Float f) {
        String b = il2.b(f != null ? f.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        kd4.a((Object) b, "NumberHelper.decimalForm\u2026Number(calories ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String b(Integer num) {
        String b = il2.b(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2);
        kd4.a((Object) b, "NumberHelper.decimalForm\u2026teps?.toFloat() ?: 0F, 2)");
        return b;
    }

    @DexIgnore
    public final String a(Integer num) {
        String b = il2.b(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        kd4.a((Object) b, "NumberHelper.decimalForm\u2026Time?.toFloat() ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String a(Float f, Unit unit) {
        kd4.b(unit, Constants.PROFILE_KEY_UNIT);
        Unit unit2 = Unit.IMPERIAL;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (unit == unit2) {
            if (f != null) {
                f2 = f.floatValue();
            }
            String b = il2.b(pk2.j(f2), 2);
            kd4.a((Object) b, "NumberHelper.decimalForm\u2026Miles(distance ?: 0F), 2)");
            return b;
        }
        if (f != null) {
            f2 = f.floatValue();
        }
        String b2 = il2.b(pk2.i(f2), 2);
        kd4.a((Object) b2, "NumberHelper.decimalForm\u2026eters(distance ?: 0F), 2)");
        return b2;
    }
}
