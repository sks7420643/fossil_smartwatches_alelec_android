package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oj4 implements zg4 {
    @DexIgnore
    public /* final */ CoroutineContext e;

    @DexIgnore
    public oj4(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        this.e = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.e;
    }
}
