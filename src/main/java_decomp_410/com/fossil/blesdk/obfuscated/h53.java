package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h53 implements MembersInjector<WeatherSettingActivity> {
    @DexIgnore
    public static void a(WeatherSettingActivity weatherSettingActivity, WeatherSettingPresenter weatherSettingPresenter) {
        weatherSettingActivity.B = weatherSettingPresenter;
    }
}
