package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.google.android.gms.internal.measurement.zzuv;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nb1 {
    @DexIgnore
    public static void a(byte b, char[] cArr, int i) {
        cArr[i] = (char) b;
    }

    @DexIgnore
    public static boolean a(byte b) {
        return b >= 0;
    }

    @DexIgnore
    public static boolean b(byte b) {
        return b < -32;
    }

    @DexIgnore
    public static boolean c(byte b) {
        return b < -16;
    }

    @DexIgnore
    public static boolean d(byte b) {
        return b > -65;
    }

    @DexIgnore
    public static void a(byte b, byte b2, char[] cArr, int i) throws zzuv {
        if (b < -62 || d(b2)) {
            throw zzuv.zzwx();
        }
        cArr[i] = (char) (((b & 31) << 6) | (b2 & 63));
    }

    @DexIgnore
    public static void a(byte b, byte b2, byte b3, char[] cArr, int i) throws zzuv {
        if (d(b2) || ((b == -32 && b2 < -96) || ((b == -19 && b2 >= -96) || d(b3)))) {
            throw zzuv.zzwx();
        }
        cArr[i] = (char) (((b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) << 12) | ((b2 & 63) << 6) | (b3 & 63));
    }

    @DexIgnore
    public static void a(byte b, byte b2, byte b3, byte b4, char[] cArr, int i) throws zzuv {
        if (d(b2) || (((b << 28) + (b2 + 112)) >> 30) != 0 || d(b3) || d(b4)) {
            throw zzuv.zzwx();
        }
        byte b5 = ((b & 7) << DateTimeFieldType.MINUTE_OF_DAY) | ((b2 & 63) << 12) | ((b3 & 63) << 6) | (b4 & 63);
        cArr[i] = (char) ((b5 >>> 10) + 55232);
        cArr[i + 1] = (char) ((b5 & FileType.MASKED_INDEX) + 56320);
    }
}
