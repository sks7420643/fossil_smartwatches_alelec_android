package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oq */
public final class C2584oq implements com.fossil.blesdk.obfuscated.C1885gq {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2430mq<com.fossil.blesdk.obfuscated.C2584oq.C2585a, java.lang.Object> f8184a; // = new com.fossil.blesdk.obfuscated.C2430mq<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2584oq.C2586b f8185b; // = new com.fossil.blesdk.obfuscated.C2584oq.C2586b();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Map<java.lang.Class<?>, java.util.NavigableMap<java.lang.Integer, java.lang.Integer>> f8186c; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C1820fq<?>> f8187d; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f8188e;

    @DexIgnore
    /* renamed from: f */
    public int f8189f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oq$a */
    public static final class C2585a implements com.fossil.blesdk.obfuscated.C2825rq {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2584oq.C2586b f8190a;

        @DexIgnore
        /* renamed from: b */
        public int f8191b;

        @DexIgnore
        /* renamed from: c */
        public java.lang.Class<?> f8192c;

        @DexIgnore
        public C2585a(com.fossil.blesdk.obfuscated.C2584oq.C2586b bVar) {
            this.f8190a = bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14484a(int i, java.lang.Class<?> cls) {
            this.f8191b = i;
            this.f8192c = cls;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C2584oq.C2585a)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2584oq.C2585a aVar = (com.fossil.blesdk.obfuscated.C2584oq.C2585a) obj;
            if (this.f8191b == aVar.f8191b && this.f8192c == aVar.f8192c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f8191b * 31;
            java.lang.Class<?> cls = this.f8192c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        @DexIgnore
        public java.lang.String toString() {
            return "Key{size=" + this.f8191b + "array=" + this.f8192c + '}';
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11713a() {
            this.f8190a.mo12068a(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oq$b")
    /* renamed from: com.fossil.blesdk.obfuscated.oq$b */
    public static final class C2586b extends com.fossil.blesdk.obfuscated.C2056iq<com.fossil.blesdk.obfuscated.C2584oq.C2585a> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2584oq.C2585a mo14488a(int i, java.lang.Class<?> cls) {
            com.fossil.blesdk.obfuscated.C2584oq.C2585a aVar = (com.fossil.blesdk.obfuscated.C2584oq.C2585a) mo12069b();
            aVar.mo14484a(i, cls);
            return aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2584oq.C2585a m11885a() {
            return new com.fossil.blesdk.obfuscated.C2584oq.C2585a(this);
        }
    }

    @DexIgnore
    public C2584oq(int i) {
        this.f8188e = i;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T> T mo11282a(int i, java.lang.Class<T> cls) {
        return mo14476a(this.f8185b.mo14488a(i, cls), cls);
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized <T> T mo11285b(int i, java.lang.Class<T> cls) {
        com.fossil.blesdk.obfuscated.C2584oq.C2585a aVar;
        java.lang.Integer ceilingKey = mo14478b((java.lang.Class<?>) cls).ceilingKey(java.lang.Integer.valueOf(i));
        if (mo14477a(i, ceilingKey)) {
            aVar = this.f8185b.mo14488a(ceilingKey.intValue(), cls);
        } else {
            aVar = this.f8185b.mo14488a(i, cls);
        }
        return mo14476a(aVar, cls);
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo14483c(int i) {
        return i <= this.f8188e / 2;
    }

    @DexIgnore
    public synchronized <T> void put(T t) {
        java.lang.Class<?> cls = t.getClass();
        com.fossil.blesdk.obfuscated.C1820fq<?> a = mo14473a(cls);
        int a2 = a.mo11010a(t);
        int b = a.mo11012b() * a2;
        if (mo14483c(b)) {
            com.fossil.blesdk.obfuscated.C2584oq.C2585a a3 = this.f8185b.mo14488a(a2, cls);
            this.f8184a.mo13695a(a3, t);
            java.util.NavigableMap<java.lang.Integer, java.lang.Integer> b2 = mo14478b(cls);
            java.lang.Integer num = (java.lang.Integer) b2.get(java.lang.Integer.valueOf(a3.f8191b));
            java.lang.Integer valueOf = java.lang.Integer.valueOf(a3.f8191b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            b2.put(valueOf, java.lang.Integer.valueOf(i));
            this.f8189f += b;
            mo14479b();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo14482c() {
        int i = this.f8189f;
        return i == 0 || this.f8188e / i >= 2;
    }

    @DexIgnore
    /* renamed from: a */
    public final <T> T mo14476a(com.fossil.blesdk.obfuscated.C2584oq.C2585a aVar, java.lang.Class<T> cls) {
        com.fossil.blesdk.obfuscated.C1820fq<T> a = mo14473a(cls);
        T a2 = mo14475a(aVar);
        if (a2 != null) {
            this.f8189f -= a.mo11010a(a2) * a.mo11012b();
            mo14481c(a.mo11010a(a2), cls);
        }
        if (a2 != null) {
            return a2;
        }
        if (android.util.Log.isLoggable(a.mo11011a(), 2)) {
            android.util.Log.v(a.mo11011a(), "Allocated " + aVar.f8191b + " bytes");
        }
        return a.newArray(aVar.f8191b);
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo14481c(int i, java.lang.Class<?> cls) {
        java.util.NavigableMap<java.lang.Integer, java.lang.Integer> b = mo14478b(cls);
        java.lang.Integer num = (java.lang.Integer) b.get(java.lang.Integer.valueOf(i));
        if (num == null) {
            throw new java.lang.NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            b.remove(java.lang.Integer.valueOf(i));
        } else {
            b.put(java.lang.Integer.valueOf(i), java.lang.Integer.valueOf(num.intValue() - 1));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14479b() {
        mo14480b(this.f8188e);
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14480b(int i) {
        while (this.f8189f > i) {
            java.lang.Object a = this.f8184a.mo13692a();
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
            com.fossil.blesdk.obfuscated.C1820fq a2 = mo14474a(a);
            this.f8189f -= a2.mo11010a(a) * a2.mo11012b();
            mo14481c(a2.mo11010a(a), a.getClass());
            if (android.util.Log.isLoggable(a2.mo11011a(), 2)) {
                android.util.Log.v(a2.mo11011a(), "evicted: " + a2.mo11010a(a));
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final <T> T mo14475a(com.fossil.blesdk.obfuscated.C2584oq.C2585a aVar) {
        return this.f8184a.mo13693a(aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo14477a(int i, java.lang.Integer num) {
        return num != null && (mo14482c() || num.intValue() <= i * 8);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo11283a() {
        mo14480b(0);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo11284a(int i) {
        if (i >= 40) {
            try {
                mo11283a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i >= 20 || i == 15) {
            mo14480b(this.f8188e / 2);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final java.util.NavigableMap<java.lang.Integer, java.lang.Integer> mo14478b(java.lang.Class<?> cls) {
        java.util.NavigableMap<java.lang.Integer, java.lang.Integer> navigableMap = this.f8186c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        java.util.TreeMap treeMap = new java.util.TreeMap();
        this.f8186c.put(cls, treeMap);
        return treeMap;
    }

    @DexIgnore
    /* renamed from: a */
    public final <T> com.fossil.blesdk.obfuscated.C1820fq<T> mo14474a(T t) {
        return mo14473a(t.getClass());
    }

    @DexIgnore
    /* renamed from: a */
    public final <T> com.fossil.blesdk.obfuscated.C1820fq<T> mo14473a(java.lang.Class<T> cls) {
        com.fossil.blesdk.obfuscated.C1820fq<T> fqVar = this.f8187d.get(cls);
        if (fqVar == null) {
            if (cls.equals(int[].class)) {
                fqVar = new com.fossil.blesdk.obfuscated.C2500nq();
            } else if (cls.equals(byte[].class)) {
                fqVar = new com.fossil.blesdk.obfuscated.C2339lq();
            } else {
                throw new java.lang.IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.f8187d.put(cls, fqVar);
        }
        return fqVar;
    }
}
