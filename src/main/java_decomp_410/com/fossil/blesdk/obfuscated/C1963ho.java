package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ho */
public interface C1963ho<T> {
    @DexIgnore
    /* renamed from: a */
    boolean mo11698a(T t, java.io.File file, com.fossil.blesdk.obfuscated.C2337lo loVar);
}
