package com.fossil.blesdk.obfuscated;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vv3 {
    @DexIgnore
    public /* final */ Set<lv3> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(lv3 lv3) {
        this.a.remove(lv3);
    }

    @DexIgnore
    public synchronized void b(lv3 lv3) {
        this.a.add(lv3);
    }

    @DexIgnore
    public synchronized boolean c(lv3 lv3) {
        return this.a.contains(lv3);
    }
}
