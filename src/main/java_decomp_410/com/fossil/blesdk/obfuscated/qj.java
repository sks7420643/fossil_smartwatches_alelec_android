package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import androidx.work.impl.background.systemjob.SystemJobService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qj {
    @DexIgnore
    public static /* final */ String a; // = dj.a("Schedulers");

    @DexIgnore
    public static void a(xi xiVar, WorkDatabase workDatabase, List<pj> list) {
        if (list != null && list.size() != 0) {
            il d = workDatabase.d();
            workDatabase.beginTransaction();
            try {
                List<hl> a2 = d.a(xiVar.e());
                if (a2 != null && a2.size() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    for (hl hlVar : a2) {
                        d.a(hlVar.a, currentTimeMillis);
                    }
                }
                workDatabase.setTransactionSuccessful();
                if (a2 != null && a2.size() > 0) {
                    hl[] hlVarArr = (hl[]) a2.toArray(new hl[0]);
                    for (pj a3 : list) {
                        a3.a(hlVarArr);
                    }
                }
            } finally {
                workDatabase.endTransaction();
            }
        }
    }

    @DexIgnore
    public static pj a(Context context, tj tjVar) {
        if (Build.VERSION.SDK_INT >= 23) {
            ek ekVar = new ek(context, tjVar);
            rl.a(context, SystemJobService.class, true);
            dj.a().a(a, "Created SystemJobScheduler and enabled SystemJobService", new Throwable[0]);
            return ekVar;
        }
        pj a2 = a(context);
        if (a2 != null) {
            return a2;
        }
        bk bkVar = new bk(context);
        rl.a(context, SystemAlarmService.class, true);
        dj.a().a(a, "Created SystemAlarmScheduler", new Throwable[0]);
        return bkVar;
    }

    @DexIgnore
    public static pj a(Context context) {
        try {
            pj pjVar = (pj) Class.forName("androidx.work.impl.background.gcm.GcmScheduler").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            dj.a().a(a, String.format("Created %s", new Object[]{"androidx.work.impl.background.gcm.GcmScheduler"}), new Throwable[0]);
            return pjVar;
        } catch (Throwable th) {
            dj.a().a(a, "Unable to create GCM Scheduler", th);
            return null;
        }
    }
}
