package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface w91 extends y91 {
    @DexIgnore
    void a(zztv zztv) throws IOException;

    @DexIgnore
    x91 c();

    @DexIgnore
    zzte d();

    @DexIgnore
    int e();

    @DexIgnore
    x91 f();
}
