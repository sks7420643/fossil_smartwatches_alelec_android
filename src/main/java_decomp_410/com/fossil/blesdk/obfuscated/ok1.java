package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ok1 implements Runnable {
    @DexIgnore
    public /* final */ nk1 e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ tg1 g;
    @DexIgnore
    public /* final */ Intent h;

    @DexIgnore
    public ok1(nk1 nk1, int i, tg1 tg1, Intent intent) {
        this.e = nk1;
        this.f = i;
        this.g = tg1;
        this.h = intent;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g, this.h);
    }
}
