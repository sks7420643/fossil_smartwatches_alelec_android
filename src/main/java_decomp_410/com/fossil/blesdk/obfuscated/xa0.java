package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.blesdk.hid.HIDProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xa0 {
    @DexIgnore
    public static /* final */ String a; // = a;
    @DexIgnore
    public static boolean b; // = true;
    @DexIgnore
    public static /* final */ xa0 c; // = new xa0();

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "value");
        va0.f.a(str);
        ca0.e.a().a(str);
    }

    @DexIgnore
    public final String b() {
        return null;
    }

    @DexIgnore
    public final String c() {
        return va0.f.e();
    }

    @DexIgnore
    public final boolean d() {
        return va0.f.a() != null;
    }

    @DexIgnore
    public final boolean a() {
        return b;
    }

    @DexIgnore
    public final void a(Context context) throws IllegalArgumentException, IllegalStateException {
        kd4.b(context, "applicationContext");
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            va0.f.a(context);
            t90.c.a(a, "init: sdkVersion=5.8.5-production-release");
            HIDProfile.e.a(context);
            BluetoothLeAdapter.l.c(context);
            System.loadLibrary("FitnessAlgorithm");
            System.loadLibrary("EllipticCurveCrypto");
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void a(na0 na0) {
        kd4.b(na0, "sdkLogEndpoint");
        va0.f.a(na0);
    }
}
