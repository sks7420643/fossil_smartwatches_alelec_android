package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pp1 implements Parcelable.Creator<op1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        qp1 qp1 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                qp1 = (qp1) SafeParcelReader.a(parcel, a, qp1.CREATOR);
            } else if (a2 == 3) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 4) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                i3 = SafeParcelReader.q(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new op1(qp1, i, i2, i3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new op1[i];
    }
}
