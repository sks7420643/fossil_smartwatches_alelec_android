package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzco;
import com.google.android.gms.internal.clearcut.zzfq;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xv0<T> implements jw0<T> {
    @DexIgnore
    public /* final */ sv0 a;
    @DexIgnore
    public /* final */ ax0<?, ?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ gu0<?> d;

    @DexIgnore
    public xv0(ax0<?, ?> ax0, gu0<?> gu0, sv0 sv0) {
        this.b = ax0;
        this.c = gu0.a(sv0);
        this.d = gu0;
        this.a = sv0;
    }

    @DexIgnore
    public static <T> xv0<T> a(ax0<?, ?> ax0, gu0<?> gu0, sv0 sv0) {
        return new xv0<>(ax0, gu0, sv0);
    }

    @DexIgnore
    public final int a(T t) {
        int hashCode = this.b.c(t).hashCode();
        return this.c ? (hashCode * 53) + this.d.a((Object) t).hashCode() : hashCode;
    }

    @DexIgnore
    public final T a() {
        return this.a.e().v();
    }

    @DexIgnore
    public final void a(T t, ox0 ox0) throws IOException {
        Iterator<Map.Entry<?, Object>> e = this.d.a((Object) t).e();
        while (e.hasNext()) {
            Map.Entry next = e.next();
            nu0 nu0 = (nu0) next.getKey();
            if (nu0.b() != zzfq.MESSAGE || nu0.c() || nu0.a()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            }
            ox0.zza(nu0.zzc(), next instanceof zu0 ? ((zu0) next).a().b() : next.getValue());
        }
        ax0<?, ?> ax0 = this.b;
        ax0.b(ax0.c(t), ox0);
    }

    @DexIgnore
    public final void a(T t, byte[] bArr, int i, int i2, pt0 pt0) throws IOException {
        ru0 ru0 = (ru0) t;
        bx0 bx0 = ru0.zzjp;
        if (bx0 == bx0.d()) {
            bx0 = bx0.e();
            ru0.zzjp = bx0;
        }
        bx0 bx02 = bx0;
        while (i < i2) {
            int a2 = ot0.a(bArr, i, pt0);
            int i3 = pt0.a;
            if (i3 != 11) {
                i = (i3 & 7) == 2 ? ot0.a(i3, bArr, a2, i2, bx02, pt0) : ot0.a(i3, bArr, a2, i2, pt0);
            } else {
                int i4 = 0;
                zzbb zzbb = null;
                while (a2 < i2) {
                    a2 = ot0.a(bArr, a2, pt0);
                    int i5 = pt0.a;
                    int i6 = i5 >>> 3;
                    int i7 = i5 & 7;
                    if (i6 != 2) {
                        if (i6 == 3 && i7 == 2) {
                            a2 = ot0.e(bArr, a2, pt0);
                            zzbb = (zzbb) pt0.c;
                        }
                    } else if (i7 == 0) {
                        a2 = ot0.a(bArr, a2, pt0);
                        i4 = pt0.a;
                    }
                    if (i5 == 12) {
                        break;
                    }
                    a2 = ot0.a(i5, bArr, a2, i2, pt0);
                }
                if (zzbb != null) {
                    bx02.a((i4 << 3) | 2, (Object) zzbb);
                }
                i = a2;
            }
        }
        if (i != i2) {
            throw zzco.zzbo();
        }
    }

    @DexIgnore
    public final boolean a(T t, T t2) {
        if (!this.b.c(t).equals(this.b.c(t2))) {
            return false;
        }
        if (this.c) {
            return this.d.a((Object) t).equals(this.d.a((Object) t2));
        }
        return true;
    }

    @DexIgnore
    public final int b(T t) {
        ax0<?, ?> ax0 = this.b;
        int d2 = ax0.d(ax0.c(t)) + 0;
        return this.c ? d2 + this.d.a((Object) t).g() : d2;
    }

    @DexIgnore
    public final void b(T t, T t2) {
        lw0.a(this.b, t, t2);
        if (this.c) {
            lw0.a(this.d, t, t2);
        }
    }

    @DexIgnore
    public final boolean c(T t) {
        return this.d.a((Object) t).d();
    }

    @DexIgnore
    public final void zzc(T t) {
        this.b.a(t);
        this.d.c(t);
    }
}
