package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzdi;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ov0 implements nv0 {
    @DexIgnore
    public final int a(int i, Object obj, Object obj2) {
        zzdi zzdi = (zzdi) obj;
        if (zzdi.isEmpty()) {
            return 0;
        }
        Iterator it = zzdi.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final mv0<?, ?> a(Object obj) {
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final Object b(Object obj) {
        return zzdi.zzbz().zzca();
    }

    @DexIgnore
    public final Object c(Object obj) {
        ((zzdi) obj).zzv();
        return obj;
    }

    @DexIgnore
    public final boolean d(Object obj) {
        return !((zzdi) obj).isMutable();
    }

    @DexIgnore
    public final Map<?, ?> e(Object obj) {
        return (zzdi) obj;
    }

    @DexIgnore
    public final Map<?, ?> f(Object obj) {
        return (zzdi) obj;
    }

    @DexIgnore
    public final Object zzb(Object obj, Object obj2) {
        zzdi zzdi = (zzdi) obj;
        zzdi zzdi2 = (zzdi) obj2;
        if (!zzdi2.isEmpty()) {
            if (!zzdi.isMutable()) {
                zzdi = zzdi.zzca();
            }
            zzdi.zza(zzdi2);
        }
        return zzdi;
    }
}
