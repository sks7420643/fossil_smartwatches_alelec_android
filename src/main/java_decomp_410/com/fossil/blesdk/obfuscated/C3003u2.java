package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u2 */
public class C3003u2 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.ThreadLocal<android.util.TypedValue> f9834a; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int[] f9835b; // = {-16842910};

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int[] f9836c; // = {16842908};

    @DexIgnore
    /* renamed from: d */
    public static /* final */ int[] f9837d; // = {16842919};

    @DexIgnore
    /* renamed from: e */
    public static /* final */ int[] f9838e; // = {16842912};

    @DexIgnore
    /* renamed from: f */
    public static /* final */ int[] f9839f; // = new int[0];

    @DexIgnore
    /* renamed from: g */
    public static /* final */ int[] f9840g; // = new int[1];

    /*
    static {
        new int[1][0] = 16843518;
        new int[1][0] = 16842913;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static int m14531a(android.content.Context context, int i) {
        android.content.res.ColorStateList c = m14535c(context, i);
        if (c != null && c.isStateful()) {
            return c.getColorForState(f9835b, c.getDefaultColor());
        }
        android.util.TypedValue a = m14533a();
        context.getTheme().resolveAttribute(16842803, a, true);
        return m14532a(context, i, a.getFloat());
    }

    @DexIgnore
    /* renamed from: b */
    public static int m14534b(android.content.Context context, int i) {
        int[] iArr = f9840g;
        iArr[0] = i;
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17204a(context, (android.util.AttributeSet) null, iArr);
        try {
            return a.mo18415a(0, 0);
        } finally {
            a.mo18418a();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static android.content.res.ColorStateList m14535c(android.content.Context context, int i) {
        int[] iArr = f9840g;
        iArr[0] = i;
        com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17204a(context, (android.util.AttributeSet) null, iArr);
        try {
            return a.mo18416a(0);
        } finally {
            a.mo18418a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.util.TypedValue m14533a() {
        android.util.TypedValue typedValue = f9834a.get();
        if (typedValue != null) {
            return typedValue;
        }
        android.util.TypedValue typedValue2 = new android.util.TypedValue();
        f9834a.set(typedValue2);
        return typedValue2;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14532a(android.content.Context context, int i, float f) {
        int b = m14534b(context, i);
        return com.fossil.blesdk.obfuscated.C2943t6.m14033c(b, java.lang.Math.round(((float) android.graphics.Color.alpha(b)) * f));
    }
}
