package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ih3 extends dh3 {
    @DexIgnore
    public /* final */ eh3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        kd4.a((Object) ih3.class.getSimpleName(), "ReplaceBatteryPresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public ih3(eh3 eh3) {
        kd4.b(eh3, "mView");
        this.f = eh3;
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }
}
