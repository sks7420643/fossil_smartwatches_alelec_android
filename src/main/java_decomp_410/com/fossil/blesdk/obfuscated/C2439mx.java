package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mx */
public class C2439mx extends com.fossil.blesdk.obfuscated.C1665dx<com.fossil.blesdk.obfuscated.C2439mx> {

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f7600c;

    @DexIgnore
    public C2439mx(java.lang.String str) {
        if (str != null) {
            this.f7600c = this.f4539a.mo10634a(str);
            return;
        }
        throw new java.lang.NullPointerException("eventName must not be null");
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo13752b() {
        return this.f7600c;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "{eventName:\"" + this.f7600c + '\"' + ", customAttributes:" + this.f4540b + "}";
    }
}
