package com.fossil.blesdk.obfuscated;

import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eu1<E> extends hu1 implements Collection<E> {
    @DexIgnore
    public boolean add(E e) {
        return delegate().add(e);
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        return delegate().addAll(collection);
    }

    @DexIgnore
    public void clear() {
        delegate().clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return delegate().contains(obj);
    }

    @DexIgnore
    public boolean containsAll(Collection<?> collection) {
        return delegate().containsAll(collection);
    }

    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    public abstract Collection<E> delegate();

    @DexIgnore
    public boolean isEmpty() {
        return delegate().isEmpty();
    }

    @DexIgnore
    public Iterator<E> iterator() {
        return delegate().iterator();
    }

    @DexIgnore
    public boolean remove(Object obj) {
        return delegate().remove(obj);
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        return delegate().removeAll(collection);
    }

    @DexIgnore
    public boolean retainAll(Collection<?> collection) {
        return delegate().retainAll(collection);
    }

    @DexIgnore
    public int size() {
        return delegate().size();
    }

    @DexIgnore
    public boolean standardAddAll(Collection<? extends E> collection) {
        return lu1.a(this, collection.iterator());
    }

    @DexIgnore
    public void standardClear() {
        lu1.a((Iterator<?>) iterator());
    }

    @DexIgnore
    public boolean standardContains(Object obj) {
        return lu1.a((Iterator<?>) iterator(), obj);
    }

    @DexIgnore
    public boolean standardContainsAll(Collection<?> collection) {
        return cu1.a((Collection<?>) this, collection);
    }

    @DexIgnore
    public boolean standardIsEmpty() {
        return !iterator().hasNext();
    }

    @DexIgnore
    public boolean standardRemove(Object obj) {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (rt1.a(it.next(), obj)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public abstract boolean standardRemoveAll(Collection<?> collection);

    @DexIgnore
    public boolean standardRetainAll(Collection<?> collection) {
        return lu1.b((Iterator<?>) iterator(), collection);
    }

    @DexIgnore
    public Object[] standardToArray() {
        return toArray(new Object[size()]);
    }

    @DexIgnore
    public String standardToString() {
        return cu1.a((Collection<?>) this);
    }

    @DexIgnore
    public Object[] toArray() {
        return delegate().toArray();
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return delegate().toArray(tArr);
    }

    @DexIgnore
    public <T> T[] standardToArray(T[] tArr) {
        return su1.a((Collection<?>) this, tArr);
    }
}
