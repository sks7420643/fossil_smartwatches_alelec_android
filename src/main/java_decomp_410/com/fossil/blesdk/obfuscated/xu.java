package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xu extends Fragment {
    @DexIgnore
    public /* final */ ju e;
    @DexIgnore
    public /* final */ vu f;
    @DexIgnore
    public /* final */ Set<xu> g;
    @DexIgnore
    public xu h;
    @DexIgnore
    public xn i;
    @DexIgnore
    public Fragment j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vu {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Set<xn> a() {
            Set<xu> N0 = xu.this.N0();
            HashSet hashSet = new HashSet(N0.size());
            for (xu next : N0) {
                if (next.Q0() != null) {
                    hashSet.add(next.Q0());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + xu.this + "}";
        }
    }

    @DexIgnore
    public xu() {
        this(new ju());
    }

    @DexIgnore
    public static FragmentManager c(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    @DexIgnore
    public Set<xu> N0() {
        xu xuVar = this.h;
        if (xuVar == null) {
            return Collections.emptySet();
        }
        if (equals(xuVar)) {
            return Collections.unmodifiableSet(this.g);
        }
        HashSet hashSet = new HashSet();
        for (xu next : this.h.N0()) {
            if (a(next.P0())) {
                hashSet.add(next);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public ju O0() {
        return this.e;
    }

    @DexIgnore
    public final Fragment P0() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.j;
    }

    @DexIgnore
    public xn Q0() {
        return this.i;
    }

    @DexIgnore
    public vu R0() {
        return this.f;
    }

    @DexIgnore
    public final void S0() {
        xu xuVar = this.h;
        if (xuVar != null) {
            xuVar.b(this);
            this.h = null;
        }
    }

    @DexIgnore
    public void a(xn xnVar) {
        this.i = xnVar;
    }

    @DexIgnore
    public final void b(xu xuVar) {
        this.g.remove(xuVar);
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentManager c = c(this);
        if (c != null) {
            try {
                a(getContext(), c);
            } catch (IllegalStateException e2) {
                if (Log.isLoggable("SupportRMFragment", 5)) {
                    Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
                }
            }
        } else if (Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        this.e.a();
        S0();
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.j = null;
        S0();
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        this.e.b();
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        this.e.c();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{parent=" + P0() + "}";
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public xu(ju juVar) {
        this.f = new a();
        this.g = new HashSet();
        this.e = juVar;
    }

    @DexIgnore
    public final void a(xu xuVar) {
        this.g.add(xuVar);
    }

    @DexIgnore
    public void b(Fragment fragment) {
        this.j = fragment;
        if (fragment != null && fragment.getContext() != null) {
            FragmentManager c = c(fragment);
            if (c != null) {
                a(fragment.getContext(), c);
            }
        }
    }

    @DexIgnore
    public final boolean a(Fragment fragment) {
        Fragment P0 = P0();
        while (true) {
            Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(P0)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void a(Context context, FragmentManager fragmentManager) {
        S0();
        this.h = rn.a(context).h().a(context, fragmentManager);
        if (!equals(this.h)) {
            this.h.a(this);
        }
    }
}
