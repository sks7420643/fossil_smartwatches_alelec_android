package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jn1 extends de0.a<wm1, vm1> {
    @DexIgnore
    public final /* synthetic */ de0.f a(Context context, Looper looper, kj0 kj0, Object obj, ge0.b bVar, ge0.c cVar) {
        vm1 vm1 = (vm1) obj;
        if (vm1 == null) {
            vm1 = vm1.m;
        }
        return new wm1(context, looper, true, kj0, vm1, bVar, cVar);
    }
}
