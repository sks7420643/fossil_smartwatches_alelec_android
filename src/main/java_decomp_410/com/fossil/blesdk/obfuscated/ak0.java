package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ak0 {
    @DexIgnore
    public static /* final */ b a; // = new gl0();

    @DexIgnore
    public interface a<R extends me0, T> {
        @DexIgnore
        T a(R r);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        ApiException a(Status status);
    }

    @DexIgnore
    public static <R extends me0, T> wn1<T> a(he0<R> he0, a<R, T> aVar) {
        b bVar = a;
        xn1 xn1 = new xn1();
        he0.a((he0.a) new hl0(he0, xn1, aVar, bVar));
        return xn1.a();
    }

    @DexIgnore
    public static <R extends me0, T extends le0<R>> wn1<T> a(he0<R> he0, T t) {
        return a(he0, new il0(t));
    }

    @DexIgnore
    public static <R extends me0> wn1<Void> a(he0<R> he0) {
        return a(he0, new jl0());
    }
}
