package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hr4 extends dr4.a {
    @DexIgnore
    public /* final */ Executor a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements dr4<Object, Call<?>> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public a(hr4 hr4, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @DexIgnore
        public Type a() {
            return this.a;
        }

        @DexIgnore
        public Call<Object> a(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new b(executor, call);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor e;
        @DexIgnore
        public /* final */ Call<T> f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements er4<T> {
            @DexIgnore
            public /* final */ /* synthetic */ er4 e;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hr4$b$a$a")
            /* renamed from: com.fossil.blesdk.obfuscated.hr4$b$a$a  reason: collision with other inner class name */
            public class C0088a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ qr4 e;

                @DexIgnore
                public C0088a(qr4 qr4) {
                    this.e = qr4;
                }

                @DexIgnore
                public void run() {
                    if (b.this.f.o()) {
                        a aVar = a.this;
                        aVar.e.onFailure(b.this, new IOException("Canceled"));
                        return;
                    }
                    a aVar2 = a.this;
                    aVar2.e.onResponse(b.this, this.e);
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hr4$b$a$b")
            /* renamed from: com.fossil.blesdk.obfuscated.hr4$b$a$b  reason: collision with other inner class name */
            public class C0089b implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable e;

                @DexIgnore
                public C0089b(Throwable th) {
                    this.e = th;
                }

                @DexIgnore
                public void run() {
                    a aVar = a.this;
                    aVar.e.onFailure(b.this, this.e);
                }
            }

            @DexIgnore
            public a(er4 er4) {
                this.e = er4;
            }

            @DexIgnore
            public void onFailure(Call<T> call, Throwable th) {
                b.this.e.execute(new C0089b(th));
            }

            @DexIgnore
            public void onResponse(Call<T> call, qr4<T> qr4) {
                b.this.e.execute(new C0088a(qr4));
            }
        }

        @DexIgnore
        public b(Executor executor, Call<T> call) {
            this.e = executor;
            this.f = call;
        }

        @DexIgnore
        public void a(er4<T> er4) {
            ur4.a(er4, "callback == null");
            this.f.a(new a(er4));
        }

        @DexIgnore
        public void cancel() {
            this.f.cancel();
        }

        @DexIgnore
        public dm4 n() {
            return this.f.n();
        }

        @DexIgnore
        public boolean o() {
            return this.f.o();
        }

        @DexIgnore
        public qr4<T> r() throws IOException {
            return this.f.r();
        }

        @DexIgnore
        public Call<T> clone() {
            return new b(this.e, this.f.clone());
        }
    }

    @DexIgnore
    public hr4(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    public dr4<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (dr4.a.a(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b2 = ur4.b(0, (ParameterizedType) type);
            if (!ur4.a(annotationArr, (Class<? extends Annotation>) sr4.class)) {
                executor = this.a;
            }
            return new a(this, b2, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
