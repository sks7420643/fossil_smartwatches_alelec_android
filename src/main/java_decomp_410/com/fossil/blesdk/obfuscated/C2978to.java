package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.to */
public interface C2978to<T> {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.to$a */
    public interface C2979a<T> {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C2978to<T> mo12468a(T t);

        @DexIgnore
        java.lang.Class<T> getDataClass();
    }

    @DexIgnore
    /* renamed from: a */
    void mo12466a();

    @DexIgnore
    /* renamed from: b */
    T mo12467b() throws java.io.IOException;
}
