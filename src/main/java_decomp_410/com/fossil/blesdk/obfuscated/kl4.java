package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kl4 {
    @DexIgnore
    void onFailure(jl4 jl4, IOException iOException);

    @DexIgnore
    void onResponse(jl4 jl4, Response response) throws IOException;
}
