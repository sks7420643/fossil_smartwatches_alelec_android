package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j90 {
    @DexIgnore
    public static final <T> ArrayList<T> a(ArrayList<T> arrayList, ArrayList<T> arrayList2) {
        kd4.b(arrayList, "$this$combine");
        kd4.b(arrayList2, FacebookRequestErrorClassification.KEY_OTHER);
        ArrayList<T> arrayList3 = new ArrayList<>(arrayList);
        arrayList3.addAll(arrayList2);
        return arrayList3;
    }
}
