package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class si0 extends Fragment implements ye0 {
    @DexIgnore
    public static WeakHashMap<FragmentActivity, WeakReference<si0>> h; // = new WeakHashMap<>();
    @DexIgnore
    public Map<String, LifecycleCallback> e; // = new g4();
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public Bundle g;

    @DexIgnore
    public static si0 a(FragmentActivity fragmentActivity) {
        WeakReference weakReference = h.get(fragmentActivity);
        if (weakReference != null) {
            si0 si0 = (si0) weakReference.get();
            if (si0 != null) {
                return si0;
            }
        }
        try {
            si0 si02 = (si0) fragmentActivity.getSupportFragmentManager().a("SupportLifecycleFragmentImpl");
            if (si02 == null || si02.isRemoving()) {
                si02 = new si0();
                bb a = fragmentActivity.getSupportFragmentManager().a();
                a.a((Fragment) si02, "SupportLifecycleFragmentImpl");
                a.b();
            }
            h.put(fragmentActivity, new WeakReference(si02));
            return si02;
        } catch (ClassCastException e2) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e2);
        }
    }

    @DexIgnore
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback a : this.e.values()) {
            a.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    public final /* synthetic */ Activity o0() {
        return getActivity();
    }

    @DexIgnore
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback a : this.e.values()) {
            a.a(i, i2, intent);
        }
    }

    @DexIgnore
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = 1;
        this.g = bundle;
        for (Map.Entry next : this.e.entrySet()) {
            ((LifecycleCallback) next.getValue()).a(bundle != null ? bundle.getBundle((String) next.getKey()) : null);
        }
    }

    @DexIgnore
    public final void onDestroy() {
        super.onDestroy();
        this.f = 5;
        for (LifecycleCallback b : this.e.values()) {
            b.b();
        }
    }

    @DexIgnore
    public final void onResume() {
        super.onResume();
        this.f = 3;
        for (LifecycleCallback c : this.e.values()) {
            c.c();
        }
    }

    @DexIgnore
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry next : this.e.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) next.getValue()).b(bundle2);
                bundle.putBundle((String) next.getKey(), bundle2);
            }
        }
    }

    @DexIgnore
    public final void onStart() {
        super.onStart();
        this.f = 2;
        for (LifecycleCallback d : this.e.values()) {
            d.d();
        }
    }

    @DexIgnore
    public final void onStop() {
        super.onStop();
        this.f = 4;
        for (LifecycleCallback e2 : this.e.values()) {
            e2.e();
        }
    }

    @DexIgnore
    public final <T extends LifecycleCallback> T a(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.e.get(str));
    }

    @DexIgnore
    public final void a(String str, LifecycleCallback lifecycleCallback) {
        if (!this.e.containsKey(str)) {
            this.e.put(str, lifecycleCallback);
            if (this.f > 0) {
                new bz0(Looper.getMainLooper()).post(new ti0(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }
}
