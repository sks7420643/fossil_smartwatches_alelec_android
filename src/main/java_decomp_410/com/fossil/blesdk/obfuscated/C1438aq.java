package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.aq */
public interface C1438aq<Z> {
    @DexIgnore
    /* renamed from: a */
    void mo8887a();

    @DexIgnore
    /* renamed from: b */
    int mo8888b();

    @DexIgnore
    /* renamed from: c */
    java.lang.Class<Z> mo8889c();

    @DexIgnore
    Z get();
}
