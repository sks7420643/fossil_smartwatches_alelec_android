package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ComponentName e;
    @DexIgnore
    public /* final */ /* synthetic */ hk1 f;

    @DexIgnore
    public jk1(hk1 hk1, ComponentName componentName) {
        this.f = hk1;
        this.e = componentName;
    }

    @DexIgnore
    public final void run() {
        this.f.c.a(this.e);
    }
}
