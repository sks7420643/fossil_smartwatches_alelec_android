package com.fossil.blesdk.obfuscated;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lb0 {
    @DexIgnore
    public static /* final */ HashMap<String, a> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ lb0 b; // = new lb0();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public long b;

        @DexIgnore
        public a(int i, long j) {
            this.a = i;
            this.b = j;
        }

        @DexIgnore
        public static /* synthetic */ a a(a aVar, int i, long j, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = aVar.a;
            }
            if ((i2 & 2) != 0) {
                j = aVar.b;
            }
            return aVar.a(i, j);
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final a a(int i, long j) {
            return new a(i, j);
        }

        @DexIgnore
        public final long b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (this.a == aVar.a) {
                        if (this.b == aVar.b) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            long j = this.b;
            return (this.a * 31) + ((int) (j ^ (j >>> 32)));
        }

        @DexIgnore
        public String toString() {
            return "ExponentBackOffInfo(currentExponent=" + this.a + ", maxRate=" + this.b + ")";
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public final long a(String str) {
        long j;
        kd4.b(str, "key");
        synchronized (a) {
            a aVar = a.get(str);
            if (aVar == null) {
                aVar = new a(0, 128);
            }
            long min = Math.min(128, td4.b(Math.pow((double) 2, (double) (aVar.a() + 1))));
            if (min < aVar.b()) {
                aVar.a(aVar.a() + 1);
            }
            a.put(str, aVar);
            t90 t90 = t90.c;
            t90.a("getNextRate", "key=" + str + ", rate=" + min);
            j = min * 1000;
        }
        return j;
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "key");
        synchronized (a) {
            a aVar = a.get(str);
            if (aVar == null) {
                aVar = new a(0, 128);
            }
            a.put(str, a.a(aVar, 0, 0, 2, (Object) null));
            qa4 qa4 = qa4.a;
        }
    }
}
