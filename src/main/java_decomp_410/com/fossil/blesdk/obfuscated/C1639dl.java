package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dl */
public final class C1639dl implements com.fossil.blesdk.obfuscated.C1564cl {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.room.RoomDatabase f4422a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf f4423b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C3217wf f4424c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dl$a */
    public class C1640a extends com.fossil.blesdk.obfuscated.C2322lf<com.fossil.blesdk.obfuscated.C1490bl> {
        @DexIgnore
        public C1640a(com.fossil.blesdk.obfuscated.C1639dl dlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.fossil.blesdk.obfuscated.C1490bl blVar) {
            java.lang.String str = blVar.f3770a;
            if (str == null) {
                kgVar.mo11930a(1);
            } else {
                kgVar.mo11932a(1, str);
            }
            kgVar.mo11934b(2, (long) blVar.f3771b);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR REPLACE INTO `SystemIdInfo`(`work_spec_id`,`system_id`) VALUES (?,?)";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dl$b")
    /* renamed from: com.fossil.blesdk.obfuscated.dl$b */
    public class C1641b extends com.fossil.blesdk.obfuscated.C3217wf {
        @DexIgnore
        public C1641b(com.fossil.blesdk.obfuscated.C1639dl dlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "DELETE FROM SystemIdInfo where work_spec_id=?";
        }
    }

    @DexIgnore
    public C1639dl(androidx.room.RoomDatabase roomDatabase) {
        this.f4422a = roomDatabase;
        this.f4423b = new com.fossil.blesdk.obfuscated.C1639dl.C1640a(this, roomDatabase);
        this.f4424c = new com.fossil.blesdk.obfuscated.C1639dl.C1641b(this, roomDatabase);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9557a(com.fossil.blesdk.obfuscated.C1490bl blVar) {
        this.f4422a.assertNotSuspendingTransaction();
        this.f4422a.beginTransaction();
        try {
            this.f4423b.insert(blVar);
            this.f4422a.setTransactionSuccessful();
        } finally {
            this.f4422a.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9558b(java.lang.String str) {
        this.f4422a.assertNotSuspendingTransaction();
        com.fossil.blesdk.obfuscated.C2221kg acquire = this.f4424c.acquire();
        if (str == null) {
            acquire.mo11930a(1);
        } else {
            acquire.mo11932a(1, str);
        }
        this.f4422a.beginTransaction();
        try {
            acquire.mo12781n();
            this.f4422a.setTransactionSuccessful();
        } finally {
            this.f4422a.endTransaction();
            this.f4424c.release(acquire);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1490bl mo9556a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT * FROM SystemIdInfo WHERE work_spec_id=?", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f4422a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f4422a, b, false);
        try {
            return a.moveToFirst() ? new com.fossil.blesdk.obfuscated.C1490bl(a.getString(com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "work_spec_id")), a.getInt(com.fossil.blesdk.obfuscated.C1416ag.m4427b(a, "system_id"))) : null;
        } finally {
            a.close();
            b.mo16767c();
        }
    }
}
