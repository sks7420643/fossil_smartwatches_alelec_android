package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.v4 */
public class C3083v4 extends com.fossil.blesdk.obfuscated.C1464b5 {

    @DexIgnore
    /* renamed from: m0 */
    public int f10140m0; // = 0;

    @DexIgnore
    /* renamed from: n0 */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C1703e5> f10141n0; // = new java.util.ArrayList<>(4);

    @DexIgnore
    /* renamed from: o0 */
    public boolean f10142o0; // = true;

    @DexIgnore
    /* renamed from: G */
    public void mo1280G() {
        super.mo1280G();
        this.f10141n0.clear();
    }

    @DexIgnore
    /* renamed from: H */
    public void mo1281H() {
        com.fossil.blesdk.obfuscated.C1703e5 e5Var;
        float f;
        com.fossil.blesdk.obfuscated.C1703e5 e5Var2;
        int i = this.f10140m0;
        float f2 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                e5Var = this.f718u.mo1266d();
            } else if (i == 2) {
                e5Var = this.f717t.mo1266d();
            } else if (i == 3) {
                e5Var = this.f719v.mo1266d();
            } else {
                return;
            }
            f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            e5Var = this.f716s.mo1266d();
        }
        int size = this.f10141n0.size();
        com.fossil.blesdk.obfuscated.C1703e5 e5Var3 = null;
        int i2 = 0;
        while (i2 < size) {
            com.fossil.blesdk.obfuscated.C1703e5 e5Var4 = this.f10141n0.get(i2);
            if (e5Var4.f5384b == 1) {
                int i3 = this.f10140m0;
                if (i3 == 0 || i3 == 2) {
                    f = e5Var4.f4704g;
                    if (f < f2) {
                        e5Var2 = e5Var4.f4703f;
                    } else {
                        i2++;
                    }
                } else {
                    f = e5Var4.f4704g;
                    if (f > f2) {
                        e5Var2 = e5Var4.f4703f;
                    } else {
                        i2++;
                    }
                }
                e5Var3 = e5Var2;
                f2 = f;
                i2++;
            } else {
                return;
            }
        }
        if (com.fossil.blesdk.obfuscated.C2703q4.m12575j() != null) {
            com.fossil.blesdk.obfuscated.C2703q4.m12575j().f8855y++;
        }
        e5Var.f4703f = e5Var3;
        e5Var.f4704g = f2;
        e5Var.mo11137a();
        int i4 = this.f10140m0;
        if (i4 == 0) {
            this.f718u.mo1266d().mo10325a(e5Var3, f2);
        } else if (i4 == 1) {
            this.f716s.mo1266d().mo10325a(e5Var3, f2);
        } else if (i4 == 2) {
            this.f719v.mo1266d().mo10325a(e5Var3, f2);
        } else if (i4 == 3) {
            this.f717t.mo1266d().mo10325a(e5Var3, f2);
        }
    }

    @DexIgnore
    /* renamed from: L */
    public boolean mo16973L() {
        return this.f10142o0;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1287a(int i) {
        com.fossil.blesdk.obfuscated.C1703e5 e5Var;
        com.fossil.blesdk.obfuscated.C1703e5 e5Var2;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f666D;
        if (constraintWidget != null && ((com.fossil.blesdk.obfuscated.C3342y4) constraintWidget).mo18016u(2)) {
            int i2 = this.f10140m0;
            if (i2 == 0) {
                e5Var = this.f716s.mo1266d();
            } else if (i2 == 1) {
                e5Var = this.f718u.mo1266d();
            } else if (i2 == 2) {
                e5Var = this.f717t.mo1266d();
            } else if (i2 == 3) {
                e5Var = this.f719v.mo1266d();
            } else {
                return;
            }
            e5Var.mo10329b(5);
            int i3 = this.f10140m0;
            if (i3 == 0 || i3 == 1) {
                this.f717t.mo1266d().mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.f719v.mo1266d().mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                this.f716s.mo1266d().mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.f718u.mo1266d().mo10325a((com.fossil.blesdk.obfuscated.C1703e5) null, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.f10141n0.clear();
            for (int i4 = 0; i4 < this.f3674l0; i4++) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = this.f3673k0[i4];
                if (this.f10142o0 || constraintWidget2.mo1311b()) {
                    int i5 = this.f10140m0;
                    if (i5 == 0) {
                        e5Var2 = constraintWidget2.f716s.mo1266d();
                    } else if (i5 == 1) {
                        e5Var2 = constraintWidget2.f718u.mo1266d();
                    } else if (i5 == 2) {
                        e5Var2 = constraintWidget2.f717t.mo1266d();
                    } else if (i5 != 3) {
                        e5Var2 = null;
                    } else {
                        e5Var2 = constraintWidget2.f719v.mo1266d();
                    }
                    if (e5Var2 != null) {
                        this.f10141n0.add(e5Var2);
                        e5Var2.mo11138a(e5Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo1311b() {
        return true;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16974c(boolean z) {
        this.f10142o0 = z;
    }

    @DexIgnore
    /* renamed from: u */
    public void mo16975u(int i) {
        this.f10140m0 = i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r1 = true;
     */
    @DexIgnore
    /* renamed from: a */
    public void mo1297a(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr;
        boolean z;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr2 = this.f663A;
        constraintAnchorArr2[0] = this.f716s;
        constraintAnchorArr2[2] = this.f717t;
        constraintAnchorArr2[1] = this.f718u;
        constraintAnchorArr2[3] = this.f719v;
        int i = 0;
        while (true) {
            constraintAnchorArr = this.f663A;
            if (i >= constraintAnchorArr.length) {
                break;
            }
            constraintAnchorArr[i].f657i = q4Var.mo15010a((java.lang.Object) constraintAnchorArr[i]);
            i++;
        }
        int i2 = this.f10140m0;
        if (i2 >= 0 && i2 < 4) {
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintAnchorArr[i2];
            int i3 = 0;
            while (true) {
                if (i3 >= this.f3674l0) {
                    z = false;
                    break;
                }
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f3673k0[i3];
                if (this.f10142o0 || constraintWidget.mo1311b()) {
                    int i4 = this.f10140m0;
                    if ((i4 != 0 && i4 != 1) || constraintWidget.mo1334k() != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        int i5 = this.f10140m0;
                        if ((i5 == 2 || i5 == 3) && constraintWidget.mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i3++;
            }
            int i6 = this.f10140m0;
            if (i6 == 0 || i6 == 1 ? mo1336l().mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT : mo1336l().mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < this.f3674l0; i7++) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = this.f3673k0[i7];
                if (this.f10142o0 || constraintWidget2.mo1311b()) {
                    androidx.constraintlayout.solver.SolverVariable a = q4Var.mo15010a((java.lang.Object) constraintWidget2.f663A[this.f10140m0]);
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.f663A;
                    int i8 = this.f10140m0;
                    constraintAnchorArr3[i8].f657i = a;
                    if (i8 == 0 || i8 == 2) {
                        q4Var.mo15023b(constraintAnchor.f657i, a, z);
                    } else {
                        q4Var.mo15016a(constraintAnchor.f657i, a, z);
                    }
                }
            }
            int i9 = this.f10140m0;
            if (i9 == 0) {
                q4Var.mo15011a(this.f718u.f657i, this.f716s.f657i, 0, 6);
                if (!z) {
                    q4Var.mo15011a(this.f716s.f657i, this.f666D.f718u.f657i, 0, 5);
                }
            } else if (i9 == 1) {
                q4Var.mo15011a(this.f716s.f657i, this.f718u.f657i, 0, 6);
                if (!z) {
                    q4Var.mo15011a(this.f716s.f657i, this.f666D.f716s.f657i, 0, 5);
                }
            } else if (i9 == 2) {
                q4Var.mo15011a(this.f719v.f657i, this.f717t.f657i, 0, 6);
                if (!z) {
                    q4Var.mo15011a(this.f717t.f657i, this.f666D.f719v.f657i, 0, 5);
                }
            } else if (i9 == 3) {
                q4Var.mo15011a(this.f717t.f657i, this.f719v.f657i, 0, 6);
                if (!z) {
                    q4Var.mo15011a(this.f717t.f657i, this.f666D.f717t.f657i, 0, 5);
                }
            }
        }
    }
}
