package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ot */
public class C2602ot implements com.fossil.blesdk.obfuscated.C2427mo<android.net.Uri, android.graphics.drawable.Drawable> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f8230a;

    @DexIgnore
    public C2602ot(android.content.Context context) {
        this.f8230a = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo14540b(android.content.Context context, android.net.Uri uri) {
        java.util.List<java.lang.String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 2) {
            return mo14535a(context, uri);
        }
        if (pathSegments.size() == 1) {
            return mo14536a(uri);
        }
        throw new java.lang.IllegalArgumentException("Unrecognized Uri format: " + uri);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(android.net.Uri uri, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return uri.getScheme().equals("android.resource");
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.Drawable> mo9299a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        android.content.Context a = mo14537a(uri, uri.getAuthority());
        return com.fossil.blesdk.obfuscated.C2503nt.m11435a(com.fossil.blesdk.obfuscated.C2342lt.m10273a(this.f8230a, a, mo14540b(a, uri)));
    }

    @DexIgnore
    /* renamed from: a */
    public final android.content.Context mo14537a(android.net.Uri uri, java.lang.String str) {
        if (str.equals(this.f8230a.getPackageName())) {
            return this.f8230a;
        }
        try {
            return this.f8230a.createPackageContext(str, 0);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            if (str.contains(this.f8230a.getPackageName())) {
                return this.f8230a;
            }
            throw new java.lang.IllegalArgumentException("Failed to obtain context or unrecognized Uri format for: " + uri, e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo14535a(android.content.Context context, android.net.Uri uri) {
        java.util.List<java.lang.String> pathSegments = uri.getPathSegments();
        java.lang.String authority = uri.getAuthority();
        java.lang.String str = pathSegments.get(0);
        java.lang.String str2 = pathSegments.get(1);
        int identifier = context.getResources().getIdentifier(str2, str, authority);
        if (identifier == 0) {
            identifier = android.content.res.Resources.getSystem().getIdentifier(str2, str, "android");
        }
        if (identifier != 0) {
            return identifier;
        }
        throw new java.lang.IllegalArgumentException("Failed to find resource id for: " + uri);
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo14536a(android.net.Uri uri) {
        try {
            return java.lang.Integer.parseInt(uri.getPathSegments().get(0));
        } catch (java.lang.NumberFormatException e) {
            throw new java.lang.IllegalArgumentException("Unrecognized Uri format: " + uri, e);
        }
    }
}
