package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g1 */
public class C1852g1 extends android.widget.BaseAdapter {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1915h1 f5342e;

    @DexIgnore
    /* renamed from: f */
    public int f5343f; // = -1;

    @DexIgnore
    /* renamed from: g */
    public boolean f5344g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ boolean f5345h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.view.LayoutInflater f5346i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ int f5347j;

    @DexIgnore
    public C1852g1(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.LayoutInflater layoutInflater, boolean z, int i) {
        this.f5345h = z;
        this.f5346i = layoutInflater;
        this.f5342e = h1Var;
        this.f5347j = i;
        mo11099a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11100a(boolean z) {
        this.f5344g = z;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1915h1 mo11101b() {
        return this.f5342e;
    }

    @DexIgnore
    public int getCount() {
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = this.f5345h ? this.f5342e.mo11511j() : this.f5342e.mo11515n();
        if (this.f5343f < 0) {
            return j.size();
        }
        return j.size() - 1;
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
        if (view == null) {
            view = this.f5346i.inflate(this.f5347j, viewGroup, false);
        }
        int groupId = getItem(i).getGroupId();
        int i2 = i - 1;
        androidx.appcompat.view.menu.ListMenuItemView listMenuItemView = (androidx.appcompat.view.menu.ListMenuItemView) view;
        listMenuItemView.setGroupDividerEnabled(this.f5342e.mo11516o() && groupId != (i2 >= 0 ? getItem(i2).getGroupId() : groupId));
        com.fossil.blesdk.obfuscated.C2697q1.C2698a aVar = (com.fossil.blesdk.obfuscated.C2697q1.C2698a) view;
        if (this.f5344g) {
            listMenuItemView.setForceShowIcon(true);
        }
        aVar.mo349a(getItem(i), 0);
        return view;
    }

    @DexIgnore
    public void notifyDataSetChanged() {
        mo11099a();
        super.notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11099a() {
        com.fossil.blesdk.obfuscated.C2179k1 f = this.f5342e.mo11502f();
        if (f != null) {
            java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = this.f5342e.mo11511j();
            int size = j.size();
            for (int i = 0; i < size; i++) {
                if (j.get(i) == f) {
                    this.f5343f = i;
                    return;
                }
            }
        }
        this.f5343f = -1;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2179k1 getItem(int i) {
        java.util.ArrayList<com.fossil.blesdk.obfuscated.C2179k1> j = this.f5345h ? this.f5342e.mo11511j() : this.f5342e.mo11515n();
        int i2 = this.f5343f;
        if (i2 >= 0 && i >= i2) {
            i++;
        }
        return j.get(i);
    }
}
