package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l3<K, V> implements Iterable<Map.Entry<K, V>> {
    @DexIgnore
    public c<K, V> e;
    @DexIgnore
    public c<K, V> f;
    @DexIgnore
    public WeakHashMap<f<K, V>, Boolean> g; // = new WeakHashMap<>();
    @DexIgnore
    public int h; // = 0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends e<K, V> {
        @DexIgnore
        public a(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        public c<K, V> b(c<K, V> cVar) {
            return cVar.h;
        }

        @DexIgnore
        public c<K, V> c(c<K, V> cVar) {
            return cVar.g;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends e<K, V> {
        @DexIgnore
        public b(c<K, V> cVar, c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        public c<K, V> b(c<K, V> cVar) {
            return cVar.g;
        }

        @DexIgnore
        public c<K, V> c(c<K, V> cVar) {
            return cVar.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public /* final */ K e;
        @DexIgnore
        public /* final */ V f;
        @DexIgnore
        public c<K, V> g;
        @DexIgnore
        public c<K, V> h;

        @DexIgnore
        public c(K k, V v) {
            this.e = k;
            this.f = v;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!this.e.equals(cVar.e) || !this.f.equals(cVar.f)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public K getKey() {
            return this.e;
        }

        @DexIgnore
        public V getValue() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            return this.e.hashCode() ^ this.f.hashCode();
        }

        @DexIgnore
        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        @DexIgnore
        public String toString() {
            return this.e + SimpleComparison.EQUAL_TO_OPERATION + this.f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> e;
        @DexIgnore
        public boolean f; // = true;

        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(c<K, V> cVar) {
            c<K, V> cVar2 = this.e;
            if (cVar == cVar2) {
                this.e = cVar2.h;
                this.f = this.e == null;
            }
        }

        @DexIgnore
        public boolean hasNext() {
            if (!this.f) {
                c<K, V> cVar = this.e;
                if (cVar == null || cVar.g == null) {
                    return false;
                }
                return true;
            } else if (l3.this.e != null) {
                return true;
            } else {
                return false;
            }
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            if (this.f) {
                this.f = false;
                this.e = l3.this.e;
            } else {
                c<K, V> cVar = this.e;
                this.e = cVar != null ? cVar.g : null;
            }
            return this.e;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K, V> implements Iterator<Map.Entry<K, V>>, f<K, V> {
        @DexIgnore
        public c<K, V> e;
        @DexIgnore
        public c<K, V> f;

        @DexIgnore
        public e(c<K, V> cVar, c<K, V> cVar2) {
            this.e = cVar2;
            this.f = cVar;
        }

        @DexIgnore
        public void a(c<K, V> cVar) {
            if (this.e == cVar && cVar == this.f) {
                this.f = null;
                this.e = null;
            }
            c<K, V> cVar2 = this.e;
            if (cVar2 == cVar) {
                this.e = b(cVar2);
            }
            if (this.f == cVar) {
                this.f = a();
            }
        }

        @DexIgnore
        public abstract c<K, V> b(c<K, V> cVar);

        @DexIgnore
        public abstract c<K, V> c(c<K, V> cVar);

        @DexIgnore
        public boolean hasNext() {
            return this.f != null;
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            c<K, V> cVar = this.f;
            this.f = a();
            return cVar;
        }

        @DexIgnore
        public final c<K, V> a() {
            c<K, V> cVar = this.f;
            c<K, V> cVar2 = this.e;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return c(cVar);
        }
    }

    @DexIgnore
    public interface f<K, V> {
        @DexIgnore
        void a(c<K, V> cVar);
    }

    @DexIgnore
    public c<K, V> a(K k) {
        c<K, V> cVar = this.e;
        while (cVar != null && !cVar.e.equals(k)) {
            cVar = cVar.g;
        }
        return cVar;
    }

    @DexIgnore
    public V b(K k, V v) {
        c a2 = a(k);
        if (a2 != null) {
            return a2.f;
        }
        a(k, v);
        return null;
    }

    @DexIgnore
    public Map.Entry<K, V> c() {
        return this.f;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> descendingIterator() {
        b bVar = new b(this.f, this.e);
        this.g.put(bVar, false);
        return bVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l3)) {
            return false;
        }
        l3 l3Var = (l3) obj;
        if (size() != l3Var.size()) {
            return false;
        }
        Iterator it = iterator();
        Iterator it2 = l3Var.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Object next = it2.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        Iterator it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((Map.Entry) it.next()).hashCode();
        }
        return i;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> iterator() {
        a aVar = new a(this.e, this.f);
        this.g.put(aVar, false);
        return aVar;
    }

    @DexIgnore
    public V remove(K k) {
        c a2 = a(k);
        if (a2 == null) {
            return null;
        }
        this.h--;
        if (!this.g.isEmpty()) {
            for (f<K, V> a3 : this.g.keySet()) {
                a3.a(a2);
            }
        }
        c<K, V> cVar = a2.h;
        if (cVar != null) {
            cVar.g = a2.g;
        } else {
            this.e = a2.g;
        }
        c<K, V> cVar2 = a2.g;
        if (cVar2 != null) {
            cVar2.h = a2.h;
        } else {
            this.f = a2.h;
        }
        a2.g = null;
        a2.h = null;
        return a2.f;
    }

    @DexIgnore
    public int size() {
        return this.h;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((Map.Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public c<K, V> a(K k, V v) {
        c<K, V> cVar = new c<>(k, v);
        this.h++;
        c<K, V> cVar2 = this.f;
        if (cVar2 == null) {
            this.e = cVar;
            this.f = this.e;
            return cVar;
        }
        cVar2.g = cVar;
        cVar.h = cVar2;
        this.f = cVar;
        return cVar;
    }

    @DexIgnore
    public l3<K, V>.d b() {
        l3<K, V>.d dVar = new d();
        this.g.put(dVar, false);
        return dVar;
    }

    @DexIgnore
    public Map.Entry<K, V> a() {
        return this.e;
    }
}
