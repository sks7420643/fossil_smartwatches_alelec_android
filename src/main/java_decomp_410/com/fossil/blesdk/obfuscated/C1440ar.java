package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ar */
public interface C1440ar {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ar$a */
    public interface C1441a {
        @DexIgnore
        /* renamed from: a */
        void mo8909a(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar);
    }

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C1438aq<?> mo8904a(com.fossil.blesdk.obfuscated.C2143jo joVar);

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C1438aq<?> mo8905a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C1438aq<?> aqVar);

    @DexIgnore
    /* renamed from: a */
    void mo8906a();

    @DexIgnore
    /* renamed from: a */
    void mo8907a(int i);

    @DexIgnore
    /* renamed from: a */
    void mo8908a(com.fossil.blesdk.obfuscated.C1440ar.C1441a aVar);
}
