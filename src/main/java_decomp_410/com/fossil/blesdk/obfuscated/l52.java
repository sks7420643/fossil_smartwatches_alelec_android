package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l52 implements Factory<en2> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<Context> b;

    @DexIgnore
    public l52(n42 n42, Provider<Context> provider) {
        this.a = n42;
        this.b = provider;
    }

    @DexIgnore
    public static l52 a(n42 n42, Provider<Context> provider) {
        return new l52(n42, provider);
    }

    @DexIgnore
    public static en2 b(n42 n42, Provider<Context> provider) {
        return a(n42, provider.get());
    }

    @DexIgnore
    public static en2 a(n42 n42, Context context) {
        en2 a2 = n42.a(context);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public en2 get() {
        return b(this.a, this.b);
    }
}
