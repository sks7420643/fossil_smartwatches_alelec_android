package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.l3;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k3<K, V> extends l3<K, V> {
    @DexIgnore
    public HashMap<K, l3.c<K, V>> i; // = new HashMap<>();

    @DexIgnore
    public l3.c<K, V> a(K k) {
        return this.i.get(k);
    }

    @DexIgnore
    public V b(K k, V v) {
        l3.c a = a(k);
        if (a != null) {
            return a.f;
        }
        this.i.put(k, a(k, v));
        return null;
    }

    @DexIgnore
    public boolean contains(K k) {
        return this.i.containsKey(k);
    }

    @DexIgnore
    public V remove(K k) {
        V remove = super.remove(k);
        this.i.remove(k);
        return remove;
    }

    @DexIgnore
    public Map.Entry<K, V> b(K k) {
        if (contains(k)) {
            return this.i.get(k).h;
        }
        return null;
    }
}
