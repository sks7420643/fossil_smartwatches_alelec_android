package com.fossil.blesdk.obfuscated;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wx implements j64 {
    @DexIgnore
    public /* final */ j64 a;
    @DexIgnore
    public /* final */ Random b;
    @DexIgnore
    public /* final */ double c;

    @DexIgnore
    public wx(j64 j64, double d) {
        this(j64, d, new Random());
    }

    @DexIgnore
    public long a(int i) {
        return (long) (a() * ((double) this.a.a(i)));
    }

    @DexIgnore
    public wx(j64 j64, double d, Random random) {
        if (d < 0.0d || d > 1.0d) {
            throw new IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (j64 == null) {
            throw new NullPointerException("backoff must not be null");
        } else if (random != null) {
            this.a = j64;
            this.c = d;
            this.b = random;
        } else {
            throw new NullPointerException("random must not be null");
        }
    }

    @DexIgnore
    public double a() {
        double d = this.c;
        double d2 = 1.0d - d;
        return d2 + (((d + 1.0d) - d2) * this.b.nextDouble());
    }
}
