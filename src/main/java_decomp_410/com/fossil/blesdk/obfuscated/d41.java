package com.fossil.blesdk.obfuscated;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d41 extends xd1 {
    @DexIgnore
    public /* final */ ze0<rc1> e;

    @DexIgnore
    public d41(ze0<rc1> ze0) {
        this.e = ze0;
    }

    @DexIgnore
    public final synchronized void o() {
        this.e.a();
    }

    @DexIgnore
    public final synchronized void onLocationChanged(Location location) {
        this.e.a(new e41(this, location));
    }
}
