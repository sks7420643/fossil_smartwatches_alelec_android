package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface gz2 extends gq2<fz2> {
    @DexIgnore
    void a(int i, ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    void b(int i, ArrayList<String> arrayList);

    @DexIgnore
    void c(int i, ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    void close();

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    void f(List<Object> list);

    @DexIgnore
    void h(int i);

    @DexIgnore
    void j();

    @DexIgnore
    void p(boolean z);
}
