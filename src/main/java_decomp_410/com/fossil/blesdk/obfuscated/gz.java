package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gz {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ x74 b;

    @DexIgnore
    public gz(Context context, x74 x74) {
        this.a = context;
        this.b = x74;
    }

    @DexIgnore
    public String a() {
        return a("com.crashlytics.CrashSubmissionAlwaysSendTitle", this.b.g);
    }

    @DexIgnore
    public String b() {
        return a("com.crashlytics.CrashSubmissionCancelTitle", this.b.e);
    }

    @DexIgnore
    public String c() {
        return a("com.crashlytics.CrashSubmissionPromptMessage", this.b.b);
    }

    @DexIgnore
    public String d() {
        return a("com.crashlytics.CrashSubmissionSendTitle", this.b.c);
    }

    @DexIgnore
    public String e() {
        return a("com.crashlytics.CrashSubmissionPromptTitle", this.b.a);
    }

    @DexIgnore
    public final String a(String str, String str2) {
        return b(CommonUtils.b(this.a, str), str2);
    }

    @DexIgnore
    public final String b(String str, String str2) {
        return a(str) ? str2 : str;
    }

    @DexIgnore
    public final boolean a(String str) {
        return str == null || str.length() == 0;
    }
}
