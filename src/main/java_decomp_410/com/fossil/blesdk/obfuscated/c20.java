package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c20 extends b20 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public c20(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        this.a = characteristicId;
        this.b = bArr;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId a() {
        return this.a;
    }

    @DexIgnore
    public final byte[] b() {
        return this.b;
    }
}
