package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bs */
public class C1504bs<Data> implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, Data> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.Set<java.lang.String> f3898b; // = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"file", "android.resource", "content"})));

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1504bs.C1507c<Data> f3899a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bs$a")
    /* renamed from: com.fossil.blesdk.obfuscated.bs$a */
    public static final class C1505a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, android.content.res.AssetFileDescriptor>, com.fossil.blesdk.obfuscated.C1504bs.C1507c<android.content.res.AssetFileDescriptor> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ContentResolver f3900a;

        @DexIgnore
        public C1505a(android.content.ContentResolver contentResolver) {
            this.f3900a = contentResolver;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, android.content.res.AssetFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1504bs(this);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2902so<android.content.res.AssetFileDescriptor> mo9270a(android.net.Uri uri) {
            return new com.fossil.blesdk.obfuscated.C2662po(this.f3900a, uri);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bs$b")
    /* renamed from: com.fossil.blesdk.obfuscated.bs$b */
    public static class C1506b implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, android.os.ParcelFileDescriptor>, com.fossil.blesdk.obfuscated.C1504bs.C1507c<android.os.ParcelFileDescriptor> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ContentResolver f3901a;

        @DexIgnore
        public C1506b(android.content.ContentResolver contentResolver) {
            this.f3901a = contentResolver;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2902so<android.os.ParcelFileDescriptor> mo9270a(android.net.Uri uri) {
            return new com.fossil.blesdk.obfuscated.C3302xo(this.f3901a, uri);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, android.os.ParcelFileDescriptor> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1504bs(this);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.bs$c */
    public interface C1507c<Data> {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C2902so<Data> mo9270a(android.net.Uri uri);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bs$d")
    /* renamed from: com.fossil.blesdk.obfuscated.bs$d */
    public static class C1508d implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream>, com.fossil.blesdk.obfuscated.C1504bs.C1507c<java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.ContentResolver f3902a;

        @DexIgnore
        public C1508d(android.content.ContentResolver contentResolver) {
            this.f3902a = contentResolver;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2902so<java.io.InputStream> mo9270a(android.net.Uri uri) {
            return new com.fossil.blesdk.obfuscated.C1572cp(this.f3902a, uri);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1504bs(this);
        }
    }

    @DexIgnore
    public C1504bs(com.fossil.blesdk.obfuscated.C1504bs.C1507c<Data> cVar) {
        this.f3899a = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<Data> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(uri), this.f3899a.mo9270a(uri));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return f3898b.contains(uri.getScheme());
    }
}
