package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dt2 extends rd<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ jt2 e;
    @DexIgnore
    public /* final */ FragmentManager f;
    @DexIgnore
    public /* final */ zr2 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            kd4.b(str, "mDayOfWeek");
            kd4.b(str2, "mDayOfMonth");
            kd4.b(str3, "mDailyValue");
            kd4.b(str4, "mDailyUnit");
            kd4.b(str5, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5, int i, fd4 fd4) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) == 0 ? z2 : false, (r0 & 8) != 0 ? r5 : str, (r0 & 16) != 0 ? r5 : str2, (r0 & 32) != 0 ? r5 : str3, (r0 & 64) != 0 ? r5 : str4, (r0 & 128) == 0 ? str5 : r5);
            int i2 = i;
            Date date2 = (i2 & 1) != 0 ? null : date;
            String str6 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final void c(String str) {
            kd4.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void d(String str) {
            kd4.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            kd4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void b(String str) {
            kd4.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void a(String str) {
            kd4.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ og2 b;
        @DexIgnore
        public /* final */ /* synthetic */ dt2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a = this.e.a;
                if (a != null) {
                    this.e.c.e.b(a);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dt2 dt2, og2 og2, View view) {
            super(view);
            kd4.b(og2, "binding");
            kd4.b(view, "root");
            this.c = dt2;
            this.b = og2;
            this.b.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void a(ActivitySummary activitySummary) {
            b a2 = this.c.a(activitySummary);
            this.a = a2.d();
            FlexibleTextView flexibleTextView = this.b.u;
            kd4.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            kd4.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.e());
            FlexibleTextView flexibleTextView3 = this.b.s;
            kd4.a((Object) flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(a2.c());
            FlexibleTextView flexibleTextView4 = this.b.r;
            kd4.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(a2.b());
            FlexibleTextView flexibleTextView5 = this.b.v;
            kd4.a((Object) flexibleTextView5, "binding.ftvEst");
            flexibleTextView5.setText(a2.a());
            if (a2.g()) {
                this.b.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.disabledCalendarDay));
                FlexibleTextView flexibleTextView6 = this.b.r;
                kd4.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setAllCaps(true);
            } else {
                this.b.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.nonReachGoal));
                FlexibleTextView flexibleTextView7 = this.b.r;
                kd4.a((Object) flexibleTextView7, "binding.ftvDailyUnit");
                flexibleTextView7.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            kd4.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.g());
            FlexibleTextView flexibleTextView8 = this.b.u;
            kd4.a((Object) flexibleTextView8, "binding.ftvDayOfWeek");
            flexibleTextView8.setSelected(a2.h());
            FlexibleTextView flexibleTextView9 = this.b.t;
            kd4.a((Object) flexibleTextView9, "binding.ftvDayOfMonth");
            flexibleTextView9.setSelected(a2.h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            kd4.b(str, "mWeekly");
            kd4.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, fd4 fd4) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            kd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            kd4.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ qg2 f;
        @DexIgnore
        public /* final */ /* synthetic */ dt2 g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public a(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d != null && this.e.e != null) {
                    jt2 c = this.e.g.e;
                    Date b = this.e.d;
                    if (b != null) {
                        Date a = this.e.e;
                        if (a != null) {
                            c.b(b, a);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(dt2 dt2, qg2 qg2) {
            super(dt2, r0, r1);
            kd4.b(qg2, "binding");
            this.g = dt2;
            og2 og2 = qg2.r;
            if (og2 != null) {
                kd4.a((Object) og2, "binding.dailyItem!!");
                View d2 = qg2.d();
                kd4.a((Object) d2, "binding.root");
                this.f = qg2;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public void a(ActivitySummary activitySummary) {
            d b = this.g.b(activitySummary);
            this.e = b.a();
            this.d = b.b();
            FlexibleTextView flexibleTextView = this.f.s;
            kd4.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.f.t;
            kd4.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(activitySummary);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ dt2 e;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public f(dt2 dt2, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.e = dt2;
            this.f = viewHolder;
            this.g = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            kd4.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id=" + this.e.g.getId() + ", isAdded=" + this.e.g.isAdded());
            this.f.itemView.removeOnAttachStateChangeListener(this);
            Fragment a = this.e.f.a(this.e.g.R0());
            if (a == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                bb a2 = this.e.f.a();
                a2.a(view.getId(), this.e.g, this.e.g.R0());
                a2.d();
            } else if (this.g) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
                bb a3 = this.e.f.a();
                a3.d(a);
                a3.d();
                bb a4 = this.e.f.a();
                a4.a(view.getId(), this.e.g, this.e.g.R0());
                a4.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.e.g.getId() + ", isAdded2=" + this.e.g.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            kd4.b(view, "v");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dt2(ct2 ct2, PortfolioApp portfolioApp, jt2 jt2, FragmentManager fragmentManager, zr2 zr2) {
        super(ct2);
        kd4.b(ct2, "activityDifference");
        kd4.b(portfolioApp, "mApp");
        kd4.b(jt2, "mOnItemClick");
        kd4.b(fragmentManager, "mFragmentManager");
        kd4.b(zr2, "mFragment");
        this.d = portfolioApp;
        this.e = jt2;
        this.f = fragmentManager;
        this.g = zr2;
    }

    @DexIgnore
    public long getItemId(int i) {
        if (getItemViewType(i) != 0) {
            return super.getItemId(i);
        }
        if (this.g.getId() == 0) {
            return 1010101;
        }
        return (long) this.g.getId();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) a(i);
        if (activitySummary == null) {
            return 1;
        }
        this.c.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
        Calendar calendar = this.c;
        kd4.a((Object) calendar, "mCalendar");
        Boolean s = rk2.s(calendar.getTime());
        kd4.a((Object) s, "DateHelper.isToday(mCalendar.time)");
        if (s.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeAdapter", "onBindViewHolder - position=" + i);
        int itemViewType = getItemViewType(i);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            kd4.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            kd4.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardActiveTimeAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            kd4.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((ActivitySummary) a(i));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((ActivitySummary) a(i));
        } else {
            ((e) viewHolder).a((ActivitySummary) a(i));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i == 1) {
            og2 a2 = og2.a(from, viewGroup, false);
            kd4.a((Object) a2, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View d2 = a2.d();
            kd4.a((Object) d2, "itemActiveTimeDayBinding.root");
            return new c(this, a2, d2);
        } else if (i != 2) {
            og2 a3 = og2.a(from, viewGroup, false);
            kd4.a((Object) a3, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View d3 = a3.d();
            kd4.a((Object) d3, "itemActiveTimeDayBinding.root");
            return new c(this, a3, d3);
        } else {
            qg2 a4 = qg2.a(from, viewGroup, false);
            kd4.a((Object) a4, "ItemActiveTimeWeekBindin\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(qd<ActivitySummary> qdVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
        local.d("DashboardActiveTimeAdapter", sb.toString());
        super.b(qdVar);
    }

    @DexIgnore
    public final b a(ActivitySummary activitySummary) {
        b bVar = new b((Date) null, false, false, (String) null, (String) null, (String) null, (String) null, (String) null, 255, (fd4) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i = instance.get(7);
            kd4.a((Object) instance, "calendar");
            Boolean s = rk2.s(instance.getTime());
            kd4.a((Object) s, "DateHelper.isToday(calendar.time)");
            if (s.booleanValue()) {
                String a2 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_ActiveMinutesToday_Text__Today);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026MinutesToday_Text__Today)");
                bVar.e(a2);
            } else {
                bVar.e(ll2.b.b(i));
            }
            bVar.a(instance.getTime());
            bVar.d(String.valueOf(instance.get(5)));
            if (activitySummary.getActiveTime() > 0) {
                int activeTime = activitySummary.getActiveTime();
                bVar.c(ol2.a.a(Integer.valueOf(activeTime)));
                String a3 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_ActiveMinutesToday_Label__Mins);
                kd4.a((Object) a3, "LanguageHelper.getString\u2026MinutesToday_Label__Mins)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    bVar.b(lowerCase);
                    bVar.a("");
                    boolean z = false;
                    if (activitySummary.getActiveTimeGoal() > 0) {
                        if (activeTime >= activitySummary.getActiveTimeGoal()) {
                            z = true;
                        }
                        bVar.b(z);
                    } else {
                        bVar.b(false);
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String a4 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_ActiveMinutes_DetailPageNoRecord_Text__NoRecord);
                kd4.a((Object) a4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                bVar.b(a4);
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(ActivitySummary activitySummary) {
        String str;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (fd4) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            kd4.a((Object) instance, "calendar");
            Boolean s = rk2.s(instance.getTime());
            int i = instance.get(5);
            int i2 = instance.get(2);
            String b2 = rk2.b(i2);
            int i3 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i4 = instance.get(5);
            int i5 = instance.get(2);
            String b3 = rk2.b(i5);
            int i6 = instance.get(1);
            dVar.b(instance.getTime());
            kd4.a((Object) s, "isToday");
            if (s.booleanValue()) {
                str = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_ActiveMinutesToday_Title__ThisWeek);
                kd4.a((Object) str, "LanguageHelper.getString\u2026tesToday_Title__ThisWeek)");
            } else if (i2 == i5) {
                str = b3 + ' ' + i4 + " - " + b3 + ' ' + i;
            } else if (i6 == i3) {
                str = b3 + ' ' + i4 + " - " + b2 + ' ' + i;
            } else {
                str = b3 + ' ' + i4 + ", " + i6 + " - " + b2 + ' ' + i + ", " + i3;
            }
            dVar.a(str);
            pd4 pd4 = pd4.a;
            String a2 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_ActiveMinutesToday_Text__NumberMins);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026esToday_Text__NumberMins)");
            Object[] objArr = new Object[1];
            ol2 ol2 = ol2.a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            objArr[0] = ol2.a(totalValuesOfWeek != null ? Integer.valueOf(totalValuesOfWeek.getTotalActiveTimeOfWeek()) : null);
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }
}
