package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p4 extends n4 {
    @DexIgnore
    public p4(o4 o4Var) {
        super(o4Var);
    }

    @DexIgnore
    public void a(SolverVariable solverVariable) {
        super.a(solverVariable);
        solverVariable.j--;
    }
}
