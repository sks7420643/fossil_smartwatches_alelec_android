package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nb */
public class C2471nb {

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C2471nb f7694c; // = new com.fossil.blesdk.obfuscated.C2471nb();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.Class, com.fossil.blesdk.obfuscated.C2471nb.C2472a> f7695a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.Class, java.lang.Boolean> f7696b; // = new java.util.HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nb$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nb$a */
    public static class C2472a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.Map<androidx.lifecycle.Lifecycle.Event, java.util.List<com.fossil.blesdk.obfuscated.C2471nb.C2473b>> f7697a; // = new java.util.HashMap();

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.Map<com.fossil.blesdk.obfuscated.C2471nb.C2473b, androidx.lifecycle.Lifecycle.Event> f7698b;

        @DexIgnore
        public C2472a(java.util.Map<com.fossil.blesdk.obfuscated.C2471nb.C2473b, androidx.lifecycle.Lifecycle.Event> map) {
            this.f7698b = map;
            for (java.util.Map.Entry next : map.entrySet()) {
                androidx.lifecycle.Lifecycle.Event event = (androidx.lifecycle.Lifecycle.Event) next.getValue();
                java.util.List list = this.f7697a.get(event);
                if (list == null) {
                    list = new java.util.ArrayList();
                    this.f7697a.put(event, list);
                }
                list.add(next.getKey());
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13900a(androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.lifecycle.Lifecycle.Event event, java.lang.Object obj) {
            m11143a(this.f7697a.get(event), lifecycleOwner, event, obj);
            m11143a(this.f7697a.get(androidx.lifecycle.Lifecycle.Event.ON_ANY), lifecycleOwner, event, obj);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m11143a(java.util.List<com.fossil.blesdk.obfuscated.C2471nb.C2473b> list, androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.lifecycle.Lifecycle.Event event, java.lang.Object obj) {
            if (list != null) {
                for (int size = list.size() - 1; size >= 0; size--) {
                    list.get(size).mo13901a(lifecycleOwner, event, obj);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nb$b")
    /* renamed from: com.fossil.blesdk.obfuscated.nb$b */
    public static class C2473b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f7699a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.reflect.Method f7700b;

        @DexIgnore
        public C2473b(int i, java.lang.reflect.Method method) {
            this.f7699a = i;
            this.f7700b = method;
            this.f7700b.setAccessible(true);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13901a(androidx.lifecycle.LifecycleOwner lifecycleOwner, androidx.lifecycle.Lifecycle.Event event, java.lang.Object obj) {
            try {
                int i = this.f7699a;
                if (i == 0) {
                    this.f7700b.invoke(obj, new java.lang.Object[0]);
                } else if (i == 1) {
                    this.f7700b.invoke(obj, new java.lang.Object[]{lifecycleOwner});
                } else if (i == 2) {
                    this.f7700b.invoke(obj, new java.lang.Object[]{lifecycleOwner, event});
                }
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException("Failed to call observer method", e.getCause());
            } catch (java.lang.IllegalAccessException e2) {
                throw new java.lang.RuntimeException(e2);
            }
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C2471nb.C2473b.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2471nb.C2473b bVar = (com.fossil.blesdk.obfuscated.C2471nb.C2473b) obj;
            if (this.f7699a != bVar.f7699a || !this.f7700b.getName().equals(bVar.f7700b.getName())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (this.f7699a * 31) + this.f7700b.getName().hashCode();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.reflect.Method[] mo13897a(java.lang.Class cls) {
        try {
            return cls.getDeclaredMethods();
        } catch (java.lang.NoClassDefFoundError e) {
            throw new java.lang.IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", e);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2471nb.C2472a mo13898b(java.lang.Class cls) {
        com.fossil.blesdk.obfuscated.C2471nb.C2472a aVar = this.f7695a.get(cls);
        if (aVar != null) {
            return aVar;
        }
        return mo13895a(cls, (java.lang.reflect.Method[]) null);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo13899c(java.lang.Class cls) {
        java.lang.Boolean bool = this.f7696b.get(cls);
        if (bool != null) {
            return bool.booleanValue();
        }
        java.lang.reflect.Method[] a = mo13897a(cls);
        for (java.lang.reflect.Method annotation : a) {
            if (((com.fossil.blesdk.obfuscated.C1613dc) annotation.getAnnotation(com.fossil.blesdk.obfuscated.C1613dc.class)) != null) {
                mo13895a(cls, a);
                return true;
            }
        }
        this.f7696b.put(cls, false);
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13896a(java.util.Map<com.fossil.blesdk.obfuscated.C2471nb.C2473b, androidx.lifecycle.Lifecycle.Event> map, com.fossil.blesdk.obfuscated.C2471nb.C2473b bVar, androidx.lifecycle.Lifecycle.Event event, java.lang.Class cls) {
        androidx.lifecycle.Lifecycle.Event event2 = map.get(bVar);
        if (event2 != null && event != event2) {
            java.lang.reflect.Method method = bVar.f7700b;
            throw new java.lang.IllegalArgumentException("Method " + method.getName() + " in " + cls.getName() + " already declared with different @OnLifecycleEvent value: previous value " + event2 + ", new value " + event);
        } else if (event2 == null) {
            map.put(bVar, event);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2471nb.C2472a mo13895a(java.lang.Class cls, java.lang.reflect.Method[] methodArr) {
        int i;
        java.lang.Class superclass = cls.getSuperclass();
        java.util.HashMap hashMap = new java.util.HashMap();
        if (superclass != null) {
            com.fossil.blesdk.obfuscated.C2471nb.C2472a b = mo13898b(superclass);
            if (b != null) {
                hashMap.putAll(b.f7698b);
            }
        }
        for (java.lang.Class b2 : cls.getInterfaces()) {
            for (java.util.Map.Entry next : mo13898b(b2).f7698b.entrySet()) {
                mo13896a(hashMap, (com.fossil.blesdk.obfuscated.C2471nb.C2473b) next.getKey(), (androidx.lifecycle.Lifecycle.Event) next.getValue(), cls);
            }
        }
        if (methodArr == null) {
            methodArr = mo13897a(cls);
        }
        boolean z = false;
        for (java.lang.reflect.Method method : methodArr) {
            com.fossil.blesdk.obfuscated.C1613dc dcVar = (com.fossil.blesdk.obfuscated.C1613dc) method.getAnnotation(com.fossil.blesdk.obfuscated.C1613dc.class);
            if (dcVar != null) {
                java.lang.Class[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length <= 0) {
                    i = 0;
                } else if (parameterTypes[0].isAssignableFrom(androidx.lifecycle.LifecycleOwner.class)) {
                    i = 1;
                } else {
                    throw new java.lang.IllegalArgumentException("invalid parameter type. Must be one and instanceof LifecycleOwner");
                }
                androidx.lifecycle.Lifecycle.Event value = dcVar.value();
                if (parameterTypes.length > 1) {
                    if (!parameterTypes[1].isAssignableFrom(androidx.lifecycle.Lifecycle.Event.class)) {
                        throw new java.lang.IllegalArgumentException("invalid parameter type. second arg must be an event");
                    } else if (value == androidx.lifecycle.Lifecycle.Event.ON_ANY) {
                        i = 2;
                    } else {
                        throw new java.lang.IllegalArgumentException("Second arg is supported only for ON_ANY value");
                    }
                }
                if (parameterTypes.length <= 2) {
                    mo13896a(hashMap, new com.fossil.blesdk.obfuscated.C2471nb.C2473b(i, method), value, cls);
                    z = true;
                } else {
                    throw new java.lang.IllegalArgumentException("cannot have more than 2 params");
                }
            }
        }
        com.fossil.blesdk.obfuscated.C2471nb.C2472a aVar = new com.fossil.blesdk.obfuscated.C2471nb.C2472a(hashMap);
        this.f7695a.put(cls, aVar);
        this.f7696b.put(cls, java.lang.Boolean.valueOf(z));
        return aVar;
    }
}
