package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.data.file.FileType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p20 extends s20<qa4, DeviceInformation> {
    @DexIgnore
    public static /* final */ k20<qa4>[] a; // = new k20[0];
    @DexIgnore
    public static /* final */ l20<DeviceInformation>[] b; // = {new a(FileType.DEVICE_INFO), new b(FileType.DEVICE_INFO)};
    @DexIgnore
    public static /* final */ p20 c; // = new p20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends t20<DeviceInformation> {
        @DexIgnore
        public a(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public DeviceInformation b(byte[] bArr) {
            kd4.b(bArr, "data");
            return p20.c.b(bArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends u20<DeviceInformation> {
        @DexIgnore
        public b(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public DeviceInformation b(byte[] bArr) {
            kd4.b(bArr, "data");
            return p20.c.b(bArr);
        }
    }

    @DexIgnore
    public l20<DeviceInformation>[] b() {
        return b;
    }

    @DexIgnore
    public k20<qa4>[] a() {
        return a;
    }

    @DexIgnore
    public final DeviceInformation b(byte[] bArr) {
        return DeviceInformation.Companion.a(ya4.a(bArr, 12, bArr.length - 4));
    }
}
