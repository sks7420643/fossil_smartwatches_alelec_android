package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pz0 extends k21<v01> {
    @DexIgnore
    public static /* final */ de0.g<pz0> E; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0<Object> F; // = new de0<>("Fitness.GOALS_API", new rz0(), E);

    /*
    static {
        new de0("Fitness.GOALS_CLIENT", new sz0(), E);
    }
    */

    @DexIgnore
    public pz0(Context context, Looper looper, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 125, bVar, cVar, kj0);
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitGoalsApi");
        if (queryLocalInterface instanceof v01) {
            return (v01) queryLocalInterface;
        }
        return new w01(iBinder);
    }

    @DexIgnore
    public final int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String y() {
        return "com.google.android.gms.fitness.internal.IGoogleFitGoalsApi";
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.GoalsApi";
    }
}
