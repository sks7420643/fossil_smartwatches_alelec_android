package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mi extends ri implements li {
    @DexIgnore
    public b f;
    @DexIgnore
    public Context g;
    @DexIgnore
    public ArgbEvaluator h;
    @DexIgnore
    public /* final */ Drawable.Callback i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Drawable.Callback {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            mi.this.invalidateSelf();
        }

        @DexIgnore
        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            mi.this.scheduleSelf(runnable, j);
        }

        @DexIgnore
        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            mi.this.unscheduleSelf(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public si b;
        @DexIgnore
        public AnimatorSet c;
        @DexIgnore
        public ArrayList<Animator> d;
        @DexIgnore
        public g4<Animator, String> e;

        @DexIgnore
        public b(Context context, b bVar, Drawable.Callback callback, Resources resources) {
            if (bVar != null) {
                this.a = bVar.a;
                si siVar = bVar.b;
                if (siVar != null) {
                    Drawable.ConstantState constantState = siVar.getConstantState();
                    if (resources != null) {
                        this.b = (si) constantState.newDrawable(resources);
                    } else {
                        this.b = (si) constantState.newDrawable();
                    }
                    si siVar2 = this.b;
                    siVar2.mutate();
                    this.b = siVar2;
                    this.b.setCallback(callback);
                    this.b.setBounds(bVar.b.getBounds());
                    this.b.a(false);
                }
                ArrayList<Animator> arrayList = bVar.d;
                if (arrayList != null) {
                    int size = arrayList.size();
                    this.d = new ArrayList<>(size);
                    this.e = new g4<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = bVar.d.get(i);
                        Animator clone = animator.clone();
                        String str = bVar.e.get(animator);
                        clone.setTarget(this.b.a(str));
                        this.d.add(clone);
                        this.e.put(clone, str);
                    }
                    a();
                }
            }
        }

        @DexIgnore
        public void a() {
            if (this.c == null) {
                this.c = new AnimatorSet();
            }
            this.c.playTogether(this.d);
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }
    }

    @DexIgnore
    public mi() {
        this((Context) null, (b) null, (Resources) null);
    }

    @DexIgnore
    public static mi a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        mi miVar = new mi(context);
        miVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return miVar;
    }

    @DexIgnore
    public void applyTheme(Resources.Theme theme) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, theme);
        }
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return c7.a(drawable);
        }
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        this.f.b.draw(canvas);
        if (this.f.c.isStarted()) {
            invalidateSelf();
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return c7.c(drawable);
        }
        return this.f.b.getAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f.a;
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.e;
        if (drawable == null || Build.VERSION.SDK_INT < 24) {
            return null;
        }
        return new c(drawable.getConstantState());
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return this.f.b.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return this.f.b.getIntrinsicWidth();
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return this.f.b.getOpacity();
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if ("animated-vector".equals(name)) {
                    TypedArray a2 = s6.a(resources, theme, attributeSet, ki.e);
                    int resourceId = a2.getResourceId(0, 0);
                    if (resourceId != 0) {
                        si a3 = si.a(resources, resourceId, theme);
                        a3.a(false);
                        a3.setCallback(this.i);
                        si siVar = this.f.b;
                        if (siVar != null) {
                            siVar.setCallback((Drawable.Callback) null);
                        }
                        this.f.b = a3;
                    }
                    a2.recycle();
                } else if ("target".equals(name)) {
                    TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, ki.f);
                    String string = obtainAttributes.getString(0);
                    int resourceId2 = obtainAttributes.getResourceId(1, 0);
                    if (resourceId2 != 0) {
                        Context context = this.g;
                        if (context != null) {
                            a(string, oi.a(context, resourceId2));
                        } else {
                            obtainAttributes.recycle();
                            throw new IllegalStateException("Context can't be null when inflating animators");
                        }
                    }
                    obtainAttributes.recycle();
                } else {
                    continue;
                }
            }
            eventType = xmlPullParser.next();
        }
        this.f.a();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return c7.f(drawable);
        }
        return this.f.b.isAutoMirrored();
    }

    @DexIgnore
    public boolean isRunning() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return ((AnimatedVectorDrawable) drawable).isRunning();
        }
        return this.f.c.isRunning();
    }

    @DexIgnore
    public boolean isStateful() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.isStateful();
        }
        return this.f.b.isStateful();
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.mutate();
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setBounds(rect);
        } else {
            this.f.b.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setLevel(i2);
        }
        return this.f.b.setLevel(i2);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        return this.f.b.setState(iArr);
    }

    @DexIgnore
    public void setAlpha(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else {
            this.f.b.setAlpha(i2);
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, z);
        } else {
            this.f.b.setAutoMirrored(z);
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
        } else {
            this.f.b.setColorFilter(colorFilter);
        }
    }

    @DexIgnore
    public void setTint(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.b(drawable, i2);
        } else {
            this.f.b.setTint(i2);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, colorStateList);
        } else {
            this.f.b.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, mode);
        } else {
            this.f.b.setTintMode(mode);
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        this.f.b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        Drawable drawable = this.e;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).start();
        } else if (!this.f.c.isStarted()) {
            this.f.c.start();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void stop() {
        Drawable drawable = this.e;
        if (drawable != null) {
            ((AnimatedVectorDrawable) drawable).stop();
        } else {
            this.f.c.end();
        }
    }

    @DexIgnore
    public mi(Context context) {
        this(context, (b) null, (Resources) null);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Drawable.ConstantState a;

        @DexIgnore
        public c(Drawable.ConstantState constantState) {
            this.a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            mi miVar = new mi();
            miVar.e = this.a.newDrawable();
            miVar.e.setCallback(miVar.i);
            return miVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            mi miVar = new mi();
            miVar.e = this.a.newDrawable(resources);
            miVar.e.setCallback(miVar.i);
            return miVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            mi miVar = new mi();
            miVar.e = this.a.newDrawable(resources, theme);
            miVar.e.setCallback(miVar.i);
            return miVar;
        }
    }

    @DexIgnore
    public mi(Context context, b bVar, Resources resources) {
        this.h = null;
        this.i = new a();
        this.g = context;
        if (bVar != null) {
            this.f = bVar;
        } else {
            this.f = new b(context, bVar, this.i, resources);
        }
    }

    @DexIgnore
    public final void a(Animator animator) {
        if (animator instanceof AnimatorSet) {
            ArrayList<Animator> childAnimations = ((AnimatorSet) animator).getChildAnimations();
            if (childAnimations != null) {
                for (int i2 = 0; i2 < childAnimations.size(); i2++) {
                    a(childAnimations.get(i2));
                }
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.h == null) {
                    this.h = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.h);
            }
        }
    }

    @DexIgnore
    public final void a(String str, Animator animator) {
        animator.setTarget(this.f.b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        b bVar = this.f;
        if (bVar.d == null) {
            bVar.d = new ArrayList<>();
            this.f.e = new g4<>();
        }
        this.f.d.add(animator);
        this.f.e.put(animator, str);
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
    }
}
