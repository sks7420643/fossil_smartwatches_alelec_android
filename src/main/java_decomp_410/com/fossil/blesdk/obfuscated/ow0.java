package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ow0 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int e;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> f;
    @DexIgnore
    public /* final */ /* synthetic */ mw0 g;

    @DexIgnore
    public ow0(mw0 mw0) {
        this.g = mw0;
        this.e = this.g.f.size();
    }

    @DexIgnore
    public /* synthetic */ ow0(mw0 mw0, nw0 nw0) {
        this(mw0);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.f == null) {
            this.f = this.g.j.entrySet().iterator();
        }
        return this.f;
    }

    @DexIgnore
    public final boolean hasNext() {
        int i = this.e;
        return (i > 0 && i <= this.g.f.size()) || a().hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        Object obj;
        if (a().hasNext()) {
            obj = a().next();
        } else {
            List b = this.g.f;
            int i = this.e - 1;
            this.e = i;
            obj = b.get(i);
        }
        return (Map.Entry) obj;
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
