package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.VideoUploader;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.DeferredCoroutine;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ag4 {
    @DexIgnore
    public static /* synthetic */ gh4 a(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4 yc4, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 2) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        return yf4.a(zg4, coroutineContext, coroutineStart, yc4);
    }

    @DexIgnore
    public static /* synthetic */ fi4 b(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4 yc4, int i, Object obj) {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        if ((i & 2) != 0) {
            coroutineStart = CoroutineStart.DEFAULT;
        }
        return yf4.b(zg4, coroutineContext, coroutineStart, yc4);
    }

    @DexIgnore
    public static final <T> gh4<T> a(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4) {
        DeferredCoroutine deferredCoroutine;
        kd4.b(zg4, "$this$async");
        kd4.b(coroutineContext, "context");
        kd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        kd4.b(yc4, "block");
        CoroutineContext a = tg4.a(zg4, coroutineContext);
        if (coroutineStart.isLazy()) {
            deferredCoroutine = new ni4(a, yc4);
        } else {
            deferredCoroutine = new DeferredCoroutine(a, true);
        }
        deferredCoroutine.a(coroutineStart, deferredCoroutine, yc4);
        return deferredCoroutine;
    }

    @DexIgnore
    public static final fi4 b(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4<? super zg4, ? super yb4<? super qa4>, ? extends Object> yc4) {
        uf4 uf4;
        kd4.b(zg4, "$this$launch");
        kd4.b(coroutineContext, "context");
        kd4.b(coroutineStart, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        kd4.b(yc4, "block");
        CoroutineContext a = tg4.a(zg4, coroutineContext);
        if (coroutineStart.isLazy()) {
            uf4 = new oi4(a, yc4);
        } else {
            uf4 = new xi4(a, true);
        }
        uf4.a(coroutineStart, uf4, yc4);
        return uf4;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> Object a(CoroutineContext coroutineContext, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4, yb4<? super T> yb4) {
        Object obj;
        CoroutineContext context = yb4.getContext();
        CoroutineContext plus = context.plus(coroutineContext);
        hj4.a(plus);
        if (plus == context) {
            ak4 ak4 = new ak4(plus, yb4);
            obj = lk4.a(ak4, ak4, yc4);
        } else if (kd4.a((Object) (zb4) plus.get(zb4.b), (Object) (zb4) context.get(zb4.b))) {
            gj4 gj4 = new gj4(plus, yb4);
            Object b = ThreadContextKt.b(plus, (Object) null);
            try {
                Object a = lk4.a(gj4, gj4, yc4);
                ThreadContextKt.a(plus, b);
                obj = a;
            } catch (Throwable th) {
                ThreadContextKt.a(plus, b);
                throw th;
            }
        } else {
            kh4 kh4 = new kh4(plus, yb4);
            kh4.k();
            kk4.a(yc4, kh4, kh4);
            obj = kh4.m();
        }
        if (obj == cc4.a()) {
            ic4.c(yb4);
        }
        return obj;
    }
}
