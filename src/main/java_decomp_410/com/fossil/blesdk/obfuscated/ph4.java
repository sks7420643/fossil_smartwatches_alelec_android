package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ph4 extends bg4 {
    @DexIgnore
    public /* final */ oh4 e;

    @DexIgnore
    public ph4(oh4 oh4) {
        kd4.b(oh4, "handle");
        this.e = oh4;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.e.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCancel[" + this.e + ']';
    }
}
