package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oi */
public class C2570oi {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oi$a */
    public static class C2571a implements android.animation.TypeEvaluator<com.fossil.blesdk.obfuscated.C3009u6.C3011b[]> {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3009u6.C3011b[] f8132a;

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3009u6.C3011b[] evaluate(float f, com.fossil.blesdk.obfuscated.C3009u6.C3011b[] bVarArr, com.fossil.blesdk.obfuscated.C3009u6.C3011b[] bVarArr2) {
            if (com.fossil.blesdk.obfuscated.C3009u6.m14580a(bVarArr, bVarArr2)) {
                com.fossil.blesdk.obfuscated.C3009u6.C3011b[] bVarArr3 = this.f8132a;
                if (bVarArr3 == null || !com.fossil.blesdk.obfuscated.C3009u6.m14580a(bVarArr3, bVarArr)) {
                    this.f8132a = com.fossil.blesdk.obfuscated.C3009u6.m14583a(bVarArr);
                }
                for (int i = 0; i < bVarArr.length; i++) {
                    this.f8132a[i].mo16665a(bVarArr[i], bVarArr2[i], f);
                }
                return this.f8132a;
            }
            throw new java.lang.IllegalArgumentException("Can't interpolate between two incompatible pathData");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m11792a(android.content.Context context, int i) throws android.content.res.Resources.NotFoundException {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return android.animation.AnimatorInflater.loadAnimator(context, i);
        }
        return m11793a(context, context.getResources(), context.getTheme(), i);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m11807a(int i) {
        return i >= 28 && i <= 31;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m11793a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, int i) throws android.content.res.Resources.NotFoundException {
        return m11794a(context, resources, theme, i, 1.0f);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m11794a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, int i, float f) throws android.content.res.Resources.NotFoundException {
        android.content.res.XmlResourceParser xmlResourceParser = null;
        try {
            android.content.res.XmlResourceParser animation = resources.getAnimation(i);
            android.animation.Animator a = m11795a(context, resources, theme, (org.xmlpull.v1.XmlPullParser) animation, f);
            if (animation != null) {
                animation.close();
            }
            return a;
        } catch (org.xmlpull.v1.XmlPullParserException e) {
            android.content.res.Resources.NotFoundException notFoundException = new android.content.res.Resources.NotFoundException("Can't load animation resource ID #0x" + java.lang.Integer.toHexString(i));
            notFoundException.initCause(e);
            throw notFoundException;
        } catch (java.io.IOException e2) {
            android.content.res.Resources.NotFoundException notFoundException2 = new android.content.res.Resources.NotFoundException("Can't load animation resource ID #0x" + java.lang.Integer.toHexString(i));
            notFoundException2.initCause(e2);
            throw notFoundException2;
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v6, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v26, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v29, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    public static android.animation.PropertyValuesHolder m11801a(android.content.res.TypedArray typedArray, int i, int i2, int i3, java.lang.String str) {
        int i4;
        int i5;
        int i6;
        android.animation.PropertyValuesHolder propertyValuesHolder;
        float f;
        float f2;
        float f3;
        android.animation.PropertyValuesHolder propertyValuesHolder2;
        android.util.TypedValue peekValue = typedArray.peekValue(i2);
        boolean z = peekValue != null;
        int i7 = z ? peekValue.type : 0;
        android.util.TypedValue peekValue2 = typedArray.peekValue(i3);
        boolean z2 = peekValue2 != null;
        int i8 = z2 ? peekValue2.type : 0;
        if (i == 4) {
            i = ((!z || !m11807a(i7)) && (!z2 || !m11807a(i8))) ? 0 : 3;
        }
        boolean z3 = i == 0;
        android.animation.PropertyValuesHolder propertyValuesHolder3 = null;
        if (i == 2) {
            java.lang.String string = typedArray.getString(i2);
            java.lang.String string2 = typedArray.getString(i3);
            com.fossil.blesdk.obfuscated.C3009u6.C3011b[] a = com.fossil.blesdk.obfuscated.C3009u6.m14582a(string);
            com.fossil.blesdk.obfuscated.C3009u6.C3011b[] a2 = com.fossil.blesdk.obfuscated.C3009u6.m14582a(string2);
            if (a == null && a2 == null) {
                return null;
            }
            if (a != null) {
                com.fossil.blesdk.obfuscated.C2570oi.C2571a aVar = new com.fossil.blesdk.obfuscated.C2570oi.C2571a();
                if (a2 == null) {
                    propertyValuesHolder2 = android.animation.PropertyValuesHolder.ofObject(str, aVar, new java.lang.Object[]{a});
                } else if (com.fossil.blesdk.obfuscated.C3009u6.m14580a(a, a2)) {
                    propertyValuesHolder2 = android.animation.PropertyValuesHolder.ofObject(str, aVar, new java.lang.Object[]{a, a2});
                } else {
                    throw new android.view.InflateException(" Can't morph from " + string + " to " + string2);
                }
                return propertyValuesHolder2;
            } else if (a2 == null) {
                return null;
            } else {
                return android.animation.PropertyValuesHolder.ofObject(str, new com.fossil.blesdk.obfuscated.C2570oi.C2571a(), new java.lang.Object[]{a2});
            }
        } else {
            com.fossil.blesdk.obfuscated.C2655pi a3 = i == 3 ? com.fossil.blesdk.obfuscated.C2655pi.m12276a() : null;
            if (z3) {
                if (z) {
                    if (i7 == 5) {
                        f2 = typedArray.getDimension(i2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else {
                        f2 = typedArray.getFloat(i2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    if (z2) {
                        if (i8 == 5) {
                            f3 = typedArray.getDimension(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            f3 = typedArray.getFloat(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        }
                        propertyValuesHolder = android.animation.PropertyValuesHolder.ofFloat(str, new float[]{f2, f3});
                    } else {
                        propertyValuesHolder = android.animation.PropertyValuesHolder.ofFloat(str, new float[]{f2});
                    }
                } else {
                    if (i8 == 5) {
                        f = typedArray.getDimension(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else {
                        f = typedArray.getFloat(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    propertyValuesHolder = android.animation.PropertyValuesHolder.ofFloat(str, new float[]{f});
                }
                propertyValuesHolder3 = propertyValuesHolder;
            } else if (z) {
                if (i7 == 5) {
                    i5 = (int) typedArray.getDimension(i2, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (m11807a(i7)) {
                    i5 = typedArray.getColor(i2, 0);
                } else {
                    i5 = typedArray.getInt(i2, 0);
                }
                if (z2) {
                    if (i8 == 5) {
                        i6 = (int) typedArray.getDimension(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else if (m11807a(i8)) {
                        i6 = typedArray.getColor(i3, 0);
                    } else {
                        i6 = typedArray.getInt(i3, 0);
                    }
                    propertyValuesHolder3 = android.animation.PropertyValuesHolder.ofInt(str, new int[]{i5, i6});
                } else {
                    propertyValuesHolder3 = android.animation.PropertyValuesHolder.ofInt(str, new int[]{i5});
                }
            } else if (z2) {
                if (i8 == 5) {
                    i4 = (int) typedArray.getDimension(i3, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (m11807a(i8)) {
                    i4 = typedArray.getColor(i3, 0);
                } else {
                    i4 = typedArray.getInt(i3, 0);
                }
                propertyValuesHolder3 = android.animation.PropertyValuesHolder.ofInt(str, new int[]{i4});
            }
            if (propertyValuesHolder3 == null || a3 == null) {
                return propertyValuesHolder3;
            }
            propertyValuesHolder3.setEvaluator(a3);
            return propertyValuesHolder3;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m11804a(android.animation.ValueAnimator valueAnimator, android.content.res.TypedArray typedArray, android.content.res.TypedArray typedArray2, float f, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        long b = (long) com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "duration", 1, 300);
        long b2 = (long) com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "startOffset", 2, 0);
        int b3 = com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, com.facebook.LegacyTokenHelper.JSON_VALUE_TYPE, 7, 4);
        if (com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "valueFrom") && com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser, "valueTo")) {
            if (b3 == 4) {
                b3 = m11791a(typedArray, 5, 6);
            }
            android.animation.PropertyValuesHolder a = m11801a(typedArray, b3, 5, 6, "");
            if (a != null) {
                valueAnimator.setValues(new android.animation.PropertyValuesHolder[]{a});
            }
        }
        valueAnimator.setDuration(b);
        valueAnimator.setStartDelay(b2);
        valueAnimator.setRepeatCount(com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "repeatCount", 3, 0));
        valueAnimator.setRepeatMode(com.fossil.blesdk.obfuscated.C2863s6.m13546b(typedArray, xmlPullParser, "repeatMode", 4, 1));
        if (typedArray2 != null) {
            m11803a(valueAnimator, typedArray2, b3, f, xmlPullParser);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m11803a(android.animation.ValueAnimator valueAnimator, android.content.res.TypedArray typedArray, int i, float f, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        android.animation.ObjectAnimator objectAnimator = (android.animation.ObjectAnimator) valueAnimator;
        java.lang.String a = com.fossil.blesdk.obfuscated.C2863s6.m13543a(typedArray, xmlPullParser, "pathData", 1);
        if (a != null) {
            java.lang.String a2 = com.fossil.blesdk.obfuscated.C2863s6.m13543a(typedArray, xmlPullParser, "propertyXName", 2);
            java.lang.String a3 = com.fossil.blesdk.obfuscated.C2863s6.m13543a(typedArray, xmlPullParser, "propertyYName", 3);
            if (i != 2) {
            }
            if (a2 == null && a3 == null) {
                throw new android.view.InflateException(typedArray.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
            }
            m11805a(com.fossil.blesdk.obfuscated.C3009u6.m14584b(a), objectAnimator, f * 0.5f, a2, a3);
            return;
        }
        objectAnimator.setPropertyName(com.fossil.blesdk.obfuscated.C2863s6.m13543a(typedArray, xmlPullParser, "propertyName", 0));
    }

    @DexIgnore
    /* renamed from: a */
    public static void m11805a(android.graphics.Path path, android.animation.ObjectAnimator objectAnimator, float f, java.lang.String str, java.lang.String str2) {
        android.animation.PropertyValuesHolder propertyValuesHolder;
        android.graphics.Path path2 = path;
        android.animation.ObjectAnimator objectAnimator2 = objectAnimator;
        java.lang.String str3 = str;
        java.lang.String str4 = str2;
        android.graphics.PathMeasure pathMeasure = new android.graphics.PathMeasure(path2, false);
        java.util.ArrayList arrayList = new java.util.ArrayList();
        arrayList.add(java.lang.Float.valueOf(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        float f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        do {
            f2 += pathMeasure.getLength();
            arrayList.add(java.lang.Float.valueOf(f2));
        } while (pathMeasure.nextContour());
        android.graphics.PathMeasure pathMeasure2 = new android.graphics.PathMeasure(path2, false);
        int min = java.lang.Math.min(100, ((int) (f2 / f)) + 1);
        float[] fArr = new float[min];
        float[] fArr2 = new float[min];
        float[] fArr3 = new float[2];
        float f3 = f2 / ((float) (min - 1));
        int i = 0;
        float f4 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i2 = 0;
        while (true) {
            propertyValuesHolder = null;
            if (i >= min) {
                break;
            }
            pathMeasure2.getPosTan(f4 - ((java.lang.Float) arrayList.get(i2)).floatValue(), fArr3, (float[]) null);
            fArr[i] = fArr3[0];
            fArr2[i] = fArr3[1];
            f4 += f3;
            int i3 = i2 + 1;
            if (i3 < arrayList.size() && f4 > ((java.lang.Float) arrayList.get(i3)).floatValue()) {
                pathMeasure2.nextContour();
                i2 = i3;
            }
            i++;
        }
        android.animation.PropertyValuesHolder ofFloat = str3 != null ? android.animation.PropertyValuesHolder.ofFloat(str3, fArr) : null;
        if (str4 != null) {
            propertyValuesHolder = android.animation.PropertyValuesHolder.ofFloat(str4, fArr2);
        }
        if (ofFloat == null) {
            objectAnimator2.setValues(new android.animation.PropertyValuesHolder[]{propertyValuesHolder});
        } else if (propertyValuesHolder == null) {
            objectAnimator2.setValues(new android.animation.PropertyValuesHolder[]{ofFloat});
        } else {
            objectAnimator2.setValues(new android.animation.PropertyValuesHolder[]{ofFloat, propertyValuesHolder});
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m11795a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser, float f) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        return m11796a(context, resources, theme, xmlPullParser, android.util.Xml.asAttributeSet(xmlPullParser), (android.animation.AnimatorSet) null, 0, f);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ba  */
    /* renamed from: a */
    public static android.animation.Animator m11796a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.animation.AnimatorSet animatorSet, int i, float f) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int i2;
        android.content.res.Resources resources2 = resources;
        android.content.res.Resources.Theme theme2 = theme;
        org.xmlpull.v1.XmlPullParser xmlPullParser2 = xmlPullParser;
        android.animation.AnimatorSet animatorSet2 = animatorSet;
        int depth = xmlPullParser.getDepth();
        android.animation.AnimatorSet animatorSet3 = null;
        java.util.ArrayList arrayList = null;
        while (true) {
            int next = xmlPullParser.next();
            i2 = 0;
            if ((next != 3 || xmlPullParser.getDepth() > depth) && next != 1) {
                if (next == 2) {
                    java.lang.String name = xmlPullParser.getName();
                    if (name.equals("objectAnimator")) {
                        animatorSet3 = m11799a(context, resources, theme, attributeSet, f, xmlPullParser);
                    } else if (name.equals("animator")) {
                        animatorSet3 = m11802a(context, resources, theme, attributeSet, (android.animation.ValueAnimator) null, f, xmlPullParser);
                    } else {
                        if (name.equals("set")) {
                            android.animation.AnimatorSet animatorSet4 = new android.animation.AnimatorSet();
                            android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources2, theme2, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6874h);
                            m11796a(context, resources, theme, xmlPullParser, attributeSet, animatorSet4, com.fossil.blesdk.obfuscated.C2863s6.m13546b(a, xmlPullParser2, "ordering", 0, 0), f);
                            a.recycle();
                            android.content.Context context2 = context;
                            animatorSet3 = animatorSet4;
                        } else if (name.equals("propertyValuesHolder")) {
                            android.animation.PropertyValuesHolder[] a2 = m11808a(context, resources2, theme2, xmlPullParser2, android.util.Xml.asAttributeSet(xmlPullParser));
                            if (!(a2 == null || animatorSet3 == null || !(animatorSet3 instanceof android.animation.ValueAnimator))) {
                                ((android.animation.ValueAnimator) animatorSet3).setValues(a2);
                            }
                            i2 = 1;
                        } else {
                            throw new java.lang.RuntimeException("Unknown animator name: " + xmlPullParser.getName());
                        }
                        if (animatorSet2 != null && i2 == 0) {
                            if (arrayList == null) {
                                arrayList = new java.util.ArrayList();
                            }
                            arrayList.add(animatorSet3);
                        }
                    }
                    android.content.Context context3 = context;
                    if (arrayList == null) {
                    }
                    arrayList.add(animatorSet3);
                }
            }
        }
        if (!(animatorSet2 == null || arrayList == null)) {
            android.animation.Animator[] animatorArr = new android.animation.Animator[arrayList.size()];
            java.util.Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                animatorArr[i2] = (android.animation.Animator) it.next();
                i2++;
            }
            if (i == 0) {
                animatorSet2.playTogether(animatorArr);
            } else {
                animatorSet2.playSequentially(animatorArr);
            }
        }
        return animatorSet3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    /* renamed from: a */
    public static android.animation.PropertyValuesHolder[] m11808a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        org.xmlpull.v1.XmlPullParser xmlPullParser2 = xmlPullParser;
        android.animation.PropertyValuesHolder[] propertyValuesHolderArr = null;
        java.util.ArrayList arrayList = null;
        while (true) {
            int eventType = xmlPullParser.getEventType();
            if (eventType == 3 || eventType == 1) {
                if (arrayList != null) {
                    int size = arrayList.size();
                    propertyValuesHolderArr = new android.animation.PropertyValuesHolder[size];
                    for (int i = 0; i < size; i++) {
                        propertyValuesHolderArr[i] = (android.animation.PropertyValuesHolder) arrayList.get(i);
                    }
                }
            } else if (eventType != 2) {
                xmlPullParser.next();
            } else {
                if (xmlPullParser.getName().equals("propertyValuesHolder")) {
                    android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6875i);
                    java.lang.String a2 = com.fossil.blesdk.obfuscated.C2863s6.m13543a(a, xmlPullParser2, "propertyName", 3);
                    int b = com.fossil.blesdk.obfuscated.C2863s6.m13546b(a, xmlPullParser2, com.facebook.LegacyTokenHelper.JSON_VALUE_TYPE, 2, 4);
                    int i2 = b;
                    android.animation.PropertyValuesHolder a3 = m11800a(context, resources, theme, xmlPullParser, a2, b);
                    if (a3 == null) {
                        a3 = m11801a(a, i2, 0, 1, a2);
                    }
                    if (a3 != null) {
                        if (arrayList == null) {
                            arrayList = new java.util.ArrayList();
                        }
                        arrayList.add(a3);
                    }
                    a.recycle();
                } else {
                    android.content.res.Resources resources2 = resources;
                    android.content.res.Resources.Theme theme2 = theme;
                    android.util.AttributeSet attributeSet2 = attributeSet;
                }
                xmlPullParser.next();
            }
        }
        if (arrayList != null) {
        }
        return propertyValuesHolderArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11790a(android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, org.xmlpull.v1.XmlPullParser xmlPullParser) {
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6876j);
        int i = 0;
        android.util.TypedValue b = com.fossil.blesdk.obfuscated.C2863s6.m13547b(a, xmlPullParser, "value", 0);
        if ((b != null) && m11807a(b.type)) {
            i = 3;
        }
        a.recycle();
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11791a(android.content.res.TypedArray typedArray, int i, int i2) {
        android.util.TypedValue peekValue = typedArray.peekValue(i);
        boolean z = true;
        boolean z2 = peekValue != null;
        int i3 = z2 ? peekValue.type : 0;
        android.util.TypedValue peekValue2 = typedArray.peekValue(i2);
        if (peekValue2 == null) {
            z = false;
        }
        int i4 = z ? peekValue2.type : 0;
        if ((!z2 || !m11807a(i3)) && (!z || !m11807a(i4))) {
            return 0;
        }
        return 3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0043  */
    /* renamed from: a */
    public static android.animation.PropertyValuesHolder m11800a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, org.xmlpull.v1.XmlPullParser xmlPullParser, java.lang.String str, int i) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.animation.PropertyValuesHolder propertyValuesHolder = null;
        int i2 = i;
        java.util.ArrayList arrayList = null;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 3 || next == 1) {
                if (arrayList != null) {
                    int size = arrayList.size();
                    if (size > 0) {
                        android.animation.Keyframe keyframe = (android.animation.Keyframe) arrayList.get(0);
                        android.animation.Keyframe keyframe2 = (android.animation.Keyframe) arrayList.get(size - 1);
                        float fraction = keyframe2.getFraction();
                        if (fraction < 1.0f) {
                            if (fraction < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                keyframe2.setFraction(1.0f);
                            } else {
                                arrayList.add(arrayList.size(), m11797a(keyframe2, 1.0f));
                                size++;
                            }
                        }
                        float fraction2 = keyframe.getFraction();
                        if (fraction2 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            if (fraction2 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                keyframe.setFraction(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                arrayList.add(0, m11797a(keyframe, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                                size++;
                            }
                        }
                        android.animation.Keyframe[] keyframeArr = new android.animation.Keyframe[size];
                        arrayList.toArray(keyframeArr);
                        for (int i3 = 0; i3 < size; i3++) {
                            android.animation.Keyframe keyframe3 = keyframeArr[i3];
                            if (keyframe3.getFraction() < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                if (i3 == 0) {
                                    keyframe3.setFraction(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                                } else {
                                    int i4 = size - 1;
                                    if (i3 == i4) {
                                        keyframe3.setFraction(1.0f);
                                    } else {
                                        int i5 = i3 + 1;
                                        int i6 = i3;
                                        while (i5 < i4 && keyframeArr[i5].getFraction() < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                            i6 = i5;
                                            i5++;
                                        }
                                        m11806a(keyframeArr, keyframeArr[i6 + 1].getFraction() - keyframeArr[i3 - 1].getFraction(), i3, i6);
                                    }
                                }
                            }
                        }
                        propertyValuesHolder = android.animation.PropertyValuesHolder.ofKeyframe(str, keyframeArr);
                        if (i2 == 3) {
                            propertyValuesHolder.setEvaluator(com.fossil.blesdk.obfuscated.C2655pi.m12276a());
                        }
                    }
                }
            } else if (xmlPullParser.getName().equals("keyframe")) {
                if (i2 == 4) {
                    i2 = m11790a(resources, theme, android.util.Xml.asAttributeSet(xmlPullParser), xmlPullParser);
                }
                android.animation.Keyframe a = m11798a(context, resources, theme, android.util.Xml.asAttributeSet(xmlPullParser), i2, xmlPullParser);
                if (a != null) {
                    if (arrayList == null) {
                        arrayList = new java.util.ArrayList();
                    }
                    arrayList.add(a);
                }
                xmlPullParser.next();
            }
        }
        if (arrayList != null) {
        }
        return propertyValuesHolder;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Keyframe m11797a(android.animation.Keyframe keyframe, float f) {
        if (keyframe.getType() == java.lang.Float.TYPE) {
            return android.animation.Keyframe.ofFloat(f);
        }
        if (keyframe.getType() == java.lang.Integer.TYPE) {
            return android.animation.Keyframe.ofInt(f);
        }
        return android.animation.Keyframe.ofObject(f);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m11806a(android.animation.Keyframe[] keyframeArr, float f, int i, int i2) {
        float f2 = f / ((float) ((i2 - i) + 2));
        while (i <= i2) {
            keyframeArr[i].setFraction(keyframeArr[i - 1].getFraction() + f2);
            i++;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Keyframe m11798a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, int i, org.xmlpull.v1.XmlPullParser xmlPullParser) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.animation.Keyframe keyframe;
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6876j);
        float a2 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser, "fraction", 3, -1.0f);
        android.util.TypedValue b = com.fossil.blesdk.obfuscated.C2863s6.m13547b(a, xmlPullParser, "value", 0);
        boolean z = b != null;
        if (i == 4) {
            i = (!z || !m11807a(b.type)) ? 0 : 3;
        }
        if (z) {
            if (i == 0) {
                keyframe = android.animation.Keyframe.ofFloat(a2, com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser, "value", 0, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            } else if (i == 1 || i == 3) {
                keyframe = android.animation.Keyframe.ofInt(a2, com.fossil.blesdk.obfuscated.C2863s6.m13546b(a, xmlPullParser, "value", 0, 0));
            } else {
                keyframe = null;
            }
        } else if (i == 0) {
            keyframe = android.animation.Keyframe.ofFloat(a2);
        } else {
            keyframe = android.animation.Keyframe.ofInt(a2);
        }
        int c = com.fossil.blesdk.obfuscated.C2863s6.m13548c(a, xmlPullParser, "interpolator", 1, 0);
        if (c > 0) {
            keyframe.setInterpolator(com.fossil.blesdk.obfuscated.C2491ni.m11269a(context, c));
        }
        a.recycle();
        return keyframe;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.ObjectAnimator m11799a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, float f, org.xmlpull.v1.XmlPullParser xmlPullParser) throws android.content.res.Resources.NotFoundException {
        android.animation.ObjectAnimator objectAnimator = new android.animation.ObjectAnimator();
        m11802a(context, resources, theme, attributeSet, objectAnimator, f, xmlPullParser);
        return objectAnimator;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.ValueAnimator m11802a(android.content.Context context, android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, android.animation.ValueAnimator valueAnimator, float f, org.xmlpull.v1.XmlPullParser xmlPullParser) throws android.content.res.Resources.NotFoundException {
        android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6873g);
        android.content.res.TypedArray a2 = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C2223ki.f6877k);
        if (valueAnimator == null) {
            valueAnimator = new android.animation.ValueAnimator();
        }
        m11804a(valueAnimator, a, a2, f, xmlPullParser);
        int c = com.fossil.blesdk.obfuscated.C2863s6.m13548c(a, xmlPullParser, "interpolator", 0, 0);
        if (c > 0) {
            valueAnimator.setInterpolator(com.fossil.blesdk.obfuscated.C2491ni.m11269a(context, c));
        }
        a.recycle();
        if (a2 != null) {
            a2.recycle();
        }
        return valueAnimator;
    }
}
