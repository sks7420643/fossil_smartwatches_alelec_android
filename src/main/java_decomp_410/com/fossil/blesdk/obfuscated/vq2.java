package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.util.SparseArray;
import com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig;
import com.fossil.blesdk.device.data.notification.NotificationVibePattern;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.CRC32;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vq2 extends CoroutineUseCase<b, d, c> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ e e; // = new e();
    @DexIgnore
    public /* final */ NotificationsRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ List<ContactWrapper> a;
        @DexIgnore
        public /* final */ List<AppWrapper> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(List<ContactWrapper> list, List<AppWrapper> list2, int i) {
            kd4.b(list, "contactWrapperList");
            kd4.b(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
            this.c = i;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.b;
        }

        @DexIgnore
        public final List<ContactWrapper> b() {
            return this.a;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            kd4.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            kd4.b(communicateMode, "communicateMode");
            kd4.b(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetNotificationFiltersUserCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS && vq2.this.d()) {
                boolean z = false;
                vq2.this.a(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    vq2.this.a(new d());
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                vq2.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public vq2(NotificationsRepository notificationsRepository, DeviceRepository deviceRepository) {
        kd4.b(notificationsRepository, "mNotificationsRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        this.f = notificationsRepository;
        this.g = deviceRepository;
    }

    @DexIgnore
    public final List<AppNotificationFilter> b(List<ContactWrapper> list, short s, boolean z) {
        short s2 = s;
        ArrayList arrayList = new ArrayList();
        for (ContactWrapper next : list) {
            short s3 = -1;
            if (next.isAdded()) {
                Contact contact = next.getContact();
                if (contact != null) {
                    if (contact.isUseCall()) {
                        if (z) {
                            s3 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                        }
                        NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s2, s2, s3, 10000);
                        DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                        FNotification fNotification = r8;
                        FNotification fNotification2 = new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), -1, "", aApplicationName.getNotificationType());
                        AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                        appNotificationFilter.setSender(contact.getDisplayName());
                        appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                        appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                        arrayList.add(appNotificationFilter);
                    }
                    if (contact.isUseSms()) {
                        if (z) {
                            s3 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                        }
                        NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(s2, s2, s3, 10000);
                        DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.MESSAGES;
                        AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType()));
                        appNotificationFilter2.setSender(contact.getDisplayName());
                        appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                        appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                        arrayList.add(appNotificationFilter2);
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public String c() {
        return "SetNotificationFiltersUserCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.e, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(b bVar, yb4<Object> yb4) {
        try {
            FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "running UseCase");
            this.d = true;
            Integer num = null;
            List<ContactWrapper> b2 = bVar != null ? bVar.b() : null;
            List<AppWrapper> a2 = bVar != null ? bVar.a() : null;
            if (bVar != null) {
                num = dc4.a(bVar.c());
            }
            if (b2 == null || a2 == null || num == null) {
                a(new c(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG, -1, new ArrayList()));
            } else {
                String e2 = PortfolioApp.W.c().e();
                boolean a3 = NotificationAppHelper.b.a(this.g.getSkuModelBySerialPrefix(DeviceHelper.o.b(e2)), e2);
                short c2 = (short) nl2.c(num.intValue());
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(a(this.f.getAllNotificationsByHour(e2, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), b2, a2, c2, a3));
                arrayList.addAll(b(b2, c2, a3));
                arrayList.addAll(a(a2, c2, a3));
                AppNotificationFilterSettings appNotificationFilterSettings = new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
                PortfolioApp.W.c().a(appNotificationFilterSettings, e2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetNotificationFiltersUserCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
            }
            return new Object();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SetNotificationFiltersUserCase", "Error inside SetNotificationFiltersUserCase.connectDevice - e=" + e3);
            return new c(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:108:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0041 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0082 A[EDGE_INSN: B:116:0x0082->B:27:0x0082 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0190 A[EDGE_INSN: B:120:0x0190->B:71:0x0190 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01c3  */
    public final List<AppNotificationFilter> a(SparseArray<List<BaseFeatureModel>> sparseArray, List<ContactWrapper> list, List<AppWrapper> list2, short s, boolean z) {
        T t;
        boolean z2;
        short s2;
        boolean z3;
        T t2;
        boolean z4;
        SparseArray<List<BaseFeatureModel>> sparseArray2 = sparseArray;
        ArrayList arrayList = new ArrayList();
        if (sparseArray2 != null) {
            int size = sparseArray.size();
            short s3 = -1;
            for (int i = 0; i < size; i++) {
                for (BaseFeatureModel baseFeatureModel : sparseArray2.valueAt(i)) {
                    if (baseFeatureModel.isEnabled()) {
                        if (baseFeatureModel instanceof ContactGroup) {
                            ContactGroup contactGroup = (ContactGroup) baseFeatureModel;
                            short s4 = s3;
                            boolean z5 = false;
                            short s5 = -1;
                            for (Contact next : contactGroup.getContacts()) {
                                Iterator<T> it = list.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        t2 = null;
                                        break;
                                    }
                                    t2 = it.next();
                                    ContactWrapper contactWrapper = (ContactWrapper) t2;
                                    if (contactWrapper.getContact() != null) {
                                        Contact contact = contactWrapper.getContact();
                                        if (contact != null) {
                                            int contactId = contact.getContactId();
                                            kd4.a((Object) next, "existedContact");
                                            if (contactId == next.getContactId()) {
                                                z4 = true;
                                                continue;
                                                if (z4) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    z4 = false;
                                    continue;
                                    if (z4) {
                                    }
                                }
                                ContactWrapper contactWrapper2 = (ContactWrapper) t2;
                                if (contactWrapper2 == null || !contactWrapper2.isAdded()) {
                                    if (contactWrapper2 == null) {
                                        s5 = (short) nl2.c(contactGroup.getHour());
                                    }
                                    if (!z5) {
                                        kd4.a((Object) next, "existedContact");
                                        if (next.isUseCall()) {
                                            if (z) {
                                                s4 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s5, s5, s4, 10000);
                                            DianaNotificationObj.AApplicationName aApplicationName = DianaNotificationObj.AApplicationName.PHONE_INCOMING_CALL;
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(aApplicationName.getAppName(), aApplicationName.getBundleCrc(), aApplicationName.getGroupId(), aApplicationName.getPackageName(), -1, "", aApplicationName.getNotificationType()));
                                            appNotificationFilter.setSender(next.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                                            appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (next.isUseSms()) {
                                            if (z) {
                                                s4 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(s5, s5, s4, 10000);
                                            DianaNotificationObj.AApplicationName aApplicationName2 = DianaNotificationObj.AApplicationName.MESSAGES;
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(aApplicationName2.getAppName(), aApplicationName2.getBundleCrc(), aApplicationName2.getGroupId(), aApplicationName2.getPackageName(), aApplicationName2.getIconResourceId(), aApplicationName2.getIconFwPath(), aApplicationName2.getNotificationType()));
                                            appNotificationFilter2.setSender(next.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                                            appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                    }
                                } else {
                                    s5 = s;
                                }
                                z5 = true;
                                if (!z5) {
                                }
                            }
                            s3 = s4;
                        } else if (baseFeatureModel instanceof AppFilter) {
                            AppFilter appFilter = (AppFilter) baseFeatureModel;
                            String type = appFilter.getType();
                            if (!(type == null || qf4.a(type))) {
                                Iterator<T> it2 = list2.iterator();
                                while (true) {
                                    if (!it2.hasNext()) {
                                        t = null;
                                        break;
                                    }
                                    t = it2.next();
                                    AppWrapper appWrapper = (AppWrapper) t;
                                    if (appWrapper.getInstalledApp() != null) {
                                        InstalledApp installedApp = appWrapper.getInstalledApp();
                                        if (installedApp != null) {
                                            if (kd4.a((Object) installedApp.getIdentifier(), (Object) appFilter.getType())) {
                                                z3 = true;
                                                continue;
                                                if (z3) {
                                                    break;
                                                }
                                            }
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    }
                                    z3 = false;
                                    continue;
                                    if (z3) {
                                    }
                                }
                                AppWrapper appWrapper2 = (AppWrapper) t;
                                if (appWrapper2 != null) {
                                    InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                                    if (installedApp2 != null) {
                                        Boolean isSelected = installedApp2.isSelected();
                                        kd4.a((Object) isSelected, "app.installedApp!!.isSelected");
                                        if (isSelected.booleanValue()) {
                                            s2 = s;
                                            z2 = true;
                                            if (!z2) {
                                                continue;
                                            } else {
                                                if (z) {
                                                    s3 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                                }
                                                CRC32 crc32 = new CRC32();
                                                String type2 = appFilter.getType();
                                                kd4.a((Object) type2, "item.type");
                                                Charset charset = bf4.a;
                                                if (type2 != null) {
                                                    byte[] bytes = type2.getBytes(charset);
                                                    kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                                                    crc32.update(bytes);
                                                    long value = crc32.getValue();
                                                    String name = appFilter.getName() == null ? "" : appFilter.getName();
                                                    NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(s2, s2, s3, 10000);
                                                    kd4.a((Object) name, "appName");
                                                    String type3 = appFilter.getType();
                                                    kd4.a((Object) type3, "item.type");
                                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(name, value, (byte) 2, type3, -1, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                                    appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
                                                    appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                                                    arrayList.add(appNotificationFilter3);
                                                } else {
                                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                                }
                                            }
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                                if (appWrapper2 == null) {
                                    s2 = (short) nl2.c(appFilter.getHour());
                                    z2 = true;
                                    if (!z2) {
                                    }
                                } else {
                                    s2 = -1;
                                    z2 = false;
                                    if (!z2) {
                                    }
                                }
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(List<AppWrapper> list, short s, boolean z) {
        short s2;
        ArrayList arrayList = new ArrayList();
        if (!z) {
            s2 = -1;
        } else {
            s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        }
        for (AppWrapper installedApp : list) {
            InstalledApp installedApp2 = installedApp.getInstalledApp();
            if (installedApp2 != null) {
                Boolean isSelected = installedApp2.isSelected();
                kd4.a((Object) isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    CRC32 crc32 = new CRC32();
                    String identifier = installedApp2.getIdentifier();
                    kd4.a((Object) identifier, "it.identifier");
                    Charset charset = bf4.a;
                    if (identifier != null) {
                        byte[] bytes = identifier.getBytes(charset);
                        kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        crc32.update(bytes);
                        long value = crc32.getValue();
                        String title = installedApp2.getTitle() == null ? "" : installedApp2.getTitle();
                        short s3 = s;
                        NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s3, s3, s2, 10000);
                        kd4.a((Object) title, "appName");
                        String title2 = installedApp2.getTitle();
                        kd4.a((Object) title2, "it.title");
                        AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(title, value, (byte) 2, title2, -1, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                        appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                        appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                        arrayList.add(appNotificationFilter);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
            }
            short s4 = s;
        }
        return arrayList;
    }
}
