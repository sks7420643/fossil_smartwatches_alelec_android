package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vo4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public vo4 f;
    @DexIgnore
    public vo4 g;

    @DexIgnore
    public vo4() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    @DexIgnore
    public final vo4 a(vo4 vo4) {
        vo4.g = this;
        vo4.f = this.f;
        this.f.g = vo4;
        this.f = vo4;
        return vo4;
    }

    @DexIgnore
    public final vo4 b() {
        vo4 vo4 = this.f;
        if (vo4 == this) {
            vo4 = null;
        }
        vo4 vo42 = this.g;
        vo42.f = this.f;
        this.f.g = vo42;
        this.f = null;
        this.g = null;
        return vo4;
    }

    @DexIgnore
    public final vo4 c() {
        this.d = true;
        return new vo4(this.a, this.b, this.c, true, false);
    }

    @DexIgnore
    public vo4(byte[] bArr, int i, int i2, boolean z, boolean z2) {
        this.a = bArr;
        this.b = i;
        this.c = i2;
        this.d = z;
        this.e = z2;
    }

    @DexIgnore
    public final vo4 a(int i) {
        vo4 vo4;
        if (i <= 0 || i > this.c - this.b) {
            throw new IllegalArgumentException();
        }
        if (i >= 1024) {
            vo4 = c();
        } else {
            vo4 = wo4.a();
            System.arraycopy(this.a, this.b, vo4.a, 0, i);
        }
        vo4.c = vo4.b + i;
        this.b += i;
        this.g.a(vo4);
        return vo4;
    }

    @DexIgnore
    public final void a() {
        vo4 vo4 = this.g;
        if (vo4 == this) {
            throw new IllegalStateException();
        } else if (vo4.e) {
            int i = this.c - this.b;
            if (i <= (8192 - vo4.c) + (vo4.d ? 0 : vo4.b)) {
                a(this.g, i);
                b();
                wo4.a(this);
            }
        }
    }

    @DexIgnore
    public final void a(vo4 vo4, int i) {
        if (vo4.e) {
            int i2 = vo4.c;
            if (i2 + i > 8192) {
                if (!vo4.d) {
                    int i3 = vo4.b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = vo4.a;
                        System.arraycopy(bArr, i3, bArr, 0, i2 - i3);
                        vo4.c -= vo4.b;
                        vo4.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            System.arraycopy(this.a, this.b, vo4.a, vo4.c, i);
            vo4.c += i;
            this.b += i;
            return;
        }
        throw new IllegalArgumentException();
    }
}
