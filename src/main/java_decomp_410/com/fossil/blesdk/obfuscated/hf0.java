package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.b;
import com.fossil.blesdk.obfuscated.ze0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hf0<A extends de0.b, L> {
    @DexIgnore
    public /* final */ ze0.a<L> a;

    @DexIgnore
    public hf0(ze0.a<L> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public ze0.a<L> a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(A a2, xn1<Boolean> xn1) throws RemoteException;
}
