package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ab3 extends zr2 implements za3, jt2, ks2 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<ab2> j;
    @DexIgnore
    public ya3 k;
    @DexIgnore
    public gt2 l;
    @DexIgnore
    public GoalTrackingOverviewFragment m;
    @DexIgnore
    public bu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ab3.p;
        }

        @DexIgnore
        public final ab3 b() {
            return new ab3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends bu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ab3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, ab3 ab3, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = ab3;
        }

        @DexIgnore
        public void a(int i) {
            ab3.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = ab3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "DashboardGoalTrackingFra\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ya3 a(ab3 ab3) {
        ya3 ya3 = ab3.k;
        if (ya3 != null) {
            return ya3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (isVisible() && this.j != null) {
            ab2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    RecyclerView.ViewHolder c = recyclerView.c(0);
                    if (c != null) {
                        View view = c.itemView;
                        if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            return;
                        }
                    }
                    recyclerView.j(0);
                    bu3 bu3 = this.n;
                    if (bu3 != null) {
                        bu3.a();
                    }
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final ab2 T0() {
        tr3<ab2> tr3 = this.j;
        if (tr3 != null) {
            return tr3.a();
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.D;
            kd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        bu3 bu3 = this.n;
        if (bu3 != null) {
            bu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (ab2) qa.a(layoutInflater, R.layout.fragment_dashboard_goal_tracking, viewGroup, false, O0()));
        tr3<ab2> tr3 = this.j;
        if (tr3 != null) {
            ab2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        ya3 ya3 = this.k;
        if (ya3 == null) {
            return;
        }
        if (ya3 != null) {
            ya3.i();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onDestroyView() {
        ya3 ya3 = this.k;
        if (ya3 != null) {
            ya3.i();
            super.onDestroyView();
            N0();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ya3 ya3 = this.k;
        if (ya3 != null) {
            ya3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        ya3 ya3 = this.k;
        if (ya3 != null) {
            ya3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.m = (GoalTrackingOverviewFragment) getChildFragmentManager().a("GoalTrackingOverviewFragment");
        if (this.m == null) {
            this.m = new GoalTrackingOverviewFragment();
        }
        it2 it2 = new it2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.m;
        if (goalTrackingOverviewFragment != null) {
            this.l = new gt2(it2, c, this, childFragmentManager, goalTrackingOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            ab2 T0 = T0();
            if (T0 != null) {
                RecyclerView recyclerView = T0.q;
                if (recyclerView != null) {
                    kd4.a((Object) recyclerView, "it");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    gt2 gt2 = this.l;
                    if (gt2 != null) {
                        recyclerView.setAdapter(gt2);
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            this.n = new b(recyclerView, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                            bu3 bu3 = this.n;
                            if (bu3 != null) {
                                recyclerView.a((RecyclerView.q) bu3);
                                recyclerView.setItemViewCacheSize(0);
                                l73 l73 = new l73(linearLayoutManager.M());
                                Drawable c2 = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_dashboard_line_1w);
                                if (c2 != null) {
                                    kd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                    l73.a(c2);
                                    recyclerView.a((RecyclerView.l) l73);
                                    ya3 ya3 = this.k;
                                    if (ya3 != null) {
                                        ya3.h();
                                    } else {
                                        kd4.d("mPresenter");
                                        throw null;
                                    }
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                    } else {
                        kd4.d("mDashboardGoalTrackingAdapter");
                        throw null;
                    }
                }
            }
            ab2 T02 = T0();
            if (T02 != null) {
                RecyclerView recyclerView2 = T02.q;
            }
            ab2 T03 = T0();
            if (T03 != null) {
                RecyclerView recyclerView3 = T03.q;
                if (recyclerView3 != null) {
                    kd4.a((Object) recyclerView3, "recyclerView");
                    RecyclerView.j itemAnimator = recyclerView3.getItemAnimator();
                    if (itemAnimator instanceof af) {
                        ((af) itemAnimator).setSupportsChangeAnimations(false);
                    }
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ic a2 = lc.a(activity).a(iu3.class);
                kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                iu3 iu3 = (iu3) a2;
                return;
            }
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(qd<GoalTrackingSummary> qdVar) {
        gt2 gt2 = this.l;
        if (gt2 != null) {
            gt2.c(qdVar);
        } else {
            kd4.d("mDashboardGoalTrackingAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ya3 ya3) {
        kd4.b(ya3, "presenter");
        this.k = ya3;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        kd4.b(date, "startWeekDate");
        kd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
