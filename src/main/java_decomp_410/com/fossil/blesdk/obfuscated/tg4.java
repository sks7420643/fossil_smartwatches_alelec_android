package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tg4 {
    @DexIgnore
    public static /* final */ boolean a;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r0.equals("on") != false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r0.equals("") != false) goto L_0x0033;
     */
    /*
    static {
        boolean z;
        String a2 = ek4.a("kotlinx.coroutines.scheduler");
        if (a2 != null) {
            int hashCode = a2.hashCode();
            if (hashCode != 0) {
                if (hashCode != 3551) {
                    if (hashCode == 109935 && a2.equals("off")) {
                        z = false;
                        a = z;
                    }
                }
            }
            throw new IllegalStateException(("System property 'kotlinx.coroutines.scheduler' has unrecognized value '" + a2 + '\'').toString());
        }
        z = true;
        a = z;
    }
    */

    @DexIgnore
    public static final ug4 a() {
        return a ? nk4.k : lg4.g;
    }

    @DexIgnore
    public static final CoroutineContext a(zg4 zg4, CoroutineContext coroutineContext) {
        kd4.b(zg4, "$this$newCoroutineContext");
        kd4.b(coroutineContext, "context");
        CoroutineContext plus = zg4.A().plus(coroutineContext);
        CoroutineContext plus2 = ch4.c() ? plus.plus(new xg4(ch4.b().incrementAndGet())) : plus;
        return (plus == nh4.a() || plus.get(zb4.b) != null) ? plus2 : plus2.plus(nh4.a());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0025, code lost:
        if (r4 != null) goto L_0x002a;
     */
    @DexIgnore
    public static final String a(CoroutineContext coroutineContext) {
        String str;
        kd4.b(coroutineContext, "$this$coroutineName");
        if (!ch4.c()) {
            return null;
        }
        xg4 xg4 = (xg4) coroutineContext.get(xg4.f);
        if (xg4 == null) {
            return null;
        }
        yg4 yg4 = (yg4) coroutineContext.get(yg4.f);
        if (yg4 != null) {
            str = yg4.C();
        }
        str = "coroutine";
        return str + '#' + xg4.C();
    }
}
