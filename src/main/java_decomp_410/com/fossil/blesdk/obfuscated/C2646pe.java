package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pe */
public interface C2646pe {
    @DexIgnore
    /* renamed from: a */
    void mo14671a(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, android.view.View view, float f, float f2, int i, boolean z);

    @DexIgnore
    /* renamed from: a */
    void mo14672a(android.view.View view);

    @DexIgnore
    /* renamed from: b */
    void mo14673b(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, android.view.View view, float f, float f2, int i, boolean z);

    @DexIgnore
    /* renamed from: b */
    void mo14674b(android.view.View view);
}
