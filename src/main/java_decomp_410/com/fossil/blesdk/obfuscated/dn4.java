package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn4 implements Interceptor.Chain {
    @DexIgnore
    public /* final */ List<Interceptor> a;
    @DexIgnore
    public /* final */ wm4 b;
    @DexIgnore
    public /* final */ zm4 c;
    @DexIgnore
    public /* final */ tm4 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ dm4 f;
    @DexIgnore
    public /* final */ jl4 g;
    @DexIgnore
    public /* final */ vl4 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public dn4(List<Interceptor> list, wm4 wm4, zm4 zm4, tm4 tm4, int i2, dm4 dm4, jl4 jl4, vl4 vl4, int i3, int i4, int i5) {
        this.a = list;
        this.d = tm4;
        this.b = wm4;
        this.c = zm4;
        this.e = i2;
        this.f = dm4;
        this.g = jl4;
        this.h = vl4;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    public int a() {
        return this.j;
    }

    @DexIgnore
    public int b() {
        return this.k;
    }

    @DexIgnore
    public nl4 c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.i;
    }

    @DexIgnore
    public jl4 e() {
        return this.g;
    }

    @DexIgnore
    public vl4 f() {
        return this.h;
    }

    @DexIgnore
    public zm4 g() {
        return this.c;
    }

    @DexIgnore
    public wm4 h() {
        return this.b;
    }

    @DexIgnore
    public dm4 n() {
        return this.f;
    }

    @DexIgnore
    public Response a(dm4 dm4) throws IOException {
        return a(dm4, this.b, this.c, this.d);
    }

    @DexIgnore
    public Response a(dm4 dm4, wm4 wm4, zm4 zm4, tm4 tm4) throws IOException {
        if (this.e < this.a.size()) {
            this.l++;
            if (this.c != null && !this.d.a(dm4.g())) {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                dn4 dn4 = new dn4(this.a, wm4, zm4, tm4, this.e + 1, dm4, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.a.get(this.e);
                Response intercept = interceptor.intercept(dn4);
                if (zm4 != null && this.e + 1 < this.a.size() && dn4.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.y() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }
}
