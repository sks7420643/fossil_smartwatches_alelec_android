package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.view.cardstackview.StackFrom;
import com.portfolio.platform.view.cardstackview.SwipeDirection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mt3 {
    @DexIgnore
    public int a; // = 3;
    @DexIgnore
    public float b; // = 0.75f;
    @DexIgnore
    public float c; // = 12.0f;
    @DexIgnore
    public float d; // = 0.02f;
    @DexIgnore
    public StackFrom e; // = StackFrom.Top;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public int i; // = 0;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public List<SwipeDirection> l; // = SwipeDirection.FREEDOM;
}
