package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uk<T> {
    @DexIgnore
    public static /* final */ String f; // = dj.a("ConstraintTracker");
    @DexIgnore
    public /* final */ zl a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<fk<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List e;

        @DexIgnore
        public a(List list) {
            this.e = list;
        }

        @DexIgnore
        public void run() {
            for (fk a : this.e) {
                a.a(uk.this.e);
            }
        }
    }

    @DexIgnore
    public uk(Context context, zl zlVar) {
        this.b = context.getApplicationContext();
        this.a = zlVar;
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(fk<T> fkVar) {
        synchronized (this.c) {
            if (this.d.add(fkVar)) {
                if (this.d.size() == 1) {
                    this.e = a();
                    dj.a().a(f, String.format("%s: initial state = %s", new Object[]{getClass().getSimpleName(), this.e}), new Throwable[0]);
                    b();
                }
                fkVar.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public void b(fk<T> fkVar) {
        synchronized (this.c) {
            if (this.d.remove(fkVar) && this.d.isEmpty()) {
                c();
            }
        }
    }

    @DexIgnore
    public abstract void c();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    @DexIgnore
    public void a(T t) {
        synchronized (this.c) {
            if (this.e != t) {
                if (this.e == null || !this.e.equals(t)) {
                    this.e = t;
                    this.a.a().execute(new a(new ArrayList(this.d)));
                }
            }
        }
    }
}
