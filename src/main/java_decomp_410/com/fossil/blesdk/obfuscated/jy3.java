package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.blesdk.obfuscated.iy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jy3 extends iy3 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public jy3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(gy3 gy3) {
        if (gy3.e != 0) {
            return true;
        }
        return "android.resource".equals(gy3.d.getScheme());
    }

    @DexIgnore
    public iy3.a a(gy3 gy3, int i) throws IOException {
        Resources a2 = oy3.a(this.a, gy3);
        return new iy3.a(a(a2, oy3.a(a2, gy3), gy3), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public static Bitmap a(Resources resources, int i, gy3 gy3) {
        BitmapFactory.Options b = iy3.b(gy3);
        if (iy3.a(b)) {
            BitmapFactory.decodeResource(resources, i, b);
            iy3.a(gy3.h, gy3.i, b, gy3);
        }
        return BitmapFactory.decodeResource(resources, i, b);
    }
}
