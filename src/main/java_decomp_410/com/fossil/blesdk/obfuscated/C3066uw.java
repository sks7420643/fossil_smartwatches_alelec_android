package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uw */
public final class C3066uw {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ char[] f10072a; // = "0123456789abcdef".toCharArray();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ char[] f10073b; // = new char[64];

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uw$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uw$a */
    public static /* synthetic */ class C3067a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f10074a; // = new int[android.graphics.Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            f10074a[android.graphics.Bitmap.Config.ALPHA_8.ordinal()] = 1;
            f10074a[android.graphics.Bitmap.Config.RGB_565.ordinal()] = 2;
            f10074a[android.graphics.Bitmap.Config.ARGB_4444.ordinal()] = 3;
            f10074a[android.graphics.Bitmap.Config.RGBA_F16.ordinal()] = 4;
            f10074a[android.graphics.Bitmap.Config.ARGB_8888.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14919a(int i, int i2) {
        return (i2 * 31) + i;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m14925a(byte[] bArr) {
        java.lang.String a;
        synchronized (f10073b) {
            a = m14926a(bArr, f10073b);
        }
        return a;
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m14932b(int i) {
        return i > 0 || i == Integer.MIN_VALUE;
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m14933b(int i, int i2) {
        return m14932b(i) && m14932b(i2);
    }

    @DexIgnore
    /* renamed from: c */
    public static boolean m14935c() {
        return !m14936d();
    }

    @DexIgnore
    /* renamed from: d */
    public static boolean m14936d() {
        return android.os.Looper.myLooper() == android.os.Looper.getMainLooper();
    }

    @DexIgnore
    /* renamed from: b */
    public static void m14931b() {
        if (!m14936d()) {
            throw new java.lang.IllegalArgumentException("You must call this method on the main thread");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m14926a(byte[] bArr, char[] cArr) {
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
            int i2 = i * 2;
            char[] cArr2 = f10072a;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & org.joda.time.DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new java.lang.String(cArr);
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m14934b(java.lang.Object obj, java.lang.Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    @android.annotation.TargetApi(19)
    /* renamed from: a */
    public static int m14922a(android.graphics.Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            if (android.os.Build.VERSION.SDK_INT >= 19) {
                try {
                    return bitmap.getAllocationByteCount();
                } catch (java.lang.NullPointerException unused) {
                }
            }
            return bitmap.getHeight() * bitmap.getRowBytes();
        }
        throw new java.lang.IllegalStateException("Cannot obtain size for recycled Bitmap: " + bitmap + "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig());
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14920a(int i, int i2, android.graphics.Bitmap.Config config) {
        return i * i2 * m14921a(config);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14921a(android.graphics.Bitmap.Config config) {
        if (config == null) {
            config = android.graphics.Bitmap.Config.ARGB_8888;
        }
        int i = com.fossil.blesdk.obfuscated.C3066uw.C3067a.f10074a[config.ordinal()];
        if (i == 1) {
            return 1;
        }
        if (i == 2 || i == 3) {
            return 2;
        }
        return i != 4 ? 4 : 8;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14929a() {
        if (!m14935c()) {
            throw new java.lang.IllegalArgumentException("You must call this method on a background thread");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> java.util.Queue<T> m14928a(int i) {
        return new java.util.ArrayDeque(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> java.util.List<T> m14927a(java.util.Collection<T> collection) {
        java.util.ArrayList arrayList = new java.util.ArrayList(collection.size());
        for (T next : collection) {
            if (next != null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m14930a(java.lang.Object obj, java.lang.Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        if (obj instanceof com.fossil.blesdk.obfuscated.C2763qr) {
            return ((com.fossil.blesdk.obfuscated.C2763qr) obj).mo15358a(obj2);
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14917a(float f) {
        return m14918a(f, 17);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14918a(float f, int i) {
        return m14919a(java.lang.Float.floatToIntBits(f), i);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14923a(java.lang.Object obj, int i) {
        return m14919a(obj == null ? 0 : obj.hashCode(), i);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14924a(boolean z, int i) {
        return m14919a(z ? 1 : 0, i);
    }
}
