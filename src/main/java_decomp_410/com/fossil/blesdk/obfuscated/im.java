package com.fossil.blesdk.obfuscated;

import bolts.ExecutorException;
import bolts.UnobservedTaskException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class im<TResult> {
    @DexIgnore
    public static /* final */ Executor i; // = fm.b();
    @DexIgnore
    public static volatile g j;
    @DexIgnore
    public static im<?> k; // = new im<>((Object) null);
    @DexIgnore
    public static im<Boolean> l; // = new im<>(true);
    @DexIgnore
    public static im<Boolean> m; // = new im<>(false);
    @DexIgnore
    public static im<?> n; // = new im<>(true);
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public TResult d;
    @DexIgnore
    public Exception e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public km g;
    @DexIgnore
    public List<hm<TResult, Void>> h; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements hm<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ jm a;
        @DexIgnore
        public /* final */ /* synthetic */ hm b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ gm d;

        @DexIgnore
        public a(im imVar, jm jmVar, hm hmVar, Executor executor, gm gmVar) {
            this.a = jmVar;
            this.b = hmVar;
            this.c = executor;
            this.d = gmVar;
        }

        @DexIgnore
        public Void then(im<TResult> imVar) {
            im.d(this.a, this.b, imVar, this.c, this.d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements hm<TResult, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ jm a;
        @DexIgnore
        public /* final */ /* synthetic */ hm b;
        @DexIgnore
        public /* final */ /* synthetic */ Executor c;
        @DexIgnore
        public /* final */ /* synthetic */ gm d;

        @DexIgnore
        public b(im imVar, jm jmVar, hm hmVar, Executor executor, gm gmVar) {
            this.a = jmVar;
            this.b = hmVar;
            this.c = executor;
            this.d = gmVar;
        }

        @DexIgnore
        public Void then(im<TResult> imVar) {
            im.c(this.a, this.b, imVar, this.c, this.d);
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements hm<TResult, im<TContinuationResult>> {
        @DexIgnore
        public /* final */ /* synthetic */ gm a;
        @DexIgnore
        public /* final */ /* synthetic */ hm b;

        @DexIgnore
        public c(im imVar, gm gmVar, hm hmVar) {
            this.a = gmVar;
            this.b = hmVar;
        }

        @DexIgnore
        public im<TContinuationResult> then(im<TResult> imVar) {
            gm gmVar = this.a;
            if (gmVar != null) {
                gmVar.a();
                throw null;
            } else if (imVar.e()) {
                return im.b(imVar.a());
            } else {
                if (imVar.c()) {
                    return im.h();
                }
                return imVar.a((hm<TResult, TContinuationResult>) this.b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ gm e;
        @DexIgnore
        public /* final */ /* synthetic */ jm f;
        @DexIgnore
        public /* final */ /* synthetic */ hm g;
        @DexIgnore
        public /* final */ /* synthetic */ im h;

        @DexIgnore
        public d(gm gmVar, jm jmVar, hm hmVar, im imVar) {
            this.e = gmVar;
            this.f = jmVar;
            this.g = hmVar;
            this.h = imVar;
        }

        @DexIgnore
        public void run() {
            gm gmVar = this.e;
            if (gmVar == null) {
                try {
                    this.f.a(this.g.then(this.h));
                } catch (CancellationException unused) {
                    this.f.b();
                } catch (Exception e2) {
                    this.f.a(e2);
                }
            } else {
                gmVar.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ gm e;
        @DexIgnore
        public /* final */ /* synthetic */ jm f;
        @DexIgnore
        public /* final */ /* synthetic */ hm g;
        @DexIgnore
        public /* final */ /* synthetic */ im h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements hm<TContinuationResult, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public Void then(im<TContinuationResult> imVar) {
                gm gmVar = e.this.e;
                if (gmVar == null) {
                    if (imVar.c()) {
                        e.this.f.b();
                    } else if (imVar.e()) {
                        e.this.f.a(imVar.a());
                    } else {
                        e.this.f.a(imVar.b());
                    }
                    return null;
                }
                gmVar.a();
                throw null;
            }
        }

        @DexIgnore
        public e(gm gmVar, jm jmVar, hm hmVar, im imVar) {
            this.e = gmVar;
            this.f = jmVar;
            this.g = hmVar;
            this.h = imVar;
        }

        @DexIgnore
        public void run() {
            gm gmVar = this.e;
            if (gmVar == null) {
                try {
                    im imVar = (im) this.g.then(this.h);
                    if (imVar == null) {
                        this.f.a(null);
                    } else {
                        imVar.a(new a());
                    }
                } catch (CancellationException unused) {
                    this.f.b();
                } catch (Exception e2) {
                    this.f.a(e2);
                }
            } else {
                gmVar.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends jm<TResult> {
        @DexIgnore
        public f(im imVar) {
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(im<?> imVar, UnobservedTaskException unobservedTaskException);
    }

    /*
    static {
        fm.a();
        bm.b();
    }
    */

    @DexIgnore
    public im() {
    }

    @DexIgnore
    public static <TResult> im<TResult> h() {
        return n;
    }

    @DexIgnore
    public static <TResult> im<TResult>.f i() {
        return new f(new im());
    }

    @DexIgnore
    public static g j() {
        return j;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.b;
        }
        return z;
    }

    @DexIgnore
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = a() != null;
        }
        return z;
    }

    @DexIgnore
    public final void f() {
        synchronized (this.a) {
            for (hm then : this.h) {
                try {
                    then.then(this);
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeException(e3);
                }
            }
            this.h = null;
        }
    }

    @DexIgnore
    public boolean g() {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.c = true;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public Exception a() {
        Exception exc;
        synchronized (this.a) {
            if (this.e != null) {
                this.f = true;
                if (this.g != null) {
                    this.g.a();
                    this.g = null;
                }
            }
            exc = this.e;
        }
        return exc;
    }

    @DexIgnore
    public TResult b() {
        TResult tresult;
        synchronized (this.a) {
            tresult = this.d;
        }
        return tresult;
    }

    @DexIgnore
    public im(TResult tresult) {
        a(tresult);
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void d(jm<TContinuationResult> jmVar, hm<TResult, TContinuationResult> hmVar, im<TResult> imVar, Executor executor, gm gmVar) {
        try {
            executor.execute(new d(gmVar, jmVar, hmVar, imVar));
        } catch (Exception e2) {
            jmVar.a((Exception) new ExecutorException(e2));
        }
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> c(hm<TResult, TContinuationResult> hmVar, Executor executor, gm gmVar) {
        return a(new c(this, gmVar, hmVar), executor);
    }

    @DexIgnore
    public static <TResult> im<TResult> b(TResult tresult) {
        if (tresult == null) {
            return k;
        }
        if (tresult instanceof Boolean) {
            return ((Boolean) tresult).booleanValue() ? l : m;
        }
        jm jmVar = new jm();
        jmVar.a(tresult);
        return jmVar.a();
    }

    @DexIgnore
    public static <TContinuationResult, TResult> void c(jm<TContinuationResult> jmVar, hm<TResult, im<TContinuationResult>> hmVar, im<TResult> imVar, Executor executor, gm gmVar) {
        try {
            executor.execute(new e(gmVar, jmVar, hmVar, imVar));
        } catch (Exception e2) {
            jmVar.a((Exception) new ExecutorException(e2));
        }
    }

    @DexIgnore
    public im(boolean z) {
        if (z) {
            g();
        } else {
            a((Object) null);
        }
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> a(hm<TResult, TContinuationResult> hmVar, Executor executor, gm gmVar) {
        boolean d2;
        jm jmVar = new jm();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new a(this, jmVar, hmVar, executor, gmVar));
            }
        }
        if (d2) {
            d(jmVar, hmVar, this, executor, gmVar);
        }
        return jmVar.a();
    }

    @DexIgnore
    public static <TResult> im<TResult> b(Exception exc) {
        jm jmVar = new jm();
        jmVar.a(exc);
        return jmVar.a();
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> b(hm<TResult, im<TContinuationResult>> hmVar, Executor executor, gm gmVar) {
        boolean d2;
        jm jmVar = new jm();
        synchronized (this.a) {
            d2 = d();
            if (!d2) {
                this.h.add(new b(this, jmVar, hmVar, executor, gmVar));
            }
        }
        if (d2) {
            c(jmVar, hmVar, this, executor, gmVar);
        }
        return jmVar.a();
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> a(hm<TResult, TContinuationResult> hmVar) {
        return a(hmVar, i, (gm) null);
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> a(hm<TResult, im<TContinuationResult>> hmVar, Executor executor) {
        return b(hmVar, executor, (gm) null);
    }

    @DexIgnore
    public boolean a(TResult tresult) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.d = tresult;
            this.a.notifyAll();
            f();
            return true;
        }
    }

    @DexIgnore
    public <TContinuationResult> im<TContinuationResult> b(hm<TResult, TContinuationResult> hmVar) {
        return c(hmVar, i, (gm) null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        return true;
     */
    @DexIgnore
    public boolean a(Exception exc) {
        synchronized (this.a) {
            if (this.b) {
                return false;
            }
            this.b = true;
            this.e = exc;
            this.f = false;
            this.a.notifyAll();
            f();
            if (!this.f && j() != null) {
                this.g = new km(this);
            }
        }
    }
}
