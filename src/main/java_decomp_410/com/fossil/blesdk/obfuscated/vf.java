package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.pf;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vf<T> extends LiveData<T> {
    @DexIgnore
    public /* final */ RoomDatabase k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Callable<T> m;
    @DexIgnore
    public /* final */ of n;
    @DexIgnore
    public /* final */ pf.c o;
    @DexIgnore
    public /* final */ AtomicBoolean p; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean q; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable s; // = new a();
    @DexIgnore
    public /* final */ Runnable t; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            if (vf.this.r.compareAndSet(false, true)) {
                vf.this.k.getInvalidationTracker().b(vf.this.o);
            }
            do {
                if (vf.this.q.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (vf.this.p.compareAndSet(true, false)) {
                        try {
                            obj = vf.this.m.call();
                            z = true;
                        } catch (Exception e2) {
                            throw new RuntimeException("Exception while computing database live data.", e2);
                        } catch (Throwable th) {
                            vf.this.q.set(false);
                            throw th;
                        }
                    }
                    if (z) {
                        vf.this.a(obj);
                    }
                    vf.this.q.set(false);
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (vf.this.p.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean c = vf.this.c();
            if (vf.this.p.compareAndSet(false, true) && c) {
                vf.this.f().execute(vf.this.s);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends pf.c {
        @DexIgnore
        public c(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            h3.c().b(vf.this.t);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public vf(RoomDatabase roomDatabase, of ofVar, boolean z, Callable<T> callable, String[] strArr) {
        this.k = roomDatabase;
        this.l = z;
        this.m = callable;
        this.n = ofVar;
        this.o = new c(strArr);
    }

    @DexIgnore
    public void d() {
        super.d();
        this.n.a(this);
        f().execute(this.s);
    }

    @DexIgnore
    public void e() {
        super.e();
        this.n.b(this);
    }

    @DexIgnore
    public Executor f() {
        if (this.l) {
            return this.k.getTransactionExecutor();
        }
        return this.k.getQueryExecutor();
    }
}
