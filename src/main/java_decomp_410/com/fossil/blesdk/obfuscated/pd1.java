package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pd1 extends s31 {
    @DexIgnore
    public /* final */ /* synthetic */ xn1 e;

    @DexIgnore
    public pd1(oc1 oc1, xn1 xn1) {
        this.e = xn1;
    }

    @DexIgnore
    public final void a(o31 o31) throws RemoteException {
        Status G = o31.G();
        if (G == null) {
            this.e.b((Exception) new ApiException(new Status(8, "Got null status from location service")));
        } else if (G.I() == 0) {
            this.e.a(true);
        } else {
            this.e.b((Exception) hj0.a(G));
        }
    }
}
