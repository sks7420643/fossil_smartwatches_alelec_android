package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface t13 extends bs2<s13> {
    @DexIgnore
    void a(f13 f13);

    @DexIgnore
    void a(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    void a(String str, String str2, String str3, boolean z);

    @DexIgnore
    void b(String str, String str2, String str3, boolean z);

    @DexIgnore
    void c();

    @DexIgnore
    void c(String str);

    @DexIgnore
    void f(int i);

    @DexIgnore
    void f(boolean z);

    @DexIgnore
    void g(boolean z);

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void p();

    @DexIgnore
    void p(String str);

    @DexIgnore
    void q();

    @DexIgnore
    void r(String str);
}
