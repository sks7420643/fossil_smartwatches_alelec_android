package com.fossil.blesdk.obfuscated;

import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qi3 extends v52<pi3> {
    @DexIgnore
    void G0();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void b(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration);

    @DexIgnore
    void d();

    @DexIgnore
    void e();
}
