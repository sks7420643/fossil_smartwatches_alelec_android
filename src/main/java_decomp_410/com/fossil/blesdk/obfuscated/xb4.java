package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xb4 implements CoroutineContext.a {
    @DexIgnore
    public /* final */ CoroutineContext.b<?> key;

    @DexIgnore
    public xb4(CoroutineContext.b<?> bVar) {
        kd4.b(bVar, "key");
        this.key = bVar;
    }

    @DexIgnore
    public <R> R fold(R r, yc4<? super R, ? super CoroutineContext.a, ? extends R> yc4) {
        kd4.b(yc4, "operation");
        return CoroutineContext.a.C0169a.a(this, r, yc4);
    }

    @DexIgnore
    public <E extends CoroutineContext.a> E get(CoroutineContext.b<E> bVar) {
        kd4.b(bVar, "key");
        return CoroutineContext.a.C0169a.a((CoroutineContext.a) this, bVar);
    }

    @DexIgnore
    public CoroutineContext.b<?> getKey() {
        return this.key;
    }

    @DexIgnore
    public CoroutineContext minusKey(CoroutineContext.b<?> bVar) {
        kd4.b(bVar, "key");
        return CoroutineContext.a.C0169a.b(this, bVar);
    }

    @DexIgnore
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        return CoroutineContext.a.C0169a.a((CoroutineContext.a) this, coroutineContext);
    }
}
