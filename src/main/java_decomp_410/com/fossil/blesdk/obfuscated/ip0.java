package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ip0 implements Parcelable.Creator<fp0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        Bundle bundle = null;
        int[] iArr = null;
        float[] fArr = null;
        byte[] bArr = null;
        int i = 0;
        boolean z = false;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel, a);
                    break;
                case 2:
                    z = SafeParcelReader.i(parcel, a);
                    break;
                case 3:
                    f = SafeParcelReader.n(parcel, a);
                    break;
                case 4:
                    str = SafeParcelReader.f(parcel, a);
                    break;
                case 5:
                    bundle = SafeParcelReader.a(parcel, a);
                    break;
                case 6:
                    iArr = SafeParcelReader.e(parcel, a);
                    break;
                case 7:
                    fArr = SafeParcelReader.d(parcel, a);
                    break;
                case 8:
                    bArr = SafeParcelReader.b(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new fp0(i, z, f, str, bundle, iArr, fArr, bArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new fp0[i];
    }
}
