package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.portfolio.platform.enums.ServiceStatus;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pp2 extends Service {
    @DexIgnore
    public static rp2 g;
    @DexIgnore
    public int e;
    @DexIgnore
    public ServiceStatus f; // = ServiceStatus.NOT_START;

    @DexIgnore
    public static void a(rp2 rp2) {
        g = rp2;
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public int c() {
        return this.e;
    }

    @DexIgnore
    public void d() {
        if (g != null) {
            ServiceStatus serviceStatus = this.f;
            ServiceStatus serviceStatus2 = ServiceStatus.RUNNING;
            if (serviceStatus != serviceStatus2) {
                this.f = serviceStatus2;
                g.a(this.e, serviceStatus2);
                g.a(this);
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return null;
    }

    @DexIgnore
    public void a() {
        if (g != null) {
            ServiceStatus serviceStatus = this.f;
            ServiceStatus serviceStatus2 = ServiceStatus.FINISHED;
            if (serviceStatus != serviceStatus2) {
                this.f = serviceStatus2;
                g.a(this.e, serviceStatus2);
                g.b(this);
            }
        }
    }
}
