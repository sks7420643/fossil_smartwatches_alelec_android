package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.to;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uo {
    @DexIgnore
    public static /* final */ to.a<?> b; // = new a();
    @DexIgnore
    public /* final */ Map<Class<?>, to.a<?>> a; // = new HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements to.a<Object> {
        @DexIgnore
        public to<Object> a(Object obj) {
            return new b(obj);
        }

        @DexIgnore
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements to<Object> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public Object b() {
            return this.a;
        }
    }

    @DexIgnore
    public synchronized void a(to.a<?> aVar) {
        this.a.put(aVar.getDataClass(), aVar);
    }

    @DexIgnore
    public synchronized <T> to<T> a(T t) {
        to.a<?> aVar;
        tw.a(t);
        aVar = this.a.get(t.getClass());
        if (aVar == null) {
            Iterator<to.a<?>> it = this.a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                to.a<?> next = it.next();
                if (next.getDataClass().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = b;
        }
        return aVar.a(t);
    }
}
