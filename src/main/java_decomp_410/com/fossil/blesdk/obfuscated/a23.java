package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a23 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public ArrayList<f13> a;
    @DexIgnore
    public d b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public View e;
        @DexIgnore
        public TextView f;
        @DexIgnore
        public /* final */ /* synthetic */ a23 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(a23 a23, View view) {
            super(view);
            kd4.b(view, "view");
            this.g = a23;
            View findViewById = view.findViewById(R.id.btn_add);
            kd4.a((Object) findViewById, "view.findViewById(R.id.btn_add)");
            this.e = findViewById;
            View findViewById2 = view.findViewById(R.id.tv_create_new_preset);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.tv_create_new_preset)");
            this.f = (TextView) findViewById2;
            this.e.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(boolean z) {
            if (z) {
                this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_grey));
                this.e.setClickable(false);
                this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_MaximumPresets_Actions_Text__YouveSavedMaximumNumberOfPresets));
                return;
            }
            this.e.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.bg_button_black));
            this.e.setClickable(true);
            this.f.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwipedCreate_Text__CreateANewPreset));
        }

        @DexIgnore
        public void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if (view != null && view.getId() == R.id.btn_add) {
                this.g.b().z0();
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2);

        @DexIgnore
        void b(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2, String str, int i);

        @DexIgnore
        void b(boolean z, String str, String str2, String str3);

        @DexIgnore
        void c(String str, String str2);

        @DexIgnore
        void y0();

        @DexIgnore
        void z0();
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public a23(ArrayList<f13> arrayList, d dVar) {
        kd4.b(arrayList, "mData");
        kd4.b(dVar, "mListener");
        this.a = arrayList;
        this.b = dVar;
        setHasStableIds(true);
    }

    @DexIgnore
    public final d b() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) this.a.get(i).hashCode();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == this.a.size() - 1) {
            return 0;
        }
        return i == 0 ? 2 : 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "onBindViewHolder pos=" + i);
        boolean z = true;
        if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            f13 f13 = this.a.get(i);
            kd4.a((Object) f13, "mData[position]");
            f13 f132 = f13;
            if (i != 0) {
                z = false;
            }
            bVar.a(f132, z);
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            if (this.a.size() <= 10) {
                z = false;
            }
            cVar.a(z);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        if (i == 1 || i == 2) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_diana_customize_preset_detail, viewGroup, false);
            kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026et_detail, parent, false)");
            return new b(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_customize_preset_add, viewGroup, false);
        kd4.a((Object) inflate2, "LayoutInflater.from(pare\u2026reset_add, parent, false)");
        return new c(this, inflate2);
    }

    @DexIgnore
    public final void a(List<f13> list) {
        kd4.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeAdapter", "setData - data=" + list);
        this.a.clear();
        this.a.addAll(list);
        this.a.add(new f13("", "", new ArrayList(), new ArrayList(), false, (WatchFaceWrapper) null, 32, (fd4) null));
        notifyDataSetChanged();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public TextView e;
        @DexIgnore
        public CustomizeWidget f;
        @DexIgnore
        public CustomizeWidget g;
        @DexIgnore
        public CustomizeWidget h;
        @DexIgnore
        public CustomizeWidget i;
        @DexIgnore
        public View j;
        @DexIgnore
        public ImageView k;
        @DexIgnore
        public CustomizeWidget l;
        @DexIgnore
        public CustomizeWidget m;
        @DexIgnore
        public CustomizeWidget n;
        @DexIgnore
        public ImageButton o;
        @DexIgnore
        public TextView p;
        @DexIgnore
        public View q;
        @DexIgnore
        public TextView r;
        @DexIgnore
        public View s;
        @DexIgnore
        public View t;
        @DexIgnore
        public View u;
        @DexIgnore
        public View v;
        @DexIgnore
        public f13 w;
        @DexIgnore
        public /* final */ /* synthetic */ a23 x;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(a23 a23, View view) {
            super(view);
            kd4.b(view, "view");
            this.x = a23;
            View findViewById = view.findViewById(R.id.tv_preset_name);
            kd4.a((Object) findViewById, "view.findViewById(R.id.tv_preset_name)");
            this.e = (TextView) findViewById;
            this.e.setOnClickListener(this);
            kd4.a((Object) view.findViewById(R.id.cl_complications), "view.findViewById(R.id.cl_complications)");
            kd4.a((Object) view.findViewById(R.id.cl_preset_buttons), "view.findViewById(R.id.cl_preset_buttons)");
            View findViewById2 = view.findViewById(R.id.iv_watch_hands_background);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.iv_watch_hands_background)");
            this.j = findViewById2;
            View findViewById3 = view.findViewById(R.id.iv_watch_theme_background);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.iv_watch_theme_background)");
            this.k = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(R.id.wc_top);
            kd4.a((Object) findViewById4, "view.findViewById(R.id.wc_top)");
            this.f = (CustomizeWidget) findViewById4;
            View findViewById5 = view.findViewById(R.id.wc_bottom);
            kd4.a((Object) findViewById5, "view.findViewById(R.id.wc_bottom)");
            this.g = (CustomizeWidget) findViewById5;
            View findViewById6 = view.findViewById(R.id.wc_start);
            kd4.a((Object) findViewById6, "view.findViewById(R.id.wc_start)");
            this.h = (CustomizeWidget) findViewById6;
            View findViewById7 = view.findViewById(R.id.wc_end);
            kd4.a((Object) findViewById7, "view.findViewById(R.id.wc_end)");
            this.i = (CustomizeWidget) findViewById7;
            View findViewById8 = view.findViewById(R.id.ib_edit_themes);
            kd4.a((Object) findViewById8, "view.findViewById(R.id.ib_edit_themes)");
            this.o = (ImageButton) findViewById8;
            View findViewById9 = view.findViewById(R.id.v_underline);
            kd4.a((Object) findViewById9, "view.findViewById(R.id.v_underline)");
            this.v = findViewById9;
            View findViewById10 = view.findViewById(R.id.wa_top);
            kd4.a((Object) findViewById10, "view.findViewById(R.id.wa_top)");
            this.l = (CustomizeWidget) findViewById10;
            View findViewById11 = view.findViewById(R.id.wa_middle);
            kd4.a((Object) findViewById11, "view.findViewById(R.id.wa_middle)");
            this.m = (CustomizeWidget) findViewById11;
            View findViewById12 = view.findViewById(R.id.wa_bottom);
            kd4.a((Object) findViewById12, "view.findViewById(R.id.wa_bottom)");
            this.n = (CustomizeWidget) findViewById12;
            View findViewById13 = view.findViewById(R.id.line_bottom);
            kd4.a((Object) findViewById13, "view.findViewById(R.id.line_bottom)");
            this.u = findViewById13;
            View findViewById14 = view.findViewById(R.id.line_center);
            kd4.a((Object) findViewById14, "view.findViewById(R.id.line_center)");
            this.t = findViewById14;
            View findViewById15 = view.findViewById(R.id.line_top);
            kd4.a((Object) findViewById15, "view.findViewById(R.id.line_top)");
            this.s = findViewById15;
            View findViewById16 = view.findViewById(R.id.layout_right);
            kd4.a((Object) findViewById16, "view.findViewById(R.id.layout_right)");
            this.q = findViewById16;
            View findViewById17 = view.findViewById(R.id.tv_right);
            kd4.a((Object) findViewById17, "view.findViewById(R.id.tv_right)");
            this.r = (TextView) findViewById17;
            View findViewById18 = view.findViewById(R.id.tv_left);
            kd4.a((Object) findViewById18, "view.findViewById(R.id.tv_left)");
            this.p = (TextView) findViewById18;
            this.q.setOnClickListener(this);
            this.p.setOnClickListener(this);
            this.f.setOnClickListener(this);
            this.g.setOnClickListener(this);
            this.h.setOnClickListener(this);
            this.i.setOnClickListener(this);
            this.o.setOnClickListener(this);
            this.l.setOnClickListener(this);
            this.m.setOnClickListener(this);
            this.n.setOnClickListener(this);
        }

        @DexIgnore
        public final List<f8<View, String>> a() {
            f8[] f8VarArr = new f8[7];
            TextView textView = this.e;
            f8VarArr[0] = new f8(textView, textView.getTransitionName());
            View view = this.j;
            f8VarArr[1] = new f8(view, view.getTransitionName());
            ImageView imageView = this.k;
            if (imageView != null) {
                f8VarArr[2] = new f8(imageView, imageView.getTransitionName());
                View view2 = this.v;
                f8VarArr[3] = new f8(view2, view2.getTransitionName());
                View view3 = this.u;
                f8VarArr[4] = new f8(view3, view3.getTransitionName());
                View view4 = this.t;
                f8VarArr[5] = new f8(view4, view4.getTransitionName());
                View view5 = this.s;
                f8VarArr[6] = new f8(view5, view5.getTransitionName());
                return cb4.c(f8VarArr);
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }

        @DexIgnore
        public final List<f8<CustomizeWidget, String>> b() {
            CustomizeWidget customizeWidget = this.f;
            CustomizeWidget customizeWidget2 = this.g;
            CustomizeWidget customizeWidget3 = this.i;
            CustomizeWidget customizeWidget4 = this.h;
            CustomizeWidget customizeWidget5 = this.l;
            CustomizeWidget customizeWidget6 = this.m;
            CustomizeWidget customizeWidget7 = this.n;
            return cb4.c(new f8(customizeWidget, customizeWidget.getTransitionName()), new f8(customizeWidget2, customizeWidget2.getTransitionName()), new f8(customizeWidget3, customizeWidget3.getTransitionName()), new f8(customizeWidget4, customizeWidget4.getTransitionName()), new f8(customizeWidget5, customizeWidget5.getTransitionName()), new f8(customizeWidget6, customizeWidget6.getTransitionName()), new f8(customizeWidget7, customizeWidget7.getTransitionName()));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0104, code lost:
            if (r0 != null) goto L_0x0108;
         */
        @DexIgnore
        public void onClick(View view) {
            String str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "onClick - adapterPosition=" + getAdapterPosition());
            if ((getAdapterPosition() != -1 || this.w == null) && view != null) {
                switch (view.getId()) {
                    case R.id.ib_edit_themes /*2131362344*/:
                        this.x.b().b(this.w, a(), b());
                        return;
                    case R.id.layout_right /*2131362478*/:
                        this.x.b().y0();
                        return;
                    case R.id.tv_left /*2131362897*/:
                        if (this.x.a.size() > 2) {
                            Object obj = this.x.a.get(1);
                            kd4.a(obj, "mData[1]");
                            f13 f13 = (f13) obj;
                            d b = this.x.b();
                            f13 f132 = this.w;
                            if (f132 != null) {
                                boolean b2 = f132.b();
                                f13 f133 = this.w;
                                if (f133 != null) {
                                    b.b(b2, f133.d(), f13.d(), f13.c());
                                    return;
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            return;
                        }
                    case R.id.tv_preset_name /*2131362920*/:
                        d b3 = this.x.b();
                        f13 f134 = this.w;
                        String str2 = "";
                        if (f134 != null) {
                            str = f134.d();
                            break;
                        }
                        str = str2;
                        f13 f135 = this.w;
                        if (f135 != null) {
                            String c = f135.c();
                            if (c != null) {
                                str2 = c;
                            }
                        }
                        b3.c(str, str2);
                        return;
                    case R.id.wa_bottom /*2131363027*/:
                        this.x.b().a(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wa_middle /*2131363028*/:
                        this.x.b().a(this.w, a(), b(), "middle", getAdapterPosition());
                        return;
                    case R.id.wa_top /*2131363029*/:
                        this.x.b().a(this.w, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    case R.id.wc_bottom /*2131363030*/:
                        this.x.b().b(this.w, a(), b(), "bottom", getAdapterPosition());
                        return;
                    case R.id.wc_end /*2131363034*/:
                        this.x.b().b(this.w, a(), b(), "right", getAdapterPosition());
                        return;
                    case R.id.wc_start /*2131363037*/:
                        this.x.b().b(this.w, a(), b(), ViewHierarchy.DIMENSION_LEFT_KEY, getAdapterPosition());
                        return;
                    case R.id.wc_top /*2131363038*/:
                        this.x.b().b(this.w, a(), b(), ViewHierarchy.DIMENSION_TOP_KEY, getAdapterPosition());
                        return;
                    default:
                        return;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0146  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x019c  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01f1  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0246  */
        /* JADX WARNING: Removed duplicated region for block: B:83:0x0290  */
        public final void a(f13 f13, boolean z) {
            Iterator it;
            WatchFaceWrapper f2;
            WatchFaceWrapper f3;
            WatchFaceWrapper f4;
            WatchFaceWrapper f5;
            kd4.b(f13, "preset");
            if (z) {
                this.q.setSelected(true);
                this.r.setText(PortfolioApp.W.c().getString(R.string.Customization_Delete_PresetDeleted_Label__Applied));
                this.r.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_white_tint, 0, 0, 0);
                this.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.white));
                this.q.setClickable(false);
                if (this.x.a.size() == 2) {
                    this.p.setVisibility(4);
                } else {
                    this.p.setVisibility(0);
                }
            } else {
                this.q.setSelected(false);
                this.r.setText(PortfolioApp.W.c().getString(R.string.Customization_CreateNew_PresetSwiped_CTA__Apply));
                this.r.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                this.r.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryColor));
                this.q.setClickable(true);
                this.p.setVisibility(0);
            }
            this.w = f13;
            this.e.setText(f13.d());
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(f13.a());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(f13.e());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeAdapter", "build with complications=" + arrayList + " watchapps=" + arrayList2 + " mWatchFaceWrapper=" + f13.f());
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                h13 h13 = (h13) it2.next();
                String f6 = h13.f();
                if (f6 != null) {
                    switch (f6.hashCode()) {
                        case -1383228885:
                            if (!f6.equals("bottom")) {
                                break;
                            } else {
                                this.g.b(h13.d());
                                this.g.setBottomContent(h13.g());
                                WatchFaceWrapper f7 = f13.f();
                                if (f7 != null) {
                                    Drawable topComplication = f7.getTopComplication();
                                    if (topComplication != null) {
                                        this.g.setBackgroundDrawableCus(topComplication);
                                        f2 = f13.f();
                                        if (f2 != null) {
                                            WatchFaceWrapper.MetaData bottomMetaData = f2.getBottomMetaData();
                                            if (bottomMetaData != null) {
                                                this.g.a((Integer) null, (Integer) null, bottomMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.g.h();
                                                break;
                                            }
                                        }
                                        this.g.g();
                                        this.g.h();
                                    }
                                }
                                this.g.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f2 = f13.f();
                                if (f2 != null) {
                                }
                                this.g.g();
                                this.g.h();
                            }
                        case 115029:
                            if (!f6.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                break;
                            } else {
                                this.f.b(h13.d());
                                this.f.setBottomContent(h13.g());
                                WatchFaceWrapper f8 = f13.f();
                                if (f8 != null) {
                                    Drawable topComplication2 = f8.getTopComplication();
                                    if (topComplication2 != null) {
                                        this.f.setBackgroundDrawableCus(topComplication2);
                                        f3 = f13.f();
                                        if (f3 != null) {
                                            WatchFaceWrapper.MetaData topMetaData = f3.getTopMetaData();
                                            if (topMetaData != null) {
                                                this.f.a((Integer) null, (Integer) null, topMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.f.h();
                                                break;
                                            }
                                        }
                                        this.f.g();
                                        this.f.h();
                                    }
                                }
                                this.f.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f3 = f13.f();
                                if (f3 != null) {
                                }
                                this.f.g();
                                this.f.h();
                            }
                        case 3317767:
                            if (!f6.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                break;
                            } else {
                                this.h.b(h13.d());
                                this.h.setBottomContent(h13.g());
                                WatchFaceWrapper f9 = f13.f();
                                if (f9 != null) {
                                    Drawable leftComplication = f9.getLeftComplication();
                                    if (leftComplication != null) {
                                        this.h.setBackgroundDrawableCus(leftComplication);
                                        f4 = f13.f();
                                        if (f4 != null) {
                                            WatchFaceWrapper.MetaData leftMetaData = f4.getLeftMetaData();
                                            if (leftMetaData != null) {
                                                this.h.a((Integer) null, (Integer) null, leftMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.h.h();
                                                break;
                                            }
                                        }
                                        this.h.g();
                                        this.h.h();
                                    }
                                }
                                this.h.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f4 = f13.f();
                                if (f4 != null) {
                                }
                                this.h.g();
                                this.h.h();
                            }
                        case 108511772:
                            if (!f6.equals("right")) {
                                break;
                            } else {
                                this.i.b(h13.d());
                                this.i.setBottomContent(h13.g());
                                WatchFaceWrapper f10 = f13.f();
                                if (f10 != null) {
                                    Drawable rightComplication = f10.getRightComplication();
                                    if (rightComplication != null) {
                                        this.i.setBackgroundDrawableCus(rightComplication);
                                        f5 = f13.f();
                                        if (f5 != null) {
                                            WatchFaceWrapper.MetaData rightMetaData = f5.getRightMetaData();
                                            if (rightMetaData != null) {
                                                this.i.a((Integer) null, (Integer) null, rightMetaData.getUnselectedForegroundColor(), (Integer) null);
                                                this.i.h();
                                                break;
                                            }
                                        }
                                        this.i.g();
                                        this.i.h();
                                    }
                                }
                                this.i.setBackgroundRes(Integer.valueOf(R.drawable.bg_widget_control_transparent));
                                f5 = f13.f();
                                if (f5 != null) {
                                }
                                this.i.g();
                                this.i.h();
                            }
                    }
                }
            }
            WatchFaceWrapper f11 = f13.f();
            if (f11 != null) {
                Drawable background = f11.getBackground();
                if (background != null) {
                    this.k.setImageDrawable(background);
                    it = arrayList2.iterator();
                    while (it.hasNext()) {
                        h13 h132 = (h13) it.next();
                        String f12 = h132.f();
                        if (f12 != null) {
                            int hashCode = f12.hashCode();
                            if (hashCode != -1383228885) {
                                if (hashCode != -1074341483) {
                                    if (hashCode == 115029 && f12.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        this.l.d(h132.d());
                                        this.l.h();
                                    }
                                } else if (f12.equals("middle")) {
                                    this.m.d(h132.d());
                                    this.m.h();
                                }
                            } else if (f12.equals("bottom")) {
                                this.n.d(h132.d());
                                this.n.h();
                            }
                        }
                    }
                }
            }
            this.k.setImageDrawable(PortfolioApp.W.c().getDrawable(R.drawable.nothing_bg));
            it = arrayList2.iterator();
            while (it.hasNext()) {
            }
        }
    }
}
