package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qg extends ImageView {
    @DexIgnore
    public Animation.AnimationListener e;
    @DexIgnore
    public int f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OvalShape {
        @DexIgnore
        public RadialGradient e;
        @DexIgnore
        public Paint f; // = new Paint();

        @DexIgnore
        public a(int i) {
            qg.this.f = i;
            a((int) rect().width());
        }

        @DexIgnore
        public final void a(int i) {
            float f2 = (float) (i / 2);
            this.e = new RadialGradient(f2, f2, (float) qg.this.f, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.f.setShader(this.e);
        }

        @DexIgnore
        public void draw(Canvas canvas, Paint paint) {
            int width = qg.this.getWidth() / 2;
            float f2 = (float) width;
            float height = (float) (qg.this.getHeight() / 2);
            canvas.drawCircle(f2, height, f2, this.f);
            canvas.drawCircle(f2, height, (float) (width - qg.this.f), paint);
        }

        @DexIgnore
        public void onResize(float f2, float f3) {
            super.onResize(f2, f3);
            a((int) f2);
        }
    }

    @DexIgnore
    public qg(Context context, int i) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f2 = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f2);
        int i3 = (int) (LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES * f2);
        this.f = (int) (3.5f * f2);
        if (a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            f9.b((View) this, f2 * 4.0f);
        } else {
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(new a(this.f));
            setLayerType(1, shapeDrawable2.getPaint());
            shapeDrawable2.getPaint().setShadowLayer((float) this.f, (float) i3, (float) i2, 503316480);
            int i4 = this.f;
            setPadding(i4, i4, i4, i4);
            shapeDrawable = shapeDrawable2;
        }
        shapeDrawable.getPaint().setColor(i);
        f9.a((View) this, (Drawable) shapeDrawable);
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public void onAnimationEnd() {
        super.onAnimationEnd();
        Animation.AnimationListener animationListener = this.e;
        if (animationListener != null) {
            animationListener.onAnimationEnd(getAnimation());
        }
    }

    @DexIgnore
    public void onAnimationStart() {
        super.onAnimationStart();
        Animation.AnimationListener animationListener = this.e;
        if (animationListener != null) {
            animationListener.onAnimationStart(getAnimation());
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.f * 2), getMeasuredHeight() + (this.f * 2));
        }
    }

    @DexIgnore
    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    @DexIgnore
    public void a(Animation.AnimationListener animationListener) {
        this.e = animationListener;
    }
}
