package com.fossil.blesdk.obfuscated;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nh1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ g81 e;
    @DexIgnore
    public /* final */ /* synthetic */ ServiceConnection f;
    @DexIgnore
    public /* final */ /* synthetic */ mh1 g;

    @DexIgnore
    public nh1(mh1 mh1, g81 g81, ServiceConnection serviceConnection) {
        this.g = mh1;
        this.e = g81;
        this.f = serviceConnection;
    }

    @DexIgnore
    public final void run() {
        mh1 mh1 = this.g;
        lh1 lh1 = mh1.b;
        String a = mh1.a;
        g81 g81 = this.e;
        ServiceConnection serviceConnection = this.f;
        Bundle a2 = lh1.a(a, g81);
        lh1.a.a().e();
        if (a2 != null) {
            long j = a2.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                lh1.a.d().s().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a2.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    lh1.a.d().s().a("No referrer defined in install referrer response");
                } else {
                    lh1.a.d().A().a("InstallReferrer API result", string);
                    nl1 s = lh1.a.s();
                    String valueOf = String.valueOf(string);
                    Bundle a3 = s.a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a3 == null) {
                        lh1.a.d().s().a("No campaign params defined in install referrer result");
                    } else {
                        String string2 = a3.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a2.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                lh1.a.d().s().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a3.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == lh1.a.t().k.a()) {
                            lh1.a.b();
                            lh1.a.d().A().a("Campaign has already been logged");
                        } else {
                            lh1.a.t().k.a(j);
                            lh1.a.b();
                            lh1.a.d().A().a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a3.putString("_cis", "referrer API");
                            lh1.a.k().b("auto", "_cmp", a3);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            dm0.a().a(lh1.a.getContext(), serviceConnection);
        }
    }
}
