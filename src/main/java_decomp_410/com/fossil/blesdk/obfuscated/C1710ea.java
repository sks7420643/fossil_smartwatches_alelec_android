package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ea */
public abstract class C1710ea extends android.widget.BaseAdapter implements android.widget.Filterable, com.fossil.blesdk.obfuscated.C1780fa.C1781a {

    @DexIgnore
    /* renamed from: e */
    public boolean f4736e;

    @DexIgnore
    /* renamed from: f */
    public boolean f4737f;

    @DexIgnore
    /* renamed from: g */
    public android.database.Cursor f4738g;

    @DexIgnore
    /* renamed from: h */
    public android.content.Context f4739h;

    @DexIgnore
    /* renamed from: i */
    public int f4740i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C1710ea.C1711a f4741j;

    @DexIgnore
    /* renamed from: k */
    public android.database.DataSetObserver f4742k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1780fa f4743l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ea$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ea$a */
    public class C1711a extends android.database.ContentObserver {
        @DexIgnore
        public C1711a() {
            super(new android.os.Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            com.fossil.blesdk.obfuscated.C1710ea.this.mo10381b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ea$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ea$b */
    public class C1712b extends android.database.DataSetObserver {
        @DexIgnore
        public C1712b() {
        }

        @DexIgnore
        public void onChanged() {
            com.fossil.blesdk.obfuscated.C1710ea eaVar = com.fossil.blesdk.obfuscated.C1710ea.this;
            eaVar.f4736e = true;
            eaVar.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            com.fossil.blesdk.obfuscated.C1710ea eaVar = com.fossil.blesdk.obfuscated.C1710ea.this;
            eaVar.f4736e = false;
            eaVar.notifyDataSetInvalidated();
        }
    }

    @DexIgnore
    public C1710ea(android.content.Context context, android.database.Cursor cursor, boolean z) {
        mo10376a(context, cursor, z ? 1 : 2);
    }

    @DexIgnore
    /* renamed from: a */
    public abstract android.view.View mo10375a(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup);

    @DexIgnore
    /* renamed from: a */
    public void mo10376a(android.content.Context context, android.database.Cursor cursor, int i) {
        boolean z = false;
        if ((i & 1) == 1) {
            i |= 2;
            this.f4737f = true;
        } else {
            this.f4737f = false;
        }
        if (cursor != null) {
            z = true;
        }
        this.f4738g = cursor;
        this.f4736e = z;
        this.f4739h = context;
        this.f4740i = z ? cursor.getColumnIndexOrThrow(com.j256.ormlite.field.FieldType.FOREIGN_ID_FIELD_SUFFIX) : -1;
        if ((i & 2) == 2) {
            this.f4741j = new com.fossil.blesdk.obfuscated.C1710ea.C1711a();
            this.f4742k = new com.fossil.blesdk.obfuscated.C1710ea.C1712b();
        } else {
            this.f4741j = null;
            this.f4742k = null;
        }
        if (z) {
            com.fossil.blesdk.obfuscated.C1710ea.C1711a aVar = this.f4741j;
            if (aVar != null) {
                cursor.registerContentObserver(aVar);
            }
            android.database.DataSetObserver dataSetObserver = this.f4742k;
            if (dataSetObserver != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo10378a(android.view.View view, android.content.Context context, android.database.Cursor cursor);

    @DexIgnore
    /* renamed from: b */
    public abstract android.view.View mo10379b(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup);

    @DexIgnore
    /* renamed from: b */
    public abstract java.lang.CharSequence mo10380b(android.database.Cursor cursor);

    @DexIgnore
    /* renamed from: b */
    public void mo10381b() {
        if (this.f4737f) {
            android.database.Cursor cursor = this.f4738g;
            if (cursor != null && !cursor.isClosed()) {
                this.f4736e = this.f4738g.requery();
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public android.database.Cursor mo10382c(android.database.Cursor cursor) {
        android.database.Cursor cursor2 = this.f4738g;
        if (cursor == cursor2) {
            return null;
        }
        if (cursor2 != null) {
            com.fossil.blesdk.obfuscated.C1710ea.C1711a aVar = this.f4741j;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            android.database.DataSetObserver dataSetObserver = this.f4742k;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.f4738g = cursor;
        if (cursor != null) {
            com.fossil.blesdk.obfuscated.C1710ea.C1711a aVar2 = this.f4741j;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            android.database.DataSetObserver dataSetObserver2 = this.f4742k;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.f4740i = cursor.getColumnIndexOrThrow(com.j256.ormlite.field.FieldType.FOREIGN_ID_FIELD_SUFFIX);
            this.f4736e = true;
            notifyDataSetChanged();
        } else {
            this.f4740i = -1;
            this.f4736e = false;
            notifyDataSetInvalidated();
        }
        return cursor2;
    }

    @DexIgnore
    public int getCount() {
        if (!this.f4736e) {
            return 0;
        }
        android.database.Cursor cursor = this.f4738g;
        if (cursor != null) {
            return cursor.getCount();
        }
        return 0;
    }

    @DexIgnore
    public android.view.View getDropDownView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
        if (!this.f4736e) {
            return null;
        }
        this.f4738g.moveToPosition(i);
        if (view == null) {
            view = mo10375a(this.f4739h, this.f4738g, viewGroup);
        }
        mo10378a(view, this.f4739h, this.f4738g);
        return view;
    }

    @DexIgnore
    public android.widget.Filter getFilter() {
        if (this.f4743l == null) {
            this.f4743l = new com.fossil.blesdk.obfuscated.C1780fa(this);
        }
        return this.f4743l;
    }

    @DexIgnore
    public java.lang.Object getItem(int i) {
        if (!this.f4736e) {
            return null;
        }
        android.database.Cursor cursor = this.f4738g;
        if (cursor == null) {
            return null;
        }
        cursor.moveToPosition(i);
        return this.f4738g;
    }

    @DexIgnore
    public long getItemId(int i) {
        if (this.f4736e) {
            android.database.Cursor cursor = this.f4738g;
            if (cursor != null && cursor.moveToPosition(i)) {
                return this.f4738g.getLong(this.f4740i);
            }
        }
        return 0;
    }

    @DexIgnore
    public android.view.View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
        if (!this.f4736e) {
            throw new java.lang.IllegalStateException("this should only be called when the cursor is valid");
        } else if (this.f4738g.moveToPosition(i)) {
            if (view == null) {
                view = mo10379b(this.f4739h, this.f4738g, viewGroup);
            }
            mo10378a(view, this.f4739h, this.f4738g);
            return view;
        } else {
            throw new java.lang.IllegalStateException("couldn't move cursor to position " + i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.database.Cursor mo10374a() {
        return this.f4738g;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10377a(android.database.Cursor cursor) {
        android.database.Cursor c = mo10382c(cursor);
        if (c != null) {
            c.close();
        }
    }
}
