package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jv3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ww3 implements dx3 {
    @DexIgnore
    public /* final */ uw3 a;
    @DexIgnore
    public /* final */ sw3 b;

    @DexIgnore
    public ww3(uw3 uw3, sw3 sw3) {
        this.a = uw3;
        this.b = sw3;
    }

    @DexIgnore
    public xo4 a(hv3 hv3, long j) throws IOException {
        if ("chunked".equalsIgnoreCase(hv3.a("Transfer-Encoding"))) {
            return this.b.f();
        }
        if (j != -1) {
            return this.b.a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void b() throws IOException {
        if (d()) {
            this.b.h();
        } else {
            this.b.b();
        }
    }

    @DexIgnore
    public jv3.b c() throws IOException {
        return this.b.i();
    }

    @DexIgnore
    public boolean d() {
        if (!"close".equalsIgnoreCase(this.a.f().a("Connection")) && !"close".equalsIgnoreCase(this.a.g().a("Connection")) && !this.b.d()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final yo4 b(jv3 jv3) throws IOException {
        if (!uw3.b(jv3)) {
            return this.b.b(0);
        }
        if ("chunked".equalsIgnoreCase(jv3.a("Transfer-Encoding"))) {
            return this.b.a(this.a);
        }
        long a2 = xw3.a(jv3);
        if (a2 != -1) {
            return this.b.b(a2);
        }
        return this.b.g();
    }

    @DexIgnore
    public void a() throws IOException {
        this.b.c();
    }

    @DexIgnore
    public void a(ax3 ax3) throws IOException {
        this.b.a(ax3);
    }

    @DexIgnore
    public void a(hv3 hv3) throws IOException {
        this.a.o();
        this.b.a(hv3.c(), zw3.a(hv3, this.a.e().e().b().type(), this.a.e().d()));
    }

    @DexIgnore
    public kv3 a(jv3 jv3) throws IOException {
        return new yw3(jv3.g(), so4.a(b(jv3)));
    }
}
