package com.fossil.blesdk.obfuscated;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t31 extends l31 implements r31 {
    @DexIgnore
    public t31(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }
}
