package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z0 */
public class C3399z0 implements android.view.Window.Callback {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.view.Window.Callback f11440e;

    @DexIgnore
    public C3399z0(android.view.Window.Callback callback) {
        if (callback != null) {
            this.f11440e = callback;
            return;
        }
        throw new java.lang.IllegalArgumentException("Window callback may not be null");
    }

    @DexIgnore
    public boolean dispatchGenericMotionEvent(android.view.MotionEvent motionEvent) {
        return this.f11440e.dispatchGenericMotionEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchKeyEvent(android.view.KeyEvent keyEvent) {
        return this.f11440e.dispatchKeyEvent(keyEvent);
    }

    @DexIgnore
    public boolean dispatchKeyShortcutEvent(android.view.KeyEvent keyEvent) {
        return this.f11440e.dispatchKeyShortcutEvent(keyEvent);
    }

    @DexIgnore
    public boolean dispatchPopulateAccessibilityEvent(android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        return this.f11440e.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    @DexIgnore
    public boolean dispatchTouchEvent(android.view.MotionEvent motionEvent) {
        return this.f11440e.dispatchTouchEvent(motionEvent);
    }

    @DexIgnore
    public boolean dispatchTrackballEvent(android.view.MotionEvent motionEvent) {
        return this.f11440e.dispatchTrackballEvent(motionEvent);
    }

    @DexIgnore
    public void onActionModeFinished(android.view.ActionMode actionMode) {
        this.f11440e.onActionModeFinished(actionMode);
    }

    @DexIgnore
    public void onActionModeStarted(android.view.ActionMode actionMode) {
        this.f11440e.onActionModeStarted(actionMode);
    }

    @DexIgnore
    public void onAttachedToWindow() {
        this.f11440e.onAttachedToWindow();
    }

    @DexIgnore
    public void onContentChanged() {
        this.f11440e.onContentChanged();
    }

    @DexIgnore
    public boolean onCreatePanelMenu(int i, android.view.Menu menu) {
        return this.f11440e.onCreatePanelMenu(i, menu);
    }

    @DexIgnore
    public android.view.View onCreatePanelView(int i) {
        return this.f11440e.onCreatePanelView(i);
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.f11440e.onDetachedFromWindow();
    }

    @DexIgnore
    public boolean onMenuItemSelected(int i, android.view.MenuItem menuItem) {
        return this.f11440e.onMenuItemSelected(i, menuItem);
    }

    @DexIgnore
    public boolean onMenuOpened(int i, android.view.Menu menu) {
        return this.f11440e.onMenuOpened(i, menu);
    }

    @DexIgnore
    public void onPanelClosed(int i, android.view.Menu menu) {
        this.f11440e.onPanelClosed(i, menu);
    }

    @DexIgnore
    public void onPointerCaptureChanged(boolean z) {
        this.f11440e.onPointerCaptureChanged(z);
    }

    @DexIgnore
    public boolean onPreparePanel(int i, android.view.View view, android.view.Menu menu) {
        return this.f11440e.onPreparePanel(i, view, menu);
    }

    @DexIgnore
    public void onProvideKeyboardShortcuts(java.util.List<android.view.KeyboardShortcutGroup> list, android.view.Menu menu, int i) {
        this.f11440e.onProvideKeyboardShortcuts(list, menu, i);
    }

    @DexIgnore
    public boolean onSearchRequested(android.view.SearchEvent searchEvent) {
        return this.f11440e.onSearchRequested(searchEvent);
    }

    @DexIgnore
    public void onWindowAttributesChanged(android.view.WindowManager.LayoutParams layoutParams) {
        this.f11440e.onWindowAttributesChanged(layoutParams);
    }

    @DexIgnore
    public void onWindowFocusChanged(boolean z) {
        this.f11440e.onWindowFocusChanged(z);
    }

    @DexIgnore
    public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode.Callback callback) {
        return this.f11440e.onWindowStartingActionMode(callback);
    }

    @DexIgnore
    public boolean onSearchRequested() {
        return this.f11440e.onSearchRequested();
    }

    @DexIgnore
    public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode.Callback callback, int i) {
        return this.f11440e.onWindowStartingActionMode(callback, i);
    }
}
