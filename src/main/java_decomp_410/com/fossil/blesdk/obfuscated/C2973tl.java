package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tl */
public class C2973tl implements java.util.concurrent.Executor {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.ArrayDeque<com.fossil.blesdk.obfuscated.C2973tl.C2974a> f9705e; // = new java.util.ArrayDeque<>();

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.util.concurrent.Executor f9706f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.Object f9707g; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: h */
    public volatile java.lang.Runnable f9708h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.tl$a */
    public static class C2974a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C2973tl f9709e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ java.lang.Runnable f9710f;

        @DexIgnore
        public C2974a(com.fossil.blesdk.obfuscated.C2973tl tlVar, java.lang.Runnable runnable) {
            this.f9709e = tlVar;
            this.f9710f = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.f9710f.run();
            } finally {
                this.f9709e.mo16489b();
            }
        }
    }

    @DexIgnore
    public C2973tl(java.util.concurrent.Executor executor) {
        this.f9706f = executor;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16488a() {
        boolean z;
        synchronized (this.f9707g) {
            z = !this.f9705e.isEmpty();
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16489b() {
        synchronized (this.f9707g) {
            java.lang.Runnable poll = this.f9705e.poll();
            this.f9708h = poll;
            if (poll != null) {
                this.f9706f.execute(this.f9708h);
            }
        }
    }

    @DexIgnore
    public void execute(java.lang.Runnable runnable) {
        synchronized (this.f9707g) {
            this.f9705e.add(new com.fossil.blesdk.obfuscated.C2973tl.C2974a(this, runnable));
            if (this.f9708h == null) {
                mo16489b();
            }
        }
    }
}
