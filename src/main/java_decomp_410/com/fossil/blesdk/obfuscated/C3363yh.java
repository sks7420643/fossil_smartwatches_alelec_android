package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yh */
public class C3363yh implements com.fossil.blesdk.obfuscated.C1418ai {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3363yh.C3364a f11248a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yh$a */
    public static class C3364a extends android.view.ViewGroup {

        @DexIgnore
        /* renamed from: e */
        public android.view.ViewGroup f11249e;

        @DexIgnore
        /* renamed from: f */
        public android.view.View f11250f;

        @DexIgnore
        /* renamed from: g */
        public java.util.ArrayList<android.graphics.drawable.Drawable> f11251g; // = null;

        @DexIgnore
        /* renamed from: h */
        public com.fossil.blesdk.obfuscated.C3363yh f11252h;

        /*
        static {
            java.lang.Class<android.view.ViewGroup> cls = android.view.ViewGroup.class;
            try {
                cls.getDeclaredMethod("invalidateChildInParentFast", new java.lang.Class[]{java.lang.Integer.TYPE, java.lang.Integer.TYPE, android.graphics.Rect.class});
            } catch (java.lang.NoSuchMethodException unused) {
            }
        }
        */

        @DexIgnore
        public C3364a(android.content.Context context, android.view.ViewGroup viewGroup, android.view.View view, com.fossil.blesdk.obfuscated.C3363yh yhVar) {
            super(context);
            this.f11249e = viewGroup;
            this.f11250f = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.f11252h = yhVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo18086a(android.graphics.drawable.Drawable drawable) {
            if (this.f11251g == null) {
                this.f11251g = new java.util.ArrayList<>();
            }
            if (!this.f11251g.contains(drawable)) {
                this.f11251g.add(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(this);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo18090b(android.graphics.drawable.Drawable drawable) {
            java.util.ArrayList<android.graphics.drawable.Drawable> arrayList = this.f11251g;
            if (arrayList != null) {
                arrayList.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback((android.graphics.drawable.Drawable.Callback) null);
            }
        }

        @DexIgnore
        public void dispatchDraw(android.graphics.Canvas canvas) {
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            this.f11249e.getLocationOnScreen(iArr);
            this.f11250f.getLocationOnScreen(iArr2);
            canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
            canvas.clipRect(new android.graphics.Rect(0, 0, this.f11250f.getWidth(), this.f11250f.getHeight()));
            super.dispatchDraw(canvas);
            java.util.ArrayList<android.graphics.drawable.Drawable> arrayList = this.f11251g;
            int size = arrayList == null ? 0 : arrayList.size();
            for (int i = 0; i < size; i++) {
                this.f11251g.get(i).draw(canvas);
            }
        }

        @DexIgnore
        public boolean dispatchTouchEvent(android.view.MotionEvent motionEvent) {
            return false;
        }

        @DexIgnore
        public android.view.ViewParent invalidateChildInParent(int[] iArr, android.graphics.Rect rect) {
            if (this.f11249e == null) {
                return null;
            }
            rect.offset(iArr[0], iArr[1]);
            if (this.f11249e instanceof android.view.ViewGroup) {
                iArr[0] = 0;
                iArr[1] = 0;
                int[] iArr2 = new int[2];
                mo18088a(iArr2);
                rect.offset(iArr2[0], iArr2[1]);
                return super.invalidateChildInParent(iArr, rect);
            }
            invalidate(rect);
            return null;
        }

        @DexIgnore
        public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        @DexIgnore
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        @DexIgnore
        public boolean verifyDrawable(android.graphics.drawable.Drawable drawable) {
            if (!super.verifyDrawable(drawable)) {
                java.util.ArrayList<android.graphics.drawable.Drawable> arrayList = this.f11251g;
                if (arrayList == null || !arrayList.contains(drawable)) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo18091b(android.view.View view) {
            super.removeView(view);
            if (mo18089a()) {
                this.f11249e.removeView(this);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo18087a(android.view.View view) {
            if (view.getParent() instanceof android.view.ViewGroup) {
                android.view.ViewGroup viewGroup = (android.view.ViewGroup) view.getParent();
                if (!(viewGroup == this.f11249e || viewGroup.getParent() == null || !com.fossil.blesdk.obfuscated.C1776f9.m6859y(viewGroup))) {
                    int[] iArr = new int[2];
                    int[] iArr2 = new int[2];
                    viewGroup.getLocationOnScreen(iArr);
                    this.f11249e.getLocationOnScreen(iArr2);
                    com.fossil.blesdk.obfuscated.C1776f9.m6831c(view, iArr[0] - iArr2[0]);
                    com.fossil.blesdk.obfuscated.C1776f9.m6834d(view, iArr[1] - iArr2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view, getChildCount() - 1);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo18089a() {
            if (getChildCount() == 0) {
                java.util.ArrayList<android.graphics.drawable.Drawable> arrayList = this.f11251g;
                if (arrayList == null || arrayList.size() == 0) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo18088a(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            this.f11249e.getLocationOnScreen(iArr2);
            this.f11250f.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }
    }

    @DexIgnore
    public C3363yh(android.content.Context context, android.view.ViewGroup viewGroup, android.view.View view) {
        this.f11248a = new com.fossil.blesdk.obfuscated.C3363yh.C3364a(context, viewGroup, view, this);
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C3363yh m16850c(android.view.View view) {
        android.view.ViewGroup d = m16851d(view);
        if (d == null) {
            return null;
        }
        int childCount = d.getChildCount();
        for (int i = 0; i < childCount; i++) {
            android.view.View childAt = d.getChildAt(i);
            if (childAt instanceof com.fossil.blesdk.obfuscated.C3363yh.C3364a) {
                return ((com.fossil.blesdk.obfuscated.C3363yh.C3364a) childAt).f11252h;
            }
        }
        return new com.fossil.blesdk.obfuscated.C2878sh(d.getContext(), d, view);
    }

    @DexIgnore
    /* renamed from: d */
    public static android.view.ViewGroup m16851d(android.view.View view) {
        while (view != null) {
            if (view.getId() == 16908290 && (view instanceof android.view.ViewGroup)) {
                return (android.view.ViewGroup) view;
            }
            if (view.getParent() instanceof android.view.ViewGroup) {
                view = (android.view.ViewGroup) view.getParent();
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8741a(android.graphics.drawable.Drawable drawable) {
        this.f11248a.mo18086a(drawable);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8742b(android.graphics.drawable.Drawable drawable) {
        this.f11248a.mo18090b(drawable);
    }
}
