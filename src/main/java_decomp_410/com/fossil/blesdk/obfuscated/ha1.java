package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzuv;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ha1<MessageType> {
    @DexIgnore
    MessageType a(y71 y71, i81 i81) throws zzuv;
}
