package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g21 {
    @DexIgnore
    public static <DP, DT> String a(DP dp, e21<DP, DT> e21) {
        double d;
        f21<DT> zzb = e21.zzb();
        if (!zzb.a(e21.zzb(dp))) {
            return null;
        }
        DT zza = e21.zza(dp);
        for (int i = 0; i < zzb.zzc(zza); i++) {
            String b = zzb.b(zza, i);
            if (e21.c(dp, i)) {
                double c = (double) zzb.c(zza, i);
                if (c == 1.0d) {
                    d = (double) e21.b(dp, i);
                } else if (c == 2.0d) {
                    d = e21.a(dp, i);
                } else {
                    continue;
                }
                j21 a = h21.a().a(b);
                if (a != null && !a.a(d)) {
                    return "Field out of range";
                }
                j21 a2 = h21.a().a(zzb.zzd(zza), b);
                if (a2 != null) {
                    long a3 = e21.a(dp, TimeUnit.NANOSECONDS);
                    if (a3 == 0) {
                        if (d == 0.0d) {
                            return null;
                        }
                        return "DataPoint out of range";
                    } else if (!a2.a(d / ((double) a3))) {
                        return "DataPoint out of range";
                    }
                } else {
                    continue;
                }
            } else if (!zzb.a(zza, i) && !h21.g.contains(b)) {
                return String.valueOf(b).concat(" not set");
            }
        }
        return null;
    }
}
