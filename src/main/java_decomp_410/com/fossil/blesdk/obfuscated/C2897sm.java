package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sm */
public class C2897sm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f9390a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ byte[] f9391b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Map<java.lang.String, java.lang.String> f9392c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2660pm> f9393d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ boolean f9394e;

    @DexIgnore
    @java.lang.Deprecated
    public C2897sm(int i, byte[] bArr, java.util.Map<java.lang.String, java.lang.String> map, boolean z, long j) {
        this(i, bArr, map, m13800a(map), z, j);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.Map<java.lang.String, java.lang.String> m13801a(java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return java.util.Collections.emptyMap();
        }
        java.util.TreeMap treeMap = new java.util.TreeMap(java.lang.String.CASE_INSENSITIVE_ORDER);
        for (com.fossil.blesdk.obfuscated.C2660pm next : list) {
            treeMap.put(next.mo14804a(), next.mo14805b());
        }
        return treeMap;
    }

    @DexIgnore
    public C2897sm(int i, byte[] bArr, boolean z, long j, java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list) {
        this(i, bArr, m13801a(list), list, z, j);
    }

    @DexIgnore
    @java.lang.Deprecated
    public C2897sm(byte[] bArr, java.util.Map<java.lang.String, java.lang.String> map) {
        this(200, bArr, map, false, 0);
    }

    @DexIgnore
    public C2897sm(int i, byte[] bArr, java.util.Map<java.lang.String, java.lang.String> map, java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list, boolean z, long j) {
        this.f9390a = i;
        this.f9391b = bArr;
        this.f9392c = map;
        if (list == null) {
            this.f9393d = null;
        } else {
            this.f9393d = java.util.Collections.unmodifiableList(list);
        }
        this.f9394e = z;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<com.fossil.blesdk.obfuscated.C2660pm> m13800a(java.util.Map<java.lang.String, java.lang.String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return java.util.Collections.emptyList();
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(map.size());
        for (java.util.Map.Entry next : map.entrySet()) {
            arrayList.add(new com.fossil.blesdk.obfuscated.C2660pm((java.lang.String) next.getKey(), (java.lang.String) next.getValue()));
        }
        return arrayList;
    }
}
