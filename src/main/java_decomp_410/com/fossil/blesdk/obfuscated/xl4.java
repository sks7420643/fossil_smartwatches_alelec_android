package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.security.cert.Certificate;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import okhttp3.TlsVersion;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xl4 {
    @DexIgnore
    public /* final */ TlsVersion a;
    @DexIgnore
    public /* final */ ml4 b;
    @DexIgnore
    public /* final */ List<Certificate> c;
    @DexIgnore
    public /* final */ List<Certificate> d;

    @DexIgnore
    public xl4(TlsVersion tlsVersion, ml4 ml4, List<Certificate> list, List<Certificate> list2) {
        this.a = tlsVersion;
        this.b = ml4;
        this.c = list;
        this.d = list2;
    }

    @DexIgnore
    public static xl4 a(SSLSession sSLSession) throws IOException {
        Certificate[] certificateArr;
        List list;
        List list2;
        String cipherSuite = sSLSession.getCipherSuite();
        if (cipherSuite == null) {
            throw new IllegalStateException("cipherSuite == null");
        } else if (!"SSL_NULL_WITH_NULL_NULL".equals(cipherSuite)) {
            ml4 a2 = ml4.a(cipherSuite);
            String protocol = sSLSession.getProtocol();
            if (protocol == null) {
                throw new IllegalStateException("tlsVersion == null");
            } else if (!"NONE".equals(protocol)) {
                TlsVersion forJavaName = TlsVersion.forJavaName(protocol);
                try {
                    certificateArr = sSLSession.getPeerCertificates();
                } catch (SSLPeerUnverifiedException unused) {
                    certificateArr = null;
                }
                if (certificateArr != null) {
                    list = jm4.a((T[]) certificateArr);
                } else {
                    list = Collections.emptyList();
                }
                Certificate[] localCertificates = sSLSession.getLocalCertificates();
                if (localCertificates != null) {
                    list2 = jm4.a((T[]) localCertificates);
                } else {
                    list2 = Collections.emptyList();
                }
                return new xl4(forJavaName, a2, list, list2);
            } else {
                throw new IOException("tlsVersion == NONE");
            }
        } else {
            throw new IOException("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }
    }

    @DexIgnore
    public List<Certificate> b() {
        return this.d;
    }

    @DexIgnore
    public List<Certificate> c() {
        return this.c;
    }

    @DexIgnore
    public TlsVersion d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof xl4)) {
            return false;
        }
        xl4 xl4 = (xl4) obj;
        if (!this.a.equals(xl4.a) || !this.b.equals(xl4.b) || !this.c.equals(xl4.c) || !this.d.equals(xl4.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    public static xl4 a(TlsVersion tlsVersion, ml4 ml4, List<Certificate> list, List<Certificate> list2) {
        if (tlsVersion == null) {
            throw new NullPointerException("tlsVersion == null");
        } else if (ml4 != null) {
            return new xl4(tlsVersion, ml4, jm4.a(list), jm4.a(list2));
        } else {
            throw new NullPointerException("cipherSuite == null");
        }
    }

    @DexIgnore
    public ml4 a() {
        return this.b;
    }
}
