package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ k04 g;

    @DexIgnore
    public x04(Context context, String str, k04 k04) {
        this.e = context;
        this.f = str;
        this.g = k04;
    }

    @DexIgnore
    public final void run() {
        Long l;
        try {
            j04.f(this.e);
            synchronized (j04.k) {
                l = (Long) j04.k.remove(this.f);
            }
            if (l != null) {
                Long valueOf = Long.valueOf((System.currentTimeMillis() - l.longValue()) / 1000);
                if (valueOf.longValue() <= 0) {
                    valueOf = 1L;
                }
                Long l2 = valueOf;
                String j = j04.j;
                if (j != null && j.equals(this.f)) {
                    j = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                r04 r04 = new r04(this.e, j, this.f, j04.a(this.e, false, this.g), l2, this.g);
                if (!this.f.equals(j04.i)) {
                    j04.m.h("Invalid invocation since previous onResume on diff page.");
                }
                new c14(r04).a();
                String unused = j04.j = this.f;
                return;
            }
            t14 f2 = j04.m;
            f2.c("Starttime for PageID:" + this.f + " not found, lost onResume()?");
        } catch (Throwable th) {
            j04.m.a(th);
            j04.a(this.e, th);
        }
    }
}
