package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ac */
public class C1411ac<T> extends androidx.lifecycle.MutableLiveData<T> {

    @DexIgnore
    /* renamed from: k */
    public com.fossil.blesdk.obfuscated.C2282l3<androidx.lifecycle.LiveData<?>, com.fossil.blesdk.obfuscated.C1411ac.C1412a<?>> f3464k; // = new com.fossil.blesdk.obfuscated.C2282l3<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ac$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ac$a */
    public static class C1412a<V> implements com.fossil.blesdk.obfuscated.C1548cc<V> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ androidx.lifecycle.LiveData<V> f3465a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1548cc<? super V> f3466b;

        @DexIgnore
        /* renamed from: c */
        public int f3467c; // = -1;

        @DexIgnore
        public C1412a(androidx.lifecycle.LiveData<V> liveData, com.fossil.blesdk.obfuscated.C1548cc<? super V> ccVar) {
            this.f3465a = liveData;
            this.f3466b = ccVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8688a() {
            this.f3465a.mo2279a(this);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8690b() {
            this.f3465a.mo2283b(this);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8689a(V v) {
            if (this.f3467c != this.f3465a.mo2281b()) {
                this.f3467c = this.f3465a.mo2281b();
                this.f3466b.mo8689a(v);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <S> void mo8687a(androidx.lifecycle.LiveData<S> liveData, com.fossil.blesdk.obfuscated.C1548cc<? super S> ccVar) {
        com.fossil.blesdk.obfuscated.C1411ac.C1412a aVar = new com.fossil.blesdk.obfuscated.C1411ac.C1412a(liveData, ccVar);
        com.fossil.blesdk.obfuscated.C1411ac.C1412a b = this.f3464k.mo12618b(liveData, aVar);
        if (b != null && b.f3466b != ccVar) {
            throw new java.lang.IllegalArgumentException("This source was already added with the different observer");
        } else if (b == null && mo2285c()) {
            aVar.mo8688a();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo2286d() {
        java.util.Iterator<java.util.Map.Entry<androidx.lifecycle.LiveData<?>, com.fossil.blesdk.obfuscated.C1411ac.C1412a<?>>> it = this.f3464k.iterator();
        while (it.hasNext()) {
            ((com.fossil.blesdk.obfuscated.C1411ac.C1412a) it.next().getValue()).mo8688a();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo2287e() {
        java.util.Iterator<java.util.Map.Entry<androidx.lifecycle.LiveData<?>, com.fossil.blesdk.obfuscated.C1411ac.C1412a<?>>> it = this.f3464k.iterator();
        while (it.hasNext()) {
            ((com.fossil.blesdk.obfuscated.C1411ac.C1412a) it.next().getValue()).mo8690b();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <S> void mo8686a(androidx.lifecycle.LiveData<S> liveData) {
        com.fossil.blesdk.obfuscated.C1411ac.C1412a remove = this.f3464k.remove(liveData);
        if (remove != null) {
            remove.mo8690b();
        }
    }
}
