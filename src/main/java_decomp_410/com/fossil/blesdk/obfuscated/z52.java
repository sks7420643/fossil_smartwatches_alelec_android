package com.fossil.blesdk.obfuscated;

import android.os.Build;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z52 {
    @DexIgnore
    public z52() {
        Executors.newFixedThreadPool(Build.MANUFACTURER.equalsIgnoreCase(LocationUtils.HUAWEI_MODEL) ? 1 : 5);
    }

    @DexIgnore
    public static void a() {
        new z52();
    }
}
