package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.iv */
public class C2076iv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.atomic.AtomicReference<com.fossil.blesdk.obfuscated.C2925sw> f6214a; // = new java.util.concurrent.atomic.AtomicReference<>();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<com.fossil.blesdk.obfuscated.C2925sw, java.util.List<java.lang.Class<?>>> f6215b; // = new com.fossil.blesdk.obfuscated.C1855g4<>();

    @DexIgnore
    /* renamed from: a */
    public java.util.List<java.lang.Class<?>> mo12132a(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3) {
        java.util.List<java.lang.Class<?>> list;
        com.fossil.blesdk.obfuscated.C2925sw andSet = this.f6214a.getAndSet((java.lang.Object) null);
        if (andSet == null) {
            andSet = new com.fossil.blesdk.obfuscated.C2925sw(cls, cls2, cls3);
        } else {
            andSet.mo16203a(cls, cls2, cls3);
        }
        synchronized (this.f6215b) {
            list = this.f6215b.get(andSet);
        }
        this.f6214a.set(andSet);
        return list;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12133a(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3, java.util.List<java.lang.Class<?>> list) {
        synchronized (this.f6215b) {
            this.f6215b.put(new com.fossil.blesdk.obfuscated.C2925sw(cls, cls2, cls3), list);
        }
    }
}
