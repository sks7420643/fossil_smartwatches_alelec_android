package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.home.HomePresenter;
import com.portfolio.platform.util.UserUtils;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cv2 implements Factory<HomePresenter> {
    @DexIgnore
    public static HomePresenter a(ru2 ru2, en2 en2, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, j62 j62, vj2 vj2, qr2 qr2, UserUtils userUtils, ServerSettingRepository serverSettingRepository, kr2 kr2, ak3 ak3) {
        return new HomePresenter(ru2, en2, deviceRepository, portfolioApp, updateFirmwareUsecase, j62, vj2, qr2, userUtils, serverSettingRepository, kr2, ak3);
    }
}
