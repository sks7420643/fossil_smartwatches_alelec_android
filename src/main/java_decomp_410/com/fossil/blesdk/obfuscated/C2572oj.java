package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oj */
public class C2572oj implements com.fossil.blesdk.obfuscated.C2419mj {

    @DexIgnore
    /* renamed from: n */
    public static /* final */ java.lang.String f8136n; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("Processor");

    @DexIgnore
    /* renamed from: e */
    public android.content.Context f8137e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C3290xi f8138f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C3444zl f8139g;

    @DexIgnore
    /* renamed from: h */
    public androidx.work.impl.WorkDatabase f8140h;

    @DexIgnore
    /* renamed from: i */
    public java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C3040uj> f8141i; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: j */
    public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> f8142j;

    @DexIgnore
    /* renamed from: k */
    public java.util.Set<java.lang.String> f8143k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2419mj> f8144l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.lang.Object f8145m;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oj$a */
    public static class C2573a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C2419mj f8146e;

        @DexIgnore
        /* renamed from: f */
        public java.lang.String f8147f;

        @DexIgnore
        /* renamed from: g */
        public com.fossil.blesdk.obfuscated.zv1<java.lang.Boolean> f8148g;

        @DexIgnore
        public C2573a(com.fossil.blesdk.obfuscated.C2419mj mjVar, java.lang.String str, com.fossil.blesdk.obfuscated.zv1<java.lang.Boolean> zv1) {
            this.f8146e = mjVar;
            this.f8147f = str;
            this.f8148g = zv1;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.f8148g.get().booleanValue();
            } catch (java.lang.InterruptedException | java.util.concurrent.ExecutionException unused) {
                z = true;
            }
            this.f8146e.mo3804a(this.f8147f, z);
        }
    }

    @DexIgnore
    public C2572oj(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar, androidx.work.impl.WorkDatabase workDatabase, java.util.List<com.fossil.blesdk.obfuscated.C2656pj> list) {
        this.f8137e = context;
        this.f8138f = xiVar;
        this.f8139g = zlVar;
        this.f8140h = workDatabase;
        this.f8142j = list;
        this.f8143k = new java.util.HashSet();
        this.f8144l = new java.util.ArrayList();
        this.f8145m = new java.lang.Object();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14426a(java.lang.String str, androidx.work.WorkerParameters.C0359a aVar) {
        synchronized (this.f8145m) {
            if (this.f8141i.containsKey(str)) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("Work %s is already enqueued for processing", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
                return false;
            }
            com.fossil.blesdk.obfuscated.C3040uj.C3043c cVar = new com.fossil.blesdk.obfuscated.C3040uj.C3043c(this.f8137e, this.f8138f, this.f8139g, this.f8140h, str);
            cVar.mo16799a(this.f8142j);
            cVar.mo16798a(aVar);
            com.fossil.blesdk.obfuscated.C3040uj a = cVar.mo16800a();
            com.fossil.blesdk.obfuscated.zv1<java.lang.Boolean> a2 = a.mo16780a();
            a2.mo3818a(new com.fossil.blesdk.obfuscated.C2572oj.C2573a(this, str, a2), this.f8139g.mo8787a());
            this.f8141i.put(str, a);
            this.f8139g.mo8789b().execute(a);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("%s: processing %s", new java.lang.Object[]{com.fossil.blesdk.obfuscated.C2572oj.class.getSimpleName(), str}), new java.lang.Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo14428b(java.lang.String str) {
        boolean containsKey;
        synchronized (this.f8145m) {
            containsKey = this.f8141i.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo14429c(java.lang.String str) {
        return mo14426a(str, (androidx.work.WorkerParameters.C0359a) null);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo14430d(java.lang.String str) {
        synchronized (this.f8145m) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("Processor cancelling %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            this.f8143k.add(str);
            com.fossil.blesdk.obfuscated.C3040uj remove = this.f8141i.remove(str);
            if (remove != null) {
                remove.mo16784a(true);
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("WorkerWrapper cancelled for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
                return true;
            }
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("WorkerWrapper could not be found for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo14431e(java.lang.String str) {
        synchronized (this.f8145m) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("Processor stopping %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            com.fossil.blesdk.obfuscated.C3040uj remove = this.f8141i.remove(str);
            if (remove != null) {
                remove.mo16784a(false);
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("WorkerWrapper stopped for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
                return true;
            }
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("WorkerWrapper could not be found for %s", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14427b(com.fossil.blesdk.obfuscated.C2419mj mjVar) {
        synchronized (this.f8145m) {
            this.f8144l.remove(mjVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14425a(java.lang.String str) {
        boolean contains;
        synchronized (this.f8145m) {
            contains = this.f8143k.contains(str);
        }
        return contains;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14424a(com.fossil.blesdk.obfuscated.C2419mj mjVar) {
        synchronized (this.f8145m) {
            this.f8144l.add(mjVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3804a(java.lang.String str, boolean z) {
        synchronized (this.f8145m) {
            this.f8141i.remove(str);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f8136n, java.lang.String.format("%s %s executed; reschedule = %s", new java.lang.Object[]{getClass().getSimpleName(), str, java.lang.Boolean.valueOf(z)}), new java.lang.Throwable[0]);
            for (com.fossil.blesdk.obfuscated.C2419mj a : this.f8144l) {
                a.mo3804a(str, z);
            }
        }
    }
}
