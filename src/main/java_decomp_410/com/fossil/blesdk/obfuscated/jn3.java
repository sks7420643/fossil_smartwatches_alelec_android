package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.enums.SyncResponseCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class jn3 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[SyncResponseCode.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];

    /*
    static {
        a[SyncResponseCode.FAIL_DUE_TO_LACK_PERMISSION.ordinal()] = 1;
        a[SyncResponseCode.FAIL_DUE_TO_PENDING_WORKOUT.ordinal()] = 2;
        a[SyncResponseCode.FAIL_DUE_TO_SYNC_FAIL.ordinal()] = 3;
        a[SyncResponseCode.FAIL_DUE_TO_USER_DENY_STOP_WORKOUT.ordinal()] = 4;
        b[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 1;
    }
    */
}
