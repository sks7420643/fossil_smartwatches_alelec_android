package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ma2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ RecyclerView s;
    @DexIgnore
    public /* final */ RecyclerView t;
    @DexIgnore
    public /* final */ Switch u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public ma2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ImageView imageView, RecyclerView recyclerView, RecyclerView recyclerView2, Switch switchR, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = imageView;
        this.s = recyclerView;
        this.t = recyclerView2;
        this.u = switchR;
        this.v = flexibleTextView;
        this.w = flexibleTextView2;
        this.x = flexibleTextView3;
        this.y = flexibleTextView4;
        this.z = flexibleTextView5;
    }
}
