package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xm */
public class C3296xm {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String f11006a; // = "Volley";

    @DexIgnore
    /* renamed from: b */
    public static boolean f11007b; // = android.util.Log.isLoggable(f11006a, 2);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.String f11008c; // = com.fossil.blesdk.obfuscated.C3296xm.class.getName();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xm$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xm$a */
    public static class C3297a {

        @DexIgnore
        /* renamed from: c */
        public static /* final */ boolean f11009c; // = com.fossil.blesdk.obfuscated.C3296xm.f11007b;

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a> f11010a; // = new java.util.ArrayList();

        @DexIgnore
        /* renamed from: b */
        public boolean f11011b; // = false;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xm$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.xm$a$a */
        public static class C3298a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.lang.String f11012a;

            @DexIgnore
            /* renamed from: b */
            public /* final */ long f11013b;

            @DexIgnore
            /* renamed from: c */
            public /* final */ long f11014c;

            @DexIgnore
            public C3298a(java.lang.String str, long j, long j2) {
                this.f11012a = str;
                this.f11013b = j;
                this.f11014c = j2;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public synchronized void mo17808a(java.lang.String str, long j) {
            if (!this.f11011b) {
                java.util.List<com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a> list = this.f11010a;
                com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a aVar = new com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a(str, j, android.os.SystemClock.elapsedRealtime());
                list.add(aVar);
            } else {
                throw new java.lang.IllegalStateException("Marker added to finished log");
            }
        }

        @DexIgnore
        public void finalize() throws java.lang.Throwable {
            if (!this.f11011b) {
                mo17807a("Request on the loose");
                com.fossil.blesdk.obfuscated.C3296xm.m16422c("Marker log finalized without finish() - uncaught exit point for request", new java.lang.Object[0]);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public synchronized void mo17807a(java.lang.String str) {
            this.f11011b = true;
            long a = mo17806a();
            if (a > 0) {
                long j = this.f11010a.get(0).f11014c;
                com.fossil.blesdk.obfuscated.C3296xm.m16421b("(%-4d ms) %s", java.lang.Long.valueOf(a), str);
                for (com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a next : this.f11010a) {
                    long j2 = next.f11014c;
                    com.fossil.blesdk.obfuscated.C3296xm.m16421b("(+%-4d) [%2d] %s", java.lang.Long.valueOf(j2 - j), java.lang.Long.valueOf(next.f11013b), next.f11012a);
                    j = j2;
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final long mo17806a() {
            if (this.f11010a.size() == 0) {
                return 0;
            }
            long j = this.f11010a.get(0).f11014c;
            java.util.List<com.fossil.blesdk.obfuscated.C3296xm.C3297a.C3298a> list = this.f11010a;
            return list.get(list.size() - 1).f11014c - j;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m16420a(java.lang.Throwable th, java.lang.String str, java.lang.Object... objArr) {
        android.util.Log.e(f11006a, m16419a(str, objArr), th);
    }

    @DexIgnore
    /* renamed from: b */
    public static void m16421b(java.lang.String str, java.lang.Object... objArr) {
        android.util.Log.d(f11006a, m16419a(str, objArr));
    }

    @DexIgnore
    /* renamed from: c */
    public static void m16422c(java.lang.String str, java.lang.Object... objArr) {
        android.util.Log.e(f11006a, m16419a(str, objArr));
    }

    @DexIgnore
    /* renamed from: d */
    public static void m16423d(java.lang.String str, java.lang.Object... objArr) {
        if (f11007b) {
            android.util.Log.v(f11006a, m16419a(str, objArr));
        }
    }

    @DexIgnore
    /* renamed from: e */
    public static void m16424e(java.lang.String str, java.lang.Object... objArr) {
        android.util.Log.wtf(f11006a, m16419a(str, objArr));
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m16419a(java.lang.String str, java.lang.Object... objArr) {
        java.lang.String str2;
        if (objArr != null) {
            str = java.lang.String.format(java.util.Locale.US, str, objArr);
        }
        java.lang.StackTraceElement[] stackTrace = new java.lang.Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClassName().equals(f11008c)) {
                java.lang.String className = stackTrace[i].getClassName();
                java.lang.String substring = className.substring(className.lastIndexOf(46) + 1);
                java.lang.String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                str2 = substring2 + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return java.lang.String.format(java.util.Locale.US, "[%d] %s: %s", new java.lang.Object[]{java.lang.Long.valueOf(java.lang.Thread.currentThread().getId()), str2, str});
    }
}
