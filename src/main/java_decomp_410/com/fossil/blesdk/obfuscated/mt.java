package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mt<T extends Drawable> implements aq<T>, wp {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public mt(T t) {
        tw.a(t);
        this.e = (Drawable) t;
    }

    @DexIgnore
    public void d() {
        T t = this.e;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof ut) {
            ((ut) t).e().prepareToDraw();
        }
    }

    @DexIgnore
    public final T get() {
        Drawable.ConstantState constantState = this.e.getConstantState();
        if (constantState == null) {
            return this.e;
        }
        return constantState.newDrawable();
    }
}
