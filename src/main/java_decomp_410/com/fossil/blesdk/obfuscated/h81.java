package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h81 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a();

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("com.google.protobuf.ExtensionRegistry");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static i81 b() {
        if (a != null) {
            try {
                return a("getEmptyRegistry");
            } catch (Exception unused) {
            }
        }
        return i81.c;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0014  */
    /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000e  */
    public static i81 c() {
        i81 i81;
        if (a != null) {
            try {
                i81 = a("loadGeneratedRegistry");
            } catch (Exception unused) {
            }
            if (i81 == null) {
                i81 = i81.a();
            }
            return i81 != null ? b() : i81;
        }
        i81 = null;
        if (i81 == null) {
        }
        if (i81 != null) {
        }
    }

    @DexIgnore
    public static final i81 a(String str) throws Exception {
        return (i81) a.getDeclaredMethod(str, new Class[0]).invoke((Object) null, new Object[0]);
    }
}
