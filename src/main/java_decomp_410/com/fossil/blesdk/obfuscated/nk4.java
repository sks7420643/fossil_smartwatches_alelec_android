package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nk4 extends ok4 {
    @DexIgnore
    public static /* final */ ug4 j;
    @DexIgnore
    public static /* final */ nk4 k;

    /*
    static {
        nk4 nk4 = new nk4();
        k = nk4;
        j = nk4.b(gk4.a("kotlinx.coroutines.io.parallelism", ee4.a(64, ek4.a()), 0, 0, 12, (Object) null));
    }
    */

    @DexIgnore
    public nk4() {
        super(0, 0, (String) null, 7, (fd4) null);
    }

    @DexIgnore
    public final ug4 D() {
        return j;
    }

    @DexIgnore
    public void close() {
        throw new UnsupportedOperationException("DefaultDispatcher cannot be closed");
    }

    @DexIgnore
    public String toString() {
        return "DefaultDispatcher";
    }
}
