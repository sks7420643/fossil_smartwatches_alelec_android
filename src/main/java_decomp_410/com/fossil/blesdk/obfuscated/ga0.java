package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ga0 {
    @DexIgnore
    public static /* final */ ga0 a; // = new ga0();

    @DexIgnore
    public final byte[] a(Integer num, JSONObject jSONObject) {
        kd4.b(jSONObject, "responseSettingJSONData");
        JSONObject jSONObject2 = new JSONObject();
        String str = num == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", num);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e) {
            da0.l.a(e);
        }
        String jSONObject4 = jSONObject2.toString();
        kd4.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = ua0.y.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
