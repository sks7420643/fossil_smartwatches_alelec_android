package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManagerImpl;
import com.fossil.blesdk.obfuscated.va;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wa implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wa> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int[] e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ CharSequence k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ CharSequence m;
    @DexIgnore
    public /* final */ ArrayList<String> n;
    @DexIgnore
    public /* final */ ArrayList<String> o;
    @DexIgnore
    public /* final */ boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<wa> {
        @DexIgnore
        public wa createFromParcel(Parcel parcel) {
            return new wa(parcel);
        }

        @DexIgnore
        public wa[] newArray(int i) {
            return new wa[i];
        }
    }

    @DexIgnore
    public wa(va vaVar) {
        int size = vaVar.b.size();
        this.e = new int[(size * 6)];
        if (vaVar.i) {
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                va.a aVar = vaVar.b.get(i3);
                int[] iArr = this.e;
                int i4 = i2 + 1;
                iArr[i2] = aVar.a;
                int i5 = i4 + 1;
                Fragment fragment = aVar.b;
                iArr[i4] = fragment != null ? fragment.mIndex : -1;
                int[] iArr2 = this.e;
                int i6 = i5 + 1;
                iArr2[i5] = aVar.c;
                int i7 = i6 + 1;
                iArr2[i6] = aVar.d;
                int i8 = i7 + 1;
                iArr2[i7] = aVar.e;
                i2 = i8 + 1;
                iArr2[i8] = aVar.f;
            }
            this.f = vaVar.g;
            this.g = vaVar.h;
            this.h = vaVar.k;
            this.i = vaVar.m;
            this.j = vaVar.n;
            this.k = vaVar.o;
            this.l = vaVar.p;
            this.m = vaVar.q;
            this.n = vaVar.r;
            this.o = vaVar.s;
            this.p = vaVar.t;
            return;
        }
        throw new IllegalStateException("Not on back stack");
    }

    @DexIgnore
    public va a(FragmentManagerImpl fragmentManagerImpl) {
        va vaVar = new va(fragmentManagerImpl);
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.e.length) {
            va.a aVar = new va.a();
            int i4 = i2 + 1;
            aVar.a = this.e[i2];
            if (FragmentManagerImpl.I) {
                Log.v("FragmentManager", "Instantiate " + vaVar + " op #" + i3 + " base fragment #" + this.e[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.e[i4];
            if (i6 >= 0) {
                aVar.b = fragmentManagerImpl.i.get(i6);
            } else {
                aVar.b = null;
            }
            int[] iArr = this.e;
            int i7 = i5 + 1;
            aVar.c = iArr[i5];
            int i8 = i7 + 1;
            aVar.d = iArr[i7];
            int i9 = i8 + 1;
            aVar.e = iArr[i8];
            aVar.f = iArr[i9];
            vaVar.c = aVar.c;
            vaVar.d = aVar.d;
            vaVar.e = aVar.e;
            vaVar.f = aVar.f;
            vaVar.a(aVar);
            i3++;
            i2 = i9 + 1;
        }
        vaVar.g = this.f;
        vaVar.h = this.g;
        vaVar.k = this.h;
        vaVar.m = this.i;
        vaVar.i = true;
        vaVar.n = this.j;
        vaVar.o = this.k;
        vaVar.p = this.l;
        vaVar.q = this.m;
        vaVar.r = this.n;
        vaVar.s = this.o;
        vaVar.t = this.p;
        vaVar.a(1);
        return vaVar;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
        TextUtils.writeToParcel(this.k, parcel, 0);
        parcel.writeInt(this.l);
        TextUtils.writeToParcel(this.m, parcel, 0);
        parcel.writeStringList(this.n);
        parcel.writeStringList(this.o);
        parcel.writeInt(this.p ? 1 : 0);
    }

    @DexIgnore
    public wa(Parcel parcel) {
        this.e = parcel.createIntArray();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readString();
        this.i = parcel.readInt();
        this.j = parcel.readInt();
        this.k = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.l = parcel.readInt();
        this.m = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.n = parcel.createStringArrayList();
        this.o = parcel.createStringArrayList();
        this.p = parcel.readInt() != 0;
    }
}
