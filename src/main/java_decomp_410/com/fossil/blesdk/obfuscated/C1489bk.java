package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bk */
public class C1489bk implements com.fossil.blesdk.obfuscated.C2656pj {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f3766f; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("SystemAlarmScheduler");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f3767e;

    @DexIgnore
    public C1489bk(android.content.Context context) {
        this.f3767e = context.getApplicationContext();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9223a(com.fossil.blesdk.obfuscated.C1954hl... hlVarArr) {
        for (com.fossil.blesdk.obfuscated.C1954hl a : hlVarArr) {
            mo9221a(a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9222a(java.lang.String str) {
        this.f3767e.startService(com.fossil.blesdk.obfuscated.C3293xj.m16365c(this.f3767e, str));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9221a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f3766f, java.lang.String.format("Scheduling work with workSpecId %s", new java.lang.Object[]{hlVar.f5771a}), new java.lang.Throwable[0]);
        this.f3767e.startService(com.fossil.blesdk.obfuscated.C3293xj.m16364b(this.f3767e, hlVar.f5771a));
    }
}
