package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u4 */
public class C3007u4 {
    @DexIgnore
    /* renamed from: a */
    public static void m14564a(com.fossil.blesdk.obfuscated.C3342y4 y4Var) {
        if ((y4Var.mo17999N() & 32) != 32) {
            m14569b(y4Var);
            return;
        }
        y4Var.f11160D0 = true;
        y4Var.f11175x0 = false;
        y4Var.f11176y0 = false;
        y4Var.f11177z0 = false;
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList = y4Var.f5944k0;
        java.util.List<com.fossil.blesdk.obfuscated.C3413z4> list = y4Var.f11174w0;
        boolean z = y4Var.mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z2 = y4Var.mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (androidx.constraintlayout.solver.widgets.ConstraintWidget next : arrayList) {
            next.f713p = null;
            next.f696d0 = false;
            next.mo1280G();
        }
        for (androidx.constraintlayout.solver.widgets.ConstraintWidget next2 : arrayList) {
            if (next2.f713p == null && !m14568a(next2, list, z3)) {
                m14569b(y4Var);
                y4Var.f11160D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (com.fossil.blesdk.obfuscated.C3413z4 next3 : list) {
            i = java.lang.Math.max(i, m14561a(next3, 0));
            i2 = java.lang.Math.max(i2, m14561a(next3, 1));
        }
        if (z) {
            y4Var.mo1293a(androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED);
            y4Var.mo1345p(i);
            y4Var.f11175x0 = true;
            y4Var.f11176y0 = true;
            y4Var.f11157A0 = i;
        }
        if (z2) {
            y4Var.mo1307b(androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.FIXED);
            y4Var.mo1329h(i2);
            y4Var.f11175x0 = true;
            y4Var.f11177z0 = true;
            y4Var.f11158B0 = i2;
        }
        m14566a(list, 0, y4Var.mo1352t());
        m14566a(list, 1, y4Var.mo1332j());
    }

    @DexIgnore
    /* renamed from: b */
    public static void m14569b(com.fossil.blesdk.obfuscated.C3342y4 y4Var) {
        y4Var.f11174w0.clear();
        y4Var.f11174w0.add(0, new com.fossil.blesdk.obfuscated.C3413z4(y4Var.f5944k0));
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m14568a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, java.util.List<com.fossil.blesdk.obfuscated.C3413z4> list, boolean z) {
        com.fossil.blesdk.obfuscated.C3413z4 z4Var = new com.fossil.blesdk.obfuscated.C3413z4(new java.util.ArrayList(), true);
        list.add(z4Var);
        return m14567a(constraintWidget, z4Var, list, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0181, code lost:
        if (r3.f650b == r4) goto L_0x0183;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0128, code lost:
        if (r3.f650b == r4) goto L_0x012a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x01a0  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01c2  */
    /* renamed from: a */
    public static boolean m14567a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, com.fossil.blesdk.obfuscated.C3413z4 z4Var, java.util.List<com.fossil.blesdk.obfuscated.C3413z4> list, boolean z) {
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor;
        if (constraintWidget == null) {
            return true;
        }
        constraintWidget.f694c0 = false;
        com.fossil.blesdk.obfuscated.C3342y4 y4Var = (com.fossil.blesdk.obfuscated.C3342y4) constraintWidget.mo1336l();
        com.fossil.blesdk.obfuscated.C3413z4 z4Var2 = constraintWidget.f713p;
        if (z4Var2 == null) {
            constraintWidget.f692b0 = true;
            z4Var.f11487a.add(constraintWidget);
            constraintWidget.f713p = z4Var;
            if (constraintWidget.f716s.f652d == null && constraintWidget.f718u.f652d == null && constraintWidget.f717t.f652d == null && constraintWidget.f719v.f652d == null && constraintWidget.f720w.f652d == null && constraintWidget.f723z.f652d == null) {
                m14565a(y4Var, constraintWidget, z4Var);
                if (z) {
                    return false;
                }
            }
            if (!(constraintWidget.f717t.f652d == null || constraintWidget.f719v.f652d == null)) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour r = y4Var.mo1348r();
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    m14565a(y4Var, constraintWidget, z4Var);
                    return false;
                } else if (!(constraintWidget.f717t.f652d.f650b == constraintWidget.mo1336l() && constraintWidget.f719v.f652d.f650b == constraintWidget.mo1336l())) {
                    m14565a(y4Var, constraintWidget, z4Var);
                }
            }
            if (!(constraintWidget.f716s.f652d == null || constraintWidget.f718u.f652d == null)) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour k = y4Var.mo1334k();
                androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (z) {
                    m14565a(y4Var, constraintWidget, z4Var);
                    return false;
                } else if (!(constraintWidget.f716s.f652d.f650b == constraintWidget.mo1336l() && constraintWidget.f718u.f652d.f650b == constraintWidget.mo1336l())) {
                    m14565a(y4Var, constraintWidget, z4Var);
                }
            }
            if (((constraintWidget.mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) ^ (constraintWidget.mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT)) && constraintWidget.f669G != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                m14558a(constraintWidget);
            } else if (constraintWidget.mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || constraintWidget.mo1348r() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                m14565a(y4Var, constraintWidget, z4Var);
                if (z) {
                    return false;
                }
            }
            if (!(constraintWidget.f716s.f652d == null && constraintWidget.f718u.f652d == null)) {
                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintWidget.f716s.f652d;
                if (!(constraintAnchor2 != null && constraintAnchor2.f650b == constraintWidget.f666D && constraintWidget.f718u.f652d == null)) {
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor3 = constraintWidget.f718u.f652d;
                    if (!(constraintAnchor3 != null && constraintAnchor3.f650b == constraintWidget.f666D && constraintWidget.f716s.f652d == null)) {
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor4 = constraintWidget.f716s.f652d;
                        if (constraintAnchor4 != null) {
                            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintAnchor4.f650b;
                            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = constraintWidget.f666D;
                            if (constraintWidget2 == constraintWidget3) {
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor5 = constraintWidget.f718u.f652d;
                                if (constraintAnchor5 != null) {
                                }
                            }
                        }
                        if (!(constraintWidget.f717t.f652d == null && constraintWidget.f719v.f652d == null)) {
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor6 = constraintWidget.f717t.f652d;
                            if (!(constraintAnchor6 != null && constraintAnchor6.f650b == constraintWidget.f666D && constraintWidget.f719v.f652d == null)) {
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor7 = constraintWidget.f719v.f652d;
                                if (!(constraintAnchor7 != null && constraintAnchor7.f650b == constraintWidget.f666D && constraintWidget.f717t.f652d == null)) {
                                    constraintAnchor = constraintWidget.f717t.f652d;
                                    if (constraintAnchor != null) {
                                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4 = constraintAnchor.f650b;
                                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget5 = constraintWidget.f666D;
                                        if (constraintWidget4 == constraintWidget5) {
                                            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor8 = constraintWidget.f719v.f652d;
                                            if (constraintAnchor8 != null) {
                                            }
                                        }
                                    }
                                    if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5) {
                                        m14565a(y4Var, constraintWidget, z4Var);
                                        if (z) {
                                            return false;
                                        }
                                        com.fossil.blesdk.obfuscated.C1464b5 b5Var = (com.fossil.blesdk.obfuscated.C1464b5) constraintWidget;
                                        for (int i = 0; i < b5Var.f3674l0; i++) {
                                            if (!m14567a(b5Var.f3673k0[i], z4Var, list, z)) {
                                                return false;
                                            }
                                        }
                                    }
                                    for (androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor9 : constraintWidget.f663A) {
                                        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor10 = constraintAnchor9.f652d;
                                        if (!(constraintAnchor10 == null || constraintAnchor10.f650b == constraintWidget.mo1336l())) {
                                            if (constraintAnchor9.f651c == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.CENTER) {
                                                m14565a(y4Var, constraintWidget, z4Var);
                                                if (z) {
                                                    return false;
                                                }
                                            } else {
                                                m14562a(constraintAnchor9);
                                            }
                                            if (!m14567a(constraintAnchor9.f652d.f650b, z4Var, list, z)) {
                                                return false;
                                            }
                                        }
                                    }
                                    return true;
                                }
                            }
                        }
                        if (constraintWidget.f723z.f652d == null && constraintWidget.f720w.f652d == null && !(constraintWidget instanceof com.fossil.blesdk.obfuscated.C1382a5) && !(constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5)) {
                            z4Var.f11493g.add(constraintWidget);
                        }
                        if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5) {
                        }
                        while (r4 < r3) {
                        }
                        return true;
                    }
                }
            }
            if (constraintWidget.f723z.f652d == null && !(constraintWidget instanceof com.fossil.blesdk.obfuscated.C1382a5) && !(constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5)) {
                z4Var.f11492f.add(constraintWidget);
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor62 = constraintWidget.f717t.f652d;
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor72 = constraintWidget.f719v.f652d;
            constraintAnchor = constraintWidget.f717t.f652d;
            if (constraintAnchor != null) {
            }
            if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5) {
            }
            while (r4 < r3) {
            }
            return true;
        }
        if (z4Var2 != z4Var) {
            z4Var.f11487a.addAll(z4Var2.f11487a);
            z4Var.f11492f.addAll(constraintWidget.f713p.f11492f);
            z4Var.f11493g.addAll(constraintWidget.f713p.f11493g);
            if (!constraintWidget.f713p.f11490d) {
                z4Var.f11490d = false;
            }
            list.remove(constraintWidget.f713p);
            for (androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget6 : constraintWidget.f713p.f11487a) {
                constraintWidget6.f713p = z4Var;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14565a(com.fossil.blesdk.obfuscated.C3342y4 y4Var, androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, com.fossil.blesdk.obfuscated.C3413z4 z4Var) {
        z4Var.f11490d = false;
        y4Var.f11160D0 = false;
        constraintWidget.f692b0 = false;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14561a(com.fossil.blesdk.obfuscated.C3413z4 z4Var, int i) {
        int i2 = i * 2;
        java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> a = z4Var.mo18436a(i);
        int size = a.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = a.get(i4);
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget.f663A;
            int i5 = i2 + 1;
            i3 = java.lang.Math.max(i3, m14560a(constraintWidget, i, constraintAnchorArr[i5].f652d == null || !(constraintAnchorArr[i2].f652d == null || constraintAnchorArr[i5].f652d == null), 0));
        }
        z4Var.f11491e[i] = i3;
        return i3;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14560a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i, boolean z, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int t;
        int i9;
        int i10;
        int i11;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintWidget;
        int i12 = i;
        boolean z2 = z;
        int i13 = 0;
        if (!constraintWidget2.f692b0) {
            return 0;
        }
        boolean z3 = constraintWidget2.f720w.f652d != null && i12 == 1;
        if (z2) {
            i6 = constraintWidget.mo1317d();
            i5 = constraintWidget.mo1332j() - constraintWidget.mo1317d();
            i4 = i12 * 2;
            i3 = i4 + 1;
        } else {
            i6 = constraintWidget.mo1332j() - constraintWidget.mo1317d();
            i5 = constraintWidget.mo1317d();
            i3 = i12 * 2;
            i4 = i3 + 1;
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget2.f663A;
        if (constraintAnchorArr[i3].f652d == null || constraintAnchorArr[i4].f652d != null) {
            i7 = i3;
            i8 = 1;
        } else {
            i7 = i4;
            i4 = i3;
            i8 = -1;
        }
        int i14 = z3 ? i2 - i6 : i2;
        int b = (constraintWidget2.f663A[i4].mo1264b() * i8) + m14559a(constraintWidget, i);
        int i15 = i14 + b;
        int t2 = (i12 == 0 ? constraintWidget.mo1352t() : constraintWidget.mo1332j()) * i8;
        java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it = constraintWidget2.f663A[i4].mo1266d().f5383a.iterator();
        while (it.hasNext()) {
            i13 = java.lang.Math.max(i13, m14560a(((com.fossil.blesdk.obfuscated.C1703e5) it.next()).f4700c.f650b, i12, z2, i15));
        }
        int i16 = 0;
        for (java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it2 = constraintWidget2.f663A[i7].mo1266d().f5383a.iterator(); it2.hasNext(); it2 = it2) {
            i16 = java.lang.Math.max(i16, m14560a(((com.fossil.blesdk.obfuscated.C1703e5) it2.next()).f4700c.f650b, i12, z2, t2 + i15));
        }
        if (z3) {
            i13 -= i6;
            t = i16 + i5;
        } else {
            t = i16 + ((i12 == 0 ? constraintWidget.mo1352t() : constraintWidget.mo1332j()) * i8);
        }
        int i17 = 1;
        if (i12 == 1) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it3 = constraintWidget2.f720w.mo1266d().f5383a.iterator();
            int i18 = 0;
            while (it3.hasNext()) {
                java.util.Iterator<com.fossil.blesdk.obfuscated.C1857g5> it4 = it3;
                com.fossil.blesdk.obfuscated.C1703e5 e5Var = (com.fossil.blesdk.obfuscated.C1703e5) it3.next();
                if (i8 == i17) {
                    i18 = java.lang.Math.max(i18, m14560a(e5Var.f4700c.f650b, i12, z2, i6 + i15));
                    i11 = i7;
                } else {
                    i11 = i7;
                    i18 = java.lang.Math.max(i18, m14560a(e5Var.f4700c.f650b, i12, z2, (i5 * i8) + i15));
                }
                it3 = it4;
                i7 = i11;
                i17 = 1;
            }
            i9 = i7;
            int i19 = i18;
            i10 = (constraintWidget2.f720w.mo1266d().f5383a.size() <= 0 || z3) ? i19 : i8 == 1 ? i19 + i6 : i19 - i5;
        } else {
            i9 = i7;
            i10 = 0;
        }
        int max = b + java.lang.Math.max(i13, java.lang.Math.max(t, i10));
        int i20 = i15 + t2;
        if (i8 != -1) {
            int i21 = i15;
            i15 = i20;
            i20 = i21;
        }
        if (z2) {
            com.fossil.blesdk.obfuscated.C1536c5.m5284a(constraintWidget2, i12, i20);
            constraintWidget2.mo1289a(i20, i15, i12);
        } else {
            constraintWidget2.f713p.mo18438a(constraintWidget2, i12);
            constraintWidget2.mo1320d(i20, i12);
        }
        if (constraintWidget.mo1312c(i) == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && constraintWidget2.f669G != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            constraintWidget2.f713p.mo18438a(constraintWidget2, i12);
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr2 = constraintWidget2.f663A;
        if (!(constraintAnchorArr2[i4].f652d == null || constraintAnchorArr2[i9].f652d == null)) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget l = constraintWidget.mo1336l();
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr3 = constraintWidget2.f663A;
            if (constraintAnchorArr3[i4].f652d.f650b == l && constraintAnchorArr3[i9].f652d.f650b == l) {
                constraintWidget2.f713p.mo18438a(constraintWidget2, i12);
            }
        }
        return max;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14562a(androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor) {
        com.fossil.blesdk.obfuscated.C1703e5 d = constraintAnchor.mo1266d();
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintAnchor.f652d;
        if (constraintAnchor2 != null && constraintAnchor2.f652d != constraintAnchor) {
            constraintAnchor2.mo1266d().mo11138a(d);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14566a(java.util.List<com.fossil.blesdk.obfuscated.C3413z4> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (androidx.constraintlayout.solver.widgets.ConstraintWidget next : list.get(i3).mo18440b(i)) {
                if (next.f692b0) {
                    m14563a(next, i, i2);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14563a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i, int i2) {
        int i3 = i * 2;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget.f663A;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintAnchorArr[i3];
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i3 + 1];
        if ((constraintAnchor.f652d == null || constraintAnchor2.f652d == null) ? false : true) {
            com.fossil.blesdk.obfuscated.C1536c5.m5284a(constraintWidget, i, m14559a(constraintWidget, i) + constraintAnchor.mo1264b());
        } else if (constraintWidget.f669G == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || constraintWidget.mo1312c(i) != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            int e = i2 - constraintWidget.mo1322e(i);
            int d = e - constraintWidget.mo1318d(i);
            constraintWidget.mo1289a(d, e, i);
            com.fossil.blesdk.obfuscated.C1536c5.m5284a(constraintWidget, i, d);
        } else {
            int a = m14558a(constraintWidget);
            int i4 = (int) constraintWidget.f663A[i3].mo1266d().f4704g;
            constraintAnchor2.mo1266d().f4703f = constraintAnchor.mo1266d();
            constraintAnchor2.mo1266d().f4704g = (float) a;
            constraintAnchor2.mo1266d().f5384b = 1;
            constraintWidget.mo1289a(i4, i4 + a, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14559a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i) {
        int i2 = i * 2;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget.f663A;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor = constraintAnchorArr[i2];
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintAnchorArr[i2 + 1];
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor3 = constraintAnchor.f652d;
        if (constraintAnchor3 == null) {
            return 0;
        }
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintAnchor3.f650b;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = constraintWidget.f666D;
        if (constraintWidget2 != constraintWidget3) {
            return 0;
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor4 = constraintAnchor2.f652d;
        if (constraintAnchor4 == null || constraintAnchor4.f650b != constraintWidget3) {
            return 0;
        }
        return (int) (((float) (((constraintWidget3.mo1318d(i) - constraintAnchor.mo1264b()) - constraintAnchor2.mo1264b()) - constraintWidget.mo1318d(i))) * (i == 0 ? constraintWidget.f684V : constraintWidget.f685W));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m14558a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        float f;
        float f2;
        if (constraintWidget.mo1334k() == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            if (constraintWidget.f670H == 0) {
                f2 = ((float) constraintWidget.mo1332j()) * constraintWidget.f669G;
            } else {
                f2 = ((float) constraintWidget.mo1332j()) / constraintWidget.f669G;
            }
            int i = (int) f2;
            constraintWidget.mo1345p(i);
            return i;
        } else if (constraintWidget.mo1348r() != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (constraintWidget.f670H == 1) {
                f = ((float) constraintWidget.mo1352t()) * constraintWidget.f669G;
            } else {
                f = ((float) constraintWidget.mo1352t()) / constraintWidget.f669G;
            }
            int i2 = (int) f;
            constraintWidget.mo1329h(i2);
            return i2;
        }
    }
}
