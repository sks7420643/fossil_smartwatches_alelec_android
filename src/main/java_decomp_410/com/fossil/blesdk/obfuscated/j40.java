package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface j40 {
    @DexIgnore
    f90<DeviceConfigKey[]> a(DeviceConfigItem[] deviceConfigItemArr);
}
