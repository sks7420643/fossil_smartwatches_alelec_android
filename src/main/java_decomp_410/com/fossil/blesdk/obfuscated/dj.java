package com.fossil.blesdk.obfuscated;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dj {
    @DexIgnore
    public static dj a; // = null;
    @DexIgnore
    public static /* final */ int b; // = 20;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends dj {
        @DexIgnore
        public int c;

        @DexIgnore
        public a(int i) {
            super(i);
            this.c = i;
        }

        @DexIgnore
        public void a(String str, String str2, Throwable... thArr) {
            if (this.c > 3) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.d(str, str2);
            } else {
                Log.d(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        public void b(String str, String str2, Throwable... thArr) {
            if (this.c > 6) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.e(str, str2);
            } else {
                Log.e(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        public void c(String str, String str2, Throwable... thArr) {
            if (this.c > 4) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.i(str, str2);
            } else {
                Log.i(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        public void d(String str, String str2, Throwable... thArr) {
            if (this.c > 2) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.v(str, str2);
            } else {
                Log.v(str, str2, thArr[0]);
            }
        }

        @DexIgnore
        public void e(String str, String str2, Throwable... thArr) {
            if (this.c > 5) {
                return;
            }
            if (thArr == null || thArr.length < 1) {
                Log.w(str, str2);
            } else {
                Log.w(str, str2, thArr[0]);
            }
        }
    }

    @DexIgnore
    public dj(int i) {
    }

    @DexIgnore
    public static synchronized void a(dj djVar) {
        synchronized (dj.class) {
            a = djVar;
        }
    }

    @DexIgnore
    public abstract void a(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void b(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void c(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void d(String str, String str2, Throwable... thArr);

    @DexIgnore
    public abstract void e(String str, String str2, Throwable... thArr);

    @DexIgnore
    public static String a(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder(23);
        sb.append("WM-");
        int i = b;
        if (length >= i) {
            sb.append(str.substring(0, i));
        } else {
            sb.append(str);
        }
        return sb.toString();
    }

    @DexIgnore
    public static synchronized dj a() {
        dj djVar;
        synchronized (dj.class) {
            if (a == null) {
                a = new a(3);
            }
            djVar = a;
        }
        return djVar;
    }
}
