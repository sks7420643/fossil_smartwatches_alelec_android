package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a0 */
public final class C1368a0 {
    @DexIgnore
    public static /* final */ int[] ActionBar; // = {com.fossil.wearables.fossil.R.attr.background, com.fossil.wearables.fossil.R.attr.backgroundSplit, com.fossil.wearables.fossil.R.attr.backgroundStacked, com.fossil.wearables.fossil.R.attr.contentInsetEnd, com.fossil.wearables.fossil.R.attr.contentInsetEndWithActions, com.fossil.wearables.fossil.R.attr.contentInsetLeft, com.fossil.wearables.fossil.R.attr.contentInsetRight, com.fossil.wearables.fossil.R.attr.contentInsetStart, com.fossil.wearables.fossil.R.attr.contentInsetStartWithNavigation, com.fossil.wearables.fossil.R.attr.customNavigationLayout, com.fossil.wearables.fossil.R.attr.displayOptions, com.fossil.wearables.fossil.R.attr.divider, com.fossil.wearables.fossil.R.attr.elevation, com.fossil.wearables.fossil.R.attr.height, com.fossil.wearables.fossil.R.attr.hideOnContentScroll, com.fossil.wearables.fossil.R.attr.homeAsUpIndicator, com.fossil.wearables.fossil.R.attr.homeLayout, com.fossil.wearables.fossil.R.attr.icon, com.fossil.wearables.fossil.R.attr.indeterminateProgressStyle, com.fossil.wearables.fossil.R.attr.itemPadding, com.fossil.wearables.fossil.R.attr.logo, com.fossil.wearables.fossil.R.attr.navigationMode, com.fossil.wearables.fossil.R.attr.popupTheme, com.fossil.wearables.fossil.R.attr.progressBarPadding, com.fossil.wearables.fossil.R.attr.progressBarStyle, com.fossil.wearables.fossil.R.attr.subtitle, com.fossil.wearables.fossil.R.attr.subtitleTextStyle, com.fossil.wearables.fossil.R.attr.title, com.fossil.wearables.fossil.R.attr.titleTextStyle};
    @DexIgnore
    public static /* final */ int[] ActionBarLayout; // = {16842931};
    @DexIgnore
    public static /* final */ int ActionBarLayout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionBar_backgroundStacked; // = 2;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEnd; // = 3;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetEndWithActions; // = 4;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetLeft; // = 5;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetRight; // = 6;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStart; // = 7;
    @DexIgnore
    public static /* final */ int ActionBar_contentInsetStartWithNavigation; // = 8;
    @DexIgnore
    public static /* final */ int ActionBar_customNavigationLayout; // = 9;
    @DexIgnore
    public static /* final */ int ActionBar_displayOptions; // = 10;
    @DexIgnore
    public static /* final */ int ActionBar_divider; // = 11;
    @DexIgnore
    public static /* final */ int ActionBar_elevation; // = 12;
    @DexIgnore
    public static /* final */ int ActionBar_height; // = 13;
    @DexIgnore
    public static /* final */ int ActionBar_hideOnContentScroll; // = 14;
    @DexIgnore
    public static /* final */ int ActionBar_homeAsUpIndicator; // = 15;
    @DexIgnore
    public static /* final */ int ActionBar_homeLayout; // = 16;
    @DexIgnore
    public static /* final */ int ActionBar_icon; // = 17;
    @DexIgnore
    public static /* final */ int ActionBar_indeterminateProgressStyle; // = 18;
    @DexIgnore
    public static /* final */ int ActionBar_itemPadding; // = 19;
    @DexIgnore
    public static /* final */ int ActionBar_logo; // = 20;
    @DexIgnore
    public static /* final */ int ActionBar_navigationMode; // = 21;
    @DexIgnore
    public static /* final */ int ActionBar_popupTheme; // = 22;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarPadding; // = 23;
    @DexIgnore
    public static /* final */ int ActionBar_progressBarStyle; // = 24;
    @DexIgnore
    public static /* final */ int ActionBar_subtitle; // = 25;
    @DexIgnore
    public static /* final */ int ActionBar_subtitleTextStyle; // = 26;
    @DexIgnore
    public static /* final */ int ActionBar_title; // = 27;
    @DexIgnore
    public static /* final */ int ActionBar_titleTextStyle; // = 28;
    @DexIgnore
    public static /* final */ int[] ActionMenuItemView; // = {16843071};
    @DexIgnore
    public static /* final */ int ActionMenuItemView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int[] ActionMenuView; // = new int[0];
    @DexIgnore
    public static /* final */ int[] ActionMode; // = {com.fossil.wearables.fossil.R.attr.background, com.fossil.wearables.fossil.R.attr.backgroundSplit, com.fossil.wearables.fossil.R.attr.closeItemLayout, com.fossil.wearables.fossil.R.attr.height, com.fossil.wearables.fossil.R.attr.subtitleTextStyle, com.fossil.wearables.fossil.R.attr.titleTextStyle};
    @DexIgnore
    public static /* final */ int ActionMode_background; // = 0;
    @DexIgnore
    public static /* final */ int ActionMode_backgroundSplit; // = 1;
    @DexIgnore
    public static /* final */ int ActionMode_closeItemLayout; // = 2;
    @DexIgnore
    public static /* final */ int ActionMode_height; // = 3;
    @DexIgnore
    public static /* final */ int ActionMode_subtitleTextStyle; // = 4;
    @DexIgnore
    public static /* final */ int ActionMode_titleTextStyle; // = 5;
    @DexIgnore
    public static /* final */ int[] ActivityChooserView; // = {com.fossil.wearables.fossil.R.attr.expandActivityOverflowButtonDrawable, com.fossil.wearables.fossil.R.attr.initialActivityCount};
    @DexIgnore
    public static /* final */ int ActivityChooserView_expandActivityOverflowButtonDrawable; // = 0;
    @DexIgnore
    public static /* final */ int ActivityChooserView_initialActivityCount; // = 1;
    @DexIgnore
    public static /* final */ int[] AlertDialog; // = {16842994, com.fossil.wearables.fossil.R.attr.buttonIconDimen, com.fossil.wearables.fossil.R.attr.buttonPanelSideLayout, com.fossil.wearables.fossil.R.attr.listItemLayout, com.fossil.wearables.fossil.R.attr.listLayout, com.fossil.wearables.fossil.R.attr.multiChoiceItemLayout, com.fossil.wearables.fossil.R.attr.showTitle, com.fossil.wearables.fossil.R.attr.singleChoiceItemLayout};
    @DexIgnore
    public static /* final */ int AlertDialog_android_layout; // = 0;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonIconDimen; // = 1;
    @DexIgnore
    public static /* final */ int AlertDialog_buttonPanelSideLayout; // = 2;
    @DexIgnore
    public static /* final */ int AlertDialog_listItemLayout; // = 3;
    @DexIgnore
    public static /* final */ int AlertDialog_listLayout; // = 4;
    @DexIgnore
    public static /* final */ int AlertDialog_multiChoiceItemLayout; // = 5;
    @DexIgnore
    public static /* final */ int AlertDialog_showTitle; // = 6;
    @DexIgnore
    public static /* final */ int AlertDialog_singleChoiceItemLayout; // = 7;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableCompat; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableItem; // = {16842960, 16843161};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_drawable; // = 1;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_id; // = 0;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableTransition; // = {16843161, 16843849, 16843850, 16843851};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_fromId; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_reversible; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_toId; // = 1;
    @DexIgnore
    public static /* final */ int[] AppCompatImageView; // = {16843033, com.fossil.wearables.fossil.R.attr.srcCompat, com.fossil.wearables.fossil.R.attr.tint, com.fossil.wearables.fossil.R.attr.tintMode};
    @DexIgnore
    public static /* final */ int AppCompatImageView_android_src; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatImageView_srcCompat; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatImageView_tintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatSeekBar; // = {16843074, com.fossil.wearables.fossil.R.attr.tickMark, com.fossil.wearables.fossil.R.attr.tickMarkTint, com.fossil.wearables.fossil.R.attr.tickMarkTintMode};
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_android_thumb; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMark; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTint; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatSeekBar_tickMarkTintMode; // = 3;
    @DexIgnore
    public static /* final */ int[] AppCompatTextHelper; // = {16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667};
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableBottom; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableEnd; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableLeft; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableRight; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableStart; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_drawableTop; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextHelper_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int[] AppCompatTextView; // = {16842804, com.fossil.wearables.fossil.R.attr.autoSizeMaxTextSize, com.fossil.wearables.fossil.R.attr.autoSizeMinTextSize, com.fossil.wearables.fossil.R.attr.autoSizePresetSizes, com.fossil.wearables.fossil.R.attr.autoSizeStepGranularity, com.fossil.wearables.fossil.R.attr.autoSizeTextType, com.fossil.wearables.fossil.R.attr.firstBaselineToTopHeight, com.fossil.wearables.fossil.R.attr.fontFamily, com.fossil.wearables.fossil.R.attr.lastBaselineToBottomHeight, com.fossil.wearables.fossil.R.attr.lineHeight, com.fossil.wearables.fossil.R.attr.textAllCaps};
    @DexIgnore
    public static /* final */ int AppCompatTextView_android_textAppearance; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMaxTextSize; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeMinTextSize; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizePresetSizes; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeStepGranularity; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTextView_autoSizeTextType; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTextView_firstBaselineToTopHeight; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTextView_fontFamily; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lastBaselineToBottomHeight; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTextView_lineHeight; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTextView_textAllCaps; // = 10;
    @DexIgnore
    public static /* final */ int[] AppCompatTheme; // = {16842839, 16842926, com.fossil.wearables.fossil.R.attr.actionBarDivider, com.fossil.wearables.fossil.R.attr.actionBarItemBackground, com.fossil.wearables.fossil.R.attr.actionBarPopupTheme, com.fossil.wearables.fossil.R.attr.actionBarSize, com.fossil.wearables.fossil.R.attr.actionBarSplitStyle, com.fossil.wearables.fossil.R.attr.actionBarStyle, com.fossil.wearables.fossil.R.attr.actionBarTabBarStyle, com.fossil.wearables.fossil.R.attr.actionBarTabStyle, com.fossil.wearables.fossil.R.attr.actionBarTabTextStyle, com.fossil.wearables.fossil.R.attr.actionBarTheme, com.fossil.wearables.fossil.R.attr.actionBarWidgetTheme, com.fossil.wearables.fossil.R.attr.actionButtonStyle, com.fossil.wearables.fossil.R.attr.actionDropDownStyle, com.fossil.wearables.fossil.R.attr.actionMenuTextAppearance, com.fossil.wearables.fossil.R.attr.actionMenuTextColor, com.fossil.wearables.fossil.R.attr.actionModeBackground, com.fossil.wearables.fossil.R.attr.actionModeCloseButtonStyle, com.fossil.wearables.fossil.R.attr.actionModeCloseDrawable, com.fossil.wearables.fossil.R.attr.actionModeCopyDrawable, com.fossil.wearables.fossil.R.attr.actionModeCutDrawable, com.fossil.wearables.fossil.R.attr.actionModeFindDrawable, com.fossil.wearables.fossil.R.attr.actionModePasteDrawable, com.fossil.wearables.fossil.R.attr.actionModePopupWindowStyle, com.fossil.wearables.fossil.R.attr.actionModeSelectAllDrawable, com.fossil.wearables.fossil.R.attr.actionModeShareDrawable, com.fossil.wearables.fossil.R.attr.actionModeSplitBackground, com.fossil.wearables.fossil.R.attr.actionModeStyle, com.fossil.wearables.fossil.R.attr.actionModeWebSearchDrawable, com.fossil.wearables.fossil.R.attr.actionOverflowButtonStyle, com.fossil.wearables.fossil.R.attr.actionOverflowMenuStyle, com.fossil.wearables.fossil.R.attr.activityChooserViewStyle, com.fossil.wearables.fossil.R.attr.alertDialogButtonGroupStyle, com.fossil.wearables.fossil.R.attr.alertDialogCenterButtons, com.fossil.wearables.fossil.R.attr.alertDialogStyle, com.fossil.wearables.fossil.R.attr.alertDialogTheme, com.fossil.wearables.fossil.R.attr.autoCompleteTextViewStyle, com.fossil.wearables.fossil.R.attr.borderlessButtonStyle, com.fossil.wearables.fossil.R.attr.buttonBarButtonStyle, com.fossil.wearables.fossil.R.attr.buttonBarNegativeButtonStyle, com.fossil.wearables.fossil.R.attr.buttonBarNeutralButtonStyle, com.fossil.wearables.fossil.R.attr.buttonBarPositiveButtonStyle, com.fossil.wearables.fossil.R.attr.buttonBarStyle, com.fossil.wearables.fossil.R.attr.buttonStyle, com.fossil.wearables.fossil.R.attr.buttonStyleSmall, com.fossil.wearables.fossil.R.attr.checkboxStyle, com.fossil.wearables.fossil.R.attr.checkedTextViewStyle, com.fossil.wearables.fossil.R.attr.colorAccent, com.fossil.wearables.fossil.R.attr.colorBackgroundFloating, com.fossil.wearables.fossil.R.attr.colorButtonNormal, com.fossil.wearables.fossil.R.attr.colorControlActivated, com.fossil.wearables.fossil.R.attr.colorControlHighlight, com.fossil.wearables.fossil.R.attr.colorControlNormal, com.fossil.wearables.fossil.R.attr.colorError, com.fossil.wearables.fossil.R.attr.colorPrimary, com.fossil.wearables.fossil.R.attr.colorPrimaryDark, com.fossil.wearables.fossil.R.attr.colorSwitchThumbNormal, com.fossil.wearables.fossil.R.attr.controlBackground, com.fossil.wearables.fossil.R.attr.dialogCornerRadius, com.fossil.wearables.fossil.R.attr.dialogPreferredPadding, com.fossil.wearables.fossil.R.attr.dialogTheme, com.fossil.wearables.fossil.R.attr.dividerHorizontal, com.fossil.wearables.fossil.R.attr.dividerVertical, com.fossil.wearables.fossil.R.attr.dropDownListViewStyle, com.fossil.wearables.fossil.R.attr.dropdownListPreferredItemHeight, com.fossil.wearables.fossil.R.attr.editTextBackground, com.fossil.wearables.fossil.R.attr.editTextColor, com.fossil.wearables.fossil.R.attr.editTextStyle, com.fossil.wearables.fossil.R.attr.homeAsUpIndicator, com.fossil.wearables.fossil.R.attr.imageButtonStyle, com.fossil.wearables.fossil.R.attr.listChoiceBackgroundIndicator, com.fossil.wearables.fossil.R.attr.listDividerAlertDialog, com.fossil.wearables.fossil.R.attr.listMenuViewStyle, com.fossil.wearables.fossil.R.attr.listPopupWindowStyle, com.fossil.wearables.fossil.R.attr.listPreferredItemHeight, com.fossil.wearables.fossil.R.attr.listPreferredItemHeightLarge, com.fossil.wearables.fossil.R.attr.listPreferredItemHeightSmall, com.fossil.wearables.fossil.R.attr.listPreferredItemPaddingLeft, com.fossil.wearables.fossil.R.attr.listPreferredItemPaddingRight, com.fossil.wearables.fossil.R.attr.panelBackground, com.fossil.wearables.fossil.R.attr.panelMenuListTheme, com.fossil.wearables.fossil.R.attr.panelMenuListWidth, com.fossil.wearables.fossil.R.attr.popupMenuStyle, com.fossil.wearables.fossil.R.attr.popupWindowStyle, com.fossil.wearables.fossil.R.attr.radioButtonStyle, com.fossil.wearables.fossil.R.attr.ratingBarStyle, com.fossil.wearables.fossil.R.attr.ratingBarStyleIndicator, com.fossil.wearables.fossil.R.attr.ratingBarStyleSmall, com.fossil.wearables.fossil.R.attr.searchViewStyle, com.fossil.wearables.fossil.R.attr.seekBarStyle, com.fossil.wearables.fossil.R.attr.selectableItemBackground, com.fossil.wearables.fossil.R.attr.selectableItemBackgroundBorderless, com.fossil.wearables.fossil.R.attr.spinnerDropDownItemStyle, com.fossil.wearables.fossil.R.attr.spinnerStyle, com.fossil.wearables.fossil.R.attr.switchStyle, com.fossil.wearables.fossil.R.attr.textAppearanceLargePopupMenu, com.fossil.wearables.fossil.R.attr.textAppearanceListItem, com.fossil.wearables.fossil.R.attr.textAppearanceListItemSecondary, com.fossil.wearables.fossil.R.attr.textAppearanceListItemSmall, com.fossil.wearables.fossil.R.attr.textAppearancePopupMenuHeader, com.fossil.wearables.fossil.R.attr.textAppearanceSearchResultSubtitle, com.fossil.wearables.fossil.R.attr.textAppearanceSearchResultTitle, com.fossil.wearables.fossil.R.attr.textAppearanceSmallPopupMenu, com.fossil.wearables.fossil.R.attr.textColorAlertDialogListItem, com.fossil.wearables.fossil.R.attr.textColorSearchUrl, com.fossil.wearables.fossil.R.attr.toolbarNavigationButtonStyle, com.fossil.wearables.fossil.R.attr.toolbarStyle, com.fossil.wearables.fossil.R.attr.tooltipForegroundColor, com.fossil.wearables.fossil.R.attr.tooltipFrameBackground, com.fossil.wearables.fossil.R.attr.viewInflaterClass, com.fossil.wearables.fossil.R.attr.windowActionBar, com.fossil.wearables.fossil.R.attr.windowActionBarOverlay, com.fossil.wearables.fossil.R.attr.windowActionModeOverlay, com.fossil.wearables.fossil.R.attr.windowFixedHeightMajor, com.fossil.wearables.fossil.R.attr.windowFixedHeightMinor, com.fossil.wearables.fossil.R.attr.windowFixedWidthMajor, com.fossil.wearables.fossil.R.attr.windowFixedWidthMinor, com.fossil.wearables.fossil.R.attr.windowMinWidthMajor, com.fossil.wearables.fossil.R.attr.windowMinWidthMinor, com.fossil.wearables.fossil.R.attr.windowNoTitle};
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarDivider; // = 2;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarItemBackground; // = 3;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarPopupTheme; // = 4;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSize; // = 5;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarSplitStyle; // = 6;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarStyle; // = 7;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabBarStyle; // = 8;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabStyle; // = 9;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTabTextStyle; // = 10;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarTheme; // = 11;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionBarWidgetTheme; // = 12;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionButtonStyle; // = 13;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionDropDownStyle; // = 14;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextAppearance; // = 15;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionMenuTextColor; // = 16;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeBackground; // = 17;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseButtonStyle; // = 18;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCloseDrawable; // = 19;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCopyDrawable; // = 20;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeCutDrawable; // = 21;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeFindDrawable; // = 22;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePasteDrawable; // = 23;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModePopupWindowStyle; // = 24;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSelectAllDrawable; // = 25;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeShareDrawable; // = 26;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeSplitBackground; // = 27;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeStyle; // = 28;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionModeWebSearchDrawable; // = 29;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowButtonStyle; // = 30;
    @DexIgnore
    public static /* final */ int AppCompatTheme_actionOverflowMenuStyle; // = 31;
    @DexIgnore
    public static /* final */ int AppCompatTheme_activityChooserViewStyle; // = 32;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogButtonGroupStyle; // = 33;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogCenterButtons; // = 34;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogStyle; // = 35;
    @DexIgnore
    public static /* final */ int AppCompatTheme_alertDialogTheme; // = 36;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int AppCompatTheme_android_windowIsFloating; // = 0;
    @DexIgnore
    public static /* final */ int AppCompatTheme_autoCompleteTextViewStyle; // = 37;
    @DexIgnore
    public static /* final */ int AppCompatTheme_borderlessButtonStyle; // = 38;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarButtonStyle; // = 39;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNegativeButtonStyle; // = 40;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarNeutralButtonStyle; // = 41;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarPositiveButtonStyle; // = 42;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonBarStyle; // = 43;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyle; // = 44;
    @DexIgnore
    public static /* final */ int AppCompatTheme_buttonStyleSmall; // = 45;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkboxStyle; // = 46;
    @DexIgnore
    public static /* final */ int AppCompatTheme_checkedTextViewStyle; // = 47;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorAccent; // = 48;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorBackgroundFloating; // = 49;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorButtonNormal; // = 50;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlActivated; // = 51;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlHighlight; // = 52;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorControlNormal; // = 53;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorError; // = 54;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimary; // = 55;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorPrimaryDark; // = 56;
    @DexIgnore
    public static /* final */ int AppCompatTheme_colorSwitchThumbNormal; // = 57;
    @DexIgnore
    public static /* final */ int AppCompatTheme_controlBackground; // = 58;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogCornerRadius; // = 59;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogPreferredPadding; // = 60;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dialogTheme; // = 61;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerHorizontal; // = 62;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dividerVertical; // = 63;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropDownListViewStyle; // = 64;
    @DexIgnore
    public static /* final */ int AppCompatTheme_dropdownListPreferredItemHeight; // = 65;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextBackground; // = 66;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextColor; // = 67;
    @DexIgnore
    public static /* final */ int AppCompatTheme_editTextStyle; // = 68;
    @DexIgnore
    public static /* final */ int AppCompatTheme_homeAsUpIndicator; // = 69;
    @DexIgnore
    public static /* final */ int AppCompatTheme_imageButtonStyle; // = 70;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listChoiceBackgroundIndicator; // = 71;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listDividerAlertDialog; // = 72;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listMenuViewStyle; // = 73;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPopupWindowStyle; // = 74;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeight; // = 75;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightLarge; // = 76;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemHeightSmall; // = 77;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingLeft; // = 78;
    @DexIgnore
    public static /* final */ int AppCompatTheme_listPreferredItemPaddingRight; // = 79;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelBackground; // = 80;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListTheme; // = 81;
    @DexIgnore
    public static /* final */ int AppCompatTheme_panelMenuListWidth; // = 82;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupMenuStyle; // = 83;
    @DexIgnore
    public static /* final */ int AppCompatTheme_popupWindowStyle; // = 84;
    @DexIgnore
    public static /* final */ int AppCompatTheme_radioButtonStyle; // = 85;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyle; // = 86;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleIndicator; // = 87;
    @DexIgnore
    public static /* final */ int AppCompatTheme_ratingBarStyleSmall; // = 88;
    @DexIgnore
    public static /* final */ int AppCompatTheme_searchViewStyle; // = 89;
    @DexIgnore
    public static /* final */ int AppCompatTheme_seekBarStyle; // = 90;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackground; // = 91;
    @DexIgnore
    public static /* final */ int AppCompatTheme_selectableItemBackgroundBorderless; // = 92;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerDropDownItemStyle; // = 93;
    @DexIgnore
    public static /* final */ int AppCompatTheme_spinnerStyle; // = 94;
    @DexIgnore
    public static /* final */ int AppCompatTheme_switchStyle; // = 95;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceLargePopupMenu; // = 96;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItem; // = 97;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSecondary; // = 98;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceListItemSmall; // = 99;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearancePopupMenuHeader; // = 100;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultSubtitle; // = 101;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSearchResultTitle; // = 102;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textAppearanceSmallPopupMenu; // = 103;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorAlertDialogListItem; // = 104;
    @DexIgnore
    public static /* final */ int AppCompatTheme_textColorSearchUrl; // = 105;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarNavigationButtonStyle; // = 106;
    @DexIgnore
    public static /* final */ int AppCompatTheme_toolbarStyle; // = 107;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipForegroundColor; // = 108;
    @DexIgnore
    public static /* final */ int AppCompatTheme_tooltipFrameBackground; // = 109;
    @DexIgnore
    public static /* final */ int AppCompatTheme_viewInflaterClass; // = 110;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBar; // = 111;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionBarOverlay; // = 112;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowActionModeOverlay; // = 113;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMajor; // = 114;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedHeightMinor; // = 115;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMajor; // = 116;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowFixedWidthMinor; // = 117;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMajor; // = 118;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowMinWidthMinor; // = 119;
    @DexIgnore
    public static /* final */ int AppCompatTheme_windowNoTitle; // = 120;
    @DexIgnore
    public static /* final */ int[] ButtonBarLayout; // = {com.fossil.wearables.fossil.R.attr.allowStacking};
    @DexIgnore
    public static /* final */ int ButtonBarLayout_allowStacking; // = 0;
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, com.fossil.wearables.fossil.R.attr.alpha};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] CompoundButton; // = {16843015, com.fossil.wearables.fossil.R.attr.buttonTint, com.fossil.wearables.fossil.R.attr.buttonTintMode};
    @DexIgnore
    public static /* final */ int CompoundButton_android_button; // = 0;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTint; // = 1;
    @DexIgnore
    public static /* final */ int CompoundButton_buttonTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout; // = {com.fossil.wearables.fossil.R.attr.keylines, com.fossil.wearables.fossil.R.attr.statusBarBackground};
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout_Layout; // = {16842931, com.fossil.wearables.fossil.R.attr.layout_anchor, com.fossil.wearables.fossil.R.attr.layout_anchorGravity, com.fossil.wearables.fossil.R.attr.layout_behavior, com.fossil.wearables.fossil.R.attr.layout_dodgeInsetEdges, com.fossil.wearables.fossil.R.attr.layout_insetEdge, com.fossil.wearables.fossil.R.attr.layout_keyline};
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchor; // = 1;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchorGravity; // = 2;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_behavior; // = 3;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_dodgeInsetEdges; // = 4;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_insetEdge; // = 5;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_keyline; // = 6;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_keylines; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_statusBarBackground; // = 1;
    @DexIgnore
    public static /* final */ int[] DrawerArrowToggle; // = {com.fossil.wearables.fossil.R.attr.arrowHeadLength, com.fossil.wearables.fossil.R.attr.arrowShaftLength, com.fossil.wearables.fossil.R.attr.barLength, com.fossil.wearables.fossil.R.attr.color, com.fossil.wearables.fossil.R.attr.drawableSize, com.fossil.wearables.fossil.R.attr.gapBetweenBars, com.fossil.wearables.fossil.R.attr.spinBars, com.fossil.wearables.fossil.R.attr.thickness};
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowHeadLength; // = 0;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_arrowShaftLength; // = 1;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_barLength; // = 2;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_color; // = 3;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_drawableSize; // = 4;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_gapBetweenBars; // = 5;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_spinBars; // = 6;
    @DexIgnore
    public static /* final */ int DrawerArrowToggle_thickness; // = 7;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {com.fossil.wearables.fossil.R.attr.fontProviderAuthority, com.fossil.wearables.fossil.R.attr.fontProviderCerts, com.fossil.wearables.fossil.R.attr.fontProviderFetchStrategy, com.fossil.wearables.fossil.R.attr.fontProviderFetchTimeout, com.fossil.wearables.fossil.R.attr.fontProviderPackage, com.fossil.wearables.fossil.R.attr.fontProviderQuery};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, com.fossil.wearables.fossil.R.attr.font, com.fossil.wearables.fossil.R.attr.fontStyle, com.fossil.wearables.fossil.R.attr.fontVariationSettings, com.fossil.wearables.fossil.R.attr.fontWeight, com.fossil.wearables.fossil.R.attr.ttcIndex};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat; // = {16842927, 16842948, 16843046, 16843047, 16843048, com.fossil.wearables.fossil.R.attr.divider, com.fossil.wearables.fossil.R.attr.dividerPadding, com.fossil.wearables.fossil.R.attr.measureWithLargestChild, com.fossil.wearables.fossil.R.attr.showDividers};
    @DexIgnore
    public static /* final */ int[] LinearLayoutCompat_Layout; // = {16842931, 16842996, 16842997, 16843137};
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_height; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_weight; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_Layout_android_layout_width; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAligned; // = 2;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_baselineAlignedChildIndex; // = 3;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_orientation; // = 1;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_android_weightSum; // = 4;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_divider; // = 5;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_dividerPadding; // = 6;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_measureWithLargestChild; // = 7;
    @DexIgnore
    public static /* final */ int LinearLayoutCompat_showDividers; // = 8;
    @DexIgnore
    public static /* final */ int[] ListPopupWindow; // = {16843436, 16843437};
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownHorizontalOffset; // = 0;
    @DexIgnore
    public static /* final */ int ListPopupWindow_android_dropDownVerticalOffset; // = 1;
    @DexIgnore
    public static /* final */ int[] MenuGroup; // = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    @DexIgnore
    public static /* final */ int MenuGroup_android_checkableBehavior; // = 5;
    @DexIgnore
    public static /* final */ int MenuGroup_android_enabled; // = 0;
    @DexIgnore
    public static /* final */ int MenuGroup_android_id; // = 1;
    @DexIgnore
    public static /* final */ int MenuGroup_android_menuCategory; // = 3;
    @DexIgnore
    public static /* final */ int MenuGroup_android_orderInCategory; // = 4;
    @DexIgnore
    public static /* final */ int MenuGroup_android_visible; // = 2;
    @DexIgnore
    public static /* final */ int[] MenuItem; // = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, com.fossil.wearables.fossil.R.attr.actionLayout, com.fossil.wearables.fossil.R.attr.actionProviderClass, com.fossil.wearables.fossil.R.attr.actionViewClass, com.fossil.wearables.fossil.R.attr.alphabeticModifiers, com.fossil.wearables.fossil.R.attr.contentDescription, com.fossil.wearables.fossil.R.attr.iconTint, com.fossil.wearables.fossil.R.attr.iconTintMode, com.fossil.wearables.fossil.R.attr.numericModifiers, com.fossil.wearables.fossil.R.attr.showAsAction, com.fossil.wearables.fossil.R.attr.tooltipText};
    @DexIgnore
    public static /* final */ int MenuItem_actionLayout; // = 13;
    @DexIgnore
    public static /* final */ int MenuItem_actionProviderClass; // = 14;
    @DexIgnore
    public static /* final */ int MenuItem_actionViewClass; // = 15;
    @DexIgnore
    public static /* final */ int MenuItem_alphabeticModifiers; // = 16;
    @DexIgnore
    public static /* final */ int MenuItem_android_alphabeticShortcut; // = 9;
    @DexIgnore
    public static /* final */ int MenuItem_android_checkable; // = 11;
    @DexIgnore
    public static /* final */ int MenuItem_android_checked; // = 3;
    @DexIgnore
    public static /* final */ int MenuItem_android_enabled; // = 1;
    @DexIgnore
    public static /* final */ int MenuItem_android_icon; // = 0;
    @DexIgnore
    public static /* final */ int MenuItem_android_id; // = 2;
    @DexIgnore
    public static /* final */ int MenuItem_android_menuCategory; // = 5;
    @DexIgnore
    public static /* final */ int MenuItem_android_numericShortcut; // = 10;
    @DexIgnore
    public static /* final */ int MenuItem_android_onClick; // = 12;
    @DexIgnore
    public static /* final */ int MenuItem_android_orderInCategory; // = 6;
    @DexIgnore
    public static /* final */ int MenuItem_android_title; // = 7;
    @DexIgnore
    public static /* final */ int MenuItem_android_titleCondensed; // = 8;
    @DexIgnore
    public static /* final */ int MenuItem_android_visible; // = 4;
    @DexIgnore
    public static /* final */ int MenuItem_contentDescription; // = 17;
    @DexIgnore
    public static /* final */ int MenuItem_iconTint; // = 18;
    @DexIgnore
    public static /* final */ int MenuItem_iconTintMode; // = 19;
    @DexIgnore
    public static /* final */ int MenuItem_numericModifiers; // = 20;
    @DexIgnore
    public static /* final */ int MenuItem_showAsAction; // = 21;
    @DexIgnore
    public static /* final */ int MenuItem_tooltipText; // = 22;
    @DexIgnore
    public static /* final */ int[] MenuView; // = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, com.fossil.wearables.fossil.R.attr.preserveIconSpacing, com.fossil.wearables.fossil.R.attr.subMenuArrow};
    @DexIgnore
    public static /* final */ int MenuView_android_headerBackground; // = 4;
    @DexIgnore
    public static /* final */ int MenuView_android_horizontalDivider; // = 2;
    @DexIgnore
    public static /* final */ int MenuView_android_itemBackground; // = 5;
    @DexIgnore
    public static /* final */ int MenuView_android_itemIconDisabledAlpha; // = 6;
    @DexIgnore
    public static /* final */ int MenuView_android_itemTextAppearance; // = 1;
    @DexIgnore
    public static /* final */ int MenuView_android_verticalDivider; // = 3;
    @DexIgnore
    public static /* final */ int MenuView_android_windowAnimationStyle; // = 0;
    @DexIgnore
    public static /* final */ int MenuView_preserveIconSpacing; // = 7;
    @DexIgnore
    public static /* final */ int MenuView_subMenuArrow; // = 8;
    @DexIgnore
    public static /* final */ int[] PopupWindow; // = {16843126, 16843465, com.fossil.wearables.fossil.R.attr.overlapAnchor};
    @DexIgnore
    public static /* final */ int[] PopupWindowBackgroundState; // = {com.fossil.wearables.fossil.R.attr.state_above_anchor};
    @DexIgnore
    public static /* final */ int PopupWindowBackgroundState_state_above_anchor; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupAnimationStyle; // = 1;
    @DexIgnore
    public static /* final */ int PopupWindow_android_popupBackground; // = 0;
    @DexIgnore
    public static /* final */ int PopupWindow_overlapAnchor; // = 2;
    @DexIgnore
    public static /* final */ int[] RecycleListView; // = {com.fossil.wearables.fossil.R.attr.paddingBottomNoButtons, com.fossil.wearables.fossil.R.attr.paddingTopNoTitle};
    @DexIgnore
    public static /* final */ int RecycleListView_paddingBottomNoButtons; // = 0;
    @DexIgnore
    public static /* final */ int RecycleListView_paddingTopNoTitle; // = 1;
    @DexIgnore
    public static /* final */ int[] SearchView; // = {16842970, 16843039, 16843296, 16843364, com.fossil.wearables.fossil.R.attr.closeIcon, com.fossil.wearables.fossil.R.attr.commitIcon, com.fossil.wearables.fossil.R.attr.defaultQueryHint, com.fossil.wearables.fossil.R.attr.goIcon, com.fossil.wearables.fossil.R.attr.iconifiedByDefault, com.fossil.wearables.fossil.R.attr.layout, com.fossil.wearables.fossil.R.attr.queryBackground, com.fossil.wearables.fossil.R.attr.queryHint, com.fossil.wearables.fossil.R.attr.searchHintIcon, com.fossil.wearables.fossil.R.attr.searchIcon, com.fossil.wearables.fossil.R.attr.submitBackground, com.fossil.wearables.fossil.R.attr.suggestionRowLayout, com.fossil.wearables.fossil.R.attr.voiceIcon};
    @DexIgnore
    public static /* final */ int SearchView_android_focusable; // = 0;
    @DexIgnore
    public static /* final */ int SearchView_android_imeOptions; // = 3;
    @DexIgnore
    public static /* final */ int SearchView_android_inputType; // = 2;
    @DexIgnore
    public static /* final */ int SearchView_android_maxWidth; // = 1;
    @DexIgnore
    public static /* final */ int SearchView_closeIcon; // = 4;
    @DexIgnore
    public static /* final */ int SearchView_commitIcon; // = 5;
    @DexIgnore
    public static /* final */ int SearchView_defaultQueryHint; // = 6;
    @DexIgnore
    public static /* final */ int SearchView_goIcon; // = 7;
    @DexIgnore
    public static /* final */ int SearchView_iconifiedByDefault; // = 8;
    @DexIgnore
    public static /* final */ int SearchView_layout; // = 9;
    @DexIgnore
    public static /* final */ int SearchView_queryBackground; // = 10;
    @DexIgnore
    public static /* final */ int SearchView_queryHint; // = 11;
    @DexIgnore
    public static /* final */ int SearchView_searchHintIcon; // = 12;
    @DexIgnore
    public static /* final */ int SearchView_searchIcon; // = 13;
    @DexIgnore
    public static /* final */ int SearchView_submitBackground; // = 14;
    @DexIgnore
    public static /* final */ int SearchView_suggestionRowLayout; // = 15;
    @DexIgnore
    public static /* final */ int SearchView_voiceIcon; // = 16;
    @DexIgnore
    public static /* final */ int[] Spinner; // = {16842930, 16843126, 16843131, 16843362, com.fossil.wearables.fossil.R.attr.popupTheme};
    @DexIgnore
    public static /* final */ int Spinner_android_dropDownWidth; // = 3;
    @DexIgnore
    public static /* final */ int Spinner_android_entries; // = 0;
    @DexIgnore
    public static /* final */ int Spinner_android_popupBackground; // = 1;
    @DexIgnore
    public static /* final */ int Spinner_android_prompt; // = 2;
    @DexIgnore
    public static /* final */ int Spinner_popupTheme; // = 4;
    @DexIgnore
    public static /* final */ int[] StateListDrawable; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int[] StateListDrawableItem; // = {16843161};
    @DexIgnore
    public static /* final */ int StateListDrawableItem_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] SwitchCompat; // = {16843044, 16843045, 16843074, com.fossil.wearables.fossil.R.attr.showText, com.fossil.wearables.fossil.R.attr.splitTrack, com.fossil.wearables.fossil.R.attr.switchMinWidth, com.fossil.wearables.fossil.R.attr.switchPadding, com.fossil.wearables.fossil.R.attr.switchTextAppearance, com.fossil.wearables.fossil.R.attr.thumbTextPadding, com.fossil.wearables.fossil.R.attr.thumbTint, com.fossil.wearables.fossil.R.attr.thumbTintMode, com.fossil.wearables.fossil.R.attr.track, com.fossil.wearables.fossil.R.attr.trackTint, com.fossil.wearables.fossil.R.attr.trackTintMode};
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOff; // = 1;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_textOn; // = 0;
    @DexIgnore
    public static /* final */ int SwitchCompat_android_thumb; // = 2;
    @DexIgnore
    public static /* final */ int SwitchCompat_showText; // = 3;
    @DexIgnore
    public static /* final */ int SwitchCompat_splitTrack; // = 4;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchMinWidth; // = 5;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchPadding; // = 6;
    @DexIgnore
    public static /* final */ int SwitchCompat_switchTextAppearance; // = 7;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTextPadding; // = 8;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTint; // = 9;
    @DexIgnore
    public static /* final */ int SwitchCompat_thumbTintMode; // = 10;
    @DexIgnore
    public static /* final */ int SwitchCompat_track; // = 11;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTint; // = 12;
    @DexIgnore
    public static /* final */ int SwitchCompat_trackTintMode; // = 13;
    @DexIgnore
    public static /* final */ int[] TextAppearance; // = {16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, com.fossil.wearables.fossil.R.attr.fontFamily, com.fossil.wearables.fossil.R.attr.textAllCaps};
    @DexIgnore
    public static /* final */ int TextAppearance_android_fontFamily; // = 10;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowColor; // = 6;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDx; // = 7;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowDy; // = 8;
    @DexIgnore
    public static /* final */ int TextAppearance_android_shadowRadius; // = 9;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColor; // = 3;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorHint; // = 4;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textColorLink; // = 5;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textSize; // = 0;
    @DexIgnore
    public static /* final */ int TextAppearance_android_textStyle; // = 2;
    @DexIgnore
    public static /* final */ int TextAppearance_android_typeface; // = 1;
    @DexIgnore
    public static /* final */ int TextAppearance_fontFamily; // = 11;
    @DexIgnore
    public static /* final */ int TextAppearance_textAllCaps; // = 12;
    @DexIgnore
    public static /* final */ int[] Toolbar; // = {16842927, 16843072, com.fossil.wearables.fossil.R.attr.buttonGravity, com.fossil.wearables.fossil.R.attr.collapseContentDescription, com.fossil.wearables.fossil.R.attr.collapseIcon, com.fossil.wearables.fossil.R.attr.contentInsetEnd, com.fossil.wearables.fossil.R.attr.contentInsetEndWithActions, com.fossil.wearables.fossil.R.attr.contentInsetLeft, com.fossil.wearables.fossil.R.attr.contentInsetRight, com.fossil.wearables.fossil.R.attr.contentInsetStart, com.fossil.wearables.fossil.R.attr.contentInsetStartWithNavigation, com.fossil.wearables.fossil.R.attr.logo, com.fossil.wearables.fossil.R.attr.logoDescription, com.fossil.wearables.fossil.R.attr.maxButtonHeight, com.fossil.wearables.fossil.R.attr.navigationContentDescription, com.fossil.wearables.fossil.R.attr.navigationIcon, com.fossil.wearables.fossil.R.attr.popupTheme, com.fossil.wearables.fossil.R.attr.subtitle, com.fossil.wearables.fossil.R.attr.subtitleTextAppearance, com.fossil.wearables.fossil.R.attr.subtitleTextColor, com.fossil.wearables.fossil.R.attr.title, com.fossil.wearables.fossil.R.attr.titleMargin, com.fossil.wearables.fossil.R.attr.titleMarginBottom, com.fossil.wearables.fossil.R.attr.titleMarginEnd, com.fossil.wearables.fossil.R.attr.titleMarginStart, com.fossil.wearables.fossil.R.attr.titleMarginTop, com.fossil.wearables.fossil.R.attr.titleMargins, com.fossil.wearables.fossil.R.attr.titleTextAppearance, com.fossil.wearables.fossil.R.attr.titleTextColor};
    @DexIgnore
    public static /* final */ int Toolbar_android_gravity; // = 0;
    @DexIgnore
    public static /* final */ int Toolbar_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int Toolbar_buttonGravity; // = 2;
    @DexIgnore
    public static /* final */ int Toolbar_collapseContentDescription; // = 3;
    @DexIgnore
    public static /* final */ int Toolbar_collapseIcon; // = 4;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEnd; // = 5;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetEndWithActions; // = 6;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetLeft; // = 7;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetRight; // = 8;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStart; // = 9;
    @DexIgnore
    public static /* final */ int Toolbar_contentInsetStartWithNavigation; // = 10;
    @DexIgnore
    public static /* final */ int Toolbar_logo; // = 11;
    @DexIgnore
    public static /* final */ int Toolbar_logoDescription; // = 12;
    @DexIgnore
    public static /* final */ int Toolbar_maxButtonHeight; // = 13;
    @DexIgnore
    public static /* final */ int Toolbar_navigationContentDescription; // = 14;
    @DexIgnore
    public static /* final */ int Toolbar_navigationIcon; // = 15;
    @DexIgnore
    public static /* final */ int Toolbar_popupTheme; // = 16;
    @DexIgnore
    public static /* final */ int Toolbar_subtitle; // = 17;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextAppearance; // = 18;
    @DexIgnore
    public static /* final */ int Toolbar_subtitleTextColor; // = 19;
    @DexIgnore
    public static /* final */ int Toolbar_title; // = 20;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargin; // = 21;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginBottom; // = 22;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginEnd; // = 23;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginStart; // = 24;
    @DexIgnore
    public static /* final */ int Toolbar_titleMarginTop; // = 25;
    @DexIgnore
    public static /* final */ int Toolbar_titleMargins; // = 26;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextAppearance; // = 27;
    @DexIgnore
    public static /* final */ int Toolbar_titleTextColor; // = 28;
    @DexIgnore
    public static /* final */ int[] View; // = {16842752, 16842970, com.fossil.wearables.fossil.R.attr.paddingEnd, com.fossil.wearables.fossil.R.attr.paddingStart, com.fossil.wearables.fossil.R.attr.theme};
    @DexIgnore
    public static /* final */ int[] ViewBackgroundHelper; // = {16842964, com.fossil.wearables.fossil.R.attr.backgroundTint, com.fossil.wearables.fossil.R.attr.backgroundTintMode};
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_android_background; // = 0;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTint; // = 1;
    @DexIgnore
    public static /* final */ int ViewBackgroundHelper_backgroundTintMode; // = 2;
    @DexIgnore
    public static /* final */ int[] ViewStubCompat; // = {16842960, 16842994, 16842995};
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_id; // = 0;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_inflatedId; // = 2;
    @DexIgnore
    public static /* final */ int ViewStubCompat_android_layout; // = 1;
    @DexIgnore
    public static /* final */ int View_android_focusable; // = 1;
    @DexIgnore
    public static /* final */ int View_android_theme; // = 0;
    @DexIgnore
    public static /* final */ int View_paddingEnd; // = 2;
    @DexIgnore
    public static /* final */ int View_paddingStart; // = 3;
    @DexIgnore
    public static /* final */ int View_theme; // = 4;
}
