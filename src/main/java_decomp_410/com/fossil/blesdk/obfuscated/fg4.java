package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg4 {
    @DexIgnore
    public static final void a(dg4<?> dg4, sj4 sj4) {
        kd4.b(dg4, "$this$removeOnCancellation");
        kd4.b(sj4, "node");
        dg4.b((xc4<? super Throwable, qa4>) new ui4(sj4));
    }

    @DexIgnore
    public static final void a(dg4<?> dg4, oh4 oh4) {
        kd4.b(dg4, "$this$disposeOnCancellation");
        kd4.b(oh4, "handle");
        dg4.b((xc4<? super Throwable, qa4>) new ph4(oh4));
    }
}
