package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.op */
public class C2582op<DataType, ResourceType, Transcode> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Class<DataType> f8175a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<? extends com.fossil.blesdk.obfuscated.C2427mo<DataType, ResourceType>> f8176b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1906gu<ResourceType, Transcode> f8177c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> f8178d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f8179e;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.op$a */
    public interface C2583a<ResourceType> {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C1438aq<ResourceType> mo3976a(com.fossil.blesdk.obfuscated.C1438aq<ResourceType> aqVar);
    }

    @DexIgnore
    public C2582op(java.lang.Class<DataType> cls, java.lang.Class<ResourceType> cls2, java.lang.Class<Transcode> cls3, java.util.List<? extends com.fossil.blesdk.obfuscated.C2427mo<DataType, ResourceType>> list, com.fossil.blesdk.obfuscated.C1906gu<ResourceType, Transcode> guVar, com.fossil.blesdk.obfuscated.C1862g8<java.util.List<java.lang.Throwable>> g8Var) {
        this.f8175a = cls;
        this.f8176b = list;
        this.f8177c = guVar;
        this.f8178d = g8Var;
        this.f8179e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<Transcode> mo14465a(com.fossil.blesdk.obfuscated.C2978to<DataType> toVar, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar, com.fossil.blesdk.obfuscated.C2582op.C2583a<ResourceType> aVar) throws com.bumptech.glide.load.engine.GlideException {
        return this.f8177c.mo9632a(aVar.mo3976a(mo14464a(toVar, i, i2, loVar)), loVar);
    }

    @DexIgnore
    public java.lang.String toString() {
        return "DecodePath{ dataClass=" + this.f8175a + ", decoders=" + this.f8176b + ", transcoder=" + this.f8177c + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<ResourceType> mo14464a(com.fossil.blesdk.obfuscated.C2978to<DataType> toVar, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws com.bumptech.glide.load.engine.GlideException {
        java.util.List<java.lang.Throwable> a = this.f8178d.mo11162a();
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
        java.util.List list = a;
        try {
            com.fossil.blesdk.obfuscated.C1438aq<ResourceType> a2 = mo14466a(toVar, i, i2, loVar, (java.util.List<java.lang.Throwable>) list);
            return a2;
        } finally {
            this.f8178d.mo11163a(list);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C1438aq<ResourceType> mo14466a(com.fossil.blesdk.obfuscated.C2978to<DataType> toVar, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar, java.util.List<java.lang.Throwable> list) throws com.bumptech.glide.load.engine.GlideException {
        int size = this.f8176b.size();
        com.fossil.blesdk.obfuscated.C1438aq<ResourceType> aqVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            com.fossil.blesdk.obfuscated.C2427mo moVar = (com.fossil.blesdk.obfuscated.C2427mo) this.f8176b.get(i3);
            try {
                if (moVar.mo9301a(toVar.mo12467b(), loVar)) {
                    aqVar = moVar.mo9299a(toVar.mo12467b(), i, i2, loVar);
                }
            } catch (java.io.IOException | java.lang.OutOfMemoryError | java.lang.RuntimeException e) {
                if (android.util.Log.isLoggable("DecodePath", 2)) {
                    android.util.Log.v("DecodePath", "Failed to decode data for " + moVar, e);
                }
                list.add(e);
            }
            if (aqVar != null) {
                break;
            }
        }
        if (aqVar != null) {
            return aqVar;
        }
        throw new com.bumptech.glide.load.engine.GlideException(this.f8179e, (java.util.List<java.lang.Throwable>) new java.util.ArrayList(list));
    }
}
