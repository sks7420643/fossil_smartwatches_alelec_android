package com.fossil.blesdk.obfuscated;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zb0 implements me0 {
    @DexIgnore
    public Status e;
    @DexIgnore
    public GoogleSignInAccount f;

    @DexIgnore
    public zb0(GoogleSignInAccount googleSignInAccount, Status status) {
        this.f = googleSignInAccount;
        this.e = status;
    }

    @DexIgnore
    public Status G() {
        return this.e;
    }

    @DexIgnore
    public GoogleSignInAccount a() {
        return this.f;
    }

    @DexIgnore
    public boolean b() {
        return this.e.L();
    }
}
