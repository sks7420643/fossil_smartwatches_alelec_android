package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.pt2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nw2 extends as2 implements mw2, ws3.g {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a((fd4) null);
    @DexIgnore
    public lw2 k;
    @DexIgnore
    public tr3<sd2> l;
    @DexIgnore
    public pt2 m;
    @DexIgnore
    public ix2 n;
    @DexIgnore
    public mx2 o;
    @DexIgnore
    public dx2 p;
    @DexIgnore
    public j42 q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return nw2.s;
        }

        @DexIgnore
        public final nw2 b() {
            return new nw2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements pt2.b {
        @DexIgnore
        public /* final */ /* synthetic */ nw2 a;

        @DexIgnore
        public b(nw2 nw2) {
            this.a = nw2;
        }

        @DexIgnore
        public void a() {
            NotificationContactsActivity.C.a(this.a);
        }

        @DexIgnore
        public void a(ContactGroup contactGroup) {
            kd4.b(contactGroup, "contactGroup");
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(childFragmentManager, contactGroup);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nw2 e;

        @DexIgnore
        public c(nw2 nw2) {
            this.e = nw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(nw2.t.a(), "press on button back");
            nw2.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nw2 e;

        @DexIgnore
        public d(nw2 nw2) {
            this.e = nw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            nw2.a(this.e).a(true);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nw2 e;

        @DexIgnore
        public e(nw2 nw2) {
            this.e = nw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            nw2.a(this.e).a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nw2 e;

        @DexIgnore
        public f(nw2 nw2) {
            this.e = nw2;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.C.a(this.e);
        }
    }

    /*
    static {
        String simpleName = nw2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ lw2 a(nw2 nw2) {
        lw2 lw2 = nw2.k;
        if (lw2 != null) {
            return lw2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    public int E() {
        CharSequence charSequence;
        tr3<sd2> tr3 = this.l;
        CharSequence charSequence2 = null;
        if (tr3 != null) {
            sd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    charSequence = flexibleTextView.getText();
                    if (!kd4.a((Object) charSequence, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
                        return 0;
                    }
                    tr3<sd2> tr32 = this.l;
                    if (tr32 != null) {
                        sd2 a3 = tr32.a();
                        if (a3 != null) {
                            FlexibleTextView flexibleTextView2 = a3.r;
                            if (flexibleTextView2 != null) {
                                charSequence2 = flexibleTextView2.getText();
                            }
                        }
                        return kd4.a((Object) charSequence2, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts)) ? 1 : 2;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
            }
            charSequence = null;
            if (!kd4.a((Object) charSequence, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    public int J() {
        CharSequence charSequence;
        tr3<sd2> tr3 = this.l;
        CharSequence charSequence2 = null;
        if (tr3 != null) {
            sd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    charSequence = flexibleTextView.getText();
                    if (!kd4.a((Object) charSequence, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
                        return 0;
                    }
                    tr3<sd2> tr32 = this.l;
                    if (tr32 != null) {
                        sd2 a3 = tr32.a();
                        if (a3 != null) {
                            FlexibleTextView flexibleTextView2 = a3.s;
                            if (flexibleTextView2 != null) {
                                charSequence2 = flexibleTextView2.getText();
                            }
                        }
                        return kd4.a((Object) charSequence2, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__FavoriteContacts)) ? 1 : 2;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
            }
            charSequence = null;
            if (!kd4.a((Object) charSequence, (Object) sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_AllowCalllsFrom_Text__Everyone))) {
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void N() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Window window = activity.getWindow();
            if (window != null) {
                window.clearFlags(16);
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void P() {
        FLogger.INSTANCE.getLocal().d(s, "showNotificationSettingDialog");
        if (isActive()) {
            ix2 ix2 = this.n;
            if (ix2 != null) {
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ix2.show(childFragmentManager, ix2.t.a());
            }
        }
    }

    @DexIgnore
    public List<ContactGroup> Q() {
        pt2 pt2 = this.m;
        if (pt2 != null) {
            return pt2.b();
        }
        kd4.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d(s, "onActivityBackPressed -");
        lw2 lw2 = this.k;
        if (lw2 != null) {
            lw2.i();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void T() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Window window = activity.getWindow();
            if (window != null) {
                window.setFlags(16, 16);
            }
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void i(String str) {
        kd4.b(str, "settingsTypeName");
        tr3<sd2> tr3 = this.l;
        if (tr3 != null) {
            sd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void l(List<ContactGroup> list) {
        kd4.b(list, "mListContactGroup");
        pt2 pt2 = this.m;
        if (pt2 != null) {
            pt2.a(list);
        } else {
            kd4.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void m(String str) {
        kd4.b(str, "settingsTypeName");
        tr3<sd2> tr3 = this.l;
        if (tr3 != null) {
            sd2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        l42 g = PortfolioApp.W.c().g();
        ix2 ix2 = this.n;
        if (ix2 != null) {
            g.a(new kx2(ix2)).a(this);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
                j42 j42 = this.q;
                if (j42 != null) {
                    ic a2 = lc.a((FragmentActivity) notificationCallsAndMessagesActivity, (kc.b) j42).a(dx2.class);
                    kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    this.p = (dx2) a2;
                    lw2 lw2 = this.k;
                    if (lw2 != null) {
                        dx2 dx2 = this.p;
                        if (dx2 != null) {
                            lw2.a(dx2);
                        } else {
                            kd4.d("mNotificationSettingViewModel");
                            throw null;
                        }
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    kd4.d("viewModelFactory");
                    throw null;
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        sd2 sd2 = (sd2) qa.a(layoutInflater, R.layout.fragment_notification_calls_and_messages, viewGroup, false, O0());
        this.n = (ix2) getChildFragmentManager().a(ix2.t.a());
        if (this.n == null) {
            this.n = ix2.t.b();
        }
        sd2.t.setOnClickListener(new c(this));
        sd2.u.setOnClickListener(new d(this));
        sd2.v.setOnClickListener(new e(this));
        sd2.q.setOnClickListener(new f(this));
        pt2 pt2 = new pt2();
        pt2.a((pt2.b) new b(this));
        this.m = pt2;
        RecyclerView recyclerView = sd2.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        pt2 pt22 = this.m;
        if (pt22 != null) {
            recyclerView.setAdapter(pt22);
            recyclerView.setHasFixedSize(true);
            l42 g = PortfolioApp.W.c().g();
            ix2 ix2 = this.n;
            if (ix2 != null) {
                g.a(new kx2(ix2)).a(this);
                this.l = new tr3<>(this, sd2);
                R("call_message_view");
                kd4.a((Object) sd2, "binding");
                return sd2.d();
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        kd4.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        lw2 lw2 = this.k;
        if (lw2 != null) {
            lw2.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        lw2 lw2 = this.k;
        if (lw2 != null) {
            lw2.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(lw2 lw2) {
        kd4.b(lw2, "presenter");
        this.k = lw2;
    }

    @DexIgnore
    public void a(ContactGroup contactGroup) {
        kd4.b(contactGroup, "contactGroup");
        pt2 pt2 = this.m;
        if (pt2 != null) {
            pt2.a(contactGroup);
        } else {
            kd4.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i == R.id.tv_ok) {
                lw2 lw2 = this.k;
                if (lw2 != null) {
                    lw2.h();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i == R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactGroup contactGroup = (ContactGroup) extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE");
                if (contactGroup != null) {
                    lw2 lw22 = this.k;
                    if (lw22 != null) {
                        lw22.a(contactGroup);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
