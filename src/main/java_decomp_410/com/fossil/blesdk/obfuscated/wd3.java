package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wd3 implements Factory<SleepOverviewWeekPresenter> {
    @DexIgnore
    public static SleepOverviewWeekPresenter a(ud3 ud3, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository) {
        return new SleepOverviewWeekPresenter(ud3, userRepository, sleepSummariesRepository);
    }
}
