package com.fossil.blesdk.obfuscated;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eb {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList g;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList h;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList i;

        @DexIgnore
        public a(eb ebVar, int i2, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, ArrayList arrayList4) {
            this.e = i2;
            this.f = arrayList;
            this.g = arrayList2;
            this.h = arrayList3;
            this.i = arrayList4;
        }

        @DexIgnore
        public void run() {
            for (int i2 = 0; i2 < this.e; i2++) {
                f9.a((View) this.f.get(i2), (String) this.g.get(i2));
                f9.a((View) this.h.get(i2), (String) this.i.get(i2));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ Map f;

        @DexIgnore
        public b(eb ebVar, ArrayList arrayList, Map map) {
            this.e = arrayList;
            this.f = map;
        }

        @DexIgnore
        public void run() {
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                View view = (View) this.e.get(i);
                String q = f9.q(view);
                if (q != null) {
                    f9.a(view, eb.a((Map<String, String>) this.f, q));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ Map f;

        @DexIgnore
        public c(eb ebVar, ArrayList arrayList, Map map) {
            this.e = arrayList;
            this.f = map;
        }

        @DexIgnore
        public void run() {
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                View view = (View) this.e.get(i);
                f9.a(view, (String) this.f.get(f9.q(view)));
            }
        }
    }

    @DexIgnore
    public abstract Object a(Object obj, Object obj2, Object obj3);

    @DexIgnore
    public void a(View view, Rect rect) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
    }

    @DexIgnore
    public abstract void a(ViewGroup viewGroup, Object obj);

    @DexIgnore
    public abstract void a(Object obj, Rect rect);

    @DexIgnore
    public abstract void a(Object obj, View view);

    @DexIgnore
    public abstract void a(Object obj, View view, ArrayList<View> arrayList);

    @DexIgnore
    public abstract void a(Object obj, Object obj2, ArrayList<View> arrayList, Object obj3, ArrayList<View> arrayList2, Object obj4, ArrayList<View> arrayList3);

    @DexIgnore
    public abstract void a(Object obj, ArrayList<View> arrayList);

    @DexIgnore
    public abstract void a(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    @DexIgnore
    public abstract boolean a(Object obj);

    @DexIgnore
    public abstract Object b(Object obj);

    @DexIgnore
    public abstract Object b(Object obj, Object obj2, Object obj3);

    @DexIgnore
    public abstract void b(Object obj, View view);

    @DexIgnore
    public abstract void b(Object obj, View view, ArrayList<View> arrayList);

    @DexIgnore
    public abstract void b(Object obj, ArrayList<View> arrayList, ArrayList<View> arrayList2);

    @DexIgnore
    public abstract Object c(Object obj);

    @DexIgnore
    public abstract void c(Object obj, View view);

    @DexIgnore
    public ArrayList<String> a(ArrayList<View> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            View view = arrayList.get(i);
            arrayList2.add(f9.q(view));
            f9.a(view, (String) null);
        }
        return arrayList2;
    }

    @DexIgnore
    public void a(View view, ArrayList<View> arrayList, ArrayList<View> arrayList2, ArrayList<String> arrayList3, Map<String, String> map) {
        int size = arrayList2.size();
        ArrayList arrayList4 = new ArrayList();
        for (int i = 0; i < size; i++) {
            View view2 = arrayList.get(i);
            String q = f9.q(view2);
            arrayList4.add(q);
            if (q != null) {
                f9.a(view2, (String) null);
                String str = map.get(q);
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    } else if (str.equals(arrayList3.get(i2))) {
                        f9.a(arrayList2.get(i2), q);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
        gb.a(view, new a(this, size, arrayList2, arrayList3, arrayList, arrayList4));
    }

    @DexIgnore
    public void a(ArrayList<View> arrayList, View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (h9.a(viewGroup)) {
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                a(arrayList, viewGroup.getChildAt(i));
            }
            return;
        }
        arrayList.add(view);
    }

    @DexIgnore
    public void a(Map<String, View> map, View view) {
        if (view.getVisibility() == 0) {
            String q = f9.q(view);
            if (q != null) {
                map.put(q, view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    a(map, viewGroup.getChildAt(i));
                }
            }
        }
    }

    @DexIgnore
    public void a(View view, ArrayList<View> arrayList, Map<String, String> map) {
        gb.a(view, new b(this, arrayList, map));
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, ArrayList<View> arrayList, Map<String, String> map) {
        gb.a(viewGroup, new c(this, arrayList, map));
    }

    @DexIgnore
    public static void a(List<View> list, View view) {
        int size = list.size();
        if (!a(list, view, size)) {
            list.add(view);
            for (int i = size; i < list.size(); i++) {
                View view2 = list.get(i);
                if (view2 instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view2;
                    int childCount = viewGroup.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        View childAt = viewGroup.getChildAt(i2);
                        if (!a(list, childAt, size)) {
                            list.add(childAt);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public static boolean a(List<View> list, View view, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (list.get(i2) == view) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean a(List list) {
        return list == null || list.isEmpty();
    }

    @DexIgnore
    public static String a(Map<String, String> map, String str) {
        for (Map.Entry next : map.entrySet()) {
            if (str.equals(next.getValue())) {
                return (String) next.getKey();
            }
        }
        return null;
    }
}
