package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.util.UserUtils;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o52 implements Factory<UserUtils> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public o52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static o52 a(n42 n42) {
        return new o52(n42);
    }

    @DexIgnore
    public static UserUtils b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static UserUtils c(n42 n42) {
        UserUtils n = n42.n();
        n44.a(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    public UserUtils get() {
        return b(this.a);
    }
}
