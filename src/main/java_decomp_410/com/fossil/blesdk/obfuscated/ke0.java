package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.IntentSender;
import android.util.Log;
import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ke0<R extends me0> extends oe0<R> {
    @DexIgnore
    public /* final */ Activity a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public ke0(Activity activity, int i) {
        bk0.a(activity, (Object) "Activity must not be null");
        this.a = activity;
        this.b = i;
    }

    @DexIgnore
    public final void a(Status status) {
        if (status.K()) {
            try {
                status.a(this.a, this.b);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", e);
                b(new Status(8));
            }
        } else {
            b(status);
        }
    }

    @DexIgnore
    public abstract void b(Status status);
}
