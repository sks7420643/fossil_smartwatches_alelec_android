package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zi */
public final class C3439zi {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C3439zi.C3440a> f11562a; // = new java.util.HashSet();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zi$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zi$a */
    public static final class C3440a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.net.Uri f11563a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f11564b;

        @DexIgnore
        public C3440a(android.net.Uri uri, boolean z) {
            this.f11563a = uri;
            this.f11564b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public android.net.Uri mo18499a() {
            return this.f11563a;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo18500b() {
            return this.f11564b;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C3439zi.C3440a.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C3439zi.C3440a aVar = (com.fossil.blesdk.obfuscated.C3439zi.C3440a) obj;
            if (this.f11564b != aVar.f11564b || !this.f11563a.equals(aVar.f11563a)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (this.f11563a.hashCode() * 31) + (this.f11564b ? 1 : 0);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18495a(android.net.Uri uri, boolean z) {
        this.f11562a.add(new com.fossil.blesdk.obfuscated.C3439zi.C3440a(uri, z));
    }

    @DexIgnore
    /* renamed from: b */
    public int mo18496b() {
        return this.f11562a.size();
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C3439zi.class != obj.getClass()) {
            return false;
        }
        return this.f11562a.equals(((com.fossil.blesdk.obfuscated.C3439zi) obj).f11562a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f11562a.hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Set<com.fossil.blesdk.obfuscated.C3439zi.C3440a> mo18494a() {
        return this.f11562a;
    }
}
