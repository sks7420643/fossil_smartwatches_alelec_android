package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class z94<T, U> extends o84<U> implements m94<T> {
    @DexIgnore
    public /* final */ p84<T> e;

    @DexIgnore
    public z94(p84<T> p84) {
        this.e = p84;
    }
}
