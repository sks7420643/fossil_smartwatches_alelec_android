package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ur {
    @DexIgnore
    public /* final */ wr a;
    @DexIgnore
    public /* final */ a b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Map<Class<?>, C0036a<?>> a; // = new HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ur$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ur$a$a  reason: collision with other inner class name */
        public static class C0036a<Model> {
            @DexIgnore
            public /* final */ List<sr<Model, ?>> a;

            @DexIgnore
            public C0036a(List<sr<Model, ?>> list) {
                this.a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.a.clear();
        }

        @DexIgnore
        public <Model> void a(Class<Model> cls, List<sr<Model, ?>> list) {
            if (this.a.put(cls, new C0036a(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @DexIgnore
        public <Model> List<sr<Model, ?>> a(Class<Model> cls) {
            C0036a aVar = this.a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.a;
        }
    }

    @DexIgnore
    public ur(g8<List<Throwable>> g8Var) {
        this(new wr(g8Var));
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, tr<? extends Model, ? extends Data> trVar) {
        this.a.a(cls, cls2, trVar);
        this.b.a();
    }

    @DexIgnore
    public final synchronized <A> List<sr<A, ?>> b(Class<A> cls) {
        List<sr<A, ?>> a2;
        a2 = this.b.a(cls);
        if (a2 == null) {
            a2 = Collections.unmodifiableList(this.a.a(cls));
            this.b.a(cls, a2);
        }
        return a2;
    }

    @DexIgnore
    public ur(wr wrVar) {
        this.b = new a();
        this.a = wrVar;
    }

    @DexIgnore
    public <A> List<sr<A, ?>> a(A a2) {
        List b2 = b(b(a2));
        int size = b2.size();
        List<sr<A, ?>> emptyList = Collections.emptyList();
        boolean z = true;
        for (int i = 0; i < size; i++) {
            sr srVar = (sr) b2.get(i);
            if (srVar.a(a2)) {
                if (z) {
                    emptyList = new ArrayList<>(size - i);
                    z = false;
                }
                emptyList.add(srVar);
            }
        }
        return emptyList;
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return a2.getClass();
    }

    @DexIgnore
    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.a.b(cls);
    }
}
