package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fo */
public class C1816fo {

    @DexIgnore
    /* renamed from: a */
    public /* final */ byte[] f5232a; // = new byte[256];

    @DexIgnore
    /* renamed from: b */
    public java.nio.ByteBuffer f5233b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1739eo f5234c;

    @DexIgnore
    /* renamed from: d */
    public int f5235d; // = 0;

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1816fo mo10977a(java.nio.ByteBuffer byteBuffer) {
        mo10992m();
        this.f5233b = byteBuffer.asReadOnlyBuffer();
        this.f5233b.position(0);
        this.f5233b.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo10980b(int i) {
        boolean z = false;
        while (!z && !mo10981b() && this.f5234c.f4874c <= i) {
            int d = mo10983d();
            if (d == 33) {
                int d2 = mo10983d();
                if (d2 == 1) {
                    mo10993n();
                } else if (d2 == 249) {
                    this.f5234c.f4875d = new com.fossil.blesdk.obfuscated.C1646do();
                    mo10987h();
                } else if (d2 == 254) {
                    mo10993n();
                } else if (d2 != 255) {
                    mo10993n();
                } else {
                    mo10985f();
                    java.lang.StringBuilder sb = new java.lang.StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.f5232a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        mo10990k();
                    } else {
                        mo10993n();
                    }
                }
            } else if (d == 44) {
                com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5234c;
                if (eoVar.f4875d == null) {
                    eoVar.f4875d = new com.fossil.blesdk.obfuscated.C1646do();
                }
                mo10984e();
            } else if (d != 59) {
                this.f5234c.f4873b = 1;
            } else {
                z = true;
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1739eo mo10982c() {
        if (this.f5233b == null) {
            throw new java.lang.IllegalStateException("You must call setData() before parseHeader()");
        } else if (mo10981b()) {
            return this.f5234c;
        } else {
            mo10988i();
            if (!mo10981b()) {
                mo10986g();
                com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5234c;
                if (eoVar.f4874c < 0) {
                    eoVar.f4873b = 1;
                }
            }
            return this.f5234c;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final int mo10983d() {
        try {
            return this.f5233b.get() & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
        } catch (java.lang.Exception unused) {
            this.f5234c.f4873b = 1;
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo10984e() {
        this.f5234c.f4875d.f4459a = mo10991l();
        this.f5234c.f4875d.f4460b = mo10991l();
        this.f5234c.f4875d.f4461c = mo10991l();
        this.f5234c.f4875d.f4462d = mo10991l();
        int d = mo10983d();
        boolean z = false;
        boolean z2 = (d & 128) != 0;
        int pow = (int) java.lang.Math.pow(2.0d, (double) ((d & 7) + 1));
        com.fossil.blesdk.obfuscated.C1646do doVar = this.f5234c.f4875d;
        if ((d & 64) != 0) {
            z = true;
        }
        doVar.f4463e = z;
        if (z2) {
            this.f5234c.f4875d.f4469k = mo10979a(pow);
        } else {
            this.f5234c.f4875d.f4469k = null;
        }
        this.f5234c.f4875d.f4468j = this.f5233b.position();
        mo10994o();
        if (!mo10981b()) {
            com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5234c;
            eoVar.f4874c++;
            eoVar.f4876e.add(eoVar.f4875d);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo10985f() {
        this.f5235d = mo10983d();
        if (this.f5235d > 0) {
            int i = 0;
            int i2 = 0;
            while (i < this.f5235d) {
                try {
                    i2 = this.f5235d - i;
                    this.f5233b.get(this.f5232a, i, i2);
                    i += i2;
                } catch (java.lang.Exception e) {
                    if (android.util.Log.isLoggable("GifHeaderParser", 3)) {
                        android.util.Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.f5235d, e);
                    }
                    this.f5234c.f4873b = 1;
                    return;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo10986g() {
        mo10980b(Integer.MAX_VALUE);
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo10987h() {
        mo10983d();
        int d = mo10983d();
        com.fossil.blesdk.obfuscated.C1646do doVar = this.f5234c.f4875d;
        doVar.f4465g = (d & 28) >> 2;
        boolean z = true;
        if (doVar.f4465g == 0) {
            doVar.f4465g = 1;
        }
        com.fossil.blesdk.obfuscated.C1646do doVar2 = this.f5234c.f4875d;
        if ((d & 1) == 0) {
            z = false;
        }
        doVar2.f4464f = z;
        int l = mo10991l();
        if (l < 2) {
            l = 10;
        }
        com.fossil.blesdk.obfuscated.C1646do doVar3 = this.f5234c.f4875d;
        doVar3.f4467i = l * 10;
        doVar3.f4466h = mo10983d();
        mo10983d();
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo10988i() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) mo10983d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.f5234c.f4873b = 1;
            return;
        }
        mo10989j();
        if (this.f5234c.f4879h && !mo10981b()) {
            com.fossil.blesdk.obfuscated.C1739eo eoVar = this.f5234c;
            eoVar.f4872a = mo10979a(eoVar.f4880i);
            com.fossil.blesdk.obfuscated.C1739eo eoVar2 = this.f5234c;
            eoVar2.f4883l = eoVar2.f4872a[eoVar2.f4881j];
        }
    }

    @DexIgnore
    /* renamed from: j */
    public final void mo10989j() {
        this.f5234c.f4877f = mo10991l();
        this.f5234c.f4878g = mo10991l();
        int d = mo10983d();
        this.f5234c.f4879h = (d & 128) != 0;
        this.f5234c.f4880i = (int) java.lang.Math.pow(2.0d, (double) ((d & 7) + 1));
        this.f5234c.f4881j = mo10983d();
        this.f5234c.f4882k = mo10983d();
    }

    @DexIgnore
    /* renamed from: k */
    public final void mo10990k() {
        do {
            mo10985f();
            byte[] bArr = this.f5232a;
            if (bArr[0] == 1) {
                this.f5234c.f4884m = ((bArr[2] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX) << 8) | (bArr[1] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX);
            }
            if (this.f5235d <= 0) {
                return;
            }
        } while (!mo10981b());
    }

    @DexIgnore
    /* renamed from: l */
    public final int mo10991l() {
        return this.f5233b.getShort();
    }

    @DexIgnore
    /* renamed from: m */
    public final void mo10992m() {
        this.f5233b = null;
        java.util.Arrays.fill(this.f5232a, (byte) 0);
        this.f5234c = new com.fossil.blesdk.obfuscated.C1739eo();
        this.f5235d = 0;
    }

    @DexIgnore
    /* renamed from: n */
    public final void mo10993n() {
        int d;
        do {
            d = mo10983d();
            this.f5233b.position(java.lang.Math.min(this.f5233b.position() + d, this.f5233b.limit()));
        } while (d > 0);
    }

    @DexIgnore
    /* renamed from: o */
    public final void mo10994o() {
        mo10983d();
        mo10993n();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10978a() {
        this.f5233b = null;
        this.f5234c = null;
    }

    @DexIgnore
    /* renamed from: a */
    public final int[] mo10979a(int i) {
        byte[] bArr = new byte[(i * 3)];
        int[] iArr = null;
        try {
            this.f5233b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                byte b = bArr[i3] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
                int i5 = i4 + 1;
                byte b2 = bArr[i4] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
                int i6 = i5 + 1;
                int i7 = i2 + 1;
                iArr[i2] = (b << org.joda.time.DateTimeFieldType.CLOCKHOUR_OF_DAY) | -16777216 | (b2 << 8) | (bArr[i5] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX);
                i3 = i6;
                i2 = i7;
            }
        } catch (java.nio.BufferUnderflowException e) {
            if (android.util.Log.isLoggable("GifHeaderParser", 3)) {
                android.util.Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.f5234c.f4873b = 1;
        }
        return iArr;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10981b() {
        return this.f5234c.f4873b != 0;
    }
}
