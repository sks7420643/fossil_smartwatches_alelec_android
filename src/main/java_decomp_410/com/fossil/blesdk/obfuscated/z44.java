package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import retrofit.mime.MultipartTypedOutput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z44 extends v44<Boolean> {
    @DexIgnore
    public /* final */ z64 k; // = new y64();
    @DexIgnore
    public PackageManager l;
    @DexIgnore
    public String m;
    @DexIgnore
    public PackageInfo n;
    @DexIgnore
    public String o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public /* final */ Future<Map<String, x44>> t;
    @DexIgnore
    public /* final */ Collection<v44> u;

    @DexIgnore
    public z44(Future<Map<String, x44>> future, Collection<v44> collection) {
        this.t = future;
        this.u = collection;
    }

    @DexIgnore
    public Map<String, x44> a(Map<String, x44> map, Collection<v44> collection) {
        for (v44 next : collection) {
            if (!map.containsKey(next.p())) {
                map.put(next.p(), new x44(next.p(), next.r(), MultipartTypedOutput.DEFAULT_TRANSFER_ENCODING));
            }
        }
        return map;
    }

    @DexIgnore
    public final boolean b(String str, m74 m74, Collection<x44> collection) {
        return new q74(this, v(), m74.b, this.k).a(a(w74.a(l(), str), collection));
    }

    @DexIgnore
    public final boolean c(String str, m74 m74, Collection<x44> collection) {
        return a(m74, w74.a(l(), str), collection);
    }

    @DexIgnore
    public String p() {
        return "io.fabric.sdk.android:fabric";
    }

    @DexIgnore
    public String r() {
        return "1.4.8.32";
    }

    @DexIgnore
    public boolean u() {
        try {
            this.q = o().g();
            this.l = l().getPackageManager();
            this.m = l().getPackageName();
            this.n = this.l.getPackageInfo(this.m, 0);
            this.o = Integer.toString(this.n.versionCode);
            this.p = this.n.versionName == null ? "0.0" : this.n.versionName;
            this.r = this.l.getApplicationLabel(l().getApplicationInfo()).toString();
            this.s = Integer.toString(l().getApplicationInfo().targetSdkVersion);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            q44.g().e("Fabric", "Failed init", e);
            return false;
        }
    }

    @DexIgnore
    public String v() {
        return CommonUtils.b(l(), "com.crashlytics.ApiEndpoint");
    }

    @DexIgnore
    public final b84 w() {
        try {
            z74 d = z74.d();
            d.a(this, this.i, this.k, this.o, this.p, v(), o54.a(l()));
            d.b();
            return z74.d().a();
        } catch (Exception e) {
            q44.g().e("Fabric", "Error dealing with settings", e);
            return null;
        }
    }

    @DexIgnore
    public Boolean k() {
        boolean z;
        Map map;
        String c = CommonUtils.c(l());
        b84 w = w();
        if (w != null) {
            try {
                if (this.t != null) {
                    map = this.t.get();
                } else {
                    map = new HashMap();
                }
                a((Map<String, x44>) map, this.u);
                z = a(c, w.a, (Collection<x44>) map.values());
            } catch (Exception e) {
                q44.g().e("Fabric", "Error performing auto configuration.", e);
            }
            return Boolean.valueOf(z);
        }
        z = false;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final boolean a(String str, m74 m74, Collection<x44> collection) {
        if ("new".equals(m74.a)) {
            if (b(str, m74, collection)) {
                return z74.d().c();
            }
            q44.g().e("Fabric", "Failed to create app with Crashlytics service.", (Throwable) null);
            return false;
        } else if ("configured".equals(m74.a)) {
            return z74.d().c();
        } else {
            if (m74.e) {
                q44.g().d("Fabric", "Server says an update is required - forcing a full App update.");
                c(str, m74, collection);
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean a(m74 m74, w74 w74, Collection<x44> collection) {
        return new g84(this, v(), m74.b, this.k).a(a(w74, collection));
    }

    @DexIgnore
    public final l74 a(w74 w74, Collection<x44> collection) {
        Context l2 = l();
        return new l74(new k54().d(l2), o().d(), this.p, this.o, CommonUtils.a(CommonUtils.n(l2)), this.r, DeliveryMechanism.determineFrom(this.q).getId(), this.s, "0", w74, collection);
    }
}
