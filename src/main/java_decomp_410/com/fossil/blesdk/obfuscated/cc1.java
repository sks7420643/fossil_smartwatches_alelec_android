package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cc1 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public cc1(int i, byte[] bArr) {
        this.a = i;
        this.b = bArr;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof cc1)) {
            return false;
        }
        cc1 cc1 = (cc1) obj;
        return this.a == cc1.a && Arrays.equals(this.b, cc1.b);
    }

    @DexIgnore
    public final int hashCode() {
        return ((this.a + 527) * 31) + Arrays.hashCode(this.b);
    }
}
