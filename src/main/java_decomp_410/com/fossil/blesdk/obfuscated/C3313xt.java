package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xt */
public class C3313xt implements com.fossil.blesdk.obfuscated.C2581oo<com.fossil.blesdk.obfuscated.C3060ut> {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> f11076b;

    @DexIgnore
    public C3313xt(com.fossil.blesdk.obfuscated.C2581oo<android.graphics.Bitmap> ooVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(ooVar);
        this.f11076b = ooVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<com.fossil.blesdk.obfuscated.C3060ut> mo12877a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<com.fossil.blesdk.obfuscated.C3060ut> aqVar, int i, int i2) {
        com.fossil.blesdk.obfuscated.C3060ut utVar = aqVar.get();
        com.fossil.blesdk.obfuscated.C2675ps psVar = new com.fossil.blesdk.obfuscated.C2675ps(utVar.mo16862e(), com.fossil.blesdk.obfuscated.C2815rn.m13272a(context).mo15645c());
        com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> a = this.f11076b.mo12877a(context, psVar, i, i2);
        if (!psVar.equals(a)) {
            psVar.mo8887a();
        }
        utVar.mo16857a(this.f11076b, a.get());
        return aqVar;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C3313xt) {
            return this.f11076b.equals(((com.fossil.blesdk.obfuscated.C3313xt) obj).f11076b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f11076b.hashCode();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        this.f11076b.mo8934a(messageDigest);
    }
}
