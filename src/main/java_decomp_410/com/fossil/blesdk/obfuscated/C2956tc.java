package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@android.annotation.TargetApi(21)
/* renamed from: com.fossil.blesdk.obfuscated.tc */
public class C2956tc implements com.fossil.blesdk.obfuscated.C2869sc {

    @DexIgnore
    /* renamed from: a */
    public android.media.AudioAttributes f9628a;

    @DexIgnore
    /* renamed from: b */
    public int f9629b; // = -1;

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2956tc)) {
            return false;
        }
        return this.f9628a.equals(((com.fossil.blesdk.obfuscated.C2956tc) obj).f9628a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f9628a.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "AudioAttributesCompat: audioattributes=" + this.f9628a;
    }
}
