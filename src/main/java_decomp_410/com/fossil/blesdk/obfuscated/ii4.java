package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ii4 {
    @DexIgnore
    public static final mg4 a(fi4 fi4) {
        return ji4.a(fi4);
    }

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, CancellationException cancellationException) {
        ji4.a(coroutineContext, cancellationException);
    }

    @DexIgnore
    public static final oh4 a(fi4 fi4, oh4 oh4) {
        return ji4.a(fi4, oh4);
    }
}
