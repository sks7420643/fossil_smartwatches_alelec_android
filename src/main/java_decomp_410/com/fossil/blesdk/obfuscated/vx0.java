package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vx0 {
    @DexIgnore
    public volatile int e; // = -1;

    @DexIgnore
    public static final void a(vx0 vx0, byte[] bArr, int i, int i2) {
        try {
            px0 a = px0.a(bArr, 0, i2);
            vx0.a(a);
            a.a();
        } catch (IOException e2) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e2);
        }
    }

    @DexIgnore
    public final int a() {
        int b = b();
        this.e = b;
        return b;
    }

    @DexIgnore
    public void a(px0 px0) throws IOException {
    }

    @DexIgnore
    public int b() {
        return 0;
    }

    @DexIgnore
    /* renamed from: c */
    public vx0 clone() throws CloneNotSupportedException {
        return (vx0) super.clone();
    }

    @DexIgnore
    public String toString() {
        return xx0.a(this);
    }
}
