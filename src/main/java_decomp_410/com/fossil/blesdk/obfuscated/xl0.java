package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xl0 extends IInterface {
    @DexIgnore
    boolean a(mn0 mn0, sn0 sn0) throws RemoteException;
}
