package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rn */
public class C2815rn implements android.content.ComponentCallbacks2 {

    @DexIgnore
    /* renamed from: m */
    public static volatile com.fossil.blesdk.obfuscated.C2815rn f9043m;

    @DexIgnore
    /* renamed from: n */
    public static volatile boolean f9044n;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f9045e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C1440ar f9046f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2977tn f9047g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.bumptech.glide.Registry f9048h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f9049i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C3062uu f9050j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.fossil.blesdk.obfuscated.C2436mu f9051k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C3299xn> f9052l; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.rn$a */
    public interface C2816a {
        @DexIgnore
        com.fossil.blesdk.obfuscated.C2842rv build();
    }

    @DexIgnore
    public C2815rn(android.content.Context context, com.fossil.blesdk.obfuscated.C2755qp qpVar, com.fossil.blesdk.obfuscated.C1440ar arVar, com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C1885gq gqVar, com.fossil.blesdk.obfuscated.C3062uu uuVar, com.fossil.blesdk.obfuscated.C2436mu muVar, int i, com.fossil.blesdk.obfuscated.C2815rn.C2816a aVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C3371yn<?, ?>> map, java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> list, boolean z, boolean z2, int i2, int i3) {
        com.fossil.blesdk.obfuscated.C2427mo moVar;
        com.fossil.blesdk.obfuscated.C2427mo moVar2;
        android.content.Context context2 = context;
        com.fossil.blesdk.obfuscated.C2149jq jqVar2 = jqVar;
        com.fossil.blesdk.obfuscated.C1885gq gqVar2 = gqVar;
        java.lang.Class<com.fossil.blesdk.obfuscated.C1570co> cls = com.fossil.blesdk.obfuscated.C1570co.class;
        java.lang.Class<byte[]> cls2 = byte[].class;
        com.bumptech.glide.MemoryCategory memoryCategory = com.bumptech.glide.MemoryCategory.NORMAL;
        this.f9045e = jqVar2;
        this.f9049i = gqVar2;
        this.f9046f = arVar;
        this.f9050j = uuVar;
        this.f9051k = muVar;
        android.content.res.Resources resources = context.getResources();
        this.f9048h = new com.bumptech.glide.Registry();
        this.f9048h.mo3919a((com.bumptech.glide.load.ImageHeaderParser) new com.fossil.blesdk.obfuscated.C3135vs());
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            this.f9048h.mo3919a((com.bumptech.glide.load.ImageHeaderParser) new com.fossil.blesdk.obfuscated.C3465zs());
        }
        java.util.List<com.bumptech.glide.load.ImageHeaderParser> a = this.f9048h.mo3929a();
        com.fossil.blesdk.obfuscated.C2920st stVar = new com.fossil.blesdk.obfuscated.C2920st(context2, a, jqVar2, gqVar2);
        com.fossil.blesdk.obfuscated.C2427mo<android.os.ParcelFileDescriptor, android.graphics.Bitmap> b = com.fossil.blesdk.obfuscated.C2065it.m8612b(jqVar);
        if (!z2 || android.os.Build.VERSION.SDK_INT < 28) {
            com.fossil.blesdk.obfuscated.C3240ws wsVar = new com.fossil.blesdk.obfuscated.C3240ws(this.f9048h.mo3929a(), resources.getDisplayMetrics(), jqVar2, gqVar2);
            moVar = new com.fossil.blesdk.obfuscated.C2831rs(wsVar);
            moVar2 = new com.fossil.blesdk.obfuscated.C1827ft(wsVar, gqVar2);
        } else {
            moVar2 = new com.fossil.blesdk.obfuscated.C1579ct();
            moVar = new com.fossil.blesdk.obfuscated.C2918ss();
        }
        com.fossil.blesdk.obfuscated.C2602ot otVar = new com.fossil.blesdk.obfuscated.C2602ot(context2);
        com.fossil.blesdk.obfuscated.C3305xr.C3308c cVar = new com.fossil.blesdk.obfuscated.C3305xr.C3308c(resources);
        com.fossil.blesdk.obfuscated.C3305xr.C3309d dVar = new com.fossil.blesdk.obfuscated.C3305xr.C3309d(resources);
        com.fossil.blesdk.obfuscated.C3305xr.C3307b bVar = new com.fossil.blesdk.obfuscated.C3305xr.C3307b(resources);
        java.lang.Class<byte[]> cls3 = cls2;
        com.fossil.blesdk.obfuscated.C3305xr.C3306a aVar2 = new com.fossil.blesdk.obfuscated.C3305xr.C3306a(resources);
        com.fossil.blesdk.obfuscated.C2502ns nsVar = new com.fossil.blesdk.obfuscated.C2502ns(gqVar2);
        com.fossil.blesdk.obfuscated.C3305xr.C3306a aVar3 = aVar2;
        com.fossil.blesdk.obfuscated.C1580cu cuVar = new com.fossil.blesdk.obfuscated.C1580cu();
        com.fossil.blesdk.obfuscated.C1832fu fuVar = new com.fossil.blesdk.obfuscated.C1832fu();
        android.content.ContentResolver contentResolver = context.getContentResolver();
        com.bumptech.glide.Registry registry = this.f9048h;
        registry.mo3921a(java.nio.ByteBuffer.class, new com.fossil.blesdk.obfuscated.C1968hr());
        registry.mo3921a(java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C3379yr(gqVar2));
        registry.mo3926a("Bitmap", java.nio.ByteBuffer.class, android.graphics.Bitmap.class, moVar);
        registry.mo3926a("Bitmap", java.io.InputStream.class, android.graphics.Bitmap.class, moVar2);
        registry.mo3926a("Bitmap", android.os.ParcelFileDescriptor.class, android.graphics.Bitmap.class, b);
        registry.mo3926a("Bitmap", android.content.res.AssetFileDescriptor.class, android.graphics.Bitmap.class, com.fossil.blesdk.obfuscated.C2065it.m8610a(jqVar));
        registry.mo3925a(android.graphics.Bitmap.class, android.graphics.Bitmap.class, com.fossil.blesdk.obfuscated.C1442as.C1443a.m4619a());
        registry.mo3926a("Bitmap", android.graphics.Bitmap.class, android.graphics.Bitmap.class, new com.fossil.blesdk.obfuscated.C1972ht());
        registry.mo3922a(android.graphics.Bitmap.class, nsVar);
        registry.mo3926a("BitmapDrawable", java.nio.ByteBuffer.class, android.graphics.drawable.BitmapDrawable.class, new com.fossil.blesdk.obfuscated.C2341ls(resources, moVar));
        registry.mo3926a("BitmapDrawable", java.io.InputStream.class, android.graphics.drawable.BitmapDrawable.class, new com.fossil.blesdk.obfuscated.C2341ls(resources, moVar2));
        registry.mo3926a("BitmapDrawable", android.os.ParcelFileDescriptor.class, android.graphics.drawable.BitmapDrawable.class, new com.fossil.blesdk.obfuscated.C2341ls(resources, b));
        registry.mo3922a(android.graphics.drawable.BitmapDrawable.class, new com.fossil.blesdk.obfuscated.C2434ms(jqVar2, nsVar));
        registry.mo3926a("Gif", java.io.InputStream.class, com.fossil.blesdk.obfuscated.C3060ut.class, new com.fossil.blesdk.obfuscated.C1515bu(a, stVar, gqVar2));
        registry.mo3926a("Gif", java.nio.ByteBuffer.class, com.fossil.blesdk.obfuscated.C3060ut.class, stVar);
        registry.mo3922a(com.fossil.blesdk.obfuscated.C3060ut.class, new com.fossil.blesdk.obfuscated.C3140vt());
        registry.mo3925a(cls, cls, com.fossil.blesdk.obfuscated.C1442as.C1443a.m4619a());
        registry.mo3926a("Bitmap", cls, android.graphics.Bitmap.class, new com.fossil.blesdk.obfuscated.C3466zt(jqVar2));
        registry.mo3924a(android.net.Uri.class, android.graphics.drawable.Drawable.class, otVar);
        registry.mo3924a(android.net.Uri.class, android.graphics.Bitmap.class, new com.fossil.blesdk.obfuscated.C1747et(otVar, jqVar2));
        registry.mo3920a((com.fossil.blesdk.obfuscated.C2978to.C2979a<?>) new com.fossil.blesdk.obfuscated.C2157jt.C2158a());
        registry.mo3925a(java.io.File.class, java.nio.ByteBuffer.class, new com.fossil.blesdk.obfuscated.C2057ir.C2059b());
        registry.mo3925a(java.io.File.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C2237kr.C2243e());
        registry.mo3924a(java.io.File.class, java.io.File.class, new com.fossil.blesdk.obfuscated.C2766qt());
        registry.mo3925a(java.io.File.class, android.os.ParcelFileDescriptor.class, new com.fossil.blesdk.obfuscated.C2237kr.C2239b());
        registry.mo3925a(java.io.File.class, java.io.File.class, com.fossil.blesdk.obfuscated.C1442as.C1443a.m4619a());
        registry.mo3920a((com.fossil.blesdk.obfuscated.C2978to.C2979a<?>) new com.fossil.blesdk.obfuscated.C3454zo.C3455a(gqVar2));
        com.fossil.blesdk.obfuscated.C3305xr.C3308c cVar2 = cVar;
        registry.mo3925a(java.lang.Integer.TYPE, java.io.InputStream.class, cVar2);
        com.fossil.blesdk.obfuscated.C3305xr.C3307b bVar2 = bVar;
        registry.mo3925a(java.lang.Integer.TYPE, android.os.ParcelFileDescriptor.class, bVar2);
        registry.mo3925a(java.lang.Integer.class, java.io.InputStream.class, cVar2);
        registry.mo3925a(java.lang.Integer.class, android.os.ParcelFileDescriptor.class, bVar2);
        com.fossil.blesdk.obfuscated.C3305xr.C3309d dVar2 = dVar;
        registry.mo3925a(java.lang.Integer.class, android.net.Uri.class, dVar2);
        com.fossil.blesdk.obfuscated.C3305xr.C3306a aVar4 = aVar3;
        registry.mo3925a(java.lang.Integer.TYPE, android.content.res.AssetFileDescriptor.class, aVar4);
        registry.mo3925a(java.lang.Integer.class, android.content.res.AssetFileDescriptor.class, aVar4);
        registry.mo3925a(java.lang.Integer.TYPE, android.net.Uri.class, dVar2);
        registry.mo3925a(java.lang.String.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C2151jr.C2154c());
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C2151jr.C2154c());
        registry.mo3925a(java.lang.String.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C3460zr.C3463c());
        registry.mo3925a(java.lang.String.class, android.os.ParcelFileDescriptor.class, new com.fossil.blesdk.obfuscated.C3460zr.C3462b());
        registry.mo3925a(java.lang.String.class, android.content.res.AssetFileDescriptor.class, new com.fossil.blesdk.obfuscated.C3460zr.C3461a());
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1745es.C1746a());
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1821fr.C1824c(context.getAssets()));
        registry.mo3925a(android.net.Uri.class, android.os.ParcelFileDescriptor.class, new com.fossil.blesdk.obfuscated.C1821fr.C1823b(context.getAssets()));
        android.content.Context context3 = context;
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1825fs.C1826a(context3));
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1893gs.C1894a(context3));
        android.content.ContentResolver contentResolver2 = contentResolver;
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1504bs.C1508d(contentResolver2));
        registry.mo3925a(android.net.Uri.class, android.os.ParcelFileDescriptor.class, new com.fossil.blesdk.obfuscated.C1504bs.C1506b(contentResolver2));
        registry.mo3925a(android.net.Uri.class, android.content.res.AssetFileDescriptor.class, new com.fossil.blesdk.obfuscated.C1504bs.C1505a(contentResolver2));
        registry.mo3925a(android.net.Uri.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1577cs.C1578a());
        registry.mo3925a(java.net.URL.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1969hs.C1970a());
        registry.mo3925a(android.net.Uri.class, java.io.File.class, new com.fossil.blesdk.obfuscated.C2672pr.C2673a(context3));
        registry.mo3925a(com.fossil.blesdk.obfuscated.C2340lr.class, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1657ds.C1658a());
        java.lang.Class<byte[]> cls4 = cls3;
        registry.mo3925a(cls4, java.nio.ByteBuffer.class, new com.fossil.blesdk.obfuscated.C1886gr.C1887a());
        registry.mo3925a(cls4, java.io.InputStream.class, new com.fossil.blesdk.obfuscated.C1886gr.C1891d());
        registry.mo3925a(android.net.Uri.class, android.net.Uri.class, com.fossil.blesdk.obfuscated.C1442as.C1443a.m4619a());
        registry.mo3925a(android.graphics.drawable.Drawable.class, android.graphics.drawable.Drawable.class, com.fossil.blesdk.obfuscated.C1442as.C1443a.m4619a());
        registry.mo3924a(android.graphics.drawable.Drawable.class, android.graphics.drawable.Drawable.class, new com.fossil.blesdk.obfuscated.C2676pt());
        registry.mo3923a(android.graphics.Bitmap.class, android.graphics.drawable.BitmapDrawable.class, new com.fossil.blesdk.obfuscated.C1660du(resources));
        com.fossil.blesdk.obfuscated.C1580cu cuVar2 = cuVar;
        registry.mo3923a(android.graphics.Bitmap.class, cls4, cuVar2);
        com.fossil.blesdk.obfuscated.C1832fu fuVar2 = fuVar;
        registry.mo3923a(android.graphics.drawable.Drawable.class, cls4, new com.fossil.blesdk.obfuscated.C1748eu(jqVar2, cuVar2, fuVar2));
        registry.mo3923a(com.fossil.blesdk.obfuscated.C3060ut.class, cls4, fuVar2);
        android.content.Context context4 = context;
        com.fossil.blesdk.obfuscated.C1885gq gqVar3 = gqVar;
        com.fossil.blesdk.obfuscated.C2977tn tnVar = new com.fossil.blesdk.obfuscated.C2977tn(context4, gqVar3, this.f9048h, new com.fossil.blesdk.obfuscated.C3470zv(), aVar, map, list, qpVar, z, i);
        this.f9047g = tnVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2815rn m13272a(android.content.Context context) {
        if (f9043m == null) {
            com.bumptech.glide.GeneratedAppGlideModule b = m13278b(context.getApplicationContext());
            synchronized (com.fossil.blesdk.obfuscated.C2815rn.class) {
                if (f9043m == null) {
                    m13275a(context, b);
                }
            }
        }
        return f9043m;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m13279b(android.content.Context context, com.bumptech.glide.GeneratedAppGlideModule generatedAppGlideModule) {
        m13276a(context, new com.fossil.blesdk.obfuscated.C2898sn(), generatedAppGlideModule);
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2149jq mo15645c() {
        return this.f9045e;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2436mu mo15646d() {
        return this.f9051k;
    }

    @DexIgnore
    /* renamed from: e */
    public android.content.Context mo15647e() {
        return this.f9047g.getBaseContext();
    }

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2977tn mo15648f() {
        return this.f9047g;
    }

    @DexIgnore
    /* renamed from: g */
    public com.bumptech.glide.Registry mo15649g() {
        return this.f9048h;
    }

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3062uu mo15650h() {
        return this.f9050j;
    }

    @DexIgnore
    public void onConfigurationChanged(android.content.res.Configuration configuration) {
    }

    @DexIgnore
    public void onLowMemory() {
        mo15639a();
    }

    @DexIgnore
    public void onTrimMemory(int i) {
        mo15640a(i);
    }

    @DexIgnore
    /* renamed from: b */
    public static com.bumptech.glide.GeneratedAppGlideModule m13278b(android.content.Context context) {
        try {
            return (com.bumptech.glide.GeneratedAppGlideModule) java.lang.Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new java.lang.Class[]{android.content.Context.class}).newInstance(new java.lang.Object[]{context.getApplicationContext()});
        } catch (java.lang.ClassNotFoundException unused) {
            if (android.util.Log.isLoggable("Glide", 5)) {
                android.util.Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (java.lang.InstantiationException e) {
            m13277a((java.lang.Exception) e);
            throw null;
        } catch (java.lang.IllegalAccessException e2) {
            m13277a((java.lang.Exception) e2);
            throw null;
        } catch (java.lang.NoSuchMethodException e3) {
            m13277a((java.lang.Exception) e3);
            throw null;
        } catch (java.lang.reflect.InvocationTargetException e4) {
            m13277a((java.lang.Exception) e4);
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C3062uu m13280c(android.content.Context context) {
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return m13272a(context).mo15650h();
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C3299xn m13281d(android.content.Context context) {
        return m13280c(context).mo16893a(context);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13275a(android.content.Context context, com.bumptech.glide.GeneratedAppGlideModule generatedAppGlideModule) {
        if (!f9044n) {
            f9044n = true;
            m13279b(context, generatedAppGlideModule);
            f9044n = false;
            return;
        }
        throw new java.lang.IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1885gq mo15643b() {
        return this.f9049i;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15644b(com.fossil.blesdk.obfuscated.C3299xn xnVar) {
        synchronized (this.f9052l) {
            if (this.f9052l.contains(xnVar)) {
                this.f9052l.remove(xnVar);
            } else {
                throw new java.lang.IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13276a(android.content.Context context, com.fossil.blesdk.obfuscated.C2898sn snVar, com.bumptech.glide.GeneratedAppGlideModule generatedAppGlideModule) {
        android.content.Context applicationContext = context.getApplicationContext();
        java.util.List<com.fossil.blesdk.obfuscated.C1516bv> emptyList = java.util.Collections.emptyList();
        if (generatedAppGlideModule == null || generatedAppGlideModule.mo3917a()) {
            emptyList = new com.fossil.blesdk.obfuscated.C1662dv(applicationContext).mo10139a();
        }
        if (generatedAppGlideModule != null && !generatedAppGlideModule.mo3913b().isEmpty()) {
            java.util.Set<java.lang.Class<?>> b = generatedAppGlideModule.mo3913b();
            java.util.Iterator<com.fossil.blesdk.obfuscated.C1516bv> it = emptyList.iterator();
            while (it.hasNext()) {
                com.fossil.blesdk.obfuscated.C1516bv next = it.next();
                if (b.contains(next.getClass())) {
                    if (android.util.Log.isLoggable("Glide", 3)) {
                        android.util.Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (android.util.Log.isLoggable("Glide", 3)) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C1516bv> it2 = emptyList.iterator();
            while (it2.hasNext()) {
                android.util.Log.d("Glide", "Discovered GlideModule from manifest: " + it2.next().getClass());
            }
        }
        snVar.mo16121a(generatedAppGlideModule != null ? generatedAppGlideModule.mo3914c() : null);
        for (com.fossil.blesdk.obfuscated.C1516bv a : emptyList) {
            a.mo3916a(applicationContext, snVar);
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.mo3916a(applicationContext, snVar);
        }
        com.fossil.blesdk.obfuscated.C2815rn a2 = snVar.mo16120a(applicationContext);
        for (com.fossil.blesdk.obfuscated.C1516bv next2 : emptyList) {
            try {
                next2.mo3915a(applicationContext, a2, a2.f9048h);
            } catch (java.lang.AbstractMethodError e) {
                throw new java.lang.IllegalStateException("Attempting to register a Glide v3 module. If you see this, you or one of your dependencies may be including Glide v3 even though you're using Glide v4. You'll need to find and remove (or update) the offending dependency. The v3 module name is: " + next2.getClass().getName(), e);
            }
        }
        if (generatedAppGlideModule != null) {
            generatedAppGlideModule.mo3915a(applicationContext, a2, a2.f9048h);
        }
        applicationContext.registerComponentCallbacks(a2);
        f9043m = a2;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m13277a(java.lang.Exception exc) {
        throw new java.lang.IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15639a() {
        com.fossil.blesdk.obfuscated.C3066uw.m14931b();
        this.f9046f.mo8906a();
        this.f9045e.mo12444a();
        this.f9049i.mo11283a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15640a(int i) {
        com.fossil.blesdk.obfuscated.C3066uw.m14931b();
        for (com.fossil.blesdk.obfuscated.C3299xn onTrimMemory : this.f9052l) {
            onTrimMemory.onTrimMemory(i);
        }
        this.f9046f.mo8907a(i);
        this.f9045e.mo12445a(i);
        this.f9049i.mo11284a(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3299xn m13274a(androidx.fragment.app.Fragment fragment) {
        return m13280c(fragment.getContext()).mo16897a(fragment);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3299xn m13273a(android.view.View view) {
        return m13280c(view.getContext()).mo16896a(view);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo15642a(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        synchronized (this.f9052l) {
            for (com.fossil.blesdk.obfuscated.C3299xn b : this.f9052l) {
                if (b.mo17821b(bwVar)) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15641a(com.fossil.blesdk.obfuscated.C3299xn xnVar) {
        synchronized (this.f9052l) {
            if (!this.f9052l.contains(xnVar)) {
                this.f9052l.add(xnVar);
            } else {
                throw new java.lang.IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
