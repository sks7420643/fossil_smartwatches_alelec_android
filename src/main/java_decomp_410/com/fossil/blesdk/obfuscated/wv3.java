package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.Closeable;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wv3 {
    @DexIgnore
    public static /* final */ byte[] a; // = new byte[0];
    @DexIgnore
    public static /* final */ String[] b; // = new String[0];
    @DexIgnore
    public static /* final */ Charset c; // = Charset.forName("UTF-8");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public a(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, this.a);
            thread.setDaemon(this.b);
            return thread;
        }
    }

    @DexIgnore
    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    @DexIgnore
    public static boolean b(yo4 yo4, int i, TimeUnit timeUnit) throws IOException {
        long nanoTime = System.nanoTime();
        long c2 = yo4.b().d() ? yo4.b().c() - nanoTime : Long.MAX_VALUE;
        yo4.b().a(Math.min(c2, timeUnit.toNanos((long) i)) + nanoTime);
        try {
            jo4 jo4 = new jo4();
            while (yo4.b(jo4, 2048) != -1) {
                jo4.w();
            }
            if (c2 == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                yo4.b().a();
            } else {
                yo4.b().a(nanoTime + c2);
            }
            return true;
        } catch (InterruptedIOException unused) {
            if (c2 == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                yo4.b().a();
            } else {
                yo4.b().a(nanoTime + c2);
            }
            return false;
        } catch (Throwable th) {
            if (c2 == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                yo4.b().a();
            } else {
                yo4.b().a(nanoTime + c2);
            }
            throw th;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public static void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e) {
                if (!a(e)) {
                    throw e;
                }
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    public static void a(Closeable closeable, Closeable closeable2) throws IOException {
        try {
            closeable.close();
            th = null;
        } catch (Throwable th) {
            th = th;
        }
        try {
            closeable2.close();
        } catch (Throwable th2) {
            if (th == null) {
                th = th2;
            }
        }
        if (th != null) {
            if (th instanceof IOException) {
                throw ((IOException) th);
            } else if (th instanceof RuntimeException) {
                throw ((RuntimeException) th);
            } else if (th instanceof Error) {
                throw th;
            } else {
                throw new AssertionError(th);
            }
        }
    }

    @DexIgnore
    public static boolean a(yo4 yo4, int i, TimeUnit timeUnit) {
        try {
            return b(yo4, i, timeUnit);
        } catch (IOException unused) {
            return false;
        }
    }

    @DexIgnore
    public static String a(String str) {
        try {
            return ByteString.of(MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5).digest(str.getBytes("UTF-8"))).hex();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public static ByteString a(ByteString byteString) {
        try {
            return ByteString.of(MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1).digest(byteString.toByteArray()));
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public static <T> List<T> a(List<T> list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    @DexIgnore
    public static <T> List<T> a(T... tArr) {
        return Collections.unmodifiableList(Arrays.asList((Object[]) tArr.clone()));
    }

    @DexIgnore
    public static <K, V> Map<K, V> a(Map<K, V> map) {
        return Collections.unmodifiableMap(new LinkedHashMap(map));
    }

    @DexIgnore
    public static ThreadFactory a(String str, boolean z) {
        return new a(str, z);
    }

    @DexIgnore
    public static <T> T[] a(Class<T> cls, T[] tArr, T[] tArr2) {
        List a2 = a(tArr, tArr2);
        return a2.toArray((Object[]) Array.newInstance(cls, a2.size()));
    }

    @DexIgnore
    public static <T> List<T> a(T[] tArr, T[] tArr2) {
        ArrayList arrayList = new ArrayList();
        for (T t : tArr) {
            int length = tArr2.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                T t2 = tArr2[i];
                if (t.equals(t2)) {
                    arrayList.add(t2);
                    break;
                }
                i++;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static String a(dv3 dv3) {
        if (dv3.h() == dv3.b(dv3.j())) {
            return dv3.f();
        }
        return dv3.f() + ":" + dv3.h();
    }

    @DexIgnore
    public static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
