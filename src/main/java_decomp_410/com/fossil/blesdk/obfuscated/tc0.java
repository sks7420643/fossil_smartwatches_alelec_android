package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tc0 extends zr0 implements sc0 {
    @DexIgnore
    public tc0() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                a((GoogleSignInAccount) as0.a(parcel, GoogleSignInAccount.CREATOR), (Status) as0.a(parcel, Status.CREATOR));
                throw null;
            case 102:
                a((Status) as0.a(parcel, Status.CREATOR));
                break;
            case 103:
                b((Status) as0.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
