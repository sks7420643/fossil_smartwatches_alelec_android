package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.common.DeliveryMechanism;
import io.fabric.sdk.android.services.common.IdManager;
import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z74 {
    @DexIgnore
    public /* final */ AtomicReference<b84> a;
    @DexIgnore
    public /* final */ CountDownLatch b;
    @DexIgnore
    public a84 c;
    @DexIgnore
    public boolean d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ z74 a; // = new z74();
    }

    @DexIgnore
    public static z74 d() {
        return b.a;
    }

    @DexIgnore
    public synchronized z74 a(v44 v44, IdManager idManager, z64 z64, String str, String str2, String str3, o54 o54) {
        v44 v442 = v44;
        synchronized (this) {
            if (this.d) {
                return this;
            }
            if (this.c == null) {
                Context l = v44.l();
                String d2 = idManager.d();
                String d3 = new k54().d(l);
                String g = idManager.g();
                x54 x54 = new x54();
                t74 t74 = new t74();
                r74 r74 = new r74(v442);
                String c2 = CommonUtils.c(l);
                String str4 = str3;
                u74 u74 = new u74(v442, str4, String.format(Locale.US, "https://settings.crashlytics.com/spi/v2/platforms/android/apps/%s/settings", new Object[]{d2}), z64);
                String h = idManager.h();
                String str5 = h;
                String str6 = str2;
                String str7 = str;
                v44 v443 = v44;
                this.c = new s74(v443, new e84(d3, str5, idManager.i(), idManager.j(), idManager.e(), CommonUtils.a(CommonUtils.n(l)), str6, str7, DeliveryMechanism.determineFrom(g).getId(), c2), x54, t74, r74, u74, o54);
            }
            this.d = true;
            return this;
        }
    }

    @DexIgnore
    public synchronized boolean b() {
        b84 a2;
        a2 = this.c.a();
        a(a2);
        return a2 != null;
    }

    @DexIgnore
    public synchronized boolean c() {
        b84 a2;
        a2 = this.c.a(SettingsCacheBehavior.SKIP_CACHE_LOOKUP);
        a(a2);
        if (a2 == null) {
            q44.g().e("Fabric", "Failed to force reload of settings from Crashlytics.", (Throwable) null);
        }
        return a2 != null;
    }

    @DexIgnore
    public z74() {
        this.a = new AtomicReference<>();
        this.b = new CountDownLatch(1);
        this.d = false;
    }

    @DexIgnore
    public b84 a() {
        try {
            this.b.await();
            return this.a.get();
        } catch (InterruptedException unused) {
            q44.g().e("Fabric", "Interrupted while waiting for settings data.");
            return null;
        }
    }

    @DexIgnore
    public final void a(b84 b84) {
        this.a.set(b84);
        this.b.countDown();
    }
}
