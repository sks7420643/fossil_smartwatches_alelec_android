package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xt implements oo<ut> {
    @DexIgnore
    public /* final */ oo<Bitmap> b;

    @DexIgnore
    public xt(oo<Bitmap> ooVar) {
        tw.a(ooVar);
        this.b = ooVar;
    }

    @DexIgnore
    public aq<ut> a(Context context, aq<ut> aqVar, int i, int i2) {
        ut utVar = aqVar.get();
        ps psVar = new ps(utVar.e(), rn.a(context).c());
        aq<Bitmap> a = this.b.a(context, psVar, i, i2);
        if (!psVar.equals(a)) {
            psVar.a();
        }
        utVar.a(this.b, a.get());
        return aqVar;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof xt) {
            return this.b.equals(((xt) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
