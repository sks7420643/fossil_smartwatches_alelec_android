package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.pq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uq4 extends DialogFragment {
    @DexIgnore
    public pq4.a e;
    @DexIgnore
    public pq4.b f;
    @DexIgnore
    public boolean g; // = false;

    @DexIgnore
    public static uq4 a(String str, String str2, String str3, int i, int i2, String[] strArr) {
        uq4 uq4 = new uq4();
        uq4.setArguments(new tq4(str, str2, str3, i, i2, strArr).a());
        return uq4;
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        if (Build.VERSION.SDK_INT >= 17 && getParentFragment() != null) {
            if (getParentFragment() instanceof pq4.a) {
                this.e = (pq4.a) getParentFragment();
            }
            if (getParentFragment() instanceof pq4.b) {
                this.f = (pq4.b) getParentFragment();
            }
        }
        if (context instanceof pq4.a) {
            this.e = (pq4.a) context;
        }
        if (context instanceof pq4.b) {
            this.f = (pq4.b) context;
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        setCancelable(false);
        tq4 tq4 = new tq4(getArguments());
        return tq4.a(getActivity(), new sq4(this, tq4, this.e, this.f));
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.e = null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        this.g = true;
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public void a(FragmentManager fragmentManager, String str) {
        if ((Build.VERSION.SDK_INT < 26 || !fragmentManager.isStateSaved()) && !this.g) {
            show(fragmentManager, str);
        }
    }
}
