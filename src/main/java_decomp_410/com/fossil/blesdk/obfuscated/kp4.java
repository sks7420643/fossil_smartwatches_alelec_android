package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kp4 implements Iterable<np4> {
    @DexIgnore
    public /* final */ List<np4> e; // = new LinkedList();
    @DexIgnore
    public /* final */ Map<String, List<np4>> f; // = new HashMap();

    @DexIgnore
    public void a(np4 np4) {
        if (np4 != null) {
            String lowerCase = np4.b().toLowerCase(Locale.US);
            List list = this.f.get(lowerCase);
            if (list == null) {
                list = new LinkedList();
                this.f.put(lowerCase, list);
            }
            list.add(np4);
            this.e.add(np4);
        }
    }

    @DexIgnore
    public Iterator<np4> iterator() {
        return Collections.unmodifiableList(this.e).iterator();
    }

    @DexIgnore
    public String toString() {
        return this.e.toString();
    }

    @DexIgnore
    public np4 a(String str) {
        if (str == null) {
            return null;
        }
        List list = this.f.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (np4) list.get(0);
    }
}
