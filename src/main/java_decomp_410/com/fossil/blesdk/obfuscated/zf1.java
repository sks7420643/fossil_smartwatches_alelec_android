package com.fossil.blesdk.obfuscated;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public class zf1 extends ContentProvider {
    @DexIgnore
    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if ("com.google.android.gms.measurement.google_measurement_service".equals(providerInfo.authority)) {
            throw new IllegalStateException("Incorrect provider authority in manifest. Most likely due to a missing applicationId variable in application's build.gradle.");
        }
    }

    @DexIgnore
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    @DexIgnore
    public String getType(Uri uri) {
        return null;
    }

    @DexIgnore
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @DexIgnore
    public boolean onCreate() {
        xh1.a(getContext(), (og1) null);
        return false;
    }

    @DexIgnore
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    @DexIgnore
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
