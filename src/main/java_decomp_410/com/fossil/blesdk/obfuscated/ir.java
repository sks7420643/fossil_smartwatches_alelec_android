package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ir implements sr<File, ByteBuffer> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements so<ByteBuffer> {
        @DexIgnore
        public /* final */ File e;

        @DexIgnore
        public a(File file) {
            this.e = file;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super ByteBuffer> aVar) {
            try {
                aVar.a(kw.a(this.e));
            } catch (IOException e2) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e2);
                }
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements tr<File, ByteBuffer> {
        @DexIgnore
        public sr<File, ByteBuffer> a(wr wrVar) {
            return new ir();
        }
    }

    @DexIgnore
    public boolean a(File file) {
        return true;
    }

    @DexIgnore
    public sr.a<ByteBuffer> a(File file, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(file), new a(file));
    }
}
