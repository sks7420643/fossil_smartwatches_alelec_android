package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class j82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository e;
    @DexIgnore
    private /* final */ /* synthetic */ RecommendedPreset f;

    @DexIgnore
    public /* synthetic */ j82(PresetRepository presetRepository, RecommendedPreset recommendedPreset) {
        this.e = presetRepository;
        this.f = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f);
    }
}
