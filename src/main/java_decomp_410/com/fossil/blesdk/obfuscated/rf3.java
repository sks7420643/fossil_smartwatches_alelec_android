package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rf3 implements Factory<HeartRateDetailPresenter> {
    @DexIgnore
    public static HeartRateDetailPresenter a(nf3 nf3, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, h42 h42) {
        return new HeartRateDetailPresenter(nf3, heartRateSummaryRepository, heartRateSampleRepository, userRepository, workoutSessionRepository, fitnessDataDao, workoutDao, fitnessDatabase, h42);
    }
}
