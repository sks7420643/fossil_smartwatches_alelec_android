package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r0 */
public class C2778r0 implements android.text.method.TransformationMethod {

    @DexIgnore
    /* renamed from: e */
    public java.util.Locale f8812e;

    @DexIgnore
    public C2778r0(android.content.Context context) {
        this.f8812e = context.getResources().getConfiguration().locale;
    }

    @DexIgnore
    public java.lang.CharSequence getTransformation(java.lang.CharSequence charSequence, android.view.View view) {
        if (charSequence != null) {
            return charSequence.toString().toUpperCase(this.f8812e);
        }
        return null;
    }

    @DexIgnore
    public void onFocusChanged(android.view.View view, java.lang.CharSequence charSequence, boolean z, int i, android.graphics.Rect rect) {
    }
}
