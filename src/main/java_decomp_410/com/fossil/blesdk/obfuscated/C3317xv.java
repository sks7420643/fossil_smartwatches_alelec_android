package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xv */
public class C3317xv extends com.fossil.blesdk.obfuscated.C3393yv<android.graphics.drawable.Drawable> {
    @DexIgnore
    public C3317xv(android.widget.ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo17218c(android.graphics.drawable.Drawable drawable) {
        ((android.widget.ImageView) this.f4201e).setImageDrawable(drawable);
    }
}
