package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tp */
public class C2980tp implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.Object f9737b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ int f9738c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ int f9739d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.Class<?> f9740e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.lang.Class<?> f9741f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f9742g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> f9743h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2337lo f9744i;

    @DexIgnore
    /* renamed from: j */
    public int f9745j;

    @DexIgnore
    public C2980tp(java.lang.Object obj, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, java.lang.Class<?> cls, java.lang.Class<?> cls2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(obj);
        this.f9737b = obj;
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(joVar, "Signature must not be null");
        this.f9742g = joVar;
        this.f9738c = i;
        this.f9739d = i2;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(map);
        this.f9743h = map;
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(cls, "Resource class must not be null");
        this.f9740e = cls;
        com.fossil.blesdk.obfuscated.C2992tw.m14458a(cls2, "Transcode class must not be null");
        this.f9741f = cls2;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(loVar);
        this.f9744i = loVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2980tp)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2980tp tpVar = (com.fossil.blesdk.obfuscated.C2980tp) obj;
        if (!this.f9737b.equals(tpVar.f9737b) || !this.f9742g.equals(tpVar.f9742g) || this.f9739d != tpVar.f9739d || this.f9738c != tpVar.f9738c || !this.f9743h.equals(tpVar.f9743h) || !this.f9740e.equals(tpVar.f9740e) || !this.f9741f.equals(tpVar.f9741f) || !this.f9744i.equals(tpVar.f9744i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        if (this.f9745j == 0) {
            this.f9745j = this.f9737b.hashCode();
            this.f9745j = (this.f9745j * 31) + this.f9742g.hashCode();
            this.f9745j = (this.f9745j * 31) + this.f9738c;
            this.f9745j = (this.f9745j * 31) + this.f9739d;
            this.f9745j = (this.f9745j * 31) + this.f9743h.hashCode();
            this.f9745j = (this.f9745j * 31) + this.f9740e.hashCode();
            this.f9745j = (this.f9745j * 31) + this.f9741f.hashCode();
            this.f9745j = (this.f9745j * 31) + this.f9744i.hashCode();
        }
        return this.f9745j;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "EngineKey{model=" + this.f9737b + ", width=" + this.f9738c + ", height=" + this.f9739d + ", resourceClass=" + this.f9740e + ", transcodeClass=" + this.f9741f + ", signature=" + this.f9742g + ", hashCode=" + this.f9745j + ", transformations=" + this.f9743h + ", options=" + this.f9744i + '}';
    }
}
