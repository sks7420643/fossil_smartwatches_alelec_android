package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fj */
public interface C1804fj {
    @android.annotation.SuppressLint({"SyntheticAccessor"})

    @DexIgnore
    /* renamed from: a */
    public static final com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1809c f5195a = new com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1809c();
    @android.annotation.SuppressLint({"SyntheticAccessor"})

    @DexIgnore
    /* renamed from: b */
    public static final com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1808b f5196b = new com.fossil.blesdk.obfuscated.C1804fj.C1806b.C1808b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fj$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fj$b */
    public static abstract class C1806b {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fj$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.fj$b$a */
        public static final class C1807a extends com.fossil.blesdk.obfuscated.C1804fj.C1806b {

            @DexIgnore
            /* renamed from: a */
            public /* final */ java.lang.Throwable f5197a;

            @DexIgnore
            public C1807a(java.lang.Throwable th) {
                this.f5197a = th;
            }

            @DexIgnore
            /* renamed from: a */
            public java.lang.Throwable mo10949a() {
                return this.f5197a;
            }

            @DexIgnore
            public java.lang.String toString() {
                return java.lang.String.format("FAILURE (%s)", new java.lang.Object[]{this.f5197a.getMessage()});
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fj$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.fj$b$b */
        public static final class C1808b extends com.fossil.blesdk.obfuscated.C1804fj.C1806b {
            @DexIgnore
            public java.lang.String toString() {
                return "IN_PROGRESS";
            }

            @DexIgnore
            public C1808b() {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fj$b$c")
        /* renamed from: com.fossil.blesdk.obfuscated.fj$b$c */
        public static final class C1809c extends com.fossil.blesdk.obfuscated.C1804fj.C1806b {
            @DexIgnore
            public java.lang.String toString() {
                return "SUCCESS";
            }

            @DexIgnore
            public C1809c() {
            }
        }
    }
}
