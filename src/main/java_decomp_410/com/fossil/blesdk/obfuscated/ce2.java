package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ce2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView t;

    @DexIgnore
    public ce2(Object obj, View view, int i, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = imageView;
        this.s = imageView2;
        this.t = alphabetFastScrollRecyclerView;
    }
}
