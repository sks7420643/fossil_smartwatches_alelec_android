package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bi2 extends ai2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.ftv_day_of_week, 1);
        B.put(R.id.ftv_day_of_month, 2);
        B.put(R.id.line, 3);
        B.put(R.id.cl_container, 4);
        B.put(R.id.ftv_resting_value, 5);
        B.put(R.id.ftv_resting_unit, 6);
        B.put(R.id.separatedLine, 7);
        B.put(R.id.ftv_max_value, 8);
        B.put(R.id.ftv_max_unit, 9);
        B.put(R.id.ftv_no_record, 10);
        B.put(R.id.ftv_indicator, 11);
    }
    */

    @DexIgnore
    public bi2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public bi2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[4], objArr[0], objArr[2], objArr[1], objArr[11], objArr[9], objArr[8], objArr[10], objArr[6], objArr[5], objArr[3], objArr[7]);
        this.z = -1;
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
