package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ne2 extends me2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.customToolbar, 1);
        x.put(R.id.iv_back, 2);
        x.put(R.id.cl_subcribe_email, 3);
        x.put(R.id.ftv_subcribe_email, 4);
        x.put(R.id.sc_subcribe_email, 5);
        x.put(R.id.ftv_description_subcribe_email, 6);
        x.put(R.id.cl_share_data, 7);
        x.put(R.id.title_share_data, 8);
        x.put(R.id.anonymous_switch, 9);
    }
    */

    @DexIgnore
    public ne2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 10, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ne2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[9], objArr[7], objArr[3], objArr[1], objArr[6], objArr[4], objArr[2], objArr[5], objArr[8]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
