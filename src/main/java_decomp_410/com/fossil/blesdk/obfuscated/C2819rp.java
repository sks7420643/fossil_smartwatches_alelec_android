package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rp */
public class C2819rp<R> implements com.bumptech.glide.load.engine.DecodeJob.C0378b<R>, com.fossil.blesdk.obfuscated.C3145vw.C3151f {

    @DexIgnore
    /* renamed from: C */
    public static /* final */ com.fossil.blesdk.obfuscated.C2819rp.C2822c f9069C; // = new com.fossil.blesdk.obfuscated.C2819rp.C2822c();

    @DexIgnore
    /* renamed from: A */
    public com.bumptech.glide.load.engine.DecodeJob<R> f9070A;

    @DexIgnore
    /* renamed from: B */
    public volatile boolean f9071B;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2819rp.C2824e f9072e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3324xw f9073f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C3128vp.C3129a f9074g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C2819rp<?>> f9075h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C2819rp.C2822c f9076i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C2905sp f9077j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.fossil.blesdk.obfuscated.C1650dr f9078k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ com.fossil.blesdk.obfuscated.C1650dr f9079l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ com.fossil.blesdk.obfuscated.C1650dr f9080m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ com.fossil.blesdk.obfuscated.C1650dr f9081n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ java.util.concurrent.atomic.AtomicInteger f9082o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C2143jo f9083p;

    @DexIgnore
    /* renamed from: q */
    public boolean f9084q;

    @DexIgnore
    /* renamed from: r */
    public boolean f9085r;

    @DexIgnore
    /* renamed from: s */
    public boolean f9086s;

    @DexIgnore
    /* renamed from: t */
    public boolean f9087t;

    @DexIgnore
    /* renamed from: u */
    public com.fossil.blesdk.obfuscated.C1438aq<?> f9088u;

    @DexIgnore
    /* renamed from: v */
    public com.bumptech.glide.load.DataSource f9089v;

    @DexIgnore
    /* renamed from: w */
    public boolean f9090w;

    @DexIgnore
    /* renamed from: x */
    public com.bumptech.glide.load.engine.GlideException f9091x;

    @DexIgnore
    /* renamed from: y */
    public boolean f9092y;

    @DexIgnore
    /* renamed from: z */
    public com.fossil.blesdk.obfuscated.C3128vp<?> f9093z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rp$a */
    public class C2820a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C2924sv f9094e;

        @DexIgnore
        public C2820a(com.fossil.blesdk.obfuscated.C2924sv svVar) {
            this.f9094e = svVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.f9094e.mo4026a()) {
                synchronized (com.fossil.blesdk.obfuscated.C2819rp.this) {
                    if (com.fossil.blesdk.obfuscated.C2819rp.this.f9072e.mo15703a(this.f9094e)) {
                        com.fossil.blesdk.obfuscated.C2819rp.this.mo15684a(this.f9094e);
                    }
                    com.fossil.blesdk.obfuscated.C2819rp.this.mo15686b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$b")
    /* renamed from: com.fossil.blesdk.obfuscated.rp$b */
    public class C2821b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C2924sv f9096e;

        @DexIgnore
        public C2821b(com.fossil.blesdk.obfuscated.C2924sv svVar) {
            this.f9096e = svVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.f9096e.mo4026a()) {
                synchronized (com.fossil.blesdk.obfuscated.C2819rp.this) {
                    if (com.fossil.blesdk.obfuscated.C2819rp.this.f9072e.mo15703a(this.f9096e)) {
                        com.fossil.blesdk.obfuscated.C2819rp.this.f9093z.mo17153d();
                        com.fossil.blesdk.obfuscated.C2819rp.this.mo15688b(this.f9096e);
                        com.fossil.blesdk.obfuscated.C2819rp.this.mo15690c(this.f9096e);
                    }
                    com.fossil.blesdk.obfuscated.C2819rp.this.mo15686b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$c")
    /* renamed from: com.fossil.blesdk.obfuscated.rp$c */
    public static class C2822c {
        @DexIgnore
        /* renamed from: a */
        public <R> com.fossil.blesdk.obfuscated.C3128vp<R> mo15698a(com.fossil.blesdk.obfuscated.C1438aq<R> aqVar, boolean z, com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar) {
            com.fossil.blesdk.obfuscated.C3128vp vpVar = new com.fossil.blesdk.obfuscated.C3128vp(aqVar, z, true, joVar, aVar);
            return vpVar;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$d")
    /* renamed from: com.fossil.blesdk.obfuscated.rp$d */
    public static final class C2823d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2924sv f9098a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.util.concurrent.Executor f9099b;

        @DexIgnore
        public C2823d(com.fossil.blesdk.obfuscated.C2924sv svVar, java.util.concurrent.Executor executor) {
            this.f9098a = svVar;
            this.f9099b = executor;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (obj instanceof com.fossil.blesdk.obfuscated.C2819rp.C2823d) {
                return this.f9098a.equals(((com.fossil.blesdk.obfuscated.C2819rp.C2823d) obj).f9098a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.f9098a.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rp$e")
    /* renamed from: com.fossil.blesdk.obfuscated.rp$e */
    public static final class C2824e implements java.lang.Iterable<com.fossil.blesdk.obfuscated.C2819rp.C2823d> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2819rp.C2823d> f9100e;

        @DexIgnore
        public C2824e() {
            this(new java.util.ArrayList(2));
        }

        @DexIgnore
        /* renamed from: c */
        public static com.fossil.blesdk.obfuscated.C2819rp.C2823d m13342c(com.fossil.blesdk.obfuscated.C2924sv svVar) {
            return new com.fossil.blesdk.obfuscated.C2819rp.C2823d(svVar, com.fossil.blesdk.obfuscated.C2606ow.m11981a());
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15702a(com.fossil.blesdk.obfuscated.C2924sv svVar, java.util.concurrent.Executor executor) {
            this.f9100e.add(new com.fossil.blesdk.obfuscated.C2819rp.C2823d(svVar, executor));
        }

        @DexIgnore
        /* renamed from: b */
        public void mo15704b(com.fossil.blesdk.obfuscated.C2924sv svVar) {
            this.f9100e.remove(m13342c(svVar));
        }

        @DexIgnore
        public void clear() {
            this.f9100e.clear();
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.f9100e.isEmpty();
        }

        @DexIgnore
        public java.util.Iterator<com.fossil.blesdk.obfuscated.C2819rp.C2823d> iterator() {
            return this.f9100e.iterator();
        }

        @DexIgnore
        public int size() {
            return this.f9100e.size();
        }

        @DexIgnore
        public C2824e(java.util.List<com.fossil.blesdk.obfuscated.C2819rp.C2823d> list) {
            this.f9100e = list;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo15703a(com.fossil.blesdk.obfuscated.C2924sv svVar) {
            return this.f9100e.contains(m13342c(svVar));
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2819rp.C2824e mo15701a() {
            return new com.fossil.blesdk.obfuscated.C2819rp.C2824e(new java.util.ArrayList(this.f9100e));
        }
    }

    @DexIgnore
    public C2819rp(com.fossil.blesdk.obfuscated.C1650dr drVar, com.fossil.blesdk.obfuscated.C1650dr drVar2, com.fossil.blesdk.obfuscated.C1650dr drVar3, com.fossil.blesdk.obfuscated.C1650dr drVar4, com.fossil.blesdk.obfuscated.C2905sp spVar, com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar, com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C2819rp<?>> g8Var) {
        this(drVar, drVar2, drVar3, drVar4, spVar, aVar, g8Var, f9069C);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized com.fossil.blesdk.obfuscated.C2819rp<R> mo15681a(com.fossil.blesdk.obfuscated.C2143jo joVar, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f9083p = joVar;
        this.f9084q = z;
        this.f9085r = z2;
        this.f9086s = z3;
        this.f9087t = z4;
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized void mo15687b(com.bumptech.glide.load.engine.DecodeJob<R> decodeJob) {
        this.f9070A = decodeJob;
        (decodeJob.mo3972u() ? this.f9078k : mo15689c()).execute(decodeJob);
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized void mo15690c(com.fossil.blesdk.obfuscated.C2924sv svVar) {
        boolean z;
        this.f9073f.mo17919a();
        this.f9072e.mo15704b(svVar);
        if (this.f9072e.isEmpty()) {
            mo15682a();
            if (!this.f9090w) {
                if (!this.f9092y) {
                    z = false;
                    if (z && this.f9082o.get() == 0) {
                        mo15695h();
                    }
                }
            }
            z = true;
            mo15695h();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final boolean mo15691d() {
        return this.f9092y || this.f9090w || this.f9071B;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.f9077j.mo15338a(r4, r1, (com.fossil.blesdk.obfuscated.C3128vp<?>) null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.f9099b.execute(new com.fossil.blesdk.obfuscated.C2819rp.C2820a(r4, r1.f9098a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        mo15686b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: e */
    public void mo15692e() {
        synchronized (this) {
            this.f9073f.mo17919a();
            if (this.f9071B) {
                mo15695h();
            } else if (this.f9072e.isEmpty()) {
                throw new java.lang.IllegalStateException("Received an exception without any callbacks to notify");
            } else if (!this.f9092y) {
                this.f9092y = true;
                com.fossil.blesdk.obfuscated.C2143jo joVar = this.f9083p;
                com.fossil.blesdk.obfuscated.C2819rp.C2824e a = this.f9072e.mo15701a();
                mo15683a(a.size() + 1);
            } else {
                throw new java.lang.IllegalStateException("Already failed once");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.f9077j.mo15338a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.f9099b.execute(new com.fossil.blesdk.obfuscated.C2819rp.C2821b(r5, r1.f9098a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        mo15686b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: f */
    public void mo15693f() {
        synchronized (this) {
            this.f9073f.mo17919a();
            if (this.f9071B) {
                this.f9088u.mo8887a();
                mo15695h();
            } else if (this.f9072e.isEmpty()) {
                throw new java.lang.IllegalStateException("Received a resource without any callbacks to notify");
            } else if (!this.f9090w) {
                this.f9093z = this.f9076i.mo15698a(this.f9088u, this.f9084q, this.f9083p, this.f9074g);
                this.f9090w = true;
                com.fossil.blesdk.obfuscated.C2819rp.C2824e a = this.f9072e.mo15701a();
                mo15683a(a.size() + 1);
                com.fossil.blesdk.obfuscated.C2143jo joVar = this.f9083p;
                com.fossil.blesdk.obfuscated.C3128vp<?> vpVar = this.f9093z;
            } else {
                throw new java.lang.IllegalStateException("Already have resource");
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo15694g() {
        return this.f9087t;
    }

    @DexIgnore
    /* renamed from: h */
    public final synchronized void mo15695h() {
        if (this.f9083p != null) {
            this.f9072e.clear();
            this.f9083p = null;
            this.f9093z = null;
            this.f9088u = null;
            this.f9092y = false;
            this.f9071B = false;
            this.f9090w = false;
            this.f9070A.mo3955a(false);
            this.f9070A = null;
            this.f9091x = null;
            this.f9089v = null;
            this.f9075h.mo11163a(this);
        } else {
            throw new java.lang.IllegalArgumentException();
        }
    }

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C3324xw mo3959i() {
        return this.f9073f;
    }

    @DexIgnore
    public C2819rp(com.fossil.blesdk.obfuscated.C1650dr drVar, com.fossil.blesdk.obfuscated.C1650dr drVar2, com.fossil.blesdk.obfuscated.C1650dr drVar3, com.fossil.blesdk.obfuscated.C1650dr drVar4, com.fossil.blesdk.obfuscated.C2905sp spVar, com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar, com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C2819rp<?>> g8Var, com.fossil.blesdk.obfuscated.C2819rp.C2822c cVar) {
        this.f9072e = new com.fossil.blesdk.obfuscated.C2819rp.C2824e();
        this.f9073f = com.fossil.blesdk.obfuscated.C3324xw.m16582b();
        this.f9082o = new java.util.concurrent.atomic.AtomicInteger();
        this.f9078k = drVar;
        this.f9079l = drVar2;
        this.f9080m = drVar3;
        this.f9081n = drVar4;
        this.f9077j = spVar;
        this.f9074g = aVar;
        this.f9075h = g8Var;
        this.f9076i = cVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15688b(com.fossil.blesdk.obfuscated.C2924sv svVar) {
        try {
            svVar.mo4030a(this.f9093z, this.f9089v);
        } catch (Throwable th) {
            throw new com.bumptech.glide.load.engine.CallbackException(th);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo15685a(com.fossil.blesdk.obfuscated.C2924sv svVar, java.util.concurrent.Executor executor) {
        this.f9073f.mo17919a();
        this.f9072e.mo15702a(svVar, executor);
        boolean z = true;
        if (this.f9090w) {
            mo15683a(1);
            executor.execute(new com.fossil.blesdk.obfuscated.C2819rp.C2821b(svVar));
        } else if (this.f9092y) {
            mo15683a(1);
            executor.execute(new com.fossil.blesdk.obfuscated.C2819rp.C2820a(svVar));
        } else {
            if (this.f9071B) {
                z = false;
            }
            com.fossil.blesdk.obfuscated.C2992tw.m14461a(z, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15686b() {
        com.fossil.blesdk.obfuscated.C3128vp<?> vpVar;
        synchronized (this) {
            this.f9073f.mo17919a();
            com.fossil.blesdk.obfuscated.C2992tw.m14461a(mo15691d(), "Not yet complete!");
            int decrementAndGet = this.f9082o.decrementAndGet();
            com.fossil.blesdk.obfuscated.C2992tw.m14461a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                vpVar = this.f9093z;
                mo15695h();
            } else {
                vpVar = null;
            }
        }
        if (vpVar != null) {
            vpVar.mo17156g();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final com.fossil.blesdk.obfuscated.C1650dr mo15689c() {
        if (this.f9085r) {
            return this.f9080m;
        }
        return this.f9086s ? this.f9081n : this.f9079l;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15684a(com.fossil.blesdk.obfuscated.C2924sv svVar) {
        try {
            svVar.mo4028a(this.f9091x);
        } catch (Throwable th) {
            throw new com.bumptech.glide.load.engine.CallbackException(th);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15682a() {
        if (!mo15691d()) {
            this.f9071B = true;
            this.f9070A.mo3961k();
            this.f9077j.mo15337a(this, this.f9083p);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo15683a(int i) {
        com.fossil.blesdk.obfuscated.C2992tw.m14461a(mo15691d(), "Not yet complete!");
        if (this.f9082o.getAndAdd(i) == 0 && this.f9093z != null) {
            this.f9093z.mo17153d();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3975a(com.fossil.blesdk.obfuscated.C1438aq<R> aqVar, com.bumptech.glide.load.DataSource dataSource) {
        synchronized (this) {
            this.f9088u = aqVar;
            this.f9089v = dataSource;
        }
        mo15693f();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3974a(com.bumptech.glide.load.engine.GlideException glideException) {
        synchronized (this) {
            this.f9091x = glideException;
        }
        mo15692e();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3973a(com.bumptech.glide.load.engine.DecodeJob<?> decodeJob) {
        mo15689c().execute(decodeJob);
    }
}
