package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.yl4;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.internal.http2.ErrorCode;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mn4 implements zm4 {
    @DexIgnore
    public static /* final */ List<String> f; // = jm4.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority"});
    @DexIgnore
    public static /* final */ List<String> g; // = jm4.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade"});
    @DexIgnore
    public /* final */ Interceptor.Chain a;
    @DexIgnore
    public /* final */ wm4 b;
    @DexIgnore
    public /* final */ nn4 c;
    @DexIgnore
    public pn4 d;
    @DexIgnore
    public /* final */ Protocol e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends oo4 {
        @DexIgnore
        public boolean f; // = false;
        @DexIgnore
        public long g; // = 0;

        @DexIgnore
        public a(yo4 yo4) {
            super(yo4);
        }

        @DexIgnore
        public final void a(IOException iOException) {
            if (!this.f) {
                this.f = true;
                mn4 mn4 = mn4.this;
                mn4.b.a(false, mn4, this.g, iOException);
            }
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            try {
                long b = c().b(jo4, j);
                if (b > 0) {
                    this.g += b;
                }
                return b;
            } catch (IOException e) {
                a(e);
                throw e;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            super.close();
            a((IOException) null);
        }
    }

    @DexIgnore
    public mn4(OkHttpClient okHttpClient, Interceptor.Chain chain, wm4 wm4, nn4 nn4) {
        Protocol protocol;
        this.a = chain;
        this.b = wm4;
        this.c = nn4;
        if (okHttpClient.A().contains(Protocol.H2_PRIOR_KNOWLEDGE)) {
            protocol = Protocol.H2_PRIOR_KNOWLEDGE;
        } else {
            protocol = Protocol.HTTP_2;
        }
        this.e = protocol;
    }

    @DexIgnore
    public xo4 a(dm4 dm4, long j) {
        return this.d.d();
    }

    @DexIgnore
    public void b() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    public void cancel() {
        pn4 pn4 = this.d;
        if (pn4 != null) {
            pn4.c(ErrorCode.CANCEL);
        }
    }

    @DexIgnore
    public static List<jn4> b(dm4 dm4) {
        yl4 c2 = dm4.c();
        ArrayList arrayList = new ArrayList(c2.b() + 4);
        arrayList.add(new jn4(jn4.f, dm4.e()));
        arrayList.add(new jn4(jn4.g, fn4.a(dm4.g())));
        String a2 = dm4.a("Host");
        if (a2 != null) {
            arrayList.add(new jn4(jn4.i, a2));
        }
        arrayList.add(new jn4(jn4.h, dm4.g().n()));
        int b2 = c2.b();
        for (int i = 0; i < b2; i++) {
            ByteString encodeUtf8 = ByteString.encodeUtf8(c2.a(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new jn4(encodeUtf8, c2.b(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(dm4 dm4) throws IOException {
        if (this.d == null) {
            this.d = this.c.a(b(dm4), dm4.a() != null);
            this.d.h().a((long) this.a.a(), TimeUnit.MILLISECONDS);
            this.d.l().a((long) this.a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.d().close();
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        Response.a a2 = a(this.d.j(), this.e);
        if (!z || hm4.a.a(a2) != 100) {
            return a2;
        }
        return null;
    }

    @DexIgnore
    public static Response.a a(yl4 yl4, Protocol protocol) throws IOException {
        yl4.a aVar = new yl4.a();
        int b2 = yl4.b();
        hn4 hn4 = null;
        for (int i = 0; i < b2; i++) {
            String a2 = yl4.a(i);
            String b3 = yl4.b(i);
            if (a2.equals(":status")) {
                hn4 = hn4.a("HTTP/1.1 " + b3);
            } else if (!g.contains(a2)) {
                hm4.a.a(aVar, a2, b3);
            }
        }
        if (hn4 != null) {
            Response.a aVar2 = new Response.a();
            aVar2.a(protocol);
            aVar2.a(hn4.b);
            aVar2.a(hn4.c);
            aVar2.a(aVar.a());
            return aVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    public em4 a(Response response) throws IOException {
        wm4 wm4 = this.b;
        wm4.f.e(wm4.e);
        return new en4(response.e(GraphRequest.CONTENT_TYPE_HEADER), bn4.a(response), so4.a((yo4) new a(this.d.e())));
    }
}
