package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l63 extends bs2<k63> {
    @DexIgnore
    void D();

    @DexIgnore
    void a(g13 g13);

    @DexIgnore
    void b(String str, String str2);

    @DexIgnore
    void b(String str, String str2, String str3);

    @DexIgnore
    void c();

    @DexIgnore
    void f(boolean z);

    @DexIgnore
    void g(boolean z);

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void n(String str);

    @DexIgnore
    void p();

    @DexIgnore
    void q();
}
