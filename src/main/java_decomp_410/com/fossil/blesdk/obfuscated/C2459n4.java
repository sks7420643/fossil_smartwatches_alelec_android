package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n4 */
public class C2459n4 implements com.fossil.blesdk.obfuscated.C2703q4.C2704a {

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable f7655a; // = null;

    @DexIgnore
    /* renamed from: b */
    public float f7656b; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    /* renamed from: c */
    public boolean f7657c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2375m4 f7658d;

    @DexIgnore
    /* renamed from: e */
    public boolean f7659e; // = false;

    @DexIgnore
    public C2459n4(com.fossil.blesdk.obfuscated.C2526o4 o4Var) {
        this.f7658d = new com.fossil.blesdk.obfuscated.C2375m4(this, o4Var);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13833a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.f7656b = (float) i;
        }
        if (!z) {
            this.f7658d.mo13509a(solverVariable, -1.0f);
            this.f7658d.mo13509a(solverVariable2, 1.0f);
        } else {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo13846b() {
        androidx.constraintlayout.solver.SolverVariable solverVariable = this.f7655a;
        return solverVariable != null && (solverVariable.f644g == androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED || this.f7656b >= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13849c(androidx.constraintlayout.solver.SolverVariable solverVariable, int i) {
        if (i < 0) {
            this.f7656b = (float) (i * -1);
            this.f7658d.mo13509a(solverVariable, 1.0f);
        } else {
            this.f7656b = (float) i;
            this.f7658d.mo13509a(solverVariable, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public void clear() {
        this.f7658d.mo13507a();
        this.f7655a = null;
        this.f7656b = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo13852d() {
        this.f7655a = null;
        this.f7658d.mo13507a();
        this.f7656b = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f7659e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ce  */
    /* renamed from: e */
    public java.lang.String mo13854e() {
        java.lang.String str;
        boolean z;
        java.lang.String str2;
        java.lang.String str3;
        if (this.f7655a == null) {
            str = "" + "0";
        } else {
            str = "" + this.f7655a;
        }
        java.lang.String str4 = str + " = ";
        if (this.f7656b != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            str4 = str4 + this.f7656b;
            z = true;
        } else {
            z = false;
        }
        int i = this.f7658d.f7388a;
        for (int i2 = 0; i2 < i; i2++) {
            androidx.constraintlayout.solver.SolverVariable a = this.f7658d.mo13504a(i2);
            if (a != null) {
                float b = this.f7658d.mo13515b(i2);
                int i3 = (b > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (b == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i3 != 0) {
                    java.lang.String solverVariable = a.toString();
                    if (!z) {
                        if (b < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            str2 = str2 + "- ";
                        }
                        if (b == 1.0f) {
                            str3 = str2 + solverVariable;
                        } else {
                            str3 = str2 + b + " " + solverVariable;
                        }
                        z = true;
                    } else if (i3 > 0) {
                        str2 = str2 + " + ";
                        if (b == 1.0f) {
                        }
                        z = true;
                    } else {
                        str2 = str2 + " - ";
                    }
                    b *= -1.0f;
                    if (b == 1.0f) {
                    }
                    z = true;
                }
            }
        }
        if (z) {
            return str2;
        }
        return str2 + "0.0";
    }

    @DexIgnore
    public androidx.constraintlayout.solver.SolverVariable getKey() {
        return this.f7655a;
    }

    @DexIgnore
    public java.lang.String toString() {
        return mo13854e();
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo13847b(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        return this.f7658d.mo13513a(solverVariable);
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13843b(androidx.constraintlayout.solver.SolverVariable solverVariable, int i) {
        this.f7655a = solverVariable;
        float f = (float) i;
        solverVariable.f642e = f;
        this.f7656b = f;
        this.f7659e = true;
        return this;
    }

    @DexIgnore
    /* renamed from: c */
    public androidx.constraintlayout.solver.SolverVariable mo13848c(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        return this.f7658d.mo13506a((boolean[]) null, solverVariable);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo13853d(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        androidx.constraintlayout.solver.SolverVariable solverVariable2 = this.f7655a;
        if (solverVariable2 != null) {
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7655a = null;
        }
        float a = this.f7658d.mo13503a(solverVariable, true) * -1.0f;
        this.f7655a = solverVariable;
        if (a != 1.0f) {
            this.f7656b /= a;
            this.f7658d.mo13508a(a);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13832a(androidx.constraintlayout.solver.SolverVariable solverVariable, int i) {
        this.f7658d.mo13509a(solverVariable, (float) i);
        return this;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo13850c() {
        return this.f7655a == null && this.f7656b == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.f7658d.f7388a == 0;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13836a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.f7656b = (float) i;
        }
        if (!z) {
            this.f7658d.mo13509a(solverVariable, -1.0f);
            this.f7658d.mo13509a(solverVariable2, 1.0f);
            this.f7658d.mo13509a(solverVariable3, 1.0f);
        } else {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7658d.mo13509a(solverVariable3, -1.0f);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13844b(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.f7656b = (float) i;
        }
        if (!z) {
            this.f7658d.mo13509a(solverVariable, -1.0f);
            this.f7658d.mo13509a(solverVariable2, 1.0f);
            this.f7658d.mo13509a(solverVariable3, -1.0f);
        } else {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7658d.mo13509a(solverVariable3, 1.0f);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13831a(float f, float f2, float f3, androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4) {
        this.f7656b = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7658d.mo13509a(solverVariable4, 1.0f);
            this.f7658d.mo13509a(solverVariable3, -1.0f);
        } else if (f == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
        } else if (f3 == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f7658d.mo13509a(solverVariable3, 1.0f);
            this.f7658d.mo13509a(solverVariable4, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7658d.mo13509a(solverVariable4, f4);
            this.f7658d.mo13509a(solverVariable3, -f4);
        }
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13845b(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4, float f) {
        this.f7658d.mo13509a(solverVariable3, 0.5f);
        this.f7658d.mo13509a(solverVariable4, 0.5f);
        this.f7658d.mo13509a(solverVariable, -0.5f);
        this.f7658d.mo13509a(solverVariable2, -0.5f);
        this.f7656b = -f;
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13834a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, int i, float f, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4, int i2) {
        if (solverVariable2 == solverVariable3) {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable4, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -2.0f);
            return this;
        }
        if (f == 0.5f) {
            this.f7658d.mo13509a(solverVariable, 1.0f);
            this.f7658d.mo13509a(solverVariable2, -1.0f);
            this.f7658d.mo13509a(solverVariable3, -1.0f);
            this.f7658d.mo13509a(solverVariable4, 1.0f);
            if (i > 0 || i2 > 0) {
                this.f7656b = (float) ((-i) + i2);
            }
        } else if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f7658d.mo13509a(solverVariable, -1.0f);
            this.f7658d.mo13509a(solverVariable2, 1.0f);
            this.f7656b = (float) i;
        } else if (f >= 1.0f) {
            this.f7658d.mo13509a(solverVariable3, -1.0f);
            this.f7658d.mo13509a(solverVariable4, 1.0f);
            this.f7656b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.f7658d.mo13509a(solverVariable, f2 * 1.0f);
            this.f7658d.mo13509a(solverVariable2, f2 * -1.0f);
            this.f7658d.mo13509a(solverVariable3, -1.0f * f);
            this.f7658d.mo13509a(solverVariable4, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.f7656b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13838a(com.fossil.blesdk.obfuscated.C2703q4 q4Var, int i) {
        this.f7658d.mo13509a(q4Var.mo15008a(i, "ep"), 1.0f);
        this.f7658d.mo13509a(q4Var.mo15008a(i, com.facebook.appevents.UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13835a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, float f) {
        this.f7658d.mo13509a(solverVariable, -1.0f);
        this.f7658d.mo13509a(solverVariable2, 1.0f - f);
        this.f7658d.mo13509a(solverVariable3, f);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2459n4 mo13837a(androidx.constraintlayout.solver.SolverVariable solverVariable, androidx.constraintlayout.solver.SolverVariable solverVariable2, androidx.constraintlayout.solver.SolverVariable solverVariable3, androidx.constraintlayout.solver.SolverVariable solverVariable4, float f) {
        this.f7658d.mo13509a(solverVariable, -1.0f);
        this.f7658d.mo13509a(solverVariable2, 1.0f);
        this.f7658d.mo13509a(solverVariable3, f);
        this.f7658d.mo13509a(solverVariable4, -f);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13839a() {
        float f = this.f7656b;
        if (f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.f7656b = f * -1.0f;
            this.f7658d.mo13517b();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13842a(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        boolean z;
        androidx.constraintlayout.solver.SolverVariable a = this.f7658d.mo13505a(q4Var);
        if (a == null) {
            z = true;
        } else {
            mo13853d(a);
            z = false;
        }
        if (this.f7658d.f7388a == 0) {
            this.f7659e = true;
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable mo13830a(com.fossil.blesdk.obfuscated.C2703q4 q4Var, boolean[] zArr) {
        return this.f7658d.mo13506a(zArr, (androidx.constraintlayout.solver.SolverVariable) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13841a(com.fossil.blesdk.obfuscated.C2703q4.C2704a aVar) {
        if (aVar instanceof com.fossil.blesdk.obfuscated.C2459n4) {
            com.fossil.blesdk.obfuscated.C2459n4 n4Var = (com.fossil.blesdk.obfuscated.C2459n4) aVar;
            this.f7655a = null;
            this.f7658d.mo13507a();
            int i = 0;
            while (true) {
                com.fossil.blesdk.obfuscated.C2375m4 m4Var = n4Var.f7658d;
                if (i < m4Var.f7388a) {
                    this.f7658d.mo13510a(m4Var.mo13504a(i), n4Var.f7658d.mo13515b(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13840a(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        int i = solverVariable.f641d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.f7658d.mo13509a(solverVariable, f);
    }
}
