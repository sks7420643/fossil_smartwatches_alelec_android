package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pl1 extends SSLSocket {
    @DexIgnore
    public /* final */ SSLSocket e;

    @DexIgnore
    public pl1(ol1 ol1, SSLSocket sSLSocket) {
        this.e = sSLSocket;
    }

    @DexIgnore
    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.e.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    public final void bind(SocketAddress socketAddress) throws IOException {
        this.e.bind(socketAddress);
    }

    @DexIgnore
    public final synchronized void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public final void connect(SocketAddress socketAddress) throws IOException {
        this.e.connect(socketAddress);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return this.e.equals(obj);
    }

    @DexIgnore
    public final SocketChannel getChannel() {
        return this.e.getChannel();
    }

    @DexIgnore
    public final boolean getEnableSessionCreation() {
        return this.e.getEnableSessionCreation();
    }

    @DexIgnore
    public final String[] getEnabledCipherSuites() {
        return this.e.getEnabledCipherSuites();
    }

    @DexIgnore
    public final String[] getEnabledProtocols() {
        return this.e.getEnabledProtocols();
    }

    @DexIgnore
    public final InetAddress getInetAddress() {
        return this.e.getInetAddress();
    }

    @DexIgnore
    public final InputStream getInputStream() throws IOException {
        return this.e.getInputStream();
    }

    @DexIgnore
    public final boolean getKeepAlive() throws SocketException {
        return this.e.getKeepAlive();
    }

    @DexIgnore
    public final InetAddress getLocalAddress() {
        return this.e.getLocalAddress();
    }

    @DexIgnore
    public final int getLocalPort() {
        return this.e.getLocalPort();
    }

    @DexIgnore
    public final SocketAddress getLocalSocketAddress() {
        return this.e.getLocalSocketAddress();
    }

    @DexIgnore
    public final boolean getNeedClientAuth() {
        return this.e.getNeedClientAuth();
    }

    @DexIgnore
    public final boolean getOOBInline() throws SocketException {
        return this.e.getOOBInline();
    }

    @DexIgnore
    public final OutputStream getOutputStream() throws IOException {
        return this.e.getOutputStream();
    }

    @DexIgnore
    public final int getPort() {
        return this.e.getPort();
    }

    @DexIgnore
    public final synchronized int getReceiveBufferSize() throws SocketException {
        return this.e.getReceiveBufferSize();
    }

    @DexIgnore
    public final SocketAddress getRemoteSocketAddress() {
        return this.e.getRemoteSocketAddress();
    }

    @DexIgnore
    public final boolean getReuseAddress() throws SocketException {
        return this.e.getReuseAddress();
    }

    @DexIgnore
    public final synchronized int getSendBufferSize() throws SocketException {
        return this.e.getSendBufferSize();
    }

    @DexIgnore
    public final SSLSession getSession() {
        return this.e.getSession();
    }

    @DexIgnore
    public final int getSoLinger() throws SocketException {
        return this.e.getSoLinger();
    }

    @DexIgnore
    public final synchronized int getSoTimeout() throws SocketException {
        return this.e.getSoTimeout();
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.e.getSupportedCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedProtocols() {
        return this.e.getSupportedProtocols();
    }

    @DexIgnore
    public final boolean getTcpNoDelay() throws SocketException {
        return this.e.getTcpNoDelay();
    }

    @DexIgnore
    public final int getTrafficClass() throws SocketException {
        return this.e.getTrafficClass();
    }

    @DexIgnore
    public final boolean getUseClientMode() {
        return this.e.getUseClientMode();
    }

    @DexIgnore
    public final boolean getWantClientAuth() {
        return this.e.getWantClientAuth();
    }

    @DexIgnore
    public final boolean isBound() {
        return this.e.isBound();
    }

    @DexIgnore
    public final boolean isClosed() {
        return this.e.isClosed();
    }

    @DexIgnore
    public final boolean isConnected() {
        return this.e.isConnected();
    }

    @DexIgnore
    public final boolean isInputShutdown() {
        return this.e.isInputShutdown();
    }

    @DexIgnore
    public final boolean isOutputShutdown() {
        return this.e.isOutputShutdown();
    }

    @DexIgnore
    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.e.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    public final void sendUrgentData(int i) throws IOException {
        this.e.sendUrgentData(i);
    }

    @DexIgnore
    public final void setEnableSessionCreation(boolean z) {
        this.e.setEnableSessionCreation(z);
    }

    @DexIgnore
    public final void setEnabledCipherSuites(String[] strArr) {
        this.e.setEnabledCipherSuites(strArr);
    }

    @DexIgnore
    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.e.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.e.setEnabledProtocols(strArr);
    }

    @DexIgnore
    public final void setKeepAlive(boolean z) throws SocketException {
        this.e.setKeepAlive(z);
    }

    @DexIgnore
    public final void setNeedClientAuth(boolean z) {
        this.e.setNeedClientAuth(z);
    }

    @DexIgnore
    public final void setOOBInline(boolean z) throws SocketException {
        this.e.setOOBInline(z);
    }

    @DexIgnore
    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.e.setPerformancePreferences(i, i2, i3);
    }

    @DexIgnore
    public final synchronized void setReceiveBufferSize(int i) throws SocketException {
        this.e.setReceiveBufferSize(i);
    }

    @DexIgnore
    public final void setReuseAddress(boolean z) throws SocketException {
        this.e.setReuseAddress(z);
    }

    @DexIgnore
    public final synchronized void setSendBufferSize(int i) throws SocketException {
        this.e.setSendBufferSize(i);
    }

    @DexIgnore
    public final void setSoLinger(boolean z, int i) throws SocketException {
        this.e.setSoLinger(z, i);
    }

    @DexIgnore
    public final synchronized void setSoTimeout(int i) throws SocketException {
        this.e.setSoTimeout(i);
    }

    @DexIgnore
    public final void setTcpNoDelay(boolean z) throws SocketException {
        this.e.setTcpNoDelay(z);
    }

    @DexIgnore
    public final void setTrafficClass(int i) throws SocketException {
        this.e.setTrafficClass(i);
    }

    @DexIgnore
    public final void setUseClientMode(boolean z) {
        this.e.setUseClientMode(z);
    }

    @DexIgnore
    public final void setWantClientAuth(boolean z) {
        this.e.setWantClientAuth(z);
    }

    @DexIgnore
    public final void shutdownInput() throws IOException {
        this.e.shutdownInput();
    }

    @DexIgnore
    public final void shutdownOutput() throws IOException {
        this.e.shutdownOutput();
    }

    @DexIgnore
    public final void startHandshake() throws IOException {
        this.e.startHandshake();
    }

    @DexIgnore
    public final String toString() {
        return this.e.toString();
    }

    @DexIgnore
    public final void connect(SocketAddress socketAddress, int i) throws IOException {
        this.e.connect(socketAddress, i);
    }
}
