package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ss1 extends Transition {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ TextView a;

        @DexIgnore
        public a(ss1 ss1, TextView textView) {
            this.a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public void a(ph phVar) {
        d(phVar);
    }

    @DexIgnore
    public void c(ph phVar) {
        d(phVar);
    }

    @DexIgnore
    public final void d(ph phVar) {
        View view = phVar.b;
        if (view instanceof TextView) {
            phVar.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    public Animator a(ViewGroup viewGroup, ph phVar, ph phVar2) {
        if (phVar == null || phVar2 == null || !(phVar.b instanceof TextView)) {
            return null;
        }
        View view = phVar2.b;
        if (!(view instanceof TextView)) {
            return null;
        }
        TextView textView = (TextView) view;
        Map<String, Object> map = phVar.a;
        Map<String, Object> map2 = phVar2.a;
        float f = 1.0f;
        float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
        if (map2.get("android:textscale:scale") != null) {
            f = ((Float) map2.get("android:textscale:scale")).floatValue();
        }
        if (floatValue == f) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{floatValue, f});
        ofFloat.addUpdateListener(new a(this, textView));
        return ofFloat;
    }
}
