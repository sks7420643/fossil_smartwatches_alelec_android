package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oh1 {
    @DexIgnore
    public /* final */ rh1 a;

    @DexIgnore
    public oh1(rh1 rh1) {
        bk0.a(rh1);
        this.a = rh1;
    }

    @DexIgnore
    public static boolean a(Context context) {
        bk0.a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null) {
                return false;
            }
            ActivityInfo receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0);
            if (receiverInfo == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final void a(Context context, Intent intent) {
        xh1 a2 = xh1.a(context, (og1) null);
        tg1 d = a2.d();
        if (intent == null) {
            d.v().a("Receiver called with null intent");
            return;
        }
        a2.b();
        String action = intent.getAction();
        d.A().a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            d.A().a("Starting wakeful intent.");
            this.a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                a2.a().a((Runnable) new ph1(this, a2, d));
            } catch (Exception e) {
                d.v().a("Install Referrer Reporter encountered a problem", e);
            }
            BroadcastReceiver.PendingResult a3 = this.a.a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                d.A().a("Install referrer extras are null");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            d.y().a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            Bundle a4 = a2.s().a(Uri.parse(stringExtra));
            if (a4 == null) {
                d.A().a("No campaign defined in install referrer broadcast");
                if (a3 != null) {
                    a3.finish();
                    return;
                }
                return;
            }
            long longExtra = intent.getLongExtra("referrer_timestamp_seconds", 0) * 1000;
            if (longExtra == 0) {
                d.v().a("Install referrer is missing timestamp");
            }
            a2.a().a((Runnable) new qh1(this, a2, longExtra, a4, context, d, a3));
        }
    }
}
