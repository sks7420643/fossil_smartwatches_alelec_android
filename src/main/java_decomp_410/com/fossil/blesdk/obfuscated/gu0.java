package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.nu0;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gu0<T extends nu0<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract ku0<T> a(Object obj);

    @DexIgnore
    public abstract void a(ox0 ox0, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean a(sv0 sv0);

    @DexIgnore
    public abstract ku0<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
