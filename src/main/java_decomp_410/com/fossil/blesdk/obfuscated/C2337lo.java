package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lo */
public final class C2337lo implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1855g4<com.fossil.blesdk.obfuscated.C2232ko<?>, java.lang.Object> f7250b; // = new com.fossil.blesdk.obfuscated.C2355lw();

    @DexIgnore
    /* renamed from: a */
    public void mo13320a(com.fossil.blesdk.obfuscated.C2337lo loVar) {
        this.f7250b.mo1220a(loVar.f7250b);
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj instanceof com.fossil.blesdk.obfuscated.C2337lo) {
            return this.f7250b.equals(((com.fossil.blesdk.obfuscated.C2337lo) obj).f7250b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f7250b.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Options{values=" + this.f7250b + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public <T> com.fossil.blesdk.obfuscated.C2337lo mo13318a(com.fossil.blesdk.obfuscated.C2232ko<T> koVar, T t) {
        this.f7250b.put(koVar, t);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public <T> T mo13319a(com.fossil.blesdk.obfuscated.C2232ko<T> koVar) {
        return this.f7250b.containsKey(koVar) ? this.f7250b.get(koVar) : koVar.mo12839a();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        for (int i = 0; i < this.f7250b.size(); i++) {
            m10232a(this.f7250b.mo1224c(i), this.f7250b.mo1229e(i), messageDigest);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> void m10232a(com.fossil.blesdk.obfuscated.C2232ko<T> koVar, java.lang.Object obj, java.security.MessageDigest messageDigest) {
        koVar.mo12840a(obj, messageDigest);
    }
}
