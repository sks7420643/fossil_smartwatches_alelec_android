package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ls2 extends RecyclerView.g<a> {
    @DexIgnore
    public List<String> a;
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ ls2 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ls2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ls2$a$a  reason: collision with other inner class name */
        public static final class C0094a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0094a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (this.e.getAdapterPosition() != -1) {
                    b b = this.e.b.b;
                    if (b != null) {
                        List a = this.e.b.a;
                        if (a != null) {
                            b.a((String) a.get(adapterPosition));
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ls2 ls2, View view) {
            super(view);
            kd4.b(view, "view");
            this.b = ls2;
            view.setOnClickListener(new C0094a(this));
            View findViewById = view.findViewById(R.id.tv_text);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public int getItemCount() {
        List<String> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        TextView a2 = aVar.a();
        List<String> list = this.a;
        if (list != null) {
            a2.setText(list.get(i));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_address_search, viewGroup, false);
        kd4.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<String> list) {
        kd4.b(list, "addressSearchList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.b = bVar;
    }
}
