package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a9 */
public class C1405a9 {

    @DexIgnore
    /* renamed from: a */
    public int f3423a;

    @DexIgnore
    public C1405a9(android.view.ViewGroup viewGroup) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8637a(android.view.View view, android.view.View view2, int i) {
        mo8638a(view, view2, i, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8638a(android.view.View view, android.view.View view2, int i, int i2) {
        this.f3423a = i;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo8634a() {
        return this.f3423a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8635a(android.view.View view) {
        mo8636a(view, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8636a(android.view.View view, int i) {
        this.f3423a = 0;
    }
}
