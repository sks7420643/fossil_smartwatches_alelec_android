package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Binder;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wc0 extends rc0 {
    @DexIgnore
    public /* final */ Context e;

    @DexIgnore
    public wc0(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void k() {
        o();
        cc0 a = cc0.a(this.e);
        GoogleSignInAccount b = a.b();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.s;
        if (b != null) {
            googleSignInOptions = a.c();
        }
        ge0.a aVar = new ge0.a(this.e);
        aVar.a(qb0.e, googleSignInOptions);
        ge0 a2 = aVar.a();
        try {
            if (a2.a().L()) {
                if (b != null) {
                    qb0.f.a(a2);
                } else {
                    a2.b();
                }
            }
        } finally {
            a2.d();
        }
    }

    @DexIgnore
    public final void l() {
        o();
        pc0.a(this.e).a();
    }

    @DexIgnore
    public final void o() {
        if (!zd0.isGooglePlayServicesUid(this.e, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }
}
