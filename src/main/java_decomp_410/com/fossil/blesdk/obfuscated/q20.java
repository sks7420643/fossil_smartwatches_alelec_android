package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.utils.Crc32Calculator;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class q20<T> extends k20<T> {
    @DexIgnore
    public q20() {
        super(new Version((byte) 1, (byte) 0));
    }

    @DexIgnore
    public byte[] a(short s, T t) {
        kd4.b(t, "entries");
        byte[] a = a(t);
        byte[] array = ByteBuffer.allocate(a.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put(a().getMajor()).put(a().getMinor()).putInt(0).putInt(a.length).put(a).putInt((int) Crc32Calculator.a.a(a, Crc32Calculator.CrcType.CRC32)).array();
        kd4.a((Object) array, "result.array()");
        return array;
    }
}
