package com.fossil.blesdk.obfuscated;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ym0 {
    @DexIgnore
    public static boolean a() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}
