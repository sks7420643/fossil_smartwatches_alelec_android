package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h3 extends j3 {
    @DexIgnore
    public static volatile h3 c;
    @DexIgnore
    public static /* final */ Executor d; // = new a();
    @DexIgnore
    public static /* final */ Executor e; // = new b();
    @DexIgnore
    public j3 a; // = this.b;
    @DexIgnore
    public j3 b; // = new i3();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            h3.c().c(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            h3.c().a(runnable);
        }
    }

    @DexIgnore
    public static Executor b() {
        return e;
    }

    @DexIgnore
    public static h3 c() {
        if (c != null) {
            return c;
        }
        synchronized (h3.class) {
            if (c == null) {
                c = new h3();
            }
        }
        return c;
    }

    @DexIgnore
    public static Executor d() {
        return d;
    }

    @DexIgnore
    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    @DexIgnore
    public boolean a() {
        return this.a.a();
    }

    @DexIgnore
    public void c(Runnable runnable) {
        this.a.c(runnable);
    }
}
