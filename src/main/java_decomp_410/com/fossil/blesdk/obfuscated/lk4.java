package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.TimeoutCancellationException;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lk4 {
    @DexIgnore
    public static final <T, R> Object a(uf4<? super T> uf4, R r, yc4<? super R, ? super yb4<? super T>, ? extends Object> yc4) {
        Object obj;
        kd4.b(uf4, "$this$startUndispatchedOrReturn");
        kd4.b(yc4, "block");
        uf4.k();
        try {
            qd4.a((Object) yc4, 2);
            obj = yc4.invoke(r, uf4);
        } catch (Throwable th) {
            obj = new ng4(th, false, 2, (fd4) null);
        }
        if (obj == cc4.a()) {
            return cc4.a();
        }
        if (!uf4.b(obj, 4)) {
            return cc4.a();
        }
        Object d = uf4.d();
        if (!(d instanceof ng4)) {
            return mi4.b(d);
        }
        throw bk4.a(uf4, ((ng4) d).a);
    }

    @DexIgnore
    public static final <T, R> Object b(uf4<? super T> uf4, R r, yc4<? super R, ? super yb4<? super T>, ? extends Object> yc4) {
        Object obj;
        kd4.b(uf4, "$this$startUndispatchedOrReturnIgnoreTimeout");
        kd4.b(yc4, "block");
        uf4.k();
        boolean z = false;
        try {
            qd4.a((Object) yc4, 2);
            obj = yc4.invoke(r, uf4);
        } catch (Throwable th) {
            obj = new ng4(th, false, 2, (fd4) null);
        }
        if (obj == cc4.a()) {
            return cc4.a();
        }
        if (!uf4.b(obj, 4)) {
            return cc4.a();
        }
        Object d = uf4.d();
        if (!(d instanceof ng4)) {
            return mi4.b(d);
        }
        ng4 ng4 = (ng4) d;
        Throwable th2 = ng4.a;
        if (!(th2 instanceof TimeoutCancellationException) || ((TimeoutCancellationException) th2).coroutine != uf4) {
            z = true;
        }
        if (z) {
            throw bk4.a(uf4, ng4.a);
        } else if (!(obj instanceof ng4)) {
            return obj;
        } else {
            throw bk4.a(uf4, ((ng4) obj).a);
        }
    }

    @DexIgnore
    public static final <T> void a(xc4<? super yb4<? super T>, ? extends Object> xc4, yb4<? super T> yb4) {
        CoroutineContext context;
        Object b;
        kd4.b(xc4, "$this$startCoroutineUndispatched");
        kd4.b(yb4, "completion");
        ic4.a(yb4);
        try {
            context = yb4.getContext();
            b = ThreadContextKt.b(context, (Object) null);
            qd4.a((Object) xc4, 1);
            Object invoke = xc4.invoke(yb4);
            ThreadContextKt.a(context, b);
            if (invoke != cc4.a()) {
                Result.a aVar = Result.Companion;
                yb4.resumeWith(Result.m3constructorimpl(invoke));
            }
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(yc4<? super R, ? super yb4<? super T>, ? extends Object> yc4, R r, yb4<? super T> yb4) {
        CoroutineContext context;
        Object b;
        kd4.b(yc4, "$this$startCoroutineUndispatched");
        kd4.b(yb4, "completion");
        ic4.a(yb4);
        try {
            context = yb4.getContext();
            b = ThreadContextKt.b(context, (Object) null);
            qd4.a((Object) yc4, 2);
            Object invoke = yc4.invoke(r, yb4);
            ThreadContextKt.a(context, b);
            if (invoke != cc4.a()) {
                Result.a aVar = Result.Companion;
                yb4.resumeWith(Result.m3constructorimpl(invoke));
            }
        } catch (Throwable th) {
            Result.a aVar2 = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        }
    }
}
