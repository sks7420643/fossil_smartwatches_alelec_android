package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class fz2 extends u52 {
    @DexIgnore
    public boolean f;

    @DexIgnore
    public abstract void a(int i, boolean z, boolean z2);

    @DexIgnore
    public abstract void a(ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    public final void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public abstract void b(ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    public abstract void c(ArrayList<String> arrayList);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public final boolean i() {
        return this.f;
    }

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
