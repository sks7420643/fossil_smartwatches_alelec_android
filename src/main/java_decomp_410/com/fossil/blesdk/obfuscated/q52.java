package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.WatchParamHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q52 implements Factory<WatchParamHelper> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public q52(n42 n42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static q52 a(n42 n42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new q52(n42, provider, provider2);
    }

    @DexIgnore
    public static WatchParamHelper b(n42 n42, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return a(n42, provider.get(), provider2.get());
    }

    @DexIgnore
    public static WatchParamHelper a(n42 n42, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        WatchParamHelper a2 = n42.a(deviceRepository, portfolioApp);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public WatchParamHelper get() {
        return b(this.a, this.b, this.c);
    }
}
