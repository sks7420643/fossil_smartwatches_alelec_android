package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k42 implements Factory<j42> {
    @DexIgnore
    public /* final */ Provider<Map<Class<? extends ic>, Provider<ic>>> a;

    @DexIgnore
    public k42(Provider<Map<Class<? extends ic>, Provider<ic>>> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static k42 a(Provider<Map<Class<? extends ic>, Provider<ic>>> provider) {
        return new k42(provider);
    }

    @DexIgnore
    public static j42 b(Provider<Map<Class<? extends ic>, Provider<ic>>> provider) {
        return new j42(provider.get());
    }

    @DexIgnore
    public j42 get() {
        return b(this.a);
    }
}
