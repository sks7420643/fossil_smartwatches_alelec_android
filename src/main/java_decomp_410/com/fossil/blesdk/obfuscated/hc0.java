package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hc0 implements wb0 {
    @DexIgnore
    public final he0<Status> a(ge0 ge0) {
        return jc0.b(ge0, ge0.e(), false);
    }

    @DexIgnore
    public final zb0 a(Intent intent) {
        return jc0.a(intent);
    }
}
