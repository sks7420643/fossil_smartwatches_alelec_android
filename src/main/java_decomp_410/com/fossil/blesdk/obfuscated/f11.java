package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f11 extends nz0 implements e11 {
    @DexIgnore
    public f11(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
    }

    @DexIgnore
    public final void a(aq0 aq0) throws RemoteException {
        Parcel o = o();
        y01.a(o, (Parcelable) aq0);
        a(3, o);
    }
}
