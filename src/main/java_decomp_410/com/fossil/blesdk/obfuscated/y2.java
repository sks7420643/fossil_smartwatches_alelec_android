package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y2 extends q2 {
    @DexIgnore
    public /* final */ WeakReference<Context> b;

    @DexIgnore
    public y2(Context context, Resources resources) {
        super(resources);
        this.b = new WeakReference<>(context);
    }

    @DexIgnore
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = (Context) this.b.get();
        if (!(drawable == null || context == null)) {
            c2.a();
            c2.a(context, i, drawable);
        }
        return drawable;
    }
}
