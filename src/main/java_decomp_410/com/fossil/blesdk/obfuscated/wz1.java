package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wz1 extends JsonElement {
    @DexIgnore
    public static /* final */ wz1 a; // = new wz1();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof wz1);
    }

    @DexIgnore
    public int hashCode() {
        return wz1.class.hashCode();
    }
}
