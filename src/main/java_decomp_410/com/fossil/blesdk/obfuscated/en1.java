package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class en1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<en1> CREATOR; // = new fn1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ ck0 f;

    @DexIgnore
    public en1(int i, ck0 ck0) {
        this.e = i;
        this.f = ck0;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, (Parcelable) this.f, i, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public en1(ck0 ck0) {
        this(1, ck0);
    }
}
