package com.fossil.blesdk.obfuscated;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class km0 implements hm0 {
    @DexIgnore
    public static /* final */ km0 a; // = new km0();

    @DexIgnore
    public static hm0 d() {
        return a;
    }

    @DexIgnore
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
