package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.od */
public abstract class C2545od<Key, Value> extends com.fossil.blesdk.obfuscated.C2120jd<Key, Value> {
    @DexIgnore
    public /* final */ java.lang.Object mKeyLock; // = new java.lang.Object();
    @DexIgnore
    public Key mNextKey; // = null;
    @DexIgnore
    public Key mPreviousKey; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$a")
    /* renamed from: com.fossil.blesdk.obfuscated.od$a */
    public static abstract class C2546a<Key, Value> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo14338a(java.util.List<Value> list, Key key);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$b")
    /* renamed from: com.fossil.blesdk.obfuscated.od$b */
    public static class C2547b<Key, Value> extends com.fossil.blesdk.obfuscated.C2545od.C2546a<Key, Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2312d<Value> f8047a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2545od<Key, Value> f8048b;

        @DexIgnore
        public C2547b(com.fossil.blesdk.obfuscated.C2545od<Key, Value> odVar, int i, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f8047a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(odVar, i, executor, aVar);
            this.f8048b = odVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14338a(java.util.List<Value> list, Key key) {
            if (!this.f8047a.mo13247a()) {
                if (this.f8047a.f7183a == 1) {
                    this.f8048b.setNextKey(key);
                } else {
                    this.f8048b.setPreviousKey(key);
                }
                this.f8047a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$c")
    /* renamed from: com.fossil.blesdk.obfuscated.od$c */
    public static abstract class C2548c<Key, Value> {
        @DexIgnore
        /* renamed from: a */
        public abstract void mo14339a(java.util.List<Value> list, Key key, Key key2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$d")
    /* renamed from: com.fossil.blesdk.obfuscated.od$d */
    public static class C2549d<Key, Value> extends com.fossil.blesdk.obfuscated.C2545od.C2548c<Key, Value> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2307ld.C2312d<Value> f8049a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C2545od<Key, Value> f8050b;

        @DexIgnore
        public C2549d(com.fossil.blesdk.obfuscated.C2545od<Key, Value> odVar, boolean z, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
            this.f8049a = new com.fossil.blesdk.obfuscated.C2307ld.C2312d<>(odVar, 0, (java.util.concurrent.Executor) null, aVar);
            this.f8050b = odVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14339a(java.util.List<Value> list, Key key, Key key2) {
            if (!this.f8049a.mo13247a()) {
                this.f8050b.initKeys(key, key2);
                this.f8049a.mo13245a(new com.fossil.blesdk.obfuscated.C2644pd(list, 0, 0, 0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$e")
    /* renamed from: com.fossil.blesdk.obfuscated.od$e */
    public static class C2550e<Key> {
        @DexIgnore
        public C2550e(int i, boolean z) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.od$f")
    /* renamed from: com.fossil.blesdk.obfuscated.od$f */
    public static class C2551f<Key> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ Key f8051a;

        @DexIgnore
        public C2551f(Key key, int i) {
            this.f8051a = key;
        }
    }

    @DexIgnore
    private Key getNextKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mNextKey;
        }
        return key;
    }

    @DexIgnore
    private Key getPreviousKey() {
        Key key;
        synchronized (this.mKeyLock) {
            key = this.mPreviousKey;
        }
        return key;
    }

    @DexIgnore
    public final void dispatchLoadAfter(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        java.lang.Object nextKey = getNextKey();
        if (nextKey != null) {
            loadAfter(new com.fossil.blesdk.obfuscated.C2545od.C2551f(nextKey, i2), new com.fossil.blesdk.obfuscated.C2545od.C2547b(this, 1, executor, aVar));
        } else {
            aVar.mo12730a(1, com.fossil.blesdk.obfuscated.C2644pd.m12217b());
        }
    }

    @DexIgnore
    public final void dispatchLoadBefore(int i, Value value, int i2, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        java.lang.Object previousKey = getPreviousKey();
        if (previousKey != null) {
            loadBefore(new com.fossil.blesdk.obfuscated.C2545od.C2551f(previousKey, i2), new com.fossil.blesdk.obfuscated.C2545od.C2547b(this, 2, executor, aVar));
        } else {
            aVar.mo12730a(2, com.fossil.blesdk.obfuscated.C2644pd.m12217b());
        }
    }

    @DexIgnore
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, java.util.concurrent.Executor executor, com.fossil.blesdk.obfuscated.C2644pd.C2645a<Value> aVar) {
        com.fossil.blesdk.obfuscated.C2545od.C2549d dVar = new com.fossil.blesdk.obfuscated.C2545od.C2549d(this, z, aVar);
        loadInitial(new com.fossil.blesdk.obfuscated.C2545od.C2550e(i, z), dVar);
        dVar.f8049a.mo13246a(executor);
    }

    @DexIgnore
    public final Key getKey(int i, Value value) {
        return null;
    }

    @DexIgnore
    public void initKeys(Key key, Key key2) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
            this.mNextKey = key2;
        }
    }

    @DexIgnore
    public abstract void loadAfter(com.fossil.blesdk.obfuscated.C2545od.C2551f<Key> fVar, com.fossil.blesdk.obfuscated.C2545od.C2546a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadBefore(com.fossil.blesdk.obfuscated.C2545od.C2551f<Key> fVar, com.fossil.blesdk.obfuscated.C2545od.C2546a<Key, Value> aVar);

    @DexIgnore
    public abstract void loadInitial(com.fossil.blesdk.obfuscated.C2545od.C2550e<Key> eVar, com.fossil.blesdk.obfuscated.C2545od.C2548c<Key, Value> cVar);

    @DexIgnore
    public void setNextKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mNextKey = key;
        }
    }

    @DexIgnore
    public void setPreviousKey(Key key) {
        synchronized (this.mKeyLock) {
            this.mPreviousKey = key;
        }
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return false;
    }

    @DexIgnore
    public final <ToValue> com.fossil.blesdk.obfuscated.C2545od<Key, ToValue> map(com.fossil.blesdk.obfuscated.C2374m3<Value, ToValue> m3Var) {
        return mapByPage((com.fossil.blesdk.obfuscated.C2374m3) com.fossil.blesdk.obfuscated.C2307ld.createListFunction(m3Var));
    }

    @DexIgnore
    public final <ToValue> com.fossil.blesdk.obfuscated.C2545od<Key, ToValue> mapByPage(com.fossil.blesdk.obfuscated.C2374m3<java.util.List<Value>, java.util.List<ToValue>> m3Var) {
        return new com.fossil.blesdk.obfuscated.C3355yd(this, m3Var);
    }
}
