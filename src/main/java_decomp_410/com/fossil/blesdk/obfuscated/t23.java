package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.enums.DirectionBy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class t23 extends u52 {
    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2, DirectionBy directionBy, boolean z, String str3);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract CommuteTimeSetting i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
