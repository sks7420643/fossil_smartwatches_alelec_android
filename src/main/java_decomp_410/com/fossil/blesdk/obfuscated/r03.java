package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r03 implements Factory<NotificationHybridContactPresenter> {
    @DexIgnore
    public static NotificationHybridContactPresenter a(l03 l03, int i, ArrayList<ContactWrapper> arrayList, LoaderManager loaderManager, j62 j62, t03 t03) {
        return new NotificationHybridContactPresenter(l03, i, arrayList, loaderManager, j62, t03);
    }
}
