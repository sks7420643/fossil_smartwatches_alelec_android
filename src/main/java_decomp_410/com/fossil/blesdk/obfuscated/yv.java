package com.fossil.blesdk.obfuscated;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.fossil.blesdk.obfuscated.ew;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class yv<Z> extends cw<ImageView, Z> implements ew.a {
    @DexIgnore
    public Animatable k;

    @DexIgnore
    public yv(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        super.a(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        super.b(drawable);
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public void c(Drawable drawable) {
        super.c(drawable);
        Animatable animatable = this.k;
        if (animatable != null) {
            animatable.stop();
        }
        d((Object) null);
        d(drawable);
    }

    @DexIgnore
    public abstract void c(Z z);

    @DexIgnore
    public void d(Drawable drawable) {
        ((ImageView) this.e).setImageDrawable(drawable);
    }

    @DexIgnore
    public final void d(Z z) {
        c(z);
        b(z);
    }

    @DexIgnore
    public void a(Z z, ew<? super Z> ewVar) {
        if (ewVar == null || !ewVar.a(z, this)) {
            d(z);
        } else {
            b(z);
        }
    }

    @DexIgnore
    public final void b(Z z) {
        if (z instanceof Animatable) {
            this.k = (Animatable) z;
            this.k.start();
            return;
        }
        this.k = null;
    }

    @DexIgnore
    public void c() {
        Animatable animatable = this.k;
        if (animatable != null) {
            animatable.stop();
        }
    }

    @DexIgnore
    public void a() {
        Animatable animatable = this.k;
        if (animatable != null) {
            animatable.start();
        }
    }
}
