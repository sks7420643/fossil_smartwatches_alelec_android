package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ka0 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ ha0 b;

    @DexIgnore
    public ka0(ha0 ha0) {
        kd4.b(ha0, "microAppHandle");
        this.b = ha0;
        byte[] array = ByteBuffer.allocate(this.b.a().length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).put((byte) 0).array();
        kd4.a((Object) array, "ByteBuffer.allocate(micr\u2026x00)\n            .array()");
        this.a = array;
    }

    @DexIgnore
    public final byte[] a() {
        return this.a;
    }
}
