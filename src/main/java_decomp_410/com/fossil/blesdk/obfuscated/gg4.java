package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gg4 extends ng4 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater c; // = AtomicIntegerFieldUpdater.newUpdater(gg4.class, "_resumed");
    @DexIgnore
    public volatile int _resumed;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public gg4(yb4<?> yb4, Throwable th, boolean z) {
        super(th, z);
        kd4.b(yb4, "continuation");
        if (th == null) {
            th = new CancellationException("Continuation " + yb4 + " was cancelled normally");
        }
        this._resumed = 0;
    }

    @DexIgnore
    public final boolean c() {
        return c.compareAndSet(this, 0, 1);
    }
}
