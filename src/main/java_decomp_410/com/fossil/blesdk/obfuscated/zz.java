package com.fossil.blesdk.obfuscated;

import android.app.ActivityManager;
import com.crashlytics.android.core.CodedOutputStream;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import io.fabric.sdk.android.services.common.IdManager;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zz {
    @DexIgnore
    public static /* final */ oy a; // = oy.a("0");
    @DexIgnore
    public static /* final */ oy b; // = oy.a("Unity");

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, String str, String str2, long j) throws Exception {
        codedOutputStream.a(1, oy.a(str2));
        codedOutputStream.a(2, oy.a(str));
        codedOutputStream.a(3, j);
    }

    @DexIgnore
    public static int b(oy oyVar) {
        return CodedOutputStream.b(1, oyVar) + 0;
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, String str, String str2, String str3, String str4, String str5, int i, String str6) throws Exception {
        oy a2 = oy.a(str);
        oy a3 = oy.a(str2);
        oy a4 = oy.a(str3);
        oy a5 = oy.a(str4);
        oy a6 = oy.a(str5);
        oy a7 = str6 != null ? oy.a(str6) : null;
        codedOutputStream.c(7, 2);
        codedOutputStream.e(a(a2, a3, a4, a5, a6, i, a7));
        codedOutputStream.a(1, a2);
        codedOutputStream.a(2, a4);
        codedOutputStream.a(3, a5);
        codedOutputStream.c(5, 2);
        codedOutputStream.e(b(a3));
        codedOutputStream.a(1, a3);
        codedOutputStream.a(6, a6);
        if (a7 != null) {
            codedOutputStream.a(8, b);
            codedOutputStream.a(9, a7);
        }
        codedOutputStream.a(10, i);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, String str, String str2, boolean z) throws Exception {
        oy a2 = oy.a(str);
        oy a3 = oy.a(str2);
        codedOutputStream.c(8, 2);
        codedOutputStream.e(a(a2, a3, z));
        codedOutputStream.a(1, 3);
        codedOutputStream.a(2, a2);
        codedOutputStream.a(3, a3);
        codedOutputStream.a(4, z);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, int i, String str, int i2, long j, long j2, boolean z, Map<IdManager.DeviceIdentifierType, String> map, int i3, String str2, String str3) throws Exception {
        CodedOutputStream codedOutputStream2 = codedOutputStream;
        oy a2 = a(str);
        oy a3 = a(str3);
        oy a4 = a(str2);
        codedOutputStream2.c(9, 2);
        oy oyVar = a4;
        codedOutputStream2.e(a(i, a2, i2, j, j2, z, map, i3, a4, a3));
        codedOutputStream2.a(3, i);
        codedOutputStream2.a(4, a2);
        codedOutputStream2.d(5, i2);
        codedOutputStream2.a(6, j);
        codedOutputStream2.a(7, j2);
        codedOutputStream2.a(10, z);
        for (Map.Entry next : map.entrySet()) {
            codedOutputStream2.c(11, 2);
            codedOutputStream2.e(a((IdManager.DeviceIdentifierType) next.getKey(), (String) next.getValue()));
            codedOutputStream2.a(1, ((IdManager.DeviceIdentifierType) next.getKey()).protobufIndex);
            codedOutputStream2.a(2, oy.a((String) next.getValue()));
        }
        codedOutputStream2.d(12, i3);
        if (oyVar != null) {
            codedOutputStream2.a(13, oyVar);
        }
        if (a3 != null) {
            codedOutputStream2.a(14, a3);
        }
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, String str, String str2, String str3) throws Exception {
        if (str == null) {
            str = "";
        }
        oy a2 = oy.a(str);
        oy a3 = a(str2);
        oy a4 = a(str3);
        int b2 = CodedOutputStream.b(1, a2) + 0;
        if (str2 != null) {
            b2 += CodedOutputStream.b(2, a3);
        }
        if (str3 != null) {
            b2 += CodedOutputStream.b(3, a4);
        }
        codedOutputStream.c(6, 2);
        codedOutputStream.e(b2);
        codedOutputStream.a(1, a2);
        if (str2 != null) {
            codedOutputStream.a(2, a3);
        }
        if (str3 != null) {
            codedOutputStream.a(3, a4);
        }
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, long j, String str, d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, Map<String, String> map, jz jzVar, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i, String str2, String str3, Float f, int i2, boolean z, long j2, long j3) throws Exception {
        oy oyVar;
        CodedOutputStream codedOutputStream2 = codedOutputStream;
        String str4 = str3;
        oy a2 = oy.a(str2);
        if (str4 == null) {
            oyVar = null;
        } else {
            oyVar = oy.a(str4.replace(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, ""));
        }
        oy oyVar2 = oyVar;
        oy b2 = jzVar.b();
        if (b2 == null) {
            q44.g().d("CrashlyticsCore", "No log data to include with this event.");
        }
        jzVar.a();
        codedOutputStream2.c(10, 2);
        codedOutputStream2.e(a(j, str, d00, thread, stackTraceElementArr, threadArr, list, 8, map, runningAppProcessInfo, i, a2, oyVar2, f, i2, z, j2, j3, b2));
        codedOutputStream2.a(1, j);
        codedOutputStream2.a(2, oy.a(str));
        a(codedOutputStream, d00, thread, stackTraceElementArr, threadArr, list, 8, a2, oyVar2, map, runningAppProcessInfo, i);
        a(codedOutputStream, f, i2, z, i, j2, j3);
        a(codedOutputStream2, b2);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, oy oyVar, oy oyVar2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) throws Exception {
        codedOutputStream.c(3, 2);
        codedOutputStream.e(a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2, map, runningAppProcessInfo, i2));
        a(codedOutputStream, d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2);
        if (map != null && !map.isEmpty()) {
            a(codedOutputStream, map);
        }
        if (runningAppProcessInfo != null) {
            codedOutputStream.a(3, runningAppProcessInfo.importance != 100);
        }
        codedOutputStream.d(4, i2);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, oy oyVar, oy oyVar2) throws Exception {
        codedOutputStream.c(1, 2);
        codedOutputStream.e(a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2));
        a(codedOutputStream, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            a(codedOutputStream, threadArr[i2], list.get(i2), 0, false);
        }
        a(codedOutputStream, d00, 1, i, 2);
        codedOutputStream.c(3, 2);
        codedOutputStream.e(a());
        codedOutputStream.a(1, a);
        codedOutputStream.a(2, a);
        codedOutputStream.a(3, 0);
        codedOutputStream.c(4, 2);
        codedOutputStream.e(a(oyVar, oyVar2));
        codedOutputStream.a(1, 0);
        codedOutputStream.a(2, 0);
        codedOutputStream.a(3, oyVar);
        if (oyVar2 != null) {
            codedOutputStream.a(4, oyVar2);
        }
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, Map<String, String> map) throws Exception {
        for (Map.Entry next : map.entrySet()) {
            codedOutputStream.c(2, 2);
            codedOutputStream.e(a((String) next.getKey(), (String) next.getValue()));
            codedOutputStream.a(1, oy.a((String) next.getKey()));
            String str = (String) next.getValue();
            if (str == null) {
                str = "";
            }
            codedOutputStream.a(2, oy.a(str));
        }
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, d00 d00, int i, int i2, int i3) throws Exception {
        codedOutputStream.c(i3, 2);
        codedOutputStream.e(a(d00, 1, i2));
        codedOutputStream.a(1, oy.a(d00.b));
        String str = d00.a;
        if (str != null) {
            codedOutputStream.a(3, oy.a(str));
        }
        int i4 = 0;
        for (StackTraceElement a2 : d00.c) {
            a(codedOutputStream, 4, a2, true);
        }
        d00 d002 = d00.d;
        if (d002 == null) {
            return;
        }
        if (i < i2) {
            a(codedOutputStream, d002, i + 1, i2, 6);
            return;
        }
        while (d002 != null) {
            d002 = d002.d;
            i4++;
        }
        codedOutputStream.d(7, i4);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) throws Exception {
        codedOutputStream.c(1, 2);
        codedOutputStream.e(a(thread, stackTraceElementArr, i, z));
        codedOutputStream.a(1, oy.a(thread.getName()));
        codedOutputStream.d(2, i);
        for (StackTraceElement a2 : stackTraceElementArr) {
            a(codedOutputStream, 3, a2, z);
        }
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, int i, StackTraceElement stackTraceElement, boolean z) throws Exception {
        codedOutputStream.c(i, 2);
        codedOutputStream.e(a(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            codedOutputStream.a(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            codedOutputStream.a(1, 0);
        }
        codedOutputStream.a(2, oy.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            codedOutputStream.a(3, oy.a(stackTraceElement.getFileName()));
        }
        int i2 = 4;
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            codedOutputStream.a(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i2 = 0;
        }
        codedOutputStream.d(5, i2);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, Float f, int i, boolean z, int i2, long j, long j2) throws Exception {
        codedOutputStream.c(5, 2);
        codedOutputStream.e(a(f, i, z, i2, j, j2));
        if (f != null) {
            codedOutputStream.a(1, f.floatValue());
        }
        codedOutputStream.b(2, i);
        codedOutputStream.a(3, z);
        codedOutputStream.d(4, i2);
        codedOutputStream.a(5, j);
        codedOutputStream.a(6, j2);
    }

    @DexIgnore
    public static void a(CodedOutputStream codedOutputStream, oy oyVar) throws Exception {
        if (oyVar != null) {
            codedOutputStream.c(6, 2);
            codedOutputStream.e(a(oyVar));
            codedOutputStream.a(1, oyVar);
        }
    }

    @DexIgnore
    public static int a(oy oyVar, oy oyVar2, oy oyVar3, oy oyVar4, oy oyVar5, int i, oy oyVar6) {
        int b2 = b(oyVar2);
        int b3 = CodedOutputStream.b(1, oyVar) + 0 + CodedOutputStream.b(2, oyVar3) + CodedOutputStream.b(3, oyVar4) + CodedOutputStream.l(5) + CodedOutputStream.j(b2) + b2 + CodedOutputStream.b(6, oyVar5);
        if (oyVar6 != null) {
            b3 = b3 + CodedOutputStream.b(8, b) + CodedOutputStream.b(9, oyVar6);
        }
        return b3 + CodedOutputStream.e(10, i);
    }

    @DexIgnore
    public static int a(oy oyVar, oy oyVar2, boolean z) {
        return CodedOutputStream.e(1, 3) + 0 + CodedOutputStream.b(2, oyVar) + CodedOutputStream.b(3, oyVar2) + CodedOutputStream.b(4, z);
    }

    @DexIgnore
    public static int a(IdManager.DeviceIdentifierType deviceIdentifierType, String str) {
        return CodedOutputStream.e(1, deviceIdentifierType.protobufIndex) + CodedOutputStream.b(2, oy.a(str));
    }

    @DexIgnore
    public static int a(int i, oy oyVar, int i2, long j, long j2, boolean z, Map<IdManager.DeviceIdentifierType, String> map, int i3, oy oyVar2, oy oyVar3) {
        int i4;
        int i5;
        int i6 = 0;
        int e = CodedOutputStream.e(3, i) + 0;
        if (oyVar == null) {
            i4 = 0;
        } else {
            i4 = CodedOutputStream.b(4, oyVar);
        }
        int g = e + i4 + CodedOutputStream.g(5, i2) + CodedOutputStream.b(6, j) + CodedOutputStream.b(7, j2) + CodedOutputStream.b(10, z);
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a2 = a((IdManager.DeviceIdentifierType) next.getKey(), (String) next.getValue());
                g += CodedOutputStream.l(11) + CodedOutputStream.j(a2) + a2;
            }
        }
        int g2 = g + CodedOutputStream.g(12, i3);
        if (oyVar2 == null) {
            i5 = 0;
        } else {
            i5 = CodedOutputStream.b(13, oyVar2);
        }
        int i7 = g2 + i5;
        if (oyVar3 != null) {
            i6 = CodedOutputStream.b(14, oyVar3);
        }
        return i7 + i6;
    }

    @DexIgnore
    public static int a(oy oyVar, oy oyVar2) {
        int b2 = CodedOutputStream.b(1, 0) + 0 + CodedOutputStream.b(2, 0) + CodedOutputStream.b(3, oyVar);
        return oyVar2 != null ? b2 + CodedOutputStream.b(4, oyVar2) : b2;
    }

    @DexIgnore
    public static int a(long j, String str, d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, oy oyVar, oy oyVar2, Float f, int i3, boolean z, long j2, long j3, oy oyVar3) {
        long j4 = j;
        int b2 = CodedOutputStream.b(1, j) + 0 + CodedOutputStream.b(2, oy.a(str));
        int a2 = a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2, map, runningAppProcessInfo, i2);
        int a3 = a(f, i3, z, i2, j2, j3);
        int l = b2 + CodedOutputStream.l(3) + CodedOutputStream.j(a2) + a2 + CodedOutputStream.l(5) + CodedOutputStream.j(a3) + a3;
        if (oyVar3 == null) {
            return l;
        }
        int a4 = a(oyVar3);
        return l + CodedOutputStream.l(6) + CodedOutputStream.j(a4) + a4;
    }

    @DexIgnore
    public static int a(d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, oy oyVar, oy oyVar2, Map<String, String> map, ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int a2 = a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2);
        int l = CodedOutputStream.l(1) + CodedOutputStream.j(a2) + a2;
        boolean z = false;
        int i3 = l + 0;
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                int a3 = a((String) next.getKey(), (String) next.getValue());
                i3 += CodedOutputStream.l(2) + CodedOutputStream.j(a3) + a3;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance != 100) {
                z = true;
            }
            i3 += CodedOutputStream.b(3, z);
        }
        return i3 + CodedOutputStream.g(4, i2);
    }

    @DexIgnore
    public static int a(d00 d00, Thread thread, StackTraceElement[] stackTraceElementArr, Thread[] threadArr, List<StackTraceElement[]> list, int i, oy oyVar, oy oyVar2) {
        int a2 = a(thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        int l = CodedOutputStream.l(1) + CodedOutputStream.j(a2) + a2 + 0;
        for (int i2 = 0; i2 < length; i2++) {
            int a3 = a(threadArr[i2], list.get(i2), 0, false);
            l += CodedOutputStream.l(1) + CodedOutputStream.j(a3) + a3;
        }
        int a4 = a(d00, 1, i);
        int a5 = a();
        int a6 = a(oyVar, oyVar2);
        return l + CodedOutputStream.l(2) + CodedOutputStream.j(a4) + a4 + CodedOutputStream.l(3) + CodedOutputStream.j(a5) + a5 + CodedOutputStream.l(3) + CodedOutputStream.j(a6) + a6;
    }

    @DexIgnore
    public static int a(String str, String str2) {
        int b2 = CodedOutputStream.b(1, oy.a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b2 + CodedOutputStream.b(2, oy.a(str2));
    }

    @DexIgnore
    public static int a(Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = 0 + CodedOutputStream.b(1, f.floatValue());
        }
        return i3 + CodedOutputStream.f(2, i) + CodedOutputStream.b(3, z) + CodedOutputStream.g(4, i2) + CodedOutputStream.b(5, j) + CodedOutputStream.b(6, j2);
    }

    @DexIgnore
    public static int a(oy oyVar) {
        return CodedOutputStream.b(1, oyVar);
    }

    @DexIgnore
    public static int a(d00 d00, int i, int i2) {
        int i3 = 0;
        int b2 = CodedOutputStream.b(1, oy.a(d00.b)) + 0;
        String str = d00.a;
        if (str != null) {
            b2 += CodedOutputStream.b(3, oy.a(str));
        }
        int i4 = b2;
        for (StackTraceElement a2 : d00.c) {
            int a3 = a(a2, true);
            i4 += CodedOutputStream.l(4) + CodedOutputStream.j(a3) + a3;
        }
        d00 d002 = d00.d;
        if (d002 == null) {
            return i4;
        }
        if (i < i2) {
            int a4 = a(d002, i + 1, i2);
            return i4 + CodedOutputStream.l(6) + CodedOutputStream.j(a4) + a4;
        }
        while (d002 != null) {
            d002 = d002.d;
            i3++;
        }
        return i4 + CodedOutputStream.g(7, i3);
    }

    @DexIgnore
    public static int a() {
        return CodedOutputStream.b(1, a) + 0 + CodedOutputStream.b(2, a) + CodedOutputStream.b(3, 0);
    }

    @DexIgnore
    public static int a(StackTraceElement stackTraceElement, boolean z) {
        int i;
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i = CodedOutputStream.b(1, (long) Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i = CodedOutputStream.b(1, 0);
        }
        int b2 = i + 0 + CodedOutputStream.b(2, oy.a(stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b2 += CodedOutputStream.b(3, oy.a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b2 += CodedOutputStream.b(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i2 = 2;
        }
        return b2 + CodedOutputStream.g(5, i2);
    }

    @DexIgnore
    public static int a(Thread thread, StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int b2 = CodedOutputStream.b(1, oy.a(thread.getName())) + CodedOutputStream.g(2, i);
        for (StackTraceElement a2 : stackTraceElementArr) {
            int a3 = a(a2, z);
            b2 += CodedOutputStream.l(3) + CodedOutputStream.j(a3) + a3;
        }
        return b2;
    }

    @DexIgnore
    public static oy a(String str) {
        if (str == null) {
            return null;
        }
        return oy.a(str);
    }
}
