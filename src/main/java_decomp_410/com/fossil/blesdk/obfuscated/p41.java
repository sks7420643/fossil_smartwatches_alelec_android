package com.fossil.blesdk.obfuscated;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p41 {
    @DexIgnore
    public static Looper a() {
        bk0.b(Looper.myLooper() != null, "Can't create handler inside thread that has not called Looper.prepare()");
        return Looper.myLooper();
    }

    @DexIgnore
    public static Looper a(Looper looper) {
        return looper != null ? looper : a();
    }
}
