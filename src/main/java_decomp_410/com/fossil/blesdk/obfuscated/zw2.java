package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zw2 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(yw2 yw2) {
        LoaderManager a = yw2.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
