package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dd4 implements he4<Object>, cd4 {
    @DexIgnore
    public /* final */ Class<?> e;

    @DexIgnore
    public dd4(Class<?> cls) {
        kd4.b(cls, "jClass");
        this.e = cls;
    }

    @DexIgnore
    public Class<?> a() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof dd4) && kd4.a((Object) vc4.a(this), (Object) vc4.a((he4) obj));
    }

    @DexIgnore
    public int hashCode() {
        return vc4.a(this).hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString() + " (Kotlin reflection is not available)";
    }
}
