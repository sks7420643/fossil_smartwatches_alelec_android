package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ma3 implements Factory<pa3> {
    @DexIgnore
    public static pa3 a(ka3 ka3) {
        pa3 b = ka3.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
