package com.fossil.blesdk.obfuscated;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.fossil.wearables.fossil.R;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cz2 extends xs3 {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a((fd4) null);
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ pa r; // = new v62(this);
    @DexIgnore
    public tr3<od2> s;
    @DexIgnore
    public b t;
    @DexIgnore
    public HashMap u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return cz2.v;
        }

        @DexIgnore
        public final cz2 b() {
            return new cz2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cz2 e;

        @DexIgnore
        public c(cz2 cz2) {
            this.e = cz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            cz2 cz2 = this.e;
            cz2.onDismiss(cz2.getDialog());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ od2 f;

        @DexIgnore
        public d(cz2 cz2, od2 od2) {
            this.e = cz2;
            this.f = od2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.u;
            kd4.a((Object) imageView, "binding.ivCallsMessageCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.u;
                kd4.a((Object) imageView2, "binding.ivCallsMessageCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.t;
                kd4.a((Object) imageView3, "binding.ivCallsCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.v;
                kd4.a((Object) imageView4, "binding.ivMessagesCheck");
                imageView4.setVisibility(4);
                this.e.o = true;
                this.e.n = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ od2 f;

        @DexIgnore
        public e(cz2 cz2, od2 od2) {
            this.e = cz2;
            this.f = od2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.t;
            kd4.a((Object) imageView, "binding.ivCallsCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.t;
                kd4.a((Object) imageView2, "binding.ivCallsCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.u;
                kd4.a((Object) imageView3, "binding.ivCallsMessageCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.v;
                kd4.a((Object) imageView4, "binding.ivMessagesCheck");
                imageView4.setVisibility(4);
                this.e.n = true;
                this.e.o = false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cz2 e;
        @DexIgnore
        public /* final */ /* synthetic */ od2 f;

        @DexIgnore
        public f(cz2 cz2, od2 od2) {
            this.e = cz2;
            this.f = od2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ImageView imageView = this.f.v;
            kd4.a((Object) imageView, "binding.ivMessagesCheck");
            if (imageView.getVisibility() == 4) {
                ImageView imageView2 = this.f.v;
                kd4.a((Object) imageView2, "binding.ivMessagesCheck");
                imageView2.setVisibility(0);
                ImageView imageView3 = this.f.u;
                kd4.a((Object) imageView3, "binding.ivCallsMessageCheck");
                imageView3.setVisibility(4);
                ImageView imageView4 = this.f.t;
                kd4.a((Object) imageView4, "binding.ivCallsCheck");
                imageView4.setVisibility(4);
                this.e.o = true;
                this.e.n = false;
            }
        }
    }

    /*
    static {
        String simpleName = cz2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void P0() {
        if (this.o && this.n) {
            tr3<od2> tr3 = this.s;
            if (tr3 != null) {
                od2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.u;
                    kd4.a((Object) imageView, "it.ivCallsMessageCheck");
                    imageView.setVisibility(0);
                    ImageView imageView2 = a2.t;
                    kd4.a((Object) imageView2, "it.ivCallsCheck");
                    imageView2.setVisibility(4);
                    ImageView imageView3 = a2.v;
                    kd4.a((Object) imageView3, "it.ivMessagesCheck");
                    imageView3.setVisibility(4);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else if (this.n) {
            tr3<od2> tr32 = this.s;
            if (tr32 != null) {
                od2 a3 = tr32.a();
                if (a3 != null) {
                    ImageView imageView4 = a3.u;
                    kd4.a((Object) imageView4, "it.ivCallsMessageCheck");
                    imageView4.setVisibility(4);
                    ImageView imageView5 = a3.t;
                    kd4.a((Object) imageView5, "it.ivCallsCheck");
                    imageView5.setVisibility(0);
                    ImageView imageView6 = a3.v;
                    kd4.a((Object) imageView6, "it.ivMessagesCheck");
                    imageView6.setVisibility(4);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else {
            tr3<od2> tr33 = this.s;
            if (tr33 != null) {
                od2 a4 = tr33.a();
                if (a4 != null) {
                    ImageView imageView7 = a4.u;
                    kd4.a((Object) imageView7, "it.ivCallsMessageCheck");
                    imageView7.setVisibility(4);
                    ImageView imageView8 = a4.t;
                    kd4.a((Object) imageView8, "it.ivCallsCheck");
                    imageView8.setVisibility(4);
                    ImageView imageView9 = a4.v;
                    kd4.a((Object) imageView9, "it.ivMessagesCheck");
                    imageView9.setVisibility(0);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        od2 od2 = (od2) qa.a(layoutInflater, R.layout.fragment_notification_allow_calls_and_messages, viewGroup, false, this.r);
        od2.w.setOnClickListener(new c(this));
        this.s = new tr3<>(this, od2);
        P0();
        od2.r.setOnClickListener(new d(this, od2));
        od2.q.setOnClickListener(new e(this, od2));
        od2.s.setOnClickListener(new f(this, od2));
        kd4.a((Object) od2, "binding");
        return od2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        if (!(this.p == this.n && this.q == this.o)) {
            b bVar = this.t;
            if (bVar != null) {
                bVar.a(this.m, this.n, this.o);
            }
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        P0();
    }

    @DexIgnore
    public final void a(int i, boolean z, boolean z2) {
        this.m = i;
        this.n = z;
        this.o = z2;
        this.p = z;
        this.q = z2;
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "onDismissListener");
        this.t = bVar;
    }
}
