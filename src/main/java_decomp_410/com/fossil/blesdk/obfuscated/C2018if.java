package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.if */
public class C2018if extends com.fossil.blesdk.obfuscated.C1944hf {
    @DexIgnore
    public C2018if(long j, androidx.renderscript.RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2018if m8372a(androidx.renderscript.RenderScript renderScript, androidx.renderscript.Element element) {
        if (element.mo3172a(androidx.renderscript.Element.m2016h(renderScript)) || element.mo3172a(androidx.renderscript.Element.m2015g(renderScript))) {
            boolean z = renderScript.mo3197d() && android.os.Build.VERSION.SDK_INT < 19;
            com.fossil.blesdk.obfuscated.C2018if ifVar = new com.fossil.blesdk.obfuscated.C2018if(renderScript.mo3175a(5, element.mo10463a(renderScript), z), renderScript);
            ifVar.mo11218a(z);
            ifVar.mo11925a(5.0f);
            return ifVar;
        }
        throw new androidx.renderscript.RSIllegalArgumentException("Unsupported element type.");
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11926b(androidx.renderscript.Allocation allocation) {
        if (allocation.mo3169e().mo3247i() != 0) {
            mo11216a(0, (androidx.renderscript.Allocation) null, allocation, (com.fossil.blesdk.obfuscated.C1800ff) null);
            return;
        }
        throw new androidx.renderscript.RSIllegalArgumentException("Output is a 1D Allocation");
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11927c(androidx.renderscript.Allocation allocation) {
        if (allocation.mo3169e().mo3247i() != 0) {
            mo11217a(1, (com.fossil.blesdk.obfuscated.C1725ef) allocation);
            return;
        }
        throw new androidx.renderscript.RSIllegalArgumentException("Input set to a 1D Allocation");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11925a(float f) {
        if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new androidx.renderscript.RSIllegalArgumentException("Radius out of range (0 < r <= 25).");
        }
        mo11215a(0, f);
    }
}
