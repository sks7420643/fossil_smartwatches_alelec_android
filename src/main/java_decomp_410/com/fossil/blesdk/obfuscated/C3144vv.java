package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vv */
public class C3144vv extends com.fossil.blesdk.obfuscated.C3393yv<android.graphics.Bitmap> {
    @DexIgnore
    public C3144vv(android.widget.ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17218c(android.graphics.Bitmap bitmap) {
        ((android.widget.ImageView) this.f4201e).setImageBitmap(bitmap);
    }
}
