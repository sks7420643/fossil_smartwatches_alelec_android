package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lk */
public abstract class C2331lk<T> implements com.fossil.blesdk.obfuscated.C1810fk<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<java.lang.String> f7230a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: b */
    public T f7231b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C3044uk<T> f7232c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2331lk.C2332a f7233d;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.lk$a */
    public interface C2332a {
        @DexIgnore
        /* renamed from: a */
        void mo12010a(java.util.List<java.lang.String> list);

        @DexIgnore
        /* renamed from: b */
        void mo12012b(java.util.List<java.lang.String> list);
    }

    @DexIgnore
    public C2331lk(com.fossil.blesdk.obfuscated.C3044uk<T> ukVar) {
        this.f7232c = ukVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13301a(com.fossil.blesdk.obfuscated.C2331lk.C2332a aVar) {
        if (this.f7233d != aVar) {
            this.f7233d = aVar;
            mo13304b();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo12371a(com.fossil.blesdk.obfuscated.C1954hl hlVar);

    @DexIgnore
    /* renamed from: b */
    public final void mo13304b() {
        if (!this.f7230a.isEmpty() && this.f7233d != null) {
            T t = this.f7231b;
            if (t == null || mo12373b(t)) {
                this.f7233d.mo12012b(this.f7230a);
            } else {
                this.f7233d.mo12010a(this.f7230a);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public abstract boolean mo12373b(T t);

    @DexIgnore
    /* renamed from: a */
    public void mo13302a(java.util.List<com.fossil.blesdk.obfuscated.C1954hl> list) {
        this.f7230a.clear();
        for (com.fossil.blesdk.obfuscated.C1954hl next : list) {
            if (mo12371a(next)) {
                this.f7230a.add(next.f5771a);
            }
        }
        if (this.f7230a.isEmpty()) {
            this.f7232c.mo16805b(this);
        } else {
            this.f7232c.mo16803a(this);
        }
        mo13304b();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13300a() {
        if (!this.f7230a.isEmpty()) {
            this.f7230a.clear();
            this.f7232c.mo16805b(this);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13303a(java.lang.String str) {
        T t = this.f7231b;
        return t != null && mo12373b(t) && this.f7230a.contains(str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10956a(T t) {
        this.f7231b = t;
        mo13304b();
    }
}
