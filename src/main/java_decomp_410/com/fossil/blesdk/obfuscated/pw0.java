package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pw0 extends vw0 {
    @DexIgnore
    public /* final */ /* synthetic */ mw0 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pw0(mw0 mw0) {
        super(mw0, (nw0) null);
        this.f = mw0;
    }

    @DexIgnore
    public /* synthetic */ pw0(mw0 mw0, nw0 nw0) {
        this(mw0);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new ow0(this.f, (nw0) null);
    }
}
