package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m2 */
public interface C2368m2 {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.m2$a */
    public interface C2369a {
        @DexIgnore
        /* renamed from: a */
        void mo10227a(android.graphics.Rect rect);
    }

    @DexIgnore
    void setOnFitSystemWindowsListener(com.fossil.blesdk.obfuscated.C2368m2.C2369a aVar);
}
