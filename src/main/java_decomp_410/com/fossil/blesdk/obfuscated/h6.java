package com.fossil.blesdk.obfuscated;

import android.app.RemoteInput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h6 {
    @DexIgnore
    public static RemoteInput[] a(h6[] h6VarArr) {
        if (h6VarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[h6VarArr.length];
        if (h6VarArr.length <= 0) {
            return remoteInputArr;
        }
        a(h6VarArr[0]);
        throw null;
    }

    @DexIgnore
    public String a() {
        throw null;
    }

    @DexIgnore
    public static RemoteInput a(h6 h6Var) {
        h6Var.a();
        throw null;
    }
}
