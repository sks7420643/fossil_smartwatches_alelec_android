package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.ls2;
import com.fossil.blesdk.obfuscated.q62;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o23 extends zr2 implements y23, q62.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<ea2> j;
    @DexIgnore
    public x23 k;
    @DexIgnore
    public ls2 l;
    @DexIgnore
    public q62 m;
    @DexIgnore
    public String n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final o23 a() {
            return new o23();
        }

        @DexIgnore
        public final String b() {
            return o23.p;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ls2.b {
        @DexIgnore
        public /* final */ /* synthetic */ o23 a;

        @DexIgnore
        public b(o23 o23) {
            this.a = o23;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "address");
            o23.c(this.a).a(StringsKt__StringsKt.d(str).toString());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ ea2 f;

        @DexIgnore
        public c(View view, ea2 ea2) {
            this.e = view;
            this.f = ea2;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.e.getWindowVisibleDisplayFrame(rect);
            int height = this.e.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.f.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ea2 ea2 = this.f;
                kd4.a((Object) ea2, "binding");
                ea2.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = o23.q.b();
                local.d(b, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = this.f.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "binding.autocompletePlaces");
                appCompatAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o23 e;

        @DexIgnore
        public d(o23 o23, ea2 ea2) {
            this.e = o23;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            q62 a = this.e.m;
            if (a != null) {
                AutocompletePrediction item = a.getItem(i);
                if (item != null) {
                    SpannableString fullText = item.getFullText((CharacterStyle) null);
                    kd4.a((Object) fullText, "item.getFullText(null)");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = p23.q.b();
                    local.i(b, "Autocomplete item selected: " + fullText);
                    this.e.n = fullText.toString();
                    if (this.e.n != null) {
                        String b2 = this.e.n;
                        if (b2 != null) {
                            if (b2.length() > 0) {
                                x23 c = o23.c(this.e);
                                String b3 = this.e.n;
                                if (b3 == null) {
                                    kd4.a();
                                    throw null;
                                } else if (b3 != null) {
                                    c.a(StringsKt__StringsKt.d(b3).toString());
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                                }
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ea2 e;

        @DexIgnore
        public e(o23 o23, ea2 ea2) {
            this.e = ea2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.e.r;
            kd4.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ o23 e;

        @DexIgnore
        public f(o23 o23, ea2 ea2) {
            this.e = o23;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            kd4.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.e.S0();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o23 e;

        @DexIgnore
        public g(o23 o23) {
            this.e = o23;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o23 e;
        @DexIgnore
        public /* final */ /* synthetic */ ea2 f;

        @DexIgnore
        public h(o23 o23, ea2 ea2) {
            this.e = o23;
            this.f = ea2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n = "";
            this.f.q.setText("");
            this.e.T0();
        }
    }

    /*
    static {
        String simpleName = o23.class.getSimpleName();
        kd4.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ x23 c(o23 o23) {
        x23 x23 = o23.k;
        if (x23 != null) {
            return x23;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void B(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void M(boolean z) {
        tr3<ea2> tr3 = this.j;
        if (tr3 != null) {
            ea2 a2 = tr3.a();
            if (a2 != null) {
                if (z) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(appCompatAutoCompleteTextView.getText())) {
                        this.n = null;
                        U0();
                        return;
                    }
                }
                T0();
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        tr3<ea2> tr3 = this.j;
        String str = null;
        if (tr3 == null) {
            kd4.d("mBinding");
            throw null;
        } else if (tr3.a() == null) {
            return true;
        } else {
            x23 x23 = this.k;
            if (x23 != null) {
                String str2 = this.n;
                if (str2 != null) {
                    if (str2 != null) {
                        str = StringsKt__StringsKt.d(str2).toString();
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                x23.a(str);
                return true;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void T0() {
        tr3<ea2> tr3 = this.j;
        if (tr3 != null) {
            ea2 a2 = tr3.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.w;
                kd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void U0() {
        tr3<ea2> tr3 = this.j;
        if (tr3 != null) {
            ea2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                kd4.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.W.c();
                AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(R.string.Customization_Buttons_CommuteTimeDestinationSettings_Text__NothingFoundForInputaddress, new Object[]{appCompatAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.s;
                kd4.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.w;
                kd4.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j(List<String> list) {
        kd4.b(list, "recentSearchedList");
        T0();
        if (!list.isEmpty()) {
            ls2 ls2 = this.l;
            if (ls2 != null) {
                ls2.a((List<String>) kb4.d(list));
            }
            tr3<ea2> tr3 = this.j;
            if (tr3 != null) {
                ea2 a2 = tr3.a();
                if (a2 != null) {
                    LinearLayout linearLayout = a2.w;
                    if (linearLayout != null) {
                        linearLayout.setVisibility(0);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        tr3<ea2> tr32 = this.j;
        if (tr32 != null) {
            ea2 a3 = tr32.a();
            if (a3 != null) {
                LinearLayout linearLayout2 = a3.w;
                if (linearLayout2 != null) {
                    linearLayout2.setVisibility(4);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ea2 ea2 = (ea2) qa.a(layoutInflater, R.layout.fragment_commute_time_settings_default_address, viewGroup, false, O0());
        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = ea2.q;
        appCompatAutoCompleteTextView.setDropDownBackgroundDrawable(k6.c(appCompatAutoCompleteTextView.getContext(), R.drawable.autocomplete_dropdown));
        appCompatAutoCompleteTextView.setOnItemClickListener(new d(this, ea2));
        appCompatAutoCompleteTextView.addTextChangedListener(new e(this, ea2));
        appCompatAutoCompleteTextView.setOnKeyListener(new f(this, ea2));
        ea2.u.setOnClickListener(new g(this));
        ea2.r.setOnClickListener(new h(this, ea2));
        ls2 ls2 = new ls2();
        ls2.a((ls2.b) new b(this));
        this.l = ls2;
        RecyclerView recyclerView = ea2.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        kd4.a((Object) ea2, "binding");
        View d2 = ea2.d();
        kd4.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, ea2));
        this.j = new tr3<>(this, ea2);
        return ea2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        x23 x23 = this.k;
        if (x23 != null) {
            x23.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        x23 x23 = this.k;
        if (x23 != null) {
            x23.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void setTitle(String str) {
        kd4.b(str, "title");
        tr3<ea2> tr3 = this.j;
        if (tr3 != null) {
            ea2 a2 = tr3.a();
            FlexibleTextView flexibleTextView = a2 != null ? a2.t : null;
            if (flexibleTextView != null) {
                kd4.a((Object) flexibleTextView, "mBinding.get()?.ftvTitle!!");
                flexibleTextView.setText(str);
                return;
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void v(String str) {
        kd4.b(str, "address");
        if (isActive()) {
            tr3<ea2> tr3 = this.j;
            if (tr3 != null) {
                ea2 a2 = tr3.a();
                if (a2 != null) {
                    AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                    kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = appCompatAutoCompleteTextView.getText();
                    kd4.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = StringsKt__StringsKt.d(text);
                    boolean z = true;
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(x23 x23) {
        kd4.b(x23, "presenter");
        this.k = x23;
    }

    @DexIgnore
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                this.m = new q62(context, placesClient);
                q62 q62 = this.m;
                if (q62 != null) {
                    q62.a((q62.b) this);
                }
                tr3<ea2> tr3 = this.j;
                if (tr3 != null) {
                    ea2 a2 = tr3.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.m);
                        AppCompatAutoCompleteTextView appCompatAutoCompleteTextView = a2.q;
                        kd4.a((Object) appCompatAutoCompleteTextView, "it.autocompletePlaces");
                        Editable text = appCompatAutoCompleteTextView.getText();
                        kd4.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = StringsKt__StringsKt.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        T0();
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.a();
            throw null;
        }
    }
}
