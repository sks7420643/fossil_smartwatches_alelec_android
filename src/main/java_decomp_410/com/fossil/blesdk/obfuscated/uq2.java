package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uq2 implements Factory<tq2> {
    @DexIgnore
    public static /* final */ uq2 a; // = new uq2();

    @DexIgnore
    public static uq2 a() {
        return a;
    }

    @DexIgnore
    public static tq2 b() {
        return new tq2();
    }

    @DexIgnore
    public tq2 get() {
        return b();
    }
}
