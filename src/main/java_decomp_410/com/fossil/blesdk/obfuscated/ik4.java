package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ik4 {
    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(hk4<?> hk4);

    @DexIgnore
    hk4<?> i();

    @DexIgnore
    int j();
}
