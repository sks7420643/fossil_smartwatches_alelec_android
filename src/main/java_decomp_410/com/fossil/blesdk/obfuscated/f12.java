package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f12 extends l12 {
    @DexIgnore
    public static volatile f12[] f;
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String[] c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;

    @DexIgnore
    public f12() {
        a();
    }

    @DexIgnore
    public static f12[] b() {
        if (f == null) {
            synchronized (k12.a) {
                if (f == null) {
                    f = new f12[0];
                }
            }
        }
        return f;
    }

    @DexIgnore
    public f12 a() {
        this.a = "";
        this.b = "";
        this.c = n12.a;
        this.d = "";
        this.e = "";
        return this;
    }

    @DexIgnore
    public f12 a(j12 j12) throws IOException {
        while (true) {
            int j = j12.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                this.a = j12.i();
            } else if (j == 18) {
                this.b = j12.i();
            } else if (j == 26) {
                int a2 = n12.a(j12, 26);
                String[] strArr = this.c;
                int length = strArr == null ? 0 : strArr.length;
                String[] strArr2 = new String[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, strArr2, 0, length);
                }
                while (length < strArr2.length - 1) {
                    strArr2[length] = j12.i();
                    j12.j();
                    length++;
                }
                strArr2[length] = j12.i();
                this.c = strArr2;
            } else if (j == 34) {
                this.d = j12.i();
            } else if (j == 42) {
                this.e = j12.i();
            } else if (j == 48) {
                j12.c();
            } else if (!n12.b(j12, j)) {
                return this;
            }
        }
    }
}
