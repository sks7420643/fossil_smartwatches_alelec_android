package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public abstract class cw<T extends View, Z> extends uv<Z> {
    @DexIgnore
    public static int j; // = vn.glide_custom_view_target_tag;
    @DexIgnore
    public /* final */ T e;
    @DexIgnore
    public /* final */ a f;
    @DexIgnore
    public View.OnAttachStateChangeListener g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public cw(T t) {
        tw.a(t);
        this.e = (View) t;
        this.f = new a(t);
    }

    @DexIgnore
    public void a(aw awVar) {
        this.f.b(awVar);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        super.b(drawable);
        f();
    }

    @DexIgnore
    public void c(Drawable drawable) {
        super.c(drawable);
        this.f.b();
        if (!this.h) {
            g();
        }
    }

    @DexIgnore
    public ov d() {
        Object e2 = e();
        if (e2 == null) {
            return null;
        }
        if (e2 instanceof ov) {
            return (ov) e2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    public final Object e() {
        return this.e.getTag(j);
    }

    @DexIgnore
    public final void f() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.g;
        if (onAttachStateChangeListener != null && !this.i) {
            this.e.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.i = true;
        }
    }

    @DexIgnore
    public final void g() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.g;
        if (onAttachStateChangeListener != null && this.i) {
            this.e.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.i = false;
        }
    }

    @DexIgnore
    public String toString() {
        return "Target for: " + this.e;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static Integer e;
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ List<aw> b; // = new ArrayList();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public C0009a d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cw$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.cw$a$a  reason: collision with other inner class name */
        public static final class C0009a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public /* final */ WeakReference<a> e;

            @DexIgnore
            public C0009a(a aVar) {
                this.e = new WeakReference<>(aVar);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                a aVar = (a) this.e.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public static int a(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                tw.a(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        @DexIgnore
        public final boolean a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        public final void b(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((aw) it.next()).a(i, i2);
            }
        }

        @DexIgnore
        public final int c() {
            int paddingTop = this.a.getPaddingTop() + this.a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        @DexIgnore
        public final int d() {
            int paddingLeft = this.a.getPaddingLeft() + this.a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        @DexIgnore
        public void b(aw awVar) {
            this.b.remove(awVar);
        }

        @DexIgnore
        public void b() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        @DexIgnore
        public void a() {
            if (!this.b.isEmpty()) {
                int d2 = d();
                int c2 = c();
                if (a(d2, c2)) {
                    b(d2, c2);
                    b();
                }
            }
        }

        @DexIgnore
        public void a(aw awVar) {
            int d2 = d();
            int c2 = c();
            if (a(d2, c2)) {
                awVar.a(d2, c2);
                return;
            }
            if (!this.b.contains(awVar)) {
                this.b.add(awVar);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
                this.d = new C0009a(this);
                viewTreeObserver.addOnPreDrawListener(this.d);
            }
        }

        @DexIgnore
        public final boolean a(int i, int i2) {
            return a(i) && a(i2);
        }

        @DexIgnore
        public final int a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return a(this.a.getContext());
        }
    }

    @DexIgnore
    public void a(ov ovVar) {
        a((Object) ovVar);
    }

    @DexIgnore
    public final void a(Object obj) {
        this.e.setTag(j, obj);
    }

    @DexIgnore
    public void b(aw awVar) {
        this.f.a(awVar);
    }
}
